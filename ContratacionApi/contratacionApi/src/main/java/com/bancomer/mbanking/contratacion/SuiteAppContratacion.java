package com.bancomer.mbanking.contratacion;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.IngresarDatosViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomer.classes.gui.controllers.contratacion.SuiteViewsController;

import android.app.Activity;
import android.content.Context;

import com.bancomer.base.callback.CallBackBConnect;

public class SuiteAppContratacion extends com.bancomer.base.SuiteApp {
	
	public static boolean isSubAppRunning;
	public static Context appContext;
	private static SuiteAppContratacion me;
	private SuiteViewsController suiteViewsController;

	public Activity getActivityToReturn() {
		return activityToReturn;
	}

	public void setActivityToReturn(final Activity activityToReturn) {
		this.activityToReturn = activityToReturn;
	}

	private Activity activityToReturn;
	/**
	 * Intent para regresar a consulta estatus desactivada
	 */
	private static CallBackBConnect consultaEstatusDesactivada;
	/**
	 * Intent para ingresar a contratacion de softtoken
	 */
	//private static BaseViewControllerCommons softtokenContratacion;
	
	public void onCreate(final Context context) {
		super.onCreate(context);
		suiteViewsController = new SuiteViewsController();
		isSubAppRunning = false;
		appContext = context;
		me = this;

		//startBmovilApp();
	};
	
	public void cierraAplicacionSuite() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public SuiteViewsController getSuiteViewsController() {
		return suiteViewsController;
	}

	public static SuiteAppContratacion getInstance() {
		return me;
	}
	
	// #region BmovilApp
	private BmovilApp bmovilApplication;
	
	public BmovilApp getBmovilApplication() {
		if(bmovilApplication == null)
			bmovilApplication = new BmovilApp(this);
		return bmovilApplication;
	}
	
	public static boolean getBmovilStatus() {
		return PropertiesManager.getCurrent().getBmovilActivated();
	}
	
	public void startBmovilApp() {
		bmovilApplication = new BmovilApp(this);
		isSubAppRunning = true;
	}
	
	/*public void cierraAplicacionBmovil() {
		bmovilApplication.cierraAplicacion();
		bmovilApplication = null;
		isSubAppRunning = false;		
		//reiniciaAplicacionBmovil();
	}
	
	public void reiniciaAplicacionBmovil() {
		if(bmovilApplication == null)
			bmovilApplication = new BmovilApp(this);
		bmovilApplication.reiniciaAplicacion();
		isSubAppRunning = true;
	}
	// #endregion
	
	public void closeBmovilAppSession(){
		
		if(suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController)
			((MenuSuiteViewController)suiteViewsController.getCurrentViewControllerApp()).setShouldHideLogin(true);
		
		suiteViewsController.showMenuSuite(true);
		
		
		SuiteAppContratacion suiteApp = SuiteAppContratacion.getInstance();
		Session session = Session.getInstance(suiteApp.getApplicationContext());

		if(session.getValidity() == Session.VALID_STATUS){
			session.setValidity(Session.INVALID_STATUS);
		}
		
		bmovilApplication.closeBmovilAppSession(suiteViewsController.getCurrentViewControllerApp());;
		
	}*/

	/**
	 * @return the consultaEstatusDesactivada
	 */
	public static CallBackBConnect getConsultaEstatusDesactivada() {
		return consultaEstatusDesactivada;
	}

	/**
	 * @param consultaEstatusDesactivada the consultaEstatusDesactivada to set
	 */
	public static void setConsultaEstatusDesactivada(final CallBackBConnect consultaEstatusDesactivada) {
		SuiteAppContratacion.consultaEstatusDesactivada = consultaEstatusDesactivada;
	}

	public static int getResourceId(final String nombre, final String tipo){
		return appContext.getResources().getIdentifier(nombre,tipo, appContext.getPackageName());
	}
	
}
