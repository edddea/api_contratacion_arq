package com.bancomer.mbanking.contratacion;

import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.BmovilViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
//import suitebancomer.classes.gui.controllers.contratacion.MenuSuiteViewController;
import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

@SuppressLint("HandlerLeak")
public class BmovilApp extends BaseSubapplication {

	/**
	 * Message for the handler, indicates that session must be expired.
	 */
	private static final int LOGOUT_MESSAGE = 0;

	/**
	 * The local session validity timer.
	 */
	//private Timer sessionTimer = null;

	/**
	 * Task that manages session logout
	 */
	//public SessionLogoutTask logoutTask;

	public static int STEP_TOTAL = 0;
	public static int STEP_ACTUAL = 0;


	//////////////////////////////////////////////////////////////
	//					Methods									//
	//////////////////////////////////////////////////////////////    

	/**
	 * Constructor básico, se inicializa aqui debido a que no puede extender de un objeto Aplicación, ya que no es posible
	 * que exista mas de uno
	 */
	public BmovilApp(final SuiteAppContratacion suiteApp) {
		super(suiteApp);
		viewsController = new BmovilViewsController(this);
	}

	public void setBmovilViewsController(final BmovilViewsController viewsController) {
		this.viewsController = viewsController;
	}
	
	public BmovilViewsController getBmovilViewsController() {
		return (BmovilViewsController)viewsController;
	}

	/*public void reiniciaAplicacion() {
		if(!(((BmovilViewsController)viewsController).getCurrentViewControllerApp() instanceof MenuSuiteViewController)) 
		{
			((BmovilViewsController)viewsController).cierraViewsController();
			viewsController = new BmovilViewsController(this);
		}

		server.cancelNetworkOperations();
		if (sessionTimer != null) {
			sessionTimer.cancel();
			logoutTask.cancel();
			sessionTimer = null;
			logoutTask = null;
		}
	}*/
	
	/*public Timer getSessionTimer() {
		return sessionTimer;
	}
	public void setSessionTimer(Timer sessionTimer) {
		this.sessionTimer=sessionTimer;
		
	}

	@Override
	public void cierraAplicacion() {
		super.cierraAplicacion();
		
		if (sessionTimer != null) {
			sessionTimer.cancel();
		}
		if (logoutTask != null){
			logoutTask.cancel();
		}
		sessionTimer = null;
		logoutTask = null;
	}

	public void initTimer() {
		sessionTimer = new Timer(true);
		//logoutTask = new SessionLogoutTask();
	}*/
	
	/*public TimerTask getLogoutTask() {
		return logoutTask;
	}*/
	
	//////////////////////////////////////////////////////////////////////////////
	//                Network operations										//
	//////////////////////////////////////////////////////////////////////////////
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * custom texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param progressLabel text with the title of the progress popup
	 * @param progressMessage text with the content of the progress popup
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	@Override
	protected void invokeNetworkOperation(final int operationId,
										  final Hashtable<String, ?> params,
										  final boolean isJson, ParsingHandler handler,
										  final BaseViewControllerCommons caller,
										  final String progressLabel,
										  final String progressMessage,
										  final boolean callerHandlesError) {
		//we stop the timer during server calls
		/*if (logoutTask != null) {
			logoutTask.cancel();
		}*/
		super.invokeNetworkOperation(operationId, params, isJson,handler, caller, progressLabel, progressMessage, callerHandlesError);
	}

	/**
	 * Analyze the network response obtained from the server
	 * @param operationId network identifier. See Server class.
	 * @param response the ServerResponse instance built from the server response. It
	 * contains the type of result (success, failure), and the real content for
	 * the application business.
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	@Override
	protected void analyzeNetworkResponse(final Integer operationId,
										  final ServerResponse response,
										  final Throwable throwable,
										  final BaseViewControllerCommons caller,
										  final boolean callerHandlesError) {

		/*if (operationId.intValue() == Server.CLOSE_SESSION_OPERATION) {

			cancelOngoingNetworkOperations();

			Session.getInstance(SuiteAppContratacion.appContext).setValidity(Session.INVALID_STATUS);
			Session.getInstance(SuiteAppContratacion.appContext).saveRecordStore();
			new Thread(new Runnable() {
				public void run() {
					try {
						System.gc();
						//Session.getInstance(SuiteApp.appContext).storeSession();
						//Incidencia 22218
						
					} catch (Throwable t) {
					} finally {
						suiteApp.getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp().runOnUiThread(new Runnable() {
							public void run() {
								
								// invalid session, request login
								//sessionTimer.cancel();
								if (!applicationInBackground) {
									suiteApp.getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp().ocultaIndicadorActividad();
									((MenuSuiteViewController)suiteApp.getSuiteViewsController().getCurrentViewControllerApp()).setShouldHideLogin(true);
								}
								
								if (!(suiteApp.getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp() instanceof MenuSuiteViewController)) {
									 suiteApp.getSuiteViewsController().showMenuSuite(true);										
								}else{
										MenuSuiteViewController menuSuite = (MenuSuiteViewController) suiteApp.getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp();
//										menuSuite.getLoginViewController().reestableceLogin();
								}
								suiteApp.getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp().hideCurrentDialog();
								
								//suiteApp.cierraAplicacionBmovil();
								if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Si se cerro la sesión");
							}
						});
					}
				}
			}).start();

		} else {*/
			super.analyzeNetworkResponse(operationId, response, throwable, caller, callerHandlesError);
			//resetLogoutTimer();
		//}
	}

	/**
	 * Initiates a session closure process
	 */
	public void closeSession(final boolean isHomeKeyPressed) {
		this.setHomeKeyPressed(isHomeKeyPressed);
		Session session = Session.getInstance(suiteApp.getApplicationContext());
		closeSession(session.getUsername(), session.getIum(), session.getClientNumber());
	}
	
	/**
	 * Initiates a session closure process, cuando se borran los datos y es
	 * necesario hacer el cierre de sesion
	 */
	public void closeSession(final String username, final String ium, final String client) {
		applicationLogged = false;
		
		// close session
		System.gc();
		final Hashtable<String, String> params = new Hashtable<String, String>();

		params.put(Server.USERNAME_PARAM, username);
		params.put(ServerConstants.IUM_ETIQUETA, ium);
		params.put(Server.USER_ID_PARAM, client);
		
		BaseViewControllerCommons caller = null;
		if (viewsController != null) {
			caller =  viewsController.getCurrentViewController();
		}
		//JAIG
		invokeNetworkOperation(Server.CLOSE_SESSION_OPERATION, params,false,null, caller,
				suiteApp.getApplicationContext().getString(R.string.alert_closeSession),
				suiteApp.getApplicationContext().getString(R.string.alert_thank_you), true);
		
		if(Server.ALLOW_LOG) Log.e("CerrarSesion", "La sesion fue cerrada satisfactoriamente.");
	}
	
	public void closeBmovilAppSession(final BaseViewControllerCommons caller) {
		applicationLogged = false;
		final Session session = Session.getInstance(suiteApp.getApplicationContext());
		final String username = session.getUsername();
		final String ium = session.getIum();
		final String client = session.getClientNumber();
				
		// close session
		System.gc();
		final Hashtable<String, String> params = new Hashtable<String, String>();

		params.put(Server.USERNAME_PARAM, username);
		params.put(ServerConstants.IUM_ETIQUETA, ium);
		params.put(Server.USER_ID_PARAM, client);
		
		//BaseViewController caller = null;
		//if (viewsController != null) {
			//caller = viewsController.getCurrentViewController();
		//}

		invokeNetworkOperation(Server.CLOSE_SESSION_OPERATION, params,false,null, caller,
				suiteApp.getApplicationContext().getString(R.string.alert_closeSession),
				suiteApp.getApplicationContext().getString(R.string.alert_thank_you), true);
		
		if(Server.ALLOW_LOG) Log.e("CerrarSesion", "La sesion fue cerrada satisfactoriamente.");
	}

	public int getApplicationStatus() {
		return Session.getInstance(SuiteAppContratacion.appContext).getPendingStatus();
	}
	
	/////////////////////////////////////////////////////////////////////////////
	//                Local session management                                 //
	/////////////////////////////////////////////////////////////////////////////
	/**
	 * Records a user activity event (keystroke), in order to
	 * check session validity
	 */
	public void userActivityEvent() {
		/*resetLogoutTimer();*/
	}

	/**
	 * Logs the user out and discards the current session.
	 */
	/*public synchronized void logoutApp(boolean isHomeKeyPressed){
		Session session = Session.getInstance(suiteApp.getApplicationContext());

		if(session.getValidity() == Session.VALID_STATUS){
			session.setValidity(Session.INVALID_STATUS);
			closeSession(isHomeKeyPressed);
		}
	}*/

	/**
	 * Called whenever the user performs an action or touches the screen
	 */
	/*public void resetLogoutTimer() {
		if (logoutTask != null) {
			logoutTask.cancel();	
		}
		if(sessionTimer != null) {
			logoutTask = new SessionLogoutTask();
			if(Session.getInstance(SuiteAppContratacion.appContext).getValidity() == Session.VALID_STATUS){
				long timeoutMilis = Session.getInstance(SuiteAppContratacion.appContext).getTimeout();
				sessionTimer.schedule(logoutTask, timeoutMilis);
			}
		}
	}*/

	/**
	 * We must use a TimerTask class to execute logout actions after the
	 * timer runs out.
	 */
	/*private class SessionLogoutTask extends TimerTask {

		@Override
		public void run() {
			applicationLogged = false;
			Message msg = new Message();
			msg.what = BmovilApp.LOGOUT_MESSAGE;
			mApplicationHandler.sendMessage(msg);
		}

	}*/

	/**
	 * Use handler to respond to logout messages given by the timer.
	 */
	/*private Handler mApplicationHandler = new Handler() {

		public void handleMessage(Message msg) {

			if(msg.what == BmovilApp.LOGOUT_MESSAGE){
				logoutApp(false);
			}
		}
	};*/


	
}
