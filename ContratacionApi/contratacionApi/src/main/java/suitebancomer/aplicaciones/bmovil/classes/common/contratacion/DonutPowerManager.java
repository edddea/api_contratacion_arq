/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.common.contratacion;

/**
 * Implementation of the App power manager for apis 4 to 6.
 */
public final class DonutPowerManager extends AbstractSuitePowerManager {

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.common.AbstractSuitePowerManager#isScreenOn()
	 */
	@Override
	public boolean isScreenOn() {
		return false;
	}

}
