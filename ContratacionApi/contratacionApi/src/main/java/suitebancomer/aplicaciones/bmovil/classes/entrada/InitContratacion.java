package suitebancomer.aplicaciones.bmovil.classes.entrada;

import android.app.Activity;
import android.content.Context;

import com.bancomer.base.SuiteApp;
import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.mbanking.contratacion.SuiteAppContratacion;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.IngresarDatosViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.DelegateBaseAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.proxys.contratacion.ContratacionAutenticacionServiceProxy;
import suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.handlers.ConfirmacionAutenticacionHandler;
import suitebancomer.aplicaciones.resultados.handlers.ContratacionAutenticacionHandler;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.IngresoDatosSTViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;

/**
 * Created by amgonzaleze on 14/08/2015.
 */
public class InitContratacion {
    final SuiteAppContratacion apiContratacion=new SuiteAppContratacion();
    final BmovilViewsController bmovilViewsController;
    CallBackBConnect callbackSoftoken;

    /**
     * Constructor class
     * @param context
     * @param callback
     * @param activityToReturn
     * @param banderasServer
     */
    public InitContratacion(final Context context, final CallBackBConnect callback, final Activity activityToReturn, final BanderasServer banderasServer) {
        ServerCommons.ALLOW_LOG = banderasServer.getLogActivo();
        ServerCommons.DEVELOPMENT = banderasServer.isDesarrollo();
        ServerCommons.EMULATOR = banderasServer.getEmulador();
        ServerCommons.SIMULATION = banderasServer.getSimulacion();

        //up the context api
        callbackSoftoken = callback;
        apiContratacion.onCreate(context);
        apiContratacion.setActivityToReturn(activityToReturn);
        apiContratacion.setConsultaEstatusDesactivada(callback);
        bmovilViewsController = apiContratacion.getBmovilApplication().getBmovilViewsController();
    }

    public void startContratacionDefinicionPassword(){

         ContratacionDelegate delegate = (ContratacionDelegate) bmovilViewsController.getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
        if (null == delegate) {
            delegate = new ContratacionDelegate();
            bmovilViewsController.addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID, delegate);
        }

        bmovilViewsController.showViewController(ContratacionDefinicionPasswordViewController.class);

    }

    /**
     * init the api
     * @param consultaEstatus
     * @param estatusServicio
     * @param deleteData
     */
    public  void startContratacion(final ConsultaEstatus consultaEstatus,
                                   final String estatusServicio, final boolean deleteData){
        //entrada a contratacion normal
        if(consultaEstatus==null && estatusServicio==null){

            ContratacionDelegate delegate = (ContratacionDelegate) bmovilViewsController.getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
            if (null == delegate) {
                delegate = new ContratacionDelegate();
                bmovilViewsController.addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,
                        delegate);
            }
            bmovilViewsController.showViewController(IngresarDatosViewController.class);
        }else{
            //Entrada al api con valores
            bmovilViewsController.setActivityContext((Activity)apiContratacion.appContext);
            ContratacionDelegate delegate = (ContratacionDelegate) bmovilViewsController.getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
            if (null == delegate) {
                delegate = new ContratacionDelegate();
                bmovilViewsController.addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,
                        delegate);
            }
            delegate.setConsultaEstatus(consultaEstatus);
            delegate.setDeleteData(deleteData);

            if (estatusServicio.equalsIgnoreCase(Constants.STATUS_ENGAGEMENT_UNCOMPLETE)) {
                //entrada a contratacion autenticacion
                if(delegate.reglaNegocioSoftoken()){

                    if (delegate.reglaTokenMovilActivado()){
                        bmovilViewsController.showContratacionAutenticacionPS();
                    }
                    else {
                        Session.getInstance(SuiteAppContratacion.appContext).saveBanderasBMovil(Constants.BANDERAS_CONTRATAR, true, true);
                        SuiteAppApi.getInstanceApi().startSofttokenAppApi();
                        SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(IngresoDatosSTViewController.class);
                    }
                }
                else {
                    bmovilViewsController.showContratacionAutenticacion();
                }
            } else {
                //entrada a ingresa datos
                bmovilViewsController.showViewController(IngresarDatosViewController.class);
            }
        }
    }


    public void startContratacionViewController(final DelegateBaseAutenticacion autenticacionDelegate){
        ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) bmovilViewsController.getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);

        if (delegate != null){
            bmovilViewsController.removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
        }

        delegate = new ContratacionAutenticacionDelegate(autenticacionDelegate);
        bmovilViewsController.addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID, delegate);
        //bmovilViewsController.showViewController(ContratacionAutenticacionViewController.class);

        final Integer idText = delegate.consultaOperationsDelegate().getTextoEncabezado();
        final Integer idNombreImagenEncabezado = delegate.consultaOperationsDelegate().getTextoEncabezado();
        final  Boolean statusDesactivado =false;
        final SuiteAppCRApi cr=new SuiteAppCRApi();
        cr.onCreate(SuiteAppContratacion.appContext);
        final ContratacionAutenticacionServiceProxy proxy = new ContratacionAutenticacionServiceProxy(delegate);
        cr.setProxy(proxy);
        final ConfirmacionAutenticacionHandler handler = new ConfirmacionAutenticacionHandler(
                proxy, bmovilViewsController, new ContratacionAutenticacionViewController() );
        handler.setActivityChanging(bmovilViewsController.isActivityChanging());
        handler.setDelegateId(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
        handler.invokeActivity(bmovilViewsController.estados,idText, idNombreImagenEncabezado, statusDesactivado);
    }

    public void startContratacionAutenticacion(){
        final ContratacionDelegate contrataciondelegate = (ContratacionDelegate) bmovilViewsController.getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
        ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) bmovilViewsController.getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);

        if (delegate != null) {
            bmovilViewsController.removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
        }

        delegate = new ContratacionAutenticacionDelegate(contrataciondelegate);
        bmovilViewsController.addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID, delegate);
        //bmovilViewsController.showViewController(ContratacionAutenticacionViewController.class);

        final Integer idText = delegate.consultaOperationsDelegate().getTextoEncabezado();
        final Integer idNombreImagenEncabezado = delegate.consultaOperationsDelegate().getTextoEncabezado();
        final Boolean statusDesactivado =false;
        final SuiteAppCRApi cr=new SuiteAppCRApi();
        cr.onCreate(SuiteAppContratacion.appContext);
        final ContratacionAutenticacionServiceProxy proxy = new ContratacionAutenticacionServiceProxy(delegate);
        cr.setProxy(proxy);
        final ContratacionAutenticacionHandler handler = new ContratacionAutenticacionHandler(
                proxy, bmovilViewsController, new ContratacionAutenticacionViewController() );
        handler.setActivityChanging(bmovilViewsController.isActivityChanging());
        handler.setDelegateId(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
        handler.invokeActivity(bmovilViewsController.estados,idText, idNombreImagenEncabezado, statusDesactivado);

    }


    public void showViewController(final Class<?> viewController, final int flags, final boolean inverted, final String[] extrasKeys, final Object[] extras) {
        //getBmovilApplicationApi().getBmovilViewsController().showViewController( viewController,  flags,  inverted,  extrasKeys,  extras);
        if(SuiteAppContratacion.getInstance() !=null && SuiteAppContratacion.getInstance().getBmovilApplication().getViewsController().getCurrentViewControllerApp()!=null){
            bmovilViewsController.setCurrentActivityApp(SuiteAppContratacion.getInstance().getBmovilApplication().getViewsController().getCurrentViewControllerApp());
        }
        bmovilViewsController.showViewController(viewController, flags, inverted, extrasKeys, extras);
    }


}
