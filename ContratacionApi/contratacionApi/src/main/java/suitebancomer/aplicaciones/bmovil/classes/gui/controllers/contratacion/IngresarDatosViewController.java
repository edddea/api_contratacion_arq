	package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion;

	import java.util.ArrayList;
	import java.util.List;

	import bancomer.api.common.model.Compania;
	import suitebancomer.aplicaciones.bmovil.classes.common.contratacion.AbstractSuitePowerManager;
	import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionDelegate;
	import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
	import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
	import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
	import suitebancomercoms.classes.common.GuiTools;
	import suitebancomer.classes.gui.controllers.contratacion.BaseViewController;
	import suitebancomer.classes.gui.views.contratacion.ListaDatosViewController;
	import suitebancomer.classes.gui.views.contratacion.SeleccionHorizontalViewController;
	import android.app.ProgressDialog;
	import android.os.Bundle;
	import android.text.Editable;
	import android.text.TextWatcher;
	import android.view.View;
	import android.widget.Button;
	import android.widget.CheckBox;
	import android.widget.EditText;
	import android.widget.ImageButton;
	import android.widget.ImageView;
	import android.widget.LinearLayout;
	import android.widget.LinearLayout.LayoutParams;
	import android.widget.TextView;

	import com.bancomer.base.SuiteApp;
	import com.bancomer.mbanking.contratacion.R;
	import com.bancomer.mbanking.contratacion.SuiteAppContratacion;
	import com.bbva.apicuentadigital.common.APICuentaDigital;
	//import com.bbva.apicuentadigital.common.APICuentaDigital;

	/**
	 * Controlador de la pantalla para ingresar datos.
	 */
	public class IngresarDatosViewController extends BaseViewController {
		/**
		 * Delegado de la pantalla.
		 */
		private ContratacionDelegate delegate;

		/**
		 * Componente Lista Datos.
		 */
		private ListaDatosViewController listaDatos;

		/**
		 * Componente Seleccion Horizontal para elegir un compa?ia de celular.
		 */
		private SeleccionHorizontalViewController seleccionHorizontal;

		/**
		 * Campo de texto para el numero de tarjeta.
		 */
		private EditText tbNumeroTarjeta;

		/**
		 * Texto para el numero de celular.
		 */

		private TextView lblNumero;

		/**
		 * EditText para el campo contraseña.
		 */

		private EditText txtContrasena;

		/**
		 * EditText para el campo confirma contraseña..
		 */

		private EditText txtRepite;

		/**
		 * Icono autoriza
		 */

		private ImageView imAutoriza;

		/**
		 * Icono ayuda contraseña
		 */

		private ImageButton imAyudaContrasena;

		/**
		 * Botón continuar
		 */

		private Button btnContinuar;

	/**
	 * Campo checkbox para selecci?n del tipo de usuario
	 */
	// CGI-Modif Contratacion
	//	private CheckBox checkboxToken;

		/**
		 * Bandera para indicar si se debe de regresar a la pantalla de inicio en el evento onResume.
		 */
		private boolean goBackToHome;

		/**
		 * Bandera para indicar si vino de IngresaDatosST.
		 */
		private boolean cameFromIngresaDatosST = false;
		//AMZ
		public ArrayList<String> estados = new ArrayList<String>();
		public IngresarDatosViewController(){
			super();

		}

		public IngresarDatosViewController(final boolean cameFromIngresaDatosST){
			super();
			this.cameFromIngresaDatosST = cameFromIngresaDatosST;
		}

		/* (non-Javadoc)
		* @see suitebancomer.classes.gui.controllers.BaseViewController#onCreate(android.os.Bundle)
		*/
		protected void onCreate(final Bundle savedInstanceState) {
			super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE,
					getResources().getIdentifier("layout_bmovil_ingresar_datos_api", "layout", getPackageName()));
			/*setTitle(getResources().getIdentifier("bmovil.contratacion.titulo", "string", getPackageName()),
			getResources().getIdentifier("icono_activacion", "drawable", getPackageName()));*/ //titulo
			SuiteApp.appContext = this;
			setParentViewsController(SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController());
			setDelegate((ContratacionDelegate) parentViewsController.getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID));
			delegate = (ContratacionDelegate)getDelegate();
			delegate.setOwnerController(this);
			delegate.setPaso(ContratacionDelegate.AvanceContratacion.IngresarNumTarjeta);
			delegate.setPaso(ContratacionDelegate.AvanceContratacion.TokenUsuario);
			init();

			TrackingHelper.trackState("ingresodatos", estados);
		}

		/* (non-Javadoc)
		* @see android.app.Activity#onPause()
		*/
		@Override
		protected void onPause() {
			super.onPause();
			goBackToHome = !AbstractSuitePowerManager.getSuitePowerManager().isScreenOn();
		}

		/* (non-Javadoc)
		* @see android.app.Activity#onResume()
		*/
		@Override
		protected void onResume() {
			super.onResume();
			SuiteApp.appContext = this;
			if(goBackToHome) {
			//SuiteAppContratacion.getInstance().getSuiteViewsController().showMenuSuite(true);
				SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(this);
				SuiteAppContratacion.getConsultaEstatusDesactivada().returnToActivity(SuiteAppContratacion.appContext,SuiteAppContratacion.getInstance().getActivityToReturn(), true);
			//SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(SuiteAppContratacion.getConsultaEstatusDesactivada().getClass());
			}
			getParentViewsController().setCurrentActivityApp(this);
			delegate.setOwnerController(this);
		}

		/**
		 * Inicializa los elementos de la pantalla.
		 */
		private void init() {
			findViews();
			cargarComponenteListaDatos(); // Lista de datos muestra Numero celular de usuario
			//cargarComponenteSeleccionHorizontal(); // Pobla linear de compañias telefónicas
			//scaleToCurrentScreen();
			goBackToHome = false;
			// CGI-Modif Contratacion
			//	checkboxToken.setChecked(false);

			if(delegate.isDeleteData()) {
				delegate.deleteData();
			}

			tbNumeroTarjeta.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

				}

				@Override
				public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
					if(delegate.validaCampos(txtContrasena.getText().toString(),txtRepite.getText().toString(),tbNumeroTarjeta.getText().toString(),false))
						successData();
					else {
						missingData();
					}
				}

				@Override
				public void afterTextChanged(Editable editable) {

				}
			});

			txtContrasena.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

				}

				@Override
				public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
					if(delegate.validaCampos(txtContrasena.getText().toString(),txtRepite.getText().toString(),tbNumeroTarjeta.getText().toString(),false))
						successData();
					else {
						missingData();
					}
				}

				@Override
				public void afterTextChanged(Editable editable) {

				}
			});

			txtRepite.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

				}

				@Override
				public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
					if(delegate.validaCampos(txtContrasena.getText().toString(),txtRepite.getText().toString(),tbNumeroTarjeta.getText().toString(),false))
						successData();
					else {
						missingData();
					}
				}

				@Override
				public void afterTextChanged(Editable editable) {

				}
			});
		}

		/**
		 * Busca todas las referencias necesarias para el control de la pantalla.
		 */
		private void findViews() {
			tbNumeroTarjeta = (EditText)findViewById(SuiteAppContratacion.getResourceId("textNumTarjeta", "id"));
			lblNumero = (TextView) findViewById(R.id.lblgetNumero);
			txtContrasena = (EditText) findViewById(R.id.txtContrasena);
			txtRepite = (EditText) findViewById(R.id.txtRepite);
			imAutoriza = (ImageView) findViewById(R.id.imAutoriza);
			imAyudaContrasena = (ImageButton) findViewById(R.id.imAyudaContrasena);
			btnContinuar = (Button) findViewById(R.id.btnContinuar);


	// CGI-Modif Contratacion
	//	checkboxToken = (CheckBox)findViewById(R.id.switchConToken);
		}

		/**
		 * Interactúa con UI de acuerdo al estatus de ingreso de datos listo
		 */

		public void successData(){
			imAutoriza.setImageResource(R.drawable.icon_palima_activa);
			btnContinuar.setBackgroundResource(R.drawable.btn_confirmar);
			imAyudaContrasena.setImageResource(R.drawable.icon_ojo_verde);
		}

		/**
		 *Interactúa con UI de acuerdo al estatus de ingreso de datos incompleto
		 */

		public void missingData(){
			imAutoriza.setImageResource(R.drawable.icon_paloma_inactiva);
			btnContinuar.setBackgroundResource(R.drawable.btn_confirmar_inactivo);
			imAyudaContrasena.setImageResource(R.drawable.icon_ojo_magenta);
		}

		/**
		 * Escala los elementos de la pantalla para la resoluci?n actual.
		 */
		private void scaleToCurrentScreen() {
			final GuiTools guiTools = GuiTools.getCurrent();
			guiTools.init(getWindowManager());
			guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("layoutListaDatos", "id")));
			guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("lblCompaniaCelular", "id")), true);
			guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("lblNumeroTarjeta", "id")), true);
			guiTools.scale(tbNumeroTarjeta, true);
			guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("btnContinuar", "id")), false);
			//	guiTools.scale(findViewById(R.id.lblInstruccionesNumeroTarjeta), true);
			//	guiTools.scale(findViewById(R.id.LayoutOperarToken));
			//	guiTools.scale(findViewById(R.id.lblBancaMovilToken), true);
			//	guiTools.scale(findViewById(R.id.informacionBmovil), false);
			//	guiTools.scale(findViewById(R.id.switchConToken), false);
		}

		/**
		 * Carga el componente lista datos con los elementos necesarios.
		 */
		private void cargarComponenteListaDatos() {
			final ArrayList<Object> datos = delegate.cargarElementosListaDatos();

			final LinearLayout.LayoutParams layoutParams = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

			// Llena lista de datos (Número de celular y etiqueta) //////////
			/*listaDatos = new ListaDatosViewController(SuiteAppContratacion.appContext, layoutParams, getParentViewsController());
			listaDatos.setTitulo(SuiteAppContratacion.getResourceId("bmovil.contratacion.ingresar.datos.ingresar.datos", "string"));
			listaDatos.setNumeroCeldas(2);
			listaDatos.setLista(datos);
			listaDatos.setNumeroFilas(datos.size());
			listaDatos.showLista();

			final LinearLayout listaDatosLayout = (LinearLayout)findViewById(SuiteAppContratacion.getResourceId("layoutListaDatos", "id"));
			listaDatosLayout.addView(listaDatos);*/

			//////////////////////////////////////***************************************/////////
			List<String> userData = ((List<String>)datos.get(datos.size() - 1));
			lblNumero.setText(userData.get(userData.size() - 1));
		}

		/**
		 * Carga el componente seleccion horizontal con los elementos necesarios.
		 */
		private void cargarComponenteSeleccionHorizontal() {
			final LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			final ArrayList<Object> companias = delegate.cargarCompaniasSeleccionHorizontal();
			seleccionHorizontal = new SeleccionHorizontalViewController(this, params, companias, getDelegate(), false);

			final LinearLayout seleccionHorizontalLayout = (LinearLayout)findViewById(SuiteAppContratacion.getResourceId("layoutSeleccionHorizontal", "id"));
			seleccionHorizontalLayout.addView(seleccionHorizontal);
		}

		/* (non-Javadoc)
		* @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
		*/
		@Override
		public void processNetworkResponse(final int operationId, final ServerResponse response) {
			delegate.analyzeResponse(operationId, response);
		}

		/**
		 * Intenta continuar con el flujo del caso de uso.
		 * @param sender La vista que invoca este método.
		 */
		public void botonContinuarClick(final View sender) {
			// CGI-Modif Contratacion
			//delegate.validaCampos(seleccionHorizontal.getSelectedItem(), tbNumeroTarjeta.getText().toString(), false);//checkboxToken.isChecked()); //Contratación flujo con compañia celular
			if(delegate.validaCampos(txtContrasena.getText().toString(),txtRepite.getText().toString(),tbNumeroTarjeta.getText().toString(),false))
			delegate.validaCampos(txtContrasena.getText().toString(),txtRepite.getText().toString(),tbNumeroTarjeta.getText().toString(),true);
		}

		/**
		 * llamar al api cuenta digital
		 * @param sender La vista que invoca este método.*/

		public void botonQuieroClick(final View sender) {
			final Compania compania= (Compania)seleccionHorizontal.getSelectedItem();
			if(compania!=null){
				final String celular= delegate.getConsultaEstatus().getNumCelular();
				final String companiaTel=compania.getNombre();
				final APICuentaDigital getInstance = new APICuentaDigital();

				getInstance.ejecutaAPICuentaDigital(this,celular, companiaTel, Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR, Server.ALLOW_LOG);

			}else{
				showErrorMessage(R.string.valida_compania_telefonica_contratacion);
			}

		}

		/**
		 * Carga la pantalla de informaci?n sobre tipos de usuario
		 */
			//IDS CAMBIOS EA#14
		//public void botonInformacionClick(final View sender) {
		//	delegate.mostrarInformacion();
		//}

			/**
			 * Places a progress dialog, for long waiting processes.
			 *
			 * @param strTitle the dialog title
			 * @param strMessage the message to show in the while
			 */
		@Override
		public void muestraIndicadorActividad(final String strTitle, final String strMessage) {
			if(!cameFromIngresaDatosST){
				if(mProgressDialog != null){
					ocultaIndicadorActividad();
				}
				mProgressDialog = ProgressDialog.show(this, strTitle,
						strMessage, true);
				mProgressDialog.setCancelable(false);
			}else{
				cameFromIngresaDatosST=false;
			}
		}

		public ContratacionDelegate getContratacionDelegate() {
			return delegate;
		}

		public void setContratacionDelegate(final ContratacionDelegate delegate) {
			this.delegate = delegate;
		}
	}
