package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion;

import suitebancomer.aplicaciones.bmovil.classes.common.contratacion.AbstractSuitePowerManager;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionDelegate.AvanceContratacion;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.contratacion.BaseViewController;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.contratacion.SuiteAppContratacion;

/**
 * Controlador para la pantalla de ingresar contraseña.
 */
public class ContratacionDefinicionPasswordViewController extends BaseViewController {
	/**
	 * Delegado de la pantalla
	 */
	private ContratacionDelegate delegate;
	
	/**
	 * Campo de texto para ingresar la contraseña.
	 */
	private EditText tbIngresarPwd;
	
	/**
	 * Campo de texto pra confirmar la contraseña.
	 */
	private EditText tbConfirmarPwd;
	
	/**
	 * Campo de texto para ingresar el email.
	 */
	private EditText tbIngresarEmail;
	
	/**
	 * Campo de texto para confirmar el email.
	 */
	private EditText tbConfirmarEmail;
	
	/**
	 * Bandera para indicar si se debe de regresar al men� se Suite en el onResume.
	 */
	private boolean goBackToHome;
	
	//AMZ
	private BmovilViewsController parentManager;
	//public BaseViewController bvc;
	

	public ContratacionDefinicionPasswordViewController() {
		super();
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#onCreate(android.os.Bundle)
	 */
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, SuiteAppContratacion.getResourceId("layout_bmovil_contratacion_confirma_password", "layout"));
		SuiteApp.appContext = this;
		setTitle(SuiteAppContratacion.getResourceId("bmovil.contratacion.titulo", "string"), SuiteAppContratacion.getResourceId("icono_activacion", "drawable"));
		//AMZ
		parentManager = SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("contratacion pass", parentManager.estados);
	//	(BaseViewController)parentManager.auxEstados = parentManager.estados;
		
		SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(this);
		
		setParentViewsController(SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController());
		if(parentViewsController.getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID) instanceof suitebancomer.aplicaciones.bmovil.classes.gui.delegates.token.ContratacionDelegate)
		{
			suitebancomer.aplicaciones.bmovil.classes.gui.delegates.token.ContratacionDelegate delegateToken=(suitebancomer.aplicaciones.bmovil.classes.gui.delegates.token.ContratacionDelegate)parentViewsController.getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
			final ContratacionDelegate delegateAux=new ContratacionDelegate();
			final ConsultaEstatus estatus=delegateToken.getConsultaEstatus();
			delegateAux.setConsultaEstatus(estatus);
			delegateAux.setDeleteData(delegateToken.getDeleteData());
			delegateAux.setEscenarioAlternativoEA11(false);

			delegateAux.setOwnerController(this);
			parentViewsController.removeDelegateFromHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
			parentViewsController.addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID, delegateAux);
			delegateAux.validaCamposST(estatus.getCompaniaCelular(),
					estatus.getNumTarjeta(), true);
		}
		setDelegate((ContratacionDelegate)parentViewsController.getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID));
		
		delegate = (ContratacionDelegate)getDelegate();
		delegate.setOwnerController(this);
		delegate.setPaso(AvanceContratacion.IngresarPassword);
		
		init();
	}
	
	/**
	 * Inicializa los elementos de la pantalla.
	 */
	private void init() {
		findViews();
		scaleToCurrentScreen();
		goBackToHome = false;
		
	}
	
	/**
	 * Busca todas las referencias necesarias para el control de la pantalla.
	 */
	private void findViews() {
		tbIngresarPwd = (EditText)findViewById(SuiteAppContratacion.getResourceId("tbIntroducePassword", "id"));
		tbConfirmarPwd = (EditText)findViewById(SuiteAppContratacion.getResourceId("tbConfirmaPassword", "id"));
		tbIngresarEmail = (EditText)findViewById(SuiteAppContratacion.getResourceId("tbIngresarEmail", "id"));
		tbConfirmarEmail = (EditText)findViewById(SuiteAppContratacion.getResourceId("tbConfirmarEmail", "id"));
	}
	
	/**
	 * Escala los elementos de la pantalla para la resoluci�n actual.
	 */
	private void scaleToCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("lblIntroducePassword", "id")), true);
		guiTools.scale(tbIngresarPwd, true);
		guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("lblConfirmaPassword", "id")), true);
		guiTools.scale(tbConfirmarPwd, true);
		guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("lblIngresarEmail", "id")), true);
		guiTools.scale(tbIngresarEmail, true);
		guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("lblAvisoEmail", "id")), true);
		guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("lblConfirmarEmail", "id")), true);
		guiTools.scale(tbConfirmarEmail, true);
		guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("botonContinuar", "id")), false);
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		super.onPause();
		goBackToHome = true;
//		parentViewsController.consumeAccionesDePausa();
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		if(goBackToHome) {
			//SuiteAppContratacion.getInstance().getSuiteViewsController().showMenuSuite(true);
			SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(this);
			SuiteAppContratacion.getConsultaEstatusDesactivada().returnToActivity(SuiteAppContratacion.appContext, SuiteAppContratacion.getInstance().getActivityToReturn(), true);

			//SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(SuiteAppContratacion.getConsultaEstatusDesactivada().getClass());
		}
		
//		if (parentViewsController.consumeAccionesDeReinicio()) {
//			return;
//		}
		getParentViewsController().setCurrentActivityApp(this);
		delegate.setOwnerController(this);
	}

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(final int operationId,final ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}

	/**
	 * Intenta continuar con el flujo del caso de uso.
	 * @param sender La vista que invoca este método.
	 */
	public void botonContinuarClick(final View sender) {
		delegate.validaCampos(tbIngresarPwd.getText().toString(),
				tbConfirmarPwd.getText().toString(),
				tbIngresarEmail.getText().toString(),
				tbConfirmarEmail.getText().toString());
	}
	
	@Override
	public void goBack() {
		goBackToHome = !AbstractSuitePowerManager.getSuitePowerManager().isScreenOn();
		super.goBack();
	}

	@Override
	public void onBackPressed() {
		TrackingHelper.touchAtrasState();
	}
}
