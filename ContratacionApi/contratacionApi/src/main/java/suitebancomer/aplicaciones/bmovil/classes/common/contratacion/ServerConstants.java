package suitebancomer.aplicaciones.bmovil.classes.common.contratacion;

/**
 * Constantes para la parte servidora de la aplicacion.
 * 
 * @author CGI
 */
public class ServerConstants {
	
	public static final String NUMERO_TELEFONO_ETIQUETA = "NT";
	public static final String CONTRASENA_ETIQUETA = "NP";
	public static final String IUM_ETIQUETA = "IU";
	public static final String JSON_IUM_ETIQUETA = "IUM";
	public static final String NUMERO_CLIENTE_ETIQUETA = "TE";
	public static final String CUENTA_CARGO_ETIQUETA = "CC";
	public static final String IMPORTE_ETIQUETA = "IM";
	public static final String NOMBRE_BENEFICIARIO_ETIQUETA = "BF";
	public static final String CONCEPTO_ETIQUETA = "CP";
	public static final String NUMERO_CELULAR_TERCERO_ETIQUETA = "NU";
	public static final String OPERADORA_TELEFONIA_CELULAR_ETIQUETA = "OA";
	public static final String NIP_ETIQUETA = "NI";
	public static final String CVV_ETIQUETA = "CV";
	public static final String OTP_TOKEN_ETIQUETA = "OT";
	public static final String INSTRUCCION_VALIDACION_ETIQUETA = "VA";
	public static final String PERIODO_ETIQUETA= "PE";

	
	public static final String CODIGO_TRANSACCION= "codTrans";
	
	/** Constante del numero de telefono. */
	public static final String NUMERO_TELEFONO = "numeroTelefono";
	
	/** Constante del numero de tarjeta. */
    public static final String NUMERO_TARJETA = "numeroTarjeta";
    
    /** Constante de la compania celular. */
    public static final String COMPANIA_CELULAR = "companiaCelular";
    
    /** Constante del numero celular compania nuevo. */
    public static final String COMPANIA_CELULAR_NUEVO = "companiaCelularNueva";
    
    /** Constante numero celular nuevo.*/
    public static final String NUMERO_CELULAR_NUEVO = "numeroCelNuevo";
    
    /** Constante del estatus de Bmovil. */
    public static final String ESTATUS_BMOVIL = "estatusBmovil";

    /** Constante de la validacion del numero de telefono y compannia, de alertas y de Bmovil. */
    public static final String VALIDACION_ALERTAS = "validacionAlertas";

    /** Constante del numero de telufono asociado a Alertas. */
    public static final String NUMERO_ALERTAS = "numeroAlertas";

    /** Constante de la compannia telefonica asociada a Alertas. */
    public static final String COMPANNIA_ALERTAS = "companiaAlertas";

    /** Constante del numero del cliente. */
    public static final String NUMERO_CLIENTE = "numeroCliente";

    /** Constante del indicador de Softtoken. */
    public static final String INDICADOR_CONTRATACION = "indicadorContratacion";

    /** Constante del numero de serie de Softtoken. */
    public static final String NUM_SERIE_TOKEN = "numSerieToken";

    /** Constante del tipo de instrumento. */
    public static final String TIPO_INSTRUMENTO = "tipoInstrumento";

    /** Constante del correo electronico del cliente. */
    public static final String CORREO_ELECTRONICO = "correoElectronico";

    /** Constante del nombre del cliente. */
    public static final String NOMBRE_CLIENTE = "nombreCliente";
    
    /** Constante del cve de acceso. */
    public static final String CVE_ACCESO = "cveAcceso";
    
    /** Constante del codigo nip. */
    public static final String CODIGO_NIP = "codigoNIP";
    
    /** Constante del codigo otp. */
    public static final String CODIGO_OTP = "codigoOTP";
    
    /** Constante de la Version APP */
    public static final String VERSION_APP = "versionApp";
    
    /** Constante de la Version */
    public static final String VERSION = "version";
    
    /** Constante del codigo cvv2. */
    public static final String CODIGO_CVV2 = "codigoCVV2";
    
    /** Constante del codigo cvv2. */
    public static final String CODIGO_CVV = "codigoCVV";
    
    /** Constante de la cadena de autenticacion. */
    public static final String CADENA_AUTENTICACION = "cadenaAutenticacion";
			
    /** Constante del perfil del cliente. */
    public static final String PERFIL_CLIENTE = "perfilCliente";
    
    /** Constante del perfil del cliente. */
    public static final String EMAIL_CLIENTE = "emailCliente";
    
    /** Constante del perfil*/
    public static final String EMAIL = "email";
    
    /** Constante de acepto terminos y condiciones. */
    public static final String ACEPTO_TERMINOS_CONDICIONES = "aceptoTerminosCondiciones";
    
    /** Constante del tipo de mantenimiento de alerta. */
    public static final String INDICADOR = "indicador";
    
    /** Constante de los cinco digitos de la tarjeta . */
    public static final String TARJETA_5DIG = "tarjeta5Dig";
    	
	/** Constante del folio. */
	public static final String FOLIO = "folio";
	
	/** Constante de la fecha. */
	public static final String FECHA = "fecha";
	
	/** Constante de la hora. */
	public static final String HORA = "hora";
	
	/** Constante del switchEnrolamiento. */
	public static final String SWITCH_ENROLAMIENTO = "switchEnrolamiento";
	
	/** Constante del dispositivoFisico. */
	public static final String DISPOSITIVO_FISICO = "dispositivoFisico";

	/** Constante del estatusDispositivo. */
	public static final String ESTATUS_DISPOSITIVO = "estatusDispositivo";
	
	/** Constante de nuevo perfil */
	public static final String PERFIL_NUEVO_ETIQUETA = "perfilNuevo";
	

	/** Constante del estatusDispositivo. */
	public static final String VERSION_MIDLET = "VM";
	
	public static final String APP_VERSION_CONSULTA = "100";
	
	public static final String VERSION_FLUJO = "versionFlujo";
	
	//SPEI
	//CÛdigo Alex
	public static final String CELLPHONENUMBER_PARAM = "TelefonoDeposito";
	
	public static final String PERIODO = "periodo";
	
	//O3
	public static final String IUM="IUM";
	public static final String CUENTA="numeroCuenta";
	public static final String TIPO_CUENTA="tipoCuenta";
	public static final String TIPO_OPERACION="tipoOperacion";
	//public static final String PERIODO="periodo";
	public static final String NUMERO_CELULAR="numeroCelular";
	public static final String NUM_MOVTO = "numMovto";
	public static final String REFERENCIA_INTERNA = "referenciaInterna";
	public static final String REFERENCIA_AMPLIADA = "referenciaAmpliada";

	// Importes TDC
	public static final String OPERACION = "operacion";
	
	
	// Sinc Exp ST
	public static final String TIPO_SOLICITUD = "tipoSolicitud";
	public static final String OTP1 = "OTP1";
	public static final String OTP2 = "OTP2";
	public static final String NOMBRE_TOKEN = "nombreToken";
	
	//PARAMETROS
		public static final String PARAMS_TEXTO_NT= "NT";
		public static final String PARAMS_TEXTO_NP= "NP";
		public static final String PARAMS_TEXTO_IU= "IU";
		public static final String PARAMS_TEXTO_TE= "TE";
		public static final String PARAMS_TEXTO_FO= "FO";
		public static final String PARAMS_TEXTO_CG= "CG";
		public static final String PARAMS_TEXTO_FE= "FE";
		public static final String PARAMS_TEXTO_NI= "NI";
		public static final String PARAMS_TEXTO_CV= "CV";
		public static final String PARAMS_TEXTO_OT= "OT";
		public static final String PARAMS_TEXTO_VA= "VA";
		public static final String PARAMS_TEXTO_DE= "DE";
		public static final String PARAMS_TEXTO_CA= "CA";
		public static final String PARAMS_TEXTO_RF= "RF";
		public static final String PARAMS_TEXTO_TP= "TP";
		public static final String PARAMS_TEXTO_CP= "CP";
		public static final String PARAMS_TEXTO_OA= "OA";
		public static final String PARAMS_TEXTO_IT= "IT";
	
	
}