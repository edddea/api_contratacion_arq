package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion;

import java.util.ArrayList;

import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.handlers.ConfirmacionAutenticacionHandler;
import suitebancomer.aplicaciones.resultados.handlers.ContratacionAutenticacionHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.DelegateBaseAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.DelegateBaseOperacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.proxys.contratacion.ContratacionAutenticacionServiceProxy;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomer.classes.gui.controllers.contratacion.BaseViewController;
import suitebancomer.classes.gui.controllers.contratacion.BaseViewsController;


import com.bancomer.mbanking.contratacion.BmovilApp;
import com.bancomer.mbanking.contratacion.SuiteAppContratacion;

public class BmovilViewsController extends BaseViewsController {

	private BmovilApp bmovilApp;
	//AMZ
	public ArrayList<String> estados = new ArrayList<String>();
	
	/*
	 * @Override public void setCurrentActivity(BaseViewController
	 * currentViewController) { // TODO Auto-generated method stub
	 * super.setCurrentActivity(currentViewController); Log.i("Si", "No"); }
	 */
	public BmovilViewsController(final BmovilApp bmovilApp) {
		super();
		this.bmovilApp = bmovilApp;
	}

	/*public void cierraViewsController() {
		bmovilApp = null;
		clearDelegateHashMap();
		super.cierraViewsController();
	}

	public void cerrarSesionBackground() {
		SuiteAppContratacion.getInstance().getBmovilApplication()
				.setApplicationInBackground(true);
		bmovilApp.logoutApp(true);
	}*/

//	@Override
//	public void showMenuInicial() {
//		showMenuPrincipal();
//	}

//	public void showMenuPrincipal() {
//		showMenuPrincipal(false);
//	}

//	public void showMenuPrincipal(boolean inverted) {
//
//		//One CLick
//		MenuPrincipalDelegate delegate = (MenuPrincipalDelegate) getBaseDelegateForKey(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new MenuPrincipalDelegate(new Promociones());
//			addDelegateToHashMap(
//					MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID, delegate);
//		}		
//		//Termina One click
//
//		showViewController(MenuPrincipalViewController.class,
//				Intent.FLAG_ACTIVITY_CLEAR_TOP, inverted);
//		SuiteAppContratacion.getInstance().getBmovilApplication()
//				.setApplicationLogged(true);
//	}

	public void showLogin() {
//		MenuSuiteViewController menuSuiteViewController = (MenuSuiteViewController) getCurrentViewControllerApp();
//		LoginDelegate loginDelegate = (LoginDelegate) getBaseDelegateForKey(LoginDelegate.LOGIN_DELEGATE_ID);
//		if (loginDelegate == null) {
//			loginDelegate = new LoginDelegate();
//			addDelegateToHashMap(LoginDelegate.LOGIN_DELEGATE_ID, loginDelegate);
//		}
//		loginDelegate.setParentViewController(menuSuiteViewController);
//		if (menuSuiteViewController.getLoginViewController() == null) {
//			LoginViewController loginViewController = new LoginViewController(
//					menuSuiteViewController, this);
//			loginViewController.setDelegate(loginDelegate);
//			menuSuiteViewController.setLoginViewController(loginViewController);
//			menuSuiteViewController.getBaseLayout()
//					.addView(loginViewController);
//			loginViewController.setVisibility(View.GONE);
//		}
//
//		menuSuiteViewController.plegarOpcion();
//		//AMZ
//		estados.clear();
//		TrackingHelper.trackState("login", estados);
//		//AMZ
				
	}

	public void showCambioPerfil() {
//		CambioPerfilDelegate delegate = (CambioPerfilDelegate) getBaseDelegateForKey(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new CambioPerfilDelegate();
//			addDelegateToHashMap(
//					CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID, delegate);
//		}
//		showViewController(CambioPerfilViewController.class);
	}

	public BmovilApp getBmovilApp() {
		return bmovilApp;
	}

	public void setBmovilApp(final BmovilApp bmovilApp) {
		this.bmovilApp = bmovilApp;
	}

	@Override
	public void onUserInteraction() {
		
		/*if (bmovilApp != null) {
			if(SuiteAppContratacion.getInstance().getBmovilApplication().getSessionTimer()==null){
				bmovilApp.initTimer();
			}
			bmovilApp.resetLogoutTimer();
		}*/
		super.onUserInteraction();
	}

	@Override
	public boolean consumeAccionesDeReinicio() {
		if (!SuiteAppContratacion.getInstance().getBmovilApplication()
				.isApplicationLogged()) {
			/*((MenuSuiteViewController) SuiteAppContratacion.getInstance()
					.getSuiteViewsController().getCurrentViewControllerApp())
					.setShouldHideLogin(true);
			SuiteAppContratacion.getInstance().getSuiteViewsController()
					.showMenuSuite(true);*/
			return true;
		}
		return false;
	}

	@Override
	public boolean consumeAccionesDePausa() {
		if (SuiteAppContratacion.getInstance().getBmovilApplication().isApplicationLogged()
				&& !isActivityChanging()) {
			//cerrarSesionBackground();
			return true;
		}
		return false;
	}

	@Override
	public boolean consumeAccionesDeAlto() {
		if (!SuiteAppContratacion.getInstance().getBmovilApplication().isChangingActivity()
				&& currentViewControllerApp != null) {
			currentViewControllerApp.hideSoftKeyboard();
		}
		SuiteAppContratacion.getInstance().getBmovilApplication()
				.setChangingActivity(false);
		return true;
	}

	/**
	 * Muestra la pantalla de confirmacion autenticacion
	 */
//	public void showConfirmacionAutenticacionViewController(
//			DelegateBaseAutenticacion autenticacionDelegate, int resIcon,
//			int resTitle, int resSubtitle) {
//		showConfirmacionAutenticacionViewController(autenticacionDelegate,
//				resIcon, resTitle, resSubtitle, R.color.primer_azul);
//	}

	/**
	 * Muestra la pantalla de confirmacion autenticacion
	 */
//	public void showConfirmacionAutenticacionViewController(
//			DelegateBaseAutenticacion autenticacionDelegate, int resIcon,
//			int resTitle, int resSubtitle, int resTitleColor) {
//		ConfirmacionAutenticacionDelegate confirmacionDelegate = (ConfirmacionAutenticacionDelegate) getBaseDelegateForKey(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
//		if (confirmacionDelegate != null) {
//			removeDelegateFromHashMap(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
//		}
//		confirmacionDelegate = new ConfirmacionAutenticacionDelegate(
//				autenticacionDelegate);
//		addDelegateToHashMap(
//				ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID,
//				confirmacionDelegate);
//		//AMZ
//				if(estados.size() == 0)
//				{
//					//AMZ
//					TrackingHelper.trackState("reactivacion", estados);
//					//AMZ
//				}
//		showViewController(ConfirmacionAutenticacionViewController.class);
//	}
	
	public void showResultadosViewControllerWithoutFreq(
			final DelegateBaseOperacion delegateBaseOperacion, final int resIcon,
			final int resTitle) {
//		ResultadosDelegate resultadosDelegate = (ResultadosDelegate) getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
//		if (resultadosDelegate != null) {
//			removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
//		}
//		resultadosDelegate = new ResultadosDelegate(delegateBaseOperacion);
//		resultadosDelegate.setFrecOpOK(true);
//		addDelegateToHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID,
//				resultadosDelegate);
//		showViewController(ResultadosViewController.class);
	}

	public void showOpcionesTransferir(final String tipoOperacion, final int resIcon,
									   final int resTitle, final int resSubtitle) {
		final String[] llaves = new String[4];
		final Object[] valores = new Object[4];

		llaves[0] = Constants.PANTALLA_BASE_ICONO;
		valores[0] = Integer.valueOf(resIcon);
		llaves[1] = Constants.PANTALLA_BASE_TITULO;
		valores[1] = Integer.valueOf(resTitle);
		llaves[2] = Constants.PANTALLA_BASE_SUBTITULO;
		valores[2] = Integer.valueOf(resSubtitle);
		llaves[3] = Constants.PANTALLA_BASE_TIPO_OPERACION;
		valores[3] = String.valueOf(tipoOperacion);

//		TransferirDelegate delegate = (TransferirDelegate) getBaseDelegateForKey(TransferirDelegate.TRANSFERIR_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new TransferirDelegate();
//			addDelegateToHashMap(TransferirDelegate.TRANSFERIR_DELEGATE_ID,
//					delegate);
//		}
//
//		showViewController(OpcionesTransferViewController.class, 0, false,
//				llaves, valores);
	}

	/**
	 * Ejecuta flujo
	 * 
	 */
	public void showContratacionEP11(final BaseViewController ownerController, final ConsultaEstatus consultaEstatus,
									 final String estatusServicio, final boolean deleteData) {

		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionDelegate();
			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,
					delegate);
		}

		delegate.setEscenarioAlternativoEA11(false);
		delegate.setConsultaEstatus(consultaEstatus);
		delegate.setDeleteData(deleteData);
		final IngresarDatosViewController ingresarDatosViewController = new IngresarDatosViewController(
				true);
		ingresarDatosViewController.setDelegate(delegate);
		ingresarDatosViewController.setContratacionDelegate(delegate);
		ingresarDatosViewController.setParentViewsController(SuiteAppContratacion
				.getInstance().getBmovilApplication()
				.getBmovilViewsController());
		
		//showViewController(IngresarDatosViewController.class);
		
	
		
		SuiteAppContratacion.getInstance().getBmovilApplication()
				.getBmovilViewsController()
				.setCurrentActivityApp(ownerController);

		delegate.setOwnerController(ownerController);
		
		delegate.validaCamposST(consultaEstatus.getCompaniaCelular(),
				consultaEstatus.getNumTarjeta(), true);
	}


	/**
	 * Muestra la pantalla de ingresar datos de contratación.
	 */
	public void showContratacion() {
		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionDelegate();
			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,
					delegate);
		}
		showViewController(IngresarDatosViewController.class);

	}

	/**
	 * Muestra la pantalla de definir contraseña la contratación.
	 */
	public void showDefinirPassword() {		
		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionDelegate();
			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID, delegate);
		}

		showViewController(ContratacionDefinicionPasswordViewController.class);
	}

	/**
	 * Muestra la pantalla de configurar alertas.
	 */
//	public void showConfigurarAlertas() {
//		ConfigurarAlertasDelegate delegate = (ConfigurarAlertasDelegate) getBaseDelegateForKey(ConfigurarAlertasDelegate.CONFIGURAR_ALERTAS_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new ConfigurarAlertasDelegate();
//			addDelegateToHashMap(
//					ConfigurarAlertasDelegate.CONFIGURAR_ALERTAS_DELEGATE_ID,
//					delegate);
//		}
//
//		showViewController(ConfigurarAlertasViewController.class);
//	}

	/**
	 * Muestra la pantalla de aplicación desactivada.
	 */
	public void showConsultaEstatusAplicacionDesactivada() {

		SuiteAppContratacion.getConsultaEstatusDesactivada().returnToActivity(SuiteAppContratacion.appContext, SuiteAppContratacion.getInstance().getActivityToReturn(),true);
		//showViewController(SuiteAppContratacion.getConsultaEstatusDesactivada().getClass());
	}

	/**
	 * Muestra la pantalla de informaci�n contratacion con y sin Token.
	 */
    //IDS CAMBIOS EA#14
	//public void showAyudaContratacionTokens() {
	//	showViewController(AyudaContratacionTokensViewController.class);
	//}

	/**
	 * Muestra la pantalla de confirmaci�n autenticación para el estatus PS.
	 */
	public void showContratacionAutenticacion() {
		final ContratacionDelegate contrataciondelegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
		ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);

		if (delegate != null){
			removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		}
		
		delegate = new ContratacionAutenticacionDelegate(contrataciondelegate);
		addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID, delegate);

		//Llamado anterior
		//showViewController(ContratacionAutenticacionViewController.class);

		final Integer idText = delegate.consultaOperationsDelegate().getTextoEncabezado();
		final Integer idNombreImagenEncabezado = delegate.consultaOperationsDelegate().getNombreImagenEncabezado();
		final Boolean statusDesactivado =false;
		/*
				Boolean statusDesactivado = !( (confirmacionDelegate.getOperationDelegate() instanceof
				suitebancomer.aplicaciones.bmovil.classes.gui.delegates.QuitarSuspencionDelegate )
				|| (confirmacionDelegate.getOperationDelegate() instanceof
				suitebancomer.aplicaciones.bmovil.classes.gui.delegates.NuevaContraseniaDelegate
				|| (confirmacionDelegate.getOperationDelegate() instanceof
				suitebancomercoms.aplicaciones.bmovil.classes.gui.delegates.ReactivacionDelegate) ));
		*/
		final SuiteAppCRApi cr=new SuiteAppCRApi();
		cr.onCreate(SuiteAppContratacion.appContext);
		final ContratacionAutenticacionServiceProxy proxy = new ContratacionAutenticacionServiceProxy(delegate);
		cr.setProxy(proxy);
		final ContratacionAutenticacionHandler handler = new ContratacionAutenticacionHandler(
				proxy, this, new ContratacionAutenticacionViewController() );
		handler.setActivityChanging(isActivityChanging());
		handler.setDelegateId(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		handler.invokeActivity(this.estados,idText, idNombreImagenEncabezado, statusDesactivado);

	}

	/**
	 * Muestra la pantalla de confirmacion autenticacion
	 */
	public void showContratacionAutenticacion(final DelegateBaseAutenticacion autenticacionDelegate) {
		ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		
		if (delegate != null){
			removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		}
		
		delegate = new ContratacionAutenticacionDelegate(autenticacionDelegate);
		addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID, delegate);
		//showViewController(ContratacionAutenticacionViewController.class);

		final Integer idText = delegate.consultaOperationsDelegate().getTextoEncabezado();
		final Integer idNombreImagenEncabezado = delegate.consultaOperationsDelegate().getNombreImagenEncabezado();
		final Boolean statusDesactivado =false;
		/*
				Boolean statusDesactivado = !( (confirmacionDelegate.getOperationDelegate() instanceof
				suitebancomer.aplicaciones.bmovil.classes.gui.delegates.QuitarSuspencionDelegate )
				|| (confirmacionDelegate.getOperationDelegate() instanceof
				suitebancomer.aplicaciones.bmovil.classes.gui.delegates.NuevaContraseniaDelegate
				|| (confirmacionDelegate.getOperationDelegate() instanceof
				suitebancomercoms.aplicaciones.bmovil.classes.gui.delegates.ReactivacionDelegate) ));
		*/
		final SuiteAppCRApi cr=new SuiteAppCRApi();
		cr.onCreate(SuiteAppContratacion.appContext);
		ContratacionAutenticacionServiceProxy proxy = new ContratacionAutenticacionServiceProxy(delegate);
		cr.setProxy(proxy);
		final ContratacionAutenticacionHandler handler = new ContratacionAutenticacionHandler(
				proxy, this, new ContratacionAutenticacionViewController() );
		handler.setActivityChanging(isActivityChanging());
		handler.setDelegateId(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		handler.invokeActivity(this.estados,idText, idNombreImagenEncabezado, statusDesactivado);

	}

	/**
	 * Muestra los terminos y condiciones de uso.
	 * 
	 * @param terminosDeUso
	 *            Terminos de uso.
	 */
	public void showTerminosDeUso(final String terminosDeUso) {
		showViewController(TerminosCondicionesViewController.class, 0, false,
				new String[] { Constants.TERMINOS_DE_USO_EXTRA },
				new Object[] { terminosDeUso });
	}

    /**
     * Muestra la pantalla de confirmacion autenticación para el estatus PS.
     */
    public void showContratacionAutenticacionPS() {
        final ContratacionDelegate contrataciondelegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
        ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);

        if (delegate != null) {
           removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
        }

        delegate = new ContratacionAutenticacionDelegate(contrataciondelegate);
        addDelegateToHashMap(
                ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID,
                delegate);
        //bmovilViewsController.showViewController(ContratacionAutenticacionViewController.class);


        final Integer idText = delegate.consultaOperationsDelegate().getTextoEncabezado();
        final Integer idNombreImagenEncabezado = delegate.consultaOperationsDelegate().getTextoEncabezado();
        final Boolean statusDesactivado =false;
        final SuiteAppCRApi cr=new SuiteAppCRApi();
        cr.onCreate(SuiteAppContratacion.appContext);
        final ContratacionAutenticacionServiceProxy proxy = new ContratacionAutenticacionServiceProxy(delegate);
        cr.setProxy(proxy);
        final ContratacionAutenticacionHandler handler = new ContratacionAutenticacionHandler(
                proxy, this, new ContratacionAutenticacionViewController() );
        handler.setActivityChanging(isActivityChanging());
        handler.setDelegateId(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
        handler.invokeActivity(estados,idText, idNombreImagenEncabezado, statusDesactivado);
    }

	public void showContratacionAutenticacionViewController(DelegateBaseAutenticacion autenticacionDelegate) {

		ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);

		if (delegate != null){
			removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		}

		delegate = new ContratacionAutenticacionDelegate(autenticacionDelegate);
		addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID, delegate);


		showViewController(ContratacionAutenticacionViewController.class);
	}

	/**
	 * Muestra la pantalla de confirmacion para transferencias.
	 * 
	 * @param delegateBaseOperacion
	 *            EL DelegateBaseOperacion que manda a llamar la pantalla de
	 *            confirmaci�n.
	 */
//	public void showConfirmacionRegistro(
//			DelegateBaseOperacion delegateBaseOperacion) {
//		// El delegado de confirmaci�n se crea siempre, esto ya que el delegado
//		// que contiene el internamente cambia segun quien invoque este método.
//		ConfirmacionRegistroDelegate delegate = (ConfirmacionRegistroDelegate) getBaseDelegateForKey(ConfirmacionRegistroDelegate.CONFIRMACION_REGISTRO_DELEGATE_DELEGATE_ID);
//		if (delegate != null) {
//			removeDelegateFromHashMap(ConfirmacionRegistroDelegate.CONFIRMACION_REGISTRO_DELEGATE_DELEGATE_ID);
//		}
//		delegate = new ConfirmacionRegistroDelegate(delegateBaseOperacion);
//		addDelegateToHashMap(
//				ConfirmacionRegistroDelegate.CONFIRMACION_REGISTRO_DELEGATE_DELEGATE_ID,
//				delegate);
//		showViewController(ConfirmacionRegistroViewController.class);
//	}

	/**
	 * Shows the terms and conditions screen.
	 * @param terminosDeUso Terms and conditions HTML.
	 * @param title The title resource identifier.
	 * @param icon The icon recource identifier.
	 */
	public void showTermsAndConditions(final String termsHtml, final int title, final int icon) {
		final String[] keys = {Constants.TERMINOS_DE_USO_EXTRA, Constants.TITLE_EXTRA, Constants.ICON_EXTRA};
		final Object[] values = {termsHtml, Integer.valueOf(title), Integer.valueOf(icon)};
		showViewController(TerminosCondicionesViewController.class, 0, false, keys, values);
	}
}