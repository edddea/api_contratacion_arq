package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.contratacion.BaseViewController;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.contratacion.SuiteAppContratacion;

public class AyudaContratacionTokensViewController extends BaseViewController {
	//AMZ
	private BmovilViewsController parentManager;
	/** Titulos de los apartados de ayuda */
	private TextView tituloBancaMovilConToken;
	private TextView tituloBancaMovilSinToken;
	
	/** Encabezados de los apartados de ayuda */
	private TextView encabezadoBancaMovilConToken;
	private TextView encabezadoBancaMovilSinTokenConAlertas;
	private TextView encabezadoBancaMovilSinTokenSinAlertas;
	
	/** Textos de ayuda */
	private TextView textoBancaMovilConToken;
	private TextView textoBancaMovilSinTokenConAlertas;
	private TextView textoBancaMovilSinTokenSinAlertas;
	
	/** Mensaje de activación de alertas */
	private TextView mensajeActivaAlertas;

	/** Boton de aceptar. */
	private Button mButtonAceptar;
	
	/**
	 * Delegado de la pantalla.
	 */
	private ContratacionDelegate delegate;	

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, SuiteAppContratacion.getResourceId("layout_bmovil_ayuda_contratacion_tokens", "layout"));
		SuiteApp.appContext = this;
		setTitle(SuiteAppContratacion.getResourceId("bmovil.contratacion.titulo", "string"), SuiteAppContratacion.getResourceId("icono_activacion", "drawable"));
		setParentViewsController(SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate((ContratacionDelegate) parentViewsController.getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID));
		delegate = (ContratacionDelegate)getDelegate();
		delegate.setOwnerController(this);
		init();

		parentManager = SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("ayudacontratacion", parentManager.estados);
	}
	
	/**
	 * Inicializa la pantalla
	 */
	private void init(){
		findViews();
		setMessages();
		scaleToScreenSize();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	/**
	 * Vuelve a la pantalla anterior.
	 * @param sender La vista que invoca este método.
	 */
	public void botonContinuarClick(final View sender) {
		super.onBackPressed();
	}	
	
	/**
	 * Inicializa las vistas de la pantalla.
	 */
	private void findViews() {
		tituloBancaMovilConToken = (TextView) findViewById(SuiteAppContratacion.getResourceId("ayuda_contratacion_contoken_titulo", "id"));
		encabezadoBancaMovilConToken = (TextView) findViewById(SuiteAppContratacion.getResourceId("ayuda_contratacion_contoken_encabezado", "id"));
		textoBancaMovilConToken = (TextView) findViewById(SuiteAppContratacion.getResourceId("ayuda_contratacion_contoken_texto", "id"));
		tituloBancaMovilSinToken = (TextView) findViewById(SuiteAppContratacion.getResourceId("ayuda_contratacion_sintoken_titulo", "id"));
		encabezadoBancaMovilSinTokenConAlertas = (TextView) findViewById(SuiteAppContratacion.getResourceId("ayuda_contratacion_sintoken_conalertas_encabezado", "id"));
		textoBancaMovilSinTokenConAlertas = (TextView) findViewById(SuiteAppContratacion.getResourceId("ayuda_contratacion_sintoken_conalertas_texto", "id"));
		encabezadoBancaMovilSinTokenSinAlertas = (TextView) findViewById(SuiteAppContratacion.getResourceId("ayuda_contratacion_sintoken_sinalertas_encabezado", "id"));
		textoBancaMovilSinTokenSinAlertas = (TextView) findViewById(SuiteAppContratacion.getResourceId("ayuda_contratacion_sintoken_sinalertas_texto", "id"));
		mensajeActivaAlertas = (TextView) findViewById(SuiteAppContratacion.getResourceId("ayuda_contratacion_activaalertas_mensaje", "id"));
 
		mButtonAceptar = (Button) findViewById(SuiteAppContratacion.getResourceId("btnContinuar", "id"));
	}

	/**
	 * Escala el layout al tama�o de la pantalla.
	 */
	private void scaleToScreenSize() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(tituloBancaMovilConToken, true);
		guiTools.scale(encabezadoBancaMovilConToken, true);
		guiTools.scale(textoBancaMovilConToken, true);
		guiTools.scale(tituloBancaMovilSinToken, true);
		guiTools.scale(encabezadoBancaMovilSinTokenConAlertas, true);
		guiTools.scale(textoBancaMovilSinTokenConAlertas, true);
		guiTools.scale(encabezadoBancaMovilSinTokenSinAlertas, true);
		guiTools.scale(textoBancaMovilSinTokenSinAlertas, true);
		guiTools.scale(mensajeActivaAlertas, true);
		
		guiTools.scale(mButtonAceptar);
	}

	/**
	 * Establece los mensajes de informaci�n que se mostrarán
	 */
	private void setMessages() {
		tituloBancaMovilConToken.setText(SuiteAppContratacion.getResourceId("bmovil.informacion.contratacion.contoken.titulo", "string"));
		encabezadoBancaMovilConToken.setText(SuiteAppContratacion.getResourceId("bmovil.informacion.contratacion.contoken.encabezado", "string"));
		textoBancaMovilConToken.setText(SuiteAppContratacion.getResourceId("bmovil.informacion.contratacion.contoken.texto", "string"));
		tituloBancaMovilSinToken.setText(SuiteAppContratacion.getResourceId("bmovil.informacion.contratacion.sintoken.titulo", "string"));
		encabezadoBancaMovilSinTokenConAlertas.setText(SuiteAppContratacion.getResourceId("bmovil.informacion.contratacion.sintoken.conalertas.encabezado", "string"));
		textoBancaMovilSinTokenConAlertas.setText(SuiteAppContratacion.getResourceId("bmovil.informacion.contratacion.sintoken.conalertas.texto", "string"));
		encabezadoBancaMovilSinTokenSinAlertas.setText(SuiteAppContratacion.getResourceId("bmovil.informacion.contratacion.sintoken.sinalertas.encabezado", "string"));
		textoBancaMovilSinTokenSinAlertas.setText(SuiteAppContratacion.getResourceId("bmovil.informacion.contratacion.sintoken.sinalertas.texto", "string"));
		mensajeActivaAlertas.setText(SuiteAppContratacion.getResourceId("bmovil.informacion.contratacion.activaalertas.mensaje", "string"));
	}
}
