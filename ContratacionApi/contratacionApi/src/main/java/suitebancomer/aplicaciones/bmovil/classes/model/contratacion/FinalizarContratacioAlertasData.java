package suitebancomer.aplicaciones.bmovil.classes.model.contratacion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by alanmichaelgonzalez on 22/12/15.
 */
public class FinalizarContratacioAlertasData implements ParsingHandler {

    private String estado;
    private String estadoAlertas;
    private String mensajeAlertas;
    private String ivr;

    public String getEstado() {
        return estado;
    }

    public void setEstado(final String estado) {
        this.estado = estado;
    }

    public String getEstadoAlertas() {
        return estadoAlertas;
    }

    public void setEstadoAlertas(final String estadoAlertas) {
        this.estadoAlertas = estadoAlertas;
    }

    public String getMensajeAlertas() {
        return mensajeAlertas;
    }

    public void setMensajeAlertas(final String mensajeAlertas) {
        this.mensajeAlertas = mensajeAlertas;
    }

    public String getIvr() {
        return ivr;
    }

    public void setIvr(final String ivr) {
        this.ivr = ivr;
    }


    @Override
    public void process(final Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(final ParserJSON parser) throws IOException, ParsingException {

    }
}
