package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion;

import java.util.HashMap;
import java.util.Map;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;


import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.contratacion.BaseViewController;

import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.webkit.WebView;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.contratacion.SuiteAppContratacion;

public class TerminosCondicionesViewController extends BaseViewController {
	/**
	 * Campo de texto para terminos de uso.
	 */
	private WebView wvTerminos;
	//AMZ
		private BmovilViewsController parentManager;
		
		//ARR
		Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
		
	
	public TerminosCondicionesViewController() {
		wvTerminos = null;
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppContratacion.getResourceId("layout_bmovil_terminos_y_condiciones", "layout"));
		SuiteApp.appContext = this;
		setParentViewsController(SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController());
		setTitle(SuiteAppContratacion.getResourceId("bmovil.contratacion.titulo", "string"),SuiteAppContratacion.getResourceId("icono_contratacion", "drawable"));
		findViews();
		scaleToScreenSize();
		final String terminos = (String)this.getIntent().getExtras().get(Constants.TERMINOS_DE_USO_EXTRA);
		if(null != terminos) {
			if(Build.VERSION.SDK_INT < 15){
				wvTerminos.loadDataWithBaseURL(null, terminos, "text/html", "utf-8", null);
			} else {
				wvTerminos.loadData(terminos, "text/html", "utf-8");
			}
		}
		//AMZ
				parentManager = SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController();						
				TrackingHelper.trackState("condiciones", parentManager.estados);
	}

	/**
	 * Busca las referencias a las vistas.
	 */
	private void findViews() {
		wvTerminos = (WebView)findViewById(SuiteAppContratacion.getResourceId("webViewTerminos", "id"));
	}
	
	/**
	 * Escala las vistas para acomodarse a la pantalla actual.
	 */
	private void scaleToScreenSize() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("layoutBaseContainer", "id")));
		guiTools.scale(findViewById(SuiteAppContratacion.getResourceId("lblTitulo", "id")), true);
		guiTools.scale(wvTerminos);
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		SuiteApp.appContext = this;
		setParentViewsController(SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController());
		SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(this);
		super.onResume();
	}

	@Override
	public boolean dispatchTouchEvent(final MotionEvent ev) {
		final float touchX = ev.getX();
		final float touchY = ev.getY();
		final int[] webViewPos = new int[2];
		
		wvTerminos.getLocationOnScreen(webViewPos);

		final float listaSeleccionX2 = webViewPos[0] + wvTerminos.getMeasuredWidth();
		final float listaSeleccionY2 = webViewPos[1] + wvTerminos.getMeasuredHeight();
		
		if ((touchX >= webViewPos[0] && touchX <= listaSeleccionX2) &&
			(touchY >= webViewPos[1] && touchY <= listaSeleccionY2)) {
			wvTerminos.getParent().requestDisallowInterceptTouchEvent(true);
		}
		
		return super.dispatchTouchEvent(ev);
	}
}
