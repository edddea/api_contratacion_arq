package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;
import java.util.regex.Pattern;

import suitebancomer.aplicaciones.bmovil.classes.model.contratacion.FinalizarContratacioAlertasData;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.CatalogoAutenticacionFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.EncriptarConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.SessionStoredListener;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionAutenticacionViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import bancomer.api.common.model.CatalogoVersionado;
import bancomer.api.common.model.Compania;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatusMantenimientoData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaGetStatusData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaTarjetaContratacionData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Contratacion;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ContratacionBmovilData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.RespuestaConfirmacion;
import suitebancomercoms.aplicaciones.bmovil.classes.model.TemporalST;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.IngresoDatosSTViewController;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomer.classes.gui.controllers.contratacion.BaseViewController;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.contratacion.R;
import com.bancomer.mbanking.contratacion.SuiteAppContratacion;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

/**
 * Delegado para la contratacion de Bmovil.
 */
public class ContratacionDelegate extends DelegateBaseAutenticacion implements
        SessionStoredListener {
    /**
     * Enumeraci?n con los pasos del flujo de contratación.
     */
    public enum AvanceContratacion {
        IngresarNumTarjeta, IngresarPassword, TokenUsuario
    }

    // #region Variables.
    /**
     * Identificador ?nico del delegado.
     */
    public static final long CONTRATACION_DELEGATE_ID = 0x68b4fd27c67e239dL;

    /**
     * Email matching pattern regex.
     */
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    /**
     * Codigos de operacion a ejecutar dependiendo del escenario alterno de
     * Contratacion
     */
    private static final String TIPO_PERSONA_NO_PERMITIDO = "EA16";


    /**
     * Estado Alerta
     */

    private static final String TEXTO_ESTADO_ALERTA = "Alertas";//"Estado Alerta";

    /**
     * Tipo de operación actual.
     */
    private Operacion tipoOperacion;

    /**
     * Controlador actual.
     */
    private BaseViewControllerCommons ownerController;

    /**
     * Avance del proceso de contratacion.
     */
    private AvanceContratacion paso;

    /**
     * Modelo con datos básicos de contratación.
     */

    private Contratacion contratacion;
    /**
     * Referencia a la consulta de estatus de la aplicación.
     */
    private ConsultaEstatus consultaEstatus;

    /**
     * Respuesta del servidor para la operación de consultar tarjeta.
     */
    private ConsultaTarjetaContratacionData consultaTarjetaResponse;

    /**
     * Respuesta del servidor para la operación de Contratación de Bmovil.
     */
    private ContratacionBmovilData contratacionBmovilResponse;

    /**
     * Bandera para indicar si se debe de borrar los datos de sesion.
     */
    private boolean deleteData;

    /**
     * Parametros de respuesta de la pantalla de confirmaci?n.
     */
    private RespuestaConfirmacion respuestaConfirmacion;

    /**
     * Bandera para decidir que logica realizar tras confirmar en la pantalla
     * Definicion contrasennas.
     */
    private boolean escenarioAlternativoEA11 = false;

    private String NO_ALERTS = "NO";

    // #endregion

    // #region Setters y Getters

    /**
     * @param ownerController El controlador actual a establecer.
     */
    public void setOwnerController(final BaseViewController ownerController) {
        this.ownerController = ownerController;
    }

    /**
     * @return El avance del flujo de contratacion.
     */
    public AvanceContratacion getPaso() {
        return paso;
    }

    /**
     * @param paso El avance del flujo de contratacion.
     */
    public void setPaso(final AvanceContratacion paso) {
        this.paso = paso;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.classes.gui.delegates.BaseDelegate#getDelegateIdentifier()
     */
    @Override
    public long getDelegateIdentifier() {
        return CONTRATACION_DELEGATE_ID;
    }

    /**
     * @return La respuesta del servidor para la operación de consultar tarjeta.
     */
    public ConsultaTarjetaContratacionData getConsultaTarjetaResponse() {
        return consultaTarjetaResponse;
    }

    /**
     * @return El modelo de datos para el flujo de contratación.
     */
    public Contratacion getContratacion() {
        return contratacion;
    }

    /**
     * @return Referencia a la consulta de estatus de la aplicación.
     */
    public ConsultaEstatus getConsultaEstatus() {
        return consultaEstatus;
    }

    /**
     * @param consultaEstatus Referencia a la consulta de estatus de la aplicación.
     */
    public void setConsultaEstatus(final ConsultaEstatus consultaEstatus) {
        this.consultaEstatus = consultaEstatus;
        cargarDatosDeConsultaEstatus();
    }

    /**
     * @return Tipo de operación actual.
     */
    public Operacion getTipoOperacion() {
        return tipoOperacion;
    }

    /**
     * @param tipoOperacion Tipo de operación actual.
     */
    public void setTipoOperacion(final Operacion tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    /**
     * @return Respuesta del servidor para la operación de Contratación de
     * Bmovil.
     */
    public ContratacionBmovilData getContratacionBmovilResponse() {
        return contratacionBmovilResponse;
    }

    /**
     * @param deleteData Bandera para indicar si se debe de borrar los datos de sesion.
     */
    public void setDeleteData(final boolean deleteData) {
        this.deleteData = deleteData;
    }

    /**
     * @return Bandera para indicar si se debe de borrar los datos de sesion.
     */
    public boolean isDeleteData() {
        return deleteData;
    }

    // #endregion

    /**
     * Constructor por defecto.
     */
    public ContratacionDelegate() {
        super();

        tipoOperacion = Operacion.contratacion;
        paso = AvanceContratacion.IngresarNumTarjeta;
        contratacion = new Contratacion();
        consultaEstatus = null;
        consultaTarjetaResponse = null;
        contratacionBmovilResponse = null;
        ownerController = null;
        deleteData = false;
    }

    // #region Copiado de datos.

    /**
     * Copia los datos necesarios del objeto "consulta estatus" hacia el objeto
     * "contratación".
     *
     * @throws NullPointerException Si alguno de los dos objetos es nulo.
     */
    public void cargarDatosDeConsultaEstatus() throws NullPointerException {
        if (null == consultaEstatus)
            throw new NullPointerException(
                    "Consulta estatus no puede ser nulo.");
        if (null == contratacion)
            throw new NullPointerException("Contratacion no puede ser nulo.");

        contratacion.setPerfil(consultaEstatus.getPerfil());
        contratacion.setNumCelular(consultaEstatus.getNumCelular());
        contratacion.setCompaniaCelular(consultaEstatus.getCompaniaCelular());
        //contratacion.setEmailCliente(consultaEstatus.getEmailCliente());
        contratacion.setTipoInstrumento(consultaEstatus.getInstrumento());
        contratacion.setEstatusInstrumento(consultaEstatus.getEstatusInstrumento());
        contratacion.setEstatusAlertas(consultaEstatus.getEstatusAlertas());

    }

    /**
     * Copia los datos necesarios del objeto "consulta tarjeta" hacia el objeto
     * "contratación".
     *
     * @throws NullPointerException Si alguno de los dos objetos es nulo.
     */
    public void cargarDatosDeConsultaTarjeta() throws NullPointerException {
        if (null == consultaTarjetaResponse)
            throw new NullPointerException(
                    "Consulta Tarjeta no puede ser nulo.");
        if (null == contratacion)
            throw new NullPointerException("Contratacion no puede ser nulo.");

        contratacion.setNumCelular(consultaEstatus.getNumCelular());
        //contratacion.setEmailCliente(consultaEstatus.getEmailCliente());
        //contratacion.setNumeroCuenta(consultaTarjetaResponse.getNumeroCuenta());
        contratacion.setTipoInstrumento(consultaTarjetaResponse.getTipoInstrumento());
        contratacion.setEstatusInstrumento(consultaTarjetaResponse.getEstatusInstrumento());
        contratacion.setFechaContratacion(consultaTarjetaResponse.getFechaContratacion());
        contratacion.setFechaModificacion(consultaTarjetaResponse.getFechaModificacion());
    }

    // #endregion

    // #region Network
    /*
* (non-Javadoc)
*
* @see
* suitebancomer.classes.gui.delegates.BaseDelegate#doNetworkOperation(int,
* java.util.Hashtable,
* suitebancomer.classes.gui.controllers.BaseViewController)
*/
    @Override
    public void doNetworkOperation(final int operationId,
                                   final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewControllerCommons caller) {
        if (ownerController != null)
            if (ownerController.getParentViewsController() instanceof BmovilViewsController) {
                ((BmovilViewsController) ownerController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, caller);
            } else if (SuiteAppContratacion.isInstanceClassOf(ownerController.getParentViewsController().getClass(), SofttokenViewsController.class)) {
                SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().invokeNetworkOperation(operationId, params, isJson, handler, caller, false);
            }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.classes.gui.delegates.BaseDelegate#analyzeResponse(int,
     * suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
     */
    @Override
    public void analyzeResponse(final int operationId, final ServerResponse response) {
        if (ServerResponse.OPERATION_SUCCESSFUL == response.getStatus()) {
            switch (operationId) {
                case Server.GET_STATE: // Get State, response de flujo Contratación Autenticación
                    ConsultaGetStatusData updateStatus = (ConsultaGetStatusData) response.getResponse();
                    final Hashtable<String, String> paramsTable;

                    if (updateStatus.state.equals(Constants.STATUS_USER_CANCELED)
                            || updateStatus.state.equals(Constants.STATUS_BANK_CANCELED)
                            || updateStatus.state.equals(Constants.STATUS_CLIENT_NOT_FOUND)) {

                        paramsTable = armarParametrosDeFinalizarContratacion();


                        paramsTable.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
                        final List<String> listaEncriptar = Arrays.asList(
                                "cveAcceso", "codigoNIP", "codigoCVV2", "otpDIGITAL"//"cveAcceso", "codigoNIP", "codigoCVV2", "codigoOTP", "tarjeta5Dig"
                        );
                        Encripcion.setContext(SuiteAppContratacion.appContext);
                        Encripcion.encriptaCadenaAutenticacion(paramsTable, listaEncriptar, false);

                        //JAIG
                        doNetworkOperation(Server.CONTRATACION_ENROLAMIENTO_ST, paramsTable, true, null, ownerController);

                    } else if (updateStatus.state.equals(Constants.STATUS_ENGAGEMENT_UNCOMPLETE)) {

                        paramsTable = armarParametrosDeFinalizarContratacion();


                        paramsTable.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
                        final List<String> listaEncriptar = Arrays.asList(
                                "cveAcceso", "codigoNIP", "codigoCVV2", "otpDIGITAL"//"cveAcceso", "codigoNIP", "codigoCVV2", "otpDIGITAL", "tarjeta5Dig"
                        );
                        Encripcion.setContext(SuiteAppContratacion.appContext);
                        Encripcion.encriptaCadenaAutenticacion(paramsTable, listaEncriptar, false);

                        //JAIG
                        doNetworkOperation(Server.OP_FINALIZAR_CONTRATACION_ALERTAS, paramsTable, true, new FinalizarContratacioAlertasData(), ownerController);
                    } else if (updateStatus.state.equals(Constants.STATUS_NIP_BLOCKED)) {
                        ownerController.showInformationAlert(
                                R.string.bmovil_estatus_aplicacion_desactivada_estatus_nip_bloqueado,
                                new OnClickListener() {
                                    @Override
                                    public void onClick(
                                            final DialogInterface dialog,
                                            final int which) {
                                        SuiteAppContratacion.getInstance()
                                                .getBmovilApplication()
                                                .getBmovilViewsController()
                                                .showConsultaEstatusAplicacionDesactivada();
                                    }
                                });
                    } else {
                        SuiteAppContratacion.getInstance().getBmovilApplication()
                                .getBmovilViewsController()
                                .showConsultaEstatusAplicacionDesactivada();
                    }
                    break;
                case Server.CONSULTA_MANTENIMIENTO:
                    final ConsultaEstatusMantenimientoData data = (ConsultaEstatusMantenimientoData) response.getResponse();
                    final Hashtable<String, String> paramTable;

                    if (data.getEstatusServicio().equals(Constants.STATUS_USER_CANCELED)
                            || data.getEstatusServicio().equals(Constants.STATUS_BANK_CANCELED)
                            || data.getEstatusServicio().equals(Constants.STATUS_CLIENT_NOT_FOUND)) {
                        paramTable = armarParametrosDeContratacion();
                        //JAIG
                        doNetworkOperation(Server.OP_CONTRATACION_BMOVIL_ALERTAS, paramTable, true, new ContratacionBmovilData(), ownerController);
                    } else if (data.getEstatusServicio().equals(Constants.STATUS_ENGAGEMENT_UNCOMPLETE)) {

                        paramTable = armarParametrosDeFinalizarContratacion();


                        paramTable.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
                        final List<String> listaEncriptar = Arrays.asList(
                                "cveAcceso", "codigoNIP", "codigoCVV2", "codigoOTP", "tarjeta5Dig"
                        );
                        Encripcion.setContext(SuiteAppContratacion.appContext);
                        Encripcion.encriptaCadenaAutenticacion(paramTable, listaEncriptar, false);

                        //JAIG
                        doNetworkOperation(Server.OP_FINALIZAR_CONTRATACION_ALERTAS, paramTable, true, new FinalizarContratacioAlertasData(), ownerController);
                    } else if (data.getEstatusServicio().equals(Constants.STATUS_NIP_BLOCKED)) {
                        ownerController.showInformationAlert(
                                R.string.bmovil_estatus_aplicacion_desactivada_estatus_nip_bloqueado,
                                new OnClickListener() {
                                    @Override
                                    public void onClick(
                                            final DialogInterface dialog,
                                            final int which) {
                                        SuiteAppContratacion.getInstance()
                                                .getBmovilApplication()
                                                .getBmovilViewsController()
                                                .showConsultaEstatusAplicacionDesactivada();
                                    }
                                });
                    } else {
                        SuiteAppContratacion.getInstance().getBmovilApplication()
                                .getBmovilViewsController()
                                .showConsultaEstatusAplicacionDesactivada();
                    }
                    break;

                case Server.CONSULTA_TARJETA_OPERATION: //Respuesta enroll state, flujo viene de IngresaDatos
                    contrataBMovilSinAlertas(response);
                    //direccionarFlujoConsultaTarjeta(response);

                    break;
                case Server.OP_CONTRATACION_BMOVIL_ALERTAS:
                    break;
                case Server.OP_FINALIZAR_CONTRATACION_ALERTAS:
                    Session.getInstance(SuiteAppContratacion.appContext).saveBanderasBMovil(Constants.BANDERAS_INDICADOR_CONTRATACION, true, true);
                    if (!Tools.isFirstActivationStored()) {
                        Tools.storeFirstActivationDate();
                    }

                    Session.getInstance(SuiteAppContratacion.appContext).setUsername(contratacion.getNumCelular());
                    Session.getInstance(SuiteAppContratacion.appContext).saveRecordStore();

                    //TODO esto es correcto???
                    if (Server.SIMULATION) {
                        Server.CODIGO_ESTATUS_APLICACION = "PA";
                    }

                    muestraAvisoActivacion(SuiteApp.appContext);


                    break;
                case Server.CONSULTA_TARJETA_ST:
                    this.setEscenarioAlternativoEA11(false);
                    cambiarDeBMovilTokenMovil().showActivacionSTEA11(response);
                    // mostrarConfirmacionST();
                    break;

                case Server.CONTRATACION_ENROLAMIENTO_ST: // Response de Contratación Enrrolamiento, Concluye flujo de Contratación Autenticación

                    Session.getInstance(SuiteAppContratacion.appContext).saveBanderasBMovil(Constants.BANDERAS_INDICADOR_CONTRATACION, true, true);
                    if (!Tools.isFirstActivationStored()) {
                        Tools.storeFirstActivationDate();
                    }

                    Session.getInstance(SuiteAppContratacion.appContext).setUsername(contratacion.getNumCelular());
                    Session.getInstance(SuiteAppContratacion.appContext).setApplicationActivated(true);
                    Session.getInstance(SuiteAppContratacion.appContext).saveRecordStore();

                    //TODO esto es correcto???
                    if (Server.SIMULATION) {
                        Server.CODIGO_ESTATUS_APLICACION = "PA";
                    }
                    //EA06
                        Session.getInstance(SuiteAppContratacion.appContext).saveBanderasBMovil(Constants.BANDERAS_CONTRATAR, true, true);
                        muestraAvisoFinalizaContratacion(SuiteApp.appContext); //mostrarActivacionST();
                    break;
                case Server.SOLICITUD_ST:
                    ((ContratacionSTDelegate) SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController()
                            .getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID)).analyzeResponse(Server.SOLICITUD_ST, response);
                default:
                    break;
            }
        } else {

            SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp().showInformationAlert(response.getMessageText());

        }
    }

    private void muestraAvisoActivacion(final Context apContex) {

        final Dialog contactDialog = new Dialog(apContex);
        final LayoutInflater inflater = (LayoutInflater) apContex.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.aviso_ivr_contratacion, null);
        contactDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        contactDialog.setCancelable(false);
        contactDialog.getWindow().setBackgroundDrawable(new ColorDrawable(apContex.getResources().getColor(android.R.color.transparent)));
        contactDialog.setContentView(layout);

        final int width = (int) (apContex.getResources().getDisplayMetrics().widthPixels * 0.90);
        contactDialog.getWindow().setLayout(width, DrawerLayout.LayoutParams.WRAP_CONTENT);

        final Button aceptarIvr = (Button) layout.findViewById(R.id.btnAceptarivr);
        aceptarIvr.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        aceptarIvr();
                        contactDialog.dismiss();
                    }
                }
        );

        contactDialog.show();
    }

    private void muestraAvisoFinalizaContratacion(final Context apContex) {

        final Dialog contactDialog = new Dialog(apContex);
        final LayoutInflater inflater = (LayoutInflater) apContex.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.aviso_finalizar_contratacion, null);
        contactDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        contactDialog.setCancelable(false);
        contactDialog.getWindow().setBackgroundDrawable(new ColorDrawable(apContex.getResources().getColor(android.R.color.transparent)));
        contactDialog.setContentView(layout);

        final int width = (int) (apContex.getResources().getDisplayMetrics().widthPixels * 0.90);
        contactDialog.getWindow().setLayout(width, DrawerLayout.LayoutParams.WRAP_CONTENT);

        final Button aceptarIvr = (Button) layout.findViewById(R.id.btnAceptarivr);
        aceptarIvr.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(ownerController);
                        SuiteAppContratacion.getConsultaEstatusDesactivada().returnToPrincipal();
                       // mostrarActivacionST();
                        contactDialog.dismiss();
                    }
                }
        );

        contactDialog.show();
    }

    private void aceptarIvr() {
        SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(ownerController);
        SuiteAppContratacion.getConsultaEstatusDesactivada().returnToPrincipal();

    }
    /*
    *
    *
    * INICIO >>>>>>>>>>>>>>>>>> FUNCIONES -> P026 - CONTRATACION
    */

    /**
     * Maneja el flujo de ejecucion de la operación Consulta Tarjeta
     *
     * @param response Respuesta del servidor
     */
    public void direccionarFlujoConsultaTarjeta(final ServerResponse response) {
        consultaTarjetaResponse = (ConsultaTarjetaContratacionData) response.getResponse();

        //Resoluci?n incidencia #21961
        cargarDatosDeConsultaTarjeta();
        //EA#24 Solo se puede contratar básico si es cuenta express o TC - Modificación 50505
        //EA#22 se elimino
        /*if (contratacion.getNumeroTarjeta().substring(0, 6).equalsIgnoreCase("409851") || consultaTarjetaResponse.getTipoTarjeta().equalsIgnoreCase("TC")) {
        if (validacionAlertas01()) {
                        // IDS - MODIFICACION FLUJO CONTRATACION
        contratacion.setPerfil(Perfil.avanzado);
        cargarDatosDeConsultaTarjeta();

        mostrarDefinirPassword();
        }
        //hcf
        else
                    if (validacionAlertas02()){
        //alertas 02 con token
        if (determinarTokenActivo()){
        mostrarAlertInformacionYVoyAppDesactivada(ownerController.getString(R.string.contratacion_validacion0203));
        } else {
        contratacion.setPerfil(Perfil.recortado);
                            ownerController.showInformationAlert(ownerController.getString(R.string.label_information), ownerController.getString(R.string.contratacion_validacion02c), new OnClickListener() {
                                        //EA#22
                                        @Override
                                        public void onClick(final DialogInterface dialog, final int which) {
                                            ownerController.getParentViewsController().removeDelegateFromHashMap(CONTRATACION_DELEGATE_ID);
                                            SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(ownerController);
                                            SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(SuiteAppContratacion.getConsultaEstatusDesactivada().getClass());
                                            //SuiteApp.getInstance().getSuiteViewsController().showMenuSuite(true, new String[]{"bmovilselected"});
                                        }
                                    }
                            );
        *//*ownerController.showYesNoAlert(
                                    ownerController.getString(R.string.label_information), ownerController.getString(R.string.contratacion_validacion02),
                                    ownerController.getString(R.string.common_alert_yesno_positive_button),
                                    ownerController.getString(R.string.common_alert_yesno_negative_button),
                                    new OnClickListener() {
                                        @Override
                                        public void onClick(final DialogInterface dialog, final int which) {
                                            contratacion.setPerfil(Perfil.recortado);
                                            cargarDatosDeConsultaTarjeta();
                                            mostrarDefinirPassword();
                                        }
                                    }, new OnClickListener() {
                                        //EA#22
                                        @Override
                                        public void onClick(final DialogInterface dialog, final int which) {
                                            ownerController.getParentViewsController().removeDelegateFromHashMap(CONTRATACION_DELEGATE_ID);
                                            SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(ownerController);
                                            SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(SuiteAppContratacion.getConsultaEstatusDesactivada().getClass());
                                            //SuiteApp.getInstance().getSuiteViewsController().showMenuSuite(true, new String[]{"bmovilselected"});
                                        }
                                    }
                            );*//*
        }
        } else if (validacionAlertas0304()) {
        // EA#10
        mostrarAlertInformacionYVoyAppDesactivada(ownerController.getString(R.string.contratacion_validacion0203));
        }
        //hcf
        }else {*/


        //Resolucion incidencia #21977
        //Para que en la pantalla contratacionAutenticacion pida el token
        //debe de setearse en session.
        Session.getInstance(SuiteAppContratacion.appContext).setSecurityInstrument(consultaTarjetaResponse.getTipoInstrumento());

        if (Server.ALLOW_LOG)
           // Log.i("ValidacionAlertas", consultaTarjetaResponse.getValidacionAlertas());

        contratarBancomerMovil();


    }

    /**
     * Se direcciona el flujo en funcion de la respuesta a consultaTarjetaContratacionE
     */
    private void contratarBancomerMovil() {
        final Perfil perfil = contratacion.getPerfil();
        if (!determinarTokenActivo()) {
            if (validacionAlertas01()) {
                contratacion.setPerfil(Perfil.avanzado);//hcf

                // CGI-Modif Contratacion
                //	if (Constants.Perfil.basico.equals(perfil) || Constants.Perfil.recortado.equals(perfil)) {
                //	// EA#8
                //	cargarDatosDeConsultaTarjeta();
                //	mostrarDefinirPassword();
                //	} else {
                //	if(Perfil.avanzado.equals(perfil)){
                if (!validacionPersonaNoPermitida()) {
                    // EA#11
                    cargarDatosDeConsultaTarjeta();
                    mostrarDefinirPassword();
                    contratacion.setPerfil(Perfil.basico);
                    this.setEscenarioAlternativoEA11(true);
                } else {
                    // EA#16 (persona no permitida)
                    mostrarAlertAceptar(ownerController.getString(R.string.contratacion_validacion_personas_c), TIPO_PERSONA_NO_PERMITIDO);
                }
                //}
            } else if (validacionAlertas02()) {

                //if(Perfil.basico.equals(perfil)){//hcf original
                //	if(Constants.Perfil.avanzado.equals(perfil)){//hcf original
                //	if(Constants.Perfil.recortado.equals(perfil)){//hcf
                //	contratacion.setPerfil(Perfil.recortado);//hcf
                // EA#15
                ownerController.showInformationAlert(ownerController.getString(R.string.label_information), ownerController.getString(R.string.contratacion_validacion02c), new OnClickListener() {
                            //EA#22
                            @Override
                            public void onClick(final DialogInterface dialog, final int which) {
                                ownerController.getParentViewsController().removeDelegateFromHashMap(CONTRATACION_DELEGATE_ID);
                                SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(ownerController);
                                SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(SuiteAppContratacion.getConsultaEstatusDesactivada().getClass());
                                //SuiteApp.getInstance().getSuiteViewsController().showMenuSuite(true, new String[]{"bmovilselected"});
                            }
                        }
                );
                /*ownerController.showYesNoAlert(
                ownerController.getString(R.string.label_information), ownerController.getString(R.string.contratacion_validacion02),
                ownerController.getString(R.string.common_alert_yesno_positive_button),
                ownerController.getString(R.string.common_alert_yesno_negative_button),
                new OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                contratacion.setPerfil(Perfil.recortado);
                cargarDatosDeConsultaTarjeta();
                mostrarDefinirPassword();
                }
                }, new OnClickListener() {
                //EA#22
                @Override
                public void onClick(final DialogInterface dialog, final int which) {
                ownerController.getParentViewsController().removeDelegateFromHashMap(CONTRATACION_DELEGATE_ID);
                SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(ownerController);
                SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(SuiteAppContratacion.getConsultaEstatusDesactivada().getClass());
                //SuiteAppContratacion.getInstance().getSuiteViewsController().showMenuSuite(true, new String[] {"bmovilselected"});
                }
                }
                );*/

                                //}
                                // CGI-Modif Contratacion
                /*else{
                // EA#23
                contratacion.setPerfil(Perfil.recortado);
                cargarDatosDeConsultaTarjeta();
                mostrarDefinirPassword();
                }*/
            } else if (validacionAlertas0304()) {
                // EA#10
                mostrarAlertInformacionYVoyAppDesactivada(ownerController.getString(R.string.contratacion_validacion0203));
            }
        } else {
            if (!validacionPersonaNoPermitida()) {
                //EA01
                cargarDatosDeConsultaTarjeta();

                if (validacionAlertas01()) {
                    contratacion.setPerfil(Perfil.avanzado);//hcf
                    // EP

                    mostrarDefinirPassword();

                } else if (validacionAlertas02()) {
//                    contratacion.setPerfil(Perfil.avanzado);//hcf original CGI
                    contratacion.setPerfil(Perfil.recortado);//hcf
                    // CGI-Modif Contratacion
                    /*if (Constants.Perfil.basico.equals(perfil) || Constants.Perfil.recortado.equals(perfil)) {
                    // EA#19
                    mostrarAlertInformacionYVoyAppDesactivada(ownerController.getString(R.string.contratacion_validacion02_token_new));
                    } else {*/
                    // EA#12, EA#14 (cancelar)
                    //contratacion.setEstatusAlertas(Constants.OPERACION_ALERTAS_CONTRATAR);
                    mostrarAlertAceptar(ownerController.getString(R.string.contratacion_validacion02_token_new), Constants.MANTENIMIENTO_ALERTAS_CONTRATAR);
                //	}

                } else if (validacionAlertas0304()) {
                    // CGI-Modif Contratacion
                    /*if (Constants.Perfil.basico.equals(perfil) || Constants.Perfil.recortado.equals(perfil)) {
                    // EA#20
                    mostrarAlertInformacionYVoyAppDesactivada(ownerController.getString(R.string.contratacion_validacion0304_token_new));
                    }else{*/
                    // EA#13, EA#14 (cancelar)
                    //contratacion.setEstatusAlertas(Constants.OPERACION_ALERTAS_ACTUALIZAR);
                    //mostrarAlertSiNo(ownerController.getString(R.string.contratacion_validacion0304_token), Constants.MANTENIMIENTO_ALERTAS_ACTUALIZAR);
                    mostrarAlertAceptar(ownerController.getString(R.string.contratacion_validacion0304_token_new), Constants.MANTENIMIENTO_ALERTAS_CONTRATAR);
                    //	}

                }
            } else {
                // EA#16 (persona no permitida)
                mostrarAlertAceptar(ownerController.getString(R.string.contratacion_validacion_personas), TIPO_PERSONA_NO_PERMITIDO);
            }
        }
    }

    /**
     * Guarda temporalST
     */
    private void guardarTemporalST() {
        final Session session = Session.getInstance(SuiteAppContratacion.appContext);
        String perfil = Constants.PROFILE_BASIC_01;
        if (Perfil.avanzado.equals(contratacion.getPerfil())) {
            perfil = Constants.PROFILE_ADVANCED_03;
        }
        final TemporalST temporalST = new TemporalST(contratacion.getNumCelular(),
                contratacion.getNumeroTarjeta(),
                contratacion.getCompaniaCelular(),
                contratacion.getContrasena(), contratacion.getEmailCliente(),
                perfil);

        session.saveTemporalST(temporalST, true);
    }

    /**
     * Muestra una alerta por pantalla // Aceptar
     *
     * @param mensaje
     */
    private void mostrarAlertAceptar(final String mensaje, final String operacion) {
        // ownerController.ocultaIndicadorActividad();
        ownerController.setHabilitado(true);
        if (null == ownerController)
            return;

        ownerController.showInformationAlert(
                ownerController.getString(R.string.label_information), mensaje,
                ownerController.getString(R.string.common_alert_yesno_positive_button),
                new OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        // dialog.dismiss();
                        if (Constants.MANTENIMIENTO_ALERTAS_CONTRATAR.equals(operacion)) {
                            //contratacion.setEstatusAlertas(Constants.OPERACION_ALERTAS_CONTRATAR);
                            //mostrarDefinirPassword();

                            //} else if (Constants.MANTENIMIENTO_ALERTAS_ACTUALIZAR.equals(operacion)) {
                            //	contratacion.setEstatusAlertas(Constants.OPERACION_ALERTAS_ACTUALIZAR);
                            //	mostrarDefinirPassword();

                            //	SuiteAppContratacion.getInstance().getSuiteViewsController()
                            //	.showMenuSuite(true,new String[] { "bmovilselected" });
                            SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(ownerController);
                            SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(SuiteAppContratacion.getConsultaEstatusDesactivada().getClass());

                        } else if (TIPO_PERSONA_NO_PERMITIDO.equals(operacion)) {
                            // CGI-Modif Contratacion
                            /*contratacion.setPerfil(Constants.Perfil.basico);
                            cargarDatosDeConsultaTarjeta();
                            contratarBancomerMovil();*/
                        }
                    }
                }
        );
    }

    /**
     * Muestra una alerta por pantalla // Aceptar y cancelar
     *
     * @param mensaje
     */
    private void mostrarAlertSiNo(final String mensaje, final String operacion) {
        // ownerController.ocultaIndicadorActividad();
        ownerController.setHabilitado(true);
        if (null == ownerController)
            return;

        ownerController.showYesNoAlert(
                ownerController.getString(R.string.label_information), mensaje,
                ownerController.getString(R.string.common_alert_yesno_positive_button),
                ownerController.getString(R.string.common_alert_yesno_negative_button),
                new OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        // dialog.dismiss();
                        if (Constants.MANTENIMIENTO_ALERTAS_CONTRATAR.equals(operacion)) {
                            //contratacion.setEstatusAlertas(Constants.OPERACION_ALERTAS_CONTRATAR);
                            //mostrarDefinirPassword();

                            //} else if (Constants.MANTENIMIENTO_ALERTAS_ACTUALIZAR.equals(operacion)) {
                            //	contratacion.setEstatusAlertas(Constants.OPERACION_ALERTAS_ACTUALIZAR);
                            //	mostrarDefinirPassword();

                            SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(ownerController);
                            SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(SuiteAppContratacion.getConsultaEstatusDesactivada().getClass());

                        } else if (TIPO_PERSONA_NO_PERMITIDO.equals(operacion)) {
                            // CGI-Modif Contratacion
                            //	contratacion.setPerfil(Constants.Perfil.basico);
                            //	cargarDatosDeConsultaTarjeta();
                            //	contratarBancomerMovil();
                        }
                    }
                }, null
                //	, new OnClickListener() {
                //	@Override
                //	public void onClick(DialogInterface dialog, int which) {
                //	// dialog.dismiss();
                //	// el usuario selecciona cancelar
                //	if (Constants.MANTENIMIENTO_ALERTAS_CONTRATAR.equals(operacion)	|| Constants.MANTENIMIENTO_ALERTAS_ACTUALIZAR.equals(operacion)) {
                //	contratacion.setEstatusAlertas(Constants.EMPTY_STRING);
                //	mostrarDefinirPassword();
                //	}
                //	}
                //	}
        );
    }

    /**
     * Muestra una alerta por pantalla con Aceptar y ejecuta Aplicación Desactivada.
     *
     * @param mensaje
     */
    private void mostrarAlertInformacionYVoyAppDesactivada(final String mensaje) {
        //Incidencia #22034
        ownerController.showInformationAlert(ownerController.getString(R.string.label_information), mensaje, ownerController.getString(R.string.common_accept),
                new OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int which) {
                        ownerController.getParentViewsController().removeDelegateFromHashMap(CONTRATACION_DELEGATE_ID);
                        //	SuiteAppContratacion.getInstance().getSuiteViewsController()
                        //	.showMenuSuite(true, new String[] {"bmovilselected"});
                        SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(ownerController);
                        SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(SuiteAppContratacion.getConsultaEstatusDesactivada().getClass());
                    }
                });

    }

    /**
     * Valida si el tipo de persona es el correcto para Contratacion
     *
     * @return
     */
    private boolean validacionPersonaNoPermitida() {
        final boolean perfilAvanzado = Perfil.avanzado.equals(contratacion.getPerfil());
        final boolean personaNoPuedeContratar = Constants.PERSONA_F33.equals(consultaTarjetaResponse.getTipoPersona());
        // IDS - MODIFICACION FLUJO CONTRATACION
        // boolean isTC = Constants.CREDIT_TYPE.equals(consultaTarjetaResponse.getTipoTarjeta());
        // boolean isCE = Constants.EXPRESS_TYPE.equals(consultaTarjetaResponse.getTipoTarjeta());
        // return perfilAvanzado && (personaNoPuedeContratar || (isTC || isCE));
        return perfilAvanzado && personaNoPuedeContratar;
    }

    /**
     * Determina si el cliente tiene una alerta de tipo '01'
     *
     * @return
     */
    private boolean validacionAlertas01() {
        return Constants.ALERT01.equals(consultaTarjetaResponse
                .getValidacionAlertas());
    }

    /**
     * Determina si el cliente tiene una alerta de tipo '02'
     *
     * @return
     */
    private boolean validacionAlertas02() {
        return Constants.ALERT02.equals(consultaTarjetaResponse
                .getValidacionAlertas());
    }

    /**
     * Determina si el cliente tiene una alerta de tipo '03' o '04'
     *
     * @return
     */
    private boolean validacionAlertas0304() {
        return Constants.ALERT03.equals(consultaTarjetaResponse
                .getValidacionAlertas())
                || Constants.ALERT04.equals(consultaTarjetaResponse
                .getValidacionAlertas());
    }

    /**
     * Determina si el cliente tiene un Token activo
     *
     * @return
     */
    private boolean determinarTokenActivo() {
        //Resoluci?n incidencia #21961
        final String instrumento = contratacion.getTipoInstrumento();
        return ((Constants.IS_TYPE_DP270.equals(instrumento)
                || Constants.IS_TYPE_OCRA.equals(instrumento)
                || bancomer.api.common.commons.Constants.TYPE_SOFTOKEN.S1.value.equals(instrumento))
                && Constants.STATUS_APP_ACTIVE.equals(contratacion.getEstatusInstrumento()));
    }

    /**
     * Muestra la pantalla con la informaci?n sobre operar con o sin Token
     */
    //IDS CAMBIOS EA#14
    //public void mostrarInformacion() {
    //	if (null != ownerController)
    //	((BmovilViewsController) ownerController.getParentViewsController())
    //	.showAyudaContratacionTokens();
    //}

    /*
    *
    *
    * FIN >>>>>>>>>>>>>>>>>> FUNCIONES -> P026 - CONTRATACION
    */

    // #endregion

    // #region IngresarDatos.

    /**
     * Carga la lista de compañías para el componente seleccion horizontal.
     *
     * @return La lista de compañías en el orden requerido.
     */
    public ArrayList<Object> cargarCompaniasSeleccionHorizontal() {
        // Compania companiaArray[] = new Compania[4];

        final CatalogoVersionado catalogoDineroMovil = Session.getInstance(
                SuiteAppContratacion.appContext).getCatalogoTelefonicas();
        // CatalogoVersionado catalogoCompanias =
        // Session.getInstance(SuiteApp.appContext).getCatalogoTiempoAire();
        Vector<Object> vectorCompanias = catalogoDineroMovil.getObjetos();
        final int companiasSize = vectorCompanias.size();
        final ArrayList<Object> listaCompanias = new ArrayList<Object>(companiasSize);
        Compania currentCompania = null;
        for (int i = 0; i < companiasSize; i++) {
            currentCompania = (Compania) vectorCompanias.get(i);
            listaCompanias.add(currentCompania);
            currentCompania = null;
        }
        vectorCompanias = null;
        return listaCompanias;

        // return new ArrayList<Object>(Arrays.asList(companiaArray));
    }

    /**
     * Carga los elementos a mostrar en el componente ListaDatos de la pantalla
     * Ingresar Datos.
     *
     * @return Los elementos a mostrar.
     */
    public ArrayList<Object> cargarElementosListaDatos() {
        final ArrayList<Object> tabla = new ArrayList<Object>();
        final ArrayList<String> fila = new ArrayList<String>();

        fila.add(ownerController.getString(R.string.bmovil_contratacion_ingresar_datos_numero_celular));
        fila.add(contratacion.getNumCelular());
        tabla.add(fila);

        return tabla;
    }

    /**
     * Valida que los elementos sean correctos antes de continuar con el flujo.
     *
     * @param compania      La compa??a telefónica del usuario.
     * @param numeroTarjeta El número de tarjeta del usuario.
     */
    public void validaCampos(final Object compania, final String numeroTarjeta,
                             final boolean usarToken) {
        if (null == ownerController) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "No se registro un ViewController.");
            return;
        } else if (null == compania) {
            if (Server.ALLOW_LOG)
                Log.e(this.getClass().getName(), "La compañia no puede ser nula.");
            ownerController.showErrorMessage(R.string.valida_compania_telefonica_contratacion);
            return;
        } else if (null == numeroTarjeta) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "El numero de tarjeta de nulo o vacio.");
            return;
        }

        if (numeroTarjeta.length() < Constants.CARD_NUMBER_LENGTH) {
            ownerController.showInformationAlert(R.string.bmovil_contratacion_error_numero_tarjeta_corto);
            return;
        }

        if (null == contratacion) {
            contratacion = new Contratacion();
        }

        contratacion.setCompaniaCelular(((Compania) compania).getNombre());
        contratacion.setNumeroTarjeta(numeroTarjeta);

        final Hashtable<String, String> paramTable = new Hashtable<String, String>();
        // CGI-Modif Contratacion
        //	if (!usarToken) {
        //	// perfil basico
        contratacion.setPerfil(Perfil.avanzado);
        paramTable.put(ServerConstants.PERFIL_CLIENTE, Constants.PROFILE_ADVANCED_03);
        //	} else {
                // perfil avanzado
        //	contratacion.setPerfil(Constants.Perfil.avanzado);
        //	paramTable.put(ServerConstants.PERFIL_CLIENTE, Constants.PROFILE_ADVANCED_03);hcf
        //	paramTable.put(ServerConstants.PERFIL_CLIENTE, Constants.PROFILE_BASIC_01);//hcf
        //	}


        final int operacion = Server.CONSULTA_TARJETA_OPERATION;

        //JAIG
        paramTable.put(ServerConstants.NUMERO_TELEFONO, consultaEstatus.getNumCelular());// "numeroTelefono"
        paramTable.put("numeroTarjeta", numeroTarjeta);// numeroTarjeta
        paramTable.put(ServerConstants.COMPANIA_CELULAR, contratacion.getCompaniaCelular());// compa?iaTelefono
        paramTable.put(ServerConstants.VERSION_FLUJO, Constants.VERSION_FLUJO_CONTRATACION);

        final List<String> listaEncriptar = Arrays.asList(
                ApiConstants.OPERACION, ServerConstants.NUMERO_TELEFONO,
                "numeroTarjeta", ServerConstants.COMPANIA_CELULAR, ServerConstants.PERFIL_CLIENTE, ServerConstants.VERSION_FLUJO
        );
        CommContext.operacionCode = Server.CONSULTA_TARJETA_OPERATION;
        CommContext.listaEncriptar = listaEncriptar;


        //JAIG
        doNetworkOperation(operacion, paramTable, true, new ConsultaTarjetaContratacionData(), ownerController);
    }

    /**
     * Valida que los elementos sean correctos antes de continuar con el flujo.
     *
     * @param compania      La compañía telefónica del usuario.
     * @param numeroTarjeta El número de tarjeta del usuario.
     */
    public void validaCamposST(final String compania, final String numeroTarjeta,
                               final boolean usarToken) {
        if (null == ownerController) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "No se registro un ViewController.");
            return;
        } else if (null == compania) {
            if (Server.ALLOW_LOG)
                Log.e(this.getClass().getName(), "La compa?ia no puede ser nula.");
            return;
        } else if (null == numeroTarjeta) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "El numero de tarjeta de nulo o vacio.");
            return;
        }

        if (numeroTarjeta.length() < Constants.CARD_NUMBER_LENGTH) {
            ownerController
                    .showInformationAlert(R.string.bmovil_contratacion_error_numero_tarjeta_corto);
            return;
        }

        if (null == contratacion) {
            contratacion = new Contratacion();
        }

        contratacion.setCompaniaCelular(compania);
        contratacion.setNumeroTarjeta(numeroTarjeta);

        final Hashtable<String, String> paramTable = new Hashtable<String, String>();


        /*if (!usarToken) {
        // perfil basico
        contratacion.setPerfil(Constants.Perfil.basico);
        paramTable.put(ServerConstants.PERFIL_CLIENTE, Constants.PROFILE_BASIC_01 );
        } else {*/
        // perfil avanzado
        contratacion.setPerfil(Perfil.avanzado);//hcf
        paramTable.put(ServerConstants.PERFIL_CLIENTE, Constants.PROFILE_ADVANCED_03);//hcf
        //	contratacion.setPerfil(Constants.Perfil.basico);//hcf
        //	paramTable.put(ServerConstants.PERFIL_CLIENTE, Constants.PROFILE_BASIC_01 );//hcf
        //	}

        final int operacion = Server.CONSULTA_TARJETA_OPERATION;
        //JAIG
        paramTable.put(ServerConstants.NUMERO_TELEFONO, consultaEstatus.getNumCelular());// "numeroTelefono"
        paramTable.put("numeroTarjeta", numeroTarjeta);// numeroTarjeta
        paramTable.put(ServerConstants.COMPANIA_CELULAR, contratacion.getCompaniaCelular());// compa?iaTelefono
        paramTable.put(ServerConstants.VERSION_FLUJO, Constants.VERSION_FLUJO_CONTRATACION);
        //JAIG
        doNetworkOperation(operacion, paramTable, true, new ConsultaTarjetaContratacionData(), ownerController);
    }

    // #endregion

    // #region DefinirPassword.

    /**
     * Valida que los elementos sean correctos antes de continuar con el flujo.
     *
     * @param password        Nueva contraseña.
     * @param passwordConfirm Confirmación de la nueva contraseña.
     * @param email           Email del cliente.
     * @param emailConfirm    Confirmacion del Email.
     */
    public void validaCampos(final String password, final String passwordConfirm,
                             final String email, final String emailConfirm) {
        if (null == password) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "No se registro un número de cuenta.");
            return;
        } else if (null == passwordConfirm) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "No se registro una confirmaci?n de número de cuenta..");
            return;
        } else if (null == email) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(), "No se registro un email.");
            return;
        } else if (null == emailConfirm) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "No se registro una confirmaci?n de email.");
            return;
        }

        if (Tools.isEmptyOrNull(password)) {
            ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_password_vacio);
        } else if (password.length() != Constants.PASSWORD_LENGTH) {
            ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_password_corto);
        } else if (!validatePasswordPolicy1(password)) {
            ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_numeros_repetidos);
        } else if (!validatePasswordPolicy2(password)) {
            ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_numeros_consecutivos);
        } else if (Tools.isEmptyOrNull(passwordConfirm)) {
            ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_confirmar_password_vacio);
        } else if (passwordConfirm.length() != Constants.PASSWORD_LENGTH) {
            ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_confirmacion_password_corto);
        } else if (!password.equals(passwordConfirm)) {
            ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_passwords_diferentes);
        } else if (Tools.isEmptyOrNull(email)) {
            ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_email_vacio);
        } else if (!Pattern.compile(EMAIL_PATTERN).matcher(email).matches()) {
            ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_formato_email);
        } else if (Tools.isEmptyOrNull(emailConfirm)) {
            ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_confirmar_email_vacio);
        } else if (!email.equals(emailConfirm)) {
            ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_confirmar_email_diferente);
        } else {
            //EA01
            Session.getInstance(SuiteAppContratacion.appContext).setPassword(password);
            contratacion.setEmailCliente(email);
            contratacion.setContrasena(password);

            if (this.isEscenarioAlternativoEA11()) {
                Session.getInstance(SuiteAppContratacion.appContext).saveBanderasBMovil(Constants.BANDERAS_CONTRATAR, true, true);
                guardarTemporalST();
                ownerController.muestraIndicadorActividad(
                        ownerController.getString(R.string.alert_operation),
                        ownerController.getString(R.string.alert_connecting));

                armarContratacionPS();

                ownerController.ocultaIndicadorActividad();

            } else {
                if (determinarTokenActivo()) {

                    if (reglaTokenMovilActivado() || ((Constants.IS_TYPE_DP270.equals(contratacion.getTipoInstrumento())
                            || Constants.IS_TYPE_OCRA.equals(contratacion.getTipoInstrumento())) && Constants.STATUS_APP_ACTIVE.equals(contratacion.getEstatusInstrumento()))) {
                        if (ownerController.getParentViewsController() instanceof BmovilViewsController) {
                            ((BmovilViewsController) ownerController.getParentViewsController()).showContratacionAutenticacion(this);
                        } else if (SuiteAppContratacion.isInstanceClassOf(ownerController.getParentViewsController().getClass(), SofttokenViewsController.class)) {
                            //SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivity(IngresoDatosSTViewController.getInstance());
                            SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController().showContratacionAutenticacion(this);

                        }
                    } else {
                        armarContratacionPS();
                    }


                }
            }
        }
    }

    public void contrataBMovilSinAlertas(ServerResponse response ) {
        consultaTarjetaResponse = (ConsultaTarjetaContratacionData) response.getResponse();
        cargarDatosDeConsultaTarjeta();

        if (this.isEscenarioAlternativoEA11()) {
            Session.getInstance(SuiteAppContratacion.appContext).saveBanderasBMovil(Constants.BANDERAS_CONTRATAR, true, true);
            guardarTemporalST();
            ownerController.muestraIndicadorActividad(
                    ownerController.getString(R.string.alert_operation),
                    ownerController.getString(R.string.alert_connecting));

            //armarContratacionPS();
            mostrarActivacionST();

            ownerController.ocultaIndicadorActividad();
        } else if (determinarTokenActivo()) { // RN3
            if(contratacion.getEstatusAlertas().equals(NO_ALERTS)) {
                mostrarAlertAceptar(consultaTarjetaResponse.getDescripcion(),Constants.MANTENIMIENTO_ALERTAS_CONTRATAR);
                return;
            }

            if(!validacionPersonaNoPermitida()) {
                if (ownerController.getParentViewsController() instanceof BmovilViewsController) {
                    ((BmovilViewsController) ownerController.getParentViewsController()).showContratacionAutenticacion(this);
                } else if (SuiteAppContratacion.isInstanceClassOf(ownerController.getParentViewsController().getClass(), SofttokenViewsController.class)) {
                    //SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivity(IngresoDatosSTViewController.getInstance());
                    SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController().showContratacionAutenticacion(this);

                }
            } else {
                // EA#16 (persona no permitida)
                mostrarAlertAceptar(ownerController.getString(R.string.contratacion_validacion_personas), TIPO_PERSONA_NO_PERMITIDO);
            }
        } else {
            mostrarActivacionST();
        }


    }

    /**
     * Valida que los elementos sean correctos antes de continuar con el flujo.
     *
     * @param password        Nueva contraseña.
     * @param passwordConfirm Confirmación de la nueva contraseña.
     * @param numeroTarjeta   Número de tarjeta del cliente
     */
    public boolean validaCampos(final String password, final String passwordConfirm, String numeroTarjeta,Boolean requestReady) {
        if (null == numeroTarjeta) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "No se registro un número de cuenta.");
            return false;
        } else if (null == password) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "No se registro ningún password");
            return false;
        } else if (null == passwordConfirm) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "Confirmación de password es nulo o vacio.");
            return false;
        }

        if (numeroTarjeta.length() < Constants.CARD_NUMBER_LENGTH) {
            if(requestReady) {
                ownerController.showInformationAlert(R.string.bmovil_contratacion_error_numero_tarjeta_corto);
                return false;
            } else {
                return false;
            }
        } else if (Tools.isEmptyOrNull(password)) {
            if(requestReady) {
                ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_password_vacio);
                return false;
            } else {
                return false;
            }
        } else if (password.length() != Constants.PASSWORD_LENGTH) {
            if(requestReady) {
                ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_password_corto);
                return false;
            } else {
                return false;
            }
        } else if (!validatePasswordPolicy1(password)) {
            if(requestReady) {
                ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_numeros_repetidos);
                return false;
            } else {
                return false;
            }
        } else if (!validatePasswordPolicy2(password)) {
            if(requestReady) {
                ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_numeros_consecutivos);
                return false;
            } else {
                return false;
            }
        } else if (Tools.isEmptyOrNull(passwordConfirm)) {
            if(requestReady) {
                ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_confirmar_password_vacio);
                return false;
            } else {
                return false;
            }
        } else if (passwordConfirm.length() != Constants.PASSWORD_LENGTH) {
            if(requestReady) {
                ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_confirmacion_password_corto);
                return false;
            } else {
                return false;
            }
        } else if (!password.equals(passwordConfirm)) {
            if(requestReady) {
                ownerController.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_passwords_diferentes);
                return false;
            } else {
                return false;
            }
        } else if(requestReady) {
                //EA01
                Session.getInstance(SuiteAppContratacion.appContext).setPassword(password);
                contratacion.setContrasena(password);
                contratacion.setNumeroTarjeta(numeroTarjeta);


                final Hashtable<String, String> paramTable = new Hashtable<String, String>();

                ///////////////// Obtener Token /////////////////

            /*if (!usarToken) {
            // perfil basico
            contratacion.setPerfil(Constants.Perfil.basico);
            paramTable.put(ServerConstants.PERFIL_CLIENTE, Constants.PROFILE_BASIC_01 );
            } else {*/
                // perfil avanzado
                contratacion.setPerfil(Perfil.avanzado);//hcf
                //paramTable.put(ServerConstants.PERFIL_CLIENTE, Constants.PROFILE_ADVANCED_03);//hcf
                //	contratacion.setPerfil(Constants.Perfil.basico);//hcf
                //	paramTable.put(ServerConstants.PERFIL_CLIENTE, Constants.PROFILE_BASIC_01 );//hcf
                //	}

                final int operacion = Server.CONSULTA_TARJETA_OPERATION;
                //JAIG
                paramTable.put(ServerConstants.CELL_PHONE_NUMBER, consultaEstatus.getNumCelular());// "numeroTelefono"
                paramTable.put("cardNumber", numeroTarjeta);// numeroTarjeta
                //paramTable.put(ServerConstants.COMPANIA_CELULAR, contratacion.getCompaniaCelular());// compa?iaTelefono
                //paramTable.put(ServerConstants.VERSION_FLUJO, Constants.VERSION_FLUJO_CONTRATACION);
                //JAIG
                doNetworkOperation(operacion, paramTable, true, new ConsultaTarjetaContratacionData(), ownerController);

                /////////************************////////////////////
                return true;
            } else {
                return true;
            }


    }

    /**
     * Validates the new password according to the following policy: a password
     * cannot have more than 2 repeated digits together, i.e: 123338 would be an
     * invalid password
     *
     * @param pwd the password to validate
     * @return true if the password passes the policy , false if it fails
     */
    private boolean validatePasswordPolicy1(final String pwd) {
        if (pwd == null) {
            return false;
        }
        boolean validate = true;
        for (int i = 0; ((i < pwd.length()) && (validate)); i++) {
            if (i > 1) {
                final char c0 = pwd.charAt(i - 2);
                final char c1 = pwd.charAt(i - 1);
                final char c2 = pwd.charAt(i);
                final boolean fails = ((c0 == c1) && (c1 == c2));
                validate = !fails;
            }
        }
        return validate;
    }

    /**
     * Validates the new password according to the following policy: a password
     * cannot have more than 2 digits consecutive, either ascending or
     * descending. i.e., 12348 or 43276 would fail.
     *
     * @param pwd the password to validate
     * @return true if the password passes the policy , false if it fails
     */
    private boolean validatePasswordPolicy2(final String pwd) {
        if (pwd == null) {
            return false;
        }
        boolean validate = true;
        for (int i = 0; ((i < pwd.length()) && (validate)); i++) {
            if (i > 1) {
                final char c0 = pwd.charAt(i - 2);
                final char c1 = pwd.charAt(i - 1);
                final char c2 = pwd.charAt(i);
                final boolean fails = (((c2 == c1 + 1) && (c1 == c0 + 1)) // ascending
                        // order
                        || ((c2 == c1 - 1) && (c1 == c0 - 1)) // descending order
                );
                validate = !fails;
            }
        }
        return validate;
    }

    // #endregion

    // #region Métodos de autenticación contratación
    @Override
    public void realizaOperacion(
            final ContratacionAutenticacionViewController contratacionAutenticacionViewController,
            final String nip, final String token, final String cvv, final String contrasenia,
            final boolean terminos, final String campoTarjeta) {
        final Session session = Session.getInstance(ownerController);

        final String autVersion = CatalogoAutenticacionFileManager.getCurrent().leerVersionArchivoCatalogoAutenticacion();
        final String compVersion = session.getVersionCatalogoTelefonicas();

        respuestaConfirmacion = new RespuestaConfirmacion(contrasenia, cvv,
                nip, token, campoTarjeta);

        final Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(ServerConstants.NUMERO_TELEFONO,
                contratacion.getNumCelular()); //Sólo se envia numero celular para petición getState()
       params.put(ServerConstants.VERSION_MIDLET,
                ServerConstants.APP_VERSION_CONSULTA);
         /*params.put("verCatAutenticacion", Tools.isEmptyOrNull(autVersion) ? "0"
                : autVersion);
        params.put("verCatTelefonicas",
                Tools.isEmptyOrNull(autVersion) ? "0" : compVersion);*/


        /*final List<String> listaEncriptar = Arrays.asList(
                ApiConstants.OPERACION, ServerConstants.NUMERO_TELEFONO,
                ServerConstants.VERSION_MIDLET, "verCatTelefonicas", "verCatAutenticacion"
        );*/

        final List<String> listaEncriptar = Arrays.asList(
                ApiConstants.OPERACION, ServerConstants.NUMERO_TELEFONO,
                ServerConstants.VERSION_MIDLET);


        CommContext.operacionCode = Server.GET_STATE;
        CommContext.listaEncriptar = listaEncriptar;

        //JAIG
        ownerController = contratacionAutenticacionViewController;
        doNetworkOperation(Server.GET_STATE, params, true, new ConsultaGetStatusData(),
                ownerController);
    }

    /**
     * Arma la lista de parametros para la operación Contratacion Bmovil.
     *
     * @return La lista de parametros para Server.
     */
    private Hashtable<String, String> armarParametrosDeContratacion() {
        final Hashtable<String, String> params = new Hashtable<String, String>();
        //JAIG
        params.put(ServerConstants.NUMERO_TELEFONO, contratacion.getNumCelular());
        params.put("cveAcceso", Session.getInstance(SuiteAppContratacion.appContext).getPassword());
        params.put("numeroTarjeta", contratacion.getNumeroTarjeta());
        params.put("perfilCliente", Tools.determinarPerfil(contratacion.getPerfil()));
        //params.put("companiaCelular", contratacion.getCompaniaCelular());
        //params.put("emailCliente", contratacion.getEmailCliente());
        params.put("aceptoTerminosCondiciones", "SI");
        params.put("versionFlujo", Constants.VERSION_FLUJO_CONTRATACION);
        params.put("estatusAlertas", (null == contratacion.getEstatusAlertas()) ? "" : contratacion.getEstatusAlertas());
        params.put("codigoNIP", (null == respuestaConfirmacion.getNip()) ? "" : respuestaConfirmacion.getNip());
        params.put("codigoCVV2", (null == respuestaConfirmacion.getCvv()) ? "" : respuestaConfirmacion.getCvv());
        params.put("codigoOTP", (null == respuestaConfirmacion.getOtp()) ? "" : respuestaConfirmacion.getOtp());
        params.put("cadenaAutenticacion", Autenticacion.getInstance().getCadenaAutenticacion(tipoOperacion, contratacion.getPerfil()));
        params.put(Server.MARCA, Build.BRAND);
        params.put(Server.MODELO, Build.MODEL);
        params.put("tarjeta5Dig", respuestaConfirmacion.getNumTarjeta() == null ? "" : respuestaConfirmacion.getNumTarjeta());
        params.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
        params.put("sistemaOperativo", "android");
        final List<String> listaEncriptar = Arrays.asList("cveAcceso", "codigoNIP",
                "codigoCVV2");
        Encripcion.setContext(SuiteAppContratacion.appContext);
        Encripcion.encriptaCadenaAutenticacion(params, listaEncriptar);
        return params;
    }

    /**
     * Arma la lista de parametros para la operación Finalizar Contratación.
     *
     * @return La lista de parametros para Server.
     */
    private Hashtable<String, String> armarParametrosDeFinalizarContratacion() {
        final Hashtable<String, String> params = new Hashtable<String, String>();
        //JAIG
        params.put(ServerConstants.CELL_PHONE_NUMBER, contratacion.getNumCelular());
        params.put("cve", (null == contratacion.getContrasena()) ? "" : contratacion.getContrasena());
        params.put("cardNumber", contratacion.getNumeroTarjeta() == null ? "" : contratacion.getNumeroTarjeta());
        params.put("termsAndConditions","SI");
        params.put("otp", (null == respuestaConfirmacion.getOtp()) ? "" : respuestaConfirmacion.getOtp());
        params.put("activationCode", Autenticacion.getInstance().getCadenaAutenticacion(tipoOperacion, contratacion.getPerfil()));
        params.put(Server.BRAND,Build.BRAND);
        params.put(Server.MODEL,Build.MODEL);
        params.put("nip", (null == respuestaConfirmacion.getNip()) ? "" : respuestaConfirmacion.getNip());
        params.put("cvv2", (null == respuestaConfirmacion.getCvv()) ? "" : respuestaConfirmacion.getCvv());

        //params.put("companiaCelular", contratacion.getCompaniaCelular());
        //params.put("estatusAlertas", (null == contratacion.getEstatusAlertas()) ? "" : contratacion.getEstatusAlertas());
        //params.put("tarjeta5Dig", respuestaConfirmacion.getNumTarjeta() == null ? "" : respuestaConfirmacion.getNumTarjeta());
        //params.put("sistemaOperativo", "android");
        //params.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
        //List<String> listaEncriptar = Arrays.asList("cveAcceso", "codigoNIP",
        //	"codigoCVV2");
        //Encripcion.setContext(SuiteAppContratacion.appContext);
        //Encripcion.encriptaCadenaAutenticacion(params, listaEncriptar);
        return params;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getDatosTablaConfirmacion()
     */
    @Override
    public ArrayList<Object> getDatosTablaConfirmacion() {
        ArrayList<Object> registros;
        final ArrayList<Object> lista = new ArrayList<Object>();
        if (ownerController == null || ownerController instanceof ContratacionAutenticacionViewController) {
            ownerController = (BaseViewControllerCommons) SuiteApp.appContext;
        }
        registros = new ArrayList<Object>();
        registros.add(ownerController.getString(R.string.bmovil_contratacion_autorizacion_label_celular));
        registros.add(consultaEstatus.getNumCelular());
        lista.add(registros);

        registros = new ArrayList<Object>();
        registros.add(ownerController.getString(R.string.bmovil_contratacion_autorizacion_label_compania));
        registros.add(contratacion.getCompaniaCelular());
        lista.add(registros);

        registros = new ArrayList<Object>();
        registros.add(ownerController.getString(R.string.bmovil_contratacion_autorizacion_label_operacion));
        registros.add(ownerController.getString(R.string.bmovil_contratacion_autorizacion_label_contratacion));
        lista.add(registros);

        registros = new ArrayList<Object>();
        registros.add(ownerController.getString(R.string.bmovil_contratacion_autorizacion_label_servicio));

        //Incidencia #22122
        registros.add((Perfil.avanzado == contratacion.getPerfil()) ?
                ownerController.getString(R.string.bmovil_contratacion_autorizacion_label_servicio_avanzado) :
                ownerController.getString(R.string.bmovil_contratacion_autorizacion_label_servicio_basico));
        lista.add(registros);

        if (Constants.OPERACION_ALERTAS_CONTRATAR.equals(contratacion.getEstatusAlertas())) {
            registros = new ArrayList<Object>();

            registros.add(TEXTO_ESTADO_ALERTA);


            registros.add(Constants.ESTADO_ALERTAS_ALTA);
            lista.add(registros);

        } else if (Constants.OPERACION_ALERTAS_ACTUALIZAR.equals(contratacion.getEstatusAlertas())) {
            registros = new ArrayList<Object>();

            registros.add(TEXTO_ESTADO_ALERTA);


            registros.add(Constants.ESTADO_ALERTAS_MODIFICACION);
            lista.add(registros);
        }


        return lista;
    }


    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #mostrarContrasenia()
     */
    @Override
    public boolean mostrarContrasenia() {
        return Autenticacion.getInstance().mostrarContrasena(tipoOperacion,
                this.contratacion.getPerfil());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #tokenAMostrar()
     */
    @Override
    public TipoOtpAutenticacion tokenAMostrar() {
        return Autenticacion.getInstance().tokenAMostrar(tipoOperacion,
                this.contratacion.getPerfil());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #mostrarNIP()
     */
    @Override
    public boolean mostrarNIP() {
        return Autenticacion.getInstance().mostrarNIP(tipoOperacion,
                this.contratacion.getPerfil());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #mostrarCVV()
     */
    @Override
    public boolean mostrarCVV() {
        return Autenticacion.getInstance().mostrarCVV(tipoOperacion,
                this.contratacion.getPerfil());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getTextoAyudaNIP()
     */
    @Override
    public String getTextoAyudaNIP() {
        return SuiteAppContratacion.appContext
                .getString(R.string.confirmation_autenticacion_ayudaNip);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getNombreImagenEncabezado()
     */
    @Override
    public int getNombreImagenEncabezado() {
        return R.drawable.icono_contratacion;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getTextoEncabezado()
     */
    @Override
    public int getTextoEncabezado() {
        return R.string.bmovil_contratacion_titulo;
    }

    // #endregion

    /**
     * Elimina los datos necesarios.
     */
    public void deleteData() {
        Tools.deleteFirstActivationData();
        final Session session = Session.getInstance(SuiteAppContratacion.appContext);
        final ContratacionDelegate me = this;
        ownerController.muestraIndicadorActividad("", "");

        // Clear session data
        session.setUsername(null);
        session.setCompaniaUsuario(null);
        session.setApplicationActivated(false);
        session.setSeed(0);
        session.setAceptaCambioPerfil(true);
        session.clearCatalogs(false);

        deleteData = false;
        session.storeSession(me);
    }

    @Override
    public void sessionStored() {
        ownerController.ocultaIndicadorActividad();
    }

    @Override
    public boolean mostrarCampoTarjeta() {
        return (mostrarCVV() || mostrarNIP());
    }

    /**
     * Se valida la subaplicación Softtoken esta lista para ser activada. <br/>
     * Esto sucede si el usuario tiene como instrumento de seguridad Softtoken
     * activo y la subaplicación Softtoken no esta activa en el dispositivo.
     */
    public boolean isSofttokenListoParaActivar() {
        final boolean isSofttoken = bancomer.api.common.commons.Constants.TYPE_SOFTOKEN.S1.value.equals(contratacion
                .getTipoInstrumento());
        final boolean isActive = Constants.ESTATUS_IS_ACTIVO.equals(contratacion
                .getEstatusInstrumento());
        final boolean isSofttokenActive = PropertiesManager.getCurrent()
                .getSofttokenActivated();
        return (isSofttoken && isActive && !isSofttokenActive);
    }

    public boolean reglaNegocioSoftoken() {
        final boolean isSofttoken = bancomer.api.common.commons.Constants.TYPE_SOFTOKEN.S1.value.equals(contratacion
                .getTipoInstrumento());
        final boolean isActive = Constants.ESTATUS_IS_ACTIVO.equals(contratacion
                .getEstatusInstrumento());
        return (isSofttoken && isActive);
    }

    public boolean reglaTokenMovilActivado() {
        final boolean isSofttokenActive = PropertiesManager.getCurrent()
                .getSofttokenActivated();

        return isSofttokenActive;
    }

    /**
     * Permite al cliente decidir si desea activar o no Softtoken.
     */
    public void intentaActivarSofttoken() {
        if (null == ownerController)
            return;

        /*ownerController.showYesNoAlert(R.string.label_error,
        R.string.softtoken_listo_para_activar,
        R.string.common_alert_yesno_positive_button,
        R.string.common_alert_yesno_negative_button,
        new OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();*/

        if (null == SuiteAppApi.getInstanceApi().getSofttokenApplicationApi())
            SuiteAppApi.getInstanceApi().startSofttokenAppApi();
        //	MenuSuiteViewController menuSuiteViewController = new MenuSuiteViewController();
        // SuiteApp.getInstance().getSuiteViewsController().setCurrentActivityApp(menuSuiteViewController);
        //	MenuSuiteViewController suiteViewController = (MenuSuiteViewController) SuiteApp
        //	.getInstance().getSuiteViewsController()
        //	.getCurrentViewControllerApp();
        //	SofttokenViewsController viewsController = SuiteApp
        //	.getInstance().getSofttokenApplication()
        //	.getSottokenViewsController();

        // el método showContratacionSotfttoken no muestra
        // realmente ninguna pantalla, sin embargo lleva a cabo
        // varia
        // inicializaciones requeridas a lo largo o al final del
        // flujo de contratación de Softtoken.
        //	viewsController
        //	.showContratacionSotfttoken(suiteViewController);
        //	viewsController.showPantallaIngresoDatos();
        // SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(ownerController);
        // SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(SuiteAppContratacion.getSofttokenContratacion().getClass());
        //SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(SuiteAppContratacion.getInstance().getIntentSofttoken(),0,true, new String[]{},new Object[]{});

        //SuiteAppApi.getInstanceApi().startSofttokenAppApi();
        SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(ownerController);
        SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(IngresoDatosSTViewController.class);


        /*}
        }, new OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
        ((BmovilViewsController) ownerController
        .getParentViewsController())
        .showDefinirPassword();
        }
        });*/
    }

    private void mostrarActivacionST() {
        cambiarDeBMovilTokenMovil().showActivacionST(consultaEstatus, contratacion);
    }

    private void armarContratacionPS() {
        final Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(ServerConstants.CELL_PHONE_NUMBER, contratacion.getNumCelular());
        params.put(ServerConstants.CVE, Session.getInstance(SuiteAppContratacion.appContext).getPassword());
        params.put(ServerConstants.CARD_NUMBER, contratacion.getNumeroTarjeta());
        //params.put(ServerConstants.PERFIL_CLIENTE, (Perfil.avanzado == contratacion.getPerfil()) ? Constants.PROFILE_ADVANCED_03 : Constants.PROFILE_BASIC_01);
        //params.put(ServerConstants.COMPANIA_CELULAR, contratacion.getCompaniaCelular());
        //params.put(ServerConstants.EMAIL, contratacion.getEmailCliente());
        params.put(ServerConstants.TERMS_AND_CONDTIONS, "true");
        //params.put(ServerConstants.VERSION_FLUJO, Constants.VERSION_FLUJO_CONTRATACION);
        //codigo para encriptacion
        params.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
        final List<String> listaEncriptar = Arrays.asList(
                ServerConstants.CELL_PHONE_NUMBER, ServerConstants.CVE,
                ServerConstants.CARD_NUMBER //ServerConstants.COMPANIA_CELULAR
                //ServerConstants.EMAIL
        );
        Encripcion.setContext(SuiteAppContratacion.appContext);
        Encripcion.encriptaCadenaAutenticacion(params, listaEncriptar, false);
        //JAIG
        doNetworkOperation(Server.CONTRATACION_ENROLAMIENTO_ST, params, true, null, ownerController);
    }

        //	private void mostrarConfirmacionST() {
        //	cambiarDeBMovilTokenMovil().showConfirmacionST();
        //	}

    private SofttokenViewsController cambiarDeBMovilTokenMovil() {
        final SofttokenViewsController viewsController = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
        ownerController.setParentViewsController(viewsController);
        viewsController.setCurrentActivityApp(ownerController);
        return viewsController;
    }

    public void mostrarDefinirPassword() {
        //	((BmovilViewsController) ownerController.getParentViewsController()).showDefinirPassword();
        if (ownerController != null)
            if (ownerController.getParentViewsController() instanceof BmovilViewsController) {
                ((BmovilViewsController) ownerController.getParentViewsController()).setCurrentActivityApp(ownerController);
                ((BmovilViewsController) ownerController.getParentViewsController()).showDefinirPassword();
            } else if (SuiteAppContratacion.isInstanceClassOf(ownerController.getParentViewsController().getClass(), SofttokenViewsController.class)) {
                ((BmovilViewsController) ownerController.getParentViewsController()).setCurrentActivityApp(ownerController);
                ((BmovilViewsController) ownerController.getParentViewsController()).showDefinirPassword();
            }
    }

    public boolean isEscenarioAlternativoEA11() {
        return escenarioAlternativoEA11;
    }

    public void setEscenarioAlternativoEA11(final boolean escenarioAlternativoEA11) {
        this.escenarioAlternativoEA11 = escenarioAlternativoEA11;
    }

    public void alertaAcudeCajero() {
        ownerController.showInformationAlert(R.string.bmovil_activacion_alert_acude_atm, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        );
    }

    public void alertaVerificaDatos() {
        ownerController.showInformationAlert(R.string.bmovil_activacion_alert_verifica_datos, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        );
    }
}