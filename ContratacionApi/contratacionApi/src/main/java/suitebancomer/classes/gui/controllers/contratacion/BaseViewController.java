/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.classes.gui.controllers.contratacion;

import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;
import suitebancomer.classes.gui.delegates.contratacion.BaseDelegate;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.BmovilViewsController;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;

import com.bancomer.mbanking.contratacion.SuiteAppContratacion;
import com.bancomer.mbanking.contratacion.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import com.bancomer.base.SuiteApp;

import java.lang.reflect.Method;

/**
 * BaseScreen is the base class for all the screens in the MBanking application
 * It provides basic, common functionality to be included in the business logic
 * so that the application can manage screens in a uniform, consistent way.
 *
 * @author Stefanini IT Solutions
 */
public abstract class BaseViewController extends BaseViewControllerCommons {
	// Repicacion API Mtto
	private static Method methodOverridePendingTransition;
	static {
		final Method[] actMethods = Activity.class.getMethods();
		final int methodSize = actMethods.length;
		for (int i=0; i<methodSize; i++) {
			if (actMethods[i].getName().equals("overridePendingTransition")) {
				methodOverridePendingTransition = actMethods[i];
				break;
			}
		}
	}
	//

	//AMZ
		private BmovilViewsController bm;
		private SofttokenViewsController st;

	// #region Variables.
	/**
	 * indica si es un flujo de estatus desactivado
	 */
	public boolean statusdesactivado;
	
	/**
     * To use as parameter in aplicaEstilo method in order to show the header
     */
    public final static int SHOW_HEADER = 2;
    
    /**
     * To use as parameter in aplicaEstilo method in order to show the title
     */
    public final static int SHOW_TITLE = 4;
    
	/**
     * Current view edit fields' references. Each view should implement this variable.
     */
	protected static EditText[] sFields;
	
	/**
     * Determines if an alert pop-up is being shown
     */
    protected boolean mShowingDialogPopup = false;

    /**
     * A progress dialog for long waiting processes.
     */
    protected ProgressDialog mProgressDialog;
    
    /**
     * The alert error or information dialog.
     */
    private AlertDialog mAlertDialog;
	
	/**
	 * Referencias al controlador de vistas padre de esta vista
	 */
	protected BaseViewsControllerCommons parentViewsController;
	
	/**
     * Referencia al layout del cabecero
     */
    private LinearLayout mHeaderLayout;
    
    /**
     * Referencia al layout del t�tulo
     */
    private LinearLayout mTitleLayout;
    
    /**
     * Referencia al separador del t�tulo
     */
    private ImageView mTitleDivider;

	/**
	 * Botón back
	 */

	private ImageView btnBack;
	
	/**
	 * Layout where each view will be drawn.
	 */
	private ViewGroup mBodyLayout;
	
	/**
	 * Par�metros con los que se construir� la pantalla b�sica
	 */
	private int activityParameters;
	
	/**
     * Referencia al delegate de esta vista
     */
	private BaseDelegate delegate;
	
	/**
     * Referencia a la clase misma
     */
	private static BaseViewController me;
	
	/**
     * M�todo que devuelve una instancia de la clase
     * @return una instancia de esta clase
     */
	public static BaseViewController getInstance() {
		return me;
	}
	// #endregion
	
	/**
	 * Constructor por defecto
	 */
	public BaseViewController() {
		me = this;	
	}
	
	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState state
	 */
	public void onCreate(final Bundle savedInstanceState, final int parameters, final int layoutID) {
		// Repicacion API Mtto
		try {
			methodOverridePendingTransition.invoke(this, new Object[]{R.anim.screen_enter, R.anim.screen_leave});
		} catch (Exception e) {
			Log.d("APIMtto",e.toString());
		}
		//

		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
    	activityParameters = parameters;
    	setContentView(getResources().getIdentifier("layout_base_activity_cont", "layout", getPackageName()));
    	
    	// references to ui elements
        mHeaderLayout = (LinearLayout) findViewById(SuiteAppContratacion.getResourceId("header_layout_cont", "id"));
        mTitleLayout = (LinearLayout) findViewById(SuiteAppContratacion.getResourceId("title_layout_cont", "id"));
        mTitleDivider = (ImageView) findViewById(SuiteAppContratacion.getResourceId("title_divider_cont", "id"));
        mBodyLayout = (ViewGroup) findViewById(SuiteAppContratacion.getResourceId("body_layout_cont", "id"));
		btnBack = (ImageView) findViewById(R.id.btnAtras);
        
        // 	set header visibility
        mHeaderLayout.setVisibility(((activityParameters&SHOW_HEADER)==SHOW_HEADER)?View.VISIBLE:View.GONE);
		mHeaderLayout.setVisibility(View.GONE); // Ocultar header de bancomer

		//set title visibility, by default is gone until a call to aplicaEstilo is made
        mTitleLayout.setVisibility(View.GONE);
        mTitleDivider.setVisibility(View.GONE);
        
        // load received layout id in the body
		final LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(layoutID, mBodyLayout, true);
        
        //AMZ
        bm = SuiteAppContratacion.getInstance().getBmovilApplication().getBmovilViewsController();
        st = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
        
        
        /* ---------------------------------------------------- */
        scaleForCurrentScreen();

		btnBack.setOnClickListener(new View.OnClickListener() { //Acción de boton back en header
			@Override
			public void onClick(View view) {
				goBack();
			}
		});
	}
	
	/**
	 * Screen is about to be shown.
	 */
	@Override
	protected void onStart() {
		super.onStart();
		sFields = null;
	}
	
	/* (non-Javadoc)
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onStop() {
		parentViewsController.consumeAccionesDeAlto();
		super.onStop();
	}
	
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		return false;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(final Menu menu) {
		return false;
	}
	
    /**
     * Places the title string and respective icon on screen.
     * @param titleText the screen title
     * @param iconResource the resource of the screen icon
     */
    public void setTitle(final int titleText, final int iconResource) {
    	setTitle(titleText, iconResource, R.color.segundo_azul);
    }
    
    /**
     * Places the title string and respective icon on screen with the desired color
     * @param titleText the screen title
     * @param iconResource the resource of the screen icon
     */
    public void setTitle(final int titleText, final int iconResource, final int colorResource) {
    	
    	if ((activityParameters&SHOW_TITLE)==SHOW_TITLE) {
        	mTitleLayout.setVisibility(View.VISIBLE);
			final ImageView icon = (ImageView)mTitleLayout.findViewById(R.id.title_icon);
        	if(iconResource > 0){
        		icon.setImageDrawable(getResources().getDrawable(iconResource));
        	}
        	if(titleText > 0){
				final String title = getString(titleText);
				final TextView titleTextView = (TextView)mTitleLayout.findViewById(R.id.title_text);
        		titleTextView.setTextColor(getResources().getColor(colorResource));
        		titleTextView.setText(title);
        	}
        	mTitleDivider.setVisibility(View.VISIBLE);
        } else {
        	mTitleLayout.setVisibility(View.GONE);
        	mTitleDivider.setVisibility(View.GONE);
        }
    }

    /**
     * Overrides and cancels android's back button, if the button is visible then it does the same
     * as clicking on the application's back button.
     */
    @Override    
    public boolean onKeyDown(final int keyCode, final KeyEvent event) {
    	onUserInteraction();
    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			//AMZ
    			if(bm!=null){
    			//bm.touchAtras();
    			}
    			if(st!=null){
				//st.touchAtras();
    			}
    			goBack();
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}
    } 
 
    /**
     * 
     * This function has to be overridden by child members, it implements
     * the processing for server response.
     * 
     * @param operationId
     * @param response
     */
    public void processNetworkResponse(final int operationId, final ServerResponse response) {}
    
    /**
     * Determines if the user touched or performed some action in the screen.
     * If he or she did, then reset the logout timer.
     */
	@Override
	public void onUserInteraction() {
		if(Server.ALLOW_LOG) Log.i(getClass().getSimpleName(), "User Interacted");
		if (parentViewsController != null) {
			parentViewsController.onUserInteraction();
		}
		super.onUserInteraction();
	}
	
	/**
	 * Defines when the last click occurred
	 */
	private long touchDownTime;
	
	/*
	 * Called to process touch screen events. 
	 */
	@Override
	public boolean dispatchTouchEvent(final MotionEvent ev) {
		
	    switch (ev.getAction()){
		    case MotionEvent.ACTION_DOWN:
		        touchDownTime = SystemClock.elapsedRealtime();
		        break;
		        
		    case MotionEvent.ACTION_UP:
		    	//to avoid drag events
		        if (SystemClock.elapsedRealtime() - touchDownTime <= 150){

					final EditText[] textFields = this.getFields();
		        	if(textFields != null && textFields.length > 0){
						boolean clickIsOutsideEditTexts = true;
		        		
		        		for(final EditText field : textFields){
		        			if(isPointInsideView(ev.getRawX(), ev.getRawY(), field)){
		        				clickIsOutsideEditTexts = false;
		        				break;
		        			}
		        		}
		        		
		        		if(clickIsOutsideEditTexts){
		        			this.hideSoftKeyboard();
		        		}        		
		        	} else {
		        		this.hideSoftKeyboard();
		        	}
		        }
		        break;
	    }
		
		return super.dispatchTouchEvent(ev);
	}
	
	/**
	 * Override this from children to return the view's fields.
	 * @return the current screen fields.
	 */
	protected EditText[] getFields(){
		return sFields;
	}
	
	/**
	 * Gets the trimmed text of a text field content.
	 * @param textField the EditText component to retrieve text
	 * @return the textField value without trailing spaces
	 */
	protected String getText(final EditText textField){
		if(textField != null){
			return textField.getText().toString().trim();
		} else {
			return "";
		}
	}
	
	public float getHeaderHeight() {
		mHeaderLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		mTitleLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		mTitleDivider.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);

		final float headerHeight = mHeaderLayout.getMeasuredHeight() + mTitleLayout.getMeasuredHeight() +
								mTitleDivider.getMeasuredHeight();
		
		return headerHeight;
	}
	
	public float getBodyHeight() {
		mBodyLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		return mBodyLayout.getMeasuredHeight();
	}
	
	/**
	 * Sets the layout that will fill the empty body layout in base screen
	 * template.
	 *
	 * @param layoutResource the layout resources
	 */
	protected final void setBodyLayout(final int layoutResource) {
		final LayoutInflater inflater = (LayoutInflater)
							this.getSystemService(
									Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(layoutResource, mBodyLayout);
	}


    /**
     * Hides the soft keyboard if any editbox called it.
     */
    public void hideSoftKeyboard() {
		final InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		final View focusedView = getCurrentFocus();
    	if(imm != null && focusedView != null){
    		imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
    	}
    }
    
    /**
     * This is called whenever the back button is pressed.
     * Normally, this function will return to the previous screen.
     */
    public void goBack() {
    	
    	this.hideSoftKeyboard();
    	parentViewsController.setActivityChanging(true);
    	finish();
    	parentViewsController.overrideScreenBackTransition();
		TrackingHelper.touchAtrasState();
    }
    
    
    /**
     * Shows an error message if the provided resource exists. The alert
     * view title will be "Error", and it will have one button to accept.
     *
     * @param errorMessage the error message resource to show
     */
    public void showErrorMessage(final int errorMessage) {
    	if (errorMessage > 0) {	
    		showErrorMessage(getString(errorMessage), null);
    	}
    }
    
    public void showErrorMessage(final String errorMessage) {
    	showErrorMessage(errorMessage, null);
    }
    
    /**
     * Places a progress dialog, for long waiting processes.
     * 
     * @param strTitle the dialog title
     * @param strMessage the message to show in the while
     */
    public void muestraIndicadorActividad(final String strTitle, final String strMessage) {
    	if(mProgressDialog != null){
    		ocultaIndicadorActividad();
    	}
		// Repicacion API Mtto
		mProgressDialog = ProgressDialog.show(SuiteApp.appContext, strTitle,strMessage, true);
		mProgressDialog.setCancelable(false);
    }
    
    /**
     * Hides the progress dialog.
     */
    public void ocultaIndicadorActividad() {
    	if(mProgressDialog != null){
    		mProgressDialog.dismiss();
    	}
    }
	
	/**
     * Shows an error message if the message length is greater than 0. The alert
     * view title will be "Error", and it will have one button to accept.
     *
     * @param errorMessage the error message text to show
     *
     */
    public void showErrorMessage(final String errorMessage, final OnClickListener listener){
    	
    	if(errorMessage.length() > 0){
			// Repicacion API Mtto
			final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SuiteApp.appContext);
			alertDialogBuilder.setTitle(R.string.label_error);
    		alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
    		alertDialogBuilder.setMessage(errorMessage);
    		alertDialogBuilder.setPositiveButton(R.string.label_ok, listener);
    		alertDialogBuilder.setCancelable(false);
        	mAlertDialog = alertDialogBuilder.show();		
    	}
    }
	
    /**
     * Determines if given points are inside view
     * @param x - x coordinate of point
     * @param y - y coordinate of point
     * @param view - view object to compare
     * @return true if the points are within view bounds, false otherwise
     */
    private boolean isPointInsideView(final float x, final float y, final View view){
		final int location[] = new int[2];
		view.getLocationOnScreen(location);
		final int viewX = location[0];
		final int viewY = location[1];

		//point is inside view bounds
		if(( x > viewX && x < (viewX + view.getWidth())) &&
				( y > viewY && y < (viewY + view.getHeight()))){
			return true;
		} else {
			return false;
		}
    }

	public BaseViewsControllerCommons getParentViewsController() {
		return parentViewsController;
	}

	public void setParentViewsController(final BaseViewsControllerCommons parentViewsController) {
		this.parentViewsController = parentViewsController;
	}

	public BaseDelegate getDelegate() {
		return delegate;
	}

	public void setDelegate(final BaseDelegate delegate) {
		this.delegate = delegate;
	}
	
	public void hideCurrentDialog() {
		try {
			if (mAlertDialog != null) {
				mAlertDialog.dismiss();
				mAlertDialog = null;
			}
		} catch(Exception ex) {
			
		}
	}
	
	public void setCurrentDialog(final AlertDialog dialog) {
		this.mAlertDialog = dialog;
	}
	
	/**
	 * Establece la l�gica a seguir para el redimensionamiento de la pantalla.
	 */
	private void scaleForCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        
        guiTools.scale(mHeaderLayout);
		guiTools.scale(mTitleLayout);
		guiTools.scale(mTitleLayout.findViewById(R.id.title_icon));
		guiTools.scale(mTitleLayout.findViewById(R.id.title_text), true);
		guiTools.scale(mTitleDivider);
		guiTools.scale(mBodyLayout);
	}
	
	private ScrollView getScrollView(){
		return(ScrollView)findViewById(SuiteAppContratacion.getResourceId("body_layout_cont", "id"));		
	}

	protected void moverScroll() {
		getScrollView().post(new Runnable() {

			@Override
			public void run() {
				getScrollView().fullScroll(ScrollView.FOCUS_UP);
				if(Server.ALLOW_LOG) Log.d(getClass().getName(), "Mover scroll");
			}
		});		
	}

	// #region Information Alert.
    /**
     * Muestra una alerta con el mensaje dado.
     * @param message el mensaje a mostrar
     */
    public void showInformationAlert(final int message) {
    	if (message > 0) {		
    		showInformationAlert(getString(message));
    	}
    }
    
    /**
     * Shows an information alert with the given message text. No action is performed after this alert is closed.
     * @param message the message text to show
     */
    public void showInformationAlert(final String message) {
    	if (message.length() > 0) {
    		showInformationAlert(message, null);
    	}
    }
    
    /**
     * Shows an information alert with the given message.
     * @param message the message resource to show
     */
    public void showInformationAlert(final int message, final OnClickListener listener) {
    	if (message > 0) {
    		//
			showInformationAlert(SuiteApp.appContext.getString(message), listener);
    	}
    }
    
	/**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(final String message, final OnClickListener listener) {
    	//
		showInformationAlert(SuiteApp.appContext.getString(R.string.label_information), message, listener);
    }
    
    /**
	 * Shows an alert dialog.
	 * @param title The alert title resource id.
	 * @param message The alert message resource id.
	 * @param listener The listener for the "Ok" button. 
	 */
    public void showInformationAlert(final int title, final int message, final OnClickListener listener) {
    	if(message > 0) {
    		showInformationAlert(SuiteApp.appContext.getString(title), getString(message), listener);
    	}
    }
    
    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param title The alert title resource id.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(final String title, final String message, final OnClickListener listener) {
    	showInformationAlert(title, message, SuiteApp.appContext.getString(R.string.label_ok), listener);
    }
    
    
    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param title The alert title resource id.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlertEspecial(final String title, final String errorCode, final String descripcionError, final OnClickListener listener) {
    	showInformationAlertEspecial(title, errorCode, descripcionError, SuiteApp.appContext.getString(R.string.label_ok), listener);
		//Integracion C_R
		//showInformationAlertEspecial(title, errorCode, descripcionError,SuiteAppContratacion.appContext.getString(R.string.label_ok), listener);
    }
    
    /**
	 * Shows an alert dialog.
	 * @param title The alert title resource id.
	 * @param message The alert message resource id.
	 * @param okText the ok text
	 * @param listener The listener for the "Ok" button. 
	 */
    public void showInformationAlert(final int title, final int message, final int okText, final OnClickListener listener) {
    	if(message > 0) {
    		showInformationAlert(getString(title), getString(message), getString(okText), listener);
    	}
    }
    
    /**
	 * Shows an alert dialog.
	 * @param title The alert title.
	 * @param message The alert message.
	 * @param okText the ok text
	 * @param listener The listener for the "Ok" button. 
	 */
	public void showInformationAlert(final String title, final String message, final String okText, final OnClickListener listener) {
		if(!habilitado)
			return;		
		habilitado = false;
    	if(message.length() > 0){
			//AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
			//Replicacion API mtto
			final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SuiteApp.appContext);
    		alertDialogBuilder.setTitle(title);
    		alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);    		
    		alertDialogBuilder.setMessage(message);
			OnClickListener listener1;
    		if (null == listener) {
    			listener1 = new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						dialog.dismiss();
					}
				};
    		}else{
				listener1=listener;
			}

    		alertDialogBuilder.setPositiveButton(okText, listener1);
    		alertDialogBuilder.setCancelable(false);
    		mAlertDialog = alertDialogBuilder.create();
    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(final DialogInterface dialog) {
					habilitado = true;
				}
			});
    		mAlertDialog.show();
    	}
    }
	
	
	 /**
		 * Shows an alert dialog.
		 * @param title The alert title.
		 * @param message The alert message.
		 * @param okText the ok text
		 * @param listener The listener for the "Ok" button. 
		 */
		public void showInformationAlertEspecial(final String title, final String errorCode, final String descripcionError, final String okText, final OnClickListener listener) {
			if(!habilitado)
				return;		
			habilitado = false;
	    	if(descripcionError.length() > 0){

				/*
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(title);
				builder.setIcon(R.drawable.anicalertaaviso);
				LayoutInflater inflater = getLayoutInflater();
				*/
				// Replicacion API Mtto
				final LayoutInflater inflater = (LayoutInflater)SuiteApp.appContext.getSystemService
						(Context.LAYOUT_INFLATER_SERVICE);
				final AlertDialog.Builder builder = new AlertDialog.Builder(SuiteApp.appContext);
				builder.setTitle(title);
				builder.setIcon(SuiteAppContratacion.getResourceId("anicalertaaviso", "drawable"));

				final View vista = inflater.inflate(SuiteAppContratacion.getResourceId("layout_alert_codigo_error_cont", "layout"), null);
	    	    /*builder.setView(vista)
	    	       .setPositiveButton(okText, new DialogInterface.OnClickListener() {
	    	           public void onClick(DialogInterface dialog, int id) {
	    	                  dialog.dismiss();
	    	           }
	    	    });*/
				final OnClickListener listener1;
	    	    if (null == listener) {
	    			listener1 = new OnClickListener() {
						@Override
						public void onClick(final DialogInterface dialog, final int which) {
							dialog.dismiss();
						}
					};
	    		}else{
					listener1=listener;
				}
	    	    builder.setView(vista)
	    	       .setPositiveButton(okText, listener1);
	    	    
	    	    TextView text1 = (TextView) vista.findViewById(SuiteAppContratacion.getResourceId("descripcionError", "id"));
	    	    TextView text2 = (TextView) vista.findViewById(SuiteAppContratacion.getResourceId("codigoError", "id"));
	    	    text1.setText(descripcionError);
	    	    text2.setText(errorCode);
	    	 
	    	    builder.setCancelable(false);
	    		
	    	    mAlertDialog = builder.create();
	    	    
	    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
					@Override
					public void onDismiss(final DialogInterface dialog) {
						habilitado = true;
					}
				});
	    		mAlertDialog.show();
	    	    
	    	}
	    }
	
	public void showYesNoAlert(final String message, final OnClickListener positiveListener) {
		showYesNoAlert(getString(R.string.label_information), 
					   message, 
					   getString(R.string.common_alert_yesno_positive_button), 
					   getString(R.string.common_alert_yesno_negative_button), 
					   positiveListener, 
					   null);
	}
	public void showYesNoAlert1(final String message, final OnClickListener positiveListener, final OnClickListener negativeListener) {
		showYesNoAlert(getString(R.string.label_information),
				message,
				getString(R.string.button_no),
				getString(R.string.button_yes),
				negativeListener,
				positiveListener);
	}
	
	public void showYesNoAlertEspecialTitulo(final String errorCode, final String descripcionError, final OnClickListener positiveListener) {
		showYesNoAlertEspecial(getString(R.string.label_information), 
				 	   errorCode,
				 	   descripcionError,
					   getString(R.string.common_alert_yesno_positive_button), 
					   getString(R.string.common_alert_yesno_negative_button), 
					   positiveListener, 
					   null);
	}
	
	public void showYesNoAlert(final int message, final OnClickListener positiveListener) {
		showYesNoAlert(R.string.label_information, 
					   message, 
					   R.string.common_alert_yesno_positive_button, 
					   R.string.common_alert_yesno_negative_button, 
					   positiveListener, 
					   null);
	}
	
	public void showYesNoAlert(final int message, final OnClickListener positiveListener, final OnClickListener negativeListener) {
		showYesNoAlert(R.string.label_information, 
					   message, 
					   R.string.common_alert_yesno_positive_button, 
					   R.string.common_alert_yesno_negative_button, 
					   positiveListener, 
					   negativeListener);
	}
	
	public void showYesNoAlert(final String title, final String message, final OnClickListener positiveListener) {
		showYesNoAlert(title, 
					   message, 
					   getString(R.string.common_alert_yesno_positive_button), 
					   getString(R.string.common_alert_yesno_negative_button), 
					   positiveListener, 
					   null);
	}
	public void showYesNoAlert(final String message, final OnClickListener positiveListener, final OnClickListener negativeListener) {
		showYesNoAlert("", 
					   message, 
					   getString(R.string.common_alert_yesno_positive_button), 
					   getString(R.string.common_alert_yesno_negative_button), 
					   positiveListener, 
					   negativeListener);
	}
	
	public void showYesNoAlertEspecialTitulo(final String errorCode, final String descripcionError, final OnClickListener positiveListener, final OnClickListener negativeListener) {
		showYesNoAlertEspecial(getString(R.string.label_information), 
					   errorCode,
					   descripcionError,
					   getString(R.string.common_alert_yesno_positive_button), 
					   getString(R.string.common_alert_yesno_negative_button), 
					   positiveListener, 
					   negativeListener);
	}
	
	
	public void showYesNoAlert(final int title, final int message, final OnClickListener positiveListener) {
		showYesNoAlert(title, 
					   message, 
					   R.string.common_alert_yesno_positive_button, 
					   R.string.common_alert_yesno_negative_button, 
					   positiveListener, 
					   null);
	}
	
	
	
	public void showYesNoAlert(final String title, final String message, final String okText, final OnClickListener positiveListener) {
		showYesNoAlert(title, message, okText, getString(R.string.common_alert_yesno_negative_button), positiveListener, null);
	}
	
	public void showYesNoAlert(final int title, final int message, final int okText, final OnClickListener positiveListener) {
		showYesNoAlert(title, message, okText, R.string.common_alert_yesno_negative_button, positiveListener, null);
	}
	
	public void showYesNoAlert(final String title, final String message, final String okText, final String calcelText, final OnClickListener positiveListener) {
		showYesNoAlert(title, message, okText, calcelText, positiveListener, null);
	}
	
	public void showYesNoAlert(final int title, final int message, final int okText, final int calcelText, final OnClickListener positiveListener) {
		showYesNoAlert(title, message, okText, calcelText, positiveListener, null);
	}
	
	public void showYesNoAlert(final int title,
							   final int message,
							   final int okText,
							   final int calcelText,
							   final OnClickListener positiveListener,
							   final OnClickListener negativeListener) {
		showYesNoAlert(getString(title), getString(message), getString(okText), getString(calcelText), positiveListener, negativeListener);
	}
	
	public void showYesNoAlert(final String title,
							   final String message,
							   final String okText,
							   final String calcelText,
							   final OnClickListener positiveListener,
							   final OnClickListener negativeListener) {
		if(!habilitado)
			return;		
		habilitado = false;
    	if(message.length() > 0){
			final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    		alertDialogBuilder.setTitle(title);
    		alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
    		alertDialogBuilder.setMessage(message);
    		alertDialogBuilder.setPositiveButton(okText, positiveListener);
    		
    		if(null == negativeListener) {
    			alertDialogBuilder.setNegativeButton(calcelText, new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						habilitado = true;
						dialog.dismiss();
					}
				});
    		} else {
    			alertDialogBuilder.setNegativeButton(calcelText, negativeListener);
    		}
    		
    		alertDialogBuilder.setCancelable(false);
    		mAlertDialog = alertDialogBuilder.create();
    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(final DialogInterface dialog) {
					habilitado = true;
				}
			});
    		mAlertDialog.show();
	    	
    	}
    }
	
	
	
	
	public void showYesNoAlertEspecial(final String title, final String errorCode, final String descripcionError, final String okText,
									   final String calcelText, final OnClickListener positiveListener,
			OnClickListener negativeListener) {
		if (!habilitado)
			return;
		habilitado = false;
		if (descripcionError.length() > 0) {

			final AlertDialog.Builder builder = new AlertDialog.Builder(this);
    		builder.setTitle(title);
    		builder.setIcon(R.drawable.anicalertaaviso);
			final LayoutInflater inflater = getLayoutInflater();
			final View vista = inflater.inflate(R.layout.layout_alert_codigo_error_cont, null);
    	    builder.setView(vista);
    	    builder.setPositiveButton(okText, positiveListener);
    	    
    	    if (null == negativeListener) {
    	    	builder.setNegativeButton(calcelText,
						new OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog,
												final int which) {
								habilitado = true;
								dialog.dismiss();
							}
						});
			} else {
				builder.setNegativeButton(calcelText,
						negativeListener);
			}

			final TextView text1 = (TextView) vista.findViewById(R.id.descripcionError);
			final TextView text2 = (TextView) vista.findViewById(R.id.codigoError);
    	    text1.setText(descripcionError);
    	    text2.setText(errorCode);
    	 
    	    builder.setCancelable(false);
    		
    	    mAlertDialog = builder.create();
    	    
    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(final DialogInterface dialog) {
					habilitado = true;
				}
			});
    		mAlertDialog.show();
			

		}
	}
	
    // #endregion	
	//private boolean habilitado = true;
	  protected boolean habilitado = true;

	public void setHabilitado(final boolean habilitado) {
		this.habilitado = habilitado;
	}
	
	public boolean isHabilitado() {
		return habilitado;
	}
}