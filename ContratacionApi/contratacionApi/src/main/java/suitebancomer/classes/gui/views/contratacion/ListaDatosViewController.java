package suitebancomer.classes.gui.views.contratacion;

import java.util.ArrayList;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;
import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.bancomer.mbanking.contratacion.R;
import com.bancomer.mbanking.contratacion.SuiteAppContratacion;

public class ListaDatosViewController  extends LinearLayout{

	private BaseViewsControllerCommons parentManager;
	private ListView listview;
	private ArrayList<Object> lista;
	private int numeroCeldas;
	private int numeroFilas;
	private LlenarListadoAdapter adapter;
	private LinearLayout.LayoutParams params;
	private float anchoColumna;
	private LayoutInflater inflater;
	private String title;
	private TextView titulo;
	private ArrayList<String> tablaDatosAColorear;

	private ArrayList<String> tablaTitulosAColorear;
	private ArrayList<String> tablaDatosACambiarTamaño;
	private ArrayList<String> nuevaTablaDatosACambiarTamaño;

	public ArrayList<String> getTablaDatosAColorear() {
		return tablaDatosAColorear;
	}


	public void setTablaDatosAColorear(final ArrayList<String> tablaDatosAColorear) {
		this.tablaDatosAColorear = tablaDatosAColorear;
	}

	public ListaDatosViewController(final Context context, final LinearLayout.LayoutParams layoutParams, final BaseViewsControllerCommons parentManager)
	{
		super(context);
	    inflater = LayoutInflater.from(context);
		final LinearLayout viewLayout = (LinearLayout) inflater.inflate(SuiteAppContratacion.getResourceId("layout_lista_datos_view_cont", "layout"), this, true);
	    viewLayout.setLayoutParams(layoutParams);
		this.parentManager = parentManager;

		listview = (ListView) findViewById(SuiteAppContratacion.getResourceId("layout_lista_Datos_cont", "id"));
		tablaDatosAColorear = new ArrayList<String>();
		tablaDatosAColorear.add(SuiteAppContratacion.appContext.getString(R.string.transferir_interbancario_tabla_resultados_importe));
		tablaDatosAColorear.add(SuiteAppContratacion.appContext.getString(R.string.transferir_dineromovil_resultados_codigo));
		tablaDatosAColorear.add(SuiteAppContratacion.appContext.getString(R.string.administrar_cambiarpassword_folioOperacionLabel));
	
		tablaDatosAColorear.add(SuiteAppContratacion.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_liquidada));
		tablaDatosAColorear.add(SuiteAppContratacion.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_devuelta));
		tablaDatosAColorear.add(SuiteAppContratacion.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_proceso));
		tablaDatosAColorear.add(SuiteAppContratacion.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_validacion));
		tablaDatosAColorear.add(SuiteAppContratacion.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_enviada));

		tablaTitulosAColorear = new ArrayList<String>();
		tablaTitulosAColorear.add(SuiteAppContratacion.appContext.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_titulo_datodelenvio));
		tablaTitulosAColorear.add(SuiteAppContratacion.appContext.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_titulo_datodeenvio));
		tablaTitulosAColorear.add(SuiteAppContratacion.appContext.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_titulo_datosuc));
		
		tablaDatosACambiarTamaño = new ArrayList<String>();
		nuevaTablaDatosACambiarTamaño = new ArrayList<String>();
		nuevaTablaDatosACambiarTamaño.add(SuiteAppContratacion.appContext.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_deporecibido));
		nuevaTablaDatosACambiarTamaño.add(SuiteAppContratacion.appContext.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_titulo_datosuc));
		nuevaTablaDatosACambiarTamaño.add(SuiteAppContratacion.appContext.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_referencianum));
	}

	public void setNumeroFilas(final int numeroFilas) {
		this.numeroFilas = numeroFilas;
	}
	
	public void setTitulo(final int resTitle) {
		this.title = SuiteAppContratacion.appContext.getString(resTitle);
	}

	public void showLista()
	{
		if (title != null) {
			titulo = (TextView) findViewById(SuiteAppContratacion.getResourceId("lista_datos_titulo_cont", "id"));
			titulo.setText(title);
			titulo.setVisibility(View.VISIBLE);
			GuiTools.getCurrent().scale(titulo, true);
		}
		adapter = new LlenarListadoAdapter();
		listview.setAdapter(adapter);
		listview.setEnabled(false);

		final ListAdapter listAdapter = listview.getAdapter();
		int totalHeight = 0;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			final View listItem = listAdapter.getView(i, null, listview);
			listItem.measure(0, 0);
			totalHeight = listItem.getMeasuredHeight()*numeroFilas;
		}

		final ViewGroup.LayoutParams params = listview.getLayoutParams();
		params.height = totalHeight	+ (listview.getDividerHeight() * (listview.getCount() - 1));
		listview.setLayoutParams(params);
		listview.requestLayout();

	}
	
	public ArrayList<Object> getLista() {
		return lista;
	}

	public void setLista(final ArrayList<Object> lista) {
		this.lista = lista;
	}

	public int getNumeroCeldas() {
		return numeroCeldas;
	}

	public void setNumeroCeldas(final int numeroCeldas) {
		this.numeroCeldas = numeroCeldas;
	}

	private class LlenarListadoAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return lista.size();
        }

        @Override
        public Object getItem(final int position) {
            return lista.get(position);
        }

		@Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
			GuiTools guiTools = GuiTools.getCurrent();

			final View convertView1;
			if (convertView == null) {
                convertView1 = inflater.inflate(SuiteAppContratacion.getResourceId("list_item_productos_cont", "layout"), null);
            }else{
				convertView1=convertView;
			}
            
            anchoColumna = 1.0f/getNumeroCeldas();

            if (getNumeroCeldas() == 2) {
	            params = new LayoutParams(0,LayoutParams.WRAP_CONTENT,0.47f);
				final ArrayList<Object> registro = (ArrayList<Object>) lista.get(position);

				final TextView celda1 = (TextView)convertView1.findViewById(SuiteAppContratacion.getResourceId("lista_celda_1", "id"));
	            celda1.setText((String) registro.get(0));
	            celda1.setLayoutParams(params);
	            celda1.setGravity(Gravity.LEFT);
	            celda1.setMaxLines(1);
				celda1.setSingleLine(true);
	            celda1.setSelected(true);

	            if (tablaTitulosAColorear.contains((String) registro.get(0))) {
		            celda1.setTextColor(getResources().getColor(R.color.primer_azul));
				}
	            if (nuevaTablaDatosACambiarTamaño.contains((String) registro.get(0)))
	            {
	            	celda1.setEllipsize(null);
	            	celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 12.0f);
	            }
	            else
		            celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
	            guiTools.tryScaleText(celda1);
	            
	            params = new LayoutParams(0,LayoutParams.WRAP_CONTENT,0.53f);

				final TextView celda2 = (TextView)convertView1.findViewById(SuiteAppContratacion.getResourceId("lista_celda_2", "id"));
	            celda2.setText((String) registro.get(1));
	            if (tablaDatosAColorear.contains((String) registro.get(0))) {
	            	celda2.setTextColor(getResources().getColor(R.color.tercer_azul));
	            	
				}else if(tablaDatosAColorear.contains((String) registro.get(1))){
					if(registro.get(1).toString().equalsIgnoreCase(SuiteAppContratacion.getInstance().getString(R.string.bmovil_consultar_interbancario_estatus_liquidada))){
	            		celda2.setTextColor(getResources().getColor(R.color.verde));
	            	}else if(registro.get(1).toString().equalsIgnoreCase(SuiteAppContratacion.getInstance().getString(R.string.bmovil_consultar_interbancario_estatus_devuelta))){
	            		celda2.setTextColor(getResources().getColor(R.color.magenta));
	            	}else if(registro.get(1).toString().equalsIgnoreCase(SuiteAppContratacion.getInstance().getString(R.string.bmovil_consultar_interbancario_estatus_enviada))){
	            		celda2.setTextColor(getResources().getColor(R.color.tercer_azul));
	            	}else if(registro.get(1).toString().equalsIgnoreCase(SuiteAppContratacion.getInstance().getString(R.string.bmovil_consultar_interbancario_estatus_proceso))){
	            		celda2.setTextColor(getResources().getColor(R.color.naranja));
	            	}else if(registro.get(1).toString().equalsIgnoreCase(SuiteAppContratacion.getInstance().getString(R.string.bmovil_consultar_interbancario_estatus_validacion))){
	            		celda2.setTextColor(getResources().getColor(R.color.naranja));
	            	}
	            }
	            
	            celda2.setLayoutParams(params);
	            celda2.setGravity(Gravity.RIGHT);
	            celda2.setMaxLines(1);
	            if (tablaDatosACambiarTamaño.contains((String) registro.get(0))) {
	            	celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10.0f);
	            	
				}else{
		            celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
				}
				celda2.setSingleLine(true);
	            celda2.setSelected(true);
				guiTools.tryScaleText(celda2);
	            
	            ((TextView)convertView1.findViewById(SuiteAppContratacion.getResourceId("lista_celda_3", "id"))).setVisibility(View.GONE);
	            ((TextView)convertView1.findViewById(SuiteAppContratacion.getResourceId("lista_celda_4", "id"))).setVisibility(View.GONE);
	            ((ImageButton)convertView1.findViewById(SuiteAppContratacion.getResourceId("lista_celda_check", "id"))).setVisibility(View.GONE);
	            
			}
            if(Server.ALLOW_LOG) Log.d("Filas", "fila "+position);
            
            return convertView1;
        }

		@Override
		public long getItemId(final int position) {
			// TODO Auto-generated method stub
			return 0;
		}
    }

}
