package tracking.contratacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.adobe.mobile.Analytics;

public class TrackingHelper {
	
	public static void trackState(final String state, final ArrayList<String> mapa){
		final HashMap<String, Object> purchaseDictionary = new HashMap<String,Object>();
	       purchaseDictionary.put("channel", "android");

	       mapa.add(state);
	       String listString = "";

	       for (final String s : mapa)
	       {
	           listString += ":" + s;
	       }
		   final String res = (purchaseDictionary.get("channel").toString() + listString);
	       
	       switch (mapa.size()) {
	       case 1:
	             purchaseDictionary.put("prop1",res);
	             break;
	       case 2:
	             purchaseDictionary.put("prop2",res);
	             break;
	       case 3:
	             purchaseDictionary.put("prop3",res);
	             break;
	       case 4:
	             purchaseDictionary.put("prop4",res);
	             break;
	       case 5:
	             purchaseDictionary.put("prop5",res);
	             break;
	       case 6:
	             purchaseDictionary.put("prop6",res);
	             break;
	       case 7:
	             purchaseDictionary.put("prop7",res);
	             break;
	       case 8:
	             purchaseDictionary.put("prop8",res);
	             break;
	       case 9:
	             purchaseDictionary.put("prop9",res);
	             break;
	       case 10:
	             purchaseDictionary.put("prop10",res);
	             break;
	       case 11:
	    	   purchaseDictionary.put("prop11",res);
	             break;
	             
	       default:
	             break;
	       }
	       
	       
	       purchaseDictionary.put("appState",res);
	       purchaseDictionary.put("hier1",res);
	       
	       //ARR Encriptacion
	       Analytics.trackState(res,purchaseDictionary);
	       
	}


	//ARR
	public static void trackClickLogin (final Map<String, Object> contextData){
	
		Analytics.trackAction("clickLogin", contextData);

	}
	
	//ARR	
	public static void trackDesconexiones(final Map<String, Object> contextData){
		
		Analytics.trackAction("clickDesconexiones", contextData);
	}
	
	//ARR
	public static void trackMenuPrincipal(final Map<String, Object> contextData){
		
		Analytics.trackAction("clickMenu", contextData);
		
	}
	
	//ARR
	public static void trackFrecuente(final Map<String, Object> contextData){
		
		Analytics.trackAction("clickFrecuentes", contextData);
	}
	
	//ARR
	public static void trackInicioOperacion(final Map<String, Object> contextData){
		Analytics.trackAction("inicioOperacion", contextData);
	}
	
	//ARR
	public static void trackPaso1Operacion(final Map<String, Object> contextData){
		Analytics.trackAction("paso1Operacion", contextData);
	}
	
	//ARR
	public static void trackPaso2Operacion(final Map<String, Object> contextData){
		Analytics.trackAction("paso2Operacion", contextData);
	}
	
	//ARR
		public static void trackPaso3Operacion(final Map<String, Object> contextData){
			Analytics.trackAction("paso3Operacion", contextData);
		}
		
	//ARR
		public static void trackOperacionRealizada(final Map<String, Object> contextData){
			Analytics.trackAction("operacionRealizada", contextData);
		}
	
	//ARR
		public static void trackInicioConsulta(final Map<String, Object> contextData){
			Analytics.trackAction("inicioConsulta", contextData);
		}
		
	//ARR
		public static void trackEnvioConfirmacion(final Map<String, Object> contextData){
			Analytics.trackAction("envioConfirmacion", contextData);
		}
		
		public static void trackConsultaRealizada(final Map<String, Object> contextData){
			Analytics.trackAction("consultaRealizada", contextData);
		}
		//ARR

			public static void trackClickBanner(final Map<String, Object> click_bannerMap, final Map<String, Object> click_bannerMap2) {
				Analytics.trackAction("clickBanner", click_bannerMap);
				// TODO Auto-generated method stub
			}
}
	

