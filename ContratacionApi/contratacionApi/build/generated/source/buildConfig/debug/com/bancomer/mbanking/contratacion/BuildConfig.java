/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.bancomer.mbanking.contratacion;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.bancomer.mbanking.contratacion";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 924;
  public static final String VERSION_NAME = "";
}
