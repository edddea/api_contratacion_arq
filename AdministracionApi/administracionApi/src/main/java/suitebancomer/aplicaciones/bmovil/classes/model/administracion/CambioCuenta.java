package suitebancomer.aplicaciones.bmovil.classes.model.administracion;


import suitebancomer.aplicaciones.bmovil.classes.model.Account;

public class CambioCuenta {

	private Account account;
	
	public Account getAccount() {
		return account;
	}
	
	public void setAccount(final Account account) {
		this.account = account;
	}
}
