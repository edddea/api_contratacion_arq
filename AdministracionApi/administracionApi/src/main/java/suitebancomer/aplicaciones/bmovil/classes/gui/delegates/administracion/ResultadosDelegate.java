package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.util.Log;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ResultadosViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;


public class ResultadosDelegate extends DelegateBaseOperacion {

    public final static long RESULTADOS_DELEGATE_ID = 0x1ef4f4c61ca112bfL;

    private ArrayList<Object> datosLista;
    private DelegateBaseOperacion operationDelegate;
    private int listaOpcionesMenu;
    //One Click
    private boolean isSMS = false;
    private boolean isEmail = false;
    //Termina One Click
    private Boolean frecOpOK = false;

    /**
     * PendingIntent to tell the SMS app to notify us.
     */
    private PendingIntent mSentPendingIntent;

    /**
     * The BroadcastReceiver that we use to listen for the notification back.
     */
    private BroadcastReceiver mBroadcastReceiver;

    private ResultadosViewController resultadosViewController;

    public Boolean getFrecOpOK() {
        return frecOpOK;
    }

    public void setFrecOpOK(final Boolean frecOpOK) {
        this.frecOpOK = frecOpOK;
    }

    public ResultadosDelegate(final DelegateBaseOperacion operationDelegate) {
        this.operationDelegate = operationDelegate;
        listaOpcionesMenu = operationDelegate.getOpcionesMenuResultados();
    }

    public DelegateBaseOperacion getOperationDelegate() {
        return operationDelegate;
    }

    public void setResultadosViewController(final ResultadosViewController viewController) {
        this.resultadosViewController = viewController;
    }

    public BroadcastReceiver getmBroadcastReceiver() {
        return mBroadcastReceiver;
    }

    public void setmBroadcastReceiver(final BroadcastReceiver mBroadcastReceiver) {
        this.mBroadcastReceiver = mBroadcastReceiver;
    }

    public void consultaDatosLista() {
        resultadosViewController.setListaDatos(operationDelegate.getDatosTablaResultados());
        /*AYMB
		 * if(operationDelegate instanceof InterbancariosDelegate){
			//resultadosViewController.setListaClave(((InterbancariosDelegate)operationDelegate).getDatosTablaClave());
			if(!((InterbancariosDelegate)operationDelegate).isBajaFrecuente() && ((InterbancariosDelegate)operationDelegate).validaTC())
				resultadosViewController.setListaClave(((InterbancariosDelegate)operationDelegate).getDatosTablaClave());

		}*/
    }

    public DelegateBaseOperacion consultaOperationDelegate() {
        return operationDelegate;
    }

    public void enviaPeticionOperacion() {

    }

    public void consultaOpcionesMenu() {

    }

    public void consultaTextoSMS() {

    }

    public void enviaSMS() {
        final String smsText = Tools.removeSpecialCharacters(operationDelegate.getTextoSMS());

        mSentPendingIntent = PendingIntent.getBroadcast(resultadosViewController, 0, new Intent(Constants.SENT), 0);
        final SmsManager smsMgr = SmsManager.getDefault();
        resultadosViewController.muestraIndicadorActividad(resultadosViewController.getString(R.string.label_information),
                resultadosViewController.getString(R.string.sms_sending));
        final String mPhone = Session.getInstance(SuiteAppAdmonApi.appContext).getUsername();

        if (mBroadcastReceiver == null) {
            mBroadcastReceiver = resultadosViewController.createBroadcastReceiver();
        }

        final ArrayList<String> messages = smsMgr.divideMessage(smsText);
        for (int i = 0; i < messages.size(); i++) {
            final String text = messages.get(i).trim();
            if (text.length() > 0) {
                if (Server.ALLOW_LOG) Log.d("sms mensaje", text);
                // send the message, passing in the pending intent, sentPI
                smsMgr.sendTextMessage(mPhone, null, text, mSentPendingIntent, null);
                resultadosViewController.registerReceiver(mBroadcastReceiver, new IntentFilter(Constants.SENT));
            }
        }
    }

    public void guardaPDF() {

    }

    public void enviaEmail() {
        //One Click
		/*TODO AMB
		if(operationDelegate instanceof ExitoILCDelegate){
			isEmail=true;
			isSMS=false;
			((ExitoILCDelegate) operationDelegate).setcontroladorExitoILCView(resultadosViewController);
			((ExitoILCDelegate) operationDelegate).realizaOperacion(Server.EXITO_OFERTA, resultadosViewController, true, false);

		}else if(operationDelegate instanceof ExitoEFIDelegate){
			isEmail=true;
			isSMS=false;
			((ExitoEFIDelegate) operationDelegate).setcontroladorExitoILCView(resultadosViewController);
			((ExitoEFIDelegate) operationDelegate).realizaOperacion(Server.EXITO_OFERTA_EFI, resultadosViewController, true, false);
		}else{// Termina One click
			SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().showEnviarCorreo(this);
		}*/
    }

    public void guardaFrecuente() {
        ((BmovilViewsController) resultadosViewController.getParentViewsController()).showAltaFrecuente(operationDelegate);
    }

    public void guardaRapido() {

    }

    public void borraOperacion() {

    }

    @Override
    public int getNombreImagenEncabezado() {
        return operationDelegate.getNombreImagenEncabezado();
    }

    @Override
    public int getTextoEncabezado() {
        return operationDelegate.getTextoEncabezado();
    }

    @Override
    public String getTextoTituloResultado() {
        return operationDelegate.getTextoTituloResultado();
    }

    @Override
    public int getColorTituloResultado() {
        return operationDelegate.getColorTituloResultado();
    }

    @Override
    public String getTextoPantallaResultados() {
        return operationDelegate.getTextoPantallaResultados();
    }

    @Override
    public String getTituloTextoEspecialResultados() {
        return operationDelegate.getTituloTextoEspecialResultados();
    }

    @Override
    public String getTextoEspecialResultados() {
        return operationDelegate.getTextoEspecialResultados();
    }

    @Override
    public int getOpcionesMenuResultados() {
        return listaOpcionesMenu;
    }

    @Override
    public String getTextoAyudaResultados() {
        return operationDelegate.getTextoAyudaResultados();
    }

    //One CLick
    public void analyzeResponse(final int operationId, final ServerResponse response) {
        if (operationId == Server.EXITO_OFERTA) {
            if (isSMS) {
                resultadosViewController.showInformationAlert(R.string.bmovil_alert_exitoefi_sms);
            } else if (isEmail) {
                resultadosViewController.showInformationAlert(R.string.bmovil_alert_exitoefi_email);
            }
        } else if (operationId == Server.EXITO_OFERTA_EFI) {
            if (isSMS) {
                resultadosViewController.showInformationAlert(R.string.bmovil_alert_exitoefi_sms);
            } else if (isEmail) {
                resultadosViewController.showInformationAlert(R.string.bmovil_alert_exitoefi_email);
            }
        }
    }
    //Termina One CLick
}
