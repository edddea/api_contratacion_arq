package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;


public class OfertaILC implements ParsingHandler{
	String importe;
	Account account= new Account();
	String contrato;
	String lineaActual;
	String lineaFinal;
	String fechaCat;
	String cat;
	public ArrayList<Account> accountILC;
	
	public Account getAccount() {
		return account;
	}

	public void setAccount(final Account account) {
		this.account = account;
	}

	public ArrayList<Account> getAccountILC() {
		return accountILC;
	}

	public void setAccountILC(final ArrayList<Account> accountILC) {
		this.accountILC = accountILC;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(final String importe) {
		this.importe = importe;
	}
	public String getContrato() {
		return contrato;
	}

	public void setContrato(final String contrato) {
		this.contrato = contrato;
	}

	public String getLineaActual() {
		return lineaActual;
	}

	public void setLineaActual(final String lineaActual) {
		this.lineaActual = lineaActual;
	}

	public String getLineaFinal() {
		return lineaFinal;
	}

	public void setLineaFinal(final String lineaFinal) {
		this.lineaFinal = lineaFinal;
	}

	public String getFechaCat() {
		return fechaCat;
	}

	public void setFechaCat(final String fechaCat) {
		this.fechaCat = fechaCat;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(final String cat) {
		this.cat = cat;
	}	
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
		importe=parser.parseNextValue("importe");		
		contrato=parser.parseNextValue("numContrato");
		lineaActual=parser.parseNextValue("lineaActual");
		lineaFinal=parser.parseNextValue("lineaFinal");
		fechaCat=parser.parseNextValue("fechaCat");
		cat=parser.parseNextValue("Cat");
		account.setAlias(parser.parseNextValue("alias"));
		account.setNumber(parser.parseNextValue("numeroTarjeta"));			
	} 

}
