package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;


import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.common.administracion.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConfigurarCorreoDeleate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class ConfigurarCorreoViewController extends BaseViewController {

	// #region Variables.
	/**
	 * Delegado asociado con el controlador.
	 */
	private ConfigurarCorreoDeleate ccDelegate;
	
	/**
	 * Campo de texto para ingresar el nuevo correo.
	 */
	private EditText tbNuevoCorreo;
	
	/**
	 * Campo de texto para confirmar el nuevo correo.
	 */
	private EditText tbConfirmarCorreo;
	
	/**
	 * Etiqueta con el correo actual del cliente.
	 */
	private TextView lblCorreoActual;
	// #endregion
	//AMZ
	BmovilViewsController parentManager;
	
	public ConfigurarCorreoViewController() {
		super();
		ccDelegate = null;
		tbNuevoCorreo = null;
		tbConfirmarCorreo = null;
	}
	
	// #region Ciclo de vida.
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, SuiteAppAdmonApi.getResourceId("layout_bmovil_configurar_correo_admon", "layout"));
		SuiteApp.appContext=this;
		setTitle(R.string.bmovil_configurar_correo_titulo, R.drawable.icono_conf_correo);
		//AMZ
		parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("conf correo",parentManager.estados);
		//TrackingHelper.trackState("conf correo");

		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate((ConfigurarCorreoDeleate)parentViewsController.getBaseDelegateForKey(ConfigurarCorreoDeleate.CONFIGURAR_CORREO_DELEGATE_ID));
		ccDelegate = (ConfigurarCorreoDeleate)getDelegate(); 
		ccDelegate.setOwnerController(this);
		
		init();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(ConfigurarCorreoDeleate.CONFIGURAR_CORREO_DELEGATE_ID);
		super.goBack();
	}
	// #endregion
	
	// #region Configurar pantalla.
	/**
	 * Inicializa la pantalla.
	 */
	private void init() {
		findViews();
		scaleForCurrentScreen();
		
		lblCorreoActual.setSelected(true);
		
		lblCorreoActual.setText(Session.getInstance(SuiteAppAdmonApi.appContext).getEmail());
		if(0 == lblCorreoActual.getText().toString().length()) {
			final View correoActualLayout = findViewById(SuiteAppAdmonApi.getResourceId("layoutCorreoActual", "id"));
			if(null != correoActualLayout)
				correoActualLayout.setVisibility(View.GONE);
		}
		
		tbNuevoCorreo.addTextChangedListener(new BmovilTextWatcher(this));
		tbConfirmarCorreo.addTextChangedListener(new BmovilTextWatcher(this));
	}

	/**
	 * Busca la referencia a todas las subvistas necesarias.
	 */
	private void findViews() {
		tbNuevoCorreo = (EditText)findViewById(SuiteAppAdmonApi.getResourceId("tbNuevoCorreo", "id"));
		tbConfirmarCorreo = (EditText)findViewById(SuiteAppAdmonApi.getResourceId("tbConfirmarCorreo", "id"));
		lblCorreoActual = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("lblCorreo", "id"));
	}
	
	/**
	 * Escala las subvistas para la pantalla actual.
	 */
	private void scaleForCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("rootLayout", "id")));
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("lblNuevoCorreo", "id")), true);
		guiTools.scale(tbNuevoCorreo, true);
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("lblConfirmarCorreo", "id")), true);
		guiTools.scale(tbConfirmarCorreo, true);
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("layoutCorreoActual", "id")));
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("lblCorreoActual", "id")), true);
		guiTools.scale(lblCorreoActual, true);
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("btnContinuar", "id")));
	}
	// #endregion
	
	// #region Eventos.
	/**
	 * Evento al presionar el boton Confirmar.
	 * @param sender Vista que invoca el método.
	 */
	public void onBotonConfirmarClick(final View sender) {
		ccDelegate.validarDatos(tbNuevoCorreo.getText().toString(), tbConfirmarCorreo.getText().toString());
		if (ccDelegate.res){
			//AMZ
			final Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
			//AMZ
			paso1OperacionMap.put("evento_paso1", "event46");
			paso1OperacionMap.put("&&products", "operaciones;admin+configura correo");
			paso1OperacionMap.put("eVar12", "paso1:introduce datos");
			TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
		}
	}
	// #endregion
}
