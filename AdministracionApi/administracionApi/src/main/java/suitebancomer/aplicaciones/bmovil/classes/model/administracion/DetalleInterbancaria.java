package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;
/*
 * "nombreBanco": "BANAMEX",
      "importe": "1000",
      "fechaAplicacion": "2014-09-26",
      "IVA": "2.5",
      "cuentaOrigen": "12345678901234567890",
      "cuentaDestino": "12345678901234567891",
      "descripcion": "Prueba Transferencia 1",
      "tipoServicio": "92",
      "referencia": "1234567",
      "claveRastreo": "BNET12345678901234567890",
      "folio": "12345678",
      "estatusOperacion": "301",
      "nombreBeneficiario": "Martha Lopez Perez",
      "DetalleDevolucion": "Cuenta inexistente",
      "horaDevolucion": "10:30:25",
      "horaAcuse": "10:30:00",
      "horaLiquidacion": "",
      "horaAprobacion": "10:29:55",
      "horaAlta": "10:29:40",
      "fechaDevolución": "2014-09-26" 
 * */

public class DetalleInterbancaria implements Comparable<DetalleInterbancaria>{

	String nombreBanco;
	String importe;
	String fechaAplicacion;
	String iva;
	String cuentaOrigen;
	String cuentaDestino;
	String descripcion;
	String tipoServicio;
	String referencia;
	String claveRastreo;
	String folio;
	String estatusOperacion;
	String nombreBeneficiario;
	String detalleDevolicion;
	String horaDevolucion;
	String horaAcuse;
	String horaLiquidacion;
	String horaAprobacion;
	String horaAlta;
	String fechaDevolucion;
	
	public String getNombreBanco() {
		return nombreBanco;
	}
	public void setNombreBanco(final String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(final String importe) {
		this.importe = importe;
	}
	public String getFechaAplicacion() {
		return fechaAplicacion;
	}
	public void setFechaAplicacion(final String fechaAplicacion) {
		this.fechaAplicacion = fechaAplicacion;
	}
	public String getIva() {
		return iva;
	}
	public void setIva(final String iva) {
		this.iva = iva;
	}
	public String getCuentaOrigen() {
		return cuentaOrigen;
	}
	public void setCuentaOrigen(final String cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}
	public String getCuentaDestino() {
		return cuentaDestino;
	}
	public void setCuentaDestino(final String cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}
	public String getTipoServicio() {
		return tipoServicio;
	}
	public void setTipoServicio(final String tipoServicio) {
		this.tipoServicio = tipoServicio;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(final String referencia) {
		this.referencia = referencia;
	}
	public String getClaveRastreo() {
		return claveRastreo;
	}
	public void setClaveRastreo(final String claveRastreo) {
		this.claveRastreo = claveRastreo;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(final String folio) {
		this.folio = folio;
	}
	public String getEstatusOperacion() {
		return estatusOperacion;
	}
	public void setEstatusOperacion(final String estatusOperacion) {
		this.estatusOperacion = estatusOperacion;
	}
	public String getNombreBeneficiario() {
		return nombreBeneficiario;
	}
	public void setNombreBeneficiario(final String nombreBeneficiario) {
		this.nombreBeneficiario = nombreBeneficiario;
	}
	public String getDetalleDevolicion() {
		return detalleDevolicion;
	}
	public void setDetalleDevolicion(final String detalleDevolicion) {
		this.detalleDevolicion = detalleDevolicion;
	}
	public String getHoraDevolucion() {
		return horaDevolucion;
	}
	public void setHoraDevolucion(final String horaDevolucion) {
		this.horaDevolucion = horaDevolucion;
	}
	public String getHoraAcuse() {
		return horaAcuse;
	}
	public void setHoraAcuse(final String horaAcuse) {
		this.horaAcuse = horaAcuse;
	}
	public String getHoraLiquidacion() {
		return horaLiquidacion;
	}
	public void setHoraLiquidacion(final String horaLiquidacion) {
		this.horaLiquidacion = horaLiquidacion;
	}
	public String getHoraAprobacion() {
		return horaAprobacion;
	}
	public void setHoraAprobacion(final String horaAprobacion) {
		this.horaAprobacion = horaAprobacion;
	}
	public String getHoraAlta() {
		return horaAlta;
	}
	public void setHoraAlta(final String horaAlta) {
		this.horaAlta = horaAlta;
	}
	public String getFechaDevolucion() {
		return fechaDevolucion;
	}
	public void setFechaDevolucion(final String fechaDevolucion) {
		this.fechaDevolucion = fechaDevolucion;
	}
	public String getTextoEstatus() {
		// TODO Auto-generated method stub
		if(estatusOperacion.equalsIgnoreCase("201")||estatusOperacion.equalsIgnoreCase("200"))
			return SuiteAppAdmonApi.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_validacion);//"En validación";
		else if(estatusOperacion.equalsIgnoreCase("205"))
			return SuiteAppAdmonApi.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_proceso);//return "En proceso";
		else if(estatusOperacion.equalsIgnoreCase("206"))
			return SuiteAppAdmonApi.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_enviada);//return "Enviada";
		else if(estatusOperacion.equalsIgnoreCase("207"))
			return SuiteAppAdmonApi.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_liquidada);//return "Liquidada";
		else if(estatusOperacion.equalsIgnoreCase("301"))
			return SuiteAppAdmonApi.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_devuelta);//return "Devuelta";
		
		return "";
	}
	@Override
	public int compareTo(final DetalleInterbancaria another) {
		// TODO Auto-generated method stub
		try{
			return claveRastreo.compareTo(another.getClaveRastreo())*-1;
		}catch(Exception e){
			return 0;
		}
		
	}

	
	
}
