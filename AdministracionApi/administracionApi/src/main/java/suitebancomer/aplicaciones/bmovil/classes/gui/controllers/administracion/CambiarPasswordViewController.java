/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.common.administracion.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambiarPasswordDelegate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

/**
 * @author Francisco.Garcia
 *
 */
public class CambiarPasswordViewController extends BaseViewController implements OnClickListener {
	
	CambiarPasswordDelegate cambiarPasswordDelegate;
	EditText contrasenaActualText;
	EditText contrasenaNuevaText;
	EditText contrasenaConfirmacion;
	ImageButton confirmarBoton;
	//AMZ
			private BmovilViewsController parentManager;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE,
				SuiteAppAdmonApi.getResourceId("layout_bmovil_cambiar_password_admon", "layout"));
		SuiteApp.appContext=this;
		setTitle(R.string.administrar_menu_cambiar_contrasena_titulo, R.drawable.bmovil_cambiar_password_icono);
		
		//AMZ
		parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("cambia password",parentManager.estados);
		//TrackingHelper.trackState("cambia password");
		setParentViewsController(SuiteAppAdmonApi.getInstance()
				.getBmovilApplication().getBmovilViewsController());
		
		setDelegate((CambiarPasswordDelegate)parentViewsController.getBaseDelegateForKey(CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID));
		cambiarPasswordDelegate = (CambiarPasswordDelegate)getDelegate(); 
		cambiarPasswordDelegate.setCambiarPasswordViewController(this);
		contrasenaActualText = (EditText)findViewById(SuiteAppAdmonApi.getResourceId("cambio_contrasena_contrasenaActual_text", "id"));
		contrasenaNuevaText = (EditText)findViewById(SuiteAppAdmonApi.getResourceId("cambio_contrasena_contrasenaNueva_text", "id"));
		contrasenaConfirmacion = (EditText)findViewById(SuiteAppAdmonApi.getResourceId("cambio_contrasena_contrasenaConfirmacion_text", "id"));
		confirmarBoton = (ImageButton)findViewById(SuiteAppAdmonApi.getResourceId("cambio_contrasena_boton_confirmar", "id"));
		confirmarBoton.setOnClickListener(this);
		InputFilter[] filters = new InputFilter[1];
		filters[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
		contrasenaActualText.setFilters(filters);
		contrasenaNuevaText.setFilters(filters);
		contrasenaConfirmacion.setFilters(filters);
		contrasenaActualText.setLongClickable(false);
		contrasenaNuevaText.setLongClickable(false);
		contrasenaConfirmacion.setLongClickable(false);
		configurarPantalla();
		
		contrasenaActualText.addTextChangedListener(new BmovilTextWatcher(this));
		contrasenaNuevaText.addTextChangedListener(new BmovilTextWatcher(this));
		contrasenaConfirmacion.addTextChangedListener(new BmovilTextWatcher(this));
	}

	private void configurarPantalla(){
		final GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		//Edittext
		gTools.scale(contrasenaActualText,true);
		gTools.scale(contrasenaNuevaText,true);
		gTools.scale(contrasenaConfirmacion,true);
		//Textview
		gTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("cambio_contrasena_contrasenaActual_label", "id")),true);
		gTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("cambio_contrasena_contrasenaNueva_label", "id")),true);
		gTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("cambio_contrasena_contrasenaConfirmacion_label", "id")),true);
		//Button
		gTools.scale(confirmarBoton);	
		gTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("cambiar_password_resultado_contenedor_superior", "id")));
	}
	
	/*
	 * Estos de cajon! :|
	 */
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID);
		super.goBack();
	}
	@Override
	public void onClick(final View v) {
		// TODO Auto-generated method stub
		if(v == confirmarBoton){			
			if(validaPasswordNuevo()){				
				//peticion al server
				cambiarPasswordDelegate.cambiarPassword();	
				//AMZ
				final Map<String,Object> OperacionMap = new HashMap<String, Object>();
				//AMZ
				OperacionMap.put("evento_realizada","event52");
				OperacionMap.put("&&products","operaciones;admin+cambia password");
				OperacionMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionMap);
			}
		}		
	}
	
	public boolean validaPasswordNuevo(){
	return cambiarPasswordDelegate.validaPasswordNuevo();
	}
	//2
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		cambiarPasswordDelegate.analyzeResponse(operationId, response);
	}
	public EditText getContrasenaActualText() {
		return contrasenaActualText;
	}
	public EditText getContrasenaNuevaText() {
		return contrasenaNuevaText;
	}
	public EditText getContrasenaConfirmacion() {
		return contrasenaConfirmacion;
	}

}
