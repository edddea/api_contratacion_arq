package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioCuentaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConsultarEstatusEnvioEstadodeCuentaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.CuentaEC;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.ConsultarEstatusEnvioEC;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.EstatusEnvioEC;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.ListaSeleccionViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class ConsultarEstatusEnvioEstadodeCuentaViewController extends BaseViewController  implements OnClickListener  {

	ConsultarEstatusEnvioEstadodeCuentaDelegate delegate;
	EstatusEnvioEC consultaEstatusEnviodeEC;
	
	
	private TextView texto1;
	private TextView textoAviso;
	LinearLayout contenedorAviso;
	LinearLayout contenedorPrincipal;
	LinearLayout contenedorCuentas;
	ImageButton  btnContinuar;
	ListaSeleccionViewController listaCuentas;
	//AMZ
	private BmovilViewsController parentManager;
	private TextView textoInfo;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_consulta_estatus_envio_estado_de_cuenta);
		setTitle(R.string.bmovil_Consultar_Estatus_EnvioEC_title, R.drawable.bmovil_administrar_icono);
//		SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController().setCurrentActivityApp(this);
		SuiteApp.appContext=this;
		//AMZ
				parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();
				TrackingHelper.trackState("consultar estatus envio EC",parentManager.estados);

		final SuiteAppAdmonApi suiteApp = (SuiteAppAdmonApi)SuiteAppAdmonApi.getInstance();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate((ConsultarEstatusEnvioEstadodeCuentaDelegate)parentViewsController.getBaseDelegateForKey(ConsultarEstatusEnvioEstadodeCuentaDelegate.CONSULTARESTATUSENVIOESTADODECUENTA_DELEGATE_ID));
		delegate = (ConsultarEstatusEnvioEstadodeCuentaDelegate)getDelegate(); 
		delegate.setViewController(this);		
		if(consultaEstatusEnviodeEC == null)
			consultaEstatusEnviodeEC = new EstatusEnvioEC();
		findViews();
		setMessages();
		scaleForCurrentScreen();
		cargaContenido(delegate);
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		setHabilitado(true);
		getParentViewsController().setCurrentActivityApp(this);
		if (delegate != null) {
		    delegate.setCallerController(this);
		}
	}
	
	@Override
	public void onBackPressed() {
		parentViewsController.removeDelegateFromHashMap(CambioCuentaDelegate.CAMBIO_CUENTA_DELEGATE_ID);
		super.onBackPressed();
	}
	
	/**
	 * 
	 */
	private void findViews() {
	    contenedorAviso = (LinearLayout) findViewById(R.id.seeAviso);
	    contenedorAviso.setVisibility(View.GONE);
		btnContinuar = (ImageButton) findViewById(R.id.consulta_estatus_envio_estado_de_cuenta_btn_continuar);
		btnContinuar.setOnClickListener(this);
		contenedorPrincipal = (LinearLayout) findViewById(R.id.contenedorPrincipal);
		texto1 = (TextView) findViewById(R.id.consulta_estatus_envio_estado_de_cuenta_texto1);
		textoAviso = (TextView) findViewById(R.id.consulta_estatus_envio_estado_de_cuenta_texto2);
		textoInfo = (TextView) findViewById(R.id.consulta_estatus_envio_estado_de_cuenta_informacion);
		contenedorCuentas = (LinearLayout) findViewById(R.id.consulta_estatus_envio_contenedor_cuenta);
		
	}
	
	/**
	 * Escala la vistas en base a la pantalla donde se muestra
	 */
	private void scaleForCurrentScreen(){
		final GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		
		gTools.scale(texto1, true);
		gTools.scale(textoAviso, true);
		gTools.scale(textoInfo, true);
		gTools.scale(contenedorPrincipal);
		gTools.scale(contenedorCuentas);
		gTools.scale(btnContinuar);
	}
	
	/**
	 * Inicializacion de componentes con la lista de cuentas del usuario
	 */
	@SuppressWarnings("deprecation")
	public void inicializarPantalla() {
		final LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		listaCuentas = new ListaSeleccionViewController(this, params,parentViewsController);
		listaCuentas.setConsultarEstatusEnvio(this);

		final ArrayList<Object> cuentasAMostrar = delegate.getCuentasUsuario();
		final ArrayList<Object> encabezado = new ArrayList<Object>();
		encabezado.add(null);
	    encabezado.add(getString(R.string.bmovil_Consultar_Estatus_EnvioEC_Cuenta));
	    encabezado.add(getString(R.string.bmovil_Consultar_Estatus_EnvioEC_Envio));
	    encabezado.add(null);
        listaCuentas.setEncabezado(encabezado);
	    listaCuentas.setDelegate(delegate);
		listaCuentas.setNumeroColumnas(3);
		listaCuentas.setEcFlag(true);
		listaCuentas.setLista(cuentasAMostrar);
		listaCuentas.setOpcionSeleccionada(-1);
		listaCuentas.setSeleccionable(false);
		listaCuentas.setAlturaFija(true); /****/
		listaCuentas.setNumeroFilas(cuentasAMostrar.size());
		listaCuentas.setExisteFiltro(false);
		listaCuentas.setClickable(false);
		listaCuentas.setMarqueeEnabled(false); /**/
		listaCuentas.cargarTabla();
		contenedorCuentas.addView(listaCuentas);			
	}
	
	/**
	 * Retorna el objeto ConsultaEstatusEnviodeEC con la cuenta seleccionada por el usuario 
	 * @return cambioCuenta
	 */
	
	private void cargaContenido(final ConsultarEstatusEnvioEstadodeCuentaDelegate delegate)
	{
		final int operationId = Server.CONSULTAR_ESTATUS_EC;//OP Consulta de estatus envio de estado de cuenta
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
	//	Autenticacion aut = Autenticacion.getInstance();
	//	Perfil perfil = session.getClientProfile();
	//	String cadAutenticacion =  aut.getCadenaAutenticacion(Constants.Operacion.actualizarEstatusEnvioEC, perfil);
		final Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
	//	params.put(ServerConstants.CODIGO_OTP, "");
	//	params.put(Server.J_AUT, cadAutenticacion);
		if(ServerCommons.ALLOW_LOG) {
			Log.d("IUM", session.getIum());
		}
		delegate.doNetworkOperation(operationId, params,true,new ConsultarEstatusEnvioEC(), this);
		
	}
	
	public EstatusEnvioEC getConsultaEstatusEnviodeEC() {
		return consultaEstatusEnviodeEC;
	}
	

	/**
	 * Realiza el set de la cuenta seleccionada al Objeto ConsultaEstatusEnviodeEC
	 * e invoca la confirmacion de la operacion 
	 * 
	 */
	/*
	public void actualizarCuenta(Object cuenta) {
		Account account = (Account) cuenta;
		consultaEstatusEnviodeEC.setAccount(account);
		//AMZ
		Map<String,Object> Paso1OperacionMap = new HashMap<String, Object>();
		//AMZ
		Paso1OperacionMap.put("evento_paso1","event46");
		Paso1OperacionMap.put("&&products","operaciones;admin+cambio cuenta");
		Paso1OperacionMap.put("eVar12","paso1:cambio cuenta");
		TrackingHelper.trackPaso1Operacion(Paso1OperacionMap);
		showConfirmacion();
	} 
	
	*/

	/**
	 * Invoca mostrar la confirmacion al delegate de operacion
	 * 
	 */
	private void showConfirmacion(){
		delegate.showConfirmacion();	
	}
	
	/**
	 * Muestra los siguientes mensajes: texto informativo de reactivacion 
	 * o suspension de envio de estado de cuenta, texto de ayuda para 
	 * autorizar los cambios es necesario oprimir el boton continuar.
	 * 
	 */
	private void setMessages() {
		
		texto1.setText(R.string.bmovil_Consultar_Estatus_EnvioEC_info);
		textoAviso.setText(R.string.bmovil_Consultar_Estatus_EnvioEC_ayuda);
	}
	
	
	public boolean estateChange() {
	    boolean modify = false;
		final ArrayList<CuentaEC> aux = Session.getListaEC();
	    for(int i=0; i<aux.size(); i++) {
		if(aux.get(i).isFlag()) {
		    modify = true;		    
		    break;
		}
	    }
	    return modify;
	}
	/**
	 * 
	 */
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}
	@Override
	public void onClick(final View v) {

	    if  (v == btnContinuar && !parentViewsController.isActivityChanging() ) {
		
	    delegate.setCallerController(this);
	    delegate.validarDatos();

	}if(delegate.res)
	{
		//ARR
		final Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
		
		//ARR
		paso2OperacionMap.put("evento_paso2", "event47");
		paso2OperacionMap.put("&&products", "operaciones;consultar  estatus envio de estado de cuenta");
		paso2OperacionMap.put("eVar12", "paso2:revisa y autoriza");

		TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
	}
	    
	} 
	
	//Muestra el contenedor aviso despues de haber dado 
	//click en checkbox de lista reactivar/suspender envio de estado de cuenta
    public void setContenedorAviso() {
    	contenedorAviso.setVisibility(View.VISIBLE);
	}
	
}