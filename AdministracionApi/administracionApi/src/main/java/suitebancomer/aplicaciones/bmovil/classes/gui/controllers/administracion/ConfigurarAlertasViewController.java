package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import suitebancomer.aplicaciones.bmovil.classes.common.administracion.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConfigurarAlertasDelegate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.CuentaOrigenViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;



public class ConfigurarAlertasViewController extends BaseViewController {
	private ConfigurarAlertasDelegate configurarAlertasDelegate;
	
	private LinearLayout cuentaOrigenLayout;
	private EditText tbMontoRetiros;
	private EditText tbMontoDepositos;
	private CuentaOrigenViewController ctaOrigen;
	//AMZ
	private BmovilViewsController parentManager;
	
	
	public ConfigurarAlertasViewController() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, SuiteAppAdmonApi.getResourceId("layout_bmovil_configurar_alertas_admon", "layout") );
		SuiteApp.appContext = this;
		setTitle(R.string.bmovil_configurar_alertas_titulo, R.drawable.bmovil_alta_frecuente_icono);

		//AMZ
		parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("configurar alertas", parentManager.estados);

		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate((ConfigurarAlertasDelegate)getParentViewsController().getBaseDelegateForKey(ConfigurarAlertasDelegate.CONFIGURAR_ALERTAS_DELEGATE_ID));
		configurarAlertasDelegate = (ConfigurarAlertasDelegate)getDelegate();
		configurarAlertasDelegate.setOwnerController(this);
		init();
	}
	
	private void init() {
		findViews();
		scaleToCurrentScreen();
		cargarCuentaOrigen();
		tbMontoDepositos.addTextChangedListener(new BmovilTextWatcher(this));
		tbMontoRetiros.addTextChangedListener(new BmovilTextWatcher(this));
	}
	
	private void findViews() {
		cuentaOrigenLayout = (LinearLayout)findViewById(R.id.layoutCuentaOrigen);
		tbMontoRetiros = (EditText)findViewById(R.id.tbMontoRetiros);
		tbMontoDepositos = (EditText)findViewById(R.id.tbMontoDepositos);
	}
	
	private void scaleToCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.lblRecibirAlertas), true);
		guiTools.scale(findViewById(R.id.lblRetiros), true);
		guiTools.scale(findViewById(R.id.toggleRetiros), false);
		guiTools.scale(findViewById(R.id.lblMontoRetiros), true);
		guiTools.scale(findViewById(R.id.tbMontoRetiros), true);
		guiTools.scale(findViewById(R.id.lblDepositos), true);
		guiTools.scale(findViewById(R.id.toggleDepositos), true);
		guiTools.scale(findViewById(R.id.lblMontoDepositos), true);
		guiTools.scale(findViewById(R.id.tbMontoDepositos), true);
		guiTools.scale(findViewById(R.id.lblMostarSaldo), true);
		guiTools.scale(findViewById(R.id.toggleMostrarSaldo), true);
		guiTools.scale(findViewById(R.id.lblConsultarTarifas), true);
		
	}
	
	private void cargarCuentaOrigen() {
		cuentaOrigenLayout.addView(ctaOrigen);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		super.goBack();
	}

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		// TODO Auto-generated method stub
		super.processNetworkResponse(operationId, response);
	}
	
	
}
