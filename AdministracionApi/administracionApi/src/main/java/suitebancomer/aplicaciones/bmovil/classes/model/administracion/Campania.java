package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

public class Campania {
	private String claveCampania;
	private String descripcionOferta;
	private String monto;
	private String carrusel;

	public Campania(final String claveCampania, final String descripcionOferta, final String monto, final String carrusel) {
		this.claveCampania = claveCampania;
		this.descripcionOferta = descripcionOferta;
		this.monto = monto;
		this.carrusel = carrusel;
	}

	public String getClaveCampania() {
		return claveCampania;
	}

	public void setClaveCampania(final String claveCampania) {
		this.claveCampania = claveCampania;
	}

	public String getDescripcionOferta() {
		return descripcionOferta;
	}

	public void setDescripcionOferta(final String descripcionOferta) {
		this.descripcionOferta = descripcionOferta;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(final String monto) {
		this.monto = monto;
	}

	public String getCarrusel() {
		return carrusel;
	}

	public void setCarrusel(final String carrusel) {
		this.carrusel = carrusel;
	}

}
