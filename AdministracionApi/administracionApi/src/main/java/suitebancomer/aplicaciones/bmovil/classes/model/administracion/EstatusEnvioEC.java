package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import suitebancomer.aplicaciones.bmovil.classes.model.Account;

public class EstatusEnvioEC {

	private Account account;

	public Account getAccount() {
		return account;
	}
	
	public void setAccount(Account account) {
		this.account = account;
	}
}
