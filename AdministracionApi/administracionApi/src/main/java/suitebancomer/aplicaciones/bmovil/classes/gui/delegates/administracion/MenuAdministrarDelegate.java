package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.util.Log;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.AcercaDeViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.MenuAdministrarViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.ActualizarCuentasResult;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.ConfigurarMontos;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.ConsultaTerminosDeUsoData;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.ConsultarLimitesResult;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;





public class MenuAdministrarDelegate extends DelegateBaseAutenticacion {
	public final static long MENU_ADMINISTRAR_DELEGATE_ID = 0x4ed434c61ca109f1L;

	MenuAdministrarViewController menuAdministrarViewController;

	public MenuAdministrarViewController getMenuAdministrarViewController() {
		return menuAdministrarViewController;
	}

	public void setMenuAdministrarViewController(
			final MenuAdministrarViewController menuAdministrarViewController) {
		this.menuAdministrarViewController = menuAdministrarViewController;
	}

	/**
	 * AcercaDe...
	 * 
	 * */
	AcercaDeViewController acercaDeViewController;

	public AcercaDeViewController getAcercaDeViewController() {
		return acercaDeViewController;
	}

	public void setAcercaDeViewController(
			final AcercaDeViewController acercaDeViewController) {
		this.acercaDeViewController = acercaDeViewController;
	}

	/**
	 * Se encarga de inicializar las etiquetas con la info conrrespondiente
	 */
	public void inicializaAcercaDe() {
		final String version = SuiteAppAdmonApi.appContext
				.getString(R.string.administrar_acercade_version)
				+ " "
				+ Tools.getDoubleAmountFromServerString(Constants.APPLICATION_VERSION);
		final String identificador = SuiteAppAdmonApi.appContext
				.getString(R.string.administrar_acercade_identificador)
				+ " "
				+ SuiteAppAdmonApi.appContext.getString(R.string.app_identifier);
		final String terminal = SuiteAppAdmonApi.appContext
				.getString(R.string.administrar_acercade_terminal)
				+ " "
				+ Tools.getDeviceForAcercaDe();
		acercaDeViewController.getLblVersion().setText(version);
		acercaDeViewController.getLblIdentificador().setText(identificador);
		acercaDeViewController.getLblTerminal().setText(terminal);
	}

	/**
	 * MenuAdministrar
	 */

	@Override
	public void performAction(final Object obj) {

		if (obj instanceof String) {
			final String opcionSeleccionada = (String) obj;
			menuAdministrarViewController
					.opcionSeleccionada(opcionSeleccionada);
		}
	}

	/**
	 * 
	 * @return lista de opciones del menu de administrar
	 */
	public ArrayList<Object> getDatosMenu() {
		final ArrayList<Object> lista = new ArrayList<Object>();
		ArrayList<Object> registros;
		registros = new ArrayList<Object>(2);
		registros.add(Constants.MADMINISTRAR_CAMBIAR_CONTRASENA);
		registros
				.add(SuiteAppAdmonApi.appContext
						.getString(R.string.administrar_menu_cambiar_contrasena_titulo));
		lista.add(registros);
		
//		if(SuiteAppAdmonApi.getInstance().getBmovilApplication().isApplicationLogged()){
//			session = Session.getInstanceAdmin(SuiteApp.appContext);//(SuiteAppAdmonApi.getInstance());
//		}else{
		final Session session = Session.getInstance(SuiteApp.appContext);//(SuiteAppAdmonApi.getInstance());
//
// }
		
		if (configurarMontos()) {
			if (Autenticacion.getInstance().mostrarOperacion(
					Constants.Operacion.cambioLimites,
					session.getClientProfile())) {
				registros = new ArrayList<Object>(2);
				registros.add(Constants.MADMINISTRAR_CONFIGURAR_MONTOS);
				registros
						.add(SuiteAppAdmonApi.appContext
								.getString(R.string.administrar_menu_configurarmontos_titulo));
				lista.add(registros);
			}
		}else{
			if (Autenticacion.getInstance().mostrarOperacion(
					Constants.Operacion.cambioLimites,
					session.getClientProfile())) {
				registros = new ArrayList<Object>(2);
				registros.add(Constants.MADMINISTRAR_CONFIGURAR_MONTOS);
				registros
						.add(SuiteAppAdmonApi.appContext
								.getString(R.string.administrar_menu_configurarmontos_bas_rec_titulo));
				lista.add(registros);
			}
		}

		// if (configurarAlertas()) {
		// registros = new ArrayList<Object>(2);
		// registros.add(Constants.MADMINISTRAR_CONFIGURAR_ALERTAS);
		// registros.add(SuiteApp.appContext.getString(R.string.administrar_menu_configuraralertas_titulo));
		// lista.add(registros);
		// }
		if (Autenticacion.getInstance().mostrarOperacion(
				Constants.Operacion.configurarCorreo,
				session.getClientProfile())) {
			registros = new ArrayList<Object>(2);
			registros.add(Constants.MADMINISTRAR_CONFIGURAR_CORREO);
			registros
					.add(SuiteAppAdmonApi.appContext
							.getString(R.string.administrar_menu_configurarcorreo_titulo));
			lista.add(registros);
		}

		registros = new ArrayList<Object>(2);
		registros.add(Constants.MADMINISTRAR_ACTUALIZAR_CUENTAS);
		registros.add(SuiteAppAdmonApi.appContext
				.getString(R.string.administrar_menu_actualizarcuentas_titulo));
		lista.add(registros);
		if (Autenticacion.getInstance().mostrarOperacion(
				Constants.Operacion.cambioTelefono, session.getClientProfile())) {
			registros = new ArrayList<Object>(2);
			registros.add(Constants.MADMINISTRAR_CAMBIO_TELEFONO);
			registros
					.add(SuiteAppAdmonApi.appContext
							.getString(R.string.administrar_menu_cambiotelefono_titulo));
			lista.add(registros);

		}

		// Incidencia #21907
		final Constants.Perfil profile = Session.getInstance(SuiteAppAdmonApi.appContext)
				.getClientProfile();
		if (Autenticacion.getInstance().mostrarOperacion(
				Constants.Operacion.cambioPerfil, session.getClientProfile())) {
			/*if (profile == Constants.Perfil.avanzado) {
				registros = new ArrayList<Object>(2);
				registros.add(Constants.MADMINISTRAR_OPERAR_SIN_TOKEN);
				registros
						.add(SuiteAppAdmonApi.appContext
								.getString(R.string.administrar_menu_operar_sin_token_titulo));
				lista.add(registros);

			} else*/
			if (profile == Constants.Perfil.basico) {
				if (esVisible() && tipoValido()) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.MADMINISTRAR_OPERAR_CON_TOKEN);
					registros
							.add(SuiteAppAdmonApi.appContext
									.getString(R.string.administrar_menu_operar_con_token_titulo));
					lista.add(registros);
				}
			}else if (profile == Constants.Perfil.recortado) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.MADMINISTRAR_OPERAR_RECORTADO);
					registros
							.add(SuiteAppAdmonApi.appContext
									.getString(R.string.administrar_menu_recortado));
					lista.add(registros);
				}

		}
		registros = new ArrayList<Object>(2);
		registros.add(Constants.MADMINISTRAR_CONSULTAR_CONTRATO);
		registros
				.add(SuiteAppAdmonApi.appContext
						.getString(R.string.administrar_menu_consultar_contrato_titulo));
		lista.add(registros);

		if (Autenticacion.getInstance().mostrarOperacion(
				Constants.Operacion.cambioCuenta, session.getClientProfile())) {
			if (mostrarCambioCuenta()) {
				registros = new ArrayList<Object>(2);
				registros.add(Constants.MADMINISTRAR_CAMBIO_CUENTA);
				registros
						.add(SuiteAppAdmonApi.appContext
								.getString(R.string.administrar_menu_cambiocuenta_titulo));
				lista.add(registros);
			}
		}

		if (Autenticacion.getInstance().mostrarOperacion(
				Constants.Operacion.suspenderCancelar,
				session.getClientProfile())) {
			registros = new ArrayList<Object>(2);
			registros.add(Constants.MADMINISTRAR_SUSPENDER_CANCELAR);
			registros.add(SuiteAppAdmonApi.appContext
					.getString(R.string.administrar_menu_supender_titulo));
			lista.add(registros);
		}
		//SPEI
		if (Autenticacion.getInstance().mostrarOperacion(
				Constants.Operacion.MantenimientoSpeimovil,
				session.getClientProfile())) {
			registros = new ArrayList<Object>(2);
			registros.add(Constants.MADMINISTRAR_ASOCIAR_CELULAR);
			registros.add(SuiteAppAdmonApi.appContext.getString(R.string.bmovil_asociar_cuenta_telefono_menu_consultar));		
			lista.add(registros);
		}
		//Termina SPEI

		//Envio de estado de cuenta
		//if(ConsultarEstatusEnvioEstadodeCuenta()){

		//	registros = new ArrayList<Object>(2);
		//	registros.add(Constants.MADMINISTRAR_CONSULTA_ESTATUS_ENVIO_ESTADO_DE_CUENTA);
		//	registros.add(SuiteApp.appContext.getString(R.string.administrar_menu_envioestadocuenta_titulo));
		//	lista.add(registros);
		//}

		//ModificaciÛn "Menu Administrar"
		/*registros = new ArrayList<Object>(2);
		registros.add(Constants.MADMINISTRAR_NOVEDADES);
		registros.add(SuiteAppAdmonApi.appContext
				.getString(R.string.administrar_novedades));
		lista.add(registros);*/
		
		registros = new ArrayList<Object>(2);
		registros.add(Constants.MADMINISTRAR_ACERCADE);
		registros.add(SuiteAppAdmonApi.appContext
				.getString(R.string.administrar_acercade_titulo));
		lista.add(registros);
		return lista;

	}

	private boolean tipoValido() {
		final String type = Tools.obtenerCuentaEje().getType();
		return !Constants.CREDIT_TYPE.equals(type)
				&& !Constants.EXPRESS_TYPE.equals(type);
	}

	private boolean esVisible() {
		final Account account = Tools.obtenerCuentaEje();
		boolean response = false;
		if (account != null) {
			response = true;
		}
		return response;
	}

	/**
	 * La opcion de configurar montos es visible en el menu administrar
	 * 
	 * @return true si el usuario cuenta con un perfil avanzado
	 */
	boolean configurarMontos() {
		boolean visibleConfigMontos = false;
		final Constants.Perfil profile = Session.getInstance(SuiteAppAdmonApi.appContext)
				.getClientProfile();
		visibleConfigMontos = profile == Constants.Perfil.avanzado;
		return visibleConfigMontos;
	}

	/** TODO QUITAR METODO AYMB
	 * La opcion de enviar estado de cuenta es visible en el menu administrar
	 *
	 * @return false si el usuario cuenta con un perfil recortado
	 *
	boolean ConsultarEstatusEnvioEstadodeCuenta() {
		boolean visibleEstadodeCuenta = true;
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext)
				.getClientProfile();
		visibleEstadodeCuenta = (profile != Constants.Perfil.recortado);
		return visibleEstadodeCuenta;
	}*/

	/**TODO QUITAR METODO AYMB
	 * La opcion de configurar Alertas es visible en el menu administrar
	 * 
	 * @return true si el usuario cuenta con un perfil avanzado
	 *
	boolean configurarAlertas() {
		boolean visibleConfigAlertas = false;
		Constants.Perfil profile = Session.getInstance(SuiteAppAdmonApi.appContext)
				.getClientProfile();
		visibleConfigAlertas = (profile == Constants.Perfil.avanzado);
		return visibleConfigAlertas;
	}*/

	/**
	 * Actualiza las cuentas del usuario
	 *
	public void mostrarAlertaActualizarCuentas() {
		menuAdministrarViewController.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				menuAdministrarViewController.showInformationAlert(
						R.string.borrarDatos_dialogSucceed,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								actualizarCuentas();
							}
						});
			}
		});

	}*/

	/**
	 * Invoca la operacion al server para actualizar las cuentas desde el menu
	 * de administrar
	 * 
	 */
	public void actualizarCuentas() {
		final int operationId = Server.ACTUALIZAR_CUENTAS;
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		final Hashtable<String, String> params = new Hashtable<String, String>();
		params.put("numeroTelefono", session.getUsername());
		params.put("numeroCliente", session.getClientNumber());
		params.put("IUM", session.getIum());
		//JAIG
		doNetworkOperation(operationId, params,true,new ActualizarCuentasResult(), menuAdministrarViewController);
	}

	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {

		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			if (response.getResponse() instanceof ActualizarCuentasResult) {
				final ActualizarCuentasResult result = (ActualizarCuentasResult) response
						.getResponse();
				actualizacionDeCuentasExitosa(result.getAsuntos());
			} else if (response.getResponse() instanceof ConsultarLimitesResult) {
				final ConsultarLimitesResult result = (ConsultarLimitesResult) response
						.getResponse();
				showConfigurarMontos(result);
			} else if (response.getResponse() instanceof ConsultaTerminosDeUsoData) {
				final ConsultaTerminosDeUsoData terminosResponse = (ConsultaTerminosDeUsoData) response
						.getResponse();
				showTerminosDeUso(terminosResponse.getTerminosHtml());
			}
		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
			menuAdministrarViewController.showInformationAlert(response
					.getMessageText());
		}
		menuAdministrarViewController.setRunningTask(false);
	}

	@Override
	public void doNetworkOperation(final int operationId,
			final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {

		BmovilViewsController parent = null;

		if (menuAdministrarViewController.getParentViewsController() instanceof BmovilViewsController) {

			parent = ((BmovilViewsController) menuAdministrarViewController
					.getParentViewsController());
		} else {

			parent = SuiteAppAdmonApi.getInstance().getBmovilApplication()
					.getBmovilViewsController();
		}

		parent.getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,
				caller);
	}

	/**
	 * Realiza la actualizacion de cuentas del usuario cuando la operacion fue
	 * exitosa.
	 * 
	 * @param accounts
	 */
	private void actualizacionDeCuentasExitosa(final Account[] accounts) {
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		session.updateAccounts(accounts);
		menuAdministrarViewController.muestraCuentasActualizadas();
	}

	public void consultarLimites() {

		int operationId = Server.CONSULTAR_LIMITES;
		Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put("numeroCliente", session.getClientNumber());
		params.put("IUM", session.getIum());
		//JAIG
		doNetworkOperation(operationId, params,true,new ConsultarLimitesResult(), menuAdministrarViewController);
	}

	/**
	 * Muestra la pantalla de configurar montos, con los montos indicados en el
	 * objeto ConfigurarMontos
	 * 
	 * @param result
	 */
	private void showConfigurarMontos(final ConsultarLimitesResult result) {

		final ConfigurarMontos cm = new ConfigurarMontos(result.getLimusuOp(),
				result.getLimusuDiario(), result.getLimusuMensual(),
				result.getLimmaxOp(), result.getLimmaxDiario(),
				result.getLimmaxMensual());
		menuAdministrarViewController.showConfigurarMontos(cm);
	}

	/**
	 * Muestra los terminos de uso contratados
	 * 
	 * @param msg
	 */
	private void showTerminosDeUso(final String msg) {
		menuAdministrarViewController.showTerminosDeUso(msg);
	}

	public void showTerminosDeUso() {
		final int operationId = Server.OP_CONSULTAR_TERMINOS_SESION;
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);

		final Constants.Perfil perfil = session.getClientProfile();

		final Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(
				ServerConstants.PERFIL_CLIENTE,
				perfil.equals(Constants.Perfil.avanzado) ? Constants.PROFILE_ADVANCED_03
						: Constants.PROFILE_BASIC_01);
			//jaig
		doNetworkOperation(operationId, params, true,new ConsultaTerminosDeUsoData(), menuAdministrarViewController);
	}

	/**
	 * Retorna true si el usuario tiene mas de una cuenta, false si la opcion de
	 * cambio de cuenta no debe mostrarse.
	 */
	private boolean mostrarCambioCuenta() {
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		return session.getAccounts().length > 1;
	}
	public void showConfirmacion(){
		final int resSubtitle = 0;
		final int resTitleColor = 0;
		final int resIcon = R.drawable.bmovil_configurarmontos_icono;
		final int resTitle = R.string.bmovil_configurar_montos_title;
		SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(this, resIcon, resTitle, resSubtitle, resTitleColor);
	}
	
	
	
	@Override
	public boolean mostrarContrasenia() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.consultaLimites,
				perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		return Autenticacion.getInstance().mostrarCVV(Constants.Operacion.consultaLimites, perfil);
	}
	
	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		return Autenticacion.getInstance().mostrarNIP(Constants.Operacion.consultaLimites,
				perfil);
	}

	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.consultaLimites,
									perfil);
		} catch (Exception ex) {
			Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
			tipoOTP = null;
		}		
		return tipoOTP;
	}
	
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
	
		final ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add("Operación");
		fila.add("Consultar Límites");
		tabla.add(fila);
		fila = new ArrayList<String>();
		fila.add("Perfil");
		final Session sesion= Session.getInstance(menuAdministrarViewController);
		if(Constants.Perfil.avanzado.equals(sesion.getClientProfile())){
		
			fila.add("Avanzado");
		
		}else if(Constants.Perfil.basico.equals(sesion.getClientProfile())){
			fila.add("Básico");
			
		}
		else{
			fila.add("MicroPago");
		}
		tabla.add(fila);
		
		return tabla;
	}
	

	/**
	 * Invoca la operacion para hacer el cambio de cuenta asociada
	 */
	@Override
	public void realizaOperacion(final ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController, final String contrasenia, final String nip, final String token, final String cvv, final String campoTarjeta) {
		//SERVER-DIFERENCE
		
		final int operationId = Server.CONSULTAR_LIMITES;
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		final Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		params.put(ServerConstants.CVE_ACCESO, contrasenia==null?"":contrasenia);
		params.put(ServerConstants.TARJETA_5DIG, campoTarjeta==null?"":campoTarjeta);
		params.put(ServerConstants.CODIGO_NIP, nip==null?"":nip);
		params.put(ServerConstants.CODIGO_CVV2, cvv==null?"":cvv);
		params.put(ServerConstants.CODIGO_OTP, token==null?"":token);
		

		if(Server.ALLOW_LOG) Log.d(">> CGI", "Perfil peticion >> "+session.getClientProfile().name());
		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultaLimites, session.getClientProfile());

		
		
		params.put(ServerConstants.CADENA_AUTENTICACION,cadenaAutenticacion);
		params.put(ServerConstants.VERSION_FLUJO, Constants.APPLICATION_VERSION);
		//JAIG
		doNetworkOperation(operationId, params,true, new ConsultarLimitesResult(), confirmacionAutenticacionViewController);
		
	}
	@Override
	public int getTextoEncabezado() {
		int resTitle;
			resTitle = R.string.administrar_menu_configurarmontos_bas_rec_titulo;	
		return resTitle;
	}

	@Override
	public int getNombreImagenEncabezado() {
		final int resIcon = R.drawable.bmovil_configurarmontos_icono;
		return resIcon;
	}

	
	@Override
	public String getTextoTituloResultado() {
		final int txtTitulo = R.string.transferir_detalle_operacion_exitosaTitle;
		return menuAdministrarViewController.getString(txtTitulo);
	}
	
	public void conusltarLimitesJumpOp(final MenuAdministrarViewController administrarViewController, final String contrasenia, final String nip, final String token, final String cvv, final String campoTarjeta) {
	
		final int operationId = Server.CONSULTAR_LIMITES;
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		final Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		params.put(ServerConstants.CVE_ACCESO, contrasenia==null?"":contrasenia);
		params.put(ServerConstants.TARJETA_5DIG, campoTarjeta==null?"":campoTarjeta);
		params.put(ServerConstants.CODIGO_NIP, nip==null?"":nip);
		params.put(ServerConstants.CODIGO_CVV2, cvv==null?"":cvv);
		params.put(ServerConstants.CODIGO_OTP, token==null?"":token);
		
		params.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
		List<String> listaEncriptar = Arrays.asList(ServerConstants.CVE_ACCESO,
				ServerConstants.CODIGO_NIP, ServerConstants.CODIGO_CVV2);
		Encripcion.setContext(SuiteAppAdmonApi.appContext);
		Encripcion.encriptaCadenaAutenticacion(params, listaEncriptar);
		
		if(Server.ALLOW_LOG) Log.d(">> CGI", "Perfil peticion jump >> "+session.getClientProfile().name());
		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultaLimites, session.getClientProfile());

		
		
		params.put(ServerConstants.CADENA_AUTENTICACION,cadenaAutenticacion);
		params.put(ServerConstants.VERSION_FLUJO, Constants.APPLICATION_VERSION);
		doNetworkOperation(operationId, params,true,new ConsultarLimitesResult(), administrarViewController);
		
	}


}
