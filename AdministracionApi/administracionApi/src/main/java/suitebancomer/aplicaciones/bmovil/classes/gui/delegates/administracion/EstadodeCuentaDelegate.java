package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.bancomer.mbanking.administracion.BmovilApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.EstadodeCuentaViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaEC;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.ConsultaECExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.ConsultaECResult;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;


public class EstadodeCuentaDelegate extends DelegateBaseAutenticacion{

	public final static long ESTADODECUENTA_DELEGATE_ID = 9022323031661701077L;
	protected SuiteAppAdmonApi suiteApp = null;
        
	Account cuentaActual;
	EstadodeCuentaViewController estadodecuentaViewController;
	ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController;
	ConsultaECExtract totalPeriodos;
	ConsultaEC consultaEC;
	String pdfUrl;
	
	public boolean res = false;
	
	private int indicePeriodoSeleccionado;
	
	
	//NEW CODE
	public void setConfirmacionAutenticacionViewController(final ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController)
	{
	    this.confirmacionAutenticacionViewController=confirmacionAutenticacionViewController;
	}
	
	public ConsultaECExtract getTotalPeriodos() {
		return totalPeriodos;
	}
	
	public EstadodeCuentaViewController getEstadodeCuentaViewController() {
		return estadodecuentaViewController;
	}

	public void setEstadodeCuentaViewController(final EstadodeCuentaViewController estadodecuentaViewController) {
		this.estadodecuentaViewController = estadodecuentaViewController;
	}
	
	public Account getCuentaSeleccionada(){
		return estadodecuentaViewController.getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada();
	}
	
	/**
	 * The owner view controller.
	 */
	private BaseViewController ownerController;
	
	/**
	 * @return The view controller associated to this delegate.
	 */
	public BaseViewController getCallerController() {
		return ownerController;
	}

	/**
	 * @param callerController The view controller to associate to this delegate.
	 */
	public void setCallerController(final BaseViewController callerController) {
		this.ownerController = callerController;
	}
	
	public ConsultaEC getconsultaEC() {
		return consultaEC;
	}

	public void setConsultaEC(final ConsultaEC consultaEC) {
		this.consultaEC = consultaEC;
	}

	@Override
	public void performAction(final Object obj) {
		if (obj instanceof Account) {
		    estadodecuentaViewController.borrarListaPeriodos();
		    cuentaActual = getCuentaSeleccionada();
		    cargarPeriodos();
		}

	}
	
	public int getIndicePeriodoSeleccionado() {
		return indicePeriodoSeleccionado;
	}
	

	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada según sea requerido.
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		final Constants.Perfil profile = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final ArrayList<Account> accountsArray = new ArrayList<Account>();
		final Account[] accounts = Session.getInstance(SuiteAppAdmonApi.appContext).getAccounts();

		if(profile ==  Constants.Perfil.avanzado ){

			for(final Account acc : accounts) {
				if(acc.isVisible()) {
					accountsArray.add(acc);
					break;
				}
			}

			for(final Account acc : accounts) {
				if(!acc.isVisible())
					accountsArray.add(acc);
			}

		}else{
			accountsArray.add(Tools.obtenerCuentaEje());
		}
		return accountsArray;
	}

	/**
	 * Carga los periodos
	 *
	 */
	public void cargarPeriodos() {
		final int operationId = Server.OBTENER_PERIODO_EC;
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		final Hashtable<String, String> params = new Hashtable<String, String>();
		params.put("numeroCelular", session.getUsername());
		//params.put("numeroCuenta",getCuentaSeleccionada().getNumber());
		final String cuentaR = getCuentaSeleccionada().getNumber();
		params.put("numeroCuenta",cuentaR.substring(cuentaR.length()-10)); /***AMBIENTACION***/
		params.put("IUM", session.getIum());
		doNetworkOperation(operationId, params, estadodecuentaViewController);
	}

	public void doNetworkOperation(final int operationId, final Hashtable<String, ?> params, final BaseViewController caller) {
		((BmovilViewsController) estadodecuentaViewController
				.getParentViewsController()).getBmovilApp()
				.invokeNetworkOperation(operationId, params,true,null, caller);
	}


	public void analyzeResponse(final int operationId, final ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
		    if(response.getResponse() instanceof ConsultaECExtract){
		    	totalPeriodos = (ConsultaECExtract) response.getResponse();
		    	Log.e("tamano", ""+totalPeriodos.getPeriodos().size());
		    	estadodecuentaViewController.llenaListaDatos();
		    }else if (response.getResponse() instanceof ConsultaECResult) {
				final ConsultaECResult data = (ConsultaECResult) response.getResponse();
		    	//showPdf(Tools.DecodeBase64(data.getPdf()));
		    	showPdf(data.getPdf());
		    }

		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING ||
				response.getStatus() == ServerResponse.OPERATION_ERROR) {
			totalPeriodos = null;
			estadodecuentaViewController.showInformationAlertEspecial(estadodecuentaViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
		}
	}

	/*
	public void validarDatos() {

		if (((EstadodeCuentaViewController) ownerController).comboPeriodo
				.getText().toString().equals("")) {
			((EstadodeCuentaViewController) ownerController)
					.showInformationAlert(R.string.bmovil_Estado_de_cuenta_periodoInvalido);
		} else {
			if (!ownerController.isHabilitado())
				return;
			ownerController.setHabilitado(false);

			/*consultaEC.setPeriodo(((EstadodeCuentaViewController) ownerController).comboPeriodo
							.getText().toString());*/
			/*
			String texto = ((EstadodeCuentaViewController) ownerController).comboPeriodo.getText().toString();
			consultaEC.setPeriodo(texto.substring(0,texto.indexOf("-")-1));
			consultaEC.setFechaCorte(texto.substring(texto.indexOf("-")+2));

			showConfirmacion();

			res = true;
		}
	} */

	public boolean validarDatos() {

		if (((EstadodeCuentaViewController) ownerController).comboPeriodo
				.getText().toString().equals("")) {
			((EstadodeCuentaViewController) ownerController)
					.showInformationAlert(R.string.bmovil_Estado_de_cuenta_periodoInvalido);
			return false;
		} else if (!ownerController.isHabilitado())
				return false;
		else {
			res = true;
			return true;
		}
	}

	@SuppressLint("NewApi")
	public void showPdf(final String pdfData){
		final byte[] data = Base64.decode(pdfData, Base64.DEFAULT);
		final File edoctaFile = new File(Environment.getExternalStorageDirectory().toString() + "/edocta.pdf");
		OutputStream out = null;
		   try {
		     out = new BufferedOutputStream(new FileOutputStream(edoctaFile));
			 out.write(data);
		   } catch (Exception e) {
			if(ServerCommons.ALLOW_LOG)Log.d("FILE", Log.getStackTraceString(e));
		}finally {
		     if (out != null) {
		       try {
				out.close();
			} catch (IOException e) {
				   if(ServerCommons.ALLOW_LOG) Log.d("CLOSE", Log.getStackTraceString(e));
			}
		     }
		   }

		final Uri path = Uri.fromFile(edoctaFile);
		final Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
		//pdfIntent.setData(path);
		//Log.d("URI",path.toString());
		pdfIntent.setDataAndType(path, "application/pdf");
		//pdfIntent.setType("application/pdf");
		pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		try{
			estadodecuentaViewController.startActivity(pdfIntent);
		}catch(ActivityNotFoundException e){
			Toast.makeText(ownerController, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
	    }catch(Exception e){
			if(ServerCommons.ALLOW_LOG) Log.d("ACT",Log.getStackTraceString(e));
	    }
	}




	/**
	 * Obtiene el texto para el encabezado de la confirmacion
	 */
	@Override
	public int getTextoEncabezado() {
		int textoEncabezado = 0;

		textoEncabezado = R.string.consultar_estados_de_cuenta_title;

		return textoEncabezado;
	}

	/**
	 * Obtiene la imagen para mostrar encabezado
	 */
	@Override
	public int getNombreImagenEncabezado() {
		int imgEncabezado = 0;

		imgEncabezado = R.drawable.bmovil_consultar_icono;

		return imgEncabezado;
	}

	public int getColorTituloResultado() {
		int colorRecurso = 0;

		colorRecurso = R.color.verde_bmovil_detalle;

		return colorRecurso;
	}

	public void showConfirmacion() {
		final BmovilApp app = SuiteAppAdmonApi.getInstance().getBmovilApplication();
		final BmovilViewsController bMovilVC = app.getBmovilViewsController();
		if (registrarOperacion())
			bMovilVC.showRegistrarOperacion(this);
		else
			bMovilVC.showConfirmacionAutenticacionViewController(this,
				getNombreImagenEncabezado(),
				getTextoEncabezado(),
				R.string.confirmation_subtitulo, getColorTituloResultado());
	}

	/**
	 * @category RegistrarOperacion
	 * @return
	 */
	public boolean registrarOperacion() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext)
				.getClientProfile();
		Operacion operacion;
			operacion = Operacion.consultarEstadoCuenta;

		final Autenticacion aut = Autenticacion.getInstance();
		final boolean value = aut.validaRegistro(operacion, perfil);
		if(ServerCommons.ALLOW_LOG) Log.d("RegistroOP", value + " consultarEstadoCuenta? " );
		return value;
	}
	/**
	 * @return true si se debe mostrar contrasena, false en caso contrario.
	 */
	@Override

	public boolean mostrarContrasenia() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.consultarEstadoCuenta,
				perfil);

		Log.e("mostrarContrasenia()",String.valueOf(value));
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarCVV(Constants.Operacion.consultarEstadoCuenta, perfil);
		Log.e("mostrarCVV()",String.valueOf(value));
		return value;
	}

	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.consultarEstadoCuenta,
				perfil);
		Log.e("mostrarNIP()",String.valueOf(value));
		return value;
	}

	@Override
	public boolean mostrarCampoTarjeta() {
		return (mostrarCVV() || mostrarNIP());
	}

	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext)
			.getClientProfile();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
		    tipoOTP = Autenticacion.getInstance().tokenAMostrar(Operacion.consultarEstadoCuenta,perfil);

		} catch (Exception ex) {
			Log.e(this.getClass().getName(),
					"Error on Autenticacion.mostrarNIP execution.", ex);
			tipoOTP = null;
		}

		return tipoOTP;
	}

	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {

		final ArrayList<Object> datosConfirmacion = new ArrayList<Object>();
		ArrayList<String> registro;

		registro = new ArrayList<String>();
		registro.add(ownerController
				.getString(R.string.bmovil_Estado_de_cuenta_lblCuenta));
		registro.add(Tools.hideAccountNumber(consultaEC
				.getCuentaOrigen().getNumber()));
		datosConfirmacion.add(registro);

		registro = new ArrayList<String>();
		registro.add(ownerController
				.getString(R.string.bmovil_Estado_de_cuenta_lblPeriodo));
		registro.add(consultaEC.getPeriodo());
		datosConfirmacion.add(registro);

		/*
		registro = new ArrayList<String>();
		registro.add(ownerController
				.getString(R.string.bmovil_Estado_de_cuenta_lblCorreo));
		registro.add(consultaEC.geteMail());
		datosConfirmacion.add(registro);
		*/

	return datosConfirmacion;
}
/*	public ArrayList<Object> getDatosTablaResultados(){

		ArrayList<Object> tabla = new ArrayList<Object>();

		ArrayList<String> fila1;
		fila1 = new ArrayList<String>();
		fila1.add(ownerController.getString(R.string.bmovil_Estado_de_cuenta_lblCuenta));
		fila1.add(Tools.hideAccountNumber(consultaEC.getCuentaOrigen().getNumber()));

		ArrayList<String> fila2;
		fila2 = new ArrayList<String>();
		fila2.add(ownerController.getString(R.string.bmovil_Estado_de_cuenta_lblPeriodo));
		fila2.add(consultaEC.getPeriodo());

		ArrayList<String> fila3;
		fila3 = new ArrayList<String>();
		fila3.add(ownerController.getString(R.string.bmovil_Estado_de_cuenta_lblCorreo));
		fila3.add(consultaEC.geteMail());

		ArrayList<String> fila4;
		fila4 = new ArrayList<String>();
		fila4.add(ownerController.getString(R.string.bmovil_result_fecha));
		String fecha = Tools.formatDate(ConsultaECResult.getFecha());
		fila4.add(fecha);

		ArrayList<String> fila5;
		fila5 = new ArrayList<String>();
		fila5.add(ownerController.getString(R.string.bmovil_result_hora));
		String hora = Tools.parsetime(ConsultaECResult.getHora());
		fila5.add(hora);

		ArrayList<String> fila6;
		fila6 = new ArrayList<String>();
		fila6.add(ownerController.getString(R.string.bmovil_result_folio));
		String folio = ConsultaECResult.getFolio();
		fila6.add(folio);

		tabla.add(fila1);
		tabla.add(fila2);
		tabla.add(fila3);
		tabla.add(fila4);
		tabla.add(fila5);
		tabla.add(fila6);

		return tabla;
	}*/


	/**
	 * Invoka la conexion al server para consultar el estado de cuenta
	 *
	 */
	public void realizaOperacion(final ConfirmacionAutenticacionViewController confirmacionAut,final String contrasenia, final String nip, final String token, final String cvv, final String campoTarjeta) {
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		final Autenticacion aut = Autenticacion.getInstance();
		final Perfil perfil = session.getClientProfile();

		final String cadAutenticacion =  aut.getCadenaAutenticacion(Constants.Operacion.consultarEstadoCuenta, perfil);

		final int operationId = Server.CONSULTAR_ESTADO_CUENTA;
		final Hashtable<String, String> params = new Hashtable<String, String>();

		final String cuentaR = getCuentaSeleccionada().getNumber();
		params.put("numeroCuenta",cuentaR.substring(cuentaR.length()-10)); /***AMBIENTACION***/
		params.put(ServerConstants.PERIODO,consultaEC.getPeriodo());
		params.put(ServerConstants.REFERENCIA ,consultaEC.getReferencia());		
		params.put(ServerConstants.EMAIL,  session.getEmail());
		params.put(ServerConstants.FECHA_CORTE,  consultaEC.getFechaCorte());		
		params.put(ServerConstants.NUMERO_CELULAR, session.getUsername() );
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		
		
			
		
		/**VERIFICAR VALIDAR 10/06**/
		
		params.put(ServerConstants.CADENA_AUTENTICACION, "00100"); //validar
		params.put(ServerConstants.CVE_ACCESO, contrasenia);
		params.put(ServerConstants.CODIGO_NIP, nip);
		params.put(ServerConstants.CODIGO_OTP, token);
		params.put(ServerConstants.TARJETA_5DIG, "¿?");
		params.put(ServerConstants.CODIGO_CVV2, cvv);
		
		
		/**VERIFICAR VALIDAR 10/06**/
		
		params.put(ServerConstants.CVE_ACCESO, contrasenia == null ? "" : contrasenia);			
		params.put(Server.J_NIP, nip == null ? "" : nip);
		params.put(Server.J_CVV2, cvv== null ? "" : cvv);
		params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
		params.put(Server.J_AUT, cadAutenticacion);	
		params.put("tarjeta5Dig", campoTarjeta == null ? "" : campoTarjeta);
		
		/*		
		//params.put(ServerConstants.CADENA_AUTENTICACION, cadAutenticacion); //validar
		Log.e("cadAutenticacion",cadAutenticacion);	
		Log.e("numeroCuenta",cuentaR.substring(cuentaR.length()-10));	
		Log.e("PERIODO",consultaEC.getPeriodo());	
		//Log.e("REFERENCIA",consultaEC.getReferencia());	
		Log.e("EMAIL",session.getEmail());	
		//Log.e("FECHA_CORTE",consultaEC.getFechaCorte());	
		Log.e("NUMERO_CELULAR",session.getUsername());	
		Log.e("JSON_IUM_ETIQUETA",session.getIum());
		Log.e("CVE_ACCESO",contrasenia == null ? "no hay" : contrasenia);	
		Log.e("J_NIP",nip == null ? "no hay" : nip);	
		Log.e("J_CVV2",cvv== null ? "no hay" : cvv);	
		Log.e("CODIGO_OTP",token == null ? "no hay" : token);	
		Log.e("J_AUT",cadAutenticacion);	
		Log.e("tarjeta5Dig",campoTarjeta == null ? "no hay" : campoTarjeta);	
		*/
		
		doNetworkOperation(operationId, params, confirmacionAut);	
	}
	
		
	public String getTextoTituloResultado() {

		final String textoTituloResultado = SuiteAppAdmonApi.appContext.getString(R.string.bmovil_common_resultado_operacion_exitosa);
	    return textoTituloResultado;
	    }
	
	public String getTextoEspecialResultados() {

		final String textoEspecialResultados = SuiteAppAdmonApi.appContext.getString(R.string.bmovil_Estado_de_cuenta_lbltextoEspecial);
	    return textoEspecialResultados;
	    }
	
//	private void showResultados(){
//		BmovilViewsController bmovilParentController = ((BmovilViewsController) estadodecuentaViewController.getParentViewsController());
//		bmovilParentController.showResultadosViewController(this, getNombreImagenEncabezado(), getTextoEncabezado());
//	}
	
	@Override
	public int getOpcionesMenuResultados() {
	// TODO Auto-generated method stub
	return SHOW_MENU_EMAIL;
	}
	
	
}
