package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.webkit.WebView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.HashMap;
import java.util.Map;

import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;



public class TerminosCondicionesViewController extends BaseViewController {
	/**
	 * Campo de texto para terminos de uso.
	 */
	private WebView wvTerminos;
	//AMZ
	BmovilViewsController parentManager;

	//ARR
	Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
		
	
	public TerminosCondicionesViewController() {
		wvTerminos = null;
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppAdmonApi.getResourceId("layout_bmovil_terminos_y_condiciones_admon", "layout"));
		SuiteApp.appContext = this;
		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		setTitle(R.string.bmovil_contratacion_titulo, R.drawable.icono_contratacion);

		findViews();
		scaleToScreenSize();
		final String terminos = (String)this.getIntent().getExtras().get(Constants.TERMINOS_DE_USO_EXTRA);
		if(null != terminos) {
			if(Build.VERSION.SDK_INT < 15){
				wvTerminos.loadDataWithBaseURL(null, terminos, "text/html", "utf-8", null);
			} else {
				wvTerminos.loadData(terminos, "text/html", "utf-8");
			}
		}
		//AMZ
				parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();						
				TrackingHelper.trackState("condiciones",parentManager.estados);
	}

	/**
	 * Busca las referencias a las vistas.
	 */
	private void findViews() {
		wvTerminos = (WebView)findViewById(SuiteAppAdmonApi.getResourceId("webViewTerminos", "id"));
	}
	
	/**
	 * Escala las vistas para acomodarse a la pantalla actual.
	 */
	private void scaleToScreenSize() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("layoutBaseContainer", "id")));
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("lblTitulo", "id")), true);
		guiTools.scale(wvTerminos);
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		SuiteApp.appContext = this;
		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(this);
		super.onResume();
	}

	@Override
	public boolean dispatchTouchEvent(final MotionEvent ev) {
		final float touchX = ev.getX();
		final float touchY = ev.getY();
		final int[] webViewPos = new int[2];
		
		wvTerminos.getLocationOnScreen(webViewPos);

		final float listaSeleccionX2 = webViewPos[0] + wvTerminos.getMeasuredWidth();
		final float listaSeleccionY2 = webViewPos[1] + wvTerminos.getMeasuredHeight();
		
		if ((touchX >= webViewPos[0] && touchX <= listaSeleccionX2) &&
			(touchY >= webViewPos[1] && touchY <= listaSeleccionY2)) {
			wvTerminos.getParent().requestDisallowInterceptTouchEvent(true);
		}
		
		return super.dispatchTouchEvent(ev);
	}
}
