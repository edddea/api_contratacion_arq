/**
 * Parser para obtener la respuesta del server al consultar los limites 
 * de montos del usuario
 */
package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * @author Francisco.Garcia
 * 
 */
public class ConsultarLimitesResult implements ParsingHandler {

	private String limmaxOp;
	private String limmaxDiario;
	private String limmaxMensual;
	private String limusuOp;
	private String limusuDiario;
	private String limusuMensual;
	
	/**
	 * L�mite máximo por operación
	 * @return L�mite máximo por operación definido por Bancomer 
	 */
	public String getLimmaxOp() {
		return limmaxOp;
	}
	
	/**
	 * 
	 * @return L�mite máximo diario definido por Bancomer
	 */
	public String getLimmaxDiario() {
		return limmaxDiario;
	}
	
	/**
	 * 
	 * @return L�mite máximo mensual definido por Bancomer
	 */
	public String getLimmaxMensual() {
		return limmaxMensual;
	}
	
	/***
	 * 
	 * @return L�mite máximo por operación definido por el cliente
	 */
	public String getLimusuOp() {
		return limusuOp;
	}
	
	/**
	 * 
	 * @return L�mite máximo diario definido por el cliente
	 */
	public String getLimusuDiario() {
		return limusuDiario;
	}
	
	/**
	 * 
	 * @return L�mite máximo mensual definido por el cliente
	 */
	public String getLimusuMensual() {
		return limusuMensual;
	}	
	
	/**
	 * 
	 */
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		throw new ParsingException("Error de formato");
	}

	/**
	 * 
	 */
	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		limmaxOp = parser.parseNextValue("limmaxOp");
		limmaxDiario = parser.parseNextValue("limmaxDiario");
		limmaxMensual = parser.parseNextValue("limmaxMensual");
		limusuOp = parser.parseNextValue("limusuOp");
		limusuDiario = parser.parseNextValue("limusuDiario");
		limusuMensual = parser.parseNextValue("limusuMensual");
	}

}
