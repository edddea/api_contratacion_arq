package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;


import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.administracion.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConfirmacionAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.DelegateBaseAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MantenimientoAlertasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.NuevaContraseniaDelegate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.ListaDatosViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class ConfirmacionAutenticacionViewController extends BaseViewController
		implements View.OnClickListener {

	private ImageButton confirmarButton;

	private LinearLayout contenedorPrincipal;
	private LinearLayout contenedorContrasena;
	private LinearLayout contenedorNIP;
	private LinearLayout contenedorASM;
	private LinearLayout contenedorCVV;

	private TextView campoContrasena;
	private TextView campoNIP;
	private TextView campoASM;
	private TextView campoCVV;
	private EditText contrasena;
	private EditText nip;
	private EditText asm;
	private EditText cvv;
	private TextView instruccionesContrasena;
	private TextView instruccionesNIP;
	private TextView instruccionesASM;
	private TextView instruccionesCVV;

	// Nuevo Campo
	private TextView campoTarjeta;
	private LinearLayout contenedorCampoTarjeta;
	private EditText tarjeta;
	private TextView instruccionesTarjeta;

	private ConfirmacionAutenticacionDelegate confirmacionAutenticacionDelegate;
	//AMZ
		public BmovilViewsController parentManager;
		public String titulo;
		//AMZ

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE,
				 SuiteAppAdmonApi.getResourceId("layout_bmovil_confirmacion", "layout"));
		SuiteApp.appContext = this;
		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication()
				.getBmovilViewsController());
		setDelegate((ConfirmacionAutenticacionDelegate)getParentViewsController()
				.getBaseDelegateForKey(
						ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID));
		setTitulo();

		confirmacionAutenticacionDelegate = (ConfirmacionAutenticacionDelegate) getDelegate();
		confirmacionAutenticacionDelegate
				.setConfirmacionAutenticacionViewController(this);

		findViews();
		scaleToScreenSize();

		
		confirmacionAutenticacionDelegate.consultaDatosLista();

		
		configuraPantalla();
		
		moverScroll();
		
		this.statusdesactivado = !((confirmacionAutenticacionDelegate.getOperationDelegate() instanceof NuevaContraseniaDelegate));

		contrasena.addTextChangedListener(new BmovilTextWatcher(this));
		nip.addTextChangedListener(new BmovilTextWatcher(this));
		cvv.addTextChangedListener(new BmovilTextWatcher(this));
		asm.addTextChangedListener(new BmovilTextWatcher(this));
		tarjeta.addTextChangedListener(new BmovilTextWatcher(this));
		//AMZ
		parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("confautent",parentManager.estados);

				/*TODO AMB
				if(!(confirmacionAutenticacionDelegate.getOperationDelegate() instanceof ReactivacionDelegate)){
				//TrackingHelper.trackState("reactivacion", parentManager.estados);
				//}else{
				TrackingHelper.trackState("confautent", parentManager.estados);
				}*/
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		parentViewsController.removeDelegateFromHashMap( confirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		//super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		habilitarBtnContinuar();
		if (statusdesactivado)
			if (!(confirmacionAutenticacionDelegate
					.getOperationDelegate() instanceof MantenimientoAlertasDelegate)) {
				if (parentViewsController.consumeAccionesDeReinicio()) {
					return;
				}
			}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
		//parentViewsController.consumeAccionesDePausa();
		//statusdesactivado = true;
	}

	private void configuraPantalla() {
		mostrarContrasena(confirmacionAutenticacionDelegate
				.consultaDebePedirContrasena());
		mostrarCampoTarjeta(confirmacionAutenticacionDelegate
				.mostrarCampoTarjeta());
		mostrarNIP(confirmacionAutenticacionDelegate.consultaDebePedirNIP());
		mostrarASM(confirmacionAutenticacionDelegate
				.consultaInstrumentoSeguridad());
		mostrarCVV(confirmacionAutenticacionDelegate.consultaDebePedirCVV());

		final LinearLayout contenedorPadre = (LinearLayout) findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_campos_layout","id"));

		if (contenedorContrasena.getVisibility() == View.GONE
				&& contenedorNIP.getVisibility() == View.GONE
				&& contenedorASM.getVisibility() == View.GONE
				&& contenedorCVV.getVisibility() == View.GONE
				&& contenedorCampoTarjeta.getVisibility() == View.GONE) {
			// contenedorPadre.setVisibility(View.GONE);
			contenedorPadre.setBackgroundColor(0);
		}

		contenedorPadre.measure(MeasureSpec.UNSPECIFIED,
				MeasureSpec.UNSPECIFIED);
		final float camposHeight = contenedorPadre.getMeasuredHeight();
		// contenedorPadre.measure(MeasureSpec.UNSPECIFIED,
		// MeasureSpec.UNSPECIFIED);
		// float camposHeight = contenedorPadre.getMeasuredHeight();
		//
		// LinearLayout layoutListaDatos =
		// (LinearLayout)findViewById(R.id.confirmacion_lista_datos);
		// layoutListaDatos.measure(MeasureSpec.UNSPECIFIED,
		// MeasureSpec.UNSPECIFIED);
		// float listaHeight = layoutListaDatos.getMeasuredHeight();
		//
		// ViewGroup contenido =
		// (ViewGroup)this.findViewById(android.R.id.content).getRootView();//findViewById(android.R.id.content);
		// contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		// float contentHeight = contenido.getMeasuredHeight();
		//
		// System.out.println("Los valores " + camposHeight + " y " +
		// contentHeight + " y " + listaHeight);
		//
		// float margin =
		// getResources().getDimension(R.dimen.confirmacion_fields_initial_margin);

		final LinearLayout layoutListaDatos = (LinearLayout) findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_lista_datos","id"));
		layoutListaDatos.measure(MeasureSpec.UNSPECIFIED,
				MeasureSpec.UNSPECIFIED);
		final float listaHeight = layoutListaDatos.getMeasuredHeight();
		// float maximumSize = (contentHeight * 4) / 5;
		// System.out.println("Altura maxima " +maximumSize);
		// float elementsSize = listaHeight + camposHeight;
		// System.out.println("Altura mixta " +elementsSize);
		// float heightParaValidar = (contentHeight*3)/4;
		// System.out.println("heightParaValidar " +contentHeight);
		//
		// if (elementsSize >= contentHeight) {
		// RelativeLayout.LayoutParams camposLayout =
		// (RelativeLayout.LayoutParams)contenedorPadre.getLayoutParams();
		// camposLayout.addRule(RelativeLayout.BELOW,
		// R.id.confirmacion_lista_datos);
		// }

		final ViewGroup contenido = (ViewGroup) this.findViewById(
				android.R.id.content).getRootView();// findViewById(android.R.id.content);
		contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		final float contentHeight = contenido.getMeasuredHeight();

		//System.out.println("Los valores " + camposHeight + " y "
		//	+ contentHeight + " y " + listaHeight);

		final float margin = getResources().getDimension(
				R.dimen.confirmacion_fields_initial_margin);

		final float maximumSize = (contentHeight * 4) / 5;
		//System.out.println("Altura maxima " + maximumSize);
		final float elementsSize = listaHeight + camposHeight;
		//System.out.println("Altura mixta " + elementsSize);
		final float heightParaValidar = (contentHeight * 3) / 4;
		//System.out.println("heightParaValidar " + contentHeight);

		if (elementsSize >= contentHeight) {
			// RelativeLayout.LayoutParams camposLayout =
			// (RelativeLayout.LayoutParams)contenedorPadre.getLayoutParams();
			// camposLayout.addRule(RelativeLayout.BELOW,
			// R.id.confirmacion_lista_datos);
		}

		confirmarButton.setOnClickListener(this);
	}

	public void setTitulo() {
	
		ConfirmacionAutenticacionDelegate confirmacionAutenticacionDelegate = (ConfirmacionAutenticacionDelegate) getDelegate();
		if(confirmacionAutenticacionDelegate==null){
			final DelegateBaseAutenticacion delegateBaseAutenticacion = new DelegateBaseAutenticacion();
			confirmacionAutenticacionDelegate = new ConfirmacionAutenticacionDelegate(delegateBaseAutenticacion);
			setDelegate(confirmacionAutenticacionDelegate);
			
		}
		setTitle(confirmacionAutenticacionDelegate.consultaOperationsDelegate()
				.getTextoEncabezado(), confirmacionAutenticacionDelegate
				.consultaOperationsDelegate().getNombreImagenEncabezado());
	}

	@SuppressWarnings("deprecation")
	public void setListaDatos(final ArrayList<Object> datos) {
		final LinearLayout.LayoutParams params = new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		// params.topMargin =
		// getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
		// params.leftMargin =
		// getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
		// params.rightMargin =
		// getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);

		final ListaDatosViewController listaDatos = new ListaDatosViewController(
				this, params, parentViewsController);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.setTitulo(SuiteAppAdmonApi.getResourceId("confirmation.subtitulo","string"));
		listaDatos.showLista();
		final LinearLayout layoutListaDatos = (LinearLayout) findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_lista_datos","id"));
		layoutListaDatos.addView(listaDatos);
	}

	public void pideContrasenia() {
		findViewById(SuiteAppAdmonApi.getResourceId("campo_confirmacion_contrasena_layout","id")).setVisibility(
				View.GONE);
	}

	public void pideClaveSeguridad() {
		findViewById(SuiteAppAdmonApi.getResourceId("campo_confirmacion_asm_layout","id")).setVisibility(
				View.GONE);
	}

	/*
	* 
	*/
	public void mostrarContrasena(final boolean visibility) {
		contenedorContrasena.setVisibility(visibility ? View.VISIBLE
				: View.GONE);
		campoContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
		contrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
		if (visibility) {
			campoContrasena.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoContrasenia());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(
					Constants.PASSWORD_LENGTH);
			contrasena.setFilters(userFilterArray);
			contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		instruccionesContrasena.setVisibility(View.GONE);
	}

	/*
	* 
	*/
	public void mostrarNIP(final boolean visibility) {
		contenedorNIP.setVisibility(visibility ? View.VISIBLE : View.GONE);
		campoNIP.setVisibility(visibility ? View.VISIBLE : View.GONE);
		nip.setVisibility(visibility ? View.VISIBLE : View.GONE);
		if (visibility) {
			campoNIP.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoNip());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(
					Constants.NIP_LENGTH);
			nip.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
			final String instrucciones = confirmacionAutenticacionDelegate
					.getTextoAyudaNIP();
			if (instrucciones.equals("")) {
				instruccionesNIP.setVisibility(View.GONE);
			} else {
				instruccionesNIP.setVisibility(View.VISIBLE);
				instruccionesNIP.setText(instrucciones);
			}
		} else {
			nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}

	/*
	* 
	*/
	public void mostrarASM(final Constants.TipoOtpAutenticacion tipoOTP) {
		switch (tipoOTP) {
		case ninguno:
			contenedorASM.setVisibility(View.GONE);
			campoASM.setVisibility(View.GONE);
			asm.setVisibility(View.GONE);
			asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
			break;
		case codigo:
		case registro:
			contenedorASM.setVisibility(View.VISIBLE);
			campoASM.setVisibility(View.VISIBLE);
			asm.setVisibility(View.VISIBLE);
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(
					Constants.ASM_LENGTH);
			asm.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			cambiarAccionTexto(nip);
			asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
			break;
		}

		final Constants.TipoInstrumento tipoInstrumento = confirmacionAutenticacionDelegate
				.consultaTipoInstrumentoSeguridad();

		switch (tipoInstrumento) {
		case OCRA:
			campoASM.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoOCRA());
			//asm.setTransformationMethod(null);
			break;
		case DP270:
			campoASM.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoDP270());
			//asm.setTransformationMethod(null);
			break;
		case SoftToken:
			if (SuiteAppAdmonApi.getSofttokenStatus()) {
				asm.setText(Constants.DUMMY_OTP);
				asm.setEnabled(false);
				campoASM.setText(confirmacionAutenticacionDelegate
						.getEtiquetaCampoSoftokenActivado());
			} else {
				asm.setText("");
				asm.setEnabled(true);
				campoASM.setText(confirmacionAutenticacionDelegate
						.getEtiquetaCampoSoftokenDesactivado());
				//asm.setTransformationMethod(null);
			}
			// String otp = GeneraOTPSTDelegate.generarOtpTiempo();
			// if(null != otp) {
			// asm.setText(otp);
			// asm.setEnabled(false);
			// }
			// campoASM.setText(confirmacionAutenticacionDelegate.getOperationDelegate().getEtiquetaCampoSoftokenActivado());
			// break;
		default:
			break;
		}
		final String instrucciones = confirmacionAutenticacionDelegate
				.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
		if (instrucciones.equals("")) {
			instruccionesASM.setVisibility(View.GONE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}
	}

	private void cambiarAccionTexto(final EditText campo) {
		if (campo.getVisibility() == View.VISIBLE) {
			campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
	}

	public String pideContrasena() {
		if (contrasena.getVisibility() == View.GONE) {
			return "";
		} else {
			return contrasena.getText().toString();
		}
	}

	public String pideNIP() {
		if (nip.getVisibility() == View.GONE) {
			return "";
		} else {
			return nip.getText().toString();
		}
	}

	public String pideASM() {
		if (asm.getVisibility() == View.GONE) {
			return "";
		} else {
			return asm.getText().toString();
		}
	}

	public String pideCVV() {
		if (cvv.getVisibility() == View.GONE) {
			return "";
		} else {
			return cvv.getText().toString();
		}
	}

	/*
	* 
	*/
	public void mostrarCVV(final boolean visibility) {
		contenedorCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
		campoCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
		cvv.setVisibility(visibility ? View.VISIBLE : View.GONE);
		instruccionesCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);

		if (visibility) {
			campoCVV.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoCVV());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(
					Constants.CVV_LENGTH);
			cvv.setFilters(userFilterArray);
			final String instrucciones = confirmacionAutenticacionDelegate
					.getTextoAyudaCVV();
			instruccionesCVV.setText(instrucciones);
			instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE
					: View.VISIBLE);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			cambiarAccionTexto(nip);
			cambiarAccionTexto(asm);
			cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}

	@Override
	public void onClick(final View v) {
		confirmarButton.setEnabled(false);
		if (v == confirmarButton && !parentViewsController.isActivityChanging()) {
			botonConfirmarClick();
		}
	}

	public void botonConfirmarClick() {
		confirmacionAutenticacionDelegate.enviaPeticionOperacion();
		if(confirmacionAutenticacionDelegate.res == true){
			//AMZ inicio
			titulo = SuiteAppAdmonApi.appContext.getString(confirmacionAutenticacionDelegate.getOperationDelegate().getTextoEncabezado());

			final Map<String,Object> OperacionRealizadaMap = new HashMap<String, Object>();
				if(titulo == SuiteAppAdmonApi.appContext.getString(SuiteAppAdmonApi.getResourceId("bmovil.consultar.dineromovil.titulo","string"))){
					//AMZ
					OperacionRealizadaMap.put("evento_realizada","event52");
					OperacionRealizadaMap.put("&&products","operaciones;cancelar dinero movil");
					OperacionRealizadaMap.put("eVar12","operacion_cancelada");
					TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);

				}else if(titulo == SuiteAppAdmonApi.appContext.getString(SuiteAppAdmonApi.getResourceId("bmovil.cambio.cuenta.title","string"))){
					//AMZ
					OperacionRealizadaMap.put("evento_realizada","event52");
					OperacionRealizadaMap.put("&&products","operaciones;admin+cambio cuenta");
					OperacionRealizadaMap.put("eVar12","operacion realizada");
					TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == SuiteAppAdmonApi.appContext.getString(SuiteAppAdmonApi.getResourceId("bmovil.configurar.correo.titulo","string"))){
					//AMZ
					OperacionRealizadaMap.put("evento_realizada","event52");
					OperacionRealizadaMap.put("&&products","operaciones;admin+configura correo");
					OperacionRealizadaMap.put("eVar12","operacion realizada");
					TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == SuiteAppAdmonApi.appContext.getString(SuiteAppAdmonApi.getResourceId("bmovil.cambio.telefono.title","string"))){
					//AMZ
					OperacionRealizadaMap.put("evento_realizada","event52");
					OperacionRealizadaMap.put("&&products","operaciones;admin+cambio celular");
					OperacionRealizadaMap.put("eVar12","operacion realizada");
					TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == SuiteAppAdmonApi.appContext.getString(SuiteAppAdmonApi.getResourceId("bmovil.suspender.title","string"))){
					//AMZ
					OperacionRealizadaMap.put("evento_realizada","event52");
					OperacionRealizadaMap.put("&&products","operaciones;admin+suspender o cancelar");
					OperacionRealizadaMap.put("eVar12","operacion realizada");
					TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == SuiteAppAdmonApi.appContext.getString(SuiteAppAdmonApi.getResourceId("bmovil.cancelar.title","string"))){
					//AMZ
					OperacionRealizadaMap.put("evento_realizada","event52");
					OperacionRealizadaMap.put("&&products","operaciones;admin+suspender o cancelar");
					OperacionRealizadaMap.put("eVar12","operacion realizada");
					TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == SuiteAppAdmonApi.appContext.getString(SuiteAppAdmonApi.getResourceId("bmovil.consultar.montos.title","string"))){
					//AMZ
					OperacionRealizadaMap.put("evento_realizada","event52");
					OperacionRealizadaMap.put("&&products","operaciones;admin+configura montos");
					OperacionRealizadaMap.put("eVar12","operacion realizada");
					TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}
			}
	}

	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {

		if(SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController().getBaseDelegateForKey(
				ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID) !=null	){
			confirmacionAutenticacionDelegate =
					(ConfirmacionAutenticacionDelegate)SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController()
					.getBaseDelegateForKey(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		}
		confirmacionAutenticacionDelegate.analyzeResponse(operationId, response);
	}

	private void findViews() {
		
		
		contenedorPrincipal = (LinearLayout) findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_lista_datos","id"));
		contenedorContrasena = (LinearLayout) findViewById(SuiteAppAdmonApi.getResourceId("campo_confirmacion_contrasena_layout","id"));
		contenedorNIP = (LinearLayout) findViewById(SuiteAppAdmonApi.getResourceId("campo_confirmacion_nip_layout","id"));
		contenedorASM = (LinearLayout) findViewById(SuiteAppAdmonApi.getResourceId("campo_confirmacion_asm_layout","id"));
		contenedorCVV = (LinearLayout) findViewById(SuiteAppAdmonApi.getResourceId("campo_confirmacion_cvv_layout","id"));

		contrasena = (EditText) contenedorContrasena
				.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_contrasena_edittext","id"));
		nip = (EditText) contenedorNIP
				.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_nip_edittext","id"));
		asm = (EditText) contenedorASM
				.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_asm_edittext","id"));
		cvv = (EditText) contenedorCVV
				.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_cvv_edittext","id"));

		campoContrasena = (TextView) contenedorContrasena
				.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_contrasena_label","id"));
		campoNIP = (TextView) contenedorNIP
				.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_nip_label","id"));
		campoASM = (TextView) contenedorASM
				.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_asm_label","id"));
		campoCVV = (TextView) contenedorCVV
				.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_cvv_label","id"));

		instruccionesContrasena = (TextView) contenedorContrasena
				.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_contrasena_instrucciones_label","id"));
		instruccionesNIP = (TextView) contenedorNIP
				.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_nip_instrucciones_label","id"));
		instruccionesASM = (TextView) contenedorASM
				.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_asm_instrucciones_label","id"));
		instruccionesCVV = (TextView) contenedorCVV
				.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_cvv_instrucciones_label","id"));

		contenedorCampoTarjeta = (LinearLayout) findViewById(SuiteAppAdmonApi.getResourceId("campo_confirmacion_campotarjeta_layout","id"));
		tarjeta = (EditText) contenedorCampoTarjeta.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_campotarjeta_edittext","id"));
		campoTarjeta = (TextView) contenedorCampoTarjeta.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_campotarjeta_label","id"));
		instruccionesTarjeta = (TextView) contenedorCampoTarjeta.findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_campotarjeta_instrucciones_label","id"));



		confirmarButton = (ImageButton) findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_confirmar_button","id"));
	}

	private void scaleToScreenSize() {

		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		// guiTools.scaleAll(contenedorPrincipal);

		// guiTools.scaleAll((LinearLayout)findViewById(R.id.confirmacion_parent_parent_view));

		guiTools.scale(contenedorPrincipal);
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("confirmacion_campos_layout","id")));

		guiTools.scale(contenedorContrasena);
		guiTools.scale(contenedorNIP);
		guiTools.scale(contenedorASM);
		guiTools.scale(contenedorCVV);

		guiTools.scale(contrasena, true);
		guiTools.scale(nip, true);
		guiTools.scale(asm, true);
		guiTools.scale(cvv, true);

		guiTools.scale(campoContrasena, true);
		guiTools.scale(campoNIP, true);
		guiTools.scale(campoASM, true);
		guiTools.scale(campoCVV, true);

		guiTools.scale(instruccionesContrasena, true);
		guiTools.scale(instruccionesNIP, true);
		guiTools.scale(instruccionesASM, true);
		guiTools.scale(instruccionesCVV, true);

		// nuevo campo
		guiTools.scale(contenedorCampoTarjeta);
		guiTools.scale(tarjeta, true);
		guiTools.scale(campoTarjeta, true);
		guiTools.scale(instruccionesTarjeta, true);

		guiTools.scale(confirmarButton);

	}

	public void limpiarCampos() {
		contrasena.setText("");
		nip.setText("");
		asm.setText("");
		cvv.setText("");
		tarjeta.setText("");
	}

	public void habilitarBtnContinuar() {
		confirmarButton.setEnabled(true);
	}

	public String pideTarjeta() {
		if (tarjeta.getVisibility() == View.GONE) {
			return "";
		} else {
			return tarjeta.getText().toString();
		}
	}

	private void mostrarCampoTarjeta(final boolean visibility) {
		contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE
				: View.GONE);
		campoTarjeta.setVisibility(visibility ? View.VISIBLE : View.GONE);
		tarjeta.setVisibility(visibility ? View.VISIBLE : View.GONE);
		if (visibility) {
			campoTarjeta.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoTarjeta());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(5);
			tarjeta.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
			final String instrucciones = confirmacionAutenticacionDelegate
					.getTextoAyudaTarjeta();
			if (instrucciones.equals("")) {
				instruccionesTarjeta.setVisibility(View.GONE);
			} else {
				instruccionesTarjeta.setVisibility(View.VISIBLE);
				instruccionesTarjeta.setText(instrucciones);
			}
		} else {
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}

	}


	


}
