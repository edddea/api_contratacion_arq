package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ConsultaTarjetaContratacionData implements ParsingHandler {
	/**
	 * Identificador
	 */
	private static final long serialVersionUID = -6969030739631399018L;

	// #region Variables.
	/**
	 * Estado de la operación
	 */
	private String estado;

	/**
	 * El número del cliente.
	 */
	private String numeroCliente;

	/**
	 * El numero de cuenta del cliente.
	 */
	private String numeroCuenta;

	/**
	 * Tipo de tarjeta ingresada
	 */
	private String tipoTarjeta;

	/**
	 * Indica si es la unica tarjeta del cliente
	 */
	private String monoProducto;

	/**
	 * Validacion de las alertas
	 */
	private String validacionAlertas;

	/**
	 * Numero telefonico asociado a alertas
	 */
	private String numeroAlertas;

	/**
	 * Compa�ia telefonica asociada a Alertas
	 */
	private String companiaAlertas;

	/**
	 * Tipo de instrumento contrtado.
	 */
	private String tipoInstrumento;

	/**
	 * El estatus del instrumento.
	 */
	private String estatusInstrumento;

	/**
	 * Tipo de persona.
	 */
	private String tipoPersona;

	/**
	 * La fecha de contratación.
	 */
	private String fechaContratacion;

	/**
	 * La fecha de modificación.
	 */
	private String fechaModificacion;

	// #endregion

	// #region Setters/Getters

	public String getEstado() {
		return estado;
	}

	public void setEstado(final String estado) {
		this.estado = estado;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(final String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getMonoProducto() {
		return monoProducto;
	}

	public void setMonoProducto(final String monoProducto) {
		this.monoProducto = monoProducto;
	}

	public String getValidacionAlertas() {
		return validacionAlertas;
	}

	public void setValidacionAlertas(final String validacionAlertas) {
		this.validacionAlertas = validacionAlertas;
	}

	public String getNumeroAlertas() {
		return numeroAlertas;
	}

	public void setNumeroAlertas(final String numeroAlertas) {
		this.numeroAlertas = numeroAlertas;
	}

	public String getCompaniaAlertas() {
		return companiaAlertas;
	}

	public void setCompaniaAlertas(final String companiaAlertas) {
		this.companiaAlertas = companiaAlertas;
	} 
	/**
	 * @return El número del cliente.
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}

	/**
	 * @param numeroCliente
	 *            El número del cliente a establecer.
	 */
	public void setNumeroCliente(final String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	/**
	 * @return El número de cuenta del cliente.
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	/**
	 * @param numeroCuenta
	 *            El número de cuenta del cliente a establecer.
	 */
	public void setNumeroCuenta(final String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	/**
	 * @return Tipo de instrumento contrtado.
	 */
	public String getTipoInstrumento() {
		return tipoInstrumento;
	}

	/**
	 * @param tipoInstrumento
	 *            Tipo de instrumento contrtado a establecer.
	 */
	public void setTipoInstrumento(final String tipoInstrumento) {
		this.tipoInstrumento = tipoInstrumento;
	}

	/**
	 * @return El estatus del instrumento.
	 */
	public String getEstatusInstrumento() {
		return estatusInstrumento;
	}

	/**
	 * @param estatusInstrumento
	 *            El estatus del instrumento a establecer.
	 */
	public void setEstatusInstrumento(final String estatusInstrumento) {
		this.estatusInstrumento = estatusInstrumento;
	}

	/**
	 * @return La fecha de contratación.
	 */
	public String getFechaContratacion() {
		return fechaContratacion;
	}

	/**
	 * @param fechaContratacion
	 *            La fecha de contratación a establecer.
	 */
	public void setFechaContratacion(final String fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}

	/**
	 * @return La fecha de modificación.
	 */
	public String getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion
	 *            La fecha de modificación a establecer.
	 */
	public void setFechaModificacion(final String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return Tipo de persona.
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}

	/**
	 * @param tipoPersona
	 *            Tipo de persona.
	 */
	public void setTipoPersona(final String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	
	// #endregion

	/**
	 * Crea una nueva instncia vacia de la clase.
	 */
	public ConsultaTarjetaContratacionData() {
		estado = null;
		numeroCliente = null;
		numeroCuenta = null;
		tipoTarjeta = null;
		monoProducto = null;
		validacionAlertas = null;
		numeroAlertas = null;
		companiaAlertas = null;
		tipoInstrumento = null;
		estatusInstrumento = null;
		fechaContratacion = null;
		fechaModificacion = null;
		tipoPersona = null;
	}

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		estado = null;
		numeroCliente = null;
		numeroCuenta = null;
		tipoTarjeta = null;
		monoProducto = null;
		validacionAlertas = null;
		numeroAlertas = null;
		companiaAlertas = null;
		tipoInstrumento = null;
		estatusInstrumento = null;
		fechaContratacion = null;
		fechaModificacion = null;
		tipoPersona = null;
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		estado = parser.parseNextValue("estado");
		numeroCliente = parser.parseNextValue("numeroCliente");
		numeroCuenta = parser.parseNextValue("numeroCuenta");
		tipoTarjeta = parser.parseNextValue("tipoTarjeta");
		monoProducto = parser.parseNextValue("monoProducto");
		validacionAlertas = parser.parseNextValue("validacionAlertas");
		numeroAlertas = parser.parseNextValue("numeroAlertas");
		companiaAlertas = parser.parseNextValue("companiaAlertas");
		tipoInstrumento = parser.parseNextValue("tipoInstrumento");
		estatusInstrumento = parser.parseNextValue("estatusInstrumento");
		tipoPersona = parser.parseNextValue("tipoPersona");
		fechaContratacion = parser.parseNextValue("fechaContratacion");		
		fechaModificacion = parser.parseNextValue("fechaModificacion");
	}
}
