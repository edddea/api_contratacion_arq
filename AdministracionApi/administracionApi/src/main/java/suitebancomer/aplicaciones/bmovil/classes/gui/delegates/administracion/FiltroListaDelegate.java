package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.FiltroListaViewController;

public class FiltroListaDelegate
{
	
	ArrayList<Object> listaOriginal;
	FiltroListaViewController filtroLlistaviewcontroller;
	ArrayList<Object> nuevaLista;
	
	
	public FiltroListaViewController getFiltroListaviewcontroller() {
		return filtroLlistaviewcontroller;
	}

	public void setFiltroListaviewcontroller(final FiltroListaViewController listaviewcontroller) {
		this.filtroLlistaviewcontroller = listaviewcontroller;
	}

	public ArrayList<Object> getListaOriginal() {
		return listaOriginal;
	}

	public void guardaCopiaOriginal(final ArrayList<Object> listaOriginal)
	{
		this.listaOriginal = listaOriginal;
	}
	
	@SuppressWarnings("unchecked")
	public void realizaBusqueda(String str)
	{
		if (!str.equals("")) {
			str = str.toLowerCase();
			nuevaLista = new ArrayList<Object>();
			for (int i = 0; i < listaOriginal.size(); i++) {
				final ArrayList<Object> registro = (ArrayList<Object>) listaOriginal.get(i);
				for (int j = 1; j < registro.size(); j++) {
					String reg = ((String)registro.get(j)).toLowerCase();
					reg = reg.replace(",", "");
					if (reg.contains(str)) {
						if (!nuevaLista.contains(registro)) {
							nuevaLista.add(registro);
						}
					}
				}
			}
		}
		else {
			nuevaLista = listaOriginal;
		}
	}
	
	public void actualizaCampos()
	{
		filtroLlistaviewcontroller.getListaSeleccion().setLista(nuevaLista);
		filtroLlistaviewcontroller.getListaSeleccion().cargarTabla();		
	}

}
