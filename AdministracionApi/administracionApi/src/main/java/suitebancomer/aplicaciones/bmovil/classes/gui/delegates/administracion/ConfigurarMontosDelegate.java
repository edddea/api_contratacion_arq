package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfigurarMontosViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.CambiarLimitesResult;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.ConfigurarMontos;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

import static suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.formatAmount;

public class ConfigurarMontosDelegate extends DelegateBaseAutenticacion{

	/**
	 * Delegate ID
	 */
	public static final long CONFIGURAR_MONTOS_DELEGATE_ID = 4278392383047444830L;
	
	private ConfigurarMontos configurarMontos;	
	private ConfigurarMontosViewController viewController;
	private String folio;
	
	public void setViewController(final ConfigurarMontosViewController viewController) {
		this.viewController = viewController;
	}

	public void setConfigurarMontos(final ConfigurarMontos configurarMontos) {
		this.configurarMontos = configurarMontos;
	}	
	public ConfigurarMontos getConfigurarMontos() {
		return configurarMontos;
	}
	
	public boolean validarDatos() {
		boolean validos = true;
		String msgError = "";
		final long montoDiario = Long.valueOf(configurarMontos.getMontoDiario()).longValue();
		final long montoMes = Long.valueOf(configurarMontos.getMontoMensual()).longValue();
		final long montoOp = Long.valueOf(configurarMontos.getMontoPorOperacion()).longValue();
		final long maxDiario = Long.valueOf(configurarMontos.getMontoMaxDiario()).longValue();
		final long maxMes = Long.valueOf(configurarMontos.getMontoMaxMensual()).longValue();
		final long maxOp = Long.valueOf(configurarMontos.getMontoMaxPorOperacion()).longValue();
		final ConfigurarMontosViewController me = viewController;
		final OnClickListener listener = new OnClickListener() {
			
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.dismiss();
				me.habilitaBtn();				
			}
		};
		
		if (montoOp == 0) {
			msgError = viewController.getString(R.string.bmovil_configurar_montos_error_opvacio);
			validos = false;
		} else if (montoDiario == 0) {
			msgError = viewController.getString(R.string.bmovil_configurar_montos_error_diariovacio);
			validos = false;
		} else if (montoMes == 0) {
			msgError = viewController.getString(R.string.bmovil_configurar_montos_error_mensualvacio);
			validos = false;
		} else if (montoOp > maxOp) {
			msgError = viewController.getString(R.string.bmovil_configurar_montos_error4);
			validos = false;
		} else if (montoDiario > maxDiario) {
			msgError = viewController.getString(R.string.bmovil_configurar_montos_error5);
			validos = false;
		} else if (montoMes > maxMes) {
			msgError = viewController.getString(R.string.bmovil_configurar_montos_error6);
			validos = false;
		} else if (montoOp > montoDiario) {
			msgError = viewController.getString(R.string.bmovil_configurar_montos_error7);
			validos = false;
		} else if (montoOp > montoMes) {
			msgError = viewController.getString(R.string.bmovil_configurar_montos_error8);
			validos = false;
		} else if (montoDiario > montoMes) {
			msgError = viewController.getString(R.string.bmovil_configurar_montos_error9);
			validos = false;
		}

		if (!validos) {
			viewController.showInformationAlert(msgError,listener);
		}
		return validos;
	}
	
	/**
	 * @return true si se debe mostrar contrasena, false en caso contrario.
	 */
	@Override
	public boolean mostrarContrasenia() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.cambioLimites,
				perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarCVV(Constants.Operacion.cambioLimites, perfil);
		return value;
	}
	
	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.cambioLimites,
				perfil);
		return value;
	}
	
	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.cambioLimites,
									perfil);
		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
			tipoOTP = null;
		}		
		return tipoOTP;
	}
	
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
		final String montoOp = formatAmount(configurarMontos.getMontoPorOperacion(),false);
		final String montoDiario = formatAmount(configurarMontos.getMontoDiario(),false);
		final String montoMes = formatAmount(configurarMontos.getMontoMensual(),false);

		final ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_configurar_montos_operacion));		
		fila.add(montoOp);
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_configurar_montos_diario));		
		fila.add(montoDiario);
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_configurar_montos_mensual));		
		fila.add(montoMes);
		tabla.add(fila);
		
		return tabla;
	}
	
	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		return getDatosTablaConfirmacion();
	}

	public void showConfirmacion() {
		final int resSubtitle = 0;
		final int resTitleColor = 0;
		final int resIcon = R.drawable.bmovil_configurarmontos_icono;
		final int resTitle = R.string.bmovil_configurar_montos_title;
		SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(this, resIcon, resTitle, resSubtitle, resTitleColor);
	}


	@Override
	public void realizaOperacion(final ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController,
								 final String contrasenia, final String nip, final String token, final String cvv, final String campoTarjeta) {

		final int operationId = Server.CAMBIAR_LIMITES;//OP Cambio de limites de op
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		final Autenticacion aut = Autenticacion.getInstance();
		final Perfil perfil = session.getClientProfile();
		final String cadAutenticacion =  aut.getCadenaAutenticacion(Constants.Operacion.cambioLimites, perfil);
		final Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());		
		params.put("limusuOp", configurarMontos.getMontoPorOperacion());
		params.put("limusuDiario", configurarMontos.getMontoDiario());
		params.put("limusuMensual", configurarMontos.getMontoMensual());
		params.put(ServerConstants.CVE_ACCESO, contrasenia == null ? "" : contrasenia);
		params.put("codigoNIP", nip == null ? "" : nip);
		params.put("codigoCVV2", cvv == null ? "" : cvv);
		params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
		params.put(Server.J_AUT, cadAutenticacion);	
		params.put("tarjeta5Dig", campoTarjeta == null ? "" : campoTarjeta);
		
		params.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
		final List<String> listaEncriptar = Arrays.asList(ServerConstants.CVE_ACCESO,
				"codigoNIP", "codigoCVV2");
		Encripcion.setContext(SuiteAppAdmonApi.appContext);
		Encripcion.encriptaCadenaAutenticacion(params, listaEncriptar);
		
		//JAIG
		doNetworkOperation(operationId, params,true, new CambiarLimitesResult(), confirmacionAutenticacionViewController);
	}
	
	@Override
	public void doNetworkOperation(final int operationId,	final Hashtable<String, ?> params,  final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
		((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler, caller);
	}
	
	
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
			if(response.getResponse() instanceof CambiarLimitesResult){
				final CambiarLimitesResult result = (CambiarLimitesResult) response.getResponse();
				folio = result.getFolio();
				if(Server.ALLOW_LOG) Log.d(getClass().getName(), folio);
				SuiteApp.appContext=SuiteAppAdmonApi.appContext;
				showResultados();
			}			
		}else if(response.getStatus() == ServerResponse.OPERATION_WARNING){
			final BaseViewControllerCommons current = ((BmovilViewsController)viewController.getParentViewsController()).getCurrentViewControllerApp();
			current.showInformationAlert(response.getMessageText());
		}
		
	}
	
	private void showResultados(){
		SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController()
		.showResultadosViewController(this, -1, -1);
	}

	@Override
	public int getTextoEncabezado() {
		//int resTitle = R.string.bmovil_configurar_montos_title;
		return R.string.bmovil_configurar_montos_title;
	}

	@Override
	public int getNombreImagenEncabezado() {
		//int resIcon = R.drawable.bmovil_configurarmontos_icono;
		return R.drawable.bmovil_configurarcorreo_icono;
	}

	@Override
	public String getTextoPantallaResultados() {
		//String text = viewController.getString(R.string.bmovil_configurar_montos_resultados);
		return viewController.getString(R.string.bmovil_configurar_montos_resultados);
	}

	@Override
	public String getTextoTituloResultado() {
		final int txtTitulo = R.string.transferir_detalle_operacion_exitosaTitle;
		return viewController.getString(txtTitulo);
	}

	@Override
	public int getColorTituloResultado() {
		return R.color.verde_limon;
	}
	
	@Override
	public boolean mostrarCampoTarjeta() {
		return (mostrarCVV() || mostrarNIP());
	}

}
