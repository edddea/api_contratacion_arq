package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;




public class ConfirmacionDelegate extends DelegateBaseOperacion
{
	public final static long CONFIRMACION_DELEGATE_DELEGATE_ID = 0x1ef4f4c61ca109bfL;


	private final DelegateBaseOperacion operationDelegate;
	private final boolean debePedirContrasena;
	private final boolean debePedirNip;
	private final Constants.TipoOtpAutenticacion tokenAMostrar;
	private final boolean debePedirCVV;
	
	
	private Constants.TipoInstrumento tipoInstrumentoSeguridad;
	//private String textoInstrumentoSeguridad;
	private ConfirmacionViewController confirmacionViewController;
	
	private final boolean debePedirTarjeta;
	//AMZ
		public boolean res = false;

	public ConfirmacionDelegate(final DelegateBaseOperacion delegateBaseOperacion) {
		this.operationDelegate = delegateBaseOperacion;
		debePedirContrasena = operationDelegate.mostrarContrasenia();
		debePedirNip = operationDelegate.mostrarNIP();
		debePedirCVV = operationDelegate.mostrarCVV();
		tokenAMostrar = operationDelegate.tokenAMostrar();
		debePedirTarjeta = mostrarCampoTarjeta();
		final String instrumento = Session.getInstance(SuiteAppAdmonApi.appContext).getSecurityInstrument();
		if (instrumento.equals(Constants.IS_TYPE_DP270)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
		} else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
		} else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
		} else {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
		}
	}

	public void setConfirmacionViewController(final ConfirmacionViewController confirmacionViewController) {
		this.confirmacionViewController = confirmacionViewController;
	}

	public void consultaDatosLista() {
		confirmacionViewController.setListaDatos(operationDelegate.getDatosTablaConfirmacion());
	}
	
	public DelegateBaseOperacion consultaOperationsDelegate() {
		return operationDelegate;
	}
	
	public boolean consultaDebePedirContrasena() {
		return debePedirContrasena;
	}
	
	public boolean consultaDebePedirNIP() {
		return debePedirNip;
	}
	
	public boolean consultaDebePedirCVV() {
		return debePedirCVV;
	}
	
	public Constants.TipoInstrumento consultaTipoInstrumentoSeguridad() {
		return tipoInstrumentoSeguridad;
	}
	
	public Constants.TipoOtpAutenticacion consultaInstrumentoSeguridad() {
		return tokenAMostrar;
	}

	public void enviaPeticionOperacion() {
		String contrasena = null;



		res = false;
		if (debePedirContrasena) {
			contrasena = confirmacionViewController.pideContrasena();
			if (Constants.EMPTY_STRING.equals(contrasena)) {
				final StringBuilder mensaje = new StringBuilder(confirmacionViewController.getString(R.string.confirmation_valorVacio));
				mensaje.append(" ");
				mensaje.append(confirmacionViewController.getString(R.string.confirmation_componenteContrasena));
				mensaje.append(".");
				confirmacionViewController.showInformationAlert(mensaje.toString());
				return;
			} else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
				final StringBuilder mensaje = new StringBuilder(confirmacionViewController.getString(R.string.confirmation_valorIncompleto1));
				mensaje.append(" ");
				mensaje.append(Constants.PASSWORD_LENGTH);
				mensaje.append(" ");
				mensaje.append(confirmacionViewController.getString(R.string.confirmation_valorIncompleto2));
				mensaje.append(" ");
				mensaje.append(confirmacionViewController.getString(R.string.confirmation_componenteContrasena));
				mensaje.append(".");
				confirmacionViewController.showInformationAlert(mensaje.toString());
				return;
			}
		}
		
		String tarjeta = null;
		if(debePedirTarjeta){
			tarjeta = confirmacionViewController.pideTarjeta();
			String mensaje = "";
			if(Constants.EMPTY_STRING.equals(tarjeta)){
				mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
				confirmacionViewController.showInformationAlert(mensaje);
				return;
			}else if(tarjeta.length() != 5){
				mensaje =  "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
				confirmacionViewController.showInformationAlert(mensaje);
				return;
			}			
		}
		
		if (debePedirNip) {
			final String nip = confirmacionViewController.pideNIP();
			if (Constants.EMPTY_STRING.equals(nip)) {
				final StringBuilder mensaje = new StringBuilder(confirmacionViewController.getString(R.string.confirmation_valorVacio));
				mensaje.append(" ");
				mensaje.append(confirmacionViewController.getString(R.string.confirmation_componenteNip));
				mensaje.append(".");
				confirmacionViewController.showInformationAlert(mensaje.toString());
				return;
			} else if (nip.length() != Constants.NIP_LENGTH) {
				final StringBuilder mensaje = new StringBuilder(confirmacionViewController.getString(R.string.confirmation_valorIncompleto1));
				mensaje.append(" ");
				mensaje.append(Constants.NIP_LENGTH);
				mensaje.append(" ");
				mensaje.append(confirmacionViewController.getString(R.string.confirmation_valorIncompleto2));
				mensaje.append(" ");
				mensaje.append(confirmacionViewController.getString(R.string.confirmation_componenteNip));
				mensaje.append(".");
				confirmacionViewController.showInformationAlert(mensaje.toString());
				return;
			}
		}
		if (tokenAMostrar != Constants.TipoOtpAutenticacion.ninguno) {
			final String asm = confirmacionViewController.pideASM();
			if (Constants.EMPTY_STRING.equals(asm)) {
				final StringBuilder mensaje = new StringBuilder(confirmacionViewController.getString(R.string.confirmation_valorVacio));
				mensaje.append(" ");
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje.append(getEtiquetaCampoOCRA());
						break;
					case DP270:
						mensaje.append(getEtiquetaCampoDP270());
						break;
					case SoftToken:
						if (SuiteAppAdmonApi.getSofttokenStatus()) {
							mensaje.append(getEtiquetaCampoSoftokenActivado());
						} else {
							mensaje.append(getEtiquetaCampoSoftokenDesactivado());
						}
						break;
					default:
						break;
				}
				mensaje.append(".");
				confirmacionViewController.showInformationAlert(mensaje.toString());
				return;
			} else if (asm.length() != Constants.ASM_LENGTH) {
				final StringBuilder mensaje = new StringBuilder(confirmacionViewController.getString(R.string.confirmation_valorIncompleto1));
				mensaje.append(" ");
				mensaje.append(Constants.ASM_LENGTH);
				mensaje.append(" ");
				mensaje.append(confirmacionViewController.getString(R.string.confirmation_valorIncompleto2));
				mensaje.append(" ");
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje.append(getEtiquetaCampoOCRA());
						break;
					case DP270:
						mensaje.append(getEtiquetaCampoDP270());
						break;
					case SoftToken:
						if (SuiteAppAdmonApi.getSofttokenStatus()) {
							mensaje.append(getEtiquetaCampoSoftokenActivado());
						} else {
							mensaje.append(getEtiquetaCampoSoftokenDesactivado());
						}
						break;
					default:
						break;
				}	
				mensaje.append(".");
				confirmacionViewController.showInformationAlert(mensaje.toString());
				return;
			}
		}
		if (debePedirCVV) {
			final String cvv = confirmacionViewController.pideCVV();
			if (Constants.EMPTY_STRING.equals(cvv)) {
				final StringBuilder mensaje = new StringBuilder(confirmacionViewController.getString(R.string.confirmation_valorVacio));
				mensaje.append(" ");
				mensaje.append(confirmacionViewController.getString(R.string.confirmation_componenteCvv));
				mensaje.append(".");
				confirmacionViewController.showInformationAlert(mensaje.toString());
				return;
			} else if (cvv.length() != Constants.CVV_LENGTH) {
				final StringBuilder mensaje = new StringBuilder(confirmacionViewController.getString(R.string.confirmation_valorIncompleto1));
				mensaje.append(" ");
				mensaje.append(Constants.CVV_LENGTH);
				mensaje.append(" ");
				mensaje.append(confirmacionViewController.getString(R.string.confirmation_valorIncompleto2));
				mensaje.append(" ");
				mensaje.append(confirmacionViewController.getString(R.string.confirmation_componenteCvv));
				mensaje.append(".");
				confirmacionViewController.showInformationAlert(mensaje.toString());
				return;
			}
		}
		
		String newToken = null;
		if(tokenAMostrar != TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == TipoInstrumento.SoftToken && SuiteAppAdmonApi.getSofttokenStatus())
			newToken = loadOtpFromSofttoken(tokenAMostrar);
		if(null != newToken) {
			String asm = newToken;
		}
		/*AYMB
		 * if ((operationDelegate instanceof AltaRetiroSinTarjetaDelegate) || (operationDelegate instanceof ConsultaRetiroSinTarjetaDelegate)) {
			operationDelegate.realizaOperacion(confirmacionViewController, contrasena, nip, asm, tarjeta, cvv);
		}else {
			operationDelegate.realizaOperacion(confirmacionViewController, contrasena, nip, asm, tarjeta);
		}*/
		res = true;

	}
	

	@Override
	public String getEtiquetaCampoNip() {
		return confirmacionViewController.getString(R.string.confirmation_nip);
	}
	
	@Override
	public String getTextoAyudaNIP() {
		return confirmacionViewController.getString(R.string.confirmation_ayudaNip);
	}
	
	@Override
	public String getEtiquetaCampoContrasenia() {
		return confirmacionViewController.getString(R.string.confirmation_contrasena);
	}
	
	@Override
	public String getEtiquetaCampoOCRA() {
		return confirmacionViewController.getString(R.string.confirmation_ocra);
	}
	
	@Override
	public String getEtiquetaCampoDP270() {
		return confirmacionViewController.getString(R.string.confirmation_dp270);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenActivado() {
		return confirmacionViewController.getString(R.string.confirmation_softtokenActivado);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenDesactivado() {
		return confirmacionViewController.getString(R.string.confirmation_softtokenDesactivado);
	}
	
	@Override
	public String getEtiquetaCampoCVV() {
		return confirmacionViewController.getString(R.string.confirmation_CVV);
	}
	
	@Override
	public String getTextoAyudaInstrumentoSeguridad(final Constants.TipoInstrumento tipoInstrumento) {
		if (tokenAMostrar == Constants.TipoOtpAutenticacion.ninguno) {
			return "";
		} else if (tokenAMostrar == Constants.TipoOtpAutenticacion.registro) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (SuiteAppAdmonApi.getSofttokenStatus()) {
						return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroSofttokenActivado);
					} else {
						return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroSofttokenDesactivado);
					}
				case OCRA:
					return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroOCRA);
				case DP270:
					return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroDP270);
				case sinInstrumento:
				default:
					return "";
			}
		} else if (tokenAMostrar == Constants.TipoOtpAutenticacion.codigo) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (SuiteAppAdmonApi.getSofttokenStatus()) {
						return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoSofttokenActivado);
					} else {
						return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoSofttokenDesactivado);
					}					
				case OCRA:
					return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoOCRA);
				case DP270:
					return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoDP270);
				case sinInstrumento:
				default:
					return "";
			}
		}
		return "";
	}
	
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		final Constants.Perfil perfil = session.getClientProfile();
	
		if(response.getStatus() == ServerResponse.OPERATION_ERROR && !Constants.Perfil.recortado.equals(perfil)){
			confirmacionViewController.limpiarCampos();
			((BmovilViewsController)confirmacionViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
		}
		operationDelegate.analyzeResponse(operationId, response);
	}
	
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		return tokenAMostrar;
	}
	
	@Override
	public boolean mostrarCampoTarjeta() {
		return operationDelegate.mostrarCampoTarjeta();
	}

	@Override
	public String loadOtpFromSofttoken(final TipoOtpAutenticacion tipoOTP) {
		return loadOtpFromSofttoken(tipoOTP, operationDelegate);
	}

	
}
