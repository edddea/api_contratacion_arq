/*
 * Copyright (c) 2009 BBVA. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * ConsultaECExtract wraps an account periods, the list of the last 12 periods
 * 
 * @author Carlos Santoyo
 */
public class ConsultaECResult  implements ParsingHandler {

    /**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 3272545845918202838L;

	   /**
	     * The pdf link
	     */
	    private static String pdf = null;
	    
	   
	    
  
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		
	}

	/**
	 * Metodo Proces
	 * @param parser
	 * @throws IOException
	 * @throws ParsingException
	 */
	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
	   setPdf(parser.parseNextValue("pdf"));
	  
	}

	public String getPdf() {
		return pdf;
	}

	public void setPdf(final String pdf) {
		ConsultaECResult.pdf = pdf;
	}

	
    
}
