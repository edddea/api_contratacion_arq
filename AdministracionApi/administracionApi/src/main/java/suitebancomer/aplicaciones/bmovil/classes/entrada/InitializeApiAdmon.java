package suitebancomer.aplicaciones.bmovil.classes.entrada;

import android.content.Context;

import com.bancomer.base.callback.CallBackSession;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import suitebancomer.aplicaciones.bmovil.classes.model.administracion.BanderasServer;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by jinclan on 14/08/15.
 */
public class InitializeApiAdmon {

    public static InitializeApiAdmon initAdmon;
    private Context context;
    boolean isActivityChanging;
    //Estas son las APIS que dependen
    private SuiteAppAdmonApi suiteAppAdmonApi;
    private SuiteAppApi suiteAppApiSoftoken;
    private CallBackSession callBackSession;

    private InitializeApiAdmon(final BanderasServer banderasServer) {
        ServerCommons.ALLOW_LOG = banderasServer.getLogActivo();
        ServerCommons.DEVELOPMENT = banderasServer.isDesarrollo();
        ServerCommons.EMULATOR = banderasServer.getEmulador();
        ServerCommons.SIMULATION = banderasServer.getSimulacion();
    }

    public static InitializeApiAdmon getInstance(final Context context, final BanderasServer bandera){
        if(initAdmon == null){
            initAdmon= new InitializeApiAdmon(bandera);
            initAdmon.setContext(context);
        }
        return  initAdmon;
    }

    public void setParameters(final boolean ALLOW_LOG,final boolean DEVELOPMENT,final boolean EMULATOR,final boolean SIMULATION){
        ServerCommons.ALLOW_LOG = ALLOW_LOG;
        ServerCommons.DEVELOPMENT =DEVELOPMENT;
        ServerCommons.EMULATOR = EMULATOR;
        ServerCommons.SIMULATION = SIMULATION;
       // initializeDependenceApis();
    }

    public void initializeDependenceApis(){
        //Levanta la Api de Administracion
        if(suiteAppAdmonApi ==null){
            suiteAppAdmonApi= new SuiteAppAdmonApi();
            suiteAppAdmonApi.onCreate(context);
        }

        //Levanta el Api de Softoken
        if(suiteAppApiSoftoken == null){
            suiteAppApiSoftoken= new SuiteAppApi();
            suiteAppApiSoftoken.onCreate(context);
        }
    }

    /*
    * Para poder utilizarlas y setearles valores si es necesario
    * */
    public SuiteAppAdmonApi getSuiteAppAdmonApi(){
        return suiteAppAdmonApi;
    }
    public  SuiteAppApi getSuiteAppApiSoftoken(){
        return  suiteAppApiSoftoken;
    }
    public  Context getContext(){
        return  context;
    }
    public  void setContext(final Context context){
       this.context=context;
    }

    public CallBackSession getCallBackSession() {
        return callBackSession;
    }

    public void setCallBackSession(final CallBackSession callBackSession) {
        this.callBackSession = callBackSession;
    }
}
