package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

public class InhibirEEC {
	private String operacion;
	private String contrasena;
	private String token;
	private String cvv2;
	private String nip;
	private String registro;
	private String operar;
	private String visible;
	
	public InhibirEEC(){
		
	}
	
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(final String operacion) {
		this.operacion = operacion;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(final String contrasena) {
		this.contrasena = contrasena;
	}
	public String getToken() {
		return token;
	}
	public void setToken(final String token) {
		this.token = token;
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(final String cvv2) {
		this.cvv2 = cvv2;
	}
	public String getNip() {
		return nip;
	}
	public void setNip(final String nip) {
		this.nip = nip;
	}
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(final String registro) {
		this.registro = registro;
	}
	public String getOperar() {
		return operar;
	}
	public void setOperar(final String operar) {
		this.operar = operar;
	}
	public String getVisible() {
		return visible;
	}
	public void setVisible(final String visible) {
		this.visible = visible;
	}
	
	
}
