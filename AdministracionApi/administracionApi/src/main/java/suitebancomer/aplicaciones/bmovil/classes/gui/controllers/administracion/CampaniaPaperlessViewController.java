package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.os.Bundle;
import android.widget.LinearLayout;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CampaniaPaperlessDelegate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;


public class CampaniaPaperlessViewController extends BaseViewController {
	
	LinearLayout contenedorPrincipal, aviso;
	CampaniaPaperlessDelegate delegate; 
	//AMZ
	private BmovilViewsController parentManager;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE,
				R.layout.layout_bmovil_confirmacion);
		SuiteApp.appContext = this;
		setTitle(R.string.confirmacion_paperless_title, R.drawable.paperless_icono);
		//AMZ
		parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("CampaniaPaperles",parentManager.estados);

		final SuiteAppAdmonApi suiteApp = (SuiteAppAdmonApi) getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate( (CampaniaPaperlessDelegate)parentViewsController.getBaseDelegateForKey(CampaniaPaperlessDelegate.CAMPANIA_PAPERLESS_DELEGATE_ID));
		delegate = (CampaniaPaperlessDelegate) getDelegate();
		delegate.setViewController(this);
		findViews();
		scaleForCurrentScreen();
		confirmarcampania();
	}
	
	/**
	 * 
	 */
	private void findViews() {
		contenedorPrincipal = (LinearLayout) findViewById(R.id.confirmacion_parent_parent_view);
		//aviso = (LinearLayout) findViewById(R.id.seeAviso);
		//aviso.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Escala la vistas en base a la pantalla donde se muestra
	 */
	private void scaleForCurrentScreen(){
		final GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		//gTools.scale(contenedorPrincipal);
		//gTools.scale(aviso);
	}
	
	
	@Override
	protected void onPause() {
		parentViewsController.consumeAccionesDePausa();
		super.onPause();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	public void onBackPressed() {		
	}
	
	/**
	 * Muestra la pantalla de confirmacion 
	 */
	private void confirmarcampania(){
		//AMZ
		//TrackingHelper.trackState("suspender", parentManager.estados);	
		delegate.realizaConfirmar();
	}
	
	
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}
	
}

