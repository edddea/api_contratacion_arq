package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import android.util.Log;

import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

@SuppressWarnings("serial")
public class ActualizarCuentasResult  implements ParsingHandler{

	private Account[] asuntos;
	
	/**
	 * 
	 * @return lista de Accounts del usuario
	 */
	public Account[] getAsuntos() {
		return asuntos;
	}
	
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		throw new ParsingException("Invalid process.");
	}
	
	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {

		final JSONArray arrayAsuntos = parser.parseNextValueWithArray("asuntos", false);
		final int numAsuntos = arrayAsuntos.length();
		asuntos = new Account[numAsuntos];
		for (int i = 0; i < numAsuntos; i++) {
			
			
			try {
				final JSONObject asuntoObj = arrayAsuntos.getJSONObject(i);
				final String tipoCuenta = asuntoObj.getString("tipoCuenta");
				final String alias = asuntoObj.getString("alias");
				final String divisa = asuntoObj.getString("divisa");
				final String asunto = asuntoObj.getString("asunto");
				final String saldo = asuntoObj.getString("saldo");


				final String concepto = asuntoObj.getString("concepto");
				final String visible = asuntoObj.getString("visible");
				final String celularAsociado = asuntoObj.getString("celularAsociado");
				final String codigoCompania = asuntoObj.getString("codigoCompania");
				final String descripcionCompania = asuntoObj.getString("descripcionCompania");
				final String fechaUltimaModificacion = asuntoObj.getString("fechaUltimaModificacion");
				final String indicadorSPEI = asuntoObj.getString("indicadorSPEI");
				
					//if(tipoCuenta =="LI" || tipoCuenta =="AH" || tipoCuenta =="CH"
						//|| tipoCuenta =="CE" || tipoCuenta =="TC" || tipoCuenta =="TP" ){
				final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
				final String date = Tools.dateForReference(session.getServerDate());
				if(Server.ALLOW_LOG) Log.e("value of this parser",String.valueOf(Double.valueOf(saldo)));
				
				asuntos[i] = new Account(asunto,
						Tools.getDoubleAmountFromServerString(saldo), 						
						Tools.formatDate(date), "S".equals(visible), divisa,
						tipoCuenta, concepto, alias, celularAsociado, 
						codigoCompania, descripcionCompania, fechaUltimaModificacion , indicadorSPEI);
				
				//System.out.println("array"+asuntos);

					//}
			} catch (JSONException e) {
				if(Server.ALLOW_LOG) throw new ParsingException("Error formato");
			}

		}
	}

}
