package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionAsignacionSpeiViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.SpeiTermsAndConditionsResult;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.ListaDatosViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;


public class ConfirmacionAsignacionSpeiDelegate extends	DelegateBaseAutenticacion {
	//#region Class fields.
	/**
	 * The unique identifier of this delegate.
	 */
	public static final long CONFIRMATION_SPEI_DELEGATE_ID = -5967914594319368287L;
	
	/**
	 * The delegate of the current operation.
	 */
	private DelegateBaseAutenticacion operationDelegate;
	
	/**
	 * The owner controller of this delegate.
	 */
	private BaseViewController ownerController;
	//#endregion

	//#region Setters and Getters
	/**
	 * @return The delegate of the current operation.
	 */
	public DelegateBaseAutenticacion getOperationDelegate() {
		return operationDelegate;
	}

	/**
	 * @param operationDelegate The delegate of the current operation.
	 */
	public void setOperationDelegate(final DelegateBaseAutenticacion operationDelegate) {
		this.operationDelegate = operationDelegate;
	}
	
	/**
	 * @return The owner controller of this delegate.
	 */
	public BaseViewController getOwnerController() {
		return ownerController;
	}
	
	/**
	 * @param ownerController The owner controller of this delegate.
	 */
	public void setOwnerController(final BaseViewController ownerController) {
		this.ownerController = ownerController;
	}
	//#endregion

	//#region Constructors.
	public ConfirmacionAsignacionSpeiDelegate() {
		operationDelegate = null;
	}
	
	public ConfirmacionAsignacionSpeiDelegate(final DelegateBaseAutenticacion operationDelegate) {
		this.operationDelegate = operationDelegate;
	}
	//#endregion

	//#region Configure screen.
	/**
	 * General configuration for the screen.
	 */
	public void configureScreen() {
		final ConfirmacionAsignacionSpeiViewController controller = (ConfirmacionAsignacionSpeiViewController)ownerController;
		
		configureCardElement();
		configureNipElement();
		configurePasswordElement();
		configureCvvElement();
		configureOtpElement();
		
		@SuppressWarnings("deprecation")
		final LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		final ArrayList<Object> data = operationDelegate.getDatosTablaConfirmacion();
		final ListaDatosViewController dataTable = new ListaDatosViewController(ownerController, params, ownerController.getParentViewsController());
		dataTable.setNumeroCeldas(2);
		dataTable.setLista(data);
		dataTable.setNumeroFilas(data.size());
		dataTable.setTitulo(R.string.confirmation_subtitulo);
		dataTable.showLista();
		
		controller.configureScreen(dataTable);
		controller.setTitle(operationDelegate.getTextoEncabezado(), operationDelegate.getNombreImagenEncabezado());
	}
	
	/**
	 * Obtains the data to configure the card element and send it to the controller.
	 */
	private void configureCardElement() {
		final ConfirmacionAsignacionSpeiViewController controller = (ConfirmacionAsignacionSpeiViewController)ownerController;
		final boolean visible = operationDelegate.mostrarCampoTarjeta();
		final String elementLabel = operationDelegate.getEtiquetaCampoTarjeta();
		final String elementInstructions = operationDelegate.getTextoAyudaTarjeta();
		
		controller.configureCardElement(visible, elementLabel, elementInstructions);
	}

	/**
	 * Obtains the data to configure the nip element and send it to the controller.
	 */
	private void configureNipElement() {
		final ConfirmacionAsignacionSpeiViewController controller = (ConfirmacionAsignacionSpeiViewController)ownerController;
		final boolean visible = operationDelegate.mostrarNIP();
		final String elementLabel = operationDelegate.getEtiquetaCampoNip();
		final String elementInstructions = operationDelegate.getTextoAyudaNIP();
		
		controller.configureNipElement(visible, elementLabel, elementInstructions);
	}
	
	/**
	 * Obtains the data to configure the password element and send it to the controller.
	 */
	private void configurePasswordElement() {
		final ConfirmacionAsignacionSpeiViewController controller = (ConfirmacionAsignacionSpeiViewController)ownerController;
		final boolean visible = operationDelegate.mostrarContrasenia();
		final String elementLabel = operationDelegate.getEtiquetaCampoContrasenia();
		final String elementInstructions = Constants.EMPTY_STRING;
		
		controller.configurePasswordElement(visible, elementLabel, elementInstructions);
	}
	
	/**
	 * Obtains the data to configure the cvv element and send it to the controller.
	 */
	private void configureCvvElement() {
		final ConfirmacionAsignacionSpeiViewController controller = (ConfirmacionAsignacionSpeiViewController)ownerController;
		final boolean visible = operationDelegate.mostrarCVV();
		final String elementLabel = operationDelegate.getEtiquetaCampoCVV();
		final String elementInstructions = operationDelegate.getTextoAyudaCVV();
		
		controller.configureCvvElement(visible, elementLabel, elementInstructions);
	}
	
	/**
	 * Obtains the data to configure the otp element and send it to the controller.
	 */
	private void configureOtpElement() {
		boolean visible;
		String code, elementLabel, elementInstructions;
		final TipoOtpAutenticacion otpType = operationDelegate.tokenAMostrar();
		final ConfirmacionAsignacionSpeiViewController controller = (ConfirmacionAsignacionSpeiViewController)ownerController;
		
		visible = otpType != TipoOtpAutenticacion.ninguno;
		final String instrument = Session.getInstance(SuiteAppAdmonApi.appContext).getSecurityInstrument();
		
		TipoInstrumento instrumentType = 
				instrument.equals(Constants.IS_TYPE_DP270) ? TipoInstrumento.DP270 : 
					(instrument.equals(Constants.IS_TYPE_OCRA) ? TipoInstrumento.OCRA : 
						(instrument.equals(Constants.TYPE_SOFTOKEN.S1.value) ? instrumentType = TipoInstrumento.SoftToken : 
							TipoInstrumento.sinInstrumento));
		
		elementLabel = ((MantenimientoSpeiMovilDelegate)operationDelegate).getEtiquetaCampoOTP(instrumentType);
		elementInstructions = operationDelegate.getTextoAyudaInstrumentoSeguridad(instrumentType);
		code = (instrumentType == TipoInstrumento.SoftToken && SuiteAppAdmonApi.getSofttokenStatus()) ? Constants.DUMMY_OTP : Constants.EMPTY_STRING;
		
		controller.configureOtpElement(visible, elementLabel, elementInstructions, code);
	}
	//#endregion

	//#region Network.
	@Override
	public void doNetworkOperation(final int operationId,	final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
		if (ownerController != null || operationId == Server.SPEI_TERMS_REQUEST) {
			//SuiteAppAdmonApi.getInstance().getBmovilApplication().invokeNetworkOperation(operationId, params,isJson,handler, caller, true);
			((BmovilViewsController) SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController()).getBmovilApp()
					.invokeNetworkOperation(operationId, params, isJson, handler, caller);
		}
	}

	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			if(operationId == Server.SPEI_TERMS_REQUEST){
				analyzeSpeiTermsAndConditions(response);
			} else {
				operationDelegate.analyzeResponse(operationId, response);
			}
		} else if(response.getStatus() == ServerResponse.OPERATION_WARNING) {
			if(Server.ALLOW_LOG){Log.d("ConfirAsignSpeiDel",">>>analyzeResponse:showInformationAlert:Warrning: " + response.getMessageText());}
			if(ownerController != null)
				ownerController.showInformationAlert(response.getMessageText());
		}else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
			if(Server.ALLOW_LOG){Log.d("ConfirAsignSpeiDel",">>>analyzeResponse:showInformationAlert:Error: " + response.getMessageText());}
			if(ownerController != null)
				ownerController.showInformationAlert(response.getMessageText());
		}
	}
	//#endregion
	
	//#region Terms and conditions.
	/**
	 * Sends the request for the terms and conditions of the SPEI operations.
	 */
	public void requestSpeiTermsAndConditions() {
		MantenimientoSpeiMovilDelegate speiDelegate = null;
		
		try {
			speiDelegate = (MantenimientoSpeiMovilDelegate)operationDelegate;
		} catch(Exception ex) {
			speiDelegate = null;
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while casting the operation delegate to \"MantenimientoSpeiMovilDelegate\".", ex);
			return;
		}

		final Hashtable<String, String> params = new Hashtable<String, String>();
		params.put("idProducto", speiDelegate.getSpeiModel().getTipoOperacion().getOperationCodeForServer());
		//JAIG
		doNetworkOperation(Server.SPEI_TERMS_REQUEST, params,true, new SpeiTermsAndConditionsResult(), ownerController);
	}
	
	/**
	 * @param response Process the terms and conditions server response. 
	 */
	private void analyzeSpeiTermsAndConditions(final ServerResponse response) {
		final SpeiTermsAndConditionsResult data = (SpeiTermsAndConditionsResult)response.getResponse();

		final String termsHtml = data.getTerms();
		//termsHtml = termsHtml.replace("charset=windows-1252", "charset=utf-8");
		//termsHtml = termsHtml.replace("�", "");

		final int counterStep = 1000;
		for(int counter = 1; counter < 100; counter++) {
			if(termsHtml.length() > (counter * counterStep)) {
				if(Server.ALLOW_LOG) Log.d("TerminosYCondiciones", "Indice: " + counter + "\n" + termsHtml.substring((counter - 1) * counterStep, counter * counterStep));
			} else {
				if(Server.ALLOW_LOG) Log.d("TerminosYCondiciones", "Indice: " + counter + "\n" + termsHtml.substring((counter - 1) * counterStep));
				break;
			}
		}
		
		
		
		//termsHtml = "<html> <head> <meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\"> <meta name=Generator content=\"Microsoft Word 12 (filtered)\"> <title>64 (Primera Secci�n) DIARIO OFICIAL Viernes 6 de diciembre de 2013</title> <style> <]--/* Font Definitions */ @font-face {font-family:\"Cambria Math\"; panose-1:2 4 5 3 5 4 6 3 2 4;} @font-face {font-family:Calibri; panose-1:2 155 2 2 2 4 3 2 4;} @font-face {font-family:Tahoma; panose-1:2 11 6 4 3 5 4 4 2 4;} /* Style Definitions */ p.MsoNormal, li.MsoNormal, div.MsoNormal {margin-top:0cm; margin-right:0cm; margin-bottom:10.0pt; margin-left:0cm; line-height:115%; font-size:11.0pt; font-family:\"Calibri\",\"sans-serif\";} p.MsoHeader, li.MsoHeader, div.MsoHeader {mso-style-link:\"Encabezado Car\"; margin:0cm; margin-bottom:.0001pt; font-size:11.0pt; font-family:\"Calibri\",\"sans-serif\";} p.MsoFooter, li.MsoFooter, div.MsoFooter {mso-style-link:\"Pie de página Car\"; margin:0cm; margin-bottom:.0001pt; font-size:11.0pt; font-family:\"Calibri\",\"sans-serif\";} p.MsoAcetate, li.MsoAcetate, div.MsoAcetate {mso-style-link:\"Texto de globo Car\"; margin:0cm; margin-bottom:.0001pt; font-size:8.0pt; font-family:\"Tahoma\",\"sans-serif\";}span.EncabezadoCar {mso-style-name:\"Encabezado Car\"; mso-style-link:Encabezado;} span.PiedepginaCar {mso-style-name:\"Pie de página Car\"; mso-style-link:\"Pie de página\";} span.TextodegloboCar {mso-style-name:\"Texto de globo Car\"; mso-style-link:\"Texto de globo\"; font-family:\"Tahoma\",\"sans-serif\";} .MsoPapDefault {margin-bottom:10.0pt; line-height:115%;} /* Page Definitions */ @page WordSection1 {size:595.3pt 841.9pt; margin:70.85pt 3.0cm 70.85pt 3.0cm;} div.WordSection1 {page:WordSection1;} --> </style> </head> <body lang=ES-MX> <div class=WordSection1> <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 style='border-collapse:collapse;border:none'> <tr> <td width=576 valign=top style='width:432.2pt;border:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt'> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent: 92.15pt;line-height:normal'><b><span style='font-size:9.0pt'> </span></b></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent: 92.15pt;line-height:normal'><b><span style='font-size:9.0pt'>Formato de solicitud para desasociar un n&uacute;mero de teléfono m�vil</span></b></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent: 155.95pt;line-height:normal'><b><span style='font-size:9.0pt'>a una cuenta de depósito a la vista.</span></b></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><span style='font-size:9.0pt'> </span></b></p><p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><span style='font-size:9.0pt'> </span></b></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent: 304.8pt;line-height:normal'><span style='font-size:9.0pt'> ___de _________de 20__. </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align: justify;text-justify:inter-ideograph;line-height:normal'><span style='font-size:9.0pt'> </span></p> <p class=MsoNormalstyle='margin-bottom:0cm;margin-bottom:.0001pt;text-align: justify;text-justify:inter-ideograph;line-height:normal'><span style='font-size:9.0pt'> </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><span style='font-size:10.0pt'>BBVA BANCOMER, S.A.</span></b></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><b><span style='font-size:10.0pt'>INSTITUCI�N DE BANCA M�LTIPLE</span></b></p> <p class=MsoNormalstyle='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><span style='font-size:10.0pt'>GRUPO FINANCIERO BBVA BANCOMER</span></b></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><span style='font-size:9.0pt'> </span></b></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><span style='font-size:9.0pt'> </span></b></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent: 35.4pt;line-height:normal'><span style='font-size:9.0pt'> </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent: 35.4pt;line-height:normal'><span style='font-size:9.0pt'> </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align:justify;text-justify:inter-ideograph;text-indent:35.45pt;line-height:normal'><span style='font-size:9.0pt'>Solicito a ese Banco que desasocie la línea de telefon�a m�vil con número <u>__________</u> (últimos diez dígitos) a la cuenta número <u>___________</u>, para que deje de recibir transferencias electr�nicas de fondos que indiquen dicho número de telefon�a m�vil.</span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent: 35.4pt;line-height:normal'><span style='font-size:9.0pt'> </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-align: justify;text-justify:inter-ideograph;text-indent:35.4pt;line-height:normal'><span style='font-size:9.0pt'>Estoy enterado de que la desasociaci�n del númerode telefon�a m�vil que solicito es sin costo a mi cargo y que surtir� efectos en un plazo no mayor a un d�a h�bil bancario contado a partir de la fecha de presentaci�n de �sta solicitud. Asimismo, reconozco que a partir del d�a en que surta efectos la desasociaci�n que solicito, todas las transferencias electr�nicas de fondos que indiquen los dígitos de línea de telefon�a m�vil referidos dejar�n de acreditarse a dicha cuenta. </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent: 35.4pt;line-height:normal'><span style='font-size:9.0pt'> </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent: 35.4pt;line-height:normal'><span style='font-size:9.0pt'> </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:normal'><span style='font-size:9.0pt'> </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><span style='font-size:9.0pt'> </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent: 163.05pt;line-height:normal'><span style='font-size:9.0pt'>Atentamente,</span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent: 35.4pt;line-height:normal'><span style='font-size:9.0pt'> </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><span style='font-size:9.0pt'> </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><span style='font-size:9.0pt'> </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent:2.0cm;line-height:normal'><span style='font-size:9.0pt'> __________________________________________________________________</span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><span style='font-size:9.0pt'> </span></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;text-indent: 70.9pt;line-height:normal'><b><span style='font-size:9.0pt'> (NOMBRE, DENOMINACION O RAZON SOCIAL DEL TITULAR DE LA CUENTA)</span></b></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><span style='font-size:9.0pt'> </span></b></p> <p class=MsoNormalstyle='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><span style='font-size:9.0pt'> </span></b></p> </td> </tr> </table> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><span style='font-size:9.0pt'> </span></b></p><p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><span style='font-size:9.0pt'> </span></b></p> <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height: normal'><b><span style='font-size:9.0pt'> </span></b></p> </div> </body> </html>";
		//termsHtml = "<html><head></head><body><div style=\"width:90%; font-family: Arial, Helvetica, sans-serif; font-size:9pt;\"><div style=\"text-align:center;\"><b>Formato de solicitud para desasociar un número de teléfono m�vil</b><br/><b>a una cuenta de depósito a la vista.</b><br/></div><div  style=\"height:30px; clear:both;\"></div><div  style=\"text-align:right;\"><u>___</u> de <u>_________</u> de 20<u>__.</u></div><div  style=\"height:30px; clear:both;\"></div><div style=\"font-size:9pt;\"><b>BBVA BANCOMER, S.A.</b><br/><b>INSTITUCI�N DE BANCA M�LTIPLE</b><br/><b>GRUPO FINANCIERO BBVA BANCOMER</b></div><div  style=\"height:30px; clear:both;\"></div><div style=\"text-align:justify; text-indent:45px\">Solicito a ese banco que desasocie la línea de telefon�a m�vil con número __________ (últimos diez dígitos) a la cuenta número ___________, para que deje de recibir transferencias electr�nicas de fondos que indiquen dicho número de telefon�a m�vil.</div><div  style=\"height:30px; clear:both;\"></div><div style=\"text-align:justify; text-indent:45px\">Estoy enterado de que la desasociaci�n del número de telefon�a m�vil que solicito es sin costo a mi cargo y que surtir� efectos en un plazo no mayor a un d�a h�bil bancario contado a partir de la fecha de presentaci�n de �sta solicitud. Asimismo, reconozco que a partir del d�a en que surta efectos la desasociaci�n que solicito, todas las transferencias electr�nicas de fondos que indiquen los dígitos de línea de telefon�a m�vil referidos dejar�n de acreditarse a dicha cuenta.</div><div  style=\"height:50px; clear:both;\"></div><div style=\"text-align:center;\">Atentamente</div><div  style=\"height:50px; clear:both;\"></div><div style=\"text-align:center;\">__________________________________</div><div  style=\"height:5px; clear:both;\"></div><div style=\"text-align:center;\"><b>(NOMBRE, DENOMINACION O RAZON SOCIAL DEL TITULAR DE LA CUENTA)</b></div></body></html>";
		final int title = R.string.bmovil_asociar_cuenta_telefono_title;
		final int icon = R.drawable.bmovil_consultar_icono;
		
		SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().showTermsAndConditions(termsHtml, title, icon);
	}
	//#endregion
	
	/**
	 * Validate the user inputs.
	 * @param termsAcepted The flag to indicate if the terms was accepted.
	 * @param card The user's card last digits input.
	 * @param nip The user's NIP input.
	 * @param pwd The user's password input.
	 * @param cvv The user's CVV input.
	 * @param otp The user's OTP input.
	 */
	public void confirm(final boolean termsAcepted, final String card, final String nip, final String pwd, final String cvv, String otp) {
		String errorMessage = null;
		
		if(operationDelegate.tokenAMostrar() != TipoOtpAutenticacion.ninguno) {
			final String fieldName = ((ConfirmacionAsignacionSpeiViewController)ownerController).getOtpLabelText();
			if(Tools.isEmptyOrNull(otp)) {
				errorMessage = String.format(ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_missing_otp), fieldName);
			} else if(Constants.ASM_LENGTH != otp.length()) {
				errorMessage = String.format(ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_length_otp), fieldName);
			}
		}
		
		if(operationDelegate.mostrarCVV()) {
			if(Tools.isEmptyOrNull(cvv))
				errorMessage = ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_missing_cvv);
			else if(Constants.CVV_LENGTH != cvv.length())
				errorMessage = ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_length_cvv);
		}
		
		if(operationDelegate.mostrarContrasenia()) {
			if(Tools.isEmptyOrNull(pwd))
				errorMessage = ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_missing_password);
			else if(Constants.PASSWORD_LENGTH != pwd.length())
				errorMessage = ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_length_password);
		}
		
		if(operationDelegate.mostrarContrasenia()) {
			if(Tools.isEmptyOrNull(pwd))
				errorMessage = ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_missing_password);
			else if(Constants.PASSWORD_LENGTH != pwd.length())
				errorMessage = ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_length_password);
		}
		
		if(operationDelegate.mostrarNIP()) {
			if(Tools.isEmptyOrNull(nip))
				errorMessage = ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_missing_nip);
			else if(Constants.NIP_LENGTH != nip.length())
				errorMessage = ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_length_nip);	
		}
		
		if(operationDelegate.mostrarCampoTarjeta()) {
			if(Tools.isEmptyOrNull(card))
				errorMessage = ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_missing_card);
			else if(Constants.CARD_LAST_DIGITS_LENGTH != card.length())
				errorMessage = ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_length_card);
		}
		
		if(!termsAcepted)
			errorMessage = ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_terms);
		
		// If the message value is greater than 0 then an error message was set and must be showed. 
		if(errorMessage != null) {
			ownerController.showInformationAlert(errorMessage);
		} else {
			// If needed loads the OTP from the Softtoken application.
			final TipoOtpAutenticacion otpType = operationDelegate.tokenAMostrar();
			final String instrument = Session.getInstance(SuiteAppAdmonApi.appContext).getSecurityInstrument();
			String newToken = null;
			if(otpType != TipoOtpAutenticacion.ninguno && instrument.equals(Constants.TYPE_SOFTOKEN.S1.value) && SuiteAppAdmonApi.getSofttokenStatus())
				newToken = loadOtpFromSofttoken(otpType);
			if(null != newToken)
				otp = newToken;
			
			((MantenimientoSpeiMovilDelegate)operationDelegate).realizeOperation(ownerController, pwd, nip, otp, cvv, card);
		}
	}
}
