package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class NombreCIEData implements ParsingHandler {

	private String nombreCIE;
	
	public String getNombreCIE() {
		return nombreCIE;
	}
	
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		nombreCIE = parser.parseNextValue("DE");
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

}
