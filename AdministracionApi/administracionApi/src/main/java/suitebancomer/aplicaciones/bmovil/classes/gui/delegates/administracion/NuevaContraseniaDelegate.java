/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.util.Log;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.NuevaContraseniaViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.Desbloqueo;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.DesbloqueoResult;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

import static suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.validatePasswordPolicy1;
import static suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.validatePasswordPolicy2;

/**
 * @author Francisco.Garcia
 *
 */
public class NuevaContraseniaDelegate extends DelegateBaseAutenticacion {

	public static final long NUEVA_CONTRASENIA_DELEGATE_ID = 4943309525631958195L;
	private NuevaContraseniaViewController viewController;
	private ConsultaEstatus consultaEstatus;


//	private String folio;
	private Desbloqueo desbloqueo;
	public Desbloqueo getDesbloqueo() {
		if(desbloqueo == null)
			desbloqueo = new Desbloqueo();
		return desbloqueo;
	}
	
	public ConsultaEstatus getConsultaEstatus() {
		return consultaEstatus;
	}
	
	public void setViewController(final NuevaContraseniaViewController viewController) {
		this.viewController = viewController;
	}
	
	public NuevaContraseniaDelegate(final ConsultaEstatus ce) {
		consultaEstatus = ce;
	}
	
	 
	/**
	 * @return true si se debe mostrar contrasena, false en caso contrario.
	 */
	@Override
	public boolean mostrarContrasenia() {
		final Perfil perfil = consultaEstatus.getPerfil();
		final boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.desbloqueo,
				perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		final Perfil perfil = consultaEstatus.getPerfil();
		final boolean value =  Autenticacion.getInstance().mostrarCVV(Constants.Operacion.desbloqueo, perfil);
		return value;
	}
	
	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		final Perfil perfil = consultaEstatus.getPerfil();
		final boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.desbloqueo,
				perfil);
		return value;
	}
	
	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		final Perfil perfil = consultaEstatus.getPerfil();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.desbloqueo,
									perfil);
		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
			tipoOTP = null;
		}		
		return tipoOTP;
	}
	
	
	
	
	public boolean validaDatos(final String pass, final String cPass){
		boolean esOk = true;
		int msgError = 0;
		if(pass.length() == 0){
			esOk = false;
			msgError = R.string.bmovil_desbloqueo_error_contra_vacio;
		}else if(pass.length() != Constants.PASSWORD_LENGTH){
			esOk = false;
			msgError =  R.string.error_passwordTooShort;
		}else if(!validatePasswordPolicy1(pass)){
			esOk = false;
			msgError =  R.string.bmovil_desbloqueo_error_policy1;
		}else if(!validatePasswordPolicy2(pass)){
			esOk = false;
			msgError =  R.string.bmovil_desbloqueo_error_policy2;
		}else if(cPass.length() == 0){
			esOk = false;
			msgError =  R.string.bmovil_desbloqueo_error_confir_vacio;
		}else if(cPass.length() != Constants.PASSWORD_LENGTH){
			esOk = false;
			msgError =  R.string.bmovil_desbloqueo_error_confir_corta;
		}else if(!pass.equals(cPass)){
			esOk = false;
			msgError =  R.string.bmovil_desbloqueo_error_diferentes;
		}
		if(!esOk){
			viewController.showInformationAlert(msgError);
		}		
		return esOk;
	}
	
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
		final ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
	
		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_cambio_telefono_telefonoLabel));
		fila.add(desbloqueo.getNumCelular());
		tabla.add(fila);	
				
		return tabla;
	}

	/**
	 * Muestra la pantalla de confirmacion.
	 */
	public void showConfirmacion() {
		final int resSubtitle = 0;
		final int resTitleColor = 0;
		final int resIcon = 0;
		final int resTitle = 0;
		SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(this, resIcon, resTitle, resSubtitle, resTitleColor);
	}
	
	@Override
	public void realizaOperacion(final ConfirmacionAutenticacionViewController confirmacionAut,
								 final String contrasenia, final String nip, final String token, final String cvv, final String campoTarjeta) {
		final int operationId = Server.DESBLOQUEO;
		final Autenticacion aut = Autenticacion.getInstance();
		final String cadAutenticacion = aut.getCadenaAutenticacion(Constants.Operacion.desbloqueo, consultaEstatus.getPerfil());

		final Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.NUMERO_TELEFONO, consultaEstatus.getNumCelular());
		params.put("companiaCelular", consultaEstatus.getCompaniaCelular());
		params.put(ServerConstants.NUMERO_CLIENTE, consultaEstatus.getNumCliente());
		params.put("cveAccesoNueva", desbloqueo.getContrasenaNueva());
		
		params.put(Server.J_NIP, nip == null ? "" : nip);
		params.put(Server.J_CVV2, cvv == null ? "" : cvv);
		params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
		params.put(Server.J_AUT, cadAutenticacion);
		params.put("tarjeta5Dig", campoTarjeta == null ? "" : campoTarjeta);
		//JAIG
		doNetworkOperation(operationId, params, true,new DesbloqueoResult(), confirmacionAut);
	}
	
	@Override
	public void doNetworkOperation(final int operationId,	final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
		((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler, caller);
	}
	
	
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {

		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			
			if (response.getResponse() instanceof DesbloqueoResult) {
//				DesbloqueoResult result = (DesbloqueoResult) response.getResponse();
//				folio = result.getFolio();
				showPantallaPrincipal();
			}
			
		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
			final BaseViewControllerCommons current = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp();
			current.showInformationAlert(response.getMessageText());
		}

	}

	@Override
	public int getTextoEncabezado() {
		//int resTitle = R.string.bmovil_desbloqueo_title;
		return R.string.bmovil_desbloqueo_title;
	}

	@Override
	public int getNombreImagenEncabezado() {
		//int resIcon = R.drawable.bmovil_desbloqueo_icono;
		return R.drawable.bmovil_desbloqueo_icono;
	}

	private void showPantallaPrincipal(){
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), "mostrarPantallaPrincipal");
		SuiteAppAdmonApi.getInstance().getBmovilApplication().reiniciaAplicacion();		
		SuiteAppAdmonApi.getInstance().getSuiteViewsController().showMenuSuite(true);
	}

	@Override
	public boolean mostrarCampoTarjeta() {
		return (mostrarCVV() || mostrarNIP());
	}
	
}
