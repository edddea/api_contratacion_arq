package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.util.Log;

import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.TextoPaperless;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.delegates.administracion.BaseDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.CambioPerfilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Promociones;

public class PopUpPaperlessDelegate extends BaseDelegate {
	//AMZ
			public boolean res = false;
		// #region Variables.
		/**
		 * Identificador �nico del delegado.
		 */
		public static long TEXTO_PAPERLESS_ID = 35454523421432455L;
		
		
		/**
		 * Controlador asociado al delegado.
		 */
		private BaseViewController ownerController;
		
		private TextoPaperless textoPaperless;
		
		private String claveCampana; /*** CLAVE COMPLETA ***/
		private Promociones promoPaperless;
		
		// #endregion
		
		// #region Getters y Setters.
		/**
		 * @return Controlador asociado al delegado.
		 */
		public BaseViewController getOwnerController() {
			return ownerController;
		}

		public TextoPaperless getTextopaperless(){
			return textoPaperless;
		}
		
		public void setTextoPaperless(final TextoPaperless textoPaperless){
			this.textoPaperless = textoPaperless;
		}
		/**
		 * @param ownerController Controlador asociado al delegado.
		 */
		public void setOwnerController(final BaseViewController ownerController) {
			this.ownerController = ownerController;
		}

		@Override
		public long getDelegateIdentifier() {
			return TEXTO_PAPERLESS_ID;
		}

		// #endregion

		public PopUpPaperlessDelegate(final TextoPaperless textoPaperless, final String claveCampanaPaperless) {
			this.textoPaperless = textoPaperless;
			claveCampana = claveCampanaPaperless;
		}
		
		public void realizaOperacionRechazo(final BaseViewController baseViewController) {
			final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
			final Autenticacion aut = Autenticacion.getInstance();
			final Perfil perfil = session.getClientProfile();

			final String cadAutenticacion =  aut.getCadenaAutenticacion(Constants.Operacion.inhibirEnvioEstadoCuenta, perfil);
			final String companiaCelular = session.getCompaniaUsuario();


			final int operationId = Server.INHIBIR_ENVIO_EC;
			final Hashtable<String, String> params = new Hashtable<String, String>();
			
			//params.put(ServerConstants.ID_PRODUCTO, Constants.ID_PRODUCTO);
			//params.put(ServerConstants.CLAVE_CONTRATACION, Constants.CLAVE_CONTRATACION);
			params.put(ServerConstants.CLAVE_RESPUESTA, Constants.CLAVE_RESPUESTA_RECHAZO);
			
			String cveCamp = "";
			final Promociones[] promociones=Session.getInstance(ownerController).getPromociones();
			for(final Promociones p : promociones){
				final String cve=p.getCveCamp();
				if(cve.substring(0, 4).equals("0429")){
					promoPaperless = p;
					cveCamp = cve;
				}
			}
			//params.put(ServerConstants.ID_CAMPAÑA, Constants.ID_CAMPAÑA);
			//params.put(ServerConstants.ID_CAMPAÑA, claveCampana);
			//Log.d("CLAVE ")
			params.put(ServerConstants.ID_CAMPAÑA, cveCamp);
			params.put(ServerConstants.EMAIL,  session.getEmail());
			//params.put(ServerConstants.CAUSA_INHIBICION, Constants.CAUSA_INHIBICION);
			params.put(ServerConstants.NUMERO_CELULAR, session.getUsername() );
			params.put(ServerConstants.IUM, session.getIum());
			/*
			params.put(ServerConstants.CVE_ACCESO, contrasenia == null ? "" : contrasenia);			
			params.put(Server.J_NIP, nip == null ? "" : nip);
			params.put(Server.J_CVV2, cvv== null ? "" : cvv);
			params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
			params.put(Server.J_AUT, cadAutenticacion);	
			params.put("tarjeta5Dig", campoTarjeta == null ? "" : campoTarjeta);*/
			
			doNetworkOperation(operationId, params,true,null, baseViewController);
		}
		public void rechazoPaperless(){
			realizaOperacionRechazo(ownerController);
		}
		
		// #region Network.
		/* (non-Javadoc)
		 * @see suitebancomer.classes.gui.delegates.BaseDelegate#doNetworkOperation(int, java.util.Hashtable, suitebancomer.classes.gui.controllers.BaseViewController)
		 */
		@Override
		public void doNetworkOperation(final int operationId, final Hashtable<String,?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
			SuiteAppAdmonApi.getInstance().getBmovilApplication().invokeNetworkOperation(operationId, params,isJson,handler, caller);
		}

		/* (non-Javadoc)
		 * @see suitebancomer.classes.gui.delegates.BaseDelegate#analyzeResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
		 */
		@Override
		public void analyzeResponse(final int operationId, final ServerResponse response) {
			
			if(operationId==Server.INHIBIR_ENVIO_EC){
				if (Server.ALLOW_LOG) Log.d("Rechazo Paperless", "Exito");
				ownerController.finish();
				removePaperlessCampaign();
			}
		}
		// #endregion
		
		private void removePaperlessCampaign() {
			Promociones[] nuevoPromo = new Promociones[0];
			final Promociones[] promociones=Session.getInstance(ownerController).getPromociones();
			
			for(final Promociones p : promociones){
				if (Server.ALLOW_LOG) Log.d("PROMOS ANTERIORES", p.getCveCamp().substring(0, 4));
			}

			final List<Promociones> list = new ArrayList<Promociones>(Arrays.asList(promociones));
			list.remove(promoPaperless);
			nuevoPromo = list.toArray(nuevoPromo);
			for(final Promociones p : nuevoPromo){
				if (Server.ALLOW_LOG) Log.d("PROMOS NUEVAS", p.getCveCamp().substring(0, 4));
			}
			
			Session.getInstance(ownerController).setPromocion(nuevoPromo);
			
		}

		public void guardaNoAceptacion() {
			final CambioPerfilFileManager manager = CambioPerfilFileManager.getCurrent();
			manager.setNoAceptacionCampanaPaperless(Constants.CAMPAÑA_PAPERLESS_NO_ACEPTADA);
		}
		
		// #region Confirmacion.
}
