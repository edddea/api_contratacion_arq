package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.PasswordTransformationMethod;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConfirmacionAsignacionSpeiDelegate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.ListaDatosViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class ConfirmacionAsignacionSpeiViewController extends BaseViewController {
	//#region Class fields.
	/**
	 * The layout to add the ListaDatos component.
	 */
	private LinearLayout dataTableLayout;
	
	/**
	 * The terms and conditions label.
	 */
	private TextView termsLink;
	
	/**
	 * The terms check box to verify if the user accepted the terms of use.
	 */
	private CheckBox termsCheckBox;
	
	/**
	 * Card element layout.
	 */
	private View cardElementLayout;
	/**
	 * Card element title label.
	 */
	private TextView cardElementLabel;
	/**
	 * Card element text field.
	 */
	private EditText cardElementEdittext;
	/**
	 * Card element instructions label.
	 */
	private TextView cardElementInstructionsLabel;
	
	/**
	 * Nip element layout.
	 */
	private View nipElementLayout;
	/**
	 * Nip element title label.
	 */
	private TextView nipElementLabel;
	/**
	 * Nip element text field.
	 */
	private EditText nipElementEdittext;
	/**
	 * Nip element instructions label.
	 */
	private TextView nipElementInstructionsLabel;
	
	/**
	 * Password element layout.
	 */
	private View pwdElementLayout;
	/**
	 * Password element title label.
	 */
	private TextView pwdElementLabel;
	/**
	 * Password element text field.
	 */
	private EditText pwdElementEdittext;
	/**
	 * Password element instructions label.
	 */
	private TextView pwdElementInstructionsLabel;
	
	/**
	 * Cvv element layout.
	 */
	private View cvvElementLayout;
	/**
	 * Cvv element title label.
	 */
	private TextView cvvElementLabel;
	/**
	 * Cvv element text field.
	 */
	private EditText cvvElementEdittext;
	/**
	 * Cvv element instructions label.
	 */
	private TextView cvvElementInstructionsLabel;
	
	/**
	 * Otp element layout.
	 */
	private View otpElementLayout;
	/**
	 * Otp element title label.
	 */
	private TextView otpElementLabel;
	/**
	 * Otp element text field.
	 */
	private EditText otpElementEdittext;
	/**
	 * Otp element instructions label.
	 */
	private TextView otpElementInstructionsLabel;
	
	private View helpImage;
	
	/**
	 * The delegate for this screen.
	 */
	private ConfirmacionAsignacionSpeiDelegate ownDelegate;
	//#endregion
	
	//#region Getters and Setters
	public String getOtpLabelText() {
		return otpElementLabel.getText().toString();
	}
	//Etiquetado Ale
		//AMZ Ale
		private BmovilViewsController parentManager;
		//AMZ Ale
		Map<String,Object> OperacionRealizadaMap = new HashMap<String, Object>();
		//AMZ Ale
	//#endregion
	
	//#region Life-cycle methods.
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppAdmonApi.getResourceId("layout_bmovil_confirmacion_asignacion_spei_admon", "layout"));
		SuiteApp.appContext = this;
		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate((ConfirmacionAsignacionSpeiDelegate)getParentViewsController().getBaseDelegateForKey(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID));
		ownDelegate = (ConfirmacionAsignacionSpeiDelegate)getDelegate();
	
		findViews();
		scaleToCurrentScreen();

		//AMZ
		TrackingHelper.trackState("confirma",parentManager.estados);

		final boolean showHelpImage = (Boolean)getIntent().getExtras().get("showHelpImage");
		helpImage.setVisibility(showHelpImage ? View.VISIBLE : View.GONE);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		if (parentViewsController.consumeAccionesDeReinicio())
			return;
		
		ownDelegate.setOwnerController(this);
		getParentViewsController().setCurrentActivityApp(this);
		ownDelegate.configureScreen();
		moverScroll();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public void goBack() {
		super.goBack();
		getParentViewsController().removeDelegateFromHashMap(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID);
	}

	/**
	 * Finds the view to be used in by the activity.
	 */
	private void findViews() {
		dataTableLayout = (LinearLayout)findViewById(SuiteAppAdmonApi.getResourceId("confirmationDataTableLayout", "id"));
		termsLink = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("termsLink", "id"));
		termsCheckBox = (CheckBox)findViewById(SuiteAppAdmonApi.getResourceId("termsCheckbox", "id"));
		
		cardElementLayout = findViewById(SuiteAppAdmonApi.getResourceId("confirmationCardLayout", "id"));		
		cardElementLabel = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("confirmationCardLabel", "id"));
		cardElementEdittext = (EditText)findViewById(SuiteAppAdmonApi.getResourceId("confirmationCardEdittext", "id"));
		cardElementInstructionsLabel = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("confirmationCardInstructionsLabel", "id"));
		
		nipElementLayout = findViewById(SuiteAppAdmonApi.getResourceId("confirmationNipLayout", "id"));		
		nipElementLabel = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("confirmationNipLabel", "id"));
		nipElementEdittext = (EditText)findViewById(SuiteAppAdmonApi.getResourceId("confirmationNipEdittext","id"));
		nipElementInstructionsLabel = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("confirmationNipInstructionsLabel", "id"));
		
		pwdElementLayout = findViewById(SuiteAppAdmonApi.getResourceId("confirmationPasswordLayout", "id"));		
		pwdElementLabel = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("confirmationPasswordLabel", "id"));
		pwdElementEdittext = (EditText)findViewById(SuiteAppAdmonApi.getResourceId("confirmationPasswordEdittext", "id"));
		pwdElementInstructionsLabel = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("confirmationPasswordInstructionsLabel", "id"));
		
		cvvElementLayout = findViewById(SuiteAppAdmonApi.getResourceId("confirmationCvvLayout", "id"));		
		cvvElementLabel = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("confirmationCvvLabel", "id"));
		cvvElementEdittext = (EditText)findViewById(SuiteAppAdmonApi.getResourceId("confirmationCvvEdittext", "id"));
		cvvElementInstructionsLabel = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("confirmationCvvInstructionsLabel", "id"));
		
		otpElementLayout = findViewById(SuiteAppAdmonApi.getResourceId("confirmationOtpLayout", "id"));		
		otpElementLabel = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("confirmationOtpLabel", "id"));
		otpElementEdittext = (EditText)findViewById(SuiteAppAdmonApi.getResourceId("confirmationOtpEdittext", "id"));
		otpElementInstructionsLabel = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("confirmationOtpInstructionsLabel", "id"));
		
		helpImage = findViewById(SuiteAppAdmonApi.getResourceId("helpImage", "id"));
	}
	
	/**
	 * Scales all the views to match the current screen resolution.
	 */
	private void scaleToCurrentScreen() {
		final GuiTools guitools = GuiTools.getCurrent();
		guitools.init(getWindowManager());
		
		guitools.scale(dataTableLayout);
		
		guitools.scale(findViewById(SuiteAppAdmonApi.getResourceId("termsLabel", "id")), true);
		guitools.scale(findViewById(SuiteAppAdmonApi.getResourceId("seeTermsLayout", "id")));
		guitools.scale(termsLink, true);
		guitools.scale(termsCheckBox);
		
		guitools.scale(findViewById(SuiteAppAdmonApi.getResourceId("confirmationFieldsLayout", "id")));
		
		guitools.scale(cardElementLayout);
		guitools.scale(cardElementLabel, true);
		guitools.scale(cardElementEdittext, true);
		guitools.scale(cardElementInstructionsLabel, true);
		guitools.scale(findViewById(SuiteAppAdmonApi.getResourceId("confirmationInnerCardLayout","id")));
		
		guitools.scale(nipElementLayout);
		guitools.scale(nipElementLabel, true);
		guitools.scale(nipElementEdittext, true);
		guitools.scale(nipElementInstructionsLabel, true);
		guitools.scale(findViewById(SuiteAppAdmonApi.getResourceId("confirmationInnerNipLayout","id")));
		
		guitools.scale(pwdElementLayout);
		guitools.scale(pwdElementLabel, true);
		guitools.scale(pwdElementEdittext, true);
		guitools.scale(pwdElementInstructionsLabel, true);
		guitools.scale(findViewById(SuiteAppAdmonApi.getResourceId("confirmationInnerPasswordLayout","id")));
		
		guitools.scale(cvvElementLayout);
		guitools.scale(cvvElementLabel, true);
		guitools.scale(cvvElementEdittext, true);
		guitools.scale(cvvElementInstructionsLabel, true);
		guitools.scale(findViewById(SuiteAppAdmonApi.getResourceId("confirmationInnerCvvLayout","id")));
		
		guitools.scale(otpElementLayout);
		guitools.scale(otpElementLabel, true);
		guitools.scale(otpElementEdittext, true);
		guitools.scale(otpElementInstructionsLabel, true);
		guitools.scale(findViewById(SuiteAppAdmonApi.getResourceId("confirmationInnerOtpLayout","id")));
		
		guitools.scale(findViewById(SuiteAppAdmonApi.getResourceId("confirmationConfirmButton","id")));
		guitools.scale(helpImage);
	}
	//#endregion
	
	//#region Configure screen.
	/**
	 * Sets the basic configuration for the screen.
	 * @param dataTable
	 */
	public void configureScreen(final ListaDatosViewController dataTable) {
		if (cardElementEdittext.getVisibility() == View.GONE &&
		    nipElementEdittext.getVisibility() == View.GONE &&
		    pwdElementEdittext.getVisibility() == View.GONE &&
		    cvvElementEdittext.getVisibility() == View.GONE &&
		    otpElementEdittext.getVisibility() == View.GONE)
			((LinearLayout)findViewById(SuiteAppAdmonApi.getResourceId("confirmationFieldsLayout","id"))).setBackgroundColor(Color.TRANSPARENT);

		final String mystring = termsLink.getText().toString();
		final SpannableString content = new SpannableString(mystring);
		content.setSpan(new UnderlineSpan(), 0, mystring.length(), 0);
		termsLink.setText(content);
		
		dataTableLayout.removeAllViews();
		dataTableLayout.addView(dataTable);
		
		if(otpElementEdittext.getVisibility() != View.GONE)
			otpElementEdittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
		else if(cvvElementEdittext.getVisibility() != View.GONE)
			cvvElementEdittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
		else if(pwdElementEdittext.getVisibility() != View.GONE)
			pwdElementEdittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
		else if(nipElementEdittext.getVisibility() != View.GONE)
			nipElementEdittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
		else if(cardElementEdittext.getVisibility() != View.GONE)
			cardElementEdittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
	}
	
	/**
	 * Configures the card layout and inner views.
	 * @param visible Flag to indicate if the element is visible.
	 * @param label The title resource id.
	 * @param instructions The instructions resource id.
	 */
	public void configureCardElement(final boolean visible, final String label, final String instructions) {
		cardElementLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
		
		if(visible) {
			cardElementLabel.setText(label);
			cardElementInstructionsLabel.setText(instructions);
			cardElementEdittext.setText(Constants.EMPTY_STRING);
			
			cardElementInstructionsLabel.setVisibility(instructions.equals(Constants.EMPTY_STRING) ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Configures the nip layout and inner views.
	 * @param visible Flag to indicate if the element is visible.
	 * @param label The title resource id.
	 * @param instructions The instructions resource id.
	 */
	public void configureNipElement(final boolean visible, final String label, final String instructions) {
		nipElementLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
		
		if(visible) {
			nipElementLabel.setText(label);
			nipElementInstructionsLabel.setText(instructions);
			nipElementEdittext.setText(Constants.EMPTY_STRING);
			
			nipElementInstructionsLabel.setVisibility(instructions.equals(Constants.EMPTY_STRING) ? View.GONE : View.VISIBLE);
		}
	}
	
	/**
	 * Configures the password layout and inner views.
	 * @param visible Flag to indicate if the element is visible.
	 * @param label The title resource id.
	 * @param instructions The instructions resource id.
	 */
	public void configurePasswordElement(final boolean visible, final String label, final String instructions) {
		pwdElementLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
		
		if(visible) {
			pwdElementLabel.setText(label);
			pwdElementInstructionsLabel.setText(instructions);
			pwdElementEdittext.setText(Constants.EMPTY_STRING);
			
			pwdElementInstructionsLabel.setVisibility(instructions.equals(Constants.EMPTY_STRING) ? View.GONE : View.VISIBLE);
		}
	}
	
	/**
	 * Configures the cvv layout and inner views.
	 * @param visible Flag to indicate if the element is visible.
	 * @param label The title resource id.
	 * @param instructions The instructions resource id.
	 */
	public void configureCvvElement(final boolean visible, final String label, final String instructions) {
		cvvElementLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
		
		if(visible) {
			cvvElementLabel.setText(label);
			cvvElementInstructionsLabel.setText(instructions);
			cvvElementEdittext.setText(Constants.EMPTY_STRING);
			
			cvvElementInstructionsLabel.setVisibility(instructions.equals(Constants.EMPTY_STRING) ? View.GONE : View.VISIBLE);
		}
	}
	
	/**
	 * Configures the OTP layout and inner views.
	 * @param visible Flag to indicate if the element is visible.
	 * @param label The title resource id.
	 * @param instructions The instructions resource id.
	 * @param code The code to be set in the OTP view, migth be and empty string.
	 */
	public void configureOtpElement(final boolean visible, final String label, final String instructions, final String code) {
		otpElementLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
		
		if(visible) {
			otpElementLabel.setText(label);
			otpElementInstructionsLabel.setText(instructions);
			otpElementEdittext.setText(code);
			
			otpElementInstructionsLabel.setVisibility(instructions.equals(Constants.EMPTY_STRING) ? View.GONE : View.VISIBLE);
		}
		
		if(Constants.EMPTY_STRING.equals(code)) {
			otpElementEdittext.setEnabled(true);
			otpElementEdittext.setTransformationMethod(null);
		} else {
			otpElementEdittext.setEnabled(false);
			otpElementEdittext.setTransformationMethod(PasswordTransformationMethod.getInstance());
		}
	}
	//#endregion
	
	//#region OnClick listeners.
	/**
	 * Behavior when the confirm button is clicked.
	 * @param sender The clicked view.
	 */
	public void onConfirmButtonClick(final View sender) {
	

		//AMZ Ale
		OperacionRealizadaMap.put("evento_realizada","event52");
		OperacionRealizadaMap.put("&&products","operaciones;admin+ asociar numero");
		OperacionRealizadaMap.put("eVar12","operacion realizada");
		TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);

		final String card = cardElementEdittext.getText().toString();
		final String nip = nipElementEdittext.getText().toString();
		final String pwd = pwdElementEdittext.getText().toString();
		final String cvv = cvvElementEdittext.getText().toString();
		final String otp = otpElementEdittext.getText().toString();
		final boolean termsAccepted = termsCheckBox.isChecked();
		
		ownDelegate.confirm(termsAccepted, card, nip, pwd, cvv, otp);
	}
	
	/**
	 * Behavior when the terms link is clicked.
	 * @param sender The clicked view.
	 */
	public void onShowTermsLinkClick(final View sender) {
		ownDelegate.requestSpeiTermsAndConditions();
	}
	//#endregion
	
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {

		if(SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController().getBaseDelegateForKey(
				ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID) !=null	){
			ownDelegate = (ConfirmacionAsignacionSpeiDelegate)SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController()
					.getBaseDelegateForKey(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID);
		}

		ownDelegate.analyzeResponse(operationId, response);
	}
	
	/**
	 * Reset the text values for all the authentication elements.
	 */
	public void cleanAuthenticationFields() {
		cardElementEdittext.setText(Constants.EMPTY_STRING);
		nipElementEdittext.setText(Constants.EMPTY_STRING);
		pwdElementEdittext.setText(Constants.EMPTY_STRING);
		cvvElementEdittext.setText(Constants.EMPTY_STRING);
		otpElementEdittext.setText(Constants.EMPTY_STRING);
	}
}
