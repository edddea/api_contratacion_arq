/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.gui.proxys.administracion;

import android.util.Log;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionAsignacionSpeiViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConfirmacionAsignacionSpeiDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MantenimientoSpeiMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.SpeiTermsAndConditionsResult;
import suitebancomer.aplicaciones.resultados.proxys.IConfirmacionAsignacionSpeiServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionAsignacionSpeiViewTo;
import suitebancomer.aplicaciones.resultados.to.ItemTo;
import suitebancomer.aplicaciones.resultados.to.ParamTo;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

/**
 * @author lbermejo
 *
 */
public class ConfirmacionAsignacionSpeiServiceProxy 
implements IConfirmacionAsignacionSpeiServiceProxy {
	
	/**
	 */
	private static final long serialVersionUID = 3925667201345924488L;
	private BaseDelegateCommons baseDelegateCommons;
	
	public ConfirmacionAsignacionSpeiServiceProxy(final BaseDelegateCommons bdc ) {
		this.baseDelegateCommons = bdc;
	}
	
	@Override
	public ArrayList<Object> getListaDatos() {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getListaDatos >> delegate");
		final ConfirmacionAsignacionSpeiDelegate delegate = (ConfirmacionAsignacionSpeiDelegate)baseDelegateCommons;
		final ArrayList<Object> list = delegate.getOperationDelegate().getDatosTablaConfirmacion();
		return list;				
	}

	@Override
	public ConfirmacionAsignacionSpeiViewTo showFields() {
		final ConfirmacionAsignacionSpeiViewTo to = new ConfirmacionAsignacionSpeiViewTo();
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy showFields >> delegate");
		final ConfirmacionAsignacionSpeiDelegate delegate = 
				(ConfirmacionAsignacionSpeiDelegate)baseDelegateCommons;

		final ItemTo itemCard = new ItemTo();
		itemCard.setVisible(delegate.getOperationDelegate().mostrarCampoTarjeta());
		itemCard.setLabel(delegate.getOperationDelegate().getEtiquetaCampoTarjeta());
		itemCard.setInstruction(delegate.getOperationDelegate().getTextoAyudaTarjeta());
		to.setCardItem(itemCard);
		//controller.configureCardElement(visible, elementLabel, elementInstructions);

		final ItemTo itemNip = new ItemTo();
		itemNip.setVisible(delegate.getOperationDelegate().mostrarNIP());
		itemNip.setLabel(delegate.getOperationDelegate().getEtiquetaCampoNip());
		itemNip.setInstruction(delegate.getOperationDelegate().getTextoAyudaNIP());
		to.setNipItem(itemNip);
		//controller.configureNipElement(visible, elementLabel, elementInstructions);

		final ItemTo itemPswd = new ItemTo();
		itemPswd.setVisible(delegate.getOperationDelegate().mostrarContrasenia());
		itemPswd.setLabel(delegate.getOperationDelegate().getEtiquetaCampoContrasenia());
		itemPswd.setInstruction(Constants.EMPTY_STRING);
		to.setPwdItem(itemPswd);
		//controller.configurePasswordElement(visible, elementLabel, elementInstructions);

		final ItemTo itemCvv = new ItemTo();
		itemCvv.setVisible(delegate.getOperationDelegate().mostrarCVV());
		itemCvv.setLabel(delegate.getOperationDelegate().getEtiquetaCampoCVV());
		itemCvv.setInstruction(delegate.getOperationDelegate().getTextoAyudaCVV());
		to.setCvvItem(itemCvv);
		//controller.configureCvvElement(visible, elementLabel, elementInstructions);
		
		final Constants.TipoOtpAutenticacion otpType = delegate.getOperationDelegate().tokenAMostrar();
		final String instrument = Session.getInstance(SuiteApp.appContext).getSecurityInstrument();
		Constants.TipoInstrumento instrumentType =
				instrument.equals(Constants.IS_TYPE_DP270) ? Constants.TipoInstrumento.DP270 :
					(instrument.equals(Constants.IS_TYPE_OCRA) ? Constants.TipoInstrumento.OCRA :
						(instrument.equals(Constants.TYPE_SOFTOKEN.S1.value) ? instrumentType = Constants.TipoInstrumento.SoftToken :
							Constants.TipoInstrumento.sinInstrumento));

		final ItemTo itemOtp = new ItemTo();
		itemOtp.setVisible(otpType != Constants.TipoOtpAutenticacion.ninguno);
		itemOtp.setLabel(((MantenimientoSpeiMovilDelegate) delegate.getOperationDelegate()).getEtiquetaCampoOTP(instrumentType));
		itemOtp.setInstruction(delegate.getOperationDelegate().getTextoAyudaInstrumentoSeguridad(instrumentType));
		itemOtp.setCode((instrumentType == Constants.TipoInstrumento.SoftToken
				&& SuiteApp.getSofttokenStatus())
				? Constants.DUMMY_OTP : Constants.EMPTY_STRING);
		to.setOtpItem(itemOtp);
		//controller.configureOtpElement(visible, elementLabel, elementInstructions, code);
		
		to.setTokenAMostrar(otpType);
		to.setInstrumentoSeguridad(instrumentType);

		return to;
	}
	
	
	@Override
	public Integer getMessageAsmError(final Constants.TipoInstrumento tipoInstrumento) {
		//TODO Cambiar por Log.
		System.out.println(" llego al metodo del proxy getMessageAsmError  y va al delegate ");
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getMessageAsmError >> delegate");
		int idMsg = 0;

		switch (tipoInstrumento) {
			case OCRA:
				idMsg = R.string.confirmation_ocra;
				break;
			case DP270:
				idMsg = R.string.confirmation_dp270;
				break;
			case SoftToken:
				if (SuiteApp.getSofttokenStatus()) {
					idMsg = R.string.confirmation_softtokenActivado;
				} else {
					idMsg = R.string.confirmation_softtokenDesactivado;
				}
				break;
			default:
				break;
		}

		return idMsg;
		
		/*switch (tipoInstrumentoSeguridad) {
			case OCRA:
				mensaje += getEtiquetaCampoOCRA();
				break;
			case DP270:
				mensaje += getEtiquetaCampoDP270();
				break;
			case SoftToken:
				if (SuiteApp.getSofttokenStatus()) {
					mensaje += getEtiquetaCampoSoftokenActivado();
				} else {
					mensaje += getEtiquetaCampoSoftokenDesactivado();
				}
				break;
			default:
				break;
		}
		*/ 
	}

	
	@Override
	public String loadOtpFromSofttoken(final Constants.TipoOtpAutenticacion tipoOtpAutenticacion) {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy loadOtpFromSofttoken >> delegate");
		final ConfirmacionAsignacionSpeiDelegate delegate = 
				(ConfirmacionAsignacionSpeiDelegate)baseDelegateCommons;
		final String res = delegate.loadOtpFromSofttoken(tipoOtpAutenticacion);
		return res;
	}

	
	@Override
	public Integer consultaOperationsIdTextoEncabezado(){
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy consultaOperationsIdTextoEncabezado >> delegate");
		//getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()
		final ConfirmacionAsignacionSpeiDelegate delegate = 
								(ConfirmacionAsignacionSpeiDelegate)baseDelegateCommons;
		final int res = delegate.getOperationDelegate().getTextoEncabezado();
		
		return res;
	}

	
	@Override
	public Boolean doOperation(final ParamTo to) {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy doOperation >> delegate");
		final ConfirmacionAsignacionSpeiViewTo viewTo = (ConfirmacionAsignacionSpeiViewTo)to;
		final String newToken = getASMFromDelegate(viewTo);

		if(null != newToken){
			viewTo.setOtp(newToken); 
		}
		
		try {

			//((MantenimientoSpeiMovilDelegate)operationDelegate).
			//	realizeOperation(ownerController, pwd, nip, otp, cvv, card);
			final ConfirmacionAsignacionSpeiDelegate delegate = 
					(ConfirmacionAsignacionSpeiDelegate)baseDelegateCommons;

			final ConfirmacionAsignacionSpeiViewController caller = new ConfirmacionAsignacionSpeiViewController();
			caller.setDelegate(delegate);
			delegate.setOwnerController(caller);
			caller.setDelegate(delegate);
			caller.setParentViewsController(((BmovilViewsController)
					SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController()));
			
			((MantenimientoSpeiMovilDelegate)delegate.getOperationDelegate())
			.realizeOperation(caller, viewTo.getContrasena(), 
					viewTo.getNip(), viewTo.getOtp(), viewTo.getCvv(), 
					viewTo.getTarjeta());
			
			return Boolean.TRUE;

		}catch(Exception e){
			//TODO Log
			return Boolean.FALSE;
		}
	}
	
	/**
	 * 
	 * @param to
	 * @return
	 */
	private String getASMFromDelegate(final ParamTo to){
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getASMFromDelegate >> delegate");
		final ConfirmacionAsignacionSpeiDelegate delegate = 
				(ConfirmacionAsignacionSpeiDelegate)baseDelegateCommons;

		final Constants.TipoOtpAutenticacion otpType = delegate.getOperationDelegate().tokenAMostrar();
		final String instrument = Session.getInstance(SuiteApp.appContext).getSecurityInstrument();
		String newToken = null;
		if(otpType != Constants.TipoOtpAutenticacion.ninguno &&
				instrument.equals(Constants.TYPE_SOFTOKEN.S1.value)
								&& SuiteApp.getSofttokenStatus())
			newToken = loadOtpFromSofttoken(otpType);
		
		return newToken;
	}
	
	@Override
	public BaseDelegateCommons getDelegate() {
		return baseDelegateCommons;
	}
	
	public Boolean requestSpeiTermsAndConditions(){
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy requestSpeiTermsAndConditions >> delegate");
		final ConfirmacionAsignacionSpeiDelegate delegate = 
				(ConfirmacionAsignacionSpeiDelegate)baseDelegateCommons;
		
		MantenimientoSpeiMovilDelegate speiDelegate = null;
		
		try {
			speiDelegate = (MantenimientoSpeiMovilDelegate)delegate.getOperationDelegate();
		} catch(Exception ex) {
			speiDelegate = null;
			Log.e(this.getClass().getSimpleName(), "Error while casting the operation delegate to \"MantenimientoSpeiMovilDelegate\".", ex);
			return Boolean.FALSE;
		}

		final ConfirmacionAsignacionSpeiViewController caller = new ConfirmacionAsignacionSpeiViewController();
		caller.setDelegate(delegate);
		delegate.setOwnerController(caller);
		final Hashtable<String, String> params = new Hashtable<String, String>();
		params.put("idProducto", speiDelegate.getSpeiModel().getTipoOperacion().getOperationCodeForServer());
		//JAIG SI

		//isJsonValueCode.NONE,
		delegate.doNetworkOperation(Server.SPEI_TERMS_REQUEST, params, true, new SpeiTermsAndConditionsResult(),caller);

		return Boolean.TRUE;
		
	}
}
