package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

public class ConfigurarMontos {
	
	private String montoPorOperacion;
	private String montoDiario;
	private String montoMensual;
	private String montoMaxPorOperacion;
	private String montoMaxDiario;
	private String montoMaxMensual;
	
	public ConfigurarMontos(final String montoPorOperacion, final String montoDiario,
							final String montoMensual, final String montoMaxPorOperacion,
							final String montoMaxDiario, final String montoMaxMensual) {
		super();
		this.montoPorOperacion = montoPorOperacion;
		this.montoDiario = montoDiario;
		this.montoMensual = montoMensual;
		this.montoMaxPorOperacion = montoMaxPorOperacion;
		this.montoMaxDiario = montoMaxDiario;
		this.montoMaxMensual = montoMaxMensual;
	}

	/**
	 * @param montoPorOperacion the montoPorOperacion to set
	 */
	public void setMontoPorOperacion(final String montoPorOperacion) {
		this.montoPorOperacion = montoPorOperacion;
	}
	
	/**
	 * @param montoDiario the montoDiario to set
	 */
	public void setMontoDiario(final String montoDiario) {
		this.montoDiario = montoDiario;
	}

	/**
	 * @param montoMensual the montoMensual to set
	 */
	public void setMontoMensual(final String montoMensual) {
		this.montoMensual = montoMensual;
	}	
	
	/**
	 * El importe máximo que podr� operar bmovil.
	 * @return the montoPorOperacion
	 */
	public String getMontoPorOperacion() {
		return montoPorOperacion;
	}

	/**
	 * El importe máximo que podr� operar diariamente bmovil.
	 * @return the montoDiario
	 */
	public String getMontoDiario() {
		return montoDiario;
	}

	/**
	 * El importe máximo que podr� operar mensualmente bmovil.
	 * @return the montoMensual
	 */
	public String getMontoMensual() {
		return montoMensual;
	}

	/**
	 * El importe máximo por operación devuelto por Naca.
	 * @return the montoMaxPorOperacion
	 */
	public String getMontoMaxPorOperacion() {
		return montoMaxPorOperacion;
	}

	/**
	 * El importe máximo diario devuelto por Nacar.
	 * @return the montoMaxDiario
	 */
	public String getMontoMaxDiario() {
		return montoMaxDiario;
	}

	/**
	 * El importe máximo mensual  devuelto por Nacar.
	 * @return the montoMaxMensual
	 */
	public String getMontoMaxMensual() {
		return montoMaxMensual;
	}	
}
