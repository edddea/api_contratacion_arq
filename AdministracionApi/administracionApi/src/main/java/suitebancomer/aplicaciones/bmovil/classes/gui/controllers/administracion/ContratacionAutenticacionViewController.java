package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioPerfilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ContratacionAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ContratacionDelegate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.ListaDatosViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class ContratacionAutenticacionViewController extends BaseViewController implements View.OnClickListener {

	private ImageButton confirmarButton;
	
	private LinearLayout contenedorPrincipal;
	private LinearLayout contenedorContrasena;
	private LinearLayout contenedorNIP;
	private LinearLayout contenedorASM;
	private LinearLayout contenedorCVV;
	
	private TextView campoContrasena;
	private TextView campoNIP;
	private TextView campoASM;
	private TextView campoCVV;
	private EditText contrasena;
	private EditText nip;	
	private EditText asm;
	private EditText cvv;
	private TextView instruccionesContrasena;
	private TextView instruccionesNIP;
	private TextView instruccionesASM;
	private TextView instruccionesCVV;
	private TextView aceptoTerminos;
	private TextView verTerminos;
	private CheckBox aceptoTerminosCB;
	private boolean goBackToHome;

	private ContratacionAutenticacionDelegate contratacionAutenticacionDelegate;
	
	//Nuevo Campo
	private TextView campoTarjeta;
	private LinearLayout contenedorCampoTarjeta;
	private EditText tarjeta;
	private TextView instruccionesTarjeta;

	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_contratacion_autenticacion_admon);
		SuiteApp.appContext = this;
		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate((ContratacionAutenticacionDelegate)getParentViewsController().getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID));
		setTitulo();
		//AMZ
				parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();
				TrackingHelper.trackState("confirma",parentManager.estados);
		
		contratacionAutenticacionDelegate = (ContratacionAutenticacionDelegate)getDelegate();
		contratacionAutenticacionDelegate.setcontratacionAutenticacionViewController(this);
		
		if(contratacionAutenticacionDelegate.consultaOperationsDelegate() instanceof ContratacionDelegate){ //Llegamos desde contratación
			final ContratacionDelegate contratacionDelegate = (ContratacionDelegate)contratacionAutenticacionDelegate.consultaOperationsDelegate();
			
			contratacionDelegate.setOwnerController(this);
			
			findViews();
			scaleToScreenSize();
			
			contratacionAutenticacionDelegate.consultaDatosLista();
			
			configuraPantalla();
			moverScroll();
			
			goBackToHome = false;
			
			if(contratacionDelegate.isDeleteData())
				contratacionDelegate.deleteData();
		
		} else { // Se crea la pantalla desde el flujo de cambio de perfil
			final CambioPerfilDelegate cambioPerfilDelegate = (CambioPerfilDelegate) contratacionAutenticacionDelegate.consultaOperationsDelegate();

			cambioPerfilDelegate.setOwnerController(this);

			findViews();
			scaleToScreenSize();

			contratacionAutenticacionDelegate.consultaDatosLista();

			configuraPantalla();
			moverScroll();

			goBackToHome = false;
		}							
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if(goBackToHome)
			SuiteAppAdmonApi.getInstance().getSuiteViewsController().showMenuSuite(true);
//		if (parentViewsController.consumeAccionesDeReinicio()) {
//			return;
//		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		//goBackToHome = true;
		//parentViewsController.consumeAccionesDePausa();
	}
	
	private void configuraPantalla() {
//		String mystring = getString(R.string.contratacion_autenticacion_acepto_terminos_y_condiciones_link);
//		SpannableString content = new SpannableString(mystring);
//		content.setSpan(new UnderlineSpan(), 0, mystring.length(), 0);
//		verTerminos.setText(content);
		
		mostrarContrasena(contratacionAutenticacionDelegate.consultaDebePedirContrasena());
		mostrarNIP(contratacionAutenticacionDelegate.consultaDebePedirNIP());
		mostrarASM(contratacionAutenticacionDelegate.consultaInstrumentoSeguridad());
		mostrarCVV(contratacionAutenticacionDelegate.consultaDebePedirCVV());
		mostrarCampoTarjeta(contratacionAutenticacionDelegate.mostrarCampoTarjeta());

		final LinearLayout contenedorPadre = (LinearLayout)findViewById(R.id.contratacion_autenticacion_campos_layout);
		
		if (contenedorContrasena.getVisibility() == View.GONE &&
			contenedorNIP.getVisibility() == View.GONE &&
			contenedorASM.getVisibility() == View.GONE &&
			contenedorCVV.getVisibility() == View.GONE &&
			contenedorCampoTarjeta.getVisibility() == View.GONE) {
			contenedorPadre.setBackgroundColor(0);
		}
		
		contenedorPadre.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		final float camposHeight = contenedorPadre.getMeasuredHeight();

		final LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.contratacion_autenticacion_lista_datos);
		layoutListaDatos.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		final float listaHeight = layoutListaDatos.getMeasuredHeight();

		final ViewGroup contenido = (ViewGroup)this.findViewById(android.R.id.content).getRootView();//findViewById(android.R.id.content);
		contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		final float contentHeight = contenido.getMeasuredHeight();
		
		//System.out.println("Los valores " + camposHeight + " y " + contentHeight + " y " + listaHeight);

		final float margin = getResources().getDimension(R.dimen.confirmacion_fields_initial_margin);

		final float maximumSize = (contentHeight * 4) / 5;
		//System.out.println("Altura maxima " +maximumSize);
		final float elementsSize = listaHeight + camposHeight;
		//System.out.println("Altura mixta " +elementsSize);
		final float heightParaValidar = (contentHeight*3)/4;
		//System.out.println("heightParaValidar " +contentHeight);
		
		
		confirmarButton.setOnClickListener(this);
	}
	
	public void setTitulo() {
		final ContratacionAutenticacionDelegate contratacionAutenticacionDelegate = (ContratacionAutenticacionDelegate)getDelegate();
		
		setTitle(contratacionAutenticacionDelegate.consultaOperationsDelegate().getTextoEncabezado(),
				 contratacionAutenticacionDelegate.consultaOperationsDelegate().getNombreImagenEncabezado());
		
		 if(Server.ALLOW_LOG) Log.e("Contratacion", ""+contratacionAutenticacionDelegate.consultaOperationsDelegate().getTextoEncabezado());
	}
	
	@SuppressWarnings("deprecation")
	public void setListaDatos(final ArrayList<Object> datos) {
		final LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		final ListaDatosViewController listaDatos = new ListaDatosViewController(this, params, parentViewsController);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.setTitulo(R.string.confirmation_subtitulo);
		listaDatos.showLista();
		final LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.contratacion_autenticacion_lista_datos);
		layoutListaDatos.addView(listaDatos);
	}
	
	public void pideContrasenia() {
		findViewById(R.id.campo_contratacion_autenticacion_contrasena_layout).setVisibility(View.GONE);
	}
	
	public void pideClaveSeguridad() {
		findViewById(R.id.campo_contratacion_autenticacion_asm_layout).setVisibility(View.GONE);
	}
	
	/*
	* 
	*/
	public void mostrarContrasena(final boolean visibility){
		contenedorContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		contrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoContrasena.setText(contratacionAutenticacionDelegate.getEtiquetaCampoContrasenia());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
			contrasena.setFilters(userFilterArray);
			contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		instruccionesContrasena.setVisibility(View.GONE);
	}
	
	/*
	* 
	*/
	public void mostrarNIP(final boolean visibility){
		contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		nip.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoNIP.setText(contratacionAutenticacionDelegate.getEtiquetaCampoNip());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
			nip.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
			final String instrucciones = contratacionAutenticacionDelegate.getTextoAyudaNIP();
			if (instrucciones.equals("")) {
				instruccionesNIP.setVisibility(View.GONE);
			} else {
				instruccionesNIP.setVisibility(View.VISIBLE);
				instruccionesNIP.setText(instrucciones);
			}
		} else {
			nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}
	
	/*
	* 
	*/
	public void mostrarASM(final Constants.TipoOtpAutenticacion tipoOTP){
		switch(tipoOTP) {
			case ninguno:
				contenedorASM.setVisibility(View.GONE);
				campoASM.setVisibility(View.GONE);
				asm.setVisibility(View.GONE);
				asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
				break;
			case codigo:
			case registro:
				contenedorASM.setVisibility(View.VISIBLE);
				campoASM.setVisibility(View.VISIBLE);
				asm.setVisibility(View.VISIBLE);
				InputFilter[] userFilterArray = new InputFilter[1];
				userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
				asm.setFilters(userFilterArray);
				cambiarAccionTexto(contrasena);
				cambiarAccionTexto(nip);
				asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
		}

		final Constants.TipoInstrumento tipoInstrumento = contratacionAutenticacionDelegate.consultaTipoInstrumentoSeguridad();

		switch (tipoInstrumento) {
			case OCRA:
				campoASM.setText(contratacionAutenticacionDelegate.getEtiquetaCampoOCRA());
				//asm.setTransformationMethod(null);
				break;
			case DP270:
				campoASM.setText(contratacionAutenticacionDelegate.getEtiquetaCampoDP270());
				//asm.setTransformationMethod(null);
				break;
			case SoftToken:
				if(SuiteAppAdmonApi.getSofttokenStatus()) {
					asm.setText(Constants.DUMMY_OTP);
					asm.setEnabled(false);
					campoASM.setText(contratacionAutenticacionDelegate.getEtiquetaCampoSoftokenActivado());
				} else {
					asm.setText("");
					asm.setEnabled(true);
					//asm.setTransformationMethod(null);
					campoASM.setText(contratacionAutenticacionDelegate.getEtiquetaCampoSoftokenDesactivado());
				}
				break;
			default:
				break;
		}
		final String instrucciones = contratacionAutenticacionDelegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
		if (instrucciones.equals("")) {
			instruccionesASM.setVisibility(View.GONE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}		
	}
	
	private void cambiarAccionTexto(final EditText campo) {
		if (campo.getVisibility() == View.VISIBLE) {
			campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
	}
	
	public String pideContrasena() {
		if (contrasena.getVisibility() == View.GONE) {
			return "";
		} else {
			return contrasena.getText().toString();
		}
	}
	
	public String pideNIP() {
		if (nip.getVisibility() == View.GONE) {
			return "";
		} else {
			return nip.getText().toString();
		}
	}
	
	public String pideASM() {
		if (asm.getVisibility() == View.GONE) {
			return "";
		} else {
			return asm.getText().toString();
		}
	}
	
	public String pideCVV() {
		if (cvv.getVisibility() == View.GONE) {
			return "";
		} else {
			return cvv.getText().toString();
		}
	}
	
	/*
	* 
	*/
	public void mostrarCVV(final boolean visibility){
		contenedorCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		cvv.setVisibility(visibility ? View.VISIBLE:View.GONE);
		instruccionesCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		
		if (visibility) {
			campoCVV.setText(contratacionAutenticacionDelegate.getEtiquetaCampoCVV());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
			cvv.setFilters(userFilterArray);
			final String instrucciones = contratacionAutenticacionDelegate.getTextoAyudaCVV();
			instruccionesCVV.setText(instrucciones);
			instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(nip);
			cambiarAccionTexto(asm);
			cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}

	@Override
	public void onClick(final View v) {
		if (v == confirmarButton) {
			botonConfirmarClick();
		}
	}
	
	public void botonConfirmarClick() {
		contratacionAutenticacionDelegate.enviaPeticionOperacion();
		//AMZ
				if(contratacionAutenticacionDelegate.res)
				{
					//AMZ
					final Map<String,Object> OperacionRealizadaMap = new HashMap<String, Object>();
					//AMZ
					final CambioPerfilDelegate cambioPerfilDelegate = new CambioPerfilDelegate();
					if (Constants.Perfil.avanzado.equals( Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile()))
					{
						if(parentManager.estados.size() > 3)
						{
							//AMZ
							OperacionRealizadaMap.put("evento_realizada","event52");
							OperacionRealizadaMap.put("&&products","operaciones;admin+operar con token");
							OperacionRealizadaMap.put("eVar12","operacion realizada");
							TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
						}
						
					}
					else
					{
						if(parentManager.estados.size() > 3)
						{
							//AMZ
							OperacionRealizadaMap.put("evento_realizada","event52");
							OperacionRealizadaMap.put("&&products","operaciones;admin+operar sin token");
							OperacionRealizadaMap.put("eVar12","operacion realizada");
							TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);

						}
						
				
					}
				}
	}
	
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {

		if(

				getParentViewsController().getBaseDelegateForKey(
				ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID)!= null){
		//if(SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController()
		//		.getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID) !=null	){

			contratacionAutenticacionDelegate =	(ContratacionAutenticacionDelegate)
					getParentViewsController().getBaseDelegateForKey(
							ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);

			//contratacionAutenticacionDelegate =
			//		(ContratacionAutenticacionDelegate)
			//				SuiteAppAdmonApi.getInstance().getBmovilApplication()
			//						.getViewsController()
			//						.getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		}
		contratacionAutenticacionDelegate.analyzeResponse(operationId, response);
	}
	
	private void findViews() {
		contenedorPrincipal		= (LinearLayout)findViewById(R.id.contratacion_autenticacion_lista_datos);
		contenedorContrasena	= (LinearLayout)findViewById(R.id.campo_contratacion_autenticacion_contrasena_layout);
		contenedorNIP 			= (LinearLayout)findViewById(R.id.campo_contratacion_autenticacion_nip_layout);
		contenedorASM 			= (LinearLayout)findViewById(R.id.campo_contratacion_autenticacion_asm_layout);
		contenedorCVV 			= (LinearLayout)findViewById(R.id.campo_contratacion_autenticacion_cvv_layout);
		
		contrasena 				= (EditText)contenedorContrasena.findViewById(R.id.contratacion_autenticacion_contrasena_edittext);
		nip 					= (EditText)contenedorNIP.findViewById(R.id.contratacion_autenticacion_nip_edittext);
		asm						= (EditText)contenedorASM.findViewById(R.id.contratacion_autenticacion_asm_edittext);
		cvv						= (EditText)contenedorCVV.findViewById(R.id.contratacion_autenticacion_cvv_edittext);
		
		campoContrasena			= (TextView)contenedorContrasena.findViewById(R.id.contratacion_autenticacion_contrasena_label);
		campoNIP				= (TextView)contenedorNIP.findViewById(R.id.contratacion_autenticacion_nip_label);
		campoASM				= (TextView)contenedorASM.findViewById(R.id.contratacion_autenticacion_asm_label);
		campoCVV				= (TextView)contenedorCVV.findViewById(R.id.contratacion_autenticacion_cvv_label);
		
		
		instruccionesContrasena	= (TextView)contenedorContrasena.findViewById(R.id.contratacion_autenticacion_contrasena_instrucciones_label);
		instruccionesNIP		= (TextView)contenedorNIP.findViewById(R.id.contratacion_autenticacion_nip_instrucciones_label);
		instruccionesASM		= (TextView)contenedorASM.findViewById(R.id.contratacion_autenticacion_asm_instrucciones_label);
		instruccionesCVV		= (TextView)contenedorCVV.findViewById(R.id.contratacion_autenticacion_cvv_instrucciones_label);
		
		confirmarButton 		= (ImageButton)findViewById(R.id.contratacion_autenticacion_confirmar_button);
		
		aceptoTerminos     		= (TextView)findViewById(R.id.acepto_terminos_label);
		verTerminos				= (TextView)findViewById(R.id.acepto_terminos_link);
		aceptoTerminosCB		= (CheckBox)findViewById(R.id.acepto_terminos_checkbox);
		
		/*contenedorCampoTarjeta  = (LinearLayout)findViewById(R.id.campo_confirmacion_campotarjeta_layout);
		tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
		campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
		instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);
*/
		if ((contratacionAutenticacionDelegate.consultaOperationsDelegate().mostrarCVV())
				&& (!contratacionAutenticacionDelegate.consultaOperationsDelegate().mostrarNIP())) {
			// El contenedor de campo tarjeta deberÌa ir entre el instrumento de seguridad y el cvv
			contenedorCampoTarjeta  = (LinearLayout)findViewById(R.id.campo_confirmacion_campotarjeta_sin_nip_layout);
			tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_sin_nip_edittext);
			campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_sin_nip_label);
			instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_sin_nip_instrucciones_label);
		} else {
			// El contenedor de campo tarjeta deberÌa ir encima del nip
			contenedorCampoTarjeta  = (LinearLayout)findViewById(R.id.campo_confirmacion_campotarjeta_layout);
			tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
			campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
			instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);
		}
	}

	private void scaleToScreenSize() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(contenedorPrincipal);
		guiTools.scale(findViewById(R.id.contratacion_autenticacion_campos_layout));
		
		guiTools.scale(contenedorContrasena);
		guiTools.scale(contenedorNIP);
		guiTools.scale(contenedorASM);
		guiTools.scale(contenedorCVV);
		
		guiTools.scale(contrasena, true);
		guiTools.scale(nip, true);
		guiTools.scale(asm, true);
		guiTools.scale(cvv, true);
		
		guiTools.scale(campoContrasena, true);
		guiTools.scale(campoNIP, true);
		guiTools.scale(campoASM, true);
		guiTools.scale(campoCVV, true);
		
		guiTools.scale(instruccionesContrasena, true);
		guiTools.scale(instruccionesNIP, true);
		guiTools.scale(instruccionesASM, true);
		guiTools.scale(instruccionesCVV, true);
		
		guiTools.scale(findViewById(R.id.aceptar_terminos_layout));
		guiTools.scale(aceptoTerminos, true);
		guiTools.scale(verTerminos, true);
		guiTools.scale(aceptoTerminosCB);
		
		guiTools.scale(contenedorCampoTarjeta);
		guiTools.scale(tarjeta, true);
		guiTools.scale(campoTarjeta, true);
		guiTools.scale(instruccionesTarjeta, true);
		
		guiTools.scale(confirmarButton);
		
	}

	public void limpiarCampos(){
		contrasena.setText("");
		nip.setText("");
		asm.setText("");
		cvv.setText("");
		tarjeta.setText("");
	}
	
	/**
	 * Muestra los terminos y condiciones.
	 */
	public void onVerTerminosLinkClik(final View sernder) {
		contratacionAutenticacionDelegate.consultarTerminosDeUso();
		if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Ver terminos y condiciones.");
	}

	/**
	 * Se dsactiva el boton de atras.
	 */
	@Override
	public void goBack() {
		if(Server.ALLOW_LOG) Log.d(getClass().getSimpleName(), "Back presionado.");
		parentViewsController.removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		super.goBack();
	}
	
	/**
	 * @return True si los terminos y condiciones fueron aceptados, false de otro modo.
	 */
	public boolean getTerminosAceptados() {
		return aceptoTerminosCB.isChecked();
	}
	
	public String pideTarjeta(){
		if (tarjeta.getVisibility() == View.GONE) {
			return "";
		}else 
			return tarjeta.getText().toString();		
	}
	
	private void mostrarCampoTarjeta(final boolean visibility) {
		contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		tarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoTarjeta.setText(contratacionAutenticacionDelegate.getEtiquetaCampoTarjeta());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(5);
			tarjeta.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
			final String instrucciones = contratacionAutenticacionDelegate.getTextoAyudaTarjeta();
			if (instrucciones.equals("")) {
				instruccionesTarjeta.setVisibility(View.GONE);
			} else {
				instruccionesTarjeta.setVisibility(View.VISIBLE);
				instruccionesTarjeta.setText(instrucciones);
			}
		} else {
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		
	}

}
