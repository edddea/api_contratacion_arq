/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import suitebancomer.aplicaciones.bmovil.classes.common.administracion.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.NuevaContraseniaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.Desbloqueo;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

;
/**
 * @author Francisco.Garcia
 *
 */
public class NuevaContraseniaViewController extends BaseViewController implements OnClickListener {

	//AMZ
	private BmovilViewsController parentManager;

	private NuevaContraseniaDelegate delegate;
	private TextView lblContrasena,lblConfirmacion;
	private EditText contrasena;
	private EditText contrasenaConfirmacion;
	private ImageButton btnActivar;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_cambiar_password_admon);
		SuiteApp.appContext = this;
		setTitle(R.string.bmovil_desbloqueo_title, R.drawable.bmovil_desbloqueo_icono);
		final SuiteAppAdmonApi suiteApp = (SuiteAppAdmonApi)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate((NuevaContraseniaDelegate) parentViewsController.getBaseDelegateForKey(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID));
		delegate = (NuevaContraseniaDelegate)getDelegate(); 
		delegate.setViewController(this);
		findViews();
		scaleForCurrentScreen();
		inicializarPantalla();
        /*
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("nuevo password", parentManager.estados);
		*/
		contrasena.addTextChangedListener(new BmovilTextWatcher(this));
		contrasenaConfirmacion.addTextChangedListener(new BmovilTextWatcher(this));
	}
	
	/**
	 * referencias de vistas
	 */
	private void findViews(){
		contrasena = (EditText)findViewById(R.id.cambio_contrasena_contrasenaNueva_text);
		contrasenaConfirmacion = (EditText)findViewById(R.id.cambio_contrasena_contrasenaConfirmacion_text);
		btnActivar = (ImageButton)findViewById(R.id.cambio_contrasena_boton_confirmar);
		lblContrasena = (TextView) findViewById(R.id.cambio_contrasena_contrasenaNueva_label);
		lblConfirmacion = (TextView) findViewById(R.id.cambio_contrasena_contrasenaConfirmacion_label);
	}
	
	/**
	 * escalamiento de pantalla
	 */
	private void scaleForCurrentScreen(){
		final GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());

		gTools.scale(contrasena,true);
		gTools.scale(contrasenaConfirmacion,true);

		gTools.scale(lblContrasena,true);
		gTools.scale(lblConfirmacion,true);

		gTools.scale(btnActivar);	
		gTools.scale(findViewById(R.id.cambiar_password_resultado_contenedor_superior));
		
		
	}
	
	/**
	 * inicializa la pantalla con los elementos necesarios
	 */
	private void inicializarPantalla(){
		findViewById(R.id.cambio_contrasena_contrasenaActual_label).setVisibility(View.GONE);
		findViewById(R.id.cambio_contrasena_contrasenaActual_text).setVisibility(View.GONE);
		
		btnActivar.setOnClickListener(this);
		InputFilter[] filters = new InputFilter[1];
		filters[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);

		contrasena.setFilters(filters);
		contrasenaConfirmacion.setFilters(filters);

		contrasena.setLongClickable(false);
		contrasenaConfirmacion.setLongClickable(false);
		final int backgroundBtn = R.drawable.btn_continuar;
		btnActivar.setBackgroundResource(backgroundBtn);
		statusdesactivado = false;
		
		lblContrasena.setText(R.string.bmovil_desbloqueo_contrasena_label);
		lblConfirmacion.setText(R.string.bmovil_desbloqueo_confirmacion_label);
		btnActivar.setBackgroundResource(R.drawable.bmovil_btn_activar);
		contrasena.setHint(R.string.bmovil_common_hint_confirmar_contrasena);
		contrasenaConfirmacion.setHint(R.string.bmovil_common_hint_confirmar_contrasena);
	}

	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		if(statusdesactivado)
			if (parentViewsController.consumeAccionesDeReinicio()) {
					return;
				}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		final boolean accionesP = parentViewsController.consumeAccionesDePausa();
		statusdesactivado = accionesP;
	}

	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID);
		super.goBack();
	}
	
	@Override
	public void onClick(final View v) {
		if(v == btnActivar){			
			if(validaDatos()){
				final Perfil perfilCliente = delegate.getConsultaEstatus().getPerfil();
				final String contrasenaNueva = contrasena.getText().toString();
				final String numCelular = delegate.getConsultaEstatus().getNumCelular();

				final Desbloqueo desbloqueo = delegate.getDesbloqueo();
				desbloqueo.setContrasenaNueva(contrasenaNueva);
				desbloqueo.setPerfilCliente(perfilCliente);
				desbloqueo.setNumCelular(numCelular);
				
				showConfirmacion();
			}
		}		
	}
	
	/**
	 * validacion de los datos ingresados por el usuario
	 * @return true si los datos son validos.
	 */
	private boolean validaDatos(){
		final String pass = contrasena.getText().toString();
		final String cPass = contrasenaConfirmacion.getText().toString();
		return delegate.validaDatos(pass,cPass);
	}
	
	/**
	 * muestra la pantalla de confirmacion
	 */
	private void showConfirmacion(){
		delegate.showConfirmacion();		
	}
		
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}

}
