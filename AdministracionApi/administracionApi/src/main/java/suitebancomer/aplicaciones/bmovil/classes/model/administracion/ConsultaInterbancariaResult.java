package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public class ConsultaInterbancariaResult implements ParsingHandler{

	ArrayList<DetalleInterbancaria> arrTransferenciaSPEI;
	
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	
	
	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		arrTransferenciaSPEI=new ArrayList<DetalleInterbancaria>();
		try{
			final JSONArray arrTransferencias=parser.parseNextValueWithArray("transferencias");
			for(int i=0;i<arrTransferencias.length();i++){
				final JSONObject jsonTransferencia=arrTransferencias.getJSONObject(i);
				final DetalleInterbancaria transferenciaSPEI=new DetalleInterbancaria();
				transferenciaSPEI.setNombreBanco(jsonTransferencia.getString("nombreBanco"));
				transferenciaSPEI.setImporte(jsonTransferencia.getString("importe"));
				transferenciaSPEI.setFechaAplicacion(jsonTransferencia.getString("fechaAplicacion"));
				transferenciaSPEI.setIva(jsonTransferencia.getString("IVA"));
				transferenciaSPEI.setCuentaOrigen(jsonTransferencia.getString("cuentaOrigen"));
				transferenciaSPEI.setCuentaDestino(jsonTransferencia.getString("cuentaDestino"));
				transferenciaSPEI.setDescripcion(jsonTransferencia.getString("descripcion"));
				transferenciaSPEI.setTipoServicio(jsonTransferencia.getString("tipoServicio"));
				transferenciaSPEI.setReferencia(jsonTransferencia.getString("referencia"));
				transferenciaSPEI.setClaveRastreo(jsonTransferencia.getString("claveRastreo"));
				transferenciaSPEI.setFolio(jsonTransferencia.getString("Folio"));
				transferenciaSPEI.setEstatusOperacion(jsonTransferencia.getString("estatusOperacion"));
				transferenciaSPEI.setNombreBeneficiario(jsonTransferencia.getString("nombreBeneficiario"));
				transferenciaSPEI.setDetalleDevolicion(jsonTransferencia.getString("DetalleDevolucion"));
				transferenciaSPEI.setHoraDevolucion(jsonTransferencia.getString("horaDevolucion"));
				transferenciaSPEI.setHoraAcuse(jsonTransferencia.getString("horaAcuse"));
				transferenciaSPEI.setHoraLiquidacion(jsonTransferencia.getString("horaLiquidacion"));
				transferenciaSPEI.setHoraAprobacion(jsonTransferencia.getString("horaAprobacion"));
				transferenciaSPEI.setHoraAlta(jsonTransferencia.getString("horaAlta"));
				transferenciaSPEI.setFechaDevolucion(jsonTransferencia.getString("fechaDevolucion"));
				arrTransferenciaSPEI.add(transferenciaSPEI);
			}
		}catch(Exception e){
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
		}
	}



	public ArrayList<DetalleInterbancaria> getArrTransferenciaSPEI() {
		return arrTransferenciaSPEI;
	}



	public void setArrTransferenciaSPEI(
			final ArrayList<DetalleInterbancaria> arrTransferenciaSPEI) {
		this.arrTransferenciaSPEI = arrTransferenciaSPEI;
	}

}
