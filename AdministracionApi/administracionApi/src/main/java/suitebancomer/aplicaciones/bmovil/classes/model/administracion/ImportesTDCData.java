package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ImportesTDCData implements ParsingHandler {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mensajeInformativo;
	
	private String fechaPago;
	
	private String fechaCorte;
	
	private String pagoMinimo;
	
	private String pagoNogeneraInt;
	
	private String saldoAlCorte;

	public ImportesTDCData(final String mensajeInformativo, final String fechaPago, final String fechaCorte,
						   final String pagoMinimo, final String pagoNogeneraInt, final String saldoAlCorte) {
		super();
		this.mensajeInformativo = mensajeInformativo;
		this.fechaCorte = fechaCorte;
		this.fechaPago = fechaPago;
		this.pagoMinimo = pagoMinimo;
		this.pagoNogeneraInt = pagoNogeneraInt;
		this.saldoAlCorte = saldoAlCorte;
	}

	public ImportesTDCData() {
		// TODO Auto-generated constructor stubthis.mensajeInformativo = mensajeInformativo;
		this.fechaPago = "";
		this.fechaCorte = "";
		this.pagoMinimo = "";
		this.pagoNogeneraInt = "";
		this.saldoAlCorte = "";
	}

	public String getMensajeInformativo() {
		return mensajeInformativo;
	}

	public void setMensajeInformativo(final String mensajeInformativo) {
		this.mensajeInformativo = mensajeInformativo;
	}

	public String getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(final String fechaPago) {
		this.fechaPago = fechaPago;
	}
	
	public String getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(final String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public String getPagoMinimo() {
		return pagoMinimo;
	}

	public void setPagoMinimo(final String pagoMinimo) {
		this.pagoMinimo = pagoMinimo;
	}

	public String getPagoNogeneraInt() {
		return pagoNogeneraInt;
	}

	public void setPagoNogeneraInt(final String pagoNogeneraInt) {
		this.pagoNogeneraInt = pagoNogeneraInt;
	}

	public String getSaldoAlCorte() {
		return saldoAlCorte;
	}

	public void setSaldoAlCorte(final String saldoAlCorte) {
		this.saldoAlCorte = saldoAlCorte;
	}
	
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		try{
			this.mensajeInformativo = parser.parseNextValue("mensajeInformativo");
			final JSONArray datos = parser.parseNextValueWithArray("datos");
			
			for (int i = 0; i < datos.length(); i++) {
				final JSONObject jsonDatos= datos.getJSONObject(i);
				this.fechaPago = jsonDatos.getString("fechaPago");
				this.fechaCorte = jsonDatos.getString("fechaCorte");
				this.pagoMinimo = jsonDatos.getString("pagoMinimo");
				this.pagoNogeneraInt = jsonDatos.getString("pagoNoGeneraInt");
				this.saldoAlCorte = jsonDatos.getString("saldoAlCorte");
			}
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
		}
		
	}

}
