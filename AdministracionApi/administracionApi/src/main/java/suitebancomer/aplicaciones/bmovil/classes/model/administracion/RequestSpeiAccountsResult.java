package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import android.util.Log;

import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;


public class RequestSpeiAccountsResult implements ParsingHandler {
	/**
	 * The SPEI switch value.
	 */
	private boolean speiSwitch;
	
	/**
	 * The list of SPEI accounts.
	 */
	private ArrayList<SpeiAccount> speiAccounts;
	
	/**
	 * @return The SPEI switch value.
	 */
	public boolean isSpeiSwitch() {
		return speiSwitch;
	}
	
	/**
	 * @return The list of SPEI accounts.
	 */
	public ArrayList<SpeiAccount> getSpeiAccounts() {
		return speiAccounts;
	}
	
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		speiAccounts = null;

	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		speiSwitch = Boolean.parseBoolean(parser.parseNextValue("switch", true));
		JSONArray accountsArray = parser.parseNextValueWithArray("cuentas", true);
		
		speiAccounts = new ArrayList<SpeiAccount>();
		JSONObject currentAccount;
		SpeiAccount newAccount;
		String numeroCliente = Constants.EMPTY_STRING;
		String numeroCuenta = Constants.EMPTY_STRING;
		String telefono = Constants.EMPTY_STRING;
		String codigoCompania = Constants.EMPTY_STRING;
		String descripcionCompania = Constants.EMPTY_STRING;
		String fechaUltimaModificacion = Constants.EMPTY_STRING;
		String indicadorSPEI = Constants.EMPTY_STRING;
		final Account[] sessionAccounts = Session.getInstance(SuiteAppAdmonApi.appContext).getAccounts();
		Account account;
		//código Karen
		String temp1;
		
		numeroCuenta = Constants.EMPTY_STRING;
		for (int i = 0; i < accountsArray.length(); i++) {
			try {
				currentAccount = accountsArray.getJSONObject(i);
				
				numeroCliente = currentAccount.getString("numeroCliente");
				numeroCuenta = currentAccount.getString("numeroCuenta");
				telefono = currentAccount.getString("telefono");
				codigoCompania = currentAccount.getString("codigoCompania");
				descripcionCompania = currentAccount.getString("descripcionCompania");
				fechaUltimaModificacion = currentAccount.getString("fechaUltimaModificacion");
				indicadorSPEI = currentAccount.getString("indicadorSPEI");
			} catch (JSONException e) {
				if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Error while parsing the account at index: " + i, e);
				continue;
			}
			
			account = null;
			//Inicia Código Karen
			temp1 = null;
			for(final Account acc : sessionAccounts) {
				
				temp1 = acc.getNumber();
				
				if (temp1.length()>19) {
	                temp1 = temp1.substring(0,8) + temp1.substring(10); 
	                if(Server.ALLOW_LOG) Log.d("temp", temp1);
				}
				
				//if(acc.getNumber().equals(numeroCuenta))
				if(temp1.equals(numeroCuenta))
					account = acc;
			}
			//Termina Código Karen
			/*for(Account acc : sessionAccounts) {
				if(acc.getNumber().equals(numeroCuenta))
					account = acc;
			}*/
			if(null == account)
				continue;
			
			newAccount = new SpeiAccount(account, numeroCliente, telefono, codigoCompania, descripcionCompania, fechaUltimaModificacion, indicadorSPEI);
			speiAccounts.add(newAccount);
		}
	}

}
