package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.util.Log;

import com.bancomer.mbanking.administracion.BmovilApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambiarPasswordDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioCuentaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioPerfilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioTelefonoDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CampaniaPaperlessDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConfigurarCorreoDeleate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConfigurarMontosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConfirmacionAsignacionSpeiDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConfirmacionAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConfirmacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ContratacionAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ContratacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.DelegateBaseAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.DelegateBaseOperacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MantenimientoSpeiMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MenuAdministrarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MenuSuspenderCancelarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ResultadosAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ResultadosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.proxys.administracion.ConfirmacionAsignacionSpeiServiceProxy;
import suitebancomer.aplicaciones.bmovil.classes.gui.proxys.administracion.ConfirmacionAutenticacionServiceProxy;
import suitebancomer.aplicaciones.bmovil.classes.gui.proxys.administracion.ConfirmacionServiceProxy;
import suitebancomer.aplicaciones.bmovil.classes.gui.proxys.administracion.ContratacionAutenticacionServiceProxy;
import suitebancomer.aplicaciones.bmovil.classes.gui.proxys.administracion.ResultadosAutenticacionServiceProxy;
import suitebancomer.aplicaciones.bmovil.classes.gui.proxys.administracion.ResultadosServiceProxy;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.ConfigurarMontos;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.handlers.ConfirmacionAsignacionSpeiHandler;
import suitebancomer.aplicaciones.resultados.handlers.ConfirmacionAutenticacionHandler;
import suitebancomer.aplicaciones.resultados.handlers.ConfirmacionHandler;
import suitebancomer.aplicaciones.resultados.handlers.ContratacionAutenticacionHandler;
import suitebancomer.aplicaciones.resultados.handlers.ResultadosAutenticacionHandler;
import suitebancomer.aplicaciones.resultados.handlers.ResultadosHandler;
import suitebancomer.classes.gui.controllers.administracion.BaseViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class BmovilViewsController extends BaseViewsController {

	private BmovilApp bmovilApp;
	//AMZ
	public ArrayList<String> estados = new ArrayList<String>();

	/*
	 * @Override public void setCurrentActivity(BaseViewController
	 * currentViewController) { // TODO Auto-generated method stub
	 * super.setCurrentActivity(currentViewController); Log.i("Si", "No"); }
	 */
	public BmovilViewsController(final BmovilApp bmovilApp) {
		super();
		this.bmovilApp = bmovilApp;
	}

	public void cierraViewsController() {
		bmovilApp = null;
		clearDelegateHashMap();
		super.cierraViewsController();
	}

	public void cerrarSesionBackground() {
		SuiteAppAdmonApi.getInstance().getBmovilApplication()
				.setApplicationInBackground(true);
	}

	@Override
	public void showMenuInicial() {
		showMenuPrincipal();
	}

	public void showMenuPrincipal() {
		showMenuPrincipal(false);
	}

	public void showMenuPrincipal(final boolean inverted) {
		SuiteAppAdmonApi.getInstance().getBmovilApplication()
				.setApplicationLogged(true);
		if(SuiteAppAdmonApi.getCallBackSession() != null) {
			if(ServerCommons.ALLOW_LOG) {
				Log.d(getClass().getSimpleName(), "Se ejecuta returnMenuPrincipal desde CallBackSession");
			}
			SuiteAppAdmonApi.getCallBackSession().returnMenuPrincipal();
		} else if(SuiteAppAdmonApi.getCallBackBConnect() != null) {
			if(ServerCommons.ALLOW_LOG) {
				Log.d(getClass().getSimpleName(), "Se ejecuta returnMenuPrincipal desde CallBackBConnect");
			}
			SuiteAppAdmonApi.getCallBackBConnect().returnMenuPrincipal();
		}
	}

	public void showCambioPerfil() {
		CambioPerfilDelegate delegate = (CambioPerfilDelegate) getBaseDelegateForKey(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);
		if (null == delegate) {
			delegate = new CambioPerfilDelegate();
			addDelegateToHashMap(
					CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID, delegate);
		}
		pantallaActivada();
		showViewController(CambioPerfilViewController.class);
		
	}

	public BmovilApp getBmovilApp() {
		return bmovilApp;
	}

	public void setBmovilApp(final BmovilApp bmovilApp) {
		this.bmovilApp = bmovilApp;
	}


	@Override
	public void onUserInteraction() {
		
		if (bmovilApp != null) {

		}
		super.onUserInteraction();
	}

	@Override
	public boolean consumeAccionesDeReinicio() {
		/*if (!SuiteAppAdmonApi.getInstance().getBmovilApplication()
				.isApplicationLogged()) {
			if (SuiteAppAdmonApi.getIntentMenuPrincipal()!=null){
				if(Server.ALLOW_LOG) Log.d("Admon-BmovilViewsCont", "getIntentMenuPrincipal() >>" +
						SuiteAppAdmonApi.getIntentMenuPrincipal().getClass().getSimpleName() );

				SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().showViewController(
						SuiteAppAdmonApi.getIntentMenuPrincipal().getClass());
			}else{
				if(Server.ALLOW_LOG) Log.d("Admon-BmovilViewsCont", "Null object in getIntentMenuPrincipal()");
				showMenuAdministrar();
			}

			return true;
		}*/
		return false;
	}

	@Override
	public boolean consumeAccionesDePausa() {
		/*if (SuiteAppAdmonApi.getInstance().getBmovilApplication().isApplicationLogged()
				&& !isActivityChanging()) {
			cerrarSesionBackground();
			return true;
		}*/
		return false;
	}

	@Override
	public boolean consumeAccionesDeAlto() {
		if (!SuiteAppAdmonApi.getInstance().getBmovilApplication().isChangingActivity()
				&& currentViewControllerApp != null) {
			currentViewControllerApp.hideSoftKeyboard();
		}
		SuiteAppAdmonApi.getInstance().getBmovilApplication()
				.setChangingActivity(false);
		return true;
	}

	/*
	 * Muestra la pantalla de acerca de...
	 */
	public void showAcercaDe() {
		MenuAdministrarDelegate delegate = (MenuAdministrarDelegate) getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuAdministrarDelegate();
			addDelegateToHashMap(
					MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID,
					delegate);
		}
		showViewController(AcercaDeViewController.class);
	}

	/*
	 * Muestra la pantalla de Novedades
	 */
	public void showNovedades() {
		MenuAdministrarDelegate delegate = (MenuAdministrarDelegate) getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuAdministrarDelegate();
			addDelegateToHashMap(
					MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID,
					delegate);
		}
		pantallaActivada();
		showViewController(NovedadesViewController.class);
	}

	/*
	 * muestra la pantalla para cambiar password
	 */
	public void showCambiarPassword() {
		CambiarPasswordDelegate delegate = (CambiarPasswordDelegate) getBaseDelegateForKey(CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID);
		if (delegate == null) {
			delegate = new CambiarPasswordDelegate();
			addDelegateToHashMap(
					CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID,
					delegate);
		}
		pantallaActivada();
		showViewController(CambiarPasswordViewController.class);
	}

	/*
	 * Muestra la pantalla con el resultado del cambio de password
	 */
	public void showCambiarPasswordResultado() {
		CambiarPasswordDelegate delegate = (CambiarPasswordDelegate) getBaseDelegateForKey(CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID);
		if (delegate == null) {
			delegate = new CambiarPasswordDelegate();
			addDelegateToHashMap(
					CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID,
					delegate);
		}
		showViewController(CambiarPasswordResultadoViewController.class);
	}

	/**
	 * Muestra la pantalla de confirmacion autenticacion
	 */
	public void showConfirmacionAutenticacionViewController(
			final DelegateBaseAutenticacion autenticacionDelegate, final int resIcon,
			final int resTitle, final int resSubtitle) {
		showConfirmacionAutenticacionViewController(autenticacionDelegate,
				resIcon, resTitle, resSubtitle, R.color.primer_azul);
	}

	/**
	 * Muestra la pantalla de confirmacion autenticacion
	 */
	public void showConfirmacionAutenticacionViewController(
			final DelegateBaseAutenticacion autenticacionDelegate, final int resIcon,
			final int resTitle, final int resSubtitle, final int resTitleColor) {
		
		ConfirmacionAutenticacionDelegate confirmacionDelegate = (ConfirmacionAutenticacionDelegate) getBaseDelegateForKey(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		if (confirmacionDelegate != null) {
			removeDelegateFromHashMap(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		}
		confirmacionDelegate = new ConfirmacionAutenticacionDelegate(
				autenticacionDelegate);
		addDelegateToHashMap(
				ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID,
				confirmacionDelegate);
		//AMZ
				if(estados.size() == 0)
				{
					//AMZ
					TrackingHelper.trackState("reactivacion", estados);
					//AMZ
				}
				
		//showViewController(ConfirmacionAutenticacionViewController.class);
	    //SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().showViewController(SuiteAppAdmonApi.getIntentConfirmacionAut().getClass());

		// API confirmacion   ConfirmacionAutenticacion
		final Integer idText = confirmacionDelegate.getOperationDelegate().getTextoEncabezado();
		final Integer idNombreImagenEncabezado = confirmacionDelegate.getOperationDelegate().getNombreImagenEncabezado();
		final Boolean statusDesactivado =false;
		final SuiteAppCRApi cr=new SuiteAppCRApi();
		cr.onCreate(SuiteAppAdmonApi.appContext);
		SuiteAppCRApi.setCallBackSession(SuiteAppAdmonApi.getCallBackSession());
		SuiteAppCRApi.setCallBackBConnect(SuiteAppAdmonApi.getCallBackBConnect());
		final ConfirmacionAutenticacionServiceProxy proxy = new ConfirmacionAutenticacionServiceProxy(confirmacionDelegate);
		cr.setProxy(proxy);

		final ConfirmacionAutenticacionHandler handler = new ConfirmacionAutenticacionHandler(proxy, this,new ConfirmacionAutenticacionViewController() );
		handler.setActivityChanging(isActivityChanging());
		handler.setDelegateId(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		handler.invokeActivity(this.estados,idText, idNombreImagenEncabezado, statusDesactivado);

	}
	
	/**
	 * Muestra la pantalla de resultados
	 */
	public void showResultadosViewController(
			final DelegateBaseOperacion delegateBaseOperacion, final int resIcon,
			final int resTitle) {
		ResultadosDelegate resultadosDelegate = (ResultadosDelegate) getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		if (resultadosDelegate != null) {
			removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		}
		resultadosDelegate = new ResultadosDelegate(delegateBaseOperacion);
		addDelegateToHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID,
				resultadosDelegate);
		//showViewController(ResultadosViewController.class);

		// API confirmacion   ResultadosAutenticacion
		final Integer idText = resultadosDelegate.getOperationDelegate().getTextoEncabezado();
		final Integer idNombreImagenEncabezado = resultadosDelegate.getOperationDelegate().getNombreImagenEncabezado();
		final Boolean statusDesactivado =false;
		final SuiteAppCRApi cr=new SuiteAppCRApi();
		cr.onCreate(SuiteAppAdmonApi.appContext);
		SuiteAppCRApi.setCallBackSession(SuiteAppAdmonApi.getCallBackSession());
		SuiteAppCRApi.setCallBackBConnect(SuiteAppAdmonApi.getCallBackBConnect());
		final ResultadosServiceProxy proxy = new ResultadosServiceProxy(resultadosDelegate);
		cr.setProxy(proxy);


		final ResultadosHandler handler = new ResultadosHandler(proxy, this,new ResultadosViewController() );
		handler.setActivityChanging(isActivityChanging());
		handler.setDelegateId(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		handler.invokeActivity(this.estados,idText, idNombreImagenEncabezado);


	}


	/**
	 * Muestra la pantalla de confirmacion para transferencias.
	 * 
	 * @param delegateBaseOperacion
	 *            EL DelegateBaseOperacion que manda a llamar la pantalla de
	 *            confirmaci�n.
	 */
	public void showConfirmacion(final DelegateBaseOperacion delegateBaseOperacion) {
		// El delegado de confirmaci�n se crea siempre, esto ya que el delegado
		// que contiene el internamente cambia segun quien invoque este método.
		ConfirmacionDelegate delegate = (ConfirmacionDelegate) getBaseDelegateForKey(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
		if (delegate != null) {
			removeDelegateFromHashMap(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
		}
		delegate = new ConfirmacionDelegate(delegateBaseOperacion);
		addDelegateToHashMap(
				ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID,
				delegate);
		//showViewController(ConfirmacionViewController.class);

		// API confirmacion   Confirmacion
		final Integer idText = delegate.consultaOperationsDelegate().getTextoEncabezado();
		final Integer idNombreImagenEncabezado = delegate.consultaOperationsDelegate().getNombreImagenEncabezado();
		final Boolean statusDesactivado =false;
		final SuiteAppCRApi cr=new SuiteAppCRApi();
		cr.onCreate(SuiteAppAdmonApi.appContext);
		SuiteAppCRApi.setCallBackSession(SuiteAppAdmonApi.getCallBackSession());
		SuiteAppCRApi.setCallBackBConnect(SuiteAppAdmonApi.getCallBackBConnect());
		final ConfirmacionServiceProxy proxy = new ConfirmacionServiceProxy(delegate);
		cr.setProxy(proxy);

		final ConfirmacionHandler handler = new ConfirmacionHandler(proxy, this,new ConfirmacionViewController() );
		handler.setActivityChanging(isActivityChanging());
		handler.setDelegateId(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
		handler.invokeActivity(this.estados,idText, idNombreImagenEncabezado);

	}

	/*
	 * Muestra la pantalla para alta de frecuente
	 */
	public void showAltaFrecuente(final DelegateBaseOperacion delegateBaseOperacion) {
//		AltaFrecuenteDelegate delegate = (AltaFrecuenteDelegate) getBaseDelegateForKey(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID);
//		if (delegate != null) {
//			removeDelegateFromHashMap(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID);
//		}
//		delegate = new AltaFrecuenteDelegate(delegateBaseOperacion);
//		addDelegateToHashMap(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID,
//				delegate);
//		showViewController(AltaFrecuenteViewController.class);
	}


	@Override
	public void removeDelegateFromHashMap(final long key) {
		// TODO Auto-generated method stub
//		if (key == BorrarDatosDelegate.BORRAR_DATOS_DELEGATE_ID) {
//			if(Server.ALLOW_LOG) Log.d("MES", "MES");
//		}
		super.removeDelegateFromHashMap(key);
	}

	/**
	 * Muestra la pantalla para cambio de telefono asociado
	 */
	public void showCambioDeTelefono() {
		CambioTelefonoDelegate delegate = (CambioTelefonoDelegate) getBaseDelegateForKey(CambioTelefonoDelegate.CAMBIO_TELEFONO_DELEGATE_ID);
		if (delegate == null) {
			delegate = new CambioTelefonoDelegate();
			addDelegateToHashMap(
					CambioTelefonoDelegate.CAMBIO_TELEFONO_DELEGATE_ID,
					delegate);
		}
		pantallaActivada();
		showViewController(CambioTelefonoViewController.class);
	}

	/**
	 * Muestra la pantalla para cambio de cuenta asociado
	 */
	public void showCambioCuentaAsociada() {
		CambioCuentaDelegate delegate = (CambioCuentaDelegate) getBaseDelegateForKey(CambioCuentaDelegate.CAMBIO_CUENTA_DELEGATE_ID);
		if (delegate == null) {
			delegate = new CambioCuentaDelegate();
			addDelegateToHashMap(
					CambioCuentaDelegate.CAMBIO_CUENTA_DELEGATE_ID, delegate);
		}
		pantallaActivada();
		showViewController(CambioCuentaViewController.class);
	}

	/**
	 * Muestra la pantalla del menu Suspender / Cancelar
	 */
	public void showMenuSuspenderCancelar() {
		MenuSuspenderCancelarDelegate delegate = (MenuSuspenderCancelarDelegate) getBaseDelegateForKey(MenuSuspenderCancelarDelegate.MENU_SUSPENDER_CANCELAR_ID);
		if (delegate == null) {
			delegate = new MenuSuspenderCancelarDelegate();
			addDelegateToHashMap(
					MenuSuspenderCancelarDelegate.MENU_SUSPENDER_CANCELAR_ID,
					delegate);
		}
		pantallaActivada();
		showViewController(MenuSuspenderCancelarViewController.class);
	}


	/**
	 * Muestra la pantalla de definir contraseña la contratación.
	 */
	public void showDefinirPassword() {
		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionDelegate();
			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID, delegate);
		}

//		showViewController(ContratacionDefinicionPasswordViewController.class);
	}


	/**
	 * Muestra la pantalla de configurar montos con los limites elegidos por el
	 * usuario.
	 */
	public void showConfigurarMontos(final ConfigurarMontos cm) {
		ConfigurarMontosDelegate delegate = (ConfigurarMontosDelegate) getBaseDelegateForKey(ConfigurarMontosDelegate.CONFIGURAR_MONTOS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new ConfigurarMontosDelegate();
			addDelegateToHashMap(
					ConfigurarMontosDelegate.CONFIGURAR_MONTOS_DELEGATE_ID,
					delegate);
		}
		delegate.setConfigurarMontos(cm);
		showViewController(ConfigurarMontosViewController.class);
	}

	/**
	 * Muestra la pantalla de resultados
	 */
	public void showResultadosAutenticacionViewController(
			final DelegateBaseOperacion delegateBaseOperacion, final int resIcon,
			final int resTitle) {
		//ResultadosDelegate.RESULTADOS_DELEGATE_ID
		ResultadosAutenticacionDelegate delegate = (ResultadosAutenticacionDelegate)
				getBaseDelegateForKey(ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID);
		if (delegate != null) {
			//removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
			removeDelegateFromHashMap(ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID);
		}
		delegate = new ResultadosAutenticacionDelegate(delegateBaseOperacion);
		addDelegateToHashMap(
				ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID,
				delegate);
		//showViewController(ResultadosAutenticacionViewController.class);

		// API confirmacion   ResultadosAutenticacion
		final Integer idText = delegate.getOperationDelegate().getTextoEncabezado();
		final Integer idNombreImagenEncabezado = delegate.getOperationDelegate().getNombreImagenEncabezado();
		final Boolean statusDesactivado =false;
		final SuiteAppCRApi cr=new SuiteAppCRApi();
		cr.onCreate(SuiteAppAdmonApi.appContext);
		SuiteAppCRApi.setCallBackSession(SuiteAppAdmonApi.getCallBackSession());
		SuiteAppCRApi.setCallBackBConnect(SuiteAppAdmonApi.getCallBackBConnect());
		final ResultadosAutenticacionServiceProxy proxy = new ResultadosAutenticacionServiceProxy(delegate);
		cr.setProxy(proxy);

		final ResultadosAutenticacionHandler handler = new ResultadosAutenticacionHandler(proxy, this,new ResultadosAutenticacionViewController() );
		handler.setActivityChanging(isActivityChanging());
		handler.setDelegateId(ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID);
		handler.invokeActivity(this.estados,idText, idNombreImagenEncabezado, statusDesactivado);

	}


	/**
	 * Muestra la pantalla de confirmaci�n autenticación para el estatus PS.
	 */
	public void showContratacionAutenticacion() {
		final ContratacionDelegate contrataciondelegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
		ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);

		if (delegate != null){
			removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		}
		
		delegate = new ContratacionAutenticacionDelegate(contrataciondelegate);
		addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID, delegate);
		//showViewController(ContratacionAutenticacionViewController.class);

		// API confirmacion   ContratacionAutenticacion
		final Integer idText = delegate.consultaOperationsDelegate().getTextoEncabezado();
		final Integer idNombreImagenEncabezado = delegate.consultaOperationsDelegate().getNombreImagenEncabezado();
		final Boolean statusDesactivado =false;
		final SuiteAppCRApi cr=new SuiteAppCRApi();
		cr.onCreate(SuiteAppAdmonApi.appContext);
		SuiteAppCRApi.setCallBackSession(SuiteAppAdmonApi.getCallBackSession());
		SuiteAppCRApi.setCallBackBConnect(SuiteAppAdmonApi.getCallBackBConnect());
		final ContratacionAutenticacionServiceProxy proxy = new ContratacionAutenticacionServiceProxy(delegate);
		cr.setProxy(proxy);

		final ContratacionAutenticacionHandler handler = new ContratacionAutenticacionHandler(proxy, this,new ContratacionAutenticacionViewController() );
		handler.setActivityChanging(isActivityChanging());
		handler.setDelegateId(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		handler.invokeActivity(this.estados,idText, idNombreImagenEncabezado, statusDesactivado);

	}

	/**
	 * Muestra la pantalla de confirmacion autenticacion
	 */
	public void showContratacionAutenticacion(final DelegateBaseAutenticacion autenticacionDelegate) {
		ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		
		if (delegate != null){
			removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		}
		
		delegate = new ContratacionAutenticacionDelegate(autenticacionDelegate);
		addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID, delegate);
		//showViewController(ContratacionAutenticacionViewController.class);

		// API confirmacion   ContratacionAutenticacion
		final Integer idText = delegate.consultaOperationsDelegate().getTextoEncabezado();
		final Integer idNombreImagenEncabezado = delegate.consultaOperationsDelegate().getNombreImagenEncabezado();
		final Boolean statusDesactivado =false;
		final SuiteAppCRApi cr=new SuiteAppCRApi();
		cr.onCreate(SuiteAppAdmonApi.appContext);
		SuiteAppCRApi.setCallBackSession(SuiteAppAdmonApi.getCallBackSession());
		SuiteAppCRApi.setCallBackBConnect(SuiteAppAdmonApi.getCallBackBConnect());
		final ContratacionAutenticacionServiceProxy proxy = new ContratacionAutenticacionServiceProxy(delegate);
		cr.setProxy(proxy);

		final ContratacionAutenticacionHandler handler = new ContratacionAutenticacionHandler(proxy, this,new ContratacionAutenticacionViewController() );
		handler.setActivityChanging(isActivityChanging());
		handler.setDelegateId(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		handler.invokeActivity(this.estados,idText, idNombreImagenEncabezado, statusDesactivado);

	}

	/**
	 * Muestra los terminos y condiciones de uso.
	 * 
	 * @param terminosDeUso
	 *            Terminos de uso.
	 */
	public void showTerminosDeUso(final String terminosDeUso) {
		showViewController(TerminosCondicionesViewController.class, 0, false,
				new String[] { Constants.TERMINOS_DE_USO_EXTRA },
				new Object[] { terminosDeUso });
	}

	/**
	 * Muestra la pantalla de configurrar correo.
	 */
	public void showConfigurarCorreo() {
		ConfigurarCorreoDeleate delegate = (ConfigurarCorreoDeleate) getBaseDelegateForKey(ConfigurarCorreoDeleate.CONFIGURAR_CORREO_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ConfigurarCorreoDeleate();
			addDelegateToHashMap(
					ConfigurarCorreoDeleate.CONFIGURAR_CORREO_DELEGATE_ID,
					delegate);
		}
		pantallaActivada();
		showViewController(ConfigurarCorreoViewController.class);
	}
	
	//SPEI

		/**
		 * Shows the account association screen.
		 */
		public void showAsociacionCuentaTelefono() {
			MantenimientoSpeiMovilDelegate delegate = (MantenimientoSpeiMovilDelegate)getBaseDelegateForKey(MantenimientoSpeiMovilDelegate.DELEGATE_ID);
			if(null == delegate) {
				delegate = new MantenimientoSpeiMovilDelegate();
				addDelegateToHashMap(MantenimientoSpeiMovilDelegate.DELEGATE_ID, delegate);
			}
			showViewController(ConsultaCuentaSpeiMovilViewController.class);
		}
		
		/**
		 * Shows the screen to associate an account with a phone.
		 */
		public void showDetailAsociacionCuentaTelefono() {
			MantenimientoSpeiMovilDelegate delegate = (MantenimientoSpeiMovilDelegate)getBaseDelegateForKey(MantenimientoSpeiMovilDelegate.DELEGATE_ID);
			if(null == delegate) {
				delegate = new MantenimientoSpeiMovilDelegate();
				addDelegateToHashMap(MantenimientoSpeiMovilDelegate.DELEGATE_ID, delegate);
			}
			showViewController(DetalleCuentaSpeiMovilViewController.class);
		}

	
	//Termina SPEI

	/**
	 * 
	 * @param delegateOp
	 */
	public void showRegistrarOperacion(final DelegateBaseAutenticacion delegateOp) {
		//SPEI
		showRegistrarOperacion(delegateOp, false);
	}
	public void showRegistrarOperacion(final DelegateBaseAutenticacion delegateOp, final boolean showHelpImage){
		//Termina SPEI
		
		/*AYMB
		 * RegistrarOperacionDelegate delegate = (RegistrarOperacionDelegate) getBaseDelegateForKey(RegistrarOperacionDelegate.REGISTRAR_OPERACION_DELEGATE_ID);
		if (delegate != null)
			removeDelegateFromHashMap(RegistrarOperacionDelegate.REGISTRAR_OPERACION_DELEGATE_ID);
		delegate = new RegistrarOperacionDelegate(delegateOp);
		addDelegateToHashMap(
				RegistrarOperacionDelegate.REGISTRAR_OPERACION_DELEGATE_ID,
				delegate);
		//SPEI
		
		String[] extrasKeys = { "showHelpImage" };
		Object[] extrasValues = { Boolean.valueOf(showHelpImage) };
		showViewController(RegistrarOperacionViewController.class, 0, false, extrasKeys, extrasValues);*/
	}
	
	
	/**
	 * Shows the terms and conditions screen.
	 * @param termsHtml Terms and conditions HTML.
	 * @param title The title resource identifier.
	 * @param icon The icon recource identifier.
	 */
	public void showTermsAndConditions(final String termsHtml, final int title, final int icon) {
		final String[] keys = {Constants.TERMINOS_DE_USO_EXTRA, Constants.TITLE_EXTRA, Constants.ICON_EXTRA};
		final Object[] values = {termsHtml, Integer.valueOf(title), Integer.valueOf(icon)};
		
		showViewController(TerminosCondicionesViewController.class, 0, false, keys, values);
	}

	public void touchMenu(){
		TrackingHelper.touchMenu();
		/*String eliminado="";
		if (estados.size() == 2){
			eliminado = estados.remove(1);
		}else{
			final int tam = estados.size();
		for (int i=1;i<tam;i++){
			eliminado = estados.remove(1);
		}
		}*/
	}
	
	public void touchAtras()
	{
		TrackingHelper.touchAtrasState();
		/*
		final int ultimo = estados.size()-1;
		String eliminado;
		if(ultimo >= 0)
		{
			final String ult = estados.get(ultimo);
			if(ult == "reactivacion" || ult == "activacion" || ult == "menu token" || ult == "contratacion datos" || ult == "activacion datos")
			{
				estados.clear();
			}else
			{
				 eliminado = estados.remove(ultimo);
			}
		} */
	}

	/**
	 * Shows the confirmation screen for the SPEI maintenance operations. 
	 * @param operationDelegate The delegate of the current operation. 
	 */
	public void showSpeiConfirmation(final DelegateBaseAutenticacion operationDelegate, final boolean showHelpImage) {
		ConfirmacionAsignacionSpeiDelegate confirmDelegate;
		confirmDelegate = (ConfirmacionAsignacionSpeiDelegate)getBaseDelegateForKey(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID);
		if(confirmDelegate != null)
			removeDelegateFromHashMap(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID);
		confirmDelegate = new ConfirmacionAsignacionSpeiDelegate(operationDelegate);
		
		addDelegateToHashMap(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID, confirmDelegate);
		
		final String[] keys = new String[] { "showHelpImage" };
		final Object[] values = new Object[] { Boolean.valueOf(showHelpImage) };
		
		//showViewController(ConfirmacionAsignacionSpeiViewController.class, 0, false, keys, values);

		// API confirmacion   ConfirmacionAsignacionSpei

		final Integer idText = confirmDelegate.getOperationDelegate().getTextoEncabezado();
		final Integer idNombreImagenEncabezado = confirmDelegate.getOperationDelegate().getNombreImagenEncabezado();
		final Boolean statusDesactivado =false;
		final SuiteAppCRApi cr=new SuiteAppCRApi();
		cr.onCreate(SuiteAppAdmonApi.appContext);
		SuiteAppCRApi.setCallBackSession(SuiteAppAdmonApi.getCallBackSession());
		SuiteAppCRApi.setCallBackBConnect(SuiteAppAdmonApi.getCallBackBConnect());
		final ConfirmacionAsignacionSpeiServiceProxy proxy = new ConfirmacionAsignacionSpeiServiceProxy(confirmDelegate);
		cr.setProxy(proxy);

		final ConfirmacionAsignacionSpeiHandler handler = new ConfirmacionAsignacionSpeiHandler(proxy, this,new ConfirmacionAsignacionSpeiViewController() );
		handler.setActivityChanging(isActivityChanging());
		handler.setDelegateId(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID);
		handler.invokeActivity(keys,values,this.estados,idText, idNombreImagenEncabezado, statusDesactivado);

	}

	public void pantallaActivada() {
		setActivityChanging(true);
	}


	/**
	 * Muestra la pantalla del confirmar paperless
	 */

	public void showConfirmarPaperless() {
		CampaniaPaperlessDelegate delegate = (CampaniaPaperlessDelegate) getBaseDelegateForKey(CampaniaPaperlessDelegate.CAMPANIA_PAPERLESS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new CampaniaPaperlessDelegate();
			addDelegateToHashMap(
					CampaniaPaperlessDelegate.CAMPANIA_PAPERLESS_DELEGATE_ID,
					delegate);
		}
		showViewController(CampaniaPaperlessViewController.class);
	}


}