package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.MenuSuspenderCancelarViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.SuspenderCancelarResult;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.SessionStoredListener;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;



public class MenuSuspenderCancelarDelegate extends DelegateBaseAutenticacion implements SessionStoredListener{

	/**
	 * 
	 */
	public static final long MENU_SUSPENDER_CANCELAR_ID = 9022323031661701077L;

	private boolean suspender;
	//private String folio = null;
	private MenuSuspenderCancelarViewController viewController;
	
	private String username = null;
	private String ium = null;
	private String client = null;
	
	/**
	 * Determina si la operacion es suspender o cancelar
	 * @return true, si la op es suspender; false si la operacion es cancelar
	 */
	public boolean isSuspender() {
		return suspender;
	}
	
	public void setViewController(final MenuSuspenderCancelarViewController viewController) {
		this.viewController = viewController;
	}

	/**
	 * Obtiene la lista de items a mostrar en el componente
	 * ListaSeleccionViewController del MenuSuspenderCancelar 
	 * 
	 * @return menuItems 
	 */
	public ArrayList<Object> getListaMenu(){
		final ArrayList<Object> menuItems = new ArrayList<Object>(2);
		ArrayList<Object> item; 
		item = new ArrayList<Object>(2);
		item.add(Constants.SUSPENDER_OP);
		item.add(viewController.getString(R.string.bmovil_suspender_opcion));
		menuItems.add(item);
		
		item = new ArrayList<Object>(2);
		item.add(Constants.CANCELAR_OP);
		item.add(viewController.getString(R.string.bmovil_cancelar_opcion));
		menuItems.add(item);
		return menuItems;	
		
	}

	/**
	 * Obtiene la opcion seleccionada entre cancelacion y suspension
	 */
	@Override
	public void performAction(final Object obj) {
		if(obj != null && obj instanceof String)
		viewController.opcionSeleccionada((String)obj);
	}
	
	/**
	 * muestra la pantalla de confirmacion para realizar la suspension
	 */
	public void realizaSuspender(){
		suspender = true;
		showConfirmacion();		
	}
	
	/**
	 * muestra la pantalla de confirmacion para realizar la cancelacion
	 */
	public void realizaCancelar(){
		suspender = false;
		showConfirmacion();
	}
	
	/**
	 * @return true si se debe mostrar contrasena, false en caso contrario.
	 */
	@Override
	public boolean mostrarContrasenia() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.suspenderCancelar,
				perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarCVV(Constants.Operacion.suspenderCancelar, perfil);
		return value;
	}
	
	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.suspenderCancelar,
				perfil);
		return value;
	}
	
	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.suspenderCancelar,
									perfil);
		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
			tipoOTP = null;
		}		
		return tipoOTP;
	}
	
	/**
	 * Obtiene el texto para el encabezado de la confirmacion
	 */
	@Override
	public int getTextoEncabezado() {
		int textoEncabezado = 0;
		if(suspender)
			textoEncabezado = R.string.bmovil_suspender_title;
		else 
			textoEncabezado = R.string.bmovil_cancelar_title;
		return textoEncabezado;
	}
	
	/**
	 * Obtiene la imagen para mostrar encabezado
	 */
	@Override
	public int getNombreImagenEncabezado() {
		int imgEncabezado = 0;
		if(suspender)
			imgEncabezado = R.drawable.bmovil_suspender_icono;
		else 
			imgEncabezado = R.drawable.bmovil_cancelar_icono;
		return imgEncabezado;
	}
	 
	/**
	 * Muestra la pantalla de confirmacion
	 */
	void showConfirmacion(){
		//BmovilViewsController bmovilParentController = ((BmovilViewsController) viewController.getParentViewsController());
		final BmovilViewsController bmovilParentController = getParentBmovilViewsController();
		bmovilParentController.showConfirmacionAutenticacionViewController(
				this,
				getNombreImagenEncabezado(), 
				getTextoEncabezado(),
				R.string.confirmation_subtitulo);
	}

	/**
	 * Invoka la conexion al server para suspender o cancelar el servicio
	 * 
	 */
	@Override
	public void realizaOperacion(final ConfirmacionAutenticacionViewController confirmacionAut,final String contrasenia, final String nip, final String token, final String cvv, final String campoTarjeta) {
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		final Autenticacion aut = Autenticacion.getInstance();
		final Perfil perfil = session.getClientProfile();

		final String cadAutenticacion =  aut.getCadenaAutenticacion(Constants.Operacion.suspenderCancelar, perfil);
		final String companiaCelular = session.getCompaniaUsuario();

		final int operationId = Server.SUSPENDER_CANCELAR;
		final Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		params.put(ServerConstants.CVE_ACCESO, contrasenia == null ? "" : contrasenia);
		params.put("companiaCelular", companiaCelular);
		params.put("idOperacion",suspender ? "S" : "C");			
		params.put(Server.J_NIP, nip == null ? "" : nip);
		params.put(Server.J_CVV2, cvv== null ? "" : cvv);
		params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
		params.put(Server.J_AUT, cadAutenticacion);	
		params.put("tarjeta5Dig", campoTarjeta == null ? "" : campoTarjeta);
		
		params.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
		final List<String> listaEncriptar = Arrays.asList(ServerConstants.CVE_ACCESO,
				Server.J_NIP, Server.J_CVV2);
		Encripcion.setContext(SuiteAppAdmonApi.appContext);
		Encripcion.encriptaCadenaAutenticacion(params, listaEncriptar);
		
		//JAIG
		doNetworkOperation(operationId, params,true,new SuspenderCancelarResult(), confirmacionAut);	
	}
	
	@Override
	public void doNetworkOperation(final int operationId,	final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
		//((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler, caller);
		getParentBmovilViewsController().getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler, caller);
	}
	
	
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
			//suspender_cancelar exitoso
			if(response.getResponse() instanceof SuspenderCancelarResult){
				//SuspenderCancelarResult result = (SuspenderCancelarResult)response.getResponse();
				//folio = result.getFolio();
				showResultados();
			}
		}else if(response.getStatus() == ServerResponse.OPERATION_WARNING){
			//suspender_cancelar no exitoso
			//BaseViewControllerCommons current = ((BmovilViewsController)viewController.getParentViewsController()).getCurrentViewControllerApp();
			final BaseViewControllerCommons current = getParentBmovilViewsController().getCurrentViewControllerApp();
			current.showInformationAlert(response.getMessageText());
		}		
	}
	
	private  void showResultados(){
		int message = 0;
		if(suspender)
			message = R.string.bmovil_suspender_alert;
		else
			message = R.string.bmovil_cancelar_alert;
		
		//((BmovilViewsController)viewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(message, new OnClickListener() {
		getParentBmovilViewsController().getCurrentViewControllerApp().showInformationAlert(message, new OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.dismiss();
				//si es cancelacion ... borrar datos.
				if(suspender){
					if(SuiteAppAdmonApi.getCallBackSession() != null) {
						SuiteAppAdmonApi.getCallBackSession().cierraSesion();
					}
				}else{
					saveTempData();
					borrarDatos();
				}
			}
		});
	}

	private void saveTempData(){
		final SuiteAppAdmonApi suiteApp = SuiteAppAdmonApi.getInstance();
		final Session session = Session.getInstance(suiteApp.appContext);
		username = session.getUsername();
		ium = session.getIum();
		client = session.getClientNumber();		
	}
	
	
	/**
	 * Elimina la info despues de realizar el cambio de telefono
 	   La informaci�n que debe ser borrada del teléfono es la siguiente:
		1.	Deber� eliminarse toda la informaci�n almacenada en el archivo DatosBmovil del teléfono, que contiene 
		a.	Usuario
		b.	Compa��a
		c.	fecha mensaje perfil
		d.	iteraciones
		e.	Todas las versiones de catálogos
		f.	Todos los catálogos
		2.	Deber� setear a Nulo esta misma informaci�n en el objeto sesión.
		3.	Deber� cambiarse a false la propiedad bmovilActivado (bmovilActivado=false) del archivo EstatusAplicaciones
	 */
	private void borrarDatos(){
		final Context appContext = SuiteAppAdmonApi.appContext;
		final Session session = Session.getInstance(appContext);
		final String title = "borrando datos";
		//String msg = viewController.getString(R.string.alert_StoreSession);
		final String msg = SuiteAppAdmonApi.appContext.getString(R.string.alert_StoreSession);
		//viewController.muestraIndicadorActividad(title, msg);
		getParentBmovilViewsController().getCurrentViewControllerApp().muestraIndicadorActividad(title, msg);
		session.clearSession();
		session.setApplicationActivated(false);
		session.storeSession(this);
		session.setUsername(username);
	}
	
	public void sessionStored() {
		viewController.ocultaIndicadorActividad();
		viewController.runOnUiThread(new Runnable(){
			@Override
			public void run() {
				//cerrar sesion
				if(SuiteAppAdmonApi.getCallBackSession() != null) {
					SuiteApp.contextoActual = SuiteApp.appContext;
					SuiteAppAdmonApi.getCallBackSession().closeSession(username, ium, client);
				}
			}
		});
	}
	
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {

		final ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;		
		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_quitarsuspension_tabla_operacion));
		final String op = viewController.getString(suspender ? R.string.bmovil_suspender_op:R.string.bmovil_cancelar_op);
		fila.add(op);
		tabla.add(fila);
		
		return tabla;
	}
	
	@Override
	public boolean mostrarCampoTarjeta() {
		return (mostrarCVV() || mostrarNIP());
	}

}
