package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CampaniaPaperlessDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MenuPrincipalDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.PopUpPaperlessDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.TextoPaperless;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.common.administracion.ScaleTool;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

//import suitebancomer.classes.common.ScaleTool;

public class PopUpPaperlessController extends BaseViewController {
	
	private ImageView logo;
	
	private RelativeLayout mainLayout;
	
	private BmovilViewsController parentManager;
	private CampaniaPaperlessDelegate delegateCP;
	// #region Variables.
	/**
	 * Delegado asociado con el controlador.
	 */
	private PopUpPaperlessDelegate ppDelegate;
	private PopUpPaperlessController ppView;
	
	private TextView tcTitulo;
	private TextView tcSubtitulo;
	private TextView tcDescripcion;
	private TextView tcFraseDelDia;
	private TextView tcPieDeMensaje;
	
	/**
	 * Boton Rechazar.
	 */
	private ImageButton btnRechazar;
	
	/**
	 * Boton Aceptar.
	 */
	private ImageButton btnAceptar;
	private Session session;
	
	public PopUpPaperlessController() {
		super();
		ppDelegate = null;
	}

	
	// #region Ciclo de vida.
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SuiteApp.appContext = this;
		setContentView(R.layout.layout_paperless_pop_up);
		//AMZ
		parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("TetxoPaperles",parentManager.estados);
		//TrackingHelper.trackState("TetxoPaperles");

		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate((PopUpPaperlessDelegate)parentViewsController.getBaseDelegateForKey(PopUpPaperlessDelegate.TEXTO_PAPERLESS_ID));
		ppDelegate = (PopUpPaperlessDelegate)getDelegate(); 
		ppDelegate.setOwnerController(this);
		
		
		
		init();
	}

	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		
	}
	// #endregion
	
	// #region Configurar pantalla.
	/**
	 * Inicializa la pantalla.
	 */
	private void init() {
		findViews();
		setTextoCampanaPaperless();
		
		scaleForCurrentScreen();
		scale();
		btnAceptar.setOnClickListener(clickListener);
		btnRechazar.setOnClickListener(clickListener);
	}
	
	private void setTextoCampanaPaperless() {
		final TextoPaperless texto = ppDelegate.getTextopaperless();
		
		tcTitulo.setText(texto.getTcTitulo());
		tcSubtitulo.setText(texto.getTcSubtitulo());
		tcDescripcion.setText(texto.getTcDescripcion());
		tcFraseDelDia.setText(texto.getTcFraseDelDia());
		tcPieDeMensaje.setText(texto.getTcPieDeMensaje());
		
	}

	OnClickListener clickListener=new OnClickListener() {

		@Override
		public void onClick(final View v) {
			// TODO Auto-generated method stub
			if(v==btnAceptar){
				session = Session.getInstance(SuiteAppAdmonApi.appContext);
				if (Server.ALLOW_LOG) Log.w("getSecurityInstrument-->", session.getSecurityInstrument());
				if (Server.ALLOW_LOG) Log.w("getEstatusIS-->", session.getEstatusIS());
				if(((session.getSecurityInstrument().equals(Constants.TYPE_SOFTOKEN.S1.value) || session.getSecurityInstrument().equals(Constants.IS_TYPE_DP270) || session.getSecurityInstrument().equals(Constants.IS_TYPE_OCRA)) && (session.getEstatusIS().equals(Constants.STATUS_APP_ACTIVE)))){
					parentManager.showConfirmarPaperless();
				}else{
					muestraAlertClienteSinToken();					
				}				
				//parentManager.showConfirmarPaperless();
			}else if(v==btnRechazar){
				ppDelegate.rechazoPaperless(); /**LLAMADA NACAR**/
				//ppDelegate.guardaNoAceptacion(); //
				parentManager.setActivityChanging(true);
				final MenuPrincipalDelegate delegate = (MenuPrincipalDelegate) parentViewsController.getBaseDelegateForKey(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID);
				delegate.restoreMenuPrincipal();
	//			finish();
/*
		    	parentViewsController.setActivityChanging(true);
		    	finish();
		    	parentViewsController.overrideScreenBackTransition();*/				
			}
		}
	};


	/**
	 * Busca la referencia a todas las subvistas necesarias.
	 */
	private void findViews() {
		tcTitulo = (TextView) findViewById(R.id.tcTitulo);
		tcSubtitulo = (TextView) findViewById(R.id.tcSubtitulo);
		tcDescripcion = (TextView) findViewById(R.id.tcDescripcion);
		tcFraseDelDia = (TextView) findViewById(R.id.tcFraseDelDia);
		tcPieDeMensaje = (TextView) findViewById(R.id.tcPieDeMensaje);
		
		btnRechazar = (ImageButton) findViewById(R.id.imgBtnRechazar);
		btnAceptar = (ImageButton) findViewById(R.id.imgBtnAceptar);
		logo = (ImageView) findViewById(R.id.imgLogo);
		mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
	}
	
	private void scale() {
		final ScaleTool scaleTool = new ScaleTool(640, 640, getBaseContext());
		scaleTool.fixScaleHeight(logo, 178);
		scaleTool.fixScaleWidth(logo, 128);
		
		scaleTool.fixScaleWidth(mainLayout, 600);
		scaleTool.fixScaleHeight(mainLayout, 750);
	}
	
	/**
	 * Escala las subvistas para la pantalla actual.
	 */
	private void scaleForCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		

		guiTools.scale(findViewById(R.layout.layout_paperless_pop_up));
		guiTools.scale(tcTitulo);
		guiTools.scale(tcSubtitulo);
		guiTools.scale(tcDescripcion);
		guiTools.scale(tcFraseDelDia);
		guiTools.scale(tcPieDeMensaje);
	}
	
	
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		ppDelegate.analyzeResponse(operationId, response);
	}
	
	public void muestraAlertClienteSinToken(){
		final String clientProfile = String.valueOf(session.getClientProfile());
		if (Server.ALLOW_LOG) Log.w("clientProfile-->", clientProfile);
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(PopUpPaperlessController.this);
	    alertDialog.setTitle(R.string.common_confirmation);	     	
	    	
	    if(clientProfile.equals(Constants.AU_TAG_BASICO) || clientProfile.equals(Constants.AU_TAG_AVANZADO))
	    	alertDialog.setMessage(getResources().getString(R.string.popup_paperless_alert_client_basic_advanced));
	    else
	    	alertDialog.setMessage(getResources().getString(R.string.popup_paperless_alert_client_recortado));
	    	
	    alertDialog.setPositiveButton(R.string.common_cancel, null);
		alertDialog.setNegativeButton(R.string.common_accept, new DialogInterface.OnClickListener() {
			public void onClick(final DialogInterface dialog, final int which) {

				//MenuSuiteDelegate delegate = (MenuSuiteDelegate)Session.takeViewToken;

				if (SuiteAppAdmonApi.getInstance().getSofttokenStatus()) {
					
					// /viewsController.showPantallaGeneraOTPST(false);
				} else {
					if (existePendienteDescarga()) {
						//delegate.showActivacionSTEA12();
						
					} else {
						//delegate.showActivacionSTEP();
						
					}
				}
			}
		});

		alertDialog.show();
	}

	private boolean existePendienteDescarga(){
		boolean respuesta = false;
		final SofttokenActivationBackupManager manager = SofttokenActivationBackupManager
				.getCurrent();
		if (manager.existsABackup()) {
			respuesta = true;
		}
		return respuesta;
	}
}