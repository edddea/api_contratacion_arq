package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MantenimientoSpeiMovilDelegate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.ListaSeleccionViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class ConsultaCuentaSpeiMovilViewController extends BaseViewController {
	//#region Class fields.
	/**
	 * The delegate for this screen.
	 */
	private MantenimientoSpeiMovilDelegate ownDelegate; 
	
	/**
	 * The linear layout for hold the accounts list.
	 */
	private LinearLayout accountsListLayout;
	
	/**
	 * The ListaSeleccion component to show the accounts list.
	 */
	private ListaSeleccionViewController accountsList;
	
	/**
	 * The edit button.
	 */
	private Button editButton;
	
	/**
	 * The delete button.
	 */
	private Button deleteButton;
	
	
	//Etiquetado Ale
	//AMZ Ale
	private BmovilViewsController parentManager;
	//#endregion 
	
	//#region Life-cycle management.
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppAdmonApi.getResourceId("layout_bmovil_asociacion_cuenta_telefono_admon", "layout"));
		setTitle(R.string.bmovil_asociar_cuenta_telefono_alternative_title, R.drawable.icono_spei);

		SuiteApp.appContext=this;
		
		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate((MantenimientoSpeiMovilDelegate)parentViewsController.getBaseDelegateForKey(MantenimientoSpeiMovilDelegate.DELEGATE_ID));
		ownDelegate = (MantenimientoSpeiMovilDelegate) getDelegate();
		ownDelegate.setOwnerController(this);
		
		findViews();
		scaleForCurrentScreen();
		
//		accountsList = ownDelegate.loadAccountsList();
//		if(null != accountsList)
//			accountsListLayout.addView(accountsList);
//		else
//			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while loading the accounts list.");
	}

	/**
	 * Search the required views for the operation of this screen.
	 */
	private void findViews() {
		accountsListLayout = (LinearLayout)findViewById(SuiteAppAdmonApi.getResourceId("account_list_layout", "id"));
		editButton = (Button)findViewById(SuiteAppAdmonApi.getResourceId("editButton", "id"));
		deleteButton = (Button)findViewById(SuiteAppAdmonApi.getResourceId("deleteButton", "id"));
	}
	
	/**
	 * Scale the shown views to fit the current screen resolution.
	 */
	private void scaleForCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("mainLayout", "id")));
		guiTools.scale(accountsListLayout);
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("helpImage", "id")));
		guiTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("buttonsLayout", "id")));
		guiTools.scale(editButton);
		guiTools.scale(deleteButton);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		ownDelegate.setOwnerController(this);
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		
		ownDelegate.requestSpeiAccounts();
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(MantenimientoSpeiMovilDelegate.DELEGATE_ID);
		super.goBack();
	}
	//#endregion
	
	//#region Network.
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		ownDelegate.analyzeResponse(operationId, response);
	}
	//#endregion
	
	//#region OnClick Listeners
	/**
	 * Behavior when the edit button is clicked.
	 * @param sender The clicked view.
	 */
	public void onEditButtonClick(final View sender) {
		final Object[] selectedData = getSelectedDataFromListaSeleccion();
		ownDelegate.elementSelected(selectedData, true);
	}
	
	/**
	 * Behavior when the delete button is clicked.
	 * @param sender The clicked view.
	 */
	public void onDeleteButtonClick(final View sender) {
		final Object[] selectedData = getSelectedDataFromListaSeleccion();
		ownDelegate.elementSelected(selectedData, false);
	}
	//#endregion
	
	/**
	 * Sets the accounts list element and shows it on the screen.
	 * @param accountsList The ListaSeleccion element with the data loaded.
	 */
	public void setAccountsList(final ListaSeleccionViewController accountsList) {
		this.accountsList = accountsList;
		
		if(null != this.accountsList) {
			accountsListLayout.removeAllViews();
			accountsListLayout.addView(this.accountsList);
			this.accountsList.cargarTabla();
			moverScroll();
		} else {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while loading the accounts list.");
		}
	}
	
	/**
	 * Activa o desactiva el boton de asociar.
	 * @param enabled True para activar el botón, false para desactivarlo.
	 */
	public void setEditButtonEnabled(final boolean enabled) {
		editButton.setEnabled(enabled);
		editButton.setBackgroundResource(enabled ? R.drawable.btn_asociar_activado : R.drawable.btn_asociar_desactivado);
		//Etiquetado Ale
		if(enabled == true ){
		//AMZ Ale
			final Map<String,Object> Paso1OperacionMap = new HashMap<String, Object>();
		//AMZ Ale
		Paso1OperacionMap.put("evento_paso1","event46");
		Paso1OperacionMap.put("&&products","operaciones;admin+Asociar numero");
		Paso1OperacionMap.put("eVar12","paso1:Asociar");
		TrackingHelper.trackPaso1Operacion(Paso1OperacionMap);
		}
			
	}
	
	/**
	 * Activa o desactiva el boton de desacosiar.
	 * @param enabled True para activar el botón, false para desactivarlo.
	 */
	public void setDeleteButtonEnabled(final boolean enabled) {
		deleteButton.setEnabled(enabled);
		deleteButton.setBackgroundResource(enabled ? R.drawable.btn_desasociar_activado : R.drawable.btn_desasociar_desactivado);
		//Etiquetado Ale
				if(enabled == true ){
				//AMZ Ale
					final Map<String,Object> Paso1OperacionMap = new HashMap<String, Object>();
				//AMZ Ale
				Paso1OperacionMap.put("evento_paso1","event46");
				Paso1OperacionMap.put("&&products","operaciones;admin+ Desasociar numero");
				Paso1OperacionMap.put("eVar12","paso1:Desasociar");
				TrackingHelper.trackPaso1Operacion(Paso1OperacionMap);
				}
				
				
		
	}
	
	@Override
	public boolean dispatchTouchEvent(final MotionEvent ev) {
		if(MotionEvent.ACTION_DOWN == ev.getAction())
			accountsList.setMarqueeEnabled(false);
		else if(MotionEvent.ACTION_UP == ev.getAction())
			accountsList.setMarqueeEnabled(true);
		
		return super.dispatchTouchEvent(ev);
	}
	
	/**
	 * Obtains the data from the currently selected element of the ListaSeleccion element.
	 * @return The element data.
	 */
	@SuppressWarnings("unchecked")
	private Object[] getSelectedDataFromListaSeleccion() {
		final int selectedIndex = accountsList.getOpcionSeleccionada();
		if(selectedIndex < 0)
			return null;
		
		Object[] selectedData = null;
		
		try {
			selectedData = (Object[])((ArrayList<Object>)accountsList.getLista().get(selectedIndex)).get(0);
		} catch(Exception ex) {
			selectedData = null;
			if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Error al obtener los datos del elemento seleccionado", ex);
		}
		
		return selectedData;
	}

	/**
	 * Clears the accounts list selection.
	 */
	public void clearAccountsListSelection() {
		if(null == accountsList)
			return;
		
		accountsList.setOpcionSeleccionada(-1);
		accountsList.cargarTabla();
	}
}
