package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConfirmacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConsultarEstatusEnvioEstadodeCuentaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.DelegateBaseOperacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ResultadosDelegate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.ListaDatosViewController;
import suitebancomer.classes.gui.views.administracion.ListaSeleccionViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

//One click

public class ResultadosViewController extends BaseViewController implements View.OnClickListener {
	
	private ResultadosDelegate resultadosDelegate;
	private ConsultarEstatusEnvioEstadodeCuentaDelegate consultarEstatusEnvioEstadodeCuentaDelegate;
	private TextView tituloResultados;
	private TextView textoResultados;
	private LinearLayout aviso;
	private TextView instruccionesResultados;
	private TextView titulotextoEspecialResultados;
	private TextView lblAviso;
	private TextView textoEspecialResultados;
	private ImageButton menuButton;
	//Mejoras Bmovil
	private ImageButton compartirButton;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppAdmonApi.getResourceId("layout_bmovil_resultados_admon", "layout"));
		SuiteApp.appContext = this;
		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate((ResultadosDelegate)getParentViewsController().getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID));
		
		resultadosDelegate = (ResultadosDelegate)getDelegate();
		resultadosDelegate.setResultadosViewController(this);
		
		findViews();
		scaleToScreenSize();
		
		menuButton.setOnClickListener(this);
		//Mejoras Bmovil
		compartirButton.setOnClickListener(this);
		setTitulo();
		resultadosDelegate.consultaDatosLista();
		configuraPantalla();
		moverScroll();
	/*	if (resultadosDelegate.getOperationDelegate() instanceof AltaRetiroSinTarjetaDelegate){
			showInformationAlert(R.string.retiro_sintarjetaresultado_alert);
		}*/
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		parentViewsController.setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
    	if(resultadosDelegate.getmBroadcastReceiver() != null ){
    		unregisterReceiver(resultadosDelegate.getmBroadcastReceiver());
    		resultadosDelegate.setmBroadcastReceiver(null);
    	}
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		//resultados envioec paperless
		if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.bmovil_Consultar_Estatus_EnvioEC_title)
				||getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.confirmacion_paperless_title))
		{
			return false;
		}
		else {
			//resultados envioec paperless
			final MenuInflater inflater = getMenuInflater();
			inflater.inflate(SuiteAppAdmonApi.getResourceId("menu_bmovil_resultados", "menu"), menu);
			//AMZ

			final int opc = parentManager.estados.size() - 1;
			final int opc2 = parentManager.estados.size() - 2;
			if (parentManager.estados.get(opc) == "opciones") {
				final String rem = parentManager.estados.remove(opc);
			} else if (parentManager.estados.get(opc2) == "sms" || parentManager.estados.get(opc2) == "correo"
					|| parentManager.estados.get(opc2) == "alta frecuentes") {
				String rem = parentManager.estados.remove(opc2);
				rem = parentManager.estados.remove(parentManager.estados.size() - 1);
			} else {
				TrackingHelper.trackState("opciones",parentManager.estados);
			}
			return true;
		}
	}
	
	@Override
	public boolean onPrepareOptionsMenu(final Menu menu) {
		final int opcionesMenu = resultadosDelegate.getOpcionesMenuResultados();
		boolean showMenu = false;
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_SMS) != DelegateBaseOperacion.SHOW_MENU_SMS) {
			menu.removeItem(SuiteAppAdmonApi.getResourceId("save_menu_sms_button", "id"));
		} else {
			showMenu = true;
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_EMAIL) != DelegateBaseOperacion.SHOW_MENU_EMAIL) {
			menu.removeItem(SuiteAppAdmonApi.getResourceId("save_menu_email_button", "id"));
		} else {
			showMenu = true;
			//menu.removeItem(R.id.save_menu_email_button);//TODO remover para mostrar menu completo
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_PDF) != DelegateBaseOperacion.SHOW_MENU_PDF) {
			menu.removeItem(SuiteAppAdmonApi.getResourceId("save_menu_pdf_button", "id"));
		} else {
			//showMenu = true;
			menu.removeItem(SuiteAppAdmonApi.getResourceId("save_menu_pdf_button", "id"));//TODO remover para mostrar menu completo
		}
//		resultados envioec paperless
		if (resultadosDelegate.getFrecOpOK() || ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_FRECUENTE) != DelegateBaseOperacion.SHOW_MENU_FRECUENTE)) {
			//if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_FRECUENTE) != DelegateBaseOperacion.SHOW_MENU_FRECUENTE) {
			menu.removeItem(R.id.save_menu_frecuente_button);
		} else {
			showMenu = true;
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_RAPIDA) != DelegateBaseOperacion.SHOW_MENU_RAPIDA) {
			menu.removeItem(SuiteAppAdmonApi.getResourceId("save_menu_rapida_button", "id"));
		} else {
			//showMenu = true;
			menu.removeItem(SuiteAppAdmonApi.getResourceId("save_menu_rapida_button", "id"));//TODO remover para mostrar menu completo
		}
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_BORRAR) != DelegateBaseOperacion.SHOW_MENU_BORRAR) {
			menu.removeItem(SuiteAppAdmonApi.getResourceId("save_menu_borrar_button", "id"));
		} else {
			showMenu = true;
		}
		
		return showMenu;
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		//ARR
		final Map<String,Object> envioConfirmacionMap = new HashMap<String, Object>();
		final int itemId = item.getItemId();
		if (itemId == R.id.save_menu_sms_button) {
			resultadosDelegate.enviaSMS();
			//AMZ
			final int sms = parentManager.estados.size()-1;
			if(parentManager.estados.get(sms)=="resul"){
				String rem = parentManager.estados.remove(sms);
				rem = parentManager.estados.remove(parentManager.estados.size()-1);
			}
			TrackingHelper.trackState("sms",parentManager.estados);
			TrackingHelper.trackState("resul",parentManager.estados);
			if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_miscuentas))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+mis cuentas");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrascuentasbbva))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+otra cuenta bbva bancomer");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_cuentaexpress))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+cuenta express");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrosbancos))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+otros bancos");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_dineromovil))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+dinero movil");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			return true;
		} else if (itemId == R.id.save_menu_email_button) {
			resultadosDelegate.enviaEmail();
			//AMZ
			final int correo = parentManager.estados.size()-1;
			if(parentManager.estados.get(correo)=="resul"){
				String rem = parentManager.estados.remove(correo);
				rem = parentManager.estados.remove(parentManager.estados.size()-1);
			}
			if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_miscuentas))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+mis cuentas");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrascuentasbbva))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+otra cuenta bbva bancomer");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_cuentaexpress))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+cuenta express");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrosbancos))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+otros bancos");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_dineromovil))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+dinero movil");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			return true;
		} else if (itemId == R.id.save_menu_pdf_button) {
			resultadosDelegate.guardaPDF();
			return true;
		} else if (itemId == R.id.save_menu_frecuente_button) {
			resultadosDelegate.guardaFrecuente();
			//AMZ
			final int frec = parentManager.estados.size()-1;
			if(parentManager.estados.get(frec)=="resul"){	
				String rem = parentManager.estados.remove(frec);
				rem = parentManager.estados.remove(parentManager.estados.size()-1);
			}
			return true;
		} else if (itemId == R.id.save_menu_rapida_button) {
			resultadosDelegate.guardaRapido();
			return true;
		} else if (itemId == R.id.save_menu_borrar_button) {
			resultadosDelegate.borraRapido();
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void goBack() {
		//Se supone que el boton de atras no haga nada
	}
	
	public void setTitulo() {
		super.setTitle(resultadosDelegate.getTextoEncabezado(), 
						resultadosDelegate.getNombreImagenEncabezado());
	}

	/**
	 * muestra las cuentas a modificar tratandose de consultaEstatusEnvioEstadodeCuenta.
	 * pantallaresultado envioec
	 */
	@SuppressWarnings("deprecation")
	private void mostrarCuentasResult(){
	    consultarEstatusEnvioEstadodeCuentaDelegate = new ConsultarEstatusEnvioEstadodeCuentaDelegate();

		//Log.w("Alberto", datos.toString());
		//Log.w("Alberto", "" + datos.getClass());
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(findViewById(R.id.resultados_lista_cuentas));

		final LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		final ListaSeleccionViewController listaCuentas = new ListaSeleccionViewController(this, params,parentViewsController);
		//ListaDatosViewController listaCuentas = new ListaDatosViewController(this, params, parentViewsController);
		final ArrayList<Object> datos = consultarEstatusEnvioEstadodeCuentaDelegate.getDatosResult();
		final ArrayList<Object> encabezado = new ArrayList<Object>();
		encabezado.add(null);
	    encabezado.add(getString(R.string.bmovil_Consultar_Estatus_EnvioEC_Cuenta));
	    encabezado.add(getString(R.string.bmovil_Consultar_Estatus_EnvioEC_Envio));
	    encabezado.add(null);
	        /*
	        listaCuentas.setDelegate(consultarEstatusEnvioEstadodeCuentaDelegate);
	        */
	        listaCuentas.setEncabezado(encabezado);
            listaCuentas.setNumeroColumnas(2);
            listaCuentas.setEcFlag(true);
	        listaCuentas.setLista(datos);
	        listaCuentas.setOpcionSeleccionada(-1);
	        listaCuentas.setSeleccionable(false);
	        listaCuentas.setAlturaFija(true);
	        listaCuentas.setNumeroFilas(datos.size());
	        listaCuentas.setExisteFiltro(false);
	        listaCuentas.setClickable(false);
	        listaCuentas.setMarqueeEnabled(false);
			listaCuentas.cargarTabla();

	      //  listaCuentas.setNumeroCeldas(2);

	    //	listaDatos.setTitulo(R.string.confirmation_subtitulo);
	    //  listaCuentas.showLista();

		final LinearLayout layoutCuentas = (LinearLayout)findViewById(R.id.resultados_lista_cuentas);
		layoutCuentas.addView(listaCuentas);
		layoutCuentas.setVisibility(View.VISIBLE);

	}

	public void setListaDatos(final ArrayList<Object> datos) {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(findViewById(SuiteAppAdmonApi.getResourceId("resultados_lista_datos", "id")));

		final LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		final ListaDatosViewController listaDatos = new ListaDatosViewController(this, params, parentViewsController);
		/*if(resultadosDelegate.getOperationDelegate() instanceof AltaRetiroSinTarjetaDelegate){
			ArrayList<String> lista = listaDatos.getTablaDatosAColorear();

			if(lista.contains(getString(R.string.result_retironsintarjeta_importe))){
				lista.remove(getString(R.string.result_retironsintarjeta_importe));
			}


			if(!lista.contains(getString(R.string.bmovil_consultar_retirosintarjeta_detalles_clave_retiro))) lista.add(getString(R.string.bmovil_consultar_retirosintarjeta_detalles_clave_retiro));

			listaDatos.setTablaDatosAColorear(lista);

		}*/
		cambiarColores(listaDatos);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.showLista();
		final LinearLayout layoutListaDatos = (LinearLayout)findViewById(SuiteAppAdmonApi.getResourceId("resultados_lista_datos", "id"));
		layoutListaDatos.addView(listaDatos);
	}

	private void cambiarColores(final ListaDatosViewController listaDatos) {
		/*if(resultadosDelegate.getOperationDelegate() instanceof ConsultaRetiroSinTarjetaDelegate){
			ArrayList<String> lista = listaDatos.getTablaDatosAColorear();

			if(lista.contains(getString(R.string.result_retironsintarjeta_importe))){
				lista.remove(getString(R.string.result_retironsintarjeta_importe));
			}

			if(!lista.contains(getString(R.string.bmovil_consultar_dineromovil_detalles_datos_otp))) lista.add(getString(R.string.bmovil_consultar_dineromovil_detalles_datos_otp));
			if(!lista.contains(getString(R.string.bmovil_consultar_retirosintarjeta_detalles_clave_retiro))) lista.add(getString(R.string.bmovil_consultar_retirosintarjeta_detalles_clave_retiro));

			listaDatos.setTablaDatosAColorear(lista);
		}*/
	}

	public void configuraPantalla() {
		final String titulo = resultadosDelegate.getTextoTituloResultado();
		final String texto = resultadosDelegate.getTextoPantallaResultados();
		final String instrucciones = resultadosDelegate.getTextoAyudaResultados();
		final String tituloTextoEspecial = resultadosDelegate.getTituloTextoEspecialResultados();
		final String textoEspecial = resultadosDelegate.getTextoEspecialResultados();
		
		if (titulo.equals("")) {
			tituloResultados.setVisibility(View.GONE);
		} else {
			tituloResultados.setText(titulo);
			tituloResultados.setTextColor(getResources().getColor(resultadosDelegate.getColorTituloResultado()));
		}
		
		if (texto.equals("")) {
			textoResultados.setVisibility(View.GONE);
		} else {
			textoResultados.setText(texto);
		}
		
		if (instrucciones.equals("")) {
			instruccionesResultados.setVisibility(View.GONE);
		} else {
			instruccionesResultados.setText(instrucciones);
		}
		
		if (tituloTextoEspecial.equals("")) {
			titulotextoEspecialResultados.setVisibility(View.GONE);
		}else {
			titulotextoEspecialResultados.setText(tituloTextoEspecial);
		}
		
		if (textoEspecial.equals("")) {
			textoEspecialResultados.setVisibility(View.GONE);
		} else {
			textoEspecialResultados.setText(textoEspecial);
		}
		
		//Mejoras Bmovil
		if(instruccionesResultados.getText() == ""){
			compartirButton.setVisibility(View.INVISIBLE);
		}
		
		//AMZ 
		parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();
	if(titulo == this.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_operacion_cancelada)){
		TrackingHelper.trackState("cancelada",parentManager.estados);
	}else if(titulo == this.getString(R.string.transferir_detalle_operacion_exitosaTitle)){
		TrackingHelper.trackState("resul",parentManager.estados);
	}else{
		TrackingHelper.trackState("resul",parentManager.estados);

		final LinearLayout layoutConfirmacionAviso = (LinearLayout) findViewById(R.id.seeAviso);
		//pantalla resultados envioec paperless
		/*layoutConfirmacionAviso.measure(MeasureSpec.UNSPECIFIED,
				MeasureSpec.UNSPECIFIED);
		float avisoHeight = layoutConfirmacionAviso.getMeasuredHeight();*/

	}
		//resultados envioec paperless
		if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.consultar_estados_de_cuenta_title))
		{
			aviso.setVisibility(View.VISIBLE);
		}

		if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.bmovil_Consultar_Estatus_EnvioEC_title))
		{
			textoEspecialResultados.setVisibility(View.GONE);
			tituloResultados.setTextColor(getResources().getColor(R.color.paperles_verde));
			aviso.setVisibility(View.VISIBLE);
			lblAviso.setText(R.string.bmovil_Consultar_Estatus_EnvioEC_lblAviso);
			mostrarCuentasResult();
			//btnCompartir.setVisibility(View.VISIBLE);
		}
		if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.confirmacion_paperless_title))
		{
			textoEspecialResultados.setVisibility(View.GONE);
			lblAviso.setText(R.string.resultado_texto_especial);
			super.setTitle(resultadosDelegate.getTextoEncabezado(), resultadosDelegate.getNombreImagenEncabezado(),
					resultadosDelegate.getColorTituloResultado());

		}//resultados envioec paperless







		//One click
	if(!textoEspecial.equals("")&& tituloTextoEspecial.equals("")&& !instrucciones.equals("")){
		final String text= textoEspecial;
		final SpannableString textS= new SpannableString(text);
		/*AbsoluteSizeSpan headerSpan = new AbsoluteSizeSpan(18);
		textS.setSpan(headerSpan, 0, text.length(), 0);*/
		textS.setSpan(new ForegroundColorSpan(Color.rgb(77,77,77)),0,text.length(),0);
		textoEspecialResultados.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
		textoEspecialResultados.setText(textS);
		final String text2= instrucciones;
		final SpannableString textS2= new SpannableString(text2);
		/*AbsoluteSizeSpan headerSpan2 = new AbsoluteSizeSpan(14);
		textS2.setSpan(headerSpan2, 0, text2.length(), 0);*/
		textS2.setSpan(new ForegroundColorSpan(Color.rgb(176,176,176)),0,text2.length(),0);
		instruccionesResultados.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
		instruccionesResultados.setText(textS2);
		titulotextoEspecialResultados.setVisibility(View.GONE);
	}
	
	//Termina one CLlick
		/*
		ViewGroup contenido = (ViewGroup)this.findViewById(android.R.id.content).getRootView();//findViewById(android.R.id.content);
		contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float contentHeight = contenido.getMeasuredHeight();
		LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.resultados_lista_datos);
		layoutListaDatos.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float listaHeight = layoutListaDatos.getMeasuredHeight();
		textoResultados.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float textoHeight = textoResultados.getMeasuredHeight();
		instruccionesResultados.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float instruccionesHeight = instruccionesResultados.getMeasuredHeight();
		textoEspecialResultados.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float textoEspecialHeight = textoEspecialResultados.getMeasuredHeight();
		menuButton.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float buttonHeight = menuButton.getMeasuredHeight();
		
		float margin = getResources().getDimension(R.dimen.confirmacion_fields_initial_margin);
		
		float maximumSize = (contentHeight * 4) / 5;
		System.out.println("Altura maxima " +maximumSize);
		float elementsSize = listaHeight + textoHeight + instruccionesHeight + textoEspecialHeight + buttonHeight;
		System.out.println("Altura mixta " +elementsSize);
		float heightParaValidar = (contentHeight*3)/4;
		System.out.println("heightParaValidar " +contentHeight);
		
		RelativeLayout.LayoutParams botonLayout = (RelativeLayout.LayoutParams)menuButton.getLayoutParams();
		if (elementsSize <= contentHeight) {
			botonLayout.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		} else {
			botonLayout.addRule(RelativeLayout.BELOW, R.id.resultado_texto_especial);
		}
		*/
	}
	
	@Override
	public void onClick(final View v) {
		if (v == menuButton && !parentViewsController.isActivityChanging()) {
			botonMenuClick();
		}else if (v==compartirButton){
			openOptionsMenu();
		}
	}
	
	public void botonMenuClick() {
		//AMZ
				((BmovilViewsController)parentViewsController).touchMenu();

		parentViewsController.removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		parentViewsController.removeDelegateFromHashMap(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
		((BmovilViewsController)parentViewsController).showMenuPrincipal(true);
	}
	
	public BroadcastReceiver createBroadcastReceiver() {
    	
	     return new BroadcastReceiver() {
			@Override
		     public void onReceive(final Context ctx, final Intent intent) {
		    	 
		    	 String toastMessage;
		    	 
			     switch (getResultCode()) {
				     case Activity.RESULT_OK:
					     toastMessage = getString(R.string.sms_success);    
					     break;
				     case SmsManager.RESULT_ERROR_NO_SERVICE: //"SMS: No service"
				    	 toastMessage = getString(R.string.sms_error_noService);
				    	 break;
				     case SmsManager.RESULT_ERROR_NULL_PDU: //"SMS: Null PDU"
				    	 toastMessage = getString(R.string.sms_error_nullPdu);
				    	 break;
				     case SmsManager.RESULT_ERROR_RADIO_OFF: //"SMS: Radio off"
				    	 toastMessage = getString(R.string.sms_error_radioOff);
				    	 break;
				     case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
				     default:
				    	 toastMessage = getString(R.string.sms_error);
				    	 break;
			     }
			     
			     ocultaIndicadorActividad();
			     Toast.makeText(getBaseContext(), toastMessage,Toast.LENGTH_SHORT).show();
		     }
	     };
	}
	
	private void findViews() {
		tituloResultados = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("resultado_titulo", "id"));
		textoResultados = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("resultado_texto", "id"));
		lblAviso = (TextView)findViewById(R.id.lblAviso);//paperless
		aviso = (LinearLayout)findViewById(R.id.seeAviso);//paperless
		instruccionesResultados = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("resultado_instrucciones", "id"));
		textoEspecialResultados = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("resultado_texto_especial", "id"));
		titulotextoEspecialResultados = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("resultado_titulo_texto_especial", "id"));
		menuButton = (ImageButton)findViewById(SuiteAppAdmonApi.getResourceId("resultados_menu_button", "id"));
		//Mejoras Bmovil
		compartirButton = (ImageButton)findViewById(SuiteAppAdmonApi.getResourceId("resultados_compartir", "id"));
		//Aviso resultado campa�a paperless
		if(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado() == R.string.confirmacion_paperless_title){
			aviso.setVisibility(View.VISIBLE);
		}
		else
			aviso.setVisibility(View.GONE);

		//btnCompartir = (ImageView) findViewById(R.id.imgBtnCompartir);
	}
	
	private void scaleToScreenSize() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(tituloResultados, true);
		guiTools.scale(textoResultados, true);
		guiTools.scale(instruccionesResultados, true);
		guiTools.scale(textoEspecialResultados, true);
		guiTools.scale(titulotextoEspecialResultados, true);
		guiTools.scale(menuButton);
	}
	
	public void setListaClave(final ArrayList<Object> datos) {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(findViewById(SuiteAppAdmonApi.getResourceId("resultados_lista_clave", "id")));

		final LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);


		final ListaSeleccionViewController listaSeleccion = new ListaSeleccionViewController(this, params,
					parentViewsController);
			listaSeleccion.setDelegate(resultadosDelegate);
			listaSeleccion.setFijarLista(true);
			listaSeleccion.setNumeroColumnas(1);
			listaSeleccion.setEncabezado(null);
			listaSeleccion.setLista(datos);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(datos.size());
			listaSeleccion.setExisteFiltro(false);
			listaSeleccion.cargarTabla();
		final LinearLayout layoutListaDatos = (LinearLayout)findViewById(SuiteAppAdmonApi.getResourceId("resultados_lista_clave", "id"));
			layoutListaDatos.addView(listaSeleccion);
			
		
			findViewById(SuiteAppAdmonApi.getResourceId("resultados_lista_clave", "id")).setVisibility(View.VISIBLE);
	}
	//One click

	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {

		if(SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController().getBaseDelegateForKey(
				ResultadosDelegate.RESULTADOS_DELEGATE_ID) !=null	){
			resultadosDelegate = (ResultadosDelegate)SuiteAppAdmonApi.getInstance().getBmovilApplication().getViewsController()
					.getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID );
		}

		resultadosDelegate.analyzeResponse(operationId, response);
	}
	//Termina One click
	
}
