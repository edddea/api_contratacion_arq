package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioPerfilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MenuAdministrarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.ConfigurarMontos;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.ListaSeleccionViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

/**
 * @author Francisco.Garcia
 * 
 */
public class MenuAdministrarViewController extends BaseViewController {

	private MenuAdministrarDelegate menuAdministrarDelegate;
	private ListaSeleccionViewController listaSeleccion;
	private LinearLayout vista;
	private boolean isRunningTask;
	//AMZ
	private BmovilViewsController parentManager;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE,
				SuiteAppAdmonApi.getResourceId("layout_bmovil_menu_administrar_admon", "layout"));
		SuiteAppAdmonApi.getInstance()
				.getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(this);
    	SuiteApp.appContext=this;
		setTitle(R.string.administrar_menu_titulo,
				R.drawable.bmovil_administrar_icono);

		//AMZ
		parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("administrar",parentManager.estados);

		setParentViewsController(SuiteAppAdmonApi.getInstance()
				.getBmovilApplication().getBmovilViewsController());

		setDelegate((MenuAdministrarDelegate)parentViewsController
				.getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID));
		menuAdministrarDelegate = (MenuAdministrarDelegate) getDelegate();
		menuAdministrarDelegate.setMenuAdministrarViewController(this);
		vista = (LinearLayout) findViewById(SuiteAppAdmonApi.getResourceId("menu_administrar_layout","id"));
		inicializaMenu();
	}

	@SuppressWarnings("deprecation")
	protected void inicializaMenu() {

		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT);
		GuiTools.getCurrent().init(getWindowManager());
		// params.topMargin =
		// GuiTools.getCurrent().getEquivalenceFromScaledPixels(getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin));
		params.leftMargin = GuiTools.getCurrent()
				.getEquivalenceFromScaledPixels(
						getResources().getDimensionPixelOffset(
								R.dimen.resultados_side_margin));
		params.rightMargin = GuiTools.getCurrent()
				.getEquivalenceFromScaledPixels(
						getResources().getDimensionPixelOffset(
								R.dimen.resultados_side_margin));

		final ArrayList<Object> lista = menuAdministrarDelegate.getDatosMenu();// new
																			// ArrayList<Object>();
		// ArrayList<Object> registros;
		// registros = new ArrayList<Object>(2);
		// registros.add(Constants.MENU_ADMINISTRAR_CAMBIAR_CONTRASENA_OPCION);
		// registros.add(SuiteApp.appContext.getString(R.string.administrar_menu_cambiar_contrasena_titulo));
		// lista.add(registros);
		//
		// registros = new ArrayList<Object>(2);
		// registros.add(Constants.MENU_ADMINISTRAR_ACERCADE_OPCION);
		// registros.add(SuiteApp.appContext.getString(R.string.administrar_acercade_titulo));
		// lista.add(registros);

		if (listaSeleccion == null) {
			listaSeleccion = new ListaSeleccionViewController(this, params,
					parentViewsController);
			listaSeleccion.setDelegate(menuAdministrarDelegate);
			listaSeleccion.setNumeroColumnas(1);
			listaSeleccion.setEncabezado(null);
			listaSeleccion.setLista(lista);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(false);
			listaSeleccion.setNumeroFilas(7);
			listaSeleccion.setExisteFiltro(false);
			listaSeleccion.cargarTabla();
			vista.addView(listaSeleccion);
			vista.setScrollContainer(true);
		}

	}

	/*
	 * Estos de cajon! :|
	 */
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		parentManager.setActivityChanging(true);
		if (this.parentViewsController instanceof SofttokenViewsController) {
			parentViewsController = SuiteAppAdmonApi.getInstance()
					.getBmovilApplication().getBmovilViewsController();
		}

		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		isRunningTask = false;
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentManager.setActivityChanging(true);
		if(!parentManager.isActivityChanging()) {
			parentViewsController.consumeAccionesDePausa();
		}
	}

	@Override
	public void goBack() {
		parentViewsController
				.removeDelegateFromHashMap(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
		super.goBack();
	}

	// ----------------------------------------------
	/***/

	public void opcionSeleccionada(final String opcionSeleccionada) {
		if(Server.ALLOW_LOG) Log.d("MenuAdministrar", "opcion rebotar?" + isRunningTask);
		if (isRunningTask){
			return;
		}

		BmovilViewsController parent = null;

		if (this.parentViewsController instanceof BmovilViewsController) {

			parent = ((BmovilViewsController) this.parentViewsController);
		} else {
			parent = SuiteAppAdmonApi.getInstance().getBmovilApplication()
					.getBmovilViewsController();
		}


		//AMZ
		final Map<String,Object> OperacionMap = new HashMap<String, Object>();
		final Session session = Session.getInstance(SuiteAppAdmonApi.getInstance());
		session.setCmbPerfilHamburguesa(true);
		if(opcionSeleccionada.equals(Constants.MADMINISTRAR_NOVEDADES)) {
			//AMZ
			OperacionMap.put("evento_realizada","event52");
			OperacionMap.put("&&products","operaciones;admin+novedades");
			OperacionMap.put("eVar12","operacion realizada");
			TrackingHelper.trackOperacionRealizada(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a Novedades");
			parent.showNovedades();
		} else if (opcionSeleccionada.equals(Constants.MADMINISTRAR_ACERCADE)) {
			//AMZ
			OperacionMap.put("evento_realizada","event52");
			OperacionMap.put("&&products","operaciones;admin+acerca");
			OperacionMap.put("eVar12","operacion realizada");
			TrackingHelper.trackOperacionRealizada(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a Acerca De");
			parent.showAcercaDe();
		} else if (opcionSeleccionada
				.equals(Constants.MADMINISTRAR_CAMBIAR_CONTRASENA)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+cambia password");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a Cambiar contrasena");
			parent.showCambiarPassword();
		} else if (opcionSeleccionada
				.equals(Constants.MADMINISTRAR_CAMBIO_TELEFONO)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+cambio celular");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a: cambioTelefono");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.cambioTelefono,
					session.getClientProfile())) {
				parent.showCambioDeTelefono();
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
		} else if (opcionSeleccionada
				.equals(Constants.MADMINISTRAR_ACTUALIZAR_CUENTAS)) {
			//AMZ
			OperacionMap.put("evento_realizada","event52");
			OperacionMap.put("&&products","operaciones;admin+actualiza cuentas");
			OperacionMap.put("eVar12","operacion realizada");
			TrackingHelper.trackOperacionRealizada(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a actualizarCuentas");

			actualizarCuentasSelected();

		} else if (opcionSeleccionada
				.equals(Constants.MADMINISTRAR_CAMBIO_CUENTA)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+cambio cuenta");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a cambioCuenta");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.cambioCuenta,
					session.getClientProfile())) {
				parent.showCambioCuentaAsociada();
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
		} else if (opcionSeleccionada
				.equals(Constants.MADMINISTRAR_SUSPENDER_CANCELAR)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+suspender o cancelar");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a suspenderCancelar");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.suspenderCancelar,
					session.getClientProfile())) {
				SuiteAppAdmonApi.getInstance().getBmovilApplication()
				.setApplicationLogged(true);
				parent.showMenuSuspenderCancelar();
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}

		} else if (opcionSeleccionada
				.equals(Constants.MADMINISTRAR_CONFIGURAR_MONTOS)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+configura montos");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a CONFIGURAR_MONTOS");
			if(Constants.Perfil.avanzado.equals(session.getClientProfile())){
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.cambioLimites,
					session.getClientProfile())) {
				consultarLimites();
				isRunningTask = true;
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
			}else{
				if (Autenticacion.getInstance().operarOperacion(
						Constants.Operacion.consultaLimites,
						session.getClientProfile())) {
					consultarLimites();
					isRunningTask = true;
				} else {
					// EA#13 del caso de uso de Menu Administrar
				}
			}
		} else if (opcionSeleccionada
				.equals(Constants.MADMINISTRAR_CONFIGURAR_ALERTAS)) {
			if(Server.ALLOW_LOG) Log.d(getClass().getName(), "Entraste a configurar alertas");
			// parent.showConfigurarAlertas();
		} else if (opcionSeleccionada
				.equals(Constants.MADMINISTRAR_CONFIGURAR_CORREO)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+configura correo");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a suspenderCancelar");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.configurarCorreo,
					session.getClientProfile())) {
				parent.showConfigurarCorreo();
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
		} /*else if (opcionSeleccionada
				.equals(Constants.MADMINISTRAR_OPERAR_SIN_TOKEN)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+operar sin token");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a operarSinToken");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.cambioPerfil,
					session.getClientProfile())) {
				parent.showCambioPerfil();
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
		}*/else if(opcionSeleccionada
				.equals(Constants.MADMINISTRAR_OPERAR_RECORTADO)){
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a operar Recortado");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.cambioPerfil,
					session.getClientProfile())) {
				parent.showCambioPerfil();
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
		} else if (opcionSeleccionada
				.equals(Constants.MADMINISTRAR_OPERAR_CON_TOKEN)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+operar con token");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a operarConToken");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.cambioPerfil,
					session.getClientProfile())) {

				final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext)
						.getClientProfile();

				final CambioPerfilDelegate cambioPerfilDelegate = new CambioPerfilDelegate();
				if (cambioPerfilDelegate.verificarCambioPerfil(perfil)) {
					parent.showCambioPerfil();
				} else {
					cambioPerfilDelegate.setOwnerController(this);
					cambioPerfilDelegate.cambioPerfilSinToken();

				}
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
		} else if (opcionSeleccionada
				.equals(Constants.MADMINISTRAR_CONSULTAR_CONTRATO)) {
			//AMZ
			OperacionMap.put("evento_realizada","event52");
			OperacionMap.put("&&products","operaciones;admin+consulta contrato");
			OperacionMap.put("eVar12","operacion realizada");
			TrackingHelper.trackOperacionRealizada(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a consultarContrato");
			showTerminosDeUso();
		}//SPEI
		  else if (opcionSeleccionada.equals(Constants.MADMINISTRAR_ASOCIAR_CELULAR)) {
			  if(cuentasValidasSPEI()>0){
				  if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Entraste a consultar asociaci'on SPEI");
				  ((BmovilViewsController)parentViewsController).showAsociacionCuentaTelefono();
			  }
			  else
			       alertSinCuentasSPEI();

		 //Termina SPEI
		  }
		else if(opcionSeleccionada
				.equals(Constants.MADMINISTRAR_CONSULTA_ESTATUS_ENVIO_ESTADO_DE_CUENTA)) {

			final Autenticacion aut = Autenticacion.getInstance();
			final Perfil perfil = Session.getInstance(SuiteApp.appContext)
					.getClientProfile();
			Constants.Operacion operacion;
			operacion = Constants.Operacion.actualizarEstatusEnvioEC;
			if(aut.isOperable(operacion, perfil)) {
				//AMZ
				OperacionMap.put("evento_inicio","event45");
				OperacionMap.put("&&products","operaciones;admin+consulta de envio de estado de cuenta");
				OperacionMap.put("eVar12","inicio");
				TrackingHelper.trackInicioOperacion(OperacionMap);
				if (Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a envio de estado de cuenta");

				((BmovilViewsController)parentViewsController).showConsultarEstatusEnvioEstadodeCuenta();

			}else {

				if (Server.ALLOW_LOG) Log.d("hcf", "Alert cambio de perfil");
				showNoYesAlert(R.string.administrar_menu_envio_estados_cuenta, new OnClickListener() {
//				showYesNoAlert(R.string.administrar_menu_envio_estados_cuenta, new OnClickListener() {

					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						cambioPerfil();
					}
				}, new OnClickListener() {

					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						dialog.dismiss();
					}
				} );
			}

		}


	}

	protected void cambioPerfil(){
		final BmovilViewsController parent = ((BmovilViewsController) this.parentViewsController);
		//TODO AMB
		 parent.showCambioPerfil();
	}


	/**
	 * cambio de cuentas
	 */
	private void actualizarCuentasSelected() {
		isRunningTask = true;
		menuAdministrarDelegate.actualizarCuentas();
	}

	/**
	 * mostrar alert de cuentas actualizadas
	 */
	public void muestraCuentasActualizadas() {
		final String title = SuiteAppAdmonApi.appContext
				.getString(R.string.bmovil_actualizar_cuentas_exitoso);
		showInformationAlert(title, " ", new OnClickListener() {

			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.dismiss();
				isRunningTask = false;
			}
		});
	}
	
	public int cuentasValidasSPEI(){
//		Account[] accounts = Session.getInstance(SuiteAppAdmonApi.appContext).accountsSession();
		final Account[] accounts = Session.getInstance(SuiteAppAdmonApi.appContext).getAccounts();
		int accountsTemp = 0;
		    
		  for(final Account acc : accounts)
			{
			 // if(acc.getType().equals(Constants.SAVINGS_TYPE)||acc.getType().equals(Constants.CHECK_TYPE)||acc.getType().equals(Constants.LIBRETON_TYPE)){
				if(acc.getType().equals("AH")||acc.getType().equals("CH")||acc.getType().equals("LI")){

				    accountsTemp+=1;	
				    if(Server.ALLOW_LOG) Log.e("cuentasSPEI",String.valueOf(accountsTemp));
		        }
			}
		return accountsTemp;
		
	}
	
	public void alertSinCuentasSPEI() {
		final String title = SuiteAppAdmonApi.appContext
				.getString(R.string.bmovil_asociar_cuenta_telefono_alternative_title);

		final String message = SuiteAppAdmonApi.appContext
				.getString(R.string.bmovil_asociar_cuenta_telefono_alert);
		showInformationAlert(title, message, new OnClickListener() {

			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.dismiss();
				isRunningTask = false;
			}
		});
	}

	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		menuAdministrarDelegate.analyzeResponse(operationId, response);
		if(Server.ALLOW_LOG) Log.d("MenuAdministar", "processNetworkResponse" + isRunningTask);
	}

	private void consultarLimites() {
		if (isRunningTask)
			return;

		// Get aut.string
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		final Autenticacion aut = Autenticacion.getInstance();
		final Perfil perfil = session.getClientProfile();
		final String cadAutenticacion =  aut.getCadenaAutenticacion(Constants.Operacion.consultaLimites, perfil);
		if(Server.ALLOW_LOG) Log.d(">> CGI", "Cadena de autenticacion >> "+cadAutenticacion);
		if(Server.ALLOW_LOG) Log.d(">> CGI", "Perfil autenticacion >> "+perfil.name());
		if("00000".equals(cadAutenticacion)){
			if(Server.ALLOW_LOG) Log.d(">> CGI", "Go to config montos!");
			menuAdministrarDelegate.conusltarLimitesJumpOp(menuAdministrarDelegate.getMenuAdministrarViewController(), null, null, null, null, null);
		}else{
			if(Server.ALLOW_LOG) Log.d(">> CGI", "Go to confirmacion!");
			menuAdministrarDelegate.showConfirmacion();
		}
		if(Server.ALLOW_LOG) Log.d("MenuAdministar", "consultarLimites" + isRunningTask);
		
		//menuAdministrarDelegate.consultarLimites();
		//if(Server.ALLOW_LOG) Log.d("MenuAdministar", "consultarLimites" + isRunningTask);
	}

	public void showConfigurarMontos(final ConfigurarMontos cm) {
		if(Server.ALLOW_LOG) Log.d("MenuAdministar", "showConfigurarMontos" + isRunningTask);
		final BmovilViewsController parent = ((BmovilViewsController) this.parentViewsController);
		parent.showConfigurarMontos(cm);
	}

	public void showTerminosDeUso() {
		menuAdministrarDelegate.showTerminosDeUso();
	}

	public void showTerminosDeUso(final String msg) {
		if(Server.ALLOW_LOG) Log.d("MenuAdministrar", "showTerminosDeUso" + isRunningTask);
		final BmovilViewsController parent = ((BmovilViewsController) this.parentViewsController);
		parent.showTerminosDeUso(msg);
	}

	public void setRunningTask(final boolean isRunningTask) {
		this.isRunningTask = isRunningTask;
	}

}
