/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.os.Bundle;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MenuAdministrarDelegate;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

/**
 * @author Francisco.Garcia
 *
 */
public class AcercaDeViewController extends BaseViewController {
	
	MenuAdministrarDelegate menuAdministrarDelegate;
	TextView lblVersion;
	TextView lblIdentificador;
	TextView lblTerminal;
	//AMZ
	BmovilViewsController parentManager;
		
	
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, getResources().getIdentifier("layout_bmovil_acerca_de_admon", "layout", getPackageName()));
		SuiteApp.appContext = this;
		setTitle(R.string.administrar_acercade_titulo, R.drawable.bmovil_acercade_icono);
		//AMZ
				parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();						
				TrackingHelper.trackState("acerca",parentManager.estados);
				//TrackingHelper.trackState("acerca");
		
		setParentViewsController(SuiteAppAdmonApi.getInstance()
				.getBmovilApplication().getBmovilViewsController());
		
		setDelegate((MenuAdministrarDelegate)parentViewsController.getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID));
		menuAdministrarDelegate = (MenuAdministrarDelegate)getDelegate(); 
		menuAdministrarDelegate.setAcercaDeViewController(this);
		lblVersion = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("acerca_de_version_value_label", "id"));
		lblIdentificador = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("acerca_de_identificador_value_label", "id"));
		lblTerminal = (TextView)findViewById(SuiteAppAdmonApi.getResourceId("acerca_de_terminal_value_label", "id"));
		showDescripcion();
		configurarPantalla();
	}
	
	private void configurarPantalla(){
		final GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(lblVersion,true);
		gTools.scale(lblIdentificador,true);
		gTools.scale(lblTerminal,true);
		gTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("acerca_de_legal_label", "id")),true);
		gTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("acerca_de_info_sistema_label", "id")),true);		
	}
	
	/*
	 * Estos de cajon! :|  
	 * */
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
		super.goBack();
	}
	
	/**
	 * 
	 */
	private void showDescripcion() {
		// TODO Auto-generated method stub
		menuAdministrarDelegate.inicializaAcercaDe();
		
	}
	
	/**
	 * 
	 */
	public TextView getLblVersion() {
		return lblVersion;
	}

	
	/**
	 * 
	 */
	public TextView getLblIdentificador() {
		return lblIdentificador;
	}
	
	/**
	 * 
	 */
	public TextView getLblTerminal() {
		return lblTerminal;
	}

}
