package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import java.util.Hashtable;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.delegates.administracion.BaseDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Promociones;
//Analytics

public class MenuPrincipalDelegate extends BaseDelegate {
	Promociones promocion;
	/**
	 * El controlador actual de la pantalla.
	 */
	private BaseViewController viewController;
	
	/**
	 * Identificador del delegado.
	 */
	public static final long MENU_PRINCIPAL_DELEGATE_ID = 0xb35981680eb5fe19L;
	
	public MenuPrincipalDelegate(final Promociones promocion){
		this.promocion= promocion;
	}
	
	/**
	 * @return El controlador de la pantalla.
	 */
	public BaseViewController getViewController() {
		return viewController;
	}

	/**
	 * @param viewController El controlador de la pantalla a establecer.
	 */
	public void setViewController(final BaseViewController viewController) {
		this.viewController = viewController;
	}

	
	@Override
	public void doNetworkOperation(final int operationId,	final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
		if( viewController != null)
			((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler, caller,false);
	}


	/* TODO AMB
	public void promocionesselected(View v){
		//ARR
				Map<String, Object> eventoMenu = new HashMap<String, Object>();
		int a=v.getId();
		Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		Promociones[] promociones=session.getPromociones();
		for(int x=0;x< promociones.length;x ++){
			if(a==x){
				promocion= new Promociones();
				promocion= promociones[x];
				break;
			}
		}
		String cveCamp= promocion.getCveCamp().substring(0,4);
		if(cveCamp.equals("0130")){
			//ARR
			eventoMenu.put("evento_menu", "event27");
			eventoMenu.put("eVar27", "detalleIlc");
			realizaOperacionILC(Server.CONSULTA_DETALLE_OFERTA, viewController);
		}else if(cveCamp.equals("0377")){
			//ARR
			eventoMenu.put("evento_menu", "event27");
			eventoMenu.put("eVar27", "detalleEfi");
			realizaOperacionEFI(Server.CONSULTA_DETALLE_OFERTA_EFI, viewController);
		}else if(cveCamp.equals("0060")){			
			realizaOperacionCONSUMO(Server.CONSULTA_DETALLE_OFERTA_CONSUMO, viewController);
		}
	}*/

	public void restoreMenuPrincipal() {
		//((MenuPrincipalViewController)viewController).restoreMenu();
		//TODO ALAN
	}

}
