package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion;

import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.administracion.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.ConfigurarMontosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.ConfigurarMontos;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomer.classes.gui.views.administracion.AmountField;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

import static suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.formatAmountForServer;


public class ConfigurarMontosViewController extends BaseViewController {
	
	private ConfigurarMontosDelegate delegate;
	
	private TextView limOplbl;
	private TextView limDiariolbl;
	private TextView limMensuallbl;
	private TextView textAyuda;
	private AmountField limOptxt ;
	private AmountField limDiariotxt;
	private AmountField limMensualtxt;
	private ImageButton btnContinuar;
	private boolean isRunningTask;
	//AMZ
	private BmovilViewsController parentManager;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, SuiteAppAdmonApi.getResourceId("layout_bmovil_configurar_montos_admon", "layout"));
		SuiteApp.appContext = this;
		setTitle(R.string.bmovil_configurar_montos_title, R.drawable.bmovil_configurarmontos_icono);
		if (Constants.Perfil.avanzado == Session.getInstance(this)
				.getClientProfile()) {
			setTitle(R.string.bmovil_configurar_montos_title,
					R.drawable.bmovil_configurarmontos_icono);
		} else {			
			setTitle(R.string.bmovil_consultar_montos_title,
					R.drawable.bmovil_configurarmontos_icono);
		}
		//AMZ
				parentManager = SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController();						
				TrackingHelper.trackState("confmontos",parentManager.estados);
				//TrackingHelper.trackState("confmontos");

		setParentViewsController(SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate((ConfigurarMontosDelegate)parentViewsController.getBaseDelegateForKey(ConfigurarMontosDelegate.CONFIGURAR_MONTOS_DELEGATE_ID));
		delegate = (ConfigurarMontosDelegate)getDelegate(); 
		delegate.setViewController(this);
		findViews();
		scaleForCurrentScreen();
		inicializarPantalla();
		
		limOptxt.addTextChangedListener(new BmovilTextWatcher(this));
		limMensualtxt.addTextChangedListener(new BmovilTextWatcher(this));
		limDiariotxt.addTextChangedListener(new BmovilTextWatcher(this));
	}

	private void findViews() {
		textAyuda = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("configurar_montos_ayuda", "id"));
		limOplbl = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("configurar_montos_lim_op_lbl", "id"));
		limDiariolbl = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("configurar_montos_lim_diario_lbl", "id"));
		limMensuallbl = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("configurar_montos_lim_mensual_lbl", "id"));
		limOptxt = (AmountField) findViewById(SuiteAppAdmonApi.getResourceId("configurar_montos_lim_op_txt", "id"));
		limDiariotxt = (AmountField) findViewById(SuiteAppAdmonApi.getResourceId("configurar_montos_lim_diario_txt", "id"));
		limMensualtxt = (AmountField) findViewById(SuiteAppAdmonApi.getResourceId("configurar_montos_lim_mensual_txt", "id"));
		btnContinuar = (ImageButton) findViewById(SuiteAppAdmonApi.getResourceId("configurar_montos_boton_continuar", "id"));
		
	}
	
	private void scaleForCurrentScreen(){
		final GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		
		gTools.scale(textAyuda, true);
		
		gTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("configurar_montos_op_lbl", "id")), true);
		gTools.scale(limOplbl, true);
		gTools.scale(limOptxt, true);
		
		gTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("configurar_montos_diario_lbl", "id")), true);
		gTools.scale(limDiariolbl, true);
		gTools.scale(limDiariotxt, true);
		
		gTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("configurar_montos_mensual_lbl", "id")), true);
		gTools.scale(limMensuallbl, true);
		gTools.scale(limMensualtxt, true);
		gTools.scale(findViewById(SuiteAppAdmonApi.getResourceId("configurar_montos_boton_continuar", "id")));
		
	}
	
	/**
	 * Inicializa la pantalla con los valores regresados por el Server
	 */
	private void inicializarPantalla() {
		final InputFilter[] filters ={new InputFilter.LengthFilter(Constants.AMOUNT_LENGTH)};
		limOptxt.setFilters(filters);
		limDiariotxt.setFilters(filters);
		limMensualtxt.setFilters(filters);
		final ConfigurarMontos cm = delegate.getConfigurarMontos();
		
		if (Constants.Perfil.avanzado == Session.getInstance(this).getClientProfile()) {
			limOplbl.setText(getString(R.string.bmovil_configurar_montos_montomax)
					+ " "
					+ Tools.formatAmount(cm.getMontoMaxPorOperacion(), false));
			limDiariolbl
					.setText(getString(R.string.bmovil_configurar_montos_montomax)
							+ " "
							+ Tools.formatAmount(cm.getMontoMaxDiario(), false));
			limMensuallbl
					.setText(getString(R.string.bmovil_configurar_montos_montomax)
							+ " "
							+ Tools.formatAmount(cm.getMontoMaxMensual(), false));
			btnContinuar.setVisibility(View.VISIBLE);
			textAyuda.setText(R.string.bmovil_configurar_montos_ayuda);
		} else {

			limOptxt.setEnabled(false);
			limMensualtxt.setEnabled(false);
			limDiariotxt.setEnabled(false);
			btnContinuar.setVisibility(View.GONE);
			textAyuda.setText(R.string.bmovil_consultar_montos_ayuda);
		}		
		limOptxt.setAmount(cm.getMontoPorOperacion());
		limDiariotxt.setAmount(cm.getMontoDiario());
		limMensualtxt.setAmount(cm.getMontoMensual());		
	}
	
	/**
	 * Handler para la accion de continuar 
	 * @param view
	 */
	public void botonContinuarClick(final View view) {
		if(Server.ALLOW_LOG) Log.d("ConfigurarMontos", "botonContinuarClick:"+isRunningTask);
		if(isRunningTask)
			return;
		isRunningTask = true;
		final ConfigurarMontos cm = delegate.getConfigurarMontos();
		cm.setMontoDiario(formatAmountForServer(limDiariotxt.getAmount()));
		cm.setMontoMensual(formatAmountForServer(limMensualtxt.getAmount()));
		cm.setMontoPorOperacion(formatAmountForServer(limOptxt.getAmount()));
		delegate.setConfigurarMontos(cm);
		if (delegate.validarDatos()){
			//AMZ
			final Map<String,Object> Paso1OperacionMap = new HashMap<String, Object>();
			//AMZ
			Paso1OperacionMap.put("evento_paso1","event46");
			Paso1OperacionMap.put("&&products","operaciones;admin+configura montos");
			Paso1OperacionMap.put("eVar12","paso1:monto maximo");
			TrackingHelper.trackPaso1Operacion(Paso1OperacionMap);
			showConfirmacion();
			}
	}
	
	private void showConfirmacion(){
		delegate.showConfirmacion();
	}

	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		habilitaBtn();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(ConfigurarMontosDelegate.CONFIGURAR_MONTOS_DELEGATE_ID);
		super.goBack();
	}

	public void habilitaBtn(){
		isRunningTask = false;
	}
}
