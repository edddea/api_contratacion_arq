/*
 * Copyright (c) 2009 BBVA. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.Serializable;

/**
 * define the parameters for the object Periodo
 * 
 * @author Carlos Santoyo
 */
public class Periodo implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
     * The Periodo
     */
    private String periodo = "";
    private String fechaCorte = "";
    private String referencia = "";
  
    public String getReferencia() {
		return referencia;
	}

	public void setReferencia(final String referencia) {
		this.referencia = referencia;
	}

	/**
     * Default constructor
     * @param fechaCorte 
	 * @param referencia
     * @param periodo
     */
	 
    public Periodo(final String periodo, final String fechaCorte, final String referencia) {
    	this.periodo = periodo;
    	this.fechaCorte = fechaCorte;
    	this.referencia = referencia;
    }

	public String getPeriodo() {
		return periodo;
	}

	public String getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(final String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public void setPeriodo(final String periodo) {
		this.periodo = periodo;
	}

}