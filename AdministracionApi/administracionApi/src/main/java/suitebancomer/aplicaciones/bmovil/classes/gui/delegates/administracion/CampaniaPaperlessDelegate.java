package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.util.Log;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.CampaniaPaperlessViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.CampaniaPaperlessResult;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

public class CampaniaPaperlessDelegate extends DelegateBaseAutenticacion{

	/**
	 * 
	 */
	public static final long CAMPANIA_PAPERLESS_DELEGATE_ID = 9022323031683701077L;

	private CampaniaPaperlessViewController viewController;
	private BaseViewController controladorPaperless;
	private CampaniaPaperlessResult result;
	private Promociones promoPaperless;
	
	
	public CampaniaPaperlessResult getResult() {
		return result;
	}

	public void setViewController(final CampaniaPaperlessViewController viewController) {
		this.viewController = viewController;
	}
		
	/**
	 * muestra la pantalla de confirmacion para realizar la suspension
	 */
	
	public void realizaConfirmar(){
		showConfirmacion();		
	}
		
	/**
	 * @return true si se debe mostrar contrasena, false en caso contrario.
	 */
	@Override
	
	public boolean mostrarContrasenia() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.inhibirEnvioEstadoCuenta,
				perfil);
		
		Log.e("mostrarContrasenia()",String.valueOf(value));
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarCVV(Constants.Operacion.inhibirEnvioEstadoCuenta, perfil);
		Log.e("mostrarCVV()",String.valueOf(value));
		return value;
	}
	
	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		final boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.inhibirEnvioEstadoCuenta,
				perfil);
		Log.e("mostrarNIP()",String.valueOf(value));
		return value;
	}
	
	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		final Perfil perfil = Session.getInstance(SuiteAppAdmonApi.appContext).getClientProfile();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.inhibirEnvioEstadoCuenta,
									perfil);
		} catch (Exception ex) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
			tipoOTP = null;
		}

		if(ServerCommons.ALLOW_LOG) Log.e("TipoOtpAutenticacion",String.valueOf(tipoOTP));
		return tipoOTP;
	}
	
	/**
	 * Obtiene el texto para el encabezado de la confirmacion
	 */
	@Override
	public int getTextoEncabezado() {
		int textoEncabezado = 0;
		
		textoEncabezado = R.string.confirmacion_paperless_title;
		
		return textoEncabezado;
	}
	
	/**
	 * Obtiene la imagen para mostrar encabezado
	 */
	@Override
	public int getNombreImagenEncabezado() {
		int imgEncabezado = 0;
		
		imgEncabezado = R.drawable.paperless_icono;
		
		return imgEncabezado;
	}
	
	public int getColorTituloResultado() {
		int colorRecurso = 0;
		
		colorRecurso = R.color.primer_azul;
		
		return colorRecurso;
	}
	 	
	public String getTextoEspecialResultados() {
		String textoEspecial;			
		textoEspecial = SuiteAppAdmonApi.appContext.getString(R.string.resultado_texto_ayuda);
		
		return textoEspecial;
	}
	
	public String getTextoTituloResultado() {

		final String textoTituloResultado = SuiteAppAdmonApi.appContext.getString(R.string.resultado_operacion_exitosa);
		return textoTituloResultado;
	}
	
	@Override
	public int getOpcionesMenuResultados() {

			return SHOW_MENU_EMAIL;
	}
	
	@Override
	public String getTextoEmail() {
		return super.getTextoEmail();
	}
	
	/**
	 * Muestra la pantalla de confirmacion
	 */
	void showConfirmacion(){
		final BmovilViewsController bmovilParentController = ((BmovilViewsController) viewController.getParentViewsController());
		bmovilParentController.showConfirmacionAutenticacionViewController(
				this,
				getNombreImagenEncabezado(), 
				getTextoEncabezado(),
				R.string.confirmation_subtitulo);
	}
  
	/**
	 * Invoka la conexion al server para suspender el envio de estado de cuenta
	 * 
	 */
	public void realizaOperacion(final ConfirmacionAutenticacionViewController confirmacionAut,final String contrasenia, final String nip, final String token, final String cvv, final String campoTarjeta) {
		final Session session = Session.getInstance(SuiteAppAdmonApi.appContext);
		final Autenticacion aut = Autenticacion.getInstance();
		final Perfil perfil = session.getClientProfile();

		final String cadAutenticacion =  aut.getCadenaAutenticacion(Constants.Operacion.inhibirEnvioEstadoCuenta, perfil);
		final String companiaCelular = session.getCompaniaUsuario();


		final int operationId = Server.INHIBIR_ENVIO_EC;
		final Hashtable<String, String> params = new Hashtable<String, String>();
		
		//params.put(ServerConstants.ID_PRODUCTO, Constants.ID_PRODUCTO);
		//params.put(ServerConstants.CLAVE_CONTRATACION, Constants.CLAVE_CONTRATACION);
		params.put(ServerConstants.CLAVE_RESPUESTA, Constants.CLAVE_RESPUESTA);

		final Promociones[] promociones=Session.getInstance(viewController).getPromociones();
		String cveCamp = "";
		String cve="";
		for(int i=0; i< promociones.length;i++){
			cve=promociones[i].getCveCamp();
			cveCamp = cve.substring(0, 4);
			if(cveCamp.equals("0429")){
				promoPaperless = promociones[i];
				cveCamp = cve;
			}
		}
		
		//params.put(ServerConstants.ID_CAMPAÑA, Constants.ID_CAMPAÑA);
		params.put(ServerConstants.ID_CAMPAÑA, cveCamp);
		//params.put(ServerConstants.EMAIL,  session.getEmail());
		//params.put(ServerConstants.CAUSA_INHIBICION, Constants.CAUSA_INHIBICION);
		params.put(ServerConstants.NUMERO_CELULAR, session.getUsername() );
		params.put(ServerConstants.IUM, session.getIum());
		
		/**VERIFICAR VALIDAR 10/06**/
		
		params.put(ServerConstants.CADENA_AUTENTICACION, "00100"); //validar
		params.put(ServerConstants.CVE_ACCESO, contrasenia == null ? "" : contrasenia);
		params.put(ServerConstants.CODIGO_NIP, nip == null ? "" : nip);
		params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
		params.put(ServerConstants.TARJETA_5DIG, "");
		params.put(ServerConstants.CODIGO_CVV2, cvv == null ? "" : cvv);
		
		/**VERIFICAR VALIDAR 10/06**/
		
		doNetworkOperation(operationId, params,true,null, confirmacionAut);
	}
	
	@Override
	public void doNetworkOperation(final int operationId, final Hashtable<String,?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
		((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler, caller);
	}
	
	
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
			if(response.getResponse() instanceof CampaniaPaperlessResult){
				removePaperlessCampaign();
				showResultados();
			}	
		}else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
			final BaseViewControllerCommons current = ((BmovilViewsController)viewController.getParentViewsController()).getCurrentViewControllerApp();
			current.showInformationAlert(response.getMessageText());
		}		
	}
	
	private void removePaperlessCampaign() {
		Promociones[] nuevoPromo = new Promociones[0];
		final Promociones[] promociones=Session.getInstance(viewController).getPromociones();

		final List<Promociones> list = new ArrayList<Promociones>(Arrays.asList(promociones));
		list.remove(promoPaperless);
		nuevoPromo = list.toArray(nuevoPromo);
		for(final Promociones p : nuevoPromo){
			if(ServerCommons.ALLOW_LOG) Log.d("PROMOS NUEVAS", p.getCveCamp().substring(0, 4));
		}
		
		Session.getInstance(viewController).setPromocion(nuevoPromo);
		
	}

	private void showResultados(){
		final BmovilViewsController bmovilParentController = ((BmovilViewsController) viewController.getParentViewsController());
		bmovilParentController.showResultadosViewController(this, getNombreImagenEncabezado(), getTextoEncabezado());
	}

	
	
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {

		final ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;		
		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.confirmacion_paperless_tabla_operacion));
		final String op = viewController.getString(R.string.confirmacion_paperless_operacion);
		fila.add(op);
		tabla.add(fila);
		
		return tabla;
	}
	
	public ArrayList<Object> getDatosTablaResultados(){

		final ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila1;		
		fila1 = new ArrayList<String>();
		fila1.add(viewController.getString(R.string.confirmacion_paperless_tabla_operacion));
		final String op = viewController.getString(R.string.confirmacion_paperless_operacion);
		fila1.add(op);
		
		ArrayList<String> fila2;		
		fila2 = new ArrayList<String>();
		fila2.add(viewController.getString(R.string.bmovil_result_fecha));
		final String fecha = CampaniaPaperlessResult.getFecha();
		fila2.add(fecha);
		
		ArrayList<String> fila3;		
		fila3 = new ArrayList<String>();
		fila3.add(viewController.getString(R.string.bmovil_result_hora));
		final String hora = CampaniaPaperlessResult.getHora();
		fila3.add(hora);
		
		ArrayList<String> fila4;		
		fila4 = new ArrayList<String>();
		fila4.add(viewController.getString(R.string.bmovil_result_folio));
		final String folio = CampaniaPaperlessResult.getFolio();
		fila4.add(folio);

		tabla.add(fila1);
		tabla.add(fila2);
		tabla.add(fila3);
		tabla.add(fila4);

		return tabla;
	}
	
	@Override
	public boolean mostrarCampoTarjeta() {
		return (mostrarCVV() || mostrarNIP());
	}

}

