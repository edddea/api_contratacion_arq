/*
 * Copyright (c) 2009 BBVA. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
/**
 * CambiarPasswordData wraps the server response after the password change
 * operation.
 * 
 * @author GoNet
 */
public class CambiarPasswordData implements ParsingHandler {

    /**
     * The page
     */
    private String page = null;
    private String fecha;
    private String hora;
    
    /**
     * Default constructor
     */
    public CambiarPasswordData() {
        
    }
    
    /**
     * Get the page
     * @return the page
     */
    public String getPage() {
        return page;
    }

    /**
     * Process fee data from parser
     * @param parser the parser
     * @throws IOException on communication errors 
     * @throws ParsingException on parsing errors
     */
    public void process(final Parser parser) throws IOException, ParsingException {
        this.page = parser.parseNextValue("FO");
        fecha = parser.parseNextValue("FE");
        hora = parser.parseNextValue("HR");
    }

	public String getFecha() {
		return fecha;
	}

	public void setFecha(final String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(final String hora) {
		this.hora = hora;
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

}
