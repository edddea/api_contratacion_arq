package suitebancomer.aplicaciones.bmovil.classes.common.administracion;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;
import android.util.Log;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

//import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.DineroMovilViewController;

/**
 * Clase que obtiene los datos de un contacto para la version 2.0 de Android (Api 5) o posteriores.
 */
@TargetApi(Build.VERSION_CODES.ECLAIR)
public class ContactManager extends AbstractContactMannager {
	/**
	 * Constructor por defecto.
	 */
	public ContactManager() {
		super();
	}

	@Override
	public void getContactData(final Activity callerActivity, final Intent data) {
		String telefono = "";
	    String contacto = "";
	    Cursor c = null;
		
	    if(Server.ALLOW_LOG) Log.v(this.getClass().getSimpleName(), data.getData().getLastPathSegment());
	    
		c = callerActivity.getContentResolver().query(android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI, 
				   null, 
				   android.provider.ContactsContract.CommonDataKinds.Phone._ID + "=" + data.getData().getLastPathSegment(), 
				   null, 
				   null);

		final int phoneIdx = c.getColumnIndex(android.provider.ContactsContract.CommonDataKinds.Phone.NUMBER);
		final int nameIdx = c.getColumnIndex(android.provider.ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
		
		if(Server.ALLOW_LOG) Log.v(this.getClass().getSimpleName(), "El cursor tiene " + c.getCount() + " resultados.");
		
		if(c.moveToFirst()) {
			telefono = c.getString(phoneIdx);
			contacto = c.getString(nameIdx);
			if(Server.ALLOW_LOG) Log.v("ContactManager", "Telefono seleccionado: " + telefono);
		} else {
			if(Server.ALLOW_LOG) Log.w("ContactManager", "No results");
		}
		
		telefono = Tools.formatPhoneNumberFromContact(telefono);
		
		if(Constants.TELEPHONE_NUMBER_LENGTH == telefono.length()) {
	    	this.numeroDeTelefono = telefono;
	    	this.nombreContacto = contacto;
	    } else {
	    	this.numeroDeTelefono = "";
	    	this.nombreContacto = "";
	    }
	}

	@Override
	public void requestContactData(final Activity callerActivity) {
		Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
//AYMB		callerActivity.startActivityForResult(intent, DineroMovilViewController.ContactsRequestCode);
	}
}
