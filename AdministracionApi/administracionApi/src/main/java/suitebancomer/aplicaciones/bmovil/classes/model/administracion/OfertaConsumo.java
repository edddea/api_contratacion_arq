package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class OfertaConsumo implements ParsingHandler{
	

	String plazo;
	String importe;
	String cuentaVinc;
	String plazoDes;
	String Cat;
	String fechaCat;
	String pagoMenFijo;
	String estatusOferta;
	String folioUG;
	String totalPagos;
	String descDiasPago;
	String bscPagar;
	String impSegSal;
	String tasaAnual;
	String tasaMensual;
	String producto;

	public String getPlazo() {
		return plazo;
	}



	public void setPlazo(final String plazo) {
		this.plazo = plazo;
	}



	public String getImporte() {
		return importe;
	}



	public void setImporte(final String importe) {
		this.importe = importe;
	}



	public String getCuentaVinc() {
		return cuentaVinc;
	}



	public void setCuentaVinc(final String cuentaVinc) {
		this.cuentaVinc = cuentaVinc;
	}



	public String getCat() {
		return Cat;
	}



	public void setCat(final String cat) {
		Cat = cat;
	}



	public String getFechaCat() {
		return fechaCat;
	}



	public void setFechaCat(final String fechaCat) {
		this.fechaCat = fechaCat;
	}



	public String getPagoMenFijo() {
		return pagoMenFijo;
	}



	public void setPagoMenFijo(final String pagoMenFijo) {
		this.pagoMenFijo = pagoMenFijo;
	}



	public String getEstatusOferta() {
		return estatusOferta;
	}



	public void setEstatusOferta(final String estatusOferta) {
		this.estatusOferta = estatusOferta;
	}



	public String getFolioUG() {
		return folioUG;
	}



	public void setFolioUG(final String folioUG) {
		this.folioUG = folioUG;
	}



	public String getDescDiasPago() {
		return descDiasPago;
	}



	public void setDescDiasPago(final String descDiasPago) {
		this.descDiasPago = descDiasPago;
	}



	public String getbscPagar() {
		return bscPagar;
	}



	public void setTipoSeg(final String bscPagar) {
		this.bscPagar = bscPagar;
	}



	public String getTasaAnual() {
		return tasaAnual;
	}



	public void setTasaAnual(final String tasaAnual) {
		this.tasaAnual = tasaAnual;
	}



	public String getTasaMensual() {
		return tasaMensual;
	}



	public void setTasaMensual(final String tasaMensual) {
		this.tasaMensual = tasaMensual;
	}



	public String getProducto() {
		return producto;
	}



	public void setProducto(final String producto) {
		this.producto = producto;
	}
	


	public String getPlazoDes() {
		return plazoDes;
	}



	public void setPlazoDes(final String plazoDes) {
		this.plazoDes = plazoDes;
	}



	public String getImpSegSal() {
		return impSegSal;
	}



	public void setImpSegSal(final String impSegSal) {
		this.impSegSal = impSegSal;
	}
	
	


	public String getTotalPagos() {
		return totalPagos;
	}



	public void setTotalPagos(final String totalPagos) {
		this.totalPagos = totalPagos;
	}




	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub}
		plazo=parser.parseNextValue("plazo");		
		importe=parser.parseNextValue("importe");
		cuentaVinc=parser.parseNextValue("cuentaVinc");
		plazoDes=parser.parseNextValue("plazoDes");
		Cat=parser.parseNextValue("Cat");
		fechaCat=parser.parseNextValue("fechaCat");
		pagoMenFijo=parser.parseNextValue("pagoMenFijo");
		estatusOferta=parser.parseNextValue("estatusOferta");
		folioUG=parser.parseNextValue("folioUG");
		totalPagos=parser.parseNextValue("totalPagos");
		descDiasPago=parser.parseNextValue("descDiasPago");
		bscPagar=parser.parseNextValue("bscPagar");
		impSegSal=parser.parseNextValue("impSegSal");
		tasaAnual=parser.parseNextValue("tasaAnual");
		tasaMensual=parser.parseNextValue("tasaMensual");
		producto=parser.parseNextValue("producto");
		
	}



	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	} 

}
