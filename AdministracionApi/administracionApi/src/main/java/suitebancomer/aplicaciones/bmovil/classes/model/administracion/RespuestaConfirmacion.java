package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

public class RespuestaConfirmacion {
	// #region Variables.
	/**
	 * Contraseña de bmovil.
	 */
	private String pwd;
	
	/**
	 * Cvv de la tarjeta.
	 */
	private String cvv;
	
	/**
	 * Nip del cajero.
	 */
	private String nip;
	
	/**
	 * Token de seguridad.
	 */
	private String otp;
	
	/**
	 * tarjeta num
	 */
	private String numTarjeta;
	// #endregion
	
	// #region Getters y Setters.
	/**
	 * @return Contraseña de bmovil.
	 */
	public String getPwd() {
		return pwd;
	}

	/**
	 * @param pwd Contraseña de bmovil.
	 */
	public void setPwd(final String pwd) {
		this.pwd = pwd;
	}

	/**
	 * @return Cvv de la tarjeta.
	 */
	public String getCvv() {
		return cvv;
	}

	/**
	 * @param cvv Cvv de la tarjeta.
	 */
	public void setCvv(final String cvv) {
		this.cvv = cvv;
	}

	/**
	 * @return Nip del cajero.
	 */
	public String getNip() {
		return nip;
	}

	/**
	 * @param nip Nip del cajero.
	 */
	public void setNip(final String nip) {
		this.nip = nip;
	}

	/**
	 * @return Token de seguridad.
	 */
	public String getOtp() {
		return otp;
	}

	/**
	 * @param otp Token de seguridad.
	 */
	public void setOtp(final String otp) {
		this.otp = otp;
	}
	
	public String getNumTarjeta() {
		return numTarjeta;
	}
	
	public void setNumTarjeta(final String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}
	// #endregion
	
	/**
	 * Constructor básico, crea un objeto con valores nulos.
	 */
	public RespuestaConfirmacion() {
		pwd = null;
		cvv = null;
		nip = null;
		otp = null;
		numTarjeta = null;
	}

	/**
	 * Contructor.
	 * @param pwd Contraseña de bmovil.
	 * @param cvv Cvv de la tarjeta.
	 * @param nip Nip del cajero.
	 * @param otp Token de seguridad.
	 * @param numTarjeta TODO
	 */
	public RespuestaConfirmacion(final String pwd, final String cvv, final String nip, final String otp, final String numTarjeta) {
		super();
		this.pwd = pwd;
		this.cvv = cvv;
		this.nip = nip;
		this.otp = otp;
		this.numTarjeta = numTarjeta;
	}

}
