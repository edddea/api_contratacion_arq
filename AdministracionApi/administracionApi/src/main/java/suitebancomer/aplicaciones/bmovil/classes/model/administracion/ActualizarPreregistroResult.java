package suitebancomer.aplicaciones.bmovil.classes.model.administracion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ActualizarPreregistroResult implements ParsingHandler {

	private String folio;
	
	public String getFolio() {
		return folio;
	}

	public void setFolio(final String folio) {
		this.folio = folio;
	}

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
	
		this.folio = parser.parseNextValue("FO");

	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

}
