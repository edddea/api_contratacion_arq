package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionAutenticacionViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;


public class ConfirmacionAutenticacionDelegate extends DelegateBaseAutenticacion {
	public final static long CONFIRMACION_AUTENTICACION_DELEGATE_ID = 0x9d1a3aed49317e49L;
	
//	private ArrayList<String> datosLista;
	private final DelegateBaseAutenticacion operationDelegate;
	private final boolean debePedirContrasena;
	private final boolean debePedirNip;
	private final Constants.TipoOtpAutenticacion tokenAMostrar;
	private final boolean debePedirCVV;
	private Constants.TipoInstrumento tipoInstrumentoSeguridad;
	//private String textoInstrumentoSeguridad;
	
	private ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController;

	private final boolean debePedirTarjeta;
	//AMZ
		public boolean res = false;
	
	public ConfirmacionAutenticacionDelegate(final DelegateBaseAutenticacion delegateBaseAutenticacion) {
		this.operationDelegate = delegateBaseAutenticacion;
		debePedirContrasena = operationDelegate.mostrarContrasenia();
		debePedirNip = operationDelegate.mostrarNIP();
		debePedirCVV = operationDelegate.mostrarCVV();
		tokenAMostrar = operationDelegate.tokenAMostrar();
		debePedirTarjeta = mostrarCampoTarjeta();
		final String instrumento = Session.getInstance(SuiteAppAdmonApi.appContext).getSecurityInstrument();
		if (instrumento.equals(Constants.IS_TYPE_DP270)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
		} else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
		} else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
		} else {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
		}
		
		//textoInstrumentoSeguridad = operationDelegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumentoSeguridad);
	}
	
	public void setConfirmacionAutenticacionViewController(final ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController) {
		this.confirmacionAutenticacionViewController = confirmacionAutenticacionViewController;
	}

	public void consultaDatosLista() {
		confirmacionAutenticacionViewController.setListaDatos(operationDelegate.getDatosTablaConfirmacion());
	}
	
	public DelegateBaseAutenticacion consultaOperationsDelegate() {
		return operationDelegate;
	}
	
	public boolean consultaDebePedirContrasena() {
		return debePedirContrasena;
	}
	
	public boolean consultaDebePedirNIP() {
		return debePedirNip;
	}
	
	public boolean consultaDebePedirCVV() {
		return debePedirCVV;
	}
	
	public Constants.TipoInstrumento consultaTipoInstrumentoSeguridad() {
		return tipoInstrumentoSeguridad;
	}
	
	public Constants.TipoOtpAutenticacion consultaInstrumentoSeguridad() {
		return tokenAMostrar;
	}
	
	public void enviaPeticionOperacion() {
		String contrasena = null;
		String nip = null;
		String asm = null;
		String cvv = null;
		res = false;

		final OnClickListener listener = new OnClickListener() {
			
			@Override
			public void onClick(final DialogInterface dialog, final int arg1) {
				dialog.dismiss();
				confirmacionAutenticacionViewController.habilitarBtnContinuar();
			}
		};
		if (debePedirContrasena) {
			contrasena = confirmacionAutenticacionViewController.pideContrasena();
			if (Constants.EMPTY_STRING.equals(contrasena)) {
				final StringBuilder mensaje = new StringBuilder(confirmacionAutenticacionViewController.getString(R.string.confirmation_valorVacio));
				mensaje.append(" ");
				mensaje.append(confirmacionAutenticacionViewController.getString(R.string.confirmation_componenteContrasena));
				mensaje.append(".");
				confirmacionAutenticacionViewController.showInformationAlert(mensaje.toString(),listener);
				return;
			} else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
				final StringBuilder mensaje = new StringBuilder(confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto1));
				mensaje.append(" ");
				mensaje.append(Constants.PASSWORD_LENGTH);
				mensaje.append(" ");
				mensaje.append(confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto2));
				mensaje.append(" ");
				mensaje.append(confirmacionAutenticacionViewController.getString(R.string.confirmation_componenteContrasena));
				mensaje.append(".");
				confirmacionAutenticacionViewController.showInformationAlert(mensaje.toString(),listener);
				return;
			}
		}
				
		String tarjeta = null;
		if(debePedirTarjeta){
			tarjeta = confirmacionAutenticacionViewController.pideTarjeta();
			String mensaje = "";
			if(tarjeta.equals("")){
				mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
				confirmacionAutenticacionViewController.showInformationAlert(mensaje, listener);
				return;
			}else if(tarjeta.length() != 5){
				mensaje =  "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
				confirmacionAutenticacionViewController.showInformationAlert(mensaje, listener);
				return;
			}			
		}
		
		if (debePedirNip) {
			nip = confirmacionAutenticacionViewController.pideNIP();
			if (Constants.EMPTY_STRING.equals(nip)) {
				final StringBuilder mensaje = new StringBuilder(confirmacionAutenticacionViewController.getString(R.string.confirmation_valorVacio));
				mensaje.append(" ");
				mensaje.append(confirmacionAutenticacionViewController.getString(R.string.confirmation_componenteNip));
				mensaje.append(".");
				confirmacionAutenticacionViewController.showInformationAlert(mensaje.toString(),listener);
				return;
			} else if (nip.length() != Constants.NIP_LENGTH) {
				final StringBuilder mensaje = new StringBuilder(confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto1));
				mensaje.append(" ");
				mensaje.append(Constants.NIP_LENGTH);
				mensaje.append(" ");
				mensaje.append(confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto2));
				mensaje.append(" ");
				mensaje.append(confirmacionAutenticacionViewController.getString(R.string.confirmation_componenteNip));
				mensaje.append(".");
				confirmacionAutenticacionViewController.showInformationAlert(mensaje.toString(),listener);
				return;
			}
		}
		if (tokenAMostrar != Constants.TipoOtpAutenticacion.ninguno) {
			asm = confirmacionAutenticacionViewController.pideASM();
			if (Constants.EMPTY_STRING.equals(asm)) {
				final StringBuilder mensaje = new StringBuilder(confirmacionAutenticacionViewController.getString(R.string.confirmation_valorVacio));
				mensaje.append(" ");
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje.append(getEtiquetaCampoOCRA());
						break;
					case DP270:
						mensaje.append(getEtiquetaCampoDP270());
						break;
					case SoftToken:
						if (SuiteAppAdmonApi.getSofttokenStatus()) {
							mensaje.append(getEtiquetaCampoSoftokenActivado());
						} else {
							mensaje.append(getEtiquetaCampoSoftokenDesactivado());
						}
						break;
					default:
						break;
				}
				mensaje.append(".");
				confirmacionAutenticacionViewController.showInformationAlert(mensaje.toString(),listener);
				return;
			} else if (asm.length() != Constants.ASM_LENGTH) {
				final StringBuilder mensaje = new StringBuilder(confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto1));
				mensaje.append(" ");
				mensaje.append(Constants.ASM_LENGTH);
				mensaje.append(" ");
				mensaje.append(confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto2));
				mensaje.append(" ");
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje.append(getEtiquetaCampoOCRA());
						break;
					case DP270:
						mensaje.append(getEtiquetaCampoDP270());
						break;
					case SoftToken:
						if (SuiteAppAdmonApi.getSofttokenStatus()) {
							mensaje.append(getEtiquetaCampoSoftokenActivado());
						} else {
							mensaje.append(getEtiquetaCampoSoftokenDesactivado());
						}
						break;
					default:
						break;
				}	
				mensaje.append(".");
				confirmacionAutenticacionViewController.showInformationAlert(mensaje.toString(),listener);
				return;
			}
		}
		if (debePedirCVV) {
			cvv = confirmacionAutenticacionViewController.pideCVV();
			if (Constants.EMPTY_STRING.equals(cvv)) {
				final StringBuilder mensaje = new StringBuilder(confirmacionAutenticacionViewController.getString(R.string.confirmation_valorVacio));
				mensaje.append(" ");
				mensaje.append(confirmacionAutenticacionViewController.getString(R.string.confirmation_componenteCvv));
				mensaje.append(".");
				confirmacionAutenticacionViewController.showInformationAlert(mensaje.toString(),listener);
				return;
			} else if (cvv.length() != Constants.CVV_LENGTH) {
				final StringBuilder mensaje = new StringBuilder(confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto1));
				mensaje.append(" ");
				mensaje.append(Constants.CVV_LENGTH);
				mensaje.append(" ");
				mensaje.append(confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto2));
				mensaje.append(" ");
				mensaje.append(confirmacionAutenticacionViewController.getString(R.string.confirmation_componenteCvv));
				mensaje.append(".");
				confirmacionAutenticacionViewController.showInformationAlert(mensaje.toString(),listener);
				return;
			}
		}
		
		String newToken = null;
		if(tokenAMostrar != TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == TipoInstrumento.SoftToken && SuiteAppAdmonApi.getSofttokenStatus())
			newToken = loadOtpFromSofttoken(tokenAMostrar);
		if(null != newToken)
			asm = newToken;

		operationDelegate.realizaOperacion(confirmacionAutenticacionViewController, contrasena, nip, asm, cvv, tarjeta);
		confirmacionAutenticacionViewController.habilitarBtnContinuar();
		res = true;

	}
	
	@Override
	public String getEtiquetaCampoContrasenia() {
		return confirmacionAutenticacionViewController.getString(R.string.confirmation_aut_contrasena);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenActivado() {
		return confirmacionAutenticacionViewController.getString(R.string.confirmation_softtokenActivado);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenDesactivado() {
		return confirmacionAutenticacionViewController.getString(R.string.confirmation_softtokenDesactivado);
	}
	
	@Override
	public String getEtiquetaCampoCVV() {
		return confirmacionAutenticacionViewController.getString(R.string.confirmation_CVV);
	}
	
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		if(response.getStatus() == ServerResponse.OPERATION_ERROR){
			confirmacionAutenticacionViewController.limpiarCampos();
			((BmovilViewsController)confirmacionAutenticacionViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
		}
		operationDelegate.analyzeResponse(operationId, response);
	}
	
	public DelegateBaseAutenticacion getOperationDelegate() {
		return operationDelegate;
	}
	
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		return tokenAMostrar;
	}
	
	@Override
	public boolean mostrarCampoTarjeta() {
		return operationDelegate.mostrarCampoTarjeta();
	}
	
	@Override
	public String loadOtpFromSofttoken(final TipoOtpAutenticacion tipoOTP) {
		return loadOtpFromSofttoken(tipoOTP, operationDelegate);
	}
}
