package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.MantenimientoAlertas;
import suitebancomer.aplicaciones.bmovil.classes.model.administracion.MantenimientoAlertasResultado;
import suitebancomer.classes.gui.controllers.administracion.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;


public class MantenimientoAlertasDelegate extends DelegateBaseAutenticacion {

	/** Identificador �nico del delegate. */
	public static final long MANTENIMIENTO_ALERTAS_DELEGATE_ID = 5438921148206295582L;

	/** Constante para enviar la operacino de modificacion. */
	private static final String MANTENIMIENTO_MODIFICACION = "modificacion";

	/** Constante para enviar la operacino de alta. */
	private static final String MANTENIMIENTO_ALTA = "alta";

	/** Modelo con datos básicos de Mantenimiento Alertas */
	private MantenimientoAlertas mantenimientoAlertas;

	/** Tipo de operacion actual. */
	private Constants.Operacion tipoOperacion;

	/** Controlador actual. */
	private BaseViewController ownerController;

	private MantenimientoAlertasResultado mantenimientoAlertasResultado;

	private ContratacionDelegate contratacionDelegate;

	// /** Controlador base. */
	// private BaseViewController baseViewController;

	/**
	 * Constructor por defecto.
	 */
	public MantenimientoAlertasDelegate(
			final ContratacionDelegate contratacionDelegate) {
		this.contratacionDelegate = contratacionDelegate;
	}

	private Constants.Operacion getOperacionEjecutar() {
		return tipoOperacion;
	}

	public void setOperacionEjecutar() {
		tipoOperacion = Constants.Operacion.contratacionAlertas;
		if (Constants.MANTENIMIENTO_ALERTAS_ACTUALIZAR
				.equals(mantenimientoAlertas.getOperacionEjecutar())) {
			tipoOperacion = Constants.Operacion.actualizacionAlertas;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.
	 * DelegateBaseAutenticacion
	 * #realizaOperacion(suitebancomer.aplicaciones.bmovil
	 * .classes.gui.controllers.ContratacionAutenticacionViewController,
	 * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 * boolean, java.lang.String)
	 */
	@Override
	public void realizaOperacion(
			final ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController,
			final String contrasenia, final String nip, final String token, final String cvv,
			final String campoTarjeta) {
		ownerController = confirmacionAutenticacionViewController;

		String indicador = MANTENIMIENTO_ALTA;
		if (Constants.MANTENIMIENTO_ALERTAS_ACTUALIZAR
				.equals(mantenimientoAlertas.getOperacionEjecutar())) {
			indicador = MANTENIMIENTO_MODIFICACION;
		}

		final String cadAutenticacion = Autenticacion.getInstance()
				.getCadenaAutenticacion(
						getOperacionEjecutar(),
						Session.getInstance(SuiteAppAdmonApi.appContext)
								.getClientProfile());

		final Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.NUMERO_TARJETA,
				mantenimientoAlertas.getNumeroTarjeta());
		paramTable.put(ServerConstants.INDICADOR, indicador);
		paramTable.put(ServerConstants.NUMERO_TELEFONO,
				mantenimientoAlertas.getNumCelular());
		paramTable.put(ServerConstants.COMPANIA_CELULAR,
				mantenimientoAlertas.getCompaniaCelular());
		paramTable.put(ServerConstants.CVE_ACCESO,
				Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia);
		paramTable.put(ServerConstants.CODIGO_NIP,
				Tools.isEmptyOrNull(nip) ? "" : nip);
		paramTable.put(ServerConstants.CODIGO_OTP,
				Tools.isEmptyOrNull(token) ? "" : token);
		paramTable.put(ServerConstants.CODIGO_CVV2,
				Tools.isEmptyOrNull(cvv) ? "" : cvv);
		paramTable.put(ServerConstants.PERFIL_CLIENTE,
				Tools.isEmptyOrNull(token) ? "" : (mantenimientoAlertas.getPerfilString()));
		
		
		paramTable.put(ServerConstants.CADENA_AUTENTICACION, cadAutenticacion);
		paramTable.put(ServerConstants.TARJETA_5DIG,
				Tools.isEmptyOrNull(campoTarjeta) ? "" : campoTarjeta);
		//JAIG
		SuiteAppAdmonApi.getInstance()
				.getBmovilApplication()
				.invokeNetworkOperation(Server.MANTENIMIENTO_ALERTAS,
						paramTable, true,new MantenimientoAlertasResultado(),ownerController);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.classes.gui.delegates.BaseDelegate#analyzeResponse(int,
	 * suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		if (operationId == Server.MANTENIMIENTO_ALERTAS
				&& response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			final Object object = response.getResponse();
			if (object instanceof MantenimientoAlertasResultado) {
				mantenimientoAlertasResultado = (MantenimientoAlertasResultado) object;
				mostrarResultados();
			}
		}
	}

	/**
	 * Muestra la pantalla de resultados autenticacion.
	 */
	private void mostrarResultados() {
		SuiteAppAdmonApi.getInstance().getBmovilApplication()
				.getBmovilViewsController()
				.showResultadosAutenticacionViewController(this, -1, -1);
	}

	@Override
	public boolean mostrarCampoTarjeta() {
		return (mostrarCVV() || mostrarNIP());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #mostrarContrasenia()
	 */
	@Override
	public boolean mostrarContrasenia() {
		return Autenticacion.getInstance().mostrarContrasena(
				getOperacionEjecutar(), this.mantenimientoAlertas.getPerfil());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #tokenAMostrar()
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		return Autenticacion.getInstance().tokenAMostrar(
				getOperacionEjecutar(), this.mantenimientoAlertas.getPerfil());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #mostrarNIP()
	 */
	@Override
	public boolean mostrarNIP() {
		return Autenticacion.getInstance().mostrarNIP(getOperacionEjecutar(),
				this.mantenimientoAlertas.getPerfil());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #mostrarCVV()
	 */
	@Override
	public boolean mostrarCVV() {
		return Autenticacion.getInstance().mostrarCVV(getOperacionEjecutar(),
				this.mantenimientoAlertas.getPerfil());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #getDatosTablaConfirmacion()
	 */
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
		final ArrayList<Object> lista = new ArrayList<Object>();

		ArrayList<Object> registros = new ArrayList<Object>();
		registros.add("Operación");
		if (Constants.Operacion.actualizacionAlertas
				.equals(getOperacionEjecutar())) {
			registros.add("Actualización alertas");
		} else if (Constants.Operacion.contratacionAlertas
				.equals(getOperacionEjecutar())) {
			registros.add("Contratar alertas");
		}
		lista.add(registros);

		registros = new ArrayList<Object>();
		registros.add("Número Celular");
		registros.add(mantenimientoAlertas.getNumCelular());
		lista.add(registros);

		registros = new ArrayList<Object>();
		registros.add("Compañía celular");
		registros.add(mantenimientoAlertas.getCompaniaCelular());
		lista.add(registros);

		return lista;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #getDatosTablaResultados()
	 */
	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		final ArrayList<Object> datosResultados = new ArrayList<Object>();

		ArrayList<Object> registros = new ArrayList<Object>();
		registros.add("Operación");
		registros.add("Alertas");
		datosResultados.add(registros);

		registros = new ArrayList<Object>();
		registros.add("Fecha");
		registros
				.add(Tools.formatDate(mantenimientoAlertasResultado.getFecha()));
		datosResultados.add(registros);

		registros = new ArrayList<Object>();
		registros.add("Hora");
		registros
				.add(Tools.formatTime(mantenimientoAlertasResultado.getHora()));
		datosResultados.add(registros);

		registros = new ArrayList<Object>();
		registros.add("Folio");
		registros.add(mantenimientoAlertasResultado.getFolio());
		datosResultados.add(registros);

		return datosResultados;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #getTextoTituloResultado()
	 */
	@Override
	public String getTextoTituloResultado() {
		return ownerController
				.getString(R.string.bmovil_contratacion_mantenimiento_resultados_titulo);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #getColorTituloResultado()
	 */
	@Override
	public int getColorTituloResultado() {
		return R.color.verde_limon;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #getTextoPantallaResultados()
	 */
	@Override
	public String getTextoPantallaResultados() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #getTextoEspecialResultados()
	 */
	@Override
	public String getTextoEspecialResultados() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #getTextoEncabezado()
	 */
	@Override
	public int getTextoEncabezado() {
		int title = 0;

		if (Constants.Operacion.actualizacionAlertas
				.equals(getOperacionEjecutar())) {
			title = R.string.bmovil_contratacion_mantenimiento_actualizar_title;
		} else if (Constants.Operacion.contratacionAlertas
				.equals(getOperacionEjecutar())) {
			title = R.string.bmovil_contratacion_mantenimiento_contratar_title;
		}

		return title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #getNombreImagenEncabezado()
	 */
	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.icono_activacion;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #getOpcionesMenuResultados()
	 */
	@Override
	public int getOpcionesMenuResultados() {
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.
	 * DelegateBaseAutenticacion#accionBotonResultados()
	 */
	@Override
	protected void accionBotonResultados() {
		contratacionDelegate.mostrarDefinirPassword();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.
	 * DelegateBaseAutenticacion#getImagenBotonResultados()
	 */
	@Override
	protected int getImagenBotonResultados() {
		return R.drawable.btn_continuar;
	}

	/**
	 * Obtiene el objeto Mantenimiento Alertas
	 * 
	 * @return Modelo de Mantenimiento Alertas
	 */
	public MantenimientoAlertas getMantenimientoAlertas() {
		return mantenimientoAlertas;
	}

	/**
	 * Establece el objeto Mantenimiento Alertas
	 * 
	 * @param mantenimientoAlertas
	 *            Nuevo modelo de Mantenimiento Alertas
	 */
	public void setMantenimientoAlertas(
			final MantenimientoAlertas mantenimientoAlertas) {
		this.mantenimientoAlertas = mantenimientoAlertas;
	}

	/**
	 * Devuelve el controlador actual
	 * 
	 * @return Controlador actual
	 */
	public BaseViewController getViewController() {
		return ownerController;
	}

	/**
	 * Establece un nuevo controlador
	 * 
	 * @param viewController
	 */
	public void setViewController(final BaseViewController viewController) {
		this.ownerController = viewController;
	}

}