package suitebancomer.classes.gui.views.administracion;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.Button;

import suitebancomer.classes.common.administracion.GuiTools;

public class CustomButton extends Button {
	
	private Paint mPaint = new Paint();

	public CustomButton(final Context context) {
		super(context);		
	}
	
	@Override
	protected void onDraw(final Canvas canvas) {
		

		final double factor = GuiTools.getScaleFactor();
		mPaint.setColor(Color.rgb(102, 204, 0));
		mPaint.setTextSize((float) (11d * factor + 0.5d));
		mPaint.setAntiAlias(true);
		final String text=getText().toString();
	    canvas.drawText(text, (float)(155d * factor) + 0.5f, (float)(29.2d * factor) + 0.5f, mPaint);
	}

}
