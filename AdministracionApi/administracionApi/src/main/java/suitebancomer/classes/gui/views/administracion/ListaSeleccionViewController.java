package suitebancomer.classes.gui.views.administracion;

import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.bancomer.mbanking.administracion.R;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.ConsultarEstatusEnvioEstadodeCuentaViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.FiltroListaViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.CuentaEC;
import suitebancomer.classes.common.administracion.GuiTools;
import suitebancomer.classes.gui.delegates.administracion.BaseDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;

//import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.FiltroListaViewController;

public class ListaSeleccionViewController extends LinearLayout {

	private BaseViewsControllerCommons parentManager;
	private ListView listview;
	private ArrayList<Object> encabezado;
	private ArrayList<Object> lista;
	private int numeroColumnas;
	private boolean ecFlag;
	private int numeroFilas;
	private boolean seleccionable;
	private float altura;
	private boolean alturaFija;
	private boolean existeFiltro;
	private String title;

	private TableLayout cabecera;
	private LlenarTablas adapter;
	private int opcionSeleccionada;
	private LinearLayout.LayoutParams params;
	private float anchoColumna;
	private LinearLayout vista;
	private LinearLayout vistaFiltro;
	private LayoutInflater inflater;
	private BaseDelegate delegate;
	private TextView titulo;
	private boolean singleLine;
	//paperless
	private CheckBox check;
	private int sizeCelda;

 private FiltroListaViewController filtroLista;
	
	private boolean scaled;
	private boolean encabezadoCargado;
	private String textoAMostrar;
	private boolean fijarLista=false;
	private ConsultarEstatusEnvioEstadodeCuentaViewController consultarEstatusEnvio;
	
	public ListaSeleccionViewController(final Context context,final LinearLayout.LayoutParams layoutParams, final BaseViewsControllerCommons parentManager) {
		super(context);
		marqueeEnabled = true;
		inflater = LayoutInflater.from(context);
		final LinearLayout viewLayout = (LinearLayout) inflater.inflate(SuiteAppAdmonApi.getResourceId("layout_lista_seleccion_view_admon","layout"), this, true);
		viewLayout.setLayoutParams(layoutParams);
		this.parentManager = parentManager;

		vista = (LinearLayout) findViewById(SuiteAppAdmonApi.getResourceId("layout_ListaSeleccion", "id"));
		vistaFiltro = (LinearLayout) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_filtro", "id"));
		cabecera = (TableLayout) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_tabla", "id"));

		listview = (ListView) findViewById(SuiteAppAdmonApi.getResourceId("lista_Seleccion_View","id"));
		
		scaled = false;
		encabezadoCargado = false;
	}
  public boolean isEcFlag() {
        return ecFlag;
    }


    public void setEcFlag(final boolean ecFlag) {
        this.ecFlag = ecFlag;
    }
	public void setSingleLine(final boolean singleLine) {
		this.singleLine = singleLine;
	}

	public String getTextoAMostrar() {
		return textoAMostrar;
	}

	public void setTextoAMostrar(final String textoAMostrar) {
		this.textoAMostrar = textoAMostrar;
	}
	public ConsultarEstatusEnvioEstadodeCuentaViewController getConsultarEstatusEnvio() {
		return consultarEstatusEnvio;
	}

	public void setConsultarEstatusEnvio(final ConsultarEstatusEnvioEstadodeCuentaViewController consultarEstatusEnvio) {
		this.consultarEstatusEnvio = consultarEstatusEnvio;
	}
	public void cargarEncabezado(){
		final GuiTools guiTools = GuiTools.getCurrent();
		
		if (getNumeroColumnas() == 1) {
			TableRow.LayoutParams parametros;
			
			parametros = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[0] : anchoColumna);

			final TextView celda1 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_1","id"));
			celda1.setText((String) encabezado.get(1));
			celda1.setLayoutParams(parametros);
			celda1.setSingleLine(true);
            celda1.setSelected(true);
			findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_2","id"))
					.setVisibility(View.GONE);
			findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_3","id"))
					.setVisibility(View.GONE);
			findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_4","id"))
					.setVisibility(View.GONE);
			
			if(!scaled) {
				celda1.setMaxLines(1);
//					celda1.setSingleLine(true);
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				guiTools.tryScaleText(celda1);
			}
		} else if (getNumeroColumnas() == 2) {
			TableRow.LayoutParams parametros;
			parametros = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[0] : anchoColumna);

			final TextView celda1 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_1","id"));
			celda1.setText((String) encabezado.get(1));
			celda1.setLayoutParams(parametros);
			celda1.setSingleLine(true);
            celda1.setSelected(true);

            if(overrideColumnWidth)
            	parametros = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, overridedColumnWidths[1]);

			final TextView celda2 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_2","id"));
			celda2.setText((String) encabezado.get(2));
			celda2.setLayoutParams(parametros);
			celda2.setGravity(Gravity.RIGHT);
			celda2.setSingleLine(true);
            celda2.setSelected(true);

			findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_3", "id"))
					.setVisibility(View.GONE);
			findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_4", "id"))
					.setVisibility(View.GONE);
			
			if(!scaled) {
				celda1.setMaxLines(1);
				celda2.setMaxLines(1);
				
//					celda1.setSingleLine(true);
//					celda2.setSingleLine(true);
				
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				
				guiTools.tryScaleText(celda1);
				guiTools.tryScaleText(celda2);
			}
		}else if (getNumeroColumnas() == 3) {
			TableRow.LayoutParams parametros;
			
			parametros = new TableRow.LayoutParams(0,LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[0] : 0.3f);
			final TextView celda1 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_1","id"));
			celda1.setText((String) encabezado.get(1));
			celda1.setLayoutParams(parametros);
			celda1.setSingleLine(true);
            celda1.setSelected(true);

			parametros = new TableRow.LayoutParams(0,LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[1] : 0.4f);
			final TextView celda2 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_2","id"));
			celda2.setText((String) encabezado.get(2));
			celda2.setLayoutParams(parametros);
			celda2.setGravity(Gravity.CENTER);
			celda2.setSingleLine(true);
            celda2.setSelected(true);

			parametros = new TableRow.LayoutParams(0,LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[2] : 0.3f);
			final TextView celda3 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_3","id"));
			celda3.setText((String) encabezado.get(3));
			celda3.setLayoutParams(parametros);
			celda3.setGravity(Gravity.RIGHT);
			celda3.setSingleLine(true);
            celda3.setSelected(true);
			
			findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_4","id")).setVisibility(View.GONE);

			if(!scaled) {
				celda1.setMaxLines(1);
				celda2.setMaxLines(1);
				celda3.setMaxLines(1);
				
				celda1.setSingleLine(true);
//					celda2.setSingleLine(true);
//					celda3.setSingleLine(true);
				
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				
				guiTools.tryScaleText(celda1);
				guiTools.tryScaleText(celda2);
				guiTools.tryScaleText(celda3);
			}
			
		}else if (getNumeroColumnas() == 4) {
			TableRow.LayoutParams parametros;

			parametros = new TableRow.LayoutParams(0,	LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[0] : anchoColumna);
			final TextView celda1 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_1","id"));
			celda1.setText((String) encabezado.get(1));
			celda1.setLayoutParams(parametros);
			celda1.setSingleLine(true);
            celda1.setSelected(false);

            parametros = new TableRow.LayoutParams(0,	LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[1] : anchoColumna);
			final TextView celda2 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_2","id"));
			celda2.setText((String) encabezado.get(2));
			celda2.setLayoutParams(parametros);
			celda2.setGravity(Gravity.CENTER);
			celda2.setSingleLine(true);
            celda2.setSelected(true);

            parametros = new TableRow.LayoutParams(0,	LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[2] : anchoColumna);
			final TextView celda3 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_3","id"));
			celda3.setText((String) encabezado.get(3));
			celda3.setLayoutParams(parametros);
			celda3.setGravity(Gravity.CENTER);
			celda3.setSingleLine(true);
            celda3.setSelected(true);

            parametros = new TableRow.LayoutParams(0,	LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[3] : anchoColumna);
			final TextView celda4 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_4","id"));
			celda4.setText((String) encabezado.get(4));
			celda4.setLayoutParams(parametros);
			celda4.setGravity(Gravity.RIGHT);
			celda4.setSingleLine(true);
            celda4.setSelected(true);
			
			if(!scaled) {
				celda1.setMaxLines(1);
				celda2.setMaxLines(1);
				celda3.setMaxLines(1);
				celda4.setMaxLines(1);
				
//					celda1.setSingleLine(true);
//					celda2.setSingleLine(true);
//					celda3.setSingleLine(true);
//					celda4.setSingleLine(true);
				
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda4.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				
				guiTools.tryScaleText(celda1);
				guiTools.tryScaleText(celda2);
				guiTools.tryScaleText(celda3);
				guiTools.tryScaleText(celda4);
			}
		} else if (getNumeroColumnas() == 5) {
			if (!seleccionable) {
				TableRow.LayoutParams parametros;

				parametros = new TableRow.LayoutParams(0,
						LayoutParams.WRAP_CONTENT,
						overrideColumnWidth ? overridedColumnWidths[0]
								: anchoColumna);
				final TextView celda1 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_1","id"));
				celda1.setText((String) encabezado.get(1));
				celda1.setLayoutParams(parametros);
				celda1.setSingleLine(true);
				celda1.setSelected(false);

				parametros = new TableRow.LayoutParams(0,
						LayoutParams.WRAP_CONTENT,
						overrideColumnWidth ? overridedColumnWidths[1]
								: anchoColumna);
				final TextView celda2 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_2","id"));
				celda2.setText((String) encabezado.get(2));
				celda2.setLayoutParams(parametros);
				celda2.setGravity(Gravity.CENTER);
				celda2.setSingleLine(true);
				celda2.setSelected(true);

				parametros = new TableRow.LayoutParams(0,
						LayoutParams.WRAP_CONTENT,
						overrideColumnWidth ? overridedColumnWidths[2]
								: anchoColumna);
				final TextView celda3 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_3","id"));
				celda3.setText((String) encabezado.get(3));
				celda3.setLayoutParams(parametros);
				celda3.setGravity(Gravity.CENTER);
				celda3.setSingleLine(true);
				celda3.setSelected(true);

				parametros = new TableRow.LayoutParams(0,
						LayoutParams.WRAP_CONTENT,
						overrideColumnWidth ? overridedColumnWidths[3]
								: anchoColumna);
				final TextView celda4 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_4","id"));
				celda4.setText((String) encabezado.get(4));
				celda4.setLayoutParams(parametros);
				celda4.setGravity(Gravity.RIGHT);
				celda4.setSingleLine(true);
				celda4.setSelected(true);

				parametros = new TableRow.LayoutParams(0,
						LayoutParams.WRAP_CONTENT,
						overrideColumnWidth ? overridedColumnWidths[3]
								: anchoColumna);
				final TextView celda5 = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_5","id"));
				celda5.setText((String) encabezado.get(5));
				celda5.setLayoutParams(parametros);
				celda5.setGravity(Gravity.RIGHT);
				celda5.setSingleLine(true);
				celda5.setSelected(true);
				
				if (!scaled) {
					celda1.setMaxLines(1);
					celda2.setMaxLines(1);
					celda3.setMaxLines(1);
					celda4.setMaxLines(1);
					celda5.setMaxLines(1);
					
					celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda4.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda5.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					
					guiTools.tryScaleText(celda1);
					guiTools.tryScaleText(celda2);
					guiTools.tryScaleText(celda3);
					guiTools.tryScaleText(celda4);
					guiTools.tryScaleText(celda5);
				}
			}

		}
		
	}
	
	public void cargarTabla() {

		final GuiTools guiTools = GuiTools.getCurrent();
		if(!scaled) {
			guiTools.scalePaddings(findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_tabla","id")));
		}
		
		if (getTitle() != null) {
			titulo = (TextView) findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_titulo","id"));
			titulo.setText(getTitle());
			titulo.setVisibility(View.VISIBLE);
			
			if(!scaled) {
				guiTools.scale(titulo, true);
			}
		}

		if (isSeleccionable()) {
			anchoColumna = 0.85f / getNumeroColumnas();
			final TableRow.LayoutParams parametros = new TableRow.LayoutParams(0,	LayoutParams.FILL_PARENT, 0.15f);
			findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_5","id")).setVisibility(View.VISIBLE);
			findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_5","id")).setLayoutParams(parametros);
		} else {
			if (getNumeroColumnas() == 5) {
				anchoColumna = 1.0f / getNumeroColumnas();
				final TableRow.LayoutParams parametros = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,
						LayoutParams.FILL_PARENT);
				findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_5","id"))
						.setVisibility(View.VISIBLE);
				findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_5","id"))
						.setLayoutParams(parametros);
			}else {
			anchoColumna = 1.0f / getNumeroColumnas();
			findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_5","id")).setVisibility(View.GONE);
		}
		}
		
		if (encabezado != null) {
			if (!encabezadoCargado) {
				listview.setBackgroundResource(R.drawable.list_border_bottom_cell);
				cargarEncabezado();
				encabezadoCargado = true;
			}
		} else {
			cabecera.setVisibility(View.GONE);
		}
//		findViewById(R.id.lista_seleccion_encabezados_divider).setVisibility((null == encabezado) ? View.GONE : View.VISIBLE);

		if (!Tools.isEmptyOrNull(textoAMostrar)&& lista.size()==0) {
			setNumeroColumnas(1);
			numeroFilas = 1;
			anchoColumna = 1.0f;
			listview.setEnabled(false);
			listview.setSelected(false);
			final ArrayList<Object> registro = new ArrayList<Object>();
			registro.add("");
			registro.add(textoAMostrar);
			lista = new ArrayList<Object>();
			lista.add(registro);
		}else {
			listview.setEnabled(true);
			listview.setSelected(true);
		}
		
		adapter = new LlenarTablas();
		listview.setAdapter(adapter);

		if (isAlturaFija()) {
			final ListAdapter listAdapter = listview.getAdapter();
			int filas = 0;
			if (listAdapter.getCount() <= numeroFilas) {
				filas =listAdapter.getCount();
			}else {
				filas = numeroFilas;
			}
			int totalHeight = 0;
			for (int i = 0; i < filas; i++) {
				final View listItem = listAdapter.getView(i, null, listview);
				listItem.measure(0, 0);
				totalHeight += listItem.getMeasuredHeight();
			}

			final ViewGroup.LayoutParams params = listview.getLayoutParams();
			params.height = totalHeight
					+ (listview.getDividerHeight() * (listview.getCount() - 1));
			listview.setLayoutParams(params);
			listview.requestLayout();
		}
		
		if (isExisteFiltro()) {
			final LinearLayout.LayoutParams params = new
			LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		           
		 if (getFiltroLista() == null) {
                setFiltroLista(new FiltroListaViewController(getContext(), params));
                getFiltroLista().setListaSeleccion(this);
                getFiltroLista().init();
                vistaFiltro.addView(getFiltroLista());
                vistaFiltro.setVisibility(View.VISIBLE);
            }
		}

		scaled = true;
	}

	public void getFilaSeleccionada(final Account cuenta) {

	}

	@SuppressWarnings("unchecked")
	public void ejecutarOpcionDelegate() {
		if (delegate != null && !parentManager.isActivityChanging()) {
			delegate.performAction(((ArrayList<Object>) lista.get(getOpcionSeleccionada())).get(0));
		}
	}

	public ArrayList<Object> getEncabezado() {
		return encabezado;
	}

	public void setEncabezado(final ArrayList<Object> encabezado) {
		this.encabezado = encabezado;
	}

	public ArrayList<Object> getLista() {
		return lista;
	}

	public void setLista(final ArrayList<Object> lista) {
		this.lista = lista;
	}

	public BaseDelegate getDelegate() {
		return delegate;
	}

	public void setDelegate(final BaseDelegate delegate) {
		this.delegate = delegate;
	}

	private class LlenarTablas extends BaseAdapter {

		@Override
		public int getCount() {
			return lista.size();
		}

		@Override
		public Object getItem(final int position) {
			return lista.get(position);
		}

		@SuppressWarnings({ "unchecked" })
		@Override
		public View getView(final int position, View convertView, final ViewGroup parent) {
			final GuiTools guiTools = GuiTools.getCurrent();
			
			if (convertView == null) {
				convertView = inflater.inflate(SuiteAppAdmonApi.getResourceId("list_item_productos","layout"),
						null);
			}

			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(final View v) {
					if (textoAMostrar != null && !textoAMostrar.equals("")){
						return;
					}
					if (isSeleccionable()) {
						setOpcionSeleccionada(position);
						notifyDataSetChanged();
						if(informActionEvenIfSelectable)
							ejecutarOpcionDelegate();
						if(informActionEvenIfSelectable)
							ejecutarOpcionDelegate();
					} else if (!isSeleccionable()) {
						if (!ecFlag) {
							setOpcionSeleccionada(position);
							ejecutarOpcionDelegate();
							notifyDataSetChanged();
							//ejecutarOpcionDelegate();
						}
					}
				}
			});

			final TextView celda1 = (TextView) convertView.findViewById(SuiteAppAdmonApi.getResourceId("lista_celda_1","id"));
			final TextView celda2 = (TextView) convertView.findViewById(SuiteAppAdmonApi.getResourceId("lista_celda_2","id"));
			final TextView celda3 = (TextView) convertView.findViewById(SuiteAppAdmonApi.getResourceId("lista_celda_3","id"));
			final TextView celda4 = (TextView) convertView.findViewById(SuiteAppAdmonApi.getResourceId("lista_celda_4","id"));
			celda1.setSingleLine(singleLine);
			celda2.setSingleLine(singleLine);
			celda3.setSingleLine(singleLine);
			celda4.setSingleLine(singleLine);
			final ImageButton celda5 = (ImageButton) convertView.findViewById(SuiteAppAdmonApi.getResourceId("lista_celda_check","id"));
			//paperless
			final CheckBox celda6 = (CheckBox) convertView.findViewById(R.id.chkState);
			final LinearLayout Layoutcelda6 = (LinearLayout) convertView.findViewById(R.id.LayoutChk);
			//if (opcionSeleccionada == position) {
			if(!fijarLista){
				if (opcionSeleccionada == position) {
				celda1.setTextColor(getResources().getColor(R.color.tercer_azul));
				celda2.setTextColor(getResources().getColor(R.color.tercer_azul));
				celda3.setTextColor(getResources().getColor(R.color.tercer_azul));
				celda4.setTextColor(getResources().getColor(R.color.tercer_azul));
				
				celda5.setImageResource(R.drawable.flecha_lista_seleccion);
				
				
			} else {
				celda1.setTextColor(getResources().getColor(R.color.gris_texto_1));
				celda2.setTextColor(getResources().getColor(R.color.gris_texto_1));
				celda3.setTextColor(getResources().getColor(R.color.gris_texto_1));
				celda4.setTextColor(getResources().getColor(R.color.gris_texto_1));
				celda5.setImageResource(0);
			}
		}
			if (!isSeleccionable()) {
				if(getNumeroColumnas()==5){
					
					celda5.setVisibility(View.VISIBLE);
				} else
								celda5.setVisibility(View.GONE);
			}else {
				if(!scaled) {
//					GuiTools.getCurrent().scale(celda5);
					final LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams)celda5.getLayoutParams();
					final int next = (int)(22.22 * GuiTools.getScaleFactor());
					params2.height = next;
					params2.topMargin = (int)(1.0 * GuiTools.getScaleFactor());
				
				//celda5.setLayoutParams(params);
				celda5.setScaleType(ScaleType.CENTER_INSIDE);
				}
			}
			if (getNumeroColumnas() > 4) {
				//setNumeroColumnas(4);
				if(!isSeleccionable())
					setNumeroColumnas(getNumeroColumnas());
				else
					setNumeroColumnas(4);
			}
			if (getNumeroColumnas() == 1) {
				params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[0] : 1.0f);
				final ArrayList<Object> registro = (ArrayList<Object>) lista
						.get(position);

				celda1.setText((String) registro.get(1));
				celda1.setLayoutParams(params);
				celda1.setGravity((textoAMostrar != null && !textoAMostrar.equals("")) ?Gravity.CENTER_HORIZONTAL : Gravity.LEFT);
//				celda1.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda1.setSelected(marqueeEnabled);

				celda2.setVisibility(View.GONE);
				celda3.setVisibility(View.GONE);
				celda4.setVisibility(View.GONE);
				
				celda1.setMaxLines(1);
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				guiTools.tryScaleText(celda1);


			} else if (getNumeroColumnas() == 2) {
				params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[0] : anchoColumna);
				final ArrayList<Object> registro = (ArrayList<Object>) lista
						.get(position);

                //modificado para pantalla operacion exitosa paperles
                if (registro.size() == 2)
                    celda1.setText((String) registro.get(0));
                else
                    celda1.setText((String) registro.get(1));
                celda1.setLayoutParams(params);
                celda1.setGravity(Gravity.LEFT);
//				celda1.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda1.setSelected(marqueeEnabled);

                params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[1] : anchoColumna);

                //modificado para pantalla operacion exitosa paperles
                if (registro.size() == 2) {
                    celda2.setText((String) registro.get(1));
                    celda2.setTextColor(getResources().getColor(R.color.tercer_azul));
                } else
                    celda2.setText((String) registro.get(2));
                celda2.setLayoutParams(params);
                celda2.setGravity(Gravity.RIGHT);
//				celda2.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda2.setSelected(marqueeEnabled);

				celda3.setVisibility(View.GONE);
				celda4.setVisibility(View.GONE);
				
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				
				guiTools.tryScaleText(celda1);
				guiTools.tryScaleText(celda2);


			} else if (getNumeroColumnas() == 3) {
//                ArrayList<Object> registro = (ArrayList<Object>) lista.get(position);

				//Paperless Este es el fragmento de código para el caso de el checkbox.
				if (ecFlag) {
					final ArrayList<Object> registro = (ArrayList<Object>) lista
							.get(position);
					check = (CheckBox) findViewById(R.id.chkState);
					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[0] : anchoColumna);
//				params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[0] : 0.3f);
					celda1.setText((String) registro.get(1));
					celda1.setLayoutParams(params);
					celda1.setGravity(Gravity.LEFT);
//				celda1.setTextAppearance(getContext(), R.style.movimientos_celda_style);
					celda1.setSelected(marqueeEnabled);

					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[1] : anchoColumna);
//	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[1] : 0.2f);
					celda2.setText((String) registro.get(2));
					celda2.setLayoutParams(params);
					celda2.setGravity(Gravity.CENTER);
//				celda2.setTextAppearance(getContext(), R.style.movimientos_celda_style);
					celda2.setSelected(marqueeEnabled);

					celda3.setVisibility(View.GONE);
					celda4.setVisibility(View.GONE);
					celda5.setVisibility(View.GONE);

//hcf paperless                    params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[2] : anchoColumna);
////	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[2] : 0.7f - anchoColumna);
//                    celda3.setText((String) registro.get(3));
//                    celda3.setLayoutParams(params);
//                    celda3.setGravity(Gravity.RIGHT);
////				celda3.setTextAppearance(getContext(), R.style.movimientos_celda_style);
//                    celda3.setSelected(marqueeEnabled);
//
//                    celda4.setVisibility(View.GONE);
//hcf paperless                    celda5.setVisibility(View.GONE);//paperless

					//inicia modif paperless
					Layoutcelda6.setVisibility(View.VISIBLE);
					celda6.setVisibility(View.VISIBLE);
					Layoutcelda6.setLayoutParams(params);
					celda6.setLayoutParams(params);
					celda6.setGravity(Gravity.RIGHT);

					//modificacion Envio de estado de cuentas, tabla cuentas "sin cuentas"
					if (registro.get(2).equals("")) {
						celda1.setTextColor(getResources().getColor(R.color.gris_texto_4));
						celda6.setVisibility(View.GONE);
					} else if (registro.get(2).equals("Activado"))
						celda6.setChecked(true);
					else {
						celda6.setChecked(false);
					}
					celda6.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(final View v) {
							final ArrayList<CuentaEC> aux = Session.getListaEC();
							aux.get(position).setFlag(aux.get(position).isFlag() == true ? false : true);
							//is chkIos checked? /**MODIF 2904**/

							/*
                            if (!((CheckBox) v).isChecked()) {
								celda2.setText("Activado");
								Log.e("before",aux.get(position).getEstadoEnvio().toString());
								aux.get(position).setEstadoEnvio("D");
								Session.setListaEC(aux);
								Log.e("now",aux.get(position).getEstadoEnvio().toString());
								}
							else
							{
								celda2.setText("Suspendido");
								Log.e("before",aux.get(position).getEstadoEnvio().toString());
								aux.get(position).setEstadoEnvio("I");
								Session.setListaEC(aux);
								Log.e("now",aux.get(position).getEstadoEnvio().toString());
							} */

							if (((CheckBox) v).isChecked()) {
								if (Server.ALLOW_LOG) Log.e("before", aux.get(position).getEstadoEnvio().toString());
								if (celda2.getText().equals("Activado")) {
									celda2.setText("Suspendido");
									aux.get(position).setEstadoEnvio("I");

								} else {
									celda2.setText("Activado");
									aux.get(position).setEstadoEnvio("D");
								}

								getConsultarEstatusEnvio().setContenedorAviso();
								Session.setListaEC(aux);
								if (Server.ALLOW_LOG) Log.e("now", aux.get(position).getEstadoEnvio().toString());
							} else {
								/*celda2.setText("Suspendido");
								Log.e("before",aux.get(position).getEstadoEnvio().toString());
								aux.get(position).setEstadoEnvio("I");
								Session.setListaEC(aux);
								Log.e("now",aux.get(position).getEstadoEnvio().toString());*/
								//Log.e("else","ok");

								if (Server.ALLOW_LOG) Log.e("before", aux.get(position).getEstadoEnvio().toString());
								if (celda2.getText().equals("Activado")) {
									celda2.setText("Suspendido");
									aux.get(position).setEstadoEnvio("I");
								} else {
									celda2.setText("Activado");
									aux.get(position).setEstadoEnvio("D");
								}

								getConsultarEstatusEnvio().setContenedorAviso();
								Session.setListaEC(aux);
								if (Server.ALLOW_LOG) Log.e("now", aux.get(position).getEstadoEnvio().toString());
							}

						}
					});
					//fin modif paperless


					celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);

					guiTools.tryScaleText(celda1);
					guiTools.tryScaleText(celda2);
					guiTools.tryScaleText(celda3);

				} else {
					final ArrayList<Object> registro = (ArrayList<Object>) lista.get(position);

					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[0] : anchoColumna);
					//				params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[0] : 0.3f);
					celda1.setText((String) registro.get(1));
					celda1.setLayoutParams(params);
					celda1.setGravity(Gravity.LEFT);
					//				celda1.setTextAppearance(getContext(), R.style.movimientos_celda_style);
					celda1.setSelected(marqueeEnabled);

					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[1] : anchoColumna);
					//	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[1] : 0.2f);
					celda2.setText((String) registro.get(2));
					celda2.setLayoutParams(params);
					celda2.setGravity(Gravity.CENTER);
					//				celda2.setTextAppearance(getContext(), R.style.movimientos_celda_style);
					celda2.setSelected(marqueeEnabled);

					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[2] : anchoColumna);
					//	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[2] : 0.7f - anchoColumna);
					celda3.setText((String) registro.get(3));
					celda3.setLayoutParams(params);
					celda3.setGravity(Gravity.RIGHT);
					//				celda3.setTextAppearance(getContext(), R.style.movimientos_celda_style);
					celda3.setSelected(marqueeEnabled);

					celda4.setVisibility(View.GONE);

					celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);

					guiTools.tryScaleText(celda1);
					guiTools.tryScaleText(celda2);
					guiTools.tryScaleText(celda3);
				}
			} else if (getNumeroColumnas() == 4) {
				final ArrayList<Object> registro = (ArrayList<Object>) lista
						.get(position);

				params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[0] : anchoColumna);
				celda1.setText((String) registro.get(1));
				celda1.setLayoutParams(params);
				celda1.setGravity(Gravity.LEFT);
//				celda1.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda1.setSelected(marqueeEnabled);

	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[1] : anchoColumna);
				celda2.setText((String) registro.get(2));
				celda2.setLayoutParams(params);
				celda2.setGravity(Gravity.CENTER);
//				celda2.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda2.setSelected(marqueeEnabled);

	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[2] : anchoColumna);
				celda3.setText((String) registro.get(3));
				celda3.setLayoutParams(params);
				celda3.setGravity(Gravity.CENTER);
//				celda3.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda3.setSelected(marqueeEnabled);
	            

				params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[4] : anchoColumna);
	            celda4.setText((String) registro.get(4));
				celda4.setLayoutParams(params);
				celda4.setGravity(Gravity.RIGHT);
//				celda4.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda4.setSelected(marqueeEnabled);
				
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda4.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);

				guiTools.tryScaleText(celda1);
				guiTools.tryScaleText(celda2);
				guiTools.tryScaleText(celda3);
				guiTools.tryScaleText(celda4);
				}
				
				else if (getNumeroColumnas() == 5) {
				if(!seleccionable){
					final ArrayList<Object> registro = (ArrayList<Object>) lista
							.get(position);
	
					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
							overrideColumnWidth ? overridedColumnWidths[0]
									: anchoColumna);
//					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
//							3);
					celda1.setText((String) registro.get(1));
					celda1.setLayoutParams(params);
					celda1.setGravity(Gravity.LEFT);
					// celda1.setTextAppearance(getContext(),
					// R.style.movimientos_celda_style);
					celda1.setSelected(marqueeEnabled);
	
					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
							overrideColumnWidth ? overridedColumnWidths[1]
									: anchoColumna);
//					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
//							2);
					celda2.setText((String) registro.get(2));
					celda2.setLayoutParams(params);
					celda2.setGravity(Gravity.CENTER);
					// celda2.setTextAppearance(getContext(),
					// R.style.movimientos_celda_style);
					celda2.setSelected(marqueeEnabled);
	
					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
							overrideColumnWidth ? overridedColumnWidths[2]
									: anchoColumna);
//					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
//							2);
					celda3.setText((String) registro.get(3));
					celda3.setLayoutParams(params);
					celda3.setGravity(Gravity.CENTER);
					// celda3.setTextAppearance(getContext(),
					// R.style.movimientos_celda_style);
					celda3.setSelected(marqueeEnabled);
	
					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
							overrideColumnWidth ? overridedColumnWidths[4]
									: anchoColumna);
//					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
//							3);
					celda4.setText((String) registro.get(4));
					celda4.setLayoutParams(params);
					celda4.setGravity(Gravity.RIGHT);
					// celda4.setTextAppearance(getContext(),
					// R.style.movimientos_celda_style);
					celda4.setSelected(marqueeEnabled);
					
					
					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, anchoColumna);
//					params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					celda5.setLayoutParams(params);
					final String estado=(String)registro.get(5);
					if(estado!=null){
						if(estado.equalsIgnoreCase("201")||estado.equalsIgnoreCase("205")){
							celda5.setImageResource(R.drawable.an_punto_naranja);
						}else if(estado.equalsIgnoreCase("206")){
							celda5.setImageResource(R.drawable.an_flecha_azul);
						}else if(estado.equalsIgnoreCase("207")){
							celda5.setImageResource(R.drawable.an_flecha_verde);
						}else if(estado.equalsIgnoreCase("301")){
							celda5.setImageResource(R.drawable.an_cruz_magenta);
						}
					}
					
					
					celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda4.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
	
					guiTools.tryScaleText(celda1);
					guiTools.tryScaleText(celda2);
					guiTools.tryScaleText(celda3);
					guiTools.tryScaleText(celda4);
					guiTools.scale(celda5);
				}				
				
			}
			return convertView;
		}

		@Override
		public long getItemId(final int position) {
			// TODO Auto-generated method stub
			return 0;
		}
	}

	public int getOpcionSeleccionada() {
		return opcionSeleccionada;
	}
	
	@Override
	public boolean onTouchEvent(final MotionEvent event) {
		if(Server.ALLOW_LOG) Log.e(getClass().getCanonicalName(), "onTouchEvent");
		if(MotionEvent.ACTION_MOVE == event.getAction()){		
		parentManager.getCurrentViewControllerApp().getParentViewsController().onUserInteraction();
		}
		return super.onTouchEvent(event);
	}

	public void setMarqueeEnabled(final boolean enabled) {
		findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_1","id")).setSelected(enabled);
		findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_2","id")).setSelected(enabled);
		findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_3","id")).setSelected(enabled);
		findViewById(SuiteAppAdmonApi.getResourceId("lista_seleccion_cabecera_celda_4","id")).setSelected(enabled);
		
		marqueeEnabled = enabled;
		
		if(Server.ALLOW_LOG) Log.d("ListaSeleccion", "marqueeEnabled set to: " + marqueeEnabled);
	}
	
	private boolean marqueeEnabled;

	/**
	 * @return the numeroFilas
	 */
	public int getNumeroFilas() {
		return numeroFilas;
	}

	/**
	 * @param numeroFilas the numeroFilas to set
	 */
	public void setNumeroFilas(final int numeroFilas) {
		this.numeroFilas = numeroFilas;
	}
	
	// #region Override Column Widths
	/**
	 * Bandera que indica si se deben de ignorar los valores predeterminados de anchos de columna.
	 */
	private boolean overrideColumnWidth;
	
	/**
	 * @return Bandera que indica si se deben de ignorar los valores predeterminados de anchos de columna.
	 */
	public boolean isOverrideColumnWidth() {
		return overrideColumnWidth;
	}

	/**
	 * @param overrideColumnWidth Bandera que indica si se deben de ignorar los valores predeterminados de anchos de columna.
	 */
	public void setOverrideColumnWidth(final boolean overrideColumnWidth) {
		this.overrideColumnWidth = overrideColumnWidth;
	}

	/**
	 * Arreglo de flotantes para definir un ancho de columnas personalizado.
	 */
	private float[] overridedColumnWidths;

	/**
	 * @return Arreglo de flotantes para definir un ancho de columnas personalizado.
	 */
	public float[] getOverridedColumnWidths() {
		return overridedColumnWidths;
	}

	/**
	 * @param overridedColumnWidths Arreglo de flotantes para definir un ancho de columnas personalizado.
	 */
	public void setOverridedColumnWidths(final float[] overridedColumnWidths) {
		if(null == overridedColumnWidths || 0 == overridedColumnWidths.length || 4 < overridedColumnWidths.length) {
			overrideColumnWidth = false;
		} else {
			this.overridedColumnWidths = overridedColumnWidths;
			this.setNumeroColumnas(overridedColumnWidths.length);
			overrideColumnWidth = true;
		}
	}
	// #endregion

	public int getNumeroColumnas() {
		return numeroColumnas;
	}

	public void setNumeroColumnas(final int numeroColumnas) {
		this.numeroColumnas = numeroColumnas;
	}

	public void setOpcionSeleccionada(int opcionSeleccionada) {
		this.opcionSeleccionada = opcionSeleccionada;
	}

	public boolean isSeleccionable() {
		return seleccionable;
	}

	public void setSeleccionable(final boolean seleccionable) {
		this.seleccionable = seleccionable;
	}

	public boolean isAlturaFija() {
		return alturaFija;
	}

	public void setAlturaFija(final boolean alturaFija) {
		this.alturaFija = alturaFija;
	}

	public boolean isExisteFiltro() {
		return existeFiltro;
	}

	public void setExisteFiltro(final boolean existeFiltro) {
		this.existeFiltro = existeFiltro;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

        public FiltroListaViewController getFiltroLista() {
            return filtroLista;
        }

        public void setFiltroLista(final FiltroListaViewController filtroLista) {
            this.filtroLista = filtroLista;
        }

        //SPEI
        private boolean informActionEvenIfSelectable = false;

	/**
	 * @return the informActionEvenIfSelectable
	 */
	public boolean isInformActionEvenIfSelectable() {
		return informActionEvenIfSelectable;
	}

	/**
	 * @param informActionEvenIfSelectable the informActionEvenIfSelectable to set
	 */
	public void setInformActionEvenIfSelectable(final boolean informActionEvenIfSelectable) {
		this.informActionEvenIfSelectable = informActionEvenIfSelectable;
	}	
	
	public boolean isFijarLista() {
		return fijarLista;
	}

	public void setFijarLista(final boolean fijarLista) {
		this.fijarLista = fijarLista;
	}

}
