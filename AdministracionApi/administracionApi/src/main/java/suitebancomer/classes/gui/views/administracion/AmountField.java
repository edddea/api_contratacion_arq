/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.classes.gui.views.administracion;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;



/**
 * EditText component that will format automatically the currency units entered
 * by the user in a format that contain cents.
 * 
 * @author Stefanini IT Solutions.
 */

public class AmountField extends EditText {

	/**
     * String auxiliary  for the amount
     */
    private String amountString=null;
    
    /**
     * The typed characters that represent the server amount (with or without cents)
     */
    private StringBuffer typedString= new StringBuffer();
    
    /**
     * True when the text is being formatted so it will be the final placement.
     */
    private boolean isSettingText = false;
    
    /**
     * True whenever a reset() call is being processed.
     */
    private boolean isResetting = false;
  //One Click
    private boolean mAcceptCents=false;

	/**
	 * Make a new AmountField Object.
	 * 
	 * @param context
	 *            the context of the field
	 * @param attrs
	 *            attributes for the view
	 */
	public AmountField(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		this.reset();
		setCursorVisible(true);
		
		
		/*Validation keypad*/
		this.addTextChangedListener(new TextWatcher(){
	    
			/**
			 * Save the value of string before changing the text 
			 */
			public void  beforeTextChanged  (final CharSequence s, final int start, final int count, final int after){
				final String amountField=AmountField.super.getText().toString();
	            if(!isSettingText){
	            	amountString=amountField;   
	            }    	               
	        }
			
			/**
			 * When the value string has changed
			 */
            public void  onTextChanged  (final CharSequence s, final int start, final int before, final int count){

				final String amountField= AmountField.super.getText().toString();

            	if(!isSettingText && !isResetting){    	
	            	if(length() < amountString.length()){
	            		if(typedString.length() > 0){
	            			typedString.deleteCharAt(typedString.length()-1);
	            			setFormattedText();
	            		} else {
	            			reset();
	            		}
	            	} else if (length() > amountString.length()){
	            		
	            		int newCharIndex = getSelectionEnd()-1;
	            		//there was no selection in the field
	            		if(newCharIndex == -2 ){
	            			newCharIndex = length()-1;
	            		}

						final char num = amountField.charAt(newCharIndex);
	            		if(!(num=='0' && typedString.length() == 0)){
	            			typedString.append(num);
	            		}
	            		setFormattedText();
	            	}
            	}
            }
            
			public void afterTextChanged(final Editable s) {
				isSettingText = false;
				setSelection(length());
			}
		}); /*end keypad*/
	}
	
	/**
	 * Common logic to set the formatted text to the text field. It contemplates
	 * whether the field accepts cents in the amount or not.
	 */
	private void setFormattedText(){
		isSettingText = true;
		String text = typedString.toString();
		/*
		if(!mAcceptCents){
			text += "00";
		}
		*/
		//One click
		if(mAcceptCents){
			text += "00";
		}
		//Termina One click
		setText(Tools.formatAmountFromServer(text));
	}
	
	public void setAmount(final String amount) {
		isSettingText = true;
		setText(Tools.formatAmountFromServer(amount));
	}
	//One click
			public void setAmountEFI(final String amount) {
				mAcceptCents= true;
				isSettingText = true;
				setText(Tools.formatAmountFromServer(amount));
			}
			
			//Termina One click
	
	/**
	 * Returns the string representation of the amount.
	 */
	public String getAmount(){
		return super.getText().toString().replace(",", "");
	}
	
	/**
	 * Determines if the current amount is zero, in other words, if the user
	 * has not typed any amount in the field.
	 * @return true if the amount is different from 0.00, false otherwise.
	 */
	public boolean isEmpty(){
		return typedString.length() == 0;
	}
	
	
	/**
	 * Resets the value to default amount.
	 */
	public void reset() {
		this.isResetting = true;
		this.typedString=new StringBuffer();
		this.setText("0.00");
		this.isResetting = false;
	}
	

	
}
