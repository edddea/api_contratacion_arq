package suitebancomer.classes.common.administracion;

import android.app.Application;
import android.content.Context;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ScaleTool extends Application {
	
	private float BASE_RESOLUTION = 0;
	private float ACTUAL_RESOLUTION;
	private int DISPLAY_WIDTH = 0;
	private int DISPLAY_HEIGHT = 0;
	
	public static float SCALING_PERCENTAGE = 0F;
	public static float SCALE_FACTOR = 0F;
	
	private WindowManager windowManager;
	private Display display;
	
	public ScaleTool(final float BASE_RESOLUTION, final float ACTUAL_RESOLUTION, final Context context) {
		
		windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		display = windowManager.getDefaultDisplay();
		
		DISPLAY_WIDTH = display.getWidth();
		DISPLAY_HEIGHT = display.getHeight();
		
		this.BASE_RESOLUTION = BASE_RESOLUTION;
		this.ACTUAL_RESOLUTION = ACTUAL_RESOLUTION;
		
		getScales();
	}
	
	private void getScales() {
		SCALING_PERCENTAGE = BASE_RESOLUTION / ACTUAL_RESOLUTION;
		SCALE_FACTOR = DISPLAY_WIDTH / this.BASE_RESOLUTION;
	}
	
	public void scale(final View view, int width, int height) {
		
		width = getScaledWidth(width);
		height = getScaledHeight(height);

		if (view instanceof RelativeLayout) {
			scaleRelativeLayoutParams(width, height, view);
		}
		else if (view instanceof LinearLayout) {
			scaleLinearLayoutParams(width, height, view);
		}
		else if (view instanceof ImageView) {
			final ImageView imageView = (ImageView) view;
			width = getScaledWidth(imageView.getDrawable().getIntrinsicWidth());
			height = getScaledHeight(imageView.getDrawable().getIntrinsicHeight());
			try {
				scaleRelativeLayoutParams(width, height, imageView);
			}
			catch (ClassCastException CCE) {
				scaleLinearLayoutParams(width, height, imageView);
			}
		}
	}
	
	public void fixScale(final View view, final int width, final int height) {
		try {
			scaleRelativeLayoutParams(width, height, view);
		}
		catch (ClassCastException CCE) {
			scaleLinearLayoutParams(width, height, view);
		}
	}
	
	public void scale(final View view) {
		int width = 0;
		int height = 0;
		int textSize = 0;
		
		if (view instanceof ImageView) {
			final ImageView imageView = (ImageView) view;
			
			width = getScaledWidth(imageView.getDrawable().getIntrinsicWidth());
			height = getScaledHeight(imageView.getDrawable().getIntrinsicHeight());
		}
		
		else if (view instanceof ImageButton) {
			final ImageButton imageButton = (ImageButton) view;
			
			width = getScaledWidth(imageButton.getDrawable().getIntrinsicWidth());
			height = getScaledHeight(imageButton.getDrawable().getIntrinsicHeight());
		}
		
		else if (view instanceof TextView) {
			final TextView textView = (TextView) view;
			textSize = (int) (getScaledSize((int)textView.getTextSize()) * SCALE_FACTOR);
			((TextView) view).setTextSize(textSize);
			return;
		}
		
		else if (view instanceof RelativeLayout) {

			final RelativeLayout relativeLayout = (RelativeLayout) view;
			width = relativeLayout.getLayoutParams().width;
			height = view.getHeight();
			android.util.Log.i("Layout Width", width + ".");
			android.util.Log.i("Layout Height", height + ".");
			
		}
		
		try {
			scaleRelativeLayoutParams(width, height, view);
		}
		catch (ClassCastException CCE) {
			scaleLinearLayoutParams(width, height, view);
		}
		
	}
	
	public void fixScale(final View view) {
		int width = 0;
		int height = 0;
		int textSize = 0;

		if (view instanceof ImageView) {
			final ImageView imageView = (ImageView) view;
			
			width = imageView.getDrawable().getIntrinsicWidth();
			height = imageView.getDrawable().getIntrinsicHeight();
			
		}
		
		else if (view instanceof ImageButton) {
			final ImageButton imageButton = (ImageButton) view;
			
			width = imageButton.getDrawable().getIntrinsicWidth();
			height = imageButton.getDrawable().getIntrinsicHeight();
		}
		
		else if (view instanceof TextView) {
			final TextView textView = (TextView) view;
			textSize = (int) (textView.getTextSize());
			android.util.Log.i("TextSize", textSize + "");
			textSize = (int) (textSize / SCALE_FACTOR);
			android.util.Log.i("TextSize", textSize + "");
			((TextView) view).setTextSize(textSize);
			return;
		}
		
		else if (view instanceof RelativeLayout) {

			final RelativeLayout relativeLayout = (RelativeLayout) view;
			width = relativeLayout.getLayoutParams().width;
			height = view.getHeight();
			android.util.Log.i("Layout Width", width + ".");
			android.util.Log.i("Layout Height", height + ".");
			
		}
		
		try {
			scaleRelativeLayoutParams(width, height, view);
		}
		catch (ClassCastException CCE) {
			scaleLinearLayoutParams(width, height, view);
		}
		
	}
	
	public void scaleWidth(final View view) {
		int width = 10;
		
		if (view instanceof ImageView) {
			final ImageView imageView = (ImageView) view;
			width = getScaledHeight(imageView.getDrawable().getIntrinsicWidth());
		}
		
		else if (view instanceof ImageButton) {
			final ImageButton imageButton = (ImageButton) view;
			width = getScaledHeight(imageButton.getDrawable().getIntrinsicWidth());
		}
		
		else if (view instanceof RelativeLayout) {
			final RelativeLayout relativeLayout = (RelativeLayout) view;
			final RelativeLayout.LayoutParams relativeLayoutParams = (RelativeLayout.LayoutParams) relativeLayout.getLayoutParams();
			width = relativeLayoutParams.width;
		}
		
		else if (view instanceof LinearLayout) {
			final LinearLayout linearLayout = (LinearLayout) view;
			final LinearLayout.LayoutParams lineLayoutParams = (LinearLayout.LayoutParams) linearLayout.getLayoutParams();
			width = lineLayoutParams.width;
		}
		
		try {
			scaleRelativeWidth(width, view);
		}
		catch (ClassCastException CCE) {
			scaleLinearWidth(width, view);
		}
		
	}
	
	public void scaleWidth(final View view, final int width) {
		try {
			scaleRelativeWidth(getScaledWidth(width), view);
		}
		catch (ClassCastException CCE) {
			try {
				scaleLinearWidth(getScaledWidth(width), view);
			}
			catch (ClassCastException CE) {
				scaleFrameWidth(getScaledWidth(width), view);
			}
		}
		
	}
	
	public void fixScaleWidth(final View view) {
		int width = 0;
		
		if (view instanceof ImageView) {
			final ImageView imageView = (ImageView) view;

			width = imageView.getDrawable().getIntrinsicWidth();
		}
		
		else if (view instanceof ImageButton) {
			final ImageButton imageButton = (ImageButton) view;

			width = imageButton.getDrawable().getIntrinsicWidth();
		}
		
		try {
			scaleRelativeWidth(width, view);
		}
		catch (ClassCastException CCE) {
			try {
				scaleLinearWidth(width, view);
			}
			catch (ClassCastException CE) {
				scaleFrameWidth(width, view);
			}
		}
		
	}
	
	public void fixScaleWidth(final View view, final int width) {
		try {
			scaleRelativeWidth(width, view);
		}
		catch (ClassCastException CCE) {
			try {
				scaleLinearWidth(width, view);
			}
			catch (ClassCastException CE) {
				scaleFrameWidth(width, view);
			}
		}
		
	}
	
	public void scaleHeight(final View view) {
		int height = 0;
		
		if (view instanceof ImageView) {
			final ImageView imageView = (ImageView) view;

			height = getScaledHeight(imageView.getDrawable().getIntrinsicHeight());
		}
		
		else if (view instanceof ImageButton) {
			final ImageButton imageButton = (ImageButton) view;

			height = getScaledHeight(imageButton.getDrawable().getIntrinsicHeight());
		}
		
		else if (view instanceof RelativeLayout) {

		}
		
		try {
			scaleRelativeHeight(height, view);
		}
		catch (ClassCastException CCE) {
			scaleLinearHeight(height, view);
		}
		
	}
	
	public void scaleHeight(final View view, final int height) {
		try {
			scaleRelativeHeight(getScaledHeight(height), view);
		}
		catch (ClassCastException CCE) {
			scaleLinearHeight(getScaledHeight(height), view);
		}
		
	}
	
	public void fixScaleHeight(final View view) {
		int height = 0;
		
		if (view instanceof ImageView) {
			final ImageView imageView = (ImageView) view;

			height = imageView.getDrawable().getIntrinsicHeight();
		}
		
		else if (view instanceof ImageButton) {
			final ImageButton imageButton = (ImageButton) view;

			height = imageButton.getDrawable().getIntrinsicHeight();
		}
		
		else if (view instanceof RelativeLayout) {

		}
		
		try {
			scaleRelativeHeight(height, view);
		}
		catch (ClassCastException CCE) {
			scaleLinearHeight(height, view);
		}
		
	}
	
	public void fixScaleHeight(final View view, final int height) {
		try {
			scaleRelativeHeight(height, view);
		}
		catch (ClassCastException CCE) {
			try {
				scaleLinearHeight(height, view);
			}
			catch (ClassCastException CE) {
				scaleFrameHeight(height, view);
			}
		}
	}

	public void scaleMarginTop(final View view, final int marginTop) {
		try {
			scaleRelativeMarginTop(getScaledSize(marginTop), view);
		}
		catch (ClassCastException CCE) {
			scaleLinearMarginTop(getScaledSize(marginTop), view);
		}
	}
	
	public void scaleMarginBottom(final View view, final int marginBottom) {
		try {
			scaleRelativeMarginBottom(getScaledSize(marginBottom), view);
		}
		catch (ClassCastException CCE) {
			scaleLinearMarginBottom(getScaledSize(marginBottom), view);
		}
	}
	
	public void scaleMarginLeft(final View view, final int marginLeft) {
		try {
			scaleRelativeMarginLeft(getScaledSize(marginLeft), view);
		}
		catch (ClassCastException CCE) {
			scaleLinearMarginLeft(getScaledSize(marginLeft), view);
		}
	}
	
	public void scaleMarginRight(final View view, final int marginRight) {
		try {
			scaleRelativeMarginRight(getScaledSize(marginRight), view);
		}
		catch (ClassCastException CCE) {
			scaleLinearMarginRight(getScaledSize(marginRight), view);
		}
	}
	
	public void fixScaleMarginTop(final View view, final int marginTop) {
		try {
			scaleRelativeMarginTop(marginTop, view);
		}
		catch (ClassCastException CCE) {
			scaleLinearMarginTop(marginTop, view);
		}
	}
	
	public void fixScaleMarginBottom(final View view, final int marginBottom) {
		try {
			scaleRelativeMarginBottom(marginBottom, view);
		}
		catch (ClassCastException CCE) {
			scaleLinearMarginBottom(marginBottom, view);
		}
	}
	
	public void fixScaleMarginLeft(final View view, final int marginLeft) {
		try {
			scaleRelativeMarginLeft(marginLeft, view);
		}
		catch (ClassCastException CCE) {
			scaleLinearMarginLeft(marginLeft, view);
		}
	}
	
	public void fixScaleMarginRight(final View view, final int marginRight) {
		try {
			scaleRelativeMarginRight(marginRight, view);
		}
		catch (ClassCastException CCE) {
			scaleLinearMarginRight(marginRight, view);
		}
	}
	
	private void scaleRelativeMarginTop(int marginTop, final View view) {
		final RelativeLayout.LayoutParams relativeLayoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
		marginTop = (int) (SCALE_FACTOR * marginTop);
		relativeLayoutParams.setMargins(relativeLayoutParams.leftMargin, marginTop, relativeLayoutParams.rightMargin, relativeLayoutParams.bottomMargin);
		android.util.Log.i("Margins", "L = " + relativeLayoutParams.leftMargin + "\nR = " + relativeLayoutParams.rightMargin + "\nT = " + relativeLayoutParams.topMargin + "\nB = " + relativeLayoutParams.bottomMargin);
		view.setLayoutParams(relativeLayoutParams);
	}
	
	private void scaleLinearMarginTop(int marginTop, final View view) {
		final LinearLayout.LayoutParams linearLayoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
		marginTop = (int) (SCALE_FACTOR * marginTop);
		linearLayoutParams.setMargins(linearLayoutParams.leftMargin, marginTop, linearLayoutParams.rightMargin, linearLayoutParams.bottomMargin);
		view.setLayoutParams(linearLayoutParams);
	}
	
	private void scaleRelativeMarginBottom(int marginBottom, final View view) {
		final RelativeLayout.LayoutParams relativeLayoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
		marginBottom = (int) (SCALE_FACTOR * marginBottom);
		relativeLayoutParams.setMargins(relativeLayoutParams.leftMargin, relativeLayoutParams.topMargin, relativeLayoutParams.rightMargin, marginBottom);
		android.util.Log.i("Margins", "L = " + relativeLayoutParams.leftMargin + "\nR = " + relativeLayoutParams.rightMargin + "\nT = " + relativeLayoutParams.topMargin + "\nB = " + relativeLayoutParams.bottomMargin);
		view.setLayoutParams(relativeLayoutParams);
	}
	
	private void scaleLinearMarginBottom(int marginBottom, final View view) {
		final LinearLayout.LayoutParams linearLayoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
		marginBottom = (int) (SCALE_FACTOR * marginBottom);
		linearLayoutParams.setMargins(linearLayoutParams.leftMargin, linearLayoutParams.topMargin, linearLayoutParams.rightMargin, marginBottom);
		view.setLayoutParams(linearLayoutParams);
	}
	
	private void scaleRelativeMarginLeft(int marginLeft, final View view) {
		final RelativeLayout.LayoutParams relativeLayoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
		marginLeft = (int) (SCALE_FACTOR * marginLeft);
		relativeLayoutParams.setMargins(marginLeft, relativeLayoutParams.topMargin, relativeLayoutParams.rightMargin, relativeLayoutParams.bottomMargin);
		android.util.Log.i("Margins", "L = " + relativeLayoutParams.leftMargin + "\nR = " + relativeLayoutParams.rightMargin + "\nT = " + relativeLayoutParams.topMargin + "\nB = " + relativeLayoutParams.bottomMargin);
		view.setLayoutParams(relativeLayoutParams);
	}
	
	private void scaleLinearMarginLeft(int marginLeft, final View view) {
		final LinearLayout.LayoutParams linearLayoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
		marginLeft = (int) (SCALE_FACTOR * marginLeft);
		linearLayoutParams.setMargins(marginLeft, linearLayoutParams.topMargin, linearLayoutParams.rightMargin, linearLayoutParams.bottomMargin);
		view.setLayoutParams(linearLayoutParams);
	}
	
	private void scaleRelativeMarginRight(int marginRight, final View view) {
		final RelativeLayout.LayoutParams relativeLayoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
		marginRight = (int) (SCALE_FACTOR * marginRight);
		relativeLayoutParams.setMargins(relativeLayoutParams.leftMargin, relativeLayoutParams.topMargin, marginRight, relativeLayoutParams.bottomMargin);
		android.util.Log.i("Margins", "L = " + relativeLayoutParams.leftMargin + "\nR = " + relativeLayoutParams.rightMargin + "\nT = " + relativeLayoutParams.topMargin + "\nB = " + relativeLayoutParams.bottomMargin);
		view.setLayoutParams(relativeLayoutParams);
	}
	
	private void scaleLinearMarginRight(int marginRight, final View view) {
		final LinearLayout.LayoutParams linearLayoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
		marginRight = (int) (SCALE_FACTOR * marginRight);
		linearLayoutParams.setMargins(linearLayoutParams.leftMargin, linearLayoutParams.topMargin, marginRight, linearLayoutParams.bottomMargin);
		view.setLayoutParams(linearLayoutParams);
	}
	
	private void scaleRelativeLayoutParams(final int width, final int height, final View view) {
		final RelativeLayout.LayoutParams relativeLayoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
		relativeLayoutParams.width = (int) (SCALE_FACTOR * width);
		relativeLayoutParams.height = (int) (SCALE_FACTOR * height);
		view.setLayoutParams(relativeLayoutParams);
	}
	
	private void scaleRelativeHeight(final int height, final View view) {
		final RelativeLayout.LayoutParams relativeLayoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
		relativeLayoutParams.height = (int) (SCALE_FACTOR * height);
		view.setLayoutParams(relativeLayoutParams);
	}
	
	private void scaleRelativeWidth(final int width, final View view) {
		final RelativeLayout.LayoutParams relativeLayoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
		relativeLayoutParams.width = (int) (SCALE_FACTOR * width);
		view.setLayoutParams(relativeLayoutParams);
	}
	
	private void scaleLinearLayoutParams(final int width, final int height, final View view) {
		final LinearLayout.LayoutParams linearLayoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
		linearLayoutParams.width = (int) (SCALE_FACTOR * width);
		linearLayoutParams.height = (int) (SCALE_FACTOR * height);
		view.setLayoutParams(linearLayoutParams);
	}
	
	private void scaleLinearHeight(final int height, final View view) {
		final LinearLayout.LayoutParams linearLayoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
		linearLayoutParams.height = (int) (SCALE_FACTOR * height);
		view.setLayoutParams(linearLayoutParams);
	}
	
	private void scaleLinearWidth(final int width, final View view) {
		final LinearLayout.LayoutParams linearLayoutParams = (LinearLayout.LayoutParams) view.getLayoutParams();
		linearLayoutParams.width = (int) (SCALE_FACTOR * width);
		view.setLayoutParams(linearLayoutParams);
	}
	
	private void scaleFrameHeight(final int height, final View view) {
		final FrameLayout.LayoutParams frameLayoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
		frameLayoutParams.height = (int) (SCALE_FACTOR * height);
		view.setLayoutParams(frameLayoutParams);
	}
	
	private void scaleFrameWidth(final int width, final View view) {
		final FrameLayout.LayoutParams frameLayoutParams = (FrameLayout.LayoutParams) view.getLayoutParams();
		frameLayoutParams.width = (int) (SCALE_FACTOR * width);
		view.setLayoutParams(frameLayoutParams);
	}

	private int getScaledWidth(final int width) {
		return (int) (width * SCALING_PERCENTAGE);
	}
	
	private int getScaledHeight(final int height) {
		return (int) (height * SCALING_PERCENTAGE);
	}
	
	private int getScaledSize(final int size) {
		return (int) (size * SCALING_PERCENTAGE);
	}
	
}
