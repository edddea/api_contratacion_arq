package suitebancomer.classes.gui.controllers.login;

import android.app.AlertDialog;



import com.bancomer.mbanking.login.R;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;

public class SuiteViewsController extends BaseViewsController {

	public void showContactanos() {
		final AlertDialog.Builder contactDialog = new AlertDialog.Builder(currentViewControllerApp);
		contactDialog.setTitle(currentViewControllerApp.getString(R.string.menuSuite_callTitle));
		
		if (Tools.hasPhoneAbility(currentViewControllerApp)) {
			contactDialog.setMessage(currentViewControllerApp.getString(R.string.menuSuite_callMessage));
			contactDialog.setPositiveButton(currentViewControllerApp.getString(R.string.menuSuite_callLocalNumberFormatted), (MenuSuiteViewController)currentViewControllerApp);
			contactDialog.setNeutralButton(currentViewControllerApp.getString(R.string.menuSuite_callAwayNumberFormatted), (MenuSuiteViewController)currentViewControllerApp);
			contactDialog.setNegativeButton(currentViewControllerApp.getString(R.string.common_cancel), (MenuSuiteViewController)currentViewControllerApp);
		} else {
			contactDialog.setMessage(currentViewControllerApp.getString(R.string.menuSuite_noCallMessage));
			contactDialog.setNeutralButton(currentViewControllerApp.getString(R.string.common_accept), null);
		}
		
		contactDialog.setCancelable(false);
		contactDialog.show();
	}
	
	public void showContratacionPendiente() {
		
		//TODO
	}
	
}
