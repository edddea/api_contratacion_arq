package suitebancomer.classes.gui.controllers.login;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bancomer.mbanking.login.R;
import com.bancomer.mbanking.login.SuiteAppLogin;

import suitebancomer.classes.gui.delegates.login.MigraInformativoDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

public class MigrainformativoController extends BaseViewController  implements View.OnClickListener{

    private MigraInformativoDelegate delegate;
    private Button btnAceptar;
    private  Boolean migraBasico=false;
    private TextView tvterminos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_migrainformativo_controller);
        delegate=new MigraInformativoDelegate(this);
        delegate.setParentController(this);
        Bundle b = this.getIntent().getExtras();
        if (b != null){
            migraBasico=(Boolean) b.get("realizaTransaccion");
        }

        init();
    }

    private void init() {
        btnAceptar=(Button)findViewById(R.id.bmovil_btn_aceptarmigrainformativo);
        btnAceptar.setOnClickListener(this);
        tvterminos=(TextView)findViewById(R.id.title_migrainformativoLink);
        tvterminos.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        if(view==btnAceptar){
            if(migraBasico){
                delegate.realizaTransaccionMigraBasico();
            }else{//
                //*llamar a softtoken*//
                SuiteAppLogin.getInstance().getBmovilApplication().getBmovilViewsController().showAutenticacionST();
            }
        }else if(view==tvterminos){
            //envia a termions y condiciones de st
            delegate.realizaTransaccionTerminosCondiciones();
        }
    }

    public void processNetworkResponse(int operationId, ServerResponse response) {
            delegate.analyzeResponse( operationId,  response);
    }

}
