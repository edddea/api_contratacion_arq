package suitebancomer.classes.gui.delegates.login;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;

import com.bancomer.mbanking.login.R;
import com.bancomer.mbanking.login.SuiteAppLogin;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.login.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import suitebancomer.classes.gui.controllers.login.BaseViewController;
import suitebancomer.classes.gui.controllers.login.MenuSuiteViewController;
import suitebancomer.classes.gui.controllers.login.MigrainformativoController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.CatalogoAutenticacionFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatusMantenimientoData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.LoginData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomercoms.classes.common.PropertiesManager;

public class MenuSuiteArqServiceDelegate extends BaseDelegate  {
	private final static String STORE_SESSION_LOGIN = "sessionLogin";
	private MenuSuiteViewController menuSuiteViewController;
	public MenuSuiteArqServiceDelegate() {

		menuSuiteViewController=new MenuSuiteViewController();
		menuSuiteViewController.setDelegate(this);
		bmovilViewsController =SuiteAppLogin.getInstance().getBmovilApplication().getBmovilViewsController();
	}


	public boolean isDisconnected(){
		final SuiteAppLogin suiteApp = SuiteAppLogin.getInstance();

		 final ConnectivityManager connectivity = (ConnectivityManager)
				 suiteApp.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

         if (connectivity != null){
            final NetworkInfo[] info = connectivity.getAllNetworkInfo();
             if (info != null){
                 for (int i = 0; i < info.length; i++){
                     if (info[i].getState() == NetworkInfo.State.CONNECTED){
                         return false;
                     }
                 }
             }
         }
         return true;
	}



	public MenuSuiteViewController getMenuSuiteViewController() {
		return menuSuiteViewController;
	}


	String currentPassword;
	String userCellPhone;
	public void login(final String userCellPhone,final  String password) {
		currentPassword = password;
		this.userCellPhone=userCellPhone;
		final Session session = Session.getInstance(SuiteAppLogin.appContext);


		session.setPassword(password);
		final Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, userCellPhone);
		//paramTable.put(Server.PASSWORD_PARAM, password);
		paramTable.put("password", password);
		paramTable.put("user", userCellPhone);
		paramTable.put("sc", "MB");
		String ium = session.getIum();
		if (Tools.isEmptyOrNull(ium)) {
			ium = Tools.buildIUM(userCellPhone, session.getSeed(),
					SuiteAppLogin.appContext);
			session.setIum(ium);
		}
		session.setUsername(userCellPhone);
		paramTable.put("ium", ium);

		doNetworkOperation(Server.LOGIN_OPERATION, paramTable, menuSuiteViewController,true, LoginData.class);
	}


	private void operationConsultaEstatus(){
		String autVersion = CatalogoAutenticacionFileManager.getCurrent().leerVersionArchivoCatalogoAutenticacion();
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(Server.TELEFONICAS_PARAM, "0");
		params.put(ServerConstants.VERSION_MIDLET, ServerConstants.APP_VERSION_CONSULTA);
		params.put(ServerConstants.NUMERO_TELEFONO, userCellPhone);
		params.put(Server.CAT_PARAM, Tools.isEmptyOrNull(autVersion) ? "0" : autVersion);

		List<String> listaEncriptar = Arrays.asList(
				ApiConstants.OPERACION, ServerConstants.NUMERO_TELEFONO,
				ServerConstants.VERSION_MIDLET, Server.TELEFONICAS_PARAM, Server.CAT_PARAM
		);
		CommContext.operacionCode = Server.CONSULTA_MANTENIMIENTO;
		CommContext.listaEncriptar = listaEncriptar;

		doNetworkOperation(Server.CONSULTA_MANTENIMIENTO, params, menuSuiteViewController, true,ConsultaEstatusMantenimientoData.class);
	}

	/**
	 * Obtiene la informaci�n necesaria de la respuesta del servidor
	 */
	private void llenarConsultaEstatus(ConsultaEstatusMantenimientoData serverResponse) {
		consultaEstatus=new ConsultaEstatus();
		consultaEstatus.setEstatus(serverResponse.getEstatusServicio());
		consultaEstatus.setPerfil(Tools.determinaPerfil(
				serverResponse.getPerfilCliente(),
				serverResponse.getTipoInstrumento(),
				serverResponse.getEstatusInstrumento()));
		consultaEstatus.setNombreCliente(serverResponse.getNombreCliente());
		consultaEstatus.setInstrumento(serverResponse.getTipoInstrumento());
		consultaEstatus.setEstatusInstrumento(serverResponse.getEstatusInstrumento());
		consultaEstatus.setNumCliente(serverResponse.getNumeroCliente());
		consultaEstatus.setCompaniaCelular(serverResponse.getCompaniaCelular());
		consultaEstatus.setEmailCliente(serverResponse.getEmailCliente());
		consultaEstatus.setPerfilAST(serverResponse.getPerfilCliente());
		consultaEstatus.setEstatusAlertas(serverResponse.getEstatusAlertas());
		consultaEstatus.setNumCelular(userCellPhone);
		session.setSecurityInstrument(serverResponse.getTipoInstrumento());
		session.setEstatusIS(serverResponse.getEstatusInstrumento());
		data.setEstatusInstrumento(serverResponse.getEstatusInstrumento());
	}

	private BmovilViewsController bmovilViewsController;
	private ConsultaEstatus consultaEstatus;
	private LoginData data;
	private ServerResponse responseGlobal=null;
	private Session session;
	public void analyzeResponse(final int operationId,final ServerResponse response) {
		if(operationId==Server.CONSULTA_MANTENIMIENTO){
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				llenarConsultaEstatus((ConsultaEstatusMantenimientoData) response.getResponse());
				analyseResponseAux(session);
			}
		}else if(operationId ==Server.OP_SOLICITAR_ALERTAS){
			final SolicitarAlertasData alertas=(SolicitarAlertasData)response.getResponse();
			procesaAlertas(alertas);
		}else if (operationId == Server.LOGIN_OPERATION){
				  session = Session.getInstance(SuiteAppLogin.appContext);//2x1CGI

				if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL
						|| response.getStatus() == ServerResponse.OPERATION_OPTIONAL_UPDATE) {
					responseGlobal=response;

					 try{
						final JSONObject jo=new JSONObject(response.getResponsePlain().replaceAll("\n","").replaceAll("\\\\\"",""));
						final String errorcodeJo=jo.optString("errorCode", null);
						final String statusJo=jo.optString("status", null);
						if (errorcodeJo==null) {//no es un error (A1)
							//session.getKeyStoreWrapper().storeValueForKey("incorrectIntensLogin", "0");
							saveStringPref("incorrectIntensLogin", "0");
							data=new LoginData();
							data.process(jo.getJSONObject("response"));
							//storeSessionAfterLogin(response);
							if(data.getMgBooleano()){
								solicitarAlertas();
							}else {
								SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(data, consultaEstatus, response.getResponsePlain());
							}

							return;
							//sincroniza session
						}else{//si es un estatus diferente a a1
							//si no desea continuar el flujo de bconnect solo se envia el resultado
							if(!SuiteAppLogin.getInstance().isActiveBconnectFlow()) {
								SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(null, consultaEstatus,response.getResponsePlain());
								return;
							}
						}
						data= new LoginData();
						//QUITA EL ST- del error code y lo asigna el estatus de servicio
						final String[] estatusValidos=new String[]{ "200" , "403" , "430"};
						data.setEstatusServicio(errorcodeJo.replace("ST-",""));
						if(Arrays.asList(estatusValidos).contains(statusJo) &&  !bancomer.api.common.commons.Constants.STATUS_ARQ_DATOS_INCORRECTOS.equals(errorcodeJo)){//respondio con un estado distinto a A1
							operationConsultaEstatus();
						}else{//respondio con un estado difernete a a1 y a cualquiere etado conocido
							 analyseResponseAux4();
						}
					}catch (Exception js){
						 menuSuiteViewController.showErrorMessage(R.string.error_communications);
					 }
				} else {
					menuSuiteViewController.showErrorMessage(R.string.error_communications);
				}


		}
	}
	//To Save value
	private void saveStringPref(String key, String value){
		SharedPreferences bifrostPrefs = SuiteAppLogin.appContext.getSharedPreferences("intentosLogin", android.content.Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = bifrostPrefs.edit();
		editor.putString(key, value);
		editor.commit();
	}
	//To get stored value
	private String getStringPref(String key){
		SharedPreferences bifrostPrefs = SuiteAppLogin.appContext.getSharedPreferences("intentosLogin", android.content.Context.MODE_PRIVATE);
		return bifrostPrefs.getString(key, "");
	}

	private void analyseResponseAux4(){
		final KeyStoreWrapper ks=session.getKeyStoreWrapper();
		if(data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_ARQ_DATOS_INCORRECTOS)){
			//final String intentos=ks.fetchValueForKey("incorrectIntensLogin");
			final String intentos= getStringPref("incorrectIntensLogin");
			if(intentos!=null && !intentos.equals("")){
				int intentosInt=Integer.parseInt(intentos);
				intentosInt=intentosInt+1;
				if(intentosInt>2){
					//ks.storeValueForKey("incorrectIntensLogin","0");
					saveStringPref("incorrectIntensLogin","0");
					menuSuiteViewController.showErrorMessage(R.string.login_statusBLarq);
					SuiteAppLogin.getInstance().getCallBackLogin().limpiarPasswordLogin();
				}else{
					//ks.storeValueForKey("incorrectIntensLogin",String.valueOf(intentosInt));
					saveStringPref("incorrectIntensLogin",String.valueOf(intentosInt));
					menuSuiteViewController.showErrorMessage(R.string.login_invalidCredentialsarqservice);
					SuiteAppLogin.getInstance().getCallBackLogin().limpiarPasswordLogin();
				}
			}else{
				//ks.storeValueForKey("incorrectIntensLogin","1");
				saveStringPref("incorrectIntensLogin", "1");
				menuSuiteViewController.showErrorMessage(R.string.login_invalidCredentialsarqservice);
				SuiteAppLogin.getInstance().getCallBackLogin().limpiarPasswordLogin();
			}
		}else if(data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.EMPTY_STRING)){
			menuSuiteViewController.showErrorMessage(R.string.error_communications);
		}else{
			menuSuiteViewController.showErrorMessage(R.string.error_communications);
		}
	}

	private void procesaAlertas(final SolicitarAlertasData alertas) {
		Session session = Session.getInstance(SuiteAppLogin.appContext);
		final bancomer.api.common.commons.Constants.Perfil perfil = session.getClientProfile();
		 if(Constants.ALERT01.equals(alertas.getValidacionAlertas())){
			procesaAlertas01( perfil);
		}else if(Constants.ALERT02.equals(alertas.getValidacionAlertas())){
			procesaAlertas02(perfil, session);
		} else if(Constants.ALERT03.equals(alertas.getValidacionAlertas()) || Constants.ALERT04.equals(alertas.getValidacionAlertas()) ){
			procesaAlertas03(perfil, session);
		}
	}

	private boolean isTokenActivo(){
		final Session session = Session.getInstance(SuiteAppLogin.appContext);//2x1CGI
		//instrumentoSeguridad = “S1” o “T3” o “T6” y estatusIS = “A1”
		String instrumento =session.getSecurityInstrument();
		return ((Constants.IS_TYPE_DP270.equals(instrumento)
				|| Constants.IS_TYPE_OCRA.equals(instrumento)
				|| bancomer.api.common.commons.Constants.TYPE_SOFTOKEN.S1.value.equals(instrumento))
				&& Constants.STATUS_APP_ACTIVE.equals(session.getEstatusIS()));

	}


	private void procesaAlertas01(final bancomer.api.common.commons.Constants.Perfil perfil) {

		if(perfil==bancomer.api.common.commons.Constants.Perfil.avanzado){//MF03
			SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(data, consultaEstatus, responseGlobal.getResponsePlain());
		}else if(perfil==bancomer.api.common.commons.Constants.Perfil.basico){//MF01
			//EA#36
			if(isTokenActivo()){
				//EA#36
				//pas 3 del caso de uso Cambio de Perfil
				bmovilViewsController.showCambioPerfil(data, consultaEstatus, responseGlobal);

			}else{//EA37
				if(validaDiaMenorSerDate(data.getBi())<0){//EA47
					SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(data, consultaEstatus,responseGlobal.getResponsePlain());
				}else{//continua EA37
					//se muestra la pantalla de migrainformativo y esta a su vez devera enviar a // STOPSHIP: 15/01/16
					showMigraInformativo(false);

				}
			}
		} else if(perfil==bancomer.api.common.commons.Constants.Perfil.recortado){//MF02
			//EA#38
			if(validaDiaMenorSerDate(data.getRi())<0) {//EA47
				SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(data, consultaEstatus, responseGlobal.getResponsePlain());
			}else {
				//continua EA#38
				showMigraInformativo(true);
			}
		}

	}
	private void procesaAlertas02(bancomer.api.common.commons.Constants.Perfil perfil,final Session session) {
			if(perfil==bancomer.api.common.commons.Constants.Perfil.basico){//MF01

				if(validaDiaMenorSerDate(data.getBf())<0){
				//EA#42
					long diasRestantes=comparaFechasEnDias(data.getBi(),data.getBf());
					final StringBuilder sb=new StringBuilder(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_rn41_loginapi));
					sb.append(" ").append(diasRestantes).append(" ").
						append(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_sufijo_dias));
					session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_CAMBIO_PERFIL,false,true);
					session.setAceptaCambioPerfil(false);
					muestraAlertaIrEMenuprincipal(sb.toString());
				}else{
				//EA#44 “
					muestraAlertaEA44();
				}
			} else if(perfil==bancomer.api.common.commons.Constants.Perfil.recortado){//MF02
				if(validaDiaMenorSerDate(data.getRf())<0){
				//EA#40
					final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
					long diasRestantes=comparaFechasEnDias(data.getRi(),data.getRf());
					final StringBuilder sb=new StringBuilder(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_rn41_loginapi));
					sb.append(" ").append(diasRestantes).append(" ")
							.append(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_sufijo_dias));
					muestraAlertaIrEMenuprincipal(sb.toString());

				}else{
				//EA#44 “
					muestraAlertaEA44();
				}
			}

	}
	private void muestraAlertaIrEMenuprincipal(final String msg){
		menuSuiteViewController.showInformationAlert(SuiteAppLogin.appContext
						.getString(R.string.label_information),msg,
				SuiteAppLogin.appContext
						.getString(R.string.common_alert_yesno_positive_button),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,final int which) {
						SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(data, consultaEstatus,responseGlobal.getResponsePlain());
						dialog.dismiss();
					}
				}
		);
	}
	private void procesaAlertas03(bancomer.api.common.commons.Constants.Perfil perfil,final Session session) {
		if(perfil==bancomer.api.common.commons.Constants.Perfil.basico){//MF01

			if(validaDiaMenorSerDate(data.getBf())<0){
			//EA#43
				final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
				long diasRestantes = comparaFechasEnDias(dateFormat.format(session.getServerDate()) ,data.getRi());
				final StringBuilder sb=new StringBuilder(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_rn42_loginapi));
				sb.append(" ").append(diasRestantes).append(" ")
						.append(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_sufijo_dias));
				session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_CAMBIO_PERFIL,false,true);
				session.setAceptaCambioPerfil(false);
				muestraAlertaIrEMenuprincipal(sb.toString());

			}else{
			//EA#45
				muestraAlertaEA45();
			}
		} else if(perfil==bancomer.api.common.commons.Constants.Perfil.recortado){//MF02
			if(validaDiaMenorSerDate(data.getRf())<0){
			//EA#41
				final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
				long diasRestantes =comparaFechasEnDias(dateFormat.format(session.getServerDate()) ,data.getRi());
				final StringBuilder sb=new StringBuilder(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_rn42_loginapi));
				sb.append(" ").append(diasRestantes).append(" ")
						.append(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_sufijo_dias));
				muestraAlertaIrEMenuprincipal(sb.toString());

			}else{
			//EA#45
				muestraAlertaEA45();
			}

		}
	}


	private void muestraAlertaEA44(){
		menuSuiteViewController.showInformationAlert(SuiteAppLogin.appContext
						.getString(R.string.label_information),SuiteAppLogin.appContext.getString(R.string.msg_serdate_mayor_diabasico_o_diarecortado_02),
				SuiteAppLogin.appContext
						.getString(R.string.common_alert_yesno_positive_button),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,final int which) {
						dialog.dismiss();
					}
				}
		);
	}
	private void muestraAlertaEA45(){
		menuSuiteViewController.showInformationAlert(SuiteAppLogin.appContext
						.getString(R.string.label_information),SuiteAppLogin.appContext.getString(R.string.msg_serdate_mayor_diabasico_o_diarecortado),
				SuiteAppLogin.appContext
						.getString(R.string.common_alert_yesno_positive_button),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,final int which) {
						dialog.dismiss();
					}
				}
		);
	}

	/**
	 * respuestas
	 * -1 si la fecha enviada es mayor
	 * 0 si son iguales
	 * 1 si la fecha enviada es menor
	 * @param diaComparar
	 * @return
	 */
	private int validaDiaMenorSerDate(final String diaComparar){
		final Session session = Session.getInstance(SuiteAppLogin.appContext);//2x1CGI
		final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		Date diaCompare=null;
		Date serData=session.getServerDate();
		try {
			diaCompare=dateFormat.parse(diaComparar);
			return serData.compareTo(diaCompare) ;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	private void showMigraInformativo(final Boolean realizarTransaccionMigraBasico){

		SuiteAppLogin.getInstance().setServerResponse(responseGlobal);
		SuiteAppLogin.getInstance().setConsultaEstatus(consultaEstatus);
		SuiteAppLogin.getInstance().setData(data);
		final Intent i=new Intent(SuiteAppLogin.appContext,MigrainformativoController.class);
		i.putExtra("realizaTransaccion", realizarTransaccionMigraBasico);
		SuiteAppLogin.appContext.startActivity(i);
	}

	/**
	* peticion a nacar de solicitar aletas
	* */
	public void solicitarAlertas (){
		Session session = Session.getInstance(SuiteAppLogin.appContext);
		Account cEje = Tools.obtenerCuentaEje();

		Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
		paramTable2.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		paramTable2.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		paramTable2.put(ServerConstants.NUMERO_TARJETA, cEje.getNumber());
		paramTable2.put(ServerConstants.COMPANIA_CELULAR, session.getCompaniaUsuario());

		doNetworkOperation(Server.OP_SOLICITAR_ALERTAS, paramTable2,menuSuiteViewController,true,  SolicitarAlertasData.class);

	}

	private void analyseResponseAux(final Session session) {
		if (data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_PENDING_ACTIVATION)
                || data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_PENDING_SEND)) {
			//session.getKeyStoreWrapper().storeValueForKey("incorrectIntensLogin","0");
			saveStringPref("incorrectIntensLogin", "0");
            // Eliminacion 2x1
            if (session.getSecurityInstrument().equals("S1")){
                procesadoEscenarioAlterno25(session, data);
            }else{
                SuiteAppLogin.getInstance()
                        .getSuiteViewsController().setCurrentActivityApp(menuSuiteViewController);
                bmovilViewsController.showActivacion(consultaEstatus, currentPassword, true);
            }
        } else if (data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_PASS_BLOCKED)) {
			//session.getKeyStoreWrapper().storeValueForKey("incorrectIntensLogin","0");
			saveStringPref("incorrectIntensLogin", "0");
            bmovilViewsController.showDesbloqueo(consultaEstatus);
        } else if (data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_APP_SUSPENDED)) {
			//session.getKeyStoreWrapper().storeValueForKey("incorrectIntensLogin","0");
			saveStringPref("incorrectIntensLogin", "0");
            bmovilViewsController.showQuitarSuspension(consultaEstatus);
        } else{
			AnalyseResponseAux2(session);
		}
	}

	private void AnalyseResponseAux2(final Session session) {
		if (data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_USER_CANCELED)
                || data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_BANK_CANCELED)) {
			//session.getKeyStoreWrapper().storeValueForKey("incorrectIntensLogin","0");
			saveStringPref("incorrectIntensLogin", "0");
            final String fecha = Tools.invertDateOrder(data.getFechaContratacion());
            if (fecha.equals(data.getServerDate())){
                menuSuiteViewController
                        .showInformationAlert(SuiteAppLogin.appContext
								.getString(R.string.bmovil_estatus_aplicacion_desactivada_error_fechas_iguales));
            } else{
                bmovilViewsController.showContratacion(consultaEstatus, data.getEstatusServicio(), true);
            }
        } else if (data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_CLIENT_NOT_FOUND)
                || data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_ENGAGEMENT_UNCOMPLETE)) {
			//session.getKeyStoreWrapper().storeValueForKey("incorrectIntensLogin","0");
			saveStringPref("incorrectIntensLogin", "0");
            bmovilViewsController.showContratacion(consultaEstatus, data.getEstatusServicio(), true);
        } else {
			analiseResponseAux3(session);
		};
	}

	private void analiseResponseAux3(final Session session) {
		if(data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_WRONG_IUM)
				|| data.getEstatusServicio().equals( bancomer.api.common.commons.Constants.STATUS_ARQ_WRONG_IUM)) {
			//session.getKeyStoreWrapper().storeValueForKey("incorrectIntensLogin","0");
			saveStringPref("incorrectIntensLogin", "0");
            //Eliinacion 2x1
            if("S1".equals(session.getSecurityInstrument())) {
                procesadoEscenarioAlterno25(session, data);
            } else {
                menuSuiteViewController
                        .showInformationAlert(R.string.bmovil_activacion_alerta_reactivar,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(final DialogInterface dialog,final int which) {
                                        bmovilViewsController.showReactivacion(consultaEstatus,
                                                currentPassword);
                                    }
                                }
                        );
            }
        } else if (data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_NIP_BLOCKED)){
			//session.getKeyStoreWrapper().storeValueForKey("incorrectIntensLogin","0");
			saveStringPref("incorrectIntensLogin", "0");
			menuSuiteViewController.showErrorMessage(R.string.error_nip_bloqueadoarq);
        }else{
            menuSuiteViewController.showErrorMessage(R.string.error_communications);
        }
	}

	/**
	 * Llena el objeto consulta estatus con los datos obtenidos de la respuesta
	 * de NACAR.
	 *
	 * @param data
	 *            La respuesta de NACAR.
	 */
	private void llenarConsultaEstatus(final LoginData data) {
		consultaEstatus.setCompaniaCelular(data.getCompaniaTelCliente());
		consultaEstatus.setEmailCliente(data.getEmailCliente());
		consultaEstatus.setEstatus(data.getEstatusServicio());
		consultaEstatus.setEstatusInstrumento(data.getEstatusInstrumento());
		consultaEstatus.setInstrumento(data.getInsSeguridadCliente());
		consultaEstatus.setNombreCliente("");
		consultaEstatus.setNumCelular(Session.getInstance(SuiteAppLogin.appContext)
				.getUsername());
		consultaEstatus.setNumCliente(data.getClientNumber());
		consultaEstatus.setPerfilAST(data.getPerfiCliente());
		consultaEstatus.setPerfil(Tools.determinaPerfil(
				consultaEstatus.getPerfilAST(),
				consultaEstatus.getInstrumento(),
				consultaEstatus.getEstatusInstrumento()));
		consultaEstatus.setEstatusAlertas(data.getEstatusAlertas());

		//Guardamos EA (EsatusAlertas) para EA11# p026 Transferir BancomerNFR

		final Session session = Session.getInstance(SuiteAppLogin.appContext);
		session.setEstatusAlertas(data.getEstatusAlertas());

		if(Server.ALLOW_LOG) Log.d("ConsultaEstatus", consultaEstatus.toString());
	}
	private String storeSessionType;
	/**
	 * Store session after login
	 *
	 * @param response
	 *            the server response
	 */
	LoginData loginData;
	private void storeSessionAfterLogin(final ServerResponse response) {


		 loginData = (LoginData) response.getResponse();
		final Session session = Session.getInstance(SuiteAppLogin.appContext);
		session.setClientNumber(loginData.getClientNumber());
		session.setAccounts(loginData.getAccounts(), loginData.getServerDate(),
				loginData.getServerTime());
		session.setEmail(loginData.getEmailCliente());

		session.setNombreCliente(Tools.isEmptyOrNull(loginData
				.getNombreCliente()) ? "" : loginData.getNombreCliente());

		sessionAfterLoginAux(session, loginData);



		/*
		 * if the server has returned at least 1 catalog it means that the
		 * catalog version has changed
		 */
		if (loginData.getCatalogs() != null) {
			escribirCatalogos(loginData);
		}

		// Verifica el timeout de la pantalla del teléfono, si este timeout es
		// menor que el regresado por el servidor, se toma el del teléfono
		final int defTimeOut = 0;
		final int DELAY = 3000;
		final int oneMinuteInMilis = 60000;


		if (defTimeOut < loginData.getTimeout() * oneMinuteInMilis
				&& defTimeOut > 15000)
			session.setTimeout(defTimeOut);
		else
			session.setTimeout(loginData.getTimeout() * oneMinuteInMilis);

		session.setValidity(Session.VALID_STATUS);
		storeSessionType = STORE_SESSION_LOGIN;

		session.storeSession();
	}

	private static void sessionAfterLoginAux(final Session session,final LoginData loginData) {
		if (loginData.getPerfiCliente().equals(bancomer.api.common.commons.Constants.PROFILE_ADVANCED_03)) {
			session.setClientProfile(bancomer.api.common.commons.Constants.Perfil.avanzado);
		} else if (loginData.getPerfiCliente().equals(bancomer.api.common.commons.Constants.PROFILE_RECORTADO_02)) {
			session.setClientProfile(bancomer.api.common.commons.Constants.Perfil.recortado);
		}else if (loginData.getPerfiCliente().equals(bancomer.api.common.commons.Constants.PROFILE_BASIC_01) ||loginData.getPerfiCliente().equals(bancomer.api.common.commons.Constants.PROFILE_BASIC_00) ){
			session.setClientProfile(bancomer.api.common.commons.Constants.Perfil.basico);
		}
		session.setCompaniaUsuario(loginData.getCompaniaTelCliente());
		session.setSecurityInstrument(loginData.getInsSeguridadCliente());
		session.setEstatusIS(loginData.getEstatusIS());
		session.setAuthenticationJson(loginData.getAuthenticationJson());
		if (loginData.getCatalogoTiempoAire() != null) {
			session.updateCatalogoTiempoAire(loginData.getCatalogoTiempoAire());
		}
		if (loginData.getCatalogoDineroMovil() != null) {
			session.updateCatalogoDineroMovil(loginData
					.getCatalogoDineroMovil());
		}
		if (loginData.getCatalogoServicios() != null) {
			session.updateCatalogoServicios(loginData.getCatalogoServicios());
		}
		if (loginData.getCatalogoMantenimientoSPEI() != null) {
			session.updateCatalogoMantenimientoSPEI(loginData.getCatalogoMantenimientoSPEI());
		}
	}

	public void escribirCatalogos(final LoginData loginData) {
		final Session session = Session.getInstance(SuiteAppLogin.appContext);
		session.setCatalogVersions(loginData.getCatalogVersions());
		session.updateCatalogs(loginData.getCatalogs());
	}

	/***
	 * Show an alert to inform the user that there is a new version available,
	 * and asks what to do.
	 *
	 * @param message
	 *            the information to show
	 * @param mandatory
	 *            true if the update is mandatory, false if not
	 * @param updateUrl
	 *            the url where the update can be found
	 */
	public void showUpdateConfirmationAlert(final String message,final  boolean mandatory,
											final String updateUrl) {
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				SuiteAppLogin.appContext);
		alertDialog.setTitle(R.string.menuSuite_update_alert_title);
		alertDialog.setMessage(message);
		alertDialog.setCancelable(false);
		// Update button
		alertDialog.setPositiveButton(
				R.string.menuSuite_update_alert_updateOption,
				new DialogInterface.OnClickListener() {
					public void onClick(final DialogInterface dialog,final int which) {
						String url = updateUrl;
						if (url.length() > 0) {
							if (!url.startsWith("http://")
									&& !url.startsWith("https://")) {
								url = "http://" + updateUrl;
							}
							final Intent browserIntent = new Intent(
									Intent.ACTION_VIEW, Uri.parse(url));
							dialog.dismiss();
							SuiteAppLogin.appContext.startActivity(browserIntent);
						}

					}
				});

		// Continue button
		if (!mandatory) {
			alertDialog.setNegativeButton(R.string.alert_continue,
					new DialogInterface.OnClickListener() {
						public void onClick(final DialogInterface dialog,final int which) {
							dialog.dismiss();
							if (Session.getInstance(SuiteAppLogin.appContext)
									.getValidity() == Session.VALID_STATUS) {
									return;
								//inicia sessionfdsa
							} else {
								final MenuSuiteViewController menuViewController = menuSuiteViewController;
								//menuViewController.restableceMenu();
							}

						}
					});
		} else {
			alertDialog.setNegativeButton(
					R.string.menuSuite_update_alert_exitOption,
					new DialogInterface.OnClickListener() {
						public void onClick(final DialogInterface dialog,final int which) {
							dialog.dismiss();
							SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(null, consultaEstatus,responseGlobal.getResponsePlain());
						}
					});
		}


		alertDialog.show();
	}

	private void procesadoEscenarioAlterno25(final Session session,final LoginData data) {

		final String estadoIs = data.getEstatusIS();
		session.setEstatusIS(estadoIs);

		//estadoIs="T3";
		if (estadoIs.equals("A1")){

			if (session.isSofttokenActivado()){


				menuSuiteViewController.showInformationAlert(
						R.string.bmovil_activacion_alerta_reactivar,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog,
												final int which) {

								Log.d(">> CGI-Nice-Ppl", "[EA#25] Entra en OK del alert");

								Log.d(">> CGI-Nice-Ppl", "[EA#25] Genera clase GeneraOTPSTDelegate");
								final GeneraOTPSTDelegate generaOTPSTDelegate = new GeneraOTPSTDelegate();
								Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra el token");
								if (generaOTPSTDelegate.borraToken()) {
									Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra el token >> true");
									PropertiesManager.getCurrent().setSofttokenActivated(false);
								}

								PropertiesManager.getCurrent().setBmovilActivated(false);
								Log.d(">> CGI-Nice-Ppl", "[EA#25] BmovilActivated >> false");

								Log.d(">> CGI-Nice-Ppl", "[EA#25] DatosBMovil Borrado Inicio - Softoken>" + DatosBmovilFileManager.getCurrent().getSoftoken() + " - Activado>" + DatosBmovilFileManager.getCurrent().getActivado() + " - Seed>" + DatosBmovilFileManager.getCurrent().getSeed());
								DatosBmovilFileManager.getCurrent().setSoftoken(false);
								DatosBmovilFileManager.getCurrent().setActivado(false);
								DatosBmovilFileManager.getCurrent().setSeed("");
								Log.d(">> CGI-Nice-Ppl", "[EA#25] DatosBMovil Borrado Fin - Softoken>" + DatosBmovilFileManager.getCurrent().getSoftoken() + " - Activado>" + DatosBmovilFileManager.getCurrent().getActivado() + " - Seed>" + DatosBmovilFileManager.getCurrent().getSeed());

								Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra Session Inicio");
								borrarKeychain();
								Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra Session Fin");
								//sincroniza la session del api y pasa los parametros a la session de bmovil
								SuiteAppLogin.getInstance().getCallBackLogin().returDesactivada();
								//aqui solo debe borrarse el archivo bmovil y el de softoken
								bmovilViewsController.showAutenticacionSoftoken(consultaEstatus, currentPassword);

							}
						});



			}else {

				procesadoEscenarioAlterno27();
			}


		}else {

			procesadoEscenarioAlterno26();

		}

	}


	private void borrarKeychain(){
		final KeyStoreWrapper kswrapper= KeyStoreWrapper.getInstance(SuiteAppLogin.appContext);
		try {
			kswrapper.setUserName(" ");  	//setEntry(Constants.USERNAME, " ");
			kswrapper.setSeed(" ");  		//setEntry(Constants.SEED, " ");
			kswrapper.storeValueForKey(bancomer.api.common.commons.Constants.CENTRO, " ");  //setEntry(Constants.CENTRO, " ");
			if(Server.ALLOW_LOG) {
				Log.i(this.getClass().getName(), "Eliminando datos de KeyChain...");
				Log.i("Key-> ", "UserName: " + kswrapper.getUserName());
				Log.i("Key->", "Seed: " + kswrapper.getSeed());
			}
		} catch (Exception e) { //------
			if(Server.ALLOW_LOG) {
				Log.e(this.getClass().getName(), e.getMessage());
			}
		} finally {
			final String telefono = Session.getInstance(SuiteAppLogin.appContext).getUsername();
			Session.getInstance(SuiteAppLogin.appContext).clearSession();
			Session.getInstance(SuiteAppLogin.appContext).setUsername(telefono);

		}

	}

	private void procesadoEscenarioAlterno26() {
		borrarKeychain();
		menuSuiteViewController.showInformationAlert(R.string.alert_estado_instrumento_distintoA1, process());

	}


	private void procesadoEscenarioAlterno27() {


		menuSuiteViewController.showInformationAlert(
				R.string.bmovil_activacion_alerta_reactivar,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,
										final int which) {

						PropertiesManager.getCurrent().setBmovilActivated(false);
						DatosBmovilFileManager.getCurrent().setActivado(false);
						DatosBmovilFileManager.getCurrent().setSeed("0");
						borrarKeychain();
						//aqui solo debe borrarse el archivo bmovil
						bmovilViewsController.showAutenticacionSoftoken(
								consultaEstatus, currentPassword);
					}
				});

	}

	public DialogInterface.OnClickListener process(){
		final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog,final int which) {
				dialog.dismiss();
				bmovilViewsController.showReactivacion(
						consultaEstatus, currentPassword);
			}
		};

		return listener;

	}

	public void doNetworkOperation(final int operationId,
									final Hashtable<String, ?> params, final BaseViewController caller,
									final boolean callerHandlesError,final Class<?> ojbResponse) {

		//if(ownerController instanceof MenuSuiteViewController) ((MenuSuiteViewController)ownerController).setButtonsDisabled(false);
		SuiteAppLogin.getInstance().getBmovilApplication().invokeNetworkOperation(operationId, params, caller,
				callerHandlesError,ojbResponse);
	}


	private DialogInterface.OnClickListener createEnableButtonsClickListener() {
		return new DialogInterface.OnClickListener() {

			@Override
			public void onClick(final DialogInterface dialog,final int which) {
				((MenuSuiteViewController) getMenuSuiteViewController()).setButtonsDisabled(false);
			}
		};
	}


	private long comparaFechasEnDias(final String fecha1p,final String fecha2p){
		final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		Date fecha1 = null;
		Date fecha2=null;
		try {
			fecha1 = dateFormat.parse(fecha1p);
			fecha2 = dateFormat.parse(fecha2p);
		}catch (ParseException pe){
			if(suitebancomercoms.aplicaciones.bmovil.classes.io.Server.ALLOW_LOG)
				Log.e("contratacion fechas","Error en converison de fechas");
			return 0;
		}

		// Crear 2 instancias de Calendar
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(fecha1);
		cal2.setTime(fecha2);
		// conseguir la representacion de la fecha en milisegundos
		long milis1 = cal1.getTimeInMillis();
		long milis2 = cal2.getTimeInMillis();

		// calcular la diferencia en milisengundos
		long diff = milis2 - milis1;

		final long diffDays = diff / (24 * 60 * 60 * 1000);
		return diffDays;
	}


}
