package suitebancomer.classes.gui.controllers.login;

import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.bancomer.mbanking.login.R;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.login.BmovilViewsController;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.classes.gui.delegates.login.MenuSuiteDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;

public class MenuSuiteViewController extends BaseViewController implements View.OnClickListener, DialogInterface.OnClickListener, AnimationListener {
	
	/**
	 * The layout where the options and the login view are supposed to appear
	 */
	private FrameLayout baseLayout;
	
	/**
	 * The layout containing the suite options
	 */
	//private RelativeLayout menuOptionsLayout;
	private LinearLayout menuOptionsLayout;
	
	/**
	 * Reference to the first button of the option layout
	 */
	private ImageButton firstMenuOption;
	
	/**
	 * Reference to the second button of the option layout
	 */
	private ImageButton secondMenuOption;
	

	
	/**
	 * Referencia al botón contáctanos
	 */
	private ImageButton contactButton;
	

	/**
	 * Referencia texto versión
	 */
	private TextView txtVersion;
	
	
	private MenuSuiteDelegate delegate;
	
	private boolean isFlipPerforming;
	
	private boolean shouldHideLogin;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	
	public boolean comesFromNov = false;
	
	public void setComesFromNov(final Boolean b){
		comesFromNov = b;
	}
	public void setIsFlipPerforming(final Boolean b){
		isFlipPerforming = b;
	}

	

	
	/**
	 * Method that listens whenever a view is touched (clicked)
	 */
	@Override
	public void onClick(final View v) {
	}
	

	
	public void setButtonsDisabled(final boolean value) {
		this.areButtonsDisabled = value;
	}
	

	
	private void llamarLineaBancomer(final String numeroTel) {
		delegate.llamarLineaBancomer(numeroTel);
	}
	
	public FrameLayout getBaseLayout() {
		return baseLayout;
	}
	

	public boolean getShouldHideLogin() {
		return shouldHideLogin;
	}
	
	public void setShouldHideLogin(final boolean shouldHideLogin) {
		this.shouldHideLogin = shouldHideLogin;
	}

	@Override
	public void onClick(final DialogInterface dialog,final  int which) {
		if (which == Dialog.BUTTON_POSITIVE) {
			llamarLineaBancomer(getString(R.string.menuSuite_callLocalNumber));
		} else if (which == Dialog.BUTTON_NEUTRAL) {
			llamarLineaBancomer(getString(R.string.menuSuite_callAwayNumber));
		}
		areButtonsDisabled = false;
	}

	/**
	 * Stores the session information after a login operation. If registering
	 * is taking place, it show the activation screen instead.
	 *
	 * @param response The server response to the login operation
	 */
    public void processNetworkResponse(final int operationId,final  ServerResponse response) {
		 if(operationId == Server.LOGIN_OPERATION ||operationId == Server.OP_SOLICITAR_ALERTAS || operationId==Server.CONSULTA_MANTENIMIENTO) {
			 this.getDelegate().analyzeResponse(operationId, response);
		}
    }
    

    
//    @Override
//	public void goBack() {
//    	delegate.onBackPressed();
//    }
    


	@Override
	public void onAnimationRepeat(final Animation animation) {
		isFlipPerforming = true;
	}

	@Override
	public void onAnimationStart(final Animation animation) {
		isFlipPerforming = true;
	}

	@Override
	public void onAnimationEnd(final Animation animation) {

	}

	/**
	 * Establece la l�gica a seguir para el redimensionamiento de la pantalla.
	 */
	private void scaleForCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(baseLayout);
		guiTools.scale(menuOptionsLayout);
		guiTools.scale(firstMenuOption);
		guiTools.scale(secondMenuOption);
		guiTools.scale(findViewById(R.id.suite_advertising_image));
		guiTools.scale(contactButton);
	}
	

	

	

	// #endregion
	
	private boolean areButtonsDisabled = false;



}

