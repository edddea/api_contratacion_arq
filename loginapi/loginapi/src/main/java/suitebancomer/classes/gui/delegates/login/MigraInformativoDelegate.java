package suitebancomer.classes.gui.delegates.login;

import android.content.Context;

import com.bancomer.mbanking.login.SuiteAppLogin;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.login.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.model.login.ConsultaTerminosDeUsoData;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.classes.gui.controllers.login.BaseViewController;
import suitebancomer.classes.gui.controllers.login.MenuSuiteViewController;
import suitebancomer.classes.gui.controllers.login.MigrainformativoController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.MigraBasicoData;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

/**
 * Created by alanmichaelgonzalez on 06/01/16.
 */
public class MigraInformativoDelegate extends BaseDelegate  {
    private Context context;
    private MenuSuiteViewController menuSuiteViewController;
    BmovilViewsController bmovilViewsController;
    MigrainformativoController parentController;

    public MigraInformativoDelegate(Context context){
        this.context=context;

        menuSuiteViewController=new MenuSuiteViewController();
        menuSuiteViewController.setDelegate(this);
        bmovilViewsController =SuiteAppLogin.getInstance().getBmovilApplication().getBmovilViewsController();
    }

    public void setParentController(MigrainformativoController parentDelegate) {
        this.parentController = parentDelegate;
    }

    private boolean isTokenActivo(){
        final Session session = Session.getInstance(SuiteAppLogin.appContext);//2x1CGI
        String instrumento =session.getSecurityInstrument();
        return ((Constants.IS_TYPE_DP270.equals(instrumento)
                || Constants.IS_TYPE_OCRA.equals(instrumento)
                || bancomer.api.common.commons.Constants.TYPE_SOFTOKEN.S1.value.equals(instrumento))
                && Constants.STATUS_APP_ACTIVE.equals(session.getEstatusIS()));

    }


    public void doNetworkOperation(final int operationId,
                                   final Hashtable<String, ?> params, final BaseViewController caller,
                                   final boolean callerHandlesError,Class<?> ojbResponse) {

        //if(ownerController instanceof MenuSuiteViewController) ((MenuSuiteViewController)ownerController).setButtonsDisabled(false);
        SuiteAppLogin.getInstance().getBmovilApplication().invokeNetworkOperation(operationId, params, caller,
                callerHandlesError,ojbResponse);
    }

    public  void realizaTransaccionTerminosCondiciones(){
        final Hashtable<String, String> paramTable = new Hashtable<String, String>();
        paramTable.put(ServerConstants.PERFIL_CLIENTE, Constants.PROFILE_ADVANCED_03);
        doNetworkOperation(Server.OP_CONSULTAR_TERMINOS_SESION, paramTable, parentController, true,  ConsultaTerminosDeUsoData.class);

    }

    public void realizaTransaccionMigraBasico() {
        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        Session session=Session.getInstance(context);
        paramTable.put("numeroTelefono",session.getUsername());
        paramTable.put("IUM",session.getIum());
        paramTable.put("numeroCliente",session.getClientNumber());
        paramTable.put("codigoNIP","");
        paramTable.put("cveAcceso","");
        paramTable.put("codigoCVV2","");
        paramTable.put("tarjeta5Dig", "");
        paramTable.put("codigoOTP", "");
        paramTable.put("cadenaAutenticacion","00000");


        paramTable.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);

        Encripcion.setContext(SuiteAppLogin.appContext);
        List<String> listaEncriptar = Arrays.asList(
                ApiConstants.OPERACION, "codigoNIP",
                "cveAcceso", "codigoCVV2"
        );
        Encripcion.encriptaCadenaAutenticacion(paramTable,listaEncriptar,false);

        doNetworkOperation(Server.OP_MIGRA_BASICO, paramTable, parentController, true, MigraBasicoData.class);
    }

    public void analyzeResponse(final int operationId,final ServerResponse response) {
        if(operationId==Server.OP_MIGRA_BASICO){
            MigraBasicoData migrabasico=(MigraBasicoData)response.getResponse();
            if("OK".equals( migrabasico.getEstado())){//respuesta exitosa
               if(!isTokenActivo()){//EA#38
                    //Se regresa al escenario alterno EA#19 paso 1 del caso de uso Activación SoftToken.
                   bmovilViewsController.showAutenticacionST();
               }else{
                    //Se regresa al Escenario Principal paso 3 del caso de uso Cambio de Perfil
                   bmovilViewsController.showCambioPerfil(SuiteAppLogin.getInstance().getData(),SuiteAppLogin.getInstance().getConsultaEstatus(), SuiteAppLogin.getInstance().getServerResponse());
               }
            }
        }else if(operationId==Server.OP_CONSULTAR_TERMINOS_SESION){
            analyzeResponseTerminos( operationId,response);
        }

    }

    public void analyzeResponseTerminos(final int operationId,final ServerResponse response){
        SuiteAppLogin.getInstance().getBmovilApplication().getBmovilViewsController().showTerminosYcondicionesST((ConsultaTerminosDeUsoData)response.getResponse());
    }

}
