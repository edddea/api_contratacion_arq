package suitebancomer.classes.gui.delegates.login;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;
import com.bancomer.mbanking.login.R;
import com.bancomer.mbanking.login.SuiteAppLogin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.login.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioPerfilDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Catalog;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import suitebancomer.classes.gui.controllers.login.BaseViewController;
import suitebancomer.classes.gui.controllers.login.MenuSuiteViewController;
import suitebancomer.classes.gui.controllers.login.MigrainformativoController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.CatalogoAutenticacionFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.LoginData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomercoms.classes.common.PropertiesManager;

public class MenuSuiteDelegate extends BaseDelegate  {
	private final static String STORE_SESSION_LOGIN = "sessionLogin";
	
	public final static long MENU_SUITE_DELEGATE_ID = 0x3329474451002cd6L; 
	
	private MenuSuiteViewController menuSuiteViewController;
	
	private boolean isCallActive;
	private boolean bMovilSelected;
	
	public void setbMovilSelected(final boolean bMovilSelected) {
		this.bMovilSelected = bMovilSelected;
	}
	
	public boolean isbMovilSelected() {
		return bMovilSelected;
	}
	
	public MenuSuiteDelegate() {

		menuSuiteViewController=new MenuSuiteViewController();
		menuSuiteViewController.setDelegate(this);
		bmovilViewsController =SuiteAppLogin.getInstance().getBmovilApplication().getBmovilViewsController();
	}
	
	public boolean isCallActive() {
		return isCallActive;
	}
	
	public void setCallActive(final boolean isCallActive) {
		this.isCallActive = isCallActive;
	}
	
	public boolean isDisconnected(){
		final SuiteAppLogin suiteApp = SuiteAppLogin.getInstance();
		
		 final ConnectivityManager connectivity = (ConnectivityManager)
				 suiteApp.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		 
         if (connectivity != null){
            final NetworkInfo[] info = connectivity.getAllNetworkInfo();
             if (info != null){
                 for (int i = 0; i < info.length; i++){
                     if (info[i].getState() == NetworkInfo.State.CONNECTED){
                         return false;
                     }
                 }
             }
         }
         return true;
	}
	
	public void startBmovilApp() {
		if (SuiteAppLogin.getInstance().getBmovilApplication() == null) {
			SuiteAppLogin.getInstance().startBmovilApp();
		}
	}
	
	public int getBmovilAppStatus(final SuiteAppLogin suiteApp) {
		return suiteApp.getBmovilApplication().getApplicationStatus();
	}
	
	public MenuSuiteViewController getMenuSuiteViewController() {
		return menuSuiteViewController;
	}

	public void setMenuSuiteViewController(final MenuSuiteViewController menuSuiteViewController) {
		this.menuSuiteViewController = menuSuiteViewController;
	}
	
	public void llamarLineaBancomer(final String numeroTel) {
		try {
			isCallActive = true;
	       final Intent callIntent = new Intent(Intent.ACTION_CALL);
	        callIntent.setData(Uri.parse(Constants.TEL_URI+numeroTel));
	        menuSuiteViewController.startActivity(callIntent);
	    } catch (ActivityNotFoundException e) {
	    	menuSuiteViewController.showErrorMessage(menuSuiteViewController.getString(R.string.menuSuite_callErrorMessage));
	    }
	}

	
	public void leerContratacionST() {
			
	}



	String currentPassword;
	public void login(final String userCellPhone,final  String password) {
		currentPassword = password;
		final Session session = Session.getInstance(SuiteAppLogin.appContext);


		// String via = session.getVia();

		session.setPassword(password);

		final Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, userCellPhone);
		//paramTable.put(Server.PASSWORD_PARAM, password);
		paramTable.put("NP", password);
		String ium = session.getIum();
		if (Tools.isEmptyOrNull(ium)) {
			ium = Tools.buildIUM(userCellPhone, session.getSeed(),
					SuiteAppLogin.appContext);
			session.setIum(ium);
		}
		session.setUsername(userCellPhone);

		paramTable.put(ServerConstants.IUM_ETIQUETA, ium);

		paramTable.put("VM", Constants.APPLICATION_VERSION);
		final String marcaModelo = Build.BRAND + Build.MODEL;
		paramTable.put("MM", marcaModelo);

		final String[] catalogVersions = session.getCatalogVersions();
		final Catalog[] catalogos = session.getCatalogs();

		loginAux(session, paramTable, catalogVersions, catalogos);
//		paramTable.put(Server.VERSION_AU_PARAM,
//				CatalogoAutenticacionFileManager.getCurrent().leerVersionArchivoCatalogoAutenticacion());
		//paramTable.put(Server.VERSION_AU_PARAM,
		//		CatalogoAutenticacionFileManager.getCurrent().leerVersionArchivoCatalogoAutenticacion());
		/*Cátalogo de Autenticacion revisa que el numero de elemetos guardados en el archivo
		 * con los que espera recibir la aplicación
		 *Alejandra Quintanar*/
		int i=0;
		for (final bancomer.api.common.commons.Constants.Operacion valorOperacion: bancomer.api.common.commons.Constants.Operacion.values() ) {
			i= i+1;
		}
		if (Server.ALLOW_LOG) Log.d("MenuSuiteDelegate","EL TAMAÑO DE ENUM OPERACION ES : " + i);
		final int tamaño= Autenticacion.getInstance().getAvanzado().size();
		if (Server.ALLOW_LOG)  Log.d("MenuSuiteDelegate", "este es el tamaño de operaciones" + tamaño);
		if(tamaño == (i) ){
			if (Server.ALLOW_LOG)  Log.d("MenuSuiteDelegate", "Es igual");
//			System.out.println("este es el valor de AU:"+ Session.getInstance(SuiteApp.appContext)
//					.getAuthenticationCatalogVersion());
			paramTable.put("AU",
					CatalogoAutenticacionFileManager.getCurrent().leerVersionArchivoCatalogoAutenticacion());

		}else{
			if (Server.ALLOW_LOG)  Log.d("MenuSuiteDelegate", "No Es igual");
			paramTable.put("AU","0");
		}

		paramTable.put(suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants.PARAMS_TEXTO_EN,
				suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_STRING);
		Encripcion.setContext(SuiteAppLogin.appContext);
		final List<String> listaEncriptar3 = Arrays.asList("NP");
		Encripcion.encriptarPeticionString(paramTable, listaEncriptar3, "*",
				bancomer.api.common.commons.Constants.EMPTY_STRING);

	/*Fin del código*/
		//ARR 3/07
		//ARR
		final Map<String,Object> eventoLogin = new HashMap<String, Object>();

		eventoLogin.put("evento_login", "event22");

		//TrackingHelper.trackClickLogin(eventoLogin);
		//JAIG
	//	doNetworkOperation(Server.LOGIN_OPERATION, paramTable,false, new LoginData(), Server.isJsonValueCode.NONE,
	//			menuSuiteViewController, true);
		doNetworkOperation(Server.LOGIN_OPERATION, paramTable, menuSuiteViewController,true, LoginData.class);
	}

	private void loginAux(final Session session,final  Hashtable<String, String> paramTable,final  String[] catalogVersions,final  Catalog... catalogos) {
		if ((catalogVersions != null) && (catalogVersions.length == 8)) {// solo
			// son
			// 8
			paramTable.put("C1", catalogVersions[0]);
			paramTable.put("C4", catalogVersions[1]);
			paramTable.put("C5", catalogVersions[2]);
			paramTable.put("C8", catalogVersions[3]);
			paramTable.put("TA", catalogVersions[4]);
			paramTable.put("DM", catalogVersions[5]);
			paramTable.put("SV", catalogVersions[6]);
			paramTable.put("MS", catalogVersions[7]);
		}

		if (catalogos[0] == null || catalogos[1] == null
				|| catalogos[2] == null || catalogos[3] == null) { // Solo 4
			// catalgos
			// genericos
			if(Server.ALLOW_LOG) Log.d("Catálogo 0 nulo", "Catálogo 0 nulo");
			paramTable.put("C1", "0");
			paramTable.put("C4", "0");
			paramTable.put("C5", "0");
			paramTable.put("C8", "0");
		}

		loginAux2(session, paramTable);
	}

	private void loginAux2(final Session session,final  Hashtable<String, String> paramTable) {
		if (session.getCatalogoTiempoAire() == null) {
			paramTable.put("TA", "0");
		}

		if (session.getCatalogoDineroMovil() == null) {
			paramTable.put("DM", "0");
		}

		if (session.getCatalogoServicios() == null) {
			paramTable.put("SV", "0");
		}

		if (session.getCatalogoMantenimientoSPEI() == null) {
			paramTable.put("MS", "0");
		}
	}


	private BmovilViewsController bmovilViewsController;
	private ConsultaEstatus consultaEstatus;
	private LoginData data;
	private ServerResponse responseGlobal=null;
	public void analyzeResponse(final int operationId,final ServerResponse response) {
		if(operationId ==Server.OP_SOLICITAR_ALERTAS){
			final SolicitarAlertasData alertas=(SolicitarAlertasData)response.getResponse();
			procesaAlertas(alertas);
		}else{
			responseGlobal=response;
			if (operationId == Server.LOGIN_OPERATION) {
				// Stores the session and goes to the menu screen
				final Session session = Session.getInstance(SuiteAppLogin.appContext);//2x1CGI
				// Now validates status
				if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL
						|| response.getStatus() == ServerResponse.OPERATION_OPTIONAL_UPDATE) {
					 data = (LoginData) response.getResponse();

					consultaEstatus = new ConsultaEstatus();
					llenarConsultaEstatus(data);

					Session.getInstance(SuiteAppLogin.appContext).setSecurityInstrument(data.getInsSeguridadCliente());

					if (data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_APP_ACTIVE)) {

						storeSessionAfterLogin(response);
						if(data.getMgBooleano()){
							solicitarAlertas();
						}else {
							SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(data, consultaEstatus, response.getResponsePlain());
						}
						/*if(!data.getMessage().equals("")){
							menuSuiteViewController.showInformationAlert(SuiteAppLogin.appContext
									.getString(R.string.label_information),data.getMessage(),
									SuiteAppLogin.appContext
											.getString(R.string.common_alert_yesno_positive_button),
											new DialogInterface.OnClickListener() {

												@Override
												public void onClick(final DialogInterface dialog,final int which) {
													SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(data, consultaEstatus, response.getResponsePlain());
													dialog.dismiss();
												}
											}
									);

						}else {
							SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(data,consultaEstatus,response.getResponsePlain());
						}*/
						return;
						//sincroniza session
					}else{
						//si no desea continuar el flujo de bconnect solo se envia el resultado
						if(!SuiteAppLogin.getInstance().isActiveBconnectFlow()) {
							SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(null, consultaEstatus,response.getResponsePlain());
							return;
						}
					}

					analyseResponseAux(session);

				} else {
					if (response.getMessageCode().equals(SuiteAppLogin.appContext.getString(R.string.menuSuite_update_mandatoryCode))) {
						showUpdateConfirmationAlert(response.getMessageText(), true, response.getUpdateURL());
					} else {
						menuSuiteViewController.showInformationAlert(response.getMessageText());
						SuiteAppLogin.getInstance().getCallBackLogin().limpiarPasswordLogin();
					}

				}

			}
		}
	}

	private void procesaAlertas(final SolicitarAlertasData alertas) {
		Session session = Session.getInstance(SuiteAppLogin.appContext);
		final bancomer.api.common.commons.Constants.Perfil perfil = session.getClientProfile();
		 if(Constants.ALERT01.equals(alertas.getValidacionAlertas())){
			procesaAlertas01( perfil);
		}else if(Constants.ALERT02.equals(alertas.getValidacionAlertas())){
			procesaAlertas02(perfil, session);
		} else if(Constants.ALERT03.equals(alertas.getValidacionAlertas()) || Constants.ALERT04.equals(alertas.getValidacionAlertas()) ){
			procesaAlertas03(perfil, session);
		}
	}

	private boolean isTokenActivo(){
		final Session session = Session.getInstance(SuiteAppLogin.appContext);//2x1CGI
		//instrumentoSeguridad = “S1” o “T3” o “T6” y estatusIS = “A1”
		String instrumento =session.getSecurityInstrument();
		return ((Constants.IS_TYPE_DP270.equals(instrumento)
				|| Constants.IS_TYPE_OCRA.equals(instrumento)
				|| bancomer.api.common.commons.Constants.TYPE_SOFTOKEN.S1.value.equals(instrumento))
				&& Constants.STATUS_APP_ACTIVE.equals(session.getEstatusIS()));

	}


	private void procesaAlertas01(final bancomer.api.common.commons.Constants.Perfil perfil) {

		if(perfil==bancomer.api.common.commons.Constants.Perfil.avanzado){//MF03
			SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(data, consultaEstatus, responseGlobal.getResponsePlain());
		}else if(perfil==bancomer.api.common.commons.Constants.Perfil.basico){//MF01
			//EA#36
			if(isTokenActivo()){
				//EA#36
				//pas 3 del caso de uso Cambio de Perfil
				bmovilViewsController.showCambioPerfil(data,consultaEstatus, responseGlobal);

			}else{//EA37
				if(validaDiaMenorSerDate(data.getBi())<0){//EA47
					SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(data, consultaEstatus,responseGlobal.getResponsePlain());
				}else{//continua EA37
					//se muestra la pantalla de migrainformativo y esta a su vez devera enviar a // STOPSHIP: 15/01/16
					showMigraInformativo(false);

				}
			}
		} else if(perfil==bancomer.api.common.commons.Constants.Perfil.recortado){//MF02
			//EA#38
			if(validaDiaMenorSerDate(data.getRi())<0) {//EA47
				SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(data, consultaEstatus, responseGlobal.getResponsePlain());
			}else {
				//continua EA#38
				showMigraInformativo(true);
			}
		}

	}
	private void procesaAlertas02(bancomer.api.common.commons.Constants.Perfil perfil,final Session session) {
			if(perfil==bancomer.api.common.commons.Constants.Perfil.basico){//MF01

				if(validaDiaMenorSerDate(data.getBf())<0){
				//EA#42
					long diasRestantes=comparaFechasEnDias(data.getBi(),data.getBf());
					final StringBuilder sb=new StringBuilder(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_rn41_loginapi));
					sb.append(" ").append(diasRestantes).append(" ").
						append(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_sufijo_dias));
					session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_CAMBIO_PERFIL,false,true);
					session.setAceptaCambioPerfil(false);
					muestraAlertaIrEMenuprincipal(sb.toString());
				}else{
				//EA#44 “
					muestraAlertaEA44();
				}
			} else if(perfil==bancomer.api.common.commons.Constants.Perfil.recortado){//MF02
				if(validaDiaMenorSerDate(data.getRf())<0){
				//EA#40
					final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
					long diasRestantes=comparaFechasEnDias(data.getRi(),data.getRf());
					final StringBuilder sb=new StringBuilder(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_rn41_loginapi));
					sb.append(" ").append(diasRestantes).append(" ")
							.append(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_sufijo_dias));
					muestraAlertaIrEMenuprincipal(sb.toString());

				}else{
				//EA#44 “
					muestraAlertaEA44();
				}
			}

	}
	private void muestraAlertaIrEMenuprincipal(final String msg){
		menuSuiteViewController.showInformationAlert(SuiteAppLogin.appContext
						.getString(R.string.label_information),msg,
				SuiteAppLogin.appContext
						.getString(R.string.common_alert_yesno_positive_button),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,final int which) {
						SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(data, consultaEstatus,responseGlobal.getResponsePlain());
						dialog.dismiss();
					}
				}
		);
	}
	private void procesaAlertas03(bancomer.api.common.commons.Constants.Perfil perfil,final Session session) {
		if(perfil==bancomer.api.common.commons.Constants.Perfil.basico){//MF01

			if(validaDiaMenorSerDate(data.getBf())<0){
			//EA#43
				final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
				long diasRestantes = comparaFechasEnDias(dateFormat.format(session.getServerDate()) ,data.getRi());
				final StringBuilder sb=new StringBuilder(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_rn42_loginapi));
				sb.append(" ").append(diasRestantes).append(" ")
						.append(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_sufijo_dias));
				session.saveBanderasBMovil(bancomer.api.common.commons.Constants.BANDERAS_CAMBIO_PERFIL,false,true);
				session.setAceptaCambioPerfil(false);
				muestraAlertaIrEMenuprincipal(sb.toString());

			}else{
			//EA#45
				muestraAlertaEA45();
			}
		} else if(perfil==bancomer.api.common.commons.Constants.Perfil.recortado){//MF02
			if(validaDiaMenorSerDate(data.getRf())<0){
			//EA#41
				final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
				long diasRestantes =comparaFechasEnDias(dateFormat.format(session.getServerDate()) ,data.getRi());
				final StringBuilder sb=new StringBuilder(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_rn42_loginapi));
				sb.append(" ").append(diasRestantes).append(" ")
						.append(SuiteAppLogin.appContext.getString(R.string.msg_alert_rn_sufijo_dias));
				muestraAlertaIrEMenuprincipal(sb.toString());

			}else{
			//EA#45
				muestraAlertaEA45();
			}

		}
	}


	private void muestraAlertaEA44(){
		menuSuiteViewController.showInformationAlert(SuiteAppLogin.appContext
						.getString(R.string.label_information),SuiteAppLogin.appContext.getString(R.string.msg_serdate_mayor_diabasico_o_diarecortado_02),
				SuiteAppLogin.appContext
						.getString(R.string.common_alert_yesno_positive_button),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,final int which) {
						dialog.dismiss();
					}
				}
		);
	}
	private void muestraAlertaEA45(){
		menuSuiteViewController.showInformationAlert(SuiteAppLogin.appContext
						.getString(R.string.label_information),SuiteAppLogin.appContext.getString(R.string.msg_serdate_mayor_diabasico_o_diarecortado),
				SuiteAppLogin.appContext
						.getString(R.string.common_alert_yesno_positive_button),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,final int which) {
						dialog.dismiss();
					}
				}
		);
	}

	/**
	 * respuestas
	 * -1 si la fecha enviada es mayor
	 * 0 si son iguales
	 * 1 si la fecha enviada es menor
	 * @param diaComparar
	 * @return
	 */
	private int validaDiaMenorSerDate(final String diaComparar){
		final Session session = Session.getInstance(SuiteAppLogin.appContext);//2x1CGI
		final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		Date diaCompare=null;
		Date serData=session.getServerDate();
		try {
			diaCompare=dateFormat.parse(diaComparar);
			return serData.compareTo(diaCompare) ;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

	private void showMigraInformativo(final Boolean realizarTransaccionMigraBasico){

		SuiteAppLogin.getInstance().setServerResponse(responseGlobal);
		SuiteAppLogin.getInstance().setConsultaEstatus(consultaEstatus);
		SuiteAppLogin.getInstance().setData(data);
		final Intent i=new Intent(SuiteAppLogin.appContext,MigrainformativoController.class);
		i.putExtra("realizaTransaccion",realizarTransaccionMigraBasico);
		SuiteAppLogin.appContext.startActivity(i);
	}

	/**
	* peticion a nacar de solicitar aletas
	* */
	public void solicitarAlertas (){
		Session session = Session.getInstance(SuiteAppLogin.appContext);
		Account cEje = Tools.obtenerCuentaEje();

		Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
		paramTable2.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		paramTable2.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		paramTable2.put(ServerConstants.NUMERO_TARJETA, cEje.getNumber());
		paramTable2.put(ServerConstants.COMPANIA_CELULAR, session.getCompaniaUsuario());

		doNetworkOperation(Server.OP_SOLICITAR_ALERTAS, paramTable2,menuSuiteViewController,true,  SolicitarAlertasData.class);

	}

	private void analyseResponseAux(final Session session) {
		if (data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_PENDING_ACTIVATION)
                || data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_PENDING_SEND)) {
            // Eliminacion 2x1
            if (session.getSecurityInstrument().equals("S1")){
                procesadoEscenarioAlterno25(session,data);
            }else{
                SuiteAppLogin.getInstance()
                        .getSuiteViewsController().setCurrentActivityApp(menuSuiteViewController);
                bmovilViewsController.showActivacion(consultaEstatus, currentPassword, true);
            }
        } else if (data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_PASS_BLOCKED)) {
            bmovilViewsController.showDesbloqueo(consultaEstatus);
        } else if (data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_APP_SUSPENDED)) {
            bmovilViewsController.showQuitarSuspension(consultaEstatus);
        } else{
			AnalyseResponseAux2(session);
		}
	}

	private void AnalyseResponseAux2(final Session session) {
		if (data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_USER_CANCELED)
                || data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_BANK_CANCELED)) {

            final String fecha = Tools.invertDateOrder(data.getFechaContratacion());
            if (fecha.equals(data.getServerDate())){
                menuSuiteViewController
                        .showInformationAlert(SuiteAppLogin.appContext
								.getString(R.string.bmovil_estatus_aplicacion_desactivada_error_fechas_iguales));
            } else{
                bmovilViewsController.showContratacion(consultaEstatus, data.getEstatusServicio(), true);
            }
        } else if (data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_CLIENT_NOT_FOUND)
                || data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_ENGAGEMENT_UNCOMPLETE)) {
            bmovilViewsController.showContratacion(consultaEstatus, data.getEstatusServicio(), true);
        } else {
			analiseResponseAux3(session);
		};
	}

	private void analiseResponseAux3(final Session session) {
		if(data.getEstatusServicio().equals(
                bancomer.api.common.commons.Constants.STATUS_WRONG_IUM)) {
            //Eliinacion 2x1
            if("S1".equals(session.getSecurityInstrument())) {
                procesadoEscenarioAlterno25(session, data);
            } else {
                menuSuiteViewController
                        .showInformationAlert(R.string.bmovil_activacion_alerta_reactivar,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(final DialogInterface dialog,final int which) {
                                        bmovilViewsController.showReactivacion(consultaEstatus,
                                                currentPassword);
                                    }
                                }
                        );
            }
        } else if (data.getEstatusServicio().equals(bancomer.api.common.commons.Constants.STATUS_NIP_BLOCKED)){
            menuSuiteViewController.showErrorMessage(R.string.error_nip_bloqueado);
        }else {
            menuSuiteViewController.showErrorMessage(R.string.error_communications);
        }
	}

	/**
	 * Llena el objeto consulta estatus con los datos obtenidos de la respuesta
	 * de NACAR.
	 *
	 * @param data
	 *            La respuesta de NACAR.
	 */
	private void llenarConsultaEstatus(final LoginData data) {
		consultaEstatus.setCompaniaCelular(data.getCompaniaTelCliente());
		consultaEstatus.setEmailCliente(data.getEmailCliente());
		consultaEstatus.setEstatus(data.getEstatusServicio());
		consultaEstatus.setEstatusInstrumento(data.getEstatusInstrumento());
		consultaEstatus.setInstrumento(data.getInsSeguridadCliente());
		consultaEstatus.setNombreCliente("");
		consultaEstatus.setNumCelular(Session.getInstance(SuiteAppLogin.appContext)
				.getUsername());
		consultaEstatus.setNumCliente(data.getClientNumber());
		consultaEstatus.setPerfilAST(data.getPerfiCliente());
		consultaEstatus.setPerfil(Tools.determinaPerfil(
				consultaEstatus.getPerfilAST(),
				consultaEstatus.getInstrumento(),
				consultaEstatus.getEstatusInstrumento()));
		consultaEstatus.setEstatusAlertas(data.getEstatusAlertas());

		//Guardamos EA (EsatusAlertas) para EA11# p026 Transferir BancomerNFR

		final Session session = Session.getInstance(SuiteAppLogin.appContext);
		session.setEstatusAlertas(data.getEstatusAlertas());

		if(Server.ALLOW_LOG) Log.d("ConsultaEstatus", consultaEstatus.toString());
	}
	private String storeSessionType;
	/**
	 * Store session after login
	 *
	 * @param response
	 *            the server response
	 */
	LoginData loginData;
	private void storeSessionAfterLogin(final ServerResponse response) {


		 loginData = (LoginData) response.getResponse();
		final Session session = Session.getInstance(SuiteAppLogin.appContext);
		session.setClientNumber(loginData.getClientNumber());
		session.setAccounts(loginData.getAccounts(), loginData.getServerDate(),
				loginData.getServerTime());
		session.setEmail(loginData.getEmailCliente());

		session.setNombreCliente(Tools.isEmptyOrNull(loginData
				.getNombreCliente()) ? "" : loginData.getNombreCliente());

		sessionAfterLoginAux(session, loginData);



		/*
		 * if the server has returned at least 1 catalog it means that the
		 * catalog version has changed
		 */
		if (loginData.getCatalogs() != null) {
			escribirCatalogos(loginData);
		}

		// Verifica el timeout de la pantalla del teléfono, si este timeout es
		// menor que el regresado por el servidor, se toma el del teléfono
		final int defTimeOut = 0;
		final int DELAY = 3000;
		final int oneMinuteInMilis = 60000;


		if (defTimeOut < loginData.getTimeout() * oneMinuteInMilis
				&& defTimeOut > 15000)
			session.setTimeout(defTimeOut);
		else
			session.setTimeout(loginData.getTimeout() * oneMinuteInMilis);

		session.setValidity(Session.VALID_STATUS);
		storeSessionType = STORE_SESSION_LOGIN;

		session.storeSession();
	}

	private static void sessionAfterLoginAux(final Session session,final LoginData loginData) {
		if (loginData.getPerfiCliente().equals(bancomer.api.common.commons.Constants.PROFILE_ADVANCED_03)) {
			session.setClientProfile(bancomer.api.common.commons.Constants.Perfil.avanzado);
		} else if (loginData.getPerfiCliente().equals(bancomer.api.common.commons.Constants.PROFILE_RECORTADO_02)) {
			session.setClientProfile(bancomer.api.common.commons.Constants.Perfil.recortado);
		}else if (loginData.getPerfiCliente().equals(bancomer.api.common.commons.Constants.PROFILE_BASIC_01) ||loginData.getPerfiCliente().equals(bancomer.api.common.commons.Constants.PROFILE_BASIC_00) ){
			session.setClientProfile(bancomer.api.common.commons.Constants.Perfil.basico);
		}
		session.setCompaniaUsuario(loginData.getCompaniaTelCliente());
		session.setSecurityInstrument(loginData.getInsSeguridadCliente());
		session.setEstatusIS(loginData.getEstatusIS());
		session.setAuthenticationJson(loginData.getAuthenticationJson());
		if (loginData.getCatalogoTiempoAire() != null) {
			session.updateCatalogoTiempoAire(loginData.getCatalogoTiempoAire());
		}
		if (loginData.getCatalogoDineroMovil() != null) {
			session.updateCatalogoDineroMovil(loginData
					.getCatalogoDineroMovil());
		}
		if (loginData.getCatalogoServicios() != null) {
			session.updateCatalogoServicios(loginData.getCatalogoServicios());
		}
		if (loginData.getCatalogoMantenimientoSPEI() != null) {
			session.updateCatalogoMantenimientoSPEI(loginData.getCatalogoMantenimientoSPEI());
		}
	}

	public void escribirCatalogos(final LoginData loginData) {
		final Session session = Session.getInstance(SuiteAppLogin.appContext);
		session.setCatalogVersions(loginData.getCatalogVersions());
		session.updateCatalogs(loginData.getCatalogs());
	}

	/***
	 * Show an alert to inform the user that there is a new version available,
	 * and asks what to do.
	 *
	 * @param message
	 *            the information to show
	 * @param mandatory
	 *            true if the update is mandatory, false if not
	 * @param updateUrl
	 *            the url where the update can be found
	 */
	public void showUpdateConfirmationAlert(final String message,final  boolean mandatory,
											final String updateUrl) {
		final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				SuiteAppLogin.appContext);
		alertDialog.setTitle(R.string.menuSuite_update_alert_title);
		alertDialog.setMessage(message);
		alertDialog.setCancelable(false);
		// Update button
		alertDialog.setPositiveButton(
				R.string.menuSuite_update_alert_updateOption,
				new DialogInterface.OnClickListener() {
					public void onClick(final DialogInterface dialog,final int which) {
						String url = updateUrl;
						if (url.length() > 0) {
							if (!url.startsWith("http://")
									&& !url.startsWith("https://")) {
								url = "http://" + updateUrl;
							}
							final Intent browserIntent = new Intent(
									Intent.ACTION_VIEW, Uri.parse(url));
							dialog.dismiss();
							SuiteAppLogin.appContext.startActivity(browserIntent);
						}

					}
				});

		// Continue button
		if (!mandatory) {
			alertDialog.setNegativeButton(R.string.alert_continue,
					new DialogInterface.OnClickListener() {
						public void onClick(final DialogInterface dialog,final int which) {
							dialog.dismiss();
							if (Session.getInstance(SuiteAppLogin.appContext)
									.getValidity() == Session.VALID_STATUS) {
									return;
								//inicia sessionfdsa
							} else {
								final MenuSuiteViewController menuViewController = menuSuiteViewController;
								//menuViewController.restableceMenu();
							}

						}
					});
		} else {
			alertDialog.setNegativeButton(
					R.string.menuSuite_update_alert_exitOption,
					new DialogInterface.OnClickListener() {
						public void onClick(final DialogInterface dialog,final int which) {
							dialog.dismiss();
							SuiteAppLogin.getInstance().getCallBackLogin().returnLoginApi(null, consultaEstatus,responseGlobal.getResponsePlain());
						}
					});
		}


		alertDialog.show();
	}

	private void procesadoEscenarioAlterno25(final Session session,final LoginData data) {

		final String estadoIs = data.getEstatusIS();
		session.setEstatusIS(estadoIs);

		//estadoIs="T3";
		if (estadoIs.equals("A1")){

			if (session.isSofttokenActivado()){


				menuSuiteViewController.showInformationAlert(
						R.string.bmovil_activacion_alerta_reactivar,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog,
												final int which) {

								Log.d(">> CGI-Nice-Ppl", "[EA#25] Entra en OK del alert");

								Log.d(">> CGI-Nice-Ppl", "[EA#25] Genera clase GeneraOTPSTDelegate");
								final GeneraOTPSTDelegate generaOTPSTDelegate = new GeneraOTPSTDelegate();
								Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra el token");
								if (generaOTPSTDelegate.borraToken()) {
									Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra el token >> true");
									PropertiesManager.getCurrent().setSofttokenActivated(false);
								}

								PropertiesManager.getCurrent().setBmovilActivated(false);
								Log.d(">> CGI-Nice-Ppl", "[EA#25] BmovilActivated >> false");

								Log.d(">> CGI-Nice-Ppl", "[EA#25] DatosBMovil Borrado Inicio - Softoken>" + DatosBmovilFileManager.getCurrent().getSoftoken() + " - Activado>" + DatosBmovilFileManager.getCurrent().getActivado() + " - Seed>" + DatosBmovilFileManager.getCurrent().getSeed());
								DatosBmovilFileManager.getCurrent().setSoftoken(false);
								DatosBmovilFileManager.getCurrent().setActivado(false);
								DatosBmovilFileManager.getCurrent().setSeed("");
								Log.d(">> CGI-Nice-Ppl", "[EA#25] DatosBMovil Borrado Fin - Softoken>" + DatosBmovilFileManager.getCurrent().getSoftoken() + " - Activado>" + DatosBmovilFileManager.getCurrent().getActivado() + " - Seed>" + DatosBmovilFileManager.getCurrent().getSeed());

								Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra Session Inicio");
								borrarKeychain(session);
								Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra Session Fin");
								//sincroniza la session del api y pasa los parametros a la session de bmovil
								SuiteAppLogin.getInstance().getCallBackLogin().returDesactivada();
								//aqui solo debe borrarse el archivo bmovil y el de softoken
								bmovilViewsController.showAutenticacionSoftoken(consultaEstatus, currentPassword);

							}
						});



			}else {

				procesadoEscenarioAlterno27( session, data);
			}


		}else {

			procesadoEscenarioAlterno26( session, data);

		}

	}


	private void borrarKeychain(final Session session){
		final KeyStoreWrapper kswrapper= KeyStoreWrapper.getInstance(SuiteAppLogin.appContext);
		try {
			kswrapper.setUserName(" ");  	//setEntry(Constants.USERNAME, " ");
			kswrapper.setSeed(" ");  		//setEntry(Constants.SEED, " ");
			kswrapper.storeValueForKey(bancomer.api.common.commons.Constants.CENTRO, " ");  //setEntry(Constants.CENTRO, " ");
			if(Server.ALLOW_LOG) {
				Log.i(this.getClass().getName(), "Eliminando datos de KeyChain...");
				Log.i("Key-> ", "UserName: " + kswrapper.getUserName());
				Log.i("Key->", "Seed: " + kswrapper.getSeed());
			}
		} catch (Exception e) { //------
			if(Server.ALLOW_LOG) {
				Log.e(this.getClass().getName(), e.getMessage());
			}
		} finally {
			final String telefono = Session.getInstance(SuiteAppLogin.appContext).getUsername();
			Session.getInstance(SuiteAppLogin.appContext).clearSession();
			Session.getInstance(SuiteAppLogin.appContext).setUsername(telefono);

		}

	}

	private void procesadoEscenarioAlterno26(final Session session,final  LoginData data) {
		borrarKeychain(session);
		menuSuiteViewController.showInformationAlert(R.string.alert_estado_instrumento_distintoA1, process());

	}


	private void procesadoEscenarioAlterno27(final Session session,final  LoginData data) {


		menuSuiteViewController.showInformationAlert(
				R.string.bmovil_activacion_alerta_reactivar,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,
										final int which) {

						PropertiesManager.getCurrent().setBmovilActivated(false);
						DatosBmovilFileManager.getCurrent().setActivado(false);
						DatosBmovilFileManager.getCurrent().setSeed("0");
						borrarKeychain(session);
						//aqui solo debe borrarse el archivo bmovil
						bmovilViewsController.showAutenticacionSoftoken(
								consultaEstatus, currentPassword);
					}
				});

	}

	public DialogInterface.OnClickListener process(){
		final DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog,final int which) {
				dialog.dismiss();
				bmovilViewsController.showReactivacion(
						consultaEstatus, currentPassword);
			}
		};

		return listener;

	}

	public void doNetworkOperation(final int operationId,
								   final Hashtable<String, ?> params, final BaseViewController caller,
								   final boolean callerHandlesError,final Class<?> ojbResponse) {

		//if(ownerController instanceof MenuSuiteViewController) ((MenuSuiteViewController)ownerController).setButtonsDisabled(false);
		SuiteAppLogin.getInstance().getBmovilApplication().invokeNetworkOperation(operationId, params, caller,
				callerHandlesError,ojbResponse);
	}

	public boolean validarCampos(final String userCellPhone,final  String userPass) {
		if(Server.ALLOW_LOG) Log.d("LoginDelegate", "Aquí se debe validar los datos de acceso");

		final Session session = Session.getInstance(SuiteAppLogin.appContext);
		/* Validate user field */
		if (userCellPhone.length() == 0) {
			menuSuiteViewController
					.showInformationAlert(R.string.login_usernameCannotBeEmpty, createEnableButtonsClickListener());
			return false;
		} else if (userCellPhone.length() < bancomer.api.common.commons.Constants.TELEPHONE_NUMBER_LENGTH) {
			menuSuiteViewController
					.showInformationAlert(R.string.login_usernameTooShort, createEnableButtonsClickListener());
			return false;
		}

		/* Validate password field */
		if (userPass.length() == 0) {
			menuSuiteViewController
					.showInformationAlert(session.isApplicationActivated() ? R.string.error_passwordCannotBeEmptyForLogin
							: R.string.error_passwordCannotBeEmptyForActivation, createEnableButtonsClickListener());
			return false;
		} else if (userPass.length() < bancomer.api.common.commons.Constants.PASSWORD_LENGTH) {
			menuSuiteViewController
					.showInformationAlert(R.string.error_passwordTooShort, createEnableButtonsClickListener());
			return false;
		} /*
		deshabilita las validaciones de numero repetido y la de consecutivos
		else if (!validatePasswordPolicy1(userPass)) {
			menuSuiteViewController
					.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_numeros_repetidos);
			return false;
		} else if (!validatePasswordPolicy2(userPass)) {
			menuSuiteViewController
					.showInformationAlert(R.string.bmovil_contratacion_definicion_password_error_numeros_consecutivos);
			return false;
		}*/

		return true;
	}
	private DialogInterface.OnClickListener createEnableButtonsClickListener() {
		return new DialogInterface.OnClickListener() {

			@Override
			public void onClick(final DialogInterface dialog,final int which) {
				((MenuSuiteViewController) getMenuSuiteViewController()).setButtonsDisabled(false);
			}
		};
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.common.SessionStoredListener
	 * #sessionStored()
	 */


	/**
	 * Validates the new password according to the following policy: a password
	 * cannot have more than 2 repeated digits together, i.e: 123338 would be an
	 * invalid password
	 *
	 * @param pwd
	 *            the password to validate
	 * @return true if the password passes the policy , false if it fails
	 */
	private boolean validatePasswordPolicy1(final String pwd) {
		if (pwd == null) {
			return false;
		}
		boolean validate = true;
		for (int i = 0; ((i < pwd.length()) && (validate)); i++) {
			if (i > 1) {
				final char c0 = pwd.charAt(i - 2);
				final char c1 = pwd.charAt(i - 1);
				final char c2 = pwd.charAt(i);
				final boolean fails = ((c0 == c1) && (c1 == c2));
				validate = !fails;
			}
		}
		return validate;
	}

	/**
	 * Validates the new password according to the following policy: a password
	 * cannot have more than 2 digits consecutive, either ascending or
	 * descending. i.e., 12348 or 43276 would fail.
	 *
	 * @param pwd
	 *            the password to validate
	 * @return true if the password passes the policy , false if it fails
	 */
	private boolean validatePasswordPolicy2(final String pwd) {
		if (pwd == null) {
			return false;
		}
		boolean validate = true;
		for (int i = 0; ((i < pwd.length()) && (validate)); i++) {
			if (i > 1) {
				final char c0 = pwd.charAt(i - 2);
				final char c1 = pwd.charAt(i - 1);
				final char c2 = pwd.charAt(i);
				final boolean fails = (((c2 == c1 + 1) && (c1 == c0 + 1)) // ascending
						// order
						|| ((c2 == c1 - 1) && (c1 == c0 - 1)) // descending order
				);
				validate = !fails;
			}
		}
		return validate;
	}

/*
	private long comparaFechasEnDias(Date fecha1,Date fecha2){
			// Crear 2 instancias de Calendar
			Calendar cal1 = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();
			cal1.setTime(fecha1);
			cal2.setTime(fecha2);
			// conseguir la representacion de la fecha en milisegundos
			long milis1 = cal1.getTimeInMillis();
			long milis2 = cal2.getTimeInMillis();

			// calcular la diferencia en milisengundos
			long diff = milis2 - milis1;

			final long diffDays = diff / (24 * 60 * 60 * 1000);
			return diffDays;
	}
*/
	private long comparaFechasEnDias(final String fecha1p,final String fecha2p){
		final SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		Date fecha1 = null;
		Date fecha2=null;
		try {
			fecha1 = dateFormat.parse(fecha1p);
			fecha2 = dateFormat.parse(fecha2p);
		}catch (ParseException pe){
			if(Server.ALLOW_LOG)
				Log.e("contratacion fechas","Error en converison de fechas");
			return 0;
		}

		// Crear 2 instancias de Calendar
		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTime(fecha1);
		cal2.setTime(fecha2);
		// conseguir la representacion de la fecha en milisegundos
		long milis1 = cal1.getTimeInMillis();
		long milis2 = cal2.getTimeInMillis();

		// calcular la diferencia en milisengundos
		long diff = milis2 - milis1;

		final long diffDays = diff / (24 * 60 * 60 * 1000);
		return diffDays;
	}

	private void showCambiaPerfil(){
		SuiteAppAdmonApi suiteAppAdmonApi = new SuiteAppAdmonApi();
		suiteAppAdmonApi.onCreate(SuiteAppLogin.appContext);
		SuiteAppAdmonApi.getInstance().getBmovilApplication().setApplicationLogged(Server.ALLOW_LOG);
		SuiteAppAdmonApi.setCallBackBConnect((CallBackBConnect) SuiteAppLogin.getInstance().getCallBackLogin());
		SuiteAppAdmonApi.setCallBackSession(null);
		final CambioPerfilDelegate cambioPerfilDelegate = new CambioPerfilDelegate();

		// Se valida si existe un cambio de perfil de basico a avanzado

				cambioPerfilDelegate
						.mostrarCambioPerfil((Activity)SuiteAppLogin.appContext);


	}
}
