package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.login;



import com.bancomer.mbanking.login.R;
import com.bancomer.mbanking.login.SuiteAppLogin;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.gui.controllers.login.BaseViewController;
import suitebancomer.classes.gui.delegates.login.BaseDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Rapido;
import suitebancomercoms.aplicaciones.bmovil.classes.model.SolicitarAlertasData;

public class DelegateBaseOperacion extends BaseDelegate {
	
	/**
     * Parametro para mostrar la opcion de menu SMS
     */
    public final static int SHOW_MENU_SMS = 2;
    
    /**
     * Parametro para mostrar la opcion de menu Correo electrónico
     */
    public final static int SHOW_MENU_EMAIL = 4;
    
    /**
     * Parametro para mostrar la opcion de menu PDF
     */
    public final static int SHOW_MENU_PDF = 8;
    
    /**
     * Parametro para mostrar la opcion de menu Frecuente
     */
    public final static int SHOW_MENU_FRECUENTE = 16;
    
    /**
     * Parametro para mostrar la opcion de menu Rapida
     */
    public final static int SHOW_MENU_RAPIDA = 32;
    
    /**
     * Parametro para mostrar la opcion de menu Rapida
     */
    public final static int SHOW_MENU_BORRAR = 64;
    
    /**
     * Respuesta del servidor para mostrar errores de transferencias
     */
    protected ServerResponse transferErrorResponse = null;
    protected SolicitarAlertasData sa;
    
    
	
	public ServerResponse getTransferErrorResponse() {
		return transferErrorResponse;
	}

	public void setTransferErrorResponse(final ServerResponse transferErrorResponse) {
		this.transferErrorResponse = transferErrorResponse;
	}
	
	

	public SolicitarAlertasData getSa() {
		return sa;
	}

	public void setSa(final SolicitarAlertasData sa) {
		this.sa = sa;
	}

	public String getTextoAyudaInstrumentoSeguridad(final TipoInstrumento tipoInstrumento) { return ""; }
	
	public String getEtiquetaCampoNip() { return ""; }
	
	public String getEtiquetaCampoContrasenia() { return ""; }
	
	public String getEtiquetaCampoOCRA() { return ""; }
	
	public String getEtiquetaCampoDP270() { return ""; }
	
	public String getEtiquetaCampoSoftokenActivado() { return ""; }
	
	public String getEtiquetaCampoSoftokenDesactivado() { return ""; }
	
	public String getEtiquetaCampoCVV() { return ""; }
	
	public int getOpcionesMenuResultados() { return 0; }
	
	public String getTextoSMS() { return ""; }
	
	public String getTextoEmail() { return ""; }
	
	public String getTextoPDF() { return ""; }
	
	public String getTituloTextoEspecialResultados() { return ""; }

	public String getTextoEspecialResultados() { return ""; }
	
	public String getTextoPantallaResultados() { return ""; }
	
	public boolean borraRapido() { return false; }
	
	public String getTextoTituloResultado() { return ""; }
	
	public int getColorTituloResultado() { return android.R.color.black; }
	
	public String getTextoTituloConfirmacion() { return ""; }
	
	public ArrayList<Object> getDatosTablaConfirmacion() { return null; }
	
	public ArrayList<Object> getDatosTablaResultados() { return null; }
	
	public String getTextoAyudaNIP() { return ""; }
	
	public String getTextoAyudaCVV() { return ""; }
	
	/**
	 * Consult if the password field should be visible.
	 * @return A Boolean with the flag, or null if an error occurred.
	 * @throws Exception
	 */
	public boolean mostrarContrasenia() { return false; }
	
	/**
	 * Consult the type of token that should be shown.

	 * @return The token type.
	 * @throws Exception
	 */
	public TipoOtpAutenticacion tokenAMostrar() { return null; }
	
	/**
	 * Consult if the NIP field should be visible.
	 * @return A Boolean with the flag, or null if an error occurred.
	 * @throws Exception
	 */
	public boolean mostrarNIP() { return false; }
	
	public boolean mostrarCVV() { return false; }
	
	public ArrayList<Object> getDatosTablaAltaFrecuentes() { return null; }
	
	public ArrayList<String> getDatosTablaAltaRapidos() { return null; }
	
	public int getNombreImagenEncabezado() { return -1; }
	
	public int getTextoEncabezado() { return -1; }
	
	public String getTextoBotonOperacionNueva() { return ""; }
	
	public String getTextoAyudaResultados(){ return ""; }
	
	public void consultarFrecuentes(final String tipoFrecuente) {}
	
	public ArrayList<Object> getDatosHeaderTablaFrecuentes() { return null; }
	
	public ArrayList<Object> getDatosTablaFrecuentes() { return null; }

	public Hashtable<String, String> getParametrosAltaFrecuentes(){return null;}
	
	public void operarFrecuente(final int indexSelected){}
	
	public void eliminarFrecuente(final int indexSelected){}
	/**
	 * metodo para realizar la Acci�n del botón pantalla de resultados autenticación
	 * 
	 */
	protected void accionBotonResultados(){}
	protected int getImagenBotonResultados(){return 0;}
	
	/**
	 * Operar un r�pido.
	 * @param rapido El r�pido a operar.
	 */
	public void operarRapido(final Rapido rapido) {return;}
	
	/**
	 * Lista de datos para mostrar en la pantalla de RegistarOperacion
	 * @return ArrayList con los datos a mostrar en la pantalla de RegistarOperacion
	 */
	protected ArrayList<Object> getDatosRegistroOp() {return null;}
	
	/**
	 * @return  ArrayList con los datos a mostrar en la tabla de RegistroOperacionExitoso
	 */
	public ArrayList<Object> getDatosRegistroExitoso() {return null;}
	

	
	

	
	
	
	/**
	 * Obtiene los ultimos 5 digitos de la cuenta para el registro de operación.
	 */
	public String getNumeroCuentaParaRegistroOperacion() { return "00000"; }
	

	

	
	public String getEtiquetaCampoTarjeta(){
		return SuiteAppLogin.appContext.getString(R.string.confirmation_componente_digitos_tarjeta);
		//return "Ingrese los últimos 5 dígitos de su tarjeta";
	}
	
	public String getTextoAyudaTarjeta() {
		return SuiteAppLogin.appContext.getString(R.string.confirmation_ayuda_componente_digitos_tarjeta);
		//return "Son los últimos dígitos de tu tarjeta";		
	}
	
	public boolean mostrarCampoTarjeta(){
		return false;
	}
	
	public void solicitarAlertas (final BaseViewController caller){
		final Session session = Session.getInstance(SuiteAppLogin.appContext);
		final Account cEje = Tools.obtenerCuentaEje();
		
		final Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		paramTable.put(ServerConstants.NUMERO_TARJETA, cEje.getNumber());
		paramTable.put(ServerConstants.COMPANIA_CELULAR,session.getCompaniaUsuario());
	
	
		doNetworkOperation(Server.OP_SOLICITAR_ALERTAS, paramTable, caller);
	}


}
