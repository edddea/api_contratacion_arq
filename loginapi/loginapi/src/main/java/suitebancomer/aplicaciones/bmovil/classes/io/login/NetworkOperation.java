/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.bmovil.classes.io.login;

import java.util.Hashtable;

import suitebancomer.classes.gui.controllers.login.BaseViewController;

/**
 * Private class, that represents the data operation being done
 * @author Stefanini IT Solutions.
 */
public class NetworkOperation {

    /**
     * The operation identifier
     */
    private int id;

    /**
     * the parameters table
     */
    private Hashtable<String, ?> params;

    /**
     * The caller of the operation
     */
    private BaseViewController caller;

    /**
     * Flag to determine if the operation is still active. This flag
     * is used to know if the user has cancelled the operation before
     * it ended, so the application will not take into account the
     * result of this operation
     */
    private boolean active = true;

    /**
     * Default constructor
     * @param id operation identifier
     * @param params operation parameters
     * @param caller the caller screen
     */
    public NetworkOperation(final int id,final  Hashtable<String, ?> params,final  BaseViewController caller) {
        this.id = id;
        this.params = params;
        this.caller = caller;
        this.active = true;
    }

    /**
     * Get the reference to the caller screen
     * @return the reference to the caller screen
     */
    public BaseViewController getCaller() {
        return caller;
    }

    /**
     * Set the reference to the caller screen
     * @param caller the reference to the caller screen
     */
    public void setCaller(final BaseViewController caller) {
        this.caller = caller;
    }

    /**
     * Get the operation identifier
     * @return operation identifier
     */
    public int getId() {
        return id;
    }

    /**
     * Set the operation identifier
     * @param id operation identifier
     */
    public void setId(final int id) {
        this.id = id;
    }

    /**
     * Get the parameters table
     * @return the parameters table
     */
    public Hashtable<String, ?> getParams() {
        return params;
    }

    /**
     * Set the parameters table
     * @param params the parameters table
     */
    public void setParams(final Hashtable<String, ?> params) {
        this.params = params;
    }

    /**
     * Get if the network operation is still active, that is, it has
     * not been cancelled
     * @return true if the application is active, false if not.
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Set if the network operation is still active, that is, it has
     * not been cancelled
     * @param active true if the application is active, false if not.
     */
    public void setActive(final boolean active) {
        this.active = active;
    }
}
