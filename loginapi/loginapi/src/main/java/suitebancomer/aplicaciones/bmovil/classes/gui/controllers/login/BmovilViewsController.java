package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.login;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Looper;

import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.mbanking.activacion.SuiteApp;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;
import com.bancomer.mbanking.login.BmovilApp;
import com.bancomer.mbanking.login.R;
import com.bancomer.mbanking.login.SuiteAppLogin;
import com.bancomer.mbanking.softtoken.SuiteAppApi;
import java.util.ArrayList;

import mtto.suitebancomer.aplicaciones.bmovil.classes.entrada.InitMantenimiento;
import suitebancomer.activacion.aplicaciones.bmovil.classes.entrada.InitActivacion;
import suitebancomer.aplicaciones.bmovil.classes.entrada.InitContratacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.entradas.InitReactivacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioPerfilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.login.ConsultaTerminosDeUsoData;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.AutenticacionSTViewController;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.IngresoDatosSTViewController;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.TerminosCondicionesStViewController;
import suitebancomer.classes.gui.controllers.login.BaseViewsController;
import suitebancomer.classes.gui.controllers.login.MenuSuiteViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.BanderasServer;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.LoginData;

//import com.bancomer.mbanking.reactivacion.SuiteAppApiReactivacion;

public class BmovilViewsController extends BaseViewsController {

	private BmovilApp bmovilApp;
	// AMZ
	public ArrayList<String> estados = new ArrayList<String>();
	//private MenuPrincipalViewController controladorMenuPrincipal = new MenuPrincipalViewController();

	// AMZ

	/*
	 * @Override public void setCurrentActivity(BaseViewController
	 * currentViewController) { // TODO Auto-generated method stub
	 * super.setCurrentActivity(currentViewController); Log.i("Si", "No"); }
	 */
	public BmovilViewsController(final BmovilApp bmovilApp) {
		super();
		this.bmovilApp = bmovilApp;
	}



	public void cerrarSesionBackground() {
		SuiteAppLogin.getInstance().getBmovilApplication()
				.setApplicationInBackground(true);
		bmovilApp.logoutApp(true);
	}


	public BmovilApp getBmovilApp() {
		return bmovilApp;
	}

	public void setBmovilApp(final BmovilApp bmovilApp) {
		this.bmovilApp = bmovilApp;
	}


	/**
	 * Muestra la pantalla de ingresar datos de contratación.
	 */

	public void showContratacion(final ConsultaEstatus consultaEstatus,
								 final String estatusServicio,final  boolean deleteData) {


        //__________- invocacion al api de softoken ___________//
        final SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/

        sftoken.onCreate(SuiteAppLogin.appContext);
        sftoken.setIntentToReturn(SuiteAppLogin.getInstance().getCallBackContratacion());
        sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());

        final suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
                ServerCommons.SIMULATION,ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR,ServerCommons.DEVELOPMENT
        );
        //inicializa la clase
        final InitContratacion initi=new InitContratacion(SuiteAppLogin.appContext, SuiteAppLogin.getInstance().getCallBackContratacion(), SuiteAppLogin.getInstance().getActivityForContratacion(),bs);
        //raliza el start del api
        initi.startContratacion(consultaEstatus,estatusServicio, deleteData);
	}

//	// /**
//	// * Muestra la pantalla de ingresar datos de contratación.
//	// */
	public void showContratacion() {
//		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new ContratacionDelegate();
//			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(IngresarDatosViewController.class);



        //__________- invocacion al api de softoken ___________//
        final SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/

        sftoken.onCreate(this.getCurrentViewController());
        sftoken.setIntentToReturn(SuiteAppLogin.getInstance().getCallBackContratacion());
        sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());


        //__________- invocacion al api de contratacion ___________//
        final suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
                ServerCommons.SIMULATION,ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR,ServerCommons.DEVELOPMENT
        );
        //inicializa la clase
        final InitContratacion initi=new InitContratacion(this.getCurrentViewController(), SuiteAppLogin.getInstance().getCallBackContratacion(), SuiteAppLogin.getInstance().getActivityForContratacion(),bs);
        //raliza el start del api
        initi.startContratacion(null, null, false);

	}
//
	public void showReactivacion(final ConsultaEstatus consultaEstatus) {
		//ReactivacionDelegate delegate = new ReactivacionDelegate();
		//suitebancomer.classes.gui.controllers.BaseViewsControllerCommons baseViewsController = SuiteAppLogin.getInstance().getBmovilApplication().getBmovilViewsController();
		//baseViewsController.addDelegateToHashMap(
		//		ReactivacionDelegate.REACTIVACION_DELEGATE_ID,
        //        delegate);
		//suitebancomer.classes.gui.controllers.BaseViewControllerCommons baseViewController = SuiteAppLogin.getInstance().getBmovilApplication().get.getBmovilViewController();
		// showViewController(ConfirmacionAutenticacionViewController.class, baseViewController);
		//showConfirmacionAutenticacionViewController(delegate, 0, 0, 0);
		final BanderasServer bandera = new BanderasServer(ServerCommons.SIMULATION,
				ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT);
		final InitReactivacion initReactivacion = new InitReactivacion(bandera, SuiteAppLogin.getInstance().getActivityReactivacion(),
				SuiteAppLogin.getInstance().getCallBackReactivacion(), SuiteAppLogin.getInstance().getActivityReactivacion());
		final String instrumento = Session.getInstance(SuiteAppLogin.appContext).getSecurityInstrument();
		final Session sessionApis = Session.getInstance(SuiteAppLogin.appContext);
		sessionApis.setSecurityInstrument(instrumento);
		SuiteApp.setCallsAppDesactivada(Boolean.TRUE);
		initReactivacion.showReactivacion(consultaEstatus,
				SuiteAppLogin.getInstance().getBmovilApplication().getBmovilViewsController().estados);
	}

	public void showReactivacion(final ConsultaEstatus consultaEstatus,
								 final String password) {


		final suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus consultaEstatusReac = new suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus();
		consultaEstatusReac.setPerfil(consultaEstatus.getPerfil());
		consultaEstatusReac.setPerfilAST(consultaEstatus.getPerfilAST());
		consultaEstatusReac.setNumTarjeta(consultaEstatus.getNumTarjeta());
		consultaEstatusReac.setNumCliente(consultaEstatus.getNumCliente());
		consultaEstatusReac.setNumCelular(consultaEstatus.getNumCelular());
		consultaEstatusReac.setNombreCliente(consultaEstatus.getNombreCliente());
		consultaEstatusReac.setCompaniaCelular(consultaEstatus.getCompaniaCelular());
		consultaEstatusReac.setEmailCliente(consultaEstatus.getEmailCliente());
		consultaEstatusReac.setEstatus(consultaEstatus.getEstatus());
		consultaEstatusReac.setEstatusAlertas(consultaEstatus.getEstatusAlertas());
		consultaEstatusReac.setEstatusInstrumento(consultaEstatus.getEstatusInstrumento());
		consultaEstatusReac.setInstrumento(consultaEstatus.getInstrumento());
		final BanderasServer bandera = new BanderasServer(ServerCommons.SIMULATION,
				ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT);

		final InitReactivacion initReactivacion = new InitReactivacion(bandera, SuiteAppLogin.getInstance().getActivityReactivacion(),SuiteAppLogin.getInstance().getCallBackReactivacion(),SuiteAppLogin.getInstance().getActivityReactivacion() );
		final String instrumento = Session.getInstance(SuiteAppLogin.appContext).getSecurityInstrument();
		final suitebancomercoms.aplicaciones.bmovil.classes.common.Session sessionApis = suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(SuiteAppLogin.appContext);
		sessionApis.setSecurityInstrument(instrumento);
		initReactivacion.showReactivacion(consultaEstatusReac, password,
				SuiteAppLogin.getInstance().getBmovilApplication().getBmovilViewsController().estados);
	}
//
//	// /**
//	// * Muestra la pantalla de definir contraseña la contratación.
//	// */
	 public void showDefinirPassword() {

         //__________- invocacion al api de softoken ___________//
        final SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/

         sftoken.onCreate(this.getCurrentViewController());
         sftoken.setIntentToReturn(SuiteAppLogin.getInstance().getCallBackContratacion());
         sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());

        final suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs = new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
                 ServerCommons.SIMULATION,ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR,ServerCommons.DEVELOPMENT
         );
         //inicializa la clase
         final InitContratacion initi = new InitContratacion(this.getCurrentViewController(), null, new MenuSuiteViewController(), bs);
         //raliza el start del api
         initi.startContratacionDefinicionPassword();
     }


	 /**
	 * Muestra la pantalla de confirmaci�n autenticación para el estatus
	 PS.
	 */

	public void showContratacionAutenticacion() {

        //__________- invocacion al api de softoken ___________//
        final SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/

        sftoken.onCreate(this.getCurrentViewController());
        sftoken.setIntentToReturn(SuiteAppLogin.getInstance().getCallBackContratacion());
        sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());


        final suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
                ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT
        );
        //inicializa la clase
        final InitContratacion initi=new InitContratacion(this.getCurrentViewController(), SuiteAppLogin.getInstance().getCallBackContratacion(), SuiteAppLogin.getInstance().getActivityForContratacion(),bs);
        //raliza el start del api
        initi.startContratacionAutenticacion();
	}


	 public void showActivacion(final ConsultaEstatus ce, final String password) {

		final InitActivacion initActivacion = new InitActivacion(new BanderasServer(ServerCommons.SIMULATION,
				 ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT),
				 SuiteAppLogin.getInstance().getActivityActivacion(),
				 SuiteAppLogin.getInstance().getCallBackActivacion(),
				 SuiteAppLogin.getInstance().getActivityActivacion());
		 final String instrumento = Session.getInstance(SuiteAppLogin.appContext).getSecurityInstrument();
		 final Session sessionApis = Session.getInstance(SuiteAppLogin.appContext);
		 sessionApis.setSecurityInstrument(instrumento);
		 SuiteApp.setCallsAppDesactivada(Boolean.TRUE);
		 initActivacion.showActivacion(ce, password, estados);
	}

	public void touchAtras() {

		final int ultimo = estados.size() - 1;

		if (ultimo >= 0) {
			final String ult = estados.get(ultimo);
			if (ult == "reactivacion" || ult == "activacion"
					|| ult == "menu token" || ult == "contratacion datos"
					|| ult == "activacion datos") {
				estados.clear();
			} else {
				estados.remove(ultimo);
			}
		}
	}





	/**
	 * Muestra la pantalla para desbloqueo
	 *
	 * @param ce
	 */
	public void showDesbloqueo(final ConsultaEstatus ce) {

		final mtto.suitebancomer.aplicaciones.bmovil.classes.model.BanderasServer banderas=
				new mtto.suitebancomer.aplicaciones.bmovil.classes.model.BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT);
		final InitMantenimiento initMantenimiento=new InitMantenimiento(SuiteAppLogin.appContext, SuiteAppLogin.getInstance().getCallBackForMantenimiento(),SuiteAppLogin.getInstance().getActivityForMantenimiento() , banderas);

		final suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus consutaEstats=new suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus();
		consutaEstats.setPerfil(ce.getPerfil());
		consutaEstats.setPerfilAST(ce.getPerfilAST());
		consutaEstats.setNumTarjeta(ce.getNumTarjeta());
		consutaEstats.setNumCliente(ce.getNumCliente());
		consutaEstats.setCompaniaCelular(ce.getCompaniaCelular());
		consutaEstats.setNumCelular(ce.getNumCelular());
		consutaEstats.setEmailCliente(ce.getEmailCliente());
		consutaEstats.setEstatus(ce.getEstatus());
		consutaEstats.setEstatusAlertas(ce.getEstatusAlertas());
		consutaEstats.setEstatusInstrumento(ce.getEstatusInstrumento());
		consutaEstats.setInstrumento(ce.getInstrumento());
		consutaEstats.setNombreCliente(ce.getNombreCliente());
		initMantenimiento.startDesbloqueo(consutaEstats);
	}

	/**
	 * Muestra la confirmacion para quitar la suspension
	 *
	 * @param ce
	 */
	public void showQuitarSuspension(final ConsultaEstatus ce) {

		final mtto.suitebancomer.aplicaciones.bmovil.classes.model.BanderasServer banderas=
				new mtto.suitebancomer.aplicaciones.bmovil.classes.model.BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT);
		final InitMantenimiento initMantenimiento=new InitMantenimiento(SuiteAppLogin.appContext, SuiteAppLogin.getInstance().getCallBackForMantenimiento(),SuiteAppLogin.getInstance().getActivityForMantenimiento() , banderas);
		final suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus consutaEstats=new suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus();
		consutaEstats.setPerfil(ce.getPerfil());
		consutaEstats.setPerfilAST(ce.getPerfilAST());
		consutaEstats.setNumTarjeta(ce.getNumTarjeta());
		consutaEstats.setNumCliente(ce.getNumCliente());
		consutaEstats.setCompaniaCelular(ce.getCompaniaCelular());
		consutaEstats.setNumCelular(ce.getNumCelular());
		consutaEstats.setEmailCliente(ce.getEmailCliente());
		consutaEstats.setEstatus(ce.getEstatus());
		consutaEstats.setEstatusAlertas(ce.getEstatusAlertas());
		consutaEstats.setEstatusInstrumento(ce.getEstatusInstrumento());
		consutaEstats.setInstrumento(ce.getInstrumento());
		consutaEstats.setNombreCliente(ce.getNombreCliente());

		initMantenimiento.startQuitarSuspencion(consutaEstats);

	}

	public void showActivacion(final ConsultaEstatus ce, final String password,
							   final boolean appActivada) {
		if (appActivada) {
			SuiteAppLogin.getInstance()
					.getSuiteViewsController()
					.getCurrentViewControllerApp()
					.showInformationAlert(
							R.string.bmovil_activacion_alerta_reactivar,
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(final DialogInterface dialog,
													final int which) {

									dialog.dismiss();
									SuiteAppLogin.getInstance()
											.getSuiteViewsController()
											.getCurrentViewControllerApp().runOnUiThread(new Runnable() {
										@Override
										public void run() {
											final String title = "";
											final String msg = SuiteAppLogin.appContext.getString(R.string.alert_StoreSession);
											SuiteAppLogin.getInstance()
													.getSuiteViewsController()
													.getCurrentViewControllerApp().muestraIndicadorActividad(title, msg);
											new Thread(new Runnable() {
												public void run() {
													Looper.prepare();
													final InitActivacion initActivacion = new InitActivacion(new BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT),
															SuiteAppLogin.getInstance().getActivityActivacion() ,
															SuiteAppLogin.getInstance().getCallBackActivacion(),SuiteAppLogin.getInstance().getActivityActivacion());

													final suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus consultaEstatus = new suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus();
													consultaEstatus.setCompaniaCelular(ce.getCompaniaCelular());
													consultaEstatus.setEmailCliente(ce.getEmailCliente());
													consultaEstatus.setEstatus(ce.getEstatus());
													consultaEstatus.setEstatusAlertas(ce.getEstatusAlertas());
													consultaEstatus.setEstatusInstrumento(ce.getEstatusInstrumento());
													consultaEstatus.setInstrumento(ce.getInstrumento());
													consultaEstatus.setNombreCliente(ce.getNombreCliente());
													consultaEstatus.setNumCelular(ce.getNumCelular());
													consultaEstatus.setNumCliente(ce.getNumCliente());
													consultaEstatus.setNumTarjeta(ce.getNumTarjeta());
													consultaEstatus.setPerfilAST(ce.getPerfilAST());
													consultaEstatus.setPerfil(ce.getPerfil());
													initActivacion.borraDatos(consultaEstatus, password, estados);
													SuiteAppLogin.getInstance()
															.getSuiteViewsController()
															.getCurrentViewControllerApp().ocultaIndicadorActividad();
													Looper.loop();
												}
											}).start();
										}
									});
								}
							});
		}
	}

	/**
	 * Muestra la pantalla de autenticacion softoken.
	 * @param currentPassword
	 * @param consultaEstatus
	 */
	public void showAutenticacionSoftoken(final ConsultaEstatus consultaEstatus,final  String currentPassword) {

		final String[] extrasKeys= {"login"};
		final Object[] extras= {true};

		//__________- invocacion al api de contratacion ___________//
		final suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
				ServerCommons.SIMULATION,ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR,ServerCommons.DEVELOPMENT
		);
		//inicializa la clase
		final InitContratacion initi=new InitContratacion(SuiteAppLogin.appContext,SuiteAppLogin.getInstance().getCallBackContratacion(),SuiteAppLogin.getInstance().getActivityForContratacion(),bs);


		//__________- invocacion al api de softoken ___________//
		final SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/

		sftoken.onCreate(SuiteAppLogin.appContext);
		sftoken.setIntentToReturn(SuiteAppLogin.getInstance().getCallBackForSofttoken());
		sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());
		sftoken.showViewController(IngresoDatosSTViewController.class, 0, false, extrasKeys, extras, this.getCurrentViewController());

	}

	/**
	 * Muestra la pantalla de autenticacion softoken.
	 */
	public void showAutenticacionST() {


		//__________- invocacion al api de contratacion ___________//
		final suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
				ServerCommons.SIMULATION,ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR,ServerCommons.DEVELOPMENT
		);
		//inicializa la clase
		final InitContratacion initi=new InitContratacion(SuiteAppLogin.appContext,SuiteAppLogin.getInstance().getCallBackContratacion(),SuiteAppLogin.getInstance().getActivityForContratacion(),bs);


		//__________- invocacion al api de softoken ___________//
		final SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/

		sftoken.onCreate(SuiteAppLogin.appContext);
		sftoken.setIntentToReturn(SuiteAppLogin.getInstance().getCallBackForSofttoken());
		sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());
		sftoken.showViewController(AutenticacionSTViewController.class, 0, false, new String[]{}, new  Object[]{},(Activity) SuiteAppLogin.appContext);

	}


	public void showCambioPerfil(LoginData data,ConsultaEstatus consultaEstatus,ServerResponse serverResponse){
		SuiteAppAdmonApi suiteAppAdmonApi = new SuiteAppAdmonApi();
		suiteAppAdmonApi.onCreate(SuiteAppLogin.appContext);
		SuiteAppAdmonApi.getInstance().getBmovilApplication().setApplicationLogged(ServerCommons.ALLOW_LOG);
		SuiteAppAdmonApi.setCallBackBConnect((CallBackBConnect) SuiteAppLogin.getInstance().getCallBackLogin());
		SuiteAppAdmonApi.setCallBackSession(null);
		//SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(Boolean.TRUE);
		SuiteAppLogin.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(Boolean.TRUE);
		final CambioPerfilDelegate cambioPerfilDelegate = new CambioPerfilDelegate();
		Session session = Session.getInstance(SuiteAppLogin.appContext);
		// Se valida si existe un cambio de perfil de basico a avanzado
		cambioPerfilDelegate.setIsFlujoLogin(true);
		cambioPerfilDelegate.setData(data);
		cambioPerfilDelegate.setConsultaEstatus(consultaEstatus);
		cambioPerfilDelegate.setResponseGlobal(serverResponse);
		SuiteAppAdmonApi.getInstance().getBmovilApplication().getBmovilViewsController().addDelegateToHashMap(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID, cambioPerfilDelegate);
		cambioPerfilDelegate
		.mostrarCambioPerfil((Activity) SuiteAppLogin.appContext);


	}

	public void showTerminosYcondicionesST(ConsultaTerminosDeUsoData terminos){

		//__________- invocacion al api de contratacion ___________//
		final suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
				ServerCommons.SIMULATION,ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR,ServerCommons.DEVELOPMENT
		);
		//inicializa la clase
		final InitContratacion initi=new InitContratacion(SuiteAppLogin.appContext,SuiteAppLogin.getInstance().getCallBackContratacion(),SuiteAppLogin.getInstance().getActivityForContratacion(),bs);


		//__________- invocacion al api de softoken ___________//
		final SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/

		sftoken.onCreate(SuiteAppLogin.appContext);
		sftoken.setIntentToReturn(SuiteAppLogin.getInstance().getCallBackForSofttoken());
		sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());
		sftoken.showViewController(TerminosCondicionesStViewController.class, 0, false, new String[]{suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.TERMINOS_DE_USO_EXTRA}, new  Object[]{new String(terminos.getTerminosHtml())},(Activity) SuiteAppLogin.appContext);

	}

}