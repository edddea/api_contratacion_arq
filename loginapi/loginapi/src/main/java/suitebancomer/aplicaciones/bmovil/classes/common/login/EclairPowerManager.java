/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.common.login;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

import com.bancomer.mbanking.login.SuiteAppLogin;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

/**
 * Implementation of the App power manager for api7 or greater.
 */
public final class EclairPowerManager extends AbstractSuitePowerManager {
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.common.AbstractSuitePowerManager#isScreenOn()
	 */
	@Override
	public boolean isScreenOn() {
		try {
			final PowerManager pm = (PowerManager) SuiteAppLogin.appContext.getSystemService(Context.POWER_SERVICE);
			return pm.isScreenOn();
		}catch(Exception ex) {
			if(Server.ALLOW_LOG) Log.wtf("EclairPowerManager", "Error while getting the state of the screen.", ex);
			return false;
		}
	}

}
