package tracking.login;

import com.adobe.mobile.Analytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TrackingHelper {
	
	public static void trackState(final String state,final  ArrayList<String> mapa){
	      final HashMap<String, Object> purchaseDictionary = new HashMap<String,Object>();
	       purchaseDictionary.put("channel", "android");

	       mapa.add(state);
	       String listString = "";

	       for (final String s : mapa)
	       {
	           listString += ":" + s;
	       }
	       final String res = (purchaseDictionary.get("channel").toString() + listString);
		if(mapa.size() >= 1 && mapa.size()<=11){
			purchaseDictionary.put("prop"+mapa.size(),res);
		}

	       
	       
	       purchaseDictionary.put("appState",res);
	       purchaseDictionary.put("hier1",res);
	       
	       //ARR Encriptacion
	       
//	       if(LoginDelegate.encriptacionEnLogin)
//	       {
//	    	   String nombreUsuario = LoginDelegate.getNombreUsuario();
//		       String nombreUsuarioEncriptado = null;
//		       try 
//		       {
//				nombreUsuarioEncriptado = new String(TripleDESEncryptionHelper.encryptText(nombreUsuario));
//		       } catch (Exception e) 
//		       {
//				// TODO Auto-generated catch block
//		    	   if(Server.ALLOW_LOG) e.printStackTrace();
//		       }
//
//		       purchaseDictionary.put("eVar15", nombreUsuarioEncriptado);
//		       purchaseDictionary.put("prop15", nombreUsuarioEncriptado);
//		       purchaseDictionary.put("eVar16", "personas");
//		       purchaseDictionary.put("prop16", "personas");
//	       }
//	       else if(ConsultaEstatusAplicacionDesactivadaDelegate.res)
//	       {
//	    	   String nombreUsuario = ConsultaEstatusAplicacionDesactivadaViewController.getNombreUsuario();
//	    	   
//	    	   String nombreUsuarioEncriptado = null;
//		       try 
//		       {
//				nombreUsuarioEncriptado = new String(TripleDESEncryptionHelper.encryptText(nombreUsuario));
//		       } catch (Exception e) 
//		       {
//				// TODO Auto-generated catch block
//		    	   if(Server.ALLOW_LOG) e.printStackTrace();
//		       }
//
//		       purchaseDictionary.put("eVar15", nombreUsuarioEncriptado);
//		       purchaseDictionary.put("prop15", nombreUsuarioEncriptado);
//		       purchaseDictionary.put("eVar16", "personas");
//		       purchaseDictionary.put("prop16", "personas");
//	    	   
//	       }
	       
	      
	       
	       
	       Analytics.trackState(res,purchaseDictionary);
	       
	}


	//ARR
	public static void trackClickLogin (final Map<String, Object> contextData){
	
		Analytics.trackAction("clickLogin", contextData);

	}
	
	//ARR	
	public static void trackDesconexiones(final Map<String, Object> contextData){
		
		Analytics.trackAction("clickDesconexiones", contextData);
	}
	
	//ARR
	public static void trackMenuPrincipal(final Map<String, Object> contextData){
		
		Analytics.trackAction("clickMenu", contextData);
		
	}
	
	//ARR
	public static void trackFrecuente(final Map<String, Object> contextData){
		
		Analytics.trackAction("clickFrecuentes", contextData);
	}
	
	//ARR
	public static void trackInicioOperacion(final Map<String, Object> contextData){
		Analytics.trackAction("inicioOperacion", contextData);
	}
	
	//ARR
	public static void trackPaso1Operacion(final Map<String, Object> contextData){
		Analytics.trackAction("paso1Operacion", contextData);
	}
	
	//ARR
	public static void trackPaso2Operacion(final Map<String, Object> contextData){
		Analytics.trackAction("paso2Operacion", contextData);
	}
	
	//ARR
		public static void trackPaso3Operacion(final Map<String, Object> contextData){
			Analytics.trackAction("paso3Operacion", contextData);
		}
		
	//ARR
		public static void trackOperacionRealizada(final Map<String, Object> contextData){
			Analytics.trackAction("operacionRealizada", contextData);
		}
	
	//ARR
		public static void trackInicioConsulta(final Map<String, Object> contextData){
			Analytics.trackAction("inicioConsulta", contextData);
		}
		
	//ARR
		public static void trackEnvioConfirmacion(final Map<String, Object> contextData){
			Analytics.trackAction("envioConfirmacion", contextData);
		}
		
		public static void trackConsultaRealizada(final Map<String, Object> contextData){
			Analytics.trackAction("consultaRealizada", contextData);
		}
		//ARR

			public static void trackClickBanner(final Map<String, Object> click_bannerMap,final Map<String, Object> click_bannerMap2) {
				Analytics.trackAction("clickBanner", click_bannerMap);
				// TODO Auto-generated method stub
			}

	public static void trackSimulacionRealizada (final Map<String, Object> click_simulacion_realizada) {
		Analytics.trackAction("clickSimulacionRealizada", click_simulacion_realizada);
	}

	public static void trackInicioSimulador (final Map<String, Object> click_inicio_simulador) {
		Analytics.trackAction("clickInicioSimulador", click_inicio_simulador);
	}
}
	

