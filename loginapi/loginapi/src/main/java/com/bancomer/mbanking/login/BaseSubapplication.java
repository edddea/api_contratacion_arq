package com.bancomer.mbanking.login;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.io.StringReader;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.login.AbstractSuitePowerManager;
import suitebancomer.aplicaciones.bmovil.classes.io.login.NetworkOperation;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.login.ConsultaTerminosDeUsoData;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;
import suitebancomer.aplicaciones.commservice.service.ICommService;
import suitebancomer.classes.gui.controllers.login.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.MigraBasicoData;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.model.LoginData;

public abstract class BaseSubapplication {
	// #region Variables.
	/**
	 * Determines if this activity is in process of changing screen.
	 */
	protected boolean changingActivity = false;
	
	protected boolean applicationLogged = false;
	
	protected boolean applicationInBackground = false;
	
	protected ICommService serverproxy = new CommServiceProxy(SuiteAppLogin.appContext);
	
	/**
	 * Reference to the main application.
	 */
	protected SuiteAppLogin suiteApp = null;
	
	/**
	 * Referencia al controlador de vistas.
	 */
	protected BaseViewsControllerCommons viewsController;

	/**
	 * Reference to a pending network operation, in case that an
	 * operation launched a session expired error. This operation
	 * will be relaunched after a successful login
	 */
	protected NetworkOperation pendingOperation = null;

	/**
	 * Reference to server communication handler.
	 */
	protected Server server;

	/**
	 * Pending network operations.
	 */
	protected Hashtable<Integer, NetworkOperation> networkOperationsTable = null;
	
	protected boolean isHomeKeyPressed = false;
	// #endregion
	
	// #region Getters y Setters.
	
	/**
	 * @return the changingActivity
	 */
	public boolean isChangingActivity() {
		return changingActivity;
	}

	private boolean isHomeKeyPressed() {
		return isHomeKeyPressed;
	}

	protected void setHomeKeyPressed(final boolean isHomeKeyPressed) {
		this.isHomeKeyPressed = isHomeKeyPressed;
	}

	/**
	 * @param changingActivity the changingActivity to set
	 */
	public void setChangingActivity(final boolean changingActivity) {
		this.changingActivity = changingActivity;
	}

	/**
	 * @return the applicationLogged
	 */
	public boolean isApplicationLogged() {
		return applicationLogged;
	}

	/**
	 * @param applicationLogged the applicationLogged to set
	 */
	public void setApplicationLogged(final boolean applicationLogged) {
		this.applicationLogged = applicationLogged;
	}

	/**
	 * @return the applicationInBackground
	 */
	public boolean isApplicationInBackground() {
		return applicationInBackground;
	}

	/**
	 * @param applicationInBackground the applicationInBackground to set
	 */
	public void setApplicationInBackground(final boolean applicationInBackground) {
		this.applicationInBackground = applicationInBackground;
	}

	/**
	 * @return the suiteApp
	 */
	public SuiteAppLogin getSuiteApp() {
		return suiteApp;
	}

	/**
	 * @param suiteApp the suiteApp to set
	 */
	public void setSuiteApp(final SuiteAppLogin suiteApp) {
		this.suiteApp = suiteApp;
	}

	/**
	 * @return the viewsController
	 */
	public BaseViewsControllerCommons getViewsController() {
		return viewsController;
	}

	/**
	 * @param viewsController the viewsController to set
	 */
	public void setViewsController(final BaseViewsControllerCommons viewsController) {
		this.viewsController = viewsController;
	}

	/**
	 * @return the pendingOperation
	 */
	public NetworkOperation getPendingOperation() {
		return pendingOperation;
	}

	/**
	 * @param pendingOperation the pendingOperation to set
	 */
	public void setPendingOperation(final NetworkOperation pendingOperation) {
		this.pendingOperation = pendingOperation;
	}

	/**
	 * @return the server
	 */
	public Server getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(final Server server) {
		this.server = server;
	}

	/**
	 * @return the networkOperationsTable
	 */
	public Hashtable<Integer, NetworkOperation> getNetworkOperationsTable() {
		return networkOperationsTable;
	}

	/**
	 * @param networkOperationsTable the networkOperationsTable to set
	 */
	public void setNetworkOperationsTable(
			final Hashtable<Integer, NetworkOperation> networkOperationsTable) {
		this.networkOperationsTable = networkOperationsTable;
	}
	// #endregion

	// #region Network.
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * predefined texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 */
	public void invokeNetworkOperation(final int operationId,final  Hashtable<String,?> params,final  BaseViewController caller,final Class<?> ojbResponse) {
		invokeNetworkOperation(operationId, params, caller, false,ojbResponse);
	}
	
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * predefined texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	public void invokeNetworkOperation(final int operationId,final  Hashtable<String,?> params,final  BaseViewController caller,
									   final boolean callerHandlesError,final Class<?> ojbResponse) {
		invokeNetworkOperation(operationId, params, caller,
				SuiteAppLogin.appContext.getString(R.string.alert_operation),
				SuiteAppLogin.appContext.getString(R.string.alert_connecting), callerHandlesError,ojbResponse);
	}

	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * custom texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param progressLabel text with the title of the progress popup
	 * @param progressMessage text with the content of the progress popup
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	protected void invokeNetworkOperation(final int operationId, final Hashtable<String, ?> params,
										  final BaseViewController caller,
										  final  String progressLabel,
										  final String progressMessage,
										  final boolean callerHandlesError,
										  final   Class<?> objResponse) {
		final Integer opId = Integer.valueOf(operationId);          

		if (isNotAlreadyCalling(opId)) {

			try {

				final NetworkOperation operation = new NetworkOperation(operationId, params, caller);
				this.networkOperationsTable.put(opId, operation);
				this.pendingOperation = operation;

//				//TODO quitar el numero de operacion en hardcode.
//				if (caller != null && 11 != operationId)
//				    caller.muestraIndicadorActividad(progressLabel, progressMessage);

				invokeNetworkOperationAux4(caller, progressLabel, progressMessage);

				final Thread thread = new Thread(new Runnable() {
					public void run() {
						try {
							ParametersTO parameters=new ParametersTO();
							parameters.setSimulation(ServerCommons.SIMULATION);
							if (!ServerCommons.SIMULATION) {
								parameters.setProduction(!ServerCommons.DEVELOPMENT);
								parameters.setDevelopment(ServerCommons.DEVELOPMENT);
							}
							parameters.setOperationId(opId.intValue());
                            parameters.setParameters(params.clone());
                            parameters.setJson((operationId==Server.OP_SOLICITAR_ALERTAS || operationId==Server.OP_MIGRA_BASICO || operationId==Server.OP_CONSULTAR_TERMINOS_SESION || operationId==Server.CONSULTA_MANTENIMIENTO));
                            IResponseService resultado =  serverproxy.request(parameters, objResponse);
							ServerResponse response=null;
							if(ApiConstants.OPERATION_SUCCESSFUL==resultado.getStatus()){
								if(operationId==Server.OP_SOLICITAR_ALERTAS){
									response= new ServerResponse((SolicitarAlertasData)resultado.getObjResponse());
								}else if(operationId==Server.OP_MIGRA_BASICO){
									response= new ServerResponse((MigraBasicoData)resultado.getObjResponse());
								}else if(operationId==Server.OP_CONSULTAR_TERMINOS_SESION){
									ParserJSON parse=new ParserJSON(resultado.getResponseString());
									ConsultaTerminosDeUsoData terminos= new ConsultaTerminosDeUsoData();
									terminos.process(parse);
									response= new ServerResponse(terminos);
								}else if( operationId==Server.CONSULTA_MANTENIMIENTO) {
									ParserJSON parser = new ParserJSON(resultado.getResponseString());
									response= new ServerResponse(objResponse.newInstance());
									response.process(parser);
								}else if(operationId==Server.LOGIN_OPERATION) {
									if(!ServerCommons.ARQSERVICE){
										StringReader reader = new StringReader(resultado.getResponseString());
										Parser parser = new Parser(reader);
										response= new ServerResponse(objResponse.newInstance());
										response.process(parser);
									}else{
										LoginData ld=new LoginData();
										//ld.process(new JSONObject(resultado.getResponseString()));
										response= new ServerResponse(ld);
									}
								}else{
									StringReader reader = new StringReader(resultado.getResponseString());
									Parser parser = new Parser(reader);
									response= new ServerResponse(objResponse.newInstance());
									response.process(parser);
								}
		                   	}else{
								throw new Exception();
							}
							//se podrian eliminar las 3 siguentes lineas pero
							//faltaria hacer todo el set de pruebas poara comprobar que no afecte
							String url=response.getUpdateURL();
							response = new ServerResponse(response.getStatus(), response.getMessageCode(), response.getMessageText(), response.getResponse());
							response.setUpdateURL(url);
							response.setResponsePlain(resultado.getResponseString());
							returnFromNetworkOperation(opId, response, caller, null, callerHandlesError);
						} catch (Exception t) {
							returnFromNetworkOperation(opId, null, caller, t, callerHandlesError);
						}
					}

				});

				// Starts the worker thread to perform network operation
				thread.start();

			} catch (Exception th) {
				returnFromNetworkOperation(opId, null, caller, th, callerHandlesError);
			}

		} else {
			// unable to process request, show error and return
			return;
		}

	}

	private void invokeNetworkOperationAux4(final BaseViewController caller,final  String progressLabel,final  String progressMessage) {
		if (caller != null) {
            if(AbstractSuitePowerManager.getSuitePowerManager().isScreenOn() && (!isHomeKeyPressed())) {
                caller.muestraIndicadorActividad(progressLabel, progressMessage);
            } else {
                if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "La aplicación estaba bloqueada.");
            }
        }
	}

	/**
	 * Called internally after a network operation has returned. It ends the progress
	 * popup and invokes method to analyze the result
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message from the server or method should handle in the default
	 * implementation
	 */
	protected void returnFromNetworkOperation(final Integer operationId, 
											  final ServerResponse response, 
											  final BaseViewController caller, 
											  final Throwable throwable, 
											  final boolean callerHandlesError) {

		if (caller != null) {
			invokeNetworkOperationAux(operationId, response, caller, throwable, callerHandlesError);
			
		} else { //TODO session timer expired, go to login screen
			final Thread thread = new Thread(new Runnable() {
				public void run() {
					try {
						analyzeNetworkResponse(operationId, response, throwable, null, callerHandlesError);
					} catch (Throwable t) {
						String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
						if(Server.ALLOW_LOG) Log.d(" :: returnFromNetworkOperation", message);
					}
				}
			});
			thread.run();
		}
	}

	private void invokeNetworkOperationAux(final Integer operationId, final ServerResponse response, final BaseViewController caller, final Throwable throwable, final boolean callerHandlesError) {
		if(!(operationId.equals(Server.MOVEMENTS_OPERATION))) {
        caller.ocultaIndicadorActividad();
        }

		caller.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    analyzeNetworkResponse(operationId, response, throwable, caller, callerHandlesError);
                } catch (Throwable t) {
                    if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Exception on analyzeNetworkResponse.", t);
                    if (caller != null) {
                        final String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
                        caller.showErrorMessage(message);
                    }
                }
            }
        });
	}

	// TODO revisar en hijos.
	/**
	 * Analyze the network response obtained from the server
	 * @param operationId network identifier. See Server class.
	 * @param response the ServerResponse instance built from the server response. It
	 * contains the type of result (success, failure), and the real content for
	 * the application business.
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	protected void analyzeNetworkResponse(final Integer operationId,
										  final ServerResponse response,
										  final Throwable throwable,
										  final BaseViewController caller,
										  final boolean callerHandlesError) {
		// get the caller           
		final NetworkOperation operation = this.networkOperationsTable.get(operationId);
		if (operation != null) {
			// remove the operation id from the table
			this.networkOperationsTable.remove(operationId);
			if (operation.isActive()) {
				if (response != null) {
					final BaseViewController baseViewController = operation.getCaller();
					final int resultCode = response.getStatus();
					switch (resultCode) {
					case ServerResponse.OPERATION_SUCCESSFUL:
					case ServerResponse.OPERATION_WARNING:
						caller.processNetworkResponse(operationId.intValue(), response);
											
						break;
					case ServerResponse.OPERATION_OPTIONAL_UPDATE:
						baseViewController.processNetworkResponse(operationId.intValue(), response);
						break;
					case ServerResponse.OPERATION_ERROR:
						analiseResponseAux(operationId, response, caller, callerHandlesError, baseViewController);
						break;
					case ServerResponse.OPERATION_SESSION_EXPIRED:
						this.pendingOperation = operation;
						Session.getInstance(SuiteAppLogin.appContext).setValidity(Session.INVALID_STATUS);
//						suiteApp.getSuiteViewsController().showMenuSuite(true);
						break;						
					default:
						caller.ocultaIndicadorActividad();
						caller.showErrorMessage(R.string.error_format);
						break;
					}
				} else {
					if (throwable != null) {
						caller.ocultaIndicadorActividad();
						caller.showErrorMessage(getErrorMessage(throwable));
					} else {
						caller.ocultaIndicadorActividad();
						caller.showErrorMessage(R.string.error_communications);
					}
				}
			}
		} else {
			caller.ocultaIndicadorActividad();
			caller.showErrorMessage(R.string.error_unexpected);
		}		
	}

	private void analiseResponseAux(final Integer operationId,final  ServerResponse response,final  BaseViewController caller,final  boolean callerHandlesError,final  BaseViewController baseViewController) {
		this.pendingOperation = null;
		final String errorCode = response.getMessageCode();
		final int operationError = operationId.intValue();

		if(cumpleCondicion(errorCode, operationError)){
            caller.showInformationAlert( response.getMessageText(), new OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog,final int which) {
                    SuiteAppLogin.getInstance().getBmovilApplication().closeSession(Session.getInstance(SuiteAppLogin.appContext).getUsername(),Session.getInstance(SuiteAppLogin.appContext).getIum(),Session.getInstance(SuiteAppLogin.appContext).getClientNumber());
                }
            });
        }else if ((operationError == Server.LOGIN_OPERATION) && (Constants.DEACTIVATION_ERROR_CODE.equals(errorCode))) {
            final Session session = Session.getInstance(SuiteAppLogin.appContext);
            final String username = session.getUsername();
            final String password = session.getPassword();
            Session.getInstance(SuiteAppLogin.appContext).setValidity(Session.UNSET_STATUS);

            final Bundle params = new Bundle();
            params.putString(Server.USERNAME_PARAM, username);
            params.putString(Server.PASSWORD_PARAM, password);
        } else {
			analiseResponseAux2(operationId, response, caller, callerHandlesError, baseViewController, errorCode, operationError);
		}
	}

	private boolean cumpleCondicion(final String errorCode,final  int operationError) {
		return (operationError != Server.QUITAR_SUSPENSION) && (operationError != Server.LOGIN_OPERATION) && (operationError != Server.OP_ENVIO_CLAVE_ACTIVACION) && (operationError != Server.OP_ACTIVACION) && (operationError != Server.AUTENTICACION_ST) && (Constants.BLOCKED_ERROR_CODE.equals(errorCode) || Constants.BLOCKED_ERROR_CODE_2.equals(errorCode));
	}

	private void analiseResponseAux2(final Integer operationId,final  ServerResponse response,final  BaseViewController caller,final  boolean callerHandlesError,final  BaseViewController baseViewController,final  String errorCode,final  int operationError) {
		if (isError(operationError)){

                // Mensaje de error en Transferencias.
                if(Server.ALLOW_LOG) Log.d("BaseSubapplication", "ERROR Transferencias >>> operationError = " + operationError + " errorCode = " + errorCode);
                mostrarErrorTransfer(response, errorCode, baseViewController, caller,operationId.intValue());
                //baseViewController.processNetworkResponse(operationId.intValue(), response);


        } else {
                if (callerHandlesError) {
                    baseViewController.processNetworkResponse(operationId.intValue(), response);
                } else {
            //		caller.ocultaIndicadorActividad();
//									caller.showErrorMessage(errorCode + "\n" + response.getMessageText());
                    caller.showInformationAlertEspecial(caller.getString(R.string.label_error), errorCode, response.getMessageText(), null);
                }
        }
	}

	private boolean isError(final int operationError) {
		return (operationError == Server.SELF_TRANSFER_OPERATION);
	}

	/**
	 * Muestra un aviso de error en caso de que el servidor rechace la transferencia. Si es
	 * avanzado mostrará el mensaje devuelto y si es básico mostrará uno por defecto.
	 * 
	 * @param response Respuesta del servidor
	 * @param baseViewController Controlador base
	 * @param caller Controlador padre
	 */
	private void mostrarErrorTransfer(final ServerResponse response,final  String errorCode,final  BaseViewController baseViewController, final BaseViewController caller,final  int operationValue) {
		final Session session = Session.getInstance(SuiteAppLogin.appContext);
		final Constants.Perfil perfil = session.getClientProfile();
		// Si es perfil avanzado mostrará el mensaje recibido, sino mostrará el definido en String.xml
		
		final Account cEje = Tools.obtenerCuentaEje();
		final String cEjeType = cEje == null ? "" : cEje.getType();
		
		if ((Constants.CODE_CNE0234.equals(errorCode)) || (Constants.CODE_CNE0235.equals(errorCode)) || (Constants.CODE_CNE0236.equals(errorCode))) {
			
			if ((Constants.Perfil.basico.equals(perfil)) && (!cEjeType.equals(Constants.CREDIT_TYPE))) {
				final String msg = baseViewController.getString(R.string.transferir_aviso_texto);
				caller.ocultaIndicadorActividad();
				caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,msg, null);
				
			} else if (Constants.Perfil.recortado.equals(perfil)) {
				baseViewController.processNetworkResponse(operationValue, response);
				
			} else {
				// Avanzado, o básico con tarjeta de crédito como cuenta eje
				caller.ocultaIndicadorActividad();
				caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,response.getMessageText(), null);
			}
			
		} else {
			caller.ocultaIndicadorActividad();
			caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,response.getMessageText(), null);
		}
		

	}
	

	/**
	 * Determines if the requested operation is not being called already
	 * @param opId the operation id
	 * @return true if the operation isn't already waiting for a response
	 */
	private boolean isNotAlreadyCalling(final int opId){
		return !this.networkOperationsTable.containsKey(opId);
	}
	// #endregion
	
	// #region Otros métodos.
	// TODO revisar en hijos.
	public void cierraAplicacion() {
//		viewsController.cierraViewsController();
		viewsController = null;
		server = null;
		applicationLogged = false;
	}
	
	/**
	 * Shows the Main menu screen, removing any activity on top
	 */
	public void requestMenu(final Context activityContext){
		changingActivity = true;
		viewsController.showMenuInicial();
	}

	/**
	 * Get the operation error message
	 * @param throwable the throwable
	 * @return the operation error message 
	 */
	protected String getErrorMessage(final Throwable throwable) {
		final StringBuffer sb = new StringBuffer();
		if (throwable != null) {
			if (throwable instanceof NumberFormatException) {
				sb.append(SuiteAppLogin.appContext.getString(R.string.error_format));
			} else if (throwable instanceof ParsingException) {
				sb.append(SuiteAppLogin.appContext.getString(R.string.error_format));
			} else if (throwable instanceof IOException) {
				sb.append(SuiteAppLogin.appContext.getString(R.string.error_communications));
			} else {
				sb.append(SuiteAppLogin.appContext.getString(R.string.error_communications));
			}
		}
		return sb.toString();
	}
	// #endregion
	
	// TODO revisar en hijos.
	public BaseSubapplication(final SuiteAppLogin suiteApp) {
		this.suiteApp = suiteApp;
		server = new Server();
		networkOperationsTable = new Hashtable<Integer, NetworkOperation>();
		viewsController = null;
		applicationLogged = false;
	}
}
