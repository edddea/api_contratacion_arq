package com.bancomer.mbanking.login;

import android.app.Activity;
import android.content.Context;

import com.bancomer.base.callback.CallBackBConnect;

import java.util.List;

import suitebancomer.classes.gui.controllers.login.SuiteViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.LoginData;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

public class SuiteAppLogin extends com.bancomer.base.SuiteApp {
	public static boolean isSubAppRunning;
	public static boolean isActiveBconnectFlow;
	public static Context appContext;
	private static SuiteAppLogin me;
	private SuiteViewsController suiteViewsController;
	private static BaseViewControllerCommons intentToReturn;
	private CallBackBConnect callBackForMantenimiento;
	private Activity activityForMantenimiento;
	private CallBackBConnect callBackContratacion;
	private Activity activityForContratacion;
	private CallBackBConnect callBackForSofttoken;
	private Activity activityForSofttoken;

	private ServerResponse serverResponse;
	private ConsultaEstatus consultaEstatus;
	private LoginData data;

	private CallBackBConnect callBackLogin;



	private CallBackBConnect callBackReactivacion;
	private CallBackBConnect callBackActivacion;
	private Activity activityReactivacion;
	private Activity activityActivacion;
	private List<String> estados;


	public static boolean isActiveBconnectFlow() {
		return isActiveBconnectFlow;
	}

	public static void setIsActiveBconnectFlow(final boolean isActiveBconnectFlow) {
		SuiteAppLogin.isActiveBconnectFlow = isActiveBconnectFlow;
	}
	public CallBackBConnect getCallBackLogin() {
		return callBackLogin;
	}

	public void setCallBackLogin(final CallBackBConnect callBackLogin) {
		this.callBackLogin = callBackLogin;
	}

	public List<String> getEstados() {
		return estados;
	}

	public void setEstados(final List<String> estados) {
		this.estados = estados;
	}

	public CallBackBConnect getCallBackReactivacion() {
		return callBackReactivacion;
	}

	public void setCallBackReactivacion(final CallBackBConnect callBackReactivacion) {
		this.callBackReactivacion = callBackReactivacion;
	}

	public CallBackBConnect getCallBackActivacion() {
		return callBackActivacion;
	}

	public void setCallBackActivacion(final CallBackBConnect callBackActivacion) {
		this.callBackActivacion = callBackActivacion;
	}

	public Activity getActivityReactivacion() {
		return activityReactivacion;
	}

	public void setActivityReactivacion(final Activity activityReactivacion) {
		this.activityReactivacion = activityReactivacion;
	}

	public Activity getActivityActivacion() {
		return activityActivacion;
	}

	public void setActivityActivacion(final Activity activityActivacion) {
		this.activityActivacion = activityActivacion;
	}

	public CallBackBConnect getCallBackContratacion() {
		return callBackContratacion;
	}

	public void setCallBackContratacion(final CallBackBConnect callBackContratacion) {
		this.callBackContratacion = callBackContratacion;
	}

	public Activity getActivityForContratacion() {
		return activityForContratacion;
	}

	public void setActivityForContratacion(final Activity activityForContratacion) {
		this.activityForContratacion = activityForContratacion;
	}

	public CallBackBConnect getCallBackForSofttoken() {
		return callBackForSofttoken;
	}

	public void setCallBackForSofttoken(final CallBackBConnect callBackForSofttoken) {
		this.callBackForSofttoken = callBackForSofttoken;
	}

	public Activity getActivityForSofttoken() {
		return activityForSofttoken;
	}

	public void setActivityForSofttoken(final Activity activityForSofttoken) {
		this.activityForSofttoken = activityForSofttoken;
	}

	public CallBackBConnect getCallBackForMantenimiento() {
		return callBackForMantenimiento;
	}

	public void setCallBackForMantenimiento(final CallBackBConnect callBackForMantenimiento) {
		this.callBackForMantenimiento = callBackForMantenimiento;
	}

	public Activity getActivityForMantenimiento() {
		return activityForMantenimiento;
	}

	public void setActivityForMantenimiento(final Activity activityForMantenimiento) {
		this.activityForMantenimiento = activityForMantenimiento;
	}

	/**
	 * @return the intentToReturn
	 */
	public static BaseViewControllerCommons getIntentToReturn() {
		return intentToReturn;
	}

	/**
	 * @param intentToReturn the intentToReturn to set
	 */
	public static void setIntentToReturn(final BaseViewControllerCommons intentToReturn) {
		SuiteAppLogin.intentToReturn = intentToReturn;
	}

	public void onCreate(final Context context) {
		super.onCreate(context);
		appContext = context;
		suiteViewsController = new SuiteViewsController();
		isSubAppRunning = false;
		
		me = this;
		//appContext = getApplicationContext();
	};
	
	public void cierraAplicacionSuite() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public SuiteViewsController getSuiteViewsController() {
		return suiteViewsController;
	}

	public static SuiteAppLogin getInstance() {
		return me;
	}
	
	// #region BmovilApp
	private BmovilApp bmovilApplication;
	
	public BmovilApp getBmovilApplication() {
		if(bmovilApplication == null)
			bmovilApplication = new BmovilApp(this);
		return bmovilApplication;
	}
	
	public static boolean getBmovilStatus() {
		return PropertiesManager.getCurrent().getBmovilActivated();
	}
	
	public void startBmovilApp() {
		bmovilApplication = new BmovilApp(this);
		isSubAppRunning = true;
	}







	public static int getResourceId(final String nombre,final String tipo){
		return appContext.getResources().getIdentifier(nombre,tipo, appContext.getPackageName());
	}
	public void setFlagsEnvironmment(final Boolean simulation,final  Boolean allowLog,final  Boolean emulator,final  Boolean development){
		ServerCommons.SIMULATION=simulation;
		ServerCommons.ALLOW_LOG=allowLog;
		ServerCommons.EMULATOR=emulator;
		ServerCommons.DEVELOPMENT=development;

	}

	public ConsultaEstatus getConsultaEstatus() {
		return consultaEstatus;
	}

	public void setConsultaEstatus(ConsultaEstatus consultaEstatus) {
		this.consultaEstatus = consultaEstatus;
	}

	public LoginData getData() {
		return data;
	}

	public void setData(LoginData data) {
		this.data = data;
	}

	public ServerResponse getServerResponse() {
		return serverResponse;
	}

	public void setServerResponse(ServerResponse serverResponse) {
		this.serverResponse = serverResponse;
	}
}
