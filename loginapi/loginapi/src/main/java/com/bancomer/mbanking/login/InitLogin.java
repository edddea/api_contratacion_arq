package com.bancomer.mbanking.login;

import android.content.Context;


import com.bancomer.base.SuiteApp;

import javax.security.auth.login.LoginException;

import suitebancomer.classes.gui.delegates.login.MenuSuiteArqServiceDelegate;
import suitebancomer.classes.gui.delegates.login.MenuSuiteDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by alanmichaelgonzalez on 12/10/15.
 * clase para inicializar el api de login
 */
public class InitLogin {

    Context context;

    /*
    * Construtor de la clase y punton de entrada al api
    * reciviendo el contexto de la aplicacion
    * */
    SuiteAppLogin loginSuite;
    public InitLogin(final Context context) {
        loginSuite=new SuiteAppLogin();
        loginSuite.onCreate(context);
        SuiteApp.appContext=context;
        this.context = context;
    }

    /**
     * metodo para hacer el login a la aplicaion de banca movil
     * @param numeroCeluar
     * @param password
     * @param continuaFlujoBconnect la bandera indica si se queire hacer uso de todo b-connect y solo se desea saver la respuesta del servidor
     * @return
     * @throws LoginException
     */
    public Boolean login(final String numeroCeluar,final String password,final Boolean continuaFlujoBconnect) throws LoginException{
        loginSuite.setIsActiveBconnectFlow(continuaFlujoBconnect);
        final MenuSuiteDelegate menuSuitedelegate=new MenuSuiteDelegate();
        if(menuSuitedelegate.validarCampos(numeroCeluar,password)){
            if(ServerCommons.ARQSERVICE) {
                MenuSuiteArqServiceDelegate menuSuitedelegatearq=new MenuSuiteArqServiceDelegate();
                menuSuitedelegatearq.login(numeroCeluar, password);
            } else{
                menuSuitedelegate.login(numeroCeluar, password);
            }
        }else {
            return false;
        }
        return true;
    }

}
