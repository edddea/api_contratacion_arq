/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package bancomer.api.common.gui.controllers;


import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface.OnClickListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ScrollView;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;


/**
 * BaseScreen is the base class for all the screens in the MBanking application
 * It provides basic, common functionality to be included in the business logic
 * so that the application can manage screens in a uniform, consistent way.
 *
 * @author Stefanini IT Solutions
 */
public interface BaseViewController{
    
    /**
     * Parametro para mostrar la opcion de menu SMS
     */
    public final static Integer SHOW_MENU_SMS = 2;
    
    /**
     * Parametro para mostrar la opcion de menu Correo electrónico
     */
    public final static Integer SHOW_MENU_EMAIL = 4;
    
    /**
     * Parametro para mostrar la opcion de menu PDF
     */
    public final static Integer SHOW_MENU_PDF = 8;
    
    /**
     * Parametro para mostrar la opcion de menu Frecuente
     */
    public final static Integer SHOW_MENU_FRECUENTE = 16;
    
    /**
     * Parametro para mostrar la opcion de menu Rapida
     */
    public final static Integer SHOW_MENU_RAPIDA = 32;
    
    /**
     * Parametro para mostrar la opcion de menu Rapida
     */
    public final static Integer SHOW_MENU_BORRAR = 64;
    
	/**
     * To use as parameter in aplicaEstilo method in order to show the header
     */
    public final static int SHOW_HEADER = 2;
    
    /**
     * To use as parameter in aplicaEstilo method in order to show the title
     */
    public final static int SHOW_TITLE = 4;
    
	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState state
	 */
	public void onCreate(Activity activity, int parameters, int layoutID);
	
	/**
	 * Screen is about to be shown.
	 */
	
	
    /**
     * Places the title string and respective icon on screen.
     * @param titleText the screen title
     * @param iconResource the resource of the screen icon
     */
    public void setTitle(Activity activity, final int titleText, final int iconResource);
    
    /**
     * Places the title string and respective icon on screen with the desired color
     * @param titleText the screen title
     * @param iconResource the resource of the screen icon
     */
    public void setTitle(Activity activity, final int titleText, final int iconResource, final int colorResource);
  
 
    /**
     * 
     * This function has to be overridden by child members, it implements
     * the processing for server response.
     * 
     * @param operationId
     * @param response
     */
    public void processNetworkResponse(int operationId, ServerResponse response);
	
	
	/**
	 * Override this from children to return the view's fields.
	 * @return the current screen fields.
	 */
	public EditText[] getFields();
	
	/**
	 * Gets the trimmed text of a text field content.
	 * @param textField the EditText component to retrieve text
	 * @return the textField value without trailing spaces
	 */
	public String getText(EditText textField);
	
	public float getHeaderHeight();
	
	public float getBodyHeight();
	
	/**
	 * Sets the layout that will fill the empty body layout in base screen
	 * template.
	 *
	 * @param layoutResource the layout resources
	 */
	public void setBodyLayout(Activity activity, int layoutResource);


    /**
     * Hides the soft keyboard if any editbox called it.
     */
    public void hideSoftKeyboard(Activity activity);
    
    /**
     * This is called whenever the back button is pressed.
     * Normally, this function will return to the previous screen.
     */
    public void goBack(Activity activity, BaseViewsController baseViewsController);
    
    
    /**
     * Shows an error message if the provided resource exists. The alert
     * view title will be "Error", and it will have one button to accept.
     *
     * @param errorMessage the error message resource to show
     */
    public void showErrorMessage(Activity activity, int errorMessage);
    
    public void showErrorMessage(Activity activity, String errorMessage);
    
    /**
     * Places a progress dialog, for long waiting processes.
     * 
     * @param strTitle the dialog title
     * @param strMessage the message to show in the while
     */
    public void muestraIndicadorActividad(Activity activity, String strTitle, String strMessage);
    
    /**
     * Hides the progress dialog.
     */
    public void ocultaIndicadorActividad();
	
	/**
     * Shows an error message if the message length is greater than 0. The alert
     * view title will be "Error", and it will have one button to accept.
     *
     * @param errorMessage the error message text to show
     *
     */
    public void showErrorMessage(Activity activity, String errorMessage, OnClickListener listener);
	
    /**
     * Determines if given points are inside view
     * @param x - x coordinate of point
     * @param y - y coordinate of point
     * @param view - view object to compare
     * @return true if the points are within view bounds, false otherwise
     */
    public boolean isPointInsideView(float x, float y, View view);

	public BaseViewsController getParentViewsController();

	public void setParentViewsController(BaseViewsController parentViewsController);

	public BaseDelegate getDelegate();

	public void setDelegate(BaseDelegate delegate);
	
	public void hideCurrentDialog();
	
	public void setCurrentDialog(AlertDialog dialog);
	
	/**
	 * Establece la l�gica a seguir para el redimensionamiento de la pantalla.
	 */
	public void scaleForCurrentScreen(Activity activity);
	
	public ScrollView getScrollView();

	public void moverScroll();

	// #region Information Alert.
    /**
     * Muestra una alerta con el mensaje dado.
     * @param message el mensaje a mostrar
     */
    public void showInformationAlert(Activity activity, int message);
    
    /**
     * Shows an information alert with the given message text. No action is performed after this alert is closed.
     * @param message the message text to show
     */
    public void showInformationAlert(Activity activity, String message);
    
    /**
     * Shows an information alert with the given message.
     * @param message the message resource to show
     */
    public void showInformationAlert(Activity activity,int message, OnClickListener listener);
    
	/**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(Activity activity, String message, OnClickListener listener);
    
    /**
	 * Shows an alert dialog.
	 * @param title The alert title resource id.
	 * @param message The alert message resource id.
	 * @param listener The listener for the "Ok" button. 
	 */
    public void showInformationAlert(Activity activity, int title, int message, OnClickListener listener);
    
    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param title The alert title resource id.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(Activity activity, String title, String message, OnClickListener listener);
    
    
    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param title The alert title resource id.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlertEspecial(Activity activity, String title, String errorCode,String descripcionError, OnClickListener listener);
    
    /**
	 * Shows an alert dialog.
	 * @param title The alert title resource id.
	 * @param message The alert message resource id.
	 * @param okText the ok text
	 * @param listener The listener for the "Ok" button. 
	 */
    public void showInformationAlert(Activity activity, int title, int message, int okText, OnClickListener listener);
    
    /**
	 * Shows an alert dialog.
	 * @param title The alert title.
	 * @param message The alert message.
	 * @param okText the ok text
	 * @param listener The listener for the "Ok" button. 
	 */
	public void showInformationAlert(Activity activity, String title, String message, String okText, OnClickListener listener);
	
	
	 /**
		 * Shows an alert dialog.
		 * @param title The alert title.
		 * @param message The alert message.
		 * @param okText the ok text
		 * @param listener The listener for the "Ok" button. 
		 */
		public void showInformationAlertEspecial(Activity activity, String title, String errorCode, String descripcionError, String okText, OnClickListener listener);
	
	public void showYesNoAlert(Activity activity, String message, OnClickListener positiveListener);
	
	public void showYesNoAlertEspecialTitulo(Activity activity, String errorCode,String descripcionError, OnClickListener positiveListener);
	
	public void showYesNoAlert(Activity activity, int message, OnClickListener positiveListener);
	
	public void showYesNoAlert(Activity activity, int message, OnClickListener positiveListener, OnClickListener negativeListener);
	
	public void showYesNoAlert(Activity activity, String title, String message, OnClickListener positiveListener);
	
	public void showYesNoAlert(Activity activity, String message, OnClickListener positiveListener, OnClickListener negativeListener);
	
	public void showYesNoAlertEspecialTitulo(Activity activity, String errorCode,String descripcionError, OnClickListener positiveListener, OnClickListener negativeListener);
	
	
	public void showYesNoAlert(Activity activity, int title, int message, OnClickListener positiveListener);
	
	
	public void showYesNoAlert(Activity activity, String title, String message, String okText, OnClickListener positiveListener);
	
	public void showYesNoAlert(Activity activity, int title, int message, int okText, OnClickListener positiveListener);
	
	public void showYesNoAlert(Activity activity, String title, String message, String okText, String calcelText, OnClickListener positiveListener);
	
	public void showYesNoAlert(Activity activity, int title, int message, int okText, int calcelText, OnClickListener positiveListener);
	
	public void showYesNoAlert(Activity activity, int title, 
			   				   int message, 
			   				   int okText, 
			   				   int calcelText, 
			   				   OnClickListener positiveListener, 
			   				   OnClickListener negativeListener);
	public void showYesNoAlert(Activity activity, String title, 
							   String message, 
							   String okText, 
							   String calcelText, 
							   OnClickListener positiveListener, 
							   OnClickListener negativeListener);
	
	
	
	
	public void showYesNoAlertEspecial(Activity activity, String title, String errorCode,String descripcionError, String okText,
			String calcelText, OnClickListener positiveListener,
			OnClickListener negativeListener);

	public void setHabilitado(boolean habilitado);
	
	public boolean isHabilitado();
	
	public Activity getActivity();

	public void setActivity(Activity activity);
	
	public boolean onCreateOptionsMenu(Menu menu);
	
	public boolean onPrepareOptionsMenu(Menu menu);
	
	public boolean onOptionsItemSelected(MenuItem item);
	
	public ArrayList<Integer> getOpcionesMenu();
	
	public void setOpcionesMenu(ArrayList<Integer> opcionesMenu);
	
	public void onUserInteraction();
}