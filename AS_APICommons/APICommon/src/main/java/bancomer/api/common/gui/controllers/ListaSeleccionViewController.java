package bancomer.api.common.gui.controllers;

import java.util.ArrayList;


import bancomer.api.common.R;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.delegates.BaseDelegate;
import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;


public class ListaSeleccionViewController extends LinearLayout {

	private BaseViewsController parentManager;
	private ListView listview;
	private ArrayList<Object> encabezado;
	private ArrayList<Object> lista;
	private int numeroColumnas;
	private int numeroFilas;
	private boolean seleccionable;
	private float altura;
	private boolean alturaFija;
	private boolean existeFiltro;
	private String title;

	private TableLayout cabecera;
	private LlenarTablas adapter;
	private int opcionSeleccionada;
	private LinearLayout.LayoutParams params;
	private float anchoColumna;
	private LinearLayout vista;
	private LinearLayout vistaFiltro;
	private LayoutInflater inflater;
	private BaseDelegate delegate;
	private TextView titulo;
	private boolean singleLine;
	
	private int sizeCelda;

	private FiltroListaViewController filtroLista;
	
	private boolean scaled;
	private boolean encabezadoCargado;
	private String textoAMostrar;
	private boolean fijarLista=false;

	
	public ListaSeleccionViewController(Context context,LinearLayout.LayoutParams layoutParams, BaseViewsController parentManager) {
		super(context);
		marqueeEnabled = true;
		inflater = LayoutInflater.from(context);
		LinearLayout viewLayout = (LinearLayout) inflater.inflate(R.layout.api_common_layout_lista_seleccion_view, this, true);
		viewLayout.setLayoutParams(layoutParams);
		this.parentManager = parentManager;

		vista = (LinearLayout) findViewById(R.id.layout_ListaSeleccion);
		vistaFiltro = (LinearLayout) findViewById(R.id.lista_seleccion_filtro);
		cabecera = (TableLayout) findViewById(R.id.lista_seleccion_cabecera_tabla);

		listview = (ListView) findViewById(R.id.lista_Seleccion_View);
		
		scaled = false;
		encabezadoCargado = false;
	}

	public void setSingleLine(boolean singleLine) {
		this.singleLine = singleLine;
	}

	public String getTextoAMostrar() {
		return textoAMostrar;
	}

	public void setTextoAMostrar(String textoAMostrar) {
		this.textoAMostrar = textoAMostrar;
	}

	public void cargarEncabezado(){
		GuiTools guiTools = GuiTools.getCurrent();
		
		if (getNumeroColumnas() == 1) {
			TableRow.LayoutParams parametros;
			
			parametros = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[0] : anchoColumna);
			
			TextView celda1 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_1);
			celda1.setText((String) encabezado.get(1));
			celda1.setLayoutParams(parametros);
			celda1.setSingleLine(true);
            celda1.setSelected(true);
			findViewById(R.id.lista_seleccion_cabecera_celda_2)
					.setVisibility(View.GONE);
			findViewById(R.id.lista_seleccion_cabecera_celda_3)
					.setVisibility(View.GONE);
			findViewById(R.id.lista_seleccion_cabecera_celda_4)
					.setVisibility(View.GONE);
			
			if(!scaled) {
				celda1.setMaxLines(1);
//					celda1.setSingleLine(true);
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				guiTools.tryScaleText(celda1);
			}
		} else if (getNumeroColumnas() == 2) {
			TableRow.LayoutParams parametros;
			parametros = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[0] : anchoColumna);
			
			TextView celda1 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_1);
			celda1.setText((String) encabezado.get(1));
			celda1.setLayoutParams(parametros);
			celda1.setSingleLine(true);
            celda1.setSelected(true);

            if(overrideColumnWidth)
            	parametros = new TableRow.LayoutParams(0, LayoutParams.WRAP_CONTENT, overridedColumnWidths[1]);
            
			TextView celda2 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_2);
			celda2.setText((String) encabezado.get(2));
			celda2.setLayoutParams(parametros);
			celda2.setGravity(Gravity.RIGHT);
			celda2.setSingleLine(true);
            celda2.setSelected(true);

			findViewById(R.id.lista_seleccion_cabecera_celda_3)
					.setVisibility(View.GONE);
			findViewById(R.id.lista_seleccion_cabecera_celda_4)
					.setVisibility(View.GONE);
			
			if(!scaled) {
				celda1.setMaxLines(1);
				celda2.setMaxLines(1);
				
//					celda1.setSingleLine(true);
//					celda2.setSingleLine(true);
				
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				
				guiTools.tryScaleText(celda1);
				guiTools.tryScaleText(celda2);
			}
		}else if (getNumeroColumnas() == 3) {
			TableRow.LayoutParams parametros;
			
			parametros = new TableRow.LayoutParams(0,LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[0] : 0.3f);
			TextView celda1 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_1);
			celda1.setText((String) encabezado.get(1));
			celda1.setLayoutParams(parametros);
			celda1.setSingleLine(true);
            celda1.setSelected(true);

			parametros = new TableRow.LayoutParams(0,LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[1] : 0.4f);
			TextView celda2 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_2);
			celda2.setText((String) encabezado.get(2));
			celda2.setLayoutParams(parametros);
			celda2.setGravity(Gravity.CENTER);
			celda2.setSingleLine(true);
            celda2.setSelected(true);

			parametros = new TableRow.LayoutParams(0,LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[2] : 0.3f);
			TextView celda3 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_3);
			celda3.setText((String) encabezado.get(3));
			celda3.setLayoutParams(parametros);
			celda3.setGravity(Gravity.RIGHT);
			celda3.setSingleLine(true);
            celda3.setSelected(true);
			
			findViewById(R.id.lista_seleccion_cabecera_celda_4).setVisibility(View.GONE);

			if(!scaled) {
				celda1.setMaxLines(1);
				celda2.setMaxLines(1);
				celda3.setMaxLines(1);
				
				celda1.setSingleLine(true);
//					celda2.setSingleLine(true);
//					celda3.setSingleLine(true);
				
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				
				guiTools.tryScaleText(celda1);
				guiTools.tryScaleText(celda2);
				guiTools.tryScaleText(celda3);
			}
			
		}else if (getNumeroColumnas() == 4) {
			TableRow.LayoutParams parametros;
			
			parametros = new TableRow.LayoutParams(0,	LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[0] : anchoColumna);
			TextView celda1 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_1);
			celda1.setText((String) encabezado.get(1));
			celda1.setLayoutParams(parametros);
			celda1.setSingleLine(true);
            celda1.setSelected(false);

            parametros = new TableRow.LayoutParams(0,	LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[1] : anchoColumna);
			TextView celda2 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_2);
			celda2.setText((String) encabezado.get(2));
			celda2.setLayoutParams(parametros);
			celda2.setGravity(Gravity.CENTER);
			celda2.setSingleLine(true);
            celda2.setSelected(true);

            parametros = new TableRow.LayoutParams(0,	LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[2] : anchoColumna);
			TextView celda3 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_3);
			celda3.setText((String) encabezado.get(3));
			celda3.setLayoutParams(parametros);
			celda3.setGravity(Gravity.CENTER);
			celda3.setSingleLine(true);
            celda3.setSelected(true);
			
			parametros = new TableRow.LayoutParams(0,	LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[3] : anchoColumna);
			
            
            parametros = new TableRow.LayoutParams(0,	LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[3] : anchoColumna);
			TextView celda4 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_4);
			celda4.setText((String) encabezado.get(4));
			celda4.setLayoutParams(parametros);
			celda4.setGravity(Gravity.RIGHT);
			celda4.setSingleLine(true);
            celda4.setSelected(true);
			
			if(!scaled) {
				celda1.setMaxLines(1);
				celda2.setMaxLines(1);
				celda3.setMaxLines(1);
				celda4.setMaxLines(1);
				
//					celda1.setSingleLine(true);
//					celda2.setSingleLine(true);
//					celda3.setSingleLine(true);
//					celda4.setSingleLine(true);
				
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda4.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				
				guiTools.tryScaleText(celda1);
				guiTools.tryScaleText(celda2);
				guiTools.tryScaleText(celda3);
				guiTools.tryScaleText(celda4);
			}
		} else if (getNumeroColumnas() == 5) {
			if (!seleccionable) {
				TableRow.LayoutParams parametros;

				parametros = new TableRow.LayoutParams(0,
						LayoutParams.WRAP_CONTENT,
						overrideColumnWidth ? overridedColumnWidths[0]
								: anchoColumna);
				TextView celda1 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_1);
				celda1.setText((String) encabezado.get(1));
				celda1.setLayoutParams(parametros);
				celda1.setSingleLine(true);
				celda1.setSelected(false);

				parametros = new TableRow.LayoutParams(0,
						LayoutParams.WRAP_CONTENT,
						overrideColumnWidth ? overridedColumnWidths[1]
								: anchoColumna);
				TextView celda2 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_2);
				celda2.setText((String) encabezado.get(2));
				celda2.setLayoutParams(parametros);
				celda2.setGravity(Gravity.CENTER);
				celda2.setSingleLine(true);
				celda2.setSelected(true);

				parametros = new TableRow.LayoutParams(0,
						LayoutParams.WRAP_CONTENT,
						overrideColumnWidth ? overridedColumnWidths[2]
								: anchoColumna);
				TextView celda3 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_3);
				celda3.setText((String) encabezado.get(3));
				celda3.setLayoutParams(parametros);
				celda3.setGravity(Gravity.CENTER);
				celda3.setSingleLine(true);
				celda3.setSelected(true);

				parametros = new TableRow.LayoutParams(0,
						LayoutParams.WRAP_CONTENT,
						overrideColumnWidth ? overridedColumnWidths[3]
								: anchoColumna);
				TextView celda4 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_4);
				celda4.setText((String) encabezado.get(4));
				celda4.setLayoutParams(parametros);
				celda4.setGravity(Gravity.RIGHT);
				celda4.setSingleLine(true);
				celda4.setSelected(true);

				parametros = new TableRow.LayoutParams(0,
						LayoutParams.WRAP_CONTENT,
						overrideColumnWidth ? overridedColumnWidths[3]
								: anchoColumna);
				TextView celda5 = (TextView) findViewById(R.id.lista_seleccion_cabecera_celda_5);
				celda5.setText((String) encabezado.get(5));
				celda5.setLayoutParams(parametros);
				celda5.setGravity(Gravity.RIGHT);
				celda5.setSingleLine(true);
				celda5.setSelected(true);
				
				if (!scaled) {
					celda1.setMaxLines(1);
					celda2.setMaxLines(1);
					celda3.setMaxLines(1);
					celda4.setMaxLines(1);
					celda5.setMaxLines(1);
					
					celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda4.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda5.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					
					guiTools.tryScaleText(celda1);
					guiTools.tryScaleText(celda2);
					guiTools.tryScaleText(celda3);
					guiTools.tryScaleText(celda4);
					guiTools.tryScaleText(celda5);
				}
			}

		}
		
	}
	
	public void cargarTabla() {

		GuiTools guiTools = GuiTools.getCurrent();
		if(!scaled) {
			guiTools.scalePaddings(findViewById(R.id.lista_seleccion_cabecera_tabla));
		}
		
		if (getTitle() != null) {
			titulo = (TextView) findViewById(R.id.lista_seleccion_titulo);
			titulo.setText(getTitle());
			titulo.setVisibility(View.VISIBLE);
			
			if(!scaled) {
				guiTools.scale(titulo, true);
			}
		}

		if (isSeleccionable()) {
			anchoColumna = 0.85f / getNumeroColumnas();
			TableRow.LayoutParams parametros = new TableRow.LayoutParams(0,	LayoutParams.FILL_PARENT, 0.15f);
			findViewById(R.id.lista_seleccion_cabecera_celda_5).setVisibility(View.VISIBLE);
			findViewById(R.id.lista_seleccion_cabecera_celda_5).setLayoutParams(parametros);
		} else {
			if (getNumeroColumnas() == 5) {
				anchoColumna = 1.0f / getNumeroColumnas();
				TableRow.LayoutParams parametros = new TableRow.LayoutParams(LayoutParams.WRAP_CONTENT,
						LayoutParams.FILL_PARENT);
				findViewById(R.id.lista_seleccion_cabecera_celda_5)
						.setVisibility(View.VISIBLE);
				findViewById(R.id.lista_seleccion_cabecera_celda_5)
						.setLayoutParams(parametros);
			}else {
			anchoColumna = 1.0f / getNumeroColumnas();
			findViewById(R.id.lista_seleccion_cabecera_celda_5).setVisibility(View.GONE);
		}
		}
		
		if (encabezado != null) {
			if (!encabezadoCargado) {
				listview.setBackgroundResource(R.drawable.api_common_list_border_bottom_cell);
				cargarEncabezado();
				encabezadoCargado = true;
			}
		} else {
			cabecera.setVisibility(View.GONE);
		}
//		findViewById(R.id.lista_seleccion_encabezados_divider).setVisibility((null == encabezado) ? View.GONE : View.VISIBLE);

		if (!Tools.isEmptyOrNull(textoAMostrar)&& lista.size()==0) {
			setNumeroColumnas(1);
			numeroFilas = 1;
			anchoColumna = 1.0f;
			listview.setEnabled(false);
			listview.setSelected(false);
			ArrayList<Object> registro = new ArrayList<Object>();
			registro.add("");
			registro.add(textoAMostrar);
			lista = new ArrayList<Object>();
			lista.add(registro);
		}else {
			listview.setEnabled(true);
			listview.setSelected(true);
		}
		
		adapter = new LlenarTablas();
		listview.setAdapter(adapter);

		if (isAlturaFija()) {
			ListAdapter listAdapter = listview.getAdapter();
			int filas = 0;
			if (listAdapter.getCount() <= numeroFilas) {
				filas =listAdapter.getCount();
			}else {
				filas = numeroFilas;
			}
			int totalHeight = 0;
			for (int i = 0; i < filas; i++) {
				View listItem = listAdapter.getView(i, null, listview);
				listItem.measure(0, 0);
				totalHeight += listItem.getMeasuredHeight();
			}

			ViewGroup.LayoutParams params = listview.getLayoutParams();
			params.height = totalHeight
					+ (listview.getDividerHeight() * (listview.getCount() - 1));
			listview.setLayoutParams(params);
			listview.requestLayout();
		}
		
		if (isExisteFiltro()) {
			LinearLayout.LayoutParams params = new
			LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
			if (getFiltroLista() == null) {
				setFiltroLista(new FiltroListaViewController(getContext(), params));
				getFiltroLista().setListaSeleccion(this);
				getFiltroLista().init();
				vistaFiltro.addView(getFiltroLista());
				vistaFiltro.setVisibility(View.VISIBLE);
			}
		}

		scaled = true;
	}

	@SuppressWarnings("unchecked")
	public void ejecutarOpcionDelegate() {
		// ParentManager
		if (delegate != null /*&& !parentManager.isActivityChanging()*/) {
			delegate.performAction(((ArrayList<Object>) lista.get(getOpcionSeleccionada())).get(0));
		}
	}

	public ArrayList<Object> getEncabezado() {
		return encabezado;
	}

	public void setEncabezado(ArrayList<Object> encabezado) {
		this.encabezado = encabezado;
	}

	public ArrayList<Object> getLista() {
		return lista;
	}

	public void setLista(ArrayList<Object> lista) {
		this.lista = lista;
	}

	public BaseDelegate getDelegate() {
		return delegate;
	}

	public void setDelegate(BaseDelegate delegate) {
		this.delegate = delegate;
	}

	private class LlenarTablas extends BaseAdapter {

		@Override
		public int getCount() {
			return lista.size();
		}

		@Override
		public Object getItem(int position) {
			return lista.get(position);
		}

		@SuppressWarnings({ "unchecked" })
		@Override
		public View getView(final int position, View convertView, ViewGroup parent) {
			GuiTools guiTools = GuiTools.getCurrent();
			
			if (convertView == null) {
				convertView = inflater.inflate(R.layout.api_common_list_item_productos,
						null);
			}

			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					if (textoAMostrar != null && !textoAMostrar.equals("")){
						return;
					}
					if (isSeleccionable()) {
						setOpcionSeleccionada(position);
						notifyDataSetChanged();
						if(informActionEvenIfSelectable)
							ejecutarOpcionDelegate();
						if(informActionEvenIfSelectable)
							ejecutarOpcionDelegate();
					} else if (!isSeleccionable()) {
						setOpcionSeleccionada(position);
						notifyDataSetChanged();
						ejecutarOpcionDelegate();
					}
				}
			});
			
			TextView celda1 = (TextView) convertView.findViewById(R.id.lista_celda_1);
			TextView celda2 = (TextView) convertView.findViewById(R.id.lista_celda_2);
			TextView celda3 = (TextView) convertView.findViewById(R.id.lista_celda_3);
			TextView celda4 = (TextView) convertView.findViewById(R.id.lista_celda_4);
			celda1.setSingleLine(singleLine);
			celda2.setSingleLine(singleLine);
			celda3.setSingleLine(singleLine);
			celda4.setSingleLine(singleLine);
			ImageButton celda5 = (ImageButton) convertView.findViewById(R.id.lista_celda_check);
			//if (opcionSeleccionada == position) {
			if(!fijarLista){
				if (opcionSeleccionada == position) {
				celda1.setTextColor(getResources().getColor(R.color.tercer_azul));
				celda2.setTextColor(getResources().getColor(R.color.tercer_azul));
				celda3.setTextColor(getResources().getColor(R.color.tercer_azul));
				celda4.setTextColor(getResources().getColor(R.color.tercer_azul));
				
				celda5.setImageResource(R.drawable.api_common_flecha_lista_seleccion);
				
				
			} else {
				celda1.setTextColor(getResources().getColor(R.color.gris_texto_1));
				celda2.setTextColor(getResources().getColor(R.color.gris_texto_1));
				celda3.setTextColor(getResources().getColor(R.color.gris_texto_1));
				celda4.setTextColor(getResources().getColor(R.color.gris_texto_1));
				celda5.setImageResource(0);
			}
		}
			if (!isSeleccionable()) {
				if(getNumeroColumnas()==5){
					
					celda5.setVisibility(View.VISIBLE);
				}
				else
				celda5.setVisibility(View.GONE);
			}else {
				if(!scaled) {
//					GuiTools.getCurrent().scale(celda5);
					LinearLayout.LayoutParams params2 = (LinearLayout.LayoutParams)celda5.getLayoutParams();
					int next = (int)(22.22 * GuiTools.getScaleFactor());
					params2.height = next;
					params2.topMargin = (int)(1.0 * GuiTools.getScaleFactor());
				
				//celda5.setLayoutParams(params);
				celda5.setScaleType(ScaleType.CENTER_INSIDE);
				}
			}
			if (getNumeroColumnas() > 4) {
				//setNumeroColumnas(4);
				if(!isSeleccionable())
					setNumeroColumnas(getNumeroColumnas());
				else
					setNumeroColumnas(4);
			}
			if (getNumeroColumnas() == 1) {
				params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, overrideColumnWidth ? overridedColumnWidths[0] : 1.0f);
				ArrayList<Object> registro = (ArrayList<Object>) lista
						.get(position);

				celda1.setText((String) registro.get(1));
				celda1.setLayoutParams(params);
				celda1.setGravity((textoAMostrar != null && !textoAMostrar.equals("")) ?Gravity.CENTER_HORIZONTAL : Gravity.LEFT);
//				celda1.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda1.setSelected(marqueeEnabled);

				celda2.setVisibility(View.GONE);
				celda3.setVisibility(View.GONE);
				celda4.setVisibility(View.GONE);
				
				celda1.setMaxLines(1);
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				guiTools.tryScaleText(celda1);


			} else if (getNumeroColumnas() == 2) {
				params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[0] : anchoColumna);
				ArrayList<Object> registro = (ArrayList<Object>) lista
						.get(position);

				celda1.setText((String) registro.get(1));
				celda1.setLayoutParams(params);
				celda1.setGravity(Gravity.LEFT);
//				celda1.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda1.setSelected(marqueeEnabled);

	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[1] : anchoColumna);
				celda2.setText((String) registro.get(2));
				celda2.setLayoutParams(params);
				celda2.setGravity(Gravity.RIGHT);
//				celda2.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda2.setSelected(marqueeEnabled);

				celda3.setVisibility(View.GONE);
				celda4.setVisibility(View.GONE);
				
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				
				guiTools.tryScaleText(celda1);
				guiTools.tryScaleText(celda2);

				
			} else if (getNumeroColumnas() == 3) {
				ArrayList<Object> registro = (ArrayList<Object>) lista.get(position);

				params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[0] : anchoColumna);
//				params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[0] : 0.3f);
				celda1.setText((String) registro.get(1));
				celda1.setLayoutParams(params);
				celda1.setGravity(Gravity.LEFT);
//				celda1.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda1.setSelected(marqueeEnabled);

	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[1] : anchoColumna);
//	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[1] : 0.2f);
				celda2.setText((String) registro.get(2));
				celda2.setLayoutParams(params);
				celda2.setGravity(Gravity.CENTER);
//				celda2.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda2.setSelected(marqueeEnabled);

	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[2] : anchoColumna);
//	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[2] : 0.7f - anchoColumna);
				celda3.setText((String) registro.get(3));
				celda3.setLayoutParams(params);
				celda3.setGravity(Gravity.RIGHT);
//				celda3.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda3.setSelected(marqueeEnabled);

				celda4.setVisibility(View.GONE);
				
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				
				guiTools.tryScaleText(celda1);
				guiTools.tryScaleText(celda2);
				guiTools.tryScaleText(celda3);
				
				
			} else if (getNumeroColumnas() == 4) {
				ArrayList<Object> registro = (ArrayList<Object>) lista
						.get(position);

				params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[0] : anchoColumna);
				celda1.setText((String) registro.get(1));
				celda1.setLayoutParams(params);
				celda1.setGravity(Gravity.LEFT);
//				celda1.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda1.setSelected(marqueeEnabled);

	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[1] : anchoColumna);
				celda2.setText((String) registro.get(2));
				celda2.setLayoutParams(params);
				celda2.setGravity(Gravity.CENTER);
//				celda2.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda2.setSelected(marqueeEnabled);

	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[2] : anchoColumna);
				celda3.setText((String) registro.get(3));
				celda3.setLayoutParams(params);
				celda3.setGravity(Gravity.CENTER);
//				celda3.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda3.setSelected(marqueeEnabled);
	            
	            params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[4] : anchoColumna);

	           // params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,overrideColumnWidth ? overridedColumnWidths[4] : anchoColumna);
				celda4.setText((String) registro.get(4));
				celda4.setLayoutParams(params);
				celda4.setGravity(Gravity.RIGHT);
//				celda4.setTextAppearance(getContext(), R.style.movimientos_celda_style);
	            celda4.setSelected(marqueeEnabled);
				
				celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
				celda4.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);

				
				
				guiTools.tryScaleText(celda1);
				guiTools.tryScaleText(celda2);
				guiTools.tryScaleText(celda3);
				guiTools.tryScaleText(celda4);
				
			}else if (getNumeroColumnas() == 5) {
				if(!seleccionable){
					ArrayList<Object> registro = (ArrayList<Object>) lista
							.get(position);
	
					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
							overrideColumnWidth ? overridedColumnWidths[0]
									: anchoColumna);
//					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
//							3);
					celda1.setText((String) registro.get(1));
					celda1.setLayoutParams(params);
					celda1.setGravity(Gravity.LEFT);
					// celda1.setTextAppearance(getContext(),
					// R.style.movimientos_celda_style);
					celda1.setSelected(marqueeEnabled);
	
					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
							overrideColumnWidth ? overridedColumnWidths[1]
									: anchoColumna);
//					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
//							2);
					celda2.setText((String) registro.get(2));
					celda2.setLayoutParams(params);
					celda2.setGravity(Gravity.CENTER);
					// celda2.setTextAppearance(getContext(),
					// R.style.movimientos_celda_style);
					celda2.setSelected(marqueeEnabled);
	
					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
							overrideColumnWidth ? overridedColumnWidths[2]
									: anchoColumna);
//					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
//							2);
					celda3.setText((String) registro.get(3));
					celda3.setLayoutParams(params);
					celda3.setGravity(Gravity.CENTER);
					// celda3.setTextAppearance(getContext(),
					// R.style.movimientos_celda_style);
					celda3.setSelected(marqueeEnabled);
	
					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
							overrideColumnWidth ? overridedColumnWidths[4]
									: anchoColumna);
//					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT,
//							3);
					celda4.setText((String) registro.get(4));
					celda4.setLayoutParams(params);
					celda4.setGravity(Gravity.RIGHT);
					// celda4.setTextAppearance(getContext(),
					// R.style.movimientos_celda_style);
					celda4.setSelected(marqueeEnabled);
					
					
					params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, anchoColumna);
//					params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
					celda5.setLayoutParams(params);
					String estado=(String)registro.get(5);
					if(estado!=null){
						if(estado.equalsIgnoreCase("201")||estado.equalsIgnoreCase("205")){
							celda5.setImageResource(R.drawable.api_common_an_punto_naranja);
						}else if(estado.equalsIgnoreCase("206")){
							celda5.setImageResource(R.drawable.api_common_an_flecha_azul);
						}else if(estado.equalsIgnoreCase("207")){
							celda5.setImageResource(R.drawable.api_common_an_flecha_verde);
						}else if(estado.equalsIgnoreCase("301")){
							celda5.setImageResource(R.drawable.api_common_an_cruz_magenta);
						}
					}
					
					
					celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda3.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
					celda4.setTextSize(TypedValue.COMPLEX_UNIT_PX, 14.0f);
	
					guiTools.tryScaleText(celda1);
					guiTools.tryScaleText(celda2);
					guiTools.tryScaleText(celda3);
					guiTools.tryScaleText(celda4);
					guiTools.scale(celda5);
				}				
				
			}
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
	}

	public int getOpcionSeleccionada() {
		return opcionSeleccionada;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(MotionEvent.ACTION_MOVE == event.getAction()){		
		//parentManager.getCurrentViewControllerApp().getParentViewsController().onUserInteraction();
		}
		return super.onTouchEvent(event);
	}

	public void setMarqueeEnabled(boolean enabled) {
		findViewById(R.id.lista_seleccion_cabecera_celda_1).setSelected(enabled);
		findViewById(R.id.lista_seleccion_cabecera_celda_2).setSelected(enabled);
		findViewById(R.id.lista_seleccion_cabecera_celda_3).setSelected(enabled);
		findViewById(R.id.lista_seleccion_cabecera_celda_4).setSelected(enabled);
		
		marqueeEnabled = enabled;
		
	}
	
	private boolean marqueeEnabled;

	/**
	 * @return the numeroFilas
	 */
	public int getNumeroFilas() {
		return numeroFilas;
	}

	/**
	 * @param numeroFilas the numeroFilas to set
	 */
	public void setNumeroFilas(int numeroFilas) {
		this.numeroFilas = numeroFilas;
	}
	
	// #region Override Column Widths
	/**
	 * Bandera que indica si se deben de ignorar los valores predeterminados de anchos de columna.
	 */
	private boolean overrideColumnWidth;
	
	/**
	 * @return Bandera que indica si se deben de ignorar los valores predeterminados de anchos de columna.
	 */
	public boolean isOverrideColumnWidth() {
		return overrideColumnWidth;
	}

	/**
	 * @param overrideColumnWidth Bandera que indica si se deben de ignorar los valores predeterminados de anchos de columna.
	 */
	public void setOverrideColumnWidth(boolean overrideColumnWidth) {
		this.overrideColumnWidth = overrideColumnWidth;
	}

	/**
	 * Arreglo de flotantes para definir un ancho de columnas personalizado.
	 */
	private float[] overridedColumnWidths;

	/**
	 * @return Arreglo de flotantes para definir un ancho de columnas personalizado.
	 */
	public float[] getOverridedColumnWidths() {
		return overridedColumnWidths;
	}

	/**
	 * @param overridedColumnWidths Arreglo de flotantes para definir un ancho de columnas personalizado.
	 */
	public void setOverridedColumnWidths(float[] overridedColumnWidths) {
		if(null == overridedColumnWidths || 0 == overridedColumnWidths.length || 4 < overridedColumnWidths.length) {
			overrideColumnWidth = false;
		} else {
			this.overridedColumnWidths = overridedColumnWidths;
			this.setNumeroColumnas(overridedColumnWidths.length);
			overrideColumnWidth = true;
		}
	}
	// #endregion

	public int getNumeroColumnas() {
		return numeroColumnas;
	}

	public void setNumeroColumnas(int numeroColumnas) {
		this.numeroColumnas = numeroColumnas;
	}

	public void setOpcionSeleccionada(int opcionSeleccionada) {
		this.opcionSeleccionada = opcionSeleccionada;
	}

	public boolean isSeleccionable() {
		return seleccionable;
	}

	public void setSeleccionable(boolean seleccionable) {
		this.seleccionable = seleccionable;
	}

	public boolean isAlturaFija() {
		return alturaFija;
	}

	public void setAlturaFija(boolean alturaFija) {
		this.alturaFija = alturaFija;
	}

	public boolean isExisteFiltro() {
		return existeFiltro;
	}

	public void setExisteFiltro(boolean existeFiltro) {
		this.existeFiltro = existeFiltro;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public FiltroListaViewController getFiltroLista() {
		return filtroLista;
	}

	public void setFiltroLista(FiltroListaViewController filtroLista) {
		this.filtroLista = filtroLista;
	}
	
	//SPEI
	private boolean informActionEvenIfSelectable = false;

	/**
	 * @return the informActionEvenIfSelectable
	 */
	public boolean isInformActionEvenIfSelectable() {
		return informActionEvenIfSelectable;
	}

	/**
	 * @param informActionEvenIfSelectable the informActionEvenIfSelectable to set
	 */
	public void setInformActionEvenIfSelectable(boolean informActionEvenIfSelectable) {
		this.informActionEvenIfSelectable = informActionEvenIfSelectable;
	}	
	
	public boolean isFijarLista() {
		return fijarLista;
	}

	public void setFijarLista(boolean fijarLista) {
		this.fijarLista = fijarLista;
	}

}
