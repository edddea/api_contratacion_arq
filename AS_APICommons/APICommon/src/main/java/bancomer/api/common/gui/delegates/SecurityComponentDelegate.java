package bancomer.api.common.gui.delegates;

import android.content.Context;
import android.util.Log;

import bancomer.api.common.R;
import bancomer.api.common.commons.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;


/**
 * Created by OOROZCO on 8/26/15.
 */
public class SecurityComponentDelegate {

    private Context context;


    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getEtiquetaCampoContrasenia() {
        return context.getString(R.string.confirmation_contrasena);
    }


    public String getEtiquetaCampoOCRA() {
        return context.getString(R.string.confirmation_ocra);
    }


    public String getEtiquetaCampoDP270() {
        return context.getString(R.string.confirmation_dp270);
    }


    public String getEtiquetaCampoSoftokenActivado() {
        return context.getString(R.string.confirmation_softtokenActivado);
    }


    public String getEtiquetaCampoSoftokenDesactivado() {
        return context.getString(R.string.confirmation_softtokenDesactivado);
    }


    public String getEtiquetaCampoCVV() {
        return context.getString(R.string.confirmation_CVV);
    }

/*
    public String getTextoAyudaInstrumentoSeguridad(Constants.TipoInstrumento tipoInstrumento, boolean sofTokenStatus, Constants.TipoOtpAutenticacion autStatus) {

        switch (tipoInstrumento) {
            case SoftToken:
                if (sofTokenStatus) {
                    return context.getString(R.string.confirmation_ayudaRegistroSofttokenActivado);
                } else {
                    return context.getString(R.string.confirmation_ayudaCodigoSofttokenDesactivado);
                }
            case OCRA:
                    return context.getString(R.string.confirmation_ayudaRegistroOCRA);
            case DP270:
                    return context.getString(R.string.confirmation_ayudaCodigoDP270);
            case sinInstrumento:
            default:
                return "";
        }
    }
*/

    public String getTextoAyudaInstrumentoSeguridad(Constants.TipoInstrumento tipoInstrumento, boolean sofTokenStatus, Constants.TipoOtpAutenticacion autStatus) {
        //quitar esto de simulaicon TODO
        if (autStatus == Constants.TipoOtpAutenticacion.ninguno) {
            return "";
        } else if (autStatus == Constants.TipoOtpAutenticacion.registro) {
            switch (tipoInstrumento) {
                case SoftToken:
                    if (sofTokenStatus) {
                        return context.getString(R.string.confirmation_ayudaRegistroSofttokenActivado);
                    } else {
                        return context.getString(R.string.confirmation_ayudaRegistroSofttokenDesactivado);
                    }
                case OCRA:
                    return context.getString(R.string.confirmation_ayudaRegistroOCRA);
                case DP270:
                    return context.getString(R.string.confirmation_ayudaRegistroDP270);
                case sinInstrumento:
                default:
                    return "";
            }
        } else if (autStatus == Constants.TipoOtpAutenticacion.codigo) {
            switch (tipoInstrumento) {
                case SoftToken:
                    if (sofTokenStatus) {
                        return context.getString(R.string.confirmation_ayudaCodigoSofttokenActivado);
                    } else {
                        return context.getString(R.string.confirmation_ayudaCodigoSofttokenDesactivado);
                    }
                case OCRA:
                    return context.getString(R.string.confirmation_ayudaCodigoOCRA);
                case DP270:
                    return context.getString(R.string.confirmation_ayudaCodigoDP270);
                case sinInstrumento:
                default:
                    return "";
            }
        }
        return "";
    }

    public String getEtiquetaCampoNip() {
        return context.getString(R.string.confirmation_nip);
    }

    public String getTextoAyudaNIP() {
        return context.getString(R.string.confirmation_ayudaNip);
    }

    public String getTextoAyudaCVV() {
        return context.getString(R.string.confirmation_CVV_ayuda);
    }

    public String getEtiquetaCampoTarjeta(){
        return context.getString(R.string.confirmation_componente_digitos_tarjeta);
        //return "Ingrese los últimos 5 dígitos de su tarjeta";
    }

    public String getTextoAyudaTarjeta() {
        return context.getString(R.string.confirmation_ayuda_componente_digitos_tarjeta);
        //return "Son los últimos dígitos de tu tarjeta";
    }

}
