package bancomer.api.common.io;

import android.os.AsyncTask;
import android.util.Log;

import bancomer.api.common.session.CommonSession;


public class ModuleCallback {
	
	private Hilo hilo;
	
	public ModuleCallback(){
		hilo = new Hilo();
	}
	
	public void initModuleCallback(){
		hilo.execute();
	}
	
	class Hilo extends AsyncTask<Object, Void, Boolean>{

		public Hilo() {
		}

		@Override
		protected void onPreExecute() {
			if(CommonSession.getInstance().getCommonSessionService().getAllowLog()) Log.d(">> CGI-Nice-Ppl", "Hilo - preExc -> Inicio");
			
		}

		@Override
		protected Boolean doInBackground(Object... objects) {
			if(CommonSession.getInstance().getCommonSessionService().getAllowLog()) Log.d(">> CGI-Nice-Ppl", "Hilo - doInBck -> Inicio");
			
			moduleCallbackWait();
			
		    return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			if(CommonSession.getInstance().getCommonSessionService().getAllowLog()) Log.d(">> CGI-Nice-Ppl", "Hilo - postExc -> Inicio");
			moduleCallbackDoAtEnd();
		}
	}
	
	public void moduleCallbackWait(){
		
	}
	
	public void moduleCallbackDoAtEnd(){
		
	}

}
