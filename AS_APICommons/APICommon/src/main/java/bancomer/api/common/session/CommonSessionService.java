package bancomer.api.common.session;

import android.util.Log;

/**
 * Created by niko on 29/07/2015.
 */
public class CommonSessionService {

    private Boolean allowLog = true;

    public Boolean getAllowLog() {
        return allowLog;
    }

    public void setAllowLog(Boolean allowLog) {
        this.allowLog = allowLog;
    }
}
