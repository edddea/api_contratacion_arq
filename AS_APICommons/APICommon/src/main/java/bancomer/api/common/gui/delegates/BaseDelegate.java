package bancomer.api.common.gui.delegates;


import java.util.ArrayList;
import java.util.Hashtable;
import android.app.Activity;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.io.ServerResponse;


public interface BaseDelegate {

	/**
     * Parametro para mostrar la opcion de menu SMS
     */
    public final static int SHOW_MENU_SMS = 2;
    
    /**
     * Parametro para mostrar la opcion de menu Correo electrónico
     */
    public final static int SHOW_MENU_EMAIL = 4;
    
    /**
     * Parametro para mostrar la opcion de menu PDF
     */
    public final static int SHOW_MENU_PDF = 8;
    
    /**
     * Parametro para mostrar la opcion de menu Frecuente
     */
    public final static int SHOW_MENU_FRECUENTE = 16;
    
    /**
     * Parametro para mostrar la opcion de menu Rapida
     */
    public final static int SHOW_MENU_RAPIDA = 32;
    
    /**
     * Parametro para mostrar la opcion de menu Rapida
     */
    public final static int SHOW_MENU_BORRAR = 64;
	
	/**
	 * Invoke a network operation, controlling that it is called after all previous
	 * repaint events have been completed
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 */
	public void doNetworkOperation(final int operationId, final Hashtable<String,?> params, final BaseViewController caller);
	
	/**
     * 
     * This function has to be overridden by child members, it implements
     * the processing for server response.
     * 
     * @param operationId
     * @param response
     */
    public void analyzeResponse(int operationId, ServerResponse response);
	
    public void performAction(Object obj);
    
    public long getDelegateIdentifier();
    
}
