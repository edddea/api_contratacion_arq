package bancomer.api.common.gui.controllers;

import android.app.Activity;
import bancomer.api.common.gui.delegates.BaseDelegate;


public interface BaseViewsController {
	

	public Activity getRootViewController();
	
	public Activity getCurrentViewControllerApp();
	
	
	public Activity getCurrentViewController();
	
	public void setCurrentActivityApp(Activity currentViewController);
	
	public void setCurrentActivityApp(Activity currentViewController, boolean asRootViewController);
	
	public boolean isActivityChanging();
	
	public void setActivityChanging(boolean flag);
	
	public void showViewController(Class<?> viewController);
	
	public void showViewController(Class<?> viewController, int flags);
	
	public void showViewController(Class<?> viewController, int flags, boolean inverted);
	
	/**
	 * Muestra la actividad especificada con los parametros indicados
	 * 
	 * @param viewController La actividad a mostrar
	 * @param flags Banderas que se colocaran en el intent de la actividad
	 * @param inverted Si la pantalla debe mostrarse con animaci�n hacia adelante o hacia atras
	 * @param extras Arreglo con los extras a pasar al intento, deben acomodarse de manera llave - valor y la llave debe ser
	 * de tipo String, si esta regla no se cumple no pasara nig�n par�metro al intento
	 */
	public void showViewController(Class<?> viewController, int flags, boolean inverted, String[] extrasKeys, Object[] extras);
	
	public void showViewController(Class<?> viewController, int flags, boolean inverted, String[] extrasKeys, Object[] extras, Activity activity);

	
	/**
	 * Muestra la actividad especificada esperando un resultado por parte de la actividad que la invoca
	 * 
	 * @param viewController La actividad a mostrar
	 * @param activityCode codigo que se le asiganra a esta actividad
	 */
	public void showViewControllerForResult(Class<?> viewController, int activityCode);
	
	public void backToRootViewController();

	public void cierraViewsController();
	
	public void addDelegateToHashMap(long id, BaseDelegate baseDelegate);
	
	public BaseDelegate getBaseDelegateForKey(long key);
	
	public Long getLastDelegateKey();
	
	public void removeDelegateFromHashMap(long key);
	
	public void clearDelegateHashMap();
	
	public void overrideScreenForwardTransition();

	public void overrideScreenBackTransition();
	
    public void onUserInteraction();
    
    public boolean consumeAccionesDePausa();
    
    public boolean consumeAccionesDeReinicio();
    
    public boolean consumeAccionesDeAlto();
    
    /**
     * Muestra el menu inicial de la aplicación que implemente el método.
     */
    public void showMenuInicial();

	/**
	 * Asigna a null el campo rootVoewController
	 */
	void resetRootViewController();

}
