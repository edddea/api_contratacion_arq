package bancomer.api.common.commons;


import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by OOROZCO on 8/27/15.
 */
public interface ICommonSession {

    String getUsername();

    String getIum();

    void setPromocion(Promociones[] promociones);

    String getSecurityInstrument();

    Constants.Perfil getClientProfile();

    //31/08/2015
    String getEmail();

    String getNombreCliente();

}
