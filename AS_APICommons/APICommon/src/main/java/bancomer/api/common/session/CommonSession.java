package bancomer.api.common.session;

/**
 * Created by niko on 29/07/2015.
 */
public class CommonSession {

    private static CommonSession commonSessionInstance;
    private CommonSessionService commonSessionService;

    /**
     * Getters
     */
    public CommonSessionService getCommonSessionService() {
        return commonSessionService;
    }

    /**
     * Constructores
     */
    public CommonSession() {
        commonSessionService = new CommonSessionService();
    }

    /**
     * Constructores Singleton
     */
    public static CommonSession getInstance(){
        if(commonSessionInstance == null){
            commonSessionInstance = new CommonSession();
        }

        return commonSessionInstance;
    }
}
