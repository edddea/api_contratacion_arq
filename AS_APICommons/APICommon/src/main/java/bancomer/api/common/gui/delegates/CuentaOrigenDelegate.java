package bancomer.api.common.gui.delegates;

import android.util.Log;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.gui.controllers.CuentaOrigenController;
import bancomer.api.common.model.HostAccounts;

/**
 * Created by OOROZCO on 9/8/15.
 */
public class CuentaOrigenDelegate {

    public final static long CUENTA_ORIGEN_DELEGATE_ID = 0x4fd454c56ca109f1L;
    public HostAccounts cuentaSeleccionada;
    public int indiceCuenta;
    CuentaOrigenController cuentaOrigenViewController;
    ArrayList<HostAccounts> listaCuentaOrigen;
    Constants.Operacion tipoOperacion;

    public CuentaOrigenDelegate(HostAccounts cuentaSeleccionada) {
        this.cuentaSeleccionada = cuentaSeleccionada;
    }

    public CuentaOrigenController getCuentaOrigenViewController() {
        return cuentaOrigenViewController;
    }

    public void setCuentaOrigenViewController(
            CuentaOrigenController cuentaOrigenViewController) {
        this.cuentaOrigenViewController = cuentaOrigenViewController;
    }

    public Constants.Operacion getTipoOperacion() {
        return tipoOperacion;
    }

    public void setTipoOperacion(Constants.Operacion tipoOperacion) {
        this.tipoOperacion = tipoOperacion;
    }

    public HostAccounts getCuentaSeleccionada() {
        return cuentaSeleccionada;
    }

    public void setCuentaSeleccionada(HostAccounts cuenta) {
        cuentaSeleccionada = cuenta;
        indiceCuenta = listaCuentaOrigen.indexOf(cuenta);
        Log.v(this.getClass().getSimpleName(), "CuentaOrigen envÌa: " + cuenta.getNumber());

    }

    public void setCuentaSiguiente() {
        if (listaCuentaOrigen.size() > 1) {
            if (indiceCuenta == listaCuentaOrigen.size() - 1) {
                indiceCuenta = 0;
            } else {
                indiceCuenta += 1;
            }
        }
        setCuentaSeleccionada(listaCuentaOrigen.get(indiceCuenta));
    }

    public void setCuentaPrevia() {
        if (listaCuentaOrigen.size() > 1) {
            if (indiceCuenta == 0) {
                indiceCuenta = listaCuentaOrigen.size() - 1;
            } else {
                indiceCuenta -= 1;
            }
        }
        setCuentaSeleccionada(listaCuentaOrigen.get(indiceCuenta));
    }

    public void setListaCuentasOrigen(ArrayList<HostAccounts> cuentasNuevas) {
        listaCuentaOrigen = cuentasNuevas;
        indiceCuenta = 0;
    }

    public ArrayList<HostAccounts> getListaCuentaOrigen() {
        return listaCuentaOrigen;
    }

    public void performAction(Object obj) {
        Log.d("CuentaOrigenDelegate", "Presionaste una cuenta de la lista" + ((HostAccounts) obj).getNumber());
        setCuentaSeleccionada((HostAccounts) obj);
        cuentaOrigenViewController.setIndiceCuentaSeleccionada(indiceCuenta);
        cuentaOrigenViewController.dejarDeMostrarLista();
    }

    public void actualizarCuentaSeleccionada() {
        this.setCuentaSeleccionada(getListaCuentaOrigen().get(indiceCuenta));
    }


}
