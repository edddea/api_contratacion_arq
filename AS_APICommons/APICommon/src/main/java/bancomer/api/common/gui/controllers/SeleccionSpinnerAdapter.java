package bancomer.api.common.gui.controllers;

import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;

import bancomer.api.common.R;

/**
 * Created by francisco on 25/03/16.
 */
public class SeleccionSpinnerAdapter extends ArrayAdapter<Integer> {

    private ArrayList<Integer> images;


    public SeleccionSpinnerAdapter(Context context, ArrayList<Integer> images) {
        super(context, R.layout.spinner_companias_telefonicas, images);
        this.images = images;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getImageForPosition(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getImageForPosition(position);
    }

    private View getImageForPosition(int position) {
        ImageView imageView = new ImageView(getContext());
        imageView.setBackgroundResource(images.get(position));
        imageView.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT,0));
        return imageView;
    }

}
