package bancomer.api.common.io;

/**
 * Result wraps the result data of a response (status, error code, error message).
 */
public interface Result {
	

    /**
     * Get the status code.
     * @return status code
     */
    public String getStatus();

    /**
     * Get the error code.
     * @return the error code
     */
    public String getCode();

    /**
     * Get the error message.
     * @return the error message
     */
    public String getMessage();

    /**
     * Get the update URL.
     * @return the update URL for mandatory updating
     */
    public String getUpdateURL();
}
