package bancomer.api.common.gui.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import bancomer.api.common.R;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.model.Compania;
import bancomer.api.common.model.Convenio;
import android.content.Context;
import android.graphics.Rect;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;


public class SeleccionHorizontalViewController  extends LinearLayout implements View.OnClickListener{
	public final static String SELECT_SUFFIX = "_select";
	public final static String IMAGE_FORMAT = ".png";
	
	public final static String OTHER_ELEMENT_IMAGE_NAME = "otro.png";
	
	private static HashMap<String, Integer> imagenes;
	private static HashMap<String, Integer> imagenesSelec;

	private Spinner contenidoComponenteHorizontal;
	private Object itemSeleccionado;
	private LayoutInflater inflater;
	private ArrayList<Object> componentes;
	private ArrayList<ImageView> arregloBotones;
	private BaseDelegate delegate;
	private ArrayList<Integer> img;
	private Boolean bandera = false;

	private boolean blocked;
	//public void setBandera(Boolean bandera){
	//	this.bandera=bandera;
	//}
	public SeleccionHorizontalViewController(Context context,LinearLayout.LayoutParams layoutParams,
											ArrayList<Object> listaComponentes, BaseDelegate delegate,
											boolean addOtherElement) {
		super(context);
		inflater = LayoutInflater.from(context);
		LinearLayout viewLayout = (LinearLayout) inflater.inflate(R.layout.api_common_layout_seleccion_horizontal_view, this, true);
		viewLayout.setLayoutParams(layoutParams);

		contenidoComponenteHorizontal = (Spinner) findViewById(R.id.contenido_seleccion_horizontal);
		bandera=true;
		contenidoComponenteHorizontal.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				// your code here
				//if(bandera) {
					onClickSpinner(position);
				//	bandera=false;
				//}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}
		});

        GuiTools.getCurrent().scalePaddings(contenidoComponenteHorizontal);
		
		this.componentes = listaComponentes;
		if (addOtherElement) {
			/*Convenio otroConvenio = new Convenio(getString(R.string.servicesPayment_otherServices), OTHER_ELEMENT_IMAGE_NAME, "-1", Integer.toString(componentes.size()+1));
			componentes.add(otroConvenio);*/
		}
		this.delegate = delegate;
		img = new ArrayList<Integer>();
		reloadItems();
	}
	
	static {
		imagenes = new HashMap<String, Integer>();
		imagenesSelec = new HashMap<String, Integer>();
		
		imagenes.put("telcel.png", R.drawable.telcelb);
		imagenesSelec.put("telcel.png", R.drawable.telcela);
		imagenes.put("nextel.png", R.drawable.nextelb);
		imagenesSelec.put("nextel.png", R.drawable.nextela);
		imagenes.put("unefon.png", R.drawable.unefonb);
		imagenesSelec.put("unefon.png", R.drawable.unefona);
		imagenes.put("iusacell.png", R.drawable.iusacellb);
		imagenesSelec.put("iusacell.png", R.drawable.iusacella);
		imagenes.put("movistar.png", R.drawable.movistarb);
		imagenesSelec.put("movistar.png", R.drawable.movistara);
		imagenes.put("telmex.png", R.drawable.telmex);
		imagenesSelec.put("telmex.png", R.drawable.telmex_select);
		imagenes.put("cfe.png", R.drawable.cfe);
		imagenesSelec.put("cfe.png", R.drawable.cfe_select);
		imagenes.put("liverpool.png", R.drawable.liverpool);
		imagenesSelec.put("liverpool.png", R.drawable.liverpool_select);
		imagenes.put("sky.png", R.drawable.sky);
		imagenesSelec.put("sky.png", R.drawable.sky_select);
		imagenes.put("telcelcie.png", R.drawable.telcelcie);
		imagenesSelec.put("telcelcie.png", R.drawable.telcelcie_select);
		imagenes.put("avon.png", R.drawable.avon);
		imagenesSelec.put("avon.png", R.drawable.avon_select);
		imagenes.put("jafra.png", R.drawable.jafra);
		imagenesSelec.put("jafra.png", R.drawable.jafra_select);		
		imagenes.put("otro.png", R.drawable.otro);
		imagenesSelec.put("otro.png", R.drawable.otro_select);
	}

	public void setSelectedItem(Object item) {
		this.itemSeleccionado = item;
		changeSelection();
	}
	
	public void setOtroAsSelected() {
		this.itemSeleccionado = componentes.get(componentes.size()-1);
		changeSelection();
	}
	
	public Object getSelectedItem()
	{
		return itemSeleccionado;
	}
	
	public void changeSelection()
	{
		//Log.e("AQUI ENTRA"," SIEMPRE");
		contenidoComponenteHorizontal.setAdapter(null);
		img.clear();
		reloadItems();
	}
	
	public void reloadItems()
	{
		GuiTools guitools = GuiTools.getCurrent();
        int selectedLateralSize = guitools.getEquivalenceInPixels(68);
        int basePading = guitools.getEquivalenceInPixels(8);
		
        LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.FILL_PARENT);
        params.setMargins(getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_top_margin_no_selected),
        		getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_side_margin_no_selected), 
        		getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_top_margin_no_selected),
        		getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_side_margin_no_selected));
        
        LinearLayout.LayoutParams params2 = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.FILL_PARENT);
        params2.setMargins(getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_top_margin_selected),
        		getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_side_margin_selected), 
        		getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_top_margin_selected),
        		getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_side_margin_selected));
        
        LinearLayout.LayoutParams selectedButtonParams = new LayoutParams(selectedLateralSize, selectedLateralSize);
        selectedButtonParams.setMargins(basePading, 0, basePading, 0);
        
        arregloBotones = new ArrayList<ImageView>();
        ImageView imagen = null;
		int posicion = 0;
        ImageView selectedButton = null;
        boolean imageFound = false;
        Object obj = null;
        Integer currentImage = null;
        int componentsSize = componentes.size();
        
//        for (int i = 0; i < componentsSize; i++) {
//        	imagen = new ImageButton(getContext());
//    		if ((itemSeleccionado == componentes.get(i)) ||
//    			(!imageFound && i==componentsSize-1)) {
//    			if (itemSeleccionado instanceof Convenio) {
//    				currentImage = (Integer)imagenesSelec.get(((Convenio)itemSeleccionado).getNombreImagen());
//					if (currentImage != null) {
//						imagen.setBackgroundResource(currentImage.intValue());
//					} else {
//    					imagen.setBackgroundResource(imagenesSelec.get(OTHER_ELEMENT_IMAGE_NAME));
//    				}
//    			} else if (itemSeleccionado instanceof Compania) {
//    				currentImage = imagenesSelec.get(((Compania)itemSeleccionado).getNombreImagen());
//    				if (currentImage != null) {
//						imagen.setBackgroundResource(currentImage.intValue());
//					} else {
//    					imagen.setBackgroundResource(imagenesSelec.get(OTHER_ELEMENT_IMAGE_NAME));
//    				}
//    			}
//	    		imagen.setLayoutParams(params2);
//	    		selectedButton = imagen;
//	    		imageFound = true;
//			} else {
//				obj = componentes.get(i);
//				if (obj instanceof Convenio) {
//					currentImage = imagenes.get(((Convenio)obj).getNombreImagen());
//					if (currentImage != null) {
//						imagen.setBackgroundResource(currentImage.intValue());
//					} else {
//    					imagen.setBackgroundResource(imagenes.get(OTHER_ELEMENT_IMAGE_NAME));
//    				}
//    			} else if (obj instanceof Compania) {
//    				currentImage = imagenes.get(((Compania)obj).getNombreImagen());
//    				if (currentImage != null) {
//    					imagen.setBackgroundResource(currentImage.intValue());
//    				} else {
//    					imagen.setBackgroundResource(imagenes.get(OTHER_ELEMENT_IMAGE_NAME));
//    				}
//    				
//    			}
//	    		imagen.setLayoutParams(params2);
//			}
//    		
//    		imagen.setId(i);
//    		imagen.setOnClickListener(this);
//    		arregloBotones.add(imagen);
//    		contenidoComponenteHorizontal.addView(imagen);
//		}
        
        for (int i = 0; i < componentsSize; i++) {
        	imagen = new ImageView(getContext());
    		if ((itemSeleccionado == componentes.get(i)) ||	(!imageFound && i==componentsSize-1)) {
    			if (itemSeleccionado instanceof Convenio) {
    				currentImage = (Integer)imagenesSelec.get(((Convenio)itemSeleccionado).getNombreImagen());
					if (currentImage != null) {
						imagen.setBackgroundResource(currentImage.intValue());
					} else {
    					imagen.setBackgroundResource(imagenesSelec.get(OTHER_ELEMENT_IMAGE_NAME));
    				}
    			} else if (itemSeleccionado instanceof Compania) {
    				currentImage = imagenesSelec.get(((Compania)itemSeleccionado).getNombreImagen());
    				if (currentImage != null) {
						img.add(currentImage.intValue());
						posicion = i;
						//imagen.setBackgroundResource(currentImage.intValue());
					} else {
						img.add(imagenesSelec.get(OTHER_ELEMENT_IMAGE_NAME));
    					//imagen.setBackgroundResource(imagenesSelec.get(OTHER_ELEMENT_IMAGE_NAME));
    				}
    			}
	    		//imagen.setLayoutParams(selectedButtonParams);
	    		selectedButton = imagen;
	    		imageFound = true;
			} else {
				obj = componentes.get(i);
				if (obj instanceof Convenio) {
					currentImage = imagenes.get(((Convenio)obj).getNombreImagen());
					if (currentImage != null) {
						imagen.setBackgroundResource(currentImage.intValue());
					} else {
    					imagen.setBackgroundResource(imagenes.get(OTHER_ELEMENT_IMAGE_NAME));
    				}
    			} else if (obj instanceof Compania) {
    				currentImage = imagenes.get(((Compania)obj).getNombreImagen());
    				if (currentImage != null) {
						img.add(currentImage.intValue());
    					//imagen.setBackgroundResource(currentImage.intValue());
    				} else {
						img.add(imagenesSelec.get(OTHER_ELEMENT_IMAGE_NAME));
    					//imagen.setBackgroundResource(imagenes.get(OTHER_ELEMENT_IMAGE_NAME));
    				}

    			}
	    		//imagen.setLayoutParams(selectedButtonParams);
			}
    		
    		imagen.setId(i);
    		imagen.setOnClickListener(this);
    		arregloBotones.add(imagen);


		}

		if(img!=null)
		{
			bandera = true;
			SeleccionSpinnerAdapter img2 = new SeleccionSpinnerAdapter(getContext(),img);
			contenidoComponenteHorizontal.setAdapter(img2);
			contenidoComponenteHorizontal.setSelection(posicion);
		}
	}
	
	/**
	 * @return the componentes
	 */
	public ArrayList<Object> getComponentes() {
		return componentes;
	}

	/**
	 * @param componentes the componentes to set
	 */
	public void setComponentes(ArrayList<Object> componentes) {
		this.componentes = componentes;
	}

	public void bloquearComponente() {
		blocked = true;
		int arregloBotonesSize = arregloBotones.size();
		ImageView currentImageButton = null;
		for (int i=0; i<arregloBotonesSize; i++) {
			currentImageButton = arregloBotones.get(i);
			currentImageButton.setFocusable(false);
			currentImageButton.setClickable(false);
		}
	}
	
	@Override
	public void onClick(View v) {
		ImageView selectedButton = null;
		for (int i = 0; i < arregloBotones.size(); i++) {
			selectedButton = arregloBotones.get(i);
			if (v == selectedButton) {
				Object selectedComponent = componentes.get(selectedButton.getId());
				setSelectedItem(selectedComponent);
				if (delegate != null) {
					delegate.performAction(selectedComponent);
				}
			}
			
		}
	}

	public void onClickSpinner(int v) {
		//Log.e("POS ", v+"");
		for (int i = 0; i < arregloBotones.size(); i++) {
			if (v == i) {
				Object selectedComponent = componentes.get(i);
				setSelectedItem(selectedComponent);
				if (delegate != null) {
					delegate.performAction(selectedComponent);
				}
			}

		}
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		super.onWindowFocusChanged(hasWindowFocus);
		if (hasWindowFocus) {
			if (itemSeleccionado == null) {
				setSelectedItem(componentes.get(0));
			}
			ImageView selectedButton = arregloBotones.get(componentes.indexOf(itemSeleccionado));
			Rect outRect = new Rect();
			float rightOffset = getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_side_margin_no_selected);
			contenidoComponenteHorizontal.getLocalVisibleRect(outRect);
			int scroller = selectedButton.getRight() - outRect.right + (int)rightOffset;		
		}
	}
}
