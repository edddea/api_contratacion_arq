package bancomer.api.common.generaOTPS;



public interface GeneraOTPS {


	public abstract boolean borraToken();

	public abstract void inicializaCore();

	public abstract String generaOTPTiempo();

	public abstract String generaOTPChallenger(String challenge);

	public abstract void validaDatos(String cuenta);

	public abstract void borrarDatosConAlerta();
	
	public String generarOtpTiempoApi();
	
	public String generarOtpChallengeApi(String challenge);


}