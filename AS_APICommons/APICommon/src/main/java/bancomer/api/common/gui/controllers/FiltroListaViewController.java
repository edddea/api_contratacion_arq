package bancomer.api.common.gui.controllers;

import java.util.ArrayList;


import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import bancomer.api.common.R;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.delegates.FiltroListaDelegate;


public class FiltroListaViewController extends LinearLayout {

	public static ArrayList<Object> listaOriginal;
	private EditText txtFiltro;
	private FiltroListaDelegate delegate;
	ListaSeleccionViewController listaSeleccion;

	public FiltroListaViewController(Context context, LinearLayout.LayoutParams layoutParams) {
		super(context);
	    LayoutInflater inflater = LayoutInflater.from(context);
	    LinearLayout viewLayout = (LinearLayout) inflater.inflate(R.layout.api_common_layout_filtro_lista_view, this, true);
	    viewLayout.setLayoutParams(layoutParams);
	    
	    delegate = new FiltroListaDelegate();
	    delegate.setFiltroListaviewcontroller(this);
		txtFiltro = (EditText) findViewById(R.id.txtFiltro);

		txtFiltro.addTextChangedListener(new TextWatcher(){
	        public void afterTextChanged(Editable s) {
	        	delegate.realizaBusqueda(s.toString());
	        	delegate.actualizaCampos();
	        }
	        public void beforeTextChanged(CharSequence s, int start, int count, int after){}
	        public void onTextChanged(CharSequence s, int start, int before, int count){}
	    });
		
		// Resize to screen.
		scaleForCurrentScreen();
	}
	
	public void init()
	{
		delegate.guardaCopiaOriginal(listaSeleccion.getLista());
		if (delegate.getListaOriginal() == null) {
			txtFiltro.setEnabled(false);
		}
	}

	public ListaSeleccionViewController getListaSeleccion() {
		return listaSeleccion;
	}

	public void setListaSeleccion(ListaSeleccionViewController listaSeleccion) {
		this.listaSeleccion = listaSeleccion;
	}
	
	public void setListaOriginal(ArrayList<Object> listaOriginal){
		delegate.guardaCopiaOriginal(listaOriginal);
	}

	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		
		guiTools.scale(findViewById(R.id.layoutRoot));
		guiTools.scale(txtFiltro, true);
		guiTools.scale(findViewById(R.id.btnBorrar));
		
//		GuiTools guiTools = GuiTools.getCurrent();
//		ImageButton imgBtn = null;
//		LinearLayout linearLayout = null;
//		LinearLayout.LayoutParams linearLayoutParams = null;
//		
//		if(!GuiTools.isInitialized())
//			return;
//
//		linearLayout = (LinearLayout)findViewById(R.id.layoutRoot); 
//		linearLayout.getLayoutParams().width = guiTools.getEquivalenceInPixels(280);
//		linearLayout.getLayoutParams().height = guiTools.getEquivalenceInPixels(36);
//		
//		linearLayoutParams = (LinearLayout.LayoutParams)txtFiltro.getLayoutParams();
//		txtFiltro.setLayoutParams(guiTools.scaleLayoutParams(linearLayoutParams));
//		guiTools.scalePaddings(txtFiltro);
//		guiTools.tryScaleText(txtFiltro);
//		
//		imgBtn = (ImageButton)findViewById(R.id.btnBorrar);
//		
//		linearLayoutParams = (LinearLayout.LayoutParams)imgBtn.getLayoutParams();
//		imgBtn.setLayoutParams(guiTools.scaleLayoutParams(linearLayoutParams));
//		guiTools.scalePaddings(imgBtn);
	}

}
