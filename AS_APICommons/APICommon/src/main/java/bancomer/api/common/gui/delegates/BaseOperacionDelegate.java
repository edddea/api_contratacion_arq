package bancomer.api.common.gui.delegates;

import java.util.ArrayList;

import android.app.Activity;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;

public interface BaseOperacionDelegate {

    public boolean mostrarContrasenia();
	
	public TipoOtpAutenticacion tokenAMostrar();
	
	public boolean mostrarNIP();
	
	public boolean mostrarCVV();
	
	public boolean mostrarCampoTarjeta();
	
	public String getTextoTituloResultado();
	
	public String getTextoAyudaNIP();
	
	public String getTextoAyudaCVV();
	
	public int getTituloConfirmacion();
	
	public int getIconoConfirmacion();
	
	public ArrayList<Object> getDatosTablaConfirmacion();
	
	public void realizaOperacionConfirmacion(Activity confirmacionViewController, String contrasena, String nip, String asm, String tarjeta);
}
