package bancomer.api.common.gui.controllers;

import android.app.Activity;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedHashMap;

import bancomer.api.common.R;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.session.CommonSession;
import bancomer.api.common.timer.TimerController;


public class BaseViewsControllerImpl implements BaseViewsController {

	
	private static Method methodOverridePendingTransition;
	
	private LinkedHashMap<Long, BaseDelegate> delegatesHashMap;
	private boolean isActivityChanging;
	
	protected Activity rootViewController;
	protected Activity currentViewControllerApp;
	public Activity currentViewController;

	static {
		Method[] actMethods = Activity.class.getMethods();
		int methodSize = actMethods.length;
		for (int i=0; i<methodSize; i++) {
			if (actMethods[i].getName().equals("overridePendingTransition")) {
				methodOverridePendingTransition = actMethods[i];
				break;
			}
		}
	}
	
	public BaseViewsControllerImpl() {
		delegatesHashMap = new LinkedHashMap<Long, BaseDelegate>();
	}
	
	
	@Override
	public Activity getRootViewController() {
		// TODO Auto-generated method stub
		return rootViewController;
		
	}

	@Override
	public Activity getCurrentViewControllerApp() {
		// TODO Auto-generated method stub
		return currentViewControllerApp;
	}

	@Override
	public Activity getCurrentViewController() {
		// TODO Auto-generated method stub
		return currentViewController;

	}

	@Override
	public void setCurrentActivityApp(Activity currentViewController) {
		setCurrentActivityApp(currentViewController, false);		
	}

	@Override
	public void setCurrentActivityApp(Activity currentViewController,
			boolean asRootViewController) {

		if(CommonSession.getInstance().getCommonSessionService().getAllowLog()) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[BaseViewController] Se setea el CurrentActivity");
		this.currentViewControllerApp = currentViewController; 
		if (rootViewController == null) {
			this.rootViewController = currentViewController;
		}
		isActivityChanging = false;
		
		this.currentViewController = currentViewController; 
		
	}

	@Override
	public boolean isActivityChanging() {
		return isActivityChanging;
	}

	@Override
	public void setActivityChanging(boolean flag) {
		if(CommonSession.getInstance().getCommonSessionService().getAllowLog()) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[BaseViewController] Se setea el ActivityChanging ->"+flag);
		isActivityChanging = flag;		
	}

	@Override
	public void showViewController(Class<?> viewController) {
		showViewController(viewController, 0, false);
	}

	@Override
	public void showViewController(Class<?> viewController, int flags) {
		showViewController(viewController, flags, false);
	}

	@Override
	public void showViewController(Class<?> viewController, int flags,
			boolean inverted) {
		showViewController(viewController, flags, inverted, null, null);		
	}

	@Override
	public void showViewController(Class<?> viewController, int flags,
			boolean inverted, String[] extrasKeys, Object[] extras) {
		/*isActivityChanging = true;
		Intent intent = new Intent(currentViewControllerApp.getActivity(), viewController);
		if (flags != 0) {
			intent.setFlags(flags);
		}
		
		if (extrasKeys != null && extras != null && extras.length == extrasKeys.length) {
			int extrasLength = extras.length;
			for (int i=0; i<extrasLength; i++) {
				if (extras[i] instanceof String) {
					intent.putExtra(extrasKeys[i], (String)extras[i]);
				} else if (extras[i] instanceof Integer) {
					intent.putExtra(extrasKeys[i], ((Integer)extras[i]).intValue());
				} else if (extras[i] instanceof Boolean) {
					intent.putExtra(extrasKeys[i], ((Boolean)extras[i]).booleanValue());
				}else if(extras[i] instanceof Long){
					intent.putExtra(extrasKeys[i], ((Long)extras[i]).longValue());
				}
			}
		}
		
		currentViewControllerApp.getActivity().startActivity(intent);
		if (inverted) {
			overrideScreenBackTransition();
		} else {
			overrideScreenForwardTransition();
		}*/
		
	}
	
	@Override
	public void showViewController(Class<?> viewController, int flags, boolean inverted, String[] extrasKeys, Object[] extras, Activity activity) {
		//isActivityChanging = true;
		Intent intent = new Intent(activity, viewController);
		if (flags != 0) {
			intent.setFlags(flags);
		}
		
		if (extrasKeys != null && extras != null && extras.length == extrasKeys.length) {
			int extrasLength = extras.length;
			for (int i=0; i<extrasLength; i++) {
				if (extras[i] instanceof String) {
					intent.putExtra(extrasKeys[i], (String)extras[i]);
				} else if (extras[i] instanceof Integer) {
					intent.putExtra(extrasKeys[i], ((Integer)extras[i]).intValue());
				} else if (extras[i] instanceof Boolean) {
					intent.putExtra(extrasKeys[i], ((Boolean)extras[i]).booleanValue());
				}else if(extras[i] instanceof Long){
					intent.putExtra(extrasKeys[i], ((Long)extras[i]).longValue());
				}
			}
		}
		
		activity.startActivity(intent);
    
    }

	@Override
	public void showViewControllerForResult(Class<?> viewController,
			int activityCode) {
		/*isActivityChanging = true;
		Intent intent = new Intent(currentViewControllerApp.getActivity(), viewController);
		
		intent.setAction("com.google.zxing.client.android.SCAN");
		//intent.putExtra("ACTION", "com.google.zxing.client.android.SCAN");
		
		intent.putExtra("SCAN_WIDTH", (int)(GuiTools.getCurrent().ScreenHeigth * 0.9));
		intent.putExtra("SCAN_HEIGHT", (int)(GuiTools.getCurrent().ScreenWidth * 0.2));
		
		overrideScreenForwardTransition();
		currentViewControllerApp.getActivity().startActivityForResult(intent, activityCode);	*/	
	}

	@Override
	public void backToRootViewController() {
		/*Intent intent = new Intent(currentViewControllerApp.getActivity(), rootViewController.getClass());
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		currentViewControllerApp.getActivity().startActivity(intent);
		overrideScreenBackTransition();*/
	}

	@Override
	public void cierraViewsController() {
		/*rootViewController = null;
		if (currentViewControllerApp != null) {
			currentViewControllerApp.getActivity().finish();
			overrideScreenBackTransition();
			currentViewControllerApp = null;
		}*/

		delegatesHashMap.clear();
	}

	@Override
	public void addDelegateToHashMap(long id, BaseDelegate baseDelegate) {

		if(CommonSession.getInstance().getCommonSessionService().getAllowLog()) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[BaseViewsController] Se añade a la pila de delegates una instancia de "+baseDelegate.getClass().getName());
		delegatesHashMap.put(Long.valueOf(id), baseDelegate);		
	}

	@Override
	public BaseDelegate getBaseDelegateForKey(long key) {
		BaseDelegate baseDelegate = delegatesHashMap.get(Long.valueOf(key));
		return baseDelegate;
	}

	@Override
	public Long getLastDelegateKey() {
		return (Long) delegatesHashMap.keySet().toArray()[delegatesHashMap.size() - 1];
	}

	@Override
	public void removeDelegateFromHashMap(long key) {
		delegatesHashMap.remove(Long.valueOf(key));		
	}

	@Override
	public void clearDelegateHashMap() {
		delegatesHashMap.clear();
	}

	@Override
	public void overrideScreenForwardTransition() {
		if (methodOverridePendingTransition != null) {
			try {
				methodOverridePendingTransition.invoke(currentViewController, new Object[]{ R.anim.api_common_screen_enter, R.anim.api_common_screen_leave });
			} catch (NullPointerException npex) {
				//receiver was null
//				if(Server.ALLOW_LOG) Log.d("BaseViewsController :: overrideScreenForwardTransition", "Receiver was null");
			} catch (IllegalAccessException iaex) {
				//method not accesible
//				if(Server.ALLOW_LOG) Log.d("BaseViewsController :: overrideScreenForwardTransition", "Method not accesible");
			} catch (IllegalArgumentException iarex) {
				//incorrect arguments
//				if(Server.ALLOW_LOG) Log.d("BaseViewsController :: overrideScreenForwardTransition", "Incorrect arguments");
			} catch (InvocationTargetException itex) {
				//invocation returned exception
//				if(Server.ALLOW_LOG) Log.d("BaseViewsController :: overrideScreenForwardTransition", "Invocation exception: " + itex.toString());
			}
		}
	}

	@Override
	public void overrideScreenBackTransition() {
		if (methodOverridePendingTransition != null) {
			try {
				methodOverridePendingTransition.invoke(currentViewController, new Object[]{ R.anim.api_common_screen_enter_back, R.anim.api_common_screen_leave_back });
			} catch (NullPointerException npex) {
				//receiver was null
				//if(Server.ALLOW_LOG) Log.d("BaseViewsController :: overrideScreenBackTransition", "Receiver was null");
			} catch (IllegalAccessException iaex) {
				//method not accesible
				//if(Server.ALLOW_LOG) Log.d("BaseViewsController :: overrideScreenBackTransition", "Method not accesible");
			} catch (IllegalArgumentException iarex) {
				//incorrect arguments
				//if(Server.ALLOW_LOG) Log.d("BaseViewsController :: overrideScreenBackTransition", "Incorrect arguments");
			} catch (InvocationTargetException itex) {
				//invocation returned exception
				//if(Server.ALLOW_LOG) Log.d("BaseViewsController :: overrideScreenBackTransition", "Invocation exception: " + itex.toString());
			}
		}
	}

	@Override
	public void onUserInteraction() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean consumeAccionesDePausa() {
		// TODO Auto-generated method stub
//		if (/*!TimerManager.isMainAppInControl()
//				&&*/ !isActivityChanging()) {
////			cerrarSesionBackground();
//			return true;
//		}
//		return false;

		// If the screen is off then the device has been locked
		PowerManager powerManager = (PowerManager) currentViewController.getSystemService(currentViewController.POWER_SERVICE);
		boolean isScreenOn = powerManager.isScreenOn();
		boolean ret = false;

		if (!isScreenOn) {
			TimerController.getInstance().getSessionCloser().cerrarSesion();
			ret = true;
		}

		return ret;
	}

	@Override
	public boolean consumeAccionesDeReinicio() {
//		if (!SuiteApp.getInstance().getBmovilApplication()
//				.isApplicationLogged() && !TimerManager.isMainAppInControl()) {
//			((MenuSuiteViewController) SuiteApp.getInstance()
//					.getSuiteViewsController().getCurrentViewControllerApp())
//					.setShouldHideLogin(true);
//			SuiteApp.getInstance().getSuiteViewsController()
//					.showMenuSuite(true);
//			return true;
//		}
		return false;
	}

	@Override
	public boolean consumeAccionesDeAlto() {
		if (!isActivityChanging() /*&& !TimerManager.isMainAppInControl()*/
				&& currentViewControllerApp != null) {
//			currentViewControllerApp.hideSoftKeyboard();
		}
		setActivityChanging(false);
		return true;
	}

	@Override
	public void showMenuInicial() {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Asigna a null el campo rootVoewController
	 */
	@Override
	public void resetRootViewController() {
		this.rootViewController = null;
	}
}

