package bancomer.api.common.io;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;


public interface ParserJSON {

    
    /**
     * Parse the operation result from received message.
     * @return the operation result
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public Result parseResult() throws IOException, ParsingException;

    /**
     * Obtain the next value for a tag.
     * @param tag the target tag
     * @return the value
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public String parseNextValue(String tag) throws IOException, ParsingException;

    /**
     * Obtain the next value for a tag.
     * @param tag the target tag
     * @param mandatory indicates the tag is mandatory
     * @return the value
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public String parseNextValue(String tag, boolean mandatory) throws ParsingException, IOException;


    public JSONArray parseNextValueWithArray(String tag) throws ParsingException, IOException;
    
    public JSONArray parseNextValueWithArray(String tag, boolean mandatory) throws ParsingException, IOException;
    
    public JSONObject parserNextObject(String tag) throws ParsingException, IOException;
    
    public JSONObject parserNextObject(String tag, boolean mandatory) throws ParsingException, IOException;

    boolean hasValue(String tag) throws ParsingException, IOException;
}
