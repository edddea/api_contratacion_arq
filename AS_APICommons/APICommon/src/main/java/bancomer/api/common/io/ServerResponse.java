/*
 * Copyright (c) 2010 BBVA. All Rights Reserved

 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package bancomer.api.common.io;

import java.io.IOException;


//One click

/**
 * ServerResponse wraps the response from the server, containing attributes that.
 * define if the response has been successful or not, and the data content if the
 * response was successful
 *
 * @author Stefanini IT Solutions.
 */
public interface ServerResponse{

	

	/**
     * Operation successful.
     */
    public static final int OPERATION_SUCCESSFUL = 0;

    /**
     * Operation with warning.
     */
    public static final int OPERATION_WARNING = 1;

    /**
     * Operation failed.
     */
    public static final int OPERATION_ERROR = 2;

    /**
     * Operation succesful but asks for optinal updating.
     */
    public static final int OPERATION_OPTIONAL_UPDATE = 3;

    /**
     * Session expired.
     */
    public static final int OPERATION_SESSION_EXPIRED = -100;

    /**
     * Unknown operation result.
     */
    public static final int OPERATION_STATUS_UNKNOWN = -1000;
    
    /**
     * Get the status code.
     * @return the status code
     */
    public int getStatus();

    /**
     * Get the error code.
     * @return the error code
     */
    public String getMessageCode();

    /**
     * Get the error text.
     * @return  the error text
     */
    public String getMessageText();

    /**
     * Get the response.
     * @return the content response
     */
    public Object getResponse();

    /**
     * Gets the value of the URL for mandatory application update.
     * @return SOMETHING
     */
    public String getUpdateURL();
//
//    /**
//     * Parse the response to get the attributes.
//     * @param parser instance capable of parsing the response
//     * @throws IOException exception
//     * @throws ParsingException exception
//     */
//    public void process(Parser parser) throws IOException, ParsingException;
//
//    public void process(ParserJSON parser) throws IOException, ParsingException;

}
