/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package bancomer.api.common.io;

import java.io.IOException;


/**
 * Parser is able to parse an operation response from Bancomer server.
 *
 * @author Stefanini IT Solutions.
 */
public interface Parser {


	/**
     * Response status successful.
     */
    public static final String STATUS_OK = "OK";

    /**
     * Response status warning.
     */
    public static final String STATUS_WARNING = "AVISO";

    /**
     * Response status error.
     */
    public static final String STATUS_ERROR = "ERROR";

    /**
     * Response optional application update.
     */
    public static final String STATUS_OPTIONAL_UPDATE = "AC";

    /**
     * Tag for status.
     */
    public static final String STATUS_TAG = "ES";

    /**
     * Tag for error code.
     */
    public static final String CODE_TAG = "CO";

    /**
     * Tag for error message.
     */
    public static final String MESSAGE_TAG = "ME";

    /**
     * Tag for application mandatory update URL.
     */
    public static final String URL_TAG = "UR";

    /**
     * Tag separator.
     */
    public static final char SEPARATOR_CHAR = '*';

    /**
     * Escape character for special symbols in the response.
     */
    public static final char CODE_CHAR = '%';

    /**
     * Size in characters of a special symbol.
     */
    public static final int CODE_SIZE = 2;
	
	
    /**
     * Default constructor.
     * @param rder reader to read the response from
     */
    //public Parser(Reader rder);

    /**
     * Parse the operation result from received message.
     * @return the operation result
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public Result parseResult() throws IOException, ParsingException;

    /**
     * Obtain the next entity.
     * @return the next entity
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     * @throws bancomer.api.module.io.ParsingException 
     */
    public String parseNextEntity() throws IOException, ParsingException;
    /**
     * Obtain the next entity.
     * @param throwException indicates if an exception must be thrown on end of input content
     * @return the entity
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public String parseNextEntity(boolean throwException) throws IOException, ParsingException;

    /**
     * Obtain the next value of the whole entity.
     * @return the next value
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public String parseNextValue() throws IOException, ParsingException;

    /**
     * Obtain the next value for a tag.
     * @param tag the target tag
     * @return the value
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public String parseNextValue(String tag) throws IOException, ParsingException;

    /**
     * Obtain the next value for a tag.
     * @param tag the target tag
     * @param mandatory indicates the tag is mandatory
     * @return the value
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public String parseNextValue(String tag,
    			   boolean mandatory) throws IOException, ParsingException;
                      
    
    /**
     * Gets the value for a tag in an entity.
     * @param entity the entity
     * @param tag the tag
     * @return the value
     * @throws IOException on parsing errors
     */
    public String entityValue(String entity, String tag) throws IOException;

    /**
     * Try to parse a string as an integer.
     * @param value value to convert
     * @param defaultValue value to use if the conversion cannot be accomplished
     * @return the converted integer, or defaultValue if the conversion was not
     * successful.
     */
    public int valueAsInt(String value, int defaultValue);

}