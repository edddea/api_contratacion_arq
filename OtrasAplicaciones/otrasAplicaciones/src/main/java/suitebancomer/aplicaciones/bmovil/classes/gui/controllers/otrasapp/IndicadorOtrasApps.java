package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.otrasapp;

import com.bancomer.mbanking.otrasaplicaciones.R;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;

public final class IndicadorOtrasApps {
	private static ProgressDialog mProgressDialog;
	private static AlertDialog mAlertDialog;

	private IndicadorOtrasApps() {

	}

	public static void muestraIndicadorActividad(final Context context,
			final String strTitle, final String strMessage) {
		
				if (mProgressDialog != null) {
					ocultaIndicadorActividad();
				}
				mProgressDialog = ProgressDialog.show(context, strTitle,
						strMessage, true);
				mProgressDialog.setCancelable(false);
	}

	public static void ocultaIndicadorActividad() {
		if (mProgressDialog != null) {
			mProgressDialog.dismiss();
		}
	}

	public static void showErrorMessage(final Context context,
			final String errorMessage, final OnClickListener listener) {

		if (errorMessage.length() > 0) {
			final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
					context);
			alertDialogBuilder.setTitle(R.string.label_error);
			alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
			alertDialogBuilder.setMessage(errorMessage);
			alertDialogBuilder.setPositiveButton(R.string.label_ok, listener);
			alertDialogBuilder.setCancelable(false);
			mAlertDialog = alertDialogBuilder.show();
		}
	}

	public static void ocultaAlertError() {
		if (mAlertDialog != null) {
			mAlertDialog.dismiss();
		}
	}

}
