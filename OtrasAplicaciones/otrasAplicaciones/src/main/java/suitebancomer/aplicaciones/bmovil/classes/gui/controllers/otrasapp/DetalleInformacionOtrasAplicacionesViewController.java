package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.otrasapp;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import suitebancomer.aplicaciones.bmovil.classes.common.otrasapp.OtrasAppsConstants;
import suitebancomer.aplicaciones.bmovil.classes.model.otrasapp.InfoAppData;
import suitebancomer.aplicaciones.bmovil.classes.model.otrasapp.OtrasAplicacionesData;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

import static suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.isNull;

/**
 * Created by amezquitillo on 21/09/2015.
 */
public final class DetalleInformacionOtrasAplicacionesViewController {

    private DetalleInformacionOtrasAplicacionesViewController(){}

    private static DatosBmovilFileManager datosBmovil;

    /**
     * Instancia de respuesta del servidor con la informacion de las
     * aplicaciones
     */
    private static OtrasAplicacionesData responseObject;
    /**
     * Metodo que procesa la respuesta del servidor
     *
     * @param catalogoOtrasAplicaciones
     *            Cadena con el catalogo de las aplicaciones
     * @return Regresa un objeto con la informacion de las aplicaciones
     */
    public static OtrasAplicacionesData procesarCatalogo(
            final String catalogoOtrasAplicaciones) {

        final JsonObject json = new JsonParser().parse(
                catalogoOtrasAplicaciones).getAsJsonObject();
        final JsonArray jsonArray = json.get(OtrasAppsConstants.APLICACIONES)
                .getAsJsonArray();

        final Gson gson = new Gson();

        final List<InfoAppData> listAplicaciones = new ArrayList<InfoAppData>();
        InfoAppData infoAppData;
        final Iterator<JsonElement> iterator = jsonArray.iterator();

        while (iterator.hasNext()) {
            infoAppData = gson.fromJson((JsonObject) iterator.next(),
                    InfoAppData.class);
            listAplicaciones.add(infoAppData);
        }

        final OtrasAplicacionesData objectOtrasApps = new OtrasAplicacionesData();

        objectOtrasApps.setEstado(json.get(ApiConstants.STATUS_TAG)
                .getAsString());
        objectOtrasApps.setVersionCatApp(json.get(
                OtrasAppsConstants.PARAM_VERSION_CATAPP).getAsString());
        objectOtrasApps.setAplicaciones(listAplicaciones);
        return objectOtrasApps;
    }


    /**
     * Metodo que revisa si la respuesta del server fue correcta
     *
     * @return Regresa TRUE si hubo algun error en la peticion del server
     */
    public static Boolean checkRespuesta(final DatosBmovilFileManager datosBmovil) {
        if (isNull(responseObject) || isNull(responseObject.getVersionCatApp())
                && isNull(datosBmovil.getCatalogoOtrasApps())) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    /**
     * Metodo que valida la respuesta del server fue incorrecta y existe el
     * catalogo precargado o bien si la respuesta del server fue correcta
     *
     * @return Regresa TRUE si esta el server respondio OK o si ya se tiene el
     *         catalogo precargado
     */
    public static Boolean checkCatalogoApps(final DatosBmovilFileManager datosBmovil) {
        //if ((responseObject.getEstado() == null && null != datosBmovil.getCatalogoOtrasApps())
        if (isNull(responseObject.getEstado()) && !isNull(datosBmovil.getCatalogoOtrasApps())
                || ApiConstants.OK.equals(responseObject.getEstado())) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }


    /**
     * @return the datosBmovil
     */
    public static DatosBmovilFileManager getDatosBmovil() {
        if (isNull(datosBmovil)) {
            datosBmovil = DatosBmovilFileManager.getCurrent();
            if(isNull(datosBmovil.getCatalogoOtrasAppsVersion())){
                datosBmovil.setCatalogoOtrasAppsVersion("0");
            }
        }
        return datosBmovil;
    }

    /**
     * Metodo que escribe el error en el log y quita el indicador de actividad
     *
     * @param e
     *            Excepcion lanzada
     */
    public static void logAndIndicador(final Exception e) {
        if (ServerCommons.ALLOW_LOG) {
            Log.e(OtrasAplicacionesViewControllers.class.getSimpleName(),
                    e.getMessage(), e);
        }
        IndicadorOtrasApps.ocultaIndicadorActividad();

    }

    public static OtrasAplicacionesData getResponseObject() {
        return responseObject;
    }

    public static void setResponseObject(final OtrasAplicacionesData responseObject) {
        DetalleInformacionOtrasAplicacionesViewController.responseObject = responseObject;
    }
}
