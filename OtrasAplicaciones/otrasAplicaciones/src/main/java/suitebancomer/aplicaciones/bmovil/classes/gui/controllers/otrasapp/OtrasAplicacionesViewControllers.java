package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.otrasapp;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.*;
import android.util.*;
import android.view.*;
import android.widget.*;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.otrasaplicaciones.R;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import java.io.*;
import java.util.*;

import suitebancomer.aplicaciones.bmovil.classes.common.otrasapp.OtrasAppsConstants;
import suitebancomer.aplicaciones.bmovil.classes.model.otrasapp.*;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.service.*;
import suitebancomercoms.aplicaciones.bmovil.classes.common.*;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Controlador de otras aplicaciones
 * 
 * @author evaltierrah
 * @version 1.0
 */
public class OtrasAplicacionesViewControllers extends OtrasAplicacionesBase {

	/**
	 * Instancia de la lista de las aplicaciones a mostrar
	 */
	private ExpandableListView listViewAplicaciones;



	/**
	 * Instancia de respuesta del servidor con la informacion de las
	 * aplicaciones
	 */
	private static OtrasAplicacionesData responseObject;
	/**
	 * String con la respuesta
	 */
	private static String responseString;
	/**
	 * Map
	 */
	private static Map<Object, Object> paramTable;
	/**
	 * Nombre de la aplicacion
	 */
	public static String nombreApp;


	private static Context contextApp;

	/**
	 * Instancia que permite el guardado del catalogo
	 */
	private static DatosBmovilFileManager datosBmovil;




	/**
	 * Metodo que incializa los valores
	 * 
	 * @param context
	 *            Contexto de la aplicacion
	 * @throws JSONException
	 * @throws IOException
	 * @throws IllegalStateException
	 * @throws ClientProtocolException
	 * @throws UnsupportedEncodingException
	 */
	public static void initValuesForRequest(final Context context)
			throws IllegalStateException, IOException, JSONException {
		contextApp = context;
		datosBmovil = DetalleInformacionOtrasAplicacionesViewController.getDatosBmovil();

		final String versionOtrasApps = datosBmovil.getCatalogoOtrasAppsVersion();
		paramTable = getParamTable();
		paramTable.put(OtrasAppsConstants.PARAM_OPERACION, OtrasAppsConstants.OPERACION);
		paramTable.put(OtrasAppsConstants.PARAM_NOMBRE_APLICACION, nombreApp);
		paramTable.put(OtrasAppsConstants.PARAM_VERSION_CATAPP, versionOtrasApps);
		paramTable.put(OtrasAppsConstants.PARAM_PLATAFORMA, ApiConstants.ANDROID_STRING);
		final ICommService serverproxy = new CommServiceProxy(context);

		final ParametersTO parameters = new ParametersTO();
		parameters.setSimulation(ServerCommons.SIMULATION);
		parameters.setDevelopment(ServerCommons.DEVELOPMENT);
		parameters.setParameters(((Hashtable<Object, Object>) paramTable).clone());
		parameters.setJson(true);
		parameters.setOperationId(OtrasAppsConstants.OP_OTRAS_APLICACIONES);
		// parameters.setOperationId(300);
		IResponseService resultado;
		resultado = serverproxy.request(parameters, OtrasAplicacionesData.class);

		responseObject = (OtrasAplicacionesData) resultado.getObjResponse();
		DetalleInformacionOtrasAplicacionesViewController.setResponseObject(responseObject);
		responseString = resultado.getResponseString();

		if (!DetalleInformacionOtrasAplicacionesViewController.checkCatalogoApps(datosBmovil)) {
			throw new IOException(OtrasAplicacionesViewControllers.getResponseString());
		}
	}


	/**
	 * Places the title string and respective icon on screen with the desired
	 * color
	 * 
	 * @param titleText
	 *            the screen title
	 * @param iconResource
	 *            the resource of the screen icon
	 * setTitle(titleText, iconResource,
	 *      SuiteAppOtrasApps.getResourceId("segundo_azul", "color"), true);
	 */
	private void setTitle(final int titleText, final int iconResource,
			final int colorResource, final boolean visible) {

		if (visible) {
			mTitleLayout.setVisibility(View.VISIBLE);
			final ImageView icon = (ImageView) mTitleLayout
					.findViewById(R.id.title_icon);
			if (iconResource > 0) {
				icon.setImageDrawable(getResources().getDrawable(iconResource));
			}
			if (titleText > 0) {
				final String title = getString(titleText);
				final TextView titleTextView = (TextView) mTitleLayout.findViewById(R.id.title_text);
				titleTextView.setTextColor(getResources().getColor(colorResource));
				titleTextView.setText(title);
			}
			mTitleDivider.setVisibility(View.VISIBLE);
		} else {
			mTitleLayout.setVisibility(View.GONE);
			mTitleDivider.setVisibility(View.GONE);
		}
	}


	/**
	 * Metodo que crea los iconos a mostrar
	 * 
	 * @param data
	 *            Informacion de las aplicaciones
	 */
	private void buildIcons(final OtrasAplicacionesData data) {

		final ExpandableListAdapter adapterApps = new AdapterApp(this,
				data.getAplicaciones());

		listViewAplicaciones.setAdapter(adapterApps);
		listViewAplicaciones.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

					@Override
					public boolean onGroupClick(final ExpandableListView parent,
												final View v, final int groupPosition, final long id) {
						return true;
					}
				});
		setGroupIndicatorToRight();
	}

	/**
	 * Metodo que regresa a la pantalla anterior
	 * 
	 * @return Regresa un listener
	 */
	private OnClickListener returnToBakc() {
		return new DialogInterface.OnClickListener() {

			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				finish();
				onBackPressed();
			}
		};
	}

	/**
	 * Metodo que crea los grupos de aplicaciones
	 */
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
	private void setGroupIndicatorToRight() {
		/* Get the screen width */
		final DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		final int width = dm.widthPixels;

		final int currentapiVersion = android.os.Build.VERSION.SDK_INT;
		if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
			// aplica para la version 18 en adelante
			listViewAplicaciones.setIndicatorBoundsRelative(width
					- getDipsFromPixel(90), width - getDipsFromPixel(5));
		} else {
			listViewAplicaciones.setIndicatorBounds(width
					- getDipsFromPixel(90), width - getDipsFromPixel(5));
		}
	}

	/**
	 * Convert pixel to dip
	 * 
	 * @param pixels
	 *            Pixeles
	 * @return regresa la escala
	 */
	public int getDipsFromPixel(final float pixels) {
		// Get the screen's density scale
		final float scale = getResources().getDisplayMetrics().density;
		// Convert the dps to pixels, based on density scale
		return (int) (pixels * scale + 0.5f);
	}

	/**
	 * @return the paramTable
	 */
	public static Map<Object, Object> getParamTable() {
		if (Tools.isNull(paramTable)) {
			paramTable = new Hashtable<Object, Object>();
		}
		return paramTable;
	}



	/**
	 * @param cierraAplicacion
	 *            the cierraAplicacion to set
	 */
	public void setCierraAplicacion(final Boolean cierraAplicacion) {
		this.cierraAplicacion = cierraAplicacion;
	}

	public static String getResponseString() {
		if (Tools.isNull(responseString)) {
			responseString = OtrasAppsConstants.ERROR_COMUNICACIONES;
		}
		return responseString;
	}


	/*
         * (non-Javadoc)
         *
         * @see android.app.Activity#onCreate(android.os.Bundle)
         */
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);

		listViewAplicaciones = (ExpandableListView) findViewById(R.id.listViewApps);

		SuiteApp.appContext = getApplicationContext();
		OtrasAplicacionesData otrasAplicacionesData;
		if (!DetalleInformacionOtrasAplicacionesViewController.checkRespuesta(datosBmovil) &&
				DetalleInformacionOtrasAplicacionesViewController.checkCatalogoApps(datosBmovil)) {

			if (responseObject.getAplicaciones() != null
					&& responseObject.getAplicaciones().size() != 0) {
				datosBmovil.setCatalogoOtrasApps(OtrasAplicacionesViewControllers.getResponseString());
				datosBmovil.setCatalogoOtrasAppsVersion(responseObject.getVersionCatApp());
			}

			otrasAplicacionesData = DetalleInformacionOtrasAplicacionesViewController.procesarCatalogo(datosBmovil.getCatalogoOtrasApps());
			buildIcons(otrasAplicacionesData);

		} else {
			IndicadorOtrasApps.ocultaIndicadorActividad();
			// mostrar alert
			String mensaje = SuiteApp.appContext
					.getString(R.string.bmovil_otrasaplicaciones_error_comunicaciones);
			if (responseObject.getDescripcionMensaje() != null
					&& !Constants.EMPTY_STRING.equals(
					responseObject.getDescripcionMensaje())) {
				mensaje = responseObject.getDescripcionMensaje();
			}

			IndicadorOtrasApps.showErrorMessage(contextApp, mensaje, returnToBakc());
		}
		formatView();
		setTitle(R.string.bmovil_otrasaplicaciones_titulos,
				R.drawable.an_ic_masapps, R.color.segundo_azul, true);

		IndicadorOtrasApps.ocultaIndicadorActividad();
		TrackingHelper.trackState("appsbbva", (ArrayList<String>) estados);
	}




}
