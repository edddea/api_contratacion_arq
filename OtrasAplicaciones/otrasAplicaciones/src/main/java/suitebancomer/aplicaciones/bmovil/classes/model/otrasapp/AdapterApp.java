package suitebancomer.aplicaciones.bmovil.classes.model.otrasapp;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.*;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.*;
import android.widget.*;

import com.bancomer.mbanking.otrasaplicaciones.R;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import suitebancomer.aplicaciones.bmovil.classes.common.otrasapp.OtrasAppsConstants;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.otrasapp.OtrasAplicacionesViewControllers;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Clase que crea los eventos para cada aplicacion
 * 
 * @author evaltierrah
 * @version 1.0
 */
public class AdapterApp extends BaseExpandableListAdapter {

	/**
	 * Actividad que nos permite llamar a la aplicacion correspondiente
	 */
	private final OtrasAplicacionesViewControllers actividad;
	/**
	 * Contexto de la aplicacion
	 */
	private final Context context;
	/**
	 * Lista de datos
	 */
	private final List<InfoAppData> listDataHeader;
	private static boolean isTienda=false;

	/**
	 * Constructor que recibe el contexto y lista de datos
	 * 
	 * @param context
	 *            Contexto de la aplicacion
	 * @param listDataHeader
	 *            Lista de datos
	 */
	public AdapterApp(final Context context,
			final List<InfoAppData> listDataHeader) {
		this.context = context;
		this.listDataHeader = listDataHeader;
		this.actividad = (OtrasAplicacionesViewControllers) context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getChild(int, int)
	 */
	@Override
	public Object getChild(final int groupPosition, final int childPosititon) {

		return this.listDataHeader.get(groupPosition).getInfoApp()
				.getDescripcionApp();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getChildId(int, int)
	 */
	@Override
	public long getChildId(final int groupPosition, final int childPosition) {
		return childPosition;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getChildView(int, int, boolean,
	 * android.view.View, android.view.ViewGroup)
	 */
	@SuppressLint("InflateParams")
	@Override
	public View getChildView(final int groupPosition, final int childPosition,
			final boolean isLastChild, final View convertView,
			final ViewGroup parent) {

		View localView = convertView;

		final String childText = (String) getChild(groupPosition, childPosition);

		if (localView == null) {
			final LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			localView = infalInflater.inflate(R.layout.layout_infoapp_otrasapp,
					null);
		}

		final TextView txtListChild = (TextView) localView
				.findViewById(R.id.descripcion_app);

		txtListChild.setText(childText);

		return localView;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getChildrenCount(int)
	 */
	@Override
	public int getChildrenCount(final int groupPosition) {
		if (this.isAppInstalled(this.listDataHeader.get(groupPosition)
				.getInfoApp().getNombrePaquete())) {
			return 0;
		} else {
			return 1;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getGroup(int)
	 */
	@Override
	public Object getGroup(final int groupPosition) {
		return this.listDataHeader.get(groupPosition);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getGroupCount()
	 */
	@Override
	public int getGroupCount() {
		return this.listDataHeader.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getGroupId(int)
	 */
	@Override
	public long getGroupId(final int groupPosition) {
		return groupPosition;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#getGroupView(int, boolean,
	 * android.view.View, android.view.ViewGroup)
	 */
	@SuppressLint("InflateParams")
	@Override
	public View getGroupView(final int groupPosition, final boolean isExpanded,
			final View convertView, final ViewGroup parent) {

		View localView = convertView;

		final InfoAppData objInfoApp = (InfoAppData) getGroup(groupPosition);

		if (localView == null) {
			final LayoutInflater infalInflater = (LayoutInflater) this.context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			localView = infalInflater.inflate(R.layout.layout_adapter_apps,
					null);
		}

		final ImageView imageIconApp = (ImageView) localView
				.findViewById(R.id.icono_app);

		imageIconApp.setImageResource(getIconForApp(objInfoApp));

		final TextView tituloApp = (TextView) localView
				.findViewById(R.id.titulo_app);
		tituloApp.setText(objInfoApp.getNombreApp());

		final ImageView accionApp = (ImageView) localView
				.findViewById(R.id.accion_app);
		if (this.isAppInstalled(objInfoApp.getInfoApp().getNombrePaquete())) {
			accionApp.setImageResource(R.drawable.an_bt_abrir);
		} else {
			accionApp.setImageResource(R.drawable.an_bt_descargar);
		}

		accionApp.setOnClickListener(this.addEventClickListener(objInfoApp));

		final ImageView indicador = (ImageView) localView
				.findViewById(R.id.indicadorGroup);

		final ImageView dividerPadre = (ImageView) localView
				.findViewById(R.id.image_divider_padre);

		if (getChildrenCount(groupPosition) == 0) {
			indicador.setVisibility(View.INVISIBLE);
		} else {
			indicador.setVisibility(View.VISIBLE);
			indicador.setImageResource(isExpanded ? R.drawable.arrow_up
					: R.drawable.arrow_down);

			indicador.setOnClickListener(new View.OnClickListener() {

				@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
				@Override
				public void onClick(final View v) {
					if (isExpanded) {
						((ExpandableListView) parent)
								.collapseGroup(groupPosition);
						dividerPadre.setVisibility(View.VISIBLE);
					} else {
						if(Build.VERSION.SDK_INT < 14){
							((ExpandableListView)parent).expandGroup(groupPosition);
						}else {
							((ExpandableListView) parent).expandGroup(
									groupPosition, true);
						}
						dividerPadre.setVisibility(View.INVISIBLE);
					}
				}
			});
		}

		return localView;
	}

	/*@Override
	public void onGroupExpanded(final int groupPosition) {
		super.onGroupExpanded(groupPosition);
	}*/

	/**
	 * Metodo que crea el icono de cada aplicacion
	 * 
	 * @param appData
	 *            Infomacion de la aplicacion
	 * @return Regresa el id del icono
	 */
	private int getIconForApp(final InfoAppData appData) {
		final String nombrePaqueteApp = appData.getInfoApp().getNombrePaquete();
		int codeImage = 0;

		if (OtrasAppsConstants.BMOVIL.equals(nombrePaqueteApp)) {
			codeImage = R.drawable.an_ic_bmovil;
		}

		if (OtrasAppsConstants.WALLET.equals(nombrePaqueteApp)) {
			codeImage = R.drawable.an_ic_wallet;
		}

		if (OtrasAppsConstants.VIDABANCOMER.equals(nombrePaqueteApp)) {
			codeImage = R.drawable.an_ic_vida;
		}

		return codeImage;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#hasStableIds()
	 */
	@Override
	public boolean hasStableIds() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ExpandableListAdapter#isChildSelectable(int, int)
	 */
	@Override
	public boolean isChildSelectable(final int groupPosition,
			final int childPosition) {

		final InfoAppData objInfoApp = (InfoAppData) getGroup(groupPosition);

		if (this.isAppInstalled(objInfoApp.getInfoApp().getNombrePaquete())) {
			return false;
		}
		return true;
	}

	/**
	 * Metodo que inicia el google play o la aplicacion si ya esta instalada
	 * 
	 * @param appData
	 *            Informacion de la aplicacion
	 * @return Regresa el listener para el click
	 */
	private View.OnClickListener addEventClickListener(final InfoAppData appData) {
		View.OnClickListener listener = null;

		if (isAppInstalled(appData.getInfoApp().getNombrePaquete())) {

			listener = new View.OnClickListener() {
				@Override
				public void onClick(final View arg0) {
					startApp(appData);
					isTienda = true;
				}
			};

		} else {
			listener = new View.OnClickListener() {

				@Override
				public void onClick(final View v) {
					showAppToInstall(appData);
					isTienda = true;
				}
			};
		}
		isTienda = false;
		return listener;

	}

	/**
	 * Revisa si la aplicacion esta instalada
	 * 
	 * @param namePackage
	 *            Nombre del paquete
	 * @return Indica si esta o no instalada la aplicacion
	 */
	private boolean isAppInstalled(final String namePackage) {
		final PackageManager packageManager = this.context.getPackageManager();
		boolean isInstalledApp = false;

		try {
			packageManager.getPackageInfo(namePackage,
					PackageManager.GET_ACTIVITIES);
			isInstalledApp = true;
		} catch (PackageManager.NameNotFoundException e) {
			isInstalledApp = false;
		}
		return isInstalledApp;
	}

	/**
	 * Inicia la aplicacion en caso de que ya este instalada
	 * 
	 * @param appData
	 *            informacion de la aplicacion
	 */
	private void startApp(final InfoAppData appData) {
		final PackageManager packageManager = this.context.getPackageManager();

		if (!appData.getInfoApp().getNombrePaquete()
				.equals(this.context.getApplicationContext().getPackageName())) {
			final Intent intentStartApp = packageManager
					.getLaunchIntentForPackage(appData.getInfoApp()
							.getNombrePaquete());
			intentStartApp.addCategory(Intent.CATEGORY_LAUNCHER);
			intentStartApp.addCategory(Intent.ACTION_MAIN);
			intentStartApp.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intentStartApp.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intentStartApp.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
			actividad.setCierraAplicacion(Boolean.TRUE);
			if(Build.VERSION.SDK_INT < 16){
				setIsTienda(true);
			}else {
				actividad.finishAffinity();
			}
			actividad.startActivity(intentStartApp);
		}
	}

	/**
	 * Muestra el google play o navegador para descargar e instalar la
	 * aplicacion seleccionada
	 * 
	 * @param appData
	 *            Informacion de la aplicacion
	 */
	private void showAppToInstall(final InfoAppData appData) {
		final String appPackageName = appData.getInfoApp().getNombrePaquete();
		String url = Constants.EMPTY_STRING;
		try {
			this.context.getPackageManager().getPackageInfo(
					OtrasAppsConstants.GOOGLE_PLAY, 0);
			url = OtrasAppsConstants.ID_MARKET + appPackageName;

		} catch (PackageManager.NameNotFoundException anfe) {
			url = OtrasAppsConstants.ID_URL_NAVEGADOR + appPackageName;
		}

		final Intent launchIntent = new Intent(Intent.ACTION_VIEW,
				Uri.parse(url));
		launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		launchIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		launchIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
		actividad.setCierraAplicacion(Boolean.TRUE);
		actividad.startActivity(launchIntent);
		if(Build.VERSION.SDK_INT < 16){
			setIsTienda(true);
			ActivityCompat.finishAffinity(actividad);
		}else {
			actividad.finishAffinity();
		}
	}

	/**
	 * este metodo no se usa por el momento pero, se dejara para futura
	 * implementacion
	 * 
	 * @param urlstr
	 * @return
	 */
	@SuppressWarnings("unused")
	private Drawable urlToDrawable(final String urlstr) {
		try {
			final InputStream is = (InputStream) new URL(urlstr).getContent();
			return Drawable.createFromStream(is, null);
		} catch (MalformedURLException e) {
			if (ServerCommons.ALLOW_LOG) {
				Log.e(this.getClass().getSimpleName(),
						"Se proporciono url invalida: " + urlstr, e);
			}
		} catch (IOException e) {
			if (ServerCommons.ALLOW_LOG) {
				Log.e(this.getClass().getSimpleName(), "IO exception: " + e, e);
			}
		}
		return null;
	}

	public static boolean getIsTienda() {
		return isTienda;
	}

	public static void setIsTienda(final boolean isTienda) {
		AdapterApp.isTienda = isTienda;
	}
}
