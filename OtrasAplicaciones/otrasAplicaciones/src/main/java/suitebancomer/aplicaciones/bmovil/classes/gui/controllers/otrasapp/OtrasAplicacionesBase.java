package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.otrasapp;

import android.app.Activity;
import android.os.Build;

import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.bancomer.mbanking.otrasaplicaciones.R;

import java.util.ArrayList;
import java.util.List;

import suitebancomer.aplicaciones.bmovil.classes.model.otrasapp.AdapterApp;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.GuiTools;

/**
 * Created by rodolfo on 4/27/16.
 */
public class OtrasAplicacionesBase extends Activity {
    protected Boolean cierraAplicacion = Boolean.TRUE;
    protected Boolean isActivityChanging = Boolean.TRUE;


    //AMZ
    public List<String> estados = new ArrayList<String>();

    /**
     * Layout del titulo
     */
    protected LinearLayout mTitleLayout;

    /**
     * Imagen
     */
    protected ImageView mTitleDivider;



    @Override
    protected void onStop() {
        super.onStop();
        finish();
        if (cierraAplicacion && !Tools.isNull(InitOtrasApps.getInstance().getCallBackSession())) {
            InitOtrasApps.getInstance().getCallBackSession().cierraSesionBackground(!cierraAplicacion);

        }
    }


    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        cierraAplicacion = Boolean.TRUE;
        isActivityChanging = Boolean.TRUE;
        if (ServerCommons.ALLOW_LOG) {
            Log.e(this.getClass().getSimpleName(), "Usuario deja la app");
        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        cierraAplicacion = Boolean.TRUE;
        isActivityChanging = Boolean.TRUE;
        if(InitOtrasApps.getInstance().getCallBackSession() != null) {
            InitOtrasApps.getInstance().getCallBackSession().userInteraction();
        }

    }


    @Override
    public void onBackPressed() {
        isActivityChanging = Boolean.TRUE;
        cierraAplicacion = Boolean.FALSE;
        finish();
        //TODO verificar si debe de ir
//		overridePendingTransition(R.anim.enter_from_right,
//				R.anim.enter_from_right);
        TrackingHelper.touchAtrasState();
        super.onBackPressed();
    }
    /*
         * (non-Javadoc)
         *
         * @see android.app.Activity#onPause()
         */
    @Override
    protected void onPause() {
        super.onPause();
        if(!isActivityChanging &&
                !Tools.isNull(InitOtrasApps.getInstance().getCallBackSession())) {
            InitOtrasApps.getInstance().getCallBackSession().cierraSesion();
        }
        finish();
        Log.e(this.getClass().getSimpleName(), "OnPause");
        if(AdapterApp.getIsTienda())
        {
            this.finish();
            if(Build.VERSION.SDK_INT < 16) {
                this.moveTaskToBack(true);
            }
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getCallingActivity();
        cierraAplicacion = Boolean.TRUE;
        isActivityChanging = Boolean.TRUE;
        hideTitle();
        setContentView(R.layout.layout_otras_aplicaciones);

    }

    /**
     * Oculta el titulo
     */
    private void hideTitle() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }


    /**
     * Agrega los controles al layout
     */
    protected void formatView() {
        final LinearLayout mHeaderLayout = (LinearLayout) findViewById(R.id.header_layout);
        mTitleLayout = (LinearLayout) findViewById(R.id.title_layout);
        mTitleDivider = (ImageView) findViewById(R.id.title_divider);

        final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(mHeaderLayout);
        guiTools.scale(mTitleLayout);
        guiTools.scale(mTitleLayout.findViewById(R.id.title_icon));
        guiTools.scale(mTitleLayout.findViewById(R.id.title_text), true);
        guiTools.scale(mTitleDivider);
    }

}
