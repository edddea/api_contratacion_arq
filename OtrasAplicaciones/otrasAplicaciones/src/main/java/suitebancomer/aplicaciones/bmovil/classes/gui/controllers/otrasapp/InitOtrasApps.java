package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.otrasapp;

import com.bancomer.base.callback.CallBackSession;

import suitebancomer.aplicaciones.bmovil.classes.model.otrasapp.BanderasServer;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by eluna on 16/08/2015.
 */
public class InitOtrasApps {

    private static InitOtrasApps initOtrasApps = new InitOtrasApps();

    private CallBackSession callBackSession;

    private InitOtrasApps(){

    }

    public InitOtrasApps(final BanderasServer banderasServer){
        ServerCommons.ALLOW_LOG = banderasServer.getLogActivo();
        ServerCommons.DEVELOPMENT = banderasServer.isDesarrollo();
        ServerCommons.EMULATOR = banderasServer.getEmulador();
        ServerCommons.SIMULATION = banderasServer.getSimulacion();
    }

    public CallBackSession getCallBackSession(){
        return callBackSession;
    }

    public void setCallBackSession(final CallBackSession callBackSession) {
        this.callBackSession = callBackSession;
    }
    public static InitOtrasApps getInstance(){
        return initOtrasApps;
    }
}
