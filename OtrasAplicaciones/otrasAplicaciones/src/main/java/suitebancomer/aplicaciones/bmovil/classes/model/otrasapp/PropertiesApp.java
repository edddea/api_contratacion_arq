package suitebancomer.aplicaciones.bmovil.classes.model.otrasapp;

import java.io.Serializable;

public class PropertiesApp implements Serializable {

	/**
	 * Default serial version
	 */
	private static final long serialVersionUID = 5452354802800569782L;
	/**
	 * Descripcion de la aplicacion
	 */
	private String descripcionApp;
	/**
	 * Icono
	 */
	private String icono;
	/**
	 * liga de tienda
	 */
	private String ligaTienda;
	/**
	 * Nombre del paquete
	 */
	private String nombrePaquete;

	/**
	 * @return the descripcionApp
	 */
	public String getDescripcionApp() {
		return descripcionApp;
	}

	/**
	 * @param descripcionApp the descripcionApp to set
	 */
	public void setDescripcionApp(final String descripcionApp) {
		this.descripcionApp = descripcionApp;
	}

	/**
	 * @return the icono
	 */
	public String getIcono() {
		return icono;
	}

	/**
	 * @param icono the icono to set
	 */
	public void setIcono(final String icono) {
		this.icono = icono;
	}

	/**
	 * @return the ligaTienda
	 */
	public String getLigaTienda() {
		return ligaTienda;
	}

	/**
	 * @param ligaTienda the ligaTienda to set
	 */
	public void setLigaTienda(final String ligaTienda) {
		this.ligaTienda = ligaTienda;
	}

	/**
	 * @return the nombrePaquete
	 */
	public String getNombrePaquete() {
		return nombrePaquete;
	}

	/**
	 * @param nombrePaquete the nombrePaquete to set
	 */
	public void setNombrePaquete(final String nombrePaquete) {
		this.nombrePaquete = nombrePaquete;
	}
}
