package suitebancomer.aplicaciones.bmovil.classes.model.otrasapp;

import java.io.Serializable;
import java.util.List;


public class OtrasAplicacionesData implements Serializable {

	/**
	 * Default serial version
	 */
	private static final long serialVersionUID = 5206898470528023320L;
	/**
	 * Estado
	 */
	private String estado;
	/**
	 * Lista de aplicaciones
	 */
	private List<InfoAppData> aplicaciones;
	/**
	 * Version de catalogo
	 */
	private String versionCatApp;
	/**
	 * Codigo de mensaje
	 */
	private String codigoMensaj;
	/**
	 * Descripcion del mensaje
	 */
	private String descripcionMensaje;

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}

	/**
	 * @param estado the estado to set
	 */
	public void setEstado(final String estado) {
		this.estado = estado;
	}

	/**
	 * @return the aplicaciones
	 */
	public List<InfoAppData> getAplicaciones() {
		return aplicaciones;
	}

	/**
	 * @param aplicaciones the aplicaciones to set
	 */
	public void setAplicaciones(final List<InfoAppData> aplicaciones) {
		this.aplicaciones = aplicaciones;
	}

	/**
	 * @return the versionCatApp
	 */
	public String getVersionCatApp() {
		return versionCatApp;
	}

	/**
	 * @param versionCatApp the versionCatApp to set
	 */
	public void setVersionCatApp(final String versionCatApp) {
		this.versionCatApp = versionCatApp;
	}

	/**
	 * @return the codigoMensaj
	 */
	public String getCodigoMensaj() {
		return codigoMensaj;
	}

	/**
	 * @param codigoMensaj the codigoMensaj to set
	 */
	public void setCodigoMensaj(final String codigoMensaj) {
		this.codigoMensaj = codigoMensaj;
	}

	/**
	 * @return the descripcionMensaje
	 */
	public String getDescripcionMensaje() {
		return descripcionMensaje;
	}

	/**
	 * @param descripcionMensaje the descripcionMensaje to set
	 */
	public void setDescripcionMensaje(final String descripcionMensaje) {
		this.descripcionMensaje = descripcionMensaje;
	}
		
}
