package suitebancomer.aplicaciones.bmovil.classes.common.otrasapp;

public final class OtrasAppsConstants {

	/**
	 * Identificador de Erro de comunicaciones
	 */
	public static final String ERROR_COMUNICACIONES = "Error de comunicaciones";
	/**
	 * Identificador de la llave para obtener la informacion de la aplicaciones
	 */
	public static final String KEY_INFO_APP = "objectApp";
	/**
	 * Devuelve la cadena catalogoAplicaciones
	 */
	public static final String OPERACION = "catalogoAplicaciones";
	/**
	 * Devuelve la cadena nombreApp
	 */
	public static final String PARAM_NOMBRE_APLICACION = "nombreApp";
	/**
	 * Devuelve la cadena operacion
	 */
	public static final String PARAM_OPERACION = "operacion";
	/**
	 * Devuelve la cadena versionCatApp
	 */
	public static final String PARAM_VERSION_CATAPP = "versionCatApp";
	/**
	 * Devuelve la cadena plataforma
	 */
	public static final String PARAM_PLATAFORMA = "plataforma";
	/**
	 * Devuelve la cadena aplicaciones
	 */
	public static final String APLICACIONES = "aplicaciones";
	/**
	 * Constante que indica el nombre del paquete de BMovil
	 */
	public static final String BMOVIL = "com.bancomer.mbanking";
	/**
	 * Constante que indica el nombre del paquete de Vida Bancomer
	 */
	public static final String VIDABANCOMER = "com.bancomer.vidabancomer";
	/**
	 * Constante que indica el nombre del paquete de Wallet
	 */
	public static final String WALLET = "com.bbva.bbvawalletmx";
	/**
	 * Constante que indica el nombre del paquete de google play
	 */
	public static final String GOOGLE_PLAY = "com.android.vending";
	/**
	 * Constante que devuelve el id de google play
	 */
	public static final String ID_MARKET = "market://details?id=";
	/**
	 * Constante que devuelve el id para entrar por navegador al google play
	 */
	public static final String ID_URL_NAVEGADOR = "https://play.google.com/store/apps/details?id=";
	/**
	 * Constante que indica el numero de operacion 113 para catalogo de otras aplicaciones
	 */
	public static final int OP_OTRAS_APLICACIONES = 113;
	
}
