package suitebancomer.aplicaciones.bmovil.classes.model.otrasapp;

import java.io.Serializable;

public class InfoAppData implements Serializable {

	/**
	 * Default serial version
	 */
	private static final long serialVersionUID = -2603600034075483819L;
	/**
	 * Nombre de la aplicacion
	 */
	private String nombreApp;
	/**
	 * Informacion de la aplicacion
	 */
	private PropertiesApp infoApp;

	/**
	 * @return the nombreApp
	 */
	public String getNombreApp() {
		return nombreApp;
	}

	/**
	 * @param nombreApp the nombreApp to set
	 */
	public void setNombreApp(final String nombreApp) {
		this.nombreApp = nombreApp;
	}

	/**
	 * @return the infoApp
	 */
	public PropertiesApp getInfoApp() {
		return infoApp;
	}

	/**
	 * @param infoApp the infoApp to set
	 */
	public void setInfoApp(final PropertiesApp infoApp) {
		this.infoApp = infoApp;
	}
	
	
}
