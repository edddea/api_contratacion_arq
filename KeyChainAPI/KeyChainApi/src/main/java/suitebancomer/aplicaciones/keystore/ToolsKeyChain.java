package suitebancomer.aplicaciones.keystore;

import android.util.Log;

/**
 * Created by eluna on 05/11/2015.
 */
public  abstract class ToolsKeyChain
{
    public static void writeLogeEx(final String tag, final String mensaje, final boolean showLog, final Exception e) {
        if(showLog) {
            Log.e(tag, mensaje,e);
        }
    }

    public static void writeLoge(final String tag, final String mensaje, final boolean showLog) {
        if(showLog) {
            Log.e(tag, mensaje);
        }
    }

    public static void writeLogd(final String tag, final String mensaje, final boolean showLog) {
        if(showLog) {
            Log.d(tag, mensaje);
        }
    }

    public static void writeLogi(final String tag, final String mensaje, final boolean showLog) {
        if(showLog) {
            Log.i(tag, mensaje);
        }
    }
}
