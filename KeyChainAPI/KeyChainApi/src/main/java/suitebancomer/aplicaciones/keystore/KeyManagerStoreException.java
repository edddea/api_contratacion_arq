package suitebancomer.aplicaciones.keystore;


public class KeyManagerStoreException extends Exception {

	private static final long serialVersionUID = 1934098484417065232L;

	public KeyManagerStoreException() {
		//Constructor public of the class
	}

	public KeyManagerStoreException(final String detailMessage) {
		super(detailMessage);
	}

	public KeyManagerStoreException(final Throwable throwable) {
		super(throwable);
		
	}

	public KeyManagerStoreException(final String detailMessage, final Throwable throwable) {
		super(detailMessage, throwable);
	}

}
