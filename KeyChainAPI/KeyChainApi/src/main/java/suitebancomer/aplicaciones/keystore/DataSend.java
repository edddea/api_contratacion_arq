package suitebancomer.aplicaciones.keystore;

/**
 * Created by alanmichaelgonzalez on 08/02/16.
 */
public class DataSend {
    public enum opcionesReactivacion{
        PA("PA"),//pendiente activacion
        PE("PE"),//pendiente envio
        WI("WI"),//wrong ium
        LG("LG"),//wrong ium
        ST("ST"),//softoken
        EXCKGET("ERROR GETENTRY KC"),//error al en el metodo keychay ->getentry
        EXCKSET("ERROR SETENTRY KC"),//error al en el metodo keychay ->setentry
        ERRFILE("ERROR OPEN KC");//softoken

        private String statusCode;

        private opcionesReactivacion(String s) {
            statusCode = s;
        }

        public String getStatusCode() {
            return statusCode;
        }
    }
    private opcionesReactivacion opcionReactivacion;
    private String marca;
    private String modelo;
    private String numeroSerie;
    private String plataforma="Android";
    private String versionPlataforma;
    private String versionBmovil;
    private String fecha;


    private String estatusApp;

    public void setOpcionesReactivacion(opcionesReactivacion or) {
        opcionReactivacion=or;
    }
    public opcionesReactivacion getOpcionesReactivacion( ) {
        return opcionReactivacion;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }
    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public String getPlataforma() {
        return plataforma;
    }

    public void setPlataforma(String plataforma) {
        this.plataforma = plataforma;
    }

    public String getVersionPlataforma() {
        return versionPlataforma;
    }

    public void setVersionPlataforma(String versionPlataforma) {
        this.versionPlataforma = versionPlataforma;
    }

    public String getVersionBmovil() {
        return versionBmovil;
    }

    public void setVersionBmovil(String versionBmovil) {
        this.versionBmovil = versionBmovil;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getEstatusApp() {
        return estatusApp;
    }

    public void setEstatusApp(String estatusApp) {
        this.estatusApp = estatusApp;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    private String app;


}
