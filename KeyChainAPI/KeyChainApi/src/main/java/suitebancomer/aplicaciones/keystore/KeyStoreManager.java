package suitebancomer.aplicaciones.keystore;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Calendar;

import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.x500.X500Principal;

import android.content.Context;
import android.security.KeyPairGeneratorSpec;


import com.bancomer.keygenerator.ToolsKey;


public class KeyStoreManager {
	static {
		Security.insertProviderAt(new org.spongycastle.jce.provider.BouncyCastleProvider(), 1);
	}
	/**
	 * Password por defecto del keystore.
	 */
	private static final String DEFAULT_KEY_STORE_PASSWORD = "passssword";
	private static final String ALIAS_BMOVIL = "bancomer-movil";
	private static final String TAG = KeyStoreManager.class.getSimpleName();
	//private static final String HMAC_SHA1_ALGORITHM = "HmacSHA1";
	
	private String mKeyStorePassword = DEFAULT_KEY_STORE_PASSWORD;
	private final String mKeyStoreName;
	private KeyStore mKeystore = null;

	private final File keyStoreFile;
	private Context context;
	
	/**
	 * 
	 * @param keyStoreFilePath
	 * @param keyStoreFileName
	 * @throws KeyStoreException
	 * @throws IOException 
	 */
	protected KeyStoreManager(final File keyStoreFilePath, final String keyStoreFileName, final Context context) throws KeyStoreException, IOException{
		setContext(context);
		mKeyStoreName = keyStoreFileName;
		//mKeyStorePassword = makeKeyMovil();

		mKeyStorePassword = ToolsKey.generateKey(context);
		
		// Creamos el fichero
		keyStoreFile = new File(keyStoreFilePath,mKeyStoreName );
		
		//mKeyStorePassword = ToolsKey.generateKey(context);
		ToolsKeyChain.writeLogd(TAG,"Clave para keystore " + mKeyStorePassword,SecurityConstants.allowLog);
		ToolsKeyChain.writeLogd(TAG, "Iniciando keystore manager con fichero: " + keyStoreFile.getAbsolutePath(),SecurityConstants.allowLog);
		
		openOrCreate();
	}
	
	protected Context getContext() {
		return context;
	}

	protected void setContext(final Context context) {
		this.context = context;
	}

	/**
	 * Crea el keystore
	 * @throws KeyStoreException
	 */
	private void openOrCreate() throws KeyStoreException{
		try {
			// Creamos la instancia del keystore
			mKeystore = KeyStore.getInstance(KeyStore.getDefaultType());
			
			// Comprobamos si existe el fichero.
			if (!keyStoreFile.exists() || 0 == keyStoreFile.length()) { //|| (0 == keyStoreFile.length())
				
				// No existe, iniciamos el keystore
				mKeystore.load(null,null);
				Runtime.getRuntime().exec("chmod 777 " + keyStoreFile.getAbsolutePath());
				ToolsKeyChain.writeLogd(TAG, "Keystore creado OK",SecurityConstants.allowLog);
			} else {
				// Existe, cargamos el keystore con el contenido del fichero.
				    ToolsKeyChain.writeLogd(TAG, "Accediendo a keystore " + mKeyStorePassword,SecurityConstants.allowLog);
				    final FileInputStream fist = new FileInputStream(keyStoreFile);
					mKeystore.load(fist, mKeyStorePassword.toCharArray());
					ToolsKeyChain.writeLogd(TAG, "Keystore inicializado OK",SecurityConstants.allowLog);
			}
		} catch (NoSuchAlgorithmException e) {
			throw new KeyStoreException(e);
		} catch (CertificateException e) {
			throw new KeyStoreException(e);
		} catch (IOException e) {
			throw new KeyStoreException(e);
		} catch (KeyStoreException e) {
			throw new KeyStoreException(e);
		}
	}
	
	/**
	 * Almacena en el keystore una nueva entrada, informando si se ha relizado correctamente o no la escritura. Si se produce
	 * un error lanzara la exception KeyManagerStoreException
	 * @param alias clave
	 * @param secretKey valor
	 * @return true si ha escrito correctamente, false en caso contrario
	 */
	protected  boolean setEntry(final String alias, final String secretKey) throws KeyManagerStoreException{

			boolean keyStoreEntryWritten = false;
		synchronized (this) {
			if (mKeystore != null && secretKey != null) {
				// store something in the key store
				final SecretKeySpec sks = new SecretKeySpec(secretKey.getBytes(), "MD5");
				final KeyStore.SecretKeyEntry ske = new KeyStore.SecretKeyEntry(sks);
				final KeyStore.ProtectionParameter pp = new KeyStore.PasswordProtection(null);

				try {
					ToolsKeyChain.writeLogi("Introducir dato en keystore", alias + " " + secretKey, SecurityConstants.allowLog);
					mKeystore.setEntry(alias, ske, pp);

					// Salvamos el keystore con la nueva clave
					final boolean success = save();

					if (success) {
						keyStoreEntryWritten = true;
					}
				} catch (KeyStoreException ex) {
					ToolsKeyChain.writeLoge(TAG, "Failed to read keystore" + mKeyStoreName, SecurityConstants.allowLog);
					throw new KeyManagerStoreException(ex);
				}
			}
		}
			return keyStoreEntryWritten;

	}
	
	/**
	 * Retorna el valor de la clave almacenada en el keystore.
	 * @param alias
	 * @return
	 */
	protected  String getEntry(final String alias) throws KeyManagerStoreException {

	    String secretStr = null;
	    byte[] secret = null;
		synchronized(this) {
			if (mKeystore != null) {
				try {
					if (!mKeystore.containsAlias(alias)) {
						ToolsKeyChain.writeLoge(TAG,
								new StringBuilder().append("Keystore ").append(mKeyStoreName)
										.append(" no contiene clave ").append(alias).toString(), SecurityConstants.allowLog);
						return null;
					}

					// get my entry from the key store
					final KeyStore.ProtectionParameter pp = new KeyStore.PasswordProtection(null);
					KeyStore.SecretKeyEntry ske = null;
					ske = (KeyStore.SecretKeyEntry) mKeystore.getEntry(alias, pp);

					if ( null == ske) {
						ToolsKeyChain.writeLoge(TAG, "Fallo al leer del keystore la clave " + alias, SecurityConstants.allowLog);
					} else {
						final SecretKeySpec sks = (SecretKeySpec) ske.getSecretKey();
						secret = sks.getEncoded();

						if (null == secret) {
							ToolsKeyChain.writeLoge(TAG,
									new StringBuilder().append("La lectura ha sido vacia").append(alias).toString(), SecurityConstants.allowLog);
						} else {
							secretStr = new String(secret);

						}

					}
				} catch (KeyStoreException ex) {
					ToolsKeyChain.writeLoge(TAG, "Ha fallado al salvar el keystore KEY" + mKeyStoreName, SecurityConstants.allowLog);
					throw new KeyManagerStoreException(ex);
				} catch (NoSuchAlgorithmException ex) {
					ToolsKeyChain.writeLoge(TAG, "Ha fallado al salvar el keystore ALGO" + mKeyStoreName, SecurityConstants.allowLog);
					throw new KeyManagerStoreException(ex);
				} catch (UnrecoverableEntryException ex) {
					ToolsKeyChain.writeLoge(TAG, "Ha fallado al salvar el keystore UR" + mKeyStoreName, SecurityConstants.allowLog);
					throw new KeyManagerStoreException(ex);
				}
			}
		}
	    return secretStr;
	}
	
	/**
	 * Nos permitira almacenar el keystore en un fichero.
	 * @return
	 */
	private  boolean save() {

	    FileOutputStream fos = null;
	    boolean keyStoreSaved = true;
		synchronized (this) {
			try {

				fos = new FileOutputStream(keyStoreFile);
				mKeystore.store(fos, mKeyStorePassword.toCharArray());
			} catch (IOException ex) {
				keyStoreSaved = false;
				ToolsKeyChain.writeLoge(TAG, "Ha fallado al salvar el keystore IO" + mKeyStoreName, SecurityConstants.allowLog);
			} catch (CertificateException ex) {
				ToolsKeyChain.writeLoge(TAG, "Ha fallado al salvar el keystore CER" + mKeyStoreName, SecurityConstants.allowLog);
			} catch (KeyStoreException ex) {
				ToolsKeyChain.writeLoge(TAG, "Ha fallado al salvar el keystore KEY" + mKeyStoreName, SecurityConstants.allowLog);
			} catch (NoSuchAlgorithmException ex) {
				ToolsKeyChain.writeLoge(TAG, "Ha fallado al salvar el keystore ALGO" + mKeyStoreName, SecurityConstants.allowLog);
			} finally {
				if (fos != null) {
					try {
						fos.close();
					} catch (IOException ex) {
						keyStoreSaved = false;
						ToolsKeyChain.writeLoge(TAG, "Fallo al cerrar FileOutputStream", SecurityConstants.allowLog);
					}
				}
			}
		}
	    return keyStoreSaved;
	}
	/**
	 * Cambia la contraseña de cifrado para el keystore
	 * @param mKeyStorePassword
	 */
	protected void setKeyStorePassword(final String mKeyStorePassword) {
		this.mKeyStorePassword = mKeyStorePassword;
	}
	
	
	protected String makeKeyMovil() {
			try {
		final KeyStore keyStoreMovil = KeyStore.getInstance("AndroidKeyStore", "AndroidKeyStore");
		keyStoreMovil.load(null);
		final int nBefore = keyStoreMovil.size();
		// Create the keys if necessary
		if (!keyStoreMovil.containsAlias(ALIAS_BMOVIL)) {

			final Calendar notBefore = Calendar.getInstance();
			final Calendar notAfter = Calendar.getInstance();
			notAfter.add(Calendar.YEAR, 1);
			final KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
					.setAlias(ALIAS_BMOVIL)
					.setSubject(new X500Principal(String.format("CN=%s, OU=%s", ALIAS_BMOVIL, context.getPackageName())))
					.setSerialNumber(BigInteger.ONE)
					.setStartDate(notBefore.getTime())
					.setEndDate(notAfter.getTime())
					.build();
			final KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", "AndroidKeyStore");
			generator.initialize(spec);

			final KeyPair keyPair = generator.generateKeyPair();
			final PrivateKey privateKey = keyPair.getPrivate();
			final PublicKey publicKey = keyPair.getPublic();
			ToolsKeyChain.writeLogd(TAG, "private key = " + privateKey.toString(), SecurityConstants.allowLog);
			ToolsKeyChain.writeLogd(TAG, "public key = " + publicKey.toString(), SecurityConstants.allowLog);
		}

		final int nAfter = keyStoreMovil.size();
		ToolsKeyChain.writeLogd(TAG, "Before = " + nBefore + " After = " + nAfter, SecurityConstants.allowLog);

		// Retrieve the keys
		final KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry) keyStoreMovil.getEntry(ALIAS_BMOVIL, null);
		final RSAPrivateKey privateKey = (RSAPrivateKey) privateKeyEntry.getPrivateKey();
		final RSAPublicKey publicKey = (RSAPublicKey) privateKeyEntry.getCertificate().getPublicKey();

		ToolsKeyChain.writeLogd(TAG, "private key = " + privateKey.toString(), SecurityConstants.allowLog);
		ToolsKeyChain.writeLogd(TAG, "public key = " + publicKey.toString(), SecurityConstants.allowLog);


		return md5(privateKey.toString().substring(130, 230));
	}catch (CertificateException ex){
			ToolsKeyChain.writeLoge(TAG,"Ha fallado al salvar el keystore CER" + mKeyStoreName,SecurityConstants.allowLog);
		} catch (KeyStoreException ex){
			ToolsKeyChain.writeLoge(TAG,"Ha fallado al salvar el keystore KEY" + mKeyStoreName,SecurityConstants.allowLog);
		}catch (NoSuchAlgorithmException ex){
			ToolsKeyChain.writeLoge(TAG,"Ha fallado al salvar el keystore ALGO" + mKeyStoreName,SecurityConstants.allowLog);
		} catch (Exception e) {
			ToolsKeyChain.writeLogeEx(TAG, e.getMessage(),SecurityConstants.allowLog,e);
	    }

		if(Security.getProviders().length==0){
			Security.insertProviderAt(new org.spongycastle.jce.provider.BouncyCastleProvider(), 1);	
		}
		
		return ToolsKey.generateKey(context);
	}
	
	private static final String md5(final String s) {
	    final String MD5 = "MD5";
	    try {
	        // Create MD5 Hash
			final MessageDigest digest = MessageDigest
	                .getInstance(MD5);
	        digest.update(s.getBytes());
			final byte messageDigest[] = digest.digest();

	        // Create Hex String
			final StringBuilder hexString = new StringBuilder();
	        for (final byte aMessageDigest : messageDigest) {
	            String h = Integer.toHexString(0xFF & aMessageDigest);
				StringBuffer cero = new StringBuffer("0");
	            while (h.length() < 2)
	                h = cero.append(h).toString();
	            hexString.append(h);
	            
	        }
	        
	        return hexString.toString();

	    } catch (NoSuchAlgorithmException e) {
			ToolsKeyChain.writeLoge(TAG, e.toString(),SecurityConstants.allowLog);
	    }
	    return "";
	}
	
	
}
