package suitebancomer.aplicaciones.keystore;

import android.content.Context;
import android.os.Build;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

/*import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;
import suitebancomer.aplicaciones.commservice.service.ICommService;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
*/
/**
 * Created by alanmichaelgonzalez on 08/02/16.
 */
public class Monitor {
    /*
    public static void sendData(final Context c,final String aplicationVersion,final DataSend.opcionesReactivacion or,final String estatusApp,final String app){
      try {
          final DataSend ds = Monitor.getInformation(c, aplicationVersion, or, estatusApp, app);
          final Hashtable<String, String> params = new Hashtable<String, String>();
          params.put("marca", ds.getMarca());
          params.put("modelo", ds.getModelo());
          params.put("opcionReactivacion", ds.getOpcionesReactivacion().getStatusCode());
          params.put("numSerie", ds.getNumeroSerie());
          params.put("versionPlataforma", ds.getVersionPlataforma());
          params.put("versionAplication", ds.getVersionBmovil());
          params.put("fecha", ds.getFecha());
          params.put("estatusApp", ds.getEstatusApp());
          params.put("app", ds.getApp());

          new Thread(new Runnable() {
              @Override
              public void run() {
                  try {
                      ParametersTO parameters = new ParametersTO();
                      parameters.setSimulation(ServerCommons.SIMULATION);
                      if (!ServerCommons.SIMULATION) {
                          parameters.setProduction(!ServerCommons.DEVELOPMENT);
                          parameters.setDevelopment(ServerCommons.DEVELOPMENT);
                      }
                      parameters.setOperationId(0);
                      parameters.setParameters(params.clone());
                      parameters.setJson(true);
                      ICommService serverproxy = new CommServiceProxy(c);
                      serverproxy.request(parameters, Object.class);
                  } catch (Exception t) {

                  }
              }
          }).start();
      }catch (Exception e){//cacha cualquier exepcion para que no interfiera con la app

      }
    }


    private static DataSend getInformation(final Context c,final String bmovilVersion,final DataSend.opcionesReactivacion or,final String estatusapp,final String app){
        final DataSend ds=new DataSend();
        ds.setOpcionesReactivacion(or);
        ds.setMarca(Build.BRAND);
        ds.setModelo(Build.MODEL);
        ds.setNumeroSerie(Build.SERIAL);
        ds.setVersionPlataforma(Build.VERSION.RELEASE);
        ds.setVersionBmovil(bmovilVersion);
        ds.setFecha(getDateTime());
        ds.setEstatusApp(estatusapp != null ? estatusapp : "");
        ds.setApp(app);

        return  ds;
    }


    private static String getDateTime() {
        final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        final Date date = new Date();
        return dateFormat.format(date);
    }



    public static DataSend.opcionesReactivacion getOpciontReactivacion(String statusServicio) {
        if(statusServicio.equals(Constants.STATUS_PENDING_ACTIVATION)){
            return   DataSend.opcionesReactivacion.PA;
       } else if(statusServicio.equals(Constants.STATUS_PENDING_SEND)){
           return   DataSend.opcionesReactivacion.PE;
       } else if (statusServicio.equals(Constants.STATUS_WRONG_IUM)){
           return   DataSend.opcionesReactivacion.WI;
       }
        return DataSend.opcionesReactivacion.LG;
    }
*/
}
