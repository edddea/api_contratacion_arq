/**
 * 
 */
package suitebancomer.aplicaciones.keystore;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.security.KeyStoreException;


/**
 * @author IDS Comercial
 *
 */
public final class KeyStoreWrapper {
	
	private static final String USRNAME_KEY = "username";
	private static final String SEED_KEY = "seed";
	private static final String PASSWD_KEY = "keychain.passwd";
	private static final String OTP_KEY = "keychain.otp";
	private static final String CELLPHONE_KEY = "keychain.nphone";
	private static final String KEYSTORE_NAME = "keyChain.bks";
	private static final String DEL_KEY_VALUE = " ";
	//private static final String KEYSTORE_PACKAGE = "keyChain.pcks";
	private static final String KEYSTORE_PATH = Environment.getExternalStorageDirectory() + "/.bancomer";
    private static final String OLD_KEYSTORE_PATH = "//data/data/com.bancomer.mbanking";
	//private static final String KEYSTORE_PATH = "//data/data/com.bancomer.mbanking";
	
	
	private static final String TAG = KeyStoreWrapper.class.getSimpleName();
	
	private static Context context = null;
	private static KeyStoreManager keyStoreManager = null;
	
	/**
	 * Object for singleton synchronization
	 */
	private static final Object LOCK = new Object();
	private static final KeyStoreWrapper INSTANCE = new KeyStoreWrapper();
	private KeyStoreWrapper(){}
	
	public static KeyStoreWrapper getInstance(final Context contexto){
		context = contexto;
        checkKeyStore();
		instanceKeyStoreManager();
		return INSTANCE;
	}
	
	
	private static void instanceKeyStoreManager(){
		synchronized (LOCK) {
			try {
				if (keyStoreManager == null) {
					
					//final File file = new File(_CONTEXT.getFilesDir(), KEYSTORE_NAME );
					
			    	//if(!file.exists()){
			    	//	Runtime.getRuntime().exec("chmod 777 "+ file.getAbsolutePath());
			    	//}
					//final File f = new File(_CONTEXT.getFilesDir().getAbsolutePath());

					final File f = new File(KEYSTORE_PATH);
					keyStoreManager = new KeyStoreManager(f, KEYSTORE_NAME, context);
				}
				
			} catch (KeyStoreException e) {
				if(SecurityConstants.allowLog) Log.e(TAG, e.toString());
			
			} catch (IOException e) {
				if(SecurityConstants.allowLog) Log.e(TAG, e.toString());
			}
		}
	}
	
	public void setUserName(final String value) {
		storeValueForKey(USRNAME_KEY, value);
	}
	
	public void setSeed(final String value) {
		storeValueForKey(SEED_KEY, value);
	}
	
	public void setPasswd(final String value) {
		storeValueForKey(PASSWD_KEY, value);
	}
	
	public void setOtp(final String value) {
		storeValueForKey(OTP_KEY, value);
	}
	
	public void setPhone(final String value) {
		storeValueForKey(CELLPHONE_KEY, value);
	}
	
	public void storeValueForKey(final String key, final String value){
		try {
			
			keyStoreManager.setEntry(key, value);
			synckKeyStore();
		} catch (KeyManagerStoreException e) {
			if(SecurityConstants.allowLog) Log.e(TAG, e.toString());
		}
	}
	
	public String getUserName(){
		return fetchValueForKey(USRNAME_KEY);
	}
	
	public String getSeed(){
		return fetchValueForKey(SEED_KEY);
	}
	
	public String getPasswd(){
		return fetchValueForKey(PASSWD_KEY);
	}
	
	public String getOtp() {
		return fetchValueForKey(OTP_KEY);
	}
	
	public String getPhone() {
		return fetchValueForKey(CELLPHONE_KEY);
	}
	
	public String fetchValueForKey(final String key){
		String value = null;  
		try {
			
			value = keyStoreManager.getEntry(key);
			
		} catch (KeyManagerStoreException e) {
			if(SecurityConstants.allowLog) Log.e(TAG, e.toString());
		}	
		
		return value != null ? value : "";
	}
	
	public void deleteValueForKey(final String key){
		storeValueForKey(key, DEL_KEY_VALUE);
	}

    private static void checkKeyStore() {
        final File oldKeyStore = new File(OLD_KEYSTORE_PATH, KEYSTORE_NAME);
		final File newKeyStore = new File(KEYSTORE_PATH, KEYSTORE_NAME);
		final File carpetakey = new File(KEYSTORE_PATH);
		if(!carpetakey.exists()) {
			carpetakey.mkdir();
		}
        if (oldKeyStore.exists()) {
            FileChannel inChannel = null;
            FileChannel outChannel = null;
            try {
                inChannel = new FileInputStream(oldKeyStore).getChannel();
                outChannel = new FileOutputStream(newKeyStore).getChannel();
                inChannel.transferTo(0, inChannel.size(), outChannel);
                //oldKeyStore.delete();
            } catch (FileNotFoundException e) {
                ToolsKeyChain.writeLoge(KeyStoreWrapper.class.getName(), e.getMessage(), Boolean.TRUE);
            } catch (IOException e) {
                ToolsKeyChain.writeLoge(KeyStoreWrapper.class.getName(), e.getMessage(), Boolean.TRUE);
            } finally {
                try {
                    if (inChannel != null) {
                        inChannel.close();
                    }
                    if (outChannel != null) {
                        outChannel.close();
                    }
                } catch (IOException e) {
                    ToolsKeyChain.writeLoge(KeyStoreWrapper.class.getName(), e.getMessage(), Boolean.TRUE);
                }
            }
        }else if(newKeyStore.exists()){
			/*
				si el viejo key ya no existe en la antigua ubiacion se restaura para que
				las apis no actualizadas encuentren el archivo
			*/
			synckKeyStore();
		}
    }

	/**
	 * Metodo que mantendra actualizado el keychain de la antigua ubicacion.
	 */
	private static void synckKeyStore() {
			final File oldKeyStore = new File(OLD_KEYSTORE_PATH, KEYSTORE_NAME);
			final File newKeyStore = new File(KEYSTORE_PATH, KEYSTORE_NAME);
			FileChannel inChannel = null;
			FileChannel outChannel = null;
			try {
				inChannel = new FileInputStream(newKeyStore).getChannel();
				outChannel = new FileOutputStream(oldKeyStore).getChannel();
				inChannel.transferTo(0, inChannel.size(), outChannel);
				Runtime.getRuntime()
						.exec("chmod 777 /data/data/com.bancomer.mbanking/keyChain.bks");
			} catch (FileNotFoundException e) {
				ToolsKeyChain.writeLoge(KeyStoreWrapper.class.getName(), e.getMessage(), Boolean.TRUE);
			} catch (IOException e) {
				ToolsKeyChain.writeLoge(KeyStoreWrapper.class.getName(), e.getMessage(), Boolean.TRUE);
			} finally {
				try {
					if (inChannel != null) {
						inChannel.close();
					}
					if (outChannel != null) {
						outChannel.close();
					}
				} catch (IOException e) {
					ToolsKeyChain.writeLoge(KeyStoreWrapper.class.getName(), e.getMessage(), Boolean.TRUE);
				}
			}
		}

}
