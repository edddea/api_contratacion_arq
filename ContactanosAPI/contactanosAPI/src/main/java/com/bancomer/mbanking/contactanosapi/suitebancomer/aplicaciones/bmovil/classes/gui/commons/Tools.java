package com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui.commons;

import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by eluna on 10/11/2015.
 */
public final class Tools {

    /**
     * Constructor privado para evitar instanciacion
     */
    private Tools(){}

    public static boolean appInstalled(final String uri, final Context context) {
        final PackageManager pm = context.getPackageManager();
        boolean appInstalled = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            appInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            appInstalled = false;
        }
        return appInstalled;
    }
}
