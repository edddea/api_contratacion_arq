package com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui;

import android.app.Activity;
import com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contactanos.ContactanosViewController;


/**
 * Created by eluna on 11/11/2015.
 */
public class InitContactanos {

    private final Activity context;

    ContactanosViewController contactanosViewController;

    public InitContactanos(final Activity context)
    {
        this.context = context;
    }

    public void iniciarContactanos(){
        contactanosViewController = new ContactanosViewController();
        contactanosViewController.showContactanos(context);
    }
}
