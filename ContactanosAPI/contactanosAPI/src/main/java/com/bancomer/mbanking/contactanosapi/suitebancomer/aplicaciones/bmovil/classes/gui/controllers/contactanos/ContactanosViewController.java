package com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contactanos;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui.commons.Constants;
import com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui.commons.Tools;

import java.util.ArrayList;

import contactanosapi.mbanking.bancomer.com.contactanosapi.R;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

/**
 * Created by eluna on 10/11/2015.
 */
public class ContactanosViewController
{
    public ArrayList<String> estados = new ArrayList<String>();

    public void showContactanos(final Activity ctx) {

        final Dialog contactDialog = new Dialog(ctx);
        final LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.menu_contactanos,null);
        contactDialog.setContentView(layout);
        final Button llamar = (Button) layout.findViewById(R.id.llamar);
        final Button app = (Button) layout.findViewById(R.id.app);
        final Button cancelar = (Button) layout.findViewById(R.id.cancelar);

        contactDialog.setTitle(ctx.getString(R.string.menuSuite_menuTitle));
        app.setHeight(llamar.getHeight());

        contactDialog.setOnKeyListener(new Dialog.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    TrackingHelper.touchAtrasState();
                }
                return false;
            }
        });

        TrackingHelper.trackState("contactanos", estados);

        llamar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        llamarLineaBancomer(ctx);
                        contactDialog.dismiss();
                        ctx.finish();

                    }
                }
        );
        app.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        TrackingHelper.trackState("contactanosTienda", estados);
                        appLineaBancomer(ctx);
                        contactDialog.dismiss();
                    }
                }
        );
        cancelar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        TrackingHelper.touchAtrasState();
                        contactDialog.dismiss();
                    }
                }
        );
        contactDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        contactDialog.show();
    }


    public void llamarLineaBancomer(final Context context) {
        try {
            final Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse(Constants.TEL_URI + context.getString(R.string.menuSuite_callLocalNumber)));
            context.startActivity(callIntent);
        } catch (ActivityNotFoundException e) {
            return;
        }
    }

    private void appLineaBancomer(final Context context) {
        if(Tools.appInstalled(context.getString(R.string.uri_linea_bancomer), context)){
            Intent i = new Intent(Intent.ACTION_MAIN);
            final PackageManager manager = context.getPackageManager();
            i = manager.getLaunchIntentForPackage(context.getString(R.string.uri_linea_bancomer));
            i.setAction(Intent.ACTION_SEND);
            i.addCategory(Intent.CATEGORY_LAUNCHER);
            context.startActivity(i);
            //context.finish();
        }else{
            try{
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.uri_prefix_app_market) + context.getString(R.string.uri_linea_bancomer))));
            }catch(ActivityNotFoundException anfe){
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.uri_prefix_app_market_web) + context.getString(R.string.uri_linea_bancomer))));

            }

        }
    }
}
