package com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui.commons;

/**
 * Created by eluna on 10/11/2015.
 */
public class Constants {
    /**
     * Tel uri
     */
    public final static String TEL_URI = "tel:";
}
