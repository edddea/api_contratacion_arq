/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomer.aplicaciones.bmovil.classes.common;

import android.text.TextUtils;
import android.util.Log;

import bancomer.api.common.commons.Constants;

/**
 * Tools include some utility static methods that can be used by different.
 * classes of the application
 */
public class ToolsCommons {

    /**
     * Hide part of the account number, showing the first digits of the account as
     * an asterisk (*), and only letting visible a certain number of digits. Example:
     * Account 12345678901234567890 would turn into *7890
     *
     * @param account the account number
     * @return a string with the masked account number
     */
    public static String hideAccountNumber(final String account) {
        String result = "";
        if (account != null && account.length() > Constants.VISIBLE_NUMBER_ACCOUNT) {
            final StringBuffer sb = new StringBuffer("*");
            sb.append(account.substring(account.length() - Constants.VISIBLE_NUMBER_ACCOUNT));
            result = sb.toString();
        } else {
            if (account.length() == Constants.VISIBLE_NUMBER_ACCOUNT) {
                result = account;
            }
        }

        return result;
    }

    /**
     * Metodo que escribe un error en el log
     *
     * @param tag     Tag
     * @param mensaje Mensaje
     * @param showLog True si escribe en el log
     */
    public static void writeLoge(final String tag, final String mensaje, final boolean showLog) {
        if (showLog) {
            Log.e(tag, mensaje);
        }
    }

    /**
     * Metodo que escribe info en el log
     *
     * @param tag     Tag
     * @param mensaje Mensaje
     * @param showLog True si escribe en el log
     */
    public static void writeLogi(final String tag, final String mensaje, final boolean showLog) {
        if (showLog) {
            Log.i(tag, mensaje);
        }
    }

    /**
     * Determines if a string is empty or null
     *
     * @param text the string object to evaluate
     * @return true if the string object has no real value
     */
    public static boolean isEmptyOrNull(final String text) {
        return TextUtils.isEmpty(text);
    }

    /**
     * Determines if a object is null
     *
     * @param value
     * @return true if the value is null
     */
    public static boolean isNull(final Object value) {
        return value == null;
    }

}
