package suitebancomer.aplicaciones.bmovil.classes.tracking;

import android.util.Log;

import com.adobe.mobile.Analytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultaEstatusAplicacionDesactivadaViewController;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaEstatusAplicacionDesactivadaDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.LoginDelegate;

public class TrackingHelper {
	public static ArrayList<String> mapa1 = new ArrayList<String>();
	public static ArrayList<String> mapaPrincipal = new ArrayList<String>();

	/*Ubicacion de cada una de las ventanas ANALITIC*/
	public static void trackState(final String state, final ArrayList<String> mapa){
	       HashMap<String, Object> purchaseDictionary = new HashMap<String,Object>();
	       purchaseDictionary.put("channel", "android");
			mapaPrincipal.add(state);

		String listString = "";
		Log.i("Lista",state);
		String menuss= "menu ppal";
		if (menuss.equals(state))
			Log.i("Paso",state);
		/*////////////////////*/
	       for (String s : mapaPrincipal)
	       {
	           listString += ":" + s;

	       }
		Log.i("Valores Obtenidos",listString);

	       String res = (purchaseDictionary.get("channel").toString() + listString);
	       switch (mapaPrincipal.size()) {
	       case 1:
	             purchaseDictionary.put("prop1",res);
			     Log.i("Valores Obtenidos 1",res );
	             break;
	       case 2:
	             purchaseDictionary.put("prop2",res);
			     Log.i("Valores Obtenidos 2", res);
	             break;
	       case 3:
	             purchaseDictionary.put("prop3",res);
			     Log.i("Valores Obtenidos 3", res);
	             break;
	       case 4:
	             purchaseDictionary.put("prop4",res);
			     Log.i("Valores Obtenidos 4", res);
	             break;
	       case 5:
	             purchaseDictionary.put("prop5",res);
			     Log.i("Valores Obtenidos 5", res);
	             break;
	       case 6:
	             purchaseDictionary.put("prop6",res);
			     Log.i("Valores Obtenidos 6", res);
	             break;
	       case 7:
	             purchaseDictionary.put("prop7",res);
			     Log.i("Valores Obtenidos 7", res);
	             break;
	       case 8:
	             purchaseDictionary.put("prop8",res);
			     Log.i("Valores Obtenidos 8", res);
	             break;
	       case 9:
	             purchaseDictionary.put("prop9",res);
			    Log.i("Valores Obtenidos 9", res);
	             break;
	       case 10:
	             purchaseDictionary.put("prop10",res);
			     Log.i("Valores Obtenidos 10", res);
	             break;
	       case 11:
	    	   purchaseDictionary.put("prop11",res);
			   Log.i("Valores Obtenidos", res);
	             break;

	       default:
	             break;
	       }
	       purchaseDictionary.put("appState", res);
	       purchaseDictionary.put("hier1", res);
	       //ARR Encriptacion

//	       if(LoginDelegate.encriptacionEnLogin)
//	       {
//	    	   String nombreUsuario = LoginDelegate.getNombreUsuario();
//		       String nombreUsuarioEncriptado = null;
//		       try
//		       {
//				nombreUsuarioEncriptado = new String(TripleDESEncryptionHelper.encryptText(nombreUsuario));
//		       } catch (Exception e)
//		       {
//				// TODO Auto-generated catch block
//		    	   if(Server.ALLOW_LOG) e.printStackTrace();
//		       }
//
//		       purchaseDictionary.put("eVar15", nombreUsuarioEncriptado);
//		       purchaseDictionary.put("prop15", nombreUsuarioEncriptado);
//		       purchaseDictionary.put("eVar16", "personas");
//		       purchaseDictionary.put("prop16", "personas");
//	       }
//	       else if(ConsultaEstatusAplicacionDesactivadaDelegate.res)
//	       {
//	    	   String nombreUsuario = ConsultaEstatusAplicacionDesactivadaViewController.getNombreUsuario();
//
//	    	   String nombreUsuarioEncriptado = null;
//		       try
//		       {
//				nombreUsuarioEncriptado = new String(TripleDESEncryptionHelper.encryptText(nombreUsuario));
//		       } catch (Exception e)
//		       {
//				// TODO Auto-generated catch block
//		    	   if(Server.ALLOW_LOG) e.printStackTrace();
//		       }
//
//		       purchaseDictionary.put("eVar15", nombreUsuarioEncriptado);
//		       purchaseDictionary.put("prop15", nombreUsuarioEncriptado);
//		       purchaseDictionary.put("eVar16", "personas");
//		       purchaseDictionary.put("prop16", "personas");
//
//	       }
	       Analytics.trackState(res, purchaseDictionary);
	}

	public static void trackState(final String state){
		HashMap<String, Object> purchaseDictionary = new HashMap<String,Object>();
		purchaseDictionary.put("channel", "android");
		mapa1.add(state);
		String listString = "";

		Log.i("Valor",listString);

		for (String s : mapa1)
		{
			listString += ":" + s;
		}
		String res = (purchaseDictionary.get("channel").toString() + listString);

		switch (mapa1.size()) {
			case 1:
				purchaseDictionary.put("prop1",res);
				break;
			case 2:
				purchaseDictionary.put("prop2",res);
				break;
			case 3:
				purchaseDictionary.put("prop3",res);
				break;
			case 4:
				purchaseDictionary.put("prop4",res);
				break;
			case 5:
				purchaseDictionary.put("prop5",res);
				break;
			case 6:
				purchaseDictionary.put("prop6",res);
				break;
			case 7:
				purchaseDictionary.put("prop7",res);
				break;
			case 8:
				purchaseDictionary.put("prop8",res);
				break;
			case 9:
				purchaseDictionary.put("prop9",res);
				break;
			case 10:
				purchaseDictionary.put("prop10",res);
				break;
			case 11:
				purchaseDictionary.put("prop11",res);
				break;

			default:
				break;
		}

		purchaseDictionary.put("appState", res);
		purchaseDictionary.put("hier1", res);
		Analytics.trackState(res,purchaseDictionary);
	}
         /*Se esta dando click*/
	//ARR
	public static void trackClickLogin (final Map<String, Object> contextData){
		Analytics.trackAction("clickLogin", contextData);
	}
		/*Se durmio o se desconecto*/
	//ARR
	public static void trackDesconexiones(final Map<String, Object> contextData){
		Analytics.trackAction("clickDesconexiones", contextData);
	}
	//ARR
	public static void trackMenuPrincipal(final Map<String, Object> contextData){
		Analytics.trackAction("clickMenu", contextData);
	}

	//ARR
	public static void trackFrecuente(final Map<String, Object> contextData){
		Analytics.trackAction("clickFrecuentes", contextData);
	}

	//ARR
	public static void trackInicioOperacion(final Map<String, Object> contextData){
		Analytics.trackAction("inicioOperacion", contextData);
	}

	//ARR
	public static void trackPaso1Operacion(final Map<String, Object> contextData){
		Analytics.trackAction("paso1Operacion", contextData);
	}

	//ARR
	public static void trackPaso2Operacion(final Map<String, Object> contextData){
		Analytics.trackAction("paso2Operacion", contextData);
	}

	//ARR
		public static void trackPaso3Operacion(final Map<String, Object> contextData){
			Analytics.trackAction("paso3Operacion", contextData);
		}

	//ARR
		public static void trackOperacionRealizada(final Map<String, Object> contextData){
			Analytics.trackAction("operacionRealizada", contextData);
		}

	//ARR
		public static void trackInicioConsulta(final Map<String, Object> contextData){
			Analytics.trackAction("inicioConsulta", contextData);
		}

	//ARR
		public static void trackEnvioConfirmacion(final Map<String, Object> contextData){
			Analytics.trackAction("envioConfirmacion", contextData);
		}

		public static void trackConsultaRealizada(final Map<String, Object> contextData){
			Analytics.trackAction("consultaRealizada", contextData);
		}
		//ARR
			public static void trackClickBanner(final Map<String, Object> click_bannerMap,final Map<String, Object> click_bannerMap2) {
				Analytics.trackAction("clickBanner", click_bannerMap);
				// TODO Auto-generated method stub
			}
	public static void touchAtrasState(){
		int ultimo = mapaPrincipal.size()-1;
		String eliminado;
		if(ultimo >= 0)
		{
			String ult = mapaPrincipal.get(ultimo);
			if(ult == "reactivacion" || ult == "activacion" || ult == "menu token" || ult == "contratacion datos" || ult == "activacion datos")
			{
				mapaPrincipal.clear();
			}else
			{
				eliminado = mapaPrincipal.remove(ultimo);
			}
		}
	}

	public static void touchAtrasState(int menos){
		int tamano_total=  mapaPrincipal.size()-1;
		int ultimo = mapaPrincipal.size()-menos;
		String eliminado;
		if(ultimo >= 0)
		{
			String ult = mapaPrincipal.get(ultimo);
			if(ult == "reactivacion" || ult == "activacion" || ult == "menu token" || ult == "contratacion datos" || ult == "activacion datos")
			{
				mapaPrincipal.clear();
			}else
			{   for (int i=tamano_total; i>=ultimo; i-- )
				eliminado = mapaPrincipal.remove(ultimo);
			}
		}
	}

	public static void touchMenu(){
		String eliminado="";
		if (mapaPrincipal.size() == 2){
			eliminado = mapaPrincipal.remove(1);
		}else{
			int tam = mapaPrincipal.size();
			for (int i=1;i<tam;i++){
				eliminado = mapaPrincipal.remove(1);
			}
		}
	}
	public static void trackSimulacionRealizada (Map<String, Object> click_simulacion_realizada) {
		Analytics.trackAction("clickSimulacionRealizada", click_simulacion_realizada);
	}

	public static void trackInicioSimulador (Map<String, Object> click_inicio_simulador) {
		Analytics.trackAction("clickInicioSimulador", click_inicio_simulador);
	}
}