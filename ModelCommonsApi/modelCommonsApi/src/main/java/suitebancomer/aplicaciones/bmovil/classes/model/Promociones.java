package suitebancomer.aplicaciones.bmovil.classes.model;

public class Promociones {
	String cveCamp;
	String desOferta;
	String monto;
	String carrusel;

	public Promociones(){
		//Constructor
	}
	
	public String getCveCamp() {
		return cveCamp;
	}

	public void setCveCamp(final String cveCamp) {
		this.cveCamp = cveCamp;
	}

	public String getDesOferta() {
		return desOferta;
	}

	public void setDesOferta(final String desOferta) {
		this.desOferta = desOferta;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(final String monto) {
		this.monto = monto;
	}

	public String getCarrusel() {
		return carrusel;
	}

	public void setCarrusel(final String carrusel) {
		this.carrusel = carrusel;
	}


	

}
