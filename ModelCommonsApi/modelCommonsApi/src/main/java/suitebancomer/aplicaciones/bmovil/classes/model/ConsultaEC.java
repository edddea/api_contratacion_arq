/*
 * Copyright (c) 2009 BBVA. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.Serializable;
//import suitebancomer.aplicaciones.bmovil.classes.common.Constants;

/**
 * define the parameters for the object ConsultaEC 
 * 
 * @author Carlos Santoyo
 */
public class ConsultaEC implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * The account
     */
    private Account cuentaOrigen = null;

    /**
     * The Periodo
     */
    private String Periodo = null;
    
    private String fechaCorte = "";
    
    private String referencia = "";
    
    private String cadenaAutenticacion = "";
    
    private String cveAcceso = "";
  
    private String codigoNIP = "";
    
    private String codigoOTP = "";
    
    private String codigoOTPReg = "";
    
    private String codigoCVV2 = "";
  
  
    public String getReferencia() {
		return referencia;
	}

	public void setReferencia(final String referencia) {
		this.referencia = referencia;
	}

	public String getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(final String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	/**
     * The eMail
     */
    private String eMail = null;

 
    /**
     * Default constructor
     */
    public ConsultaEC() {
		//Constructor
    }

    /**
     * Constructor with parameters
     * @param numeroCuenta the numeroCuenta
     * @param Periodo the Periodo
     * @param eMail the eMail
     */
    public ConsultaEC(final Account cuentaOrigen, final String Periodo, final String eMail, final String fechaCorte) {
        this.cuentaOrigen = cuentaOrigen;
        this.Periodo = Periodo;
        this.eMail = eMail;
        this.fechaCorte = fechaCorte;
       
    }
    
 

    public String getCadenaAutenticacion() {
		return cadenaAutenticacion;
	}

	public void setCadenaAutenticacion(final String cadenaAutenticacion) {
		this.cadenaAutenticacion = cadenaAutenticacion;
	}

	public String getCveAcceso() {
		return cveAcceso;
	}

	public void setCveAcceso(final String cveAcceso) {
		this.cveAcceso = cveAcceso;
	}

	public String getCodigoNIP() {
		return codigoNIP;
	}

	public void setCodigoNIP(final String codigoNIP) {
		this.codigoNIP = codigoNIP;
	}

	public String getCodigoOTP() {
		return codigoOTP;
	}

	public void setCodigoOTP(final String codigoOTP) {
		this.codigoOTP = codigoOTP;
	}

	public String getCodigoOTPReg() {
		return codigoOTPReg;
	}

	public void setCodigoOTPReg(final String codigoOTPReg) {
		this.codigoOTPReg = codigoOTPReg;
	}

	public String getCodigoCVV2() {
		return codigoCVV2;
	}

	public void setCodigoCVV2(final String codigoCVV2) {
		this.codigoCVV2 = codigoCVV2;
	}

	public void setCuentaOrigen(final Account cuentaOrigen) {
        this.cuentaOrigen = cuentaOrigen;
    }

    public Account getCuentaOrigen() {
		return cuentaOrigen;
	}

	public String getPeriodo() {
		return Periodo;
	}

	public void setPeriodo(final String periodo) {
		Periodo = periodo;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(final String eMail) {
		this.eMail = eMail;
	}


}
