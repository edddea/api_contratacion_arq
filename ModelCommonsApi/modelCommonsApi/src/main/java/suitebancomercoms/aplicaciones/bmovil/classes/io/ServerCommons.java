/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomercoms.aplicaciones.bmovil.classes.io;


import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Server is responsible of acting as an interface between the application
 * business logic and the HTTP operations. It receive the network operation
 * requests and builds a network operation suitable for HttpInvoker class, which
 * eventually will perform the network connection. Server returns the server
 * response as a ServerResponse object, so that the application logic can
 * perform high level checks and processes.
 *
 * @author Stefanini IT Solutions.
 */
public class ServerCommons {

    private final static String NAME_CONFIG_FILE = "appCommConfg.properties";
    private final static String KEY_LOG = "active.log";
    private final static String KEY_SIMULATION = "simulation";
    private final static String KEY_DEVELOPMENT = "development";
    public static boolean ARQSERVICE = false;
    private final static String KEY_ARQSERVICE = "arqservice";
    /**
     * Defines if the application show logs
     */
    public static boolean ALLOW_LOG = false;
    /**
     * Defines if the application is running on the emulator or in the device.
     */
    public static boolean EMULATOR = false;
    /**
     * Indicates simulation usage.
     */
    public static boolean SIMULATION = false;
    /**
     * Indicates if the base URL points to development server or production
     */
    public static boolean DEVELOPMENT = false;
    public static int ID_SENDER_OPERATION = 149;

    public static void loadEnviromentConfig(final Context context) {

        try {
            final AssetManager assetManager = context.getAssets();
            final InputStream inputStream = assetManager.open(NAME_CONFIG_FILE);
            /**
             * Loads properties from the specified InputStream,
             */
            final Properties properties = new Properties();
            properties.load(inputStream);
            String valorString;

            valorString = properties.getProperty(KEY_LOG);

            if (checkValor(valorString)) {
                ServerCommons.ALLOW_LOG = Boolean.valueOf(valorString);
            }
            valorString = properties.getProperty(KEY_SIMULATION);
            if (checkValor(valorString)) {
                ServerCommons.SIMULATION = Boolean.valueOf(valorString);
            }
            valorString = properties.getProperty(KEY_DEVELOPMENT);
            if (checkValor(valorString)) {
                ServerCommons.DEVELOPMENT = Boolean.valueOf(valorString);
            }
            valorString = properties.getProperty(KEY_ARQSERVICE);
            if (checkValor(valorString)) {
                ServerCommons.ARQSERVICE = Boolean.valueOf(valorString);
            }

        } catch (IOException e) {
            if (ServerCommons.ALLOW_LOG) {
                Log.e("ServerCommons", "No se cargo el archivo de configuracion appCommConfg");
            }
        }
    }

    private static boolean checkValor(final String valor) {
        if (valor != null && (valor.equals("true") || valor.equals("false"))) {
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }


}