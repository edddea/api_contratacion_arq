package suitebancomercoms.aplicaciones.bmovil.classes.common;

import android.content.Context;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import bancomer.api.common.commons.R;
import suitebancomer.aplicaciones.bmovil.classes.common.ToolsCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by evaltierrah on 17/11/15.
 */
public class EncripcionHelper {

    /**
     * Contexto de la aplicacion
     */
    public static Context context;
    /**
     * Properties utilizado para la encripcion
     */
    public static Properties configProperties;
    /**
     * Modulus de la llave publica
     */
    protected static String publicModulus;
    /**
     * Exponent de la llave publica
     */
    protected static String publicExponent;


    /**
     * Metodo que carga el archivo properties
     *
     * @return Regresa un Properties
     */
    public static Properties getProperties() {
        final Properties properties = new Properties();
        try {
            final InputStream inputStream = context.getResources().openRawResource(R.raw.confenccom);
            properties.load(inputStream);
        } catch (IOException e) {
            ToolsCommons.writeLoge(Encripcion.class.getSimpleName(), e.getMessage(), ServerCommons.ALLOW_LOG);
        }
        return properties;
    }

    /**
     * Metodo que obtiene las propiedades
     *
     * @param id Identificador de la propiedad
     * @return regresa el valor
     */
    public static String getConfigPropertyValue(final String id) {
        if (ToolsCommons.isNull(configProperties)) {
            loadProperties();
        }
        return configProperties.getProperty(id);
    }

    /**
     * Metodo que incializa el archivo de propiedades
     */
    public static void loadProperties() {
        if (ToolsCommons.isNull(configProperties)) {
            configProperties = getProperties();
            loadParameters();
        }
    }

    /**
     * Carga las variables a utilizar
     */
    private static void loadParameters() {
        publicModulus = getConfigPropertyValue(EncriptarConstants.ID_PUBLIC_MODULUS);
        publicExponent = getConfigPropertyValue(EncriptarConstants.ID_PUBLIC_EXPONENT);
        EncriptarConstants.encriptar = Boolean
                .valueOf(getConfigPropertyValue(EncriptarConstants.ID_ENCRIPTAR));
    }

    /**
     * @param original
     * @return null if fails
     */
    public static String urlencode(final String original) {
        return original.replace("+", "%2B").replace("/", "%2F")
                .replace("=", "%3D");
    }

}
