package suitebancomercoms.aplicaciones.bmovil.classes.common;

/**
 * Created by evaltierrah on 30/09/15.
 */
public class EncriptarConstants {

    /**
     * Identificador Modulus para el properties
     */
    public static final String ID_PUBLIC_MODULUS = "llave.publica.modulus";
    /**
     * Identificador Exponent para el properties
     */
    public static final String ID_PUBLIC_EXPONENT = "llave.publica.exponent";
    /**
     * Identificador Encriptar para el properties
     */
    public static final String ID_ENCRIPTAR = "encriptar.activado";
    /**
     * Indica si se debe o no encriptar los datos sencibles
     */
    public static Boolean encriptar = Boolean.TRUE;
    /**
     * Tipo de instancia para la encripcion Constants
     */
    public static final String TIPO_ENCRIPCION = "RSA";
    /**
     * Tipo instancia Cipher Constants
     */
    public static final String INSTANCIA_CIPHER = "RSA/ECB/PKCS1Padding";
    /**
     * Nombre del archivo Properties Constants
     */
    public static final String CONF_ENC = "ConfEncCom.properties";
    /**
     * Constante que nos indica que valor lleva por defual la variable EN al no estar activada la encripcion
     */
    public static final String EMPTY_ENCRIPTAR = "";
    /**
     * Constante que nos indica que valor lleva por defual la variable EN al no estar activada la encripcion
     */
    public static final String IZQ_ENCRIPTAR = "";
    /**
     * Constante que nos indica que valor lleva por defual la variable EN al no estar activada la encripcion
     */
    public static final String DER_ENCRIPTAR = "";

}
