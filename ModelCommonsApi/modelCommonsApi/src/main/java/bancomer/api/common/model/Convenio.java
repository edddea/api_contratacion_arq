package bancomer.api.common.model;

public class Convenio {

	/**
	 * Nombre del convenio
	 */
	private String nombre;
	
	/**
	 * Imagen del convenio
	 */
	private String nombreImagen;
	
	/**
	 * Convenio
	 */
	private String numeroConvenio;
	
	/**
	 * Orden del convenio
	 */
	private int orden;
	
	/**
	 * Constructor por default
	 * @param nombre nombre del convenio
	 * @param nombreImagen Nombre de la imagen del convenio
	 * @param numeroConvenio Numero que corresponde al convenio
	 * @param orden Orden del convenio
	 */
	public Convenio(final String nombre, final String nombreImagen, final String numeroConvenio, final String orden) {
		this.nombre = nombre;
		this.nombreImagen = nombreImagen;
		this.numeroConvenio = numeroConvenio;
		
		try {
			this.orden = Integer.parseInt(orden);
		} catch (NumberFormatException nfex) {
			this.orden = -1;
		}
	}

	/**
	 * Obtiene el nombre del convenio
	 * @return el nombre del convenio
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Obtiene el nombre de la imagen
	 * @return el nombre de la imagen
	 */
	public String getNombreImagen() {
		return nombreImagen;
	}

	/**
	 * Obtiene el numero del convenio
	 * @return el numero del convenio
	 */
	public String getNumeroConvenio() {
		return numeroConvenio;
	}

	/**
	 * Obtiene el orden del convenio
	 * @return el orden del convenio
	 */
	public int getOrden() {
		return orden;
	}

}
