package bancomer.api.common.model;

import java.io.Serializable;

import bancomer.api.common.commons.Constants.Operacion;

public class OperacionAutenticacion implements Serializable{
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 5255163202591357387L;

	/**
	 * Operation.
	 */
	private Operacion operacion;
	
	/**
	 * Basic profile info.
	 */
	private PerfilAutenticacion credenciales;
		
	/**
	 * Default constructor.
	 */
	public OperacionAutenticacion() {
		this.operacion = null;
		this.credenciales = null;
	}
	
	/**
	 * Constructor, creates a new instance with operation name and null credentials.
	 * @param operacion The operation.
	 */
	public OperacionAutenticacion(final Operacion operacion) {
		this.operacion = operacion;
		this.credenciales = null;
	}

	/**
	 * Constructor, creates a new instance with the specified operation name and credentials.
	 * @param operacion The operation.
	 * @param credenciales Crediantials info.
	 */
	public OperacionAutenticacion(final Operacion operacion, final PerfilAutenticacion credenciales) {
		this.operacion = operacion;
		this.credenciales = credenciales;
	}

	/**
	 * @return the operacion
	 */
	public Operacion getOperacion() {
		return operacion;
	}

	/**
	 * @param operacion the operacion to set
	 */
	public void setOperacion(final Operacion operacion) {
		this.operacion = operacion;
	}

	/**
	 * @return the credenciales
	 */
	public PerfilAutenticacion getCredenciales() {
		return credenciales;
	}

	/**
	 * @param credenciales the credenciales to set
	 */
	public void setCredenciales(final PerfilAutenticacion credenciales) {
		this.credenciales = credenciales;
	}
}
