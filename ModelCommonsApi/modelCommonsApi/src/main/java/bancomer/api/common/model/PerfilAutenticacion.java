package bancomer.api.common.model;

import java.io.Serializable;

import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;

public class PerfilAutenticacion implements Serializable{
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 9129083000871580555L;

	/**
	 * Flag to determine if password is needed.
	 */
	private boolean contrasena;
	
	/**
	 * Flag to determine if token is needed and the type.
	 */
	private TipoOtpAutenticacion token;
	
	/**
	 * Flag to determine if cvv2 is needed.
	 */
	private boolean cvv2;
	
	/**
	 * Flag to determine if nip is needed.
	 */
	private boolean nip;
	
	/**
	 * Flag to determine if softtoken is needed.
	 */
	private boolean registro;
	
	/**
	 * Flag to determine if softtoken is needed.
	 */
	private boolean operar;
	
	/**
	 * Flag to determine if softtoken is needed.
	 */
	private boolean visible;

	/**
	 * Flag to determine if otp is needed.
	 */
	private boolean otp;
	
	/**
	 * Default constructor.
	 */
	public PerfilAutenticacion() {
		this.contrasena = false;
		this.nip = false;
		this.cvv2 = false;
		this.token = TipoOtpAutenticacion.ninguno;
		this.registro = false;
		this.operar = false;
		this.visible = false;
		this.otp = false;
	}
			
	/**
	 * Constructor.
	 * @param contrasena The password flag.
	 * @param nip The NIP flag.
	 * @param cvv2 The CVV2 flag.
	 * @param token The token type.
	 * @param registro The Registro flag.
	 */
	public PerfilAutenticacion(final boolean contrasena, final boolean nip, final boolean cvv2, final TipoOtpAutenticacion token, final boolean registro, final boolean operar, final boolean visible, final boolean otp) {
		this.contrasena = contrasena;
		this.nip = nip;
		this.cvv2 = cvv2;
		this.token = token;
		this.registro = registro;
		this.operar = operar;
		this.visible = visible;
		this.otp = otp;
	}
	
	/**
	 * @return the contrasena.
	 */
	public boolean isContrasena() {
		return contrasena;
	}

	/**
	 * @param contrasena the contrasena to set.
	 */
	public void setContrasena(final boolean contrasena) {
		this.contrasena = contrasena;
	}

	/**
	 * @return the nip.
	 */
	public boolean isNip() {
		return nip;
	}

	/**
	 * @param nip the nip to set.
	 */
	public void setNip(final boolean nip) {
		this.nip = nip;
	}

	/**
	 * @return the cvv2.
	 */
	public boolean isCvv2() {
		return cvv2;
	}

	/**
	 * @param cvv2 the cvv2 to set.
	 */
	public void setCvv2(final boolean cvv2) {
		this.cvv2 = cvv2;
	}

	/**
	 * @return the token type.
	 */
	public TipoOtpAutenticacion getToken() {
		return token;
	}

	/**
	 * @param token the token type to set.
	 */
	public void setToken(final TipoOtpAutenticacion token) {
		this.token = token;
	}

	/**
	 * @return the registro.
	 */
	public boolean isRegistro() {
		return registro;
	}

	/**
	 * @param registro the registro to set.
	 */
	public void setRegistro(final boolean registro) {
		this.registro = registro;
	}
	
	/**
	 * @return the operar
	 */
	public boolean isOperar() {
		return operar;
	}

	/**
	 * @param operar the operar to set
	 */
	public void setOperar(final boolean operar) {
		this.operar = operar;
	}

	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible the visible to set
	 */
	public void setVisible(final boolean visible) {
		this.visible = visible;
	}

	/**
	 * @return otp the otp type to set.
	 *
	 */
	public boolean isOtp(){
		return otp;
	}

	/**
	 * @param otp the otp to set
	 */
	public void setOtp(final boolean otp) {
		this.otp = otp;
	}
	
}

