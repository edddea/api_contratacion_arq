package bancomer.api.common.model;

import android.text.TextUtils;

import bancomer.api.common.commons.Constants;

public class Compania {

	/**
	 * Nombre de la compa�ia
	 */
	private String nombre;
	
	/**
	 * Nombre de la im�gen correspondiente
	 */
	private String nombreImagen;
	
	/**
	 * Clave de la compa�ia
	 */
	private String clave;
	
	/**
	 * Importes v�lidos para se usados por esta compa��a
	 */
	private int[] montosValidos;
	
	/**
	 * Cadena original de montos validos
	 */
	private String cadenaMontos;
	
	/**
	 * Orden de la compania
	 */
	private int orden;
	
	/**
	 * Constructor por default
	 * @param nombre Nombre de la compa�ia
	 * @param imagen Imagen de la compa�ia
	 * @param clave Clave de la compa�ia
	 * @param cadenaMontos Cadena de los montos con los que trabajara esta compania, deben estar intercaladas por comas
	 */
	public Compania(final String nombre, final String imagen, final String clave, final String cadenaMontos, final String orden) {
		this.nombre = nombre;
		this.nombreImagen = imagen;
		this.clave = clave;
		this.cadenaMontos = cadenaMontos;
		this.montosValidos = separarMontos(cadenaMontos);
		try {
			this.orden = Integer.parseInt(orden);
		} catch(NumberFormatException nfex) {
			this.orden = -1;
		}
	}
	
	/**
	 * Obtiene el nombre de la compa�ia
	 * @return el nombre de la compa�ia
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Obtiene el nombre de la imagen de la compa�ia
	 * @return el nombre de la imagen de la compa�ia
	 */
	public String getNombreImagen() {
		return nombreImagen;
	}

	/**
	 * Obtiene la clave de la compa�ia
	 * @return la clave de la compa�ia
	 */
	public String getClave() {
		return clave;
	}

	/**
	 * Obtiene el arreglo con los montos validos de la compa�ia
	 * @return el arreglo con los montos validos de la compa�ia
	 */
	public int[] getMontosValidos() {
		if(null == montosValidos) {
			return null;
		} else {
			return montosValidos.clone();
		}
	}
	
	/**
	 * Obtiene los montos validos de la compa�ia en la cadena original
	 * @return los montos validos de la compa�ia en la cadena original
	 */
	public String getCadenaMontos() {
		return cadenaMontos;
	}

	/**
	 * Obtiene el orden de la compa�ia
	 * @return el orden de la compa�ia
	 */
	public int getOrden() {
		return orden;
	}

	/**
	 * Separa una cadena de montos validos y los convierte en un arreglo de entero, los valores deben estar separados por comas.
	 * Si un caracter no convertible a numero es encontrado, automaticamente le asigna el valor 0
	 * @param cadena La cadena de los montos validos separada por comas
	 * @return Un arreglo de enteros con los montos validos, null si la cadena no es v�lida
	 */
	private int[] separarMontos(final String cadena) {
		if (cadena == null || (cadena.equals("") && cadena.indexOf(",") == -1)) {
			return null;
		}
		final String[] stringMontos = TextUtils.split(cadena, Constants.SEPARADOR_MONTOS_COMPANIA);
		final int stringMontosLength = stringMontos.length;
		int[] intMontos = new int[stringMontosLength];
		for (int i=0; i<stringMontosLength; i++) {
			try {
				intMontos[i] = Integer.parseInt(stringMontos[i]);
			} catch (NumberFormatException nfex) {
				intMontos[i] = 0;
			}
		}
		
		return intMontos;
	}

}
