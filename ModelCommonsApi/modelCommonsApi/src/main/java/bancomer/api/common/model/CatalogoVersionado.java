package bancomer.api.common.model;

import java.util.Vector;

public class CatalogoVersionado {

	private String version;

	private Vector<Object> objetos;
	
	public CatalogoVersionado(final String version) {
		this.version = version;
		objetos = new Vector<Object>();
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setVersion(final String version) {
		this.version = version;
	}

	public void agregarObjeto(final Object obj) {
		objetos.add(obj);
	}
	
	public Vector<Object> getObjetos() {
		return objetos;
	}
	
	public void setObjetos(final Vector<Object> objetos) {
		this.objetos = objetos;
	}
	
	public int getNumeroObjetos() {
		return objetos.size();
	}
}
