/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomer.aplicaciones.bmovil.classes.io.token;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Server is responsible of acting as an interface between the application
 * business logic and the HTTP operations. It receive the network operation
 * requests and builds a network operation suitable for HttpInvoker class, which
 * eventually will perform the network connection. Server returns the server
 * response as a ServerResponse object, so that the application logic can
 * perform high level checks and processes.
 * 
 * @author Stefanini IT Solutions.
 */
public class Server extends ServerCommons {

	/**
	 * Indicates simulation usage.
	 */
	public static final long SIMULATION_RESPONSE_TIME = 5;

	/**
	 * Bancomer Infrastructure.
	 */
	public static final int BANCOMER = 0;

	public static String CODIGO_ESTATUS_APLICACION = "A1";

	public static final int ACTIVATION_STEP1_OPERATION = 1;

	/**
	 * Acivation step 2 operation.
	 */
	public static final int ACTIVATION_STEP2_OPERATION = 2;

	/**
	 * Login oepration.
	 */
	public static final int LOGIN_OPERATION = 3;

	/**
	 * Movements operation.
	 */
	public static final int MOVEMENTS_OPERATION = 4;

	/**
	 * Transfer money operation between the user's accounts.
	 */
	public static final int SELF_TRANSFER_OPERATION = 5;

	/**
	 * Transfer money operation to someone else's account in Bancomer.
	 */
	public static final int BANCOMER_TRANSFER_OPERATION = 6;

	/**
	 * External transfer operation.
	 */
	public static final int EXTERNAL_TRANSFER_OPERATION = 7;

	/**
	 * Nipper store purchase operation.
	 */
	public static final int NIPPER_STORE_PURCHASE_OPERATION = 8;

	/**
	 * Nipper airtime purchase operation.
	 */
	public static final int NIPPER_AIRTIME_PURCHASE_OPERATION = 9;

	/**
	 * Change password operation.
	 */
	public static final int CHANGE_PASSWORD_OPERATION = 10;

	/**
	 * Close session operation.
	 */
	public static final int CLOSE_SESSION_OPERATION = 11;

	/**
	 * Get the owner of a credit card.
	 */
	public static final int CARD_OWNER_OPERATION = 12;

	/**
	 * Calculate fee.
	 */
	public static final int CALCULATE_FEE_OPERATION = 13;

	/**
	 * Calculate fee 2.
	 */
	public static final int CALCULATE_FEE_OPERATION2 = 14;

	/**
	 * Service payment.
	 */
	public static final int SERVICE_PAYMENT_OPERATION = 15;

	/**
	 * Help image for service payment.
	 */
	public static final int HELP_IMAGE_OPERATION = 16;

	/**
	 * Request frequent service payment operation list.
	 */
	public static final int FAVORITE_PAYMENT_OPERATION = 18;

	/**
	 * Fast Payment service.
	 */
	public static final int FAST_PAYMENT_OPERATION = 19;

	/**
	 * Service enterprise name operation.
	 */
	public static final int SERVICE_NAME_OPERATION = 20;

	/**
	 * Alta de operaciones sin tarjeta
	 */
	public static final int ALTA_OPSINTARJETA = 23;

	/**
	 * Consulta de operaciones sin tarjeta
	 */
	public static final int CONSULTA_OPSINTARJETA = 24;

	/**
	 * Baja de operaciones sin tarjeta
	 */
	public static final int BAJA_OPSINTARJETA = 25;

	/**
	 * Consulta status del servicio
	 */
	public static final int CONSULTA_ESTATUSSERVICIO = 26;

	/**
	 * Cambia perfil del cliente
	 */
	public static final int CAMBIA_PERFIL = 27;

	/**
	 * Consulta comision para transferir interbancario
	 */
	public static final int CONSULTAR_COMISION_I = 28;

	/**
	 * Consulta comision para transferir interbancario
	 */
	public static final int CONSULTAR_CODIGO_PAGO_SERVICIOS = 29;

	/**
	 * Preregistro de pago de servicios
	 */
	public static final int PREREGISTRAR_PAGO_SERVICIOS = 30;

	/**
	 * Compra de tiempo aire
	 */
	public static final int COMPRA_TIEMPO_AIRE = 31;

	/**
	 * Alta de frecuente
	 */
	public static final int ALTA_FRECUENTE = 32;

	/**
	 * Consulta del beneficiario
	 */
	public static final int CONSULTA_BENEFICIARIO = 33;

	/**
	 * Baja de frecuente
	 */
	public static final int BAJA_FRECUENTE = 34; // OP144

	/**
	 * Baja de frecuente
	 */
	public static final int CONSULTA_CIE = 35;

	/**
	 * 
	 */
	public static final int ACTUALIZAR_FRECUENTE = 36;

	/**
	 * Actualizacion de preregistrado a Frecuente
	 */
	public static final int ACTUALIZAR_PREREGISTRO_FRECUENTE = 37;

	/**
	 * Cambio de telefono asociado
	 * 
	 */
	public static final int CAMBIO_TELEFONO = 38;

	/**
	 * Cambio de cuenta Asociada
	 */
	public static final int CAMBIO_CUENTA = 39;

	/**
	 * Suspencion Temporal
	 */
	public static final int SUSPENDER_CANCELAR = 40;

	/**
	 * Actualizacion de cuentas del usuario
	 */
	public static final int ACTUALIZAR_CUENTAS = 41;

	public static final int CONSULTA_TARJETA_OPERATION = 42;

	/**
	 * Consulta de limites de operacion
	 */
	public static final int CONSULTAR_LIMITES = 43;

	/**
	 * Cambio de limites de operacion
	 */
	public static final int CAMBIAR_LIMITES = 44;

	/**
	 * Consulta de estatus mantenimiento.
	 */
	public static final int CONSULTA_MANTENIMIENTO = 45;

	/**
	 * Desbloqueo de contraseñas
	 */
	public static final int DESBLOQUEO = 46;

	/**
	 * Quitar suspension
	 */
	public static final int QUITAR_SUSPENSION = 47;

	/**
	 * Reactivacion
	 */
	public static final int OP_ACTIVACION = 48;

	/**
	 * Operación de contratación final para bmovil.
	 */
	public static final int OP_CONTRATACION_BMOVIL_ALERTAS = 49;

	/**
	 * Operación de consulta de terminos y condiciones de uso.
	 */
	public static final int OP_CONSULTAR_TERMINOS = 50;

	public static final int OP_ENVIO_CLAVE_ACTIVACION = 51;

	public static final int OP_CONFIGURAR_CORREO = 52;

	public static final int OP_ENVIO_CORREO = 53;

	public static final int OP_VALIDAR_CREDENCIALES = 54;

	public static final int OP_FINALIZAR_CONTRATACION_ALERTAS = 55;

	/** Activacion softtoken - Consulta de tipo de solicitud. */
	public static final int CONSULTA_TARJETA_ST = 56;

	public static final int CONSULTA_COORDENADAS = 150;

	public static final int CONSULTA_AUTENTICACIONTOKENPM = 151;

	public static final int SINCRONIZAR_TOKENPM = 152;

	public static final int EXPORTACION_SOFTTOKENPM = 153;

	/** Activacion softtoken - Autenticacion */
	public static final int AUTENTICACION_ST = 57;

	/**
	 * Activacion softtoken
	 */
	public static final int EXPORTACION_SOFTTOKEN = 58;
	/**
	 * Activacion softtoken
	 */
	public static final int SINCRONIZACION_SOFTTOKEN = 59;

	/** Activacion softtoken - Contratacion enrolamiento. */
	public static final int CONTRATACION_ENROLAMIENTO_ST = 60;

	/** Activacion softtoken - Finalizar contratacion. */
	public static final int FINALIZAR_CONTRATACION_ST = 61;

	/** Activacion softtoken - Cambio telefono asociado. */
	public static final int CAMBIO_TELEFONO_ASOCIADO_ST = 62;

	/** Activacion softtoken - Solicitud. */
	public static final int SOLICITUD_ST = 63;

	/** Mantenimiento Alertas. */
	public static final int MANTENIMIENTO_ALERTAS = 64;

	/** Consulta de Terminos y Condiciones Sesion */

	public static final int OP_CONSULTAR_TERMINOS_SESION = 65;

	/** Solicitar Alertas */

	public static final int OP_SOLICITAR_ALERTAS = 66;

	// Empieza codigo de SPEI revisar donde se usan las constantes
	/* Identifier for the request spei accounts operation. */
	public static final int SPEI_ACCOUNTS_REQUEST = 67;// 66

	/**
	 * Identifier for the request spei terms and conditions.
	 */
	public static final int SPEI_TERMS_REQUEST = 68; // 67

	/**
	 * The SPEI maintenance operation.
	 */
	public static final int SPEI_MAINTENANCE = 69;// 68

	/**
	 * The request beneficiary account number operation.
	 */
	public static final int CONSULT_BENEFICIARY_ACCOUNT_NUMBER = 70;// 69
	// Termina Codigo de SPEI
	/** Consulta Interbancarios */

	// One CLick

	public static final int CONSULTA_DETALLE_OFERTA = 71;// 67;//66
	public static final int ACEPTACION_OFERTA = 72;// 68;//67
	public static final int EXITO_OFERTA = 73;// 69;//68
	public static final int RECHAZO_OFERTA = 74;// 70;//69

	public static final int CONSULTA_DETALLE_OFERTA_EFI = 75;// 71;//70
	public static final int ACEPTACION_OFERTA_EFI = 76;// 71
	public static final int EXITO_OFERTA_EFI = 77;// 72
	public static final int SIMULADOR_EFI = 78;// 73

	public static final int CONSULTA_DETALLE_OFERTA_CONSUMO = 79;
	public static final int POLIZA_OFERTA_CONSUMO = 80;
	public static final int TERMINOS_OFERTA_CONSUMO = 81;
	public static final int EXITO_OFERTA_CONSUMO = 82;
	public static final int DOMICILIACION_OFERTA_CONSUMO = 83;
	public static final int CONTRATO_OFERTA_CONSUMO = 84;
	public static final int RECHAZO_OFERTA_CONSUMO = 85;
	public static final int SMS_OFERTA_CONSUMO = 86;
	public static final int OP_CONSULTA_INTERBANCARIOS = 87;

	public static final int FAVORITE_PAYMENT_OPERATION_BBVA = 88;

	// Depositos Movil
	public static final int CONSULTA_DEPOSITOS_CHEQUES = 89;// 87
	public static final int CONSULTA_DEPOSITOS_EFECTIVO = 90; // Ya no se usa 88
	public static final int CONSULTA_PAGO_SERVICIOS = 91;// 89
	public static final int CONSULTA_TRANSFERENCIAS_CUENTA_BBVA = 92;// 90
	public static final int CONSULTA_TRANSFERENCIAS_MIS_CUENTAS = 93;// 91
	public static final int CONSULTA_TRANSFERENCIAS_A_OTROS_BANCOS = 94;// 92
	public static final int CONSULTA_TRANSFERENCIAS_CLIENTES_BANCOMER = 95; // Ya
																			// no
																			// se
																			// usa
																			// 93
	public static final int CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS = 96;// 94
	public static final int CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO = 97;// 95
	public static final int CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO_DETALLE = 98;// 96
	public static final int CONSULTA_DEPOSITOS_CHEQUES_DETALLE = 99;// 97

	public static final int OP_CONSULTA_TDC = 100;

	public static final int OP_RETIRO_SIN_TAR = 101;
	public static final int CONSULTA_SIN_TARJETA = 102;

	public static final int OP_RETIRO_SIN_TAR_12_DIGITOS = 103;

	public static final int OP_SINC_EXP_TOKEN = 104;

	public static final String USERNAME_PARAM = "username";

	public static final String PASSWORD_PARAM = "password";

	public static final String CARDNUMBER_PARAM = "cardowner";

	public static final String USER_ID_PARAM = "userid";

	public static final String MARCA = "marca";
	public static final String MODELO = "modelo";
	public static final String VA_PARAM = "InstruccionValidacion";
	public static final String REFERENCIA_NUMERICA_PARAM = "ReferenciaNumeria";
	public static final String NIP_PARAM = "Nip";
	public static final String CVV2_PARAM = "cvv2";
	public static final String C1_PARAM = "C1";
	public static final String CN_PARAM = "CN";
	public static final String DE_PARAM = "DE";
	public static final String C2_PARAM = "C2";
	public static final String C3_PARAM = "C3";
	public static final String AUTH_CAT_PARAM = "authenticationVersion";
	public static final String COMPANIES_PARAM = "companiesVersion";

	public static final String ALERTSTATUS_PARAM = "estadoAlertas";

	public static final String OTP1 = "otp_gene1";
	public static final String OTP2 = "otp_gene2";
	public static final String NOMBRE_TOKEN = "nombreToken";
	public static final String NUMERO_CLIENTE = "Numerocliente";
	public static final String COMPANIA_TELEFONICA = "Compañiatelefonica";
	public static final String TIPO_SOLICITUD_ST = "tiposolicitudst";
}