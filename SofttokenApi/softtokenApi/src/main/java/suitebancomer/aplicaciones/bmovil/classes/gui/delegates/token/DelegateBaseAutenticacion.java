package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.token;

import android.content.Context;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
//import suitebancomercoms.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
//import suitebancomercoms.aplicaciones.bmovil.classes.gui.controllers.ContratacionAutenticacionViewController;

public class DelegateBaseAutenticacion extends DelegateBaseOperacion {
	
	private final Context ctxt = SuiteAppApi.appContext;

	public String getEtiquetaCampoCVV() { return ""; }
	
	public boolean mostrarCVV() { return false; }
	
	public String getTextoAyudaCVV() { return SuiteAppApi.appContext.getString(R.string.confirmation_CVV_ayuda); };

	@Override
	protected int getImagenBotonResultados() {
		return R.drawable.btn_menu;
	}
	
	/**
	 * Define el texto de ayuda para los instrumentos de seguridad 
	 */
	@Override
	public String getTextoAyudaInstrumentoSeguridad(final TipoInstrumento tipoInstrumento) {
		final TipoOtpAutenticacion tokenAMostrar = tokenAMostrar();
		if (tokenAMostrar == TipoOtpAutenticacion.ninguno) {
			return "";
		} else if (tokenAMostrar == TipoOtpAutenticacion.registro) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (SuiteAppApi.getSofttokenStatusApi()) {
						return ctxt.getString(R.string.confirmation_ayudaRegistroSofttokenActivado);
					} else {
						return ctxt.getString(R.string.confirmation_ayudaRegistroSofttokenDesactivado);
					}
				case OCRA:
					return ctxt.getString(R.string.confirmation_ayudaRegistroOCRA);
				case DP270:
					return ctxt.getString(R.string.confirmation_ayudaRegistroDP270);
				case sinInstrumento:
				default:
					return "";
			}
		} else if (tokenAMostrar == TipoOtpAutenticacion.codigo) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (SuiteAppApi.getSofttokenStatusApi()) {
						return ctxt.getString(R.string.confirmation_ayudaCodigoSofttokenActivado);
					} else {
						return ctxt.getString(R.string.confirmation_ayudaCodigoSofttokenDesactivado);
					}					
				case OCRA:
					return ctxt.getString(R.string.confirmation_ayudaCodigoOCRA);
				case DP270:
					return ctxt.getString(R.string.confirmation_ayudaCodigoDP270);
				case sinInstrumento:
				default:
					return "";
			}
		}
		return "";
	}
	
	@Override
	public String getEtiquetaCampoNip() {		
		return ctxt.getString(R.string.confirmation_nip);
	}
	
	@Override
	public String getTextoAyudaNIP() {
		return ctxt.getString(R.string.confirmation_autenticacion_ayudaNip);
	}
	
	@Override
	public String getEtiquetaCampoOCRA() {
		return ctxt.getString(R.string.confirmation_ocra);
	}
	
	@Override
	public String getEtiquetaCampoDP270() {
		return ctxt.getString(R.string.confirmation_dp270);
	}
}
