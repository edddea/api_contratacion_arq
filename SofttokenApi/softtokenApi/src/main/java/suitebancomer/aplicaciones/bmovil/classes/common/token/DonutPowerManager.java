/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.common.token;

/**
 * Implementation of the App power manager for apis 4 to 6.
 */
public final class DonutPowerManager extends AbstractSuitePowerManager {

	/* (non-Javadoc)
	 * @see suitebancomercoms.aplicaciones.bmovil.classes.common.token.AbstractSuitePowerManager#isScreenOn()
	 */
	@Override
	public boolean isScreenOn() {
		return false;
	}

}
