package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.token;

import java.util.Hashtable;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.SessionStoredListener;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.token.BmovilViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaTarjetaContratacionData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Contratacion;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ContratacionBmovilData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.RespuestaConfirmacion;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomer.classes.gui.controllers.token.BaseViewController;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;
import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

/**
 * Delegado para la contratacion de Bmovil.
 */
public class ContratacionDelegate extends DelegateBaseAutenticacion implements
		SessionStoredListener {
	/**
	 * Enumeraci�n con los pasos del flujo de contratación.
	 */
	public enum AvanceContratacion {
		IngresarNumTarjeta, IngresarPassword, TokenUsuario
	}

	// #region Variables.
	/**
	 * Identificador �nico del delegado.
	 */
	public static final long CONTRATACION_DELEGATE_ID = 0x68b4fd27c67e239dL;

	/**
	 * Email matching pattern regex.
	 */
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/**
	 * Codigos de operacion a ejecutar dependiendo del escenario alterno de
	 * Contratacion
	 */
	private static final String TIPO_PERSONA_NO_PERMITIDO = "EA16";

	
	/**
	 * Estado Alerta
	 */

	private static final String TEXTO_ESTADO_ALERTA = "Alertas";//"Estado Alerta";

	/**
	 * Tipo de operación actual.
	 */
	private Operacion tipoOperacion;

	/**
	 * Controlador actual.
	 */
	private BaseViewController ownerController;

	/**
	 * Avance del proceso de contratacion.
	 */
	private AvanceContratacion paso;

	/**
	 * Modelo con datos básicos de contratación.
	 */

	private Contratacion contratacion;
	/**
	 * Referencia a la consulta de estatus de la aplicación.
	 */
	private ConsultaEstatus consultaEstatus;

	/**
	 * Respuesta del servidor para la operación de consultar tarjeta.
	 */
	private ConsultaTarjetaContratacionData consultaTarjetaResponse;

	/**
	 * Respuesta del servidor para la operación de Contratación de Bmovil.
	 */
	private ContratacionBmovilData contratacionBmovilResponse;

	/**
	 * Bandera para indicar si se debe de borrar los datos de sesion.
	 */
	private boolean deleteData;

	/**
	 * Parametros de respuesta de la pantalla de confirmaci�n.
	 */
	private RespuestaConfirmacion respuestaConfirmacion;

	/**
	 * Bandera para decidir que logica realizar tras confirmar en la pantalla
	 * Definicion contrasennas.
	 */
	private boolean escenarioAlternativoEA11 = false;

	// #endregion

	// #region Setters y Getters
	/**
	 * @param ownerController
	 *            El controlador actual a establecer.
	 */
	public void setOwnerController(final BaseViewControllerCommons ownerController) {
		this.ownerController = (BaseViewController)ownerController;
	}

	/**
	 * @param consultaEstatus
	 *            Referencia a la consulta de estatus de la aplicación.
	 */
	public void setConsultaEstatus(final ConsultaEstatus consultaEstatus) {
		this.consultaEstatus = consultaEstatus;
		cargarDatosDeConsultaEstatus();
	}

	/**
	 * @param deleteData
	 *            Bandera para indicar si se debe de borrar los datos de sesion.
	 */
	public void setDeleteData(final boolean deleteData) {
		this.deleteData = deleteData;
	}
	
	/**
	 * @param consultaEstatus
	 *            Referencia a la consulta de estatus de la aplicación.
	 */
	public ConsultaEstatus getConsultaEstatus() {
		return this.consultaEstatus ;
	}

	/**
	 * @param deleteData
	 *            Bandera para indicar si se debe de borrar los datos de sesion.
	 */
	public boolean getDeleteData() {
		return this.deleteData;
	}

	/**
	 * Constructor por defecto.
	 */
	public ContratacionDelegate() {
		super();

		tipoOperacion = Operacion.contratacion;
		paso = AvanceContratacion.IngresarNumTarjeta;
		contratacion = new Contratacion();
		consultaEstatus = null;
		consultaTarjetaResponse = null;
		contratacionBmovilResponse = null;
		ownerController = null;
		deleteData = false;
	}

	// #region Copiado de datos.
	/**
	 * Copia los datos necesarios del objeto "consulta estatus" hacia el objeto
	 * "contratación".
	 * 
	 * @throws NullPointerException
	 *             Si alguno de los dos objetos es nulo.
	 */
	public void cargarDatosDeConsultaEstatus() throws NullPointerException {
		if (null == consultaEstatus)
			throw new NullPointerException(
					"Consulta estatus no puede ser nulo.");
		if (null == contratacion)
			throw new NullPointerException("Contratacion no puede ser nulo.");

		contratacion.setPerfil(consultaEstatus.getPerfil());
		contratacion.setNumCelular(consultaEstatus.getNumCelular());
		contratacion.setCompaniaCelular(consultaEstatus.getCompaniaCelular());
		contratacion.setEmailCliente(consultaEstatus.getEmailCliente());
		contratacion.setTipoInstrumento(consultaEstatus.getInstrumento());
		contratacion.setEstatusInstrumento(consultaEstatus.getEstatusInstrumento());
		
	}

	/**
	 * Copia los datos necesarios del objeto "consulta tarjeta" hacia el objeto
	 * "contratación".
	 * 
	 * @throws NullPointerException
	 *             Si alguno de los dos objetos es nulo.
	 */
	public void cargarDatosDeConsultaTarjeta() throws NullPointerException {
		if (null == consultaTarjetaResponse)
			throw new NullPointerException(
					"Consulta Tarjeta no puede ser nulo.");
		if (null == contratacion)
			throw new NullPointerException("Contratacion no puede ser nulo.");

		contratacion.setNumCelular(consultaEstatus.getNumCelular());
		contratacion.setEmailCliente(consultaEstatus.getEmailCliente());
		contratacion.setNumeroCuenta(consultaTarjetaResponse.getNumeroCuenta());
		contratacion.setTipoInstrumento(consultaTarjetaResponse.getTipoInstrumento());
		contratacion.setEstatusInstrumento(consultaTarjetaResponse.getEstatusInstrumento());
		contratacion.setFechaContratacion(consultaTarjetaResponse.getFechaContratacion());
		contratacion.setFechaModificacion(consultaTarjetaResponse.getFechaModificacion());
	}

	// #endregion

	// #region Network
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.classes.gui.delegates.BaseDelegate#doNetworkOperation(int,
	 * java.util.Hashtable,
	 * suitebancomer.classes.gui.controllers.BaseViewController)
	 */
	@Override
	public void doNetworkOperation(final int operationId, final Hashtable<String,?> params, final boolean isJson, final ParsingHandler handler,final BaseViewControllerCommons caller){
		if (ownerController != null)
			if(ownerController.getParentViewsController() instanceof BmovilViewsController){
			/*	((BmovilViewsController) ownerController.getParentViewsController())
					.getBmovilApp().invokeNetworkOperation(operationId, params,
							caller);*/
			}else if(ownerController.getParentViewsController() instanceof SofttokenViewsController){
				 SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().invokeNetworkOperation(operationId, params,true,handler, caller, false);
			}
			
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.classes.gui.delegates.BaseDelegate#analyzeResponse(int,
	 * suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		if (ServerResponse.OPERATION_SUCCESSFUL == response.getStatus()) {
			switch (operationId) {

			case Server.CONSULTA_TARJETA_OPERATION:
				direccionarFlujoConsultaTarjeta(response);
				break;
			case Server.OP_CONTRATACION_BMOVIL_ALERTAS:
			case Server.OP_FINALIZAR_CONTRATACION_ALERTAS:
				if (!Tools.isFirstActivationStored()){
					Tools.storeFirstActivationDate();
				}

				Session.getInstance(SuiteAppApi.appContext).setUsername(contratacion.getNumCelular());
				Session.getInstance(SuiteAppApi.appContext).saveRecordStore();

				if (Server.SIMULATION){
					Server.CODIGO_ESTATUS_APLICACION = "PA";
				}

				ownerController.showInformationAlert(R.string.bmovil_contratacion_mensaje_exito,
						new OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog,
									final int which) {
								SuiteAppApi.getInstanceApi().getSuiteViewsControllerApi().showMenuSuite(true, new String[] { "bmovilselected" });
							}
						});
				break;
			case Server.CONSULTA_TARJETA_ST:
				this.setEscenarioAlternativoEA11(false);
				cambiarDeBMovilTokenMovil().showActivacionSTEA11(response);
				// mostrarConfirmacionST();
				break;
				
			case Server.CONTRATACION_ENROLAMIENTO_ST:
				final int status = response.getStatus();
				if (ServerResponse.OPERATION_SUCCESSFUL == status) {
					mostrarActivacionST();
				}							
				break;
			case Server.SOLICITUD_ST:
				((ContratacionSTDelegate) SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController()
						.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID)).analyzeResponse(Server.SOLICITUD_ST, response);
			default:
				break;
			}
		} else {
			SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getBmovilViewsController().getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
		}
	}

	/*
	 * 
	 * 
	 * INICIO >>>>>>>>>>>>>>>>>> FUNCIONES -> P026 - CONTRATACION
	 */
	/**
	 * Maneja el flujo de ejecucion de la operación Consulta Tarjeta
	 * 
	 * @param response
	 *            Respuesta del servidor
	 */
	public void direccionarFlujoConsultaTarjeta(final ServerResponse response) {
		consultaTarjetaResponse = (ConsultaTarjetaContratacionData) response.getResponse();
		
		//Resoluci�n incidencia #21961
		cargarDatosDeConsultaTarjeta();	
		
		//Resolucion incidencia #21977
		//Para que en la pantalla contratacionAutenticacion pida el token
		//debe de setearse en session.
		Session.getInstance(SuiteAppApi.appContext).setSecurityInstrument(consultaTarjetaResponse.getTipoInstrumento());
		
		if(Server.ALLOW_LOG) Log.i("ValidacionAlertas", consultaTarjetaResponse.getValidacionAlertas());

		contratarBancomerMovil();
	}
	
	/**
	 * Se direcciona el flujo en funcion de la respuesta a consultaTarjetaContratacionE
	 */
	private void contratarBancomerMovil() {
        final Perfil perfil = contratacion.getPerfil();
        if (!determinarTokenActivo()) {
            if (validacionAlertas01()) {
                contratacion.setPerfil(Perfil.avanzado);//hcf

                // CGI-Modif Contratacion
//				if (Constants.Perfil.basico.equals(perfil) || Constants.Perfil.recortado.equals(perfil)) {
//					// EA#8
//					cargarDatosDeConsultaTarjeta();
//					mostrarDefinirPassword();
//				} else {
//				if(Perfil.avanzado.equals(perfil)){
                if (!validacionPersonaNoPermitida()) {
                    // EA#11
                    cargarDatosDeConsultaTarjeta();
                    mostrarDefinirPassword();
                    contratacion.setPerfil(Perfil.basico);
                    this.setEscenarioAlternativoEA11(true);
                } else {
                    // EA#16 (persona no permitida)
                    mostrarAlertAceptar(ownerController.getString(R.string.contratacion_validacion_personas_s),TIPO_PERSONA_NO_PERMITIDO);
                }
                //}
            } else if (validacionAlertas02()) {

                //if(Perfil.basico.equals(perfil)){//hcf original
//				if(Constants.Perfil.avanzado.equals(perfil)){//hcf original
//				if(Constants.Perfil.recortado.equals(perfil)){//hcf
//				contratacion.setPerfil(Perfil.recortado);//hcf
                // EA#15
                ownerController.showInformationAlert(ownerController.getString(R.string.label_information), ownerController.getString(R.string.contratacion_validacion02s), new OnClickListener() {
                            //EA#22
                            @Override
                            public void onClick(final DialogInterface dialog, final int which) {
                                ownerController.getParentViewsController().removeDelegateFromHashMap(CONTRATACION_DELEGATE_ID);
                                SuiteAppApi.getInstanceApi().getSuiteViewsControllerApi().showMenuSuite(true, new String[]{"bmovilselected"});
                                //SuiteApp.getInstance().getSuiteViewsController().showMenuSuite(true, new String[]{"bmovilselected"});
                            }
                        }
                );
					/*ownerController.showYesNoAlert(
							ownerController.getString(R.string.label_information), ownerController.getString(R.string.contratacion_validacion02),
							ownerController.getString(R.string.common_alert_yesno_positive_button),
							ownerController.getString(R.string.common_alert_yesno_negative_button),
							new OnClickListener() {
								@Override
								public void onClick(final DialogInterface dialog, final int which) {
									contratacion.setPerfil(Perfil.recortado);
									cargarDatosDeConsultaTarjeta();
									mostrarDefinirPassword();
								}
							}, new OnClickListener() {
								//EA#22
								@Override
								public void onClick(final DialogInterface dialog, final int which) {
									ownerController.getParentViewsController().removeDelegateFromHashMap(CONTRATACION_DELEGATE_ID);
									SuiteAppContratacion.getInstance().getSuiteViewsController().setCurrentActivityApp(ownerController);
									SuiteAppContratacion.getInstance().getSuiteViewsController().showViewController(SuiteAppContratacion.getConsultaEstatusDesactivada().getClass());
									//SuiteAppContratacion.getInstance().getSuiteViewsController().showMenuSuite(true, new String[] {"bmovilselected"});
								}
							}
					);*/

                //}
                // CGI-Modif Contratacion
				/*else{
					// EA#23
					contratacion.setPerfil(Perfil.recortado);
					cargarDatosDeConsultaTarjeta();
					mostrarDefinirPassword();
				}*/
            } else if (validacionAlertas0304()) {
                // EA#10
                mostrarAlertInformacionYVoyAppDesactivada(ownerController.getString(R.string.contratacion_validacion0203));
            }
        } else {
            if (!validacionPersonaNoPermitida()) {
                //EA01
                cargarDatosDeConsultaTarjeta();

                if (validacionAlertas01()) {
                    contratacion.setPerfil(Perfil.avanzado);//hcf
                    // EP

                    mostrarDefinirPassword();

                } else if (validacionAlertas02()) {
//                    contratacion.setPerfil(Perfil.avanzado);//hcf original CGI
                    contratacion.setPerfil(Perfil.recortado);//hcf
                    // CGI-Modif Contratacion
					/*if (Constants.Perfil.basico.equals(perfil) || Constants.Perfil.recortado.equals(perfil)) {
						// EA#19
						mostrarAlertInformacionYVoyAppDesactivada(ownerController.getString(R.string.contratacion_validacion02_token_new));
					} else {*/
                    // EA#12, EA#14 (cancelar)
                    //contratacion.setEstatusAlertas(Constants.OPERACION_ALERTAS_CONTRATAR);
                    mostrarAlertAceptar(ownerController.getString(R.string.contratacion_validacion02_token_new), Constants.MANTENIMIENTO_ALERTAS_CONTRATAR);
//					}

                } else if (validacionAlertas0304()) {
                    // CGI-Modif Contratacion
					/*if (Constants.Perfil.basico.equals(perfil) || Constants.Perfil.recortado.equals(perfil)) {
						// EA#20
						mostrarAlertInformacionYVoyAppDesactivada(ownerController.getString(R.string.contratacion_validacion0304_token_new));
					}else{*/
                    // EA#13, EA#14 (cancelar)
                    //contratacion.setEstatusAlertas(Constants.OPERACION_ALERTAS_ACTUALIZAR);
                    //mostrarAlertSiNo(ownerController.getString(R.string.contratacion_validacion0304_token), Constants.MANTENIMIENTO_ALERTAS_ACTUALIZAR);
                    mostrarAlertAceptar(ownerController.getString(R.string.contratacion_validacion0304_token_new), Constants.MANTENIMIENTO_ALERTAS_CONTRATAR);
//					}

                }
            } else {
                // EA#16 (persona no permitida)
                mostrarAlertAceptar(ownerController.getString(R.string.contratacion_validacion_personas_s), TIPO_PERSONA_NO_PERMITIDO);
            }
        }
	}


	/**
	 * Muestra una alerta por pantalla // Aceptar
	 * 
	 * @param mensaje
	 */
	private void mostrarAlertAceptar(final String mensaje, final String operacion) {
		// ownerController.ocultaIndicadorActividad();
		ownerController.setHabilitado(true);
		if (null == ownerController)
			return;

		ownerController.showInformationAlert(
						ownerController.getString(R.string.label_information), mensaje,
						ownerController.getString(R.string.common_alert_yesno_positive_button),
						new OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog, final int which) {
								// dialog.dismiss();
								if (Constants.MANTENIMIENTO_ALERTAS_CONTRATAR.equals(operacion)) {
									//contratacion.setEstatusAlertas(Constants.OPERACION_ALERTAS_CONTRATAR);
									//mostrarDefinirPassword();
									
								//} else if (Constants.MANTENIMIENTO_ALERTAS_ACTUALIZAR.equals(operacion)) {								
								//	contratacion.setEstatusAlertas(Constants.OPERACION_ALERTAS_ACTUALIZAR);
								//	mostrarDefinirPassword();

									SuiteAppApi.getInstanceApi().getSuiteViewsControllerApi()
									.showMenuSuite(true,new String[] { "bmovilselected" });

								}
                                //else if (TIPO_PERSONA_NO_PERMITIDO.equals(operacion)) {
								//	contratacion.setPerfil(Perfil.basico);
								//	cargarDatosDeConsultaTarjeta();
								//	contratarBancomerMovil();
							//	}
							}
						}
	);
	}

	/**
	 * Muestra una alerta por pantalla // Aceptar y cancelar
	 * 
	 * @param mensaje
	 */
	private void mostrarAlertSiNo(final String mensaje, final String operacion) {
		// ownerController.ocultaIndicadorActividad();
		ownerController.setHabilitado(true);
		if (null == ownerController)
			return;

		ownerController.showYesNoAlert(
						ownerController.getString(R.string.label_information), mensaje,
						ownerController.getString(R.string.common_alert_yesno_positive_button),
						ownerController.getString(R.string.common_alert_yesno_negative_button),
						new OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog, final int which) {
								// dialog.dismiss();
								if (Constants.MANTENIMIENTO_ALERTAS_CONTRATAR.equals(operacion)) {
									//contratacion.setEstatusAlertas(Constants.OPERACION_ALERTAS_CONTRATAR);
									//mostrarDefinirPassword();
									
								//} else if (Constants.MANTENIMIENTO_ALERTAS_ACTUALIZAR.equals(operacion)) {								
								//	contratacion.setEstatusAlertas(Constants.OPERACION_ALERTAS_ACTUALIZAR);
								//	mostrarDefinirPassword();
									
									SuiteAppApi.getInstanceApi().getSuiteViewsControllerApi()
									.showMenuSuite(true,new String[] { "bmovilselected" });
									
								} else if (TIPO_PERSONA_NO_PERMITIDO.equals(operacion)) {
									contratacion.setPerfil(Perfil.basico);
									cargarDatosDeConsultaTarjeta();
									contratarBancomerMovil();
								}
							}
						},null
				);
	}
	
	/**
	 * Muestra una alerta por pantalla con Aceptar y ejecuta Aplicación Desactivada.
	 * 
	 * @param mensaje
	 */
	private void mostrarAlertInformacionYVoyAppDesactivada(final String mensaje){
		//Incidencia #22034
		ownerController.showInformationAlert(ownerController.getString(R.string.label_information), mensaje, ownerController.getString(R.string.common_accept), 
				new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						ownerController.getParentViewsController().removeDelegateFromHashMap(CONTRATACION_DELEGATE_ID);
						SuiteAppApi.getInstanceApi().getSuiteViewsControllerApi()
								.showMenuSuite(true, new String[] {"bmovilselected"});
					}
				});
		
	}

	/**
	 * Valida si el tipo de persona es el correcto para Contratacion
	 * 
	 * @return
	 */
	private boolean validacionPersonaNoPermitida() {
        final boolean perfilAvanzado = Perfil.avanzado.equals(contratacion.getPerfil());
        final boolean personaNoPuedeContratar = Constants.PERSONA_F33.equals(consultaTarjetaResponse.getTipoPersona());
        // IDS - MODIFICACION FLUJO CONTRATACION
        // boolean isTC = Constants.CREDIT_TYPE.equals(consultaTarjetaResponse.getTipoTarjeta());
        // boolean isCE = Constants.EXPRESS_TYPE.equals(consultaTarjetaResponse.getTipoTarjeta());
        // return perfilAvanzado && (personaNoPuedeContratar || (isTC || isCE));
        return perfilAvanzado && personaNoPuedeContratar;
	}

	/**
	 * Determina si el cliente tiene una alerta de tipo '01'
	 * 
	 * @return
	 */
	private boolean validacionAlertas01() {
		return Constants.ALERT01.equals(consultaTarjetaResponse
				.getValidacionAlertas());
	}

	/**
	 * Determina si el cliente tiene una alerta de tipo '02'
	 * 
	 * @return
	 */
	private boolean validacionAlertas02() {
		return Constants.ALERT02.equals(consultaTarjetaResponse
				.getValidacionAlertas());
	}

	/**
	 * Determina si el cliente tiene una alerta de tipo '03' o '04'
	 * 
	 * @return
	 */
	private boolean validacionAlertas0304() {
		return Constants.ALERT03.equals(consultaTarjetaResponse
				.getValidacionAlertas())
				|| Constants.ALERT04.equals(consultaTarjetaResponse
						.getValidacionAlertas());
	}

	/**
	 * Determina si el cliente tiene un Token activo
	 * 
	 * @return
	 */
	private boolean determinarTokenActivo() {
		//Resoluci�n incidencia #21961
        final String instrumento = contratacion.getTipoInstrumento();
		return ((Constants.IS_TYPE_DP270.equals(instrumento) 
				|| Constants.IS_TYPE_OCRA.equals(instrumento) 
				|| bancomer.api.common.commons.Constants.TYPE_SOFTOKEN.S1.value.equals(instrumento))
				&& Constants.STATUS_APP_ACTIVE.equals(contratacion.getEstatusInstrumento()));
	}

	/**
	 * Valida que los elementos sean correctos antes de continuar con el flujo.
	 * 
	 * @param compania
	 *            La compañía telefónica del usuario.
	 * @param numeroTarjeta
	 *            El número de tarjeta del usuario.
	 */
	public void validaCamposST(final String compania, final String numeroTarjeta,
			final boolean usarToken) {
		if (null == ownerController) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
						"No se registro un ViewController.");
			return;
		} else if (null == compania) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "La compa�ia no puede ser nula.");
			return;
		} else if (null == numeroTarjeta) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
					"El numero de tarjeta de nulo o vacio.");
			return;
		}

		if (numeroTarjeta.length() < Constants.CARD_NUMBER_LENGTH) {
			ownerController
					.showInformationAlert(R.string.bmovil_contratacion_error_numero_tarjeta_corto);
			return;
		}

		if (null == contratacion) {
			contratacion = new Contratacion();
		}

		contratacion.setCompaniaCelular(compania);
		contratacion.setNumeroTarjeta(numeroTarjeta);

        final Hashtable<String, String> paramTable = new Hashtable<String, String>();
		
		/*if (!usarToken) {
			// perfil basico
			contratacion.setPerfil(Constants.Perfil.basico);
			paramTable.put(ServerConstants.PERFIL_CLIENTE, Constants.PROFILE_BASIC_01 );
		} else {*/
        // perfil avanzado
        contratacion.setPerfil(Perfil.avanzado);//hcf
       String perfil=Constants.PROFILE_ADVANCED_03 ;
        paramTable.put(ServerConstants.PERFIL_CLIENTE, Constants.PROFILE_ADVANCED_03 );//hcf
//		contratacion.setPerfil(Constants.Perfil.basico);//hcf
//		paramTable.put(ServerConstants.PERFIL_CLIENTE, Constants.PROFILE_BASIC_01 );//hcf
//		}


        final int operacion = Server.CONSULTA_TARJETA_OPERATION;

		paramTable.put(Server.USERNAME_PARAM, consultaEstatus.getNumCelular());// "numeroTelefono"
		paramTable.put(Server.CARDNUMBER_PARAM, numeroTarjeta);// numeroTarjeta
		paramTable.put(ServerConstants.COMPANIA_CELULAR, contratacion.getCompaniaCelular());// compa�iaTelefono

        final Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
		paramTable2.put(ServerConstants.NUMERO_TELEFONO, consultaEstatus.getNumCelular());
		paramTable2.put("numeroTarjeta", numeroTarjeta);
		paramTable2.put(ServerConstants.COMPANIA_CELULAR, contratacion.getCompaniaCelular());
		paramTable2.put(ServerConstants.PERFIL_CLIENTE, perfil);
		paramTable2.put(ServerConstants.VERSION_FLUJO, Constants.VERSION_FLUJO_CONTRATACION);
		
		//JAIG
		doNetworkOperation(operacion, paramTable2,true,new ConsultaTarjetaContratacionData(), ownerController);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #tokenAMostrar()
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		return Autenticacion.getInstance().tokenAMostrar(tipoOperacion,
				this.contratacion.getPerfil());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #mostrarNIP()
	 */
	@Override
	public boolean mostrarNIP() {
		return Autenticacion.getInstance().mostrarNIP(tipoOperacion,
				this.contratacion.getPerfil());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #mostrarCVV()
	 */
	@Override
	public boolean mostrarCVV() {
		return Autenticacion.getInstance().mostrarCVV(tipoOperacion,
				this.contratacion.getPerfil());
	}

	@Override
	public void sessionStored() {
		ownerController.ocultaIndicadorActividad();
	}

	/**
	 * Se valida la subaplicación Softtoken esta lista para ser activada. <br/>
	 * Esto sucede si el usuario tiene como instrumento de seguridad Softtoken
	 * activo y la subaplicación Softtoken no esta activa en el dispositivo.
	 */
	private boolean isSofttokenListoParaActivar() {
        final boolean isSofttoken = bancomer.api.common.commons.Constants.TYPE_SOFTOKEN.S1.value.equals(contratacion
				.getTipoInstrumento());
        final boolean isActive = Constants.ESTATUS_IS_ACTIVO.equals(contratacion
				.getEstatusInstrumento());
        final boolean isSofttokenActive = PropertiesManager.getCurrent()
				.getSofttokenActivated();
		return (isSofttoken && isActive && !isSofttokenActive);
	}

	/**
	 * Permite al cliente decidir si desea activar o no Softtoken.
	 */
	private void intentaActivarSofttoken() {
		if (null == ownerController)
			return;
	}

	private void mostrarActivacionST() {
		cambiarDeBMovilTokenMovil().showActivacionST(consultaEstatus,contratacion);
	}


	private SofttokenViewsController cambiarDeBMovilTokenMovil() {
        final SofttokenViewsController viewsController = SuiteAppApi.getInstanceApi()
				.getSofttokenApplicationApi().getSottokenViewsController();
		ownerController.setParentViewsController(viewsController);
		viewsController.setCurrentActivityApp(ownerController);

		return viewsController;
	}

	public void mostrarDefinirPassword() {		
	//((BmovilViewsController) ownerController.getParentViewsController()).showDefinirPassword();		
		
		if (ownerController != null)
			if(ownerController.getParentViewsController() instanceof BmovilViewsController){
				((BmovilViewsController) ownerController.getParentViewsController()).showDefinirPassword();							
			}else if(ownerController.getParentViewsController() instanceof SofttokenViewsController){
				//SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivity(IngresoDatosSTViewController.getInstance());
				 SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getBmovilViewsController().showDefinirPassword();
			}	
			
	}


	public void setEscenarioAlternativoEA11(final boolean escenarioAlternativoEA11) {
		this.escenarioAlternativoEA11 = escenarioAlternativoEA11;
	}
}
