package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.token;

import com.bancomer.mbanking.softtoken.SuiteAppApi;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.token.ContratacionDelegate;
import suitebancomer.classes.gui.controllers.token.BaseViewController;
import suitebancomer.classes.gui.controllers.token.BaseViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;


public class BmovilViewsController extends BaseViewsController {

	public void showContratacionEP11(final BaseViewController ownerController, final ConsultaEstatus consultaEstatus,
			final String estatusServicio, final boolean deleteData) {
		//comprueba que se trate de un contratacionDelegate del api de token, si no entonce  se quita del FromHashMap
		if( getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID)!=null){
			if(!(getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID) instanceof ContratacionDelegate) ){
				removeDelegateFromHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
			}
		}

		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
		
		
		if (null == delegate) {
			delegate = new ContratacionDelegate();
			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,
					delegate);
		}

		delegate.setEscenarioAlternativoEA11(false);
		delegate.setConsultaEstatus(consultaEstatus);
		delegate.setDeleteData(deleteData);
		//IngresarDatosViewController ingresarDatosViewController = new IngresarDatosViewController(
			//	true);
		//ingresarDatosViewController.setDelegate(delegate);
		//ingresarDatosViewController.setContratacionDelegate(delegate);
		//ingresarDatosViewController.setParentViewsController(SuiteApp
		//		.getInstance().getBmovilApplication()
		//		.getBmovilViewsController());
		
		//showViewController(IngresarDatosViewController.class);
		
	
		
		SuiteAppApi.getInstanceApi().getBmovilApplicationApi()
				.getBmovilViewsController()
				.setCurrentActivityApp(ownerController);

		delegate.setOwnerController(ownerController);
		
		delegate.validaCamposST(consultaEstatus.getCompaniaCelular(),
				consultaEstatus.getNumTarjeta(), true);
	}
	
	/**
	 * Muestra la pantalla de definir contrase�a la contrataci�n.
	 */
	public void showDefinirPassword() {		
		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionDelegate();
			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID, delegate);
		}

		SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getViewsController().showViewController(SuiteAppApi.getIntentToContratacion().getClass());
	//	showViewController(ContratacionDefinicionPasswordViewController.class);
	}

}