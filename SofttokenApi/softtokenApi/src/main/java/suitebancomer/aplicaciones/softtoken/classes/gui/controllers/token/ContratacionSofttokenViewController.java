package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomer.classes.gui.controllers.token.MenuSuiteViewController;
import suitebancomercoms.classes.common.GuiTools;

public class ContratacionSofttokenViewController extends LinearLayout implements OnClickListener {
	/**
	 * The delegate reference
	 */
	private ContratacionSTDelegate delegate;
	
	/**
	 * 
	 */
	private SofttokenViewsController parentManager;
	
	/**
	 * The flip back button
	 */
	private Button mFlipBackButton;
	
	/**
	 * The button which fires the login process
	 */
	private Button mAccessButton;
	
	/**
	 * Reference to the parent controller, used to perform the flip back action
	 */
	private MenuSuiteViewController menuViewController;
	
	public ContratacionSofttokenViewController(final MenuSuiteViewController menuSuiteViewController, final SofttokenViewsController parentViewsController) {
		super(menuSuiteViewController);
		
		parentManager = parentViewsController;
		menuViewController = menuSuiteViewController;
		parentManager.setCurrentActivityApp(menuViewController);
		
	    final LayoutInflater inflater = LayoutInflater.from(menuSuiteViewController);
	    final LinearLayout viewLayout = (LinearLayout)inflater.inflate(R.layout.layout_softtoken_deactivated, this, true);
	    
	    final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1,-1); // Fill/Match the parent.
	    viewLayout.setLayoutParams(params);

	    findViews();
		scaleForCurrentScreen();
	}
	
	public void onBtnFlipclick(final View sender) {
		//menuViewController.plegarOpcionST();
	}
	
	public void onBtnContinuarclick(final View sender) {
		final SofttokenActivationBackupManager manager = SofttokenActivationBackupManager.getCurrent();
		if(manager.existsABackup()) {
			SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showPantallaCActivacionST(true);
		} else {
			SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showPantallaIngresoDatos();
		}
	}

	@Override
	public void onClick(final View v) {
		if(v.equals(mFlipBackButton)) {
			onBtnFlipclick(v);
		} else if(v.equals(mAccessButton)) {
			onBtnContinuarclick(v);
		}
	}
	
	private void findViews() {
		mFlipBackButton = (Button)findViewById(R.id.login_flip_button);
		mFlipBackButton.setOnClickListener(this);
		mAccessButton = (Button)findViewById(R.id.btnCotratar);
		mAccessButton.setOnClickListener(this);
	}
	
	private void scaleForCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		
		guiTools.scale(findViewById(R.id.softtokenDeactivatedRootLayout));
		guiTools.scale(findViewById(R.id.softtokenDeactivatedTitleLayout));
		guiTools.scale(findViewById(R.id.softtokenDeactivatedTitle), true);
		guiTools.scale(mFlipBackButton);
		guiTools.scale(findViewById(R.id.lblSofttokenDesactivado), true);
		guiTools.scale(mAccessButton);
	}

	/**
	 * @param delegate the delegate to set
	 */
	public void setDelegate(final ContratacionSTDelegate delegate) {
		this.delegate = delegate;
	}
}
