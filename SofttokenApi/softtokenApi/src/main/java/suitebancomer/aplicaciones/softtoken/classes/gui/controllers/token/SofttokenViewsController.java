/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.content.Intent;

import com.bancomer.mbanking.softtoken.SuiteAppApi;

import java.util.ArrayList;

import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import suitebancomer.aplicaciones.softtoken.classes.model.token.SoftToken;
import suitebancomer.classes.gui.controllers.token.BaseViewsController;
import suitebancomer.classes.gui.controllers.token.MenuSuiteViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Contratacion;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;


public class SofttokenViewsController extends BaseViewsController {

	//AMZ
	public ArrayList<String> estados = new ArrayList<String>();
	@Override
	public void showMenuInicial() {
		showPantallaGeneraOTPST(true);
	}

	public void showContratacionSotfttoken(final BaseViewControllerCommons comonmenuSuiteViewController) {
		final MenuSuiteViewController menuSuiteViewController=(MenuSuiteViewController)comonmenuSuiteViewController;
		ContratacionSTDelegate delegate = (ContratacionSTDelegate) getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionSTDelegate();
			addDelegateToHashMap(
					ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,
					delegate);
			addDelegateToHashMap(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID,
					delegate.generaTokendelegate);
		}
		if (SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getSessionTimer() != null) {
			SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getSessionTimer()
					.cancel();
			SuiteAppApi.getInstanceApi().getBmovilApplicationApi().setSessionTimer(null);
		}
		final ContratacionSofttokenViewController viewController = new ContratacionSofttokenViewController(
				menuSuiteViewController, this);
		viewController.setDelegate(delegate);

		//FIXME ALAN
		/*menuSuiteViewController.setContratacionSTViewController(viewController);
		menuSuiteViewController.getBaseLayout().addView(viewController);
		viewController.setVisibility(View.GONE);
*/
	}

	public void showPantallaIngresoDatos() {

		ContratacionSTDelegate delegate = (ContratacionSTDelegate) getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionSTDelegate();
			addDelegateToHashMap(
					ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,
					delegate);
			addDelegateToHashMap(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID,
					delegate.generaTokendelegate);
		}
		if (SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getSessionTimer() != null) {
			SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getSessionTimer()
					.cancel();
			SuiteAppApi.getInstanceApi().getBmovilApplicationApi().setSessionTimer(null);
		}
		
		showViewController( IngresoDatosSTViewController.class);	
		
	}

	public void showPantallaIngresoDatos(
			final MenuSuiteViewController menuSuiteViewController) {

		ContratacionSTDelegate delegate = (ContratacionSTDelegate) getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionSTDelegate();
			addDelegateToHashMap(
					ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,
					delegate);
			addDelegateToHashMap(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID,
					delegate.generaTokendelegate);
		}
		if (SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getSessionTimer() != null) {
			SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getSessionTimer()
					.cancel();
			SuiteAppApi.getInstanceApi().getBmovilApplicationApi().setSessionTimer(null);
		}


		showViewController(IngresoDatosSTViewController.class);
	}

	/**
	 * Muestra la pantalla Corfirmacion ST.
	 */
	public void showConfirmacionST(final SoftToken softToken) {
		
		ContratacionSTDelegate delegate = (ContratacionSTDelegate) getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionSTDelegate();
			delegate.setSoftToken(softToken);
			addDelegateToHashMap(
					ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,
					delegate);
			addDelegateToHashMap(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID,
					delegate.generaTokendelegate);
		}

		showViewController( ConfirmacionSTViewController.class);	
	}

	/**
	 * Muestra la pantalla Clave de activacion ST.
	 */
	public void showPantallaCActivacionST() {

		
		suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate delegate= (suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate) 
		SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().getBaseDelegateForKey(suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
		if (null == delegate) {
			delegate = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate();
			 SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().addDelegateToHashMap(
					 ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,
					delegate);
			 SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().addDelegateToHashMap(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID,
					delegate.generaTokendelegate);
		}
		delegate.setCargarDatos(false);
		showViewController( ActivacionSTViewController.class);	
	}

	public void showPantallaCActivacionST(final boolean cargarRespaldo) {

		suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate delegate= (suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate) SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().getBaseDelegateForKey(suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
		if (null == delegate) {
			delegate = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate();
			SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().addDelegateToHashMap(
					ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,
					delegate);
			SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().addDelegateToHashMap(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID,
					delegate.generaTokendelegate);
		}
		delegate.setCargarDatos(true);
		//SuiteAppApi.getInstanceApi().closeSofttokenAppApi();
		//SuiteAppApi.getInstanceApi().closeSofttokenAppApi();
		showViewController( ActivacionSTViewController.class);	
		
	}

	public void showPantallaEmailST(final Object[] extras) {

		showViewController(EmailSTViewController.class, 0, false,
				new String[] { SofttokenConstants.MENSAJE_ERROR }, extras);
	}

	public void showPantallaAyudaST() {
		showViewController(AyudaContratacionSTViewController.class);
	}

	public void showPantallaGeneraOTPST(final boolean inverted) {
		ContratacionSTDelegate delegate = (ContratacionSTDelegate) getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionSTDelegate();
			addDelegateToHashMap(
					ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,
					delegate);
			addDelegateToHashMap(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID,
					delegate.generaTokendelegate);
		}
		//if(estados.size() != 0)
				//{
					estados.clear();
				//}
				//AMZ
				TrackingHelper.trackState("menu token", estados);
				//AMZ
				
		showViewController(GeneraOTPSTViewController.class,
				Intent.FLAG_ACTIVITY_CLEAR_TOP, inverted);
	}
	

	public void showPantallaMuestraOTPST(final String token, final String tipoOtp) {
		showViewController(MuestraOTPSTViewController.class, 0, false,
				new String[] { SofttokenConstants.TOKEN_GENERADO,
						SofttokenConstants.TIPO_OTP_A_MOSTRAR_PARAM },
				new String[] { token, tipoOtp });
	}

	public void showPantallaRegistraCuentaST() {
		showViewController(RegistraCuentaSTViewController.class);
	}

	/**
	 * Muestra la pantalla de Activacion de Softtoken.
	 */
	public void showActivacionST(final ConsultaEstatus consultaEstatus,
			final Contratacion contratacion) {
		ContratacionSTDelegate delegate = (ContratacionSTDelegate) getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionSTDelegate();
			addDelegateToHashMap(
					ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,
					delegate);
			addDelegateToHashMap(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID,
					delegate.generaTokendelegate);
		}

		delegate.setOwnerController(getCurrentViewControllerApp());
		delegate.flujoContratacion(consultaEstatus, contratacion);
	}

	public void showInputRegisterView(final String result1, final String result2, final String type, final String codQR) {
		showViewController(AltaRegistroViewController.class, 0, false,
				new String[] { SofttokenConstants.RESULT_REGISTER1,
						SofttokenConstants.RESULT_REGISTER2,
						SofttokenConstants.TYPE_OPERATION,
						SofttokenConstants.CODQR_VALUE},

				new String[] { result1,result2,type,codQR});
	}

		/**
         * Muestra la pantalla de Activacion de Softtoken, EA11.
         */
	public void showActivacionSTEA11(final ServerResponse response) {
		ContratacionSTDelegate delegate = (ContratacionSTDelegate) getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionSTDelegate();
			addDelegateToHashMap(
					ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,
					delegate);
			addDelegateToHashMap(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID,
					delegate.generaTokendelegate);
		}
		if (SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getSessionTimer() != null) {
			SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getSessionTimer()
					.cancel();
			SuiteAppApi.getInstanceApi().getBmovilApplicationApi().setSessionTimer(null);
		}
		delegate.setOwnerController(getCurrentViewControllerApp());
		delegate.flujoActivacionEA11(response);
	}

	/**
	 * Muestra la pantalla de Activacion de Softtoken, EA12
	 */
	public void showActivacionSTEA12() {
		suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate delegate
			= (suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate) 
				SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().getBaseDelegateForKey(suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
		if (null == delegate) {
			delegate = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate();
			 SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().addDelegateToHashMap(
					ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,
					delegate);
			 SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().addDelegateToHashMap(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID,
					delegate.generaTokendelegate);
		}

		//delegate.setOwnerController((suitebancomercoms.classes.gui.controllers.token.BaseViewController) new ContratacionSofttokenViewController());
		delegate.setOwnerController(getCurrentViewControllerApp());
		delegate.setEA12(true);
		delegate.flujoActivacionEA12();
	}

	public void showLecturaCodigoViewController(final int codigoActividad) {

		showViewControllerForResult(SuiteAppApi.getActivityQrToken().getClass(), codigoActividad);
	}

	public void touchMenuSofttoken()
	{
		estados.clear();
	}
	
	public void touchAtras()
	{

		final int ultimo = estados.size()-1;
		
		String eliminado;
		if(ultimo >= 0)
		{
			final String ult = estados.get(ultimo);
			if(ult == "reactivacion" || ult == "activacion" || ult == "menu token" || ult == "contratacion datos" || ult == "activacion datos")
			{
				estados.clear();
			}else
			{
				 eliminado = estados.remove(ultimo);
			}
		} 
		
	}

	
}
