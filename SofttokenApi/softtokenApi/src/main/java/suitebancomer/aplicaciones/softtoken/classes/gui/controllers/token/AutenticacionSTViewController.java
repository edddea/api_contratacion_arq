/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.token.BmovilViewsController;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class AutenticacionSTViewController extends SofttokenBaseViewController{
;
	
	//AMZ
	private SofttokenViewsController parentManager;
	private BmovilViewsController bm;
//	private LinearLayout Cvv;
	private TextView nip;
	private EditText tbNIP;
	//softtoken sinc cvv
	private EditText tbCVV;
	private EditText tbOTP;
	private EditText tbTargeta;
	private ContratacionSTDelegate contratacionDelegate;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppApi.getResourceId("layout_softtoken_autenticacion_st", "layout"));
		setTitle(R.string.softtoken_activacion_titulo, R.drawable.icono_st_activado);

		parentManager = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		bm = SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getBmovilViewsController();

		parentViewsController = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		parentViewsController.setCurrentActivityApp(this);
		setDelegate((ContratacionSTDelegate) parentViewsController.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
		contratacionDelegate = (ContratacionSTDelegate)getDelegate();
		if (null == contratacionDelegate) {
			parentViewsController.addDelegateToHashMap(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,new ContratacionSTDelegate());
			setDelegate((ContratacionSTDelegate) parentViewsController.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
			contratacionDelegate = (ContratacionSTDelegate) getDelegate();
		}

		contratacionDelegate.setOwnerController(this);


		if(parentManager.estados != null)
			TrackingHelper.trackState("autenticacionST", parentManager.estados);
		init();
	}

	@Override
	protected void onResume() {
		this.parentViewsController = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		parentViewsController.setCurrentActivityApp(this);
		setDelegate((ContratacionSTDelegate)parentViewsController.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
		contratacionDelegate = (ContratacionSTDelegate)getDelegate();
		if (null == contratacionDelegate) {
			contratacionDelegate = new ContratacionSTDelegate();
		}
		contratacionDelegate.setOwnerController(this);

		super.onResume();
	}

	private void init() {
		findViews();
		scaleForCurrentScreen();
	}
	
	private void findViews() {
		tbNIP = (EditText)findViewById(R.id.tbNip);
		tbOTP = (EditText)findViewById(R.id.confirmacion_asm_edittext);
		tbTargeta = (EditText)findViewById(R.id.tbTarjeta);
		//softtoken sinc cvv
		tbCVV = (EditText)findViewById(R.id.tbCvv);
	}
	
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(tbNIP, true);
		guiTools.scale(tbOTP, true);
		guiTools.scale(tbTargeta, true);
		
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("rootLayout", "id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblNipTitulo","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblAdvertenciaNip","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("rootSecurityInstrumentsLayout","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("campo_confirmacion_asm_layout","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("confirmacion_asm_inner_layout","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("confirmacion_asm_label","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("confirmacion_asm_instrucciones_label","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("confirmacion_confirmar_button","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblAdvertenciaTiempo","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblAdvertenciaNipCvv","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("tbCvv","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblCvvTitulo","id")),true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblTarjetaTitulo","id")),true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see suitebancomercoms.classes.gui.controllers.BaseViewController#
	 * processNetworkResponse(int,
	 * suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		contratacionDelegate.analyzeResponse(operationId, response);
	}
	
	/**
	 * Se selecciona la opci�n Continuar.
	 * 
	 * @param sender
	 *            la opci�n seleccionada
	 */
	public void onBtnContinuarClick(View sender) {
//softtoken sinc cvv
		contratacionDelegate.autenticacionSTFlujo(tbNIP.getText().toString(), tbCVV.getText().toString(), tbTargeta.getText().toString());
	}
	


}
