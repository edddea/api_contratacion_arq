package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
public class RegistraCuentaSTViewController extends SofttokenBaseViewController {
	private GeneraOTPSTDelegate generaOtpDelegate;
	private ContratacionSTDelegate contratacionSTDelegate;
	private EditText tbRegistro;
	//AMZ
		private SofttokenViewsController parentManager;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_softtoken_otp_challenge);
		setTitle(R.string.softtoken_titulo, R.drawable.icono_st_activado);		
		//AMZ
				parentManager = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();						
				TrackingHelper.trackState("registrar cuenta", parentManager.estados);
		init();
	}
	
	private void init() {
		parentViewsController = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		parentViewsController.setCurrentActivityApp(this);
		setDelegate((ContratacionSTDelegate)parentViewsController.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
		contratacionSTDelegate = (ContratacionSTDelegate)getDelegate();
		generaOtpDelegate = contratacionSTDelegate.generaTokendelegate;
		contratacionSTDelegate.setOwnerController(this);
		
		findViews();
		scaleToCurrentScreen();
	}
	
	private void findViews() {
		tbRegistro = (EditText)findViewById(R.id.tbIngresaDatos);
	}
	
	private void scaleToCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.rootLayout));
		guiTools.scale(findViewById(R.id.lblIngresaDatos), true);
		guiTools.scale(findViewById(R.id.tbIngresaDatos), true);
		guiTools.scale(findViewById(R.id.lblInstrucciones), true);
		guiTools.scale(findViewById(R.id.btnMenu));
	}
	
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		return;
	}
	
	public void onBtnContinuarClick(final View sender) {
		generaOtpDelegate.validaDatos(tbRegistro.getText().toString());
	}
}
