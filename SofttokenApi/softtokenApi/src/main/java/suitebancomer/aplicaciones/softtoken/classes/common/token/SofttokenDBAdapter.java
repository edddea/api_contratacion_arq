package suitebancomer.aplicaciones.softtoken.classes.common.token;

import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SofttokenDBAdapter {
	/**
	 * Version de la base de datos.
	 */
	private static final int DATABASE_VERSION = 1;

	/**
	 *  SQL Statement to create a new table.
	 */
	private static final String CREATE_TABLE1 = "create table " + SofttokenConstants.DB_SESSION_TABLE + " (" + 
			SofttokenConstants.DB_KEY_ID + " integer primary key autoincrement, " +
			SofttokenConstants.DB_SESSION_TABLE_PROPERTY_NAME + " text not null, " +
			SofttokenConstants.DB_SESSION_TABLE_PROPERTY_VALUE + " text);";

	/**
	 *  SQL Statement to delete a content of the table
	 */
	private static final String DELETE_TABLE ="delete from ";

	/**
	 *  Variable to hold the database instance
	 */
	private SQLiteDatabase db;

	/**
	 *  Context of the application using the database.
	 */
	private final Context context;

	/**
	 * Database open/upgrade helper.
	 */
	private myDbHelper dbHelper;

	/**
	 * Opens and connects to the specified socket TCP/IP connection.
	 * @param _context Context of the application using the database.
	 */
	public SofttokenDBAdapter(final Context _context) {
		context = _context;
		dbHelper = new myDbHelper(context, SofttokenConstants.DB_FILE, null,DATABASE_VERSION);  
	}

	/**
	 * Open and return a readable/writable instance of the database.
	 * 
	 */
	public SofttokenDBAdapter open(){
		try {
			db = dbHelper.getWritableDatabase();
		}
		catch (SQLiteException ex){
			db = dbHelper.getReadableDatabase();
		}
		return this;
	}

	/**
	 * Open and return a readable/writable instance of the database. 
	 * 
	 */      
	public void close() {
		try {
			db.close();
			dbHelper.close();
		} catch (SQLiteException e) {
			if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(), "Error while closing the db.", e);
		} finally {
			db=null;
			dbHelper=null;
		}

	}
	
	/**
	 * Inserta un registro en la BD
	 * @param identificador La propiedad a establecer.
	 * @param value El valor de la propiedad.
	 * @return the row ID of the newly inserted row, or -1 if an error occurred 
	 */  
	public long insertSessionPropertyEntry(final String identificador, final String value) {
		final ContentValues contentValues = new ContentValues();
		contentValues.put(SofttokenConstants.DB_SESSION_TABLE_PROPERTY_NAME, identificador);
		contentValues.put(SofttokenConstants.DB_SESSION_TABLE_PROPERTY_VALUE, value);
		return db.insert(SofttokenConstants.DB_SESSION_TABLE, null, contentValues);
	}

	/**
	 * Borra un registro de la tabla.
	 * @param propertyName El nombre de la propiedad a borrar.
	 * @return True si al menos un registro fue borrado, False de otra forma.
	 */
	public boolean deleteSessionPropertyEntry(final String propertyName) {
		return db.delete(SofttokenConstants.DB_SESSION_TABLE, 
						 SofttokenConstants.DB_SESSION_TABLE_PROPERTY_NAME + " = '" + propertyName + "'", 
						 null) > 0;
	}

	/**
	 * Obtiene todos los registros de la tabla.
	 */
	public Cursor selectAllSessionPropertyEntries () {
		return db.query(SofttokenConstants.DB_SESSION_TABLE, 
						new String[] {SofttokenConstants.DB_KEY_ID, 
									  SofttokenConstants.DB_SESSION_TABLE_PROPERTY_NAME, 
									  SofttokenConstants.DB_SESSION_TABLE_PROPERTY_VALUE}, 
						null, 
						null, 
						null,
						null, 
						null);
	}
	
	/**
	 * Obtiene la propiedad especificada de la tabla.
	 * @param El nombre de la propiedad.
	 */
	public Cursor selectSessionPropertyEntry(final String proppertyName) {
		return db.query(SofttokenConstants.DB_SESSION_TABLE, 
						new String[] {SofttokenConstants.DB_KEY_ID, 
									  SofttokenConstants.DB_SESSION_TABLE_PROPERTY_NAME, 
									  SofttokenConstants.DB_SESSION_TABLE_PROPERTY_VALUE}, 
						SofttokenConstants.DB_SESSION_TABLE_PROPERTY_NAME + " = '" + proppertyName + "'", 
						null, 
						null,
						null, 
						null);
	}

	/**
	 * Get the number of rows in the table
	 * @param table 
	 */
	public int getSessionPropertiesCount() {
		Cursor c=null;
		int total=0;
		try{
			c = selectAllSessionPropertyEntries();
			total = c.getCount();
		}catch (SQLiteException e) {
			if(Server.ALLOW_LOG) Log.i(getClass().getSimpleName(), "Error getNumRecords()");
		}finally{
			if(c!=null)
				c.close();
		}
		return total;
	}

	/**
	 * Delete the contents of the table
	 * @param table 
	 */
	public void deleteSessionPropertiesContent() {
		db.execSQL(DELETE_TABLE + SofttokenConstants.DB_SESSION_TABLE);
	}

	/**
	 * Hide the logic used to decide if a database needs to be created or 
	 * upgraded before its opened.
	 */ 
	private static class myDbHelper extends SQLiteOpenHelper {

		/**
		 * Hide the logic used to decide if a database needs to be created or 
		 * upgraded before its opened.
		 * @param context of the application using the database.
		 * @param name of the database.
		 * @param null CursorFactory
		 * @param version database
		 */
		public myDbHelper(final Context context, final String name, final CursorFactory factory, final int version) {
			super(context, name, factory, version);
		}

		/** Called when no database exists in 
		 * disk and the helper class needs
		 * to create a new one.
		 * @param _db database.
		 */ 
		@Override
		public void onCreate(final SQLiteDatabase _db) {
			_db.execSQL(CREATE_TABLE1);
		}

		/** Called when there is a database version mismatch meaning that 
		 * the version of the database on disk needs to be upgraded to 
		 * the current version.
		 * @param _db database.
		 * @param _oldVersion of the database.
		 * @param _newVersion of the database.
		 */
		@Override
		public void onUpgrade(final SQLiteDatabase _db, final int _oldVersion, final int _newVersion) {
			// Log the version upgrade.
			if(Server.ALLOW_LOG) Log.w("TaskDBAdapter", "Upgrading from version " + _oldVersion + " to " + _newVersion);

			switch (_oldVersion) {
			
			}
		}
	}
}
