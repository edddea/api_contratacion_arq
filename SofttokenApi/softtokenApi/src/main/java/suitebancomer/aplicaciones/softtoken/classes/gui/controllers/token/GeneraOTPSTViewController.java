/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.views.token.ListaSeleccionViewController;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

public class GeneraOTPSTViewController extends SofttokenBaseViewController {
	private GeneraOTPSTDelegate generaOtpDelegate;
	private LinearLayout layoutListaSeleccion;
	private ListaSeleccionViewController listaSeleccion;
	private String cadenaQR;
	private ContratacionSTDelegate contratacionSTDelegate;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_softtoken_menu_principal);
		setTitle(R.string.softtoken_titulo, R.drawable.icono_st_activado);			
		init();
	}
	
	@Override
	protected void onResume() {
		//listaSeleccion.setDelegate(generaOtpDelegate);
		generaOtpDelegate.setOwnerController(this);
		((SofttokenViewsController)getParentViewsController()).setCurrentActivityApp(this);
		
		super.onResume();
	}
	
	private void init() {
		parentViewsController = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		parentViewsController.setCurrentActivityApp(this);
		setDelegate((ContratacionSTDelegate)parentViewsController.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
		contratacionSTDelegate = (ContratacionSTDelegate)getDelegate();
		generaOtpDelegate = contratacionSTDelegate.generaTokendelegate;
		contratacionSTDelegate.setOwnerController(this);
		
		findViews();
		scaleToCurrentScreen();
		//cargarListaSeleccion();
		final ImageButton btnRegistrar =(ImageButton)findViewById(R.id.btnRegistrar);
        final ImageButton btnGenerar =(ImageButton)findViewById(R.id.btnGenerar);
        final ImageButton btnreadQR =(ImageButton)findViewById(R.id.btnReadQRCode);

		btnGenerar.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
                final String tokenGenerado = contratacionSTDelegate.generaTokendelegate.generaOTPTiempo();
    			if(tokenGenerado!=null)
    				((SofttokenViewsController)getParentViewsController()).showPantallaMuestraOTPST(tokenGenerado, SofttokenConstants.OTP_TIEMPO);
            }
        });
		btnRegistrar.setOnClickListener(new View.OnClickListener() {
            public void onClick(final View v) {
            	((SofttokenViewsController)getParentViewsController()).showPantallaRegistraCuentaST();
            }
        });

		btnreadQR.setOnClickListener(new View.OnClickListener() {
			public void onClick(final View v) {
				try{
					((SofttokenViewsController)parentViewsController).showLecturaCodigoViewController(123456);

				} catch (NullPointerException e) {
					e.printStackTrace();
					Log.i("Error", "Error: " + e.getLocalizedMessage());
				} catch(RuntimeException e){
					e.printStackTrace();
					Log.i("Error", "Error: "+e.getLocalizedMessage());
				}
			}
		});

        final TextView textView = (TextView)findViewById(R.id.lblBorrarDatos);
        final SpannableString content = new SpannableString(textView.getText());
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		textView.setText(content);
		
		contratacionSTDelegate.generaTokendelegate.inicializaCore();
	}
	
	private void findViews() {
		layoutListaSeleccion = (LinearLayout)findViewById(R.id.layoutListaSeleccion);
	}
	
	private void scaleToCurrentScreen() {
        final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.rootLayout));
		guiTools.scale(findViewById(R.id.lblBorrarDatos), true);
		guiTools.scale(findViewById(R.id.btnContinuar));
	}

	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		return;
	}
	
	public void onBtnBorrarDatosClick(final View sender) {
		generaOtpDelegate.borrarDatosConAlerta();
	}
	
	@Override
	public void goBack() {
		super.goBack();
	}    

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		if (requestCode == 123456) {
			if (resultCode == RESULT_OK) {
				cadenaQR = data.getExtras().getString("DATA");

                final String [] otp = contratacionSTDelegate.generaTokendelegate.obtieneDatosFromQR(cadenaQR);
				if(otp[2] != null && otp[3] != null && otp[0] != null) {
					if(otp[0].equals("01")||otp[0].equals("02")||otp[0].equals("03")||otp[0].equals("04")||otp[0].equals("05")) {

						((SofttokenViewsController) getParentViewsController()).showInputRegisterView(otp[2], otp[3], otp[0],otp[4]);

					} else {
						Toast.makeText(getApplicationContext(), getResources().getString(R.string.softtoken_otp_alta_registro_failure),
								Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(getApplicationContext(), getResources().getString(R.string.softtoken_otp_alta_registro_failure),
							Toast.LENGTH_LONG).show();
				}

			}
		}
	}

}
