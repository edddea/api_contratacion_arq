package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.webkit.WebView;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;
import java.util.Hashtable;


import suitebancomer.aplicaciones.softtoken.classes.model.token.ConsultaTerminosDeUsoData;
import suitebancomer.classes.gui.controllers.token.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;

/**
 * Created by alanmichaelgonzalez on 20/01/16.
 */
public class TerminosCondicionesStViewController extends BaseViewController {
    /**
     * Campo de texto para terminos de uso.
     */
    private WebView wvTerminos;

    public TerminosCondicionesStViewController(){
        wvTerminos = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_softtoken_terminos_condiciones);
        SuiteApp.appContext = this;
        parentViewsController = SuiteAppApi.getInstanceApi()
                .getSofttokenApplicationApi().getSottokenViewsController();
        setTitle(R.string.cambioPerfil_titulost, R.drawable.icono_cambio_perfil,
                R.color.naranja);
        findViews();
        scaleToScreenSize();

        String terminos = (String)this.getIntent().getExtras().get(Constants.TERMINOS_DE_USO_EXTRA);
        if(null != terminos) {
            if(Build.VERSION.SDK_INT < 15){
                wvTerminos.loadDataWithBaseURL(null, terminos, "text/html", "utf-8", null);
            } else {
                wvTerminos.loadData(terminos, "text/html", "utf-8");
            }
        }
        //AMZ
    }

    /**
     * Busca las referencias a las vistas.
     */
    private void findViews() {
        wvTerminos = (WebView)findViewById(R.id.webViewTerminos);
    }

    /**
     * Escala las vistas para acomodarse a la pantalla actual.
     */
    private void scaleToScreenSize() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.layoutBaseContainer));
        guiTools.scale(findViewById(R.id.lblTitulo), true);
        guiTools.scale(wvTerminos);
    }

    public void showTerminosDeUso() {
        int operationId = Server.OP_CONSULTAR_TERMINOS_SESION;
        Session session = Session.getInstance(SuiteApp.appContext);

        bancomer.api.common.commons.Constants.Perfil perfil = session.getClientProfile();

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(
                ServerConstants.PERFIL_CLIENTE,
                perfil.equals(bancomer.api.common.commons.Constants.Perfil.avanzado) ? bancomer.api.common.commons.Constants.PROFILE_ADVANCED_03
                        : bancomer.api.common.commons.Constants.PROFILE_BASIC_01);

        SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().invokeNetworkOperation(operationId, params, true, new ConsultaTerminosDeUsoData(), this, true);

    }

    /* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
    @Override
    public void processNetworkResponse(final int operationId, final ServerResponse response) {
        Log.e("","");
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        float touchX = ev.getX();
        float touchY = ev.getY();
        int[] webViewPos = new int[2];

        wvTerminos.getLocationOnScreen(webViewPos);

        float listaSeleccionX2 = webViewPos[0] + wvTerminos.getMeasuredWidth();
        float listaSeleccionY2 = webViewPos[1] + wvTerminos.getMeasuredHeight();

        if ((touchX >= webViewPos[0] && touchX <= listaSeleccionX2) &&
                (touchY >= webViewPos[1] && touchY <= listaSeleccionY2)) {
            wvTerminos.getParent().requestDisallowInterceptTouchEvent(true);
        }

        return super.dispatchTouchEvent(ev);
    }

    /*// 2.0 and above
    @Override
    public void onBackPressed() {
        showAutenticacionSt();
    }

    // Before 2.0
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showAutenticacionSt();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }*/

    private void showAutenticacionSt(){
        finish();
        parentViewsController.showViewController(AutenticacionSTViewController.class);

    }
}
