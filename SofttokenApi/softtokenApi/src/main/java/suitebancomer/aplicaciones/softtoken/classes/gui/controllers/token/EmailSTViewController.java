/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

/**
 * TODO: COMENTARIO DE LA CLASE
 *
 * @author Stefanini IT Solutions.
 */

public class EmailSTViewController extends SofttokenBaseViewController{
	private ContratacionSTDelegate contratacionDelegate;
	//AMZ
		private SofttokenViewsController parentManager;
		private ScrollView vista;
		private LinearLayout exito1;
		private LinearLayout novedades2;
		private LinearLayout exito2;
		private LinearLayout exito3;
		private TextView parrafo1;
		private TextView parrafo2;
		private TextView parrafo3;
		private TextView parrafo4;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		//super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_softtoken_correo);
		//setTitle(R.string.softtoken_activacion_titulo, R.drawable.icono_st_activado);
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE,R.layout.activity_activacion_exitosa_st_view_controller);
		setTitle(R.string.menuSuite_activation_title, R.drawable.icono_st_activado);
		//AMZ
				parentManager = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();								
				TrackingHelper.trackState("activacion email", parentManager.estados);
				

		init();
	}
	
	private void init() {
		parentViewsController = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		parentViewsController.setCurrentActivityApp(this);
		setDelegate((ContratacionSTDelegate)parentViewsController.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
		contratacionDelegate = (ContratacionSTDelegate)getDelegate();
		contratacionDelegate.setOwnerController(this);
		
		findViews();

		scaleForCurrentScreen();
		//hcf2x1
		final Session session = Session.getInstance(SuiteAppApi.appContext);
		if (contratacionDelegate.getEsReact2x1()) {
			parrafo3.setText(getString(R.string.softtoken_activacion_exitosa_contratarBmovilTrue));
		}
		//hcf2x1


		//((EditText)findViewById(R.id.tbCorreo)).setText(contratacionDelegate.getSoftToken().getCorreoElectronico());
		
		final Bundle bundle = getIntent().getExtras();
		if (null != bundle) {
			final String mensajeError = bundle.getString(SofttokenConstants.MENSAJE_ERROR);
			if(null != mensajeError) {
				contratacionDelegate.setErrorFinalizarContratacion(true);
				showErrorMessage(mensajeError);
			}
		}
	}
	
	private void findViews() {
		vista = (ScrollView)  findViewById(R.id.novelties_scroll);
		exito1 = (LinearLayout)findViewById(R.id.novelties_1_layout);
		//novedades2 = (LinearLayout)findViewById(R.id.novelties_2_layout);
		exito2 = (LinearLayout)findViewById(R.id.novelties_3_layout);
//		exito3 = (LinearLayout)findViewById(R.id.novelties_4_layout);
		
		parrafo1 = (TextView)findViewById(R.id.paragraph_1_label);
		parrafo2 = (TextView)findViewById(R.id.paragraph_2_label);
		parrafo3 = (TextView)findViewById(R.id.paragraph_3_label);
//		parrafo4 = (TextView)findViewById(R.id.paragraph_4_label);
	}
	
	
	private void scaleForCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.novelties_layout));
		
		guiTools.scale(findViewById(R.id.novelties_1_layout));
		guiTools.scale(findViewById(R.id.paragraph_1_label), true);
		
		//guiTools.scale(findViewById(R.id.novelties_2_layout));
		//guiTools.scale(findViewById(R.id.paragraph_2_label), true);
		
		guiTools.scale(findViewById(R.id.novelties_3_layout));
		guiTools.scale(findViewById(R.id.paragraph_3_label), true);
		
//		guiTools.scale(findViewById(R.id.novelties_4_layout));
//		guiTools.scale(findViewById(R.id.paragraph_4_label), true);
		
		guiTools.scale(findViewById(R.id.btnContinuar));

	}
	
	/* (non-Javadoc)
	 * @see suitebancomercoms.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		contratacionDelegate.analyzeResponse(operationId, response);
	}
	
	/**
	 * Se selecciona la opci�n Continuar.
	 * 
	 * @param sender
	 *            la opci�n seleccionada
	 */
	public void onBtnContinuarClick(final View sender) {
//		contratacionDelegate.muestraAlertaBienvenida();
		
		//SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showActivacionExitosa();
		//SuiteAppApi.getInstance().getSuiteViewsController().setCurrentActivityApp(this);
		//SuiteAppApi.getInstance().getSuiteViewsController().showMenuSuite(true);
		if(SuiteAppApi.getIsAplicationLogged() && SuiteAppApi.getCallBackSession()!=null){


			SuiteAppApi.getIntentToReturn().returDesactivada();
			Session.getInstance(SuiteAppApi.appContext).setValidity(Session.VALID_STATUS);
			Session.getInstance(SuiteAppApi.appContext).setUsername(SuiteAppApi.getUserNameOld());
			Session.getInstance(SuiteAppApi.appContext).setIum(SuiteAppApi.getIumOld());
			Session.getInstance(SuiteAppApi.appContext).setClientNumber(SuiteAppApi.getClientNumberOld());

			SuiteAppApi.getCallBackSession().cierraSesion();
			//finish();
		}else {
			SuiteAppApi.getIntentToReturn().returnObjectFromApi(null);
		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	public void goBack() {

	}

}