package suitebancomer.aplicaciones.softtoken.classes.common.token;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomercoms.classes.common.PropertiesManager;
import android.database.Cursor;
import android.util.Log;

import com.bancomer.mbanking.softtoken.SuiteAppApi;

public final class SofttokenSession {
	// #region Variables.
	/**
	 * El IUM de Softtoken.
	 */
	private String ium;
	
	/**
	 * La semila para general el IUM.
	 */
	private long seed;
	
	/**
	 * EL número de telefono del cliente.
	 */
	private String username;
	
	/**
	 * Bandera que indica si la aplicación esta o no activada.
	 */
	private boolean softtokenActivated;
	
	/**
	 * El acceso a la base de datos.
	 */
	private SofttokenDBAdapter dbAdapter;
	// #endregion
	
	// #region Getters y Setters.
	/**
	 * @return El IUM de Softtoken.
	 */
	public String getIum() {
		return ium;
	}

	/**
	 * @param ium the ium to set
	 */
	public void setIum(final String ium) {
		this.ium = ium;
	}
	
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(final String username) {
		this.username = username;
	}

	/**
	 * @return La semila para general el IUM.
	 */
	public long getSeed() {
		return seed;
	}

	/**
	 * @param iumSeed La semila para general el IUM.
	 */
	public void setSeed(final long iumSeed) {
		this.seed = iumSeed;
	}
	
	/**
	 * @return Bandera que indica si la aplicación esta o no activada.
	 */
	public boolean isSofttokenActivated() {
		return softtokenActivated;
	}

	/**
	 * @param Bandera que indica si la aplicación esta o no activada.
	 */
	public void setSofttokenActivated(final boolean softtokenActivated) {
		this.softtokenActivated = softtokenActivated;
		PropertiesManager.getCurrent().setSofttokenActivated(softtokenActivated);
	}
	// #endregion

	// #region Singleton. 
	/**
	 * La instancia.
	 */
	private static SofttokenSession softtokenSession = null;
	
	/**
	 * @return Obtiene la instanc�a �nica de la clase.
	 */
	public static SofttokenSession getCurrent() {
		if(null == softtokenSession)
			softtokenSession = new SofttokenSession();
		return softtokenSession;
	}
	
	/**
	 * Constructor privado.
	 */
	private SofttokenSession() {
		seed = 0;
		ium = null;
		username = null;
		dbAdapter = null;
		softtokenActivated = PropertiesManager.getCurrent().getSofttokenActivated();
	}
	
	/**
	 * Elimina la instanc�a de la clase.
	 */
	public static void dispose() {
		if(null != softtokenSession) {
			softtokenSession.saveSottokenRecordStore();
			softtokenSession = null;
			System.gc();
		}
	}
	// #endregion
	
	// #region Manejo de la BD.
	/**
	 * Abre la base de datos.
	 * @return False si ocurrio alg�n problema, True de otro modo.
	 */
	private synchronized boolean openSottokenRecordStore() {
		try {
			dbAdapter = new SofttokenDBAdapter(SuiteAppApi.appContext);
			dbAdapter.open();
		} catch (Throwable t) {
			if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(), "Error while opening the record store.", t);
			return false;
		}
		return true;
	}
	
	/**
	 * Cierra la base de datos.
	 * @return True si la base de datos fue cerrada, False de otro modo.
	 */
	private synchronized boolean closeSottokenRecordStore() {
		if (dbAdapter != null) {
			try {
				dbAdapter.close();
				dbAdapter = null;
			} catch (Throwable t) {
				dbAdapter = null;
				if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(), "Error while closing the record store.", t);
				return false;
			}
			return true;

		} else {
			return true;
		}
	}
	
	/**
	 * Carga los datos de sesion desde la base de datos.
	 * @return False si ocurrio alg�n problema, True de otro modo.
	 */
	public synchronized boolean loadSottokenRecordStore() {
		boolean status = true;
		Cursor cursor = null;
		openSottokenRecordStore();
		try {            
			if (dbAdapter.getSessionPropertiesCount() != 0) {
				cursor = dbAdapter.selectAllSessionPropertyEntries();
				if (cursor.moveToFirst()) {
					final int propertyNameIndex = cursor.getColumnIndex(SofttokenConstants.DB_SESSION_TABLE_PROPERTY_NAME);
					final int propertyValueIndex = cursor.getColumnIndex(SofttokenConstants.DB_SESSION_TABLE_PROPERTY_VALUE);
					do {
						try {
							final String propName = cursor.getString(propertyNameIndex);
							
							if(propName.equals(SofttokenConstants.USERNAME_PROPERTY)) {
								this.username = cursor.getString(propertyValueIndex);
							} else if(propName.equals(SofttokenConstants.IUM_SEED_PROPERTY)) {
								this.seed = Long.parseLong(cursor.getString(propertyValueIndex));
								this.ium = Tools.buildIUM(username, seed, SuiteAppApi.appContext);
							}
						} catch (Throwable t)  {
							if(Server.ALLOW_LOG) Log.d(getClass().getSimpleName(), "Error while retrieving a property from db.", t);
						}
					}while(cursor.moveToNext());  
					cursor.close();
				}
			}
		} catch (Throwable t) {
			if(Server.ALLOW_LOG) Log.d(getClass().getSimpleName(), "Error while reading the db.", t);
			status = false;
		}finally{
			if(cursor!=null)
				cursor.close();
			closeSottokenRecordStore();       	  
		}
		
		return status;
	}
	
	/**
	 * Guarda la informacion de la sesion a la base de datos.
	 * @return False si ocurrio alg�n problema, True de otro modo.
	 */
	public synchronized boolean saveSottokenRecordStore() {
		boolean status = true;
		try {
			openSottokenRecordStore();
			dbAdapter.deleteSessionPropertiesContent();
			dbAdapter.insertSessionPropertyEntry(SofttokenConstants.USERNAME_PROPERTY, username);
			dbAdapter.insertSessionPropertyEntry(SofttokenConstants.IUM_SEED_PROPERTY, String.valueOf(seed));
		} catch(Throwable t) {
			if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(), "Error while saving the session data.", t);
			status = false;
		} finally {
			closeSottokenRecordStore();
		}
		
		return status;
	}
	// #endregion
}
