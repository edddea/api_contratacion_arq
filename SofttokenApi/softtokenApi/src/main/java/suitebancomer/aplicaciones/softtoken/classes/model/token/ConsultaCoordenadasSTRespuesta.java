package suitebancomer.aplicaciones.softtoken.classes.model.token;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by francisco on 03/05/16.
 */
public class ConsultaCoordenadasSTRespuesta implements ParsingHandler {

    /** El estatus de la respuesta. */
    private String statusResponse;


    /** Coordenadas de la respuesta. */
    private String coordenada;

    /** Digito vefiricador de la respuesta. */
    private String digitoVerificador;

    /**
     * Constructor por defecto.
     */
    public ConsultaCoordenadasSTRespuesta() {
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(final ParserJSON parser) throws IOException, ParsingException {
        this.statusResponse = parser
                .parseNextValue(ServerConstants.ESTADO);
        this.coordenada = parser
                .parseNextValue(ServerConstants.COORDENADA);
    }

    public String getCoordenada() {
        return coordenada;
    }

    public void setCoordenada(String coordenada) {
        this.coordenada = coordenada;
    }

    public String getStatusResponse() {
        return statusResponse;
    }

    public void setStatusResponse(String statusResponse) {
        this.statusResponse = statusResponse;
    }

    public String getDigitoVerificador() {
        return digitoVerificador;
    }

    public void setDigitoVerificador(String digitoVerificador) {
        this.digitoVerificador = digitoVerificador;
    }
}
