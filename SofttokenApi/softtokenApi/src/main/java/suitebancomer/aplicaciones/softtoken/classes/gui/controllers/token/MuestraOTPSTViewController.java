/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
/**
 * TODO: COMENTARIO DE LA CLASE
 *
 * @author Stefanini IT Solutions.
 */

public class MuestraOTPSTViewController extends SofttokenBaseViewController {
	//AMZ
	private SofttokenViewsController parentManager;
//	private GeneraOTPSTDelegate generarOtpDelegate;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_softtoken_otp_resultado);
		setTitle(R.string.softtoken_titulo, R.drawable.icono_st_activado);		
		//AMZ
				parentManager = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();							
						TrackingHelper.trackState("generar codigo", parentManager.estados);
		init();
	}
	
	private void init() {
		parentViewsController = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		parentViewsController.setCurrentActivityApp(this);
		setDelegate((GeneraOTPSTDelegate)parentViewsController.getBaseDelegateForKey(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID));
//		generarOtpDelegate = (GeneraOTPSTDelegate)getDelegate();
//		generarOtpDelegate.setOwnerController(this);
		
		scaleToCurrentScreen();
		
		String token = getIntent().getExtras().getString(SofttokenConstants.TOKEN_GENERADO);
		if(null == token)
			token = "";
		
		String tipoOtp = getIntent().getExtras().getString(SofttokenConstants.TIPO_OTP_A_MOSTRAR_PARAM);
		if(null == tipoOtp)
			tipoOtp = "";
			
		((TextView)findViewById(R.id.lblDescripcion)).setText((SofttokenConstants.OTP_TIEMPO.equals(tipoOtp)) ? 
				R.string.softtoken_otp_label_descripcion_tiempo : R.string.softtoken_otp_label_descripcion_registro);
		
		((EditText)findViewById(R.id.tbOtp)).setText(token);
	}
	
	private void scaleToCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.rootLayout));
		guiTools.scale(findViewById(R.id.lblOperacionExitosa), true);
		guiTools.scale(findViewById(R.id.lblOtp), true);
		guiTools.scale(findViewById(R.id.tbOtp), true);
		guiTools.scale(findViewById(R.id.lblDescripcion), true);
		guiTools.scale(findViewById(R.id.btnMenu));
	}
	
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		return;
	}
	
	public void onBtnMenuClick(final View sender) {
		SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showPantallaGeneraOTPST(true);
	}

    @Override
    public boolean onKeyDown(final int keyCode, final KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showPantallaGeneraOTPST(true);
        }
        return super.onKeyDown(keyCode, event);

    }

}
