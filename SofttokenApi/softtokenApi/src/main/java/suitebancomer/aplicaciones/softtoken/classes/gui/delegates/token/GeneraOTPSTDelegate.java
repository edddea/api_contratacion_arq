/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import net.otpmt.mtoken.MToken;
import net.otpmt.mtoken.MTokenCore;
import net.otpmt.mtoken.TransactionSigning;

import br.com.opencs.bincodec.ArrayAlphabet;
import br.com.opencs.bincodec.Base2NCodec;
import br.com.opencs.bincodec.random.RandomAlphabetGenerator;
import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenSession;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

public class GeneraOTPSTDelegate extends SofttokenBaseDelegate {
	public static final long GENERAR_OTP_DELEGATE_ID = -7359658479361200492L;
	private static final String TAG = GeneraOTPSTDelegate.class.getSimpleName();
	private BaseViewControllerCommons ownerController;
	private ContratacionSTDelegate delegateContatacion;
	
	public BaseViewControllerCommons getOwnerController() {
		return ownerController;
	}
	
	public void setOwnerController(final BaseViewControllerCommons ownerController) {
		this.ownerController = ownerController;
	}

	public GeneraOTPSTDelegate() {
		delegateContatacion = new ContratacionSTDelegate(this);
	}
	
	public GeneraOTPSTDelegate(final ContratacionSTDelegate contratacionSTDelegate) {
		delegateContatacion=contratacionSTDelegate;
	}

	public boolean borraToken() {
		try {
			
			final SofttokenSession session = SofttokenSession.getCurrent();
            final Session sessionApp = Session.getInstance(SuiteAppApi.appContext);
			// Intergracion KeyChainAPI
            final KeyStoreWrapper kswrapper = sessionApp.getKeyStoreWrapper();
			kswrapper.storeValueForKey(Constants.CENTRO, " ");
			// ---
			session.setSofttokenActivated(false);
			SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().resetApplicationData();
            final SofttokenActivationBackupManager manager = SofttokenActivationBackupManager.getCurrent();
			if(manager.existsABackup())
				manager.deleteBackup();
			return true;
		} catch(Exception e) {
			return false;
		}
	}

	public void inicializaCore() {
		if(!delegateContatacion.doStartApplication())
			SuiteAppApi.getInstanceApi().closeSofttokenAppApi();
	}
	
	public String generaOTPTiempo() 
	{
		MToken token;
        final MTokenCore core = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getCore();
		boolean logged = core.isLogged();
		if(!logged)
			logged = delegateContatacion.doLogin();
		
		token = this.getLoadedToken();
		if (token != null) {
			token.calculateNextTimeOTP(true);
			if(Server.ALLOW_LOG) Log.i(TAG,token.getLastOTP());
		} else {
			this.doNoToken();
		}
		return token.getLastOTP();
	}
	
	public String generaOTPChallenger(final String challenge)
	{
		MToken token;
		token = this.getLoadedToken();
		if (token != null) {
			token.calculateNextChallengeResponseOTP(challenge);
			if(Server.ALLOW_LOG) Log.e(TAG,token.getLastOTP());
		} else {
			this.doNoToken();
		}
		return token.getLastOTP();

	}

	protected MToken getLoadedToken(){
		if (SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().isLogged())
		{
			return SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getCore().getLoadedToken();
		} else {
			return null;
		}
	}
	
	protected void doNoToken(){
		try {
			ownerController.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					ownerController.showInformationAlert(R.string.msg_no_token);
				}
			});
		}catch (NullPointerException e){
			if(Server.ALLOW_LOG) Log.d("GeneraOTPSTDelegate", "Unable to proceed. Select the token and try again.: " );
		}
	}

	public void validaDatos(final String cuenta) {
		if(cuenta.length() < SofttokenConstants.TOKEN_SEED_REGISTER_LENGTH) {
			ownerController.showInformationAlert(R.string.softtoken_otp_challenge_error_registro_corto);
		} else {
			((SofttokenViewsController)ownerController.getParentViewsController()).showPantallaMuestraOTPST(generaOTPChallenger(cuenta), SofttokenConstants.OTP_REGISTRO);
		}
	}
	
	public static String generarOtpTiempo() {
		if(Server.ALLOW_LOG) Log.d("GeneraOTPSTDelegate", "Se genero una otp por tiempo");
		String result = "";
        final ContratacionSTDelegate contDelegate = new ContratacionSTDelegate();
		contDelegate.generaTokendelegate.inicializaCore();
		result = contDelegate.generaTokendelegate.generaOTPTiempo();
		return (Tools.isEmptyOrNull(result)) ? null : result;
	}
	
	public static String generarOtpChallenge(final String challenge) {
		if(challenge.length() != 5) {
			if(Server.ALLOW_LOG) Log.e("GeneraOTPSTDelegate", "El numero de challenge '" + challenge + "' no tiene exactamente 5 digitos.");
			return null;
		}
		if(Server.ALLOW_LOG) Log.d("GeneraOTPSTDelegate", "Se genero una otp challenge para: " + challenge);
		String result = "";
        final ContratacionSTDelegate contDelegate = new ContratacionSTDelegate();
		contDelegate.generaTokendelegate.inicializaCore();
		result = contDelegate.generaTokendelegate.generaOTPChallenger(challenge);
		return (Tools.isEmptyOrNull(result)) ? null : result;
	}
	
	public void borrarDatosConAlerta() {
		ownerController.showYesNoAlert(R.string.label_information, 
									   R.string.softtoken_menu_principal_aviso_borrar_datos, 
									   R.string.softtoken_menu_principal_ok_label_borrar_datos_alert, 
									   new OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				if(borraToken())
					SuiteAppApi.getIntentToReturn().returnObjectFromApi(new String("BorraToken"));
					//SuiteAppApi.getInstanceApi().closeSofttokenAppApi();
			}
		});
	}


	public String generarOtpTiempoApi() {
		if(Server.ALLOW_LOG) Log.d("GeneraOTPSTDelegate", "Se genero una otp por tiempo");
		String result = "";
        final ContratacionSTDelegate contDelegate = new ContratacionSTDelegate();
		contDelegate.generaTokendelegate.inicializaCore();
		result = contDelegate.generaTokendelegate.generaOTPTiempo();
		return (Tools.isEmptyOrNull(result)) ? null : result;
	}

	public String generarOtpChallengeApi(final String challenge) {
		if(challenge.length() != 5) {
			if(Server.ALLOW_LOG) Log.e("GeneraOTPSTDelegate", "El numero de challenge '" + challenge + "' no tiene exactamente 5 digitos.");
			return null;
		}
		if(Server.ALLOW_LOG) Log.d("GeneraOTPSTDelegate", "Se genero una otp challenge para: " + challenge);
		String result = "";
        final ContratacionSTDelegate contDelegate = new ContratacionSTDelegate();
		contDelegate.generaTokendelegate.inicializaCore();
		result = contDelegate.generaTokendelegate.generaOTPChallenger(challenge);
		return (Tools.isEmptyOrNull(result)) ? null : result;
	}

	public String generaOTPFromQR(final String cadenaQR){
		String valueOtp ="";

		try{
			byte dst[] = cadenaQR.getBytes();
			dst[3]=2;
			if(Server.ALLOW_LOG){
				{
					for(int i=0; i < dst.length;i++)
						Log.i("QRCode", "cadena desofuscada bytes hex: "+Integer.toHexString(dst[i]));
				}
			}

            final TransactionSigning transactionSigning = new TransactionSigning(dst);
			if(Server.ALLOW_LOG){
				Log.i("QRCode", "VALUE0: "+transactionSigning.getValue(0));
				Log.i("QRCode", "VALUE1: "+transactionSigning.getValue(1));
				Log.i("QRCode", "VALUE2: " + transactionSigning.getValue(2));
				Log.i("QRCode", "VALUE3: " + transactionSigning.getValue(3));
			}
            final String sTrans = transactionSigning.getChallenge(); //es el challenge, no es la otp final


			if(Server.ALLOW_LOG) Log.w("QRCode","Challenge: "+ sTrans);

			valueOtp = generaOTPChallenger(sTrans);

			if(Server.ALLOW_LOG) Log.w("QRCode","ValueOTP: "+ valueOtp);


		} catch (Exception e) {
			if(Server.ALLOW_LOG){
				e.printStackTrace();
				e.getLocalizedMessage();
				Log.e("token", "Error: "+e);
			}
		}
		return valueOtp;
	}


	public String [] obtieneDatosFromQR(final String cadenaQR){
		String []datosQR = new String[5];
		try {
			//desofuscar
			if(Server.ALLOW_LOG) Log.w("QRCode", "cadena ofuscada"+ cadenaQR);
            final char decAlphabet[] = RandomAlphabetGenerator.generateRandom(SofttokenConstants.OPTICA_SEED, SofttokenConstants.OPTICA_ROUNDS, RandomAlphabetGenerator.QRCODE_ALPHANUMERIC_NO_SPACE, 32);
            final Base2NCodec decoder = new Base2NCodec(new ArrayAlphabet(decAlphabet));

            final byte dst[] = decoder.decode(cadenaQR);

			if(Server.ALLOW_LOG){
				for(int i=0; i < dst.length;i++)
				{
					Log.i("QRCode", "cadena desofuscada bytes hex: "+Integer.toHexString(dst[i]));
				}
			}
            final TransactionSigning transactionSigning = new TransactionSigning(dst);

			datosQR [0] = transactionSigning.getValue(2);
			datosQR [1] = transactionSigning.getValue(3);
			datosQR [2] = transactionSigning.getValue(0);
			datosQR [3] = transactionSigning.getValue(1);
			datosQR [4] = new String(dst);

			if(Server.ALLOW_LOG){
				Log.i("QRCode", "VALUE0: " + transactionSigning.getValue(0));
				Log.i("QRCode", "VALUE1: "+transactionSigning.getValue(1));
				Log.i("QRCode", "VALUE2: " + transactionSigning.getValue(2));
				Log.i("QRCode", "VALUE3: "+transactionSigning.getValue(3));
			}

		} catch (Exception e) {
			if(Server.ALLOW_LOG) {
				e.printStackTrace();
				e.getLocalizedMessage();
				Log.e("token", "Error: " + e);
			}
		}
		return datosQR;
	}

}
