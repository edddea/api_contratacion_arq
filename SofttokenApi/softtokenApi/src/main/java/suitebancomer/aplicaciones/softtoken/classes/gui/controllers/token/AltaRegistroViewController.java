package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import suitebancomercoms.classes.common.GuiTools;

/**
 * Created by RPEREZ on 03/07/15.
 */
public class AltaRegistroViewController extends SofttokenBaseViewController implements View.OnClickListener {

    public TextView lblresult;
    public TextView lblresult2;
    public TextView lblRegisterUp;
    public TextView lblRegisterCenter;
    public TextView lblRegisterDown;
    public Button btnRegisterConfirm;
    public Button btnRegisterCancel;
    public String codQR="";

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_softtoken_alta_registro);
        setTitle(R.string.softtoken_titulo, R.drawable.icono_st_activado);
        relationUI();
        init();

    }

    private void init() {
        parentViewsController =  SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
        parentViewsController.setCurrentActivityApp(this);
        setDelegate(parentViewsController.getBaseDelegateForKey(GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID));
//		generarOtpDelegate = (GeneraOTPSTDelegate)getDelegate();
//		generarOtpDelegate.setOwnerController(this);

        scaleToCurrentScreen();

        final String result1 	= getIntent().getExtras().getString(SofttokenConstants.RESULT_REGISTER1);
        final String result2 	= getIntent().getExtras().getString(SofttokenConstants.RESULT_REGISTER2);
        final String type		= getIntent().getExtras().getString(SofttokenConstants.TYPE_OPERATION);
        //otp 		= getIntent().getExtras().getString(SofttokenConstants.UTP_VALUE);
        codQR      = getIntent().getExtras().getString(SofttokenConstants.CODQR_VALUE);

        if(result1 != null && result2 != null && type != null){
            lblresult.setText(Html.fromHtml("<u>"+result1+"</u>"));
            lblresult2.setText(result2);
            Log.i("alta","Result2: "+result2);


            if(type.equals("01")){
                //Terceros Bancomer View
                lblRegisterUp.setText(getResources().getString(R.string.softtoken_otp_alta_registro_cuenta));
                lblRegisterCenter.setText(getResources().getString(R.string.softtoken_otp_alta_registro_nombre));
                lblRegisterDown.setText(getResources().getString(R.string.softtoken_otp_alta_registro_transferencias));

            }else if(type.equals("02")){
                //Terceros Interbancarios View
                lblRegisterUp.setText(getResources().getString(R.string.softtoken_otp_alta_registro_cuenta));
                lblRegisterCenter.setText(getResources().getString(R.string.softtoken_otp_alta_registro_banco));
                lblRegisterDown.setText(getResources().getString(R.string.softtoken_otp_alta_registro_transferencias));

            }else if(type.equals("03")){
                //Servicios View
                lblRegisterUp.setText(getResources().getString(R.string.softtoken_otp_alta_registro_convenio));
                lblRegisterCenter.setText(getResources().getString(R.string.softtoken_otp_alta_registro_servicio));
                lblRegisterDown.setText(getResources().getString(R.string.softtoken_otp_alta_registro_pago_servicios));

            }else if(type.equals("04") || type.equals("05")){
                //Dinero Movil y Tiempo Compra Aire View
                lblRegisterUp.setText(getResources().getString(R.string.softtoken_otp_alta_registro_celular));
                lblRegisterCenter.setText(getResources().getString(R.string.softtoken_otp_alta_registro_compania));
                lblRegisterDown.setText(getResources().getString(R.string.softtoken_otp_alta_registro_envios_comprasTA));
            }

        }else {
            SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showPantallaGeneraOTPST(true);
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.softtoken_otp_alta_registro_failure),
                    Toast.LENGTH_LONG).show();
        }

    }

    public void relationUI(){
        lblresult = (TextView) findViewById(R.id.resultAltaRegistro1);
        lblresult2 = (TextView) findViewById(R.id.resultAltaRegistro2);
        lblRegisterUp = (TextView) findViewById(R.id.lblAltaRegistroUp);
        lblRegisterCenter = (TextView) findViewById(R.id.lblAltaRegistroCenter);
        lblRegisterDown = (TextView) findViewById(R.id.lblAltaRegistroDown);
        btnRegisterConfirm = (Button) findViewById(R.id.btnAltaRegistroConfirmar);
        btnRegisterCancel = (Button) findViewById(R.id.btnAltaRegistroCancelar);

        btnRegisterConfirm.setOnClickListener(this);
        btnRegisterCancel.setOnClickListener(this);
    }

    private void scaleToCurrentScreen() {
        final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.lblAltaRegistro), true);
        guiTools.scale(findViewById(R.id.lblAltaRegistroUp), true);
        guiTools.scale(findViewById(R.id.resultAltaRegistro1), true);
        guiTools.scale(findViewById(R.id.lblAltaRegistroCenter), true);
        guiTools.scale(findViewById(R.id.resultAltaRegistro2), true);
        guiTools.scale(findViewById(R.id.lblAltaRegistroDown), true);
        guiTools.scale(findViewById(R.id.btnAltaRegistroConfirmar));
        guiTools.scale(findViewById(R.id.btnAltaRegistroCancelar));
    }

    @Override
    public void onClick(final View v) {
        if(v.equals(btnRegisterConfirm)) {
            final GeneraOTPSTDelegate otpGen = new GeneraOTPSTDelegate();
            final String otp = otpGen.generaOTPFromQR(codQR);
            ((SofttokenViewsController) getParentViewsController()).showPantallaMuestraOTPST(otp, SofttokenConstants.OTP_TIEMPO);

            //((SofttokenViewsController)getParentViewsController()).showPantallaMuestraOTPST(otp, SofttokenConstants.OTP_TIEMPO);
        }
        if(v.equals(btnRegisterCancel)) {
            SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showPantallaGeneraOTPST(true);

        }

    }



}
