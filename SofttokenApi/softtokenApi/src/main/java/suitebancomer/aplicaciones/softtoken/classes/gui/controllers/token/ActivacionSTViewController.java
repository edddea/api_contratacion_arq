/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class ActivacionSTViewController extends SofttokenBaseViewController {
	private ContratacionSTDelegate contratacionDelegate;
	private EditText tbClave;
	private ImageButton btnLlamar;
	Button btnConfirmar;
	//AMZ
	private SofttokenViewsController parentManager;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppApi.getResourceId("layout_softtoken_clave_activacionapi", "layout"));
		setTitle(R.string.softtoken_activacion_titulo, R.drawable.icono_st_activado);

		//AMZ
		parentManager = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		TrackingHelper.trackState("activacion clave", parentManager.estados);
		init();
	}

	@Override
	protected void onResume() {
		parentViewsController = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController();
		parentViewsController.setCurrentActivityApp(this);
		setDelegate((ContratacionSTDelegate) parentViewsController.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
		contratacionDelegate = (ContratacionSTDelegate) getDelegate();

		if (contratacionDelegate == null) {

			parentViewsController = SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getViewsController();
			parentViewsController.setCurrentActivityApp(this);
			setDelegate((ContratacionSTDelegate) parentViewsController.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
			contratacionDelegate = (ContratacionSTDelegate) getDelegate();
		}


		contratacionDelegate.setOwnerController(this);

		if (contratacionDelegate.isCargarDatos()) {
			contratacionDelegate.cargarDatosDelRespaldo();
		}

		if (!contratacionDelegate.isEA12()) {
			muestraAvisoActivacion(this);
			contratacionDelegate.setEA12(Boolean.TRUE);
		}

		super.onResume();
	}

	private void init() {
		findViews();
		scaleToCurrentScreen();

		// Pone el texto subrayado
		final TextView textView = (TextView) findViewById(SuiteAppApi.getResourceId("lblReenviarCodigo", "id"));
		final SpannableString content = new SpannableString(textView.getText());
		content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
		textView.setText(content);
	}

	private void findViews() {
		tbClave = (EditText) findViewById(SuiteAppApi.getResourceId("tbClaveActivacion", "id"));
		btnConfirmar=(Button) findViewById(SuiteAppApi.getResourceId("btnConfirmar", "id"));

		btnLlamar = (ImageButton) findViewById(SuiteAppApi.getResourceId("btnSolicitaCodigo", "id"));
		tbClave.addTextChangedListener(new TextWatcher() {

										   @Override
										   public void onTextChanged(final CharSequence cs, final int arg1, final int arg2, final int arg3) {
											   // Empty method
										   }

										   @Override
										   public void beforeTextChanged(final CharSequence arg0, final int arg1, final int arg2, final int arg3) {
											   // Empty method
										   }

										   @Override
										   public void afterTextChanged(final Editable arg0) {
											   btnConfirmar.setEnabled((tbClave.getText().toString().length() == 10));

										   }

									   }
		);
		btnConfirmar.setEnabled(false);
	}

	private void scaleToCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		guiTools.scale(findViewById(SuiteAppApi.getResourceId("rootLayout", "id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblClave", "id")), true);
		guiTools.scale(tbClave, true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblInstrucciones", "id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblReenviarCodigo", "id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("btnConfirmar", "id")));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see suitebancomercoms.classes.gui.controllers.BaseViewController#
	 * processNetworkResponse(int,
	 * suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		contratacionDelegate.analyzeResponse(operationId, response);
	}

	/**
	 * Opcion solicitar codigo por llamada
	 *
	 * @param sender la opciOn seleccionada
	 */
	public void onBtnLlamarCodio(final View sender) {
		final Intent sIntent=new Intent(Intent.ACTION_CALL,Uri.parse(String.format("tel:%s", Uri.encode(this.getString(R.string.activacion_numero_solicita_codigo_st)))));
		sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(sIntent);
	}

	/**
	 * Se selecciona la opci�n Continuar.
	 *
	 * @param sender la opci�n seleccionada
	 */
	public void onBtnCorfirmarClick(final View sender) {
		contratacionDelegate.activacionST(tbClave.getText().toString());
		try {
			this.finalize();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void onReenviarClaveClick(final View view) {
		contratacionDelegate.mostrarAlertaCambio();
		//contratacionDelegate.reenviarClaveActivacion();
	}


	@Override
	public void onPause() {

		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	public void showCodigoIncorrecto() {
		final Dialog contactDialog = new Dialog(this);
        final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.codigo_incorrecto_st,null);
		contactDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// make dialog itself transparent
		contactDialog.setCancelable(false);
		contactDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		contactDialog.setContentView(layout);
        final int width = (int)(getResources().getDisplayMetrics().widthPixels * 0.90);
		contactDialog.getWindow().setLayout(width, DrawerLayout.LayoutParams.WRAP_CONTENT);

		final Button intentarNuevamente = (Button) layout.findViewById(R.id.btnintentarnuevamente);
		final Button nuevoCodigo = (Button) layout.findViewById(R.id.btnnuevocodigo);


		//contactDialog.setTitle(ctx.getString(R.string.menuSuite_menuTitle));
		intentarNuevamente.setHeight(nuevoCodigo.getHeight());

		intentarNuevamente.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(final View v) {
						intentarNuevamente();
						contactDialog.dismiss();
					}
				}
		);
		nuevoCodigo.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(final View v) {
						generarNuevoCodigo();
						contactDialog.dismiss();
					}
				}
		);
		contactDialog.show();
	}

	private void generarNuevoCodigo(){
		contratacionDelegate.reenviarClaveActivacion();

	}

	private void intentarNuevamente(){
		tbClave.setText("");
		btnConfirmar.setEnabled(false);
	}

	private void muestraAvisoActivacion(final Context apContex){
		final Dialog contactDialog = new Dialog(apContex);
        final LayoutInflater inflater = (LayoutInflater) apContex.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.aviso_ivr_st, null);
		if(contratacionDelegate.getSoftToken().getNumeroTarjeta().substring(0, 4).equals("0017")) {
			((ImageView) layout.findViewById(R.id.imglinea)).setBackgroundResource(R.drawable.an_im_godinbmovil_pm);
			TextView txtAvisdo=(TextView)layout.findViewById(R.id.avisoivr);
			txtAvisdo.setText("En breve recibirás un mensaje con el código de activación para continuar el registro de tu Token Móvil Empresarial.");

		}
		contactDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// make dialog itself transparent
		contactDialog.setCancelable(false);
		contactDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		contactDialog.setContentView(layout);

        final int width = (int)(getResources().getDisplayMetrics().widthPixels * 0.90);
		contactDialog.getWindow().setLayout(width, DrawerLayout.LayoutParams.WRAP_CONTENT);

        final Button aceptarIvr = (Button) layout.findViewById(R.id.btnAceptarivr);
		aceptarIvr.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(final View v) {
						//se desactiva la llamada
						//final Intent sIntent=new Intent(Intent.ACTION_CALL,Uri.parse(String.format("tel:%s", Uri.encode(contratacionDelegate.getOwnerController().getString(R.string.activacion_numero_solicita_codigo_st)))));
						//sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						//startActivity(sIntent);
						contactDialog.dismiss();
					}
				}
		);
		//contactDialog.getWindow().setLayout(DrawerLayout.LayoutParams.WRAP_CONTENT, DrawerLayout.LayoutParams.WRAP_CONTENT);
		contactDialog.show();
	}

	@Override
	public void onBackPressed() {
		TrackingHelper.touchAtrasState();
	}


}