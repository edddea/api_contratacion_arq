package suitebancomer.aplicaciones.softtoken.classes.model.token;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class AutenticacionSTRespuesta implements ParsingHandler {
	private String numeroSerie;
	private String estado;
	private String nombreToken;

	
	/**
	 * @return the numeroSerie
	 */
	public String getNumeroSerie() {
		return numeroSerie;
	}

	/**
	 * @param numeroSerie the numeroSerie to set
	 */
	public void setNumeroSerie(final String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}


	public String getEstado() {
		return estado;
	}

	public void setEstado(final String estado) {
		this.estado = estado;
	}

	/**
	 * @return the nombreToken
	 */
	public String getNombreToken() {
		return nombreToken;
	}



	/**
	 * @param nombreToken the nombreToken to set
	 */
	public void setNombreToken(final String nombreToken) {
		this.nombreToken = nombreToken;
	}
	
	public AutenticacionSTRespuesta() {
		numeroSerie = "";    
		nombreToken = "";
		estado="";
	}

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		numeroSerie = parser.parseNextValue("numeroSerie", false);
		nombreToken = parser.parseNextValue("nombreToken");
		estado= parser.parseNextValue("estado");
		
		
		
	}

}
