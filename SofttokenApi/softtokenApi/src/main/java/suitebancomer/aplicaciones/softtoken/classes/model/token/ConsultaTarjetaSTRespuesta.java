package suitebancomer.aplicaciones.softtoken.classes.model.token;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ConsultaTarjetaSTRespuesta implements ParsingHandler {

	/** El identificador de la version del serial. */
	private static final long serialVersionUID = 2153495524447904658L;

	/** El estatus de Bmovil. */
	private String estatusBmovil;

	/**
	 * La validacion del numero de telefono y compannia, de alertas y de Bmovil.
	 */
	private String validacionAlertas;

	/** Numero de telufono asociado a Alertas. */
	private String numeroAlertas;

	/** Compannia telefonica asociada a Alertas. */
	private String companiaAlertas;

	/** Numero del cliente. */
	private String numeroCliente;

	/** Indicador de Softtoken. */
	private String indicadorContratacion;

	/** Numero de serie de Softtoken. */
	private String numSerieToken;

	/** Tipo de Instrumento. */
	private String tipoInstrumento;

	/** Correo Electronico del cliente. */
	private String correoElectronico;

	/** Nombre del cliente. */
	private String nombreCliente;
	

	/** switchEnrolamiento. */
	private String switchEnrolamiento;
	
	/** dispositivoFisico */
	private String dispositivoFisico;

	private String estatusDispositivo;
	

	/**
	 * Constructor por defecto.
	 */
	public ConsultaTarjetaSTRespuesta() {
	}

	/**
	 * Obtiene el estatus de Bmovil.
	 * 
	 * @return el estatus de Bmovil
	 */
	public String getEstatusBmovil() {
		return estatusBmovil;
	}

	/**
	 * Establece el estatus de Bmovil.
	 * 
	 * @param estatusBmovil
	 *            el estatus de Bmovil a establecer
	 */
	public void setEstatusBmovil(final String estatusBmovil) {
		this.estatusBmovil = estatusBmovil;
	}

	/**
	 * Obtiene la validacion de las alertas.
	 * 
	 * @return la validacion de las alertas
	 */
	public String getValidacionAlertas() {
		return validacionAlertas;
	}

	/**
	 * Establece la validacion de las alertas.
	 * 
	 * @param validacionAlertas
	 *            la validacion de las alertas a establecer
	 */
	public void setValidacionAlertas(final String validacionAlertas) {
		this.validacionAlertas = validacionAlertas;
	}

	/**
	 * Obtiene el numero de alertas.
	 * 
	 * @return el numero de alertas
	 */
	public String getNumeroAlertas() {
		return numeroAlertas;
	}

	/**
	 * Establece el numero de alertas.
	 * 
	 * @param numeroAlertas
	 *            el numero de alertas a establecer
	 */
	public void setNumeroAlertas(final String numeroAlertas) {
		this.numeroAlertas = numeroAlertas;
	}

	/**
	 * Obtiene la compannia de alertas.
	 * 
	 * @return la compannia de alertas
	 */
	public String getCompaniaAlertas() {
		return companiaAlertas;
	}

	/**
	 * Establece la compannia de alertas.
	 * 
	 * @param companiaAlertas
	 *            la compannia de alertas a establecer
	 */
	public void setCompaniaAlertas(final String companiaAlertas) {
		this.companiaAlertas = companiaAlertas;
	}

	/**
	 * Obtiene el numero del cliente.
	 * 
	 * @return el numero del cliente
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}

	/**
	 * Establece el numero del cliente.
	 * 
	 * @param nmeroCliente
	 *            el numero del cliente a establecer
	 */
	public void setNumeroCliente(final String nmeroCliente) {
		this.numeroCliente = nmeroCliente;
	}

	/**
	 * Obtiene el indicador de contratacion.
	 * 
	 * @return el indicador de contratacion
	 */
	public String getIndicadorContratacion() {
		return indicadorContratacion;
	}

	/**
	 * Establece el indicador de contratacion.
	 * 
	 * @param indicadorContratacion
	 *            el indicador de contratacion a establecer
	 */
	public void setIndicadorContratacion(final String indicadorContratacion) {
		this.indicadorContratacion = indicadorContratacion;
	}

	/**
	 * Obtiene el numero de serie del token.
	 * 
	 * @return el numero de serie del token
	 */
	public String getNumSerieToken() {
		return numSerieToken;
	}

	/**
	 * Establece el numero de serie del token
	 * 
	 * @param numSerieToken
	 *            el numero de serie del token a establecer.
	 */
	public void setNumSerieToken(final String numSerieToken) {
		this.numSerieToken = numSerieToken;
	}

	/**
	 * Obtiene el tipo de instrumento.
	 * 
	 * @return el tipo de instrumento
	 */
	public String getTipoInstrumento() {
		return tipoInstrumento;
	}

	/**
	 * Establece el tipo de instrumento.
	 * 
	 * @param tipoInstrumento
	 *            el tipo de instrumento a establecer
	 */
	public void setTipoInstrumento(final String tipoInstrumento) {
		this.tipoInstrumento = tipoInstrumento;
	}

	/**
	 * Obtiene el correo electronico.
	 * 
	 * @return el correo electronico
	 */
	public String getCorreoElectronico() {
		return correoElectronico;
	}

	/**
	 * Establece el correo electronico.
	 * 
	 * @param correoElectronico
	 *            el correo electronico a establecer
	 */
	public void setCorreoElectronico(final String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	/**
	 * Obtiene el nombre del cliente.
	 * 
	 * @return el nombre del cliente
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}

	/**
	 * Establece el nombre del cliente.
	 * 
	 * @param nombreCliente
	 *            el nombre del cliente a establecer
	 */
	public void setNombreCliente(final String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	/**
	 * @return el switchEnrolamiento
	 */
	public String getSwitchEnrolamiento() {
		return switchEnrolamiento;
	}

	/**
	 * @param Establece el switchEnrolamiento
	 */
	public void setSwitchEnrolamiento(final String switchEnrolamiento) {
		this.switchEnrolamiento = switchEnrolamiento;
	}

	/**
	 * @return el dispositivoFisico
	 */
	public String getDispositivoFisico() {
		return dispositivoFisico;
	}

	/**
	 * @param Establece el dispositivoFisico
	 */
	public void setDispositivoFisico(final String dispositivoFisico) {
		this.dispositivoFisico = dispositivoFisico;
	}

	public String getEstatusDispositivo() {
		return estatusDispositivo;
	}

	public void setEstatusDispositivo(final String estatusDispositivo) {
		this.estatusDispositivo = estatusDispositivo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler#process(
	 * suitebancomercoms.aplicaciones.bmovil.classes.io.Parser)
	 */
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler#process(
	 * suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON)
	 */
	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		this.estatusBmovil = parser
				.parseNextValue(ServerConstants.ESTATUS_BMOVIL);
		this.validacionAlertas = parser
				.parseNextValue(ServerConstants.VALIDACION_ALERTAS);
		this.numeroAlertas = parser
				.parseNextValue(ServerConstants.NUMERO_ALERTAS);
		this.companiaAlertas = parser
				.parseNextValue(ServerConstants.COMPANNIA_ALERTAS);
		this.numeroCliente = parser
				.parseNextValue(ServerConstants.NUMERO_CLIENTE, false);
		this.indicadorContratacion = parser
				.parseNextValue(ServerConstants.INDICADOR_CONTRATACION);
		this.numSerieToken = parser
				.parseNextValue(ServerConstants.NUM_SERIE_TOKEN);
		this.tipoInstrumento = parser
				.parseNextValue(ServerConstants.TIPO_INSTRUMENTO);
		this.correoElectronico = parser
				.parseNextValue(ServerConstants.CORREO_ELECTRONICO);
		this.nombreCliente = parser
				.parseNextValue(ServerConstants.NOMBRE_CLIENTE, false);
		this.switchEnrolamiento = parser
				.parseNextValue(ServerConstants.SWITCH_ENROLAMIENTO);
		this.dispositivoFisico = parser
				.parseNextValue(ServerConstants.DISPOSITIVO_FISICO);
		this.estatusDispositivo = parser
				.parseNextValue(ServerConstants.ESTATUS_DISPOSITIVO);
	}

}