package suitebancomer.aplicaciones.softtoken.classes.model.token;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;


public class Preregistro implements ParsingHandler {

	private String numero_cliente = null;

	private String numero_celular = null;

	private String instrumento_seguridad = null;

	private String numero_clave_activacion=null;

	private String nip = null;

	private String OTP1 = null;

	private String OTP2 = null;

	private String indicador_solicitud = null;

	private String compania_telefonica = null;

	private String email=null;

	private String numero_tarjeta=null;

	private String numero_serie=null;

	private String nombre_token=null;

	/**
	 * @return the nombre_token
	 */
	 public String getNombre_token() {
		return nombre_token;
	 }

	 /**
	  * @param nombre_token the nombre_token to set
	  */
	 public void setNombre_token(final String nombre_token) {
		 this.nombre_token = nombre_token;
	 }

	 /**
	  * @return the numero_celular
	  */
	 public String getNumero_celular() {
		 return numero_celular;
	 }

	 /**
	  * @param numero_celular the numero_celular to set
	  */
	 public void setNumero_celular(final String numero_celular) {
		 this.numero_celular = numero_celular;
	 }

	 /**
	  * @return the numero_cliente
	  */
	 public String getNumero_cliente() {
		 return numero_cliente;
	 }

	 /**
	  * @param numero_cliente the numero_cliente to set
	  */
	 public void setNumero_cliente(final String numero_cliente) {
		 this.numero_cliente = numero_cliente;
	 }

	 /**
	  * @return the instrumento_seguridad
	  */
	 public String getInstrumento_seguridad() {
		 return instrumento_seguridad;
	 }

	 /**
	  * @param instrumento_seguridad the instrumento_seguridad to set
	  */
	 public void setInstrumento_seguridad(final String instrumento_seguridad) {
		 this.instrumento_seguridad = instrumento_seguridad;
	 }

	 /**
	  * @return the indicador_solicitud
	  */
	 public String getIndicador_solicitud() {
		 return indicador_solicitud;
	 }

	 /**
	  * @param indicador_solicitud the indicador_solicitud to set
	  */
	 public void setIndicador_solicitud(final String indicador_solicitud) {
		 this.indicador_solicitud = indicador_solicitud;
	 }

	 /**
	  * @return the compania_telefonica
	  */
	 public String getCompania_telefonica() {
		 return compania_telefonica;
	 }

	 /**
	  * @param compania_telefonica the compania_telefonica to set
	  */
	 public void setCompania_telefonica(final String compania_telefonica) {
		 this.compania_telefonica = compania_telefonica;
	 }

	 /**
	  * @return the email
	  */
	 public String getEmail() {
		 return email;
	 }

	 /**
	  * @param email the email to set
	  */
	 public void setEmail(final String email) {
		 this.email = email;
	 }

	 /**
	  * @return the oTP1
	  */
	 public String getOTP1() {
		 return OTP1;
	 }

	 /**
	  * @param oTP1 the oTP1 to set
	  */
	 public void setOTP1(final String oTP1) {
		 OTP1 = oTP1;
	 }

	 /**
	  * @return the oTP2
	  */
	 public String getOTP2() {
		 return OTP2;
	 }

	 /**
	  * @param oTP2 the oTP2 to set
	  */
	 public void setOTP2(final String oTP2) {
		 OTP2 = oTP2;
	 }

	 /**
	  * @return the nip
	  */
	 public String getNip() {
		 return nip;
	 }

	 /**
	  * @param nip the nip to set
	  */
	 public void setNip(final String nip) {
		 this.nip = nip;
	 }

	 /**
	  * @return the numero_serie
	  */
	 public String getNumero_serie() {
		 return numero_serie;
	 }

	 /**
	  * @param numero_serie the numero_serie to set
	  */
	 public void setNumero_serie(final String numero_serie) {
		 this.numero_serie = numero_serie;
	 }
	 
	 private String opt_token=null;

	 /**
	  * @return the opt_token
	  */
	 public String getOpt_token() {
		 return opt_token;
	 }

	 /**
	  * @param opt_token the opt_token to set
	  */
	 public void setOpt_token(final String opt_token) {
		 this.opt_token = opt_token;
	 }

	 /**
	  * @return the numero_tarjeta
	  */
	 public String getNumero_tarjeta() {
		 return numero_tarjeta;
	 }

	 /**
	  * @param numero_tarjeta the numero_tarjeta to set
	  */
	 public void setNumero_tarjeta(final String numero_tarjeta) {
		 this.numero_tarjeta = numero_tarjeta;
	 }

	 /**
	  * Process data from parser
	  * @param parser the parser
	  * @throws IOException on communication errors
	  * @throws ParsingException on format errors
	  */
	 public void process(final Parser parser) throws IOException, ParsingException {
		 String entity = parser.parseNextEntity();
		 do {
			 if (entity.startsWith("TE")) {
				 this.numero_cliente = entity.substring(2);
			 } else if (entity.startsWith("IS")) {
				 this.instrumento_seguridad = entity.substring(2);
			 } else if (entity.startsWith("TS")) {
				 this.indicador_solicitud = entity.substring(2);
			 } else if (entity.startsWith("OA")) {
				 this.compania_telefonica = entity.substring(2);
			 } else if (entity.startsWith("EM")) {
				 this.email = entity.substring(2);
			 } else if (entity.startsWith("NA")) {
				 this.nombre_token = entity.substring(2);
			 } else if (entity.startsWith("SE")) {
				 this.numero_serie = entity.substring(2);
			 } else if (entity.startsWith("AC")) {
				 this.numero_clave_activacion = entity.substring(2);
			 }

			 entity = parser.parseNextEntity();
		 } while (entity != null);  
	 }

	 /**
	  * @return the numero_clave_activacion
	  */
	 public String getNumero_clave_activacion() {
		 return numero_clave_activacion;
	 }

	 /**
	  * @param numero_clave_activacion the numero_clave_activacion to set
	  */
	 public void setNumero_clave_activacion(final String numero_clave_activacion) {
		 this.numero_clave_activacion = numero_clave_activacion;
	 }

	 @Override
	 public void process(final ParserJSON parser) throws IOException, ParsingException {
		 // Empty method
	 }
}
