/*
 * Copyright (C) 2011 Open Communications Security S.A.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;


public class DialogHelper {
//	
//	/**
//	 * This class is a special implementation of the dialog that does not accept
//	 * the back button. 
//	 */
//	public static class ProgressDialog2 extends ProgressDialog {
//
//		private boolean disableBack;
//		
//		public ProgressDialog2(Context context, boolean disableBack) {
//			super(context);
//			this.disableBack = disableBack;
//		}
//
//		/**
//		 * 
//		 */
//		@Override
//		public boolean onKeyDown(int keyCode, KeyEvent event) {
//			if ((keyCode == KeyEvent.KEYCODE_BACK) && (this.disableBack)) {
//				return true;
//			} else {
//				return super.onKeyDown(keyCode, event);
//			}
//		}
//	}	
//
//	
//	public static class DismissActivityListener implements DialogInterface.OnCancelListener,
//			DialogInterface.OnClickListener {
//
//		private Activity target;
//		
//		public DismissActivityListener(Activity target){
//			this.target = target;
//		}
//		
//		@Override
//		public void onCancel(DialogInterface dialog) {
//			target.finish();
//		}
//
//		@Override
//		public void onClick(DialogInterface dialog, int which) {
//			target.finish();
//		}
//	}
//	
//	private static final DialogInterface.OnClickListener DISMISS_ON_CLICK = new DialogInterface.OnClickListener() {
//		@Override
//		public void onClick(DialogInterface dialog, int which) {
//			dialog.dismiss();
//		}
//	};
//	
//
//	public static void showMessageBox(Context parent, String title, 
//			String message, int iconId, 
//			DialogInterface.OnCancelListener onCancel){
//		AlertDialog alertDialog = new AlertDialog.Builder(parent).create();
//
//		alertDialog.setTitle(title);
//		alertDialog.setMessage(message);
//		if (iconId != 0) {
//			alertDialog.setIcon(iconId);
//		}
//
//		if (onCancel != null) {
//			alertDialog.setOnCancelListener(onCancel);
//		}
//		alertDialog.show();		
//	}
//	
//	public static void showYesNoMessageBox(Context parent, String title, 
//			String message, int iconId,
//			DialogInterface.OnClickListener onYes,
//			DialogInterface.OnClickListener onNo,
//			DialogInterface.OnCancelListener onCancel){
//		AlertDialog alertDialog = new AlertDialog.Builder(parent).create();
//
//		alertDialog.setTitle(title);
//		alertDialog.setMessage(message);
//		if (iconId != 0) {
//			alertDialog.setIcon(iconId);
//		}
//		
//		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, 
//				parent.getString(R.string.button_yes), 
//				(onYes != null)?onYes: DISMISS_ON_CLICK);
//		alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, 
//				parent.getString(R.string.button_no), 
//				(onNo != null)?onNo: DISMISS_ON_CLICK);
//
//		if (onCancel != null) {
//			alertDialog.setOnCancelListener(onCancel);
//		}
//		alertDialog.show();		
//	}

}
