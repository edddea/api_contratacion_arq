/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SofttokenApp;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import net.otpmt.mtoken.Deployer;
import net.otpmt.mtoken.DeploymentListener;
import net.otpmt.mtoken.MTokenCore;
import net.otpmt.mtoken.SerialActivation;
import net.otpmt.mtoken.db.Account;
import net.otpmt.mtoken.db.Enterprise;
import net.otpmt.mtoken.util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.token.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.token.ContratacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenConstants;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenSession;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.ActivacionSTViewController;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.ConfirmacionSTViewController;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.IngresoDatosSTViewController;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomer.aplicaciones.softtoken.classes.model.token.AutenticacionSTRespuesta;
import suitebancomer.aplicaciones.softtoken.classes.model.token.ConsultaCoordenadasSTRespuesta;
import suitebancomer.aplicaciones.softtoken.classes.model.token.ConsultaTarjetaSTRespuesta;
import suitebancomer.aplicaciones.softtoken.classes.model.token.SoftToken;
import suitebancomercoms.aplicaciones.bmovil.classes.common.CatalogoAutenticacionFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatusMantenimientoData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Contratacion;
import suitebancomercoms.aplicaciones.bmovil.classes.model.SincroExportSTData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.TemporalCambioTelefono;
import suitebancomercoms.aplicaciones.bmovil.classes.model.TemporalST;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

/**
 * Controla toda la logica de negocio del proceso de Token Movil.
 * 
 * @author bbva
 */
public class ContratacionSTDelegate extends SofttokenBaseDelegate {

	/** Identificador unico del delegate. */
	public static final long CONTRATACION_ST_DELEGATE_ID = 8300960103526424739L;



	private Boolean isFlujoAutenticacionSt=false;
	/**
	 * Modelo con toda la informacion necesarias para las peticiones al
	 * servidor.
	 */
	private SoftToken softToken;

	/** Modelo para la consulta de estatus mantenimiento. */
	private ConsultaEstatus consultaEstatus;

	private static final String TAG = ContratacionSTDelegate.class.getName();
	public static final String PREREGISTRO = "preregistro";
	public static final String PUK = "2222";

	private BaseViewControllerCommons ownerController;
	private Deployer deployer;
	private Handler deployerMessageHandle;
	public GeneraOTPSTDelegate generaTokendelegate;

	public Boolean activando = false;
	public Boolean errorFinalizarContratacion = false;
	public String numero_cel = null;
	private Account selectedAccount;
	private Vector<Vector<Account>> accounts = new Vector<Vector<Account>>();
	private Vector<Enterprise> enterprises;
	
	private Boolean isEA12 = false;
	private Boolean isActivacionSoftokenNuevo = false;
	
	private Boolean esReact2x1 = false;
	public Boolean getEsReact2x1(){
		return esReact2x1;
	}

	/**
	 * Constructor por defecto.
	 */
	public ContratacionSTDelegate() {
		ownerController = null;
		softToken = new SoftToken();
		generaTokendelegate = new GeneraOTPSTDelegate(this);
		generaTokendelegate.setOwnerController(ownerController);
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param otherDelegate
	 *            el delegate de generar OTP.
	 */
	public ContratacionSTDelegate(final GeneraOTPSTDelegate otherDelegate) {
		ownerController = null;
		softToken = new SoftToken();
		generaTokendelegate = otherDelegate;
		generaTokendelegate.setOwnerController(ownerController);
	}

	public BaseViewControllerCommons getOwnerController() {
		return ownerController;
	}

	public void setOwnerController(final BaseViewControllerCommons ownerController) {
		this.ownerController = ownerController;

		if (null == generaTokendelegate)
			generaTokendelegate = new GeneraOTPSTDelegate(this);
		generaTokendelegate.setOwnerController(ownerController);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomercoms.classes.gui.delegates.BaseDelegate#getDelegateIdentifier()
	 */
	@Override
	public long getDelegateIdentifier() {
		return CONTRATACION_ST_DELEGATE_ID;
	}

	/**
	 * Obtiene el modelo softToken.
	 * 
	 * @return el modelo softToken
	 */
	public SoftToken getSoftToken() {
		return softToken;
	}
	
	

	public void setSoftToken(final SoftToken softToken) {
		this.softToken = softToken;
	}

	public ConsultaEstatus getConsultaEstatus() {
		return consultaEstatus;
	}

	public void setConsultaEstatus(final ConsultaEstatus consultaEstatus) {
		this.consultaEstatus = consultaEstatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomercoms.classes.gui.delegates.BaseDelegate#doNetworkOperation(int,
	 * java.util.Hashtable,
	 * suitebancomercoms.classes.gui.controllers.BaseViewController)
	 */
	@Override
	public void doNetworkOperation(final int operationId, final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewControllerCommons caller) {
		//Actualizado JAIG 
		SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().invokeNetworkOperation(operationId, params,isJson,handler, caller, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomercoms.classes.gui.delegates.BaseDelegate#analyzeResponse(int,
	 * suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		final int status = response.getStatus();
		if (ServerResponse.OPERATION_SUCCESSFUL == status) {
			analizarRespuestaExito(operationId, response);
		} else if (ServerResponse.OPERATION_ERROR == status) {
			analizarRespuestaError(operationId, response);
		}
	}

	/**
	 * Analiza las respuestas de exito del servidor.
	 * 
	 * @param operationId
	 *            el identificador de la operacion
	 * @param response
	 *            la respuesta del servidor
	 */
	private void analizarRespuestaExito(final int operationId, final ServerResponse response) {
		if (Server.CONSULTA_TARJETA_ST == operationId) {
			procesarConsultaTarjetaST((ConsultaTarjetaSTRespuesta) response.getResponse());
		} else if (Server.AUTENTICACION_ST == operationId) {
			if(buscarBanderasContratarBmovil()){
				// EA11
				Session.getInstance(SuiteAppApi.appContext).saveBanderasBMovil(Constants.BANDERAS_CONTRATAR2x1, true, true);
			}			
 			eliminaValorVariable(Constants.BANDERAS_CONTRATAR);
			addVariableKeyChain(Constants.ACTIVA_TOKEN, Constants.BMOVIL);
			procesarAutenticacionST((AutenticacionSTRespuesta) response.getResponse());
		} else if (Server.CONTRATACION_ENROLAMIENTO_ST == operationId) {
			SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showPantallaCActivacionST();
		} else if (Server.SINCRONIZACION_SOFTTOKEN == operationId) {
			exportarToken();
		} else if (Server.EXPORTACION_SOFTTOKEN == operationId) {
			if(buscarBanderasContratacion2x1()){
				Session.getInstance(SuiteAppApi.appContext).saveBanderasBMovil(Constants.BANDERAS_CONTRATAR2x1, false, false);
			}			
			
			PropertiesManager.getCurrent().setSofttokenActivated(true);
			
			final Session session = Session.getInstance(SuiteAppApi.appContext);
			if (!Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CONTRATAR))) {
				realizarCambioDeCelular();
			} else {
				finalizarContratacionST();
			}
		} else if (Server.FINALIZAR_CONTRATACION_ST == operationId) {
			procesarFinalizarContratacionST();
		} else if (Server.CAMBIO_TELEFONO_ASOCIADO_ST == operationId) {
			procesarCambioDeCelularST();
		} else if (Server.SOLICITUD_ST == operationId) {
			if(isFlujoAutenticacionSt){
				isFlujoAutenticacionSt=false;
				softToken.setTipoSolicitud("N");
				autenticacionST( this.nip, this.cvv2, "", esSustitucionDeToken());
				return;
			}
			if(SofttokenConstants.TIENE_DISPOSITIVO_FISICO.equals(softToken.getDispositivoFisico())&& SofttokenConstants.ESTATUS_DISPOSITIVO_A1.equals(softToken.getEstatusDispositivo())){
				softToken.setTipoSolicitud("S");	
			}else{
				softToken.setTipoSolicitud("N");
			}
			procesarSolicitudST();
		} else if (Server.CONSULTA_MANTENIMIENTO == operationId) {
			procesarConsultaEstatusMantenimiento(response);
		} else if (Server.CONSULTA_TARJETA_OPERATION == operationId) {
			((ContratacionDelegate) SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getBmovilViewsController().getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID)).direccionarFlujoConsultaTarjeta(response);
		} else if (Server.OP_SINC_EXP_TOKEN == operationId) {
			// Procesamos la peticion de sincronizacion exp del token
			Log.d(">> CGI-Nice-Ppl", "Respuesta de peticion OP_SINC_EXP_TOKEN");
			Log.d(">> CGI-Nice-Ppl", "Procesamos la respuesta");
			procesarSincroExpToken(response);
		}
		else if(Server.CONSULTA_COORDENADAS == operationId)
		{

			ConsultaCoordenadasSTRespuesta respuestaCoordenadas = (ConsultaCoordenadasSTRespuesta) response.getResponse();


			softToken.setCoordenadas(respuestaCoordenadas.getCoordenada());
			softToken.setDigitoVerificador(respuestaCoordenadas.getDigitoVerificador());
			SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().setCurrentActivityApp(this.ownerController);

			SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showConfirmacionST(softToken);
		} else if (Server.CONSULTA_AUTENTICACIONTOKENPM == operationId) {

			if(buscarBanderasContratarBmovil()){
				// EA11
				Session.getInstance(SuiteAppApi.appContext).saveBanderasBMovil(Constants.BANDERAS_CONTRATAR2x1, true, true);
			}
			eliminaValorVariable(Constants.BANDERAS_CONTRATAR);
			addVariableKeyChain(Constants.ACTIVA_TOKEN, Constants.BMOVIL);
			procesarAutenticacionST((AutenticacionSTRespuesta) response.getResponse());

		}
		else if(Server.SINCRONIZAR_TOKENPM==operationId)
		{
			Log.e("SINCRONIZACION LISTA", "PASAMOS A LA EXPORTACION ->>>>");
			exportarTokenPM();
		}
		else if(Server.EXPORTACION_SOFTTOKENPM==operationId)
		{
			if(buscarBanderasContratacion2x1()){
				Session.getInstance(SuiteAppApi.appContext).saveBanderasBMovil(Constants.BANDERAS_CONTRATAR2x1, false, false);
			}

			PropertiesManager.getCurrent().setSofttokenActivated(true);

			final Session session = Session.getInstance(SuiteAppApi.appContext);
			if (!Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CONTRATAR))) {
				realizarCambioDeCelular();
			} else {
				finalizarContratacionST();
			}
		}

	}
	
	private boolean buscarBanderasContratacion2x1(){
		final Session session = Session.getInstance(SuiteAppApi.appContext);
		return Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CONTRATAR2x1));
	}
	
	private boolean buscarBanderasContratarBmovil(){
		final Session session = Session.getInstance(SuiteAppApi.appContext);
		return Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CONTRATAR));
	}

	/**
	 * Analiza las respuestas de error del servidor.
	 * 
	 * @param operationId
	 *            el identificador de la operacion
	 * @param response
	 *            la respuesta del servidor
	 */
	private void analizarRespuestaError(final int operationId, final ServerResponse response) {
		if(Server.EXPORTACION_SOFTTOKEN== operationId){
			ownerController.showErrorMessage(R.string.error_communications);
		}else if (Server.FINALIZAR_CONTRATACION_ST == operationId) {
			final Session session = Session.getInstance(SuiteAppApi.appContext);
			session.deleteTemporalST();
			session.saveBanderasBMovil(Constants.BANDERAS_CONTRATAR, false,
					true);
			eliminaValorVariable(Constants.CONTRATACION_BT);

			/*Cambios en activacionSoftoken IDS Comercial eluna*/
			softToken.setNumeroTelefono("");
			softToken.setCompanniaCelular("");
			softToken.setNumeroTarjeta("");
			softToken.setTipoSolicitud("");
			softToken.setVersionApp("");

			showPantallaEmailST(response.getMessageCode() + "\n"
					+ response.getMessageText());
		} else if (Server.OP_SINC_EXP_TOKEN == operationId) {
			// Procesamos la peticion de sincronizacion exp del token
			Log.d(">> CGI-Nice-Ppl", "Respuesta de peticion OP_SINC_EXP_TOKEN KO");
			Log.d(">> CGI-Nice-Ppl", "Procesamos la respuesta KO");
			procesarSincroExpTokenKO(response);
		
		}else{
			if(Constants.CODE_CNE1506.equals(response.getMessageCode())){
				ownerController.showYesNoAlert1(R.string.bmovil_activacion_alert_cambio_numero,
						new OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog, final int which) {
								dialog.dismiss();
								ownerController.setHabilitado(true);
								mostrarAlert2();
							}
						},
						new OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog, final int which) {
								dialog.dismiss();
								ownerController.setHabilitado(true);
								alertaVerificaDatos();
							}
						});
			}else {
				ownerController.showInformationAlertEspecial(ownerController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
			}
		}
	}

	/**
	 * Se detecta si el flujo es de Activacion SoftToken Sustitucion.
	 * 
	 * @return verdadero si el flujo es el citado. Falso en caso contrario o si
	 *         la respuesta es nula.
	 */
	public boolean esSustitucionDeToken() {
		return ((null == softToken) ? false : SofttokenConstants.SUSTITUCION
				.equalsIgnoreCase(softToken.getTipoSolicitud()));
	}

	/**
	 * Realiza la operacion de consulta de tarjeta ST, validando previamente que
	 * el numero de celular y el numero de tarjeta son correctos.
	 * 
	 * @param numCelular
	 *            el numero de celular
	 * @param numTarjeta
	 *            el numero de tarjeta
	 * @param nomCompania
	 *            el nombre de la compannia telefonica
	 */
	public void consultaTarjetaST(final String numCelular, final String numTarjeta,
			final String nomCompania) {

		if (numCelular.length() < SofttokenConstants.TELEPHONE_NUMBER_LENGTH) {
			ownerController.showInformationAlert(R.string.softtoken_activacion_ingresar_datos_error_numero_celular);
		} else if (numTarjeta.length() < SofttokenConstants.CARD_NUMBER_LENGTH) {
			ownerController.showInformationAlert(R.string.softtoken_activacion_ingresar_datos_error_numero_tarjeta);
		} else {

			softToken.setNumeroTelefono(numCelular);
			softToken.setNumeroTarjeta(numTarjeta);
			softToken.setCompanniaCelular(nomCompania);
			softToken.setVersionApp(Constants.APPLICATION_VERSION);
			/*
			Hashtable<String, String> paramTable = new Hashtable<String, String>();
			paramTable.put(ServerConstants.NUMERO_TELEFONO, numCelular);
			paramTable.put(ServerConstants.NUMERO_TARJETA, numTarjeta);
			paramTable.put(ServerConstants.COMPANIA_CELULAR, nomCompania);
			// TODO Modificacion VersionApp
			paramTable.put(ServerConstants.VERSION_APP, softToken.getVersionApp());
			*/
			
			final Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
			paramTable2.put(ServerConstants.NUMERO_TELEFONO, numCelular);
			paramTable2.put(ServerConstants.NUMERO_TARJETA, numTarjeta);
			paramTable2.put(ServerConstants.COMPANIA_CELULAR, nomCompania);
			paramTable2.put(ServerConstants.VERSION_APP,softToken.getVersionApp());

			final List<String> listaEncriptar = Arrays.asList(
					ApiConstants.OPERACION, ServerConstants.NUMERO_TELEFONO,
					ServerConstants.NUMERO_TARJETA, ServerConstants.COMPANIA_CELULAR,
					ServerConstants.VERSION_APP);
			CommContext.operacionCode = Server.CONSULTA_TARJETA_ST;
			CommContext.listaEncriptar = listaEncriptar;

			//JAIG
			doNetworkOperation(Server.CONSULTA_TARJETA_ST, paramTable2, true,new ConsultaTarjetaSTRespuesta(),
					ownerController);
		}
	}


	/**
	 * Realiza la operacion de consulta de coordenadas de TAS
	 *
	 * @param numCelular
	 *            el numero de celular
	 *            el numero de tarjeta
	 */
	public void consultaCoordenadasTAS(final String numCelular, final String numTarjeta) {

		if (numCelular.length() < SofttokenConstants.TELEPHONE_NUMBER_LENGTH) {
			ownerController.showInformationAlert(R.string.softtoken_activacion_ingresar_datos_error_numero_celular);
		} else if (numTarjeta.length() < SofttokenConstants.CARD_NUMBER_LENGTH) {
			ownerController.showInformationAlert(R.string.softtoken_activacion_ingresar_datos_error_numero_tarjeta);
		} else {

			softToken.setNumeroTelefono(numCelular);
			softToken.setNumeroTarjeta(numTarjeta);
			softToken.setVersionApp(Constants.APPLICATION_VERSION);
			/*
			Hashtable<String, String> paramTable = new Hashtable<String, String>();
			paramTable.put(ServerConstants.NUMERO_TELEFONO, numCelular);
			paramTable.put(ServerConstants.NUMERO_TARJETA, numTarjeta);
			paramTable.put(ServerConstants.COMPANIA_CELULAR, nomCompania);
			// TODO Modificacion VersionApp
			paramTable.put(ServerConstants.VERSION_APP, softToken.getVersionApp());
			*/

			final Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
			paramTable2.put("numeroCelular", numCelular);
			paramTable2.put("numeroTAS", numTarjeta);
			paramTable2.put(ServerConstants.VERSION_APP,"");
			paramTable2.put("EN","");

			final List<String> listaEncriptar = Arrays.asList(
					ApiConstants.OPERACION_COORDENADAS, ServerConstants.NUMERO_TELEFONO,
					ServerConstants.NUMERO_TARJETA,
					ServerConstants.VERSION_APP);
			CommContext.operacionCode = Server.CONSULTA_TARJETA_ST;
			CommContext.listaEncriptar = listaEncriptar;

			//JAIG
			doNetworkOperation(Server.CONSULTA_COORDENADAS, paramTable2, true,new ConsultaCoordenadasSTRespuesta(),
					ownerController);
		}
	}

	/**
	 * FA#19
	 * @param nip
	 * @param cvv2
	 * @param targeta
	 */
	private String nip,  cvv2,  targeta;
	public void autenticacionSTFlujo(String nip, String cvv2, String targeta) {

		if (targeta.length() < SofttokenConstants.CARD_NUMBER_LENGTH) {
			ownerController.showInformationAlert(R.string.softtoken_activacion_confirmacion_error_targeta);
		} else if (nip.length() < SofttokenConstants.NIP_LENGTH) {
			ownerController.showInformationAlert(R.string.softtoken_activacion_confirmacion_error_nip);
		} else if (cvv2.length() < SofttokenConstants.CVV_LENGTH) {
			//else if(!esSustitucion && cvv2.length() < SofttokenConstants.CVV_LENGTH){
			ownerController.showInformationAlert(R.string.softtoken_activacion_confirmacion_error_cvv);
		} else{

			this.nip = nip;
			this.cvv2 = cvv2;
			this.targeta = targeta;

			suitebancomer.aplicaciones.bmovil.classes.model.Account cEje = Tools.obtenerCuentaEje();
			//llena objeto softtoken. RN5
			final Session session = Session.getInstance(SuiteAppApi.appContext);
			//softToken.setNumeroTarjeta(cEje.getNumber());
			softToken.setNumeroTarjeta(this.targeta);
			softToken.setNumeroTelefono(session.getUsername());
			softToken.setCompanniaCelular(session.getCompaniaUsuario());
			softToken.setCorreoElectronico(session.getEmail());
			softToken.setNumeroCliente(session.getClientNumber());
			softToken.setNombreCliente(session.getNombreCliente());
			softToken.setVersionApp(Constants.APPLICATION_VERSION);
			softToken.setEstatusDispositivo(session.getEstatusIS());
			SofttokenSession sts=SofttokenSession.getCurrent();
			sts.setIum(session.getIum());
			sts.setSeed(session.getSeed());
			sts.setUsername(session.getUsername());
			sts.setSofttokenActivated(session.isSofttokenActivado());
			sts.saveSottokenRecordStore();


			setIsFlujoAutenticacionSt(true);
			solicitudST();

		}
	}
	/**
	 * Realiza la operacion de autenticacion token, validando previamente que el
	 * nip y el opt (en caso de sustituacion) son correctos.
	 * 
	 * @param nip
	 *            el nip
	 * @param otp
	 *            el otp
	 * @param esSustitucion
	 *            indica si se llega por el flujo de sustitucion
	 */
	public void autenticacionST(final String nip, final String cvv2, final String otp, final boolean esSustitucion) {

		if (nip.length() < SofttokenConstants.NIP_LENGTH) {
			ownerController.showInformationAlert(R.string.softtoken_activacion_confirmacion_error_nip);
		}
		else if(cvv2.length() < SofttokenConstants.CVV_LENGTH){
		//else if(!esSustitucion && cvv2.length() < SofttokenConstants.CVV_LENGTH){
			ownerController.showInformationAlert(R.string.softtoken_activacion_confirmacion_error_cvv);
		} else if (esSustitucion && otp.length() < SofttokenConstants.OTP_LENGTH) {
			ownerController.showInformationAlert(R.string.softtoken_activacion_confirmacion_error_otp);
		} else {
			/*
			Hashtable<String, String> paramTable = new Hashtable<String, String>();
			paramTable.put(ServerConstants.NUMERO_TELEFONO, softToken.getNumeroTelefono());
			paramTable.put(ServerConstants.NUMERO_TARJETA, softToken.getNumeroTarjeta());
			paramTable.put(Server.NUMERO_CLIENTE, softToken.getNumeroCliente());
			paramTable.put(Server.NIP_CAJERO, nip);
			paramTable.put(Server.OTP_TOKEN, esSustitucion ? otp : "");
			paramTable.put(Server.TIPO_SOLICITUD_ST, softToken.getTipoSolicitud());
			paramTable.put(Server.NOMBRE_CLIENTE, softToken.getNombreCliente());
			paramTable.put(Server.EMAIL_ST, softToken.getCorreoElectronico());
			paramTable.put(ServerConstants.CODIGO_CVV2, cvv2);
			// TODO Modificacion VersionApp
			paramTable.put(ServerConstants.VERSION_APP, softToken.getVersionApp());
			*/
			
			final Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
			
			final String nombreCliente = softToken.getNombreCliente();
			final String primerNombre = nombreCliente.split(" ")[0];
			final String versionApp = softToken.getVersionApp();

			paramTable2.put("numeroCelular", softToken.getNumeroTelefono());
			paramTable2.put("numeroTarjeta", softToken.getNumeroTarjeta());
			paramTable2.put("codigoNIP", nip);
			paramTable2.put("codigoOTP", esSustitucion ? otp : "");
			paramTable2.put("tipoSolicitud", softToken.getTipoSolicitud());
			paramTable2.put("email", softToken.getCorreoElectronico());
			paramTable2.put("sistemaOperativo", "android");
			if(!versionApp.equalsIgnoreCase(Constants.EMPTY_STRING)){
				paramTable2.put("versionApp", softToken.getVersionApp());
				paramTable2.put("codigoCVV2", cvv2);
			}else{
				paramTable2.put("versionApp", Constants.EMPTY_STRING);
				paramTable2.put("numeroCliente",softToken.getNumeroCliente());
				paramTable2.put("nombreCliente", primerNombre);
			}

			final List<String> listaEncriptar = Arrays.asList(
					ApiConstants.OPERACION, "numeroCelular",
					"numeroTarjeta",
					"codigoNIP", "codigoCVV2", "codigoOTP",
					"tipoSolicitud", "email",
					"versionApp");
			CommContext.operacionCode = Server.AUTENTICACION_ST;
			CommContext.listaEncriptar = listaEncriptar;

			//JAIG
			doNetworkOperation(Server.AUTENTICACION_ST, paramTable2, true, new AutenticacionSTRespuesta(),
					ownerController);
		}
	}

	public void autenticacionSTPM(final String nip, final String cvv2, final String otp,final String valorDigito, final boolean esSustitucion) {

		if (nip.length() < SofttokenConstants.NIP_LENGTH) {
			ownerController.showInformationAlert(R.string.softtoken_activacion_confirmacion_error_nip);
		}
		else if(cvv2.length() < SofttokenConstants.CVV_LENGTH){
			ownerController.showInformationAlert(R.string.softtoken_activacion_confirmacion_error_tripleta);
		} else if (esSustitucion && otp.length() < SofttokenConstants.OTP_LENGTH) {
			ownerController.showInformationAlert(R.string.softtoken_activacion_confirmacion_error_otp);
		} else {

			final Hashtable<String, String> paramTable2 = new Hashtable<String, String>();

			final String nombreCliente = softToken.getNombreCliente();
			final String primerNombre = nombreCliente.split(" ")[0];
			final String versionApp = softToken.getVersionApp();

			paramTable2.put("numeroCelular", softToken.getNumeroTelefono());
			paramTable2.put("numeroTAS", softToken.getNumeroTarjeta());
			paramTable2.put("codigoNIP", nip);
			paramTable2.put("codigoTriple", cvv2);
			paramTable2.put("codigoOTP", esSustitucion ? otp : "");
			paramTable2.put("tipoSolicitud", softToken.getTipoSolicitud());
			paramTable2.put("versionApp", "");
			paramTable2.put("EN", "");
			paramTable2.put("valorDigito",valorDigito);
			paramTable2.put("digitoVerificador",softToken.getDigitoVerificador());
			paramTable2.put("coordenada",softToken.getCoordenadas());

			final List<String> listaEncriptar = Arrays.asList(
					ApiConstants.OPERACION_AUTENTICACIONPM, "numeroCelular",
					"numeroTAS",
					"codigoNIP", "codigoTriple", "codigoOTP",
					"tipoSolicitud",
					"versionApp","EN");
			CommContext.operacionCode = Server.AUTENTICACION_ST;
			CommContext.listaEncriptar = listaEncriptar;

			//JAIG
			doNetworkOperation(Server.CONSULTA_AUTENTICACIONTOKENPM, paramTable2, true, new AutenticacionSTRespuesta(),
					ownerController);
		}
	}

	public void sincronizacionToken(final String sincronizacionOTP1,
			final String sincronizacionOTP2) {
		/*Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put(Server.NOMBRE_TOKEN, softToken.getNombreToken());
		paramTable.put(Server.OTP1, sincronizacionOTP1);
		paramTable.put(Server.OTP2, sincronizacionOTP2);
		 */

		final Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
		paramTable2.put("NA", softToken.getNombreToken());
		paramTable2.put("O1",sincronizacionOTP1);
		paramTable2.put("O2", sincronizacionOTP2);
		paramTable2.put("order", "NA*O1*O2");

		//JAIG
		doNetworkOperation(Server.SINCRONIZACION_SOFTTOKEN, paramTable2,false,null,
				ownerController);
	}

	public void sincronizacionTokenPM(final String sincronizacionOTP1,
									final String sincronizacionOTP2) {

		final Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
		paramTable2.put("nombreToken", softToken.getNombreToken());
		paramTable2.put("OTP1",sincronizacionOTP1);
		paramTable2.put("OTP2", sincronizacionOTP2);

		//JAIG
		doNetworkOperation(Server.SINCRONIZAR_TOKENPM, paramTable2,true,null,
				ownerController);
	}

	public void exportarToken() {
		/*Hashtable<String, String> paramTable = new Hashtable<String, String>();

		paramTable.put(ServerConstants.NUMERO_TARJETA,
				softToken.getNombreToken());
		paramTable.put(ServerConstants.NUMERO_TELEFONO,
				softToken.getNumeroTelefono());
		paramTable.put(Server.NUMERO_CLIENTE, softToken.getNumeroCliente());
		paramTable.put(Server.TIPO_SOLICITUD_ST, softToken.getTipoSolicitud());
		paramTable.put(Server.NOMBRE_TOKEN, softToken.getNombreToken());
		paramTable.put(ServerConstants.VERSION_APP, Constants.APPLICATION_VERSION);
		*/
		final Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
		paramTable2.put("NJ", softToken.getNumeroTarjeta());
		paramTable2.put("NT", softToken.getNumeroTelefono());
		paramTable2.put("TS", softToken.getTipoSolicitud());
		paramTable2.put("NA", softToken.getNombreToken());
		paramTable2.put("TE", "AAAAAAAA");
		paramTable2.put("AV", Constants.APPLICATION_VERSION);
		paramTable2.put("order", "NJ*NT*TS*NA*TE*AV");
		
		//JAIG
		doNetworkOperation(Server.EXPORTACION_SOFTTOKEN,paramTable2,false,null,
				ownerController);
	}


	public void exportarTokenPM() {

		final Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
		paramTable2.put("numeroTAS", softToken.getNumeroTarjeta());
		paramTable2.put("numeroTelefono", softToken.getNumeroTelefono());
		paramTable2.put("indicadorContratacion", softToken.getTipoSolicitud());
		paramTable2.put("nombreToken", softToken.getNombreToken());
		paramTable2.put("versionApp", Constants.APPLICATION_VERSION);

		//JAIG
		doNetworkOperation(Server.EXPORTACION_SOFTTOKENPM,paramTable2,true,null,
				ownerController);
	}



	/**
	 * Realiza la operacion de finalizar contratacion ST.
	 */
	private void finalizarContratacionST() {
		final Session session = Session.getInstance(SuiteAppApi.appContext);
		final TemporalST temporalST = session.loadTemporalST();
		
		long seed = session.getSeed(); 
        if(seed == 0){
               seed = System.currentTimeMillis();
               session.setSeed(seed);
        }
        
        
        final String ium = Tools.buildIUM(softToken.getNumeroTelefono(),seed,SuiteAppApi.appContext);
        session.setIum(ium);
        session.saveRecordStore();
		
		if (null != temporalST) {
			/*Hashtable<String, String> paramTable = new Hashtable<String, String>();
			paramTable.put(ServerConstants.NUMERO_TARJETA, temporalST.getTarjeta());
			paramTable.put(ServerConstants.NUMERO_TELEFONO, temporalST.getCelular());
			paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, ium);
			paramTable.put(ServerConstants.COMPANIA_CELULAR, temporalST.getCompannia());
			paramTable.put(ServerConstants.CODIGO_OTP, generaTokendelegate.generaOTPTiempo());
			 */
			
			final Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
			if (temporalST.getTarjeta().equals("") || temporalST.getCelular().equals("") || temporalST.getCompannia().equals(""))
			{
				paramTable2.put(ServerConstants.NUMERO_TARJETA, softToken.getNumeroTarjeta());
				paramTable2.put(ServerConstants.NUMERO_TELEFONO, softToken.getNumeroTelefono());
				paramTable2.put(ServerConstants.COMPANIA_CELULAR, softToken.getCompanniaCelular());

			}else
			{
				paramTable2.put(ServerConstants.NUMERO_TARJETA, temporalST.getTarjeta());
				paramTable2.put(ServerConstants.NUMERO_TELEFONO, temporalST.getCelular());
				paramTable2.put(ServerConstants.COMPANIA_CELULAR, temporalST.getCompannia());
			}

			paramTable2.put(ServerConstants.JSON_IUM_ETIQUETA, ium);
			paramTable2.put(ServerConstants.CODIGO_OTP, generaTokendelegate.generaOTPTiempo());

			final List<String> listaEncriptar = Arrays.asList(
					ApiConstants.OPERACION, ServerConstants.NUMERO_TARJETA, ServerConstants.NUMERO_TELEFONO,
					ServerConstants.JSON_IUM_ETIQUETA, ServerConstants.COMPANIA_CELULAR,ServerConstants.CODIGO_OTP
			);
			CommContext.operacionCode = Server.FINALIZAR_CONTRATACION_ST;
			CommContext.listaEncriptar = listaEncriptar;

			//JAIG
			doNetworkOperation(Server.FINALIZAR_CONTRATACION_ST, paramTable2,true,null, ownerController);
		}
	}

	/**
	 * Comprueba si existe un cambio de celular. En caso afirmativo, realiza la
	 * peticion para el cambio. En cualquier caso, muestra la pantalla de
	 * informacion del correo electronico.
	 */
	private void realizarCambioDeCelular() {
		if (!Boolean.TRUE.equals(Session.getInstance(SuiteAppApi.appContext).loadBanderasBMovilAttribute(Constants.BANDERAS_CAMBIO_CELULAR))) {
			showPantallaEmailST(null);
		} else {

			final Session session = Session.getInstance(SuiteAppApi.appContext);
			final TemporalCambioTelefono temporalCambioTelefono = session.loadTemporalCambioTelefono();

			final Hashtable<String, String> paramTable = new Hashtable<String, String>();
			paramTable.put(ServerConstants.NUMERO_TELEFONO, softToken.getNumeroTelefono());
			paramTable.put(ServerConstants.COMPANIA_CELULAR, softToken.getCompanniaCelular());
			paramTable.put(ServerConstants.CODIGO_OTP, generaTokendelegate.generaOTPTiempo());
			paramTable.put(ServerConstants.NUMERO_CLIENTE, softToken.getNumeroCliente());

			paramTable.put(ServerConstants.NUMERO_TARJETA, temporalCambioTelefono.getTarjeta());
			//JAIG
			doNetworkOperation(Server.CAMBIO_TELEFONO_ASOCIADO_ST, paramTable,true,null, ownerController);
		}
	}

	/**
	 * Realiza la operacion de activacion token validando previamente que la
	 * clave de activacion es correcta
	 * 
	 * @param claveActivacion
	 *            la clave de activacion
	 */
	public void activacionST(final String claveActivacion) {
		if (claveActivacion.length() < SofttokenConstants.ACTIVATION_CODE_LENGTH) {
			ownerController
					.showInformationAlert(R.string.softtoken_activacion_error_clave_corta);
		} else {
			final SofttokenActivationBackupManager manager = SofttokenActivationBackupManager
					.getCurrent();
			if (manager.existsABackup()) {
				manager.deleteBackup();
			}

			descargarToken(softToken.getNumeroSerie(), claveActivacion);
		}
	}

	public void descargarToken(final String numeroSerie, final String claveActivacion) {
		ownerController.muestraIndicadorActividad(
				ownerController.getString(R.string.alert_operation),
				ownerController.getString(R.string.alert_connecting));
		if (doStartApplication()) {
			if (SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getCore().isInitialized()) {
				if (!activando) {
					activando = true;
					doStart(numeroSerie, claveActivacion,
							(ActivacionSTViewController) ownerController);
				}
			}
		} else {
			doStart(numeroSerie, claveActivacion,(ActivacionSTViewController) ownerController);
		}
	}

	/**
	 * Realiza la operacion de solicitud softToken.
	 */
	public void solicitudST() {
		/*Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.NUMERO_TARJETA, softToken.getNumeroTarjeta());
		paramTable.put(ServerConstants.NUMERO_TELEFONO,	softToken.getNumeroTelefono());
		paramTable.put(ServerConstants.COMPANIA_CELULAR, softToken.getCompanniaCelular());
		paramTable.put(ServerConstants.CORREO_ELECTRONICO, softToken.getCorreoElectronico());
		paramTable.put(ServerConstants.NUMERO_CLIENTE, softToken.getNumeroCliente());
		*/
		final Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
		paramTable2.put(ServerConstants.NUMERO_TARJETA, softToken.getNumeroTarjeta());
		paramTable2.put(ServerConstants.NUMERO_TELEFONO, softToken.getNumeroTelefono());
		paramTable2.put(ServerConstants.COMPANIA_CELULAR,  softToken.getCompanniaCelular());
		paramTable2.put("email", softToken.getCorreoElectronico());
		paramTable2.put(ServerConstants.NUMERO_CLIENTE, softToken.getNumeroCliente());
		final List<String> listaEncriptar = Arrays.asList(
				ApiConstants.OPERACION, ServerConstants.NUMERO_TARJETA,
				ServerConstants.NUMERO_TELEFONO, ServerConstants.COMPANIA_CELULAR,
				"email", ServerConstants.NUMERO_CLIENTE);
		CommContext.operacionCode = Server.SOLICITUD_ST;
		CommContext.listaEncriptar = listaEncriptar;
		//JAIG
		SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().invokeNetworkOperation(Server.SOLICITUD_ST, paramTable2,true,null, ownerController, false);
	}
	
	/******************************************************** Start SincroExpToken ********************************************************************/
	/**
	 * Procesamos la respuesta OK de la peticion de sinc & exp
	 * @param response
	 */
	private void procesarSincroExpToken(final ServerResponse response){
		Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpToken] Inicio procesarSincroExpToken");
		
		final SincroExportSTData respuesta = (SincroExportSTData) response.getResponse();
		
		if(respuesta.getIndReac2x1().equals("S")){
			Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpToken] 2x1 ==> S");
			
			final Session sesion= Session.getInstance(ownerController);
			
			final Long semilla= sesion.getSeed();
			borrarDatosUsuario();
			
			sesion.setSeed(semilla);
			sesion.setUsername(softToken.getNumeroTelefono());
			sesion.setApplicationActivated(true);
			
			Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpToken] setBmovilActivated + setSofttokenActivated");
			PropertiesManager.getCurrent().setSofttokenActivated(true);
			
			addVariableKeyChain(Constants.CENTRO, Constants.BMOVIL);
			eliminaValorVariable(Constants.ACTIVA_TOKEN);
			
			Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpToken] Salvamos la info del telefono");
	        sesion.saveRecordStore();
	        
	        Log.d(">> CGI-Nice-Ppl","[procesoSincroExpToken] Salvamos datos numero, seed y centro en keyChain");
	        addVariableKeyChain(Constants.USERNAME,softToken.getNumeroTelefono());
	        addVariableKeyChain(Constants.CENTRO, Constants.BMOVIL);
	        addVariableKeyChain(Constants.SEED, String.valueOf(semilla));
	        
	        Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpToken] Nos vamos al email - lets go!");
			esReact2x1 = true;
	        showPantallaEmailST("");
		}
	}
	
	/**
	 * Procesamos la respuesta KO de la peticion de sinc & exp
	 * @param response
	 */
	private void procesarSincroExpTokenKO(final ServerResponse response){
		Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpTokenKO] Inicio procesarSincroExpTokenKO");
		
		Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpTokenKO] Quizas borramos token movil");
        final GeneraOTPSTDelegate generaOTPSTDelegate = new GeneraOTPSTDelegate();
        Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpTokenKO] Borra el token");
        if (generaOTPSTDelegate.borraToken()) {
            Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpTokenKO] Borra el token >> true");
            PropertiesManager.getCurrent().setSofttokenActivated(false);
        }
		
		Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpTokenKO] Borrando datos bmovil");
		borrarDatosUsuario();
		
		Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpTokenKO] Se elimina de keyChain los valores para numero, centro y seed");
		eliminaValorVariable(Constants.USERNAME);
		eliminaValorVariable(Constants.CENTRO);
		eliminaValorVariable(Constants.SEED);
		
		Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpTokenKO] Indicador de actividad al carajo");
		ownerController.ocultaIndicadorActividad();
		
		Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpTokenKO] Mostramos el alert");
		ownerController.showInformationAlert(ownerController.getString(R.string.label_information), ownerController.getString(R.string.softtoken_reactivacion_alert_activacion_servicios_fail), 
				new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						Log.d(">> CGI-Nice-Ppl", "[procesarSincroExpTokenKO] Puede que nos vayamos a la suite");
						showMenuSuite();		
					}
		});
	}
	
	public String otp1 = "";
	public String otp2 = "";
	
	public class GeneraOTPs<T> extends AsyncTask<Object, Void, Boolean> {

		public Context context;
		public ProgressDialog dialog;

		public GeneraOTPs(final Context context) {
		    this.context = context;
		    this.dialog = new ProgressDialog(context);
		}

		@Override
		protected void onPreExecute() {
			Log.d(">> CGI-Nice-Ppl", "preExc -> Inicio");
			dialog = ProgressDialog.show(context, context.getResources().getString(R.string.alert_operation), context.getResources().getString(R.string.alert_connecting));
			dialog.show();
		}

		@Override
		protected Boolean doInBackground(final Object... objects) {
			Log.d(">> CGI-Nice-Ppl", "doInBck -> Inicio");
			otp1 = generaTokendelegate.generaOTPTiempo();
			String sincronizacionOTP2Temporal = generaTokendelegate.generaOTPTiempo();
			

			if (sincronizacionOTP2Temporal.equalsIgnoreCase(otp1)) {
				sincronizacionOTP2Temporal = generarTokenParaSincronizacion(SofttokenConstants.ESPERA_15_SEG);
				if (sincronizacionOTP2Temporal.equalsIgnoreCase(otp1)) {
					sincronizacionOTP2Temporal = generarTokenParaSincronizacion(SofttokenConstants.ESPERA_8_SEG);
					if (sincronizacionOTP2Temporal.equalsIgnoreCase(otp1)){
						sincronizacionOTP2Temporal = generarTokenParaSincronizacion(SofttokenConstants.ESPERA_7_SEG);
					}
				}
			}			
	
			otp2 = sincronizacionOTP2Temporal;
			Log.d(">> ThreadMaster in da house", "doInBck -> End");
			
		    return true;
		}

		@Override
		protected void onPostExecute(final Boolean result) {
			Log.d(">> CGI-Nice-Ppl", "postExc -> Inicio");
		    if (dialog != null && dialog.isShowing())
		        dialog.dismiss();
		    doSincronizacionExportacionST();
		}
	}
	
	/**
	 * Realiza la operacion de sinc & exp
	 */
	private void sincronizacionExportacionST() {
	
		Log.d(">> CGI-Nice-Ppl", "[sincronizacionExportacionST] Prelocura");
		if (doStartApplication()) {
			Log.d(">> CGI-Nice-Ppl", "[sincronizacionExportacionST] Postlocura");		
			
			Log.d(">> CGI-Nice-Ppl", "[sincronizacionExportacionST] Lanzamos hilo de generación de otps");
			new GeneraOTPs<String>(ownerController).execute();
			
		}
	}
					
	private void doSincronizacionExportacionST() {
			Log.d(">> CGI-Nice-Ppl", "[sincronizacionExportacionST] Inicio operacion de sincro exp st");
			final Hashtable<String, String> paramTable = new Hashtable<String, String>();
			
			final Session session= Session.getInstance(SuiteAppApi.appContext);

				session.setIum(generaIUMDatosBMOVIL());
				session.saveRecordStore();
				final String	ium=session.getIum();
			/*
			paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, ium);
			paramTable.put(ServerConstants.NUMERO_TELEFONO,	softToken.getNumeroTelefono());
			paramTable.put(ServerConstants.NUMERO_TARJETA, softToken.getNumeroTarjeta());
			paramTable.put(ServerConstants.TIPO_SOLICITUD, softToken.getTipoSolicitud());
			paramTable.put(ServerConstants.OTP1, otp1);
			paramTable.put(ServerConstants.OTP2, otp2);
			paramTable.put(ServerConstants.NOMBRE_TOKEN, softToken.getNombreToken());
			// TODO Modificacion VersionApp
			softToken.setVersionApp(Constants.APPLICATION_VERSION);
			paramTable.put(ServerConstants.VERSION_APP, softToken.getVersionApp());

			 */


			final Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
			paramTable2.put(ServerConstants.JSON_IUM_ETIQUETA, ium);
			paramTable2.put(ServerConstants.NUMERO_TELEFONO, softToken.getNumeroTelefono());
			paramTable2.put(ServerConstants.NUMERO_TARJETA, softToken.getNumeroTarjeta());
			paramTable2.put(ServerConstants.TIPO_SOLICITUD, softToken.getTipoSolicitud());
			paramTable2.put(ServerConstants.OTP1, otp1);
			paramTable2.put(ServerConstants.OTP2, otp2);
			paramTable2.put(ServerConstants.NOMBRE_TOKEN, softToken.getNombreToken());
			softToken.setVersionApp(Constants.APPLICATION_VERSION);
			paramTable2.put(ServerConstants.VERSION_APP, softToken.getVersionApp());
			Log.d(">> CGI-Nice-Ppl", "[sincronizacionExportacionST] params "+paramTable2.toString());
			//JAIG
			doNetworkOperation(Server.OP_SINC_EXP_TOKEN, paramTable2,true, new SincroExportSTData(), ownerController);
	}
	
	/***************************************************************************************************************************************************/

	/**
	 * Realiza la operacion de consulta estatus mantenimiento.
	 */
	private void consultaEstatusMantenimiento() {
		ownerController.hideCurrentDialog();
		
		consultaEstatus = new ConsultaEstatus();

		final String numeroCelular = softToken.getNumeroTelefono();
		consultaEstatus.setNumCelular(numeroCelular);

		final String autVersion = CatalogoAutenticacionFileManager.getCurrent().leerVersionArchivoCatalogoAutenticacion();
		final String compVersion = Session.getInstance(ownerController).getVersionCatalogoTelefonicas();

		/*
		 Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.NUMERO_TELEFONO, numeroCelular);
		params.put(Server.AUTH_CAT_PARAM, Tools.isEmptyOrNull(autVersion) ? "0"	: autVersion);
		params.put(Server.COMPANIES_PARAM, Tools.isEmptyOrNull(compVersion) ? "0" : compVersion);
		 */
		final Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
		
		paramTable2.put(ServerConstants.NUMERO_TELEFONO, numeroCelular);
		paramTable2.put(ServerConstants.VERSION_MIDLET, ServerConstants.APP_VERSION_CONSULTA);
		paramTable2.put("verCatTelefonicas", Tools.isEmptyOrNull(compVersion) ? "0" : compVersion);
		paramTable2.put("verCatAutenticacion", Tools.isEmptyOrNull(autVersion) ? "0" : autVersion);

		final List<String> listaEncriptar = Arrays.asList(
				ApiConstants.OPERACION, ServerConstants.NUMERO_TELEFONO
				,ServerConstants.VERSION_MIDLET, "verCatTelefonicas","verCatAutenticacion"
				);
		CommContext.operacionCode = Server.CONSULTA_MANTENIMIENTO;
		CommContext.listaEncriptar = listaEncriptar;

		//JAIG
		doNetworkOperation(Server.CONSULTA_MANTENIMIENTO, paramTable2, true, new ConsultaEstatusMantenimientoData(), ownerController);
	}

	/**
	 * Procesa la respuesta de la operacion de consulta tarjeta ST para mostrar
	 * la pantalla de Autenticacion para token nuevo (ConfirmationST) cuando el
	 * tipo de contrato es de token nuevo o de una reactivacion.
	 * 
	 * @param consultaTarjetaRespuesta
	 *            la respuesta a la peticion
	 */
	private void procesarConsultaTarjetaST(final ConsultaTarjetaSTRespuesta consultaTarjetaRespuesta) {

		ownerController.ocultaIndicadorActividad();
		ownerController.hideCurrentDialog();
		
		softToken.setCorreoElectronico(consultaTarjetaRespuesta.getCorreoElectronico());
		softToken.setNumeroCliente(consultaTarjetaRespuesta.getNumeroCliente());
		softToken.setTipoSolicitud(consultaTarjetaRespuesta.getIndicadorContratacion());
		softToken.setNombreCliente(consultaTarjetaRespuesta.getNombreCliente());
		
		if (SofttokenConstants.TIENE_DISPOSITIVO_FISICO.equals(consultaTarjetaRespuesta.getDispositivoFisico())
				&& Constants.STATUS_APP_ACTIVE.equals(consultaTarjetaRespuesta.getEstatusDispositivo())
				&& SofttokenConstants.NO_EXISTE_SOLICITUD.equals(consultaTarjetaRespuesta.getIndicadorContratacion())) {
			//EA#9
			ownerController.setHabilitado(true);
			if (SofttokenConstants.SWITCH_ENROLAMIENTO_ACTIVADO.equals(consultaTarjetaRespuesta.getSwitchEnrolamiento())){
				if( SofttokenConstants.VALIDACION_TARJETA_06==(Integer.valueOf(consultaTarjetaRespuesta.getValidacionAlertas()))){
					softToken.setEstatusDispositivo(consultaTarjetaRespuesta.getEstatusDispositivo());
					softToken.setDispositivoFisico(consultaTarjetaRespuesta.getDispositivoFisico());
					resultadoValidacion(Integer.valueOf(consultaTarjetaRespuesta.getValidacionAlertas()), consultaTarjetaRespuesta.getNumeroAlertas());
				}/*else{
					//switch encendido
					resultadoValidacion(Integer.valueOf(consultaTarjetaRespuesta.getValidacionAlertas()), consultaTarjetaRespuesta.getNumeroAlertas());
				}*/
			}else if(Constants.SWITCH_ENROLAMIENTO_APAGADO.equals(consultaTarjetaRespuesta.getSwitchEnrolamiento())){
				resultadoValidacion(Integer.valueOf(consultaTarjetaRespuesta.getValidacionAlertas()), consultaTarjetaRespuesta.getNumeroAlertas());
			}
			//ownerController.showInformationAlert(ownerController.getString(R.string.label_information), ownerController.getString(R.string.softoken_error_cliente_tiene_token), ownerController.getString(R.string.common_accept),null);
		} else {
			// Se comprueba que el indicador de contratacion el correspondiente
			// al flujo Activacion Softoken nuevo token (N, R) o
			// Activacion Softtoken Sustitucion (S).
			final String indicadorContratacion = softToken.getTipoSolicitud();
			if (!SofttokenConstants.NO_EXISTE_SOLICITUD.equals(indicadorContratacion)) {

				/*Reactivación Token sin Bmóvil IDS Comercial eluna*/
				if (SofttokenConstants.REACTIVACION.equals(indicadorContratacion) && (consultaTarjetaRespuesta.getEstatusBmovil().equals(Constants.STATUS_BANK_CANCELED) || consultaTarjetaRespuesta.getEstatusBmovil().equals(Constants.STATUS_USER_CANCELED))){

						final Session session = Session.getInstance(SuiteAppApi.appContext);
						session.setClientProfile(Perfil.avanzado);
						session.setUsername(softToken.getNumeroTelefono());
						session.setCompaniaUsuario(softToken.getCompanniaCelular());

						borrarDatosUsuario();
                        SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().setCurrentActivityApp(this.ownerController);
						consultaEstatusMantenimiento();


				}else {
                    //ownerController.showConfirmacionST();
					if(softToken.getNumeroTarjeta().substring(0,4).equals("0017"))
					{
						consultaCoordenadasTAS(softToken.getNumeroTelefono(),softToken.getNumeroTarjeta());
					}
					else
					{
						SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().setCurrentActivityApp(this.ownerController);

						SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showConfirmacionST(softToken);
					}
                }
				
				
				
			} else {
				final Session session = Session.getInstance(SuiteAppApi.appContext);
				
				// El sistema valida que el switch de enrolamiento este encendido.
				//if ("S".equals(consultaTarjetaRespuesta.getSwitchEnrolamiento())) {
				if (Constants.SWITCH_ENROLAMIENTO_ACTIVADO.equals(consultaTarjetaRespuesta.getSwitchEnrolamiento())) {
					//EA#4
					
					// Ejecuta caso de uso Solicitud Softtoken EP
					
					if (!Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CONTRATAR))) {
						resultadoValidacion(Integer.valueOf(consultaTarjetaRespuesta.getValidacionAlertas()), consultaTarjetaRespuesta.getNumeroAlertas());
					} else {
						solicitudST();
					}
					
				} else {
					//EA#10
					if (Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CONTRATAR))) {
						session.saveBanderasBMovil(Constants.BANDERAS_CONTRATAR, false, false);
					} 
					
					session.deleteTemporalST();
					
					ownerController.setHabilitado(true);
					ownerController.showInformationAlert(ownerController.getString(R.string.label_information), ownerController.getString(R.string.softoken_error_switch_enrolamiento_apagado), ownerController.getString(R.string.common_accept),null); 
				}
			}
		}		
	}

	/**
	 * Procesa el valor del atributo validacionAlertas tras la respuestas del
	 * servidor ante la consulta de tarjeta ST.
	 * 
	 * @param validacionAlerta
	 *            la validacion de la alerta
	 * @param numeroAlertas
	 *            el numero de las alertas
	 */
	private void resultadoValidacion(final Integer validacionAlerta,
			final String numeroAlertas) {

		if (SofttokenConstants.VALIDACION_TARJETA_00 == validacionAlerta) {
			// Solicitud Softtoken, EA#2
			ownerController.setHabilitado(true);
			ownerController.showYesNoAlert(R.string.softtoken_solicitud_invitacion_contratarBmovil,
					new OnClickListener() {
						@Override
						public void onClick(final DialogInterface dialog, final int which) {
							
							final Session session = Session.getInstance(SuiteAppApi.appContext);
							session.setClientProfile(Perfil.avanzado);
							session.setUsername(softToken.getNumeroTelefono());
							session.setCompaniaUsuario(softToken.getCompanniaCelular());
							
							borrarDatosUsuario();
							consultaEstatusMantenimiento();								
						}
					});	
		} else if (SofttokenConstants.VALIDACION_TARJETA_06 == validacionAlerta) {
			// Solicitud Softtoken, escenario principal
			solicitudST();
		} else {
			String message = null;
			boolean alertYesNo = false;
			switch (validacionAlerta) {
			case SofttokenConstants.VALIDACION_TARJETA_01:
				// Solicitud Softtoken, EA#5
				message = ownerController.getString(R.string.contratacion_validacion01);
				break;
			case SofttokenConstants.VALIDACION_TARJETA_02:
			case SofttokenConstants.VALIDACION_TARJETA_03:
				// Solicitud Softtoken, EA#6, EA#7
				message = String.format(ownerController.getString(R.string.contratacion_validacion0203), numeroAlertas);
				break;
			case SofttokenConstants.VALIDACION_TARJETA_04:
				// Solicitud Softtoken, EA#8
				//message = ownerController.getString(R.string.contratacion_validacion04);
				message = String.format(ownerController.getString(R.string.contratacion_validacion04), numeroAlertas);
				alertYesNo = true;
				break;
			case SofttokenConstants.VALIDACION_TARJETA_05:
				// Solicitud Softtoken, EA#9
				message = ownerController.getString(R.string.contratacion_validacion05);
				alertYesNo = true;
				break;
			}

			if (alertYesNo) {
				ownerController.showYesNoAlert(message, new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						Session.getInstance(SuiteAppApi.appContext).saveBanderasBMovil(Constants.BANDERAS_CAMBIO_CELULAR, true, true);
						guardarTemporalCambioTelefono();
						solicitudST();
					}
				});
			} else {
				ownerController.showInformationAlert(ownerController.getString(R.string.label_information), message, ownerController.getString(R.string.common_accept), null);
			}
		}
	}

	/**
	 * Guarda temporalCambioTelefono
	 */
	private void guardarTemporalCambioTelefono() {
		final Session session = Session.getInstance(SuiteAppApi.appContext);

		final TemporalCambioTelefono temporalCambioTelefono = new TemporalCambioTelefono(
				softToken.getNumeroTarjeta());

		session.saveTemporalCambioTelefono(temporalCambioTelefono, true);
	}

	/**
	 * Procesa la respuesta de la operacion de autenticacion ST para mostrar la
	 * pantalla de Clave Activacion ST.
	 * 
	 * @param autenticacionTokenRespuesta
	 *            la respuesta a la peticion
	 */
	private void procesarAutenticacionST(
			final AutenticacionSTRespuesta autenticacionTokenRespuesta) {
		softToken.setNombreToken(autenticacionTokenRespuesta.getNombreToken());
		softToken.setNumeroSerie(autenticacionTokenRespuesta.getNumeroSerie());

		final SofttokenActivationBackupManager manager = SofttokenActivationBackupManager
				.getCurrent();
		if (manager.existsABackup()) {
			manager.deleteBackup();
		}

		manager.initPropertiesFile();
		manager.setApplicationActivationValue(SofttokenActivationBackupManager.NUMERO_TARJETA_PROPERTY,	softToken.getNumeroTarjeta());
		manager.setApplicationActivationValue(SofttokenActivationBackupManager.NUMERO_TELEFONO_PROPERTY, softToken.getNumeroTelefono());
		manager.setApplicationActivationValue(SofttokenActivationBackupManager.NUMERO_CLIENTE_PROPERTY, softToken.getNumeroCliente());
		manager.setApplicationActivationValue(SofttokenActivationBackupManager.TIPO_SOLICITUD_PROPERTY, softToken.getTipoSolicitud());
		manager.setApplicationActivationValue(SofttokenActivationBackupManager.NOMBRE_TOKEN_PROPERTY, softToken.getNombreToken());
		manager.setApplicationActivationValue(SofttokenActivationBackupManager.NUMERO_SERIE_PROPERTY, softToken.getNumeroSerie());
		manager.setApplicationActivationValue(SofttokenActivationBackupManager.CORREO_ELECTRONICO_PROPERTY, softToken.getCorreoElectronico());
		manager.setApplicationActivationValue(SofttokenActivationBackupManager.COMPANIA_CELULAR_PROPERTY, softToken.getCompanniaCelular());
		manager.storeActivationValues();

		SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showPantallaCActivacionST();
		
	}

	/**
	 * Procesa la respuesta de la operacion de finalizar contratacion ST.
	 */
	private void procesarFinalizarContratacionST() {
		final Session session = Session.getInstance(SuiteAppApi.appContext);
		session.deleteTemporalST();
		
		//Incidencia #22026 - para mostrar en login el usuario guardado debe de activarse tambi�n en session
		session.setApplicationActivated(true);
		PropertiesManager.getCurrent().setBmovilActivated(true);

		final SofttokenSession softTokenSession = SofttokenSession.getCurrent();
		session.setUsername(softTokenSession.getUsername());
//		session.setSeed(softTokenSession.getSeed());
		session.saveRecordStore();
		
//		session.saveBanderasBMovil(Constants.BANDERAS_CONTRATAR, false, true);

		realizarCambioDeCelular();
	}

	/**
	 * Procesa la respuesta de la operacion de cambio de celular ST.
	 */
	private void procesarCambioDeCelularST() {
		final Session session = Session.getInstance(SuiteAppApi.appContext);
		session.saveBanderasBMovil(Constants.BANDERAS_CAMBIO_CELULAR, false, true);

		session.deleteTemporalCambioTelefono();

		borrarDatosUsuario();

		showPantallaEmailST(null);
	}

	/**
	 * Procesa la respuesta de la operacion de solicitud softToken.
	 */
	private void procesarSolicitudST() {
		//conprobar si añadir aqui el atributo softoken provoca error
		SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showConfirmacionST(softToken);
	}

	/**
	 * Procesa la respuesta de la operacion consulta de estatus mantenimiento.
	 * 
	 * @param response
	 *            la respuesta del servidor a dicha operacion
	 */
    private void procesarConsultaEstatusMantenimiento(final ServerResponse response) {
		final ConsultaEstatusMantenimientoData serverResponse = (ConsultaEstatusMantenimientoData) response
				.getResponse();
		Session.getInstance(SuiteAppApi.appContext).setSecurityInstrument(
				serverResponse.getTipoInstrumento());
		llenarConsultaEstatus(serverResponse);
		
		if (isActivacionSoftokenNuevo) {
			isActivacionSoftokenNuevo = false;
			// Eliminacion 2x1
			//PASO 37 flujo principal(Se valida consultaEstatus diferente a:  A1 || PA || PE)
			if (!(consultaEstatus.getEstatus().equalsIgnoreCase("A1")
					|| consultaEstatus.getEstatus().equalsIgnoreCase("PA") || consultaEstatus
					.getEstatus().equalsIgnoreCase("PE"))){
					sincronizarToken();
			}else{
				//FA 12 PASO 3 (El  instrumento de seguridad == S1)
				if(consultaEstatus.getInstrumento().equalsIgnoreCase("S1")){
					Session.getInstance(ownerController).setSeed(System.currentTimeMillis());
					DatosBmovilFileManager.getCurrent().setSeed(String.valueOf(System.currentTimeMillis()));
					
					sincronizacionExportacionST();
				}else{
					sincronizarToken();
				}
			}
			
		/*	if (!(consultaEstatus.getEstatus().equalsIgnoreCase("A1")
					|| consultaEstatus.getEstatus().equalsIgnoreCase("PA") || consultaEstatus
					.getEstatus().equalsIgnoreCase("PE"))

					&& !consultaEstatus.getInstrumento().equalsIgnoreCase("S1")) {

				sincronizarToken();
			} else {

				Session.getInstance(ownerController).setSeed(System.currentTimeMillis());
				DatosBmovilFileManager.getCurrent().setSeed(String.valueOf(System.currentTimeMillis()));

				sincronizacionExportacionST();
			}*/
		} else {
			ownerController.ocultaIndicadorActividad();
			SuiteAppApi.getInstanceApi()
			.getBmovilApplicationApi().setViewsController(new BmovilViewsController());
			//BmovilViewsController bmovil = new BmovilViewsController();
			ownerController.ocultaIndicadorActividad();
			//bmovil.showContratacionEP11(
			//		(IngresoDatosSTViewController) ownerController,
			//		consultaEstatus, consultaEstatus.getEstatus(),
			//		false);
			SuiteAppApi.getInstanceApi()
					.getBmovilApplicationApi()
					.getBmovilViewsController()
					.showContratacionEP11(
							(IngresoDatosSTViewController) ownerController,
							consultaEstatus, consultaEstatus.getEstatus(),
							false);

           }
    }

	/**
	 * Muestra la pantalla de email ST.
	 * 
	 * @param mensajeError
	 *            un mesaje de error a mostrar en la pantalla
	 */
	private void showPantallaEmailST(final String mensajeError) {
		ownerController.ocultaIndicadorActividad();

		Object[] extras = null;
		if (mensajeError != null) {
			extras = new Object[1];
			extras[0] = mensajeError;
		}

		SuiteAppApi.getInstanceApi().getSofttokenApplicationApi()
				.getSottokenViewsController().showPantallaEmailST(extras);
	}

	/**
	 * @return the softokenViewsController
	 */
	public SofttokenViewsController getSoftokenViewsController() {
		return SuiteAppApi.getInstanceApi().getSofttokenApplicationApi()
				.getSottokenViewsController();
	}

	/**
	 * Borra los datos del usuario si la aplicacion Bmovil esta activa.
	 */
	private void borrarDatosUsuario() {
		final Session session = Session.getInstance(SuiteAppApi.appContext);
		if (PropertiesManager.getCurrent().getBmovilActivated()) {
			session.deleteRecordStore();
			session.clearSession();
			PropertiesManager.getCurrent().setBmovilActivated(false);
		}
	}

	/**
	 * Muestra el mensaje de alerta tras continuar desde la pantalla Correo
	 * Electronico. Además, realiza un cambio de perfil si la variable
	 * cambioPerfil es verdadera.
	 */
	public void muestraAlertaBienvenida() {
		ownerController.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				final Session session = Session.getInstance(SuiteAppApi.appContext);

				int title = R.string.softtoken_activacion_exitosa_contratarBmovilTrue;
				if (!Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CONTRATAR))) {
					title = R.string.softtoken_activacion_exitosa_contratarBmovilFalse;
				}

				if(!esReact2x1){
					ownerController.showInformationAlert(R.string.softtoken_activacion_exitosa_titulo, title,
							new OnClickListener() {
								@Override
								public void onClick(final DialogInterface dialog, final int which) {
									session.saveBanderasBMovil(Constants.BANDERAS_CONTRATAR, false, true);
									
									ownerController.getParentViewsController().removeDelegateFromHashMap(CONTRATACION_ST_DELEGATE_ID);
									if (Boolean.FALSE.equals(errorFinalizarContratacion) && Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CAMBIO_PERFIL))) {
										SuiteAppApi.getInstanceApi().getSuiteViewsControllerApi().showMenuSuite(true, new String[] { "bmovilselected" });
									} else {
										showMenuSuite();
									}
									
								}
							});
				}else{
					esReact2x1 = false;
					ownerController.showInformationAlert("", ownerController.getString(R.string.softtoken_reactivacion_alert_activacion_servicios),
							new OnClickListener() {
								@Override
								public void onClick(final DialogInterface dialog, final int which) {
									session.saveBanderasBMovil(Constants.BANDERAS_CONTRATAR, false, true);
									
									ownerController.getParentViewsController().removeDelegateFromHashMap(CONTRATACION_ST_DELEGATE_ID);
									if (Boolean.FALSE.equals(errorFinalizarContratacion) && Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CAMBIO_PERFIL))) {
										SuiteAppApi.getInstanceApi().getSuiteViewsControllerApi().showMenuSuite(true, new String[] { "bmovilselected" });
									} else {
										showMenuSuite();
									}
								}
					});
				}
			}
		});
	}

	/**
	 * Muestra la pantalla de la menu suite.
	 */
	private void showMenuSuite() {
		//SuiteAppApi.getInstanceApi().getBmovilApplicationApi().getBmovilViewsController().showViewController(SuiteAppApi.getIntentToReturn().getClass());
		//SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getViewsController().showViewController(SuiteAppApi.getIntentToReturn().getClass());
		SuiteAppApi.getIntentToReturn().returnObjectFromApi(null);
	}
	
	/**
	 * Muestra la pantalla principal de softoken Ingresa datos
	 */
	private void showIngresaDatos() {
		SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showPantallaIngresoDatos();		
	}
	
	/**
	 * Muestra la pantalla de Consulta Estatus Desactivada
	 */
	private void showConsultaEstatusAppDesactivada() {
		//SuiteAppApi.getInstanceApi().closeSofttokenAppApi();
		//SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getViewsController().showViewController(SuiteAppApi.getIntentToReturn().getClass());
		SuiteAppApi.getIntentToReturn().returnObjectFromApi(null);
		
	}
	
	private void doRestartDatabase() {
		ownerController.showInformationAlert(R.string.reset_database_label,
				R.string.msg_reset_database_question, new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						restartDatabase();
					}
				});
	}

	protected void restartDatabase() {
		try {
			SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().resetApplicationData();
			ownerController.showInformationAlert(R.string.msg_reset_database_result);
		} catch (Exception e) {
			if(Server.ALLOW_LOG) Log.e("restartDatabase", "Unable to restart the database.", e);
			ownerController.showInformationAlert(R.string.msg_reset_database_fail);
		}
	}

	public void sincronizarToken() {
		//se muestra el indicador mientras se genran las otps
		ownerController.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				ownerController.muestraIndicadorActividad(
						ownerController.getString(R.string.alert_operation),
						ownerController.getString(R.string.alert_connecting));
			}
		});
		if (doStartApplication()) {
			final String sincronizacionOTP1 = generaTokendelegate.generaOTPTiempo();
			String sincronizacionOTP2Temporal = generaTokendelegate.generaOTPTiempo();
			if (sincronizacionOTP2Temporal.equalsIgnoreCase(sincronizacionOTP1)) {
				sincronizacionOTP2Temporal = generarTokenParaSincronizacion(SofttokenConstants.ESPERA_15_SEG);
				if (sincronizacionOTP2Temporal.equalsIgnoreCase(sincronizacionOTP1)) {
					sincronizacionOTP2Temporal = generarTokenParaSincronizacion(SofttokenConstants.ESPERA_8_SEG);
					if (sincronizacionOTP2Temporal.equalsIgnoreCase(sincronizacionOTP1)){
						sincronizacionOTP2Temporal = generarTokenParaSincronizacion(SofttokenConstants.ESPERA_7_SEG);
					}
				}
			}

			final String sincronizacionOTP2 = sincronizacionOTP2Temporal;
			ownerController.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					//se oculta el indicador
					ownerController.ocultaIndicadorActividad();
					sincronizacionToken(sincronizacionOTP1, sincronizacionOTP2);
				}
			});
		}
	}

	public void sincronizarTokenPM() {
		if (doStartApplication()) {
			final String sincronizacionOTP1 = generaTokendelegate.generaOTPTiempo();
			String sincronizacionOTP2Temporal = generaTokendelegate.generaOTPTiempo();
			if (sincronizacionOTP2Temporal.equalsIgnoreCase(sincronizacionOTP1)) {
				sincronizacionOTP2Temporal = generarTokenParaSincronizacion(SofttokenConstants.ESPERA_15_SEG);
				if (sincronizacionOTP2Temporal.equalsIgnoreCase(sincronizacionOTP1)) {
					sincronizacionOTP2Temporal = generarTokenParaSincronizacion(SofttokenConstants.ESPERA_8_SEG);
					if (sincronizacionOTP2Temporal.equalsIgnoreCase(sincronizacionOTP1)){
						sincronizacionOTP2Temporal = generarTokenParaSincronizacion(SofttokenConstants.ESPERA_7_SEG);
					}
				}
			}

			final String sincronizacionOTP2 = sincronizacionOTP2Temporal;
			ownerController.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					sincronizacionTokenPM(sincronizacionOTP1, sincronizacionOTP2);
				}
			});
		}
	}

	private String generarTokenParaSincronizacion(final long tiempoEspera) {
		try {
			Thread.sleep(tiempoEspera);
		} catch (InterruptedException e) {
			if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(),
					"Error en la espera para el siguiente token.", e);
		}

		return generaTokendelegate.generaOTPTiempo();
	}

	public Boolean doStartApplication() {
		final SofttokenApp app = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi();
		final MTokenCore core = app.getCore();

		if (core.isInitialized()) {
			if (core.isDead()) {
				if(Server.ALLOW_LOG) Log.w(TAG, "The password is dead.");
				doRestartDatabase();
			} else {
				if (core.isBlocked()) {
					if(Server.ALLOW_LOG) Log.w(TAG, "The password is blocked.");
					char passwd[] = null;
					char puk[] = null;
					try {
						passwd = generaIUM().toCharArray();
						puk = PUK.toCharArray();
						if(Server.ALLOW_LOG) Log.v(getClass().getSimpleName(), passwd.toString());
						if(Server.ALLOW_LOG) Log.v(getClass().getSimpleName(), puk.toString());
						if (core.resetPassword(puk, passwd)) {
							//guardarpass(passwd);
							return true;
						} else {
							if (core.isDead())
								ownerController
										.showInformationAlert(R.string.msg_blocked_puk);
							else
								ownerController
										.showInformationAlert(R.string.msg_wrong_puk);
							return false;
						}
					} catch (Exception e) {
						if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(),
								"Error while attending the blocked password", e);
					} finally {
						Util.shred(passwd);
						Util.shred(puk);
					}
				} else {
					// Login!
					char passwd[] = null;
					try {
						// Get the password
						passwd = generaIUM().toCharArray();
						if(Server.ALLOW_LOG) Log.v(getClass().getSimpleName(), passwd.toString());
						if (core.login(passwd)) {
							try {
								enterprises = this.getCore()
										.getEnterpriseList();
								accounts.clear();
								if(Server.ALLOW_LOG)
									Log.e(getClass().getSimpleName(), "Numero empresas  token Gmalto: ."+enterprises.size());
								accounts.setSize(enterprises.size());
							} catch (Exception e1) {
								if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(),
									"Error while trying to login.", e1);
								return false;
							}
							selectedAccount = (Account) this.getChild(0, 0);
							if (selectedAccount != null) {
								doLoadAccount(selectedAccount);
							}

							return true;
						} else {
							if (core.isBlocked()) {
								ownerController
										.showInformationAlert(R.string.msg_blocked_password);
								return false;
							} else {
								ownerController
										.showInformationAlert(R.string.msg_wrong_password);
							}
						}
					} catch (Exception e) {
						if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(),
							"Error while deleting the db data.", e);
						return false;
					} finally {
						Util.shred(passwd);
					}
				}
			}
		} else {
			// Goto initialization
			if(Server.ALLOW_LOG) Log.i(getClass().getSimpleName(),
					"The core is not initialized goto the create password screen");
			char puk[] = null;
			char passwd[] = null;
			try {
				// Create the PUK and ask for confirmation...
				passwd = generaIUM().toCharArray();
				puk = PUK.toCharArray();
				if(Server.ALLOW_LOG) Log.v(getClass().getSimpleName(), passwd.toString());
				if(Server.ALLOW_LOG) Log.v(getClass().getSimpleName(), puk.toString());
				// Set the password!
				core.initialize(passwd, puk);
				if (core.isInitialized()) {
					//guardarpass(passwd);
					return true;
				}
			} catch (Exception e) {
				String message;
				message = ownerController
						.getString(R.string.msg_create_password_failed);
				if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(), message, e);
				ownerController.showInformationAlert(message);
				return false;
			} finally {
				Util.shred(puk);
				Util.shred(passwd);
			}
		}
		return false;
	}

	// private void guardarpass(char[] passwd,Activity context) {
	// String pass="";
	// for(int i=passwd.length-1;i>=0;i--)
	// {
	// pass=passwd[i]+pass;
	// }
	//
	// Session session = Session.getInstance(context.getApplicationContext());
	// session.setIum_SF(pass);
	// session.storeSession();
	//
	// }


	// private String getpass(Activity context)
	// {
	// Session session = Session.getInstance(context.getApplicationContext());
	// return session.getIum_SF();
	// }

	private String getIUM() {
		return SofttokenSession.getCurrent().getIum();
	}
	
	private String generaIUMDatosBMOVIL() {
		final Session session = Session.getInstance(SuiteAppApi.appContext);
		long seed = session.getSeed();
		if (seed == 0) {
			seed = System.currentTimeMillis();
			session.setSeed(seed);
		}
	

		

		return Tools.buildIUM(softToken.getNumeroTelefono(), session.getSeed(), SuiteAppApi.appContext);
	}

	/**
	 * Genera la contrasenna
	 * 
	 * @return
	 */
	private String generaIUM() {
		final SofttokenSession softTokenSession = SofttokenSession.getCurrent();
		long seed = softTokenSession.getSeed();
		if (seed == 0) {
			seed = System.currentTimeMillis();
			softTokenSession.setSeed(seed);
		}

		if (softTokenSession.getUsername() == null || Constants.EMPTY_STRING.equals(softTokenSession.getUsername())) {
			softTokenSession.setUsername(softToken.getNumeroTelefono());
		}

		softTokenSession.saveSottokenRecordStore();

		return Tools.buildIUM(softTokenSession.getUsername(), softTokenSession.getSeed(), SuiteAppApi.appContext);
	}

	public void doLoadAccount(final Account account) {
		try {
			SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getCore()
					.loadToken(account);
		} catch (Exception e) {
			if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(),
				"Unable to load the account " + account.getName()
						+ " from the enterprise "
							+ account.getEnterpriseID(), e);
			ownerController.showInformationAlert(R.string.msg_token_not_loaded);
		}
	}

	private Vector<Account> getAccountList(final int enterpriseIndex) {
		Vector<Account> list;
		list = this.accounts.elementAt(enterpriseIndex);
		if (list == null) {
			try {
				list = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getCore().getAccountList(this.enterprises.elementAt(enterpriseIndex));
				this.accounts.set(enterpriseIndex, list);
			} catch (Exception e) {
				if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(), "Unable to fetch the account list.", e);
			}
		}
		return list;
	}

	private MTokenCore getCore() {
		return SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getCore();
	}

	public long getChildId(final int groupPosition, final int childPosition) {
		final Vector<Account> list = getAccountList(groupPosition);

		if (list != null) {
			return list.elementAt(childPosition).getTag();
		} else {
			return 0;
		}
	}

	public Object getChild(final int groupPosition, final int childPosition) {
		final Vector<Account> list = getAccountList(groupPosition);

		if (list != null) {
			return list.elementAt(childPosition);
		} else {
			return null;
		}
	}

	protected void tokenCommit(final Activity context) {
		try {
			// Commit the token into the database
			this.deployer.commit();
			this.deployer.dispose();
			this.deployer = null;
			// Display the success message!
			ownerController.showInformationAlert(R.string.msg_token_added);
		} catch (Exception e) {
			// Display the error message.
			if(Server.ALLOW_LOG) Log.e(TAG, "Unable to commit the token.", e);
			ownerController.showInformationAlert(R.string.msg_token_not_added);
		}
	}

	protected void doStart(final String Serial, final String ClaveActivacion,
			final ConfirmacionSTViewController context) {
		final SerialActivation serialActivation = new SerialActivation();
		// Validate the serial
		if (serialActivation.setSerial(Serial)) {
			if (serialActivation.setActivation(ClaveActivacion)) {
				// Create the deployer...
				this.deployer = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getCore().getDeployer(serialActivation);
				// Start the deployment...

				this.deployerMessageHandle = new Handler() {
					public void handleMessage(final Message msg) {
						processDownloadNotify(msg.arg1, msg.obj);
					}
				};

				startTimer();

				this.deployer.start(new DeploymentListener() {
					@Override
					public void notify(final int step, final Object params) {
						processDownloadNotify(step, params);
					}
				});
			} else {
				//clave de activacion incorrecta
				((ActivacionSTViewController) ownerController).showCodigoIncorrecto();
				//ownerController.showInformationAlert(R.string.msg_invalid_activation);

				try {
					ownerController.ocultaIndicadorActividad();
				} catch (Exception ex) {
					if(Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "No se pudo quitar el indicador de actividad, es posible que no se estuviera mostrando.", ex);
				}
			}
		} else {
			ownerController.showInformationAlert(R.string.msg_invalid_serial);
		}
	}

	protected void doStart(final String Serial, final String ClaveActivacion,
			final ActivacionSTViewController context) {
		final SerialActivation serialActivation = new SerialActivation();

		// Validate the serial
		if (serialActivation.setSerial(Serial)) {
			if (serialActivation.setActivation(ClaveActivacion)) {
				try {
					// Create the deployer...
					this.deployer = SuiteAppApi.getInstanceApi()
							.getSofttokenApplicationApi().getCore()
							.getDeployer(serialActivation);
					startTimer();

					// Start the deployment...
					this.deployer.start(new DeploymentListener() {
						@Override
						public void notify(final int step, final Object params) {
							processDownloadNotify(step, params);
						}
					});
				} catch (Exception e) {
					activando = false;
					ownerController.ocultaIndicadorActividad();

					((ActivacionSTViewController) ownerController).showCodigoIncorrecto();
					//ownerController.showErrorMessage(context.getString(R.string.msg_invalid_activation));

					try {
						ownerController.ocultaIndicadorActividad();
					} catch (Exception ex) {
						if(Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "No se pudo quitar el indicador de actividad, es posible que no se estuviera mostrando.", ex);
					}
				}
			} else {
				// Invalid activation
				activando = false;

				((ActivacionSTViewController) ownerController).showCodigoIncorrecto();
				//ownerController.showErrorMessage(context.getString(R.string.msg_invalid_activation));

				try {
					SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().resetApplicationData();
					
					ownerController.ocultaIndicadorActividad();
				} catch (Exception ex) {
					if(Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "No se pudo quitar el indicador de actividad, es posible que no se estuviera mostrando.", ex);
				}
			}
		} else {
			// Invalid serial
			activando = false;
			ownerController.showErrorMessage(context.getString(R.string.msg_invalid_serial));
			try {
				ownerController.ocultaIndicadorActividad();
			} catch (Exception ex) {
				if(Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(),"No se pudo quitar el indicador de actividad, es posible que no se estuviera mostrando.", ex);
			}
		}
	}

	public void doCommit() {
		try {
			// Commit the token into the database
			this.deployer.commit();
			this.deployer.dispose();
			this.deployer = null;
			// ownerController.runOnUiThread(new Runnable() {
			// @Override
			// public void run() {
			// ownerController.showInformationAlert(R.string.msg_token_added);
			// }
			// });
		} catch (Exception e) {
			// Display the error message.
			if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(), "Unable to commit the token.", e);
			ownerController.showInformationAlert(SuiteAppApi.appContext
					.getString(R.string.msg_token_not_added));
		}
	}

	public void processDownloadNotify(final int step, final Object params) {
		if(Server.ALLOW_LOG) Log.d(TAG, "Step=" + step + "; params=" + params);
		switch (step) {
		case DeploymentListener.DL_CONNECTING:
			break;/*eliminar al subir a svn*/
		case DeploymentListener.DL_REGISTER:/*eliminar al subir a svn*/
		/*	activando = false;
			isDownloadingToken = false;
			//doCommit();
			sincronizarToken();*/
			break;
		case DeploymentListener.DL_BROKER_AUTH:
		case DeploymentListener.DL_ENTERPRISE_REQUEST:
		case DeploymentListener.DL_ENTERPRISE_AUTH:
		case DeploymentListener.DL_REQUEST_ACCOUNT:
			break;
		case DeploymentListener.DL_COMPLETED:
			activando = false;
			isDownloadingToken = false;
			doCommit();
//			sincronizarToken();

			if(softToken.getNumeroTarjeta().substring(0,4).equals("0017"))
			{
				sincronizarTokenPM();
			}
			else{
				lanzarPeticion();
			}
			// this.downloadDialog.setTitle(R.string.add_token_sincronizando_label);
			// delegate.activando=false;
			// delegate.doCommit(this);
			// delegate.sincronizarToken(ConfirmacionSTViewController.this);
			// this.downloadDialog.setProgress(100);
			// this.downloadDialog.dismiss();
			break;
		case DeploymentListener.OFFLINE_COMPLETED:
			isDownloadingToken = false;
			ownerController.ocultaIndicadorActividad();
			break;
		case DeploymentListener.CANCELLED:
			isDownloadingToken = false;
			this.deployer.dispose();
			this.deployer = null;
			ownerController.ocultaIndicadorActividad();
			ownerController
					.showInformationAlert(R.string.msg_download_cancelled);
			break;
		case DeploymentListener.ERROR:
			isDownloadingToken = false;
			if (this.deployer != null) {
				this.deployer.dispose();
				this.deployer = null;
			}
			ownerController.ocultaIndicadorActividad();
			// this.downloadDialog.dismiss();
			// showErrorMessage(R.string.msg_token_not_added);
			if(Server.ALLOW_LOG) Log.e(TAG, "Error during the token deployment.", (Exception) params);
		}
	}

	private void lanzarPeticion() {
		isActivacionSoftokenNuevo = true;
			ownerController.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					consultaEstatusMantenimiento();
				}
			});
		
	}

	public boolean doLogin() {
		boolean ok = false;
		char passwd[] = null;
		final MTokenCore core = SuiteAppApi.getInstanceApi().getSofttokenApplicationApi()
				.getCore();
		try {
			// Get the password
			passwd = getIUM().toCharArray();
			if(Server.ALLOW_LOG) Log.v(getClass().getSimpleName(), passwd.toString());
			if (core.login(passwd)) {
				try {
					enterprises = this.getCore().getEnterpriseList();
					accounts.clear();
					accounts.setSize(enterprises.size());
				} catch (Exception e1) {
					if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(),
							"Error while trying to login.", e1);
					ok = false;
					return false;
				}
				selectedAccount = (Account) this.getChild(0, 0);
				if (selectedAccount != null) {
					doLoadAccount(selectedAccount);
				}

				ok = true;
				return true;
			}
		} catch (Exception e) {
			if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(),
					"Error while deleting the db data.", e);
			ok = false;
			return false;
		} finally {
			Util.shred(passwd);
		}
		return ok;
	}

	public void cargarDatosDelRespaldo() {
		final SofttokenActivationBackupManager manager = SofttokenActivationBackupManager
				.getCurrent();

		final String numeroTarjeta = manager.getApplicationActivationValue(SofttokenActivationBackupManager.NUMERO_TARJETA_PROPERTY);
		final String numeroTelefono = manager.getApplicationActivationValue(SofttokenActivationBackupManager.NUMERO_TELEFONO_PROPERTY);
		final String numeroCliente = manager.getApplicationActivationValue(SofttokenActivationBackupManager.NUMERO_CLIENTE_PROPERTY);
		final String tipoSolicitud = manager.getApplicationActivationValue(SofttokenActivationBackupManager.TIPO_SOLICITUD_PROPERTY);
		final String nombreToken = manager.getApplicationActivationValue(SofttokenActivationBackupManager.NOMBRE_TOKEN_PROPERTY);
		final String numeroSerie = manager.getApplicationActivationValue(SofttokenActivationBackupManager.NUMERO_SERIE_PROPERTY);
		final String correoElectronico = manager.getApplicationActivationValue(SofttokenActivationBackupManager.CORREO_ELECTRONICO_PROPERTY);
		final String companniaCelular = manager.getApplicationActivationValue(SofttokenActivationBackupManager.COMPANIA_CELULAR_PROPERTY);

		softToken = new SoftToken(numeroTarjeta, numeroTelefono, numeroCliente,
				tipoSolicitud, nombreToken, numeroSerie, correoElectronico,
				companniaCelular);

		//MOD
		//	generaTokendelegate.borraToken();
	
	}

	private boolean cargarDatos = false;

	public void setCargarDatos(final boolean cargarDatos) {
		this.cargarDatos = cargarDatos;
	}

	public boolean isCargarDatos() {
		return cargarDatos;
	}

	public void reenviarClaveActivacion() {
		final SofttokenSession session = SofttokenSession.getCurrent();
		session.setSofttokenActivated(false);
		final SofttokenActivationBackupManager manager = SofttokenActivationBackupManager.getCurrent();
		
		if (manager.existsABackup()){
			manager.deleteBackup();
			eliminaValorVariable(Constants.ACTIVA_TOKEN);
		}
		if(PropertiesManager.getCurrent().existeFile()){
			if (PropertiesManager.getCurrent().getBmovilActivated()) {
				if(SuiteAppApi.getInstanceApi().getIsAplicationLogged() && SuiteAppApi.getCallBackSession()!=null) {
					SuiteAppApi.getCallBackSession().cierraSesion();
				}else{
					showMenuSuite();
				}
			}else{
				if(SuiteAppApi.getInstanceApi().getIsAplicationLogged() && SuiteAppApi.getCallBackSession()!=null) {
					SuiteAppApi.getCallBackSession().cierraSesion();
				}else {
					showConsultaEstatusAppDesactivada();
				}
			}
		}

	}

	/**
	 * Flag to indicate if the Deployer is downloading the base token.
	 */
	private static boolean isDownloadingToken = false;

	/**
	 * Token download timeout time in miliseconds.
	 */
	private static final int TOKEN_DOWNLOAD_TIMEOUT = 30000;
	
	
	/**
	 * Token download timeout timer.
	 */
	private Timer timer;

	/**
	 * Starts the token download timeout timer.
	 */
	private void startTimer() {
		isDownloadingToken = true;
		timer = new Timer("tim", true);
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				cancelTokenDownload();
			}
		}, TOKEN_DOWNLOAD_TIMEOUT);
	}

	/**
	 * Cancels the token download and timer further executions.
	 */
	private void cancelTokenDownload() {
		if (isDownloadingToken && null != deployer) {
			if(Server.ALLOW_LOG) Log.d(getClass().getSimpleName(), "Token download canceled due to a timeout.");
			isDownloadingToken = false;
			deployer.dispose();
			deployer = null;
			ownerController.ocultaIndicadorActividad();
			ownerController.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					ownerController
							.showInformationAlert(R.string.error_communications);
				}
			});
		}
		timer.cancel();
		timer = null;
	}

	// #region IngresarDatos.
	/**
	 * Carga la lista de compañías para el componente seleccion horizontal.
	 * 
	 * @return La lista de compañías en el orden requerido.
	 */
	public ArrayList<Object> cargarCompaniasSeleccionHorizontal() {
		final ArrayList<Object> listaCompanias = new ArrayList<Object>();

		listaCompanias.add(new Compania(Constants.COMPANY_NAME_TELCEL, "telcel.png", "", "", ""));
		listaCompanias.add(new Compania(Constants.COMPANY_NAME_MOVISTAR, "movistar.png", "", "", ""));
		listaCompanias.add(new Compania(Constants.COMPANY_NAME_UNEFON, "unefon.png", "", "", ""));
		listaCompanias.add(new Compania(Constants.COMPANY_NAME_IUSACELL, "iusacell.png", "", "", ""));
		listaCompanias.add(new Compania(Constants.COMPANY_NAME_NEXTEL, "nextel.png", "", "", ""));

		return listaCompanias;

		// Descomentar si lo que se desea es recuperar desde catalogo las
		// compannias telefonicas
		/*
		 * CatalogoVersionado catalogoDineroMovil =
		 * Session.getInstance(SuiteApp.appContext).getCatalogoTelefonicas();
		 * Vector<Object> vectorCompanias = catalogoDineroMovil.getObjetos();
		 * int companiasSize = vectorCompanias.size(); ArrayList<Object>
		 * listaCompanias = new ArrayList<Object>(companiasSize); Compania
		 * currentCompania = null; for (int i=0; i<companiasSize; i++) {
		 * currentCompania = (Compania)vectorCompanias.get(i);
		 * listaCompanias.add(currentCompania); currentCompania = null; }
		 * 
		 * vectorCompanias = null; return listaCompanias;
		 */
	}

	/**
	 * @return the errorFinalizarContratacion
	 */
	public Boolean getErrorFinalizarContratacion() {
		return errorFinalizarContratacion;
	}

	/**
	 * @param errorFinalizarContratacion
	 *            the errorFinalizarContratacion to set
	 */
	public void setErrorFinalizarContratacion(final Boolean errorFinalizarContratacion) {
		this.errorFinalizarContratacion = errorFinalizarContratacion;
	}

	/**
	 * Lleva a cabo la logica de activacion ST desde Contratacion: escenario
	 * Principal EP#11.
	 * 
	 * @param consultaEstatus
	 *            el objeto consulta estatus
	 * @param contratacion
	 *            el objeto contratacion
	 */
	public void flujoContratacion(final ConsultaEstatus consultaEstatus, final Contratacion contratacion) {
		consultaTarjetaST(contratacion.getNumCelular(),	contratacion.getNumeroTarjeta(), contratacion.getCompaniaCelular());
	}

	public void flujoActivacionEA11(final ServerResponse response) {
		procesarConsultaTarjetaST((ConsultaTarjetaSTRespuesta) response.getResponse());
	}
	
	/**
	 * ActivacionST Escenario Principal paso 25
	 */
	public void flujoActivacionEP25() {
		SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showPantallaCActivacionST(true);
	}
	
	
	/**
	 * ActivacionST Escenario Alterno 12
	 */
	public void flujoActivacionEA12(){
		SuiteAppApi.getInstanceApi().getSofttokenApplicationApi().getSottokenViewsController().showPantallaCActivacionST(true);
	}
	
	public boolean isEA12(){
		return isEA12;
	}
	
	public void setEA12(final Boolean ea12){
		this.isEA12 = ea12;
	}

	/**
	 * Llena el objeto consulta estatus.
	 */
	private void llenarConsultaEstatus(
			final ConsultaEstatusMantenimientoData serverResponse) {
		consultaEstatus.setEstatus(serverResponse.getEstatusServicio());
		consultaEstatus.setNombreCliente(serverResponse.getNombreCliente());
		consultaEstatus.setInstrumento(serverResponse.getTipoInstrumento());
		consultaEstatus.setEstatusInstrumento(serverResponse.getEstatusInstrumento());
		consultaEstatus.setNumCliente(serverResponse.getNumeroCliente());
		consultaEstatus.setEmailCliente(serverResponse.getEmailCliente());
		consultaEstatus.setPerfilAST(serverResponse.getPerfilCliente());
		consultaEstatus.setEstatusAlertas(serverResponse.getEstatusAlertas());

		consultaEstatus.setPerfil(Perfil.avanzado);
		consultaEstatus.setNumCelular(softToken.getNumeroTelefono());
		consultaEstatus.setCompaniaCelular(softToken.getCompanniaCelular());
		consultaEstatus.setNumTarjeta(softToken.getNumeroTarjeta());
	}
	
	

	public void borrarDatosDeSession() {
		
		//	ownerController.muestraIndicadorActividad("", ownerController.getString(R.string.alert_StoreSession));
			
		
		/*	Session session = Session.getInstance(SuiteApp.appContext);
			session.setUsername(null);
			session.setCompaniaUsuario(null);
			session.setApplicationActivated(false);
			*/
			
		//este se borra?
			PropertiesManager.getCurrent().setSofttokenActivated(false);
			PropertiesManager.getCurrent().setBmovilActivated(false);
			
			DatosBmovilFileManager.getCurrent().setActivado(false);
			DatosBmovilFileManager.getCurrent().setSeed("0");
			//este se borra?
			DatosBmovilFileManager.getCurrent().setSoftoken(false);
			
	/*	
			session.setSeed(0);
			session.setAceptaCambioPerfil(true);
			//session.clearCatalogs();
			session.setSecurityInstrument(consultaEstatus.getInstrumento());
		//	session.saveRecordStore();
		*/
	}

	/**
	 * Valida que la variable enviada se encuentre en KeyChain y borrarl
	 * @param variable
	 */
	private void eliminaValorVariable(final String variable){
		final Session session = Session.getInstance(SuiteAppApi.appContext);
		
		// Intergracion KeyChainAPI
		// Recoger KeyStoreManager
		final KeyStoreWrapper kswrapper = session.getKeyStoreWrapper();
		try{
			kswrapper.storeValueForKey(variable, " ");
		//----
		}catch(Exception e){
			if(Server.ALLOW_LOG) e.printStackTrace();
		}
	}
	
	private void addVariableKeyChain(final String variable, final String valor){
		final Session session = Session.getInstance(SuiteAppApi.appContext);
		
		// Intergracion KeyChainAPI		
		// Recoger KeyStoreManager
		final KeyStoreWrapper kswrapper = session.getKeyStoreWrapper();
		try{
			kswrapper.storeValueForKey(variable, valor);
		//----	
		}catch(Exception e){
			if(Server.ALLOW_LOG) e.printStackTrace();
		}
	}

	public void mostrarAlertaCambio(){
		ownerController.showYesNoAlert1(R.string.bmovil_activacion_alert_cambio_numero,
				new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						dialog.dismiss();
						ownerController.setHabilitado(true);
						dialog.dismiss();
						mostrarAlert2();
					}
				},
				new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						reenviarClaveActivacion();
					}
				});
	}

	public void mostrarAlert2() {
		ownerController.showInformationAlert(R.string.bmovil_activacion_alert_acude_atm, new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						dialog.dismiss();
						reenviarClaveActivacion();
					}
				}
		);
	}
	public void alertaVerificaDatos() {
		ownerController.showInformationAlert(R.string.bmovil_activacion_alert_verifica_datos, new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						dialog.dismiss();
					}
				}
		);
	}

	public Boolean getIsFlujoAutenticacionSt() {
		return isFlujoAutenticacionSt;
	}

	public void setIsFlujoAutenticacionSt(Boolean isFlujoAutenticacionSt) {
		this.isFlujoAutenticacionSt = isFlujoAutenticacionSt;
	}
}