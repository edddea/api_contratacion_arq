/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

/**
 * TODO: COMENTARIO DE LA CLASE
 *
 * @author Stefanini IT Solutions.
 */

public class SoftokenList extends ListView implements OnItemClickListener{

	public SoftokenList(final Context context, final AttributeSet attrs, final int defStyle){
		
		super(context, attrs, defStyle);
		
		this.setOnItemClickListener(this);
		
		//this.setSelector(com.bancomer.mbanking.R.drawable.bkg_row_selected);
		this.setSelector(0);
		
	}

	/**
	 * Set as selected the clicked account. This is marked by an orange icon instead of the
	 * default gray one.
	 * 
	 * @param parent the parent view
	 * @param view the view of the click event
	 * @param position the position of element's click
	 * @param id the id of the element's click.
	 */
	public void onItemClick(final AdapterView<?> parent, final View view,
						    final int position, final long id) {
        // Empty method
	}

}


