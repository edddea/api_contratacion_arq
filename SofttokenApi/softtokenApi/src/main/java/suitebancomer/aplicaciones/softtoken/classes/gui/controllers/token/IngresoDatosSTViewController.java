/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import java.util.ArrayList;

import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomer.classes.gui.views.token.SeleccionHorizontalViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.common.GuiTools;
/*import suitebancomercoms.aplicaciones.bmovil.classes.gui.controllers.MenuAdministrarViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.gui.controllers.MenuPrincipalViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.gui.delegates.CambiarPasswordDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.gui.delegates.PagoTdcDelegate;
*/

public class IngresoDatosSTViewController extends SofttokenBaseViewController {
	private EditText tbNumeroCelular;
	private EditText tbNumeroTarjeta;
	private ContratacionSTDelegate contratacionDelegate;
	private static  final String TARJETA_TAS = "0017";

	/**
	 * Componente Seleccion Horizontal para elegir un compa�ia de celular.
	 */
	private SeleccionHorizontalViewController seleccionHorizontal;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE,
				getResources().getIdentifier("layout_softtoken_activacion_ingresa_datosapi", "layout", getPackageName())
			);
		setTitle(R.string.softtoken_activacion_titulo,R.drawable.icono_st_activado);
		
		final ContratacionSTDelegate delegate = new ContratacionSTDelegate();
		
		/*if ((this.getIntent().getExtras()!=null)&&(this.getIntent().getExtras().get("login") != null)) {

			setParentViewsController(SuiteAppApi.getInstanceApi()
					.getSofttokenApplicationApi().getSottokenViewsController());
			parentViewsController.setCurrentActivityApp(this);
			contratacionDelegate = (ContratacionSTDelegate) parentViewsController
					.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
			setDelegate(contratacionDelegate);

		} else {*/

			parentViewsController = SuiteAppApi.getInstanceApi()
					.getSofttokenApplicationApi().getSottokenViewsController();
			parentViewsController.addDelegateToHashMap(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID, delegate);
			parentViewsController.setCurrentActivityApp(this);
			setDelegate((ContratacionSTDelegate)parentViewsController
					.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
			contratacionDelegate = (ContratacionSTDelegate) getDelegate();

		//}

		contratacionDelegate.setOwnerController(this);

		init();
	}

	@Override
	protected void onResume() {

		// lo que estaba
		/*
		 * this.parentViewsController = SuiteApp.getInstance()
		 * .getSofttokenApplication().getSottokenViewsController();
		 * parentViewsController.setCurrentActivityApp(this);
		 * setDelegate(parentViewsController
		 * .getBaseDelegateForKey(ContratacionSTDelegate
		 * .CONTRATACION_ST_DELEGATE_ID)); contratacionDelegate =
		 * (ContratacionSTDelegate) getDelegate();
		 * contratacionDelegate.setOwnerController(this);
		 */

		/*if ((this.getIntent().getExtras()!=null)&&(this.getIntent().getExtras().get("login") != null)) {

			setParentViewsController(SuiteAppApi.getInstanceApi()
					.getBmovilApplicationApi().getBmovilViewsController());
			contratacionDelegate = (ContratacionSTDelegate) parentViewsController
					.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
			setDelegate(contratacionDelegate);

		} else {*/

			parentViewsController = SuiteAppApi.getInstanceApi()
					.getSofttokenApplicationApi().getSottokenViewsController();
			parentViewsController.setCurrentActivityApp(this);
			setDelegate((ContratacionSTDelegate)parentViewsController
					.getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID));
			contratacionDelegate = (ContratacionSTDelegate) getDelegate();

		//}

		contratacionDelegate.setOwnerController(this);

		super.onResume();
		// MenuSuiteViewController suiteViewController =
		// (MenuSuiteViewController)SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController();
		// suiteViewController.setShouldHideLogin(true);
		// suiteViewController.restableceMenu();
	}

	private void init() {
		findViews();
		cargarComponenteSeleccionHorizontal();
		scaleForCurrentScreen();
	}

	private void findViews() {
		tbNumeroCelular = (EditText) findViewById(SuiteAppApi.getResourceId("tbNumeroCelular", "id"));
		tbNumeroTarjeta = (EditText) findViewById(SuiteAppApi.getResourceId("tbNumeroTarjeta","id"));
		tbNumeroCelular.setText(Session.getInstance(SuiteAppApi.appContext).getUsername());

	}

	private void scaleForCurrentScreen() {
        final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		guiTools.scale(findViewById(SuiteAppApi.getResourceId("rootLayout","id")));
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblTitulo","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblNumeroCelular","id")), true);
		guiTools.scale(tbNumeroCelular, true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblCompaniaCelular","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblNumeroTarjeta","id")), true);
		guiTools.scale(tbNumeroTarjeta, true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("btnContinuar","id")), true);
		guiTools.scale(findViewById(SuiteAppApi.getResourceId("lblIntensidadInternet","id")), true);
	}

	/**
	 * Se selecciona la opci�n Continuar.
	 * 
	 * @param sender
	 *            la opci�n seleccionada
	 */
	public void onBtnContinuarClick(final View sender) {

			final Compania compania = (Compania) seleccionHorizontal.getSelectedItem();
			Session.getInstance(SuiteAppApi.appContext).setUsername(tbNumeroCelular.getText()
					.toString());
			contratacionDelegate.consultaTarjetaST(tbNumeroCelular.getText()
					.toString(), tbNumeroTarjeta.getText().toString(), compania
					.getNombre());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see suitebancomercoms.classes.gui.controllers.BaseViewController#
	 * processNetworkResponse(int,
	 * suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		contratacionDelegate.analyzeResponse(operationId, response);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see suitebancomercoms.classes.gui.controllers.BaseViewController#goBack()
	 */
	@Override
	public void goBack() {
/*
		if (SuiteApp.getInstance().getBmovilApplication()
				.getBmovilViewsController().getCurrentViewControllerApp() instanceof MenuAdministrarViewController) {
			parentViewsController
					.removeDelegateFromHashMap(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
		} else {
			MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteApp
					.getInstance().getSuiteViewsController()
					.getCurrentViewControllerApp();
			menuViewController.restableceMenu();
			menuViewController.setShouldHideLogin(true);

			((MenuSuiteViewController) SuiteApp.getInstance()
					.getSuiteViewsController().getCurrentViewControllerApp())
					.setShouldHideLogin(true);

		}
		super.goBack();
*/
	}



	{

	}
	// protected void onClickRightButton() {
	// /*Bundle data = new Bundle();
	// softokenViewsController.showPantallaCActivacionST(this, data);*/
	// numCelular=txtNumCelular.getText().toString();
	// numTarjeta=txtNumTarjeta.getText().toString();
	// if(numCelular.length() < Constants.TELEPHONE_NUMBER_LENGTH)
	// {
	// showErrorMessage(R.string.softoken_error_num_cel);
	// }
	// else if(numTarjeta.length() < Constants.CARD_NUMBER_LENGTH)
	// {
	// showErrorMessage(R.string.softoken_error_num_tar);
	// }
	// else{
	// delegate.leerStatusContracion(this);
	// }
	// }
	//
	// @Override
	// public boolean onKeyDown(int keyCode, KeyEvent event) {
	//
	// switch (keyCode){
	// case KeyEvent.KEYCODE_BACK:
	// case KeyEvent.KEYCODE_ENDCALL:
	// case KeyEvent.KEYCODE_POWER:
	// case KeyEvent.KEYCODE_HOME:
	// finish();
	// return true;
	// default:
	// return false;
	// }
	//
	// }
	//

	/**
	 * Carga el componente seleccion horizontal con los elementos necesarios.
	 */
	private void cargarComponenteSeleccionHorizontal() {
		// tituloSeleccionHorizontal.setText(getString(R.string.servicesPayment_servicesComponentTitle));
        final LinearLayout.LayoutParams params = new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        final ArrayList<Object> companias = contratacionDelegate
				.cargarCompaniasSeleccionHorizontal();
		seleccionHorizontal = new SeleccionHorizontalViewController(this,
				params, companias, getDelegate(), false);

        final LinearLayout seleccionHorizontalLayout = (LinearLayout) findViewById(SuiteAppApi.getResourceId("layoutSeleccionHorizontal", "id") );
		seleccionHorizontalLayout.addView(seleccionHorizontal);
	}

}
