package suitebancomer.aplicaciones.softtoken.classes.common.token;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import android.content.Context;
import android.util.Log;

import com.bancomer.mbanking.softtoken.SuiteAppApi;


public final class SofttokenActivationBackupManager {
	// #region Constantes.
	/**
	 * Nombre del archivo de propiedades.
	 */
	private static final String CONFIGURATION_FILE_NAME = "PendienteDescarga.prop";
	public static final String NUMERO_TARJETA_PROPERTY = "NumeroTarjeta";
	public static final String NUMERO_TELEFONO_PROPERTY = "NumeroTelefono";
	public static final String NUMERO_CLIENTE_PROPERTY = "NumeroCliente";
	public static final String TIPO_SOLICITUD_PROPERTY = "TipoSolicitud";
	public static final String NOMBRE_TOKEN_PROPERTY = "NombreToken";
	public static final String NUMERO_SERIE_PROPERTY = "NumeroSerie";	
	public static final String CORREO_ELECTRONICO_PROPERTY = "email";
	public static final String COMPANIA_CELULAR_PROPERTY = "CompaniaCelular";
	// #endregion
	
	// #region Variables.
	/**
	 * Propiedades del respaldo.
	 */
	private Properties properties;
	
	/**
	 * File model.
	 */
	private File file;
	// #endregion
	
	// #region Singleton.
	/**
	 * La instancia de la clase.
	 */
	private static SofttokenActivationBackupManager manager = null;
	
	/**
	 * @return La instancia de la clase.
	 */
	public static SofttokenActivationBackupManager getCurrent() {
		if(null == manager)
			manager = new SofttokenActivationBackupManager();
		return manager;
	}
	
	/**
	 * Inicializa el administrador de propiedades. 
	 */
	private SofttokenActivationBackupManager() {
		properties = null;
		file = new File(SuiteAppApi.appContext.getFilesDir(), CONFIGURATION_FILE_NAME);
		
		if(file.exists())
			loadPropertiesFile();
	}
	// #endregion
	
	// #region Carga del archivo de propiedades.
	/**
	 * Inicializa el archivo de propiedades con los valores iniciales para cada propiedad.
	 * @param file
	 */
	public void initPropertiesFile() {
		try {
			file.createNewFile();
		} catch(IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al crear el archivo de propiedades.", ioEx);
			return;
		}
		
		loadPropertiesFile();
		
		if(null != properties) {
			setApplicationActivationValue(NUMERO_TARJETA_PROPERTY, "");
			setApplicationActivationValue(NUMERO_TELEFONO_PROPERTY, "");
			setApplicationActivationValue(NUMERO_CLIENTE_PROPERTY, "");
			setApplicationActivationValue(TIPO_SOLICITUD_PROPERTY, "");
			setApplicationActivationValue(NOMBRE_TOKEN_PROPERTY, "");
			setApplicationActivationValue(NUMERO_SERIE_PROPERTY, "");
			setApplicationActivationValue(CORREO_ELECTRONICO_PROPERTY, "");
			setApplicationActivationValue(COMPANIA_CELULAR_PROPERTY, "");
		}
	}
	
	/**
	 * Carga el archivo de propiedades.
	 */
	private void loadPropertiesFile() {
		InputStream input;
		try {
			input = SuiteAppApi.appContext.openFileInput(CONFIGURATION_FILE_NAME);
		} catch(FileNotFoundException fnfEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargar el archivo de prpiedades para lectura.", fnfEx);
			return;
		}

		properties = new Properties();
		try {
			properties.load(input);
		} catch(IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargas las propiedades.", ioEx);
			properties = null;
			return;
		}
	}
	// #endregion
	
	// #region Utilidades el administrador.
	public boolean existsABackup() {
		return file.exists();
	}
	
	public boolean deleteBackup() {
		return (!file.exists()) ? true : file.delete();
	}
	// #endregion
	
	// #region Setters y Getters de las propiedades.
	/**
	 * Obtiene el valor de la propiedad especificada.
	 * @param propertyName El nombre de la propiedad.
	 * @return El valor de la propiedad.
	 */
	public String getApplicationActivationValue(final String propertyName) {
		if(null == properties)
			return null;
		return properties.getProperty(propertyName);
	}

	/**
	 * Establece el valor especificado a la propiedad indicada.
	 * @param propertyName El nombre de la propiedad.
	 * @param value El valor a establecer.
	 */
	public void setApplicationActivationValue(final String propertyName, final String value) {
		if(null == properties)
			return;
		properties.setProperty(propertyName, value);
	}
	
	public void setApplicationActivationValueAndStore(final String propertyName, final String value) {
		if(null == properties)
			return;
		properties.setProperty(propertyName, value);
		storeActivationValues();
	}
	
	public void storeActivationValues() {
		OutputStream output;
		try {	
			output = SuiteAppApi.appContext.openFileOutput(CONFIGURATION_FILE_NAME, Context.MODE_PRIVATE);
			properties.store(output, null);
		} catch(FileNotFoundException fnfEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al abrir el archivo para guardar las propiedades.", fnfEx);
		} catch (IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al guardar en el archivo de propiedades.", ioEx);
		}
	}
	// #endregion

	public static void reloadFile(){
		manager=null;
	}
}
