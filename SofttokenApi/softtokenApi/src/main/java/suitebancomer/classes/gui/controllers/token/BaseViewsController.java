package suitebancomer.classes.gui.controllers.token;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;
import suitebancomer.classes.gui.delegates.token.BaseDelegate;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

public class BaseViewsController extends BaseViewsControllerCommons {

	private static Method methodOverridePendingTransition;

	private boolean isActivityChanging;

	protected Activity rootViewController;
	protected BaseViewControllerCommons currentViewControllerApp;
	protected static BaseViewControllerCommons currentViewController;

	public Activity getActivityContext() {
		return activityContext;
	}

	public void setActivityContext(final Activity activityContext) {
		this.activityContext = activityContext;
		rootViewController = activityContext;
	}

	protected Activity activityContext;

	static {
		final Method[] actMethods = Activity.class.getMethods();
        final int methodSize = actMethods.length;
		for (int i = 0; i < methodSize; i++) {
			if (actMethods[i].getName().equals("overridePendingTransition")) {
				methodOverridePendingTransition = actMethods[i];
				break;
			}
		}
	}

	public Activity getRootViewController() {
		return rootViewController;
	}

	public BaseViewControllerCommons getCurrentViewControllerApp() {
		return currentViewControllerApp;
	}

	public BaseViewControllerCommons getCurrentViewController() {
		return currentViewController;
	}

	public void setCurrentActivityApp(final BaseViewControllerCommons currentViewController) {
		setCurrentActivityApp(currentViewController, false);
	}

	public void setCurrentActivityApp(final BaseViewControllerCommons currentViewController, final boolean asRootViewController) {
		this.currentViewControllerApp = currentViewController;
		if (rootViewController == null) {
			this.rootViewController = currentViewController;
		}
		isActivityChanging = false;

		BaseViewsController.currentViewController = currentViewController;
	}

	public boolean isActivityChanging() {
		return isActivityChanging;
	}

	public void setActivityChanging(final boolean flag) {
		isActivityChanging = flag;
	}

	public void showViewController(final Class<?> viewController) {
		showViewController(viewController, 0, false);
	}

	protected void showViewController(final Class<?> viewController, final int flags) {
		showViewController(viewController, flags, false);
	}

	protected void showViewController(final Class<?> viewController, final int flags, final  boolean inverted) {
		showViewController(viewController, flags, inverted, null, null);
	}


	/**
	 * Muestra la actividad especificada con los parametros indicados
	 *
	 * @param viewController La actividad a mostrar
	 * @param flags          Banderas que se colocaran en el intent de la actividad
	 * @param inverted       Si la pantalla debe mostrarse con animaci�n hacia adelante o hacia atras
	 * @param extras         Arreglo con los extras a pasar al intento, deben acomodarse de manera llave - valor y la llave debe ser
	 *                       de tipo String, si esta regla no se cumple no pasara nig�n par�metro al intento
	 */
	public void showViewController(final Class<?> viewController, final int flags, final boolean inverted, final String[] extrasKeys, final Object[] extras) {
		isActivityChanging = true;
		//currentViewControllerApp
        final Intent intent = new Intent(SuiteAppApi.appContext, viewController);
		if (flags != 0) {
			intent.setFlags(flags);
		}

		if (extrasKeys != null && extras != null && extras.length == extrasKeys.length) {
            final int extrasLength = extras.length;
			for (int i = 0; i < extrasLength; i++) {
				if (extras[i] instanceof String) {
					intent.putExtra(extrasKeys[i], (String) extras[i]);
				} else if (extras[i] instanceof Integer) {
					intent.putExtra(extrasKeys[i], ((Integer) extras[i]).intValue());
				} else if (extras[i] instanceof Boolean) {
					intent.putExtra(extrasKeys[i], ((Boolean) extras[i]).booleanValue());
				} else if (extras[i] instanceof Long) {
					intent.putExtra(extrasKeys[i], ((Long) extras[i]).longValue());
				}
			}
		}

		SuiteAppApi.appContext.startActivity(intent);
		if (inverted) {
			overrideScreenBackTransition();
		} else {
			overrideScreenForwardTransition();
		}
	}

	/**
	 * Muestra la actividad especificada esperando un resultado por parte de la actividad que la invoca
	 *
	 * @param viewController La actividad a mostrar
	 * @param activityCode   codigo que se le asiganra a esta actividad
	 */
	protected void showViewControllerForResult(final Class<?> viewController, final int activityCode) {
		isActivityChanging = true;
        final Intent intent = new Intent(SuiteAppApi.appContext, viewController);
		
		/*intent.setAction("com.google.zxing.client.android.SCAN");
		//intent.putExtra("ACTION", "com.google.zxing.client.android.SCAN");
		
		intent.putExtra("SCAN_WIDTH", (int)(GuiTools.getCurrent().ScreenHeigth * 0.9));
		intent.putExtra("SCAN_HEIGHT", (int)(GuiTools.getCurrent().ScreenWidth * 0.2));
*/

		intent.setAction("com.google.zxing.client.android.SCAN");
		intent.putExtra("ACTION", "com.google.zxing.client.android.SCAN");

		intent.putExtra("SCAN_WIDTH", (int) (GuiTools.getCurrent().ScreenHeigth * 0.9));
		intent.putExtra("SCAN_HEIGHT", (int) (GuiTools.getCurrent().ScreenWidth * 0.5));

		overrideScreenForwardTransition();
		currentViewControllerApp.startActivityForResult(intent, activityCode);

	}

	protected void backToRootViewController() {
        final Intent intent = new Intent(SuiteAppApi.appContext, rootViewController.getClass());
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		SuiteAppApi.appContext.startActivity(intent);
		overrideScreenBackTransition();
	}

	public void cierraViewsController() {
		rootViewController = null;
		if (currentViewControllerApp != null && !(currentViewControllerApp instanceof MenuSuiteViewController)) {
			currentViewControllerApp.finish();
			overrideScreenBackTransition();
			currentViewControllerApp = null;
		}
		if (delegatesHashMap.size() > 0) {
			delegatesHashMap.clear();
		}
	}

	public void addDelegateToHashMap(final long id, final BaseDelegate baseDelegate) {
		delegatesHashMap.put(Long.valueOf(id), baseDelegate);
	}

	public Long getLastDelegateKey() {
		return (Long) delegatesHashMap.keySet().toArray()[delegatesHashMap.size() - 1];
	}

	public void removeDelegateFromHashMap(final long key) {
		delegatesHashMap.remove(Long.valueOf(key));
	}

	public void clearDelegateHashMap() {
		delegatesHashMap.clear();
	}

	public void overrideScreenForwardTransition() {
		if (methodOverridePendingTransition != null) {
			try {
				methodOverridePendingTransition.invoke(currentViewControllerApp != null ? currentViewControllerApp : SuiteAppApi.appContext, new Object[]{R.anim.screen_enter, R.anim.screen_leave});
			} catch (NullPointerException npex) {
				//receiver was null
				if (Server.ALLOW_LOG)
					Log.d("BaseViewsController :: overrideScreenForwardTransition", "Receiver was null");
			} catch (IllegalAccessException iaex) {
				//method not accesible
				if (Server.ALLOW_LOG)
					Log.d("BaseViewsController :: overrideScreenForwardTransition", "Method not accesible");
			} catch (IllegalArgumentException iarex) {
				//incorrect arguments
				if (Server.ALLOW_LOG)
					Log.d("BaseViewsController :: overrideScreenForwardTransition", "Incorrect arguments");
			} catch (InvocationTargetException itex) {
				//invocation returned exception
				if (Server.ALLOW_LOG)
					Log.d("BaseViewsController :: overrideScreenForwardTransition", "Invocation exception: " + itex.toString());
			}
		}
	}

	public void overrideScreenBackTransition() {
		if (methodOverridePendingTransition != null) {
			try {
				methodOverridePendingTransition.invoke(currentViewControllerApp != null ? currentViewControllerApp : SuiteAppApi.appContext, new Object[]{R.anim.screen_enter_back, R.anim.screen_leave_back});
			} catch (NullPointerException npex) {
				//receiver was null
				if (Server.ALLOW_LOG)
					Log.d("BaseViewsController :: overrideScreenBackTransition", "Receiver was null");
			} catch (IllegalAccessException iaex) {
				//method not accesible
				if (Server.ALLOW_LOG)
					Log.d("BaseViewsController :: overrideScreenBackTransition", "Method not accesible");
			} catch (IllegalArgumentException iarex) {
				//incorrect arguments
				if (Server.ALLOW_LOG)
					Log.d("BaseViewsController :: overrideScreenBackTransition", "Incorrect arguments");
			} catch (InvocationTargetException itex) {
				//invocation returned exception
				if (Server.ALLOW_LOG)
					Log.d("BaseViewsController :: overrideScreenBackTransition", "Invocation exception: " + itex.toString());
			}
		}
	}
	@Override
	public void onUserInteraction() {
		if(SuiteAppApi.getIsAplicationLogged()){//tiene session
			if(SuiteAppApi.getCallBackSession()!=null){
				SuiteAppApi.getCallBackSession().userInteraction();
			}
		}
	}

	public boolean consumeAccionesDePausa() {
		if (SuiteAppApi.getIntentToReturn() != null && SuiteAppApi.getInstanceApi().getIsAplicationLogged()==false ) {
			SuiteAppApi.getIntentToReturn().returDesactivada();
		}
		return true;
	}


    public boolean consumeAccionesDeReinicio() {
    	return false;
    }
    
    public boolean consumeAccionesDeAlto() {


    	return false;
    }

    /**
     * Muestra el menu inicial de la aplicación que implemente el método.
     */
    public void showMenuInicial(){}
}
