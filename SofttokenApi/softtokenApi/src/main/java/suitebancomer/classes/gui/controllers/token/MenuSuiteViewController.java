package suitebancomer.classes.gui.controllers.token;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.classes.gui.delegates.token.MenuSuiteDelegate;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.mbanking.softtoken.R;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

public class MenuSuiteViewController extends BaseViewController implements View.OnClickListener, DialogInterface.OnClickListener, AnimationListener {
	
	/**
	 * The layout where the options and the login view are supposed to appear
	 */
	private FrameLayout baseLayout;
	
	/**
	 * The layout containing the suite options
	 */
	//private RelativeLayout menuOptionsLayout;
	private LinearLayout menuOptionsLayout;
	
	/**
	 * Reference to the first button of the option layout
	 */
	private ImageButton firstMenuOption;
	
	/**
	 * Reference to the second button of the option layout
	 */
	private ImageButton secondMenuOption;
	
	/**
	 * Reference to the login view
	 */
	//private LoginViewController loginViewController;
	
	/**
	 * Referencia al botón contáctanos
	 */
	private ImageButton contactButton;
	

	/**
	 * Referencia texto versión
	 */
	private TextView txtVersion;
	
	
	private MenuSuiteDelegate delegate;
	
	private boolean isFlipPerforming;
	
	private boolean shouldHideLogin;
	//AMZ
	//public BmovilViewsController parentManager;
	//AMZ
	
	public boolean comesFromNov = false;
	
	public void setComesFromNov(final Boolean b){
		comesFromNov = b;
	}
	public void setIsFlipPerforming(final Boolean b){
		isFlipPerforming = b;
	}

	
	
	/**
	 * Default constructor for this activity
	 */
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER,SuiteAppApi.getResourceId("layout_suite_menu_principalapi", "layout"));
		//AMZ

		parentViewsController = SuiteAppApi.getInstanceApi().getSuiteViewsControllerApi();
		baseLayout = (FrameLayout)findViewById(SuiteAppApi.getResourceId("suite_menu_options_layout","id"));
		menuOptionsLayout = (LinearLayout)findViewById(SuiteAppApi.getResourceId("suite_menu_options_view","id"));
		firstMenuOption = (ImageButton)findViewById(SuiteAppApi.getResourceId("suite_menu_option_first","id"));
		firstMenuOption.setOnClickListener(this);
		secondMenuOption = (ImageButton)findViewById(SuiteAppApi.getResourceId("suite_menu_option_second","id"));
		secondMenuOption.setOnClickListener(this);
		contactButton = (ImageButton)findViewById(SuiteAppApi.getResourceId("suite_contact_button","id"));
		contactButton.setOnClickListener(this);
		txtVersion = (TextView)findViewById(SuiteAppApi.getResourceId("textVersion","id"));
		txtVersion.setText(SuiteAppApi.appContext
				.getString(R.string.administrar_app_version)+" "
				+ Tools.getDoubleAmountFromServerString(Constants.APPLICATION_VERSION));
		
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
	    super.onPause();
    }

	@Override
	public void onAnimationEnd(final Animation arg0) {
		// Empty method
	}

	@Override
	public void onAnimationRepeat(final Animation arg0) {
		// Emtpy method
	}

	@Override
	public void onAnimationStart(final Animation arg0) {
		// Empty method
	}

	@Override
	public void onClick(final DialogInterface arg0, final int arg1) {
		// Emtpy method
	}
	@Override
	public void onClick(final View v) {
		// Emtpy method
	}
	
	/**
	 * Metodo para detectar si una app esta instalada o no
	 * 
	 * @param uri
	 * @return
	 */
	private boolean appInstalled(final String uri) {
		final PackageManager pm = getPackageManager();
		boolean app_installed = false;
		try {
			pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
			app_installed = true;
		} catch (PackageManager.NameNotFoundException e) {
			app_installed = false;
		}
		return app_installed;
	}
	
}
