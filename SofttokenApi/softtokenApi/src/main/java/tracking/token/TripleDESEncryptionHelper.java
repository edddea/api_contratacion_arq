package tracking.token;


//ARR
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


// import android.util.Base64;
    //string encryption
    public class TripleDESEncryptionHelper {


        // Encrypts string and encode in Base64
        public static byte[] encryptText(final String plainText) throws Exception {
            // ---- Use specified 3DES key and IV from other source --------------
            final byte[] plaintext = plainText.getBytes("utf-8");//input
            final byte[] tdesKeyData = Constants.getKey().getBytes("utf-8");// your encryption key

            final byte[] myIV = Constants.getInitializationVector().getBytes("utf-8");// initialization vector

            final Cipher c3des = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            final SecretKeySpec myKey = new SecretKeySpec(tdesKeyData, "DESede");
            final IvParameterSpec ivspec = new IvParameterSpec(myIV);

            c3des.init(Cipher.ENCRYPT_MODE, myKey, ivspec);
            final byte[] cipherText = c3des.doFinal(plaintext);
           // String encryptedString = Base64.encodeToString(cipherText,
            //        Base64.DEFAULT);
            // return Base64Coder.encodeString(new String(cipherText));
           
            
            return cipherText;
        }
        
        public static String decryptText(final byte[] encryptText) throws Exception {
        	
            // ---- Use specified 3DES key and IV from other source --------------
          //  byte[] encryptText = byteText.getBytes();//input
            final byte[] tdesKeyData = Constants.getKey().getBytes();// your encryption key

            final byte[] myIV = Constants.getInitializationVector().getBytes();// initialization vector

            final Cipher decipherc3des = Cipher.getInstance("DESede/CBC/PKCS5Padding");
            final SecretKeySpec myKey = new SecretKeySpec(tdesKeyData, "DESede");
            final IvParameterSpec ivspec = new IvParameterSpec(myIV);

            decipherc3des.init(Cipher.DECRYPT_MODE, myKey, ivspec);

            final byte[] decipherText = decipherc3des.doFinal(encryptText);
           // String encryptedString = Base64.encodeToString(cipherText,
            //        Base64.DEFAULT);
            // return Base64Coder.encodeString(new String(cipherText));
            
            
            return new String(decipherText, "UTF-8");
        }

   
        private static class Constants {
        	
        	private static final String KEY="000000000000000000000224";
        	private static final String INITIALIZATION_VECTOR="00000061";

        	public static String getKey() 
        	{
        		return KEY;
        	}


        	public static String getInitializationVector() 
        	{
        		return INITIALIZATION_VECTOR;
        	}
        }   
    }
