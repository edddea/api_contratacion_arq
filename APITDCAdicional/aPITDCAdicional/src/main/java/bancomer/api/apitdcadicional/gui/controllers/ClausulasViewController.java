package bancomer.api.apitdcadicional.gui.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.adobe.mobile.Config;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.apitdcadicional.R;
import bancomer.api.apitdcadicional.commons.TrackerAdicional;
import bancomer.api.apitdcadicional.gui.delegates.ClausulasDelegate;
import bancomer.api.apitdcadicional.implementations.InitTDCAdicional;
import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;

/**
 * Created by OOROZCO on 8/25/15.
 */
public class ClausulasViewController extends Activity implements View.OnClickListener {

    public BaseViewsController parentManager;
    private BaseViewController baseViewController;
    private InitTDCAdicional init = InitTDCAdicional.getInstance();
    private ClausulasDelegate delegate;

    //Adobe
    String screen = "";
    ArrayList<String> list= new ArrayList<String>();
    Map<String,Object> operacionMap = new HashMap<String, Object>();

    /* Views Elements*/


    private TextView labelObtenerTarjeta, labelLineaCredito,labelMonto,labelModificarMonto,labelDatosRequeridos,labelNombres,labelApellidoPaterno,labelApellidoMaterno,
            labelRfc,labelFecha,labelAvisoPrivacidad,labelEnviaremosTarjeta,labelDomicilio,txtaviso_token,labelAvisoDomicilio;

    private EditText textNombre,textApellidoPaterno,textApellidoMaterno,textRfc,textFecha;
    private ImageButton imgaviso_token,btnConfirmar,calendarIcon;

    private int day,month,year=0;
    private boolean isPickerShown=true;


    /** Pop Up**/
    private TextView labelMsjVal;
    private EditText textMonto;
    private Button buttonCancel, buttonAccept;
    private Dialog popupWindow;
    /**
     * TOKEN
     */

    private LinearLayout contenedorContrasena, contenedorNIP,contenedorASM,contenedorCVV,contenedorCampoTarjeta;
    private TextView campoContrasena,campoNIP,campoASM,campoCVV,campoTarjeta;
    private EditText contrasena,nip,asm,cvv,tarjeta;
    private TextView instruccionesContrasena,instruccionesNIP,instruccionesASM,instruccionesCVV,instruccionesTarjeta;

    /** End Attributes Region**/




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate=(ClausulasDelegate) parentManager.getBaseDelegateForKey(ClausulasDelegate.CLAUSULAS_TDC_ADICIONAL_DELEGATE);
        delegate.setController(this);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_tdc_adicional_clausulas);
        baseViewController.setTitle(this, R.string.bmovil_promocion_title_tdc_adicional, R.drawable.an_ic_tdc_adicional);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        initView();

        //Adobe
        Config.setContext(this.getApplicationContext());

        screen = "clausulas tdc Adicional";
        list.add(screen);
        TrackerAdicional.trackState(list);

        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackerAdicional.trackEntrada(eventoEntrada);
    }



    private void initView()
    {
        findViews();
        setData();
        setSecurityComponents();
        scaleViews();
    }


    @Override
    public void onClick(View v) {

        if(v == btnConfirmar){
            if(delegate.areInputOk())
                if(delegate.enviaPeticionOperacion())
                    delegate.performAction(null);
        }else if(v == buttonCancel)
        {
            popupWindow.dismiss();
            textNombre.setEnabled(true);
        }
        else if(v == labelModificarMonto)
        {
            popUpAction();
            popupWindow.show();
            buttonAccept.setEnabled(false);
        }
        else if(v == buttonAccept)
        {
            if(buttonAccept.isEnabled())
            {
                labelMonto.setText(Tools.formatAmount(textMonto.getText().toString()+"00",false));
                popupWindow.dismiss();
                textNombre.setEnabled(true);
            }
        }
        else if(v == calendarIcon)
        {
            showPicker();

        }

    }




    private void findViews() {
        labelObtenerTarjeta = (TextView) findViewById(R.id.labelObtenerTarjeta);
        labelLineaCredito = (TextView) findViewById(R.id.labelLineaCredito);
        labelMonto = (TextView) findViewById(R.id.labelMonto);
        labelModificarMonto = (TextView) findViewById(R.id.labelModificarMonto);
        labelModificarMonto.setOnClickListener(this);
        labelDatosRequeridos = (TextView) findViewById(R.id.labelDatosRequeridos);
        labelNombres = (TextView) findViewById(R.id.labelNombres);
        labelApellidoPaterno = (TextView) findViewById(R.id.labelApellidoPaterno);
        labelApellidoMaterno = (TextView) findViewById(R.id.labelApellidoMaterno);
        labelRfc = (TextView) findViewById(R.id.labelRfc);
        labelAvisoPrivacidad = (TextView) findViewById(R.id.labelAvisoPrivacidad);
        labelEnviaremosTarjeta = (TextView) findViewById(R.id.labelEnviaremosTarjeta);
        labelDomicilio = (TextView) findViewById(R.id.labelDomicilio);
        labelFecha = (TextView) findViewById(R.id.labelFecha);

        txtaviso_token = (TextView) findViewById(R.id.txtaviso_token);


        textNombre = (EditText) findViewById(R.id.textNombre);
        textApellidoPaterno = (EditText) findViewById(R.id.textApellidoPaterno);
        textApellidoMaterno = (EditText) findViewById(R.id.textApellidoMaterno);
        textRfc = (EditText) findViewById(R.id.textRfc);
        textFecha = (EditText) findViewById(R.id.textFecha);
        textFecha.setInputType(InputType.TYPE_NULL);

        textFecha.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus && isPickerShown){
                    InputMethodManager imm = (InputMethodManager) getSystemService(ClausulasViewController.this.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(textFecha.getWindowToken(), 0);
                    showPicker();
                }
            }
        });

        textRfc.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (textRfc.getText().length() > 0)
                        if (validateRFC()) {
                            isPickerShown = false;
                            textRfc.setText("");
                            DialogInterface.OnClickListener clickListener = new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    InputMethodManager imm = (InputMethodManager) getSystemService(ClausulasViewController.this.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(textRfc.getWindowToken(), 0);
                                }
                            };
                            init.getBaseViewController().showInformationAlert(ClausulasViewController.this, getResources().getString(R.string.bmovil_clausulas_invalidRFC), clickListener);
                        } else
                            isPickerShown = true;
                }
            }
        });

        btnConfirmar = (ImageButton) findViewById(R.id.btnConfirmar);
        btnConfirmar.setOnClickListener(this);
        imgaviso_token = (ImageButton) findViewById(R.id.imgaviso_token);


        /** Token **/
        contenedorContrasena	= (LinearLayout)findViewById(R.id.campo_confirmacion_contrasena_layout);
        contenedorNIP 			= (LinearLayout)findViewById(R.id.campo_confirmacion_nip_layout);
        contenedorASM 			= (LinearLayout)findViewById(R.id.campo_confirmacion_asm_layout);
        contenedorCVV 			= (LinearLayout)findViewById(R.id.campo_confirmacion_cvv_layout);

        contrasena 				= (EditText)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
        nip 					= (EditText)contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
        asm						= (EditText)contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
        cvv						= (EditText)contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);

        campoContrasena			= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_label);
        campoNIP				= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_label);
        campoASM				= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_label);
        campoCVV				= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_label);

        instruccionesContrasena	= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
        instruccionesNIP		= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
        instruccionesASM		= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
        instruccionesCVV		= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);

        contenedorCampoTarjeta  = (LinearLayout)findViewById(R.id.campo_confirmacion_campotarjeta_layout);
        tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
        campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
        instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);


        labelAvisoDomicilio = (TextView) findViewById(R.id.labelAvisoDomicilio);
        calendarIcon = (ImageButton) findViewById(R.id.calendarIcon);
        calendarIcon.setOnClickListener(this);

    }


    private void scaleViews()
    {
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(labelObtenerTarjeta,true);
        gTools.scale(labelLineaCredito,true);
        gTools.scale(labelMonto,true);
        gTools.scale(labelModificarMonto,true);
        gTools.scale(labelDatosRequeridos,true);
        gTools.scale(labelNombres,true);
        gTools.scale(labelApellidoPaterno,true);
        gTools.scale(labelApellidoMaterno,true);
        gTools.scale(labelRfc,true);
        gTools.scale(labelAvisoPrivacidad,true);
        gTools.scale(labelEnviaremosTarjeta,true);
        gTools.scale(labelDomicilio,true);
        gTools.scale(labelFecha,true);

        gTools.scale(txtaviso_token,true);

        gTools.scale(textNombre);
        gTools.scale(textApellidoPaterno);
        gTools.scale(textApellidoMaterno);
        gTools.scale(textRfc);
        gTools.scale(textFecha);

        gTools.scale(imgaviso_token);
        gTools.scale(btnConfirmar);
        gTools.scale(contenedorContrasena);
        gTools.scale(contenedorNIP);
        gTools.scale(contenedorASM);
        gTools.scale(contenedorCVV);

        gTools.scale(contrasena, true);
        gTools.scale(nip, true);
        gTools.scale(asm, true);
        gTools.scale(cvv, true);

        gTools.scale(campoContrasena, true);
        gTools.scale(campoNIP, true);
        gTools.scale(campoASM, true);
        gTools.scale(campoCVV, true);

        gTools.scale(instruccionesContrasena, true);
        gTools.scale(instruccionesNIP, true);
        gTools.scale(instruccionesASM, true);
        gTools.scale(instruccionesCVV, true);

        gTools.scale(contenedorCampoTarjeta);
        gTools.scale(tarjeta, true);
        gTools.scale(campoTarjeta, true);
        gTools.scale(instruccionesTarjeta, true);

        gTools.scale(labelAvisoDomicilio,true);
        gTools.scale(calendarIcon);


    }

    private void setData()
    {
        String card= delegate.getOfertaTDCAdicional().getDesProducto();
        String getCard = getResources().getString(R.string.bmovil_clausulas_tdc_adicional_labelObetenerTarjeta1)+
                " " + card + " "
                + getResources().getString(R.string.bmovil_clausulas_tdc_adicional_labelObetenerTarjeta2);

        labelObtenerTarjeta.setText(getCard);

        SpannableString amount = new SpannableString(Tools.formatAmount(delegate.getOfertaTDCAdicional().getLineaCredito(), false));
        amount.setSpan(new UnderlineSpan(), 0, amount.length(), 0);
        labelMonto.setText(amount);

        SpannableString modifyAmount = new SpannableString(labelModificarMonto.getText());
        //modifyAmount.setSpan(new UnderlineSpan(), 0, modifyAmount.length(), 0);
        labelModificarMonto.setText(modifyAmount);

        labelDomicilio.setText(delegate.getOfertaTDCAdicional().getDireccionTitular());

    }




    private void popUpAction()
    {
        popupWindow = new Dialog(this);


        popupWindow.requestWindowFeature(Window.FEATURE_NO_TITLE);

        popupWindow.getWindow().setBackgroundDrawable(new ColorDrawable(00000000));
        popupWindow.setContentView(R.layout.popup_modificar_monto);
        popupWindow.show();


        buttonCancel = (Button) popupWindow.findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(this);

        buttonAccept = (Button) popupWindow.findViewById(R.id.buttonAccept);
        buttonAccept.setOnClickListener(this);

        labelMsjVal = (TextView) popupWindow.findViewById(R.id.labelMsjVal);
        textMonto = (EditText) popupWindow.findViewById(R.id.textMonto);

        textNombre.setEnabled(false);

        textMonto.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                if (textMonto.getText().length() > 0) {
                    if (delegate.isNewAmountOk(textMonto.getText().toString())) {
                        labelMsjVal.setVisibility(View.GONE);
                        buttonAccept.setEnabled(true);
                    } else {
                        buttonAccept.setEnabled(false);

                    }
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
    }


    public void setFirstMessagePopUp()
    {
        labelMsjVal.setText(getResources().getString(R.string.bmovil_popup_labelMsjVal1));
        labelMsjVal.setVisibility(View.VISIBLE);
    }


    public void setSecondMessagePopUp()
    {
        labelMsjVal.setText(getResources().getString(R.string.bmovil_popup_labelMsjVal2));
        labelMsjVal.setVisibility(View.VISIBLE);
    }

    public void setThirdMessagePopUp()
    {
        labelMsjVal.setText(getResources().getString(R.string.bmovil_popup_labelMsjVal3));
        labelMsjVal.setVisibility(View.VISIBLE);
    }
    private void setDate()
    {
        if(day < 10 && month <10)
            textFecha.setText(new StringBuilder().append("0" + day).append("/").append("0" + month).append("/").append(year));
        else if(month < 10)
            textFecha.setText(new StringBuilder().append(day).append("/").append("0" + month).append("/").append(year));
        else if(day < 10)
            textFecha.setText(new StringBuilder().append("0" + day).append("/").append(month).append("/").append(year));
        else
            textFecha.setText(new StringBuilder().append(day).append("/").append(month).append("/").append(year));



        if(asm.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }


    }

    public String getDate()
    {
        String date [] = textFecha.getText().toString().split("/");
        return (date[2]+date[1]+date[0]).substring(2,8);
    }

    private void showPicker()
    {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        final int menos18=18;


        /**
         * There's a problem when using "lollipop" with DatePickerDialog, so it's necessary to check which SDK level the device has.
         */

        if(sdk >= 21)
        {
            final View dialogView = View.inflate(this, R.layout.api_tdc_adicional_date_picker, null);
            final Calendar calendar = Calendar.getInstance();

            final DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.datePicker);


            if(year==0)
                datePicker.init(calendar.get(Calendar.YEAR)-menos18, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), null);
            else
                datePicker.init(year, month - 1, day, null);


            datePicker.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.bmovil_popup_button_accept), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int arg1) {
                    year = datePicker.getYear();
                    month = datePicker.getMonth() + 1;
                    day = datePicker.getDayOfMonth();
                    setDate();
                    dialog.dismiss();
                }
            });

            Log.e("year", year + "");

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.setView(dialogView);
            alertDialog.show();


        }
        else
        {
            final DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0, int y, int m, int d) {
                    year=y;
                    month=m+1;
                    day=d;
                    setDate();
                }
            };


            if(year==0)
            {
                final Calendar calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                day = calendar.get(Calendar.DAY_OF_MONTH);
                new DatePickerDialog(ClausulasViewController.this, pickerListener, year,month,day).show();

            }
            else
                new DatePickerDialog(ClausulasViewController.this, pickerListener, year,month-1,day).show();

        }


    }

    private boolean validateRFC()
    {
        try
        {
            final String first=textRfc.getText().toString().substring(0,4);
            final String second=textRfc.getText().toString().substring(4,10);

            if(first.matches("[^0-9]"))
            {
                return true;
            }

            Integer.parseInt(second);

        }
        catch(Exception e)
        {
            Log.w("Problems with RFC caused by: ", e.getLocalizedMessage());
            return true;
        }

        return false;
    }






    public EditText getName()
    {
        return textNombre;
    }
    public EditText getFirstLastName()
    {
        return textApellidoPaterno;
    }
    public EditText getSecondLastName()
    {
        return textApellidoMaterno;
    }
    public EditText getBirthDate()
    {
        return textFecha;
    }
    public EditText getRFC()
    {
        return textRfc;
    }



    @Override
    protected void onPause() {
        super.onPause();
        Config.collectLifecycleData();
        if(!init.getParentManager().isActivityChanging())
        {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if (DialogInterface.BUTTON_POSITIVE == which) {
                        init.getParentManager().setActivityChanging(true);
                        //init.getParentManager().showMenuInicial();
                        ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                        init.getParentManager().resetRootViewController();
                        init.resetInstance();
                    }else{
                        dialog.dismiss();
                    }

                }
            };

            init.getBaseViewController().showYesNoAlert(this, R.string.bmovil_detalle_promocion_tdc_adicional_rechazo, listener);

        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Config.pauseCollectingLifecycleData();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }


    /**   Inherited Methods From Base Code**/


    private void setSecurityComponents()
    {
        mostrarContrasena(delegate.mostrarContrasenia());
        mostrarNIP(delegate.mostrarNIP());
        mostrarASM(delegate.tokenAMostrar());
        mostrarCVV(delegate.mostrarCVV());

        LinearLayout parentContainer = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);

        if (contenedorContrasena.getVisibility() == View.GONE &&
                contenedorNIP.getVisibility() == View.GONE &&
                contenedorASM.getVisibility() == View.GONE &&
                contenedorCVV.getVisibility() == View.GONE &&
                contenedorCampoTarjeta.getVisibility() == View.GONE) {
            parentContainer.setBackgroundColor(0);
            parentContainer.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        }

    }



    public void mostrarContrasena(boolean visibility){
        contenedorContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        campoContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        contrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
            campoContrasena.setText(delegate.getEtiquetaCampoContrasenia());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
            contrasena.setFilters(userFilterArray);
            contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
        instruccionesContrasena.setVisibility(View.GONE);
    }


    public void mostrarNIP(boolean visibility){
        contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
        campoNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
        nip.setVisibility(visibility ? View.VISIBLE:View.GONE);
        if (visibility) {
            campoNIP.setText(delegate.getEtiquetaCampoNip());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
            nip.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(tarjeta);
            nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = delegate.getTextoAyudaNIP();
            if (instrucciones.equals("")) {
                instruccionesNIP.setVisibility(View.GONE);
            } else {
                instruccionesNIP.setVisibility(View.VISIBLE);
                instruccionesNIP.setText(instrucciones);
            }
        } else {
            nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }


    public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){
        switch(tipoOTP) {
            case ninguno:
                contenedorASM.setVisibility(View.GONE);
                campoASM.setVisibility(View.GONE);
                asm.setVisibility(View.GONE);
                asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
                break;
            case codigo:
            case registro:
                contenedorASM.setVisibility(View.VISIBLE);
                campoASM.setVisibility(View.VISIBLE);
                asm.setVisibility(View.VISIBLE);
                InputFilter[] userFilterArray = new InputFilter[1];
                userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
                asm.setFilters(userFilterArray);
                cambiarAccionTexto(contrasena);
                cambiarAccionTexto(tarjeta);
                cambiarAccionTexto(nip);
                asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
                break;
        }

        Constants.TipoInstrumento tipoInstrumento = delegate.getTipoInstrumentoSeguridad();

        switch (tipoInstrumento) {
            case OCRA:
                campoASM.setText(delegate.getEtiquetaCampoOCRA());
                asm.setTransformationMethod(null);
                break;
            case DP270:
                campoASM.setText(delegate.getEtiquetaCampoDP270());
                asm.setTransformationMethod(null);
                break;
            case SoftToken:
                if(init.getSofTokenStatus()) {
                    asm.setText(Constants.DUMMY_OTP);
                    asm.setEnabled(false);
                    campoASM.setText(delegate.getEtiquetaCampoSoftokenActivado());
                } else {
                    asm.setText("");
                    asm.setEnabled(true);
                    campoASM.setText(delegate.getEtiquetaCampoSoftokenDesactivado());
                    asm.setTransformationMethod(null);
                }

                break;
            default:
                break;
        }
        String instrucciones = delegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
        if (instrucciones.equals("")) {
            instruccionesASM.setVisibility(View.GONE);
        } else {
            instruccionesASM.setVisibility(View.VISIBLE);
            instruccionesASM.setText(instrucciones);
        }
    }



    public void mostrarCVV(boolean visibility){
        contenedorCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
        campoCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
        cvv.setVisibility(visibility ? View.VISIBLE:View.GONE);
        instruccionesCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);

        if (visibility) {
            campoCVV.setText(delegate.getEtiquetaCampoCVV());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
            cvv.setFilters(userFilterArray);
            String instrucciones = delegate.getTextoAyudaCVV();
            instruccionesCVV.setText(instrucciones);
            instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(nip);
            cambiarAccionTexto(asm);
            cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    private void cambiarAccionTexto(EditText campo) {
        if (campo.getVisibility() == View.VISIBLE) {
            campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        }
    }



    public String pideContrasena() {
        if (contrasena.getVisibility() == View.GONE) {
            return "";
        } else {
            return contrasena.getText().toString();
        }
    }

    public String pideNIP() {
        if (nip.getVisibility() == View.GONE) {
            return "";
        } else {
            return nip.getText().toString();
        }
    }

    public String pideASM() {
        if (asm.getVisibility() == View.GONE) {
            return "";
        } else {
            return asm.getText().toString();
        }
    }

    public String pideCVV() {
        if (cvv.getVisibility() == View.GONE) {
            return "";
        } else {
            return cvv.getText().toString();
        }
    }
}
