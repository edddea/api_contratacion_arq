package bancomer.api.apitdcadicional.implementations;

import android.app.Activity;

import org.apache.http.impl.client.DefaultHttpClient;

import bancomer.api.apitdcadicional.gui.controllers.ClausulasViewController;
import bancomer.api.apitdcadicional.gui.controllers.DetalleViewController;
import bancomer.api.apitdcadicional.gui.controllers.ExitoViewController;
import bancomer.api.apitdcadicional.gui.delegates.ClausulasDelegate;
import bancomer.api.apitdcadicional.gui.delegates.ConsultaDelegate;
import bancomer.api.apitdcadicional.gui.delegates.DetalleDelegate;
import bancomer.api.apitdcadicional.gui.delegates.ExitoDelegate;
import bancomer.api.apitdcadicional.io.BaseSubapplication;
import bancomer.api.apitdcadicional.io.Server;
import bancomer.api.apitdcadicional.models.AceptaTDCAdicional;
import bancomer.api.apitdcadicional.models.OfertaTDCAdicional;
import bancomer.api.common.commons.IAutenticacion;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.BaseViewsControllerImpl;
import bancomer.api.common.gui.delegates.DelegateBaseOperacion;
import bancomer.api.common.gui.delegates.MainViewHostDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by OOROZCO on 8/20/15.
 */
public class InitTDCAdicional {


    /*Components for APITDCADICIONAL working*/

    private static InitTDCAdicional mInstance = null;

    private BaseViewController baseViewController;

    private static BaseViewsController parentManager;

    private BaseSubapplication baseSubApp;

    private static DefaultHttpClient client;

    private Promociones ofertaAdicional;

    private DelegateBaseOperacion delegateOTP;

    private ICommonSession session;

    private IAutenticacion autenticacion;

    private boolean sofTokenStatus;

    private MainViewHostDelegate hostInstance;

    /* End Region */

    /////////////  Getter's & Setter's /////////////////


    public static DefaultHttpClient getClient() {
        return client;
    }

    public ICommonSession getSession()
    {
        return session;
    }

    public DelegateBaseOperacion getDelegateOTP() {
        return delegateOTP;
    }

    public Promociones getOfertaAdicional()
    {
        return ofertaAdicional;
    }

    public BaseViewController getBaseViewController() {
        return baseViewController;
    }

    public void setBaseViewController(BaseViewController baseViewController) {
        this.baseViewController = baseViewController;
    }

    public BaseViewsController getParentManager() {
        return parentManager;
    }

    public BaseSubapplication getBaseSubApp() {
        return baseSubApp;
    }

    public IAutenticacion getAutenticacion() {  return autenticacion;  }

    public boolean getSofTokenStatus(){ return sofTokenStatus; }

    ///////////// End Region Getter's & Setter's /////////////////


    public void resetInstance()
    {   mInstance = null; }

    public void updateHostActivityChangingState()
    {
        hostInstance.updateActivityChangingState();
    }

    public static InitTDCAdicional getInstance(Activity activity, Promociones promocion,DelegateBaseOperacion delegateOTP, ICommonSession session,IAutenticacion autenticacion,boolean softTokenStatus,DefaultHttpClient client, MainViewHostDelegate hostInstance,boolean dev, boolean sim, boolean emulator, boolean log)
    {
        if(mInstance == null)
        {
            //Log.d("[CGI-Configuracion-Obligatorio] >> ", "[InitTDCAdicional] Se inicializa la instancia del singleton de datos");
            mInstance = new InitTDCAdicional(activity, promocion, delegateOTP,session, autenticacion, softTokenStatus, client, hostInstance, dev,  sim,  emulator, log);
        }

        return mInstance;
    }

    public static InitTDCAdicional getInstance()
    {
        return mInstance;
    }

    private InitTDCAdicional(Activity activity,Promociones promocion, DelegateBaseOperacion delegateOTP,ICommonSession session,IAutenticacion autenticacion,boolean sofTokenStatus, DefaultHttpClient client, MainViewHostDelegate hostInstance,boolean dev, boolean sim, boolean emulator, boolean log)
    {
        setServerParams(dev, sim, emulator, log);
        //Server.setEnviroment();
        this.hostInstance = hostInstance;
        ofertaAdicional = promocion;
        this.delegateOTP=delegateOTP;
        baseViewController = new BaseViewControllerImpl();
        this.client = client;
        parentManager = new BaseViewsControllerImpl();
        parentManager.setCurrentActivityApp(activity);
        baseSubApp = new BaseSubapplicationImpl(activity, client);
        //Log.e("Value Server",String.valueOf(Server.DEVELOPMENT));
        this.session=session;
        this.autenticacion=autenticacion;
        this.sofTokenStatus = sofTokenStatus;
    }

    private void setServerParams(boolean dev, boolean sim, boolean emulator, boolean log)
    {
        Server.DEVELOPMENT = dev;
        Server.SIMULATION = sim;
        Server.EMULATOR = emulator;
        Server.ALLOW_LOG = log;

    }


    public void showClausulas(OfertaTDCAdicional oferta, Promociones promocion){


        ClausulasDelegate delegate = (ClausulasDelegate) parentManager.getBaseDelegateForKey(ClausulasDelegate.CLAUSULAS_TDC_ADICIONAL_DELEGATE);
        if(delegate == null)
        {
            delegate = new ClausulasDelegate(oferta,promocion);
            parentManager.addDelegateToHashMap(ClausulasDelegate.CLAUSULAS_TDC_ADICIONAL_DELEGATE,delegate);
        }

        //parentManager.showViewController(ClausulasViewController.class);
        parentManager.showViewController(ClausulasViewController.class,0,false,null,null,parentManager.getCurrentViewController());

    }


    public void showExito(AceptaTDCAdicional acepta)
    {
        ExitoDelegate delegate = (ExitoDelegate) parentManager.getBaseDelegateForKey(ExitoDelegate.EXITO_DELEGATE_ADICIONAL);
        if(delegate == null)
        {
            delegate = new ExitoDelegate(acepta);
            parentManager.addDelegateToHashMap(ExitoDelegate.EXITO_DELEGATE_ADICIONAL,delegate);
        }
        //parentManager.showViewController(ExitoViewController.class);
        parentManager.showViewController(ExitoViewController.class,0,false,null,null,parentManager.getCurrentViewController());
    }

    public void showDetalle(OfertaTDCAdicional oferta){

        DetalleDelegate delegate = (DetalleDelegate) parentManager.getBaseDelegateForKey(DetalleDelegate.DETALLE_TDC_ADICIONAL_DELEGATE);
        if(delegate == null)
        {
            delegate = new DetalleDelegate(oferta);
            parentManager.addDelegateToHashMap(DetalleDelegate.DETALLE_TDC_ADICIONAL_DELEGATE,delegate);
        }

        //parentManager.showViewController(DetalleViewController.class);
        parentManager.showViewController(DetalleViewController.class,0,false,null,null,parentManager.getCurrentViewController());
    }

    public void startAdicional()
    { new ConsultaDelegate().performAction(null);}




}
