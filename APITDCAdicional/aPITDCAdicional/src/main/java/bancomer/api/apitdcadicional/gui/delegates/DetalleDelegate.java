package bancomer.api.apitdcadicional.gui.delegates;

import java.util.Hashtable;

import bancomer.api.apitdcadicional.gui.controllers.DetalleViewController;
import bancomer.api.apitdcadicional.implementations.InitTDCAdicional;
import bancomer.api.apitdcadicional.io.Server;
import bancomer.api.apitdcadicional.models.OfertaTDCAdicional;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;

/**
 * Created by OOROZCO on 8/20/15.
 */
public class DetalleDelegate implements BaseDelegate {

    public static final long DETALLE_TDC_ADICIONAL_DELEGATE = 10909334566765662L;

    private DetalleViewController controller;
    private OfertaTDCAdicional detalle;

    public DetalleDelegate(OfertaTDCAdicional oferta)
    {
        this.detalle = oferta;
    }


    public DetalleViewController getController() {
        return controller;
    }


    public OfertaTDCAdicional getDetalle() {
        return detalle;
    }

    public void setDetalle(OfertaTDCAdicional oferta) {
        this.detalle = oferta;
    }

    public void setController(DetalleViewController controller) {
        this.controller = controller;
    }

    public void doNetworkOperation(int operationId,Hashtable<String, ?> params, BaseViewController caller)
    {
    }

    public void analyzeResponse(int operationId, ServerResponse response) {

    }

    @Override
    public void performAction(Object obj) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }

}
