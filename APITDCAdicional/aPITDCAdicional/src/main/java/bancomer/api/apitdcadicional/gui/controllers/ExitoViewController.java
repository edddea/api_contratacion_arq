package bancomer.api.apitdcadicional.gui.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.text.SpannableString;

import com.adobe.mobile.Config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.apitdcadicional.R;
import bancomer.api.apitdcadicional.commons.TrackerAdicional;
import bancomer.api.apitdcadicional.gui.delegates.ExitoDelegate;
import bancomer.api.apitdcadicional.implementations.InitTDCAdicional;
import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;

/**
 * Created by OOROZCO on 8/26/15.
 */
public class ExitoViewController extends Activity implements View.OnClickListener{

    public BaseViewsController parentManager;
    private BaseViewController baseViewController;
    private InitTDCAdicional init = InitTDCAdicional.getInstance();
    private ExitoDelegate delegate;


    //// View Elements ///////////

    private TextView labelFelicidades, labelMonto, labelRecordamos, labelTextoAviso, labelSMS, labelSMSInfo, labelMail, labelMailInfo;
    private ImageView imgSeparador;
    private EditText textSMS, textMail;
    private ImageButton btnMenu;

    //Adobe
    String screen = "";
    ArrayList<String> list= new ArrayList<String>();
    Map<String,Object> operacionMap = new HashMap<String, Object>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate=(ExitoDelegate) parentManager.getBaseDelegateForKey(ExitoDelegate.EXITO_DELEGATE_ADICIONAL);
        delegate.setController(this);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_tdc_adicional_exito);
        baseViewController.setTitle(this, R.string.bmovil_promocion_title_tdc_adicional, R.drawable.an_ic_tdc_adicional);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        findViews();
        setData();
        scaleViews();
        delegate.configureScreen();

        //Adobe
        Config.setContext(this.getApplicationContext());

        screen = "exito tdc Adicional";
        list.add(screen);
        TrackerAdicional.trackState(list);

        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackerAdicional.trackEntrada(eventoEntrada);

    }


    @Override
    public void onClick(View v) {
        if(v == btnMenu)
        {
            init.getParentManager().setActivityChanging(true);
            //init.getParentManager().showMenuInicial();
            ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
            init.resetInstance();
        }
    }



    private void findViews() {
        labelFelicidades = (TextView) findViewById(R.id.labelFelicidades);
        labelMonto = (TextView) findViewById(R.id.labelMonto);
        labelRecordamos = (TextView) findViewById(R.id.labelRecordamos);

        labelTextoAviso = (TextView) findViewById(R.id.labelTextoAviso);
        labelSMS = (TextView) findViewById(R.id.labelSMS);
        labelSMSInfo = (TextView) findViewById(R.id.labelSMSInfo);
        labelMail = (TextView) findViewById(R.id.labelMail);
        labelMailInfo = (TextView) findViewById(R.id.labelMailInfo);


        imgSeparador = (ImageView) findViewById(R.id.imgSeparador);

        textSMS = (EditText) findViewById(R.id.textSMS);
        textMail = (EditText) findViewById(R.id.textMail);

        btnMenu = (ImageButton) findViewById(R.id.btnMenu);
        btnMenu.setOnClickListener(this);

    }

    private void setData() {

        String mail = init.getSession().getEmail();
        String number = init.getSession().getUsername();

        SpannableString amount = new SpannableString(Tools.formatAmount(delegate.getOferta().getLineaCredito(),false));
        amount.setSpan(new UnderlineSpan(), 0, amount.length(), 0);
        labelMonto.setText(amount);
        //MAPE comentolabelMonto.setText(Tools.formatAmount(delegate.getOferta().getLineaCredito(),false));
        textSMS.setText(Tools.hideUsername(number));
        textMail.setText(mail);
    }

    private void scaleViews()
    {
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(labelFelicidades,true);
        gTools.scale(labelMonto,true);
        gTools.scale(labelRecordamos,true);
        gTools.scale(labelTextoAviso,true);
        gTools.scale(labelSMS,true);
        gTools.scale(labelSMSInfo,true);
        gTools.scale(labelMail,true);
        gTools.scale(labelMailInfo,true);

        gTools.scale(imgSeparador);

        gTools.scale(textSMS);
        gTools.scale(textMail);

        gTools.scale(btnMenu);

    }

    public void showNumberData()
    {
        labelSMS.setVisibility(View.VISIBLE);
        labelSMSInfo.setVisibility(View.VISIBLE);
        textSMS.setVisibility(View.VISIBLE);
        textSMS.setEnabled(false);
    }

    public void showMailData()
    {
        labelMail.setVisibility(View.VISIBLE);
        labelMailInfo.setVisibility(View.VISIBLE);
        textMail.setVisibility(View.VISIBLE);
        textMail.setEnabled(false);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
        }
        return true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Config.collectLifecycleData();
        if(!init.getParentManager().isActivityChanging())
        {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        Config.pauseCollectingLifecycleData();
    }
}
