package bancomer.api.apitdcadicional.gui.delegates;

import java.util.Hashtable;

import bancomer.api.apitdcadicional.gui.controllers.ExitoViewController;
import bancomer.api.apitdcadicional.models.AceptaTDCAdicional;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;

/**
 * Created by OOROZCO on 8/26/15.
 */
public class ExitoDelegate implements BaseDelegate{

    public static final long EXITO_DELEGATE_ADICIONAL = 130701334566765662L;

    private ExitoViewController controller;

    private AceptaTDCAdicional oferta;


    public ExitoDelegate(AceptaTDCAdicional oferta)
    {
        this.oferta = oferta;
    }

    public void setController(ExitoViewController controller) {
        this.controller = controller;
    }

    public AceptaTDCAdicional getOferta() {
        return oferta;
    }


    public void configureScreen()
    {
        if(oferta.getEnvioSMS().equalsIgnoreCase("SI"))
            controller.showNumberData();
        if(oferta.getEnvioCorreoElectronico().equalsIgnoreCase("SI"))
            controller.showMailData();
    }




    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {

    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

    }

    @Override
     public void performAction(Object obj) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }
}
