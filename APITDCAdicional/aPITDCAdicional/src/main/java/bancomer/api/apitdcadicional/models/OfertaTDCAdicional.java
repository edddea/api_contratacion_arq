package bancomer.api.apitdcadicional.models;

import java.io.IOException;

import bancomer.api.apitdcadicional.commons.ConstantsTDCAdicional;
import bancomer.api.common.io.ParsingHandler;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;

/**
 * Created by OOROZCO on 8/26/15.
 */
public class OfertaTDCAdicional implements ParsingHandler {


    private String lineaCredito;
    private String producto;
    private String desProducto;
    private String contrato;
    private String direccionTitular;
    private String folio;
    private String fechaOperacion;


    public String getLineaCredito() {
        return lineaCredito;
    }

    public void setLineaCredito(String lineaCredito) {
        this.lineaCredito = lineaCredito;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getDesProducto() {
        return desProducto;
    }

    public void setDesProducto(String desProducto) {
        this.desProducto = desProducto;
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public String getDireccionTitular() {
        return direccionTitular;
    }

    public void setDireccionTitular(String direccionTitular) {
        this.direccionTitular = direccionTitular;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(String fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }



    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        lineaCredito = (parser.hasValue(ConstantsTDCAdicional.LINEA_CREDITO_TAG)) ? parser.parseNextValue(ConstantsTDCAdicional.LINEA_CREDITO_TAG):"";
        producto = (parser.hasValue(ConstantsTDCAdicional.PRODUCTO_TAG)) ? parser.parseNextValue(ConstantsTDCAdicional.PRODUCTO_TAG):"";
        desProducto = (parser.hasValue(ConstantsTDCAdicional.DES_PRODUCTO_TAG)) ? parser.parseNextValue(ConstantsTDCAdicional.DES_PRODUCTO_TAG):"";
        contrato = (parser.hasValue(ConstantsTDCAdicional.CONTRATO_TAG)) ? parser.parseNextValue(ConstantsTDCAdicional.CONTRATO_TAG):"";
        direccionTitular = (parser.hasValue(ConstantsTDCAdicional.DIRECCION_TITULAR_TAG)) ? parser.parseNextValue(ConstantsTDCAdicional.DIRECCION_TITULAR_TAG):"";
        folio = (parser.hasValue(ConstantsTDCAdicional.FOLIO_TAG)) ? parser.parseNextValue(ConstantsTDCAdicional.FOLIO_TAG):"";
        fechaOperacion = (parser.hasValue(ConstantsTDCAdicional.FECHA_OPERACION_TAG)) ? parser.parseNextValue(ConstantsTDCAdicional.FECHA_OPERACION_TAG):"";


    }
}