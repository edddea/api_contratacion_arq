package bancomer.api.apitdcadicional.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.apitdcadicional.commons.ConstantsTDCAdicional;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by OOROZCO on 8/26/15.
 */
public class AceptaTDCAdicional implements ParsingHandler{

    private String lineaCredito;
    private String producto;
    private String desProducto;
    private String contrato;
    private String direccionTitular;
    private String folio;
    private String fechaOperacion;
    private String envioCorreoElectronico;
    private String envioSMS;
    private String PM;
    private ArrayList<Promociones> promociones=null;
    private Promociones[] arrayPromociones=null;


    public Promociones[] getArrayPromociones() {
        return arrayPromociones;
    }

    public void setArrayPromociones(Promociones[] arrayPromociones) {
        this.arrayPromociones = arrayPromociones;
    }

    public ArrayList<Promociones> getPromociones() {
        return promociones;
    }

    public void setPromociones(ArrayList<Promociones> promociones) {
        this.promociones = promociones;
    }

    public String getPM() {
        return PM;
    }

    public void setPM(String PM) {
        this.PM = PM;
    }

    public String getEnvioSMS() {
        return envioSMS;
    }

    public void setEnvioSMS(String envioSMS) {
        this.envioSMS = envioSMS;
    }

    public String getEnvioCorreoElectronico() {
        return envioCorreoElectronico;
    }

    public void setEnvioCorreoElectronico(String envioCorreoElectronico) {
        this.envioCorreoElectronico = envioCorreoElectronico;
    }

    public String getFechaOperacion() {
        return fechaOperacion;
    }

    public void setFechaOperacion(String fechaOperacion) {
        this.fechaOperacion = fechaOperacion;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getDireccionTitular() {
        return direccionTitular;
    }

    public void setDireccionTitular(String direccionTitular) {
        this.direccionTitular = direccionTitular;
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public String getDesProducto() {
        return desProducto;
    }

    public void setDesProducto(String desProducto) {
        this.desProducto = desProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getLineaCredito() {
        return lineaCredito;
    }

    public void setLineaCredito(String lineaCredito) {
        this.lineaCredito = lineaCredito;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        lineaCredito = parser.hasValue(ConstantsTDCAdicional.LINEA_CREDITO_TAG) ? parser.parseNextValue(ConstantsTDCAdicional.LINEA_CREDITO_TAG) : "";
        producto = parser.hasValue(ConstantsTDCAdicional.PRODUCTO_TAG) ? parser.parseNextValue(ConstantsTDCAdicional.PRODUCTO_TAG) : "";
        desProducto = parser.hasValue(ConstantsTDCAdicional.DES_PRODUCTO_TAG) ? parser.parseNextValue(ConstantsTDCAdicional.DES_PRODUCTO_TAG) : "";
        contrato = parser.hasValue(ConstantsTDCAdicional.CONTRATO_TAG) ? parser.parseNextValue(ConstantsTDCAdicional.CONTRATO_TAG) : "";
        direccionTitular = parser.hasValue(ConstantsTDCAdicional.DIRECCION_TITULAR_TAG) ? parser.parseNextValue(ConstantsTDCAdicional.DIRECCION_TITULAR_TAG) : "";
        folio = parser.hasValue(ConstantsTDCAdicional.FOLIO_TAG) ? parser.parseNextValue(ConstantsTDCAdicional.FOLIO_TAG) : "";
        fechaOperacion = parser.hasValue(ConstantsTDCAdicional.FECHA_OPERACION_TAG) ? parser.parseNextValue(ConstantsTDCAdicional.FECHA_OPERACION_TAG) : "";
        envioCorreoElectronico = parser.hasValue(ConstantsTDCAdicional.ENVIO_CORREO_ELECTRONICO_TAG)? parser.parseNextValue(ConstantsTDCAdicional.ENVIO_CORREO_ELECTRONICO_TAG) : "";
        envioSMS = parser.hasValue(ConstantsTDCAdicional.ENVIO_SMS_TAG) ? parser.parseNextValue(ConstantsTDCAdicional.ENVIO_SMS_TAG) : "";
        PM = parser.hasValue(ConstantsTDCAdicional.PM_TAG) ? parser.parseNextValue(ConstantsTDCAdicional.PM_TAG) : "";

        if (PM.equalsIgnoreCase("SI")) {
            try {
                JSONObject json = new JSONObject(parser.parseNextValue(ConstantsTDCAdicional.LP_TAG));
                JSONArray array = json.getJSONArray(ConstantsTDCAdicional.CAMPANIAS_TAG);
                promociones = new ArrayList<Promociones>();
                Promociones promocion;

                for (int i = 0; i < array.length(); i++) {
                    promocion = new Promociones();
                    JSONObject auxiliar = array.getJSONObject(i);
                    promocion.setCveCamp(auxiliar.getString(ConstantsTDCAdicional.CVE_CAMP_TAG));
                    promocion.setDesOferta(auxiliar.getString(ConstantsTDCAdicional.DES_OFERTA_TAG));
                    promocion.setMonto(auxiliar.getString(ConstantsTDCAdicional.MONTO_TAG));
                    promocion.setCarrusel(auxiliar.getString(ConstantsTDCAdicional.CARRUSEL_TAG));
                    promociones.add(promocion);

                }

                arrayPromociones = new Promociones[promociones.size()];

                for (int i = 0; i < promociones.size(); i++) {
                    arrayPromociones[i] = promociones.get(i);
                }


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
