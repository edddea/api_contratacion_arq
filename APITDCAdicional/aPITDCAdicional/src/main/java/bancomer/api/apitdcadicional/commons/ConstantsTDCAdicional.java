package bancomer.api.apitdcadicional.commons;


public class ConstantsTDCAdicional {

    public static final String CARRUSEL_TAG="carrusel";
    public static final String CARRUSEL_YES_VALUE="SI";


    public static final String OPERACION_TAG="operacion";
    public static final String NUMERO_CELULAR_TAG="numeroCelular";
    public static final String CVE_CAMP_TAG="cveCamp";
    public static final String IUM_TAG="IUM";
    public static final String OPCION_TAG="opcion";


    public static final String OPCION_DETALLE_VALUE="D";
    public static final String OPCION_FORMALIZACION_VALUE="F";

    public static final String ONE_CLICK_TDC_ADICIONAL="oneClicTDCAdicional";

    public static final String LINEA_CREDITO_TAG="lineaCredito";
    public static final String PRODUCTO_TAG="producto";
    public static final String DES_PRODUCTO_TAG="desProducto";
    public static final String CONTRATO_TAG="contrato";
    public static final String DIRECCION_TITULAR_TAG="direccionTitular";
    public static final String FOLIO_TAG="folio";
    public static final String FECHA_OPERACION_TAG="fechaOperacion";
    public static final String NOMBRE_ADIC_TAG="nombreAdic";
    public static final String PRIMER_APELLIDO_ADIC_TAG="primerApellidoAdic";
    public static final String SEGUNDO_APELLIDO_ADIC_TAG="segundoApellidoAdic";
    public static final String FECHA_NAC_ADIC_TAG="fechaNacAdic";
    public static final String RFC_ADIC_TAG="rfcAdic";
    public static final String CREDITO_SOLICITADO_TAG="creditoSolicitado";
    public static final String CODIGO_OTP_TAG="codigoOTP";
    public static final String CADENA_AUTENTICACION_TAG="cadenaAutenticacion";
    public static final String CODIGO_NIP_TAG="codigoNip";
    public static final String CODIGO_CVV2_TAG="codigoCVV2";
    public static final String TARJETA_5IG_TAG="Tarjeta5ig";
    public static final String ENVIO_CORREO_ELECTRONICO_TAG="envioCorreoElectronico";
    public static final String ENVIO_SMS_TAG="envioSMS";
    public static final String PM_TAG="PM";
    public static final String LP_TAG="LP";
    public static final String CAMPANIAS_TAG="campanias";

    public static final String DES_OFERTA_TAG="desOferta";
    public static final String MONTO_TAG="monto";
}
