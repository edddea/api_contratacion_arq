package bancomer.api.apitdcadicional.io;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;

public class ParserJSONImpl implements ParserJSON {

    /**
     * Response status successful.
     */
    public static final String STATUS_OK = "OK";

    /**
     * Response status warning.
     */
    public static final String STATUS_WARNING = "AVISO";

    /**
     * Response status error.
     */
    public static final String STATUS_ERROR = "ERROR";

    /**
     * Response optional application update.
     */
    public static final String STATUS_OPTIONAL_UPDATE = "AC";

    /**
     * Tag for status.
     */
    private static final String STATUS_TAG = "estado";

    /**
     * Tag for error code.
     */
    private static final String CODE_TAG = "codigoMensaje";

    /**
     * Tag for error message.
     */
    private static final String MESSAGE_TAG = "descripcionMensaje";

    /**
     * Alternative Tag for error message.
     */
    private static final String MESSAGE_INFORMATIVO_TAG = "mensajeInformativo";
    
    /**
     * Tag for application mandatory update URL.
     */
    private static final String URL_TAG = "UR";

    /**
     * Response reader.
     */
    private String reader;

    /**
     * Default constructor.
     * @param rder reader to read the response from
     */

    public ParserJSONImpl(String rder) {
        this.reader = rder;
    }
    
    /**
     * Parse the operation result from received message.
     * @return the operation result
     * @throws IOException on communication errors
     * @throws ParsingExceptionImpl on parsing errors
     */
    public ResultImpl parseResult() throws IOException, ParsingException {
        ResultImpl result = null;
        String status = parseNextValue(STATUS_TAG);
        if (status != null) {
            //System.out.println("Parse result: status = " + status);
            if (STATUS_OK.equals(status) || STATUS_OPTIONAL_UPDATE.equals(status)) {
                result = new ResultImpl(status, null, null);
            } else if ((STATUS_WARNING.equals(status)) || (STATUS_ERROR.equals(status))) {
                //System.out.println("Parse Warning or Error Status  - getting code");
                String code = parseNextValue(CODE_TAG);
                //System.out.println("Parse Warning or Error Status  - getting message");
                //String message = (code != null) ? parseNextValue(MESSAGE_TAG) : null;
                String message;
                try{
                	message = (code != null) ? parseNextValue(MESSAGE_TAG) : null;
                }catch(ParsingException e){
                	Log.d(ParserJSONImpl.class.getName(),"Error de formato en el mensaje de error, posible cambio de tag");
                	message = (code != null) ? parseNextValue(MESSAGE_INFORMATIVO_TAG) : null;
                }
                //System.out.println("Code = " + code + ", message = " + message);
                String updateURL = null;
                if ((code != null) && (code.equals("MBANK1111"))) {
                    updateURL = parseNextValue(URL_TAG);
                    result = new ResultImpl(status, code, message, updateURL);
                } else {
                    result = new ResultImpl(status, code, message);
                }

            }
        }
        return result;
    }

    /**
     * Obtain the next value for a tag.
     * @param tag the target tag
     * @return the value
     * @throws IOException on communication errors
     * @throws ParsingExceptionImpl on parsing errors
     */
    public String parseNextValue(String tag) throws IOException, ParsingException {
        return parseNextValue(tag, true);
    }

    /**
     * Obtain the next value for a tag.
     * @param tag the target tag
     * @param mandatory indicates the tag is mandatory
     * @return the value
     * @throws IOException on communication errors
     * @throws ParsingExceptionImpl on parsing errors
     */
    public String parseNextValue(String tag, boolean mandatory) throws ParsingException, IOException {
        String value = null;
		try {
			JSONObject jsonObject = new JSONObject(this.reader);
			value = jsonObject.getString(tag);
		} catch (JSONException e) {
			if(Server.ALLOW_LOG) e.printStackTrace();
		}
		if (mandatory) {
			if (value == null) {
				throw new ParsingException(new StringBuffer("Tag ").append(tag)
						.append(" not found in ").append(tag).toString());
			} else {
				return value;
			}
		} else {
			return value;
		}
	}

    public JSONArray parseNextValueWithArray(String tag) throws ParsingException, IOException {
    	return parseNextValueWithArray(tag, true);
    }
    
    public JSONArray parseNextValueWithArray(String tag, boolean mandatory) throws ParsingException, IOException {
        JSONArray array = new JSONArray();
		try {
			JSONObject jsonObject = new JSONObject(this.reader);
			array = jsonObject.getJSONArray(tag);
		} catch (JSONException e) {
			if(Server.ALLOW_LOG) e.printStackTrace();
		}
		if (mandatory) {
			if (array == null) {
				throw new ParsingException(new StringBuffer("Tag ").append(tag)
						.append(" not found in ").append(tag).toString());
			} else {
				return array;
			}
		} else {
			return array;
		}
	}
    
    public JSONObject parserNextObject(String tag) throws ParsingException, IOException {
    	return parserNextObject(tag, true);
    }
    
    public JSONObject parserNextObject(String tag, boolean mandatory) throws ParsingException, IOException {
    	JSONObject result = new JSONObject();
		try {
			JSONObject jsonObject = new JSONObject(this.reader);
			result = jsonObject.getJSONObject(tag);
			
		} catch (JSONException e) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al obtener el elemento " + tag + " del JSON.", e);
		}
		if (mandatory) {
			if (result == null) {
				throw new ParsingException(new StringBuffer("Tag ").append(tag).append(" not found in ").append(tag).toString());
			} else {
				return result;
			}
		} else {
			return result;
		}
    }

    public boolean hasValue(String tag) throws ParsingException, IOException {

        try {
            JSONObject jsonObject = new JSONObject(this.reader);
            return jsonObject.has(tag);
        } catch (JSONException e) {
            if(Server.ALLOW_LOG) e.printStackTrace();
        }
        return false;
    }

}
