package bancomer.api.apitdcadicional.gui.delegates;

import android.content.DialogInterface;

import java.util.Hashtable;

import bancomer.api.apitdcadicional.commons.ConstantsTDCAdicional;
import bancomer.api.apitdcadicional.implementations.InitTDCAdicional;
import bancomer.api.apitdcadicional.io.Server;
import bancomer.api.apitdcadicional.models.OfertaTDCAdicional;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.common.io.ServerResponse;

/**
 * Created by OOROZCO on 9/3/15.
 */
public class ConsultaDelegate implements BaseDelegate {

    private InitTDCAdicional init = InitTDCAdicional.getInstance();
    private BaseViewController baseViewController;


    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
    }

    public void doNetworkOperation(int operationId, Hashtable<String, ?> params,  boolean isJson, ParsingHandler handler,
                                   Server.isJsonValueCode isJsonValue,BaseViewController caller) {
        init.getBaseSubApp().invokeNetworkOperation(operationId,params,isJson,handler,isJsonValue,caller,false);
    }


    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (response.getStatus() == ServerResponse.OPERATION_ERROR)
        {
            init.getParentManager().resetRootViewController();
            init.updateHostActivityChangingState();
            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };
            init.getBaseViewController().showInformationAlert(init.getParentManager().getCurrentViewController(),response.getMessageText(),listener);
            init.resetInstance();
        }
        else if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)
        {
            init.showDetalle((OfertaTDCAdicional)response.getResponse());
        }

    }

    @Override
    public void performAction(Object obj) {

        baseViewController = init.getBaseViewController();
        baseViewController.setActivity(init.getParentManager().getCurrentViewController());
        baseViewController.setDelegate(this);
        init.setBaseViewController(baseViewController);

        InitTDCAdicional init = InitTDCAdicional.getInstance();
        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(ConstantsTDCAdicional.NUMERO_CELULAR_TAG,init.getSession().getUsername());
        params.put(ConstantsTDCAdicional.CVE_CAMP_TAG,init.getOfertaAdicional().getCveCamp());
        params.put(ConstantsTDCAdicional.IUM_TAG,init.getSession().getIum());
        params.put(ConstantsTDCAdicional.OPCION_TAG,ConstantsTDCAdicional.OPCION_DETALLE_VALUE);

        doNetworkOperation(Server.CONSULTA_DETALLE_TDC_ADICIONAL,params,true,new OfertaTDCAdicional(),Server.isJsonValueCode.NONE,InitTDCAdicional.getInstance().getBaseViewController());

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }
}
