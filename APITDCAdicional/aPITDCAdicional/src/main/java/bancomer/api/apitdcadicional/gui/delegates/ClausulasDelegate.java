package bancomer.api.apitdcadicional.gui.delegates;

import android.content.DialogInterface;
import android.util.Log;

import java.util.Hashtable;

import bancomer.api.apitdcadicional.R;
import bancomer.api.apitdcadicional.commons.ConstantsTDCAdicional;
import bancomer.api.apitdcadicional.gui.controllers.ClausulasViewController;
import bancomer.api.apitdcadicional.implementations.InitTDCAdicional;
import bancomer.api.apitdcadicional.io.Server;
import bancomer.api.apitdcadicional.models.AceptaTDCAdicional;
import bancomer.api.apitdcadicional.models.OfertaTDCAdicional;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.delegates.SecurityComponentDelegate;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.common.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by OOROZCO on 8/26/15.
 */
public class ClausulasDelegate extends SecurityComponentDelegate implements BaseDelegate {

    public static final long CLAUSULAS_TDC_ADICIONAL_DELEGATE = 130806334566765661L;

    private OfertaTDCAdicional ofertaTDCAdicional;
    private Promociones promocion;
    private ClausulasViewController controller;
    private String newAmount=null;
    private InitTDCAdicional init;

    private Constants.Operacion tipoOperacion;
    private Constants.TipoInstrumento tipoInstrumentoSeguridad;
    private String token;



    public void setController(ClausulasViewController controller) {
        this.controller = controller;
        setContext(controller);
    }

    public OfertaTDCAdicional getOfertaTDCAdicional() {
        return ofertaTDCAdicional;
    }

    public Promociones getPromocion() {
        return promocion;
    }

    public Constants.TipoInstrumento getTipoInstrumentoSeguridad() {
        return tipoInstrumentoSeguridad;
    }

    public ClausulasDelegate(OfertaTDCAdicional oferta, Promociones promocion)
    {
        this.ofertaTDCAdicional=oferta;
        this.promocion=promocion;
        init = InitTDCAdicional.getInstance();
        this.tipoOperacion = Constants.Operacion.oneClicTDCAdicional;

        String instrumento = init.getSession().getSecurityInstrument();

        if (instrumento.equals(Constants.IS_TYPE_DP270)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
        } else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
        } else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
        } else {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
        }
    }


    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
    }

    public void doNetworkOperation(int operationId, Hashtable<String, ?> params,  boolean isJson, ParsingHandler handler,
                                   Server.isJsonValueCode isJsonValue,BaseViewController caller) {
        InitTDCAdicional.getInstance().getBaseSubApp().invokeNetworkOperation(operationId,params,isJson,handler,isJsonValue,caller,false);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

        if(operationId == Server.ACEPTACION_DETALLE_TDC_ADICIONAL)
        {
            AceptaTDCAdicional model = (AceptaTDCAdicional)response.getResponse();

            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)
            {
                init.getParentManager().setActivityChanging(true);
                init.showExito((AceptaTDCAdicional)response.getResponse());

                if(model.getPM().equalsIgnoreCase("SI"))
                    init.getSession().setPromocion(model.getArrayPromociones());

            }
            else if(response.getStatus() == ServerResponse.OPERATION_ERROR)
            {
                try
                {
                    if(!model.getPM().equals(""))
                    {
                        if(model.getPM().equalsIgnoreCase("SI"))
                        {
                            init.getSession().setPromocion(model.getArrayPromociones());
                        }

                        DialogInterface.OnClickListener clickListener=new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                init.getParentManager().setActivityChanging(true);
                                init.getParentManager().showMenuInicial();
                                init.resetInstance();
                            }
                        };

                        init.getBaseViewController().showInformationAlert(controller, response.getMessageText(), clickListener);

                    }
                    else
                    {
                        init.getBaseViewController().showInformationAlert(controller, response.getMessageText());
                    }

                }
                catch (NullPointerException ex)
                {
                    init.getBaseViewController().showErrorMessage(controller,response.getMessageText());
                }


            }else
            {
                init.getBaseViewController().showInformationAlert(controller, response.getMessageText());
            }

        }

    }

    @Override
    public void performAction(Object obj) {
        InitTDCAdicional init = InitTDCAdicional.getInstance();
        String cadAutenticacion = init.getAutenticacion().getCadenaAutenticacion(this.tipoOperacion,init.getSession().getClientProfile());

        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put(ConstantsTDCAdicional.NUMERO_CELULAR_TAG,init.getSession().getUsername());
        params.put(ConstantsTDCAdicional.CVE_CAMP_TAG,promocion.getCveCamp());
        params.put(ConstantsTDCAdicional.IUM_TAG,init.getSession().getIum());
        params.put(ConstantsTDCAdicional.OPCION_TAG,ConstantsTDCAdicional.OPCION_FORMALIZACION_VALUE);
        params.put(ConstantsTDCAdicional.NOMBRE_ADIC_TAG, controller.getName().getText().toString().trim());
        params.put(ConstantsTDCAdicional.PRIMER_APELLIDO_ADIC_TAG, controller.getFirstLastName().getText().toString().trim());
        params.put(ConstantsTDCAdicional.SEGUNDO_APELLIDO_ADIC_TAG, controller.getSecondLastName().getText().toString().trim());
        params.put(ConstantsTDCAdicional.FECHA_NAC_ADIC_TAG,getBirthDateForRequest(controller.getBirthDate().getText().toString().trim()));
        params.put(ConstantsTDCAdicional.RFC_ADIC_TAG, controller.getRFC().getText().toString().trim());
        params.put(ConstantsTDCAdicional.CREDITO_SOLICITADO_TAG, newAmount!=null?newAmount:ofertaTDCAdicional.getLineaCredito());
        params.put(ConstantsTDCAdicional.CODIGO_OTP_TAG, token);
        params.put(ConstantsTDCAdicional.CADENA_AUTENTICACION_TAG, cadAutenticacion);
        params.put(ConstantsTDCAdicional.CODIGO_NIP_TAG, "");
        params.put(ConstantsTDCAdicional.CODIGO_CVV2_TAG, "");
        params.put(ConstantsTDCAdicional.TARJETA_5IG_TAG, "");

        doNetworkOperation(Server.ACEPTACION_DETALLE_TDC_ADICIONAL,params,true,new AceptaTDCAdicional(),Server.isJsonValueCode.NONE,InitTDCAdicional.getInstance().getBaseViewController());
    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }


    public boolean isNewAmountOk(String amount)
    {
        String oldAmount = ofertaTDCAdicional.getLineaCredito().substring(0, ofertaTDCAdicional.getLineaCredito().length() - 2);

        int lastAmount = Integer.parseInt(oldAmount);
        int newAmount;

        try
        {
            newAmount = Integer.parseInt(amount);

        }catch(NumberFormatException e)
        {
            Log.w("Error caused by: ", e.getLocalizedMessage());
            return false;
        }


        if(newAmount>lastAmount){
            controller.setFirstMessagePopUp();
            return false;
        }
        else if(newAmount<1000) {
            controller.setSecondMessagePopUp();
            return false;
        }
        else if(newAmount %100 !=0) {
            controller.setThirdMessagePopUp();
            return false;
        }

        this.newAmount = String.valueOf(newAmount)+"00";

        return true;
    }



    public boolean areInputOk()
    {
        if(controller.getName().getText().toString().trim().length()<=0)
        {
            init.getBaseViewController().showInformationAlert(controller, controller.getResources().getString(R.string.bmovil_clausulas_valorvacio1) + " " + controller.getResources().getString(R.string.bmovil_clausulas_editNombre) + " " + controller.getResources().getString(R.string.bmovil_clausulas_valorvacio2));
            return false;
        }else if(controller.getFirstLastName().getText().toString().trim().length()<=0)
        {
            init.getBaseViewController().showInformationAlert(controller, controller.getResources().getString(R.string.bmovil_clausulas_valorvacio1) + " " + controller.getResources().getString(R.string.bmovil_clausulas_editAPaterno) + " " + controller.getResources().getString(R.string.bmovil_clausulas_valorvacio2));
            return false;
        }else if(controller.getSecondLastName().getText().toString().trim().length()<=0)
        {
            init.getBaseViewController().showInformationAlert(controller, controller.getResources().getString(R.string.bmovil_clausulas_valorvacio1) + " " + controller.getResources().getString(R.string.bmovil_clausulas_editAMaterno) + " " + controller.getResources().getString(R.string.bmovil_clausulas_valorvacio2));
            return false;
        }else if(controller.getRFC().getText().toString().trim().length()<=0)
        {
            init.getBaseViewController().showInformationAlert(controller, controller.getResources().getString(R.string.bmovil_clausulas_valorvacio1) + " " + controller.getResources().getString(R.string.bmovil_clausulas_editRfc) + " " + controller.getResources().getString(R.string.bmovil_clausulas_valorvacio2));
            return false;
        }else if(controller.getBirthDate().getText().toString().trim().length()<=0)
        {
            init.getBaseViewController().showInformationAlert(controller, controller.getResources().getString(R.string.bmovil_clausulas_valorvacio1a) + " " + controller.getResources().getString(R.string.bmovil_clausulas_editFecha) + " " + controller.getResources().getString(R.string.bmovil_clausulas_valorvacio2));
            return false;
        }
        else if(!validateRFCWithDate())
        {
            init.getBaseViewController().showInformationAlert(controller,controller.getResources().getString(R.string.bmovil_clausulas_rfcFechaMensaje));
            return false;
        }


        return true;
    }

    private boolean validateRFCWithDate()
    {
        String date = controller.getDate();
        String rfc="";
        try{
            rfc = controller.getRFC().getText().toString().substring(4,10);
        }catch(StringIndexOutOfBoundsException exception)
        {
            Log.e("Error while getting RFC value caused by:",exception.getLocalizedMessage());
        }
        return date.equals(rfc);
    }

    private String getBirthDateForRequest(String date)
    {
        try{
            String array [] = date.split("/");
            return array[2]+"-"+array[1]+"-"+array[0];
        }
        catch(Exception e)
        {
            Log.w("Error while formatting Date caused by: ", e.getLocalizedMessage());
        }
        return "";
    }






    /////// Methods for Security components showing. /////////


    public boolean mostrarContrasenia() {
        return init.getAutenticacion().mostrarContrasena(this.tipoOperacion,
                init.getSession().getClientProfile(),
                Tools.getDoubleAmountFromServerString(ofertaTDCAdicional.getLineaCredito()));
    }

    public boolean mostrarNIP() {
        return init.getAutenticacion().mostrarNIP(this.tipoOperacion,
                init.getSession().getClientProfile());
    }

    public Constants.TipoOtpAutenticacion tokenAMostrar() {
        Constants.TipoOtpAutenticacion tipoOTP;
        try {
            tipoOTP = init.getAutenticacion().tokenAMostrar(
                    this.tipoOperacion,
                    init.getSession().getClientProfile(),
                    Tools.getDoubleAmountFromServerString(ofertaTDCAdicional.getLineaCredito())
            );

        } catch (Exception ex) {
            if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "Error on Autenticacion.mostrarNIP execution.", ex);
            tipoOTP = null;
        }

        return tipoOTP;
    }

    public boolean mostrarCVV() {
        return init.getAutenticacion().mostrarCVV(this.tipoOperacion,
                init.getSession().getClientProfile(),
                Tools.getDoubleAmountFromServerString(ofertaTDCAdicional.getLineaCredito()));
    }




    public boolean enviaPeticionOperacion() {
        String contrasena = null;
        String nip = null;
        String asm = null;
        String cvv = null;
        if(controller instanceof ClausulasViewController){
            if (mostrarContrasenia()) {
                contrasena = controller.pideContrasena();
                if (contrasena.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteContrasena);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.PASSWORD_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteContrasena);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            if (mostrarNIP()) {
                nip = controller.pideNIP();
                if (nip.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteNip);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (nip.length() != Constants.NIP_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.NIP_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteNip);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }
            if (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) {
                asm = controller.pideASM();
                if (asm.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    switch (tipoInstrumentoSeguridad) {
                        case OCRA:
                            mensaje += getEtiquetaCampoOCRA();
                            break;
                        case DP270:
                            mensaje += getEtiquetaCampoDP270();
                            break;
                        case SoftToken:
                            if (init.getSofTokenStatus()) {
                                mensaje += getEtiquetaCampoSoftokenActivado();
                            } else {
                                mensaje += getEtiquetaCampoSoftokenDesactivado();
                            }
                            break;
                        default:
                            break;
                    }
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (asm.length() != Constants.ASM_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.ASM_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    switch (tipoInstrumentoSeguridad) {
                        case OCRA:
                            mensaje += getEtiquetaCampoOCRA();
                            break;
                        case DP270:
                            mensaje += getEtiquetaCampoDP270();
                            break;
                        case SoftToken:
                            if (init.getSofTokenStatus()) {
                                mensaje += getEtiquetaCampoSoftokenActivado();
                            } else {
                                mensaje += getEtiquetaCampoSoftokenDesactivado();
                            }
                            break;
                        default:
                            break;
                    }
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }
            if (mostrarCVV()) {
                cvv = controller.pideCVV();
                if (cvv.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteCvv);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (cvv.length() != Constants.CVV_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.CVV_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteCvv);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            String newToken = null;
            if(tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == Constants.TipoInstrumento.SoftToken && init.getSofTokenStatus())
                newToken = init.getDelegateOTP().loadOtpFromSofttoken(tokenAMostrar(),init.getDelegateOTP());
            if(null != newToken)
                asm = newToken;
            token=asm;
            return true;
        }
        return false;
    }

    public String getTextoAyudaInstrumentoSeguridad(Constants.TipoInstrumento tipoInstrumento) {
        Constants.TipoOtpAutenticacion tokenAMostrar = tokenAMostrar();
        if(getContext()==null && controller!=null){
            setContext(controller);
        }
        return getTextoAyudaInstrumentoSeguridad(tipoInstrumento, init.getSofTokenStatus(), tokenAMostrar);
    }


}
