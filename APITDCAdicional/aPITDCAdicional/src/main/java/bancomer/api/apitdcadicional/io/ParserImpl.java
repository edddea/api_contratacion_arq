/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package bancomer.api.apitdcadicional.io;

import java.io.IOException;
import java.io.Reader;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParsingException;

/**
 * Parser is able to parse an operation response from Bancomer server.
 *
 * @author Stefanini IT Solutions.
 */
public class ParserImpl implements Parser {

    /**
     * Response status successful.
     */
    public static final String STATUS_OK = "OK";

    /**
     * Response status warning.
     */
    public static final String STATUS_WARNING = "AVISO";

    /**
     * Response status error.
     */
    public static final String STATUS_ERROR = "ERROR";

    /**
     * Response optional application update.
     */
    public static final String STATUS_OPTIONAL_UPDATE = "AC";

    /**
     * Tag for status.
     */
    private static final String STATUS_TAG = "ES";

    /**
     * Tag for error code.
     */
    private static final String CODE_TAG = "CO";

    /**
     * Tag for error message.
     */
    private static final String MESSAGE_TAG = "ME";

    /**
     * Tag for application mandatory update URL.
     */
    private static final String URL_TAG = "UR";

    /**
     * Tag separator.
     */
    private static final char SEPARATOR_CHAR = '*';

    /**
     * Escape character for special symbols in the response.
     */
    private static final char CODE_CHAR = '%';

    /**
     * Size in characters of a special symbol.
     */
    private static final int CODE_SIZE = 2;

    /**
     * Response reader.
     */
    private Reader reader;

    /**
     * read buffer.
     */
    private StringBuffer readBuffer;

    /**
     * code buffer.
     */
    private StringBuffer codeBuffer;

    /**
     * Flag to indicate that the reader should not go forward due
     * to unfound optional tags.
     */
    private boolean pauseReader = false;
    /**
     * .
     */
    private String lastEntity = null;

    /**
     * Default constructor.
     * @param rder reader to read the response from
     */
    public ParserImpl(Reader rder) {
        this.reader = rder;
        this.readBuffer = new StringBuffer();
        this.codeBuffer = new StringBuffer();
    }

    /**
     * Parse the operation result from received message.
     * @return the operation result
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public ResultImpl parseResult() throws IOException, ParsingException {
        ResultImpl result = null;
        String status = parseNextValue(STATUS_TAG);
        if (status != null) {
            //System.out.println("Parse result: status = " + status);
            if (STATUS_OK.equals(status) || STATUS_OPTIONAL_UPDATE.equals(status)) {
                result = new ResultImpl(status, null, null);
            } else if ((STATUS_WARNING.equals(status)) || (STATUS_ERROR.equals(status))) {
                //System.out.println("Parse Warning or Error Status  - getting code");
            	
            	// Modificación 44540: parsear un dato que no sirve para nada
            	String periodo = parseNextValue("PE", false);
            	
                String code = parseNextValue(CODE_TAG);
                //System.out.println("Parse Warning or Error Status  - getting message");
                String message = (code != null) ? parseNextValue(MESSAGE_TAG) : null;
                //System.out.println("Code = " + code + ", message = " + message);
                String updateURL = null;
                if ((code != null) && (code.equals("MBANK1111"))) {
                    updateURL = parseNextValue(URL_TAG);
                    result = new ResultImpl(status, code, message, updateURL);
                } else {
                    result = new ResultImpl(status, code, message);
                }

            }
        }
        return result;
    }

    /**
     * Obtain the next entity.
     * @return the next entity
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public String parseNextEntity() throws IOException, ParsingException {
        return parseNextEntity(false);
    }

    /**
     * Obtain the next entity.
     * @param throwException indicates if an exception must be thrown on end of input content
     * @return the entity
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public String parseNextEntity(boolean throwException) throws IOException, ParsingException {

        String entity = null;

        boolean separator = false;
        int read;
        char character;
        char translated=' ';
        boolean finished = false;
        int codeCounter = 0;
        String codeText;
        int code;
        if (this.pauseReader) {
            entity = lastEntity;
        } else {
            synchronized (this.readBuffer) {

                this.readBuffer.setLength(0);

                do {
                    read = this.reader.read();
                    if (read == -1) {
                        finished = true;
                    } else {
                        character = (char) read;
                        
                        if(character == SEPARATOR_CHAR){
                        	if (character == SEPARATOR_CHAR && translated == '"') {                               
                                character =' ';
                            }else{
                            	separator = true;
                                finished = true;
                            }                            	
                        }/*
                        if(character == SEPARATOR_CHAR){
                                separator = true;
                                finished = true;                            	
                        }*/
                        else if (character == '\n') {
                            // Ignored
                        } else if (character == '\r') {
                            // Ignored
                        } else {
                            if (character == '\t') {
                                // Translated to blank space
                                translated = ' ';
                            } else {
                                translated = character;
                            }
                            if (codeCounter > 0) {
                                this.codeBuffer.append(translated);
                                codeCounter--;
                                if (codeCounter == 0) {
                                    codeText = this.codeBuffer.toString();
                                    this.codeBuffer.setLength(0);
                                    code = Integer.parseInt(codeText, 16);
                                    this.readBuffer.append((char) code);
                                }
                            } else {
                                if (translated == CODE_CHAR) {
                                    codeCounter = CODE_SIZE;
                                    this.codeBuffer.setLength(0);
                                } else {
                                    this.readBuffer.append(translated);
                                }
                            }
                        }
                    }
                } while (!finished);

                if ((this.readBuffer.length() > 0) || (separator)) {
                    entity = this.readBuffer.toString();
                } else if (throwException) {
                    throw new ParsingException("End of input reached!");
                }

                this.readBuffer.setLength(0);

            }
        }
        return entity;

    }

    /**
     * Obtain the next value of the whole entity.
     * @return the next value
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public String parseNextValue() throws IOException, ParsingException {
        return parseNextEntity();
    }

    /**
     * Obtain the next value for a tag.
     * @param tag the target tag
     * @return the value
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public String parseNextValue(String tag) throws IOException, ParsingException {
        return parseNextValue(tag, true);
    }

    /**
     * Obtain the next value for a tag.
     * @param tag the target tag
     * @param mandatory indicates the tag is mandatory
     * @return the value
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public String parseNextValue(String tag,
    			   boolean mandatory)
    			   throws ParsingException, IOException {
        String value = null;
        String entity = parseNextEntity();        
        if (entity != null) {
            if (tag != null) {
                if (entity.startsWith(tag)) {
                    value = entity.substring(tag.length());
                    this.pauseReader = false;
                    this.lastEntity = null;
                } else {
                    if (mandatory) {
                        throw new ParsingException(
                                new StringBuffer("Tag ").append(tag).
                                    append(" not found in ").append(entity).toString());
                    } else {
                        this.pauseReader = true;
                        this.lastEntity = entity;
                    }
                }
            } else {            	
                throw new ParsingException("Invalid tag!");
            }
        } else {      
            //throw new ParsingException("Entity not found!");
        	return null;
        }
       
        return value;
    }
    
    /**
     * Gets the value for a tag in an entity.
     * @param entity the entity
     * @param tag the tag
     * @return the value
     * @throws IOException on parsing errors
     */
    public String entityValue(String entity, String tag) throws IOException {
        String value = null;
        if (entity != null) {
            if (tag != null) {
                if (entity.startsWith(tag)) {
                    value = entity.substring(tag.length());
                }
            }
        }
        return value;
    }

    /**
     * Try to parse a string as an integer.
     * @param value value to convert
     * @param defaultValue value to use if the conversion cannot be accomplished
     * @return the converted integer, or defaultValue if the conversion was not
     * successful.
     */
    public int valueAsInt(String value, int defaultValue) {
        int result = defaultValue;
        if (value != null) {
            try {
                result = Integer.parseInt(value.trim());
            } catch (NumberFormatException e) {
                //e.printStackTrace();
            }
        }
        return result;
    }

}