package bancomer.api.apitdcadicional.gui.controllers;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.adobe.mobile.Config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.apitdcadicional.R;
import bancomer.api.apitdcadicional.commons.TrackerAdicional;
import bancomer.api.apitdcadicional.gui.delegates.DetalleDelegate;
import bancomer.api.apitdcadicional.implementations.InitTDCAdicional;
import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;

/**
 * Created by OOROZCO on 8/20/15.
 */
public class DetalleViewController extends Activity implements View.OnClickListener  {

    private BaseViewController baseViewController;
    private InitTDCAdicional init = InitTDCAdicional.getInstance();
    private DetalleDelegate delegate;
    public BaseViewsController parentManager;

    /*View Elements*/
    private TextView labelPromocion, labelImporteCredito, labelUsala;
    private ImageButton btnMeInteresa;


    //Adobe
    String screen = "";
    ArrayList<String> list= new ArrayList<String>();
    Map<String,Object> operacionMap = new HashMap<String, Object>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate=(DetalleDelegate) parentManager.getBaseDelegateForKey(DetalleDelegate.DETALLE_TDC_ADICIONAL_DELEGATE);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_tdc_adicional_detalle);
        baseViewController.setTitle(this, R.string.bmovil_promocion_title_tdc_adicional, R.drawable.an_ic_tdc_adicional);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        //Adobe
        Config.setContext(this.getApplicationContext());

        screen = "detalle tdc Adicional";
        list.add(screen);
        TrackerAdicional.trackState(list);

        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackerAdicional.trackEntrada(eventoEntrada);

        findViews();
        setDataToView();
        scaleViews();
    }


    private void scaleViews()
    {
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(labelImporteCredito, true);
        gTools.scale(labelPromocion, true);
        gTools.scale(labelUsala, true);
        gTools.scale(btnMeInteresa);
    }


    private void setDataToView()
    {
        final String promocion1Text=getResources().getString(R.string.bmovil_detalle_promocion1_tdc_adicional);
        final String promocion2Text=getResources().getString(R.string.bmovil_detalle_promocion2_tdc_adicional);
        final String promocion3Text=getResources().getString(R.string.bmovil_detalle_promocion3_tdc_adicional);
        final String promocion4Text=getResources().getString(R.string.bmovil_detalle_promocion4_tdc_adicional);


        final String promocion=promocion1Text+
                promocion2Text+
                promocion3Text+
                " "+
                delegate.getDetalle().getDesProducto()+
                " "+
                promocion4Text;

        final int indexPromocion1 = promocion.indexOf(promocion1Text);
        final int indexPromocion2=promocion.indexOf(promocion2Text);
        final int indexPromocion3=promocion.indexOf(promocion3Text);
        final int indexDesOferta=promocion.indexOf(delegate.getDetalle().getDesProducto());
        final int indexPromocion4=promocion.indexOf(promocion4Text);


        SpannableString content = new SpannableString(promocion);
        content.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primer_azul)), indexPromocion1, indexPromocion1 + promocion1Text.length(), 0);
        content.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.verde_bmovil_detalle)), indexPromocion2, indexPromocion2 + promocion2Text.length(), 0);
        content.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primer_azul)),indexPromocion3,indexPromocion3+promocion3Text.length(),0);
        content.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.verde_bmovil_detalle)),indexDesOferta,indexDesOferta+delegate.getDetalle().getDesProducto().length(),0);
        content.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.primer_azul)),indexPromocion4,indexPromocion4+promocion4Text.length(),0);

        labelPromocion.setText(content);

        SpannableString amount = new SpannableString(Tools.formatAmount(delegate.getDetalle().getLineaCredito(), false));
        amount.setSpan(new UnderlineSpan(),0,amount.length(),0);
        labelImporteCredito.setText(amount);

    }



    private void findViews()
    {
        labelPromocion = (TextView) findViewById(R.id.labelPromocion);
        labelImporteCredito = (TextView) findViewById(R.id.labelImporteCredito);
        labelUsala = (TextView) findViewById(R.id.labelUsala);
        btnMeInteresa = (ImageButton) findViewById(R.id.btnMeInteresa);
        btnMeInteresa.setOnClickListener(this);
    }



    @Override
    public void onClick(View v) {
        init.getParentManager().setActivityChanging(true);
        init.showClausulas(delegate.getDetalle(), init.getOfertaAdicional());
    }


    @Override
    protected void onPause() {
        super.onPause();
        Config.collectLifecycleData();
        if(!init.getParentManager().isActivityChanging())
        {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Config.pauseCollectingLifecycleData();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if (DialogInterface.BUTTON_POSITIVE == which) {
                        init.getParentManager().setActivityChanging(true);
                        //init.getParentManager().showMenuInicial();
                        ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                        init.getParentManager().resetRootViewController();
                        init.resetInstance();
                    }else{
                        dialog.dismiss();
                    }

                }
            };

            init.getBaseViewController().showYesNoAlert(this,R.string.bmovil_detalle_promocion_tdc_adicional_rechazo, listener);

        }
        return true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }
}
