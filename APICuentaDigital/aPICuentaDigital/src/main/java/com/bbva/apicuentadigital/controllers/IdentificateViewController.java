package com.bbva.apicuentadigital.controllers;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.common.Tools;
import com.bbva.apicuentadigital.delegates.CreaCuentaDelegate;
import com.bbva.apicuentadigital.delegates.IdentificateDelegate;
import com.bbva.apicuentadigital.models.ConsultaColonias;
import com.bbva.apicuentadigital.models.ConsultaColoniasCurp;
import com.bbva.apicuentadigital.models.ConsultaColoniasNueva;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelperCuentaDigital;

/**
 * Created by Karina on 07/12/2015.
 */
public class IdentificateViewController extends Fragment implements View.OnClickListener {

    private View v;
    private GuiTools guiTools;
    private CreaCuentaDelegate creaCuentaDelegate;
    private IdentificateDelegate identificateDelegate;
    private APICuentaDigitalSharePreferences sharePreferences;

    private ImageButton imbContinuar;


    public TextView txtNombre;
    public TextView txtApaterno;
    public TextView txtAmaterno;
    public TextView txtFechaN;
    public TextView txtLugarN;
    public TextView txtCredencialE;
    public TextView txtCP;
    public TextView txtEstado;
    public TextView txtName;
    public TextView txtGenero;
    public TextView txtMunicipios;

    public EditText edtNombre;
    public EditText edtApaterno;
    public EditText edtAmaterno;
    public EditText edtFechaN;
    private int day, month, year = 0;
    public EditText edtCredencialE;
    public ImageView img_credencial;
    public EditText edtCell;
    public EditText edtEmail;
    public EditText edtEmailConfirm;
    public EditText edtCalle;
    public EditText edtNumExt;
    public EditText edtNumInt;

    public Spinner spnLugarN;
    public Spinner spnCompany;
    private Spinner spnColonias;
    public Spinner spnEstado;

    private RadioButton cbMasculino;
    private RadioButton cbFemenino;

    private ImageView imgDatos;
    private ImageView imgContacto;
    private ImageView imgDomicilio;


    private LinearLayout lnDatos;
    private LinearLayout lnContacto;
    private LinearLayout lnDomicilio;
    private LinearLayout lnActual;
    private LinearLayout lnTelefono;

    private boolean datos;
    private boolean contacto;
    private boolean domicilio;
    private boolean isVisible;
    private boolean bntHabilitado;

    private String json;
    private Thread hilo;


    public IdentificateViewController(GuiTools guiTools, CreaCuentaDelegate creaCuentaDelegate, boolean isVisible) {
        this.guiTools = guiTools;
        this.creaCuentaDelegate = creaCuentaDelegate;
        this.isVisible = isVisible;
        identificateDelegate = new IdentificateDelegate(creaCuentaDelegate, this);
        sharePreferences = APICuentaDigitalSharePreferences.getInstance();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.layout_identificate, container, false);
        init();

        json = getActivity().getIntent().getExtras().getString(Constants.CONSULTA_COLONIA);
        return v;
    }

    private void init() {
        findViewById();
        scaleView();
        listenerDate();
        setData();
        validaCurp();
        validarCeluar();

        etiquetado();
        starThreadEnable();
    }

    private void etiquetado() {
        ArrayList<String> list = new ArrayList<String>();
        list.add(Constants.QUIERO_CUENTA);
        list.add(Constants.CREA_CUENTA);
        TrackingHelperCuentaDigital.trackState(list);
        Map<String, Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackingHelperCuentaDigital.trackEntrada(eventoEntrada);
    }

    private void findViewById() {

        txtNombre = (TextView) v.findViewById(R.id.lbl_name);
        txtApaterno = (TextView) v.findViewById(R.id.lbl_aPaterno);
        txtAmaterno = (TextView) v.findViewById(R.id.lbl_aMaterno);
        txtFechaN = (TextView) v.findViewById(R.id.lbl_fecha);
        txtLugarN = (TextView) v.findViewById(R.id.lbl_lugar);
        txtCredencialE = (TextView) v.findViewById(R.id.lbl_credential);
        txtName = (TextView) v.findViewById(R.id.txt_name);
        txtGenero = (TextView) v.findViewById(R.id.txt_genero);
        //falta txtGenero
        edtNombre = (EditText) v.findViewById(R.id.edt_name);
        edtApaterno = (EditText) v.findViewById(R.id.edt_aPaterno);
        edtAmaterno = (EditText) v.findViewById(R.id.edt_aMaterno);
        edtFechaN = (EditText) v.findViewById(R.id.edt_fecha);
        edtCredencialE = (EditText) v.findViewById(R.id.edt_credential);
        edtCell = (EditText) v.findViewById(R.id.edt_cell);
        edtEmail = (EditText) v.findViewById(R.id.edt_email);
        edtEmailConfirm = (EditText) v.findViewById(R.id.edt_email_confirm);

        spnCompany = (Spinner) v.findViewById(R.id.spn_company);
        spnLugarN = (Spinner) v.findViewById(R.id.sp_lugar);

        cbMasculino = (RadioButton) v.findViewById(R.id.cb_masculino);
        cbFemenino = (RadioButton) v.findViewById(R.id.cb_femenino);

        txtCP = (TextView) v.findViewById(R.id.txt_cp);
        txtEstado = (TextView) v.findViewById(R.id.txt_state);

        edtCalle = (EditText) v.findViewById(R.id.edt_street);
        edtNumExt = (EditText) v.findViewById(R.id.edt_num_ext);
        edtNumInt = (EditText) v.findViewById(R.id.edt_num_int);

        txtMunicipios = (TextView) v.findViewById(R.id.txt_municipaly);
        spnColonias = (Spinner) v.findViewById(R.id.spn_colony);

        spnEstado = (Spinner) v.findViewById(R.id.spn_state);

        imbContinuar = (ImageButton) v.findViewById(R.id.imb_continue);

        imgContacto = (ImageView) v.findViewById(R.id.img_contacto);
        imgDomicilio = (ImageView) v.findViewById(R.id.img_domicilio);
        imgDatos = (ImageView) v.findViewById(R.id.img_datos);

        lnContacto = (LinearLayout) v.findViewById(R.id.datosContacto);
        lnDatos = (LinearLayout) v.findViewById(R.id.datos);
        lnDomicilio = (LinearLayout) v.findViewById(R.id.domicilio);
        lnTelefono = (LinearLayout) v.findViewById(R.id.ln_telefono);

        imbContinuar.setOnClickListener(this);

        imgDatos.setOnClickListener(this);
        imgDomicilio.setOnClickListener(this);
        imgContacto.setOnClickListener(this);

        lnActual = lnDatos;

        img_credencial=((ImageView) v.findViewById(R.id.img_credencial));

        edtFechaN.setInputType(InputType.TYPE_NULL);
        edtFechaN.setOnClickListener(this);
        img_credencial.setOnClickListener(this);

       /* edtFechaN.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    String fecha = edtFechaN.getText().toString();

                }
                return false;
            }
        });*/

        spnEstado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if ((CharSequence) spnEstado.getSelectedItem() != "Selecciona") {
                    cargaDatosColonia();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                spnEstado.setBackgroundResource(R.drawable.an_listboxcorto_magenta);
            }
        });
    }

    private void scaleView() {
        guiTools.scale(v.findViewById(R.id.txt_mesage), true);
        guiTools.scale(txtGenero, true);
        //guiTools.scale(v.findViewById(R.id.txt_name_item_datos), true);
        //guiTools.scale(v.findViewById(R.id.txt_name_item_contacto), true);
        //guiTools.scale(v.findViewById(R.id.txt_name_item_domicilio), true);
        guiTools.scale(v.findViewById(R.id.lbl_cell), true);
        guiTools.scale(v.findViewById(R.id.lbl_company), true);
        guiTools.scale(v.findViewById(R.id.lbl_email), true);
        guiTools.scale(v.findViewById(R.id.lbl_email_confirm), true);
        guiTools.scale(v.findViewById(R.id.lbl_cp), true);
        //guiTools.scale(v.findViewById(R.id.lbl_state), true);
        guiTools.scale(v.findViewById(R.id.lbl_municipaly), true);
        guiTools.scale(v.findViewById(R.id.lbl_colony), true);
        guiTools.scale(v.findViewById(R.id.lbl_street), true);
        guiTools.scale(v.findViewById(R.id.lbl_num_int), true);
        guiTools.scale(v.findViewById(R.id.lbl_num_ext), true);
        guiTools.scale(txtNombre, true);
        guiTools.scale(txtAmaterno, true);
        guiTools.scale(txtApaterno, true);
        guiTools.scale(txtCP, true);
        guiTools.scale(txtCredencialE, true);
        guiTools.scale(txtEstado, true);
        guiTools.scale(txtFechaN, true);
        guiTools.scale(txtLugarN, true);
        guiTools.scale(txtName, true);
        guiTools.scale(edtAmaterno);
        guiTools.scale(edtApaterno);
        guiTools.scale(edtCalle);
        guiTools.scale(edtCell);
        guiTools.scale(edtCredencialE);
        guiTools.scale(edtEmail);
        guiTools.scale(edtEmailConfirm);
        guiTools.scale(edtFechaN);
        guiTools.scale(edtNombre);
        guiTools.scale(edtNumExt);
        guiTools.scale(edtNumInt);
        guiTools.scale(spnColonias);
        guiTools.scale(spnCompany);
        guiTools.scale(spnLugarN);
        guiTools.scale(spnEstado);
        guiTools.scale(txtMunicipios, true);

        guiTools.scale(v.findViewById(R.id.img_aviso));
        guiTools.scale(imgContacto);
        guiTools.scale(imgDatos);
        guiTools.scale(imgDomicilio);
        guiTools.scale(imbContinuar);
        guiTools.scale(img_credencial);
    }

    private void setData() {
        txtCP.setText(sharePreferences.getStringData(Constants.TAG_CODIGO_POSTAL));
        //txtEstado.setText(sharePreferences.getStringData(Constants.TAG_ENTIDAD));
        //txtMunicipios.setText(sharePreferences.getStringData(Constants.TAG_DELEGACION));
        txtMunicipios.setText("");
        //loadColonias();
        loadCompanies();
        loadStates();
    }

    private void loadColonias() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, creaCuentaDelegate.getColonias());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnColonias.setAdapter(adapter);
    }

    private void loadColoniasNuew() {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, identificateDelegate.getColonias());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnColonias.setAdapter(adapter);
    }

    private void loadCompanies() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Tools.getCompanias());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCompany.setAdapter(adapter);
    }

    private void loadStates() {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, Tools.getLugarN());
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnLugarN.setAdapter(adapter);
        spnEstado.setAdapter(adapter);
    }

    private void ocultarVista(LinearLayout ln) {
        ImageView img = null;
        ln.setVisibility(View.GONE);
        if (ln == lnDatos) {
            img = imgDatos;
            img.setImageResource(R.drawable.img_datospersonales_2);
            datos = true;
        } else if (ln == lnDomicilio) {
            img = imgDomicilio;
            img.setImageResource(R.drawable.img_domicilio_3);
            domicilio = true;
        } else if (ln == lnContacto) {
            img = imgContacto;
            img.setImageResource(R.drawable.img_datoscontacto_3);
            contacto = true;
        }
    }

    private void mostarVista(LinearLayout ln) {
        ImageView img = null;
        ln.setVisibility(View.VISIBLE);
        if (ln == lnDatos) {
            img = imgDatos;
            img.setImageResource(R.drawable.img_datospersonales_1);
            datos = false;
        } else if (ln == lnDomicilio) {
            img = imgDomicilio;
            domicilio = false;
            img.setImageResource(R.drawable.img_domicilio_1);
        } else if (ln == lnContacto) {
            img = imgContacto;
            contacto = false;
            img.setImageResource(R.drawable.img_datoscontacto_1);
        }
        //img.setImageResource(R.drawable.an_ic_arriba);
    }

    private void seleccionarItem(LinearLayout ln) {
        if (ln == lnDatos) {
            if (ln.isShown()) {
                ocultarVista(ln);
            } else
                mostarVista(ln);
        } else if (ln == lnContacto) {
            if (ln.isShown())
                ocultarVista(ln);
            else
                mostarVista(ln);
        } else if (ln == lnDomicilio) {
            if (ln.isShown())
                ocultarVista(ln);
            else
                mostarVista(ln);
        }
    }

    private void itemActual(LinearLayout lnClick) {
        boolean ok = false;
        if (lnActual == lnDatos) {
            if (datos)
                ok = true;
            else
                ok = validarDatosPersonales();
        } else if (lnActual == lnContacto) {
            if (contacto)
                ok = true;
            else
                ok = validarDatosContacto();
        } else if (lnActual == lnDomicilio) {
            if (domicilio)
                ok = true;
            else
                ok = validarDomicilio();
        }

        if (ok) {
            if (lnActual == lnClick) {
                seleccionarItem(lnClick);
            } else {
                ocultarVista(lnActual);
                mostarVista(lnClick);
            }
            lnActual = lnClick;
        } else {
            if (lnActual != lnClick)
                creaCuentaDelegate.getCreaCuentaView().alertGeneric("Error","Favor de informar todos los campos","Aceptar",true);
                //creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.alert_campos);
        }
    }

    private void validarContinuar() {
        if (activarBoton())
            alert();
        else
            creaCuentaDelegate.getCreaCuentaView().alertGeneric("Error","Favor de informar todos los campos","Aceptar",true);

            //creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.alert_campos);
    }

    @Override
    public void onClick(View v) {
        if (v == imbContinuar) {
            validarContinuar();
        } else if (imgDatos == v) {
            itemActual(lnDatos);
        } else if (imgContacto == v) {
            itemActual(lnContacto);
        } else if (imgDomicilio == v) {
            if (lnDomicilio.isShown())
                validarContinuar();
            else
                itemActual(lnDomicilio);
        } else if (v == edtFechaN) {
            showPicker();

        }else if(v==img_credencial){
            creaCuentaDelegate.getCreaCuentaView().alertDialogAyudaCredencial();
        }
    }

    private boolean validarDatosPersonales() {
       try {
           if (txtName.isShown()) {
               if (!edtCredencialE.getText().toString().equals("") && edtCredencialE.getText().toString().length() == 13) {
                   //if (edtCredencialE.getText().toString().length() == 13) {
                   identificateDelegate.setCredencial(edtCredencialE.getText().toString());
                   edtCredencialE.setBackground(getResources().getDrawable(R.drawable.an_textbox));
                   txtCredencialE.setTextColor(getResources().getColor(R.color.colorBlueDark));
                   datos = true;
                   return true;
               } else {
                   //edtCredencialE.setBackground(getResources().getDrawable(R.drawable.an_textbox_magenta));
                   //txtCredencialE.setTextColor(getResources().getColor(R.color.pink_dark));
                   datos = false;
                   return false;

               }

           } else if (identificateDelegate.validaDatos(edtNombre.getText().toString(), edtApaterno.getText().toString(), edtAmaterno.getText().toString(), edtFechaN.getText().toString(), edtCredencialE.getText().toString(), validaSexo(), spnLugarN.getSelectedItem().toString())) {
               //datos=true;
               return true;
           } else {
               datos = false;
               return false;
           }
       }catch(Exception ex){
           ex.printStackTrace();
           return false;
       }
    }

   /* private void lll(){

        Log.d("NOMBRE1", edtNombre.getText().toString());
        Log.d("NOMBRE2", edtApaterno.getText().toString());
        Log.d("NOMBRE3", edtAmaterno.getText().toString());
        Log.d("NOMBRE4", edtFechaN.getText().toString());
        Log.d("NOMBRE5", edtCredencialE.getText().toString());
        Log.d("NOMBRE6", spnLugarN.getSelectedItem().toString());
        Log.d("NOMBRE7", edtCredencialE.getText().toString());
        Log.d("NOMBRE8", edtCell.getText().toString());
        Log.d("NOMBRE9", edtEmail.getText().toString());



    }*/

    private String validaSexo() {
        if (cbFemenino.isChecked())
            return Constants.FEMENINO;
        else if (cbMasculino.isChecked())
            return Constants.MASCULINO;
        else
            return Constants.EMPTY_STRING;
    }

    private boolean validarDatosContacto() {
        boolean n = false;
        if (lnTelefono.isShown()) {
            if (identificateDelegate.validaContacto(edtCell.getText().toString(), edtEmail.getText().toString(), edtEmailConfirm.getText().toString(), spnCompany.getSelectedItem().toString()))
                n = true;

        } else {
            if (identificateDelegate.validaCorreo(edtEmailConfirm.getText().toString(), edtEmail.getText().toString()))
                n = true;
        }
        return n;
    }

    private boolean validarDomicilio() {
        if (identificateDelegate.validaDomicilio(edtCalle.getText().toString(), edtNumExt.getText().toString(), edtNumInt.getText().toString(), spnEstado.getSelectedItem().toString())) {
            return true;
        } else {
            return false;
        }
    }


    private void validaCurp() {
        if (APICuentaDigitalSharePreferences.getInstance().getBooleanData(Constants.OK_CONSULTA_CURP)) {
            txtApaterno.setVisibility(View.GONE);
            txtAmaterno.setVisibility(View.GONE);
            txtFechaN.setVisibility(View.GONE);
            txtLugarN.setVisibility(View.GONE);
            txtGenero.setVisibility(View.GONE);
            cbMasculino.setVisibility(View.GONE);
            cbFemenino.setVisibility(View.GONE);
            edtAmaterno.setVisibility(View.GONE);
            edtApaterno.setVisibility(View.GONE);
            edtNombre.setVisibility(View.GONE);
            edtFechaN.setVisibility(View.GONE);
            spnLugarN.setVisibility(View.GONE);

            txtName.setVisibility(View.VISIBLE);
            txtName.setText(Tools.getName());
        }
    }

    private void alert() {
        creaCuentaDelegate.getCreaCuentaView().alertDialog(sharePreferences.getStringData(Constants.CELULAR), sharePreferences.getStringData(Constants.COMPANIA), sharePreferences.getStringData(Constants.TAG_MAIL), this);
    }

    private void cargaDatosColonia() {
        identificateDelegate.realizaOperacion(txtCP.getText().toString(), spnEstado.getSelectedItem().toString());
    }

    public void desSpinner(){
        spnEstado.setEnabled(false);
    }
    public void generaOTP() {//JACT

        String com = "";
        String tel = "";

        if (sharePreferences.getStringData(Constants.TAG_COMPANIA) == null &&
                sharePreferences.getStringData(Constants.CELULAR) == null) {
            com = spnCompany.getSelectedItem().toString().toUpperCase();
            tel = edtCell.getText().toString();
        } else {
            com = sharePreferences.getStringData(Constants.TAG_COMPANIA);
            tel = sharePreferences.getStringData(Constants.CELULAR);
        }
        identificateDelegate.realizaOperacion(tel,
                com, edtEmail.getText().toString());
    }

    public void modificar() {
        lnTelefono.setVisibility(View.VISIBLE);
        edtCell.setText(sharePreferences.getStringData(Constants.CELULAR));
        findCompany();
        mostarVista(lnContacto);
        ocultarVista(lnDatos);
        ocultarVista(lnDomicilio);
        lnActual = lnContacto;
    }

    public void aceptar() {
        if (sharePreferences.getBooleanData(Constants.OK_CONSULTA_CURP)) {
            generaOTP();
        } else {
            identificateDelegate.realizaOperacion(edtNombre.getText().toString(),
                    edtApaterno.getText().toString(), edtAmaterno.getText().toString(), edtFechaN.getText().toString(),
                    spnLugarN.getSelectedItem().toString(), cbFemenino.isChecked() ? Constants.FEMENINO : Constants.MASCULINO);
        }
    }

    public void irSetColoniasEstado(String colonias) {

        Gson gson = new Gson();
        identificateDelegate = new IdentificateDelegate(creaCuentaDelegate, this);
        identificateDelegate.obtenerInformacionColonias(gson.fromJson(colonias, ConsultaColoniasNueva.class));

        txtMunicipios.setText(sharePreferences.getStringData(Constants.TAG_DELEGACION));

        loadColoniasNuew();
    }

    private void findCompany() {

        spnCompany.setSelection(Tools.getPositionCompany(sharePreferences.getStringData(Constants.COMPANIA)));
    }

    private void validarCeluar() {
        if (isVisible) {
            lnTelefono.setVisibility(View.GONE);
        } else
            lnTelefono.setVisibility(View.VISIBLE);
    }

    private void listenerDate() {
        edtNombre.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //validateDate(s.toString());
                if (s.length()>0) {
                    edtNombre.setBackgroundResource(R.drawable.an_textbox);
                    edtNombre.setTextColor(getResources().getColor(R.color.azulNew));

                }else{
                    edtNombre.setBackgroundResource(R.drawable.an_textbox_magenta);
                    edtNombre.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>0){
                    edtNombre.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        edtApaterno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //validateDate(s.toString());
                if (s.length() > 0) {
                    edtApaterno.setBackgroundResource(R.drawable.an_textboxcorto);
                    edtApaterno.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtApaterno.setBackgroundResource(R.drawable.an_textboxcorto_magenta);
                    edtApaterno.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtApaterno.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        edtAmaterno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //validateDate(s.toString());
                if (s.length() > 0) {
                    edtAmaterno.setBackgroundResource(R.drawable.an_textboxcorto);
                    edtAmaterno.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtAmaterno.setBackgroundResource(R.drawable.an_textboxcorto_magenta);
                    edtAmaterno.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtAmaterno.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        edtFechaN.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //validateDate(s.toString());
                if (s.length() > 0) {
                    edtFechaN.setBackgroundResource(R.drawable.an_textboxcorto);
                    edtFechaN.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtFechaN.setBackgroundResource(R.drawable.an_textboxcorto_magenta);
                    edtFechaN.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtFechaN.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });

        edtCredencialE.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //validateDate(s.toString());
                if (s.length() > 0) {
                    edtCredencialE.setBackgroundResource(R.drawable.an_textbox);
                    edtCredencialE.setTextColor(getResources().getColor(R.color.azulNew));

                } else {
                    edtCredencialE.setBackgroundResource(R.drawable.an_textbox_magenta);
                    edtCredencialE.setTextColor(getResources().getColor(R.color.pink_dark));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    edtCredencialE.setTextColor(getResources().getColor(R.color.black));
                }
            }
        });
    }

    public void validateDate(String date) {
        int lenght = date.length();
        switch (lenght) {
            case 3: {
                if (Character.isDigit(date.charAt(2))) {
                    edtFechaN.setText(Tools.putFirstSlash(date));
                }
                else {
                    edtFechaN.setText(Tools.removeFirstSlash(date));
                }
            }
            break;
            case 6: {
                if (Character.isDigit(date.charAt(5))) {
                    edtFechaN.setText(Tools.putSecondSlash(date));
                }
                else {
                    edtFechaN.setText(Tools.removeSecondSlash(date));
                }
            }
            break;
        }
        edtFechaN.setSelection(edtFechaN.getText().toString().length());
    }

    private boolean activarBoton() {
        boolean allData = false;
        if (lnContacto.isShown()) {
            if (validarDatosContacto() && datos && domicilio) {
                allData = true;
            }
        } else if (lnDatos.isShown()) {
            if (validarDatosPersonales() && contacto && domicilio) {
                allData = true;
            }
        } else if (lnDomicilio.isShown()) {
            if (validarDomicilio() && datos && contacto) {
                allData = true;
            }
        }
        return allData;
    }

    private void starThreadEnable() {
        new Thread() {
            @Override
            public void run() {
                while (!bntHabilitado) {
                    if (activarBoton()) {
                        Handler refresh = new Handler(Looper.getMainLooper());
                        refresh.post(new Runnable() {
                            public void run() {
                                imbContinuar.setImageResource(R.drawable.btn_siguiente);
                            }
                        });
                        bntHabilitado = true;
                        starThreadDisable();
                    }
                }
            }
        }.start();
    }


    private void starThreadDisable() {
        new Thread() {
            @Override
            public void run() {
                while (bntHabilitado) {
                    if (!activarBoton()) {
                        Handler refresh = new Handler(Looper.getMainLooper());
                        refresh.post(new Runnable() {
                            public void run() {
                                imbContinuar.setImageResource(R.drawable.btn_siguiente_in);
                            }
                        });
                        bntHabilitado = false;
                        starThreadEnable();
                    }
                }

            }
        }.start();
    }

   private void showPicker() {
        final int sdk = android.os.Build.VERSION.SDK_INT;
        final int menos18 = 18;
        /**
         * There's a problem when using "lollipop" with DatePickerDialog, so it's necessary to check which SDK level the device has.
         */

        if (sdk >= 21) {
            final View dialogView = View.inflate(getActivity(), R.layout.api_cuenta_digital_date_picker, null);
            final Calendar calendar = Calendar.getInstance();

            final DatePicker datePicker = (DatePicker) dialogView.findViewById(R.id.datePicker);


            if (year == 0) {
                datePicker.init(calendar.get(Calendar.YEAR) - menos18, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), null);

            }else{
                datePicker.init(year, month - 1, day, null);
            }



            datePicker.setDescendantFocusability(DatePicker.FOCUS_BLOCK_DESCENDANTS);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

            alertDialogBuilder.setPositiveButton(getResources().getString(R.string.bmovil_popup_button_accept), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int arg1) {

                    year = datePicker.getYear();
                    month = datePicker.getMonth() + 1;
                    day = datePicker.getDayOfMonth();
                    setDate();
                    dialog.dismiss();
                }
            });

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.setView(dialogView);
            alertDialog.show();


        } else {
            final DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0, int y, int m, int d) {
                    year = y;
                    month = m + 1;
                    day = d;
                    setDate();
                }
            };


            if (year == 0) {
                final Calendar calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR)-menos18;
                month = calendar.get(Calendar.MONTH);
                day = calendar.get(Calendar.DAY_OF_MONTH);
                new DatePickerDialog(getActivity(), pickerListener, year, month, day).show();

            } else {
                new DatePickerDialog(getActivity(), pickerListener, year, month - 1, day).show();

            }

        }
    }

    private void setDate() {
        if (day < 10 && month < 10)
            edtFechaN.setText(new StringBuilder().append("0" + day).append("/").append("0" + month).append("/").append(year));
        else if (month < 10)
            edtFechaN.setText(new StringBuilder().append(day).append("/").append("0" + month).append("/").append(year));
        else if (day < 10)
            edtFechaN.setText(new StringBuilder().append("0" + day).append("/").append(month).append("/").append(year));
        else
            edtFechaN.setText(new StringBuilder().append(day).append("/").append(month).append("/").append(year));


        }

    }


