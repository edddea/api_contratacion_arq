package com.bbva.apicuentadigital.io;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.bbva.apicuentadigital.common.Constants;

public class ParserJSON {

    /**
     * Response status successful.
     */
    public static final String STATUS_OK = "200";

    /**
     * Response status warning.
     */
    public static final String STATUS_WARNING = "AVISO";

    /**
     * Response status error.
     */
    public static final String STATUS_ERROR = "ERROR";

    /**
     * Response optional application update.
     */
    public static final String STATUS_OPTIONAL_UPDATE = "AC";

    /**
     * Tag for status.
     */
    private static final String STATUS_TAG = "status";

    /**
     * Tag for error code.
     */
    private static final String CODE_TAG = "code";

    /**
     * Tag for error message.
     */
    private static final String MESSAGE_TAG = "description";

    /**
     * Tag for application mandatory update URL.
     */
    private static final String URL_TAG = "UR";

    /**
     * Response reader.
     */
    private String reader;

    /**
     * Default constructor.
     * @param rder reader to read the response from
     */

    public ParserJSON(String rder) {
        this.reader = rder;
    }
    
    /**
     * Parse the operation result from received message.
     * @return the operation result
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public Result parseResult() throws IOException, ParsingException {
        Result result = null;
        android.util.Log.e("dora", "respuestaJSON " + reader);
        JSONObject status = null;
        try {
            JSONObject response = parserNextObject(Constants.TAG_RESPONSE);
            if(response==null){
                Log.e("MAPE","SI ES NULL---");
                result= new Result("","Inténtelo Nuevamente");
            }else {
                try {
                    status = response.getJSONObject(STATUS_TAG);
                    String code = status.getString(CODE_TAG);
                    String message = status.getString(MESSAGE_TAG);

                    if (STATUS_OK.equalsIgnoreCase(code)) {
                        result = new Result(code, null);
                    } else
                        result = new Result(code, message);

                } catch (JSONException e) {
                    try {
                        status = parserNextObject(STATUS_TAG);
                        String code = status.getString(CODE_TAG);
                        String message = status.getString(MESSAGE_TAG);

                        if (STATUS_OK.equalsIgnoreCase(code)) {
                            result = new Result(code, null);
                        } else
                            result = new Result(code, message);
                    } catch (JSONException i) {
                        // i.printStackTrace();
                        try {
                            JSONObject jsonObject = new JSONObject(this.reader);
                            String code = jsonObject.getString("errorCode");
                            String message = jsonObject.getString(MESSAGE_TAG);

                            if (STATUS_OK.equalsIgnoreCase(code)) {
                                result = new Result(code, null);
                            } else
                                result = new Result(code, message);
                        } catch (JSONException o) {
                            o.printStackTrace();
                        }
                    }
                }
            }
        }catch (Exception l) {
            Log.e("MAPE","Error no json---");
            result= new Result("","Inténtelo Nuevamente");
            l.printStackTrace();
        }
        return result;
    }

    /**
     * Obtain the next value for a tag.
     * @param tag the target tag
     * @return the value
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public String parseNextValue(String tag) throws IOException, ParsingException {
        return parseNextValue(tag, true);
    }

    /**
     * Obtain the next value for a tag.
     * @param tag the target tag
     * @param mandatory indicates the tag is mandatory
     * @return the value
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public String parseNextValue(String tag, boolean mandatory) throws ParsingException, IOException {
        String value = null;
		try {

            if(reader.contains("<html>")) {

                String div = "formatoHTML";

                String[] divideCadena = reader.split(div);

                String cadenaHtml = divideCadena[1];

                cadenaHtml = cadenaHtml.replace('\"', '\'');

                cadenaHtml = cadenaHtml.substring(3, cadenaHtml.length() - 2);

                reader = divideCadena[0] + div + "\":\"" + cadenaHtml + "\"}";
            }

			JSONObject jsonObject = new JSONObject(this.reader);
			value = jsonObject.getString(tag);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (mandatory) {
			if (value == null) {
				throw new ParsingException(new StringBuffer("Tag ").append(tag)
						.append(" not found in ").append(tag).toString());
			} else {
				return value;
			}
		} else {
			return value;
		}
	}

    public JSONArray parseNextValueWithArray(String tag) throws ParsingException, IOException {
    	return parseNextValueWithArray(tag, true);
    }
    
    public JSONArray parseNextValueWithArray(String tag, boolean mandatory) throws ParsingException, IOException {
        JSONArray array = new JSONArray();
		try {
			JSONObject jsonObject = new JSONObject(this.reader);
			array = jsonObject.getJSONArray(tag);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if (mandatory) {
			if (array == null) {
				throw new ParsingException(new StringBuffer("Tag ").append(tag)
						.append(" not found in ").append(tag).toString());
			} else {
				return array;
			}
		} else {
			return array;
		}
	}
    
    public JSONObject parserNextObject(String tag) throws ParsingException, IOException {
    	return parserNextObject(tag, true);
    }
    
    public JSONObject parserNextObject(String tag, boolean mandatory) throws ParsingException, IOException {
    	JSONObject result = new JSONObject();
		try {
			JSONObject jsonObject = new JSONObject(this.reader);
			result = jsonObject.getJSONObject(tag);
            Log.e("Result","parserNext"+tag);
		} catch (JSONException e) {
            android.util.Log.e("dora", "error tag");
			Log.e(this.getClass().getName(), "Error al obtener el elemento " + tag + " del JSON.", e);
            result=null;
            return result;
		}
		if (mandatory) {
			if (result == null) {
				throw new ParsingException(new StringBuffer("Tag ").append(tag).append(" not found in ").append(tag).toString());
			} else {
				return result;
			}
		} else {
			return result;
		}
    }

    public boolean hasValue(String tag) throws ParsingException, IOException {

        try {
            JSONObject jsonObject = new JSONObject(this.reader);
            return jsonObject.has(tag);
        } catch (JSONException e) {
            if(ConstantsServer.ALLOW_LOG) e.printStackTrace();
        }
        return false;
    }


}
