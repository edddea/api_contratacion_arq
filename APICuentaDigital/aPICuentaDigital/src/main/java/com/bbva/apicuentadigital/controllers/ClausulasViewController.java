package com.bbva.apicuentadigital.controllers;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.mbanking.BanderasBMovilFileManager;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.delegates.ClausulasDelegate;
import com.bbva.apicuentadigital.delegates.CreaCuentaDelegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelperCuentaDigital;

/**
 * Created by Karina on 07/12/2015.
 */
public class ClausulasViewController extends Fragment implements View.OnClickListener{

    private View rootView;

    private CheckBox cbTerms;
   // private CheckBox cbTermsTwo;
    private CheckBox cbTermsThree;
    private CheckBox cbTermsFour;

    private EditText edtPass;
    private EditText edtPassConfirm;

    private ImageView imgPass;
    private ImageView imgPassConfirm;

    private ImageButton imbContinue;

    private TextView txtMessage;
    private TextView lbl_accept_terms;

    private GuiTools guiTools;
    private CreaCuentaDelegate creaCuentaDelegate;
    private ClausulasDelegate clausulasDelegate;

    private boolean okPass;
    private boolean okPassConfirm;

    public ClausulasViewController(GuiTools guiTools, CreaCuentaDelegate creaCuentaDelegate){
        this.guiTools = guiTools;
        this.creaCuentaDelegate = creaCuentaDelegate;
        clausulasDelegate = new ClausulasDelegate(this,creaCuentaDelegate);
        okPass = false;
        okPassConfirm = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_clausulas, container, false);

        init();
        return rootView;
    }

    private void init(){
        findViewById();
        clickListener();
        scaleView();
        colorBMovil();
        etiquetado();
    }

    private void etiquetado(){
        ArrayList<String> list= new ArrayList<String>();
        list.add(Constants.QUIERO_CUENTA);
        list.add(Constants.CREA_CUENTA);
        list.add(Constants.CLAUSULAS_CUENTA);
        TrackingHelperCuentaDigital.trackState(list);
        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackingHelperCuentaDigital.trackEntrada(eventoEntrada);
    }

    private void findViewById(){
        cbTerms = (CheckBox)rootView.findViewById(R.id.cb_terms);
        //cbTermsTwo = (CheckBox)rootView.findViewById(R.id.cb_terms_two);
        cbTermsThree = (CheckBox)rootView.findViewById(R.id.cb_terms_three);
        cbTermsFour = (CheckBox)rootView.findViewById(R.id.cb_terms_four);

        edtPass = (EditText)rootView.findViewById(R.id.edt_pass);
        edtPassConfirm = (EditText)rootView.findViewById(R.id.edt_pass_confirm);

       // imgPass = (ImageView)rootView.findViewById(R.id.img_pass);
        //imgPassConfirm = (ImageView)rootView.findViewById(R.id.img_pass_confirm);

        imbContinue = (ImageButton)rootView.findViewById(R.id.imb_continue);

        txtMessage = (TextView)rootView.findViewById(R.id.lbl_message);
        lbl_accept_terms = (TextView)rootView.findViewById(R.id.lbl_accept_terms);
    }

    private void clickListener(){
        cbTerms.setOnClickListener(this);
        //cbTermsTwo.setOnClickListener(this);
        cbTermsThree.setOnClickListener(this);
        cbTermsFour.setOnClickListener(this);

        imbContinue.setOnClickListener(this);

        lbl_accept_terms.setOnClickListener(this);

        listenerPass();
        listenerPassConfirm();
    }

    private void listenerPass(){
        edtPass.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 6) {
                    if (clausulasDelegate.validatePasswordPolicy1(edtPass.getText().toString().trim())) {
                        if (clausulasDelegate.validatePasswordPolicy2(edtPass.getText().toString().trim())) {
                            //imgPass.setImageResource(R.drawable.an_ic_feliz);
                            okPass = true;
                            if (edtPass.getText().toString().equals(edtPassConfirm.getText().toString())) {
                                //imgPassConfirm.setImageResource(R.drawable.an_ic_feliz);
                                okPassConfirm = true;
                                imbContinue.setBackgroundResource(R.drawable.an_btn_confirmar);
                            } else {
                                //imgPassConfirm.setImageResource(R.drawable.an_ic_triste);
                                okPassConfirm = false;
                                imbContinue.setBackgroundResource(R.drawable.an_btn_deshabilitado);
                            }
                        } else {
                            creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.contrata_message_pass_error_numeros_repetidos);
                        }
                    } else {
                        creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.contrata_message_pass_error_numeros_consecutivos);
                    }
                } else {
                    if (okPass) {
                       // imgPass.setImageResource(R.drawable.an_ic_triste);
                        //imgPassConfirm.setImageResource(R.drawable.an_ic_triste);
                        okPass = false;
                        imbContinue.setBackgroundResource(R.drawable.an_btn_deshabilitado);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void listenerPassConfirm(){
        edtPassConfirm.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 6) {
                    if (edtPass.getText().toString().equals(edtPassConfirm.getText().toString())) {
                       // imgPassConfirm.setImageResource(R.drawable.an_ic_feliz);
                        okPassConfirm = true;
                        if (cbTermsFour.isChecked()){

                        }
                            imbContinue.setBackgroundResource(R.drawable.an_btn_confirmar);
                    }
                } else {
                    if (okPassConfirm){

                    }
                       // imgPassConfirm.setImageResource(R.drawable.an_ic_triste);
                    okPassConfirm = false;
                    imbContinue.setBackgroundResource(R.drawable.an_btn_deshabilitado);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void scaleView(){
       // guiTools.scale(rootView.findViewById(R.id.lbl_terms), true);
        //guiTools.scale(rootView.findViewById(R.id.lbl_accept_terms), true);
        //guiTools.scale(rootView.findViewById(R.id.lbl_term_two),true);
        guiTools.scale(rootView.findViewById(R.id.lbl_term_three), true);
        guiTools.scale(rootView.findViewById(R.id.lbl_term_four), true);
        guiTools.scale(txtMessage, true);
        guiTools.scale(rootView.findViewById(R.id.txt_contrata), true);
        guiTools.scale(edtPass);
        guiTools.scale(edtPassConfirm);
        guiTools.scale(lbl_accept_terms);
    }


    private void colorBMovil(){
        String message = txtMessage.getText().toString();
        String text [] = message.split("Bancomer móvil");
        txtMessage.setText(Html.fromHtml("<font>" + text[0] + "</font><b><font color='" + getResources().getColor(R.color.black) + "'>Bancomer móvil</font></b><font>" + text[1] + "</font>"), TextView.BufferType.SPANNABLE);
    }

    @Override
    public void onClick(View v) {
        if(v == imbContinue){
            validatePass();
        }/*else if(cbTermsTwo == v){
            if(!cbTerms.isChecked()){
                cbTermsTwo.setChecked(false);
                creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.contrata_alerta_no_terminos);
            }else
                cbTermsTwo.setChecked(true);
        }*/else if (cbTermsThree == v){
            if(!cbTerms.isChecked()){
                cbTermsThree.setChecked(false);
                creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.contrata_alerta_no_datos);
            }else
                cbTermsThree.setChecked(true);
        }else if(cbTermsFour == v){
            if(!cbTermsThree.isChecked()){
                cbTermsFour.setChecked(false);
                creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.contrata_alerta_no_vincular);
            }else {
                cbTermsFour.setChecked(true);
                if(okPass && okPassConfirm)
                    imbContinue.setBackgroundResource(R.drawable.an_btn_confirmar);
            }
        }else if(lbl_accept_terms == v){
            //clausulasDelegate.realizaOperacion();
            clausulasDelegate.showTerminos("");
        }else if (cbTerms == v){
            if(cbTerms.isChecked()){
                cbTerms.setChecked(true);
            }else
                cbTerms.setChecked(true);
        }
    }

    private void validatePass(){
        if(cbTerms.isChecked()/* && cbTermsTwo.isChecked() */&& cbTermsThree.isChecked() && cbTermsFour.isChecked()) {
            if(okPass){
                if(okPassConfirm){
                    clausulasDelegate.showConfirmacion();
                    /*BanderasBMovilFileManager fileManager = BanderasBMovilFileManager.getCurrent();
                    fileManager.setBanderasContratarCDigitat(true);*/
                }else if(edtPassConfirm.getText().toString().equals("")){
                    creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.contrasena_confirmacion_vacia);
                }else if(!edtPassConfirm.getText().toString().equals(edtPass.getText().toString())){
                    creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.contrasena_no_coincide);
                }

            }else if(edtPass.getText().toString().equals("") || edtPass.getText().toString() == null){
                creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.contrasena_vacia);
            }else if(edtPass.getText().toString().length() < 6){
                creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.contrasena_menor);
            }
        }else
            creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.contrata_alerta_clausulas);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        setUserVisibleHint(true);
    }



}
