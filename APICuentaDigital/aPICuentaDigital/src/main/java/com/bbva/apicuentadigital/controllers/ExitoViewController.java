package com.bbva.apicuentadigital.controllers;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.delegates.CreaCuentaDelegate;
import com.bbva.apicuentadigital.delegates.ExitoDelegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelperCuentaDigital;

/**
 * Created by Karina on 09/12/2015.
 */
public class ExitoViewController extends Fragment implements View.OnClickListener {

    private GuiTools guiTools;
    private CreaCuentaDelegate creaCuentaDelegate;
    private ExitoDelegate exitoDelegate;
    private View rootView;

    private ImageButton imbSalir;
    private TextView txtContrato;
    private TextView txtTitular;
    private TextView txtCuenta;
    private TextView txtTarjeta;
    private TextView txtClabe;
    private TextView txtServicios;
    private TextView txtCelular;
    private TextView txtEmail;

    private TextView lblTitular;
    private TextView lblCuenta;
    private TextView lblTarjeta;
    private TextView lblClabe;
    private TextView lblCelular;
    private TextView lblEmail;

    private boolean showSplash;

    public ExitoViewController(GuiTools guiTools, CreaCuentaDelegate creaCuentaDelegate){
        this.creaCuentaDelegate = creaCuentaDelegate;
        this.guiTools = guiTools;
        exitoDelegate = new ExitoDelegate(creaCuentaDelegate, this);
        showSplash = true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_exito, container, false);
        init();
        return rootView;
    }
    private void init(){
        findViewById();
        scaleView();
        exitoDelegate.obtenerDatos();
        exitoDelegate.dataKeyChain();
        etiquetado();
    }

    private void etiquetado(){
        ArrayList<String> list= new ArrayList<String>();
        list.add(Constants.QUIERO_CUENTA);
        list.add(Constants.CREA_CUENTA);
        list.add(Constants.CLAUSULAS_CUENTA);
        list.add(Constants.CONFIRMA_CUENTA);
        list.add(Constants.EXITO_CUENTA);
        TrackingHelperCuentaDigital.trackState(list);
        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackingHelperCuentaDigital.trackEntrada(eventoEntrada);
    }

    private void findViewById(){
        imbSalir = (ImageButton)rootView.findViewById(R.id.imb_salir);
        txtContrato = (TextView)rootView.findViewById(R.id.txt_contrato);
        txtCelular = (TextView)rootView.findViewById(R.id.txt_celular);
        txtClabe = (TextView)rootView.findViewById(R.id.txt_cuenta_clabe);
        txtContrato = (TextView)rootView.findViewById(R.id.txt_contrato);
        txtCuenta = (TextView)rootView.findViewById(R.id.txt_nombre_cuenta);
        txtEmail = (TextView)rootView.findViewById(R.id.txt_email);
        txtTitular = (TextView)rootView.findViewById(R.id.txt_nombre_titular);
        txtServicios = (TextView)rootView.findViewById(R.id.txt_servicios);
        txtTarjeta = (TextView)rootView.findViewById(R.id.txt_tarjeta);

        lblTitular = (TextView)rootView.findViewById(R.id.lbl_nombre_titular);
        lblCuenta = (TextView)rootView.findViewById(R.id.lbl_nombre_cuenta);
        lblTarjeta = (TextView)rootView.findViewById(R.id.lbl_tarjeta);
        lblClabe = (TextView)rootView.findViewById(R.id.lbl_cuenta_clabe);
        lblCelular = (TextView)rootView.findViewById(R.id.lbl_celular);
        lblEmail = (TextView)rootView.findViewById(R.id.lbl_email);

        imbSalir.setOnClickListener(this);
        txtContrato.setOnClickListener(this);
        setSelected();
    }



    private void setSelected(){
        txtTitular.setSelected(true);
        txtCuenta.setSelected(true);
        txtTarjeta.setSelected(true);
        txtClabe.setSelected(true);
        txtCelular.setSelected(true);
        txtEmail.setSelected(true);

        lblTitular.setSelected(true);
        lblCuenta.setSelected(true);
        lblTarjeta.setSelected(true);
        lblClabe.setSelected(true);
        lblCelular.setSelected(true);
        lblEmail.setSelected(true);
    }

    private void scaleView(){
        guiTools.scale(rootView.findViewById(R.id.txt_tu_cuenta), true);
        guiTools.scale(lblTitular, true);
        guiTools.scale(txtTitular, true);
        guiTools.scale(lblCuenta, true);
        guiTools.scale(txtCuenta, true);
        guiTools.scale(lblTarjeta, true);
        guiTools.scale(txtTarjeta, true);
        guiTools.scale(lblClabe, true);
        guiTools.scale(txtClabe, true);
        guiTools.scale(rootView.findViewById(R.id.lbl_servicios), true);
        guiTools.scale(txtServicios, true);
        guiTools.scale(lblCelular, true);
        guiTools.scale(txtCelular, true);
        guiTools.scale(lblEmail, true);
        guiTools.scale(txtEmail, true);
        guiTools.scale(txtContrato, true);
        guiTools.scale(rootView.findViewById(R.id.txt_exito), true);
    }

    @Override
    public void onResume() {
        super.onResume();
        /*if(showSplash) {
            creaCuentaDelegate.getCreaCuentaView().showSplas();
            showSplash = false;
        }*/
    }

    private void showPopUp(){
        android.app.FragmentManager fm = getFragmentManager();
        PopUpExitoViewController dialog = new PopUpExitoViewController(guiTools, this);
        dialog.setCancelable(false);
        dialog.show(fm, "Result");
    }

    @Override
    public void onClick(View v) {
        /*if(v == imbSalir)
            showPopUp();
        else */if(v == txtContrato){
           // exitoDelegate.realizaOperacion();
            exitoDelegate.showTerminos("");
        }
    }

    public void mostarDatos(String titular, String cuenta, String tarjeta, String clabe, String celular, String email){
        txtTitular.setText(titular);
        txtCuenta.setText(cuenta);
        txtTarjeta.setText(tarjeta);
        txtClabe.setText(clabe);
        txtCelular.setText(celular);
        txtEmail.setText(email);
    }

    public void finish(){
        getActivity().finish();
    }
}
