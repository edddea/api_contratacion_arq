package com.bbva.apicuentadigital.delegates;

import android.content.Intent;
import android.util.Log;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.controllers.ClausulasViewController;
import com.bbva.apicuentadigital.controllers.WebViewController;
import com.bbva.apicuentadigital.io.ConstantsServer;
import com.bbva.apicuentadigital.io.Server;
import com.bbva.apicuentadigital.io.ServerResponse;
import com.bbva.apicuentadigital.models.ContratoHTML;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

import java.util.Hashtable;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;

/**
 * Created by Karina on 07/12/2015.
 */
public class ClausulasDelegate extends BaseDelegate {

    private ClausulasViewController clausulasViewController;
    private CreaCuentaDelegate creaCuentaDelegate;

    public ClausulasDelegate(ClausulasViewController clausulasViewController, CreaCuentaDelegate creaCuentaDelegate){
        this.clausulasViewController = clausulasViewController;
        this.creaCuentaDelegate = creaCuentaDelegate;
    }

    /**
     * Validates the new password according to the following policy: a password
     * cannot have more than 2 repeated digits together, i.e: 123338 would be an
     * invalid password
     *
     * @param pwd
     *            the password to validate
     * @return true if the password passes the policy , false if it fails
     */
    public boolean validatePasswordPolicy1(String pwd) {
        if (pwd == null) {
            return false;
        }
        boolean validate = true;
        for (int i = 0; ((i < pwd.length()) && (validate)); i++) {
            if (i > 1) {
                char c0 = pwd.charAt(i - 2);
                char c1 = pwd.charAt(i - 1);
                char c2 = pwd.charAt(i);
                boolean fails = ((c0 == c1) && (c1 == c2));
                validate = !fails;
            }
        }
        return validate;
    }

    /**
     * Validates the new password according to the following policy: a password
     * cannot have more than 2 digits consecutive, either ascending or
     * descending. i.e., 12348 or 43276 would fail.
     *
     * @param pwd
     *            the password to validate
     * @return true if the password passes the policy , false if it fails
     */
    public boolean validatePasswordPolicy2(String pwd) {
        if (pwd == null) {
            return false;
        }
        boolean validate = true;
        for (int i = 0; ((i < pwd.length()) && (validate)); i++) {
            if (i > 1) {
                char c0 = pwd.charAt(i - 2);
                char c1 = pwd.charAt(i - 1);
                char c2 = pwd.charAt(i);
                boolean fails = (((c2 == c1 + 1) && (c1 == c0 + 1)) // ascending
                        // order
                        || ((c2 == c1 - 1) && (c1 == c0 - 1)) // descending order
                );
                validate = !fails;
            }
        }
        return validate;
    }

    public void showConfirmacion(){
        creaCuentaDelegate.getCreaCuentaView().confirmacion();
    }

    public void realizaOperacion(){
        creaCuentaDelegate.getCreaCuentaView().showActivityIndicator();
        APICuentaDigitalSharePreferences sharePreferences = APICuentaDigitalSharePreferences.getInstance();
        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(Constants.TAG_NUMUERO_CELULAR,sharePreferences.getStringData(Constants.CELULAR));
        params.put(Constants.TAG_ID_PRODUCTO,"PBXXXXXX01");
        params.put(Constants.TAG_IND_VACIO,Constants.TAG_TRUE);
        params.put(Constants.TAG_CONTRATO,"");

        Server server = Server.getInstance(this);
        server.doNetWorkOperation(ApiConstants.CONSULTA_CONTRATO, params);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        creaCuentaDelegate.getCreaCuentaView().hideActivityIndicator();
        if(response.getCODE().equalsIgnoreCase(response.CODE_OK)) {
            ContratoHTML contratoHTML = (ContratoHTML)response.getResponse();
            showTerminos(contratoHTML.getFormatoHTML());
        }
    }

    public void showTerminos(String url){
        Intent intent = new Intent(clausulasViewController.getActivity(), WebViewController.class);
        intent.putExtra(Constants.OPERATION, Constants.TERMINOS);
        intent.putExtra(Constants.URL, url);
        clausulasViewController.getActivity().startActivity(intent);
    }

}
