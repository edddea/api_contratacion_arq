package com.bbva.apicuentadigital.models;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.io.ParserJSON;
import com.bbva.apicuentadigital.io.ParsingException;
import com.bbva.apicuentadigital.io.ParsingHandler;

import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Karina on 08/12/2015.
 */
public class ValidaOTP implements ParsingHandler {

    private String titular;
    private String tarjeta;
    private String cuenta;
    private String cuentaClabe;

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getCuentaClabe() {
        return cuentaClabe;
    }

    public void setCuentaClabe(String cuentaClabe) {
        this.cuentaClabe = cuentaClabe;
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        if(parser.hasValue(Constants.TAG_RESPONSE))
            parser = new ParserJSON(parser.parserNextObject(Constants.TAG_RESPONSE).toString());

        if(parser.hasValue(Constants.TAG_TITULAR))
            titular = parser.parseNextValue(Constants.TAG_TITULAR);

        if(parser.hasValue(Constants.TAG_TARJETA))
            tarjeta = parser.parseNextValue(Constants.TAG_TARJETA);

        if(parser.hasValue(Constants.TAG_CUENTA))
            cuenta = parser.parseNextValue(Constants.TAG_CUENTA);

        if(parser.hasValue(Constants.TAG_CUENTA_CLABE))
            cuentaClabe = parser.parseNextValue(Constants.TAG_CUENTA_CLABE);

    }
}
