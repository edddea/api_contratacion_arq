package com.bbva.apicuentadigital.delegates;

import android.content.Intent;
import android.util.Log;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.Tools;
import com.bbva.apicuentadigital.controllers.CreaCuentaViewController;
import com.bbva.apicuentadigital.controllers.QuieroCuentaViewController;
import com.bbva.apicuentadigital.models.ConsultaColonias;
import com.bbva.apicuentadigital.models.ConsultaColoniasCurp;
import com.bbva.apicuentadigital.models.ConsultaColoniasNueva;
import com.bbva.apicuentadigital.models.ConsultaCurp;
import com.bbva.apicuentadigital.models.ValidaCurp;
import com.bbva.apicuentadigital.models.ValidaOTP;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

/**
 * Created by Karina on 08/12/2015.
 */
public class CreaCuentaDelegate {

    private APICuentaDigitalSharePreferences sharePreferences;

    private CreaCuentaViewController creaCuentaView;
    private ConsultaColoniasCurp consultaColoniasCurp;
    private ValidaOTP validaOTP;
    private String[] colonias;
    private String cveEntidadNac;

    public CreaCuentaDelegate(CreaCuentaViewController creaCuentaView, String cp, String compania, String celular){
        this.creaCuentaView = creaCuentaView;
        colonias = new String[0];
        sharePreferences = APICuentaDigitalSharePreferences.getInstance();
        sharePreferences.setStringData(Constants.CELULAR, celular);
        sharePreferences.setStringData(Constants.COMPANIA, compania);
        sharePreferences.setStringData(Constants.TAG_CODIGO_POSTAL, cp);
    }

    public CreaCuentaViewController getCreaCuentaView() {
        return creaCuentaView;
    }

    public ValidaOTP getValidaOTP() {
        return validaOTP;
    }

    public void setValidaOTP(ValidaOTP validaOTP) {
        this.validaOTP = validaOTP;
    }

    public String[] getColonias() {
        return colonias;
    }

    public void obtenerInformacion(ConsultaColoniasCurp consultaColoniasCurp){
        this.consultaColoniasCurp = consultaColoniasCurp;

        ConsultaCurp consultaCurp = null;
            consultaCurp = consultaColoniasCurp.getConsultaCurp();
                if(consultaCurp.getCode().equalsIgnoreCase(Constants.CODE_OK)){
                    cveEntidadNac = consultaCurp.getCveEntidadNac();
                    sharePreferences.setBooleanData(Constants.OK_CONSULTA_CURP,true);
                    sharePreferences.setStringData(Constants.TAG_NOMBRE, consultaCurp.getNombres());
                    sharePreferences.setStringData(Constants.TAG_AMATERNO, consultaCurp.getaMaterno());
                    sharePreferences.setStringData(Constants.TAG_APATERNO, consultaCurp.getaPaterno());
                    sharePreferences.setStringData(Constants.TAG_FECHANAC, consultaCurp.getFechNac());
                    sharePreferences.setStringData(Constants.TAG_SEXO, consultaCurp.getSexo());
                    sharePreferences.setStringData(Constants.TAG_CVEENTIDADNAC, cveEntidadNac);
                    sharePreferences.setStringData(Constants.TAG_LUGARN, Tools.getEstado(cveEntidadNac));
                    sharePreferences.setStringData(Constants.TAG_CURP, consultaCurp.getCurp());
            }else {
                sharePreferences.setBooleanData(Constants.OK_CONSULTA_CURP,false);
            }
    }

    public void quieroCuenta(){
        Intent intent = new Intent(creaCuentaView, QuieroCuentaViewController.class);
        intent.putExtra(Constants.CELULAR, sharePreferences.getStringData(Constants.CELULAR));
        intent.putExtra(Constants.COMPANIA, sharePreferences.getStringData(Constants.COMPANIA));

        creaCuentaView.startActivity(intent);
        creaCuentaView.finish();
    }

}
