package com.bbva.apicuentadigital.controllers;

import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.bbva.apicuentadigital.R;

import java.util.Timer;
import java.util.TimerTask;

import bancomer.api.common.timer.TimerController;


/**
 * Created by Karina on 09/12/2015.
 */
public class SplasViewController extends DialogFragment{

    private Timer resultTimer;
    private TimerTask resultTimerTask;
    private View rootView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_splash_exito, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCancelable(false);
        fullScreen();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        System.gc();
            resultTimer = new Timer();
            resultTimerTask = new ResultTask();
            resultTimer.schedule(resultTimerTask, 2000);

    }

    private class ResultTask extends TimerTask {

        @Override
        public void run() {
            Message msg = new Message();
            mApplicationHandler.sendMessage(msg);
        }
    }

    private Handler mApplicationHandler = new Handler() {
        public void handleMessage(Message msg) {
            dismiss();
        }
    };

    private void fullScreen(){
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setAttributes(wlp);
    }

}
