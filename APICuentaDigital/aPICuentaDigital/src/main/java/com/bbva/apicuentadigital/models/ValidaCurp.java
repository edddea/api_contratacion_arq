package com.bbva.apicuentadigital.models;

import com.bbva.apicuentadigital.io.ServerResponse;

/**
 * Created by Karina on 07/12/2015.
 */
public class ValidaCurp {

    private String code;
    private String description;
    private String encontroCURP;
    private String curp;
    private String curpStatusB;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEncontroCURP() {
        return encontroCURP;
    }

    public void setEncontroCURP(String encontroCURP) {
        this.encontroCURP = encontroCURP;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getCurpStatusB() {
        return curpStatusB;
    }

    public void setCurpStatusB(String curpStatusB) {
        this.curpStatusB = curpStatusB;
    }
}
