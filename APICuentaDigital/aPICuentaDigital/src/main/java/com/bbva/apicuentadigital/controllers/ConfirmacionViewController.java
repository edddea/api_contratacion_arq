package com.bbva.apicuentadigital.controllers;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.delegates.ConfirmacionDelegate;
import com.bbva.apicuentadigital.delegates.CreaCuentaDelegate;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelperCuentaDigital;

/**
 * Created by Karina on 09/12/2015.
 */
public class ConfirmacionViewController extends Fragment implements View.OnClickListener{

    private View rootView;
    private GuiTools guiTools;
    private CreaCuentaDelegate creaCuentaDelegate;
    private ConfirmacionDelegate confirmacionDelegate;
    //private EditText edtSMS;
    private EditText edtMail;
    private TextView txtNoCode;

    private ImageButton imbConfirm;
    private APICuentaDigitalSharePreferences sharePreferences;

    public ConfirmacionViewController(GuiTools guiTools, CreaCuentaDelegate creaCuentaDelegate){
        this.guiTools = guiTools;
        this.creaCuentaDelegate = creaCuentaDelegate;
        this.confirmacionDelegate = new ConfirmacionDelegate(creaCuentaDelegate);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_confirmacion, container, false);
        sharePreferences = APICuentaDigitalSharePreferences.getInstance();
        sharePreferences.setBooleanData(Constants.CONFIRMA,true);
        init();
        return rootView;


    }

    private void init(){
        findViewById();
        scaleView();
        etiquetado();
        listenerCode();

    }

    private void etiquetado(){
        ArrayList<String> list= new ArrayList<String>();
        list.add(Constants.QUIERO_CUENTA);
        list.add(Constants.CREA_CUENTA);
        list.add(Constants.CLAUSULAS_CUENTA);
        list.add(Constants.CONFIRMA_CUENTA);
        TrackingHelperCuentaDigital.trackState(list);
        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackingHelperCuentaDigital.trackEntrada(eventoEntrada);

    }

    private void findViewById(){
        imbConfirm = (ImageButton)rootView.findViewById(R.id.imb_confirm);
        edtMail = (EditText)rootView.findViewById(R.id.edt_code_mail);
       // edtSMS = (EditText)rootView.findViewById(R.id.edt_code_sms);
        txtNoCode = (TextView)rootView.findViewById(R.id.lbl_no_code);

        txtNoCode.setOnClickListener(this);
        imbConfirm.setOnClickListener(this);
    }

    private void scaleView(){
        guiTools.scale(edtMail);
        guiTools.scale(txtNoCode);
        guiTools.scale(rootView.findViewById(R.id.lblsub), true);
        guiTools.scale(rootView.findViewById(R.id.lblingresar), true);
       // guiTools.scale(edtSMS);
    }

    @Override
    public void onClick(View v) {
        if(v == imbConfirm){
            //if(edtSMS.getText().length() ==4 && edtMail.getText().length() == 4)
            if(edtMail.getText().length() == 8) {
                //confirmacionDelegate.realizaOperacion(edtSMS.getText().toString(), edtMail.getText().toString());
                confirmacionDelegate.realizaOperacion(edtMail.getText().toString(), "");
            }
        }else if(v == txtNoCode) {
            creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.confirmacion_message_no_code);
            confirmacionDelegate.generaOTP();
        }
    }

    private void listenerCode(){
        edtMail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 8) {
                    edtMail.setBackgroundResource(R.drawable.an_textboxcodigo_2);
                    edtMail.setTextColor(getResources().getColor(R.color.azulNew));
                }else{
                    edtMail.setBackgroundResource(R.drawable.an_textboxcodigo_1);
                    edtMail.setTextColor(getResources().getColor(R.color.azulNew));
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        setUserVisibleHint(true);
    }
}
