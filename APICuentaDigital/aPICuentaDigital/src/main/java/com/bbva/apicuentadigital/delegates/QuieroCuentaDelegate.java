package com.bbva.apicuentadigital.delegates;

import android.content.Intent;
import android.util.Log;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.Tools;
import com.bbva.apicuentadigital.controllers.CreaCuentaViewController;
import com.bbva.apicuentadigital.controllers.QuieroCuentaViewController;
import com.bbva.apicuentadigital.io.ConstantsServer;
import com.bbva.apicuentadigital.io.Server;
import com.bbva.apicuentadigital.io.ServerResponse;
import com.bbva.apicuentadigital.models.ConsultaColonias;
import com.bbva.apicuentadigital.models.ConsultaColoniasCurp;
import com.google.gson.Gson;

import java.util.Hashtable;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;

/**
 * Created by Karina on 06/12/2015.
 */
public class QuieroCuentaDelegate extends BaseDelegate{

    private QuieroCuentaViewController quieroCuentaView;
    private String celular;
    private String compania;

    private String cp;

    public QuieroCuentaDelegate(QuieroCuentaViewController quieroCuentaView, String celular, String compania){
        this.quieroCuentaView = quieroCuentaView;
        this.celular = celular;
        this.compania = compania;
    }

    public boolean validaCurp(String curp){
        curp=curp.toUpperCase().trim();

        return curp.matches("[A-Z]{4}[0-9]{6}[H,M][A-Z]{5}[0-9]{2}");

    }

    public boolean validaCP(String cp){
        if(cp == null) {
            return false;
        }else if(cp.length() == 5){
            return true;
        }else{
            return false;
        }
    }

    public void realizaOperacion(String curp, String cp){

        //quieroCuentaView.showActivityIndicator(quieroCuentaView.getResources().getString(R.string.alert_operation),
          //      quieroCuentaView.getResources().getString(R.string.alert_connecting),quieroCuentaView );

        quieroCuentaView.showActivityIndicator();

        String idRenapo =  "FALSE";

        if(!curp.equalsIgnoreCase(""))
            idRenapo = "CURP";

        this.cp = cp;

        if (idRenapo.equalsIgnoreCase("CURP")){
            Hashtable<String, String> params = new Hashtable<String, String>();
            params.put(Constants.TAG_IND_CP,"TRUE");
            params.put(Constants.TAG_IND_RENAPO, idRenapo);
            params.put(Constants.TAG_CODIGO_POSTAL,cp);
            params.put(Constants.TAG_CURP_,curp);
            params.put(Constants.TAG_NOMBRES, "");
            params.put(Constants.TAG_APATERNO,"");
            params.put(Constants.TAG_AMATERNO,"");
            params.put(Constants.TAG_SEXO,"");
            params.put(Constants.TAG_FECHANAC,"");
            params.put(Constants.TAG_CVEENTIDADNAC,"");

            Server server = Server.getInstance(this);
            server.doNetWorkOperation(ApiConstants.CONSULTA_CURP, params);
        }else {
            showCrearCuentaCP();
        }


    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        quieroCuentaView.hideActivityIndicator();

        if(response.getCODE().equalsIgnoreCase(response.CODE_OK)) {
            ConsultaColoniasCurp consultaColoniasCurp = (ConsultaColoniasCurp) response.getResponse();
            //if(ConstantsServer.ALLOW_LOG)android.util.Log.e("Dora", "analyzeResponse" + consultaColoniasCurp.getConsultaColonias().getCpValido());

            if(consultaColoniasCurp.getConsultaCurp().getCode().equalsIgnoreCase(response.CODE_OK))
                showCrearCuenta(consultaColoniasCurp);
            else{
                quieroCuentaView.limpiaCampos();
            }
        }else{
            showCrearCuentaCP();
            Log.e("Dora", "analyzeResponse errorQuieroCuenta");
        }


    }

    private void showCrearCuenta(ConsultaColoniasCurp consultaColoniasCurp){
        Gson gson = new Gson();
        Intent intent = new Intent(quieroCuentaView, CreaCuentaViewController.class);
        intent.putExtra(Constants.TAG_CODIGO_POSTAL, cp);
        intent.putExtra(Constants.COMPANIA,compania);
        intent.putExtra(Constants.CELULAR,celular);
        intent.putExtra(Constants.CONSULTA_COLONIA_CURP, gson.toJson(consultaColoniasCurp));

        quieroCuentaView.startActivity(intent);
        quieroCuentaView.finish();
    }

    private void showCrearCuentaCP(){
        Gson gson = new Gson();
        Intent intent = new Intent(quieroCuentaView, CreaCuentaViewController.class);
        intent.putExtra(Constants.TAG_CODIGO_POSTAL, cp);
        intent.putExtra(Constants.COMPANIA,compania);
        intent.putExtra(Constants.CELULAR,celular);

        quieroCuentaView.startActivity(intent);
        quieroCuentaView.finish();
    }




}
