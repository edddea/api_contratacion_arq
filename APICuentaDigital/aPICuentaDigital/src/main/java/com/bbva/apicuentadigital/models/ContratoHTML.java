package com.bbva.apicuentadigital.models;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.io.ParserJSON;
import com.bbva.apicuentadigital.io.ParsingException;
import com.bbva.apicuentadigital.io.ParsingHandler;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Created by Karina on 08/12/2015.
 */
public class ContratoHTML implements ParsingHandler{

    /**
     * Terminos de uso en formato HTML.
     */
    private String formatoHTML;


    /**
     * Tipo Producto solo para Venta TDC
     */
    private String tipoProducto;


    /**
     * @return Terminos de uso en formato HTML.
     */
    public String getFormatoHTML() {
        return formatoHTML;
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        /**
         * Modified according to: 3- D520_Contratos Vacios TDC BMovil
         * Received at : June 23th, 2015.
         * //tipoProducto = parser.parseNextValue("tipoProducto");
         */

        if(parser.hasValue(Constants.TAG_RESPONSE))
            parser = new ParserJSON(parser.parserNextObject(Constants.TAG_RESPONSE).toString());

        if(parser.hasValue(Constants.FORMATO_HTML)) {
            formatoHTML = parser.parseNextValue(Constants.FORMATO_HTML, true);
            formatoHTML = formatoHTML.replace('\'', '\"');
            formatoHTML = formatoHTML.replace("///", "/");
            grabarArchivo();
        }
    }

    private void grabarArchivo(){
        try{
            File f = new File(Constants.RUTA);
            File newFile = new File(f, Constants.NOMBRE_ARCHIVO);
            OutputStreamWriter osw = new OutputStreamWriter(
                    new FileOutputStream(newFile));
            osw.write(formatoHTML);
            osw.close();
            formatoHTML = newFile.getAbsolutePath();
        }catch(IOException e){
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
