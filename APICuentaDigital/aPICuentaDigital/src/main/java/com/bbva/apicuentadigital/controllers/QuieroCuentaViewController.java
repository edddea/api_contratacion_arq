package com.bbva.apicuentadigital.controllers;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.BaseActivity;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.delegates.CreaCuentaDelegate;
import com.bbva.apicuentadigital.delegates.QuieroCuentaDelegate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.timer.TimerController;
import tracking.TrackingHelperCuentaDigital;

/**
 * Created by Karina on 06/12/2015.
 */
public class QuieroCuentaViewController extends BaseActivity {

    private CreaCuentaDelegate creaCuentaDelegate;

    private Bundle bundle;

    private String celular;
    private String compania;

    private EditText edtCURP;
    private EditText edtCP;

    private ImageButton imbRegistrar;
    private ImageButton imbWhat;
    private ImageButton imbWhere;

    private ImageView imgClose;
    private ImageView imgDesc;
    private TextView txtDesc;
    private TextView linkVerAvisoPrivacidad;

    private GuiTools guiTools;

    private QuieroCuentaDelegate delegate;

    private LinearLayout lnBotones;
    private RelativeLayout rlDescBoton;

    public boolean atras = true;
    ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_quiero_cuenta_digital);
        bundle = getIntent().getExtras();
        init();
    }

    private void init(){
        findViewById();
        scaleView();
        getDataInten();
        etiquetado();
    }

    public void getDataInten(){
        celular = bundle.getString(Constants.CELULAR);
        compania = bundle.getString(Constants.COMPANIA);
        delegate = new QuieroCuentaDelegate(this, celular, compania);
    }

    private void findViewById(){
        edtCURP = (EditText)findViewById(R.id.edt_curp);
        edtCP = (EditText)findViewById(R.id.edt_cp);

        imbRegistrar = (ImageButton)findViewById(R.id.imb_registrar);
        imbWhat = (ImageButton)findViewById(R.id.imb_what);
        imbWhere = (ImageButton)findViewById(R.id.imb_where);

        imgClose =(ImageView)findViewById(R.id.img_close);
        imgDesc = (ImageView)findViewById(R.id.img_desc);

        txtDesc = (TextView)findViewById(R.id.txt_desc);
        linkVerAvisoPrivacidad = (TextView)findViewById(R.id.txt_verAvisoPrivacidad);

        lnBotones =(LinearLayout)findViewById(R.id.ln_botones);
        rlDescBoton = (RelativeLayout)findViewById(R.id.rl_desc);

    }

    private void scaleView(){
        guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.lbl_welcome), true);
       // guiTools.scale(findViewById(R.id.lbl_want_account), true);
        guiTools.scale(txtDesc, true);
        guiTools.scale(edtCP);
        guiTools.scale(edtCURP);
        guiTools.scale(linkVerAvisoPrivacidad);
        guiTools.scale(findViewById(R.id.imb_ayuda));
    }

    private void etiquetado(){
        list= new ArrayList<String>();
        list.add(Constants.QUIERO_CUENTA);
        TrackingHelperCuentaDigital.trackState(list);
        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackingHelperCuentaDigital.trackEntrada(eventoEntrada);
    }

    public void btnVerAviso(View v){

        alertDialogAvisoPrivacidad();

    }

    public void btnRegistrar(View v){

        boolean valida = delegate.validaCP(edtCP.getText().toString());
        if(valida) {
            delegate.realizaOperacion(edtCURP.getText().toString(), edtCP.getText().toString());
        }else{
            edtCP.setBackgroundResource(R.drawable.an_textbox_magenta);
            edtCP.setTextColor(getResources().getColor(R.color.pink_dark));
            edtCP.setHintTextColor(getResources().getColor(R.color.pink_dark));
        }
    }

    public void btnRegresar(View v){
        onBackPressed();
    }

    public void btnQueEs(View v){
        lnBotones.setVisibility(View.GONE);
        rlDescBoton.setVisibility(View.VISIBLE);
        imgDesc.setImageResource(R.drawable.img_quees);
        txtDesc.setText(getResources().getString(R.string.que_es));
    }

    public void btnDondeUsarla(View v){
        lnBotones.setVisibility(View.GONE);
        rlDescBoton.setVisibility(View.VISIBLE);
        imgDesc.setImageResource(R.drawable.img_donde);
        txtDesc.setText(getResources().getString(R.string.donde_puedo_usarla));
    }

    public void btnDescubreVentajas(View v){
        lnBotones.setVisibility(View.GONE);
        rlDescBoton.setVisibility(View.VISIBLE);
        imgDesc.setImageResource(R.drawable.img_ventajas);
        txtDesc.setText(getResources().getString(R.string.descrubre_ventajas));
    }

    public void btnBeneficios(View v){
        lnBotones.setVisibility(View.GONE);
        rlDescBoton.setVisibility(View.VISIBLE);
        imgDesc.setImageResource(R.drawable.img_beneficios);
        txtDesc.setText(getResources().getString(R.string.beneficios));
    }

    public void btnCerrar(View v){
        lnBotones.setVisibility(View.VISIBLE);
        rlDescBoton.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {

        if(atras) {
            /*showYesNoAlert(getResources().getString(R.string.label_information), getResources().getString(R.string.alert_abandonar),"NO", "SI",null,(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    atras = false;
                    onBackPressed();
                }}));*/
            showYesNoAlertNew(getResources().getString(R.string.label_information), getResources().getString(R.string.alert_abandonar),"NO", "SI",this);
        }else{
            super.onBackPressed();
        }
    }

    public void limpiaCampos(){
        showInformationAlert(R.string.error_cp);
        edtCURP.setText("");
        edtCP.setText("");
        edtCP.setBackgroundResource(R.drawable.an_textboxcp);
        edtCP.setTextColor(getResources().getColor(R.color.black));
        edtCP.setHintTextColor(getResources().getColor(R.color.black));
    }

    public void alertDialogAvisoPrivacidad(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        dialog.setContentView(R.layout.layout_alert_aviso_privacidad);
        TextView txtMessage = (TextView) dialog.findViewById(R.id.lblMessageAvisoPrivacidad);
        guiTools.scale(findViewById(R.id.encabezadoAvisoPrivacidad));
        guiTools.scale(findViewById(R.id.lblTitleAvisoPrivacidad),true);
        guiTools.scale(findViewById(R.id.btnAceptarAvisoPrivacidad));
        //guiTools.scale(findViewById(R.id.icon_aviso));
       // txtMessage.setText(Html.fromHtml("<font>BBVA Bancomer, S.A., Av. Paseo de la Reforma 510, Colonia Juárez, Delegación Cuauhtémoc, Código Postal 06600, Distrito Federal, recaba sus datos para verificar su identidad. El Aviso de Privacidad Integral actualizado está en cualquier sucursal y en </font> <b><font color='\" + getResources().getColor(R.color.colorBlueDark) +\"'>www.bancomer.com</font></b>"));
        txtMessage.setText(Html.fromHtml("<font>BBVA Bancomer, S.A., Av. Paseo de la Reforma 510, Colonia Juárez, Delegación Cuauhtémoc, Código Postal 06600, Distrito Federal, recaba sus datos para verificar su identidad. El Aviso de Privacidad Integral actualizado está en cualquier sucursal y en </font><b><font color=\"#009EE5\">www.bancomer.com</font>"));
        Button btnLate = (Button) dialog.findViewById(R.id.btnAceptarAvisoPrivacidad);
        btnLate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        scaleAlertDialog(dialog);

        dialog.show();
    }

    public void alertDialogError(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.layout_alert_aviso_privacidad);

        TextView txtMessage = (TextView) dialog.findViewById(R.id.lblMessageAvisoPrivacidad);
        txtMessage.setText("Error");

        Button btnLate = (Button) dialog.findViewById(R.id.btnAceptarAvisoPrivacidad);
        btnLate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        scaleAlertDialog(dialog);

        dialog.show();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }



}