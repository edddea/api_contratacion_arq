package com.bbva.apicuentadigital.common;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.controllers.CreaCuentaViewController;
import com.bbva.apicuentadigital.controllers.IdentificateViewController;
import com.bbva.apicuentadigital.controllers.QuieroCuentaViewController;
import com.bbva.apicuentadigital.controllers.SplasViewController;

/**
 * Created by Karina on 07/12/2015.
 */
public class BaseActivity extends Activity{

    protected boolean habilitado = true;
    static ProgressDialog mProgressDialog;

    /**
     * The alert error or information dialog.
     */
    private AlertDialog mAlertDialog;

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    // #region Information Alert.


    /**
     * Muestra una alerta con el mensaje dado.
     * @param message el mensaje a mostrar
     */


    public void showInformationAlert(int message) {
        if (message > 0) {
            showInformationAlert(getString(message));
        }
    }

    /**
     * Shows an information alert with the given message text. No action is performed after this alert is closed.
     * @param message the message text to show
     */
    public void showInformationAlert(String message) {
        if (message.length() > 0) {
            showInformationAlert(message, null);
        }
    }

    /**
     * Shows an information alert with the given message.
     * @param message the message resource to show
     */
    public void showInformationAlert(int message, OnClickListener listener) {
        if (message > 0) {
            showInformationAlert(getString(message), listener);
        }
    }

    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param message the message text to show
     * @param listener OnClickListener to perform action on close
     */
    public void showInformationAlert(String message, OnClickListener listener) {
        showInformationAlert(getString(R.string.label_information), message, listener);
    }

    /**
     * Shows an alert dialog.
     * @param title The alert title resource id.
     * @param message The alert message resource id.
     * @param listener The listener for the "Ok" button.
     */
    public void showInformationAlert(int title, int message, OnClickListener listener) {
        if(message > 0) {
            showInformationAlert(getString(title), getString(message), listener);
        }
    }

    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param title The alert title resource id.
     * @param message the message text to show
     * @param listener OnClickListener to perform action on close
     */
    public void showInformationAlert(String title, String message, OnClickListener listener) {
        showInformationAlert(title, message, getString(R.string.label_ok), listener);
    }

    /**
     * Shows an alert dialog.
     * @param title The alert title resource id.
     * @param message The alert message resource id.
     * @param okText the ok text
     * @param listener The listener for the "Ok" button.
     */
    public void showInformationAlert(int title, int message, int okText, OnClickListener listener) {
        if(message > 0) {
            showInformationAlert(getString(title), getString(message), getString(okText), listener);
        }
    }

    /**
     * Shows an alert dialog.
     * @param title The alert title.
     * @param message The alert message.
     * @param okText the ok text
     * @param listener The listener for the "Ok" button.
     */
    public void showInformationAlert(String title, String message, String okText, OnClickListener listener) {
        if(!habilitado)
            return;
        habilitado = false;
        if(message.length() > 0){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setIcon(R.drawable.an_ic_aviso);
            alertDialogBuilder.setMessage(message);
            if (null == listener) {
                listener = new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                };
            }

            alertDialogBuilder.setPositiveButton(okText, listener);
            alertDialogBuilder.setCancelable(false);
            mAlertDialog = alertDialogBuilder.create();
            mAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    habilitado = true;
                }
            });
            mAlertDialog.show();
        }
    }

    public void showActivityIndicator() {
        try {
            if (mProgressDialog != null) {
                hideActivityIndicator();
            }
                mProgressDialog = ProgressDialog.show(this, getResources().getString(R.string.alert_operation_),
                        getResources().getString(R.string.alert_connecting_), true);

            mProgressDialog.setCancelable(false);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    /**
     * Hides the progress dialog.
     */
    public void hideActivityIndicator() {
        if(mProgressDialog != null){
            mProgressDialog.dismiss();
        }
    }

    public void showSplas(){
        android.app.FragmentManager fm = getFragmentManager();
        SplasViewController dialog = new SplasViewController();
        dialog.setCancelable(false);
        dialog.show(fm, "Result");
    }


    public void alertDialog(String celular, String compania, String correo, final IdentificateViewController controller){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.layout_alert_dialog);


        TextView txtMessage = (TextView) dialog.findViewById(R.id.lblMessage);
        /*txtMessage.setText(Html.fromHtml("<font>Te enviaremos un código de seguridad por SMS al número celular: </font> <b><font color='" + getResources().getColor(R.color.colorBlueDark) +"'>"+celular+"</font></b>" +
                "<font> de la compañia <b><font color='" + getResources().getColor(R.color.colorBlueDark) +"'>"+compania+"</font><font>  al correo electrónico </font></b><b><font color='" + getResources().getColor(R.color.colorBlueDark) +"'>"+correo+"</font></b>"));*/
        txtMessage.setText("Por favor confirma tu número celular y correo electrónico donde enviaremos tus códigos para activar tu cuenta:");

        Button btnNow = (Button) dialog.findViewById(R.id.btnLate);
        btnNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.modificar();
                dialog.dismiss();
            }
        });

        Button btnLate = (Button) dialog.findViewById(R.id.btnNow);
        btnLate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.aceptar();
                dialog.dismiss();
            }
        });

        TextView edtCel = (TextView) dialog.findViewById(R.id.edtCel);
        edtCel.setText(celular);

        TextView edtCompania = (TextView) dialog.findViewById(R.id.edtCompania);
        edtCompania.setText(compania);

        TextView edtmail = (TextView) dialog.findViewById(R.id.edtmail);
        edtmail.setText(correo);

        scaleAlertDialog(dialog);

        dialog.show();
    }







    public void showYesNoAlertNew(String title,
                               String message,
                               String negative,
                               String positive,
                               final QuieroCuentaViewController controller) {

            final Dialog dialog = new Dialog(this);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.layout_alert_generic);

        TextView txtTitle = (TextView) dialog.findViewById(R.id.lblTitle);
        txtTitle.setText(title);

        TextView txtMessage = (TextView) dialog.findViewById(R.id.lblMessage);
        txtMessage.setText(message);


        Button btnLate = (Button) dialog.findViewById(R.id.btnLate);
            btnLate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            Button btnNow = (Button) dialog.findViewById(R.id.btnNow);
            btnNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    controller.atras = false;
                    controller.onBackPressed();
                    dialog.dismiss();
                }
            });

        scaleAlertYesNo(dialog);

        dialog.show();

    }


    public void showYesNoAlertNew2(String title,
                                  String message,
                                  String negative,
                                  String positive,
                                  final CreaCuentaViewController controller) {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.layout_alert_generic);

        TextView txtTitle = (TextView) dialog.findViewById(R.id.lblTitle);
        txtTitle.setText(title);

        TextView txtMessage = (TextView) dialog.findViewById(R.id.lblMessage);
        txtMessage.setText(message);


        Button btnLate = (Button) dialog.findViewById(R.id.btnLate);
        btnLate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button btnNow = (Button) dialog.findViewById(R.id.btnNow);
        btnNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                controller.creaCuentaDelegate.quieroCuenta();;

            }
        });

        scaleAlertYesNo(dialog);

        dialog.show();

    }

   public void showYesNoAlert(String title,
                               String message,
                               String okText,
                               String calcelText,
                               OnClickListener positiveListener,
                               OnClickListener negativeListener) {
        if(!habilitado)
            return;
        habilitado = false;
        if(message.length() > 0){
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setIcon(R.drawable.ic_aviso_cd);
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setPositiveButton(okText, positiveListener);

            if(null == negativeListener) {
                alertDialogBuilder.setNegativeButton(calcelText, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        habilitado = true;
                        dialog.dismiss();
                    }
                });
            } else {
                alertDialogBuilder.setNegativeButton(calcelText, negativeListener);
            }

            alertDialogBuilder.setCancelable(false);
            mAlertDialog = alertDialogBuilder.create();
            mAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    habilitado = true;
                }
            });
            mAlertDialog.show();

        }
    }

    public void alertDialogAyudaCredencial(){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.layout_ayuda_credencial);
        ImageView imgAyuda = (ImageView) dialog.findViewById(R.id.imgAyuda);
        imgAyuda.setClickable(true);
        imgAyuda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(dialog.findViewById(R.id.imgAyuda));

        dialog.show();
    }

    public void alertGeneric(String title, String message, String btnAcceptText,boolean isAviso){
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.layout_alert);

        ImageView img_aviso= (ImageView) dialog.findViewById(R.id.img_aviso);
        Button btnAccept = (Button) dialog.findViewById(R.id.btnAccept);
        btnAccept.setText(btnAcceptText);

        TextView lblTitle = (TextView) dialog.findViewById(R.id.lblTitle);
        lblTitle.setText(title);

        TextView lblMessage = (TextView) dialog.findViewById(R.id.lblMessage);
        lblMessage.setText(message);

        btnAccept.setClickable(true);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        scaleAlert(dialog);
        dialog.show();
    }

    /**
     * scale the view of alertDialog
     * @param dialog
     */
    public void scaleAlert(Dialog dialog){
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(dialog.findViewById(R.id.lblTitle));
        guiTools.scale(dialog.findViewById(R.id.lblMessage));
        guiTools.scale(dialog.findViewById(R.id.btnAccept));
        guiTools.scale(dialog.findViewById(R.id.img_aviso));
        guiTools.scale(dialog.findViewById(R.id.imgDivider));
    }


    public void scaleAlertYesNo(Dialog dialog){
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(dialog.findViewById(R.id.lblTitle));
        guiTools.scale(dialog.findViewById(R.id.lblMessage));
        guiTools.scale(dialog.findViewById(R.id.btnLate));
        guiTools.scale(dialog.findViewById(R.id.btnNow));
        guiTools.scale(dialog.findViewById(R.id.img_aviso));
    }

    /**
     * scale the view of alertDialog
     * @param dialog
     */
    public void scaleAlertDialog(Dialog dialog){
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(dialog.findViewById(R.id.lblTitle));
        guiTools.scale(dialog.findViewById(R.id.lblMessage));
        guiTools.scale(dialog.findViewById(R.id.btnNow));
        guiTools.scale(dialog.findViewById(R.id.btnLate));
        guiTools.scale(dialog.findViewById(R.id.and_cel));
        guiTools.scale(dialog.findViewById(R.id.lblCel));
        guiTools.scale(dialog.findViewById(R.id.edtCel));
        guiTools.scale(dialog.findViewById(R.id.lblCompania));
        guiTools.scale(dialog.findViewById(R.id.edtCompania));
        guiTools.scale(dialog.findViewById(R.id.img_mail));
        guiTools.scale(dialog.findViewById(R.id.lblmail));
        guiTools.scale(dialog.findViewById(R.id.edtmail));
        guiTools.scale(dialog.findViewById(R.id.img_aviso));

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
