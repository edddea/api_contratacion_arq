package com.bbva.apicuentadigital.common;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.bbva.apicuentadigital.controllers.CreaCuentaViewController;
import com.bbva.apicuentadigital.controllers.QuieroCuentaViewController;
import com.bbva.apicuentadigital.io.ConstantsServer;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

/**
 * Created by DMEJIA on 16/12/15.
 */
public class APICuentaDigital{

    public static Context appContext;
    private APICuentaDigitalSharePreferences sharePreferences;

    public void ejecutaAPICuentaDigital( Context context, String celular, String compania, boolean DEVELOPMENT, boolean SIMULATION, boolean EMULATOR, boolean ALLOW_LOG){
        appContext = context;
        ConstantsServer.SIMULATION = SIMULATION;
        ConstantsServer.DEVELOPMENT = DEVELOPMENT;
        ConstantsServer.ALLOW_LOG = ALLOW_LOG;
        sharePreferences = APICuentaDigitalSharePreferences.getInstance();
        if(sharePreferences.getBooleanData("CONFIRMAOTP")){
            Intent intent = new Intent(appContext, CreaCuentaViewController.class);
            intent.putExtra("confirmacion",true);
            intent.putExtra(Constants.CELULAR, celular);
            intent.putExtra(Constants.COMPANIA, compania);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            appContext.startActivity(intent);
        }else {
            Intent intent = new Intent(appContext, QuieroCuentaViewController.class);
            intent.putExtra(Constants.CELULAR, celular);
            intent.putExtra(Constants.COMPANIA, compania);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            appContext.startActivity(intent);
        }

    }
}
