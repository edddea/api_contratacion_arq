package com.bbva.apicuentadigital.io;

/**
 * Created by Karina on 07/12/2015.
 */
public class ConstantsServer {

    public static final String BASE_URL_TEST = "https://www.bancomermovil.net:11443/dembmv_mx_web";
    public static final String BASE_URL_PRODUCCION ="https://www.bancomermovil.com";
    public static final String BASE_CONSUMOS = "/mbmv_mult_web_mbmv_01/services/digitalAccount/V01/";
    public static boolean ALLOW_LOG=false;
    public static boolean SIMULATION=false;
    public static boolean DEVELOPMENT=false;

    public static final String OPERATION_CODE_PARAMETER = "OPERACION";
    //BASE_URL_WADL + BASE_CONSUMOS + SERVICIOS[peticion] + PARAMS

    public static final String[] SERVICIOS = {"",
            "consultaCURP","consultaContratoCD","generateOTP","validateOTP","consultaColonias"};

}
