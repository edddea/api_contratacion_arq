package com.bbva.apicuentadigital.controllers;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.mbanking.BanderasBMovilFileManager;
import com.bbva.apicuentadigital.common.BaseActivity;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.delegates.CreaCuentaDelegate;
import com.bbva.apicuentadigital.models.ConsultaColoniasCurp;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;
import com.google.gson.Gson;
import bancomer.api.common.timer.TimerController;
/**
 * Created by Karina on 07/12/2015.
 */
public class CreaCuentaViewController extends BaseActivity{

    private Fragment fragment;
    private FragmentManager fm;
    private GuiTools guiTools;

    public CreaCuentaDelegate creaCuentaDelegate;

    private String json;
    private String cp;
    private String compania;
    private String celular;
    private Boolean confirmacion;

    private boolean isVisible;

    private ImageView imgTitle;
    private Constants.VISTA actual;
    private ImageButton imbBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_crea_cuenta_digital);
        json = getIntent().getExtras().getString(Constants.CONSULTA_COLONIA_CURP);
        cp = getIntent().getExtras().getString(Constants.TAG_CODIGO_POSTAL);
        compania = getIntent().getExtras().getString(Constants.COMPANIA);
        celular = getIntent().getExtras().getString(Constants.CELULAR);
        confirmacion= getIntent().getExtras().getBoolean("confirmacion");
        init();
    }


    private void init(){
        fm = getFragmentManager();
        findViewById();
        scaleView();
        validarCelular();
        validarConfirmacion();
        //cargarDatos();
        //clausulas();
    }

    private void findViewById(){
        imgTitle = (ImageView)findViewById(R.id.img_title);
        imbBack = (ImageButton)findViewById(R.id.imb_back);
    }

    private void scaleView(){
        guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
    }

    public void identificate(){
        actual = Constants.VISTA.Identificate;
        Fragment fragment = new IdentificateViewController(guiTools, creaCuentaDelegate, isVisible);
        fm.beginTransaction().replace(R.id.frame, fragment).commit();
    }

    public void clausulas(){
        actual = Constants.VISTA.Clausulas;
        imgTitle.setImageResource(R.drawable.img_estado_2);
        this.fragment= new ClausulasViewController(guiTools, creaCuentaDelegate);
        fm.beginTransaction().replace(R.id.frame, fragment).commit();
    }

    public void confirmacion(){
        actual = Constants.VISTA.Confirmacion;
        Fragment fragment = new ConfirmacionViewController(guiTools, creaCuentaDelegate);
        fm.beginTransaction().replace(R.id.frame, fragment).commitAllowingStateLoss();
    }

    public void datosConfirmados(){
        actual = Constants.VISTA.DatosConfirmados;
        imbBack.setVisibility(View.GONE);
        Fragment fragment = new DatosConfirmadosViewController(guiTools, creaCuentaDelegate);
        fm.beginTransaction().replace(R.id.frame, fragment).commit();
    }

    public void exito(){
        actual = Constants.VISTA.Exito;
        imbBack.setVisibility(View.GONE);
        imgTitle.setImageResource(R.drawable.img_estado_3);
        Fragment fragment = new ExitoViewController(guiTools, creaCuentaDelegate);
        fm.beginTransaction().replace(R.id.frame, fragment).commit();
    }

    public void cargarFragment(){
        actual = Constants.VISTA.Clausulas;
        imbBack.setVisibility(View.VISIBLE);
        fm.beginTransaction().replace(R.id.frame, this.fragment).commit();
    }

    private void cargarDatos(){
        Gson gson = new Gson();
        creaCuentaDelegate = new CreaCuentaDelegate(this, cp, compania, celular);
        if (json==null){

            APICuentaDigitalSharePreferences.getInstance().setBooleanData(Constants.OK_CONSULTA_CURP, false);

        }else {
            creaCuentaDelegate.obtenerInformacion(gson.fromJson(json, ConsultaColoniasCurp.class));
        }
        identificate();
    }

    private void validarConfirmacion(){
        if(confirmacion) {
            creaCuentaDelegate = new CreaCuentaDelegate(this, cp, compania, celular);
            confirmacion();
        }
        else
            cargarDatos();
    }

    private void validarCelular(){
        if(celular != null && !celular.equals("")){
            isVisible = true;
        }
    }

    @Override
    public void onBackPressed() {

        switch (actual){
            case Identificate:
                vistaIdentificate();
                break;
            case Clausulas:
                datosConfirmados();
                break;
            case Confirmacion:
                cargarFragment();
                /*BanderasBMovilFileManager fileManager = BanderasBMovilFileManager.getCurrent();
                fileManager.setBanderasContratarCDigitat(true);*/
                break;
            case Exito:
                break;
        }
    }

    /*@Override
    protected void onResume() {
        Log.e("MAPE","si entro en OnResume");
        // TODO Auto-generated method stub
        switch (actual) {
            case Identificate:
                Log.e("MAPE","identificate");
                break;
            case Clausulas:
                if(isDevolver){
                   Log.e("MAPE","clausulas");
                    clausulas();
                }
                break;
            case Confirmacion:
                if(isDevolver) {
                    Log.e("MAPE","clausulas");
                    confirmacion();
                }
                break;
            case Exito:
                break;
        }
        super.onResume();
        isDevolver=false;
        Log.e("MAPE","-----"+Boolean.toString(isDevolver));
    }

    @Override
    protected void onPause() {
        Log.e("MAPE","si entro en OnPause");
        switch (actual) {
            case Identificate:
                break;
            case Clausulas:
               isDevolver= true;
                break;
            case Confirmacion:
                isDevolver= true;
                break;
            case Exito:
                break;
        }
        super.onPause();
        Log.e("MAPE","-----"+Boolean.toString(isDevolver));
    }*/

    public void onClick(View v){
        onBackPressed();
    }

    private void vistaIdentificate(){
        /*showYesNoAlert(getResources().getString(R.string.label_information), getResources().getString(R.string.alert_abandonar_contratacion), "NO", "SI", null, (new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                creaCuentaDelegate.quieroCuenta();
            }
        }));*/
        showYesNoAlertNew2(getResources().getString(R.string.label_information), getResources().getString(R.string.alert_abandonar),"NO", "SI",this);
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

}
