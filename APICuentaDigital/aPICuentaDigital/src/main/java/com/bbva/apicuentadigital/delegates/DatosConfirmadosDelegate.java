package com.bbva.apicuentadigital.delegates;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.Tools;
import com.bbva.apicuentadigital.controllers.DatosConfirmadosViewController;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

/**
 * Created by Karina on 15/12/2015.
 */
public class DatosConfirmadosDelegate {

    private CreaCuentaDelegate creaCuentaDelegate;
    private DatosConfirmadosViewController datosConfirmadosViewController;
    private APICuentaDigitalSharePreferences sharePreferences;

    public DatosConfirmadosDelegate(CreaCuentaDelegate creaCuentaDelegate, DatosConfirmadosViewController datosConfirmadosViewController){
        this.creaCuentaDelegate = creaCuentaDelegate;
        this.datosConfirmadosViewController = datosConfirmadosViewController;
        sharePreferences = APICuentaDigitalSharePreferences.getInstance();
    }

    public void obtenerInformacion(){
        datosConfirmadosViewController.llenarDatosContacto(sharePreferences.getStringData(Constants.CELULAR), sharePreferences.getStringData(Constants.COMPANIA), sharePreferences.getStringData(Constants.TAG_MAIL));
        datosConfirmadosViewController.llenarDatosPersonales(Tools.getName(),
                sharePreferences.getStringData(Constants.TAG_CREDENTIAL), sharePreferences.getStringData(Constants.TAG_FECHANAC), sharePreferences.getStringData(Constants.TAG_LUGARN), sharePreferences.getStringData(Constants.TAG_SEXO));
        datosConfirmadosViewController.llenarDomicilio(sharePreferences.getStringData(Constants.TAG_CODIGO_POSTAL), sharePreferences.getStringData(Constants.TAG_ENTIDAD), sharePreferences.getStringData(Constants.TAG_DELEGACION), sharePreferences.getStringData(Constants.TAG_COLONIA), sharePreferences.getStringData(Constants.TAG_CALLE),
                sharePreferences.getStringData(Constants.TAG_NUMEXT), sharePreferences.getStringData(Constants.TAG_NUMINT));
    }
}
