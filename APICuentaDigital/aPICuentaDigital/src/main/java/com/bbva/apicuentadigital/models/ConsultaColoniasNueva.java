package com.bbva.apicuentadigital.models;

import android.util.Log;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.io.ParserJSON;
import com.bbva.apicuentadigital.io.ParsingException;
import com.bbva.apicuentadigital.io.ParsingHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by antoniochairez on 04/03/16.
 */
public class ConsultaColoniasNueva implements ParsingHandler {

    private ConsultaColonias consultaColonias;


    public ConsultaColonias getConsultaColonias() {
        return consultaColonias;
    }

    public void setConsultaColonias(ConsultaColonias consultaColonias) {
        this.consultaColonias = consultaColonias;
    }

    public ConsultaColoniasNueva(){
        consultaColonias = new ConsultaColonias();
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        //JACT
        JSONObject jsonObjectStat = parser.parserNextObject(Constants.TAG_RESPONSE);
        try {
            if (jsonObjectStat.has(Constants.TAG_COLONIAS)){
                //JSONObject getConsultaCol = jsonObjectStat.getJSONObject(Constants.TAG_COLONIAS);
                JSONArray colonias = jsonObjectStat.getJSONArray(Constants.TAG_COLONIAS);
                String [] col = new String[colonias.length()+1];
                for (int i = 0; i < colonias.length()+1; i++){
                    if(i == 0)
                        col[i] = "Selecciona";
                    else
                        col[i] = colonias.get(i-1).toString();
                }
                consultaColonias.setColonias(col);

            }

            if (jsonObjectStat.has(Constants.TAG_CODE))
                consultaColonias.setCode("200");

            if (jsonObjectStat.has(Constants.TAG_DELEGACION))
                consultaColonias.setDelegacion(jsonObjectStat.getString(Constants.TAG_DELEGACION));

            if (jsonObjectStat.has(Constants.TAG_ENTIDAD))
                consultaColonias.setEntidad(jsonObjectStat.getString(Constants.TAG_ENTIDAD));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
