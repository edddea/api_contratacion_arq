/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package com.bbva.apicuentadigital.io;

/**
 * ParsingException notifies an exception in server response processing.
 *
 * @author Stefanini IT Solutions.
 */
public class ParsingException extends Exception {

    /**
	 * 
	 */
	private static final long	serialVersionUID	= 1L;

	/**
     * Default constructor.
     * @param message the message
     */
    public ParsingException(String message) {
        super(message);
    }
}
