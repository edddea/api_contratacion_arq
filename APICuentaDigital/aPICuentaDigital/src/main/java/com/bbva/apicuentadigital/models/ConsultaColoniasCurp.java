package com.bbva.apicuentadigital.models;

import android.util.Log;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.io.ParserJSON;
import com.bbva.apicuentadigital.io.ParsingException;
import com.bbva.apicuentadigital.io.ParsingHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Karina on 07/12/2015.
 */
public class ConsultaColoniasCurp implements ParsingHandler {

    //private ConsultaColonias consultaColonias;

    private ConsultaCurp consultaCurp;

    //private ValidaCurp validaCurp;


    /*public ConsultaColonias getConsultaColonias() {
        return consultaColonias;
    }

    public void setConsultaColonias(ConsultaColonias consultaColonias) {
        this.consultaColonias = consultaColonias;
    }



    public ValidaCurp getValidaCurp() {
        return validaCurp;
    }

    public void setValidaCurp(ValidaCurp validaCurp) {
        this.validaCurp = validaCurp;
    }*/

    public ConsultaCurp getConsultaCurp() {
        return consultaCurp;
    }

    public void setConsultaCurp(ConsultaCurp consultaCurp) {
        this.consultaCurp = consultaCurp;
    }

    public ConsultaColoniasCurp(){
        //consultaColonias = new ConsultaColonias();
        //validaCurp = new ValidaCurp();
        consultaCurp = new ConsultaCurp();
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        JSONObject jsonObjectResponse = parser.parserNextObject(Constants.TAG_RESPONSE);

            try {
                consultaCurp.setCode("200");

/*                    if (jsonObjectResponse.has(Constants.TAG_DESCRIPTION))
                        consultaCurp.setDescription(jsonObjectResponse.getString(Constants.TAG_DESCRIPTION));*/

                if (jsonObjectResponse.has(Constants.TAG_AMATERNO))
                    consultaCurp.setaMaterno(jsonObjectResponse.getString(Constants.TAG_AMATERNO));

                if (jsonObjectResponse.has(Constants.TAG_APATERNO))
                     consultaCurp.setaPaterno(jsonObjectResponse.getString(Constants.TAG_APATERNO));


                if (jsonObjectResponse.has(Constants.TAG_CURPVALIDO))
                    consultaCurp.setCurpValido(jsonObjectResponse.getString(Constants.TAG_CURPVALIDO));

                if (jsonObjectResponse.has(Constants.TAG_NOMBRES))
                    consultaCurp.setNombres(jsonObjectResponse.getString(Constants.TAG_NOMBRES));

                if (jsonObjectResponse.has(Constants.TAG_SEXO))
                    consultaCurp.setSexo(jsonObjectResponse.getString(Constants.TAG_SEXO));

                if (jsonObjectResponse.has(Constants.TAG_FECHANAC))
                    consultaCurp.setFechNac(jsonObjectResponse.getString(Constants.TAG_FECHANAC));

                if (jsonObjectResponse.has(Constants.TAG_CVEENTIDADNAC))
                    consultaCurp.setCveEntidadNac(jsonObjectResponse.getString(Constants.TAG_CVEENTIDADNAC));

                if (jsonObjectResponse.has(Constants.TAG_CURP))
                    consultaCurp.setCurp(jsonObjectResponse.getString(Constants.TAG_CURP));

            } catch (JSONException e) {
                e.printStackTrace();
            }

    }
    /*public void process(ParserJSON parser) throws IOException, ParsingException {

        if(parser.hasValue(Constants.TAG_OPERACIONES)) {
            JSONObject jsonObject = parser.parserNextObject(Constants.TAG_OPERACIONES);

            try {
                if (jsonObject.has(Constants.TAG_CONSULTACOLONIAS)) {
                    JSONObject consultaColoniasObjet = jsonObject.getJSONObject(Constants.TAG_CONSULTACOLONIAS);

                    if (consultaColoniasObjet.has(Constants.TAG_CODE))
                        consultaColonias.setCode(consultaColoniasObjet.getString(Constants.TAG_CODE));

                    if (consultaColoniasObjet.has(Constants.TAG_COLONIAS)) {
                        JSONArray colonias = consultaColoniasObjet.getJSONArray(Constants.TAG_COLONIAS);
                        String [] col = new String[colonias.length()+1];
                        for (int i = 0; i < colonias.length()+1; i++){
                            if(i == 0)
                                col[i] = "Selecciona";
                            else
                                col[i] = colonias.get(i-1).toString();
                        }

                        consultaColonias.setColonias(col);
                    }

                    if (consultaColoniasObjet.has(Constants.TAG_CPVALIDO))
                        consultaColonias.setCpValido(consultaColoniasObjet.getString(Constants.TAG_CPVALIDO));

                    if (consultaColoniasObjet.has(Constants.TAG_DELEGACION))
                        consultaColonias.setDelegacion(consultaColoniasObjet.getString(Constants.TAG_DELEGACION));

                    if (consultaColoniasObjet.has(Constants.TAG_DESCRIPTION))
                        consultaColonias.setDescription(consultaColoniasObjet.getString(Constants.TAG_DESCRIPTION));

                    if (consultaColoniasObjet.has(Constants.TAG_ENTIDAD))
                        consultaColonias.setEntidad(consultaColoniasObjet.getString(Constants.TAG_ENTIDAD));
                }

                if (jsonObject.has(Constants.TAG_CONSULTACURP)) {
                    JSONObject consultaCurpObjet = jsonObject.getJSONObject(Constants.TAG_CONSULTACURP);

                    if (consultaCurpObjet.has(Constants.TAG_CODE))
                        consultaCurp.setCode(consultaCurpObjet.getString(Constants.TAG_CODE));

                    if (consultaCurpObjet.has(Constants.TAG_DESCRIPTION))
                        consultaCurp.setDescription(consultaCurpObjet.getString(Constants.TAG_DESCRIPTION));

                    if (consultaCurpObjet.has(Constants.TAG_AMATERNO))
                        consultaCurp.setaMaterno(consultaCurpObjet.getString(Constants.TAG_AMATERNO));

                    if (consultaCurpObjet.has(Constants.TAG_APATERNO))
                        consultaCurp.setaPaterno(consultaCurpObjet.getString(Constants.TAG_APATERNO));

                    if (consultaCurpObjet.has(Constants.TAG_CURPVALIDO))
                        consultaCurp.setCurpValido(consultaCurpObjet.getString(Constants.TAG_CURPVALIDO));

                    if (consultaCurpObjet.has(Constants.TAG_NOMBRES))
                        consultaCurp.setNombres(consultaCurpObjet.getString(Constants.TAG_NOMBRES));

                    if (consultaCurpObjet.has(Constants.TAG_SEXO))
                        consultaCurp.setSexo(consultaCurpObjet.getString(Constants.TAG_SEXO));

                    if (consultaCurpObjet.has(Constants.TAG_FECHANAC))
                        consultaCurp.setFechNac(consultaCurpObjet.getString(Constants.TAG_FECHANAC));

                    if (consultaCurpObjet.has(Constants.TAG_CVEENTIDADNAC))
                        consultaCurp.setCveEntidadNac(consultaCurpObjet.getString(Constants.TAG_CVEENTIDADNAC));
                }

                if (jsonObject.has(Constants.TAG_VALIDACURP)) {
                    JSONObject validaCurpObjet = jsonObject.getJSONObject(Constants.TAG_VALIDACURP);

                    if (validaCurpObjet.has(Constants.TAG_CODE))
                        validaCurp.setCode(validaCurpObjet.getString(Constants.TAG_CODE));

                    if (validaCurpObjet.has(Constants.TAG_DESCRIPTION))
                        validaCurp.setDescription(validaCurpObjet.getString(Constants.TAG_DESCRIPTION));

                    if (validaCurpObjet.has(Constants.TAG_CURP))
                        validaCurp.setCurp(validaCurpObjet.getString(Constants.TAG_CURP));

                    if (validaCurpObjet.has(Constants.TAG_CURPSTATUSB))
                        validaCurp.setCurpStatusB(validaCurpObjet.getString(Constants.TAG_CURPSTATUSB));

                    if (validaCurpObjet.has(Constants.TAG_ENCONTROCURP))
                        validaCurp.setEncontroCURP(validaCurpObjet.getString(Constants.TAG_ENCONTROCURP));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }*/
}
