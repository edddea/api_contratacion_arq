package com.bbva.apicuentadigital.delegates;

import android.content.Intent;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.controllers.ExitoViewController;
import com.bbva.apicuentadigital.controllers.WebViewController;
import com.bbva.apicuentadigital.io.ConstantsServer;
import com.bbva.apicuentadigital.io.Server;
import com.bbva.apicuentadigital.io.ServerResponse;
import com.bbva.apicuentadigital.models.ContratoHTML;
import com.bbva.apicuentadigital.models.ValidaOTP;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

import java.util.Hashtable;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;

/**
 * Created by Karina on 09/12/2015.
 */
public class ExitoDelegate extends BaseDelegate{

    private CreaCuentaDelegate creaCuentaDelegate;
    private ExitoViewController exitoViewController;
    private APICuentaDigitalSharePreferences sharePreferences;

    public ExitoDelegate(CreaCuentaDelegate creaCuentaDelegate, ExitoViewController exitoViewController){
        this.creaCuentaDelegate = creaCuentaDelegate;
        this.exitoViewController = exitoViewController;
        sharePreferences = APICuentaDigitalSharePreferences.getInstance();
    }

    public void realizaOperacion(){
        creaCuentaDelegate.getCreaCuentaView().showActivityIndicator();

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(Constants.TAG_NUMUERO_CELULAR,"5515024046");
        params.put(Constants.TAG_ID_PRODUCTO,"PBXXXXXX01");
        params.put(Constants.TAG_IND_VACIO,Constants.TAG_FALSE);
        params.put(Constants.TAG_CONTRATO,"PBXXXXXX01");

        Server server = Server.getInstance(this);
        server.doNetWorkOperation(ApiConstants.CONSULTA_CONTRATO, params);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        creaCuentaDelegate.getCreaCuentaView().hideActivityIndicator();
        if(response.getCODE().equalsIgnoreCase(response.CODE_OK)) {
            ContratoHTML contratoHTML = (ContratoHTML)response.getResponse();
            showTerminos(contratoHTML.getFormatoHTML());
        }
    }

    public void obtenerDatos(){
        ValidaOTP validaOTP = creaCuentaDelegate.getValidaOTP();
        String cuentaClabe="";
        if(validaOTP.getCuentaClabe().equalsIgnoreCase("null"))
            cuentaClabe= "No disponible";
        else
            cuentaClabe=validaOTP.getCuentaClabe();

        exitoViewController.mostarDatos(validaOTP.getTitular(), validaOTP.getCuenta(), validaOTP.getTarjeta(), cuentaClabe, sharePreferences.getStringData(Constants.CELULAR), sharePreferences.getStringData(Constants.TAG_MAIL));
    }

    public void showTerminos(String url){
        Intent intent = new Intent(exitoViewController.getActivity(), WebViewController.class);
        intent.putExtra(Constants.OPERATION, Constants.TERMINOS);
        intent.putExtra(Constants.URL, url);
        exitoViewController.getActivity().startActivity(intent);
    }

    public void dataKeyChain(){
        /*
        KeyStoreWrapper.getInstance(APICuentaDigital.appContext).setSeed(String.valueOf(APICuentaDigitalSharePreferences.getInstance().getSeed()));
        KeyStoreWrapper.getInstance(APICuentaDigital.appContext).setUserName(APICuentaDigitalSharePreferences.getInstance().getUser());
        android.util.Log.e("Key", "seed" + KeyStoreWrapper.getInstance(APICuentaDigital.appContext).getSeed());
        android.util.Log.e("Key", "user" + KeyStoreWrapper.getInstance(APICuentaDigital.appContext).getUserName());
        */
    }
}
