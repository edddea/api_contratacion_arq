package com.bbva.apicuentadigital.io;

/**
 * Created by Karina on 08/12/2015.
 */
public class TramasSimulation {
    public static String COLONIAS_CURP_EXITO =  "{\"response\":{\"status\": {\"code\": \"200\",\"description\": \"Flujo exitoso\"}},\"Operaciones\":{\"consultaColonias\":{\"code\": \"200\",\"description\": \"Operacion Exitosa\",\"cpValido\":\"true\",\"entidad\": \"DF\",\"delegacion\": \"Benito Juarez\",\"colonias\": [\"claveria\",\"rosario\"]},\"consultaCurp\":{\"code\": \"200\",\"description\": \"Operacion Exitosa\",\"curpValido\":\"true\",\"nombres\": \"Isaac\",\"aPaterno\": \"Chavirez\",\"aMaterno\": \"Rodriguez\",\"sexo\": \"H\",\"fechNac\": \"31-02-1980\",\"cveEntidadNac\": \"DF\"},\"validaCurp\":{\"code\": \"200\",\"description\": \"Operacion Exitosa\",\"encontroCURP\":\"true\",\"curp\":\"VIVE870428\",\"curpStatusB\":\"\"}}}";
    public static String COLONIAS_CURP_ERROR =  "{\"status\": {\"code\": \"400\",\"description\": \"No disponible\"},\"response\": {}}";
    public static String COLONIAS_CURP_ERROR_COLONIAS = "{\"response\":{\"status\": {\"code\": \"200\",\"description\": \"Flujo exitoso\"}},\"Operaciones\":{\"consultaColonias\":{\"code\": \"400\",\"description\": \"Error no disponible\",\"cpValido\":\"\",\"entidad\": \"\",\"delegacion\": \"\",\"colonias\": []},\"consultaCurp\":{\"code\": \"200\",\"description\": \"Operacion Exitosa\",\"curpValido\":\"true\",\"nombres\": \"Isaac\",\"aPaterno\": \"Chavirez\",\"aMaterno\": \"Rodriguez\",\"sexo\": \"H\",\"fechNac\": \"31-02-1980\",\"cveEntidadNac\": \"DF\"},\"validaCurp\":{\"code\": \"200\",\"description\": \"Operacion Exitosa\",\"encontroCURP\":\"true\",\"curp\":\"VIVE870428\",\"curpStatusB\":\"\"}}}";
    public static String COLONIAS_CURP_ERROR_CONSULTACUPR = "{\"response\":{\"status\": {\"code\": \"200\",\"description\": \"Flujo exitoso\"}},\"Operaciones\":{\"consultaColonias\":{\"code\": \"200\",\"description\": \"Operacion Exitosa\",\"cpValido\":\"true\",\"entidad\": \"DF\",\"delegacion\": \"Benito Juarez\",\"colonias\": [\"claveria\",\"rosario\"]},\"consultaCurp\":{\"code\": \"400\",\"description\": \"Error no disponible\",\"curpValido\":\"\",\"nombres\": \"\",\"aPaterno\": \"\",\"aMaterno\": \"\",\"sexo\": \"\",\"fechNac\": \"\",\"cveEntidadNac\": \"\"},\"validaCurp\":{\"code\": \"200\",\"description\": \"Operacion Exitosa\",\"encontroCURP\":\"true\",\"curp\":\"VIVE870428\",\"curpStatusB\":\"\"}}}";
    public static String COLONIAS_CURP_ERROR_VALIDACURP = "{\"response\":{\"status\": {\"code\": \"200\",\"description\": \"Flujo exitoso\"}},\"Operaciones\":{\"consultaColonias\":{\"code\": \"200\",\"description\": \"Operacion Exitosa\",\"cpValido\":\"true\",\"entidad\": \"DF\",\"delegacion\": \"Benito Juarez\",\"colonias\": [\"claveria\",\"rosario\"]},\"consultaCurp\":{\"code\": \"200\",\"description\": \"Operacion Exitosa\",\"curpValido\":\"true\",\"nombres\": \"Isaac\",\"aPaterno\": \"Chavirez\",\"aMaterno\": \"Rodriguez\",\"sexo\": \"H\",\"fechNac\": \"31-02-1980\",\"cveEntidadNac\": \"DF\"},\"validaCurp\":{\"code\": \"400\",\"description\": \"Error no disponible\",\"encontroCURP\":\"\",\"curp\":\"\",\"curpStatusB\":\"\"}}}";
    public static String CONTRATO_HTML_EXITO = "{\"response\":{\"formatoHTML\": \"<html>Detalle contrato</html>\"},\"status\": {\"code\": \"200\",\"description\": \"Operacion Exitosa\"}}";
    public static String CONTRATO_HTML_ERROR = "{\"status\": {\"code\": \" 400\",\"description\": \"SERVICIO NO DISPONIBLE\"}}";
    public static String GENERA_OTP_EXITO = "{\"response\":{\"otpEnviadas\": \"True\"},\"status\": {\"code\": \"200\",\"description\": \"Operacion Exitosa\"}}";
    public static String GENETA_OTP_ERROR = "{\"status\": {\"code\": \" 400\",\"description\": \"SERVICIO NO DISPONIBLE\"}}";
    public static String VALIDA_OTP_EXITO = "{\"response\":{\"titular\":\"JOSE ANTONIO CHAIREZ TORRES\",\"tarjeta\":\"4098513014506866\",\"cuenta\":\"007400102800153184\",\"cuentaClabe\":null},\"status\":{\"code\":200,\"description\":\"Exito\"}}";
    //public static String VALIDA_OTP_EXITO_OK = "{\"response\":{\"titular\": \"Manuel Huawei Nokia\",\"tarjeta\":\"4910123412341234\",\"cuenta\":\"1234567890\",\"cuentaClabe\":\"1234567890123456789012345\"},\"status\": {\"code\": \"200\",\"description\": \"Operacion Exitosa\"}}";
    public static String VALIDA_OTP_ERROR = "{\"status\":\"400\",\"errorCode\": \" 400\",\"description\": \"SERVICIO NO DISPONIBLE\"}";
    /*public static String VALIDA_OTP_ERROR_OK="{\"description\": \"El servicio no esta disponible por el momento, intenta mas tarde. **<html><head><title>JBoss Web/7.5.7.Final-redhat-1 - JBWEB000064: Error report</title><style><!--H1 {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;font-size:22px;} H2 {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;font-size:16px;} H3 {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;font-size:14px;} BODY {font-family:Tahoma,Arial,sans-serif;color:black;background-color:white;} B {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;} P {font-family:Tahoma,Arial,sans-serif;background:white;color:black;font-size:12px;}A {color : black;}A.name {color : black;}HR {color : #525D76;}--></style> </head><body><h1>JBWEB000065: HTTP Status 404 - /TechArchitecture/mx/grantingTicket/V02</h1><HR size=\\\"1\\\" noshade=\\\"noshade\\\"><p><b>JBWEB000309: type</b> JBWEB000067: Status report</p><p><b>JBWEB000068: message</b> <u>/TechArchitecture/mx/grantingTicket/V02</u></p><p><b>JBWEB000069: description</b> <u>JBWEB000124: The requested resource is not available.</u></p><HR size=\\\"1\\\" noshade=\\\"noshade\\\"><h3>JBoss Web/7.5.7.Final-redhat-1</h3></body></html>\",\n" +
            "  \"status\": 404,\n" +
            "  \"errorCode\": \"EXAC\"\n" +
            "}";*/
    public static String VALIDA_OTP_ERROR_OK="Response 404 No encontrado\n" +
            "\n" +
            "<html>\n" +
            "    <head>\n" +
            "        <title>VMware vFabric tc Runtime 2.6.4.RELEASE/7.0.25.B.RELEASE - Informe de Error</title>\n" +
            "        <style>\n" +
            "            <!--H1 {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;font-size:22px;} H2 {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;font-size:16px;} H3 {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;font-size:14px;} BODY {font-family:Tahoma,Arial,sans-serif;color:black;background-color:white;} B {font-family:Tahoma,Arial,sans-serif;color:white;background-color:#525D76;} P {font-family:Tahoma,Arial,sans-serif;background:white;color:black;font-size:12px;}A {color : black;}A.name {color : black;}HR {color : #525D76;}-->\n" +
            "        </style> \n" +
            "    </head>\n" +
            "    <body>";


    public static String COLONIAS_EXITO = "{\"response\": {\"entidad\": \"DF\",\"delegacion\": \"Benito Juarez\",\"colonias\": [\"Narvarte\", \"Portales Sur\"]},\"status\": {\"code\": \"200\",\"description\": \"Flujo exitoso\"}}";;
    public static String CURP_EXITO = "{\"status\": {\"code\": \"200\",\"description\": \"Flujo exitoso\"},\"response\": {\"cveCURP\": \"VIVE870428HMXA00\",\"curp\": \"catajiw92013jsrhn9\",\"aPaterno\": \"Rodriguez\",\"aMaterno\": \"Pulido\",\"nombres\": \"Antonio\",\"sexo\": \"H\",\"cveEntidadNac\": \"DF\",\"fechNac\": \"29-02-1980\",\"nacionalidad\": \"MEX\",\"CurpStatus\": \"AN\"}}";



}
