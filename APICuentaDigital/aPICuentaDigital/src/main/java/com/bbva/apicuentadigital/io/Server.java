package com.bbva.apicuentadigital.io;

import android.os.AsyncTask;
import android.util.Log;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.delegates.BaseDelegate;
import com.bbva.apicuentadigital.models.ConsultaColoniasCurp;
import com.bbva.apicuentadigital.models.ConsultaColoniasNueva;
import com.bbva.apicuentadigital.models.ContratoHTML;
import com.bbva.apicuentadigital.models.OTP;
import com.bbva.apicuentadigital.models.ValidaOTP;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Hashtable;

/**
 * Created by Karina on 07/12/2015.
 */
public class Server {

    private static Server server;

    private ClienteHttp clienteHttp;

    private BaseDelegate baseDelegate;

    public Server(BaseDelegate baseDelegate){
        this.baseDelegate = baseDelegate;
        clienteHttp = new ClienteHttp();
    }

    public static Server getInstance(BaseDelegate baseDelegate){
        if(server == null)
            server = new Server(baseDelegate);
        else
            server.setBaseDelegate(baseDelegate);
        return server;
    }

    public void setBaseDelegate(BaseDelegate baseDelegate){
        this.baseDelegate = baseDelegate;
    }

    public void doNetWorkOperation(int operation, Hashtable<String, ?> params){
        switch (operation){
            case 154:
                consultaColoniasCurp(operation, params);
                break;
            case 155:
                consultaContrato(operation, params);
                break;
            case 156:
                generaOTP(operation, params);
                break;
            case 157:
                validaOTP(operation, params);
                break;
            case 158:
                consultaColonias(operation, params);
                break;
        }
    }


    private void consultaColoniasCurp(int operation, Hashtable<String, ?>params){
        String json = "";

        Hashtable<String, String> jsonObject = new Hashtable<String, String>();
        try {
            jsonObject.put(Constants.TAG_IND_RENAPO, (String )params.get(Constants.TAG_IND_RENAPO));
            jsonObject.put(Constants.TAG_CURP_, (String) params.get(Constants.TAG_CURP_));
            jsonObject.put(Constants.TAG_NOMBRES, (String) params.get(Constants.TAG_NOMBRES));
            jsonObject.put(Constants.TAG_APATERNO, (String) params.get(Constants.TAG_APATERNO));
            jsonObject.put(Constants.TAG_AMATERNO, (String) params.get(Constants.TAG_AMATERNO));
            jsonObject.put(Constants.TAG_SEXO, (String) params.get(Constants.TAG_SEXO));
            jsonObject.put(Constants.TAG_FECHANAC, (String) params.get(Constants.TAG_FECHANAC));
            jsonObject.put(Constants.TAG_CVEENTIDADNAC, (String) params.get(Constants.TAG_CVEENTIDADNAC));

            json = jsonObject.toString();

            if (ConstantsServer.ALLOW_LOG) android.util.Log.e("dora", "json: " + json);
            InvokeOperationTask task = new InvokeOperationTask(new ConsultaColoniasCurp(), operation, json, jsonObject);
            task.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void consultaContrato(int operation, Hashtable<String, ?> params){
        String json = "";

        Hashtable<String, String> jsonObject = new Hashtable<String, String>();
        try {
            jsonObject.put(Constants.TAG_NUMUERO_CELULAR, (String)params.get(Constants.TAG_NUMUERO_CELULAR));
            jsonObject.put(Constants.TAG_ID_PRODUCTO, (String)params.get(Constants.TAG_ID_PRODUCTO));
            jsonObject.put(Constants.TAG_IND_VACIO, (String)params.get(Constants.TAG_IND_VACIO));
            jsonObject.put(Constants.TAG_CONTRATO, (String)params.get(Constants.TAG_CONTRATO));

            json = jsonObject.toString();
            if(ConstantsServer.ALLOW_LOG)android.util.Log.e("dora", "json: " + json);
            InvokeOperationTask task  = new InvokeOperationTask(new ContratoHTML(), operation, json, jsonObject);
            task.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //JACT
    private void consultaColonias(int operation, Hashtable<String, ?> params){
        String json = "";

        Hashtable<String, String> jsonObject = new Hashtable<String, String>();
        try {
            jsonObject.put(Constants.TAG_CODIGO_POSTAL, (String)params.get(Constants.TAG_CODIGO_POSTAL));
            jsonObject.put("cveEntidad", (String)params.get(Constants.TAG_ESTADO));

            json = jsonObject.toString();
            if (ConstantsServer.ALLOW_LOG)
                android.util.Log.e("doraCOLONIAS", "json: "+ operation + json);

            InvokeOperationTask task = new InvokeOperationTask(new ConsultaColoniasNueva(), operation, json, jsonObject);
            task.execute();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void generaOTP (int operation, Hashtable<String, ?> params){
        String json = "";

        Hashtable<String, String> jsonObject = new Hashtable<String, String>();
        try {
            /*jsonObject.put(Constants.TAG_NUMUERO_CELULAR, (String)params.get(Constants.TAG_NUMUERO_CELULAR));
            jsonObject.put(Constants.TAG_COMPANIA, (String)params.get(Constants.TAG_COMPANIA));*/
            jsonObject.put(Constants.TAG_NUMUERO_CELULAR, (String)params.get(Constants.TAG_NUMUERO_CELULAR));//"5561437523");
            jsonObject.put(Constants.TAG_COMPANIA, (String)params.get(Constants.TAG_COMPANIA)); //"TELCEL");
            jsonObject.put(Constants.TAG_MAIL, (String)params.get(Constants.TAG_MAIL));
            jsonObject.put("curp", (String)params.get(Constants.TAG_CURP));
            jsonObject.put(Constants.TAG_CODIGO_POSTAL, (String)params.get(Constants.TAG_CODIGO_POSTAL));
            jsonObject.put(Constants.TAG_NOMBRE, (String)params.get(Constants.TAG_NOMBRE));
            jsonObject.put(Constants.TAG_PRIMER_APELLIDO, (String)params.get(Constants.TAG_PRIMER_APELLIDO));
            jsonObject.put(Constants.TAG_SEGUNDO_APELLIDO, (String)params.get(Constants.TAG_SEGUNDO_APELLIDO));
            jsonObject.put(Constants.TAG_FECHA_NACIEMIENTO, "1989-05-10");
            json = jsonObject.toString();
            if(ConstantsServer.ALLOW_LOG)android.util.Log.e("dora", "json: " + json);
            InvokeOperationTask task  = new InvokeOperationTask(new OTP(), operation, json, jsonObject);
            task.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validaOTP (int operation, Hashtable<String, ?> params){
        String json = "";

        Hashtable<String, String> jsonObject = new Hashtable<String, String>();
        try {
            jsonObject.put(Constants.TAG_NUMUERO_CELULAR, (String)params.get(Constants.TAG_NUMUERO_CELULAR));
            jsonObject.put(Constants.TAG_NOMBRES, (String)params.get(Constants.TAG_NOMBRES));
            jsonObject.put(Constants.TAG_APATERNO, (String)params.get(Constants.TAG_APATERNO));
            jsonObject.put(Constants.TAG_AMATERNO, (String)params.get(Constants.TAG_AMATERNO));
            jsonObject.put(Constants.TAG_COMPANIA, (String)params.get(Constants.TAG_COMPANIA));
            jsonObject.put(Constants.TAG_MAIL, (String)params.get(Constants.TAG_MAIL));
            jsonObject.put(Constants.TAG_CALLE, (String)params.get(Constants.TAG_CALLE));
            jsonObject.put(Constants.TAG_COLONIA, (String)params.get(Constants.TAG_COLONIA));
            jsonObject.put(Constants.TAG_NUMEXT, (String)params.get(Constants.TAG_NUMEXT));
            jsonObject.put(Constants.TAG_NUMINT, (String)params.get(Constants.TAG_NUMINT));
            jsonObject.put(Constants.TAG_CODIGO_POSTAL, (String)params.get(Constants.TAG_CODIGO_POSTAL));
            jsonObject.put(Constants.TAG_DELEGACION, (String)params.get(Constants.TAG_DELEGACION));
            jsonObject.put(Constants.TAG_ESTADO, (String)params.get(Constants.TAG_ESTADO));
            jsonObject.put(Constants.TAG_PAIS, (String)params.get(Constants.TAG_PAIS));
            jsonObject.put(Constants.TAG_CURP, (String) params.get(Constants.TAG_CURP));
            jsonObject.put(Constants.TAG_NUMEROID, (String)params.get(Constants.TAG_NUMEROID));
            jsonObject.put(Constants.TAG_OTPSMS, (String)params.get(Constants.TAG_OTPSMS));
            jsonObject.put(Constants.TAG_OTPMAIL, (String)params.get(Constants.TAG_OTPMAIL));
            jsonObject.put(Constants.TAG_IUM, (String)params.get(Constants.TAG_IUM));
            json = jsonObject.toString();
            if (ConstantsServer.ALLOW_LOG) android.util.Log.e("dora", "json: " + json);
            InvokeOperationTask task  = new InvokeOperationTask(new ValidaOTP(), operation, json, jsonObject);
            task.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void parseJSON(String response, ParsingHandler handler)  {
        ParserJSON parser = (response != null)?new ParserJSON(response):null;
        try {
            handler.process(parser);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParsingException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class InvokeOperationTask extends AsyncTask<String, Integer, String>{

        private ServerResponse serverResponse;
        private int op;
        private String json;
        private Hashtable<String, ?> paramsh;

        public InvokeOperationTask(ParsingHandler parsingHandler, int op, String json, Hashtable<String, ?> params) {
            this.op = op;
            this.json = json;
            this.paramsh = params;
            serverResponse = new ServerResponse(parsingHandler);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            String response = "";
            //ConstantsServer.SIMULATION

            try {
                response = clienteHttp.getDataFromUrlPost(op, json, paramsh);
            } catch (Exception e) {
                e.printStackTrace();
            }


            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            parseJSON(response, serverResponse);
            baseDelegate.analyzeResponse(op, serverResponse);
        }

        private void parseJSON(String response, ParsingHandler handler)  {

            ParserJSON parser = (response != null)?new ParserJSON(response):null;

            try {
                handler.process(parser);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParsingException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
