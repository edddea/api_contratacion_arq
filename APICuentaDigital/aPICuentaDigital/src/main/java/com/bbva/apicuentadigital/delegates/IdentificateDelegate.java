package com.bbva.apicuentadigital.delegates;

import android.graphics.drawable.Drawable;
import android.util.Log;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.Tools;
import com.bbva.apicuentadigital.controllers.IdentificateViewController;
import com.bbva.apicuentadigital.io.ConstantsServer;
import com.bbva.apicuentadigital.io.Server;
import com.bbva.apicuentadigital.io.ServerResponse;
import com.bbva.apicuentadigital.models.ConsultaColonias;
import com.bbva.apicuentadigital.models.ConsultaColoniasCurp;
import com.bbva.apicuentadigital.models.ConsultaColoniasNueva;
import com.bbva.apicuentadigital.models.OTP;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;
import com.google.gson.Gson;

import java.util.Hashtable;

import javax.net.ssl.SSLEngineResult;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;

/**
 * Created by Karina on 09/12/2015.
 */
public class IdentificateDelegate extends BaseDelegate {

    private CreaCuentaDelegate creaCuentaDelegate;
    private  IdentificateViewController identificateViewController;
    private APICuentaDigitalSharePreferences sharePreferences;

    private ConsultaColoniasNueva consultaColoniasNueva;
    private String[] colonias;

    public IdentificateDelegate (CreaCuentaDelegate creaCuentaDelegate, IdentificateViewController identificateViewController){
        this.creaCuentaDelegate = creaCuentaDelegate;
        this.identificateViewController = identificateViewController;
        sharePreferences = APICuentaDigitalSharePreferences.getInstance();
    }

    public void setCredencial(String credencial){
        sharePreferences.setStringData(Constants.TAG_CREDENTIAL, credencial);
    }

    public String[] getColonias() {
        return colonias;
    }


    public boolean validaDatos(String nombre, String aPaterno, String aMaterno, String fecha, String credencial, String genero, String lugar){

        boolean ok = false;

        if(!Tools.isEmptyOrNull(nombre))
           if(!Tools.isEmptyOrNull(aPaterno))
               if(!Tools.isEmptyOrNull(aMaterno))
                   if(!Tools.isEmptyOrNull(fecha))
                       if(!Tools.isEmptyOrNull(credencial) && credencial.length() == 13)
                           if(!Tools.isEmptyOrNull(genero))
                               if (!lugar.equalsIgnoreCase("Selecciona")) {
                                   sharePreferences.setStringData(Constants.TAG_NOMBRE, nombre);
                                   sharePreferences.setStringData(Constants.TAG_AMATERNO, aMaterno);
                                   sharePreferences.setStringData(Constants.TAG_APATERNO, aPaterno);
                                   sharePreferences.setStringData(Constants.TAG_FECHANAC, fecha);
                                   sharePreferences.setStringData(Constants.TAG_SEXO, genero);
                                   sharePreferences.setStringData(Constants.TAG_LUGARN, lugar);
                                   sharePreferences.setStringData(Constants.TAG_CREDENTIAL, credencial);
                                   ok = true;
                               }
        return ok;
    }

   /* public boolean validaDatos(String nombre, String aPaterno, String aMaterno, String fecha, String credencial, String genero, String lugar) {

        boolean ok = false;
        if (!Tools.isEmptyOrNull(nombre)) {
            identificateViewController.edtNombre.setBackground(identificateViewController.getResources().getDrawable(R.drawable.an_textbox));
            identificateViewController.txtNombre.setTextColor(identificateViewController.getResources().getColor(R.color.colorBlueDark));
            if (!Tools.isEmptyOrNull(aPaterno)) {
                if (!Tools.isEmptyOrNull(aMaterno)) {
                    if (!Tools.isEmptyOrNull(fecha)) {
                        if (!Tools.isEmptyOrNull(credencial) && credencial.length() == 13) {
                            if (!Tools.isEmptyOrNull(genero)) {
                                if (!lugar.equalsIgnoreCase("Selecciona")) {
                                    sharePreferences.setStringData(Constants.TAG_NOMBRE, nombre);
                                    sharePreferences.setStringData(Constants.TAG_AMATERNO, aMaterno);
                                    sharePreferences.setStringData(Constants.TAG_APATERNO, aPaterno);
                                    sharePreferences.setStringData(Constants.TAG_FECHANAC, fecha);
                                    sharePreferences.setStringData(Constants.TAG_SEXO, genero);
                                    sharePreferences.setStringData(Constants.TAG_LUGARN, lugar);
                                    sharePreferences.setStringData(Constants.TAG_CREDENTIAL, credencial);
                                    ok = true;
                                } else {

                                }
                            } else {
                                identificateViewController.txtGenero.setTextColor(identificateViewController.getResources().getColor(R.color.pink_dark));
                            }
                        } else {
                            identificateViewController.edtCredencialE.setBackground(identificateViewController.getResources().getDrawable(R.drawable.an_textboxcorto_magenta));
                            identificateViewController.txtCredencialE.setTextColor(identificateViewController.getResources().getColor(R.color.pink_dark));
                        }
                    } else {
                        identificateViewController.edtFechaN.setBackground(identificateViewController.getResources().getDrawable(R.drawable.an_textboxcorto_magenta));
                        identificateViewController.txtFechaN.setTextColor(identificateViewController.getResources().getColor(R.color.pink_dark));
                    }

                } else {
                    identificateViewController.edtAmaterno.setBackground(identificateViewController.getResources().getDrawable(R.drawable.an_textboxcorto_magenta));
                    identificateViewController.txtAmaterno.setTextColor(identificateViewController.getResources().getColor(R.color.pink_dark));
                }
            } else {
                identificateViewController.edtApaterno.setBackground(identificateViewController.getResources().getDrawable(R.drawable.an_textboxcorto_magenta));
                identificateViewController.txtApaterno.setTextColor(identificateViewController.getResources().getColor(R.color.pink_dark));
            }

        } else {
            identificateViewController.edtNombre.setBackground(identificateViewController.getResources().getDrawable(R.drawable.an_textbox_magenta));
            identificateViewController.txtNombre.setTextColor(identificateViewController.getResources().getColor(R.color.pink_dark));

        }
        return ok;
    }*/


    public boolean validaContacto(String numCelular, String correo, String correoConfirm, String compania){
        if(numCelular != null && numCelular.length() == 10){
            if(validaCorreo(correoConfirm,correo) && !compania.equalsIgnoreCase("Selecciona")){
                sharePreferences.setStringData(Constants.CELULAR, numCelular);
                sharePreferences.setStringData(Constants.TAG_MAIL, correo);
                sharePreferences.setStringData(Constants.COMPANIA, compania);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public boolean validaCorreo(String correoConfirm, String correo){
        if(isValidEmail(correo)) {
            if (correo.equalsIgnoreCase(correoConfirm)) {
                sharePreferences.setStringData(Constants.TAG_MAIL, correo);
                return true;
            } else {
                return false;
            }
        }else{
            return false;
        }
    }

    private boolean isValidEmail(String email) {
        if (Tools.isEmptyOrNull(email)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
        }
    }

    public boolean validaDomicilio(String calle, String numExt, String numInt, String colonia){

        if(!Tools.isEmptyOrNull(calle) && !Tools.isEmptyOrNull(numExt) && !colonia.equalsIgnoreCase("Selecciona")){
            sharePreferences.setStringData(Constants.TAG_CALLE, calle);
            sharePreferences.setStringData(Constants.TAG_NUMEXT, numExt);
            sharePreferences.setStringData(Constants.TAG_NUMINT, numInt);
            sharePreferences.setStringData(Constants.TAG_COLONIA, colonia);
            return true;
        }else
            return false;
    }

    public void realizaOperacion(String numCelular, String compania, String mail){

        //creaCuentaDelegate.getCreaCuentaView().showActivityIndicator(identificateViewController.getResources().getString(R.string.alert_operation),
          //      identificateViewController.getResources().getString(R.string.alert_connecting),creaCuentaDelegate.getCreaCuentaView());

        creaCuentaDelegate.getCreaCuentaView().showActivityIndicator();
        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(Constants.TAG_NUMUERO_CELULAR,numCelular);
        params.put(Constants.TAG_COMPANIA, compania);
        params.put(Constants.TAG_MAIL,mail);
        params.put(Constants.TAG_CURP,sharePreferences.getStringData(Constants.TAG_CURP));
        params.put(Constants.TAG_CODIGO_POSTAL,sharePreferences.getStringData(Constants.TAG_CODIGO_POSTAL));
        params.put(Constants.TAG_NOMBRE,sharePreferences.getStringData(Constants.TAG_NOMBRE));
        params.put(Constants.TAG_PRIMER_APELLIDO,sharePreferences.getStringData(Constants.TAG_APATERNO));
        params.put(Constants.TAG_SEGUNDO_APELLIDO,sharePreferences.getStringData(Constants.TAG_AMATERNO));
        params.put(Constants.TAG_FECHA_NACIEMIENTO,sharePreferences.getStringData(Constants.TAG_FECHANAC));

        Server server = Server.getInstance(this);
        server.doNetWorkOperation(ApiConstants.CONSULTA_GENERA_OTP, params);
    }

    public void realizaOperacion(String nombre, String aPaterno, String aMaterno, String fecha, String lugar, String sexo){


        creaCuentaDelegate.getCreaCuentaView().showActivityIndicator();


        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(Constants.TAG_IND_CP,"FALSE");
        params.put(Constants.TAG_IND_RENAPO, "Datos");
        params.put(Constants.TAG_CODIGO_POSTAL,sharePreferences.getStringData(Constants.TAG_CODIGO_POSTAL));
        params.put(Constants.TAG_CURP_,"");
        params.put(Constants.TAG_NOMBRES, nombre);
        params.put(Constants.TAG_APATERNO,aPaterno);
        params.put(Constants.TAG_AMATERNO,aMaterno);
        params.put(Constants.TAG_SEXO,Tools.getCveSexo(sexo));
        params.put(Constants.TAG_FECHANAC, Tools.fortmatFechaPeticion(fecha));
        params.put(Constants.TAG_CVEENTIDADNAC,Tools.cveEstado(lugar));

        Server server = Server.getInstance(this);
        server.doNetWorkOperation(ApiConstants.CONSULTA_CURP, params);
    }

    //JACT
    public void realizaOperacion(String aCodigoPostal, String acveEstado){

        creaCuentaDelegate.getCreaCuentaView().showActivityIndicator();

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(Constants.TAG_CODIGO_POSTAL, aCodigoPostal);
        params.put(Constants.TAG_ESTADO, Tools.cveEstadoEntidad(acveEstado));

        Server server = Server.getInstance(this);
        server.doNetWorkOperation(ApiConstants.CONSULTA_COLONIAS, params);

    }


    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        creaCuentaDelegate.getCreaCuentaView().hideActivityIndicator();

        if(ApiConstants.CONSULTA_GENERA_OTP == operationId) {
            if (response.getCODE().equalsIgnoreCase(response.CODE_OK)) {
                if(ConstantsServer.ALLOW_LOG)
                    android.util.Log.e("Dora", "analyzeResponse");
                //OTP otp = (OTP) response.getResponse();
                if(ConstantsServer.ALLOW_LOG)
                    android.util.Log.e("Dora", "analyzeResponse ");
                creaCuentaDelegate.getCreaCuentaView().clausulas();
            } else {
                if(ConstantsServer.ALLOW_LOG)
                    android.util.Log.e("Dora", "analyzeResponse error");
                String mesagge="";
                int asFinal;
                asFinal= response.getMESSAGE().indexOf("**");
                if(asFinal >-1){
                    mesagge= response.getMESSAGE().substring(0,asFinal);
                }else{
                    mesagge= response.getMESSAGE();
                }
                creaCuentaDelegate.getCreaCuentaView().showInformationAlert("Status "+response.getCODE()+"\n"+mesagge);
            }
        }else if(ApiConstants.CONSULTA_CURP == operationId){
            if (response.getCODE().equalsIgnoreCase(response.CODE_OK)) {
                android.util.Log.e("Dora", "analyzeResponse");
                ConsultaColoniasCurp consultaColoniasCurp = (ConsultaColoniasCurp)response.getResponse();
                if(consultaColoniasCurp.getConsultaCurp().getCode().equalsIgnoreCase(response.CODE_OK)) {
                    if(ConstantsServer.ALLOW_LOG)android.util.Log.e("Dora", "analyzeResponse ok curp");
                    sharePreferences.setStringData(Constants.TAG_CURP, consultaColoniasCurp.getConsultaCurp().getCurp());
                    identificateViewController.generaOTP();
                }else{

                    creaCuentaDelegate.getCreaCuentaView().setHabilitado(true);

                    creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.error_curp);
                }
            } else {
                String mesagge="";
                int asFinal;
                asFinal= response.getMESSAGE().indexOf("**");
                if(asFinal >-1){
                    mesagge= response.getMESSAGE().substring(0,asFinal);
                }else{
                    mesagge= response.getMESSAGE();
                }
                creaCuentaDelegate.getCreaCuentaView().showInformationAlert("Status "+response.getCODE()+"\n"+mesagge);
                if(ConstantsServer.ALLOW_LOG)android.util.Log.e("Dora", "analyzeResponse error");
            }//JACT
        }else if (ApiConstants.CONSULTA_COLONIAS == operationId){
            if (response.getCODE().equalsIgnoreCase(response.CODE_OK)) {
                ConsultaColoniasNueva consultaColonias = (ConsultaColoniasNueva)response.getResponse();
                consultaColonias.getConsultaColonias().setCode("200");

                if(consultaColonias.getConsultaColonias().getCode().equalsIgnoreCase(response.CODE_OK)) {
                    if(ConstantsServer.ALLOW_LOG)android.util.Log.e("Dora", "analyzeResponse ok Colonias");
                    //sharePreferences.setStringData(Constants.TAG_COLONIAS, consultaColonias.get);
                    Gson gson = new Gson();
                    identificateViewController.desSpinner();
                    identificateViewController.irSetColoniasEstado(gson.toJson(consultaColonias));
                }else{

                    creaCuentaDelegate.getCreaCuentaView().setHabilitado(true);

                    creaCuentaDelegate.getCreaCuentaView().showInformationAlert(R.string.error_curp);

                }
            } else {
                String mesagge="";
                int asFinal;
                asFinal= response.getMESSAGE().indexOf("**");
                if(asFinal >-1){
                    mesagge= response.getMESSAGE().substring(0,asFinal);
                }else{
                    mesagge= response.getMESSAGE();
                }
                creaCuentaDelegate.getCreaCuentaView().showInformationAlert("Status "+response.getCODE()+"\n"+mesagge);
                if(ConstantsServer.ALLOW_LOG)android.util.Log.e("Dora", "analyzeResponse error");
            }
        }
    }

    public void obtenerInformacionColonias(ConsultaColoniasNueva consultaColoniasNueva){
        this.consultaColoniasNueva = consultaColoniasNueva;

        ConsultaColonias consultaColonias = consultaColoniasNueva.getConsultaColonias();

        if(consultaColonias.getCode().equalsIgnoreCase(Constants.CODE_OK)){
            colonias = consultaColonias.getColonias();
            sharePreferences.setStringData(Constants.TAG_DELEGACION, consultaColonias.getDelegacion());
            sharePreferences.setStringData(Constants.TAG_ENTIDAD, consultaColonias.getEntidad());
        }
    }


}
