package com.bbva.apicuentadigital.models;

/**
 * Created by Karina on 07/12/2015.
 */
public class ConsultaColonias {

    private String code;
    private String description;
    private String cpValido;
    private String entidad;
    private String delegacion;
    private String [] colonias;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCpValido() {
        return cpValido;
    }

    public void setCpValido(String cpValido) {
        this.cpValido = cpValido;
    }

    public String getEntidad() {
        return entidad;
    }

    public void setEntidad(String entidad) {
        this.entidad = entidad;
    }

    public String getDelegacion() {
        return delegacion;
    }

    public void setDelegacion(String delegacion) {
        this.delegacion = delegacion;
    }

    public String[] getColonias() {
        return colonias;
    }

    public void setColonias(String[] colonias) {
        this.colonias = colonias;
    }
}
