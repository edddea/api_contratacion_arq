package com.bbva.apicuentadigital.controllers;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.delegates.CreaCuentaDelegate;
import com.bbva.apicuentadigital.delegates.DatosConfirmadosDelegate;

/**
 * Created by Karina on 14/12/2015.
 */
public class DatosConfirmadosViewController extends Fragment implements View.OnClickListener{

    private View v;
    private GuiTools guiTools;
    private CreaCuentaDelegate creaCuentaDelegate;
    private DatosConfirmadosDelegate datosConfirmadosDelegate;

    private TextView txtNombre;
    private TextView txtCredencial;
    private TextView txtFecha;
    private TextView txtLugar;
    private TextView txtGenero;
    private TextView txtCeular;
    private TextView txtCompania;
    private TextView txtEmail;
    private TextView txtCP;
    private TextView txtEstado;
    private TextView txtDelegacion;
    private TextView txtColonia;
    private TextView txtCalle;
    private TextView txtNoExt;
    private TextView txtNoInt;

    private ImageButton imbContinuar;

    public DatosConfirmadosViewController(GuiTools guiTools, CreaCuentaDelegate creaCuentaDelegate){
        this.creaCuentaDelegate = creaCuentaDelegate;
        this.guiTools = guiTools;
        datosConfirmadosDelegate = new DatosConfirmadosDelegate(creaCuentaDelegate, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.layout_datos_confirmados, container, false);

        init();
        return v;
    }

    private void init(){
        findViewById();
        scaleview();
        datosConfirmadosDelegate.obtenerInformacion();
    }

    private void findViewById(){
        txtCalle = (TextView)v.findViewById(R.id.txt_calle);
        txtCeular = (TextView)v.findViewById(R.id.txt_celular);
        txtColonia = (TextView)v.findViewById(R.id.txt_colonia);
        txtCompania = (TextView)v.findViewById(R.id.txt_company);
        txtCP = (TextView)v.findViewById(R.id.txt_cp);
        txtCredencial = (TextView)v.findViewById(R.id.txt_credencial);
        txtDelegacion = (TextView)v.findViewById(R.id.txt_delegacion);
        txtEmail = (TextView)v.findViewById(R.id.txt_email);
        txtEstado = (TextView)v.findViewById(R.id.txt_estado);
        txtFecha = (TextView)v.findViewById(R.id.txt_fecha);
        txtGenero = (TextView)v.findViewById(R.id.txt_genero);
        txtLugar = (TextView)v.findViewById(R.id.txt_lugar);
        txtNoExt = (TextView)v.findViewById(R.id.txt_no_ext);
        txtNoInt = (TextView)v.findViewById(R.id.txt_no_int);
        txtNombre = (TextView)v.findViewById(R.id.txt_nombre_titular);
        imbContinuar = (ImageButton)v.findViewById(R.id.btn_continue);
        imbContinuar.setOnClickListener(this);
    }

    private void scaleview(){
        guiTools.scale(txtCalle, true);
        guiTools.scale(txtCeular, true);
        guiTools.scale(txtColonia, true);
        guiTools.scale(txtCompania, true);
        guiTools.scale(txtCP, true);
        guiTools.scale(txtCredencial, true);
        guiTools.scale(txtDelegacion, true);
        guiTools.scale(txtEmail, true);
        guiTools.scale(txtEstado, true);
        guiTools.scale(txtFecha, true);
        guiTools.scale(txtGenero, true);
        guiTools.scale(txtLugar, true);
        guiTools.scale(txtNoExt, true);
        guiTools.scale(txtNoInt, true);
        guiTools.scale(txtNombre, true);
        guiTools.scale(v.findViewById(R.id.txt_name_item_datos), true);
        guiTools.scale(v.findViewById(R.id.txt_datos_contacto), true);
        guiTools.scale(v.findViewById(R.id.txt_datos_domicilio), true);
        guiTools.scale(v.findViewById(R.id.lbl_celular), true);
        guiTools.scale(v.findViewById(R.id.lbl_company), true);
        guiTools.scale(v.findViewById(R.id.lbl_email), true);
        guiTools.scale(v.findViewById(R.id.lbl_nombre_titular), true);
        guiTools.scale(v.findViewById(R.id.lbl_credencial), true);
        guiTools.scale(v.findViewById(R.id.lbl_fecha), true);
        guiTools.scale(v.findViewById(R.id.lbl_lugar), true);
        guiTools.scale(v.findViewById(R.id.lbl_genero), true);
        guiTools.scale(v.findViewById(R.id.lbl_cp), true);
        guiTools.scale(v.findViewById(R.id.lbl_estado), true);
        guiTools.scale(v.findViewById(R.id.lbl_delegacion), true);
        guiTools.scale(v.findViewById(R.id.lbl_colonia), true);
        guiTools.scale(v.findViewById(R.id.lbl_calle), true);
        guiTools.scale(v.findViewById(R.id.lbl_no_ext), true);
        guiTools.scale(v.findViewById(R.id.lbl_no_int), true);
    }

    @Override
    public void onClick(View v) {
        if(imbContinuar == v){
            creaCuentaDelegate.getCreaCuentaView().cargarFragment();
        }
    }

    public void llenarDatosPersonales(String nombre, String credencial, String fecha, String lugar, String genero){
        txtNombre.setText(nombre);
        txtCredencial.setText(credencial);
        /*
        txtFecha.setText(fecha);
        txtLugar.setText(lugar);
        txtGenero.setText(genero);
        */
    }

    public void llenarDatosContacto(String celular, String compania, String email){
        txtCeular.setText(celular);
        txtCompania.setText(compania);
        txtEmail.setText(email);
    }

    public void llenarDomicilio(String cp, String estado, String delegacion, String colonia, String calle, String numExxt, String numInt){
        txtCP.setText(cp);
        txtEstado.setText(estado);
        txtDelegacion.setText(delegacion);
        txtColonia.setText(colonia);
        txtCalle.setText(calle);
        txtNoExt.setText(numExxt);
        txtNoInt.setText(numInt);
    }

}
