package com.bbva.apicuentadigital.delegates;

import android.util.Log;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.Tools;
import com.bbva.apicuentadigital.io.ConstantsServer;
import com.bbva.apicuentadigital.io.Server;
import com.bbva.apicuentadigital.io.ServerResponse;
import com.bbva.apicuentadigital.mbanking.BanderasBMovilFileManager;
import com.bbva.apicuentadigital.mbanking.PropertiesManager;
import com.bbva.apicuentadigital.models.OTP;
import com.bbva.apicuentadigital.models.ValidaOTP;
import com.bbva.apicuentadigital.persistence.APICuentaDigitalSharePreferences;

import java.util.Hashtable;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;

/**
 * Created by Karina on 09/12/2015.
 */
public class ConfirmacionDelegate extends BaseDelegate{

    private CreaCuentaDelegate creaCuentaDelegate;
    private APICuentaDigitalSharePreferences sharePreferences;

    public ConfirmacionDelegate(CreaCuentaDelegate creaCuentaDelegate){
        this.creaCuentaDelegate = creaCuentaDelegate;
        sharePreferences = APICuentaDigitalSharePreferences.getInstance();
    }

    public void realizaOperacion(String otpSMS,String otpMail){

        String newOtpSms = otpSMS.substring(0,4);
        String newOtpMail = otpSMS.substring(4, otpSMS.length());
        creaCuentaDelegate.getCreaCuentaView().showActivityIndicator();

        String ium = Tools.buildIUM(sharePreferences.getStringData(Constants.CELULAR));
        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(Constants.TAG_NUMUERO_CELULAR,sharePreferences.getUser());
        params.put(Constants.TAG_NOMBRES,sharePreferences.getStringData(Constants.TAG_NOMBRE));
        params.put(Constants.TAG_APATERNO,sharePreferences.getStringData(Constants.TAG_APATERNO));
        params.put(Constants.TAG_AMATERNO,sharePreferences.getStringData(Constants.TAG_AMATERNO));//JACT
        params.put(Constants.TAG_COMPANIA, sharePreferences.getStringData(Constants.COMPANIA).toUpperCase());
        params.put(Constants.TAG_MAIL, sharePreferences.getStringData(Constants.TAG_MAIL));
        params.put(Constants.TAG_CALLE, sharePreferences.getStringData(Constants.TAG_CALLE));
        params.put(Constants.TAG_COLONIA, sharePreferences.getStringData(Constants.TAG_COLONIA));
        params.put(Constants.TAG_NUMEXT, sharePreferences.getStringData(Constants.TAG_NUMEXT));
        params.put(Constants.TAG_NUMINT, sharePreferences.getStringData(Constants.TAG_NUMINT));
        params.put(Constants.TAG_CODIGO_POSTAL,sharePreferences.getStringData(Constants.TAG_CODIGO_POSTAL));
        params.put(Constants.TAG_DELEGACION,sharePreferences.getStringData(Constants.TAG_DELEGACION));
        params.put(Constants.TAG_ESTADO,sharePreferences.getStringData(Constants.TAG_ENTIDAD));
        params.put(Constants.TAG_PAIS,Constants.MEXICO);
        params.put(Constants.TAG_CURP,sharePreferences.getStringData(Constants.TAG_CURP));
        params.put(Constants.TAG_NUMEROID, sharePreferences.getStringData(Constants.TAG_CREDENTIAL));
        params.put(Constants.TAG_OTPSMS, newOtpSms);
        params.put(Constants.TAG_OTPMAIL, newOtpMail);
        params.put(Constants.TAG_IUM, ium);

        int n=sharePreferences.getIntData(Constants.CONT);
        n=n+1;
        Log.e("MAPE","inicial"+n);

        sharePreferences.setIntData(Constants.CONT, n++);

        Log.e("MAPE","Despues de... cont" + sharePreferences.getIntData(Constants.CONT));

        if(sharePreferences.getIntData(Constants.CONT)==4){
            creaCuentaDelegate.getCreaCuentaView().showInformationAlert("Superado el numero de intentos");
            creaCuentaDelegate.quieroCuenta();

        }else{
            Server server = Server.getInstance(this);
            server.doNetWorkOperation(ApiConstants.CONSULTA_VALIDA_OTP, params);
        }
    }

    public void generaOTP(){
        realizaOperacion(sharePreferences.getStringData(Constants.CELULAR), sharePreferences.getStringData(Constants.COMPANIA), sharePreferences.getStringData(Constants.TAG_MAIL));
    }

    public void realizaOperacion(String numCelular, String compania, String mail){

        creaCuentaDelegate.getCreaCuentaView().showActivityIndicator();
        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(Constants.TAG_NUMUERO_CELULAR,numCelular);
        params.put(Constants.TAG_COMPANIA, compania);
        params.put(Constants.TAG_MAIL,mail);
        params.put(Constants.TAG_CURP,sharePreferences.getStringData(Constants.TAG_CURP));
        params.put(Constants.TAG_CODIGO_POSTAL,sharePreferences.getStringData(Constants.TAG_CODIGO_POSTAL));
        params.put(Constants.TAG_NOMBRE,sharePreferences.getStringData(Constants.TAG_NOMBRE));
        params.put(Constants.TAG_PRIMER_APELLIDO,sharePreferences.getStringData(Constants.TAG_APATERNO));
        params.put(Constants.TAG_SEGUNDO_APELLIDO,sharePreferences.getStringData(Constants.TAG_AMATERNO));
        params.put(Constants.TAG_FECHA_NACIEMIENTO,sharePreferences.getStringData(Constants.TAG_FECHANAC));

       /* params.put(Constants.TAG_NUMUERO_CELULAR,"5561437523");
        params.put(Constants.TAG_COMPANIA, "TELCEL");
        params.put(Constants.TAG_MAIL,"erika.manriquep@gmail.com");
        params.put(Constants.TAG_CURP,"mape900605mdfnlr01");
        params.put(Constants.TAG_NOMBRE,"Erika");
        params.put(Constants.TAG_PRIMER_APELLIDO,"manrique");
        params.put(Constants.TAG_SEGUNDO_APELLIDO,"plascencia");
        params.put(Constants.TAG_FECHA_NACIEMIENTO,"1990-06-05");*/

        int n=sharePreferences.getIntData(Constants.CONTGOTP);
        n=n+1;
        Log.e("MAPE", "inicialOTP"+n);

        sharePreferences.setIntData(Constants.CONTGOTP, n++);

        Log.e("MAPE","Despues de... contOTP" + sharePreferences.getIntData(Constants.CONTGOTP));

        if(sharePreferences.getIntData(Constants.CONTGOTP)==4){
            creaCuentaDelegate.getCreaCuentaView().showInformationAlert("Superado el numero de intentos");
            creaCuentaDelegate.quieroCuenta();

        }else {
            Server server = Server.getInstance(this);
            server.doNetWorkOperation(ApiConstants.CONSULTA_GENERA_OTP, params);
        }
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        creaCuentaDelegate.getCreaCuentaView().hideActivityIndicator();
        if(operationId == ApiConstants.CONSULTA_VALIDA_OTP) {
            if (response.getCODE().equalsIgnoreCase(response.CODE_OK)) {
                if(ConstantsServer.ALLOW_LOG)android.util.Log.e("Dora", "analyzeResponse");
                sharePreferences.setIntData(Constants.CONT, 0);
                ValidaOTP validaOTP = (ValidaOTP) response.getResponse();
                //guardarBanderas();
                creaCuentaDelegate.setValidaOTP(validaOTP);
                sharePreferences.setBooleanData(Constants.CONFIRMA, false);
                creaCuentaDelegate.getCreaCuentaView().exito();

            } else {
                String mesagge="";
                int asFinal;
                asFinal= response.getMESSAGE().indexOf("**");
                if(asFinal >-1){
                  mesagge= response.getMESSAGE().substring(0,asFinal);
                }else{
                    mesagge= response.getMESSAGE();
                }
                creaCuentaDelegate.getCreaCuentaView().showInformationAlert("Status "+response.getCODE()+"\n"+mesagge);
                if(ConstantsServer.ALLOW_LOG)android.util.Log.e("Dora", "analyzeResponse error");
            }
        }else if(operationId == ApiConstants.CONSULTA_GENERA_OTP){
            if(response.getCODE().equalsIgnoreCase(response.CODE_OK)) {
                if(ConstantsServer.ALLOW_LOG)android.util.Log.e("Dora", "analyzeResponse");
                sharePreferences.setIntData(Constants.CONTGOTP, 0);
                OTP otp = (OTP)response.getResponse();
            }else{
                String mesagge="";
                int asFinal;
                asFinal= response.getMESSAGE().indexOf("**");
                if(asFinal >-1){
                    mesagge= response.getMESSAGE().substring(0,asFinal);
                }else{
                    mesagge= response.getMESSAGE();
                }
                creaCuentaDelegate.getCreaCuentaView().showInformationAlert("Status "+response.getCODE()+"\n"+mesagge);
                if(ConstantsServer.ALLOW_LOG)android.util.Log.e("Dora", "analyzeResponse error");
            }
        }
    }

    private void guardarBanderas(){
        BanderasBMovilFileManager.getCurrent().setBanderasContratarCDigitat(false);

        PropertiesManager.getCurrent().setBmovilActivated(true);
    }

}
