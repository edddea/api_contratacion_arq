package com.bbva.apicuentadigital.io;

import android.os.Build;
import android.os.StrictMode;
import android.util.Log;

import com.bbva.apicuentadigital.common.APICuentaDigital;
import com.bbva.apicuentadigital.models.OTP;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Map;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.commons.PojoGeneral;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;
import suitebancomer.aplicaciones.commservice.service.ICommService;


/**
 * Created by Karina on 07/12/2015.
 */
public class ClienteHttp {
    private ParametersTO parameters;

    public String getDataFromUrlPost(int operation, String params, Hashtable<String, ?> paramsh) throws Exception {

        final int op = operation;
        String stringResponse = "";
        String url = "";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects().detectLeakedClosableObjects().penaltyLog().penaltyDeath().build());
        }

        if (ConstantsServer.DEVELOPMENT) {
            url = ConstantsServer.BASE_URL_TEST + ConstantsServer.BASE_CONSUMOS + ApiConstants.OPERATION_CODES[op];
        } else {
            url = ConstantsServer.BASE_URL_PRODUCCION + ConstantsServer.BASE_CONSUMOS + ApiConstants.OPERATION_CODES[op];
        }

        Log.e("URL", url + params);
           /* BufferedReader in = null;
            DefaultHttpClient httpClient;
            StringBuffer sb = new StringBuffer();
            try{
                httpClient = CustomHttpClient.getHttpClient();
                HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), false); //making 3G network works*
                HttpPost request = new HttpPost(url);
                request.addHeader("Content-type", "application/json");
                request.setEntity(new StringEntity(params));
                HttpResponse response = httpClient.execute(request);
                in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
                String line = "";
                String NL = System.getProperty("line.separator");
                while ((line = in.readLine()) != null) {
                    sb.append(line + NL);
                }
                in.close();*/
        final Integer opId = operation;
        parameters = new ParametersTO();
        parameters.setSimulation(ConstantsServer.SIMULATION);
        if (!ConstantsServer.SIMULATION) {
            parameters.setProduction(!ConstantsServer.DEVELOPMENT);
            parameters.setDevelopment(ConstantsServer.DEVELOPMENT);
        }
        parameters.setOperationId(opId.intValue());
        //obtener datos
        final Hashtable<String, String> mapa = new Hashtable<String, String>();
        for (final Map.Entry<String, ?> entry : paramsh.entrySet()) {
            final String key = entry.getKey();
            final Object value = entry.getValue();
            mapa.put(key, (String) (value == null ? "" : value));
        }
        //
        parameters.setParameters(mapa);
        parameters.setJson(true);
        parameters.setIsJsonValue(ApiConstants.isJsonValueCode.NONE);
        IResponseService resultado = new ResponseServiceImpl();
        final ICommService serverproxy = new CommServiceProxy(APICuentaDigital.appContext);
        try {
            /*serverproxy.request(parameters,new PojoGeneral().getClass());
            resultado = serverproxy.request(parameters, new OTP().getClass());*/
            resultado = serverproxy.request(parameters,new PojoGeneral().getClass());
        } catch (UnsupportedEncodingException e) {
            throw new Exception(e);
        } catch (IllegalStateException e) {
            throw new Exception(e);
        } catch (IOException e) {
            throw new Exception(e);
        } catch (JSONException e) {
            throw new Exception(e);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //stringResponse = sb.toString();
        Log.w("Respuesta", resultado.getResponseString());
        return resultado.getResponseString();
    }

    public String getDataFromUrlGet(int operation, String params) {
        final int op = operation;
        String cadenaRespuesta = "";
        /** String cadena = "";
         String url ="";
         HttpClient httpclient = new DefaultHttpClient();
         HttpGet httpGet = null;
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
         StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
         .detectDiskReads().detectDiskWrites().detectNetwork()
         .penaltyLog().build());

         StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
         .detectLeakedSqlLiteObjects().detectLeakedClosableObjects()
         .penaltyLog().penaltyDeath().build());
         }

         try {
         if(ConstantsServer.DEVELOPMENT){
         url = ConstantsServer.BASE_URL_TEST + ConstantsServer.BASE_CONSUMOS +
         ConstantsServer.SERVICIOS[op];
         }else{
         url = ConstantsServer.BASE_URL_PRODUCCION + ConstantsServer.BASE_CONSUMOS +
         ConstantsServer.SERVICIOS[op];
         }
         Log.e("URL", url);
         httpGet = new HttpGet(url);
         HttpResponse httpResponse = httpclient.execute(httpGet);
         InputStream inputStream = httpResponse.getEntity().getContent();

         BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
         String line = "";
         while((line = bufferedReader.readLine()) != null) {
         cadenaRespuesta += line;
         }
         inputStream.close();

         Log.w("getStatus",cadenaRespuesta);

         } catch (UnsupportedEncodingException e) {
         e.printStackTrace();
         } catch (IOException ex){
         ex.printStackTrace();
         }

         **/
        return cadenaRespuesta;
    }
}
