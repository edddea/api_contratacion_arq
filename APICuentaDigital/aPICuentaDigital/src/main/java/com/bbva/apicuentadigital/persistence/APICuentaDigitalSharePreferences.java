package com.bbva.apicuentadigital.persistence;

import android.content.Context;
import android.content.SharedPreferences;

import com.bbva.apicuentadigital.common.APICuentaDigital;
import com.bbva.apicuentadigital.common.Constants;

/**
 * Created by Karina on 09/12/2015.
 */
public class APICuentaDigitalSharePreferences {

    private SharedPreferences sharedPreferences = null;

    private static APICuentaDigitalSharePreferences theInstance = null;

    private APICuentaDigitalSharePreferences(){
        sharedPreferences =  APICuentaDigital.appContext.getSharedPreferences(Constants.SHARED_NAME, Context.MODE_PRIVATE);

    }

    private synchronized static void createInstance() {
        if (theInstance == null) {
            theInstance = new APICuentaDigitalSharePreferences();
        }
    }

    public static APICuentaDigitalSharePreferences getInstance() {
        if (theInstance == null) createInstance();
        return theInstance;
    }

    public void setStringData(String value, String data){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(value, data);
        editor.commit();
    }

    public String getUser(){
        return sharedPreferences.getString(Constants.CELULAR, "");
    }

    public Long getSeed(){
        return sharedPreferences.getLong(Constants.SEED, 0);
    }

    public void setLongData(String value, long data){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong(value, data);
        editor.commit();
    }

    public long getLongData(String value){
        long resp = 0;

        try{
            resp = sharedPreferences.getLong(value,0);
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return resp;
    }

    public void setBooleanData(String value, boolean data){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(value, String.valueOf(data));
        editor.commit();
    }

    public boolean getBooleanData(String value){
        String resp = "";
        try {
            resp = sharedPreferences.getString(value, "");
        } catch (Exception e) {
        }

        return Boolean.parseBoolean(resp);
    }

    public String getStringData(String value){
        String resp = "";
        try {
            resp = sharedPreferences.getString(value, "");
        } catch (Exception e) {
        }

        return resp;
    }

    public void setIntData(String value, int data) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(value, data);
        editor.commit();
    }

    public int getIntData(String value){
        int resp = 0;

        try {
            resp = sharedPreferences.getInt(value, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resp;
    }

    public void deleteData(String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(value);
        editor.commit();
    }

    public void deleteAll(){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }



}
