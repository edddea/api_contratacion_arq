package com.bbva.apicuentadigital.models;

import android.util.Log;

import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.io.ParserJSON;
import com.bbva.apicuentadigital.io.ParsingException;
import com.bbva.apicuentadigital.io.ParsingHandler;

import java.io.IOException;

/**
 * Created by Karina on 08/12/2015.
 */
public class OTP implements ParsingHandler{

    private String otp;

    public String getOtp() {
        return otp;
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        if(parser.hasValue(Constants.TAG_RESPONSE))
            parser = new ParserJSON(parser.parserNextObject(Constants.TAG_RESPONSE).toString());

        if(parser.hasValue(Constants.TAG_OTP_ENVIADAS)) {
            otp = parser.parseNextValue(Constants.TAG_OTP_ENVIADAS).toString();
        }

    }
}
