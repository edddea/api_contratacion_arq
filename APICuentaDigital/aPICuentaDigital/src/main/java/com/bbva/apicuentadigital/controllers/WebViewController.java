package com.bbva.apicuentadigital.controllers;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.Constants;
import com.bbva.apicuentadigital.common.GuiTools;
import com.bbva.apicuentadigital.io.ConstantsServer;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import bancomer.api.common.timer.TimerController;
import tracking.TrackingHelperCuentaDigital;

/**
 * Created by DMEJIA on 17/12/15.
 */
public class WebViewController extends Activity{

    private WebView wvpoliza;
    private TextView lblTitulo;

    private GestureDetector gestureDetector;

    private String operation;
    private String url;

    private AtomicBoolean mPreventAction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_web_view);
        operation = (String)this.getIntent().getExtras().get(Constants.OPERATION);
        url = (String)this.getIntent().getExtras().get(Constants.URL);
        init();
    }


    private void init(){
        mPreventAction = new AtomicBoolean(false);
        findViewById();
        scaleView();
        selectOperation();
        etiquetado();
    }

    private void etiquetado(){
        ArrayList<String> list= new ArrayList<String>();
        list.add(Constants.CONTRATO_CUENTA);
        TrackingHelperCuentaDigital.trackState(list);
        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackingHelperCuentaDigital.trackEntrada(eventoEntrada);
    }

    private void findViewById(){
        lblTitulo = (TextView)findViewById(R.id.title_text);
        wvpoliza = (WebView)findViewById(R.id.poliza);
    }

    private void scaleView(){
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(wvpoliza);
        guiTools.scale(lblTitulo, true);
    }

    private void selectOperation(){
        switch (operation){
            case Constants.TERMINOS:
                lblTitulo.setText(getResources().getString(R.string.consulta_terminos));
                break;
            case Constants.CONTRATO:
                lblTitulo.setText(getResources().getString(R.string.consulta_terminos));
                break;
        }
        showWebView(url);
    }

    private String leerArchivo(String url){
        String leerHTML = "";
        try {
            FileInputStream fIn = new FileInputStream(url);
            InputStreamReader archivo = new InputStreamReader(fIn);
            BufferedReader br = new BufferedReader(archivo);
            String linea = br.readLine();
            while (linea != null) {
                leerHTML += linea + "";
                linea = br.readLine();
            }
            br.close();
            archivo.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return leerHTML;
    }

    private void showWebView(String html){
       // html =leerArchivo(html);
        wvpoliza.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        //wvpoliza.getSettings().setBuiltInZoomControls(true);
        //wvpoliza.getSettings().setJavaScriptEnabled(true);
       // double factor = GuiTools.getScaleFactor();
       // wvpoliza.setInitialScale((int) (122 * factor));
        //wvpoliza.loadData(html, "text/html", "utf-8");

        String URLPDF = "http://www.bancomer.com/fbin/contrato-cuenta-multinivel_tcm1344-487163.pdf";

        wvpoliza.getSettings().setJavaScriptEnabled(true);
        wvpoliza.getSettings().setPluginState(WebSettings.PluginState.ON);

        wvpoliza.setWebViewClient(new Callback());
        wvpoliza.loadUrl("http://docs.google.com/gview?embedded=true&url=" +URLPDF);


        gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                mPreventAction.set(true);
                return true;
            }
            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                mPreventAction.set(true);
                return true;
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                mPreventAction.set(true); // this is the key! this would block double tap to zoom to fire
                return false;
            }

        });
        gestureDetector.setIsLongpressEnabled(true);
        wvpoliza.setOnTouchListener(new View.OnTouchListener() {
            @TargetApi(Build.VERSION_CODES.FROYO)
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int pointId = 0;
                if (Build.VERSION.SDK_INT > 7) {
                    int index = (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                    pointId = event.getPointerId(index);
                }
                if (pointId == 0) {
                    gestureDetector.onTouchEvent(event);
                    if (mPreventAction.get()) {
                        mPreventAction.set(false);
                        return true;
                    }
                    return wvpoliza.onTouchEvent(event);
                } else return true;
            }
        });
    }

    private class Callback extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(
                WebView view, String url) {
            return(false);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        float touchX = ev.getX();
        float touchY = ev.getY();
        int[] webViewPos = new int[2];

        wvpoliza.getLocationOnScreen(webViewPos);

        float listaSeleccionX2 = webViewPos[0] + wvpoliza.getMeasuredWidth();
        float listaSeleccionY2 = webViewPos[1] + wvpoliza.getMeasuredHeight();

        if ((touchX >= webViewPos[0] && touchX <= listaSeleccionX2) &&
                (touchY >= webViewPos[1] && touchY <= listaSeleccionY2)) {
            wvpoliza.getParent().requestDisallowInterceptTouchEvent(true);
        }

        return super.dispatchTouchEvent(ev);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }
}
