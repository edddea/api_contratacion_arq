package com.bbva.apicuentadigital.controllers;

import android.app.DialogFragment;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.bbva.apicuentadigital.R;
import com.bbva.apicuentadigital.common.GuiTools;

/**
 * Created by Karina on 10/12/2015.
 */
public class PopUpExitoViewController extends DialogFragment implements View.OnClickListener{

    private GuiTools guiTools;
    private ExitoViewController exitoViewController;

    private View rootView;
    private Button btnAceptar;

    public PopUpExitoViewController(GuiTools guiTools, ExitoViewController exitoViewController){
        this.guiTools = guiTools;
        this.exitoViewController = exitoViewController;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.layout_pop_up_exito, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCancelable(false);
        fullScreen();
        init();
        return rootView;
    }

    private void init(){
        btnAceptar = (Button)rootView.findViewById(R.id.btnAceptar);
        btnAceptar.setOnClickListener(this);
        guiTools.scale(btnAceptar);
    }

    private void fullScreen(){
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setAttributes(wlp);
    }

    @Override
    public void onClick(View v) {
        dismiss();
        exitoViewController.finish();
    }
}
