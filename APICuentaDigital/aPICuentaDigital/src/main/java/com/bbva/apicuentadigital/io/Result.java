package com.bbva.apicuentadigital.io;

/**
 * Result wraps the result data of a response (status, error code, error message).
 */
public class Result {

    /**
     * The error code.
     */
    private String code;

    /**
     * The error message.
     */
    private String message;

    /**
     * Default constructor.
     * @param cod the error code
     * @param msg the error message
     */
    public Result( String cod, String msg) {
        this.code = cod;
        this.message = msg;
    }
    /**
     * Get the error code.
     * @return the error code
     */
    public String getCode() {
        return code;
    }

    /**
     * Get the error message.
     * @return the error message
     */
    public String getMessage() {
        return message;
    }
}
