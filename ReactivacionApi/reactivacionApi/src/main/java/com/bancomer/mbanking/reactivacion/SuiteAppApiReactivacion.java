package com.bancomer.mbanking.reactivacion;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
//import suitebancomer.classes.gui.controllers.reactivacion.MenuSuiteViewController;
import suitebancomer.classes.gui.controllers.reactivacion.SuiteViewsController;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.bancomer.base.callback.CallBackBConnect;

public class SuiteAppApiReactivacion extends com.bancomer.base.SuiteApp {
	public static boolean isSubAppRunning;
	public static Context appContext;
	private static SuiteAppApiReactivacion me;
	private SuiteViewsController suiteViewsController;
	private static BaseViewControllerCommons intentRedirecActivavion;

	/**
	 * Intent para regresar a consulta estatus desactivada
	 */
	private static Activity intentToReturn;

	public static Activity getIntentToReturn() {
		return SuiteAppApiReactivacion.intentToReturn;
	}

	public static void setIntentToReturn(final Activity intentToReturn) {
		SuiteAppApiReactivacion.intentToReturn = intentToReturn;
	}

	public static CallBackBConnect getReturnMenu() {
		return SuiteAppApiReactivacion.returnMenu;
	}

	public static void setReturnMenu(final CallBackBConnect returnMenu) {
		SuiteAppApiReactivacion.returnMenu = returnMenu;
	}

	private static CallBackBConnect returnMenu;

	public static BaseViewControllerCommons getIntentRedirecActivavion() {
		return intentRedirecActivavion;
	}

	public static void setIntentRedirecActivavion(final BaseViewControllerCommons intentRedirecActivavion) {
		SuiteAppApiReactivacion.intentRedirecActivavion = intentRedirecActivavion;
	}

	public void onCreate(final Context context) {
		super.onCreate(context);
		suiteViewsController = new SuiteViewsController();
		isSubAppRunning = false;
		
		me = this;
		appContext=context;
		//appContext = com.bancomer.base.SuiteApp.getInstance().getApplicationContext();
		startBmovilApp();
	};
	
	public void cierraAplicacionSuite() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public SuiteViewsController getSuiteViewsController() {
		return suiteViewsController;
	}

	public static SuiteAppApiReactivacion getInstance() {
		return me;
	}
	
	// #region BmovilApp
	private BmovilApp bmovilApplication;
	
	public BmovilApp getBmovilApplication() {
		if(bmovilApplication == null)
			bmovilApplication = new BmovilApp(this);
		return bmovilApplication;
	}
	
	public static boolean getBmovilStatus() {
		return PropertiesManager.getCurrent().getBmovilActivated();
	}
	
	public void startBmovilApp() {
		bmovilApplication = new BmovilApp(this);
		isSubAppRunning = true;
	}

	// #endregion

	public static int getResourceId(final String nombre, final String tipo){
		return appContext.getResources().getIdentifier(nombre,tipo, appContext.getPackageName());
	}
	

	public static int getResourceId(final String nombre, final String tipo, final View vista){
		return vista.getResources().getIdentifier(nombre,tipo, appContext.getPackageName());
	}
	
	
	
}
