package com.bancomer.mbanking.reactivacion;

import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import com.bancomer.mbanking.reactivacion.R;

import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.BmovilViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomer.classes.gui.controllers.reactivacion.BaseViewController;
import android.annotation.SuppressLint;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

@SuppressLint("HandlerLeak")
public class BmovilApp extends BaseSubapplication {

	/**
	 * Message for the handler, indicates that session must be expired.
	 */
	private static final int LOGOUT_MESSAGE = 0;

	public static int STEP_TOTAL = 0;
	public static int STEP_ACTUAL = 0;


	//////////////////////////////////////////////////////////////
	//					Methods									//
	//////////////////////////////////////////////////////////////    

	/**
	 * Constructor b��sico, se inicializa aqui debido a que no puede extender de un objeto Aplicaci��n, ya que no es posible
	 * que exista mas de uno
	 */
	public BmovilApp(final SuiteAppApiReactivacion suiteApp) {
		super(suiteApp);
		viewsController = new BmovilViewsController(this);
	}

	public BmovilViewsController getBmovilViewsController() {
		return (BmovilViewsController)viewsController;
	}

	//////////////////////////////////////////////////////////////////////////////
	//                Network operations										//
	//////////////////////////////////////////////////////////////////////////////
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * custom texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param progressLabel text with the title of the progress popup
	 * @param progressMessage text with the content of the progress popup
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	@Override
	protected void invokeNetworkOperation(final int operationId,
										  final Hashtable<String, ?> params,
										  final boolean isJson,
										  final ParsingHandler handler,
										  final BaseViewControllerCommons caller,
										  final String progressLabel,
										  final String progressMessage,
										  final boolean callerHandlesError) {
		super.invokeNetworkOperation(operationId, params,isJson,handler, caller, progressLabel, progressMessage, callerHandlesError);
	}

	/**
	 * Analyze the network response obtained from the server
	 * @param operationId network identifier. See Server class.
	 * @param response the ServerResponse instance built from the server response. It
	 * contains the type of result (success, failure), and the real content for
	 * the application business.
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	@Override
	protected void analyzeNetworkResponse(final Integer operationId,
										  final ServerResponse response,
										  final Throwable throwable,
										  final BaseViewControllerCommons caller,
										  final boolean callerHandlesError) {

		super.analyzeNetworkResponse(operationId, response, throwable, caller, callerHandlesError);
	}

	/**
	 * Initiates a session closure process
	 */
	public void closeSession(final boolean isHomeKeyPressed) {
		this.setHomeKeyPressed(isHomeKeyPressed);
		final Session session = Session.getInstance(suiteApp.getApplicationContext());
		closeSession(session.getUsername(), session.getIum(), session.getClientNumber());
	}
	
	/**
	 * Initiates a session closure process, cuando se borran los datos y es
	 * necesario hacer el cierre de sesion
	 */
	public void closeSession(final String username, final String ium, final String client) {
		applicationLogged = false;
		
		// close session
		System.gc();
		final Hashtable<String, String> params = new Hashtable<String, String>();

		params.put("NT", username);
		params.put(ServerConstants.IUM_ETIQUETA, ium);
		params.put("TE", client);
		
		BaseViewControllerCommons caller = null;
		if (viewsController != null) {
			caller = viewsController.getCurrentViewController();
		}

		invokeNetworkOperation(Server.CLOSE_SESSION_OPERATION, params,false,null, caller,
				suiteApp.getApplicationContext().getString(R.string.alert_closeSession),
				suiteApp.getApplicationContext().getString(R.string.alert_thank_you), true);
		
		if(Server.ALLOW_LOG) Log.e("CerrarSesion", "La sesion fue cerrada satisfactoriamente.");
	}
	
	public void closeBmovilAppSession(final BaseViewController caller) {
		applicationLogged = false;
		final Session session = Session.getInstance(suiteApp.getApplicationContext());
		final String username = session.getUsername();
		final String ium = session.getIum();
		final String client = session.getClientNumber();
				
		// close session
		System.gc();
		final Hashtable<String, String> params = new Hashtable<String, String>();

		params.put("NT", username);
		params.put(ServerConstants.IUM_ETIQUETA, ium);
		params.put("TE", client);
		
		//BaseViewController caller = null;
		//if (viewsController != null) {
			//caller = viewsController.getCurrentViewController();
		//}

		invokeNetworkOperation(Server.CLOSE_SESSION_OPERATION, params,false,null, caller,
				suiteApp.getApplicationContext().getString(R.string.alert_closeSession),
				suiteApp.getApplicationContext().getString(R.string.alert_thank_you), true);
		
		if(Server.ALLOW_LOG) Log.e("CerrarSesion", "La sesion fue cerrada satisfactoriamente.");
	}

	public int getApplicationStatus() {
		return Session.getInstance(SuiteAppApiReactivacion.appContext).getPendingStatus();
	}
	
	/////////////////////////////////////////////////////////////////////////////
	//                Local session management                                 //
	/////////////////////////////////////////////////////////////////////////////
	/**
	 * Records a user activity event (keystroke), in order to
	 * check session validity
	 */
	public void userActivityEvent() {

	}

	/**
	 * Logs the user out and discards the current session.
	 */
	public synchronized void logoutApp(final boolean isHomeKeyPressed){
		final Session session = Session.getInstance(suiteApp.getApplicationContext());

		if(session.getValidity() == Session.VALID_STATUS){
			session.setValidity(Session.INVALID_STATUS);
			closeSession(isHomeKeyPressed);
		}
	}

	/**
	 * We must use a TimerTask class to execute logout actions after the
	 * timer runs out.
	 */
	private class SessionLogoutTask extends TimerTask {

		@Override
		public void run() {
			applicationLogged = false;
			final Message msg = new Message();
			msg.what = BmovilApp.LOGOUT_MESSAGE;
			mApplicationHandler.sendMessage(msg);
		}

	}

	/**
	 * Use handler to respond to logout messages given by the timer.
	 */
	private Handler mApplicationHandler = new Handler() {

		@Override
		public void handleMessage(final Message msg) {

			if(msg.what == BmovilApp.LOGOUT_MESSAGE){
				logoutApp(false);
			}
		}
	};


	
}
