package com.bancomer.mbanking.reactivacion;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.util.Hashtable;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;
import suitebancomer.aplicaciones.commservice.service.ICommService;

public class ConnectionFactory {

	private int operationId;
	private Hashtable<String, ?> params;
	private Object responseObject;
	private ParametersTO parameters;
	private boolean isJson;

	public ConnectionFactory(final int operationId, final Hashtable<String, ?> params,
							 final boolean isJson, final Object responseObject) {
		super();
		this.operationId = operationId;
		this.params = params;
		this.responseObject = responseObject;
		this.isJson = isJson;
	}

	public IResponseService processConnectionWithParams() throws Exception {

		final Integer opId = Integer.valueOf(operationId);
		parameters = new ParametersTO();
		parameters.setSimulation(ServerCommons.SIMULATION);
		if (!ServerCommons.SIMULATION) {
			parameters.setProduction(!ServerCommons.DEVELOPMENT);
			parameters.setDevelopment(ServerCommons.DEVELOPMENT);
		}
		parameters.setOperationId(opId.intValue());
		parameters.setParameters(params.clone());
		parameters.setJson(isJson);
		IResponseService resultado = new ResponseServiceImpl();
		final ICommService serverproxy = new CommServiceProxy(SuiteAppApiReactivacion.appContext);
		try {
			resultado = serverproxy.request(parameters,
					responseObject == null ? new Object().getClass()
							: responseObject.getClass());
			// ConsultaEstatusMantenimientoData
			// estatusM=(ConsultaEstatusMantenimientoData)resultado.getObjResponse();
		} catch (UnsupportedEncodingException e) {
			throw new Exception(e);
		} catch (ClientProtocolException e) {
			throw new Exception(e);
		} catch (IllegalStateException e) {
			throw new Exception(e);
		} catch (IOException e) {
			throw new Exception(e);
		} catch (JSONException e) {
			throw new Exception(e);
		}

		return resultado;

	}

	public ServerResponse parserConnection(final IResponseService resultado,
										   final ParsingHandler handler) throws Exception {
		ServerResponse response=null;
		ParsingHandler handlerP=null;
		if (ApiConstants.OPERATION_SUCCESSFUL == resultado.getStatus()) {
			if(resultado.getObjResponse() instanceof ParsingHandler){
				handlerP= (ParsingHandler) resultado.getObjResponse();
			}
			if (parameters.isJson()) {
				if (handler != null) {
					final ParserJSON parser = new ParserJSON(
							resultado.getResponseString());
					try {
						handlerP.process(parser);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						throw new Exception(e);
					} catch (ParsingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						throw new Exception(e);
					}
				}
				response = new ServerResponse(resultado.getStatus(),
						resultado.getMessageCode(), resultado.getMessageText(), handlerP);
			} else {
				// Que pasa si no es JSON la respuesta
				Reader reader = null;
				try {
					if (handler != null) {
						reader = new StringReader(resultado.getResponseString());
						final Parser parser = new Parser(reader);
						if(handlerP==null){
							final Class<?> clazz = handler.getClass();
							final Constructor<?> ctor = clazz.getConstructor();
							handlerP = (ParsingHandler) ctor.newInstance();
                		}
						response= new ServerResponse(handlerP);
						response.process(parser);
					}else{
						response= new ServerResponse(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),null);
					}
				}catch(Exception e){
					throw new Exception(e);
					
				}finally {
					if (reader != null) {
						try {
							reader.close();
						} catch (Throwable ignored) {
							throw new Exception(ignored);
							
						}
					}
				}
			}

		}else{
			try {
				response= new ServerResponse(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),null);
			} catch (Exception e) {
				// TODO: handle exception
				throw new Exception(e);
			}
			
		}
		
		return response;
	}

}
