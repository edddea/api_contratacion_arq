package com.bancomer.mbanking.reactivacion;

import java.io.IOException;
import java.util.Hashtable;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.common.reactivacion.AbstractSuitePowerManager;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.io.reactivacion.NetworkOperation;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;
import suitebancomer.classes.gui.controllers.reactivacion.BaseViewController;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.util.Log;

import com.bancomer.base.SuiteApp;


public abstract class BaseSubapplication {
	// #region Variables.
	/**
	 * Determines if this activity is in process of changing screen.
	 */
	protected boolean changingActivity = false;
	
	protected boolean applicationLogged = false;
	
	protected boolean applicationInBackground = false;
	
	
	
	/**
	 * Reference to the main application.
	 */
	protected SuiteAppApiReactivacion suiteApp = null;
	
	/**
	 * Referencia al controlador de vistas.
	 */
	protected BaseViewsControllerCommons viewsController;

	/**
	 * Reference to a pending network operation, in case that an
	 * operation launched a session expired error. This operation
	 * will be relaunched after a successful login
	 */
	protected NetworkOperation pendingOperation = null;

	/**
	 * Reference to server communication handler.
	 */
	protected Server server;

	/**
	 * Pending network operations.
	 */
	protected Hashtable<Integer, NetworkOperation> networkOperationsTable = null;
	
	protected boolean isHomeKeyPressed = false;
	// #endregion
	
	// #region Getters y Setters.
	
	/**
	 * @return the changingActivity
	 */
	public boolean isChangingActivity() {
		return changingActivity;
	}

	private boolean isHomeKeyPressed() {
		return isHomeKeyPressed;
	}

	protected void setHomeKeyPressed(final boolean isHomeKeyPressed) {
		this.isHomeKeyPressed = isHomeKeyPressed;
	}

	/**
	 * @param changingActivity the changingActivity to set
	 */
	public void setChangingActivity(final boolean changingActivity) {
		this.changingActivity = changingActivity;
	}

	/**
	 * @return the applicationLogged
	 */
	public boolean isApplicationLogged() {
		return applicationLogged;
	}

	/**
	 * @param applicationLogged the applicationLogged to set
	 */
	public void setApplicationLogged(final boolean applicationLogged) {
		this.applicationLogged = applicationLogged;
	}

	/**
	 * @return the applicationInBackground
	 */
	public boolean isApplicationInBackground() {
		return applicationInBackground;
	}

	/**
	 * @param applicationInBackground the applicationInBackground to set
	 */
	public void setApplicationInBackground(final boolean applicationInBackground) {
		this.applicationInBackground = applicationInBackground;
	}

	/**
	 * @return the suiteApp
	 */
	public SuiteAppApiReactivacion getSuiteApp() {
		return suiteApp;
	}

	/**
	 * @param suiteApp the suiteApp to set
	 */
	public void setSuiteApp(final SuiteAppApiReactivacion suiteApp) {
		this.suiteApp = suiteApp;
	}

	/**
	 * @return the viewsController
	 */
	public BaseViewsControllerCommons getViewsController() {
		return viewsController;
	}

	/**
	 * @param viewsController the viewsController to set
	 */
	public void setViewsController(final BaseViewsControllerCommons viewsController) {
		this.viewsController = viewsController;
	}

	/**
	 * @return the pendingOperation
	 */
	public NetworkOperation getPendingOperation() {
		return pendingOperation;
	}

	/**
	 * @param pendingOperation the pendingOperation to set
	 */
	public void setPendingOperation(final NetworkOperation pendingOperation) {
		this.pendingOperation = pendingOperation;
	}

	/**
	 * @return the server
	 */
	public Server getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(final Server server) {
		this.server = server;
	}

	/**
	 * @return the networkOperationsTable
	 */
	public Hashtable<Integer, NetworkOperation> getNetworkOperationsTable() {
		return networkOperationsTable;
	}

	/**
	 * @param networkOperationsTable the networkOperationsTable to set
	 */
	public void setNetworkOperationsTable(
			final Hashtable<Integer, NetworkOperation> networkOperationsTable) {
		this.networkOperationsTable = networkOperationsTable;
	}
	// #endregion

	// #region Network.
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * predefined texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 */
	public void invokeNetworkOperation(final int operationId, final Hashtable<String,?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
		invokeNetworkOperation(operationId, params, isJson,handler, caller, false);
	}
	
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * predefined texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	public void invokeNetworkOperation(final int operationId, final Hashtable<String,?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller,
									   final boolean callerHandlesError) {
		invokeNetworkOperation(operationId, params, isJson, handler, caller,
				suiteApp.appContext.getString(R.string.alert_operation), 
				suiteApp.appContext.getString(R.string.alert_connecting), callerHandlesError);
	}

	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * custom texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param progressLabel text with the title of the progress popup
	 * @param progressMessage text with the content of the progress popup
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	protected void invokeNetworkOperation(final int operationId, final Hashtable<String, ?> params,
										final boolean isJson,
										final ParsingHandler handler,
										  final BaseViewControllerCommons caller,
										  final String progressLabel,
										  final String progressMessage,
										  final boolean callerHandlesError) {
		final Integer opId = Integer.valueOf(operationId);          

		if (isNotAlreadyCalling(opId)) {

			try {

				final NetworkOperation operation = new NetworkOperation(operationId, params, caller);
				this.networkOperationsTable.put(opId, operation);
				this.pendingOperation = operation;

//				//TODO quitar el numero de operacion en hardcode.
//				if (caller != null && 11 != operationId)
//				    caller.muestraIndicadorActividad(progressLabel, progressMessage);
				
				if (caller != null) {
					if(AbstractSuitePowerManager.getSuitePowerManager().isScreenOn() && (!isHomeKeyPressed())) {
						caller.muestraIndicadorActividad(progressLabel, progressMessage);
					} else {
						if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "La aplicaci��n estaba bloqueada.");
					}
				}

				final Thread thread = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							//ServerResponse response = server.doNetworkOperation(opId.intValue(), params);
							//returnFromNetworkOperation(opId, response, caller, null, callerHandlesError);

							final ParametersTO parameters=new ParametersTO();
                           parameters.setOperationId(opId.intValue());
                           parameters.setParameters(params.clone());
                           parameters.setJson(isJson);
							final Hashtable<String, ?> parameters2 = ((Hashtable<String, ?>)parameters.getParameters());
							final ConnectionFactory cf= new ConnectionFactory(parameters.getOperationId(), parameters2, parameters.isJson(), handler);
							final IResponseService resultado=cf.processConnectionWithParams();
							final ServerResponse responseNew=cf.parserConnection(resultado, handler);
							if(responseNew.getStatus()==ServerResponse.OPERATION_STATUS_UNKNOWN){
								throw new Throwable();
							}
                   		 returnFromNetworkOperation(opId, responseNew, caller, null, callerHandlesError);
							
							
						} catch (Throwable t) {
							returnFromNetworkOperation(opId, null, caller, t, callerHandlesError);
						}
					}

				});

				// Starts the worker thread to perform network operation
				thread.start();
				//thread.join();
			} catch (Throwable th) {
				returnFromNetworkOperation(opId, null, caller, th, callerHandlesError);
			}

		} else {
			// unable to process request, show error and return
			return;
		}

	}

	/**
	 * Called internally after a network operation has returned. It ends the progress
	 * popup and invokes method to analyze the result
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message from the server or method should handle in the default
	 * implementation
	 */
	protected void returnFromNetworkOperation(final Integer operationId, 
											  final ServerResponse response, 
											  final BaseViewControllerCommons caller, 
											  final Throwable throwable, 
											  final boolean callerHandlesError) {

		if (caller != null) {
			if(!(operationId.equals(Server.MOVEMENTS_OPERATION))
				&& (!(operationId.equals(Server.CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO)))
				&& (!(operationId.equals(Server.CONSULTA_DEPOSITOS_CHEQUES)))
				&& (!(operationId.equals(Server.CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS)))) {
			caller.ocultaIndicadorActividad();
			}
			
			caller.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					try {
						analyzeNetworkResponse(operationId, response, throwable, caller, callerHandlesError);
					} catch (Throwable t) {
						if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Exception on analyzeNetworkResponse.", t);
						if (caller != null) {
							final String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
							caller.showErrorMessage(message);
						}
					}
				}
			});
			
		} else { //TODO session timer expired, go to login screen
			final Thread thread = new Thread(new Runnable() {
				@Override
				public void run() {
					try {
						analyzeNetworkResponse(operationId, response, throwable, null, callerHandlesError);
					} catch (Throwable t) {
						final String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
						if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName() + " :: returnFromNetworkOperation", message);
					}
				}
			});
			thread.run();
		}
	}

	// TODO revisar en hijos.
	/**
	 * Analyze the network response obtained from the server
	 * @param operationId network identifier. See Server class.
	 * @param response the ServerResponse instance built from the server response. It
	 * contains the type of result (success, failure), and the real content for
	 * the application business.
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	protected void analyzeNetworkResponse(final Integer operationId,
										  final ServerResponse response,
										  final Throwable throwable,
										  final BaseViewControllerCommons caller,
										  final boolean callerHandlesError) {
		// get the caller           
		final NetworkOperation operation = this.networkOperationsTable.get(operationId);
		if (operation != null) {
			// remove the operation id from the table
			this.networkOperationsTable.remove(operationId);
			if (operation.isActive()) {
				if (response != null) {
					final BaseViewControllerCommons baseViewController = operation.getCaller();
					final int resultCode = response.getStatus();
					if (resultCode == ServerResponse.OPERATION_SUCCESSFUL
							|| resultCode == ServerResponse.OPERATION_WARNING) {
						baseViewController.processNetworkResponse(operationId.intValue(), response);
					} else if (resultCode == ServerResponse.OPERATION_OPTIONAL_UPDATE) {
						baseViewController.processNetworkResponse(operationId.intValue(), response);
					} else if (resultCode == ServerResponse.OPERATION_ERROR) {
						this.pendingOperation = null;
						final String errorCode = response.getMessageCode();
						final int operationError = operationId.intValue();
						if( operationError != Server.QUITAR_SUSPENSION && operationError != Server.LOGIN_OPERATION && operationError != Server.OP_ENVIO_CLAVE_ACTIVACION && operationError != Server.OP_ACTIVACION && operationError != Server.AUTENTICACION_ST && (Constants.BLOCKED_ERROR_CODE.equals(errorCode) || Constants.BLOCKED_ERROR_CODE_2.equals(errorCode))){
							caller.showInformationAlert(response.getMessageText(), new OnClickListener() {
								@Override
								public void onClick(final DialogInterface dialog, final int which) {
									SuiteAppApiReactivacion.getInstance().getBmovilApplication().closeSession(Session.getInstance(SuiteApp.appContext).getUsername(), Session.getInstance(SuiteApp.appContext).getIum(), Session.getInstance(SuiteApp.appContext).getClientNumber());
								}
							});
						}else if (operationError == Server.LOGIN_OPERATION && Constants.DEACTIVATION_ERROR_CODE.equals(errorCode)) {
							final Session session = Session.getInstance(SuiteApp.appContext);
							final String username = session.getUsername();
							final String password = session.getPassword();
							Session.getInstance(suiteApp.appContext).setValidity(Session.UNSET_STATUS);

							final Bundle params = new Bundle();
							params.putString(Server.USERNAME_PARAM, username);
							params.putString(Server.PASSWORD_PARAM, password);
						} else if (operationError == Server.SELF_TRANSFER_OPERATION ||
								operationError == Server.BANCOMER_TRANSFER_OPERATION ||
								operationError == Server.EXTERNAL_TRANSFER_OPERATION ||
								operationError == Server.ALTA_OPSINTARJETA ||
								operationError == Server.SERVICE_PAYMENT_OPERATION ||
								operationError == Server.OP_RETIRO_SIN_TAR ||
								operationError == Server.COMPRA_TIEMPO_AIRE){
							
								// Mensaje de error en Transferencias.
								if(Server.ALLOW_LOG) Log.d("BaseSubapplication", "ERROR Transferencias >>> operationError = " +  operationError + " errorCode = " + errorCode);
								mostrarErrorTransfer(response, errorCode, baseViewController, caller,operationId.intValue());
								//baseViewController.processNetworkResponse(operationId.intValue(), response);
								
														
						} else {
								if (callerHandlesError) {
									baseViewController.processNetworkResponse(operationId.intValue(), response);
								} else {
							//		caller.ocultaIndicadorActividad();
//									caller.showErrorMessage(errorCode + "\n" + response.getMessageText());
									caller.showInformationAlertEspecial(SuiteApp.appContext.getString(R.string.label_error), errorCode, response.getMessageText(), null);
								}
						}
					} else if (resultCode == ServerResponse.OPERATION_SESSION_EXPIRED) {
						this.pendingOperation = operation;
						Session.getInstance(suiteApp.appContext).setValidity(Session.INVALID_STATUS);
						suiteApp.getSuiteViewsController().showMenuSuite(true);
					} else {
						caller.ocultaIndicadorActividad();
						caller.showErrorMessage(R.string.error_format);
					}
				} else {
					if (throwable != null) {
						caller.ocultaIndicadorActividad();
						caller.showErrorMessage(getErrorMessage(throwable));
					} else {
						caller.ocultaIndicadorActividad();
						caller.showErrorMessage(R.string.error_communications);
					}
				}
			}
		} else {
			caller.ocultaIndicadorActividad();
			caller.showErrorMessage(R.string.error_unexpected);
		}		
	}
	
	/**
	 * Muestra un aviso de error en caso de que el servidor rechace la transferencia. Si es
	 * avanzado mostrar�� el mensaje devuelto y si es b��sico mostrar�� uno por defecto.
	 * 
	 * @param response Respuesta del servidor
	 * @param baseViewController Controlador base
	 * @param caller Controlador padre
	 */
	private void mostrarErrorTransfer(final ServerResponse response, final String errorCode, final BaseViewControllerCommons baseViewController, final BaseViewControllerCommons caller, final int operationValue) {
		final Session session = Session.getInstance(SuiteApp.appContext);
		final Perfil perfil = session.getClientProfile();
		// Si es perfil avanzado mostrar�� el mensaje recibido, sino mostrar�� el definido en String.xml

		final Account cEje = Tools.obtenerCuentaEje();
		final String cEjeType = cEje == null ? "" : cEje.getType();
		
		if (Constants.CODE_CNE0234.equals(errorCode) || Constants.CODE_CNE0235.equals(errorCode) || Constants.CODE_CNE0236.equals(errorCode)) {
			
			if (Perfil.basico.equals(perfil) && !cEjeType.equals(Constants.CREDIT_TYPE)) {
				final String msg = baseViewController.getString(R.string.transferir_aviso_texto);
				caller.ocultaIndicadorActividad();
				caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode, msg, null);
				
			} else if (Perfil.recortado.equals(perfil)) {
				baseViewController.processNetworkResponse(operationValue, response);
				
			} else {
				// Avanzado, o b��sico con tarjeta de cr��dito como cuenta eje
				caller.ocultaIndicadorActividad();
				caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,response.getMessageText(), null);
			}
			
		} else {
			caller.ocultaIndicadorActividad();
			caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,response.getMessageText(), null);
		}
		
//		if(Constants.Perfil.recortado.equals(perfil)){
//			
//			baseViewController.processNetworkResponse(operationValue, response);
//	
//		}else if(Constants.Perfil.basico.equals(perfil) && 
//				(Constants.CODE_CNE0234.equals(errorCode) ||
//				 Constants.CODE_CNE0235.equals(errorCode) ||
//				 Constants.CODE_CNE0236.equals(errorCode)) &&
//				 !cEjeType.equals(Constants.CREDIT_TYPE)){
//			String msg = baseViewController.getString(R.string.transferir_aviso_texto);
//			caller.ocultaIndicadorActividad();
//			caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,msg, null);
//		}else {
//			caller.ocultaIndicadorActividad();
//			caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,response.getMessageText(), null);
//		}								
	}
	
	

	/**
	 * Cancel any ongoing network operation, and clears the network
	 * operation table to prevent any other network operation from
	 * executing
	 */
	protected void cancelOngoingNetworkOperations() {
		this.networkOperationsTable.clear();
		this.pendingOperation = null;
	}
	
	/**
	 * Determines if the requested operation is not being called already
	 * @param opId the operation id
	 * @return true if the operation isn't already waiting for a response
	 */
	private boolean isNotAlreadyCalling(final int opId){
		return !this.networkOperationsTable.containsKey(opId);
	}
	// #endregion
	
	// #region Otros m��todos.
	// TODO revisar en hijos.
	public void cierraAplicacion() {
		viewsController.cierraViewsController();
		viewsController = null;
		server = null;
		applicationLogged = false;
	}
	
	/**
	 * Shows the Main menu screen, removing any activity on top
	 */
	public void requestMenu(final Context activityContext){
		changingActivity = true;
		viewsController.showMenuInicial();
	}

	/**
	 * Get the operation error message
	 * @param throwable the throwable
	 * @return the operation error message 
	 */
	protected String getErrorMessage(final Throwable throwable) {
		final StringBuffer sb = new StringBuffer();
		if (throwable != null) {
			if (throwable instanceof NumberFormatException) {
				sb.append(suiteApp.appContext.getString(R.string.error_format));
			} else if (throwable instanceof ParsingException) {
				sb.append(suiteApp.appContext.getString(R.string.error_format));
			} else if (throwable instanceof IOException) {
				sb.append(suiteApp.appContext.getString(R.string.error_communications));
			} else {
				sb.append(suiteApp.appContext.getString(R.string.error_communications));
			}
		}
		return sb.toString();
	}
	// #endregion
	
	// TODO revisar en hijos.
	public BaseSubapplication(final SuiteAppApiReactivacion suiteApp) {
		this.suiteApp = suiteApp;
		server = new Server();
		networkOperationsTable = new Hashtable<Integer, NetworkOperation>();
		viewsController = null;
		applicationLogged = false;
	}
}
