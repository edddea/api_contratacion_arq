package suitebancomer.aplicaciones.bmovil.classes.model.reactivacion;

/**
 * Created by evaltierrah on 12/08/2015.
 */
public class BanderasServer {

    /**
     * Indicates simulation usage.
     */
    private Boolean simulacion = Boolean.FALSE;
    /**
     * Indica si esta activo el log.
     */
    private Boolean logActivo = Boolean.FALSE;
    /**
     * Defines if the application is running on the emulator or in the device.
     */
    private Boolean emulador = Boolean.FALSE;

    /**
     * Indicates if the base URL points to development server or production
     */
    private Boolean desarrollo = Boolean.FALSE;

    public BanderasServer(final Boolean simulacion, final Boolean logActivo, final Boolean emulador, final Boolean desarrollo) {
        this.simulacion = simulacion;
        this.logActivo = logActivo;
        this.emulador = emulador;
        this.desarrollo = desarrollo;
    }

    public Boolean getSimulacion() {
        return simulacion;
    }

    public void setSimulacion(final Boolean simulacion) {
        this.simulacion = simulacion;
    }

    public Boolean getEmulador() {
        return emulador;
    }

    public void setEmulador(final Boolean emulador) {
        this.emulador = emulador;
    }

    public Boolean isDesarrollo() {
        return desarrollo;
    }

    public void setDesarrollo(final Boolean desarrollo) {
        this.desarrollo = desarrollo;
    }

    public Boolean getLogActivo() {
        return logActivo;
    }

    public void setLogActivo(final Boolean logActivo) {
        this.logActivo = logActivo;
    }
}
