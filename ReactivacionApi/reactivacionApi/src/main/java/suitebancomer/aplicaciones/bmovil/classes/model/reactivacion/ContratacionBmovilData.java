package suitebancomer.aplicaciones.bmovil.classes.model.reactivacion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ContratacionBmovilData implements ParsingHandler {
	/**
	 *  Folio de la arquitectura que devuelve la trasacci�n.
	 */
	private String folioArq;
	
	/**
	 * @return Folio de la arquitectura que devuelve la trasacci�n.
	 */
	public String getFolioArq() {
		return folioArq;
	}

	/**
	 * @param folioArq Folio de la arquitectura que devuelve la trasacci�n.
	 */
	public void setFolioArq(final String folioArq) {
		this.folioArq = folioArq;
	}

	public ContratacionBmovilData() {
		folioArq = null;
	}

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		folioArq = null;
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		folioArq = parser.parseNextValue("folioArq");
	}
}
