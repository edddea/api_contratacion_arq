package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import suitebancomer.aplicaciones.bmovil.classes.model.reactivacion.EnvioClaveActivacion;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.SessionStoredListener;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.ConfirmacionAutenticacionViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Reactivacion;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomer.classes.gui.controllers.reactivacion.BaseViewController;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.reactivacion.R;
import com.bancomer.mbanking.reactivacion.SuiteAppApiReactivacion;
public class ReactivacionDelegate extends DelegateBaseAutenticacion implements SessionStoredListener {
	public static final long REACTIVACION_DELEGATE_ID = -6603888306287326369L;
	private ConsultaEstatus consultaEstatus;
	private Reactivacion reactivacion;
	private Operacion tipoOperacion;
	private BaseViewController ownerController;
	private String password;
	
	// #region Getters y Setters.
	/**
	 * @return the consultaEstatus
	 */
	public ConsultaEstatus getConsultaEstatus() {
		return consultaEstatus;
	}

	/**
	 * @param consultaEstatus the consultaEstatus to set
	 */
	public void setConsultaEstatus(final ConsultaEstatus consultaEstatus) {
		this.consultaEstatus = consultaEstatus;
	}

	/**
	 * @return the reactivacion
	 */
	public Reactivacion getReactivacion() {
		return reactivacion;
	}

	/**
	 * @param reactivacion the reactivacion to set
	 */
	public void setReactivacion(final Reactivacion reactivacion) {
		this.reactivacion = reactivacion;
	}

	/**
	 * @param ownerController the ownerController to set
	 */
	public void setOwnerController(final BaseViewController ownerController) {
		this.ownerController = ownerController;
	}

	/**
	 * @return Tipo de operaci��n actual.
	 */
	public Operacion getTipoOperacion() {
		return tipoOperacion;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(final String password) {
		this.password = password;
	}
	// #endregion
	
	public ReactivacionDelegate() {
		tipoOperacion = Operacion.reactivacion;
		password = null;
		ownerController = null;
	}
	
	public ReactivacionDelegate(final ConsultaEstatus consultaEstatus) {
		this.consultaEstatus = consultaEstatus;
		reactivacion = new Reactivacion();
		reactivacion.setEstatus(consultaEstatus.getEstatus());
		reactivacion.setPerfil(consultaEstatus.getPerfil());
		reactivacion.setPerfilAST(consultaEstatus.getPerfilAST());
		reactivacion.setNumCelular(consultaEstatus.getNumCelular());
		reactivacion.setCompaniaCelular(consultaEstatus.getCompaniaCelular());
		reactivacion.setNumCliente(consultaEstatus.getNumCliente());
		tipoOperacion = Operacion.reactivacion;
		ownerController = null;
	}

	// #region M��todos de autenticaci��n contrataci��n
	
//		@Override
//		public void realizaOperacion(ContratacionAutenticacionViewController contratacionAutenticacionViewController, String nip, String token, String cvv, boolean terminos) {
//			Hashtable<String,String> paramTable = new Hashtable<String, String>();
//	        int operacion = Server.OP_CONTRATACION_BMOVIL;
//	        
//	        paramTable.put(Server.PHONENUMBER_PARAM, contratacion.getNumCelular());  
//	        paramTable.put(Server.PASSWORD_PARAM, Session.getInstance(contratacionAutenticacionViewController).getPassword());     
//	        paramTable.put(ServerConstants.NUMERO_CLIENTE, contratacion.getNumeroCliente()); 
//	        paramTable.put(Server.CARDNUMBER_PARAM, contratacion.getNumeroCuenta());   
//	        paramTable.put(ServerConstants.PERFIL_CLIENTE, consultaEstatus.getPerfilAST());
//	        paramTable.put(ServerConstants.COMPANIA_CELULAR, contratacion.getCompaniaCelular()); 
//	        paramTable.put(Server.EMAIL_PARAM, contratacion.getEmailCliente());        
//	        paramTable.put(Server.USERNAME_PARAM, contratacion.getNombreCliente());     
//	        paramTable.put(Server.TERMS_PARAM, terminos ? "si" : "no");        
//	        paramTable.put(Server.ALERTSTATUS_PARAM, contratacion.getEstatusAlertas());  
//	        paramTable.put(Server.NIP_PARAM, (null == nip) ? "" : nip);          
//	        paramTable.put(Server.CVV2_PARAM, (null == cvv) ? "" : cvv);         
//	        paramTable.put(ServerConstants.CODIGO_OTP, (null == token) ? "" : token);          
//	        paramTable.put(Server.VA_PARAM, Autenticacion.getInstance().getCadenaAutenticacion(tipoOperacion, contratacion.getPerfil()));
//			
//	        ownerController = contratacionAutenticacionViewController;
//	        
//			doNetworkOperation(operacion, paramTable, ownerController);
//		}

		/* (non-Javadoc)
		 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseAutenticacion#realizaOperacion(suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
		 */
		@Override
		public void realizaOperacion(final ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController, final String contrasenia, final String nip, final String token, final String cvv, final String campoTarjeta) {
			final Hashtable<String,String> paramTable = new Hashtable<String, String>();
			final int operacion = Server.OP_ENVIO_CLAVE_ACTIVACION;
	        
	        paramTable.put(ServerConstants.NUMERO_TELEFONO, reactivacion.getNumCelular());  
	        paramTable.put(ServerConstants.NUMERO_CLIENTE, reactivacion.getNumCliente()); 
	        paramTable.put(ServerConstants.COMPANIA_CELULAR, reactivacion.getCompaniaCelular()); 
	        paramTable.put("cveAcceso", (null == contrasenia) ? "" : contrasenia); 
	        paramTable.put("codigoNIP", (null == nip) ? "" : nip);          
	        paramTable.put("codigoCVV2", (null == cvv) ? "" : cvv);         
	        paramTable.put("codigoOTP", (null == token) ? "" : token);          
	        paramTable.put("cadenaAutenticacion", Autenticacion.getInstance().getCadenaAutenticacion(tipoOperacion, reactivacion.getPerfil()));
	        paramTable.put("tarjeta5Dig", campoTarjeta == null ? "" : campoTarjeta);
	        paramTable.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
			paramTable.put("sistemaOperativo", "android" );
			final List<String> listaEncriptar = Arrays.asList("cveAcceso", "codigoNIP",
	        		"codigoCVV2");
			Encripcion.setContext(SuiteAppApiReactivacion.appContext);
			Encripcion.encriptaCadenaAutenticacion(paramTable, listaEncriptar);
	        
	        //JAIG
	        ownerController = confirmacionAutenticacionViewController;
			doNetworkOperation(operacion, paramTable, true, new EnvioClaveActivacion(), (BaseViewController) ownerController);
		}

		/* (non-Javadoc)
		 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getDatosTablaConfirmacion()
		 */
		@Override
		public ArrayList<Object> getDatosTablaConfirmacion() {
			final ArrayList<Object> registros;
			final ArrayList<Object> lista = new ArrayList<Object>();
			
			registros = new ArrayList<Object>();
			registros.add(SuiteAppApiReactivacion.appContext.getString(R.string.bmovil_contratacion_autorizacion_label_operacion));
			registros.add(SuiteAppApiReactivacion.appContext.getString(R.string.bmovil_reactivacion_titulo));
			lista.add(registros);

			return lista;
		}
		
		/* (non-Javadoc)
		 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#mostrarContrasenia()
		 */
		@Override
		public boolean mostrarContrasenia() {
			return Autenticacion.getInstance().mostrarContrasena(tipoOperacion, this.reactivacion.getPerfil());
		}
		
		/* (non-Javadoc)
		 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#tokenAMostrar()
		 */
		@Override
		public TipoOtpAutenticacion tokenAMostrar() {
			return Autenticacion.getInstance().tokenAMostrar(tipoOperacion, this.reactivacion.getPerfil());
		}

		/* (non-Javadoc)
		 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#mostrarNIP()
		 */
		@Override
		public boolean mostrarNIP() {
			return Autenticacion.getInstance().mostrarNIP(tipoOperacion, this.reactivacion.getPerfil());
		}

		/* (non-Javadoc)
		 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#mostrarCVV()
		 */
		@Override
		public boolean mostrarCVV() {
			return Autenticacion.getInstance().mostrarCVV(tipoOperacion, this.reactivacion.getPerfil());
		}

		/* (non-Javadoc)
		 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getNombreImagenEncabezado()
		 */
		@Override
		public int getNombreImagenEncabezado() {
			return R.drawable.icono_contratacion;
		}
		
		/* (non-Javadoc)
		 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getTextoEncabezado()
		 */
		@Override
		public int getTextoEncabezado() {
			return R.string.bmovil_reactivacion_titulo;
		}
		// #endregion
		
	// #region Network
	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#doNetworkOperation(int, java.util.Hashtable, suitebancomer.classes.gui.controllers.BaseViewController)
	 */
	@Override
	public void doNetworkOperation(final int operationId, final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
		if( ownerController != null)
					((BmovilViewsController) SuiteAppApiReactivacion.getInstance().getBmovilApplication().getViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, caller);
	}

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#analyzeResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		if(ServerResponse.OPERATION_SUCCESSFUL == response.getStatus()) {
			if (operationId == Server.OP_ENVIO_CLAVE_ACTIVACION) {
				    com.bancomer.mbanking.activacion.SuiteApp.appContext = SuiteApp.appContext;
					muestraAvisoActivacion(com.bancomer.mbanking.activacion.SuiteApp.appContext);
			} else {
			}
		} else {
			((BmovilViewsController)ownerController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
		}
	}
	private void muestraAvisoActivacion(final Context apContex){
		final Dialog contactDialog = new Dialog(apContex);
		final LayoutInflater inflater = (LayoutInflater) apContex.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View layout = inflater.inflate(R.layout.aviso_ivr_reactivacion,null);
		contactDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// make dialog itself transparent
		contactDialog.setCancelable(false);
		contactDialog.getWindow().setBackgroundDrawable(new ColorDrawable(apContex.getResources().getColor(android.R.color.transparent)));

		contactDialog.setContentView(layout);

		final int width = (int)(apContex.getResources().getDisplayMetrics().widthPixels * 0.90);
		contactDialog.getWindow().setLayout(width, DrawerLayout.LayoutParams.WRAP_CONTENT);

		final Button aceptarIvr = (Button) layout.findViewById(R.id.btnAceptarivr);
		aceptarIvr.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(final View v) {
						//se desactiva la llamada
						//final Intent sIntent=new Intent(Intent.ACTION_CALL, Uri.parse(String.format("tel:%s", Uri.encode(SuiteAppApiReactivacion.appContext.getString(com.bancomer.mbanking.reactivacion.R.string.activacion_numero_solicita_codigo)))));
						//sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						//SuiteAppApiReactivacion.appContext.startActivity(sIntent);
						aceptarIvr();
						contactDialog.dismiss();
					}
				}
		);

		contactDialog.show();

	}

	private void aceptarIvr(){
		if (null == password)
			SuiteAppApiReactivacion.getInstance().getBmovilApplication().getBmovilViewsController().showActivacion(reactivacion);
		else
			SuiteAppApiReactivacion.getInstance().getBmovilApplication().getBmovilViewsController().showActivacion(consultaEstatus, password);

	}

	// #endregion
	
	public void borrarDatosDeSession() {
		ownerController.muestraIndicadorActividad(SuiteAppApiReactivacion.getIntentToReturn(),
				SuiteAppApiReactivacion.appContext, "", SuiteAppApiReactivacion.appContext.getString(R.string.alert_StoreSession));
		
		final ReactivacionDelegate me = this;
		final Session session = Session.getInstance(SuiteAppApiReactivacion.appContext);
		//session.setUsername(null);
		session.setCompaniaUsuario(null);
		session.setApplicationActivated(false);
		session.setSeed(0);
		session.setAceptaCambioPerfil(true);
		//session.clearCatalogs();
		session.setSecurityInstrument(consultaEstatus.getInstrumento());
		session.storeSession(me);
	}

	@Override
	public void sessionStored() {
		ownerController.ocultaIndicadorActividad();
		final ReactivacionDelegate me = this; 
		ownerController.runOnUiThread(new Runnable(){
			@Override
			public void run() {
//				final BmovilViewsController bm=null;
				ownerController.setHabilitado(Boolean.TRUE);
				ownerController.showInformationAlert(SuiteAppApiReactivacion.getIntentToReturn(), SuiteAppApiReactivacion.appContext,
						R.string.borrarDatos_dialogSucceed, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
//						bm.showConfirmacionAutenticacionViewController(me, 0, 0, 0);
						SuiteApp.setIsReactivacion(Boolean.TRUE);
						SuiteAppApiReactivacion.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(me, 0, 0, 0);
					}
				});				
			}
		});	
	}
	
	@Override
	public boolean mostrarCampoTarjeta() {
		return (mostrarCVV() || mostrarNIP());
	}
}
