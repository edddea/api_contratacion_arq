package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.ConfirmacionAutenticacionViewController;
//import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.ContratacionAutenticacionViewController;
import suitebancomer.classes.gui.controllers.reactivacion.BaseViewsController;
import android.content.Context;

import com.bancomer.mbanking.reactivacion.R;
import com.bancomer.mbanking.reactivacion.SuiteAppApiReactivacion;
public class DelegateBaseAutenticacion extends DelegateBaseOperacion {
	
	private final Context ctxt = SuiteAppApiReactivacion.appContext;

	@Override
	public String getEtiquetaCampoCVV() { return ""; }
	
	@Override
	public boolean mostrarCVV() { return false; }
	
	@Override
	public String getTextoAyudaCVV() { return SuiteAppApiReactivacion.appContext.getString(R.string.confirmation_CVV_ayuda); };
	
	/**
	 * modificado con el campo de tarjeta.
	 * @param confirmacionAutenticacionViewController
	 * @param contrasenia
	 * @param nip
	 * @param token
	 * @param cvv
	 * @param campoTarjeta
	 */
	public void realizaOperacion(final ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController, final String contrasenia,
								 final String nip, final String token, final String cvv, final String campoTarjeta) { };
			 
			 

//	public void realizaOperacion(ContratacionAutenticacionViewController contratacionAutenticacionViewController, String contrasenia,
//			 String nip, String token, String cvv) { };
			 
	/**
	 * Respuesta de la pantalla de confirmaci�n. 
	 * @param contratacionAutenticacionViewController La pantalla de confirmacion.
	 * @param nip El nip ingresado por el usuario.
	 * @param token El token ingresado por el usuario.
	 * @param cvv El CVV ingresado por el usuario.
	 * @param terminos Bandera de aceptaci�n de terminos y servicios, true si acepto, false de otro modo.
	 * @param campoTarjeta TODO
	 */
//	public void realizaOperacion(ContratacionAutenticacionViewController contratacionAutenticacionViewController,
//			 					 String nip, 
//			 					 String token, 
//			 					 String cvv, 
//			 					 String pwd,
//			 					 boolean terminos, String campoTarjeta){};

	@Override
	protected void accionBotonResultados() {
//		BaseViewsController parentViewsController = SuiteAppApiReactivacion.getInstance().getBmovilApplication().getBmovilViewsController();
//		parentViewsController.removeDelegateFromHashMap(ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID);
//		parentViewsController.removeDelegateFromHashMap(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
//		((BmovilViewsController) parentViewsController).showMenuPrincipal(true);
	}
	
	@Override
	protected int getImagenBotonResultados() {
		return R.drawable.btn_menu;
	}
	
	/**
	 * Define el texto de ayuda para los instrumentos de seguridad 
	 */
	@Override
	public String getTextoAyudaInstrumentoSeguridad(final TipoInstrumento tipoInstrumento) {
		final TipoOtpAutenticacion tokenAMostrar = tokenAMostrar();
		if (tokenAMostrar == TipoOtpAutenticacion.ninguno) {
			return "";
		} else if (tokenAMostrar == TipoOtpAutenticacion.registro) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (SuiteAppApiReactivacion.getSofttokenStatus()) {
						return ctxt.getString(R.string.confirmation_ayudaRegistroSofttokenActivado);
					} else {
						return ctxt.getString(R.string.confirmation_ayudaRegistroSofttokenDesactivado);
					}
				case OCRA:
					return ctxt.getString(R.string.confirmation_ayudaRegistroOCRA);
				case DP270:
					return ctxt.getString(R.string.confirmation_ayudaRegistroDP270);
				case sinInstrumento:
				default:
					return "";
			}
		} else if (tokenAMostrar == TipoOtpAutenticacion.codigo) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (SuiteAppApiReactivacion.getSofttokenStatus()) {
						return ctxt.getString(R.string.confirmation_ayudaCodigoSofttokenActivado);
					} else {
						return ctxt.getString(R.string.confirmation_ayudaCodigoSofttokenDesactivado);
					}					
				case OCRA:
					return ctxt.getString(R.string.confirmation_ayudaCodigoOCRA);
				case DP270:
					return ctxt.getString(R.string.confirmation_ayudaCodigoDP270);
				case sinInstrumento:
				default:
					return "";
			}
		}
		return "";
	}
	
	@Override
	public String getEtiquetaCampoNip() {		
		return ctxt.getString(R.string.confirmation_nip);
	}
	
	@Override
	public String getTextoAyudaNIP() {
		return ctxt.getString(R.string.confirmation_autenticacion_ayudaNip);
	}
	
	@Override
	public String getEtiquetaCampoOCRA() {
		return ctxt.getString(R.string.confirmation_ocra);
	}
	
	@Override
	public String getEtiquetaCampoDP270() {
		return ctxt.getString(R.string.confirmation_dp270);
	}
}
