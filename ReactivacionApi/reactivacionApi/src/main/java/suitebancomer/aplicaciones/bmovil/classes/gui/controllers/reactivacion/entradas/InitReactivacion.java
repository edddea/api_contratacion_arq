package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.entradas;

import android.app.Activity;
import android.util.Log;

import com.bancomer.base.SuiteApp;
import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.mbanking.reactivacion.SuiteAppApiReactivacion;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import java.util.ArrayList;
import java.util.List;

import bancomer.api.common.commons.Constants;
import suitebancomer.activacion.aplicaciones.bmovil.classes.entrada.InitActivacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.ReactivacionDelegate;
import suitebancomer.aplicaciones.keystore.KeyManagerStoreException;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomercoms.aplicaciones.bmovil.classes.model.BanderasServer;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Reactivacion;

/**
 * Created by evaltierrah on 13/08/2015.
 */
public class InitReactivacion {

    public InitReactivacion(final BanderasServer banderasServer, final Activity activity,
                            final CallBackBConnect callBackBConnect, final Activity intentReturn) {
        ServerCommons.ALLOW_LOG = banderasServer.getLogActivo();
        ServerCommons.DEVELOPMENT = banderasServer.isDesarrollo();
        ServerCommons.EMULATOR = banderasServer.getEmulador();
        ServerCommons.SIMULATION = banderasServer.getSimulacion();
        final SuiteAppApiReactivacion suiteApp = new SuiteAppApiReactivacion();
        suiteApp.onCreate(activity);
        SuiteAppApiReactivacion.setIntentToReturn(intentReturn);
        SuiteAppApiReactivacion.setReturnMenu(callBackBConnect);
        final SuiteAppApi suiteAppApiSofttoken = new SuiteAppApi();
        suiteAppApiSofttoken.onCreate(activity);
        Session.getInstance(SuiteApp.appContext);
        InitActivacion initActivacion = new InitActivacion(banderasServer, (Activity)SuiteApp.appContext, callBackBConnect, intentReturn);
    }

    public void showReactivacion(final Reactivacion reactivacion, final List<String> estados) {
        SuiteAppApiReactivacion.getInstance().getBmovilApplication().getBmovilViewsController().estados = (ArrayList<String>)estados;
        final BmovilViewsController bmovilViewsController = SuiteAppApiReactivacion.getInstance().getBmovilApplication().getBmovilViewsController();
        ReactivacionDelegate delegate = (ReactivacionDelegate) bmovilViewsController.getBaseDelegateForKey(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
        if (null != delegate) {
            bmovilViewsController.removeDelegateFromHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
        }
        delegate = new ReactivacionDelegate();
        delegate.setReactivacion(reactivacion);

        bmovilViewsController.addDelegateToHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID,
                delegate);
        bmovilViewsController.showConfirmacionAutenticacionViewController(delegate, 0, 0, 0);
    }

    public void showReactivacion(final ConsultaEstatus consultaEstatus, final List<String> estados) {
        SuiteAppApiReactivacion.getInstance().getBmovilApplication().getBmovilViewsController().estados = (ArrayList<String>)estados;
        final BmovilViewsController bmovilViewsController = SuiteAppApiReactivacion.getInstance().getBmovilApplication().getBmovilViewsController();
        ReactivacionDelegate delegate = (ReactivacionDelegate) bmovilViewsController.getBaseDelegateForKey(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
        if (null != delegate) {
            bmovilViewsController.removeDelegateFromHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
        }
        delegate = new ReactivacionDelegate(consultaEstatus);

        bmovilViewsController.addDelegateToHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID,
                delegate);
        bmovilViewsController.showConfirmacionAutenticacionViewController(delegate, 0, 0, 0);
    }

    public void showReactivacion(final ConsultaEstatus consultaEstatus, final String password, final List<String> estados) {
        SuiteAppApiReactivacion.getInstance().getBmovilApplication().getBmovilViewsController().estados = (ArrayList<String>)estados;
        final BmovilViewsController bmovilViewsController = SuiteAppApiReactivacion.getInstance().getBmovilApplication().getBmovilViewsController();
        ReactivacionDelegate delegate = (ReactivacionDelegate) bmovilViewsController.getBaseDelegateForKey(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
        if (null != delegate) {
            bmovilViewsController.removeDelegateFromHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
        }
        delegate = new ReactivacionDelegate(consultaEstatus);
        bmovilViewsController.addDelegateToHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID,
                delegate);

        delegate.setPassword(password);
        delegate.setOwnerController(new ConfirmacionAutenticacionViewController());

        final Session session = Session.getInstance(SuiteApp.appContext);
        //KeyStoreManager keyStore = session.getKeyStoreManager();

        try {
            final KeyStoreWrapper kswrapper = session.getKeyStoreWrapper();
            kswrapper.setUserName(" "); //setEntry(Constants.USERNAME, " ");
            kswrapper.setSeed(" "); //setEntry(Constants.SEED, " ");
            kswrapper.storeValueForKey(Constants.CENTRO, " "); //setEntry(Constants.CENTRO, " ");
        } catch (Exception e) {
            if(ServerCommons.ALLOW_LOG) {
                Log.e("Error KeyStore", "BmovilViewsController: Error al borrar datos en KeyStore");
            }
        }
        delegate.borrarDatosDeSession();
    }

}
