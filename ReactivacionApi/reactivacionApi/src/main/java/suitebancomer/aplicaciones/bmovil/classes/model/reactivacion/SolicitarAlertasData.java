package suitebancomer.aplicaciones.bmovil.classes.model.reactivacion;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class SolicitarAlertasData implements ParsingHandler {
	/*
	 * Estado de la Operacion.
	 */
	private String estado;

	/**
	 * Validacion Alertas
	 */
	private String validacionAlertas;

	/**
	 * numero Alertas
	 */
	private String numeroAlertas;

	/**
	 * Compania Alertas
	 */
	private String companiaAlertas;

	public String getEstado() {
		return estado;
	}

	public void setEstado(final String estado) {
		this.estado = estado;
	}

	public String getValidacionAlertas() {
		return validacionAlertas;
	}

	public void setValidacionAlertas(final String validacionAlertas) {
		this.validacionAlertas = validacionAlertas;
	}

	public String getNumeroAlertas() {
		return numeroAlertas;
	}

	public void setNumeroAlertas(final String numeroAlertas) {
		this.numeroAlertas = numeroAlertas;
	}

	public String getCompaniaAlertas() {
		return companiaAlertas;
	}

	public void setCompaniaAlertas(final String companiaAlertas) {
		this.companiaAlertas = companiaAlertas;
	}

	/**
	 * Constructor por defecto.
	 */
	public SolicitarAlertasData() {
		estado = null;
		validacionAlertas = null;
		numeroAlertas = null;
		companiaAlertas = null;

	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {

		estado = parser.parseNextValue("estado");
		validacionAlertas = parser.parseNextValue("validacionAlertas");
		numeroAlertas = parser.parseNextValue("numeroAlertas");
		companiaAlertas = parser.parseNextValue("companiaAlertas");

	}

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub

	}
}
