package suitebancomer.aplicaciones.bmovil.classes.model.reactivacion;

import java.io.IOException;
import java.io.Serializable;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by alanmichaelgonzalez on 21/12/15.
 */
public class EnvioClaveActivacion implements ParsingHandler {

    private String estado;
    private String ivr;

    public String getEstado() {
        return estado;
    }

    public void setEstado(final String estado) {
        this.estado = estado;
    }

    public String getIvr() {
        return ivr;
    }

    public void setIvr(final String ivr) {
        this.ivr = ivr;
    }


    @Override
    public void process(final Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(final ParserJSON parser) throws IOException, ParsingException {

    }
}
