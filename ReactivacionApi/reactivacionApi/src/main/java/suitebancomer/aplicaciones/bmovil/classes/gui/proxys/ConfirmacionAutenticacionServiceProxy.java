/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.gui.proxys;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;

import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.ConfirmacionAutenticacionDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.resultados.proxys.IConfirmacionAutenticacionServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.resultados.to.ParamTo;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.reactivacion.R;

/**
 * @author lbermejo
 *
 */
public class ConfirmacionAutenticacionServiceProxy implements IConfirmacionAutenticacionServiceProxy, Serializable {
	
	/**
	 */
	private static final long serialVersionUID = 3925667201345924488L;
	private BaseDelegateCommons baseDelegateCommons;
	
	public ConfirmacionAutenticacionServiceProxy(BaseDelegateCommons bdc ) {
		this.baseDelegateCommons = bdc;
	}
	
	@Override
	public ArrayList<Object> getListaDatos() {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getListaDatos >> delegate");
		final ConfirmacionAutenticacionDelegate delegate = (ConfirmacionAutenticacionDelegate)baseDelegateCommons;
		final ArrayList<Object> list = delegate.consultaOperationsDelegate().getDatosTablaConfirmacion();
		return list;				
	}

	@Override
	public ConfirmacionViewTo showFields() {
		final ConfirmacionViewTo to = new ConfirmacionViewTo();
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy showFields >> delegate");
		final ConfirmacionAutenticacionDelegate delegate = 
				(ConfirmacionAutenticacionDelegate)baseDelegateCommons;
		to.setShowContrasena(delegate.consultaDebePedirContrasena());
		to.setShowNip(delegate.consultaDebePedirNIP());
		to.setTokenAMostrar(delegate.consultaInstrumentoSeguridad());
		to.setShowCvv(delegate.consultaDebePedirCVV());
		to.setShowTarjeta(delegate.mostrarCampoTarjeta() );
		to.setTokenAMostrar(delegate.consultaInstrumentoSeguridad());
		to.setInstrumentoSeguridad(delegate.consultaTipoInstrumentoSeguridad());
		to.setTextoAyudaInsSeg(
				delegate.getTextoAyudaInstrumentoSeguridad(
						to.getInstrumentoSeguridad()));
		/* 
			//mostrarContrasena(confirmacionDelegate.consultaDebePedirContrasena());
			//mostrarNIP(confirmacionDelegate.consultaDebePedirNIP());
			//mostrarASM(confirmacionDelegate.consultaInstrumentoSeguridad());
			//mostrarCVV(confirmacionDelegate.consultaDebePedirCVV());
			//mostrarCampoTarjeta(confirmacionDelegate.mostrarCampoTarjeta());  */
		return to;
	}
	
	@Override
	public Integer getMessageAsmError(final TipoInstrumento tipoInstrumento) {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getMessageAsmError >> delegate");
		int idMsg = 0;

		switch (tipoInstrumento) {
			case OCRA:
				idMsg = R.string.confirmation_ocra;
				break;
			case DP270:
				idMsg = R.string.confirmation_dp270;
				break;
			case SoftToken:
				if (SuiteApp.getSofttokenStatus()) {
					idMsg = R.string.confirmation_softtokenActivado;
				} else {
					idMsg = R.string.confirmation_softtokenDesactivado;
				}
				break;
			default:
				break;
		}

		return idMsg;
		
		/*switch (tipoInstrumentoSeguridad) {
			case OCRA:
				mensaje += getEtiquetaCampoOCRA();
				break;
			case DP270:
				mensaje += getEtiquetaCampoDP270();
				break;
			case SoftToken:
				if (SuiteApp.getSofttokenStatus()) {
					mensaje += getEtiquetaCampoSoftokenActivado();
				} else {
					mensaje += getEtiquetaCampoSoftokenDesactivado();
				}
				break;
			default:
				break;
		}
		*/ 
	}

	@Override
	public String loadOtpFromSofttoken(final TipoOtpAutenticacion tipoOtpAutenticacion) {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy loadOtpFromSofttoken >> delegate");
		final ConfirmacionAutenticacionDelegate delegate = 
				(ConfirmacionAutenticacionDelegate)baseDelegateCommons;
		//final String res = delegate.loadOtpFromSofttoken(tipoOtpAutenticacion);
		return delegate.loadOtpFromSofttoken(tipoOtpAutenticacion);
	}

	@Override
	public Integer consultaOperationsIdTextoEncabezado(){
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy consultaOperationsIdTextoEncabezado >> delegate");
		//getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()
		final ConfirmacionAutenticacionDelegate delegate = 
								(ConfirmacionAutenticacionDelegate)baseDelegateCommons;
		return delegate.consultaOperationsDelegate().getTextoEncabezado();
	}

	@Override
	public Boolean doOperation(final ParamTo to) {
		final ConfirmacionAutenticacionDelegate delegate =
				(ConfirmacionAutenticacionDelegate)baseDelegateCommons;
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy doOperation >> delegate");
		final ConfirmacionViewTo params = (ConfirmacionViewTo)to;
		final ConfirmacionAutenticacionViewController caller = new ConfirmacionAutenticacionViewController();
		caller.setDelegate(delegate);

		try {
			delegate.getOperationDelegate().realizaOperacion(caller, params.getContrasena(),
					params.getNip(), params.getAsm(),
					params.getCvv(), params.getTarjeta());

			return Boolean.TRUE;
		}catch(Exception e){
			//TODO Log
			return Boolean.FALSE;
		}
	}

	@Override
	public BaseDelegateCommons getDelegate() {
		return baseDelegateCommons;
	}

}
