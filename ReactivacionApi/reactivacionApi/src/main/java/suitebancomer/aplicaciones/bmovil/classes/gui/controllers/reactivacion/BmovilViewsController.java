package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion;


import android.app.Activity;

import com.bancomer.mbanking.activacion.SuiteApp;
import com.bancomer.mbanking.reactivacion.BmovilApp;
import com.bancomer.mbanking.reactivacion.SuiteAppApiReactivacion;

import java.util.ArrayList;

import suitebancomer.activacion.aplicaciones.bmovil.classes.entrada.InitActivacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.ConfirmacionAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.DelegateBaseAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.proxys.ConfirmacionAutenticacionServiceProxy;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.handlers.ConfirmacionAutenticacionHandler;
import suitebancomer.classes.gui.controllers.reactivacion.BaseViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.model.BanderasServer;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Reactivacion;
import tracking.reactivacion.TrackingHelper;

public class BmovilViewsController extends BaseViewsController {

	private BmovilApp bmovilApp;
	//AMZ
	public ArrayList<String> estados = new ArrayList<String>();
//	private MenuPrincipalViewController controladorMenuPrincipal = new MenuPrincipalViewController();
	//AMZ
	
	/*
	 * @Override public void setCurrentActivity(BaseViewController
	 * currentViewController) { // TODO Auto-generated method stub
	 * super.setCurrentActivity(currentViewController); Log.i("Si", "No"); }
	 */
	public BmovilViewsController(final BmovilApp bmovilApp) {
		super();
		this.bmovilApp = bmovilApp;
	}

	@Override
	public void cierraViewsController() {
		bmovilApp = null;
		clearDelegateHashMap();
		super.cierraViewsController();
	}
	
	public BmovilApp getBmovilApp() {
		return bmovilApp;
	}

	public void setBmovilApp(final BmovilApp bmovilApp) {
		this.bmovilApp = bmovilApp;
	}

	public void cerrarSesionBackground() {
		SuiteAppApiReactivacion.getInstance().getBmovilApplication()
				.setApplicationInBackground(true);
		bmovilApp.logoutApp(true);
	}

	@Override
	public void showMenuInicial() {
		showMenuPrincipal();
	}

	public void showMenuPrincipal() {

	}

	@Override
	public boolean consumeAccionesDeReinicio() {
		if (!SuiteAppApiReactivacion.getInstance().getBmovilApplication()
				.isApplicationLogged()) {
			showViewController(suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.ConfirmacionAutenticacionViewController.class, SuiteAppApiReactivacion.getInstance().getBmovilApplication().getViewsController().getCurrentViewController());
//			showViewController(SuiteAppApiReactivacion.getInstance().getIntentRedirecActivavion().getIntent(), Intent.FLAG_ACTIVITY_NEW_TASK, true, new String[]{}, new Object[]{});
			return true;
		}
		return false;
	}
//
	@Override
	public boolean consumeAccionesDePausa() {
		if (SuiteAppApiReactivacion.getInstance().getBmovilApplication().isApplicationLogged()
				&& !isActivityChanging()) {
			cerrarSesionBackground();
			return true;
		}
		return false;
	}

	@Override
	public boolean consumeAccionesDeAlto() {
		if (!SuiteAppApiReactivacion.getInstance().getBmovilApplication().isChangingActivity()
				&& currentViewControllerApp != null) {
			currentViewControllerApp.hideSoftKeyboard();
		}
		SuiteAppApiReactivacion.getInstance().getBmovilApplication()
				.setChangingActivity(false);
		return true;
	}

	/**
	 * Muestra la pantalla de confirmacion autenticacion
	 */
	public void showConfirmacionAutenticacionViewController(
			final DelegateBaseAutenticacion autenticacionDelegate, final int resIcon,
			final int resTitle, final int resSubtitle) {
//		showConfirmacionAutenticacionViewController(autenticacionDelegate,
//				resIcon, resTitle, resSubtitle, R.color.primer_azul);
		ConfirmacionAutenticacionDelegate confirmacionDelegate = (ConfirmacionAutenticacionDelegate) getBaseDelegateForKey(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		if (confirmacionDelegate != null) {
		removeDelegateFromHashMap(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		}
		confirmacionDelegate = new ConfirmacionAutenticacionDelegate(
				autenticacionDelegate);
		addDelegateToHashMap(
				ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID,
				confirmacionDelegate);
		//AMZ
				if(estados.size() == 0)
				{
					//AMZ
					TrackingHelper.trackState("reactivacion", estados);
					//AMZ
				}
	//	showViewController(ConfirmacionAutenticacionViewController.class);

		final Integer idText = confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado();
		final Integer idNombreImagenEncabezado = confirmacionDelegate.consultaOperationsDelegate().getNombreImagenEncabezado();
		final Boolean statusDesactivado =false;
		/*
				Boolean statusDesactivado = !( (confirmacionDelegate.getOperationDelegate() instanceof
				suitebancomer.aplicaciones.bmovil.classes.gui.delegates.QuitarSuspencionDelegate )
				|| (confirmacionDelegate.getOperationDelegate() instanceof
				suitebancomer.aplicaciones.bmovil.classes.gui.delegates.NuevaContraseniaDelegate
				|| (confirmacionDelegate.getOperationDelegate() instanceof
				suitebancomercoms.aplicaciones.bmovil.classes.gui.delegates.ReactivacionDelegate) ));
*/
		final SuiteAppCRApi cr=new SuiteAppCRApi();
		cr.onCreate(SuiteAppApiReactivacion.appContext);
		final ConfirmacionAutenticacionServiceProxy proxy = new ConfirmacionAutenticacionServiceProxy(confirmacionDelegate);
		SuiteAppCRApi.setCallBackBConnect(SuiteAppApiReactivacion.getReturnMenu());
		cr.setProxy(proxy);
		final ConfirmacionAutenticacionHandler handler = new ConfirmacionAutenticacionHandler(proxy, this,new ConfirmacionAutenticacionViewController() );
		handler.setActivityChanging(isActivityChanging());



		handler.setDelegateId(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		handler.invokeActivity(this.estados,idText, idNombreImagenEncabezado, statusDesactivado);

	}


		public void showActivacion(final ConsultaEstatus ce, final String password) {

			/*suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate aDelegate = (suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate) getBaseDelegateForKey(suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate.ACTIVACION_DELEGATE_ID);
			if (aDelegate != null)
				removeDelegateFromHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID);

			aDelegate = new suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate(
					ce, password);

			addDelegateToHashMap(
					ActivacionDelegate.ACTIVACION_DELEGATE_ID, aDelegate);
			// AMZ
			TrackingHelper.trackState("activacion", estados);
			// AMZ
			showViewController(suitebancomer.activacion.aplicaciones.bmovil.classes.gui.controllers.ActivacionViewController.class);*/
			final InitActivacion initActivacion = new InitActivacion(new BanderasServer(ServerCommons.SIMULATION,
					ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT),
					(Activity)com.bancomer.base.SuiteApp.appContext,
					SuiteAppApiReactivacion.getReturnMenu(),
					SuiteAppApiReactivacion.getIntentToReturn());
			final String instrumento = Session.getInstance(SuiteAppApiReactivacion.appContext).getSecurityInstrument();
			suitebancomercoms.aplicaciones.bmovil.classes.common.Session sessionApis = suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(SuiteAppApiReactivacion.appContext);
			sessionApis.setSecurityInstrument(instrumento);
			SuiteApp.setCallsAppDesactivada(Boolean.TRUE);
			initActivacion.showActivacion(ce, password, estados);
		}

		// TODO
		public void showActivacion(final Reactivacion reactivacion) {

			/*suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate aDelegate = (suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate) getBaseDelegateForKey(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
			if (aDelegate != null)
				removeDelegateFromHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID);

			aDelegate = new suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate(reactivacion);
			addDelegateToHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID, aDelegate);

			TrackingHelper.trackState("activacion", estados);

			showViewController(suitebancomer.activacion.aplicaciones.bmovil.classes.gui.controllers.ActivacionViewController.class);*/
			final InitActivacion initActivacion = new InitActivacion(new BanderasServer(ServerCommons.SIMULATION,
					ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT),
					SuiteAppApiReactivacion.getIntentToReturn(),
					SuiteAppApiReactivacion.getReturnMenu(),
					SuiteAppApiReactivacion.getIntentToReturn());
			final String instrumento = Session.getInstance(SuiteAppApiReactivacion.appContext).getSecurityInstrument();
			suitebancomercoms.aplicaciones.bmovil.classes.common.Session sessionApis = suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(SuiteAppApiReactivacion.appContext);
			sessionApis.setSecurityInstrument(instrumento);
			SuiteApp.setCallsAppDesactivada(Boolean.TRUE);
			initActivacion.showActivacion(reactivacion, estados);
		}

	public void touchAtras()
	{

		final int ultimo = estados.size()-1;
		
		final String eliminado;
		if(ultimo >= 0)
		{
			final String ult = estados.get(ultimo);
			if(ult.equals("reactivacion") || ult.equals("activacion") || ult.equals("menu token") || ult.equals("contratacion datos") || ult.equals("activacion datos"))
			{
				estados.clear();
			}else
			{
				 eliminado = estados.remove(ultimo);
			}
		} 
	}

}