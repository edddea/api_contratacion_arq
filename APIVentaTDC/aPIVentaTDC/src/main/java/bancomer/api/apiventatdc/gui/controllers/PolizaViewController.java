package bancomer.api.apiventatdc.gui.controllers;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.atomic.AtomicBoolean;

import bancomer.api.apiventatdc.R;
import bancomer.api.apiventatdc.commons.ConstantsVentaTDC;
import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.timer.TimerController;

/**
 * Created by OOROZCO on 9/1/15.
 */
public class PolizaViewController extends Activity {

    private BaseViewController baseViewController;
    private InitVentaTDC init = InitVentaTDC.getInstance();

    private WebView wvpoliza;
    private GestureDetector gestureDetector;
    private AtomicBoolean mPreventAction = new AtomicBoolean(false);


    TextView lblTitulo;

    String termino;
    String contrato;
    String domiciliacion;
    String codeHTML;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        init.getParentManager().setCurrentActivityApp(this);

        baseViewController = init.getBaseViewController();
        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_venta_tdc_poliza);
        baseViewController.setTitle(this, R.string.promocion_title_ventaTDC, R.drawable.an_ic_venta_tdc);
        init.setBaseViewController(baseViewController);

        termino= (String)this.getIntent().getExtras().get(ConstantsVentaTDC.TERMINOS_VENTATDC);
        contrato= (String)this.getIntent().getExtras().get(ConstantsVentaTDC.CONTRATO_VENTATDC);
        domiciliacion= (String)this.getIntent().getExtras().get(ConstantsVentaTDC.DOMICILIACION_VENTATDC);

        ViewGroup scroll = (ViewGroup) findViewById(R.id.body_layout);
        ViewGroup parent = (ViewGroup) scroll.getParent();
        parent.removeView(scroll);

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.api_venta_tdc_poliza, parent, true);

        findViews();
        scaleToScreenSize();


        WebSettings settings = wvpoliza.getSettings();
        String result;
        if(null != termino){
            /*wvpoliza.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            wvpoliza.getSettings().setBuiltInZoomControls(true);
            wvpoliza.loadUrl(termino);*/
            leerArchivo(termino);
            wvpoliza.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
            wvpoliza.getSettings().setBuiltInZoomControls(true);
            double factor = GuiTools.getScaleFactor();
            wvpoliza.setInitialScale((int) (122*factor));
            wvpoliza.loadData(codeHTML, "text/html", "utf-8");
        }else if(null != contrato){
            result=parse(contrato);
            settings.setUseWideViewPort(true);
            double factor = GuiTools.getScaleFactor();
            wvpoliza.setInitialScale((int) (122*factor));
            wvpoliza.loadData(result, "text/html", "utf-8");
        }else if(null != domiciliacion){
            leerArchivo(domiciliacion);
            wvpoliza.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            wvpoliza.getSettings().setBuiltInZoomControls(true);
            wvpoliza.loadData(codeHTML, "text/html", "utf-8");
        }

        gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onDoubleTap(MotionEvent e) {
                mPreventAction.set(true);
                return true;
            }
            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                mPreventAction.set(true);
                return true;
            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                mPreventAction.set(true); // this is the key! this would block double tap to zoom to fire
                return false;
            }

        });
        gestureDetector.setIsLongpressEnabled(true);
        wvpoliza.setOnTouchListener(new View.OnTouchListener(){
            @TargetApi(Build.VERSION_CODES.FROYO)
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int pointId = 0;
                if(Build.VERSION.SDK_INT > 7){
                    int index = (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK)>>MotionEvent.ACTION_POINTER_INDEX_SHIFT;
                    pointId = event.getPointerId(index);
                }
                if (pointId == 0){
                    gestureDetector.onTouchEvent(event);
                    if (mPreventAction.get()){
                        mPreventAction.set(false);
                        return true;
                    }
                    return wvpoliza.onTouchEvent(event);
                }
                else return true;
            }
        });

    }

    private String parse(String source){
        StringBuilder data = new StringBuilder();
        data.append(source);
        data.insert(data.indexOf("head")+7, "<meta name=\"viewport\" content=\"height=device-height,width=1280\"/>");
        return data.toString().replace("divstyle", "div style").replace("tdcolspan","td colspan")
                .replace("tdwidth", "td width")
                .replace("solid1px", "solid 1px")
                .replace("width:801px", " width:800px")
                .replace("</body></html><html><head><title>Contrato - Contrato Credit&oacute;n N&oacute;mina sin Seguro</title></head><body>","")
                ;
    }

    private void findViews() {
        wvpoliza = (WebView)findViewById(R.id.poliza);
        lblTitulo=(TextView)findViewById(R.id.lblTitulo);
        if(null != termino)
            lblTitulo.setText(R.string.clausulas_ventatdc_terminosycondiciones_web);
        else if(null != contrato)
            lblTitulo.setText(R.string.exito_ventatdc_contratolink1);
        else if(null != domiciliacion)
            lblTitulo.setText(R.string.exito_ventatdc_contratolink2);
    }


    private void scaleToScreenSize() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.layoutBaseContainer));
        guiTools.scale(findViewById(R.id.lblTitulo), true);
        guiTools.scale(wvpoliza);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        float touchX = ev.getX();
        float touchY = ev.getY();
        int[] webViewPos = new int[2];

        wvpoliza.getLocationOnScreen(webViewPos);

        float listaSeleccionX2 = webViewPos[0] + wvpoliza.getMeasuredWidth();
        float listaSeleccionY2 = webViewPos[1] + wvpoliza.getMeasuredHeight();

        if ((touchX >= webViewPos[0] && touchX <= listaSeleccionX2) &&
                (touchY >= webViewPos[1] && touchY <= listaSeleccionY2)) {
            wvpoliza.getParent().requestDisallowInterceptTouchEvent(true);
        }

        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(!init.getParentManager().isActivityChanging())
        {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            init.getParentManager().setActivityChanging(true);
            super.onBackPressed();
        }
        return true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    private void leerArchivo(String url){
        try {
            FileInputStream fIn = new FileInputStream(url);
            InputStreamReader archivo = new InputStreamReader(fIn);
            BufferedReader br = new BufferedReader(archivo);
            String linea = br.readLine();
            codeHTML = "";
            while (linea != null) {
                codeHTML += linea + "";
                linea = br.readLine();
            }
            br.close();
            archivo.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
