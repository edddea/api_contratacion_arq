package bancomer.api.apiventatdc.gui.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.adobe.mobile.Config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.apiventatdc.R;
import bancomer.api.apiventatdc.commons.TrackerVentaTDC;
import bancomer.api.apiventatdc.gui.delegates.ExitoDelegate;
import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;

/**
 * Created by OOROZCO on 9/1/15.
 */
public class ExitoViewController extends Activity implements View.OnClickListener{

    public BaseViewsController parentManager;
    private BaseViewController baseViewController;
    private InitVentaTDC init = InitVentaTDC.getInstance();
    private ExitoDelegate delegate;

    private TextView txtImporteTDC,lblTextoTeRecordamos,lblTextoInfoContratos,linkContrato,linkFormatoDom,lblCorreoOferta,lblinf1,lblsmsOferta,lblinf2;
    private EditText editTextemail,editTextNumero;
    private ImageButton btnMenu;

    //Adobe
    String screen = "";
    ArrayList<String> list= new ArrayList<String>();
    Map<String,Object> operacionMap = new HashMap<String, Object>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate=(ExitoDelegate) parentManager.getBaseDelegateForKey(ExitoDelegate.EXITO_DELEGATE_VENTA_TDC);
        delegate.setController(this);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_venta_tdc_exito);
        baseViewController.setTitle(this, R.string.promocion_title_ventaTDC, R.drawable.an_ic_venta_tdc);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        findViews();
        scaleForCurrentScreen();
        delegate.setValues();

        //Adobe
        Config.setContext(this.getApplicationContext());

        screen = "exito venta tdc";
        list.add(screen);
        TrackerVentaTDC.trackState(list);

        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackerVentaTDC.trackEntrada(eventoEntrada);


    }


    private void findViews(){

        txtImporteTDC = (TextView) findViewById(R.id.txtImporteTDC);
        lblTextoTeRecordamos = (TextView) findViewById(R.id.lblTextoTeRecordamos);
        lblTextoInfoContratos = (TextView) findViewById(R.id.lblTextoInfoContratos);
        linkContrato= (TextView) findViewById(R.id.linkContrato);
        linkFormatoDom = (TextView) findViewById(R.id.linkFormatoDom);


        lblCorreoOferta = (TextView) findViewById(R.id.lblCorreoOferta);
        lblinf1 = (TextView) findViewById(R.id.lblinf1);
        lblsmsOferta = (TextView) findViewById(R.id.lblsmsOferta);
        lblinf2 = (TextView) findViewById(R.id.lblinf2);

        editTextemail = (EditText) findViewById(R.id.editTextemail);
        editTextNumero = (EditText) findViewById(R.id.editTextNumero);

        btnMenu = (ImageButton) findViewById(R.id.btnMenu);

        linkContrato.setOnClickListener(this);
        linkFormatoDom.setOnClickListener(this);
        btnMenu.setOnClickListener(this);

    }


    private void scaleForCurrentScreen() {
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(findViewById(R.id.lblTextoFelicidades), true);
        gTools.scale(findViewById(R.id.txtImporteTDC), true);
        gTools.scale(findViewById(R.id.lblTextoTeRecordamos), true);
        gTools.scale(findViewById(R.id.lblTextoInfoContratos), true);
        gTools.scale(findViewById(R.id.linkContrato), true);
        gTools.scale(findViewById(R.id.linkFormatoDom), true);
        //gTools.scale(findViewById(R.id.imagSeparador));
        //gTools.scale(findViewById(R.id.lblTextoInfoNotificacion), true);
        gTools.scale(findViewById(R.id.lblCorreoOferta), true);
        gTools.scale(findViewById(R.id.editTextemail), true);
        gTools.scale(findViewById(R.id.lblinf1), true);
        gTools.scale(findViewById(R.id.lblsmsOferta), true);
        gTools.scale(findViewById(R.id.editTextNumero), true);
        gTools.scale(findViewById(R.id.lblinf2), true);
        gTools.scale(findViewById(R.id.btnMenu));

    }


    public void showMailComponents(String mail)
    {
        lblCorreoOferta.setVisibility(View.VISIBLE);
        lblinf1.setVisibility(View.VISIBLE);
        editTextemail.setText(mail);
        editTextemail.setVisibility(View.VISIBLE);
        editTextemail.setEnabled(false);
    }


    public void showSMSComponents(String phone)
    {
        lblsmsOferta.setVisibility(View.VISIBLE);
        lblinf2.setVisibility(View.VISIBLE);
        editTextNumero.setText(Tools.hideUsername(phone));
        editTextNumero.setVisibility(View.VISIBLE);
        editTextNumero.setEnabled(false);
    }


    public void hideLinkContrato()
    {
        linkContrato.setVisibility(View.GONE);
    }


    public void setAmount(String amount)
    {
        SpannableString amount1 = new SpannableString(Tools.formatAmount(amount,false));
        amount1.setSpan(new UnderlineSpan(), 0, amount1.length(), 0);
        txtImporteTDC.setText(amount1);

        //MAPE txtImporteTDC.setText(Tools.formatAmount(amount, false));
    }


    public void showAddressParagraphs()
    {
        lblTextoTeRecordamos.setText(R.string.exito_ventatdc_texto2a);
        lblTextoInfoContratos.setText(R.string.exito_ventatdc_texto3a);
    }

    public void showOfficeParagraphs()
    {
        lblTextoTeRecordamos.setText(R.string.exito_ventatdc_texto2b);
        lblTextoInfoContratos.setText(R.string.exito_ventatdc_texto3b);
    }





    @Override
    public void onClick(View v) {

        if(v == linkFormatoDom)
        {
            delegate.performAction(null);
        }else if(v == btnMenu)
        {
            init.getParentManager().setActivityChanging(true);
            //init.getParentManager().showMenuInicial();
            ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
            init.resetInstance();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        Config.pauseCollectingLifecycleData();
        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        baseViewController.setActivity(this);
        init.setBaseViewController(baseViewController);


    }

    @Override
    protected void onPause() {
        super.onPause();
        Config.collectLifecycleData();
        if(!init.getParentManager().isActivityChanging())
        {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
        }
        return true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }
}
