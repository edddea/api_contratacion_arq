package bancomer.api.apiventatdc.gui.delegates;

import android.content.DialogInterface;
import android.util.Log;

import java.util.Hashtable;

import bancomer.api.apiventatdc.commons.ConstantsVentaTDC;
import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bancomer.api.apiventatdc.io.Server;
import bancomer.api.apiventatdc.models.OfertaTDC;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.common.io.ServerResponse;

/**
 * Created by OOROZCO on 9/3/15.
 */
public class ConsultaDelegate implements BaseDelegate {

    private InitVentaTDC init = InitVentaTDC.getInstance();
    private BaseViewController baseViewController;



    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, final boolean isJson,final ParsingHandler handler) {
        init.getBaseSubApp().invokeNetworkOperation(operationId,params,caller,false,isJson,handler);
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
        //metodo vacio
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

        Log.w("Gets into analyze", "Going good");
        if (response.getStatus() == ServerResponse.OPERATION_ERROR)
        {
            Log.w("Gets into Error", "Going good");
            init.getParentManager().resetRootViewController();
            init.updateHostActivityChangingState();

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };
            init.getBaseViewController().showInformationAlert(init.getParentManager().getCurrentViewController(),response.getMessageText(),listener);
            init.resetInstance();
        }
        else if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)
        {
            Log.w("Gets into Successful", "Going good");
            init.showDetalle((OfertaTDC)response.getResponse());
        }

    }

    @Override
    public void performAction(Object obj) {

        baseViewController = init.getBaseViewController();
        baseViewController.setActivity(init.getParentManager().getCurrentViewController());
        baseViewController.setDelegate(this);
        init.setBaseViewController(baseViewController);

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(ConstantsVentaTDC.NUMERO_CELULAR_TAG, init.getSession().getUsername());
        params.put(ConstantsVentaTDC.CVE_CAMP_TAG, init.getOfertaVentaTDC().getCveCamp());
        params.put(ConstantsVentaTDC.IUM_TAG, init.getSession().getIum());
        params.put(ConstantsVentaTDC.OFICINA_TAG, init.getCuentaOneClick().substring(4,8));


        doNetworkOperation(Server.CONSULTA_DETALLE_OFERTA_TDC, params, InitVentaTDC.getInstance().getBaseViewController(),true,new OfertaTDC());



    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }
}
