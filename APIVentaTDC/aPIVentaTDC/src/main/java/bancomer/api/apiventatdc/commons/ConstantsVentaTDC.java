package bancomer.api.apiventatdc.commons;


public class ConstantsVentaTDC {

    public static final String LINEA_CREDITO_TAG="lineaCredito";
    public static final String PRODUCTO_TAG="producto";
    public static final String DES_PRODUCTO_TAG="desProducto";
    public static final String CODIGO_OTP_TAG="codigoOTP";
    public static final String CADENA_AUTENTICACION_TAG="cadenaAutenticacion";
    public static final String CODIGO_NIP_TAG="codigoNip";
    public static final String CODIGO_CVV2_TAG="codigoCVV2";
    public static final String TARJETA_5IG_TAG="Tarjeta5ig";
    public static final String OPERACION_TAG="operacion";
    public static final String NUMERO_CELULAR_TAG="numeroCelular";
    public static final String CVE_CAMP_TAG="cveCamp";
    public static final String IUM_TAG="IUM";
    public static final String TERMINOS_VENTATDC="terminosVentaTDC";
    public static final String CONTRATO_VENTATDC="contratoVentaTDC";
    public static final String DOMICILIACION_VENTATDC="domiciliacionVentaTDC";
    public static final String IND_VACIO="indVacio";
    public static final String IND_VACIO_VALUE="1";
    public static final String IND_CONTRATO="indContrato";
    public static final String IND_CONTRATO_VALUE="D";
    public static final String NUM_CONTRATO="numContrato";
    public static final String SOLO_COMPRAS_VALUE="N";
    public static final String OPCION_DOMICILIO_VALUE="SUCURSAL";
    public static final String URL_TERMS_AND_CONDITIONS = "https://www.bancomermovil.net:11443/mbank/mbank/Descargas/OneClickMB/PB/";
    public static final String END_URL=".html";
    public static final String CAT_TAG="cat";
    public static final String FECHA_CAT_TAG="fechaCat";
    public static final String ANUALIDAD_TAG="anualidad";
    public static final String TAZA_PONDERADA_TAG="tazaPonderada";
    public static final String DOMICILIO_TAG="domicilio";
    public static final String DOMICILIO_SUCURSAL_TAG="domicilioSucursal";
    public static final String SOLO_COMPRAS_TAG="soloCompras";
    public static final String ID_PRODUCTO_TAG="idProducto";
    public static final String DOMICILIACION_TAG="domiciliacion";
    public static final String CVE_ACCESO_TAG="cveAcceso";
    public static final String OPCION_DOMICILIO_TAG="opcionDomicilio";
    public static final String OFICINA_TAG="oficina";
    public static final String IND_VACIO_VALUE_TERM="0";
    public static final String IND_CONTRATO_VALUE_TERM="C";
}
