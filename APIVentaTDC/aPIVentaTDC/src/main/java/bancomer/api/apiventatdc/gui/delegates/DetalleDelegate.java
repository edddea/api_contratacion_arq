package bancomer.api.apiventatdc.gui.delegates;

import android.util.Log;

import java.util.Hashtable;


import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bancomer.api.apiventatdc.io.Server;
import bancomer.api.apiventatdc.models.OfertaTDC;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by OOROZCO on 8/28/15.
 */
public class DetalleDelegate implements BaseDelegate {

    public static final long DETALLE_DELEGATE_VENTA_TDC = 110909334566765662L;


    OfertaTDC ofertaTDC;
    Promociones promocion;
    String fechaCatVisual;
    InitVentaTDC init = InitVentaTDC.getInstance();

    public DetalleDelegate(OfertaTDC oferta)
    {
        this.ofertaTDC = oferta;
        this.promocion = init.getOfertaVentaTDC();
    }

    public OfertaTDC getOfertaTDC() {
        return ofertaTDC;
    }

    public Promociones getPromocion() {
        return promocion;
    }

    public String getFechaCatVisual() {
        return fechaCatVisual;
    }

    public void formatoFechaCatMostrar(){
        try{
            String[] meses = { "enero", "febrero", "marzo", "abril", "mayo",
                    "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };
            String fechaCatM= ofertaTDC.getFechaCat();
            int mesS=Integer.parseInt(fechaCatM.substring(5,7));
            String dia= fechaCatM.substring(fechaCatM.length()-2);
            String anio= fechaCatM.substring(0,4);

            fechaCatVisual= dia+" de "+meses[mesS-1].toString()+ " de "+anio;

        }catch(Exception ex){
            if(Server.ALLOW_LOG) Log.e("", "error formato" + ex);
        }
    }



    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {

    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

    }

    @Override
    public void performAction(Object obj) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }
}
