package bancomer.api.apiventatdc.models;

import android.util.Log;

import java.io.IOException;

import bancomer.api.apiventatdc.commons.ConstantsVentaTDC;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by OOROZCO on 8/28/15.
 */
public class OfertaTDC implements ParsingHandler {


    String lineaCredito;
    String cat;
    String fechaCat;
    String anualidad;
    String producto;
    String tazaPonderada;
    String domicilio;
    String domicilioSucursal;
    String soloCompras;
    String desProducto;





    public String getDesProducto() {
        return desProducto;
    }

    public void setDesProducto(String desProducto) {
        this.desProducto = desProducto;
    }

    public String getLineaCredito() {
        return lineaCredito;
    }

    public void setLineaCredito(String lineaCredito) {
        this.lineaCredito = lineaCredito;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getFechaCat() {
        return fechaCat;
    }

    public void setFechaCat(String fechaCat) {
        this.fechaCat = fechaCat;
    }

    public String getAnualidad() {
        return anualidad;
    }

    public void setAnualidad(String anualidad) {
        this.anualidad = anualidad;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getSoloCompras() {
        return soloCompras;
    }

    public void setSoloCompras(String soloCompras) {
        this.soloCompras = soloCompras;
    }

    public String getTazaPonderada() {
        return tazaPonderada;
    }

    public void setTazaPonderada(String tazaPonderada) {
        this.tazaPonderada = tazaPonderada;
    }

    public String getDomicilioSucursal() {
        return domicilioSucursal;
    }

    public void setDomicilioSucursal(String domicilioSucursal) {
        this.domicilioSucursal = domicilioSucursal;
    }


    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        lineaCredito = parser.hasValue(ConstantsVentaTDC.LINEA_CREDITO_TAG) ? parser.parseNextValue(ConstantsVentaTDC.LINEA_CREDITO_TAG) : "";
        producto = parser.hasValue(ConstantsVentaTDC.PRODUCTO_TAG) ? parser.parseNextValue(ConstantsVentaTDC.PRODUCTO_TAG) : "";
        desProducto = parser.hasValue(ConstantsVentaTDC.DES_PRODUCTO_TAG) ? parser.parseNextValue(ConstantsVentaTDC.DES_PRODUCTO_TAG) : "";
        cat = parser.hasValue(ConstantsVentaTDC.CAT_TAG) ? parser.parseNextValue(ConstantsVentaTDC.CAT_TAG) : "";
        fechaCat = parser.hasValue(ConstantsVentaTDC.FECHA_CAT_TAG) ? parser.parseNextValue(ConstantsVentaTDC.FECHA_CAT_TAG) : "";
        anualidad = parser.hasValue(ConstantsVentaTDC.ANUALIDAD_TAG) ? parser.parseNextValue(ConstantsVentaTDC.ANUALIDAD_TAG) : "";
        tazaPonderada = parser.hasValue(ConstantsVentaTDC.TAZA_PONDERADA_TAG) ? parser.parseNextValue(ConstantsVentaTDC.TAZA_PONDERADA_TAG) : "";
        domicilio = parser.hasValue(ConstantsVentaTDC.DOMICILIO_TAG) ? parser.parseNextValue(ConstantsVentaTDC.DOMICILIO_TAG) : "";
        domicilioSucursal = parser.hasValue(ConstantsVentaTDC.DOMICILIO_SUCURSAL_TAG) ? parser.parseNextValue(ConstantsVentaTDC.DOMICILIO_SUCURSAL_TAG) : "";
        soloCompras = parser.hasValue(ConstantsVentaTDC.SOLO_COMPRAS_TAG) ? parser.parseNextValue(ConstantsVentaTDC.SOLO_COMPRAS_TAG) : "";

    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }
}
