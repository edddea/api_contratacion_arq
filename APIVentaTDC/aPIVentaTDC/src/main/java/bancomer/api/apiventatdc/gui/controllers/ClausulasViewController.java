package bancomer.api.apiventatdc.gui.controllers;


import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.adobe.mobile.Config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.apiventatdc.R;
import bancomer.api.apiventatdc.commons.TrackerVentaTDC;
import bancomer.api.apiventatdc.gui.delegates.ClausulasDelegate;
import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;

/**
 * Created by OOROZCO on 8/31/15.
 */
public class ClausulasViewController extends Activity implements View.OnClickListener  {


    private BaseViewController baseViewController;
    private InitVentaTDC init = InitVentaTDC.getInstance();
    private ClausulasDelegate delegate;
    public BaseViewsController parentManager;



    /** View Elements **/


    // layout checkbox buro de credito
    private LinearLayout linearChecKBox1;
    private TextView clausula1;
    private CheckBox cbClausula1;

    // layout checkbox aceptacion de tarjeta
    private LinearLayout linearChecKBox2;
    private TextView clausula2;
    private CheckBox cbClausula2;

    // layout checkbox recepcion de tarjeta
    private LinearLayout linearRBEnvioTarjeta;
    private TextView txtSucursal;
    private RadioButton rbSucursal;

    // layout checkbox domiciliacion
    private LinearLayout linearClausula4;
    private TextView clausula4;
    private CheckBox cbClausula4;
    private TextView clausula4_aviso;
    private LinearLayout linearClausula4_aviso;
    private LinearLayout layoutInfoClausula4;

    // layout checkbox terminos y condiciones
    private LinearLayout linearClausula5;
    private TextView linkClausula5;
    private CheckBox cbClausula5;

    private LinearLayout layout_avisotoken;
    private LinearLayout contenedorContrasena, contenedorNIP,contenedorASM,contenedorCVV,contenedorCampoTarjeta;
    private TextView campoContrasena,campoNIP,campoASM,campoCVV,campoTarjeta;
    private EditText contrasena,nip,asm,cvv,tarjeta;
    private TextView instruccionesContrasena,instruccionesNIP,instruccionesASM,instruccionesCVV,instruccionesTarjeta;

    private LinearLayout vista;
    private ImageButton btnConfirmacion;

    //variables para reglas de negocio
    private boolean isBuro=true;
    private boolean isAceptacionTarjeta=true;
    //private boolean isAnualidad=true;
    private boolean isDomiciliacion=true;
    //private boolean isAdicionaTarjeta = true;
    private boolean isSelectedClauses=false;

    private String soloCompras="";
    private String domiciliacion ="";
    private String opcionDomicilio="";

    String textoSucursal;
    String domSucursal;
    int gris1;
    int verde;
    int azul;

    //Adobe
    String screen = "";
    ArrayList<String> list= new ArrayList<String>();
    Map<String,Object> operacionMap = new HashMap<String, Object>();


    public String getOpcionDomicilio() {
        return opcionDomicilio;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate=(ClausulasDelegate) parentManager.getBaseDelegateForKey(ClausulasDelegate.CLAUSULAS_VENTA_TDC_DELEGATE);
        delegate.setController(this);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_venta_tdc_clausulas);
        baseViewController.setTitle(this, R.string.promocion_title_ventaTDC, R.drawable.an_ic_venta_tdc);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        findViews();
        setSecurityComponents();
        scaleForCurrentScreen();

        //Adobe
        Config.setContext(this.getApplicationContext());

        screen = "clausulas venta tdc";
        list.add(screen);
        TrackerVentaTDC.trackState(list);

        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackerVentaTDC.trackEntrada(eventoEntrada);



    }


    @Override
    protected void onPause() {
        super.onPause();
        Config.collectLifecycleData();
        if(!init.getParentManager().isActivityChanging())
        {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if (DialogInterface.BUTTON_POSITIVE == which) {
                        init.getParentManager().setActivityChanging(true);
                        //init.getParentManager().showMenuInicial();
                        ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                        init.getParentManager().resetRootViewController();
                        init.resetInstance();
                    }else{
                        dialog.dismiss();
                    }

                }
            };

            init.getBaseViewController().showYesNoAlert(this, R.string.promocion_ventatdc_rechazo, listener);

        }
        return true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }


    @Override
    protected void onResume() {
        super.onResume();
        Config.pauseCollectingLifecycleData();
        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        baseViewController.setActivity(this);
        init.setBaseViewController(baseViewController);

    }



    @Override
    public void onClick(View v) {

        if(v==btnConfirmacion){
            if(isSelectedClauses){
                if(delegate.enviaPeticionOperacion())
                {
                    init.getParentManager().setActivityChanging(true);
                    delegate.performAction(null);
                }

            }else
                init.getBaseViewController().showInformationAlert(ClausulasViewController.this,getResources().getString(R.string.clausulas_ventatdc_alerta_clausulas));

        }
        else if(v == linkClausula5){
            init.getParentManager().setActivityChanging(true);
            delegate.realizaOperacionTerminosYCondiciones();
        }

    }


    public void onCheckboxClick(View v){


        if(v==cbClausula1){
            if(cbClausula1.isChecked()){
                cbClausula1.setClickable(false);
            }
        }else if(v==cbClausula2){
            if(!cbClausula1.isClickable()){

                cbClausula2.setClickable(false);
                cbClausula2.setBackgroundResource(R.drawable.an_ic_check_on);
                linearRBEnvioTarjeta.setVisibility(View.VISIBLE);
                setOpcionDomicilio("sucursal");
                setSoloCompras("NO");
            }else{
                if(isBuro){
                    init.getBaseViewController().showInformationAlert(ClausulasViewController.this, getResources().getString(R.string.clausulas_ventatdc_alerta_buro));
                    cbClausula2.setBackgroundResource(R.drawable.an_ic_check_off);
                }
            }
        }
        else if(v == cbClausula4){
            if(!cbClausula1.isClickable() && !cbClausula2.isClickable()){
                linearRBEnvioTarjeta.setVisibility(View.GONE);
                cbClausula4.setBackgroundResource(R.drawable.an_ic_check_on);
                cbClausula4.setClickable(false);
                setDomiciliacion("minimo");
                linearClausula4_aviso.setVisibility(View.VISIBLE);
                layoutInfoClausula4.setVisibility(View.VISIBLE);
            }
            else{
                if(isAceptacionTarjeta){
                    init.getBaseViewController().showInformationAlert(ClausulasViewController.this, getResources().getString(R.string.clausulas_ventatdc_alerta_aceptacionTarjeta));
                    cbClausula4.setBackgroundResource(R.drawable.an_ic_check_off);
                }
            }
        }
        else if(v == cbClausula5){
            if(!cbClausula1.isClickable() && !cbClausula2.isClickable() && !cbClausula4.isClickable()){
                cbClausula5.setBackgroundResource(R.drawable.an_ic_check_on);
                cbClausula5.setClickable(false);
                linearClausula4_aviso.setVisibility(View.GONE);
                layoutInfoClausula4.setVisibility(View.GONE);
                isSelectedClauses = true;
            }
            else{
                if(isDomiciliacion){
                    init.getBaseViewController().showInformationAlert(ClausulasViewController.this, getResources().getString(R.string.clausulas_ventatdc_alerta_domiciliacion));
                    cbClausula5.setBackgroundResource(R.drawable.an_ic_check_off);
                }
            }
        }
    }




    public void findViews(){

        gris1 = getResources().getColor(R.color.gris_texto_1);
        verde = getResources().getColor(R.color.verde);
        azul  = getResources().getColor(R.color.primer_azul);

        //buro de credito
        linearChecKBox1 = (LinearLayout) findViewById(R.id.linearChecKBox1);
        clausula1 = (TextView) findViewById(R.id.clausula1);
        cbClausula1 = (CheckBox) findViewById(R.id.cbClausula1);

        //aceptacion de tarjeta
        linearChecKBox2 = (LinearLayout) findViewById(R.id.linearChecKBox2);

        clausula2 = (TextView) findViewById(R.id.clausula2);
        String txtAceptaTarjeta= getString(R.string.clausulas_ventatdc_txt3_aceptaTarjeta);
        String tipoTarjeta= delegate.getOferta().getProducto();
        String importe = Tools.formatAmount(delegate.getPromocion().getMonto(),false);
        clausula2.setText(Html.fromHtml("<font color='" + gris1 + "'>" + txtAceptaTarjeta + "</font>"
                + "<b><font color='" + verde + "'>" + " " + importe + " " + "</font></b>"), TextView.BufferType.SPANNABLE);

        cbClausula2 = (CheckBox) findViewById(R.id.cbClausula2);



        //recepcion de tarjeta
        linearRBEnvioTarjeta = (LinearLayout) findViewById(R.id.linearRBEnvioTarjeta);

        /*txtSucursal = (TextView) findViewById(R.id.txtSucursal);
        textoSucursal = getResources().getString(R.string.bmovil_clausulas_ventatdc_txt3_2);
        domSucursal = delegate.getOferta().getDomicilioSucursal();
        txtSucursal.setText(Html.fromHtml("<font color='"+azul+"'>"+textoSucursal+"</font><br/>"
                + "<font color='"+gris1+"'>"+domSucursal+"</font>"), TextView.BufferType.SPANNABLE);*/



        linearClausula4 = (LinearLayout) findViewById(R.id.linearClausula4);
        clausula4 = (TextView) findViewById(R.id.clausula4);
        String txtDomiciliacion = getResources().getString(R.string.clausulas_ventatdc_txt5);
        String tipoPago = getResources().getString(R.string.clausulas_ventatdc_txt5_1);
        clausula4.setText(Html.fromHtml("<font color='"+gris1+"'>"+txtDomiciliacion+"</font>"
                + "<b><font color='"+verde+"'>"+" "+tipoPago+" "+"</font></b>"), TextView.BufferType.SPANNABLE);
        cbClausula4 = (CheckBox) findViewById(R.id.cbClausula4);

        linearClausula4_aviso = (LinearLayout) findViewById(R.id.linearClausula4_aviso);
        clausula4_aviso = (TextView) findViewById(R.id.clausula4_aviso);
        String txtAvisoDom = getResources().getString(R.string.clausulas_ventatdc_txt5_Domiciliacion);


        String teminacionTarjeta  = init.getCuentaOneClick().substring(init.getCuentaOneClick().length()-5,init.getCuentaOneClick().length());

        clausula4_aviso.setText(txtAvisoDom+" *"+teminacionTarjeta);
        layoutInfoClausula4 = (LinearLayout) findViewById(R.id.layoutInfoClausula4);

        //layout checkbox terminos y condiciones
        linearClausula5 = (LinearLayout) findViewById(R.id.linearClausula5);

        linkClausula5 = (TextView) findViewById(R.id.linkClausula5);
        String txtClausula6 = getString(R.string.clausulas_ventatdc_terminosycondiciones);
        SpannableString textolinkClausula6 = new SpannableString(txtClausula6);
       //MAPE textolinkClausula6.setSpan(new UnderlineSpan(), 0, txtClausula6.length(), 0);
        linkClausula5.setText(textolinkClausula6);
        linkClausula5.setOnClickListener(this);
        cbClausula5 = (CheckBox) findViewById(R.id.cbClausula5);

        //credenciales autenticacion
        layout_avisotoken	= (LinearLayout)findViewById(R.id.layout_avisotoken);
        LinearLayout contenedorPadre = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);

        contenedorContrasena	= (LinearLayout)findViewById(R.id.campo_confirmacion_contrasena_layout);
        contenedorNIP 			= (LinearLayout)findViewById(R.id.campo_confirmacion_nip_layout);
        contenedorASM 			= (LinearLayout)findViewById(R.id.campo_confirmacion_asm_layout);
        contenedorCVV 			= (LinearLayout)findViewById(R.id.campo_confirmacion_cvv_layout);

        contrasena 				= (EditText)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
        nip 					= (EditText)contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
        asm						= (EditText)contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
        cvv						= (EditText)contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);

        campoContrasena			= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_label);
        campoNIP				= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_label);
        campoASM				= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_label);
        campoCVV				= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_label);

        instruccionesContrasena	= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
        instruccionesNIP		= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
        instruccionesASM		= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
        instruccionesCVV		= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);

        contenedorCampoTarjeta  = (LinearLayout)findViewById(R.id.campo_confirmacion_campotarjeta_layout);
        tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
        campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
        instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);

        setSecurityComponents();
        //END credenciales autenticacion

        //boton confirmar
        btnConfirmacion = (ImageButton)findViewById(R.id.confirmacion_confirmar_button);
        btnConfirmacion.setOnClickListener(this);
    }


    private void setSecurityComponents()
    {
        mostrarContrasena(delegate.mostrarContrasenia());
        mostrarNIP(delegate.mostrarNIP());
        mostrarASM(delegate.tokenAMostrar());
        mostrarCVV(delegate.mostrarCVV());

        LinearLayout parentContainer = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);

        if (contenedorContrasena.getVisibility() == View.GONE &&
                contenedorNIP.getVisibility() == View.GONE &&
                contenedorASM.getVisibility() == View.GONE &&
                contenedorCVV.getVisibility() == View.GONE &&
                contenedorCampoTarjeta.getVisibility() == View.GONE) {
            parentContainer.setBackgroundColor(0);
            parentContainer.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        }

        contrasena.addTextChangedListener(listener);
        nip.addTextChangedListener(listener);
        asm.addTextChangedListener(listener);
        cvv.addTextChangedListener(listener);
        tarjeta.addTextChangedListener(listener);

    }



    public void mostrarContrasena(boolean visibility){
        contenedorContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        campoContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        contrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
            layout_avisotoken.setVisibility(View.VISIBLE);
            campoContrasena.setText(delegate.getEtiquetaCampoContrasenia());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
            contrasena.setFilters(userFilterArray);
            contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
        instruccionesContrasena.setVisibility(View.GONE);
    }


    public void mostrarNIP(boolean visibility){
        contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
        campoNIP.setVisibility(visibility ? View.VISIBLE : View.GONE);
        nip.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
            layout_avisotoken.setVisibility(View.VISIBLE);
            campoNIP.setText(delegate.getEtiquetaCampoNip());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
            nip.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(tarjeta);
            nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = delegate.getTextoAyudaNIP();
            if (instrucciones.equals("")) {
                instruccionesNIP.setVisibility(View.GONE);
            } else {
                instruccionesNIP.setVisibility(View.VISIBLE);
                instruccionesNIP.setText(instrucciones);
            }
        } else {
            nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }


    public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){
        switch(tipoOTP) {
            case ninguno:
                contenedorASM.setVisibility(View.GONE);
                campoASM.setVisibility(View.GONE);
                asm.setVisibility(View.GONE);
                asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
                break;
            case codigo:
            case registro:
                layout_avisotoken.setVisibility(View.VISIBLE);
                contenedorASM.setVisibility(View.VISIBLE);
                campoASM.setVisibility(View.VISIBLE);
                asm.setVisibility(View.VISIBLE);
                InputFilter[] userFilterArray = new InputFilter[1];
                userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
                asm.setFilters(userFilterArray);
                cambiarAccionTexto(contrasena);
                cambiarAccionTexto(tarjeta);
                cambiarAccionTexto(nip);
                asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
                break;
        }

        Constants.TipoInstrumento tipoInstrumento = delegate.getTipoInstrumentoSeguridad();

        switch (tipoInstrumento) {
            case OCRA:
                campoASM.setText(delegate.getEtiquetaCampoOCRA());
                asm.setTransformationMethod(null);
                break;
            case DP270:
                campoASM.setText(delegate.getEtiquetaCampoDP270());
                asm.setTransformationMethod(null);
                break;
            case SoftToken:
                if(init.getSofTokenStatus()) {
                    asm.setText(Constants.DUMMY_OTP);
                    asm.setEnabled(false);
                    campoASM.setText(delegate.getEtiquetaCampoSoftokenActivado());
                } else {
                    asm.setText("");
                    asm.setEnabled(true);
                    campoASM.setText(delegate.getEtiquetaCampoSoftokenDesactivado());
                    asm.setTransformationMethod(null);
                }

                break;
            default:
                break;
        }
        if(delegate.getContext()==null && getBaseContext()!=null){
            delegate.setContext(getBaseContext());
        }
        String instrucciones = delegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento,init.getSofTokenStatus(),delegate.tokenAMostrar());
        if (instrucciones.equals("")) {
            instruccionesASM.setVisibility(View.GONE);
        } else {
            instruccionesASM.setVisibility(View.VISIBLE);
            instruccionesASM.setText(instrucciones);
        }
    }



    public void mostrarCVV(boolean visibility){
        contenedorCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
        campoCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
        cvv.setVisibility(visibility ? View.VISIBLE:View.GONE);
        instruccionesCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);

        if (visibility) {
            layout_avisotoken.setVisibility(View.VISIBLE);
            campoCVV.setText(delegate.getEtiquetaCampoCVV());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
            cvv.setFilters(userFilterArray);
            String instrucciones = delegate.getTextoAyudaCVV();
            instruccionesCVV.setText(instrucciones);
            instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(nip);
            cambiarAccionTexto(asm);
            cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    private void cambiarAccionTexto(EditText campo) {
        if (campo.getVisibility() == View.VISIBLE) {
            campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        }
    }



    public String pideContrasena() {
        if (contrasena.getVisibility() == View.GONE) {
            return "";
        } else {
            return contrasena.getText().toString();
        }
    }

    public String pideNIP() {
        if (nip.getVisibility() == View.GONE) {
            return "";
        } else {
            return nip.getText().toString();
        }
    }

    public String pideASM() {
        if (asm.getVisibility() == View.GONE) {
            return "";
        } else {
            return asm.getText().toString();
        }
    }

    public String pideCVV() {
        if (cvv.getVisibility() == View.GONE) {
            return "";
        } else {
            return cvv.getText().toString();
        }
    }




    private TextWatcher listener = new TextWatcher() {

        public void afterTextChanged(Editable s) {
            ClausulasViewController.this.onUserInteraction();

        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        public void onTextChanged(CharSequence s, int start, int before, int count) {}
    };

    private void scaleForCurrentScreen() {
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(findViewById(R.id.lblTextoInfoClausulas), true);
        gTools.scale(findViewById(R.id.clausula1), true);
        gTools.scale(findViewById(R.id.cbClausula1));
        gTools.scale(findViewById(R.id.clausula2), true);
        gTools.scale(findViewById(R.id.cbClausula2));
        gTools.scale(findViewById(R.id.txtEnvioTarjeta),true);
        gTools.scale(findViewById(R.id.clausula4), true);
        gTools.scale(findViewById(R.id.cbClausula4));
        gTools.scale(findViewById(R.id.clausula4_aviso), true);
        gTools.scale(findViewById(R.id.imgInfoClausula4));
        gTools.scale(findViewById(R.id.txtInfoClausula4), true);
        gTools.scale(findViewById(R.id.linkClausula5), true);
        gTools.scale(findViewById(R.id.txtClausula5), true);
        gTools.scale(findViewById(R.id.cbClausula5));
        gTools.scale(findViewById(R.id.imgaviso_token));
        gTools.scale(findViewById(R.id.txtaviso_token), true);
        gTools.scale(findViewById(R.id.confirmacion_contrasena_edittext), true);
        gTools.scale(findViewById(R.id.confirmacion_nip_edittext), true);
        gTools.scale(findViewById(R.id.confirmacion_asm_edittext), true);
        gTools.scale(findViewById(R.id.confirmacion_cvv_edittext), true);
        gTools.scale(findViewById(R.id.confirmacion_campotarjeta_edittext), true);
        gTools.scale(findViewById(R.id.confirmacion_contrasena_label), true);
        gTools.scale(findViewById(R.id.confirmacion_nip_label), true);
        gTools.scale(findViewById(R.id.confirmacion_asm_label), true);
        gTools.scale(findViewById(R.id.confirmacion_cvv_label), true);
        gTools.scale(findViewById(R.id.confirmacion_campotarjeta_label), true);
        gTools.scale(findViewById(R.id.confirmacion_contrasena_instrucciones_label), true);
        gTools.scale(findViewById(R.id.confirmacion_nip_instrucciones_label), true);
        gTools.scale(findViewById(R.id.confirmacion_asm_instrucciones_label), true);
        gTools.scale(findViewById(R.id.confirmacion_cvv_instrucciones_label), true);
        gTools.scale(findViewById(R.id.confirmacion_campotarjeta_instrucciones_label), true);
        gTools.scale(btnConfirmacion);
    }


    public void setSoloCompras(String soloCompras) {
        this.soloCompras = soloCompras;
    }

    public String getDomiciliacion() {
        return domiciliacion;
    }

    public void setDomiciliacion(String domiciliacion) {
        this.domiciliacion = domiciliacion;
    }

    public void setOpcionDomicilio(String opcionDomicilio) {
        this.opcionDomicilio = opcionDomicilio;
    }



}
