package bancomer.api.apiventatdc.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;


import bancomer.api.apiventatdc.io.Server;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by OOROZCO on 9/1/15.
 */
public class AceptaOfertaTDC implements ParsingHandler{


    String lineaCredito;
    String numContrato;
    String anualidad;
    String producto;
    String envioCorreoElectronico;
    String envioSMS;

    //New Region Created May 28th, 2015.
    //This value is used to determine the strings values to be shown on the "Éxito" view.
    private boolean isDomicilio;

    public boolean isDomicilio() {
        return isDomicilio;
    }

    public void setDomicilio(boolean isDomicilio) {
        this.isDomicilio = isDomicilio;
    }
    //End Region


    Promociones promocion;
    Promociones[] promociones;
    String PM;


    public String getLineaCredito() {
        return lineaCredito;
    }

    public void setLineaCredito(String lineaCredito) {
        this.lineaCredito = lineaCredito;
    }

    public String getNumContrato() {
        return numContrato;
    }

    public void setNumContrato(String numCuenta) {
        this.numContrato = numCuenta;
    }

    public String getAnualidad() {
        return anualidad;
    }

    public void setAnualidad(String anualidad) {
        this.anualidad = anualidad;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getEnvioCorreoElectronico() {
        return envioCorreoElectronico;
    }

    public void setEnvioCorreoElectronico(String envioCorreoElectronico) {
        this.envioCorreoElectronico = envioCorreoElectronico;
    }

    public String getEnvioSMS() {
        return envioSMS;
    }

    public void setEnvioSMS(String envioSMS) {
        this.envioSMS = envioSMS;
    }

    public Promociones getPromocion() {
        return promocion;
    }

    public void setPromocion(Promociones promocion) {
        this.promocion = promocion;
    }

    public Promociones[] getPromociones() {
        return promociones;
    }

    public void setPromociones(Promociones[] promociones) {
        this.promociones = promociones;
    }

    public String getPM() {
        return PM;
    }

    public void setPM(String pM) {
        PM = pM;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub
        Log.e("modelo: process(ParserJSON parser)", "OK");


        lineaCredito = parser.hasValue("lineaCredito")?parser.parseNextValue("lineaCredito"):"";
        numContrato = parser.hasValue("numContrato")?parser.parseNextValue("numContrato"):"";
        anualidad = parser.hasValue("anualidad")?parser.parseNextValue("anualidad"):"";
        producto = parser.hasValue("producto")?parser.parseNextValue("producto"):"";
        envioCorreoElectronico = parser.hasValue("envioCorreoElectronico")?parser.parseNextValue("envioCorreoElectronico"):"";
        envioSMS = parser.hasValue("envioSMS")?parser.parseNextValue("envioSMS"):"";



        PM=parser.hasValue("PM")?parser.parseNextValue("PM"):"";

        try {

            if (PM.equals("SI")) {
                JSONObject jsonObjectPromociones = new JSONObject(parser.parseNextValue("LP"));
                JSONArray arrPromocionesJson = jsonObjectPromociones.getJSONArray("campanias");
                ArrayList<Promociones> arrPromociones = new ArrayList<Promociones>();
                for (int i = 0; i < arrPromocionesJson.length(); i++) {
                    JSONObject jsonPromocion = (JSONObject) arrPromocionesJson.get(i);
                    Promociones promo = new Promociones();
                    promo.setCveCamp(jsonPromocion.getString("cveCamp"));
                    promo.setDesOferta(jsonPromocion.getString("desOferta"));
                    promo.setMonto(jsonPromocion.getString("monto"));

                    arrPromociones.add(promo);
                }

                this.promociones = arrPromociones
                        .toArray(new Promociones[arrPromociones.size()]);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            if(Server.ALLOW_LOG) e.printStackTrace();
        }
    }

}
