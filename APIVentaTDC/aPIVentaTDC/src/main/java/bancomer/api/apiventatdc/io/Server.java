/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package bancomer.api.apiventatdc.io;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;


import bancomer.api.apiventatdc.commons.ConstantsVentaTDC;
import bancomer.api.apiventatdc.models.AceptaOfertaTDC;
import bancomer.api.apiventatdc.models.ConsultaTerminosCondicionesTDC;
import bancomer.api.apiventatdc.models.FormatosFinalesVentaTDC;
import bancomer.api.apiventatdc.models.OfertaTDC;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.NameValuePair;
import bancomer.api.common.io.ParsingException;


/**
 * Server is responsible of acting as an interface between the application
 * business logic and the HTTP operations. It receives the network operation
 * requests and builds a network operation suitable for ClienteHttp class, which
 * eventually will perform the network connection. Server returns the server
 * response as a ServerResponseImpl object, so that the application logic can
 * perform high level checks and processes.
 * 
 * @author GoNet.
 */

public class Server {

	/**
	 * Defines if the application show logs
	 */
	public static boolean ALLOW_LOG = true;

	/**
	 * Defines if the application is running on the emulator or in the device.
	 */
	public static boolean EMULATOR = false;

	/**
	 * Indicates simulation usage.
	 */
	public static boolean SIMULATION = false;

	/**
	 * Indicates if the base URL points to development server or production
	 */
	public static boolean DEVELOPMENT = false;
	// public static boolean DEVELOPMENT = true; //TODO remove for TEST

	/**
	 * Indicates simulation usage.
	 */
	public static final long SIMULATION_RESPONSE_TIME = 5;


    public static final String JSON_OPERATION_VENTA_TDC = "VTDC001";

	/**
	 * Bancomer Infrastructure.
	 */
	public static final int BANCOMER = 0;

	public static int PROVIDER = BANCOMER; // TODO remove for TEST


	// ///////////////////////////////////////////////////////////////////////////
	// Operations identifiers //
	// ///////////////////////////////////////////////////////////////////////////


	/**ONE CLICK
	 * consultas venta TDC*/
	public static final int CONSULTA_DETALLE_OFERTA_TDC=131;
	public static final int CONSULTA_TERMINOS_CONDICIONES_TDC=132;
	public static final int ACEPTACION_OFERTA_TDC=133;
	public static final int FORMATOS_FINALES_TDC=134;


	/**
	 * Operation codes.
	 */
	public static final String[] OPERATION_CODES = { "00","detalleTDC","contratoTDCBMovil",
			"aceptacionTDCBmovil","formatosFinalesOneClickTDC"};


	/**
	 * Base URL for providers.
	 */
	public static String[] BASE_URL;

	/**
	 * Path table for provider/operation URLs.
	 */
	public static String[][] OPERATIONS_PATH;

	/**
	 * Operation URLs map.
	 */
	public static Hashtable<String, String> OPERATIONS_MAP = new Hashtable<String, String>();

	/**
	 * Operation providers map.
	 */
	public static Hashtable<String, Integer> OPERATIONS_PROVIDER = new Hashtable<String, Integer>();

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_PARAMETER = "OPERACION";


	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_VALUE = "es_ES";



	public static void setEnviroment() {
		if (DEVELOPMENT) {
			setupDevelopmentServer();
		} else {
			setupProductionServer();
		}

		setupOperations(PROVIDER);
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupProductionServer() {
		BASE_URL = new String[] { "https://www.bancomermovil.com",
				// "https://172.17.100.173",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Produccion"
						: "" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "", "/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP1: Consulta Detalle ventaTDC *ONE CLICK*
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP2: Consulta Terminos y Condiciones ventaTDC *ONE CLICK*
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP3: Aceptacion oferta ventaTDC *ONE CLICK*
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP4: Formatos finales ventaTDC *ONE CLICK*

				},
				new String[] { "", "/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP1: Consulta Detalle ventaTDC *ONE CLICK*
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP2: Consulta Terminos y Condiciones ventaTDC *ONE CLICK*
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP3: Aceptacion oferta ventaTDC *ONE CLICK*
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP4: Formatos finales ventaTDC *ONE CLICK*


				} };
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupDevelopmentServer() {
		BASE_URL = new String[] {
				"https://www.bancomermovil.net:11443",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Desarrollo"
						: "http://189.254.77.54:8080/servermobile/Servidor/Desarrollo" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "", "/eexd_mx_web/servlet/ServletOperacionWeb",  // OP1: Consulta Detalle ventaTDC *ONE CLICK*
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP2: Consulta Terminos y Condiciones ventaTDC *ONE CLICK*
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP3: Aceptacion oferta ventaTDC *ONE CLICK*
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP4: Formatos finales ventaTDC *ONE CLICK*

				},
				new String[] { "", "/eexd_mx_web/servlet/ServletOperacionWeb",  // OP1: Consulta Detalle ventaTDC *ONE CLICK*
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP2: Consulta Terminos y Condiciones ventaTDC *ONE CLICK*
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP3: Aceptacion oferta ventaTDC *ONE CLICK*
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP4: Formatos finales ventaTDC *ONE CLICK*


				} };
	}

	/**
	 * Setup operations for a provider.
	 *
	 * @param provider
	 *            the provider
	 */
	private static void setupOperations(int provider) {

		setupOperation(CONSULTA_DETALLE_OFERTA_TDC-130, provider); // ONE CLICK venta TDC
		setupOperation(CONSULTA_TERMINOS_CONDICIONES_TDC-130, provider); // ONE CLICK venta TDC
		setupOperation(ACEPTACION_OFERTA_TDC-130, provider); // ONE CLICK venta TDC
		setupOperation(FORMATOS_FINALES_TDC-130, provider); // ONE CLICK venta TDC

		//setupOperation(OP_CONSULTA_CORREO_OTROS_CREDITOS,provider);

	}

	/**
	 * Setup operation for a provider.
	 * @param operation the operation
	 * @param provider the provider
	 */
	private static void setupOperation(int operation, int provider) {
		String code = OPERATION_CODES[operation];
		OPERATIONS_MAP.put(
				code,
				new StringBuffer(BASE_URL[provider]).append(
						OPERATIONS_PATH[provider][operation]).toString());
		OPERATIONS_PROVIDER.put(code, new Integer(provider));
	}

	/**
	 * Get the operation URL.
	 * @param operation the operation id
	 * @return the operation URL
	 */
	public static String getOperationUrl(String operation) {
		String url = (String) OPERATIONS_MAP.get(operation);
		return url;
	}

	/**
	 * Referencia a ClienteHttp, en lugar de HttpInvoker.
	 */
	private ClienteHttp clienteHttp = null;


	// protected Context mContext;

	public ClienteHttp getClienteHttp() {
		return clienteHttp;
	}

	public void setClienteHttp(ClienteHttp clienteHttp) {
		this.clienteHttp = clienteHttp;
	}



	public Server() {
		clienteHttp = new ClienteHttp();
	}

	/**
	 * perform the network operation identifier by operationId with the
	 * parameters passed.
	 * @param operationId the identifier of the operation
	 * @param params Hashtable with the parameters as key-value pairs.
	 * @return the response from the server
	 * @throws IOException exception
	 * @throws ParsingException exception
	 */
	public ServerResponseImpl doNetworkOperation(int operationId, Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

		ServerResponseImpl response = null;
		long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
		Integer provider = (Integer) OPERATIONS_PROVIDER.get(OPERATION_CODES[operationId-130]);
		if (provider != null) {
			PROVIDER = provider.intValue();
		}

		switch (operationId) {

			case CONSULTA_DETALLE_OFERTA_TDC: // ONE CLICK venta TDC
				response = consultaDetalleTDC(params);
				break;
			case CONSULTA_TERMINOS_CONDICIONES_TDC: // ONE CLICK venta TDC
				response = consultaTerminosCondicionesTDC(params);
				break;

			case ACEPTACION_OFERTA_TDC: // ONE CLICK venta TDC
				response = aceptacionOfertaTDC(params);
				break;

			case FORMATOS_FINALES_TDC: // ONE CLICK venta TDC
				response = consultaFormatosFinalesTDC(params);
				break;
		}
		//Termina SPEI
		if (sleep > 0) {
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
			}
		}
		return response;
	}


	/**
	 * Cancel the ongoing network operation.
	 */
	public void cancelNetworkOperations() {
		clienteHttp.abort();
	}






	private ServerResponseImpl consultaDetalleTDC(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

		OfertaTDC ofertaTDC=new OfertaTDC();
		ServerResponseImpl response = new ServerResponseImpl(ofertaTDC);

		String numeroCelular = (String) params.get("numeroCelular");
		String claveCamp = (String) params.get("cveCamp");
		String IUM = (String) params.get("IUM");
		String oficina = (String) params.get("oficina");

		NameValuePair[] parameters = new NameValuePair[4];
		parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
		parameters[1] = new NameValuePair("cveCamp", claveCamp);
		parameters[2] = new NameValuePair("IUM",IUM);
		parameters[3] = new NameValuePair("oficina", oficina);

		if (SIMULATION) {

			//New one
			this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_DETALLE_OFERTA_TDC-130], parameters, response,"{\"estado\":\"OK\",\"lineaCredito\":\"1900033\",\"tddAsociada\":\"\",\"cat\":\"33.5\",\"fechaCat\":\"2014-05-21\",\"anualidad\":\"320.40\",\"producto\":\"AV\",\"tazaPonderada\":\"45\", \"domicilio\":\"Av. De las Flores, col Coyoacán, Mex. D.F, CP 44444\" ,\"domicilioSucursal\":\"Av. Universidad No.1220, col Xoco,Coyoacan, Mex. D.F, CP 44444\", \"soloCompras\":\"SI\",\"desProducto\":\"Oro\"}", true);

			//traza ERROR
			//this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_DETALLE_OFERTA_TDC], parameters, response, "{\"estado\":\"ERROR\",\"codigoMensaje\":\"KZE1628\",\"descripcionMensaje\":\"SERVICIO FUERA DE SERVICIO.\"}", true);



		}else{

			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DETALLE_OFERTA_TDC-130], parameters, response, false, true);
		}
		return response;
	}




	private ServerResponseImpl consultaTerminosCondicionesTDC(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

		ConsultaTerminosCondicionesTDC polizaResponse = new ConsultaTerminosCondicionesTDC();
        ServerResponseImpl response = new ServerResponseImpl(polizaResponse);

		String idProducto = (String) params.get("idProducto");
		String numeroCelular = (String) params.get("numeroCelular");
		String IUM = (String) params.get("IUM");
		String indVacio = (String) params.get(ConstantsVentaTDC.IND_VACIO);
		String indContrato = (String) params.get(ConstantsVentaTDC.IND_CONTRATO);
		String numContrato = (String) params.get(ConstantsVentaTDC.NUM_CONTRATO);

		NameValuePair[] parameters = new NameValuePair[6];
		parameters[0] = new NameValuePair("idProducto", idProducto);
		parameters[1] = new NameValuePair("numeroCelular", numeroCelular);
		parameters[2] = new NameValuePair("IUM",IUM);
		parameters[3] = new NameValuePair(ConstantsVentaTDC.IND_VACIO, indVacio);
		parameters[4] = new NameValuePair(ConstantsVentaTDC.IND_CONTRATO, indContrato);
		parameters[5] = new NameValuePair(ConstantsVentaTDC.NUM_CONTRATO, numContrato);


		if (SIMULATION) {

			//traza OK
            this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_TERMINOS_CONDICIONES_TDC-130], parameters, response, "{\"estado\":\"OK\",\"formatoHTML\":\"<html>  <head>  <title>  Formato para solicitar la Domiciliaci&oacute;n  </title>  </head>  <body>  <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#000000 &#33;important; font-family: Arial Narrow, Arial, Helvetica, sans-serif; font-size:12pt; width:900px &#33;important; text-align:justify;\">  <tr>  <td width=\"49%\" valign=\"top\">  <center>                                                               <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"color:#000000 &#33;important; font-family: Arial Narrow, Arial, Helvetica, sans-serif; font-size:12pt; width:700px &#33;important; text-align:justify;\"> <tr>  <td width=\"49%\" valign=\"top\">  <div style=\"height:10px; clear:both;\"></div>  <div style=\"text-align:center;\">  Formato para solicitar la Domiciliaci&oacute;n  </div>  <div style=\"height:20px; clear:both;\"></div>  <div style=\"text-align:right;\">                                        <u>13</u>&nbsp;de&nbsp;<u>JULIO</u>&nbsp;de&nbsp;<u>2015</u> </div>  <div style=\"height:20px; clear:both;\"></div>  <div style=\"text-align:justify;\">  Solicito y autorizo que con base en la informaci&oacute;n que se indica en esta comunicaci&oacute;n se realicen cargos peri&oacute;dicos en mi cuenta conforme a lo siguiente:  </div>  <div style=\"height:20px; clear:both;\"></div>  <div style=\"text-align:justify;\">1. Nombre del proveedor del bien, servicio o cr&eacute;dito, seg&uacute;n corresponda, que pretende pagarse:<br>BBVA BANCOMER, S.A., INSTITUCI&Oacute;N DE BANCA M&Uacute;LTIPLE, GRUPO FINANCIERO BBVA BANCOMER. </div>  <div style=\"height:20px; clear:both;\"></div>  <div style=\"text-align:justify;\">                                           2. Bien, servicio o cr&eacute;dito, a pagar: <u>TARJETA DE CR&Eacute;DITO BBVA BANCOMER</u>. En su caso, el n&uacute;mero deidentificaci&oacute;n generado por el proveedor (dato no obligatorio): _______________ . </div>  <div style=\"height:20px; clear:both;\"></div>  <div style=\"text-align:justify;\">                                        3. Periodicidad del pago (Facturaci&oacute;n) (Ejemplo: semanal, quincenal, mensual, bimestral, semestral, anual, etc.): <u>&nbsp;&nbsp;&nbsp;MENSUAL&nbsp;&nbsp;&nbsp;</u> o, en su caso, el d&iacute;a espec&iacute;fico en el que se solicita realizar el pago:_____________________ . </div>  <div style=\"height:20px; clear:both;\"></div>  <div style=\"text-align:justify;\">                                                   4. Nombre del banco que lleva la cuenta de dep&oacute;sito a la vista o de ahorro en la que se realizar&aacute; el cargo:<br> <u>BBVA BANCOMER, S.A., INSTITUCI&Oacute;N DE BANCA M&Uacute;LTIPLE, GRUPO FINANCIERO BBVA BANCOMER</u>. </div>  <div style=\"height:20px; clear:both;\"></div>  <div style=\"text-align:justify;\">                      5. Cualquiera de los Datos de identificaci&oacute;n de la cuenta, siguientes: N&uacute;mero de tarjeta de d&eacute;bito (16 d&iacute;gitos): <u>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;</u> Clave Bancaria Estandarizada (\"CLABE\") de la Cuenta (18 d&iacute;gitos): <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>, o N&uacute;mero del tel&eacute;fono m&oacute;vil asociado a la cuenta: <u>&nbsp;&nbsp;&nbsp;5510111224&nbsp;&nbsp;&nbsp;</u> </div>  <div style=\"height:20px; clear:both;\"></div>    <div style=\"text-align:justify;\"> 6. Monto m&aacute;ximo fijo del cargo autorizado por periodo de facturaci&oacute;n: $________________.<br>  En lugar del monto m&aacute;ximo fijo, trat&aacute;ndose del pago de cr&eacute;ditos revolventes asociados a tarjetas de cr&eacute;dito, el titular de la cuenta podr&aacute; optar por autorizar alguna de las opciones de cargo siguientes:<br>                                     (Marcar con una X la opci&oacute;n que, en su caso, corresponda)<br>El importe del pago m&iacute;nimo del periodo: (X),<br>El saldo total para no generar intereses en el periodo: ( ), o<br>Un monto fijo: ( ) (Incluir monto) $ <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u> . </div>  <div style=\"height:20px; clear:both;\"></div>  <div style=\"text-align:justify;\">                            7. Esta autorizaci&oacute;n es por plazo indeterminado ( ), o vence el: ____________________ .<br>Estoy enterado de que en cualquier momento podr&eacute; solicitar la cancelaci&oacute;n de la presente domiciliaci&oacute;n sin costo a mi cargo. </div>  <div style=\"height:40px; clear:both;\"></div>                                            <div style=\"text-align:center;\">A t e n t a m e n t e ,<br><br><br></div><div style=\"text-align:center;\">CARLOS HERNANDEZ LOPEZ</div><div style=\"text-align:center;\"><span style=\"border-top:#000000 solid 1px;\"><i><b>(NOMBRE O RAZON SOCIAL DEL TITULAR DE LA CUENTA)</b></i></span></div><div style=\"height:12px; clear:both;\"></div> <div style=\"height:60px; clear:both;\"></div>  <div style=\"text-align:justify;\">\"}", true);

			//traza ERROR
			//this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_TERMINOS_CONDICIONES_TDC], parameters, response, "{\"estado\":\"ERROR\",\"codigoMensaje\":\"KZE1628\",\"descripcionMensaje\":\"SERVICIO FUERA DE SERVICIO.\"}", true);



		}else{

			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_TERMINOS_CONDICIONES_TDC-130], parameters, response, false, true);
		}
		return response;
	}

	private ServerResponseImpl aceptacionOfertaTDC(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		AceptaOfertaTDC aceptaOfertaTDC = new AceptaOfertaTDC();
		ServerResponseImpl response = new ServerResponseImpl(aceptaOfertaTDC);


		String lineaCredito = (String) params.get("lineaCredito");
		String numeroCelular = (String) params.get("numeroCelular");
		String idProducto = (String) params.get("idProducto");
		String soloCompras = (String) params.get("soloCompras");
		String domiciliacion = (String) params.get("domiciliacion");
		String cveCamp = (String) params.get("cveCamp");
		String codigoOTP = (String) params.get("codigoOTP");
		String cadenaAutenticacion = (String) params.get("cadenaAutenticacion");
		String codigoNip = (String) params.get("codigoNip");
		String codigoCVV2 = (String) params.get("codigoCVV2");
		String Tarjeta5ig = (String) params.get("Tarjeta5ig");
		String cveAcceso = (String) params.get("cveAcceso");
		String opcionDomicilio = (String) params.get("opcionDomicilio");
		String IUM = (String) params.get("IUM");


		NameValuePair[] parameters = new NameValuePair[14];
		parameters[0] = new NameValuePair("lineaCredito", lineaCredito);
		parameters[1] = new NameValuePair("numeroCelular", numeroCelular);
		parameters[2] = new NameValuePair("idProducto", idProducto);
		parameters[3] = new NameValuePair("soloCompras", soloCompras);
		parameters[4] = new NameValuePair("domiciliacion", domiciliacion);
		parameters[5] = new NameValuePair("cveCamp", cveCamp);
		parameters[6] = new NameValuePair("codigoOTP", codigoOTP);
		parameters[7] = new NameValuePair("cadenaAutenticacion", cadenaAutenticacion);
		parameters[8] = new NameValuePair("codigoNip", codigoNip);
		parameters[9] = new NameValuePair("codigoCVV2", codigoCVV2);
		parameters[10] = new NameValuePair("Tarjeta5ig", Tarjeta5ig);
		parameters[11] = new NameValuePair("cveAcceso", cveAcceso);
		parameters[12] = new NameValuePair("opcionDomicilio", opcionDomicilio);
		parameters[13] = new NameValuePair("IUM", IUM);


		if (SIMULATION) {
			//traza OK
			this.clienteHttp.simulateOperation(OPERATION_CODES[ACEPTACION_OFERTA_TDC-130], parameters, response,"{\"estado\":\"OK\",\"lineaCredito\":\"1900033\",\"numContrato\":\"00740010000178500258\",\"anualidad\":\"32.40\",\"producto\":\"ORO\",\"envioCorreoElectronico\":\"SI\",\"envioSMS\":\"SI\",\"PM\":\"SI\",\"LP\":{\"campanias\":[{\"cveCamp\":\"0130ABCDEGAS123\",\"desOferta\":\"Incremento de línea de credito\",\"monto\":\"50000\"},{\"cveCamp\":\"0296CAMP343435356\",\"desOferta\":\"VentaTDC\",\"monto\":\"30000\"},{\"cveCamp\":\"0377POIOIU23434\",\"desOferta\":\"Efectivoinmediato\",\"monto\":\"3000\"},{\"cveCamp\":\"0060CAM9123459864\",\"desOferta\":\"PIDE\",\"monto\":\"900000\"}]}}", true);
			//traza ERROR
			//this.clienteHttp.simulateOperation(OPERATION_CODES[ACEPTACION_OFERTA_TDC], parameters, response,"{\"estado\":\"ERROR\",\"codigoMensaje\":\"MBANK0001806\",\"descripcionMensaje\":\"Su TDC no puede ser autorizada, para mas detalles favor de acudir a sucursal\",\"PM\":\"SI\",\"LP\":{\"campanias\":[{\"cveCamp\":\"0130ABCDEGAS123\",\"desOferta\":\"Incremento de línea de credito\",\"monto\":\"50000\"}]}}", true);
			//this.clienteHttp.simulateOperation(OPERATION_CODES[ACEPTACION_OFERTA_TDC], parameters, response,"{\"estado\":\"ERROR\",\"codigoMensaje\":\"\",\"descripcionMensaje\":\"Error de comunicaciones\"}", true);



		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[ACEPTACION_OFERTA_TDC-130], parameters, response, false, true);
		}
		return response;
	}


    private ServerResponseImpl consultaFormatosFinalesTDC(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

        FormatosFinalesVentaTDC formatoResponse = new FormatosFinalesVentaTDC();
        ServerResponseImpl response = new ServerResponseImpl(formatoResponse);


        String idProducto = (String) params.get("idProducto");
        String numeroCelular = (String) params.get("numeroCelular");
        String indicadorFormato = (String) params.get("indicadorFormato");
        String IUM = (String) params.get("IUM");

        NameValuePair[] parameters = new NameValuePair[4];
        parameters[0] = new NameValuePair("idProducto", idProducto);
        parameters[1] = new NameValuePair("numeroCelular", numeroCelular);
        parameters[2] = new NameValuePair("indicadorFormato",indicadorFormato);
        parameters[3] = new NameValuePair("IUM",IUM);

        if (SIMULATION) {
            //traza OK
            this.clienteHttp.simulateOperation(OPERATION_CODES[FORMATOS_FINALES_TDC-130], parameters, response,"{\"estado\":\"OK\",\"txtHTML1\":\"<HTML><HEAD><BODY>Página</BODY></HEAD></HTML>\"}", true);

            //traza ERROR
            //this.httpClient.simulateOperation("{\"estado\":\"ERROR\",\"codigoMensaje\":\"KNZS1628\",\"descripcionMensaje\":\"SERVICIO FUERA DE SERVICIO.\"}", response, true);

        }else{
            clienteHttp.invokeOperation(OPERATION_CODES[FORMATOS_FINALES_TDC-130], parameters, response, false, true);
        }
        return response;

    }





}


/********************************                ********************************/
/******************************** GENERIC THINGS ********************************/
/********************************                ********************************/

			/*

			public ServerResponseImpl doNetworkOperationGeneric(String operationCode, HashMap<String, String> params,  ArrayList<String> urlsProd, ArrayList<String> urlsDev, Boolean isJSON, Object responseModel, String simulatedResponse) throws IOException, ParsingException, URISyntaxException {

				ServerResponseImpl response = null;

				if(!containOperationCode(operationCode)){

					Integer lengthZero = OPERATIONS_PATH[0].length;
					Integer lengthOne = OPERATIONS_PATH[1].length;
					String url0 = urlsProd.get(0);
					String url1 = urlsProd.get(1);
					if (DEVELOPMENT) {
						url0 = urlsDev.get(0);
						url1 = urlsDev.get(1);
					}

					OPERATIONS_PATH[0][lengthZero] = url0;
					OPERATIONS_PATH[1][lengthOne] = url1;

					OPERATION_CODES[OPERATION_CODES.length] = operationCode;

					setupOperation(OPERATION_CODES.length, PROVIDER);

				}

				long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
				Integer provider = (Integer) OPERATIONS_PROVIDER.get(operationCode);
				if (provider != null) {
					PROVIDER = provider.intValue();
				}

				response = genericOperation(params, operationCode, isJSON, responseModel, simulatedResponse);

				if (sleep > 0) {
					try {
						Thread.sleep(sleep);
					} catch (InterruptedException e) {
					}
				}

				return response;
			}

			private Boolean containOperationCode(String operation){
				Boolean ret = false;
				for(String op : OPERATION_CODES){
					if(op.equalsIgnoreCase(operation)) ret = true;
				}

				return ret;
			}

			private ServerResponseImpl genericOperation(HashMap<String, String> params, String operationCode, Boolean isJSON, Object responseModel, String simulatedResponse)
					throws IOException, ParsingException, URISyntaxException {

				//FIXME null ?
				ServerResponseImpl response = new ServerResponseImpl(null);

				if (SIMULATION) {

					this.httpClient.simulateOperation(simulatedResponse,(ParsingHandler) response, true);

				}else{

					Iterator<String> it = params.keySet().iterator();
					NameValuePair[] parameters = new NameValuePair[params.keySet().size()];

					Integer cont = 0;
					while(it.hasNext()){
						String key = it.next();
						parameters[cont] = new NameValuePair(key, params.get(key));

						++cont;
					}

					clienteHttp.invokeOperation(operationCode, parameters, response, false, isJSON);
				}

				return response;
			}


			*/

/********************************                ********************************/
/********************************                ********************************/
/********************************                ********************************/



//one Click
/**
 * clase de enum para cambio de url;
 */
			/*public enum isJsonValueCode{
				ONECLICK, NONE,CONSUMO,DEPOSITOS;
			}

			public static isJsonValueCode isjsonvalueCode= isJsonValueCode.NONE;
			//Termina One CLick*/