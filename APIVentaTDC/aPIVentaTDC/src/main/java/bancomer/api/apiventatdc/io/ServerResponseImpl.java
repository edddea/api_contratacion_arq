/*
 * Copyright (c) 2010 BBVA. All Rights Reserved

 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package bancomer.api.apiventatdc.io;

import java.io.IOException;

import bancomer.api.apiventatdc.models.AceptaOfertaTDC;
import bancomer.api.apiventatdc.models.OfertaTDC;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.common.io.Result;
import bancomer.api.common.io.ServerResponse;

/**
 * ServerResponse wraps the response from the server, containing attributes that.
 * define if the response has been successful or not, and the data content if the
 * response was successful
 *
 * @author Stefanini IT Solutions.
 */
public class ServerResponseImpl implements ServerResponse,ParsingHandler{

    /////////////////////////////////////////////////////////////////////////////
    //                Status code definitions                                  //
    /////////////////////////////////////////////////////////////////////////////

    /**
	 * Default UID
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Operation successful.
     */
    public static final int OPERATION_SUCCESSFUL = 0;

    /**
     * Operation with warning.
     */
    public static final int OPERATION_WARNING = 1;

    /**
     * Operation failed.
     */
    public static final int OPERATION_ERROR = 2;

    /**
     * Operation succesful but asks for optinal updating.
     */
    public static final int OPERATION_OPTIONAL_UPDATE = 3;

    /**
     * Session expired.
     */
    public static final int OPERATION_SESSION_EXPIRED = -100;

    /**
     * Unknown operation result.
     */
    public static final int OPERATION_STATUS_UNKNOWN = -1000;

    /**
     * Status of the current response.
     */
    private int status = 0;

    /**
     * Response error code.
     */
    private String messageCode = null;

    /**
     * Error message.
     */
    private String messageText = null;

    /**
     * URL for mandatory updating(the mandatory updating order is received as.
     * error with code MBANK1111)
     */
    private String updateURL = null;

    /**
     * Response handler, containing the data content of the response.
     */
    private Object responseHandler = null;

    /**
     * Default constructor.
     */
    public ServerResponseImpl() {
    }

    /**
     * Constructor.
     * @param handler the handler of the content
     */
    public ServerResponseImpl(Object handler) {
        this.responseHandler = handler;
    }

    /**
     * Constructor with parameters.
     * @param stat the response status
     * @param msgCode the error code
     * @param msgText the error text
     * @param handler the handler of the content
     */
    public ServerResponseImpl(int stat, String msgCode,
    					  String msgText, Object handler) {
        this.status = stat;
        this.messageCode = msgCode;
        this.messageText = msgText;
        this.responseHandler = handler;
    }

    /**
     * Get the status code.
     * @return the status code
     */
    public int getStatus() {
        return status;
    }

    /**
     * Get the error code.
     * @return the error code
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Get the error text.
     * @return  the error text
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * Get the response.
     * @return the content response
     */
    public Object getResponse() {
        return responseHandler;
    }

    /**
     * Gets the value of the URL for mandatory application update.
     * @return SOMETHING
     */
    public String getUpdateURL() {
        return updateURL;
    }

    /**
     * Parse the response to get the attributes.
     * @param parser instance capable of parsing the response
     * @throws IOException exception
     * @throws ParsingExceptionImpl exception
     */
    public void process(Parser parser) throws IOException, ParsingException {

        if (parser != null) {

            status = OPERATION_STATUS_UNKNOWN;
            Result result = parser.parseResult();
            String statusText = result.getStatus();
            if (Parser.STATUS_OK.equals(statusText)) {
                status = OPERATION_SUCCESSFUL;
            } else if (Parser.STATUS_OPTIONAL_UPDATE.equals(statusText)) {
                status = OPERATION_OPTIONAL_UPDATE;
            } else if (Parser.STATUS_WARNING.equals(statusText)) {
                status = OPERATION_WARNING;
            } else if (Parser.STATUS_ERROR.equals(statusText)) {
                status = OPERATION_ERROR;
                updateURL = result.getUpdateURL();
            }

            messageCode = result.getCode();
            messageText = result.getMessage();

            if ((responseHandler != null) && (responseHandler instanceof ParsingHandler)
            		&& ((status == OPERATION_SUCCESSFUL)
            		|| (status == OPERATION_OPTIONAL_UPDATE))) {
                ((ParsingHandler) responseHandler).process(parser);
            }

        } else {

            status = OPERATION_STATUS_UNKNOWN;
            messageCode = null;
            messageText = null;

        }

    }
    
    public void process(ParserJSON parser) throws IOException, ParsingException {

        if (parser != null) {

            status = OPERATION_STATUS_UNKNOWN;
            Result result = parser.parseResult();
            String statusText = result.getStatus();
            if (Parser.STATUS_OK.equals(statusText)) {
                status = OPERATION_SUCCESSFUL;
            } else if (Parser.STATUS_OPTIONAL_UPDATE.equals(statusText)) {
                status = OPERATION_OPTIONAL_UPDATE;
            } else if (Parser.STATUS_WARNING.equals(statusText)) {
                status = OPERATION_WARNING;
            } else if (Parser.STATUS_ERROR.equals(statusText)) {
                status = OPERATION_ERROR;
                updateURL = result.getUpdateURL();
            }

            messageCode = result.getCode();
            messageText = result.getMessage();

            if ((responseHandler != null) && (responseHandler instanceof ParsingHandler)
            		&& ((status == OPERATION_SUCCESSFUL)
            		|| (status == OPERATION_OPTIONAL_UPDATE))) {
                ((ParsingHandler) responseHandler).process(parser);
            }else if ((responseHandler != null) && (responseHandler instanceof AceptaOfertaTDC)
                    && (status == OPERATION_ERROR))
            {
                ((ParsingHandler) responseHandler).process(parser);
            }

        } else {

            status = OPERATION_STATUS_UNKNOWN;
            messageCode = null;
            messageText = null;

        }

    }

}
