package bancomer.api.apiventatdc.implementations;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import org.apache.http.impl.client.DefaultHttpClient;

import bancomer.api.apiventatdc.commons.ConstantsVentaTDC;
import bancomer.api.apiventatdc.gui.controllers.ClausulasViewController;
import bancomer.api.apiventatdc.gui.controllers.DetalleViewController;
import bancomer.api.apiventatdc.gui.controllers.ExitoViewController;
import bancomer.api.apiventatdc.gui.controllers.PolizaViewController;
import bancomer.api.apiventatdc.gui.delegates.ClausulasDelegate;
import bancomer.api.apiventatdc.gui.delegates.ConsultaDelegate;
import bancomer.api.apiventatdc.gui.delegates.DetalleDelegate;
import bancomer.api.apiventatdc.gui.delegates.ExitoDelegate;
import bancomer.api.apiventatdc.io.BaseSubapplication;
import bancomer.api.apiventatdc.io.Server;
import bancomer.api.apiventatdc.models.AceptaOfertaTDC;
import bancomer.api.apiventatdc.models.OfertaTDC;
import bancomer.api.common.commons.IAutenticacion;
import bancomer.api.common.commons.ICommonSession;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.BaseViewsControllerImpl;
import bancomer.api.common.gui.delegates.DelegateBaseOperacion;
import bancomer.api.common.gui.delegates.MainViewHostDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

/**
 * Created by OOROZCO on 8/20/15.
 */
public class InitVentaTDC {


    /*Components for APIVENTATDC working*/

    private static InitVentaTDC mInstance = null;

    private BaseViewController baseViewController;

    private static BaseViewsController parentManager;

    private BaseSubapplication baseSubApp;

    private static DefaultHttpClient client;

    private Promociones ofertaAdicional;

    private DelegateBaseOperacion delegateOTP;

    private ICommonSession session;

    private IAutenticacion autenticacion;

    private boolean sofTokenStatus;

    private String cuentaOneClick;

    private MainViewHostDelegate hostInstance;


    /* End Region */

    /////////////  Getter's & Setter's /////////////////

    public static DefaultHttpClient getClient() {
        return client;
    }

    public ICommonSession getSession()
    {
        return session;
    }

    public DelegateBaseOperacion getDelegateOTP() {
        return delegateOTP;
    }

    public Promociones getOfertaVentaTDC()
    {
        return ofertaAdicional;
    }

    public BaseViewController getBaseViewController() {
        return baseViewController;
    }

    public void setBaseViewController(BaseViewController baseViewController) {
        this.baseViewController = baseViewController;
    }

    public BaseViewsController getParentManager() {
        return parentManager;
    }

    public BaseSubapplication getBaseSubApp() {
        return baseSubApp;
    }

    public IAutenticacion getAutenticacion() {  return autenticacion;  }

    public boolean getSofTokenStatus(){ return sofTokenStatus; }

    public String getCuentaOneClick() {
        return cuentaOneClick;
    }


///////////// End Region Getter's & Setter's /////////////////


    public static InitVentaTDC getInstance(Activity activity, Promociones promocion,DelegateBaseOperacion delegateOTP, ICommonSession session,IAutenticacion autenticacion,boolean softTokenStatus,String cuentaOneClick, DefaultHttpClient client, MainViewHostDelegate hostInstance, boolean dev, boolean sim, boolean emulator, boolean log)
    {
        if(mInstance == null)
        {
            //Log.d("[CGI-Configuracion-Obligatorio] >> ", "[InitVentaTDC] Se inicializa la instancia del singleton de datos");
            mInstance = new InitVentaTDC(activity,promocion,delegateOTP,session,autenticacion,softTokenStatus,cuentaOneClick,client,hostInstance, dev,  sim,  emulator, log);
        }

        return mInstance;
    }

    public static InitVentaTDC getInstance()
    {
        return mInstance;
    }

    public void resetInstance()
    {   mInstance = null; }

    public void updateHostActivityChangingState()
    {
        hostInstance.updateActivityChangingState();
    }

    private InitVentaTDC(Activity activity, Promociones promocion, DelegateBaseOperacion delegateOTP,
                         ICommonSession session, IAutenticacion autenticacion, boolean sofTokenStatus,
                         String cuentaOneClick, DefaultHttpClient client, MainViewHostDelegate hostInstance,
                         boolean dev, boolean sim, boolean emulator, boolean log)
    {
        setServerParams(dev,  sim,  emulator,  log);
        Server.setEnviroment();
        this.hostInstance = hostInstance;
        ofertaAdicional = promocion;
        this.delegateOTP=delegateOTP;
        baseViewController = new BaseViewControllerImpl();
        this.client = client;
        parentManager = new BaseViewsControllerImpl();
        parentManager.setCurrentActivityApp(activity);
        baseSubApp = new BaseSubapplicationImpl(activity, client);
        this.session=session;
        this.autenticacion=autenticacion;
        this.sofTokenStatus = sofTokenStatus;
        this.cuentaOneClick = cuentaOneClick;

    }

    private void setServerParams(boolean dev, boolean sim, boolean emulator, boolean log)
    {
        Server.DEVELOPMENT = dev;
        Server.SIMULATION = sim;
        Server.EMULATOR = emulator;
        Server.ALLOW_LOG = log;
    }


    public void showClausulas(OfertaTDC oferta, Promociones promocion){


        ClausulasDelegate delegate = (ClausulasDelegate) parentManager.getBaseDelegateForKey(ClausulasDelegate.CLAUSULAS_VENTA_TDC_DELEGATE);
        if(delegate == null)
        {
            delegate = new ClausulasDelegate(oferta,promocion);
            parentManager.addDelegateToHashMap(ClausulasDelegate.CLAUSULAS_VENTA_TDC_DELEGATE,delegate);
        }

        //showViewController(ClausulasViewController.class,0,false,null,null);
        parentManager.showViewController(ClausulasViewController.class,0,false,null,null,parentManager.getCurrentViewController());

    }


    public void showExito(AceptaOfertaTDC oferta, Promociones promocion)
    {

        ExitoDelegate delegate = (ExitoDelegate) parentManager.getBaseDelegateForKey(ExitoDelegate.EXITO_DELEGATE_VENTA_TDC);
        if(delegate == null)
        {
            delegate = new ExitoDelegate(oferta,promocion);
            parentManager.addDelegateToHashMap(ExitoDelegate.EXITO_DELEGATE_VENTA_TDC,delegate);
        }
       //showViewController(ExitoViewController.class,0,false,null,null);
        parentManager.showViewController(ExitoViewController.class,0,false,null,null,parentManager.getCurrentViewController());
    }

    public void showDetalle(OfertaTDC oferta)
    {
        DetalleDelegate delegate = (DetalleDelegate) parentManager.getBaseDelegateForKey(DetalleDelegate.DETALLE_DELEGATE_VENTA_TDC);
        if(delegate == null)
        {
            delegate = new DetalleDelegate(oferta);
            parentManager.addDelegateToHashMap(DetalleDelegate.DETALLE_DELEGATE_VENTA_TDC,delegate);
        }
        //showViewController(DetalleViewController.class,0,false,null,null);
        parentManager.showViewController(DetalleViewController.class,0,false,null,null,parentManager.getCurrentViewController());




    }


    public void showDomiciliacionTDC(String domiciliacion)
    {
       /* showViewController(PolizaViewController.class,0,false,
        new String[] { ConstantsVentaTDC.DOMICILIACION_VENTATDC },
        new Object[] { domiciliacion });*/
        parentManager.showViewController(PolizaViewController.class,0,false,new String[] { ConstantsVentaTDC.DOMICILIACION_VENTATDC },new Object[] { domiciliacion },parentManager.getCurrentViewController());
    }


    public void showTermsAndConditions(String terms)
    {
        /*showViewController(PolizaViewController.class, 0, false,
                new String[]{ConstantsVentaTDC.TERMINOS_VENTATDC},
                new Object[]{terms});*/

        parentManager.showViewController(PolizaViewController.class,0,false,new String[] { ConstantsVentaTDC.TERMINOS_VENTATDC },new Object[] { terms },parentManager.getCurrentViewController());
    }


    public void startVentaTDC()
    {
        new ConsultaDelegate().performAction(null);
    }



}
