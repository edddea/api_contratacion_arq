package bancomer.api.apiventatdc.gui.delegates;

import android.content.DialogInterface;
import android.util.Log;

import java.util.Hashtable;

import bancomer.api.apiventatdc.R;
import bancomer.api.apiventatdc.commons.ConstantsVentaTDC;
import bancomer.api.apiventatdc.gui.controllers.ClausulasViewController;
import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bancomer.api.apiventatdc.io.Server;
import bancomer.api.apiventatdc.models.AceptaOfertaTDC;
import bancomer.api.apiventatdc.models.ConsultaTerminosCondicionesTDC;
import bancomer.api.apiventatdc.models.OfertaTDC;
import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.gui.delegates.SecurityComponentDelegate;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.common.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.aplicaciones.commservice.commons.CommContext;

/**
 * Created by OOROZCO on 8/31/15.
 */
public class ClausulasDelegate extends SecurityComponentDelegate implements BaseDelegate {

    public static final long CLAUSULAS_VENTA_TDC_DELEGATE = 15022134566765661L;


    private ClausulasViewController controller;
    private InitVentaTDC init;

    private OfertaTDC oferta;
    private Promociones promocion;

    private Constants.Operacion tipoOperacion;
    private Constants.TipoInstrumento tipoInstrumentoSeguridad;
    private String token;


    public OfertaTDC getOferta() {
        return oferta;
    }

    public Promociones getPromocion() {
        return promocion;
    }

    public Constants.TipoInstrumento getTipoInstrumentoSeguridad() {
        return tipoInstrumentoSeguridad;
    }

    public void setController(ClausulasViewController controller) {
        this.controller = controller;
        setContext(controller);
    }

    public ClausulasDelegate(OfertaTDC oferta, Promociones promocion)
    {
        this.oferta=oferta;
        this.promocion=promocion;
        init = InitVentaTDC.getInstance();
        this.tipoOperacion = Constants.Operacion.oneClickBmovilConsumo;

        String instrumento = init.getSession().getSecurityInstrument();

        if (instrumento.equals(Constants.IS_TYPE_DP270)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
        } else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
        } else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
        } else {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
        }
    }




    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, final boolean isJson,final ParsingHandler handler) {
        init.getBaseSubApp().invokeNetworkOperation(operationId,params,caller,false,isJson,handler);
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
        //metodo vacio
    }
    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

        if (operationId == Server.CONSULTA_TERMINOS_CONDICIONES_TDC){
            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                ConsultaTerminosCondicionesTDC polizaResponse = (ConsultaTerminosCondicionesTDC)response.getResponse();
                init.showTermsAndConditions(polizaResponse.getFormatoHTML());
            }else if (response.getStatus() == ServerResponse.OPERATION_ERROR){
                init.getBaseViewController().showErrorMessage(controller,response.getMessageText());
            }
        }
        else if (operationId == Server.ACEPTACION_OFERTA_TDC){

            AceptaOfertaTDC aceptaOfertaTDC = (AceptaOfertaTDC)response.getResponse();

            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)
            {
                init.getSession().setPromocion(aceptaOfertaTDC.getPromociones());
                aceptaOfertaTDC.setDomicilio(controller.getOpcionDomicilio().equalsIgnoreCase("default") ? true : false);
                init.showExito(aceptaOfertaTDC,promocion);
            }
            else if(response.getStatus() == ServerResponse.OPERATION_ERROR || response.getStatus() == ServerResponse.OPERATION_WARNING)
            {
                try {

                    if(!aceptaOfertaTDC.getPM().equalsIgnoreCase("SI")){
                        init.getBaseViewController().showErrorMessage(controller,response.getMessageText());
                    }else
                    {
                        init.getSession().setPromocion(aceptaOfertaTDC.getPromociones());

                        DialogInterface.OnClickListener clickListener=new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                init.getParentManager().setActivityChanging(true);
                                //init.getParentManager().showMenuInicial();
                                ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                                init.resetInstance();
                            }
                        };
                        init.getBaseViewController().showInformationAlert(controller, response.getMessageText(), clickListener);

                    }
                }catch(NullPointerException ex)
                {
                    init.getBaseViewController().showErrorMessage(controller,response.getMessageText());
                }


            }
        }


    }

    @Override
    public void performAction(Object obj) {


        String cadAutenticacion = init.getAutenticacion().getCadenaAutenticacion(this.tipoOperacion,init.getSession().getClientProfile());

        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put(ConstantsVentaTDC.LINEA_CREDITO_TAG,oferta.getLineaCredito());
        params.put(ConstantsVentaTDC.NUMERO_CELULAR_TAG,init.getSession().getUsername());

        //params.put(ConstantsVentaTDC.ID_PRODUCTO_TAG,oferta.getProducto());
        params.put(ConstantsVentaTDC.ID_PRODUCTO_TAG, oferta.getDesProducto().substring(0, 2));

        params.put(ConstantsVentaTDC.SOLO_COMPRAS_TAG,ConstantsVentaTDC.SOLO_COMPRAS_VALUE);
        params.put(ConstantsVentaTDC.DOMICILIACION_TAG,controller.getDomiciliacion());
        params.put(ConstantsVentaTDC.CVE_CAMP_TAG,promocion.getCveCamp());
        params.put(ConstantsVentaTDC.CADENA_AUTENTICACION_TAG,cadAutenticacion);
        params.put(ConstantsVentaTDC.CODIGO_OTP_TAG,token);
        params.put(ConstantsVentaTDC.CODIGO_NIP_TAG,controller.pideNIP());
        params.put(ConstantsVentaTDC.CODIGO_CVV2_TAG,controller.pideCVV());
        params.put(ConstantsVentaTDC.TARJETA_5IG_TAG, init.getCuentaOneClick().substring(init.getCuentaOneClick().length()-5,init.getCuentaOneClick().length()));
        params.put(ConstantsVentaTDC.CVE_ACCESO_TAG,controller.pideContrasena());
        params.put(ConstantsVentaTDC.OPCION_DOMICILIO_TAG,ConstantsVentaTDC.OPCION_DOMICILIO_VALUE);
        params.put(ConstantsVentaTDC.IUM_TAG, init.getSession().getIum());



        doNetworkOperation(Server.ACEPTACION_OFERTA_TDC,params,init.getBaseViewController(),true,new AceptaOfertaTDC());

    }


    public void realizaOperacionTerminosYCondiciones() {

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(ConstantsVentaTDC.ID_PRODUCTO_TAG, oferta.getDesProducto().substring(0,2));
        params.put(ConstantsVentaTDC.NUMERO_CELULAR_TAG,init.getSession().getUsername() );
        params.put(ConstantsVentaTDC.IUM_TAG, init.getSession().getIum());
        params.put(ConstantsVentaTDC.IND_VACIO,ConstantsVentaTDC.IND_VACIO_VALUE_TERM);
        params.put(ConstantsVentaTDC.IND_CONTRATO, ConstantsVentaTDC.IND_CONTRATO_VALUE_TERM);
        params.put(ConstantsVentaTDC.NUM_CONTRATO, "");


        doNetworkOperation(Server.CONSULTA_TERMINOS_CONDICIONES_TDC, params, init.getBaseViewController(), true, new ConsultaTerminosCondicionesTDC());

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }




    /////// Methods for Security components showing. /////////


    public boolean mostrarContrasenia() {
        return init.getAutenticacion().mostrarContrasena(this.tipoOperacion,
                init.getSession().getClientProfile(),
                Tools.getDoubleAmountFromServerString(oferta.getLineaCredito()));
    }

    public boolean mostrarNIP() {
        return init.getAutenticacion().mostrarNIP(this.tipoOperacion,
                init.getSession().getClientProfile());
    }

    public Constants.TipoOtpAutenticacion tokenAMostrar() {
        Constants.TipoOtpAutenticacion tipoOTP;
        try {
            tipoOTP = init.getAutenticacion().tokenAMostrar(
                    this.tipoOperacion,
                    init.getSession().getClientProfile(),
                    Tools.getDoubleAmountFromServerString(oferta.getLineaCredito())
            );

        } catch (Exception ex) {
            if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "Error on Autenticacion.mostrarNIP execution.", ex);
            tipoOTP = null;
        }

        return tipoOTP;
    }

    public boolean mostrarCVV() {
        return init.getAutenticacion().mostrarCVV(this.tipoOperacion,
                init.getSession().getClientProfile(),
                Tools.getDoubleAmountFromServerString(oferta.getLineaCredito()));
    }

    public boolean enviaPeticionOperacion() {
        String contrasena = null;
        String nip = null;
        String asm = null;
        String cvv = null;
        if(controller instanceof ClausulasViewController){
            if (mostrarContrasenia()) {
                contrasena = controller.pideContrasena();
                if (contrasena.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteContrasena);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.PASSWORD_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteContrasena);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            if (mostrarNIP()) {
                nip = controller.pideNIP();
                if (nip.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteNip);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (nip.length() != Constants.NIP_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.NIP_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteNip);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }
            if (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) {
                asm = controller.pideASM();
                if (asm.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    switch (tipoInstrumentoSeguridad) {
                        case OCRA:
                            mensaje += getEtiquetaCampoOCRA();
                            break;
                        case DP270:
                            mensaje += getEtiquetaCampoDP270();
                            break;
                        case SoftToken:
                            if (init.getSofTokenStatus()) {
                                mensaje += getEtiquetaCampoSoftokenActivado();
                            } else {
                                mensaje += getEtiquetaCampoSoftokenDesactivado();
                            }
                            break;
                        default:
                            break;
                    }
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (asm.length() != Constants.ASM_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.ASM_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    switch (tipoInstrumentoSeguridad) {
                        case OCRA:
                            mensaje += getEtiquetaCampoOCRA();
                            break;
                        case DP270:
                            mensaje += getEtiquetaCampoDP270();
                            break;
                        case SoftToken:
                            if (init.getSofTokenStatus()) {
                                mensaje += getEtiquetaCampoSoftokenActivado();
                            } else {
                                mensaje += getEtiquetaCampoSoftokenDesactivado();
                            }
                            break;
                        default:
                            break;
                    }
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }
            if (mostrarCVV()) {
                cvv = controller.pideCVV();
                if (cvv.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteCvv);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (cvv.length() != Constants.CVV_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.CVV_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteCvv);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            String newToken = null;
            if(tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == Constants.TipoInstrumento.SoftToken && init.getSofTokenStatus())
                newToken = init.getDelegateOTP().loadOtpFromSofttoken(tokenAMostrar(),init.getDelegateOTP());
            if(null != newToken)
                asm = newToken;
            token=asm;
            return true;
        }
        return false;
    }





}
