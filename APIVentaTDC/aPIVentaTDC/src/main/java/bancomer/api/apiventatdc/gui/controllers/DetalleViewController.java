package bancomer.api.apiventatdc.gui.controllers;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.adobe.mobile.Config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.apiventatdc.R;
import bancomer.api.apiventatdc.commons.TrackerVentaTDC;
import bancomer.api.apiventatdc.gui.delegates.DetalleDelegate;
import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;

/**
 * Created by OOROZCO on 8/28/15.
 */
public class DetalleViewController extends Activity implements View.OnClickListener  {

    private BaseViewController baseViewController;
    private InitVentaTDC init = InitVentaTDC.getInstance();
    private DetalleDelegate detalleVentaTDCDelegate;
    public BaseViewsController parentManager;

    private TextView lblTextoPromocionTDC;
    private TextView textImporteCreditoTDC;
    private TextView textInformativo;
    private TextView textFechaCalculoTDC;

    private ImageButton btnmeInteresa;


    //Adobe
    String screen = "";
    ArrayList<String> list= new ArrayList<String>();
    Map<String,Object> operacionMap = new HashMap<String, Object>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        detalleVentaTDCDelegate=(DetalleDelegate) parentManager.getBaseDelegateForKey(DetalleDelegate.DETALLE_DELEGATE_VENTA_TDC);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_venta_tdc_detalle);
        baseViewController.setTitle(this, R.string.promocion_title_ventaTDC, R.drawable.an_ic_venta_tdc);
        baseViewController.setDelegate(detalleVentaTDCDelegate);
        init.setBaseViewController(baseViewController);

        //Adobe
        Config.setContext(this.getApplicationContext());

        screen = "detalle venta tdc";
        list.add(screen);
        TrackerVentaTDC.trackState(list);

        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        TrackerVentaTDC.trackEntrada(eventoEntrada);

        findViews();
        scaleForCurrentScreen();

    }


    @Override
    public void onClick(View v) {
        init.getParentManager().setActivityChanging(true);
        init.showClausulas(detalleVentaTDCDelegate.getOfertaTDC(), init.getOfertaVentaTDC());
    }


    @Override
    protected void onPause() {
        super.onPause();
        Config.collectLifecycleData();
        if(!init.getParentManager().isActivityChanging())
        {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if (DialogInterface.BUTTON_POSITIVE == which) {
                        init.getParentManager().setActivityChanging(true);
                        //init.getParentManager().showMenuInicial();
                        ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                        init.getParentManager().resetRootViewController();
                        init.resetInstance();
                    }else{
                        dialog.dismiss();
                    }

                }
            };

            init.getBaseViewController().showYesNoAlert(this,R.string.promocion_ventatdc_rechazo, listener);

        }
        return true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }



    private void findViews(){

        lblTextoPromocionTDC = (TextView) findViewById(R.id.lblTextoPromocionTDC);
        String promo1 = getResources().getString(R.string.promocion_detalle_ventaTDC_title1);
        String promo2 = getResources().getString(R.string.promocion_detalle_ventaTDC_title2);
        String promo3 = getResources().getString(R.string.promocion_detalle_ventaTDC_title3);
        String promo4 = getResources().getString(R.string.promocion_detalle_ventaTDC_title4);

        String tarjeta = detalleVentaTDCDelegate.getOfertaTDC().getDesProducto();

        lblTextoPromocionTDC.setText(Html.fromHtml("<font color='" + getResources().getColor(R.color.primer_azul) + "'>" + promo1 + "<br/> " + promo2 + "</font>"
                + "<b><font color='" + getResources().getColor(R.color.verde) + "'>" + " " + tarjeta + " " + "</font></b>"
                + "<font color='" + getResources().getColor(R.color.primer_azul) + "'>" + promo3 + "<br/>" + promo4 + "</font>"), TextView.BufferType.SPANNABLE);

        textImporteCreditoTDC = (TextView) findViewById(R.id.textImporteCreditoTDC);
        String textImporte = Tools.formatAmount(detalleVentaTDCDelegate.getPromocion().getMonto(),false);
        SpannableString textoImporte = new SpannableString(textImporte);
        textoImporte.setSpan(new UnderlineSpan(), 0, textoImporte.length(), 0);
        textImporteCreditoTDC.setText(textoImporte);

        textInformativo = (TextView) findViewById(R.id.textInformativo);
        String cat = detalleVentaTDCDelegate.getOfertaTDC().getCat()+"%";
        String cat1 = getResources().getString(R.string.promocion_detalle_ventaTDC_cat1);
        String cat2 = getResources().getString(R.string.promocion_detalle_ventaTDC_cat2);

        String tazaInteres = detalleVentaTDCDelegate.getOfertaTDC().getTazaPonderada()+"%";
        String tazaInt1 = getResources().getString(R.string.promocion_detalle_ventaTDC_tazaInteres);

        String anualidad = "$"+Tools.formatUserAmount(detalleVentaTDCDelegate.getOfertaTDC().getAnualidad());
        String anualidad1 = getResources().getString(R.string.promocion_detalle_ventaTDC_anualidad1);
        String anualidad2 = getResources().getString(R.string.promocion_detalle_ventaTDC_anualidad2);

        textInformativo.setText(Html.fromHtml("<font color='"+getResources().getColor(R.color.gris_texto_3)+"'>"+cat1+" <b>"
                +cat+"</b>"+cat2+"<br>"+tazaInt1+" <b>"+tazaInteres+". </b>"+"<br>"
                +anualidad1+" <b>"+anualidad+",</b>"+"<br>"+anualidad2+"</font>"), TextView.BufferType.SPANNABLE);

        /*MAPE textInformativo.setText(Html.fromHtml("<font color='"+getResources().getColor(R.color.gris_texto_3)+"'>"+cat1+" <b>"
                +cat+"</b>"+cat2+" "+tazaInt1+" <b>"+tazaInteres+"</b>"
                +anualidad1+" <b>"+anualidad+"</b>"+anualidad2+"</font>"), TextView.BufferType.SPANNABLE);*/

        detalleVentaTDCDelegate.formatoFechaCatMostrar();
        textFechaCalculoTDC = (TextView) findViewById(R.id.textFechaCalculoTDC);
        String fecha = getResources().getString(R.string.promocion_detalle_ventaTDC_fechaCalculo);
        String info = getResources().getString(R.string.promocion_detalle_ventaTDC_infoFechaCalculo);
        textFechaCalculoTDC.setText(Html.fromHtml("<font color='"+getResources().getColor(R.color.gris_texto_3)+"'>"
                +fecha+" <b>"+detalleVentaTDCDelegate.getFechaCatVisual()+".</b> "+"<br>"
                +info+"</font>"), TextView.BufferType.SPANNABLE);

        btnmeInteresa = (ImageButton)findViewById(R.id.bmovil_btn_meinteresa);
        btnmeInteresa.setOnClickListener(this);
    }

    private void scaleForCurrentScreen() {
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(findViewById(R.id.lblTextoPromocionTDC), true);
        gTools.scale(findViewById(R.id.textImporteCreditoTDC), true);
        gTools.scale(findViewById(R.id.textInfoTDC), true);
        gTools.scale(findViewById(R.id.textInformativo), true);
        gTools.scale(findViewById(R.id.textFechaCalculoTDC), true);
        gTools.scale(btnmeInteresa);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Config.pauseCollectingLifecycleData();
    }
}
