package bancomer.api.apiventatdc.models;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;

/**
 * Created by OOROZCO on 8/31/15.
 */
public class ConsultaTerminosCondicionesTDC implements ParsingHandler {
    /**
     * Terminos de uso en formato HTML.
     */
    private String formatoHTML;


    /**
     * Tipo Producto solo para Venta TDC
     */
    private String tipoProducto;


    /**
     * @return Terminos de uso en formato HTML.
     */
    public String getFormatoHTML() {
        return formatoHTML;
    }

    /**
     * @param terminosHtml Terminos de uso en formato HTML.
     */
    public void setFormatoHTML(String formatoHTML) {
        this.formatoHTML = formatoHTML;
    }

    /**
     * getter and setter tipoProducto
     */

    public String getTipoProducto() {
        return tipoProducto;
    }

    public void setTipoProducto(String tipoProducto) {
        this.tipoProducto = tipoProducto;
    }

    public ConsultaTerminosCondicionesTDC() {
        formatoHTML = null;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        formatoHTML = null;
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        /**
         * Modified according to: 3- D520_Contratos Vacios TDC BMovil
         * Received at : June 23th, 2015.
         * //tipoProducto = parser.parseNextValue("tipoProducto");
         */
        formatoHTML = parser.parseNextValue("formatoHTML",true);
        formatoHTML = formatoHTML.replace('\'', '\"');
        formatoHTML = formatoHTML.replace("///", "/");
        grabarArchivo();

    }

    private void grabarArchivo(){
        try{
            File f = new File("//data/data/com.bancomer.mbanking/");
            File newFile = new File(f, "terminos");
            OutputStreamWriter osw = new OutputStreamWriter(
                    new FileOutputStream(newFile));
            osw.write(formatoHTML);
            osw.close();
            formatoHTML = newFile.getAbsolutePath();
        }catch(IOException e){
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

}
