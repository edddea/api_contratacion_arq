package bancomer.api.apiventatdc.gui.delegates;

import java.util.Hashtable;

import bancomer.api.apiventatdc.commons.ConstantsVentaTDC;
import bancomer.api.apiventatdc.gui.controllers.ExitoViewController;
import bancomer.api.apiventatdc.implementations.InitVentaTDC;
import bancomer.api.apiventatdc.io.Server;
import bancomer.api.apiventatdc.models.AceptaOfertaTDC;
import bancomer.api.apiventatdc.models.ConsultaTerminosCondicionesTDC;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.common.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.aplicaciones.commservice.commons.CommContext;

/**
 * Created by OOROZCO on 9/1/15.
 */
public class ExitoDelegate implements BaseDelegate {

    public static final long EXITO_DELEGATE_VENTA_TDC = 150909334566765662L;
    private ExitoViewController controller;
    private AceptaOfertaTDC aceptaOfertaTDC;
    private Promociones promocion;
    private InitVentaTDC init;

    public ExitoDelegate(AceptaOfertaTDC oferta, Promociones promocion)
    {
        this.aceptaOfertaTDC = oferta;
        this.promocion = promocion;
        init = InitVentaTDC.getInstance();
    }



    public void setController(ExitoViewController controller) {
        this.controller = controller;
    }

	@Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
        //metodo vacio
    }

    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller, final boolean isJson,final ParsingHandler handler) {
        init.getBaseSubApp().invokeNetworkOperation(operationId,params,caller,false,isJson,handler);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)
        {
            init.getParentManager().setActivityChanging(true);
            ConsultaTerminosCondicionesTDC result = (ConsultaTerminosCondicionesTDC) response.getResponse();
            init.showDomiciliacionTDC(result.getFormatoHTML());
        }else if (response.getStatus() == ServerResponse.OPERATION_ERROR){
            init.getBaseViewController().showErrorMessage(controller,response.getMessageText());
        }
    }

    @Override
    public void performAction(Object obj) {

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(ConstantsVentaTDC.ID_PRODUCTO_TAG, aceptaOfertaTDC.getProducto());
        params.put(ConstantsVentaTDC.NUMERO_CELULAR_TAG,init.getSession().getUsername() );
        params.put(ConstantsVentaTDC.IUM_TAG, init.getSession().getIum());
        params.put(ConstantsVentaTDC.IND_VACIO,ConstantsVentaTDC.IND_VACIO_VALUE);
        params.put(ConstantsVentaTDC.IND_CONTRATO, ConstantsVentaTDC.IND_CONTRATO_VALUE);
        params.put(ConstantsVentaTDC.NUM_CONTRATO, aceptaOfertaTDC.getNumContrato());


        doNetworkOperation(Server.CONSULTA_TERMINOS_CONDICIONES_TDC, params, init.getBaseViewController(),true,new ConsultaTerminosCondicionesTDC());

    }

    

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }

    public void setValues()
    {
        /**Sets amount on the view.**/
        controller.setAmount(promocion.getMonto());

        if(!aceptaOfertaTDC.isDomicilio())
        {
            /**Shows one or two links whether or not is going to be delivered at customer address.**/
            controller.hideLinkContrato();
            /**Sets text about getting the card at Bancomer Offices.**/
            controller.showOfficeParagraphs();
        }
        else
        {
            /**Sets text about getting the card at customer address.**/
            controller.showAddressParagraphs();
        }
        /**If true shows Customer Mail.**/
        if(aceptaOfertaTDC.getEnvioCorreoElectronico().equals("SI"))
            controller.showMailComponents(init.getSession().getEmail());
        /**If true shows Customer phone.**/
        if(aceptaOfertaTDC.getEnvioSMS().equals("SI"))
            controller.showSMSComponents(init.getSession().getUsername());
    }
}
