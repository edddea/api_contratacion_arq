package bancomer.api.consultainversiones.implementations;

import android.text.Editable;
import android.text.TextWatcher;

public class BmovilTextWatcher implements TextWatcher {

    private BaseViewController controller;

    public BmovilTextWatcher(BaseViewController controller) {
        this.controller = controller;
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (null != controller)
            controller.onUserInteraction();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }
}
