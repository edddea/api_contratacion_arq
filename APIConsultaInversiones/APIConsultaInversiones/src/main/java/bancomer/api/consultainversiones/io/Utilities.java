package bancomer.api.consultainversiones.io;

import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class Utilities {
    /**
     * Print http headers. Useful for debugging.
     *
     * @param req
     */
    public static void getHeadersAsString(HttpPost req) {

        try {
            req.getEntity().writeTo(System.out);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        StringBuffer s = new StringBuffer("Method: ");
        s.append(req.getMethod());
        try {
            s.append("Entity: ");
            s.append(EntityUtils.toString(req.getEntity()));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        org.apache.http.Header[] headers = req.getAllHeaders();
        s.append("Headers: ");
        s.append("------------");
        for (org.apache.http.Header h : headers)
            s.append(h.toString());
        s.append("------------");
        System.out.println(s.toString());
    }
}
