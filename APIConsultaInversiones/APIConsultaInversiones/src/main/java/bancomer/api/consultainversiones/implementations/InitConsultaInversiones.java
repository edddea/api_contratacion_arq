package bancomer.api.consultainversiones.implementations;

import android.content.Context;

import com.bancomer.base.callback.CallBackAPI;
import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.base.callback.CallBackSession;

import bancomer.api.consultainversiones.gui.controllers.DetalleInversionViewController;
import bancomer.api.consultainversiones.gui.delegates.InversionDelegate;
import bancomer.api.consultainversiones.io.BanderasServer;
import bancomer.api.consultainversiones.models.ConsultaInversionesPlazoData;
import bancomer.api.consultainversiones.models.InversionesPlazo;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

/**
 * Created by jinclan on 14/08/15.
 */
public class InitConsultaInversiones {

    public static InitConsultaInversiones initConsultaInversiones;
    boolean isActivityChanging;
    private Context context;
    private SuiteAppConsultaInversionesApi suiteAppConsultaInversionesApi;
    private CallBackSession callBackSession;
    private CallBackAPI callBackAPI;
    private ConsultaInversionesPlazoData inversionesData;

    private InitConsultaInversiones(BanderasServer banderasServer) {
        ServerCommons.ALLOW_LOG = banderasServer.getLogActivo();
        ServerCommons.DEVELOPMENT = banderasServer.isDesarrollo();
        ServerCommons.EMULATOR = banderasServer.getEmulador();
        ServerCommons.SIMULATION = banderasServer.getSimulacion();
    }

    public static InitConsultaInversiones getInstance(Context context, BanderasServer bandera) {
        if (initConsultaInversiones == null) {
        initConsultaInversiones = new InitConsultaInversiones(bandera);
        }
        initConsultaInversiones.setContext(context);
        return initConsultaInversiones;
    }

    public void setParameters(boolean ALLOW_LOG, boolean DEVELOPMENT, boolean EMULATOR, boolean SIMULATION) {
        ServerCommons.ALLOW_LOG = ALLOW_LOG;
        ServerCommons.DEVELOPMENT = DEVELOPMENT;
        ServerCommons.EMULATOR = EMULATOR;
        ServerCommons.SIMULATION = SIMULATION;
    }

    public void initializeDependenceApis() {
        //if(suiteAppConsultaInversionesApi ==null){
        suiteAppConsultaInversionesApi = new SuiteAppConsultaInversionesApi();
        suiteAppConsultaInversionesApi.onCreate(context);
        //}
    }

    public SuiteAppConsultaInversionesApi getSuiteAppConsultaInversionesApi() {
        return suiteAppConsultaInversionesApi;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public CallBackSession getCallBackSession() {
        return callBackSession;
    }

    public void setCallBackSession(CallBackSession callBackSession) {
        this.callBackSession = callBackSession;
    }

    public CallBackAPI getCallBackAPI() {
        return callBackAPI;
    }

    public void setCallBackAPI(CallBackAPI callBackAPI) {
        this.callBackAPI = callBackAPI;
    }

    public void configurar(InversionesPlazo inversion) {

            configurarConInversion(inversion);

    }

    public void configurarConInversion(InversionesPlazo inversion) {
        initConsultaInversiones.initializeDependenceApis();
        final BaseViewsController baseViewsController = SuiteAppConsultaInversionesApi
                .getInstance().getBmovilApplication()
                .getBmovilViewsController();
        final InversionDelegate delegate = new InversionDelegate();
        delegate.setInversion(inversion);

        baseViewsController.addDelegateToHashMap(
                InversionDelegate.INVERSION_DELEGATE_ID, delegate);
    }

    public void returnToPrincipal() {
        callBackAPI.returnToPrincipal();
    }

    public void returnDataToPrincipal(String operation, ServerResponse response) {
        callBackAPI.returnDataFromModule(operation, response);
    }

    public void consultarInversiones(String fullNumber) {
        InversionDelegate delegate = (InversionDelegate) SuiteAppConsultaInversionesApi
                .getInstance().getBmovilApplication()
                .getBmovilViewsController().getBaseDelegateForKey(InversionDelegate.INVERSION_DELEGATE_ID);
        //Aqui esta nulo
        if (delegate == null) {
            delegate = new InversionDelegate();
            SuiteAppConsultaInversionesApi
                    .getInstance().getBmovilApplication()
                    .getBmovilViewsController().addDelegateToHashMap(InversionDelegate.INVERSION_DELEGATE_ID,
                    delegate);
        }

        DetalleInversionViewController detalleInversionViewController = new DetalleInversionViewController();
        detalleInversionViewController.setDelegate(delegate);
        delegate.setDetalleInversionViewController(detalleInversionViewController);

        delegate.consultaInversiones(fullNumber);
    }

    public ConsultaInversionesPlazoData getInversionesData() {
        return inversionesData;
    }

    public void setInversionesData(ConsultaInversionesPlazoData inversionesData) {
        this.inversionesData = inversionesData;
    }
}
