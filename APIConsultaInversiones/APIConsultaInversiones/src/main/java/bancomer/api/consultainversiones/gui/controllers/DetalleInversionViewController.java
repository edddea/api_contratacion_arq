
package bancomer.api.consultainversiones.gui.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.util.ArrayList;

import bancomer.api.consultainversiones.R;
import bancomer.api.consultainversiones.commons.GuiTools;
import bancomer.api.consultainversiones.commons.TrackingHelper;
import bancomer.api.consultainversiones.gui.commons.controllers.ListaDatosViewController;
import bancomer.api.consultainversiones.gui.delegates.InversionDelegate;
import bancomer.api.consultainversiones.implementations.BaseViewController;
import bancomer.api.consultainversiones.implementations.BmovilViewsController;
import bancomer.api.consultainversiones.implementations.InitConsultaInversiones;
import bancomer.api.consultainversiones.implementations.InitMenuInversiones;
import bancomer.api.consultainversiones.implementations.SuiteAppConsultaInversionesApi;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

public class DetalleInversionViewController extends BaseViewController implements View.OnClickListener {

    private InversionDelegate delegate;
    public BmovilViewsController parentManager;
    private ScrollView vista;
    private LinearLayout listaDetallesLayout;
    private ListaDatosViewController listaDetalles;
    SuiteAppConsultaInversionesApi suiteApp;

    private ImageButton menuButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.activity_detalle_inversion_view_controller);
        //marqueeEnabled = true;
        setTitle(R.string.misCuentas_inversionesPlazo, R.drawable.api_consulta_inversiones_bmovil_consultar_icono);
        //AMZ
        parentManager = SuiteAppConsultaInversionesApi.getInstance().getBmovilApplication().getBmovilViewsController();
        TrackingHelper.trackState("consulta inversiones", parentManager.estados);

        setParentViewsController(SuiteAppConsultaInversionesApi.getInstance().getBmovilApplication().getBmovilViewsController());
        delegate = (InversionDelegate) parentViewsController.getBaseDelegateForKey(InversionDelegate.INVERSION_DELEGATE_ID);
        delegate.setDetalleInversionViewController(this);

        init();
    }

    public void init() {
        muestraIndicadorActividad(getString(R.string.alert_operation), getString(R.string.alert_connecting));
        findViews();
        cargaListaDatos();
        scaleForCurrentScreen();
        ocultaIndicadorActividad();
    }

    public void cargaListaDatos() {
        if (Server.ALLOW_LOG) Log.d(">> CGI", "Pintamos Detalle de la inversion en pantalla");
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        ArrayList<Object> detalles = new ArrayList<Object>();

        listaDetalles = new ListaDatosViewController(this, params,
                parentManager);
        detalles = delegate.getDatosDetalleInversion();
        listaDetalles.setNumeroCeldas(2);
        listaDetalles.setLista(detalles);
        listaDetalles.setNumeroFilas(detalles.size());
        listaDetalles
                .setTitulo(R.string.detalle_inversion);
        listaDetalles.showLista();
        listaDetallesLayout.addView(listaDetalles);
    }

    @Override
    public void onClick(View v) {
        if (v.equals(menuButton)) {
            parentManager.removeDelegateFromHashMap(delegate.INVERSION_DELEGATE_ID);
            InitConsultaInversiones.initConsultaInversiones.getCallBackAPI().returnToPrincipal();
            parentManager.showMenuPrincipal();
        }
    }

    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    public void goBack() {
        super.goBack();
    }

    private void findViews() {
        vista = (ScrollView) findViewById(R.id.pagoTDCLL);

        listaDetallesLayout = (LinearLayout) findViewById(R.id.listaDetallesLayout);

        menuButton = (ImageButton) findViewById(R.id.detalle_inversion_boton_menu);
        menuButton.setOnClickListener(this);
    }

    private void scaleForCurrentScreen() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.pagoTDCLL));
        guiTools.scale(findViewById(R.id.rootLayout));

        guiTools.scale(findViewById(R.id.listaDetallesLayout));

        guiTools.scale(findViewById(R.id.menulayout));
        guiTools.scale(findViewById(R.id.detalle_inversion_boton_menu));
    }

    @Override
    public InversionDelegate getDelegate() {
        return delegate;
    }

    public void setDelegate(InversionDelegate delegate) {
        this.delegate = delegate;
    }
}