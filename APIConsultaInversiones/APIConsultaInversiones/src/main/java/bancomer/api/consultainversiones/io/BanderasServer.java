package bancomer.api.consultainversiones.io;

/**
 * Created by evaltierrah on 12/08/2015.
 */
public class BanderasServer {

    /**
     * Indicates simulation usage.
     */
    private Boolean simulacion = Boolean.FALSE;
    /**
     * Indica si esta activo el log.
     */
    private Boolean logActivo = Boolean.FALSE;
    /**
     * Defines if the application is running on the emulator or in the device.
     */
    private Boolean emulador = Boolean.FALSE;

    /**
     * Indicates if the base URL points to development server or production
     */
    private Boolean desarrollo = Boolean.FALSE;

    public BanderasServer(Boolean simulacion, Boolean logActivo, Boolean emulador, Boolean desarrollo) {
        this.simulacion = simulacion;
        this.logActivo = logActivo;
        this.emulador = emulador;
        this.desarrollo = desarrollo;
    }

    public Boolean getSimulacion() {
        return simulacion;
    }

    public void setSimulacion(Boolean simulacion) {
        this.simulacion = simulacion;
    }

    public Boolean getEmulador() {
        return emulador;
    }

    public void setEmulador(Boolean emulador) {
        this.emulador = emulador;
    }

    public Boolean isDesarrollo() {
        return desarrollo;
    }

    public void setDesarrollo(Boolean desarrollo) {
        this.desarrollo = desarrollo;
    }

    public Boolean getLogActivo() {
        return logActivo;
    }

    public void setLogActivo(Boolean logActivo) {
        this.logActivo = logActivo;
    }
}
