package bancomer.api.consultainversiones.gui.delegates;

import android.content.Intent;
import android.util.Log;

import com.bancomer.base.SuiteApp;
import com.bancomer.base.callback.CallBackAPI;
import com.bancomer.base.callback.CallBackSession;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.consultainversiones.R;
import bancomer.api.consultainversiones.gui.controllers.DetalleInversionViewController;
import bancomer.api.consultainversiones.gui.controllers.DetalleMenuInversionesViewController;
import bancomer.api.consultainversiones.implementations.BaseDelegate;
import bancomer.api.consultainversiones.implementations.BaseViewController;
import bancomer.api.consultainversiones.implementations.BmovilViewsController;
import bancomer.api.consultainversiones.implementations.InitConsultaInversiones;
import bancomer.api.consultainversiones.implementations.InitMenuInversiones;
import bancomer.api.consultainversiones.implementations.SuiteAppConsultaInversionesApi;
import bancomer.api.consultainversiones.models.ConsultaInversionesPlazoData;
import bancomer.api.consultainversiones.models.InversionesPlazo;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

/**
 * Created by andres.vicentelinare on 21/10/2015.
 */
public class InversionDelegate extends BaseDelegate {

    public final static long INVERSION_DELEGATE_ID = 4457665747L;

    private BaseViewController detalleInversionViewController;

    private BaseViewController detalleMenuInversionesController;

    private Constants.Operacion operacion = Constants.Operacion.consultarInversiones;

    private InversionesPlazo inversion;

    private ArrayList<InversionesPlazo> listaInversionesPlazos;

    private String fullNumber;

    public void consultaInversiones(String fullNumber) {
        Session session = Session.getInstance(SuiteApp.appContext);

        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
        paramTable.put(ServerConstants.CODIGO_NIP, "");
        paramTable.put(ServerConstants.CODIGO_CVV2, "");
        paramTable.put(ServerConstants.CODIGO_OTP, "");
        paramTable.put(ServerConstants.CADENA_AUTENTICACION, Autenticacion.getInstance().getCadenaAutenticacion(this.operacion, session.getClientProfile()));
        paramTable.put(ServerConstants.CVE_ACCESO, "");
        paramTable.put(ServerConstants.TARJETA_5DIG, "");
        paramTable.put(ServerConstants.CUENTA, fullNumber );
        this.fullNumber = fullNumber;
        doNetworkOperation(ApiConstants.OP_CONSULTA_INVERSIONES_PLAZO, paramTable, true, new ConsultaInversionesPlazoData(), detalleInversionViewController);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (operationId == ApiConstants.OP_CONSULTA_INVERSIONES_PLAZO) {
            InitConsultaInversiones.initConsultaInversiones.returnDataToPrincipal(String.valueOf(ApiConstants.OP_CONSULTA_INVERSIONES_PLAZO), response);
        }
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson, ParsingHandler handler, BaseViewController caller) {
        BmovilViewsController parent = null;
        if (detalleInversionViewController != null && detalleInversionViewController.getParentViewsController() instanceof BmovilViewsController) {
            parent = ((BmovilViewsController) detalleInversionViewController.getParentViewsController());
        } else {
            parent = SuiteAppConsultaInversionesApi.getInstance().getBmovilApplication().getBmovilViewsController();
        }
        parent.getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, caller, true);
    }

    @Override
    public long getDelegateIdentifier() {
        // TODO Auto-generated method stub
        return super.getDelegateIdentifier();
    }

    public void setDetalleInversionViewController(BaseViewController detalleInversionViewController) {
        this.detalleInversionViewController = detalleInversionViewController;
    }


    public void setDetalleMenuInversionesController(BaseViewController detalleMenuInversionesController) {
        this.detalleMenuInversionesController = detalleMenuInversionesController;
    }



    public void setInversion(InversionesPlazo inversion) {
        this.inversion = inversion;
    }

    public ArrayList<Object> getDatosDetalleInversion() {
        if (Server.ALLOW_LOG) Log.d(">> CGI", "Get Datos del Detalle Inversion");
        final ArrayList<Object> datos = new ArrayList<Object>();
        ArrayList<String> fila;


        fila = new ArrayList<String>();
        fila.add(detalleInversionViewController
                .getString(R.string.numero_contrato));

        fila.add(Tools.hideAccountNumber(obtenerCuentaInversion()));
        datos.add(fila);

        fila = new ArrayList<String>();
        fila.add(detalleInversionViewController
                .getString(R.string.numero_inversion));
        fila.add(inversion.getNumeroInversion());
        datos.add(fila);

        fila = new ArrayList<String>();
        fila.add(detalleInversionViewController
                .getString(R.string.saldo));

        fila.add(Tools.formatAmount(inversion.getSaldo(), false));
        datos.add(fila);

        fila = new ArrayList<String>();
        fila.add(detalleInversionViewController
                .getString(R.string.fecha_vencimiento));
        String fecha = inversion.getFechaVencimiento();
        if (fecha.matches("\\d{4}/\\d{2}/\\d{2}")) {
            fecha = Tools.formatDateTDC(fecha);
        }
        fila.add(fecha);
        datos.add(fila);

        fila = new ArrayList<String>();
        fila.add(detalleInversionViewController
                .getString(R.string.tasa));
        fila.add(Tools.formatAmountFromServer(inversion.getTasa()));
        datos.add(fila);

        fila = new ArrayList<String>();
        fila.add(detalleInversionViewController
                .getString(R.string.gat));
        fila.add(Tools.formatPercentage(Tools.formatAmountFromServer(inversion.getGat())));
        datos.add(fila);

        return datos;
    }

    private String obtenerCuentaInversion(){
        Session session = Session.getInstance(SuiteApp.getInstance());
        String numeroCuenta = null;
        for (Account cuenta:
             session.getAccounts()) {

            if(cuenta.getType().equals("IN")){
                numeroCuenta = cuenta.getFullNumber();
            }
        }
        return numeroCuenta;
    }

    public ArrayList<InversionesPlazo> getListaInversionesPlazos() {
        return listaInversionesPlazos;
    }

    public void setListaInversionesPlazos(ArrayList<InversionesPlazo> listaInversionesPlazos) {
        this.listaInversionesPlazos = listaInversionesPlazos;
    }


    @Override
    public void performAction(Object obj) {

        if (obj instanceof InversionesPlazo) {

            InitConsultaInversiones init = InitConsultaInversiones.getInstance(detalleMenuInversionesController, new bancomer.api.consultainversiones.io.BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
            detalleMenuInversionesController.getParentViewsController().setActivityChanging(true);
            //init.setCallBackSession(detalleInversionViewController);
            //init.setCallBackAPI(detalleInversionViewController);

            init.configurar((InversionesPlazo) obj);

            final Intent intent = new Intent(detalleMenuInversionesController,
                    DetalleInversionViewController.class);
            detalleMenuInversionesController.startActivity(intent);
        }
    }
}

