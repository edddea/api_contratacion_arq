/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package bancomer.api.consultainversiones.io;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.commservice.httpcomm.NameValuePair;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

//import org.apache.http.NameValuePair;


/**
 * TODO: COMENTARIO DE LA CLASE
 *
 * @author BBVA Bancomer, DyD.
 */

public class ClienteHttp {

    /**
     * The field separator in the response.
     */
    private static final String PARAMETER_SEPARATOR = "*";
    private static ClienteHttp instance = null;
    /**
     * Se utiliza un httpclient de Apache Jakarta
     */
    public DefaultHttpClient client;

    //protected Context mContext;

    /**
     * Constructor.
     */
    public ClienteHttp() {
        createClient();
    }

    public static ClienteHttp getCurrent() {
        if (instance == null) {
            instance = new ClienteHttp();
        }
        return instance;
    }

    /**
     * Get the URL to invoke the server.
     *
     * @param operation operation code
     * @return a string with the response
     */
    public static String getUrl(String operation) {
        return Server.getOperationUrl(operation);
    }

    /**
     * Sets the activity context for this delegate
     * @param context the activity using the delegate
     */
    //public void setContext(Context context){
    //	mContext = context;
    //}

    /**
     * Invoke a network operation.
     * @param operation operation code
     * @param parameters parameters
     * @param handler instance capable of parsing and storing the result
     * @throws IOException
     * @throws ParsingException
     */

//*****************************************************************************************

    //Los dos métodos de abajo fueron comentados para "homologar" el proceso, ahora sólo entra
    //al tercer invokeOperation que desempeña la misma función que si los tres estuvieran presentes.


    //*****************************************************************************************
    

    
    /*public void invokeOperation(String operation, NameValuePair[] parameters,
            ParsingHandler handler) throws IOException, ParsingException, URISyntaxException {
        invokeOperation(operation, parameters, handler, false, false);
    }*/

    /**
     * Get the URL to invoke the server.
     *
     * @param operation  operation code
     * @param parameters parameters to pass to the server
     * @return a string with the response
     */
    public static String getParameterString(String operation, NameValuePair[] parameters) {

        StringBuffer sb = new StringBuffer("OP").append(operation);
        if (parameters != null) {
            NameValuePair parameter;
            int size = parameters.length;
            for (int i = 0; i < size; i++) {
                parameter = parameters[i];
                String name = parameter.getName();
                String value = parameter.getValue();
                sb.append(PARAMETER_SEPARATOR);
                //if(Server.ALLOW_LOG) Log.e("Name",name);
                //if(Server.ALLOW_LOG) Log.e("Value",value);
                sb.append(name);
                sb.append(value);
            }
        }

        String params = sb.toString();
        //params = encode(params);

        sb.setLength(0);

        sb.append(Server.OPERATION_CODE_PARAMETER);
        sb.append('=');
        if (Server.OPERATION_CODES[Server.OP_CONSULTA_TDC].equals(operation)) {
            sb.append(Server.OPERATION_CODE_VALUE_BMOVIL_MEJORADO);

        } else if (Server.OPERATION_CODES[Server.OP_SOLICITAR_ALERTAS].equals(operation)) {
            sb.append(Server.OPERATION_CODE_VALUE_RECORTADO);
        } else {
            sb.append(Server.OPERATION_CODE_VALUE);

        }

        sb.append('&');

        sb.append(Server.OPERATION_LOCALE_PARAMETER);
        sb.append('=');
        sb.append(Server.OPERATION_LOCALE_VALUE);

        sb.append('&');

        sb.append(Server.OPERATION_DATA_PARAMETER);
        sb.append('=');
        sb.append(params);

        return sb.toString();

    }

    public static String getParameterStringWithJSON(String operation, NameValuePair[] parameters) {

        if (Server.ALLOW_LOG) Log.e("Entra", "aqui con jsOn");

        //Creamos un objeto de tipo JSON
        JSONObject parametros = new JSONObject();

        try {
            parametros.put("operacion", operation);
        } catch (JSONException e1) {
            if (Server.ALLOW_LOG) e1.printStackTrace();
        }
        if (parameters != null) {
            NameValuePair parameter;
            int size = parameters.length;
            for (int i = 0; i < size; i++) {
                parameter = parameters[i];
                try {
                    //Se almacenan los parámetros en el objeto
                    parametros.put(parameter.getName(), parameter.getValue());
                    //if(Server.ALLOW_LOG) Log.e("Name",parameter.getName());
                    //if(Server.ALLOW_LOG) Log.e("Value",parameter.getValue());
                } catch (JSONException e) {
                    if (Server.ALLOW_LOG) e.printStackTrace();
                }
            }
        }

        String params = parametros.toString().replaceAll("\\\\", "");
        //params = encode(params);

        StringBuffer sb = new StringBuffer();

        sb.append(Server.OPERATION_CODE_PARAMETER);
        sb.append('=');
        // if(operation.equals(Server.OPERATION_CODES[Server.OP_SOLICITAR_ALERTAS])){
        //sb.append(Server.JSON_OPERATION_SOLICITAR_ALERTAS_CODE_VALUE);
        //}else{
        if (Server.OPERATION_CODES[Server.OP_CONSULTA_TDC].equals(operation)) {
            sb.append(Server.OPERATION_CODE_VALUE_BMOVIL_MEJORADO);

        } else if (Server.OPERATION_CODES[Server.OP_SINC_EXP_TOKEN].equals(operation)) {
            sb.append(Server.OPERATION_CODE_VALUE_BMOVIL_MEJORADO);

        } else if (Server.OPERATION_CODES[Server.OP_RETIRO_SIN_TAR].equals(operation)) {
            sb.append(Server.OPERATION_CODE_VALUE_BMOVIL_MEJORADO);
        } else if (Server.OPERATION_CODES[Server.CONSULTA_SIN_TARJETA].equals(operation)) {
            sb.append(Server.OPERATION_CODE_VALUE_BMOVIL_MEJORADO);
        } else if (Server.OPERATION_CODES[Server.OP_RETIRO_SIN_TAR_12_DIGITOS].equals(operation)) {
            sb.append(Server.OPERATION_CODE_VALUE_BMOVIL_MEJORADO);
        } else if ((operation.equals(Server.OPERATION_CODES[Server.OP_SOLICITAR_ALERTAS]))
                || (operation.equals(Server.OPERATION_CODES[Server.CAMBIA_PERFIL]))) {
            sb.append(Server.JSON_OPERATION_RECORTADO_CODE_VALUE);
        } else if (operation.equals(Server.OPERATION_CODES[Server.OP_CONSULTA_INTERBANCARIOS])) {
            sb.append(Server.JSON_OPERATION_CONSULTA_INTERBANCARIOS);
        } else if (operation.equals(Server.OPERATION_CODES[Server.FAVORITE_PAYMENT_OPERATION_BBVA])) {
            sb.append(Server.JSON_OPERATION_CONSULTA_INTERBANCARIOS);
        } else if (operation.equals(Server.OPERATION_CODES[Server.OP_CONSULTA_TDC])) {
            sb.append(Server.JSON_OPERATION_MEJORAS_CODE_VALUE);
        } else {
            //One Click
            if (Server.isjsonvalueCode.name() == "ONECLICK")
                sb.append(Server.JSON_OPERATION_CODE_VALUE_ONECLICK);
            else if (Server.isjsonvalueCode.name() == "CONSUMO")
                sb.append(Server.JSON_OPERATION_CODE_VALUE_CONSUMO);
            else if (Server.isjsonvalueCode.name() == "DEPOSITOS") {
                sb.append(Server.JSON_OPERATION_CODE_VALUE_DEPOSITOS);
            } else
                sb.append(Server.JSON_OPERATION_CODE_VALUE);
            //Termina One click
            //sb.append(Server.JSON_OPERATION_CODE_VALUE);
        }
        sb.append('&');

        sb.append(Server.OPERATION_LOCALE_PARAMETER);
        sb.append('=');
        sb.append(Server.OPERATION_LOCALE_VALUE);

        sb.append('&');

        sb.append(Server.OPERATION_DATA_PARAMETER);
        sb.append('=');
        sb.append(params);

        //One Click
        Server.isjsonvalueCode = isJsonValueCode.NONE;
        return sb.toString();

    }

    /**
     * URL encoding.
     *
     * @param s the input string
     * @return the encodec string
     */
    public static String encode(String s) {
        String encoded = s;
        if (s != null) {
            try {
                StringBuffer sb = new StringBuffer(s.length() * 3);
                char c;
                int size = s.length();
                for (int i = 0; i < size; i++) {
                    c = s.charAt(i);
                    if (c == '&') {
                        sb.append("%26");
                    } else if (c == ' ') {
                        sb.append('+');
                    } else if (c == '/') {
                        sb.append("%2F");
                    } else if ((c >= ',' && c <= ';')
                            || (c >= 'A' && c <= 'Z')
                            || (c >= 'a' && c <= 'z')
                            || c == '_' || c == '?' || c == '*') {
                        sb.append(c);
                    } else {
                        sb.append('%');
                        if (c > 15) {
                            sb.append(Integer.toHexString((int) c));
                        } else {
                            sb.append("0" + Integer.toHexString((int) c));
                        }
                    }
                }
                encoded = sb.toString();
            } catch (Exception ex) {
                encoded = null;
            }
        }
        return (encoded);
    }

    /**
     * Read and parse the response.
     *
     * @param response the response from the server
     * @param handler  instance capable of parsing the result
     * @throws IOException
     * @throws ParsingException
     */
    private static void parse(String response, ParsingHandler handler) throws IOException, ParsingException {

        Reader reader = null;
        try {
            reader = new StringReader(response);
            Parser parser = new Parser(reader);
            handler.process(parser);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Throwable ignored) {
                }
            }
        }
    }

    private static void parseJSON(String response, ParsingHandler handler) throws IOException, ParsingException {

        Reader reader = null;
        try {
//            reader = new StringReader(response);
            ParserJSON parser = new ParserJSON(response);
            handler.process(parser);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Throwable ignored) {
                }
            }
        }
    }

    /**
     * Creates a new instance for the client.
     */
    private void createClient() {
        if (client == null) {
            client = new DefaultHttpClient();
            HttpParams params = new BasicHttpParams();

            // Establece el timeout de la conexión y del socket a 30 segundos
            HttpConnectionParams.setConnectionTimeout(params, 30 * 1000);
            HttpConnectionParams.setSoTimeout(params, 30 * 1000);
            client.setParams(params);
        }
    }

    /**
     * Invoke a network operation.
     *
     * @param operation    operation code
     * @param parameters   parameters
     * @param handler      instance capable of parsing the result
     * @param clearCookies true if session cookies must be deleted, false if not
     * @throws IOException
     * @throws ParsingException
     */
   /* public void invokeOperation(String operation, NameValuePair[] parameters, ParsingHandler handler, boolean clearCookies)
            throws IOException,	ParsingException, URISyntaxException {
        String url = getUrl(operation);
        String params = getParameterString(operation, parameters);
        if(Server.ALLOW_LOG) Log.e("REQUEST", "params: " + params);
        char separator = ((url.indexOf('?') == -1) ? '?' : '&');
        url = new StringBuffer(url).append(separator).append(params).toString();
        if(Server.ALLOW_LOG) Log.e("REQUEST", "url: " + url);
        retrieve(url, null, null, handler,false);

    }*/
    public void invokeOperation(String operation, NameValuePair[] parameters, ParsingHandler handler, boolean clearCookies, boolean isJSON)
            throws IOException, ParsingException, URISyntaxException {
        String url = getUrl(operation);
        String params = null;
        //if(Server.ALLOW_LOG) System.out.println("*******Tipo de Operación: "+operation);
        if (isJSON) {
            params = getParameterStringWithJSON(operation, parameters);
            if (Server.ALLOW_LOG) Log.e("REQUEST", "params: " + params);
        } else {
            params = getParameterString(operation, parameters);
            if (Server.ALLOW_LOG) Log.e("REQUEST", "params: " + params);
        }

        //char separator = ((url.indexOf('?') == -1) ? '?' : '&');
        //url = new StringBuffer(url).append(separator).append(params).toString();
        if (Server.ALLOW_LOG) Log.e("REQUEST", "url: " + url + "?" + params);
        // el método retrive es quien hace el trabajo post, antes se invocaba de la siguiente manera:
        //retrieve(url, null, null, handler,isJSON);
        //y los parametros se concatenaban con la url en las dos lineas comentadas en la linea 164 y 165.
        //retrieve(url, null, null, handler,isJSON);
        retrieve(url, null, null, handler, isJSON, params);

    }

    /**
     * Retrieve the response from the server and store it in the ParsingHandler.
     *
     * @param url     the URL to invoke
     * @param method  the HTTP method
     * @param type    the POST body content type
     * @param handler instance capable of parsing the result
     * @throws IOException
     * @throws ParsingException
     */
    @SuppressWarnings({"rawtypes", "unchecked"})
    private void retrieve(String url, String type, byte[] body,
                          ParsingHandler handler, boolean isJSON, String params) throws IOException, ParsingException, URISyntaxException {

        // Realiza la conexión http y manda la cadena de operación
        BufferedReader in = null;
        String result = null;

        try {

            // HttpGet request = new HttpGet();
            HttpPost request = new HttpPost();

            request.setURI(new URI(url));
            // Si la siguiente línea de código esté comentada entonces la aplicación se comunica por la vía 1 .173 cuando esté en producci�n,
            // Si la línea de código no esté comentada entonces se comunica por la vía 2 .174
            request.addHeader("Lan", "android");

            //New line

            String data[] = params.split("&");

            String firstData[] = data[0].split("=");
            String secondData[] = data[1].split("=");
            String thirdData[] = data[2].split("=");

			/*HttpParams postParams = new BasicHttpParams();

			postParams.setParameter(firstData[0], firstData[1]);
			postParams.setParameter(secondData[0], secondData[1]);
			postParams.setParameter(thirdData[0], thirdData[1]);
			request.setParams(postParams);
			*/
            //if(Server.ALLOW_LOG) Log.d("firstKey",firstData[0]);
            //if(Server.ALLOW_LOG) Log.d("firstValue",firstData[1]);

            //if(Server.ALLOW_LOG) Log.d("secondKey",secondData[0]);
            //if(Server.ALLOW_LOG) Log.d("secondValue",secondData[1]);

            //if(Server.ALLOW_LOG) Log.d("thirdKey",thirdData[0]);
            //if(Server.ALLOW_LOG) Log.d("thirdValue",thirdData[1]);

            List postParams = new ArrayList();
            postParams.add(new BasicNameValuePair(firstData[0], firstData[1]));
            postParams.add(new BasicNameValuePair(secondData[0], secondData[1]));
            postParams.add(new BasicNameValuePair(thirdData[0], thirdData[1]));
            request.setEntity(new UrlEncodedFormEntity(postParams));


            HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, "OK");


            client.getConnectionManager().closeExpiredConnections();

            if (Server.ALLOW_LOG) {
                Utilities.getHeadersAsString(request);
            }
            response = client.execute(request);

            CookieStore cookieStore = client.getCookieStore();
            for (Cookie cookies : cookieStore.getCookies()) {
                if (Server.ALLOW_LOG)
                    Log.e("COOKIES", cookies.getName() + " " + cookies.getValue());
            }

            in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "ISO-8859-1"));
            StringBuffer sb = new StringBuffer("");
            String line = "";
            String NL = System.getProperty("line.separator");
            while ((line = in.readLine()) != null) {
                sb.append(line + NL);
            }

            in.close();
            result = sb.toString(); //respuesta


            //TODO niko fix
//			Integer ind = result.indexOf("{");
//			result = result.substring(ind);

            if (Server.ALLOW_LOG) Log.d("debugRespuesta", result);
            //result = result.replaceAll("\\<.*?>", "");
//			String res2 = result.substring(ind);
//
            if (Server.ALLOW_LOG) Log.d("debugRespuestaParseada", result.replaceAll("\\<.*?>", ""));

        } catch (Exception e) {
            if (Server.ALLOW_LOG)
                Log.e(this.getClass().getName(), "Ha ocurrido una excepción en la respuesta.", e);
            if (Server.ALLOW_LOG) e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    if (Server.ALLOW_LOG) e.printStackTrace();
                }
            }
        }

        if (result.length() > Tools.TAM_FRAGMENTO_TEXTO) {
            Tools.trazaTexto("RESULT", result);
        } else {
            if (Server.ALLOW_LOG) Log.e("RESULT", "result: " + result);
        }

        if (isJSON) {
            parseJSON(result, handler);
        } else {
            parse(result, handler);
        }

    }

    /**
     * Gets and creates an image from the server,
     * located in a specific URL
     *
     * @param url the server image
     * @throws IOException
     */
    public Bitmap getImageFromServer(String url) throws IOException {

        HttpPost mHttppost = new HttpPost(url);
        HttpResponse mHttpResponse = client.execute(mHttppost);
        Bitmap image = null;

        if (mHttpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            HttpEntity entity = mHttpResponse.getEntity();
            if (entity != null) {
                byte[] buffer = EntityUtils.toByteArray(entity);
                image = BitmapFactory.decodeByteArray(buffer, 0, buffer.length);
            }
        }

        return image;
    }

    /**
     * Closes all current connections
     */
    public synchronized void abort() {
        synchronized (client) {
            client.getConnectionManager().shutdown();
            createClient();
        }
    }

    /**
     * Simula invocación de operación de Red.
     *
     * @param operation  operation code
     * @param parameters parameters
     * @param handler    instance capable of parsing the result
     * @param response   respuesta simulada
     * @param isJSON     define si la respuesta debe ocupar un parser de JSON.
     * @throws IOException
     * @throws ParsingException
     * @throws URISyntaxException
     */
    public void simulateOperation(String operation, NameValuePair[] parameters,
                                  ParsingHandler handler, String response, boolean isJSON) throws IOException,
            ParsingException, URISyntaxException {

        String url = getUrl(operation);
        String params;
        if (isJSON)
            params = getParameterStringWithJSON(operation, parameters);
        else
            params = getParameterString(operation, parameters);
        char separator = ((url.indexOf('?') == -1) ? '?' : '&');
        url = new StringBuffer(url).append(separator).append(params).toString();
        if (Server.ALLOW_LOG) Log.e("REQUEST", "url: " + url);

        if (response.length() > Tools.TAM_FRAGMENTO_TEXTO) {
            Tools.trazaTexto("RESULT", response);
        } else {
            if (Server.ALLOW_LOG) Log.e("RESULT", "result: " + response);
        }

        if (isJSON)
            parseJSON(response, handler);
        else
            parse(response, handler);
    }


    /**
     * Simula invocaión de operación de Red.
     *
     * @param operation  operation code
     * @param parameters parameters
     * @param handler    instance capable of parsing the result
     * @param response   respuesta simulada
     * @throws IOException
     * @throws ParsingException
     * @throws URISyntaxException
     */
    public void simulateOperation(String operation, NameValuePair[] parameters,
                                  ParsingHandler handler, String response) throws IOException,
            ParsingException, URISyntaxException {
        simulateOperation(operation, parameters, handler, response, false);
    }

}
