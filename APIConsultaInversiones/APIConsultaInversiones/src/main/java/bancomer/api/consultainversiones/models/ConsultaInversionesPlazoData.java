package bancomer.api.consultainversiones.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

/**
 * Created by miguelangel.aguilarc on 09/11/2015.
 */
public class ConsultaInversionesPlazoData implements ParsingHandler {

    private static final long serialVersionUID = 4801921121098661710L;

    private ArrayList<InversionesPlazo> listaInversiones;

    private String estado;

    private String numeroCuenta;

    private String numeroInversion;

    private String saldo;

    private String fechaVencimiento;

    private String tasa;

    private String gat;

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub
    }


    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        try {
            listaInversiones = new ArrayList<InversionesPlazo>();

            JSONArray datos = parser.parseNextValueWithArray("listaInversiones");

            for (int i = 0; i < datos.length(); i++) {
                InversionesPlazo invPla = new InversionesPlazo();
                JSONObject jsonDatos= datos.getJSONObject(i);
                invPla.setNumeroInversion(jsonDatos.getString("numeroInversion"));
                invPla.setSaldo(jsonDatos.getString("saldo"));
                invPla.setFechaVencimiento(jsonDatos.getString("fechaVencimiento"));
                invPla.setTasa(jsonDatos.getString("tasa"));
                invPla.setGat(jsonDatos.getString("gat"));
                listaInversiones.add(invPla);
            }

        } catch (Exception e) {
                if (Server.ALLOW_LOG)
                    Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
        }
    }


    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getNumeroCuenta() {
        return numeroCuenta;
    }

    public void setNumeroCuenta(String numeroCuenta) {
        this.numeroCuenta = numeroCuenta;
    }

    public String getNumeroInversion() {
        return numeroInversion;
    }

    public void setNumeroInversion(String numeroInversion) {
        this.numeroInversion = numeroInversion;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getTasa() {
        return tasa;
    }

    public void setTasa(String tasa) {
        this.tasa = tasa;
    }

    public String getGat() {
        return gat;
    }

    public void setGat(String gat) {
        this.gat = gat;
    }

    public ArrayList<InversionesPlazo> getListaInversiones() {
        return listaInversiones;
    }

    public void setListaInversiones(ArrayList<InversionesPlazo> listaInversiones) {
        this.listaInversiones = listaInversiones;
    }
}



