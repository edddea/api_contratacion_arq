/**
 *
 */
package bancomer.api.consultainversiones.commons;

import bancomer.api.consultainversiones.implementations.SuiteAppConsultaInversionesApi;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

/**
 * Implementation of the App power manager for api7 or greater.
 */
public final class EclairPowerManager extends AbstractSuitePowerManager {
    /* (non-Javadoc)
     * @see suitebancomer.aplicaciones.bmovil.classes.common.AbstractSuitePowerManager#isScreenOn()
     */
    @SuppressLint("NewApi")
    @Override
    public boolean isScreenOn() {
        try {
            PowerManager pm = (PowerManager) SuiteAppConsultaInversionesApi.appContext.getSystemService(Context.POWER_SERVICE);
            return pm.isScreenOn();
        } catch (Exception ex) {
            if (Server.ALLOW_LOG)
                Log.wtf("EclairPowerManager", "Error while getting the state of the screen.", ex);
            return false;
        }
    }
}
