package bancomer.api.consultainversiones.implementations;

import android.annotation.SuppressLint;
import android.util.Log;

import java.util.Hashtable;

import bancomer.api.consultainversiones.R;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

@SuppressLint("HandlerLeak")
public class BmovilApp extends BaseSubapplication {

    /**
     * Message for the handler, indicates that session must be expired.
     */
    private static final int LOGOUT_MESSAGE = 0;

    public static int STEP_TOTAL = 0;
    public static int STEP_ACTUAL = 0;


    //////////////////////////////////////////////////////////////
    //					Methods									//
    //////////////////////////////////////////////////////////////

    /**
     * Constructor básico, se inicializa aqui debido a que no puede extender de un objeto Aplicación, ya que no es posible
     * que exista mas de uno
     */
    public BmovilApp(SuiteAppConsultaInversionesApi suiteApp) {
        super(suiteApp);
        viewsController = new BmovilViewsController(this);
    }

    public BmovilViewsController getBmovilViewsController() {
        return (BmovilViewsController) viewsController;
    }

    public void reiniciaAplicacion() {
//		if(!(((BmovilViewsController)viewsController).getCurrentViewControllerApp() instanceof MenuSuiteViewController))
//		{
//			((BmovilViewsController)viewsController).cierraViewsController();
//			viewsController = new BmovilViewsController(this);
//		}

    }

    @Override
    public void cierraAplicacion() {
        super.cierraAplicacion();
    }

    //////////////////////////////////////////////////////////////////////////////
    //                Network operations										//
    //////////////////////////////////////////////////////////////////////////////

    /**
     * Invoke a network operation. It shows a popup indicating progress with
     * custom texts.
     *
     * @param operationId        network operation identifier. See Server class.
     * @param params             Hashtable with the parameters passed to the Server. See Server
     *                           class for parameter names.
     * @param caller             the BaseScreen instance (that is, the screen), which requests the
     *                           network operation. Must be null if the caller is not a screen.
     * @param progressLabel      text with the title of the progress popup
     * @param progressMessage    text with the content of the progress popup
     * @param callerHandlesError boolean flag indicating wheter the caller is capable
     *                           of handling an error message or the network flux should handle in the default
     *                           implementation
     */
    @Override
    protected void invokeNetworkOperation(int operationId,
                                          final Hashtable<String, ?> params,
                                          final boolean isJson,
                                          final ParsingHandler handler,
                                          final BaseViewControllerCommons caller,
                                          String progressLabel,
                                          String progressMessage,
                                          final boolean callerHandlesError) {
        //we stop the timer during server calls
        super.invokeNetworkOperation(operationId, params, isJson, handler, caller, progressLabel, progressMessage, callerHandlesError);
    }

    /**
     * Analyze the network response obtained from the server
     *
     * @param operationId        network identifier. See Server class.
     * @param response           the ServerResponse instance built from the server response. It
     *                           contains the type of result (success, failure), and the real content for
     *                           the application business.
     * @param operationId        network operation identifier. See Server class.
     * @param response           the ServerResponse instance returned from the server.
     * @param throwable          the throwable received.
     * @param callerHandlesError boolean flag indicating wheter the caller is capable
     *                           of handling an error message or the network flux should handle in the default
     *                           implementation
     */
    @Override
    protected void analyzeNetworkResponse(Integer operationId,
                                          ServerResponse response,
                                          Throwable throwable,
                                          final BaseViewControllerCommons caller,
                                          boolean callerHandlesError) {

        super.analyzeNetworkResponse(operationId, response, throwable, caller, callerHandlesError);

        if (SuiteAppConsultaInversionesApi.getCallBackSession() != null) {
            SuiteAppConsultaInversionesApi.getCallBackSession().userInteraction();
        } else if (SuiteAppConsultaInversionesApi.getCallBackAPI() != null) {
            SuiteAppConsultaInversionesApi.getCallBackAPI().userInteraction();
        }
//		}
    }

    public void closeBmovilAppSession(BaseViewControllerCommons caller) {
        applicationLogged = false;
        Session session = Session.getInstance(suiteApp.appContext);
        String username = session.getUsername();
        String ium = session.getIum();
        String client = session.getClientNumber();

        // close session
        System.gc();
        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put("NT", username);
        params.put(ServerConstants.IUM_ETIQUETA, ium);
        params.put("TE", client);

        if (viewsController != null) {
            caller = viewsController.getCurrentViewController();
        }

        //JAIG
        invokeNetworkOperation(Server.CLOSE_SESSION_OPERATION, params, false, null, caller,
                suiteApp.appContext.getString(R.string.alert_closeSession),
                suiteApp.appContext.getString(R.string.alert_thank_you), true);
        //BaseViewController caller = null;
        //if (viewsController != null) {
        //caller = viewsController.getCurrentViewController();
        //}

        if (Server.ALLOW_LOG) Log.e("CerrarSesion", "La sesion fue cerrada satisfactoriamente.");
    }

    public int getApplicationStatus() {
        return Session.getInstance(SuiteAppConsultaInversionesApi.appContext).getPendingStatus();
    }

    /////////////////////////////////////////////////////////////////////////////
    //                Local session management                                 //
    /////////////////////////////////////////////////////////////////////////////

    /**
     * Records a user activity event (keystroke), in order to
     * check session validity
     */
    public void userActivityEvent() {

    }

}
