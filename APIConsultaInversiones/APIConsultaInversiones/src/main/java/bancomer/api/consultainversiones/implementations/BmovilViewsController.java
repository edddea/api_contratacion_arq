package bancomer.api.consultainversiones.implementations;

import android.util.Log;

import java.util.ArrayList;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public class BmovilViewsController extends BaseViewsController {

    public ArrayList<String> estados = new ArrayList<String>();
    private BmovilApp bmovilApp;

    public BmovilViewsController(BmovilApp bmovilApp) {
        super();
        this.bmovilApp = bmovilApp;
    }

    public void cierraViewsController() {
        bmovilApp = null;
        clearDelegateHashMap();
        super.cierraViewsController();
    }

    public void cerrarSesionBackground() {
        SuiteAppConsultaInversionesApi.getInstance().getBmovilApplication()
                .setApplicationInBackground(true);
    }

    @Override
    public void showMenuInicial() {
        showMenuPrincipal();
    }

    public void showMenuPrincipal() {
        showMenuPrincipal(false);
    }

    public void showMenuPrincipal(boolean inverted) {
        SuiteAppConsultaInversionesApi.getInstance().getBmovilApplication()
                .setApplicationLogged(true);
        if (SuiteAppConsultaInversionesApi.getCallBackSession() != null) {
            if (ServerCommons.ALLOW_LOG) {
                Log.d(getClass().getSimpleName(), "Se ejecuta returnMenuPrincipal desde CallBackSession");
            }
            SuiteAppConsultaInversionesApi.getCallBackSession().returnMenuPrincipal();
        } else if (SuiteAppConsultaInversionesApi.getCallBackAPI() != null) {
            if (ServerCommons.ALLOW_LOG) {
                Log.d(getClass().getSimpleName(), "Se ejecuta returnMenuPrincipal desde CallBackBConnect");
            }
            SuiteAppConsultaInversionesApi.getCallBackAPI().returnMenuPrincipal();
        }
    }

    public BmovilApp getBmovilApp() {
        return bmovilApp;
    }

    public void setBmovilApp(BmovilApp bmovilApp) {
        this.bmovilApp = bmovilApp;
    }


    @Override
    public void onUserInteraction() {

        if (bmovilApp != null) {

        }
        super.onUserInteraction();
    }

    @Override
    public boolean consumeAccionesDeReinicio() {
        return false;
    }

    @Override
    public boolean consumeAccionesDePausa() {
        return false;
    }

    @Override
    public boolean consumeAccionesDeAlto() {
        if (!SuiteAppConsultaInversionesApi.getInstance().getBmovilApplication().isChangingActivity()
                && currentViewControllerApp != null) {
            currentViewControllerApp.hideSoftKeyboard();
        }
        SuiteAppConsultaInversionesApi.getInstance().getBmovilApplication()
                .setChangingActivity(false);
        return true;
    }

    @Override
    public void removeDelegateFromHashMap(long key) {
        super.removeDelegateFromHashMap(key);
    }

    public void touchMenu() {
        String eliminado = "";
        if (estados.size() == 2) {
            eliminado = estados.remove(1);
        } else {
            int tam = estados.size();
            for (int i = 1; i < tam; i++) {
                eliminado = estados.remove(1);
            }
        }
    }

    public void touchAtras() {
        int ultimo = estados.size() - 1;

        String eliminado;
        if (ultimo >= 0) {
            String ult = estados.get(ultimo);
            if (ult == "reactivacion" || ult == "activacion" || ult == "menu token" || ult == "contratacion datos" || ult == "activacion datos") {
                estados.clear();
            } else {
                eliminado = estados.remove(ultimo);
            }
        }
    }

    public void pantallaActivada() {
        setActivityChanging(true);
    }
}