/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package bancomer.api.consultainversiones.implementations;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

import java.lang.reflect.Method;

import bancomer.api.consultainversiones.R;
import bancomer.api.consultainversiones.commons.GuiTools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ControlEventos;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;

/**
 * BaseScreen is the base class for all the screens in the MBanking application
 * It provides basic, common functionality to be included in the business logic
 * so that the application can manage screens in a uniform, consistent way.
 *
 * @author Stefanini IT Solutions
 */
public abstract class BaseViewController extends BaseViewControllerCommons {
    /**
     * To use as parameter in aplicaEstilo method in order to show the header
     */
    public final static int SHOW_HEADER = 2;
    /**
     * To use as parameter in aplicaEstilo method in order to show the title
     */
    public final static int SHOW_TITLE = 4;
    //
    /**
     * Current view edit fields' references. Each view should implement this variable.
     */
    protected static EditText[] sFields;
    // Repicacion API Mtto
    private static Method methodOverridePendingTransition;

    // #region Variables.
    /**
     * Referencia a la clase misma
     */
    private static BaseViewController me;

    static {
        Method[] actMethods = Activity.class.getMethods();
        int methodSize = actMethods.length;
        for (int i = 0; i < methodSize; i++) {
            if (actMethods[i].getName().equals("overridePendingTransition")) {
                methodOverridePendingTransition = actMethods[i];
                break;
            }
        }
    }

    /**
     * indica si es un flujo de estatus desactivado
     */
    public boolean statusdesactivado;
    /**
     * Determines if an alert pop-up is being shown
     */
    protected boolean mShowingDialogPopup = false;
    /**
     * A progress dialog for long waiting processes.
     */
    protected ProgressDialog mProgressDialog;
    /**
     * Referencias al controlador de vistas padre de esta vista
     */
    protected BaseViewsControllerCommons parentViewsController;
    protected Activity activityContext;
    // #endregion
    //private boolean habilitado = true;
    protected boolean habilitado = true;
    //AMZ
    private BmovilViewsController bm;
    /**
     * The alert error or information dialog.
     */
    private AlertDialog mAlertDialog;
    /**
     * Referencia al layout del cabecero
     */
    private LinearLayout mHeaderLayout;
    /**
     * Referencia al layout del t�tulo
     */
    private LinearLayout mTitleLayout;
    /**
     * Referencia al separador del t�tulo
     */
    private ImageView mTitleDivider;
    /**
     * Layout where each view will be drawn.
     */
    private ViewGroup mBodyLayout;
    /**
     * Par�metros con los que se construir� la pantalla b�sica
     */
    private int activityParameters;
    // #endregion
    /**
     * Referencia al delegate de esta vista
     */
    private BaseDelegate delegate;
    /**
     * Defines when the last click occurred
     */
    private long touchDownTime;

    /**
     * Constructor por defecto
     */
    public BaseViewController() {
        me = this;
    }

    /**
     * M�todo que devuelve una instancia de la clase
     *
     * @return una instancia de esta clase
     */
    public static BaseViewController getInstance() {
        return me;
    }

    public void setActivityContext(Activity activityContext) {
        this.activityContext = activityContext;
    }

    /**
     * Called when the activity is first created.
     *
     * @param savedInstanceState state
     */
    public void onCreate(Bundle savedInstanceState, int parameters, int layoutID) {
        // Repicacion API Mtto
        try {
            methodOverridePendingTransition.invoke(this, new Object[]{R.anim.screen_enter, R.anim.screen_leave});
        } catch (Exception e) {
            if (ServerCommons.ALLOW_LOG) {
                Log.d("APIMtto", e.toString());
            }
        }
        //

        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        activityParameters = parameters;
        setContentView(getResources().getIdentifier("layout_base_activity_admon", "layout", getPackageName()));

        // references to ui elements
        mHeaderLayout = (LinearLayout) findViewById(SuiteApp.getResourceId("header_layout", "id"));
        mTitleLayout = (LinearLayout) findViewById(SuiteApp.getResourceId("title_layout", "id"));
        mTitleDivider = (ImageView) findViewById(SuiteApp.getResourceId("title_divider", "id"));
        mBodyLayout = (ViewGroup) findViewById(SuiteApp.getResourceId("body_layout", "id"));

        // 	set header visibility
//        mHeaderLayout.setVisibility(((activityParameters&SHOW_HEADER)==SHOW_HEADER)?View.VISIBLE:View.GONE);
        mHeaderLayout.setVisibility(View.VISIBLE);
        //set title visibility, by default is gone until a call to aplicaEstilo is made
        mTitleLayout.setVisibility(View.GONE);
        mTitleDivider.setVisibility(View.GONE);

        // load received layout id in the body
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(layoutID, mBodyLayout, true);

        //AMZ
        bm = SuiteAppConsultaInversionesApi.getInstance().getBmovilApplication().getBmovilViewsController();


        /* ---------------------------------------------------- */
        scaleForCurrentScreen();
    }

    /**
     * Screen is about to be shown.
     */
    @Override
    protected void onStart() {
        super.onStart();
        sFields = null;
    }

    @Override
    protected void onPause() {
        if (!ControlEventos.isScreenOn(this)) {
            if (ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "Se ha bloqueado el telefono");
            }
            finish();
            if (SuiteAppConsultaInversionesApi.getCallBackSession() != null) {
                if (ServerCommons.ALLOW_LOG) {
                    Log.d(getClass().getSimpleName(), "Se ejecuta cierraSesion desde CallBackSession");
                }
                SuiteAppConsultaInversionesApi.getCallBackSession().cierraSesion();
            } else if (SuiteAppConsultaInversionesApi.getCallBackAPI() != null) {
                if (ServerCommons.ALLOW_LOG) {
                    Log.d(getClass().getSimpleName(), "Se ejecuta cierraSesion desde CallBackBConnect");
                }
                SuiteAppConsultaInversionesApi.getCallBackAPI().cierraSesion();
            }
        }
        super.onPause();
    }

    /* (non-Javadoc)
         * @see android.app.Activity#onStop()
         */
    @Override
    protected void onStop() {
        if (ServerCommons.ALLOW_LOG) {
            Log.e(getClass().getSimpleName(), "OnStop");
        }
        if (ControlEventos.isAppIsInBackground(this.getBaseContext())) { //Valida el estado de la app si es change Activity no hace nada
            if (ServerCommons.ALLOW_LOG) {
                Log.d(getClass().getSimpleName(), "La app se fue a Background");
            }
            finish();
            if (SuiteAppConsultaInversionesApi.getCallBackSession() != null) {
                if (ServerCommons.ALLOW_LOG) {
                    Log.d(getClass().getSimpleName(), "Se ejecuta cierraSesionBackground desde CallBackSession");
                }
                SuiteAppConsultaInversionesApi.getCallBackSession().cierraSesionBackground(Boolean.FALSE);
            } else if (SuiteAppConsultaInversionesApi.getCallBackAPI() != null) {
                if (ServerCommons.ALLOW_LOG) {
                    Log.d(getClass().getSimpleName(), "Se ejecuta cierraSesionBackground desde CallBackBConnect");
                }
                SuiteAppConsultaInversionesApi.getCallBackAPI().cierraSesionBackground(Boolean.FALSE);
            }
        }
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return false;
    }

    /**
     * Places the title string and respective icon on screen.
     *
     * @param titleText    the screen title
     * @param iconResource the resource of the screen icon
     */
    public void setTitle(final int titleText, final int iconResource) {
        setTitle(titleText, iconResource, SuiteApp.getResourceId("segundo_azul", "color"));
    }

    /**
     * Places the title string and respective icon on screen with the desired color
     *
     * @param titleText    the screen title
     * @param iconResource the resource of the screen icon
     */
    public void setTitle(final int titleText, final int iconResource, final int colorResource) {

        if ((activityParameters & SHOW_TITLE) == SHOW_TITLE) {
            mTitleLayout.setVisibility(View.VISIBLE);
            ImageView icon = (ImageView) mTitleLayout.findViewById(SuiteApp.getResourceId("title_icon", "id"));
            if (iconResource > 0) {
                icon.setImageDrawable(getResources().getDrawable(iconResource));
            }
            if (titleText > 0) {
                String title = SuiteApp.appContext.getString(titleText);
                TextView titleTextView = (TextView) mTitleLayout.findViewById(SuiteApp.getResourceId("title_text", "id"));
                titleTextView.setTextColor(getResources().getColor(colorResource));
                titleTextView.setText(title);
            }
            mTitleDivider.setVisibility(View.VISIBLE);
        } else {
            mTitleLayout.setVisibility(View.GONE);
            mTitleDivider.setVisibility(View.GONE);
        }
    }

    /**
     * Overrides and cancels android's back button, if the button is visible then it does the same
     * as clicking on the application's back button.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        onUserInteraction();
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                //AMZ
                if (bm != null) {
                    bm.touchAtras();
                }

                goBack();
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    /**
     * This function has to be overridden by child members, it implements
     * the processing for server response.
     *
     * @param operationId
     * @param response
     */
    public void processNetworkResponse(int operationId, ServerResponse response) {
    }

    /**
     * Determines if the user touched or performed some action in the screen.
     * If he or she did, then reset the logout timer.
     */
    @Override
    public void onUserInteraction() {
        if (Server.ALLOW_LOG) {
            Log.i(getClass().getSimpleName(), "User Interacted Administrar");
        }
        if (SuiteAppConsultaInversionesApi.getCallBackSession() != null) {
            if (ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "Envia peticion de reinicio a bmovil desde CallBackSession");
            }
            SuiteAppConsultaInversionesApi.getCallBackSession().userInteraction();
        }
        if (SuiteAppConsultaInversionesApi.getCallBackAPI() != null) {
            if (ServerCommons.ALLOW_LOG) {
                Log.i(getClass().getSimpleName(), "Envia peticion de reinicio a bmovil desde CallBackBconnect");
            }
            SuiteAppConsultaInversionesApi.getCallBackAPI().userInteraction();
        }
        super.onUserInteraction();
    }

    /*
     * Called to process touch screen events.
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touchDownTime = SystemClock.elapsedRealtime();
                break;

            case MotionEvent.ACTION_UP:
                //to avoid drag events
                if (SystemClock.elapsedRealtime() - touchDownTime <= 150) {

                    EditText[] textFields = this.getFields();
                    if (textFields != null && textFields.length > 0) {
                        boolean clickIsOutsideEditTexts = true;

                        for (EditText field : textFields) {
                            if (isPointInsideView(ev.getRawX(), ev.getRawY(), field)) {
                                clickIsOutsideEditTexts = false;
                                break;
                            }
                        }

                        if (clickIsOutsideEditTexts) {
                            this.hideSoftKeyboard();
                        }
                    } else {
                        this.hideSoftKeyboard();
                    }
                }
                break;
        }

        return super.dispatchTouchEvent(ev);
    }

    /**
     * Override this from children to return the view's fields.
     *
     * @return the current screen fields.
     */
    protected EditText[] getFields() {
        return sFields;
    }

    /**
     * Gets the trimmed text of a text field content.
     *
     * @param textField the EditText component to retrieve text
     * @return the textField value without trailing spaces
     */
    protected String getText(EditText textField) {
        if (textField != null) {
            return textField.getText().toString().trim();
        } else {
            return "";
        }
    }

    public float getHeaderHeight() {
        mHeaderLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        mTitleLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        mTitleDivider.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);

        float headerHeight = mHeaderLayout.getMeasuredHeight() + mTitleLayout.getMeasuredHeight() +
                mTitleDivider.getMeasuredHeight();

        return headerHeight;
    }

    public float getBodyHeight() {
        mBodyLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        return mBodyLayout.getMeasuredHeight();
    }

    /**
     * Sets the layout that will fill the empty body layout in base screen
     * template.
     *
     * @param layoutResource the layout resources
     */
    protected final void setBodyLayout(int layoutResource) {
        LayoutInflater inflater = (LayoutInflater)
                this.getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(layoutResource, mBodyLayout);
    }

    /**
     * Hides the soft keyboard if any editbox called it.
     */
    public void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        View focusedView = getCurrentFocus();
        if (imm != null && focusedView != null) {
            imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
        }
    }

    /**
     * This is called whenever the back button is pressed.
     * Normally, this function will return to the previous screen.
     */
    public void goBack() {

        this.hideSoftKeyboard();
        parentViewsController.setActivityChanging(true);
        finish();
        parentViewsController.overrideScreenBackTransition();
    }

    /**
     * Shows an error message if the provided resource exists. The alert
     * view title will be "Error", and it will have one button to accept.
     *
     * @param errorMessage the error message resource to show
     */
    public void showErrorMessage(int errorMessage) {
        if (errorMessage > 0) {
            showErrorMessage(SuiteApp.appContext.getString(errorMessage), null);
        }
    }

    public void showErrorMessage(String errorMessage) {
        showErrorMessage(errorMessage, null);
    }

    /**
     * Places a progress dialog, for long waiting processes.
     *
     * @param strTitle   the dialog title
     * @param strMessage the message to show in the while
     */
    public void muestraIndicadorActividad(String strTitle, String strMessage) {
        if (mProgressDialog != null) {
            ocultaIndicadorActividad();
        }
        mProgressDialog = ProgressDialog.show(SuiteApp.appContext, strTitle,
                strMessage, true);
        // TODO LABM probar
        //mProgressDialog = ProgressDialog.show(SuiteApp.appContext,
        mProgressDialog.setCancelable(false);
    }

    /**
     * Hides the progress dialog.
     */
    public void ocultaIndicadorActividad() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    /**
     * Shows an error message if the message length is greater than 0. The alert
     * view title will be "Error", and it will have one button to accept.
     *
     * @param errorMessage the error message text to show
     */
    public void showErrorMessage(String errorMessage, OnClickListener listener) {

        if (errorMessage.length() > 0) {
            //AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SuiteAppConsultaInversionesApi.appContext);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SuiteApp.appContext);
            alertDialogBuilder.setTitle(R.string.label_error);
            alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
            alertDialogBuilder.setMessage(errorMessage);
            alertDialogBuilder.setPositiveButton(R.string.label_ok, listener);
            alertDialogBuilder.setCancelable(false);
            mAlertDialog = alertDialogBuilder.show();
        }
    }

    /**
     * Determines if given points are inside view
     *
     * @param x    - x coordinate of point
     * @param y    - y coordinate of point
     * @param view - view object to compare
     * @return true if the points are within view bounds, false otherwise
     */
    private boolean isPointInsideView(float x, float y, View view) {
        int location[] = new int[2];
        view.getLocationOnScreen(location);
        int viewX = location[0];
        int viewY = location[1];

        //point is inside view bounds
        if ((x > viewX && x < (viewX + view.getWidth())) &&
                (y > viewY && y < (viewY + view.getHeight()))) {
            return true;
        } else {
            return false;
        }
    }

    public BaseViewsControllerCommons getParentViewsController() {
        return parentViewsController;
    }

    public void setParentViewsController(BaseViewsControllerCommons parentViewsController) {
        this.parentViewsController = parentViewsController;
    }

    public BaseDelegate getDelegate() {
        return delegate;
    }

    public void setDelegate(BaseDelegate delegate) {
        this.delegate = delegate;
    }

    public void hideCurrentDialog() {
        try {
            if (mAlertDialog != null) {
                mAlertDialog.dismiss();
                mAlertDialog = null;
            }
        } catch (Exception ex) {

        }
    }

    public void setCurrentDialog(AlertDialog dialog) {
        this.mAlertDialog = dialog;
    }

    /**
     * Establece la l�gica a seguir para el redimensionamiento de la pantalla.
     */
    private void scaleForCurrentScreen() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(mHeaderLayout);
        guiTools.scale(mTitleLayout);
        guiTools.scale(mTitleLayout.findViewById(SuiteApp.getResourceId("title_icon", "id")));
        guiTools.scale(mTitleLayout.findViewById(SuiteApp.getResourceId("title_text", "id")), true);
        guiTools.scale(mTitleDivider);
        guiTools.scale(mBodyLayout);
    }

    private ScrollView getScrollView() {
        return (ScrollView) findViewById(SuiteApp.getResourceId("body_layout", "id"));
    }

    // #region Information Alert.

    protected void moverScroll() {
        getScrollView().post(new Runnable() {

            @Override
            public void run() {
                getScrollView().fullScroll(ScrollView.FOCUS_UP);
                if (Server.ALLOW_LOG) Log.d(getClass().getName(), "Mover scroll");
            }
        });
    }

    /**
     * Muestra una alerta con el mensaje dado.
     *
     * @param message el mensaje a mostrar
     */
    public void showInformationAlert(int message) {
        if (message > 0) {
            showInformationAlert(SuiteApp.appContext.getString(message));
        }
    }

    /**
     * Shows an information alert with the given message text. No action is performed after this alert is closed.
     *
     * @param message the message text to show
     */
    public void showInformationAlert(String message) {
        if (message.length() > 0) {
            showInformationAlert(message, null);
        }
    }

    /**
     * Shows an information alert with the given message.
     *
     * @param message the message resource to show
     */
    public void showInformationAlert(int message, OnClickListener listener) {
        if (message > 0) {
            showInformationAlert(SuiteApp.appContext.getString(message), listener);
        }
    }

    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     *
     * @param message  the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(String message, OnClickListener listener) {
        showInformationAlert(SuiteApp.appContext.getString(R.string.label_information), message, listener);
    }

    /**
     * Shows an alert dialog.
     *
     * @param title    The alert title resource id.
     * @param message  The alert message resource id.
     * @param listener The listener for the "Ok" button.
     */
    public void showInformationAlert(int title, int message, OnClickListener listener) {
        if (message > 0) {
            showInformationAlert(SuiteApp.appContext.getString(title), SuiteApp.appContext.getString(message), listener);
        }
    }

    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     *
     * @param title    The alert title resource id.
     * @param message  the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(String title, String message, OnClickListener listener) {
        showInformationAlert(title, message, SuiteApp.appContext.getString(R.string.label_ok), listener);
    }

    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     *
     * @param title     The alert title resource id.
     * @param //message the message text to show
     * @param listener  onClickListener to perform action on close
     */
    public void showInformationAlertEspecial(String title, String errorCode, String descripcionError, OnClickListener listener) {
        showInformationAlertEspecial(title, errorCode, descripcionError, SuiteApp.appContext.getString(R.string.label_ok), listener);
    }

    /**
     * Shows an alert dialog.
     *
     * @param title    The alert title resource id.
     * @param message  The alert message resource id.
     * @param okText   the ok text
     * @param listener The listener for the "Ok" button.
     */
    public void showInformationAlert(int title, int message, int okText, OnClickListener listener) {
        if (message > 0) {
            showInformationAlert(SuiteApp.appContext.getString(title), SuiteApp.appContext.getString(message), SuiteApp.appContext.getString(okText), listener);
        }
    }

    /**
     * Shows an alert dialog.
     *
     * @param title    The alert title.
     * @param message  The alert message.
     * @param okText   the ok text
     * @param listener The listener for the "Ok" button.
     */
    public void showInformationAlert(String title, String message, String okText, OnClickListener listener) {
        if (!habilitado)
            return;
        habilitado = false;
        if (message.length() > 0) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
            alertDialogBuilder.setMessage(message);
            if (null == listener) {
                listener = new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                };
            }

            alertDialogBuilder.setPositiveButton(okText, listener);
            alertDialogBuilder.setCancelable(false);
            mAlertDialog = alertDialogBuilder.create();
            mAlertDialog.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    habilitado = true;
                }
            });
            mAlertDialog.show();
        }
    }

    /**
     * Shows an alert dialog.
     *
     * @param title     The alert title.
     * @param //message The alert message.
     * @param okText    the ok text
     * @param listener  The listener for the "Ok" button.
     */
    public void showInformationAlertEspecial(String title, String errorCode, String descripcionError, String okText, OnClickListener listener) {
        if (!habilitado)
            return;
        habilitado = false;
        if (descripcionError.length() > 0) {

            AlertDialog.Builder builder = new AlertDialog.Builder(SuiteApp.appContext);
            //AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //AlertDialog.Builder builder = new AlertDialog.Builder(SuiteAppConsultaInversionesApi.appContext);
            builder.setTitle(title);
            builder.setIcon(R.drawable.anicalertaaviso);
            LayoutInflater inflater = ((Activity) SuiteApp.appContext).getLayoutInflater();
            View vista = inflater.inflate(SuiteApp.getResourceId("layout_alert_codigo_error_admon", "layout"), null);
                /*builder.setView(vista)
                   .setPositiveButton(okText, new DialogInterface.OnClickListener() {
	    	           public void onClick(DialogInterface dialog, int id) {
	    	                  dialog.dismiss();
	    	           }
	    	    });*/
            if (null == listener) {
                listener = new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                };
            }
            builder.setView(vista)
                    .setPositiveButton(okText, listener);

            TextView text1 = (TextView) vista.findViewById(SuiteApp.getResourceId("descripcionError", "id"));
            TextView text2 = (TextView) vista.findViewById(SuiteApp
                    .getResourceId("codigoError", "id"));
            text1.setText(descripcionError);
            text2.setText(errorCode);

            builder.setCancelable(false);

            mAlertDialog = builder.create();

            mAlertDialog.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    habilitado = true;
                }
            });
            mAlertDialog.show();

        }
    }

    public void showYesNoAlert(String message, OnClickListener positiveListener) {
        showYesNoAlert(SuiteApp.appContext.getString(R.string.label_information),
                message,
                SuiteApp.appContext.getString(R.string.common_alert_yesno_positive_button),
                SuiteApp.appContext.getString(R.string.common_alert_yesno_negative_button),
                positiveListener,
                null);
    }

    public void showYesNoAlertEspecialTitulo(String errorCode, String descripcionError, OnClickListener positiveListener) {
        showYesNoAlertEspecial(SuiteApp.appContext.getString(R.string.label_information),
                errorCode,
                descripcionError,
                SuiteApp.appContext.getString(R.string.common_alert_yesno_positive_button),
                SuiteApp.appContext.getString(R.string.common_alert_yesno_negative_button),
                positiveListener,
                null);
    }

    public void showYesNoAlert(int message, OnClickListener positiveListener) {
        showYesNoAlert(R.string.label_information,
                message,
                R.string.common_alert_yesno_positive_button,
                R.string.common_alert_yesno_negative_button,
                positiveListener,
                null);
    }

    public void showYesNoAlert(int message, OnClickListener positiveListener, OnClickListener negativeListener) {
        showYesNoAlert(R.string.label_information,
                message,
                R.string.common_alert_yesno_positive_button,
                R.string.common_alert_yesno_negative_button,
                positiveListener,
                negativeListener);
    }

    public void showYesNoAlert(String title, String message, OnClickListener positiveListener) {
        showYesNoAlert(title,
                message,
                SuiteApp.appContext.getString(R.string.common_alert_yesno_positive_button),
                SuiteApp.appContext.getString(R.string.common_alert_yesno_negative_button),
                positiveListener,
                null);
    }

    public void showYesNoAlert(String message, OnClickListener positiveListener, OnClickListener negativeListener) {
        showYesNoAlert("",
                message,
                SuiteApp.appContext.getString(R.string.common_alert_yesno_positive_button),
                SuiteApp.appContext.getString(R.string.common_alert_yesno_negative_button),
                positiveListener,
                negativeListener);
    }

    public void showYesNoAlertEspecialTitulo(String errorCode, String descripcionError, OnClickListener positiveListener, OnClickListener negativeListener) {
        showYesNoAlertEspecial(SuiteApp.appContext.getString(R.string.label_information),
                errorCode,
                descripcionError,
                SuiteApp.appContext.getString(R.string.common_alert_yesno_positive_button),
                SuiteApp.appContext.getString(R.string.common_alert_yesno_negative_button),
                positiveListener,
                negativeListener);
    }

    public void showYesNoAlert(int title, int message, OnClickListener positiveListener) {
        showYesNoAlert(title,
                message,
                R.string.common_alert_yesno_positive_button,
                R.string.common_alert_yesno_negative_button,
                positiveListener,
                null);
    }

    public void showYesNoAlert(String title, String message, String okText, OnClickListener positiveListener) {
        showYesNoAlert(title, message, okText, SuiteApp.appContext.getString(R.string.common_alert_yesno_negative_button), positiveListener, null);
    }

    public void showYesNoAlert(int title, int message, int okText, OnClickListener positiveListener) {
        showYesNoAlert(title, message, okText, R.string.common_alert_yesno_negative_button, positiveListener, null);
    }

    public void showYesNoAlert(String title, String message, String okText, String calcelText, OnClickListener positiveListener) {
        showYesNoAlert(title, message, okText, calcelText, positiveListener, null);
    }

    public void showYesNoAlert(int title, int message, int okText, int calcelText, OnClickListener positiveListener) {
        showYesNoAlert(title, message, okText, calcelText, positiveListener, null);
    }

    public void showYesNoAlert(int title,
                               int message,
                               int okText,
                               int calcelText,
                               OnClickListener positiveListener,
                               OnClickListener negativeListener) {
        showYesNoAlert(SuiteApp.appContext.getString(title), SuiteApp.appContext.getString(message), SuiteApp.appContext.getString(okText), SuiteApp.appContext.getString(calcelText), positiveListener, negativeListener);
    }

    public void showYesNoAlert(String title,
                               String message,
                               String okText,
                               String calcelText,
                               OnClickListener positiveListener,
                               OnClickListener negativeListener) {
        if (!habilitado)
            return;
        habilitado = false;
        if (message.length() > 0) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setPositiveButton(okText, positiveListener);

            if (null == negativeListener) {
                alertDialogBuilder.setNegativeButton(calcelText, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        habilitado = true;
                        dialog.dismiss();
                    }
                });
            } else {
                alertDialogBuilder.setNegativeButton(calcelText, negativeListener);
            }

            alertDialogBuilder.setCancelable(false);
            mAlertDialog = alertDialogBuilder.create();
            mAlertDialog.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    habilitado = true;
                }
            });
            mAlertDialog.show();

        }
    }

    public void showYesNoAlertEspecial(String title, String errorCode, String descripcionError, String okText,
                                       String calcelText, OnClickListener positiveListener,
                                       OnClickListener negativeListener) {
        if (!habilitado)
            return;
        habilitado = false;
        if (descripcionError.length() > 0) {

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(title);
            builder.setIcon(R.drawable.anicalertaaviso);
            LayoutInflater inflater = getLayoutInflater();
            View vista = inflater.inflate(R.layout.api_consulta_inversiones_layout_alert_codigo_error, null);
            builder.setView(vista);
            builder.setPositiveButton(okText, positiveListener);

            if (null == negativeListener) {
                builder.setNegativeButton(calcelText,
                        new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                habilitado = true;
                                dialog.dismiss();
                            }
                        });
            } else {
                builder.setNegativeButton(calcelText,
                        negativeListener);
            }

            TextView text1 = (TextView) vista.findViewById(SuiteApp.getResourceId("descripcionError", "id"));
            TextView text2 = (TextView) vista.findViewById(SuiteApp.getResourceId("codigoError", "id"));
            text1.setText(descripcionError);
            text2.setText(errorCode);

            builder.setCancelable(false);

            mAlertDialog = builder.create();

            mAlertDialog.setOnDismissListener(new OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    habilitado = true;
                }
            });
            mAlertDialog.show();


        }
    }

    public boolean isHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public void showNoYesAlert(int message, OnClickListener positiveListener, OnClickListener negativeListener) {
        showYesNoAlert(R.string.label_information,
                message,
                R.string.common_alert_yesno_negative_button,
                R.string.common_alert_yesno_positive_button,
                negativeListener,
                positiveListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}