package bancomer.api.consultainversiones.gui.controllers;

import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;
import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.consultainversiones.R;
import bancomer.api.consultainversiones.commons.GuiTools;
import bancomer.api.consultainversiones.commons.TrackingHelper;
import bancomer.api.consultainversiones.gui.commons.controllers.ListaDatosViewController;
import bancomer.api.consultainversiones.gui.delegates.InversionDelegate;
import bancomer.api.consultainversiones.implementations.BaseViewController;
import bancomer.api.consultainversiones.implementations.BmovilViewsController;
import bancomer.api.consultainversiones.implementations.SuiteAppConsultaInversionesApi;
import bancomer.api.consultainversiones.models.InversionesPlazo;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

/**
 * Created by User1 on 09/12/2015.
 */
public class DetalleMenuInversionesViewController extends BaseViewController{

    private InversionDelegate inversionDelegate;

    public BmovilViewsController parentManager;

    private int opcionSeleccionada;

    private ArrayList<InversionesPlazo> lista;

    private ListView listview;

    private LlenarTablaMenuInversiones adapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.activity_detalle_menu_inversiones_view_controller);
        marqueeEnabled = true;
        setTitle(R.string.misCuentas_consultar_inversionesPlazo, R.drawable.api_consulta_inversiones_bmovil_consultar_icono);

        parentManager = SuiteAppConsultaInversionesApi.getInstance().getBmovilApplication().getBmovilViewsController();
        TrackingHelper.trackState("consulta inversiones", parentManager.estados);
        setParentViewsController(SuiteAppConsultaInversionesApi.getInstance().getBmovilApplication().getBmovilViewsController());
        inversionDelegate = (InversionDelegate) parentViewsController.getBaseDelegateForKey(InversionDelegate.INVERSION_DELEGATE_ID);
        inversionDelegate.setDetalleMenuInversionesController(this);

        init();
    }

    public void init() {

        muestraIndicadorActividad(getString(R.string.alert_operation), getString(R.string.alert_connecting));
        llenarTablas();
        scaleForCurrentScreen();
        ocultaIndicadorActividad();
    }

    public void llenarTablas() {

        ArrayList<Object> encabezado;
        encabezado = new ArrayList<Object>();
        encabezado.add(null);
        encabezado.add(getString(R.string.numero_inversion));
        encabezado.add(getString(R.string.saldo));
        lista = inversionDelegate.getListaInversionesPlazos();
        llenarEncabezados(encabezado, lista);
    }


    public void llenarEncabezados(ArrayList<Object> encabezado,ArrayList<InversionesPlazo> lista){
             listview = (ListView) findViewById(R.id.mis_inversiones_tabla_View);

            this.lista = lista;
            TextView celda1 = (TextView) findViewById(R.id.lista_celda_1);
            celda1.setText((String) encabezado.get(1));

            TextView celda2 = (TextView) findViewById(R.id.lista_celda_2);
            celda2.setText((String) encabezado.get(2));
            celda2.setGravity(Gravity.RIGHT);
            celda2.setSelected(true);
            celda2.setSingleLine(true);


            adapter = new LlenarTablaMenuInversiones();
            listview.setAdapter(adapter);

            ListAdapter listAdapter = listview.getAdapter();
            int totalHeight = 0;


              for (int i = 0; i < listAdapter.getCount(); i++) {
                  View listItem = listAdapter.getView(i, null, listview);
                  listItem.measure(0, 0);
                  totalHeight = listItem.getMeasuredHeight() * lista.size();
              }

            ViewGroup.LayoutParams params = listview.getLayoutParams();
            params.height = totalHeight
                    + (listview.getDividerHeight() * (listview.getCount() - 1));
            listview.setLayoutParams(params);
            listview.requestLayout();
    }

    private boolean marqueeEnabled;

    private class LlenarTablaMenuInversiones extends BaseAdapter {

        @Override
        public int getCount() {
            return lista.size();
        }

        @Override
        public Object getItem(int position) {
            return lista.get(position);
        }

        @SuppressWarnings({ "unchecked", "deprecation" })
        @Override
        public View getView(final int position, View convertView,
                            ViewGroup parent) {

            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.list_item_productos, null);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    opcionSeleccionada = position;
                    inversionDelegate.performAction((lista.get(opcionSeleccionada)));
                }
            });

            TextView celda1 = (TextView) convertView.findViewById(R.id.lista_celda_1);
            TextView celda2 = (TextView) convertView.findViewById(R.id.lista_celda_2);


            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.FILL_PARENT, 0.5f);
            //celda1.setText((String) registro.get(1));
            celda1.setText(lista.get(position).getNumeroInversion());
            celda1.setLayoutParams(params);
            celda1.setGravity(Gravity.LEFT);
            celda1.setTextAppearance(getApplicationContext(), R.style.movimientos_celda_style);
            LinearLayout.LayoutParams params2 = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.FILL_PARENT, 0.5f);
            celda2.setText(Tools.formatAmount(lista.get(position).getSaldo(), false));
            celda2.setLayoutParams(params2);
            celda2.setGravity(Gravity.RIGHT);
            celda2.setTextAppearance(getApplicationContext(), R.style.movimientos_celda_style);
            celda2.setSelected(marqueeEnabled);
            celda2.setSingleLine(true);

            return convertView;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    public void goBack() {
        super.goBack();
    }

    private void scaleForCurrentScreen() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(findViewById(R.id.tvDetalleInversion));
    }
}
