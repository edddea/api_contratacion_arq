package com.bancomer.mbanking;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.io.IOException;
import java.io.StringReader;
import java.net.URLEncoder;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.AbstractSuitePowerManager;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.MenuHamburguesaViewsControllers;
import suitebancomer.aplicaciones.bmovil.classes.io.NetworkOperation;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.login.LoginData;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.controllers.BaseViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;


public abstract class BaseSubapplication {
	// #region Variables.
	/**
	 * Determines if this activity is in process of changing screen.
	 */
	protected boolean changingActivity = false;
	
	protected boolean applicationLogged = false;
	
	protected boolean applicationInBackground = false;
	
	
	
	/**
	 * Reference to the main application.
	 */
	protected SuiteApp suiteApp = null;
	
	/**
	 * Referencia al controlador de vistas.
	 */
	protected BaseViewsController viewsController;

	/**
	 * Reference to a pending network operation, in case that an
	 * operation launched a session expired error. This operation
	 * will be relaunched after a successful login
	 */
	protected NetworkOperation pendingOperation = null;

	/**
	 * Reference to server communication handler.
	 */
	protected Server server;

	/**
	 * Pending network operations.
	 */
	protected Hashtable<Integer, NetworkOperation> networkOperationsTable = null;
	
	protected boolean isHomeKeyPressed = false;
	// #endregion


	// #region Getters y Setters.

	/**
	 * @return the changingActivity
	 */
	public boolean isChangingActivity() {
		return changingActivity;
	}

	private boolean isHomeKeyPressed() {
		return isHomeKeyPressed;
	}

	protected void setHomeKeyPressed(boolean isHomeKeyPressed) {
		this.isHomeKeyPressed = isHomeKeyPressed;
	}

	/**
	 * @param changingActivity the changingActivity to set
	 */
	public void setChangingActivity(boolean changingActivity) {
		this.changingActivity = changingActivity;
	}

	/**
	 * @return the applicationLogged
	 */
	public boolean isApplicationLogged() {
		return applicationLogged;
	}

	/**
	 * @param applicationLogged the applicationLogged to set
	 */
	public void setApplicationLogged(boolean applicationLogged) {
		this.applicationLogged = applicationLogged;
	}

	/**
	 * @return the applicationInBackground
	 */
	public boolean isApplicationInBackground() {
		return applicationInBackground;
	}

	/**
	 * @param applicationInBackground the applicationInBackground to set
	 */
	public void setApplicationInBackground(boolean applicationInBackground) {
		this.applicationInBackground = applicationInBackground;
	}

	/**
	 * @return the suiteApp
	 */
	public SuiteApp getSuiteApp() {
		return suiteApp;
	}

	/**
	 * @param suiteApp the suiteApp to set
	 */
	public void setSuiteApp(SuiteApp suiteApp) {
		this.suiteApp = suiteApp;
	}

	/**
	 * @return the viewsController
	 */
	public BaseViewsController getViewsController() {
		return viewsController;
	}

	/**
	 * @param viewsController the viewsController to set
	 */
	public void setViewsController(BaseViewsController viewsController) {
		this.viewsController = viewsController;
	}

	/**
	 * @return the pendingOperation
	 */
	public NetworkOperation getPendingOperation() {
		return pendingOperation;
	}

	/**
	 * @param pendingOperation the pendingOperation to set
	 */
	public void setPendingOperation(NetworkOperation pendingOperation) {
		this.pendingOperation = pendingOperation;
	}

	/**
	 * @return the server
	 */
	public Server getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(Server server) {
		this.server = server;
	}

	/**
	 * @return the networkOperationsTable
	 */
	public Hashtable<Integer, NetworkOperation> getNetworkOperationsTable() {
		return networkOperationsTable;
	}

	/**
	 * @param networkOperationsTable the networkOperationsTable to set
	 */
	public void setNetworkOperationsTable(
			Hashtable<Integer, NetworkOperation> networkOperationsTable) {
		this.networkOperationsTable = networkOperationsTable;
	}
	// #endregion

	// #region Network.
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * predefined texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 */
	public void invokeNetworkOperation(int operationId, Hashtable<String,?> params,
									   boolean isJson,  ParsingHandler hadler,
									   Server.isJsonValueCode isJsonValue,
									   BaseViewController caller) {
		invokeNetworkOperation(operationId, params,isJson,hadler,isJsonValue, caller, false);
	}
	
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * predefined texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	public void invokeNetworkOperation(int operationId, Hashtable<String,?> params,
									    boolean isJson,  ParsingHandler hadler,
									    Server.isJsonValueCode isJsonValue,
									   BaseViewController caller,
										boolean callerHandlesError) {
		invokeNetworkOperation(operationId, params,isJson,hadler,isJsonValue, caller,
				suiteApp.getApplicationContext().getString(R.string.alert_operation), 
				suiteApp.getApplicationContext().getString(R.string.alert_connecting), callerHandlesError);
	}

	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * custom texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param progressLabel text with the title of the progress popup
	 * @param progressMessage text with the content of the progress popup
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	protected void invokeNetworkOperation(final int operationId, final Hashtable<String, ?> params,
										  final boolean isJson, final ParsingHandler hadler,
										  final Server.isJsonValueCode isJsonValue,
										  final BaseViewController caller, 
										  String progressLabel, 
										  String progressMessage, 
										  final boolean callerHandlesError) {
		final Integer opId = Integer.valueOf(operationId);          

		if (isNotAlreadyCalling(opId)) {

			try {

				NetworkOperation operation = new NetworkOperation(operationId, params, caller);
				this.networkOperationsTable.put(opId, operation);
				this.pendingOperation = operation;

//				//TODO quitar el numero de operacion en hardcode.
//				if (caller != null && 11 != operationId)
//				    caller.muestraIndicadorActividad(progressLabel, progressMessage);
				
				if (caller != null) {
					if(AbstractSuitePowerManager.getSuitePowerManager().isScreenOn() && (!isHomeKeyPressed())) {
						suitebancomercoms.aplicaciones.bmovil.classes.common.Session session = suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(com.bancomer.base.SuiteApp.appContext);
						session.setCmbPerfilHamburguesa(Boolean.FALSE);
						//flujo bmovil
							caller.muestraIndicadorActividad(progressLabel, progressMessage);

					} else {
						if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "La aplicación estaba bloqueada.");
					}
				}

				Thread thread = new Thread(new Runnable() {
					public void run() {
						try {
							/*ServerResponse response = server.doNetworkOperation(opId.intValue(), params);
							returnFromNetworkOperation(opId, response, caller, null, callerHandlesError);
							*/

							ServerResponse responseNew=null;
							ParametersTO parameters=new ParametersTO();
							parameters.setSimulation(Server.SIMULATION);
							parameters.setDevelopment(Server.DEVELOPMENT);
							parameters.setOperationId(opId.intValue());
							parameters.setParameters(params.clone());
							parameters.setJson(isJson);
							IResponseService resultado = new ResponseServiceImpl() ;
							//Convirtiendo el Enum al esperado por API SERVER
							int ordinalEnum=isJsonValue.ordinal();
							suitebancomer.aplicaciones.commservice.commons.ApiConstants.isJsonValueCode isJsonTransformValue=suitebancomer.aplicaciones.commservice.commons.ApiConstants.isJsonValueCode.values()[ordinalEnum];
							Hashtable<String, ?> parameters2 = ((Hashtable<String, ?>)parameters.getParameters());
							com.bancomer.mbanking.ConnectionFactory cf= new com.bancomer.mbanking.ConnectionFactory(parameters.getOperationId(), parameters2, parameters.isJson(), hadler,isJsonTransformValue);
							resultado=cf.processConnectionWithParams();

							if(operationId==Server.LOGIN_OPERATION) {
								if(!ServerCommons.ARQSERVICE) {
									StringReader reader = new StringReader(resultado.getResponseString());
									Parser parser = new Parser(reader);
									responseNew= new ServerResponse(new LoginData());
									responseNew.process(parser);
								}else{
									LoginData ld=new LoginData();
									responseNew= new ServerResponse(ld);
									responseNew.setResponsePlain(resultado.getResponseString());
								}
							}else if(operationId==Server.OP_GLOBAL){
								LoginData ld=new LoginData();
								responseNew= new ServerResponse(ld);
								responseNew.setResponsePlain(resultado.getResponseString());
							}else{
								responseNew = cf.parserConnection(resultado, hadler);
							}

							returnFromNetworkOperation(opId, responseNew, caller, null, callerHandlesError);
						} catch (Throwable t) {
							returnFromNetworkOperation(opId, null, caller, t, callerHandlesError);
						}
					}

				});

				// Starts the worker thread to perform network operation
				thread.start();

			} catch (Throwable th) {
				returnFromNetworkOperation(opId, null, caller, th, callerHandlesError);
			}

		} else {
			// unable to process request, show error and return
			return;
		}

	}

	/**
	 * Called internally after a network operation has returned. It ends the progress
	 * popup and invokes method to analyze the result
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message from the server or method should handle in the default
	 * implementation
	 */
	protected void returnFromNetworkOperation(final Integer operationId, 
											  final ServerResponse response, 
											  final BaseViewController caller, 
											  final Throwable throwable, 
											  final boolean callerHandlesError) {

		if (caller != null) {
			if(!(operationId.equals(Server.MOVEMENTS_OPERATION))
				&& (!(operationId.equals(Server.CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO)))
				&& (!(operationId.equals(Server.CONSULTA_DEPOSITOS_CHEQUES)))
				&& (!(operationId.equals(Server.CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS)))) {
			caller.ocultaIndicadorActividad();
			}
			
			caller.runOnUiThread(new Runnable() {
				public void run() {
					try {
						analyzeNetworkResponse(operationId, response, throwable, caller, callerHandlesError);
					} catch (Throwable t) {
						if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Exception on analyzeNetworkResponse.", t);
						if (caller != null) {
							String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
							caller.showErrorMessage(message);
						}
					}
				}
			});
			
		} else { //TODO session timer expired, go to login screen
			Thread thread = new Thread(new Runnable() {
				public void run() {
					try {
						analyzeNetworkResponse(operationId, response, throwable, null, callerHandlesError);
					} catch (Throwable t) {
						String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
						if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName() + " :: returnFromNetworkOperation", message);
					}
				}
			});
			thread.run();
		}
	}

	// TODO revisar en hijos.
	/**
	 * Analyze the network response obtained from the server
	 * @param operationId network identifier. See Server class.
	 * @param response the ServerResponse instance built from the server response. It
	 * contains the type of result (success, failure), and the real content for
	 * the application business.
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	protected void analyzeNetworkResponse(Integer operationId, 
										  ServerResponse response, 
										  Throwable throwable, 
										  final BaseViewController caller, 
										  boolean callerHandlesError) {
		// get the caller           
		NetworkOperation operation = this.networkOperationsTable.get(operationId);
		if (operation != null) {
			// remove the operation id from the table
			this.networkOperationsTable.remove(operationId);
			if (operation.isActive()) {
				if (response != null) {
					BaseViewController baseViewController = operation.getCaller();
					int resultCode = response.getStatus();
					switch (resultCode) {
					case ServerResponse.OPERATION_SUCCESSFUL:
					case ServerResponse.OPERATION_WARNING:
						baseViewController.processNetworkResponse(operationId.intValue(), response);						
											
						break;
					case ServerResponse.OPERATION_OPTIONAL_UPDATE:
						baseViewController.processNetworkResponse(operationId.intValue(), response);
						break;
					case ServerResponse.OPERATION_ERROR:
						this.pendingOperation = null;
						String errorCode = response.getMessageCode();						
						int operationError = operationId.intValue();
						
						if( (operationError != Server.QUITAR_SUSPENSION) && (operationError != Server.LOGIN_OPERATION) && (operationError != Server.OP_ENVIO_CLAVE_ACTIVACION) && (operationError != Server.OP_ACTIVACION) && (operationError != Server.AUTENTICACION_ST) && (Constants.BLOCKED_ERROR_CODE.equals(errorCode) || Constants.BLOCKED_ERROR_CODE_2.equals(errorCode))){
							caller.showInformationAlert( response.getMessageText(), new OnClickListener() {							
								@Override
								public void onClick(DialogInterface dialog, int which) {
									SuiteApp.getInstance().getBmovilApplication().closeSession(Session.getInstance(SuiteApp.appContext).getUsername(),Session.getInstance(SuiteApp.appContext).getIum(),Session.getInstance(SuiteApp.appContext).getClientNumber());
								}
							});
						}else if ((operationError == Server.LOGIN_OPERATION) && (Constants.DEACTIVATION_ERROR_CODE.equals(errorCode))) {
							Session session = Session.getInstance(SuiteApp.appContext);
							String username = session.getUsername();
							String password = session.getPassword();
							Session.getInstance(suiteApp.getApplicationContext()).setValidity(Session.UNSET_STATUS);

							Bundle params = new Bundle();
							params.putString(Server.USERNAME_PARAM, username);
							params.putString(Server.PASSWORD_PARAM, password);
						} else if ((operationError == Server.SELF_TRANSFER_OPERATION) ||
								(operationError == Server.BANCOMER_TRANSFER_OPERATION) ||
								(operationError == Server.EXTERNAL_TRANSFER_OPERATION) ||
								(operationError == Server.ALTA_OPSINTARJETA) ||
								(operationError == Server.SERVICE_PAYMENT_OPERATION) ||
								(operationError == Server.COMPRA_TIEMPO_AIRE)||
								(operationError == Server.OP_RETIRO_SIN_TAR)){						
								// Mensaje de error en Transferencias.
								if(Server.ALLOW_LOG) Log.d("BaseSubapplication", "ERROR Transferencias >>> operationError = " +  operationError + " errorCode = " + errorCode);
								mostrarErrorTransfer(response, errorCode, baseViewController, caller,operationId.intValue());
								//baseViewController.processNetworkResponse(operationId.intValue(), response);
								
														
						}
						else if (operationError == Server.CONSULTA_CIE)
						{
							//caller.ocultaIndicadorActividad();
							baseViewController.showInformationAlert(response.getMessageText());
						}
						else {
								if (callerHandlesError) {
									baseViewController.processNetworkResponse(operationId.intValue(), response);
								} else {
							//		caller.ocultaIndicadorActividad();
//									caller.showErrorMessage(errorCode + "\n" + response.getMessageText());
									caller.showInformationAlertEspecial(caller.getString(R.string.label_error), errorCode, response.getMessageText(), null);
								}
						}
						break;
					case ServerResponse.OPERATION_SESSION_EXPIRED:
						this.pendingOperation = operation;
						Session.getInstance(suiteApp.getApplicationContext()).setValidity(Session.INVALID_STATUS);
						suiteApp.getSuiteViewsController().showMenuSuite(true);
						break;						
					default:
						caller.ocultaIndicadorActividad();
						caller.showErrorMessage(R.string.error_format);
						break;
					}
				} else {
					if (throwable != null) {
						caller.ocultaIndicadorActividad();
						caller.showErrorMessage(getErrorMessage(throwable));
					} else {
						caller.ocultaIndicadorActividad();
						caller.showErrorMessage(R.string.error_communications);
					}
				}
			}
		} else {
			caller.ocultaIndicadorActividad();
			caller.showErrorMessage(R.string.error_unexpected);
		}		
	}

	/**
	 * regresa a el activity StartBmovilInBack indicando que ocurrio un error
	 */
	private void returnToStartBmovilInBack(BaseViewController caller){
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
		ActivityCompat.finishAffinity(caller);
		caller.finish();
		Intent i=new Intent(SuiteApp.getInstance(), StartBmovilInBack.class);
		i.putExtra( Constants.RESULT_STRING,Constants.FALSE_STRING);
		i.putExtra( Constants.TIMEOUT_STRING,Constants.FALSE_STRING);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		caller.startActivity(i);
	}
	/**
	 * Muestra un aviso de error en caso de que el servidor rechace la transferencia. Si es
	 * avanzado mostrará el mensaje devuelto y si es básico mostrará uno por defecto.
	 * 
	 * @param response Respuesta del servidor
	 * @param baseViewController Controlador base
	 * @param caller Controlador padre
	 */
	private void mostrarErrorTransfer(ServerResponse response, String errorCode, BaseViewController baseViewController, final BaseViewController caller, int operationValue) {		
		Session session = Session.getInstance(SuiteApp.appContext);
		Constants.Perfil perfil = session.getClientProfile();
		// Si es perfil avanzado mostrará el mensaje recibido, sino mostrará el definido en String.xml
		
		Account cEje = Tools.obtenerCuentaEje();
		String cEjeType = cEje == null ? "" : cEje.getType();
		
		if ((Constants.CODE_CNE0234.equals(errorCode)) || (Constants.CODE_CNE0235.equals(errorCode)) || (Constants.CODE_CNE0236.equals(errorCode))) {
			
			if ((Constants.Perfil.basico.equals(perfil)) && (!cEjeType.equals(Constants.CREDIT_TYPE))) {
				String msg = baseViewController.getString(R.string.transferir_aviso_texto);
				caller.ocultaIndicadorActividad();
				//es la peticion de retiro sin targeta, bmovil no inicio la session
				if(operationValue==Server.OP_RETIRO_SIN_TAR && !"".equals(SuiteApp.appOrigen)  ){//si el origen no es bmovil
					caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode, msg, new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							dialogInterface.dismiss();
							returnToStartBmovilInBack(caller);
						}
					});
				}else {
					caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode, msg, null);
				}
				
			} else if (Constants.Perfil.recortado.equals(perfil)) {
				baseViewController.processNetworkResponse(operationValue, response);
				
			} else {
				// Avanzado, o básico con tarjeta de crédito como cuenta eje
				caller.ocultaIndicadorActividad();
				//es la peticion de retiro sin targeta, bmovil no inicio la session
				if(operationValue==Server.OP_RETIRO_SIN_TAR && !"".equals(SuiteApp.appOrigen)  ){//si el origen no es bmovil
					caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode, response.getMessageText(), new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							dialogInterface.dismiss();
							returnToStartBmovilInBack(caller);
						}
					});
				}else {
					caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode, response.getMessageText(), null);
				}
			}
			
		} else {
			caller.ocultaIndicadorActividad();
			//es la peticion de retiro sin targeta, bmovil no inicio la session y no es un error de comunicacion
			if(operationValue==Server.OP_RETIRO_SIN_TAR && !"".equals(SuiteApp.appOrigen) && !"ERROR DE COMUNICACIONES".equalsIgnoreCase(response.getMessageText()) && !"ERROR%20DE%20COMUNICACIONES".equalsIgnoreCase(response.getMessageText())){//si el origen no es bmovil
				caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode, response.getMessageText(), new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						dialogInterface.dismiss();
						returnToStartBmovilInBack(caller);
					}
				});
			}else {
				caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode, response.getMessageText(), null);
			}
		}
		
//		if(Constants.Perfil.recortado.equals(perfil)){
//			
//			baseViewController.processNetworkResponse(operationValue, response);
//	
//		}else if(Constants.Perfil.basico.equals(perfil) && 
//				(Constants.CODE_CNE0234.equals(errorCode) ||
//				 Constants.CODE_CNE0235.equals(errorCode) ||
//				 Constants.CODE_CNE0236.equals(errorCode)) &&
//				 !cEjeType.equals(Constants.CREDIT_TYPE)){
//			String msg = baseViewController.getString(R.string.transferir_aviso_texto);
//			caller.ocultaIndicadorActividad();
//			caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,msg, null);
//		}else {
//			caller.ocultaIndicadorActividad();
//			caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,response.getMessageText(), null);
//		}								
	}
	
	

	/**
	 * Cancel any ongoing network operation, and clears the network
	 * operation table to prevent any other network operation from
	 * executing
	 */
	protected void cancelOngoingNetworkOperations() {
		this.networkOperationsTable.clear();
		this.pendingOperation = null;
		this.server.cancelNetworkOperations();
	}
	
	/**
	 * Determines if the requested operation is not being called already
	 * @param opId the operation id
	 * @return true if the operation isn't already waiting for a response
	 */
	private boolean isNotAlreadyCalling(int opId){
		return !this.networkOperationsTable.containsKey(opId);
	}
	// #endregion
	
	// #region Otros métodos.
	// TODO revisar en hijos.
	public void cierraAplicacion() {
		viewsController.cierraViewsController();
		viewsController = null;
		server.cancelNetworkOperations();
		server = null;
		applicationLogged = false;
	}
	
	/**
	 * Shows the Main menu screen, removing any activity on top
	 */
	public void requestMenu(Context activityContext){
		changingActivity = true;
		viewsController.showMenuInicial();
	}

	/**
	 * Get the operation error message
	 * @param throwable the throwable
	 * @return the operation error message 
	 */
	protected String getErrorMessage(Throwable throwable) {
		StringBuffer sb = new StringBuffer();
		if (throwable != null) {
			if (throwable instanceof NumberFormatException) {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_format));
			} else if (throwable instanceof ParsingException) {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_format));
			} else if (throwable instanceof IOException) {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_communications));
			} else {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_communications));
			}
		}
		return sb.toString();
	}
	// #endregion
	
	// TODO revisar en hijos.
	public BaseSubapplication(SuiteApp suiteApp) {
		this.suiteApp = suiteApp;
		server = new Server();
		networkOperationsTable = new Hashtable<Integer, NetworkOperation>();
		viewsController = null;
		applicationLogged = false;
	}
}
