package com.bancomer.mbanking;

import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

import android.content.Context;
import android.content.res.Resources;
import android.os.Binder;
import android.os.Bundle;
import android.os.Parcel;
import android.util.Log;

import com.bancomer.mbanking.softtoken.SuiteAppApi;

public class ServiceBconnectImpl extends Binder {

    Context context;
    ContratacionSTDelegate contDelegate;
    Constants.TipoOtpAutenticacion tipoOTP;
    public ServiceBconnectImpl(Resources res,Context contex) {
        this.context=contex;
        final SuiteAppApi softoken=new SuiteAppApi();
        softoken.onCreate(contex);
        if(null == SuiteAppApi.getInstanceApi().getSofttokenApplicationApi())
            SuiteAppApi.getInstanceApi().startSofttokenAppApi();
        contDelegate = new ContratacionSTDelegate();
        contDelegate.generaTokendelegate.inicializaCore();
    }

    public void logger(String msg){
        if(ServerCommons.ALLOW_LOG){
        Log.w("BCONNECT->service",msg);
        }
    }

    @Override
    protected boolean onTransact(int code, Parcel data, Parcel reply, int flags) {

        logger("entra al servicio");
        if(code == 33){
            Bundle values = data.readBundle();

            String otp = null;
            logger("Servicio ejecutado INTERMEDIO en BConnect");

            // Obtener nombre de la app
            String nombreApp = (String)values.get("nameApp");

            // Obtener tipo de otp
            String otpString = (String)values.get("tipoOTP");
            logger("tipo otp solicitada:" + otpString);
            try {
                 tipoOTP = Constants.TipoOtpAutenticacion.valueOf(otpString);
                logger("valor de otp:" + tipoOTP.toString());
            }catch (Exception e){
                logger("error al generar otp:" + e.getMessage());
            }

            if(TipoOtpAutenticacion.registro == tipoOTP){
                logger("solicita otp de registro");
                String cuentaDestino = (String)values.get("cuentaDestino");
                logger("cuenta destino"+cuentaDestino);
                escenarioAlt18(cuentaDestino, nombreApp);
            }else if(TipoOtpAutenticacion.codigo == tipoOTP){
                logger("solicita otp de codigo");
                escenarioAlt17(nombreApp);
            }
            logger("Saliendo de Servicio");
        }

        return true;
    }


    /**
     * Metodo de EA#17 de B-Connect "Generacion de OTP para softoken"
     * @param nombreApp
     */
    public void escenarioAlt17(String nombreApp){
        logger("ea17");
        String otp = null;
        // Generar OTP
        otp = contDelegate.generaTokendelegate.generarOtpTiempo();
        logger("otp generada: "+otp);
        saveOtpInKeyChain(otp);
    }

    /**
     * Metodo de EA#18 de B-Connect "Generacion de OTP de Registro para softoken"
     * @param cuentaDestino
     * @param nombreApp
     */
    public void escenarioAlt18(String cuentaDestino, String nombreApp){
        logger("ea18");
        String otp = null;

        // Generar OTP
        otp = (null == cuentaDestino) ? null : contDelegate.generaTokendelegate.generarOtpChallenge(cuentaDestino);
        logger("otp generada: "+otp);

        saveOtpInKeyChain(otp);
    }

    public void saveOtpInKeyChain(String otp){
        KeyStoreWrapper kswrapper = KeyStoreWrapper.getInstance(context);
        logger("Guardando otp en keychain");
        kswrapper.storeValueForKey("OTP", otp);
    }

}
