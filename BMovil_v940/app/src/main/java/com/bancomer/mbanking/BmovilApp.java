package com.bancomer.mbanking;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import java.util.Hashtable;
import java.util.Timer;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.timer.SessionCloser;
import bancomer.api.common.timer.TimerController;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.controllers.MenuSuiteViewController;

@SuppressLint("HandlerLeak")
public class BmovilApp extends BaseSubapplication implements SessionCloser{

	/**
	 * Message for the handler, indicates that session must be expired.
	 */
	private static final int LOGOUT_MESSAGE = 0;

	/**
	 * The local session validity timer.
	 */
	private Timer sessionTimer = null;

	/**
	 * Task that manages session logout
	 */
	//public SessionLogoutTask logoutTask;

	public static int STEP_TOTAL = 0;
	public static int STEP_ACTUAL = 0;


	//////////////////////////////////////////////////////////////
	//					Methods									//
	//////////////////////////////////////////////////////////////    

	/**
	 * Constructor básico, se inicializa aqui debido a que no puede extender de un objeto Aplicación, ya que no es posible
	 * que exista mas de uno
	 */
	public BmovilApp(SuiteApp suiteApp) {
		super(suiteApp);
		viewsController = new BmovilViewsController(this);
	}

	public BmovilViewsController getBmovilViewsController() {
		return (BmovilViewsController)viewsController;
	}

	public void reiniciaAplicacion() {

		TimerController timerCtr = TimerController.getInstance();

		if(!(((BmovilViewsController)viewsController).getCurrentViewControllerApp() instanceof MenuSuiteViewController)) 
		{
			((BmovilViewsController)viewsController).cierraViewsController();
			viewsController = new BmovilViewsController(this);
		}

		server.cancelNetworkOperations();
		if (timerCtr.getSessionTimer() != null) {
			timerCtr.getSessionTimer().cancel();
			timerCtr.getLogoutTask().cancel();
			timerCtr.setSessionTimer(null);
			timerCtr.setLogoutTask(null);
		}
	}
	
	public Timer getSessionTimer() {
		return sessionTimer;
	}
	public void setSessionTimer(Timer sessionTimer) {
		this.sessionTimer=sessionTimer;
		
	}

	@Override
	public void cierraAplicacion() {
		super.cierraAplicacion();

		TimerController timerCtr = TimerController.getInstance();

		if (timerCtr.getSessionTimer() != null) {
			timerCtr.getSessionTimer().cancel();
		}
		if (timerCtr.getLogoutTask() != null){
			timerCtr.getLogoutTask().cancel();
		}
		timerCtr.setSessionTimer(null);
		timerCtr.setLogoutTask(null);
	}

	/*public void initTimer() {
		sessionTimer = new Timer(true);
		logoutTask = new SessionLogoutTask();
	}*/
	
	/*public TimerTask getLogoutTask() {
		return logoutTask;
	}*/
	
	//////////////////////////////////////////////////////////////////////////////
	//                Network operations										//
	//////////////////////////////////////////////////////////////////////////////
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * custom texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param progressLabel text with the title of the progress popup
	 * @param progressMessage text with the content of the progress popup
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	@Override
	protected void invokeNetworkOperation(int operationId, 
										  final Hashtable<String, ?> params,
										  final boolean isJson,
										  final ParsingHandler handler,
										  final Server.isJsonValueCode isjJsonValueCode,
										  final BaseViewController caller, 
										  String progressLabel, 
										  String progressMessage, 
										  final boolean callerHandlesError) {
		//we stop the timer during server calls
		TimerController timerCtr = TimerController.getInstance();
		if (timerCtr.getLogoutTask() != null) {
			timerCtr.getLogoutTask().cancel();
		}
		super.invokeNetworkOperation(operationId, params,isJson,handler,isjJsonValueCode, caller, progressLabel, progressMessage, callerHandlesError);
	}

	/**
	 * Analyze the network response obtained from the server
	 * @param operationId network identifier. See Server class.
	 * @param response the ServerResponse instance built from the server response. It
	 * contains the type of result (success, failure), and the real content for
	 * the application business.
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	@Override
	protected void analyzeNetworkResponse(Integer operationId, 
										  ServerResponse response, 
										  Throwable throwable, 
										  final BaseViewController caller, 
										  boolean callerHandlesError) {

		if (operationId.intValue() == Server.CLOSE_SESSION_OPERATION) {

			cancelOngoingNetworkOperations();

			Session.getInstance(SuiteApp.appContext).setValidity(Session.INVALID_STATUS);
			Session.getInstance(SuiteApp.appContext).saveRecordStore();
			new Thread(new Runnable() {
				public void run() {
					try {
						System.gc();
						//Session.getInstance(SuiteApp.appContext).storeSession();
						//Incidencia 22218
						
					} catch (Throwable t) {
					} finally {
						suiteApp.getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp().runOnUiThread(new Runnable() {
							public void run() {
								
								// invalid session, request login
								TimerController timerCtr = TimerController.getInstance();
								timerCtr.getSessionTimer().cancel();
								if (!applicationInBackground) {
									suiteApp.getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp().ocultaIndicadorActividad();
//									((MenuSuiteViewController)suiteApp.getSuiteViewsController().getCurrentViewControllerApp()).setShouldHideLogin(true);
								}
								
								if (!(suiteApp.getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp() instanceof MenuSuiteViewController)
										|| Session.getInstance(SuiteApp.appContext).getAceptaCambioPerfil()) {

									if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//flujo bmovil
										//timeout off
										Activity ac=MainController.getInstance().getActivityController().getCurrentActivity()!=null?MainController.getInstance().getActivityController().getCurrentActivity():viewsController.getCurrentViewController();


										SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
										ActivityCompat.finishAffinity(ac);
										ac.finish();
										Intent i=new Intent(ac, StartBmovilInBack.class);
										i.putExtra( Constants.TIMEOUT_STRING,Constants.TRUE_STRING);
										i.putExtra(Constants.RESULT_STRING,Constants.FALSE_STRING);
										i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
										//i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
										ac.startActivity(i);
										//return;
									} else{
										if(null!=suiteApp.getSuiteViewsController()) {
											suiteApp.getSuiteViewsController().showMenuSuite(true);
										}
									}
								}else{
										MenuSuiteViewController menuSuite = (MenuSuiteViewController) suiteApp.getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp();
										
								}
								suiteApp.getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp().hideCurrentDialog();
								
								suiteApp.cierraAplicacionBmovil();
								if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Si se cerro la sesión");
							}
						});
					}
				}
			}).start();

		} else {
			super.analyzeNetworkResponse(operationId, response, throwable, caller, callerHandlesError);
			TimerController timerCtr = TimerController.getInstance();
			timerCtr.resetTimer();
		}
	}

	/**
	 * Initiates a session closure process
	 */
	public void closeSession(boolean isHomeKeyPressed) {
		this.setHomeKeyPressed(isHomeKeyPressed);
		Session session = Session.getInstance(suiteApp.getApplicationContext());
		closeSession(session.getUsername(), session.getIum(), session.getClientNumber());
	}
	
	/**
	 * Initiates a session closure process, cuando se borran los datos y es
	 * necesario hacer el cierre de sesion
	 */
	public void closeSession(String username, String ium, String client) {
		applicationLogged = false;
		//estes para origen
		// close session
		System.gc();
		BaseViewController caller = null;
		if (viewsController != null) {
			caller = viewsController.getCurrentViewController();
		}
		if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//flujo bmovil
			//timeout off
			SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
			Activity ac=MainController.getInstance().getActivityController().getCurrentActivity()!=null?MainController.getInstance().getActivityController().getCurrentActivity():viewsController.getCurrentViewController();

			ActivityCompat.finishAffinity(ac);
			ac.finish();
			Intent i=new Intent(ac, StartBmovilInBack.class);
			i.putExtra( Constants.TIMEOUT_STRING,Constants.TRUE_STRING);
			i.putExtra(Constants.RESULT_STRING,Constants.FALSE_STRING);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			//i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			ac.startActivity(i);
			//return;

		}
			Hashtable<String, String> params = new Hashtable<String, String>();

		params.put("NT", username);
		params.put(suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants.IUM_ETIQUETA, ium);
		params.put("TE", client);
		


		invokeNetworkOperation(Server.CLOSE_SESSION_OPERATION, params, false, null, Server.isJsonValueCode.NONE, caller,
				suiteApp.getApplicationContext().getString(R.string.alert_closeSession),
				suiteApp.getApplicationContext().getString(R.string.alert_thank_you), true);
		
		if(Server.ALLOW_LOG) Log.e("CerrarSesion", "La sesion fue cerrada satisfactoriamente.");
	}
	
	public void closeBmovilAppSession(BaseViewController caller) {
		applicationLogged = false;
		Session session = Session.getInstance(suiteApp.getApplicationContext());
		String username = session.getUsername();
		String ium = session.getIum(); 
		String client = session.getClientNumber();
				
		// close session
		System.gc();


		Hashtable<String, String> params = new Hashtable<String, String>();
		
		params.put("NT", username);
		params.put(suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants.IUM_ETIQUETA, ium);
		params.put("TE", client);


		
		//BaseViewController caller = null;
		//if (viewsController != null) {
			//caller = viewsController.getCurrentViewController();
		//}

		invokeNetworkOperation(Server.CLOSE_SESSION_OPERATION, params,false,null, Server.isJsonValueCode.NONE, caller,
				suiteApp.getApplicationContext().getString(R.string.alert_closeSession),
				suiteApp.getApplicationContext().getString(R.string.alert_thank_you), true);
		
		if(Server.ALLOW_LOG) Log.e("CerrarSesion", "La sesion fue cerrada satisfactoriamente.");
	}

	public int getApplicationStatus() {
		return Session.getInstance(SuiteApp.appContext).getPendingStatus();
	}
	
	/////////////////////////////////////////////////////////////////////////////
	//                Local session management                                 //
	/////////////////////////////////////////////////////////////////////////////
	/**
	 * Records a user activity event (keystroke), in order to
	 * check session validity
	 */
	/*public void userActivityEvent() {
		resetLogoutTimer();
	}*/

	/**
	 * Logs the user out and discards the current session.
	 */
	public synchronized void logoutApp(boolean isHomeKeyPressed){
		Session session = Session.getInstance(suiteApp.getApplicationContext());

		if(session.getValidity() == Session.VALID_STATUS){
			session.setValidity(Session.INVALID_STATUS);
			closeSession(isHomeKeyPressed);
		}
	}

	/**
	 * Called whenever the user performs an action or touches the screen
	 */
	/*public void resetLogoutTimer() {
		if (logoutTask != null) {
			logoutTask.cancel();	
		}
		if(sessionTimer != null) {
			logoutTask = new SessionLogoutTask();
			if(Session.getInstance(suiteApp.getApplicationContext()).getValidity() == Session.VALID_STATUS){
				long timeoutMilis = Session.getInstance(suiteApp.getApplicationContext()).getTimeout();
				sessionTimer.schedule(logoutTask, timeoutMilis);
			}
		}
	}*/


	/**
	 * We must use a TimerTask class to execute logout actions after the
	 * timer runs out.
	 */
	/*private class SessionLogoutTask extends TimerTask {

		@Override
		public void run() {
			applicationLogged = false;
			Message msg = new Message();
			msg.what = BmovilApp.LOGOUT_MESSAGE;
			mApplicationHandler.sendMessage(msg);
		}

	}*/

	/**
	 * Use handler to respond to logout messages given by the timer.
	 */
	/*private Handler mApplicationHandler = new Handler() {

		public void handleMessage(Message msg) {

			if(msg.what == BmovilApp.LOGOUT_MESSAGE){
				logoutApp(false);
			}
		}
	};*/


	@Override
	public void cerrarSesion() {
		applicationLogged = false;
		Session session = Session.getInstance(suiteApp.getApplicationContext());
		//Log.d("[CGI-Configuracion-Obligatorio] >> ", "[BmovilApp] TimerManager isResetTimer -> "+TimerManager.isResetTimer());
		if(session.getValidity() == Session.VALID_STATUS){
			session.setValidity(Session.INVALID_STATUS);
			closeSession(isHomeKeyPressed);
		}
	}
	
}
