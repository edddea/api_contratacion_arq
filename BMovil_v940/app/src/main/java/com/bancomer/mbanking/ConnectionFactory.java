package com.bancomer.mbanking;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.util.Log;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.util.Hashtable;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;
import suitebancomer.aplicaciones.commservice.service.ICommService;

public class ConnectionFactory {

	private int operationId;
	private Hashtable<String, ?> params;
	private Object responseObject;
	private ParametersTO parameters;
	private boolean isJson;
	private ApiConstants.isJsonValueCode isJsonValueCode;

	public ConnectionFactory(int operationId, Hashtable<String, ?> params,
			boolean isJson, Object responseObject,ApiConstants.isJsonValueCode isJsonValueCode) {
		super();
		this.operationId = operationId;
		this.params = params;
		this.responseObject = responseObject;
		this.isJson = isJson;
		this.isJsonValueCode = isJsonValueCode;
	}

	public IResponseService processConnectionWithParams() throws Exception {

		final Integer opId = Integer.valueOf(operationId);
		parameters = new ParametersTO();
		parameters.setSimulation(Server.SIMULATION);
		if (!Server.SIMULATION) {
			parameters.setProduction(!Server.DEVELOPMENT);
			parameters.setDevelopment(Server.DEVELOPMENT);
		}
		parameters.setOperationId(opId.intValue());
		parameters.setParameters(params.clone());
		parameters.setJson(isJson);
		parameters.setIsJsonValue(this.isJsonValueCode);
		IResponseService resultado = new ResponseServiceImpl();
		ICommService serverproxy = new CommServiceProxy(SuiteApp.appContext);
		try {
			resultado = serverproxy.request(parameters,
					responseObject == null ? new Object().getClass()
							: responseObject.getClass());
			// ConsultaEstatusMantenimientoData
			// estatusM=(ConsultaEstatusMantenimientoData)resultado.getObjResponse();
		} catch (UnsupportedEncodingException e) {
			throw new Exception(e);
		} catch (ClientProtocolException e) {
			throw new Exception(e);
		} catch (IllegalStateException e) {
			throw new Exception(e);
		} catch (IOException e) {
			throw new Exception(e);
		} catch (JSONException e) {
			throw new Exception(e);
		}

		return resultado;

	}

	public ServerResponse parserConnection(IResponseService resultado,
			ParsingHandler handler) throws Exception {
		ServerResponse response=null;
		ParsingHandler handlerP=null;
		if (ApiConstants.OPERATION_SUCCESSFUL == resultado.getStatus()) {
			//Verificar si es de tipo Imagen
            Hashtable<String,String>  params2= (Hashtable<String, String>) parameters.getParameters();
            if(params2.get(ApiConstants.IMAGEN_BMOVIL) != null){
                return new ServerResponse(ServerResponse.OPERATION_SUCCESSFUL, null, null, resultado.getObjResponse());
            }
			if(resultado.getObjResponse() instanceof ParsingHandler){
				handlerP= (ParsingHandler) resultado.getObjResponse();
			}
			if (parameters.isJson()) {
				if (handler != null) {
					ParserJSON parser = new ParserJSON(
							resultado.getResponseString());
					try {
						handlerP.process(parser);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						throw new Exception(e);
					} catch (ParsingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						throw new Exception(e);
					}
				}
				response = new ServerResponse(resultado.getStatus(),
						resultado.getMessageCode(), resultado.getMessageText(), handlerP);
			} else {
				// Que pasa si no es JSON la respuesta
				Reader reader = null;
				try {
						reader = new StringReader(resultado.getResponseString());
						Parser parser = new Parser(reader);
					if (handler != null) {
						if(handlerP==null){
							Class<?> clazz = handler.getClass();
							//Constructor<?> ctor = clazz.getConstructor();
							//handlerP = (ParsingHandler) ctor.newInstance();
							handlerP = (ParsingHandler)clazz.cast(handler);
                		}
						response= new ServerResponse(handlerP);
						response.process(parser);
					}else{
						response= new ServerResponse(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),null);
						response.process(parser);
					}
				}catch(Exception e){
					throw new Exception(e);
					
				}finally {
					if (reader != null) {
						try {
							reader.close();
						} catch (Throwable ignored) {
							throw new Exception(ignored);
							
						}
					}
				}
			}

		}else{
			try {
				handlerP=null;
				//caso especcial para la operacion EXITO_OFERTA_CONSUMO
				if(resultado.getObjResponse() instanceof ParsingHandler && operationId==Server.EXITO_OFERTA_CONSUMO ){
					handlerP= (ParsingHandler) resultado.getObjResponse();
					if (parameters.isJson()) {
						if (handler != null) {
							ParserJSON parser = new ParserJSON(
									resultado.getResponseString());
							try {
								handlerP.process(parser);
							} catch (IOException e) {
								if(Server.ALLOW_LOG)
									Log.e("parserConnection", e.getMessage());
							} catch (ParsingException e) {
								if(Server.ALLOW_LOG)
									Log.e("parserConnection", e.getMessage());
							}
						}
					}
				}
				response= new ServerResponse(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),handlerP);
			} catch (Exception e) {
				// TODO: handle exception
				throw new Exception(e);
			}
			
		}
		
		return response;
	}


}
