package com.bancomer.mbanking;


import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.ImageView;

import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.activities.MenuPrincipalActivity;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.MenuPrincipalCreditDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuPrincipalViewController;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.aplicaciones.commservice.httpcomm.HttpInvoker;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.timer.TimerController;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.SincronizarSesion;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.AltaRetiroSinTarjetaViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.LoginData;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.aplicaciones.bmovil.classes.common.SessionStoredListener;

import suitebancomer.classes.gui.controllers.MenuSuiteViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Reactivacion;

/**
 * Created by alanmichaelgonzalez on 28/03/16.
 */
public class StartBmovilInBack  extends BaseViewController implements  SessionStoredListener{
    private ServerResponse loginResponse;
    private LoginData loginData;
    private String storeSessionType;
    private String operacion;
    private String numCelular;
    private String password;
    private String ium;
    private BaseViewController me;

    private final static String STORE_SESSION_LOGIN = "sessionLogin";
    private SuiteApp suiteApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, 0, R.layout.layout_splash_activity);//activity_base_back
        me=this;
        //muestraIndicadorActividad("BMovil", "Recuperando datos");
        ImageView img = (ImageView)findViewById(R.id.splashImage);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        img.getLayoutParams().height = metrics.heightPixels;

        ServerCommons.loadEnviromentConfig(this);
        if(!Server.ALLOW_LOG){
            ApiConstants.allowLogPropertieName = getLocalClassName();
        }

        suiteApp = (SuiteApp)getApplication();
        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(this);
        setParentViewsController(suiteApp.getSuiteViewsController());


    }

    @Override
    protected void onResume() {
        super.onResume();
        ServerCommons.loadEnviromentConfig(this);
        try {
            Bundle b = getIntent().getExtras();
                if (b == null) {
                    Log.e("StartBmovilinBack","menu1");
                    SuiteApp.getInstance().stopTimer();
                    goBack();
                } else if (b.getString(Constants.RESULT_STRING) != null) {//un exito o un error

                    Log.e("StartBmovilinBack", "menu2" + b.getString(Constants.RESULT_STRING));
                    final String result=b.getString(Constants.RESULT_STRING);
                    final String timeout=b.getString(Constants.TIMEOUT_STRING);
                    getIntent().removeExtra(Constants.RESULT_STRING);
                    getIntent().removeExtra(Constants.TIMEOUT_STRING);
                    SuiteApp.getInstance().stopTimer();
                    SuiteApp.getInstance().returntoAppOrigen(result,Boolean.valueOf(timeout));
                    goBack();

                } else if (b.getString(Constants.COOKIES_STRING) != null){
                    Log.e("StartBmovilinBack", "menu3");
                    startPeticonLoginBack(setCookis(b));
                    getIntent().removeExtra(Constants.COOKIES_STRING);
                    getIntent().removeExtra(Constants.OPERACION_STRING);
                    getIntent().removeExtra(Constants.APP_ORIGEN_STRING);
                    getIntent().removeExtra(Constants.NUM_CELULAR_STRING);
                    getIntent().removeExtra(Constants.PASSWORD_STRING);


                }else{
                    goBack();
                }

        }catch (Exception e){
            if(Server.ALLOW_LOG){
                Log.e("StartBmovilinBack", e.getMessage());
            }
            SuiteApp.getInstance().stopTimer();
            goBack();
        }

    }


    /**
     * recive el cookie de la aplicacon base y la setea a nuestro server
     * @param b
     * @return
     * @throws Exception
     */
    private String setCookis(final Bundle b) throws Exception {
       // {"cookie":{"name":"nombrecookie","value":"valor","comment":"","domain":"","expireDate":"01/12/2016 22:45:00","path":"","version":1,"secure":"true"}}
        BasicClientCookie[] listaCookies;
        try {
            final String jsonCookie = b.getString(Constants.COOKIES_STRING);
            operacion=b.getString(Constants.OPERACION_STRING);
            suiteApp.appOrigen=b.getString(Constants.APP_ORIGEN_STRING);
            numCelular=b.getString(Constants.NUM_CELULAR_STRING);
            password=b.getString(Constants.PASSWORD_STRING);
            ium=b.getString("ium");//este no se enviara solo es porque no contamos con ambiente total de preubas



            final JSONObject jo = new JSONObject(jsonCookie);
            final JSONArray cookieArray=jo.getJSONArray(Constants.COOKIE_STRING);
            listaCookies=new BasicClientCookie[cookieArray.length()];
            for(int i=0;i<cookieArray.length();i++){
                JSONObject cook=(JSONObject)cookieArray.get(i);
                final BasicClientCookie clientCookie = new BasicClientCookie(cook.getString(Constants.COOKIE_NAME ),
                        cook.getString(Constants.COOKIE_VALUE));

                //clientCookie.setComment(cook.getString(Constants.COOKIE_COMMNET));
                clientCookie.setDomain(cook.getString(Constants.COOKIE_DOMAIN));
                //SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                //clientCookie.setExpiryDate(cook.getString(Constants.COOKIE_ESPIRE_DATE)!=null?formatter.parse(cook.getString(Constants.COOKIE_ESPIRE_DATE)):null);
                clientCookie.setPath(cook.getString(Constants.COOKIE_PATH));
                clientCookie.setVersion(cook.getInt(Constants.COOKIE_VERSION));
                clientCookie.setSecure(Boolean.valueOf(cook.getString(Constants.COOKIE_SECURE)));
                listaCookies[i]=clientCookie;
            }



            HttpInvoker.setSerialisableCookie(true, listaCookies);
            SuiteApp.getInstance().appOrigenCookie=jsonCookie;
            return "";
    }catch (Exception e) {
            if(Server.ALLOW_LOG){
                Log.e("StartBmovilinBack",e.getMessage());
            }
            throw new Exception(e);
        }
    }

    /**
     * realiza la peticion de posicion global para el nuevo servicio
     * @param cookieValue
     */
    public void startPeticonLoginBack(final String cookieValue) {
        final Session session = Session.getInstance(SuiteApp.appContext);
        session.setUsername(numCelular);
        final String userCellPhone=session.getUsername();
        final Hashtable<String, String> paramTable = new Hashtable<String, String>();
        String ium = session.getIum();
        if (suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.isEmptyOrNull(ium)) {
            ium = suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.buildIUM(userCellPhone, session.getSeed(),
                    SuiteApp.appContext);
            if(this.ium!=null){
                session.setIum(this.ium);
                paramTable.put(Constants.IUM_STRING, this.ium);
            }else{
                session.setIum(ium);
                paramTable.put(Constants.IUM_STRING, ium);
            }
        }
        if(Server.ALLOW_LOG){
            Log.e("StartInBack","posicion global ium:"+session.getIum() );
        }
        doNetworkOperation(Constants.OP_GLOBAL, paramTable, this, true);
    }

    /**
     * realiza la peticon al server
     * @param operationId
     * @param params
     * @param caller
     * @param callerHandlesError
     */
    public void doNetworkOperation(final int operationId,
                                   final Hashtable<String, ?> params, final BaseViewController caller,
                                   final boolean callerHandlesError) {
        SuiteApp.getInstance().getBmovilApplication().invokeNetworkOperation(operationId, params, false, new LoginData(), Server.isJsonValueCode.NONE, caller,
                callerHandlesError);
    }



    /**
     * regresa la respuesta de login in back
     * @param operationId
     * @param response
     */
    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        ServerResponse response2=null;
        LoginData loginData1=new LoginData();
        try {
            //falta el error new JSONObject(response.getResponsePlain()).optString("status")
            if(Constants.OP_GLOBAL_ERROR.equals(
                    new JSONObject(response.getResponsePlain()).optString(Constants.STATUS_STRING))){
                //error
                SuiteApp.getInstance().returntoAppOrigen(Constants.FALSE_STRING,Boolean.FALSE);
            }else{//ok
                //loginData1.process(new JSONObject(response.getResponsePlain()).getJSONObject(Constants.RESPONSE_STRING));
                loginData1.processGlobal(new JSONObject(response.getResponsePlain()).getJSONObject(Constants.RESPONSE_STRING));
                response2= new ServerResponse(loginData1);
            }
            storeSessionAfterLogin(response2);
        }catch (Exception e){
            if(Server.ALLOW_LOG){
                Log.e("StartBmovilinBack", e.getMessage());
            }
            SuiteApp.getInstance().stopTimer();
            goBack();
        }
    }

    /**
     * Overrides and cancels android's back button, if the button is visible then it does the same
     * as clicking on the application's back button.
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        onUserInteraction();
        switch (keyCode){
            case KeyEvent.KEYCODE_BACK:
                goBack();
                System.exit(0);
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public void goBack() {
      finish();
        System.exit(0);
    }
    /**
     * Store session after login
     *
     * @param response
     *            the server response
     */
    private void storeSessionAfterLogin(ServerResponse response) {
        SuiteApp.getInstance().getBmovilApplication()
                .setApplicationLogged(true);
        SuiteAppAdmonApi.getInstance().getBmovilApplication().setApplicationLogged(true);
        loginResponse = response;
        loginData = (LoginData) response.getResponse();
        final Session session = Session.getInstance(SuiteApp.appContext);
        session.setClientNumber(loginData.getClientNumber());
        session.setAccounts(loginData.getAccounts(), loginData.getServerDate(),
                loginData.getServerTime());
        session.setEmail(loginData.getEmailCliente());
        session.setPassword(password);

        session.setNombreCliente(Tools.isEmptyOrNull(loginData
                .getNombreCliente()) ? "" : loginData.getNombreCliente());
        if (loginData.getPerfiCliente().equals(Constants.PROFILE_ADVANCED_03)) {
            session.setClientProfile(Constants.Perfil.avanzado);
        } else if (loginData.getPerfiCliente().equals(Constants.PROFILE_RECORTADO_02)) {
            session.setClientProfile(Constants.Perfil.recortado);
        }else if (loginData.getPerfiCliente().equals(Constants.PROFILE_BASIC_01) ||loginData.getPerfiCliente().equals(Constants.PROFILE_BASIC_00) ){
            session.setClientProfile(Constants.Perfil.basico);
        }

        session.setCompaniaUsuario(loginData.getCompaniaTelCliente());
        session.setSecurityInstrument(loginData.getInsSeguridadCliente());
        session.setEstatusIS(loginData.getEstatusIS());

        session.setAuthenticationJson(loginData.getAuthenticationJson());
        if (loginData.getCatalogoTiempoAire() != null) {
            session.updateCatalogoTiempoAire(loginData.getCatalogoTiempoAire());
        }
        if (loginData.getCatalogoDineroMovil() != null) {
            session.updateCatalogoDineroMovil(loginData
                    .getCatalogoDineroMovil());
        }
        if (loginData.getCatalogoServicios() != null) {
            session.updateCatalogoServicios(loginData.getCatalogoServicios());
        }
        if (loginData.getCatalogoMantenimientoSPEI() != null) {
            session.updateCatalogoMantenimientoSPEI(loginData.getCatalogoMantenimientoSPEI());
        }

		/*
		 * if the server has returned at least 1 catalog it means that the
		 * catalog version has changed
		 */
        if (loginData.getCatalogs() != null) {
            escribirCatalogos(loginData);
        }

        // Verifica el timeout de la pantalla del teléfono, si este timeout es
        // menor que el regresado por el servidor, se toma el del teléfono
        int defTimeOut = 0;
        final int DELAY = 3000;
        final int oneMinuteInMilis = 60000;

        if (SuiteApp.getInstance()
                .getBmovilApplication().getBmovilViewsController()
                .getCurrentViewControllerApp() == null) {
            SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(this);
        }

        defTimeOut = Settings.System.getInt(SuiteApp.getInstance()
                        .getBmovilApplication().getBmovilViewsController()
                        .getCurrentViewControllerApp().getContentResolver(),
                Settings.System.SCREEN_OFF_TIMEOUT, DELAY);

        if (defTimeOut < loginData.getTimeout() * oneMinuteInMilis
                && defTimeOut > 15000)
            session.setTimeout(defTimeOut);
        else
            session.setTimeout(loginData.getTimeout() * oneMinuteInMilis);

        session.setValidity(Session.VALID_STATUS);
        muestraIndicadorActividad("", getString(R.string.alert_StoreSession));
        storeSessionType = STORE_SESSION_LOGIN;

        session.storeSession(this);
    }
    public void escribirCatalogos(LoginData loginData) {
        Session session = Session.getInstance(SuiteApp.appContext);
        session.setCatalogVersions(loginData.getCatalogVersions());
        session.updateCatalogs(loginData.getCatalogs());
    }

    /*
	 * (non-Javadoc)
	 *
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.common.SessionStoredListener
	 * #sessionStored()
	 */
    @Override
    public void sessionStored() {
        ocultaIndicadorActividad();
        if (storeSessionType.equals(STORE_SESSION_LOGIN)) {
            SincronizarSesion.getInstance().SessionToSessionApi(SuiteApp.appContext);
            if (loginResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {

                SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
                if(Constants.OPERACION_RETIRO_SIN_TARGETA.equals(operacion)) {
                    /*Intent i= new Intent(this, AltaRetiroSinTarjetaViewController.class);
                    ActivityCompat.finishAffinity(this);
                    startActivity(i);
                    finish();*/
                    SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showAltaRetiroSinTarjetaViewController();
                }else if(Constants.OPERACION_CREDITOS.equals(operacion)){

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            BBVACreditTask();
                        }
                    });

                }else if(Constants.OPERACION_MENU_PRINCIPAL.equals(operacion)){
                    SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal();
                }

                SuiteApp.getInstance().getBmovilApplication().setApplicationLogged(true);
                TimerController timerContr = TimerController.getInstance();
                timerContr.setSessionCloser(SuiteApp.getInstance().getBmovilApplication());
                timerContr.initTimer(Session.getInstance(SuiteApp.appContext).getTimeout());
                timerContr.resetTimer();
                loginData = null;
                loginResponse = null;
            }
        }
    }


    private void BBVACreditTask()
    {
        Server.SIMULATION=ServerCommons.SIMULATION;
        Server.DEVELOPMENT=ServerCommons.DEVELOPMENT;
        Server.ALLOW_LOG=ServerCommons.ALLOW_LOG;

        Session session = Session.getInstance(this);
        // Alberto
        MainController.getInstance().setCurrentActivity(this);
        MainController.getInstance().setContext(this);
        MainController.getInstance().setMenuSuiteController(null);
        MainController.getInstance().setStartBmovilInBack(this);
        MainController.getInstance().muestraIndicadorActividad(getResources().getString(R.string.indicador_operacion), getResources().getString(R.string.indicador_conectando));

        MenuPrincipalCreditDelegate delegateMP = new MenuPrincipalCreditDelegate();
        delegateMP.setSessionByBmovil(session.getClientNumber(), session.getPassword(), session.getIum(),numCelular , session.getEmail());
        delegateMP.doCalculo();


    }

    public void habilitarVista()
    {
        getParentViewsController().setCurrentActivityApp(this);
        setHabilitado(true);

    }
    public void error(String errorCode,String errormsg)
    {

        showInformationAlert(errorCode + "\n" + errormsg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                goBack();
            }
        });

    }

    public void showMenuCredit()
    {
        MainController.getInstance().ocultaIndicadorActividad();
        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuBBVACredit();
    }

    @Override
    public void onUserInteraction() {
       // SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();
        if(SuiteApp.getInstance().getBmovilApplication().isApplicationLogged()){
            TimerController timerContr = TimerController.getInstance();
            timerContr.resetTimer();
        }
     //   super.onUserInteraction();
    }

    public void ofertaConsumo(OfertaConsumo oferta, Promociones promocion)
    {
        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showDetalleConsumo(oferta, promocion);
    }

    public void ofertaILC(OfertaILC oferta,Promociones promocion)
    {
        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showDetalleILC(oferta, promocion);
    }
}


