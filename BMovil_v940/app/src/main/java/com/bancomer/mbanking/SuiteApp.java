package com.bancomer.mbanking;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.timer.TimerController;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuPrincipalViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.classes.common.PropertiesManager;
import suitebancomer.classes.gui.controllers.MenuSuiteViewController;
import suitebancomer.classes.gui.controllers.SuiteViewsController;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.widget.Switch;

import com.bancomer.mbanking.softtoken.SofttokenApp;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

public class SuiteApp extends Application {
	public static boolean isSubAppRunning;
	public static Context appContext;
	public static String appOrigen="";
	public static String appOrigenCookie="";
	public static Boolean PASOMENUPRINCIPAL= new Boolean(false);
	private static SuiteApp me;
	private SuiteViewsController suiteViewsController;
	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(newBase);
		MultiDex.install(this);
	}
	@Override
	public void onCreate() {
		Log.e("mbanking", "inicia aplication");
		suiteViewsController = new SuiteViewsController();
		isSubAppRunning = false;
		
		me = this;
		appContext = getApplicationContext();
	};

	private MenuPrincipalViewController menuPrincipalViewController;
	public MenuPrincipalViewController getMenuPrincipalViewController() {
		return menuPrincipalViewController;
	}

	public void setMenuPrincipalViewController(MenuPrincipalViewController menuPrincipalViewController) {
		this.menuPrincipalViewController = menuPrincipalViewController;
	}




	public void cierraAplicacionSuite() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public SuiteViewsController getSuiteViewsController() {
		return suiteViewsController;
	}

	public static SuiteApp getInstance() {
		return me;
	}
	
	// #region BmovilApp
	private BmovilApp bmovilApplication;
	
	public BmovilApp getBmovilApplication() {
		if(bmovilApplication == null)
			bmovilApplication = new BmovilApp(this);
		return bmovilApplication;
	}
	
	public static boolean getBmovilStatus() {
		return PropertiesManager.getCurrent().getBmovilActivated();
	}
	
	public void startBmovilApp() {
		bmovilApplication = new BmovilApp(this);
		isSubAppRunning = true;
	}
	
	public void cierraAplicacionBmovil() {
		bmovilApplication.cierraAplicacion();
		bmovilApplication = null;
		isSubAppRunning = false;		
		//reiniciaAplicacionBmovil();
	}
	
	public void reiniciaAplicacionBmovil() {
		if(bmovilApplication == null)
			bmovilApplication = new BmovilApp(this);
		bmovilApplication.reiniciaAplicacion();
		isSubAppRunning = true;
	}
	// #endregion
	
	// #region SofttokenApp
	private SofttokenApp softtokenApp;
	
	public SofttokenApp getSofttokenApplication() {
		if(softtokenApp == null)
			startSofttokenApp();
		return softtokenApp;
	}
	
	public static boolean getSofttokenStatus() {
		return PropertiesManager.getCurrent().getSofttokenActivated();
	}
	
	public void startSofttokenApp() {
		SuiteAppApi softoken=new SuiteAppApi();
		softoken.onCreate(appContext);
		softtokenApp = new SofttokenApp(softoken);
		isSubAppRunning = true;
	}
	
	public void closeSofttokenApp() {
		if(null == softtokenApp)
			return;
		softtokenApp.cierraAplicacion();
		softtokenApp = null;
		isSubAppRunning = false;
		
		if(suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController)
			((MenuSuiteViewController)suiteViewsController.getCurrentViewControllerApp()).setShouldHideLogin(true);
		
		suiteViewsController.showMenuSuite(true);
	}
	// #endregion
	
	
	public void closeBmovilAppSession(){
		
		if(suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController)
			((MenuSuiteViewController)suiteViewsController.getCurrentViewControllerApp()).setShouldHideLogin(true);
		
		suiteViewsController.showMenuSuite(true);
		
		
		SuiteApp suiteApp = SuiteApp.getInstance();
		Session session = Session.getInstance(suiteApp.getApplicationContext());

		if(session.getValidity() == Session.VALID_STATUS){
			session.setValidity(Session.INVALID_STATUS);
		}
		
		bmovilApplication.closeBmovilAppSession(suiteViewsController.getCurrentViewControllerApp());;
		
	}
	public void stopTimer(){
		TimerController timerCtr = TimerController.getInstance();

		if (timerCtr.getSessionTimer() != null) {
			timerCtr.getSessionTimer().cancel();
		}
		if (timerCtr.getLogoutTask() != null){
			timerCtr.getLogoutTask().cancel();
		}
		timerCtr.setSessionTimer(null);
		timerCtr.setLogoutTask(null);

	}
	String packageReturn;
	String activityReturn;
	String operacion;

	/**
	 * metodo que lanza la peticon a la app origen
	 * @param exito
	 * @param timeOut
	 */
	public void returntoAppOrigen(final  String exito,Boolean timeOut) {
		//regrea al origen
		getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		defineOperacion(Boolean.valueOf(exito),timeOut);

		intent.setComponent(new ComponentName(packageReturn,activityReturn ));
		intent.putExtra(Constants.APP_ORIGEN_STRING,Constants.APPORIGEN_BMOVIL);
		intent.putExtra(Constants.COOKIES_STRING, appOrigenCookie);
		intent.putExtra(Constants.OPERACION_STRING, operacion);
		if(Server.ALLOW_LOG){
			Log.e("mbanking", "se termina bmovil y llama a app anfitriona enviando apOrigien,");
		}
		startActivity(intent);
		//cierraAplicacionSuite();

	}

	/**
	 * metodo que define hacia que paquete regresar y activity asi como que operacion regresar
	 * @param exito
	 * @param timeout
	 */
	private void defineOperacion(final Boolean exito,final Boolean timeout){
		if(Constants.PACKAGE_NOMINA.equals(appOrigen)) {//NOMINA
			packageReturn=Constants.PACKAGE_NOMINA;
			if(exito){//EXITO
				activityReturn=Constants.NOMINA_CONTROLLER_OK;
				operacion="operacinExistosa";
			}else{
				if(timeout){//EL ERROR ES POR TIMEOUT
					activityReturn=Constants.NOMINA_CONTROLLER_LOGIN;
					operacion="errorTimeout";
				}else{//NO ES POR TIMEOUT
					activityReturn=Constants.NOMINA_CONTROLLER_ERROR;
					operacion="errorDistientoATimeout";
				}
			}
		}
	}
}
