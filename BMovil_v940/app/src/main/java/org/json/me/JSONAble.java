/*
 * Copyright (c) 2009 BBVA. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package org.json.me;

/**
 *
 * @author CGI
 */
public interface JSONAble {
    public void fromJSON(String jsonString);
    public String toJSON();
}
