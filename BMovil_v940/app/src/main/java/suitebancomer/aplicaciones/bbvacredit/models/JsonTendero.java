package suitebancomer.aplicaciones.bbvacredit.models;

public class JsonTendero {
	
	private String tendero;

	public JsonTendero(String tendero) {
		super();
		this.tendero = tendero;
	}

	public String getTendero() {
		return tendero;
	}

	public void setTendero(String tendero) {
		this.tendero = tendero;
	}
	
}
