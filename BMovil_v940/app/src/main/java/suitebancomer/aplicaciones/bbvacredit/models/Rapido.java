package suitebancomer.aplicaciones.bbvacredit.models;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;

import com.bancomer.mbanking.R;

import android.content.Context;

/**
 * Modelo de datos para una operaci�n r�pida.
 */
public class Rapido {
	// #region Variables.
	/**
	 * Nombre corto del r�pido.
	 */
	private String nombreCorto;
	
	/**
	 * Cuenta de origen para la operaci�n.
	 */
	private String cuentaOrigen;
	
	/**
	 * Cuenta de destino para la operaci�n.
	 */
	private String cuentaDestino;
	
	/**
	 * Tel�fono de destino para dinero m�vil o compra de tiempo aire.
	 */
	private String telefonoDestino;
	
	/**
	 * Importe de la operaci�n.
	 */
	private float importe;
	
	/**
	 * Nombre del beneficiario.
	 */
	private String beneficiario;
	
	/**
	 * Compa�ia de telefono celular para r�pidos de dinero m�vil o compra de tiempo aire.
	 */
	private String companiaCelular;
	
	/**
	 * Tipo de operaci�n.
	 */
	private String tipoRapido;
	
	/**
	 * Id de operaci�n para darla de baja.
	 */
	private String idOperacion;
	
	/**
	 * Concepto de la operaci�n para dinero m�vil.
	 */
	private String concepto;
	// #endregion
	
	// #region Setters / Getters.
	/**
	 * @return Nombre corto del r�pido.
	 */
	public String getNombreCorto() {
		return nombreCorto;
	}

	/**
	 * @param nombreCorto Nombre corto del r�pido a establecer.
	 */
	public void setNombreCorto(String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	/**
	 * @return Cuenta de origen para la operaci�n.
	 */
	public String getCuentaOrigen() {
		return cuentaOrigen;
	}

	/**
	 * @param cuentaOrigen Cuenta de origen para la operaci�n a establecer.
	 */
	public void setCuentaOrigen(String cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	/**
	 * @return Cuenta de destino para la operaci�n.
	 */
	public String getCuentaDestino() {
		return cuentaDestino;
	}

	/**
	 * @param cuentaDestino Cuenta de destino para la operaci�n a establecer.
	 */
	public void setCuentaDestino(String cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}

	/**
	 * @return Tel�fono de destino.
	 */
	public String getTelefonoDestino() {
		return telefonoDestino;
	}

	/**
	 * @param telefonoDestino Tel�fono de destino a establecer.
	 */
	public void setTelefonoDestino(String telefonoDestino) {
		this.telefonoDestino = telefonoDestino;
	}

	/**
	 * @return Importe de la operaci�n.
	 */
	public float getImporte() {
		return importe;
	}

	/**
	 * @param importe Importe de la operaci�n a establecer.
	 */
	public void setImporte(float importe) {
		this.importe = importe;
	}
		
	/**
	 * @return Nombre del beneficiario.
	 */
	public String getBeneficiario() {
		return beneficiario;
	}

	/**
	 * @param beneficiario Nombre del beneficiario a establecer.
	 */
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	/**
	 * @return Compa�ia de telefono celular.
	 */
	public String getCompaniaCelular() {
		return companiaCelular;
	}

	/**
	 * @param companiaCelular Compa�ia de telefono celular a establecer.
	 */
	public void setCompaniaCelular(String companiaCelular) {
		this.companiaCelular = companiaCelular;
	}

	/**
	 * @return Tipo de operaci�n.
	 */
	public String getTipoRapido() {
		return tipoRapido;
	}

	/**
	 * @param tipoRapido Tipo de operaci�n a establecer.
	 */
	public void setTipoRapido(String tipoRapido) {
		this.tipoRapido = tipoRapido;
	}

	/**
	 * @return Id de operaci�n.
	 */
	public String getIdOperacion() {
		return idOperacion;
	}

	/**
	 * @param idOperacion Id de operaci�n a establecer.
	 */
	public void setIdOperacion(String idOperacion) {
		this.idOperacion = idOperacion;
	}

	/**
	 * @return Concepto de la operaci�n.
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto Concepto de la operaci�n a establecer.
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	
	/**
	 * Obtiene el nombre a mostrar segun el tipo de r�pido.
	 */
	public String getNombreOperacionAMostrar() {
		Context context = MainController.getInstance().getContext();
		
		if(Tools.isEmptyOrNull(tipoRapido))
			return "";
		else if(ConstantsCredit.RAPIDOS_CODIGO_OPERACION_OTROS_BBVA.equals(tipoRapido))
			return context.getString(R.string.bmovil_rapidos_traspaso);
		else if(ConstantsCredit.RAPIDOS_CODIGO_OPERACION_TIEMPO_AIRE.equals(tipoRapido))
			return context.getString(R.string.bmovil_rapidos_tiempo_aire);
		else if(ConstantsCredit.RAPIDOS_CODIGO_OPERACION_DINERO_MOVIL.equals(tipoRapido))
			return context.getString(R.string.bmovil_rapidos_dinero_movil);
		else 
			return "";
	}
	// #endregion
	
	// #region Constructores.
	/**
	 */
	public Rapido() {
		nombreCorto = null;
		cuentaOrigen = null;
		cuentaDestino = null;
		importe = -1.0f;
		beneficiario = null;
		companiaCelular = null;
		tipoRapido = null;
		idOperacion = null;
		concepto = null;
	}

	/**
	 * @param nombreCorto Nombre corto del r�pido.
	 * @param cuentaOrigen Cuenta de origen para la operaci�n.
	 * @param cuentaDestino Cuenta de destino para la operaci�n.
	 * @param importe Importe de la operaci�n.
	 * @param beneficiario Nombre del beneficiario.
	 * @param referencia Referenc�a de la operaci�n.
	 * @param referenciaNumerica Referenc�a n�merica de la operaci�n.
	 * @param codigoBanco C�digo del banco.
	 * @param codigoOperacion C�digo de la operaci�n.
	 * @param idNumber El idNumber.
	 */
	public Rapido(String nombreCorto, String cuentaOrigen, String cuentaDestino, float importe, String beneficiario,
				  String companiaCelular, String tipoRapido, String idOperacion, String concepto) {
		super();
		this.nombreCorto = nombreCorto;
		this.cuentaOrigen = cuentaOrigen;
		this.cuentaDestino = cuentaDestino;
		this.importe = importe;
		this.beneficiario = beneficiario;
		this.companiaCelular = companiaCelular;
		this.tipoRapido = tipoRapido;
		this.idOperacion = idOperacion;
		this.concepto = concepto;
	}
	// #endregion
}
