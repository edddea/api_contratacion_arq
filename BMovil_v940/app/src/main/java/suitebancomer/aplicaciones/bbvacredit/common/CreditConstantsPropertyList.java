package suitebancomer.aplicaciones.bbvacredit.common;

public class CreditConstantsPropertyList {
	public static final String USERNAME = "username";


	public static final String CONSULTA_ALTERNATIVAS = "CONSULTA_ALTERNATIVAS";
	public static final String CONSULTA_ALTERNATIVAS_1 = "CONSULTA_ALTERNATIVAS_1";
	public static final String CONSULTA_ALTERNATIVAS_2 = "CONSULTA_ALTERNATIVAS_2";
	public static final String CONSULTA_ALTERNATIVAS_3 = "CONSULTA_ALTERNATIVAS_3";
	public static final String CONSULTA_ALTERNATIVAS_4 = "CONSULTA_ALTERNATIVAS_4";
	public static final String CONSULTA_ALTERNATIVAS_5 = "CONSULTA_ALTERNATIVAS_5";



	public static final String CONSULTA_ALTERNATIVAS_ERROR = "CONSULTA_ALTERNATIVAS_ERROR";

	public static final String CALCULO_ALTERNATIVAS = "CALCULO_ALTERNATIVAS";
	public static final String CALCULO_ALTERNATIVAS_ERROR = "CALCULO_ALTERNATIVAS_ERROR";

	public static final String CALCULO_ALTERNATIVAS_1 = "CALCULO_ALTERNATIVAS_1";
	public static final String CALCULO_ALTERNATIVAS_2 = "CALCULO_ALTERNATIVAS_2";
	public static final String CALCULO_ALTERNATIVAS_3 = "CALCULO_ALTERNATIVAS_3";
	public static final String CALCULO_ALTERNATIVAS_4 = "CALCULO_ALTERNATIVAS_4";
	public static final String CALCULO_ALTERNATIVAS_5 = "CALCULO_ALTERNATIVAS_5";
	public static final String CALCULO_ALTERNATIVAS_6 = "CALCULO_ALTERNATIVAS_6";
	public static final String CALCULO_ALTERNATIVAS_7 = "CALCULO_ALTERNATIVAS_7";
	public static final String CALCULO_ALTERNATIVAS_8 = "CALCULO_ALTERNATIVAS_8";


	public static final String ENVIO_CORREO = "ENVIO_CORREO";
	public static final String ENVIO_CORREO_ERROR = "ENVIO_CORREO_ERROR";

	public static final String CONSULTA_CORREO = "CONSULTA_CORREO";
	public static final String CONSULTA_CORREO_ERROR = "CONSULTA_CORREO_ERROR";

	public static final String CALCULO = "CALCULO";
	public static final String CALCULO_ERROR = "CALCULO_ERROR";
	public static final String CALCULO_1 = "CALCULO_1";
	public static final String CALCULO_2 = "CALCULO_2";
	public static final String CALCULO_3 = "CALCULO_3";
	public static final String CALCULO_4 = "CALCULO_4";
	public static final String CALCULO_5 = "CALCULO_5";

	public static final String CONSULTA_TDC = "CONSULTA_TDC";
	public static final String CONSULTA_TDC_ERROR = "CONSULTA_TDC_ERROR";

	public static final String LOGIN_VM500 = "LOGIN_VM500";
	public static final String LOGIN_VM350 = "LOGIN_VM350";
	public static final String LOGIN_VM300 = "LOGIN_VM300";
	public static final String LOGIN_VM910 = "LOGIN_VM910";
	public static final String LOGIN_VM411_0 = "LOGIN_VM411_0";
	public static final String LOGIN_VM411_HC = "LOGIN_VM411_HC";
	public static final String LOGIN_VM411 = "LOGIN_VM411";

	public static final String DETALLE_ALTERNATIVA_ILC = "DETALLE_ALTERNATIVA_ILC";
	public static final String DETALLE_ALTERNATIVA_CONSUMO = "DETALLE_ALTERNATIVA_CONSUMO";
	public static final String DETALLE_ALTERNATIVA_ERROR = "DETALLE_ALTERNATIVA_ERROR";
}
