package suitebancomer.aplicaciones.bbvacredit.gui.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bbvacredit.common.GuiTools;
import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.common.Session.State;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.MenuPrincipalCreditDelegate;
import suitebancomer.aplicaciones.bbvacredit.models.CreditoContratado;
import suitebancomer.aplicaciones.bbvacredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuPrincipalViewController;
import tracking.TrackingHelper;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.StartBmovilInBack;
import com.bancomer.mbanking.SuiteApp;

public class MenuPrincipalActivity extends BaseActivity  implements OnClickListener, DialogInterface.OnClickListener, OnLongClickListener{
	
	
	/**
	 * Clase para controlar los distintos items de creditos y tener cada uno estructurado en una clase
	 * @author miguelangel.pintormo
	 * @param <T>
	 *
	 */
	private class CreditItem{
		private ImageButton boton;
		private ImageView badge;
		private TextView subText;
		private TextView moneyText;
		private LinearLayout layout;
		private Class<?> redirect;
		private Integer arrayIndex;

		public CreditItem(LinearLayout layout, ImageButton boton, ImageView badge, TextView subText,
				TextView moneyText) {
			super();
			this.layout = layout;
			this.boton = boton;
			this.badge = badge;
			this.subText = subText;
			this.moneyText = moneyText;
		}
		
		public Integer getArrayIndex() {
			return arrayIndex;
		}
		public void setArrayIndex(Integer arrayIndex) {
			this.arrayIndex = arrayIndex;
		}
		public Class<?> getRedirect() {
			return redirect;
		}
		public void setRedirect(Class<?> redirect) {
			this.redirect = redirect;
		}
		public LinearLayout getLayout() {
			return layout;
		}
		public void setLayout(LinearLayout layout) {
			this.layout = layout;
		}
		public ImageButton getBoton() {
			return boton;
		}
		public void setBoton(ImageButton boton) {
			this.boton = boton;
		}
		public ImageView getBadge() {
			return badge;
		}
		public void setBadge(ImageView badge) {
			this.badge = badge;
		}
		public TextView getSubText() {
			return subText;
		}
		public void setSubText(TextView subText) {
			this.subText = subText;
		}
		public TextView getMoneyText() {
			return moneyText;
		}
		public void setMoneyText(TextView moneyText) {
			this.moneyText = moneyText;
		}
		
	}
	
	/**
	 * Toma las dos listas de CreditItem creadas y busca en ellas coincidencias por id del view
	 * @param id
	 * @return
	 */
	private CreditItem getCreditItemById(int id){
		Boolean encontrado = false;
		Iterator<CreditItem> iterator;
		CreditItem ret = null;
		
		if(!creditosOfertados.isEmpty()){
			iterator = creditosOfertados.iterator();
			while(iterator.hasNext() && !encontrado){
				CreditItem item = iterator.next();
				if(item.getBoton().getId() == id){
					ret = item;
					encontrado = true;
				}
			}
		}
		
		if(!encontrado && !creditosContratados.isEmpty()){
			iterator = creditosContratados.iterator();
			while(iterator.hasNext() && !encontrado){
				CreditItem item = iterator.next();
				if(item.getBoton().getId() == id){
					ret = item;
					encontrado = true;
				}
			}
		}
		
		return ret;
	}
	
	private CreditItem getCreditItemOfertadoById(int id){
		Boolean encontrado = false;
		Iterator<CreditItem> iterator;
		CreditItem ret = null;
		
		if(!creditosOfertados.isEmpty()){
			iterator = creditosOfertados.iterator();
			while(iterator.hasNext() && !encontrado){
				CreditItem item = iterator.next();
				if(item.getBoton().getId() == id){
					ret = item;
					encontrado = true;
				}
			}
		}
		
		return ret;
	}
	
	private CreditItem getCreditItemContratadoById(int id){
		Boolean encontrado = false;
		Iterator<CreditItem> iterator;
		CreditItem ret = null;
		
		if(!creditosContratados.isEmpty()){
			iterator = creditosContratados.iterator();
			while(iterator.hasNext() && !encontrado){
				CreditItem item = iterator.next();
				if(item.getBoton().getId() == id){
					ret = item;
					encontrado = true;
				}
			}
		}
		
		return ret;
	}
	
	private final Integer NUMERO_CREDITOS = 6;
	
	private ArrayList<CreditItem> creditosOfertados = new ArrayList<CreditItem>();
	
	private ArrayList<CreditItem> creditosContratados = new ArrayList<CreditItem>();
	
	private MenuPrincipalCreditDelegate delegate;
	
	private HashMap<Integer,State> stateList = new HashMap<Integer,State>();
	
	private ImageButton refreshBtn;
	private ImageButton btnRegresar;
	
	// Menu inferior
	private Button totalPagoMensualBtn;
	private Button guardaroMensualBtn;
	
	// Arriba
	private ImageButton incLineaCredBtn;
	private ImageButton impTarjetaCreditoBtn;
	private ImageButton simPPIBtn;
	private ImageButton simCredAutoBtn;
	private ImageButton simCredHipoBtn;
	private ImageButton simCredNomBtn;
	
	// Abajo
	private ImageButton simPPIDownBtn;
	private ImageButton simCredAutoDownBtn;
	private ImageButton simCredHipoDownBtn;
	private ImageButton simCredNomDownBtn;
	private BmovilViewsController parentManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState,SHOW_TITLE, R.layout.activity_menuprincipal);
			
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("creditpglobal", parentManager.estados);
		isOnForeground=true; 
		
		MainController.getInstance().setContext(this);
		MainController.getInstance().setCurrentActivity(this);
		
		init();
	}
	
	private void init() {
		// Tomamos el que hemos guardado en el login si existe
		Session session = Tools.getCurrentSession(); 
		if(session.getCreditos() != null){
			delegate = new MenuPrincipalCreditDelegate();
			delegate.setDataFromCredit(session.getCreditos());
			delegate.setShowCreditosContratados(session.getShowCreditosContratados());
			delegate.setShowCreditosOfertados(session.getShowCreditosOfertados());
		}else{
			delegate = new MenuPrincipalCreditDelegate();
		}
		mapearBotones();
		scaleToCurrentScreen();
		validarDatos();
		
		checkStates();
		
		session.setVisibilityButton(this, R.id.pGlobalBottomMenuGuardar);
		session.setVisibilityButton(this, R.id.pGlobalBottomMenuRC);
	}
	
	public void checkStates(){
		stateList = delegate.getStates();
		checkContratadosStates();
		checkOfertadosStates();
		delegate.saveStates(stateList);
	}
	
	private void checkContratadosStates(){
		ArrayList<CreditoContratado> creditos = delegate.getCreditosContratados();
		
		Iterator<CreditItem> it = creditosContratados.iterator();
		while(it.hasNext()){
			CreditItem cr = it.next();
			
			if(cr.getArrayIndex() != null){
				if(creditos.get(cr.getArrayIndex()).getIndicadorSim()){
					
					State st = State.OK;
					if(stateList.containsKey(cr.getBoton().getId())){
						if(stateList.get(cr.getBoton().getId()).equals(State.DELETE)) st = State.DELETE;
						stateList.remove(cr.getBoton().getId());
					}
					
					stateList.put(cr.getBoton().getId(), st);
	
					checkBadges(cr.getBoton(),false);
				}else{
					if(stateList.containsKey(cr.getBoton().getId())){
						stateList.remove(cr.getBoton().getId());
					}
					
					stateList.put(cr.getBoton().getId(), State.NORMAL);
	
					checkBadges(cr.getBoton(),false);
				}
			}
		}
	}
	
	private void checkOfertadosStates(){
		ArrayList<Producto> creditos = delegate.getCreditosOfertados();
		
		Iterator<CreditItem> it = creditosOfertados.iterator();
		while(it.hasNext()){
			CreditItem cr = it.next();
			if(cr.getArrayIndex() !=null && cr.getArrayIndex()<creditos.size()){
				if(creditos.get(cr.getArrayIndex()).getIndSimBoolean()){

					State st = State.OK;
					if(stateList.containsKey(cr.getBoton().getId())){

						if(stateList.get(cr.getBoton().getId()).equals(State.DELETE)) st = State.DELETE;
						stateList.remove(cr.getBoton().getId());
					}
					
					stateList.put(cr.getBoton().getId(), st);
	
					checkBadges(cr.getBoton(),false);
				}else{
					if(stateList.containsKey(cr.getBoton().getId())){
						stateList.remove(cr.getBoton().getId());
					}
					
					stateList.put(cr.getBoton().getId(), State.NORMAL);
	
					checkBadges(cr.getBoton(),false);
				}
			}
		}
	}
	
	/**
	 *  Valida los datos de la consulta
	 */
	private void validarDatos(){
		if(!delegate.getShowCreditosContratados()) ocultaContratados();
		
		if(!delegate.getShowCreditosOfertados()) ocultaOfertados();
		
		if((!delegate.getShowCreditosContratados()) && (!delegate.getShowCreditosOfertados())){ 
			// Muestra alert informando de no credito contratado y no credito ofertado (producto)
			showInformationAlert(R.string.label_alert_information_noCredits, new DialogInterface.OnClickListener() {
			// Controlador para redirigir a activity login tras el mensaje 
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				MainController.getInstance().showScreen(LoginActivity.class);
			}
		});
		}
	}
	
	private void ocultaContratados(){
		LinearLayout layout = (LinearLayout) findViewById(R.id.pGlobalActualLayout);
		layout.setVisibility(View.GONE);
		ImageView linea = (ImageView) findViewById(R.id.pGlobalLineaSeparacion);
		linea.setVisibility(View.GONE);
		
		LinearLayout childLayout = (LinearLayout) findViewById(R.id.pGlobalCreditoLayout);
		ViewGroup.LayoutParams params = childLayout.getLayoutParams();
		
		int h = getResources().getDisplayMetrics().heightPixels;
		h = (3*h)/4;
		params.height = h;
		childLayout.requestLayout();
	}
	
	private void ocultaOfertados(){
		LinearLayout layout = (LinearLayout) findViewById(R.id.pGlobalCreditoLayout);
		layout.setVisibility(View.GONE);
		ImageView linea = (ImageView) findViewById(R.id.pGlobalLineaSeparacion);
		linea.setVisibility(View.GONE);
		
		LinearLayout childLayout = (LinearLayout) findViewById(R.id.pGlobalActualLayout);
		ViewGroup.LayoutParams params = childLayout.getLayoutParams();
		
		int h = getResources().getDisplayMetrics().heightPixels;
		h = (3*h)/4;
		params.height = h;
		childLayout.requestLayout();
	}
	
	/**
	 *  Mapea en una clase interna cada boton de creditos ofertados para que sean mas accesibles y modificables
	 */
	private void mapearBotonesCreditosOfertados(){
		
		//ImageButton boton, ImageView badge, TextView subText,	TextView moneyText
		
		CreditItem ci1 = new CreditItem((LinearLayout)findViewById(R.id.pGlobalLayoutLineaCredito),(ImageButton)findViewById(R.id.pGlobalBotonLineaCredito), (ImageView)findViewById(R.id.pGlobalLineaCreditoBadgeImg), (TextView)findViewById(R.id.pGlobalBotonLineaCreditoT1), (TextView)findViewById(R.id.pGlobalBotonLineaCreditoT2));
		creditosOfertados.add(ci1);
		
		CreditItem ci2 = new CreditItem((LinearLayout)findViewById(R.id.pGlobalLayoutTarjCredito),(ImageButton)findViewById(R.id.pGlobalBotonTarjetaCredito), (ImageView)findViewById(R.id.pGlobalTarjetaCreditoBadgeImg), (TextView)findViewById(R.id.pGlobalBotonTarjetaCreditoT1), (TextView)findViewById(R.id.pGlobalBotonTarjetaCreditoT2));
		creditosOfertados.add(ci2);
		
		CreditItem ci3 = new CreditItem((LinearLayout)findViewById(R.id.pGlobalLayoutPPI),(ImageButton)findViewById(R.id.pGlobalBotonSeccionPPI), (ImageView)findViewById(R.id.pGlobalSeccionPPIBadgeImg), (TextView)findViewById(R.id.pGlobalBotonSeccionPPIT1), (TextView)findViewById(R.id.pGlobalBotonSeccionPPIT2));
		creditosOfertados.add(ci3);
		
		CreditItem ci4 = new CreditItem((LinearLayout)findViewById(R.id.pGlobalLayoutLineaCredito2),(ImageButton)findViewById(R.id.pGlobalBotonLineaCredito2), (ImageView)findViewById(R.id.pGlobalLineaCredito2BadgeImg), (TextView)findViewById(R.id.pGlobalBotonLineaCreditoT12), (TextView)findViewById(R.id.pGlobalBotonLineaCreditoT22));
		creditosOfertados.add(ci4);
		
		CreditItem ci5 = new CreditItem((LinearLayout)findViewById(R.id.pGlobalLayoutTarjCredito2),(ImageButton)findViewById(R.id.pGlobalBotonTarjetaCredito2), (ImageView)findViewById(R.id.pGlobalTarjCredito2BadgeImg), (TextView)findViewById(R.id.pGlobalBotonTarjetaCreditoT12), (TextView)findViewById(R.id.pGlobalBotonTarjetaCreditoT22));
		creditosOfertados.add(ci5);
		
		CreditItem ci6 = new CreditItem((LinearLayout)findViewById(R.id.pGlobalLayoutPPI2),(ImageButton)findViewById(R.id.pGlobalBotonSeccionPPI2), (ImageView)findViewById(R.id.pGlobalSeccionPPI2BadgeImg), (TextView)findViewById(R.id.pGlobalBotonSeccionPPIT12), (TextView)findViewById(R.id.pGlobalBotonSeccionPPIT22));
		creditosOfertados.add(ci6);
		
	}
	
	/**
	 *  Mapea en una clase interna cada boton de creditos contratados para que sean mas accesibles y modificables
	 */
	private void mapearBotonesCreditosContratados(){
		
		//ImageButton boton, ImageView badge, TextView subText,	TextView moneyText
		
		CreditItem ci1 = new CreditItem((LinearLayout)findViewById(R.id.pGlobalLayoutSegundaFilaPPI),(ImageButton)findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPI), (ImageView)findViewById(R.id.pGlobalSegundaFilaPPIBadgeImg), (TextView)findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPIT1), (TextView)findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPIT2));
		creditosContratados.add(ci1);
		
		CreditItem ci2 = new CreditItem((LinearLayout)findViewById(R.id.pGlobalLayoutSegundaFilaLineaCredito2),(ImageButton)findViewById(R.id.pGlobalBotonSegundaFilaLineaCredito2), (ImageView)findViewById(R.id.pGlobalSegundaFilaLineaCredito2BadgeImg), (TextView)findViewById(R.id.pGlobalBotonSegundaFilaLineaCreditoT12), (TextView)findViewById(R.id.pGlobalBotonSegundaFilaLineaCreditoT22));
		creditosContratados.add(ci2);
		
		CreditItem ci3 = new CreditItem((LinearLayout)findViewById(R.id.pGlobalLayoutSegundaFilaTarjCredito2),(ImageButton)findViewById(R.id.pGlobalBotonSegundaFilaTarjetaCredito2), (ImageView)findViewById(R.id.pGlobalSegundaFilaTarjetaCredito2BadgeImg), (TextView)findViewById(R.id.pGlobalBotonSegundaFilaTarjetaCreditoT12), (TextView)findViewById(R.id.pGlobalBotonSegundaFilaTarjetaCreditoT22));
		creditosContratados.add(ci3);
		
		CreditItem ci4 = new CreditItem((LinearLayout)findViewById(R.id.pGlobalLayoutSegundaFilaPPI2),(ImageButton)findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPI2), (ImageView)findViewById(R.id.pGlobalSegundaFilaSeccionPPI2BadgeImg), (TextView)findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPIT12), (TextView)findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPIT22));
		creditosContratados.add(ci4);
		
	}
	
	/**
	 * elimina todos los elementos de los creditos. Iconos y texto.
	 * @param lista
	 */
	private void clearCreditItems(ArrayList<CreditItem> lista){
		CreditItem cr;
		for (int i = 0; i < lista.size(); i++) {
			cr = lista.get(i);
			
			cr.getBoton().setBackgroundResource(R.color.trans);
			cr.getMoneyText().setText("");
			cr.getSubText().setText("");
			cr.getLayout().setVisibility(View.GONE);
		}
	}
	
	/**
	 * Genera los botones de creditos a visualizar
	 * @param credOf
	 */
	// TODO: hacer global ?? 
	private void visualizarCreditosOfertados(ArrayList<Producto> credOf){
		if((credOf != null)&&(!credOf.isEmpty())){
			Producto pr;
			CreditItem cr;
			int i = 0;
			while((i < credOf.size()) && (i < creditosOfertados.size())){

				pr = credOf.get(i);
				cr = creditosOfertados.get(i);
				
				String ccode = pr.getCveProd();
				
				cr.getBoton().setBackgroundResource(GuiTools.getCreditIcon(ccode));
				cr.getSubText().setTextColor(getResources().getColor(GuiTools.getCreditColor(ccode)));
				cr.getSubText().setText(getResources().getString(GuiTools.getCreditTitle(ccode)));
				
				// Comprueba si hay subproductos
				if((pr.getSubproducto() != null) && (pr.getSubproducto().size() >= 1)){
					cr.getMoneyText().setText(GuiTools.getMoneyString(pr.getSubproducto().get(0).getMonMax().toString()));
				}
				
				cr.setRedirect(GuiTools.getCreditRedirect(ccode, true));
				cr.getLayout().setVisibility(View.VISIBLE);
				
				cr.setArrayIndex(i);
				
				i++;
			}
		}
	}
	
	private void visualizarCreditosContratados(ArrayList<CreditoContratado> credOf){
		if((credOf != null)&&(!credOf.isEmpty())){
			CreditoContratado crc;
			CreditItem cr;
			int i = 0;
			while((i < credOf.size()) && (i < creditosContratados.size())){

				crc = credOf.get(i);
				cr = creditosContratados.get(i);
				
				String ccode = crc.getCveProd();
				
				cr.getBoton().setBackgroundResource(GuiTools.getCreditIcon(ccode));
				cr.getSubText().setTextColor(getResources().getColor(GuiTools.getCreditColor(ccode)));
				cr.getSubText().setText(getResources().getString(GuiTools.getCreditTitle(ccode)));
				cr.getMoneyText().setText(GuiTools.getMoneyString(crc.getMontCre().toString()));
				cr.setRedirect(GuiTools.getCreditRedirect(ccode, false));
				cr.getLayout().setVisibility(View.VISIBLE);
				
				cr.setArrayIndex(i);
				i++;
			}
		}
	}
	
	private void mapearBotonesCreditos(){
		mapearBotonesCreditosOfertados();
		mapearBotonesCreditosContratados();
		
		ArrayList<Producto> credOf = delegate.getCreditosOfertados();
		ArrayList<CreditoContratado> credCon = delegate.getCreditosContratados();
		
		clearCreditItems(creditosOfertados);
		clearCreditItems(creditosContratados);
		
		visualizarCreditosOfertados(credOf);
		visualizarCreditosContratados(credCon);
	}
	
	private void mapearBotones(){
		
		refreshBtn = (ImageButton)findViewById(R.id.pGlobalHeaderRefresh);
		refreshBtn.setOnClickListener(this);
		
		btnRegresar = (ImageButton) findViewById(R.id.pGlobalHeaderRegresar);
		btnRegresar.setOnClickListener(this);
		
		/******/
		incLineaCredBtn = (ImageButton)findViewById(R.id.pGlobalBotonLineaCredito);
		incLineaCredBtn.setOnClickListener(this);
		incLineaCredBtn.setOnLongClickListener(this);
		
		impTarjetaCreditoBtn = (ImageButton)findViewById(R.id.pGlobalBotonTarjetaCredito);
		impTarjetaCreditoBtn.setOnClickListener(this);
		impTarjetaCreditoBtn.setOnLongClickListener(this);
		/******/
		/** Arriba **/
		
		simPPIBtn = (ImageButton)findViewById(R.id.pGlobalBotonSeccionPPI);
		simPPIBtn.setOnClickListener(this);
		simPPIBtn.setOnLongClickListener(this);
		
		simCredAutoBtn = (ImageButton)findViewById(R.id.pGlobalBotonLineaCredito2);
		simCredAutoBtn.setOnClickListener(this);
		simCredAutoBtn.setOnLongClickListener(this);
		
		simCredHipoBtn = (ImageButton)findViewById(R.id.pGlobalBotonTarjetaCredito2);
		simCredHipoBtn.setOnClickListener(this);
		simCredHipoBtn.setOnLongClickListener(this);
		
		simCredNomBtn = (ImageButton)findViewById(R.id.pGlobalBotonSeccionPPI2);
		simCredNomBtn.setOnClickListener(this);
		simCredNomBtn.setOnLongClickListener(this);
		/** **/
		/** Abajo **/
		simPPIDownBtn = (ImageButton)findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPI);
		simPPIDownBtn.setOnClickListener(this);
		simPPIDownBtn.setOnLongClickListener(this);
		
		simCredAutoDownBtn = (ImageButton)findViewById(R.id.pGlobalBotonSegundaFilaLineaCredito2);
		simCredAutoDownBtn.setOnClickListener(this);
		simCredAutoDownBtn.setOnLongClickListener(this);

		simCredHipoDownBtn = (ImageButton)findViewById(R.id.pGlobalBotonSegundaFilaTarjetaCredito2);
		simCredHipoDownBtn.setOnClickListener(this);
		simCredHipoDownBtn.setOnLongClickListener(this);
		
		simCredNomDownBtn = (ImageButton)findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPI2);
		simCredNomDownBtn.setOnClickListener(this);
		simCredNomDownBtn.setOnLongClickListener(this);
		/** **/
		/**Men� abajo **/
		totalPagoMensualBtn = (Button)findViewById(R.id.pGlobalBottomMenuRC);
		totalPagoMensualBtn.setOnClickListener(this);
		
		guardaroMensualBtn = (Button)findViewById(R.id.pGlobalBottomMenuGuardar);
		guardaroMensualBtn.setOnClickListener(this);
		
		// Mapeamos los botones de todos los cr�ditos
		mapearBotonesCreditos();
	}
	

	/**
	 * Escala los elementos de la pantalla para la resoluci�n actual.
	 */
	private void scaleToCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.pGlobalScrollMainLayout));
		guiTools.scale(findViewById(R.id.pGlobalMainLayout));
		
		guiTools.scale(findViewById(R.id.pGlobalPreHeaderLayout));
		guiTools.scale(findViewById(R.id.pGlobalHeaderLayout));
		guiTools.scale(findViewById(R.id.pGlobalHeaderBtn));
		// Alberto
		//guiTools.scale(findViewById(R.id.pGlobalHeaderLogo));
		guiTools.scale(findViewById(R.id.pGlobalHeaderRefresh));
		guiTools.scale(findViewById(R.id.pGlobalHeaderRegresar));
		
		guiTools.scale(findViewById(R.id.pGlobalCreditoLayout));
		guiTools.scale(findViewById(R.id.pGlobalCreditoTitle),true);
		guiTools.scale(findViewById(R.id.pGlobalCreditoInfoTitle),true);
		// ALberto
		//guiTools.scale(findViewById(R.id.pGlobal_menu_principal_primera_fila));
		guiTools.scale(findViewById(R.id.pGlobalBotonesCreditoLayout));
		guiTools.scale(findViewById(R.id.pGlobalBotonesCreditoLayoutRow1));
		guiTools.scale(findViewById(R.id.pGlobalLayoutLineaCredito));
		guiTools.scale(findViewById(R.id.pGlobalBotonLineaCredito));
		guiTools.scale(findViewById(R.id.pGlobalBotonLineaCreditoT1),true);
		guiTools.scale(findViewById(R.id.pGlobalBotonLineaCreditoT2),true);
		guiTools.scale(findViewById(R.id.pGlobalLayoutTarjCredito));
		guiTools.scale(findViewById(R.id.pGlobalBotonTarjetaCredito));
		guiTools.scale(findViewById(R.id.pGlobalBotonTarjetaCreditoT1),true);
		guiTools.scale(findViewById(R.id.pGlobalBotonTarjetaCreditoT2),true);
		guiTools.scale(findViewById(R.id.pGlobalLayoutPPI));
		guiTools.scale(findViewById(R.id.pGlobalBotonSeccionPPI));
		guiTools.scale(findViewById(R.id.pGlobalBotonSeccionPPIT1),true);
		guiTools.scale(findViewById(R.id.pGlobalBotonSeccionPPIT2),true);

		guiTools.scale(findViewById(R.id.pGlobalBotonesCreditoLayoutRow2));
		guiTools.scale(findViewById(R.id.pGlobalLayoutLineaCredito2));
		guiTools.scale(findViewById(R.id.pGlobalBotonLineaCredito2));
		guiTools.scale(findViewById(R.id.pGlobalBotonLineaCreditoT12),true);
		guiTools.scale(findViewById(R.id.pGlobalBotonLineaCreditoT22),true);
		guiTools.scale(findViewById(R.id.pGlobalLayoutTarjCredito2));
		guiTools.scale(findViewById(R.id.pGlobalBotonTarjetaCredito2));
		guiTools.scale(findViewById(R.id.pGlobalBotonTarjetaCreditoT12),true);
		guiTools.scale(findViewById(R.id.pGlobalBotonTarjetaCreditoT22),true);
		guiTools.scale(findViewById(R.id.pGlobalLayoutPPI2));
		guiTools.scale(findViewById(R.id.pGlobalBotonSeccionPPI2));
		guiTools.scale(findViewById(R.id.pGlobalBotonSeccionPPIT12),true);
		guiTools.scale(findViewById(R.id.pGlobalBotonSeccionPPIT22),true);
		
		guiTools.scale(findViewById(R.id.pGlobalLineaSeparacion));
		
		guiTools.scale(findViewById(R.id.pGlobalActualLayout));
		guiTools.scale(findViewById(R.id.pGlobalActualTitle),true);
		guiTools.scale(findViewById(R.id.pGlobalActualInfoTitle),true);
		
		guiTools.scale(findViewById(R.id.pGlobalBotonesCreditoLayoutSegundaFila));
		guiTools.scale(findViewById(R.id.pGlobalBotonesCreditoLayoutSegundaFilaRow1));
		guiTools.scale(findViewById(R.id.pGlobalLayoutSegundaFilaPPI));
		guiTools.scale(findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPI));
		guiTools.scale(findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPIT1),true);
		guiTools.scale(findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPIT2),true);

		guiTools.scale(findViewById(R.id.pGlobalBotonesCreditoLayoutSegundaFilaRow2));
		guiTools.scale(findViewById(R.id.pGlobalLayoutSegundaFilaLineaCredito2));
		guiTools.scale(findViewById(R.id.pGlobalBotonSegundaFilaLineaCredito2));
		guiTools.scale(findViewById(R.id.pGlobalBotonSegundaFilaLineaCreditoT12),true);
		guiTools.scale(findViewById(R.id.pGlobalBotonSegundaFilaLineaCreditoT22),true);
		guiTools.scale(findViewById(R.id.pGlobalLayoutSegundaFilaTarjCredito2));
		guiTools.scale(findViewById(R.id.pGlobalBotonSegundaFilaTarjetaCredito2));
		guiTools.scale(findViewById(R.id.pGlobalBotonSegundaFilaTarjetaCreditoT12),true);
		guiTools.scale(findViewById(R.id.pGlobalBotonSegundaFilaTarjetaCreditoT22),true);
		guiTools.scale(findViewById(R.id.pGlobalLayoutSegundaFilaPPI2));
		guiTools.scale(findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPI2));
		guiTools.scale(findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPIT12),true);
		guiTools.scale(findViewById(R.id.pGlobalBotonSegundaFilaSeccionPPIT22),true);
		
		guiTools.scale(findViewById(R.id.pGlobalBottomLayout));
		guiTools.scale(findViewById(R.id.pGlobalBottomMenuGuardar));
		guiTools.scale(findViewById(R.id.pGlobalBottomMenuRC));
		
		// Test
		// Arriba 1
		guiTools.scale(findViewById(R.id.pGlobalLineaCreditoFrame));
		guiTools.scale(findViewById(R.id.pGlobalLineaCreditoBadge));
		guiTools.scale(findViewById(R.id.pGlobalLineaCreditoBadgeImg));
		guiTools.scale(findViewById(R.id.pGlobalTarjetaCreditoFrame));
		guiTools.scale(findViewById(R.id.pGlobalTarjetaCreditoBadge));
		guiTools.scale(findViewById(R.id.pGlobalTarjetaCreditoBadgeImg));
		guiTools.scale(findViewById(R.id.pGlobalSeccionPPI));
		guiTools.scale(findViewById(R.id.pGlobalSeccionPPIBadge));
		guiTools.scale(findViewById(R.id.pGlobalSeccionPPIBadgeImg));
		
		// Arriba 2
		guiTools.scale(findViewById(R.id.pGlobalLineaCredito2Frame));
		guiTools.scale(findViewById(R.id.pGlobalLineaCredito2Badge));
		guiTools.scale(findViewById(R.id.pGlobalLineaCredito2BadgeImg));
		guiTools.scale(findViewById(R.id.pGlobalTarjCredito2Frame));
		guiTools.scale(findViewById(R.id.pGlobalTarjCredito2Badge));
		guiTools.scale(findViewById(R.id.pGlobalTarjCredito2BadgeImg));
		guiTools.scale(findViewById(R.id.pGlobalSeccionPPI2Frame));
		guiTools.scale(findViewById(R.id.pGlobalSeccionPPI2Badge));
		guiTools.scale(findViewById(R.id.pGlobalSeccionPPI2BadgeImg));
		
		// Abajo 1
		guiTools.scale(findViewById(R.id.pGlobalSegundaFilaPPIFrame));
		guiTools.scale(findViewById(R.id.pGlobalSegundaFilaPPIBadge));
		guiTools.scale(findViewById(R.id.pGlobalSegundaFilaPPIBadgeImg));
		
		// Abajo 2
		guiTools.scale(findViewById(R.id.pGlobalSegundaFilaLineaCredito2Frame));
		guiTools.scale(findViewById(R.id.pGlobalSegundaFilaLineaCredito2Badge));
		guiTools.scale(findViewById(R.id.pGlobalSegundaFilaLineaCredito2BadgeImg));
		guiTools.scale(findViewById(R.id.pGlobalSegundaFilaTarjetaCredito2Frame));
		guiTools.scale(findViewById(R.id.pGlobalSegundaFilaTarjetaCredito2Badge));
		guiTools.scale(findViewById(R.id.pGlobalSegundaFilaTarjetaCredito2BadgeImg));
		guiTools.scale(findViewById(R.id.pGlobalSegundaFilaSeccionPPI2Frame));
		guiTools.scale(findViewById(R.id.pGlobalSegundaFilaSeccionPPI2Badge));
		guiTools.scale(findViewById(R.id.pGlobalSegundaFilaSeccionPPI2BadgeImg));
	}

	@Override
	public void onClick(View v) {
		isOnForeground=false;
		// TODO Auto-generated method stub
		CreditItem item = getCreditItemById(v.getId());
		
		// Linea Credito
		if((v.getId() == R.id.pGlobalBotonLineaCredito)){
			if(item != null){
				
				delegate.addProductId(item.getArrayIndex(),false);
				if(stateList.get(v.getId()).equals(State.DELETE)){
					CreditItem itC = getCreditItemContratadoById(v.getId());
					CreditItem itO = getCreditItemOfertadoById(v.getId());
					if(itC!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation, new OwnClickListener(itC, true,this));
					}else if(itO!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation,  new OwnClickListener(itO, false,this));
					}
				}else{
					Map<String,Object> click_paso1_operacion = new HashMap<String, Object>();
					click_paso1_operacion.put("evento_inicio","event45");
					click_paso1_operacion.put("&&products","simulador;simulador:simulador incremento linea credito");
					click_paso1_operacion.put("eVar12","inicio");
					TrackingHelper.trackInicioSimulador(click_paso1_operacion);
					delegate.redirectToView(item.getRedirect());
				}
			}
		// Refresh
		}else if(v.getId() == R.id.pGlobalHeaderRefresh){
			showYesNoAlertInverseButtons(R.string.label_information, R.string.posicionGlobal_label_refresh, this);
		
		// TarjetaCredito
		}else if((v.getId() == R.id.pGlobalBotonTarjetaCredito)){
			if(item != null){
				delegate.addProductId(item.getArrayIndex(),false);
				if(stateList.get(v.getId()).equals(State.DELETE)){
					CreditItem itC = getCreditItemContratadoById(v.getId());
					CreditItem itO = getCreditItemOfertadoById(v.getId());
					if(itC!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation, new OwnClickListener(itC, true,this));
					}else if(itO!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation,  new OwnClickListener(itO, false,this));
					}
				}else{
					Log.e("Nómina?","ok");
					Map<String,Object> click_paso1_operacion = new HashMap<String, Object>();
					click_paso1_operacion.put("evento_inicio","event45");
					click_paso1_operacion.put("&&products","simulador;simulador:simulador tarjeta credito");
					click_paso1_operacion.put("eVar12","inicio");
					TrackingHelper.trackInicioSimulador(click_paso1_operacion);
					delegate.redirectToView(item.getRedirect());
				}
			}
		// Arriba PPI
		}else if(v.getId() == R.id.pGlobalBotonSeccionPPI){
			if(item != null){
				delegate.addProductId(item.getArrayIndex(),false);
				if(stateList.get(v.getId()).equals(State.DELETE)){
					CreditItem itC = getCreditItemContratadoById(v.getId());
					CreditItem itO = getCreditItemOfertadoById(v.getId());
					if(itC!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation, new OwnClickListener(itC, true,this));
					}else if(itO!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation,  new OwnClickListener(itO, false,this));
					}
				}else{
					Log.e("Clic sobre PPI?","ok");
					Map<String,Object> click_paso1_operacion = new HashMap<String, Object>();
					click_paso1_operacion.put("evento_inicio","event45");
					click_paso1_operacion.put("&&products","simulador;simulador:simulador prestamo personal");
					click_paso1_operacion.put("eVar12","inicio");
					TrackingHelper.trackInicioSimulador(click_paso1_operacion);
					delegate.redirectToView(item.getRedirect());
				}
			}
		// Arriba Auto
		}else if(v.getId() == R.id.pGlobalBotonLineaCredito2){
			if(item != null){
				delegate.addProductId(item.getArrayIndex(),false);
				if(stateList.get(v.getId()).equals(State.DELETE)){
					CreditItem itC = getCreditItemContratadoById(v.getId());
					CreditItem itO = getCreditItemOfertadoById(v.getId());
					if(itC!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation, new OwnClickListener(itC, true,this));
					}else if(itO!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation,  new OwnClickListener(itO, false,this));
					}
				}else{
					Map<String,Object> click_paso1_operacion = new HashMap<String, Object>();
					click_paso1_operacion.put("evento_inicio","event45");
					click_paso1_operacion.put("&&products","simulador;simulador:simulador credito auto");
					click_paso1_operacion.put("eVar12","inicio");
					TrackingHelper.trackInicioSimulador(click_paso1_operacion);
					delegate.redirectToView(item.getRedirect());
				}
			}
		// Arriba Hipot
		}else if(v.getId() == R.id.pGlobalBotonTarjetaCredito2){
			if(item != null){
				delegate.addProductId(item.getArrayIndex(),false);
				if(stateList.get(v.getId()).equals(State.DELETE)){
					CreditItem itC = getCreditItemContratadoById(v.getId());
					CreditItem itO = getCreditItemOfertadoById(v.getId());
					if(itC!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation, new OwnClickListener(itC, true,this));
					}else if(itO!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation,  new OwnClickListener(itO, false,this));
					}
				}else{
					Map<String,Object> click_paso1_operacion = new HashMap<String, Object>();
					click_paso1_operacion.put("evento_inicio","event45");
					click_paso1_operacion.put("&&products","simulador;simulador:simulador credito hipotecario");
					click_paso1_operacion.put("eVar12","inicio");
					TrackingHelper.trackInicioSimulador(click_paso1_operacion);
					delegate.redirectToView(item.getRedirect());
				}
			}
		// Arriba Nomina
		}else if(v.getId() == R.id.pGlobalBotonSeccionPPI2){
			if(item != null){
				delegate.addProductId(item.getArrayIndex(),false);
				if(stateList.get(v.getId()).equals(State.DELETE)){
					CreditItem itC = getCreditItemContratadoById(v.getId());
					CreditItem itO = getCreditItemOfertadoById(v.getId());
					if(itC!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation, new OwnClickListener(itC, true,this));
					}else if(itO!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation,  new OwnClickListener(itO, false,this));
					}
				}else{
					Map<String,Object> click_paso1_operacion = new HashMap<String, Object>();
					click_paso1_operacion.put("evento_inicio","event45");
					click_paso1_operacion.put("&&products","simulador;simulador:simulador credito nomina");
					click_paso1_operacion.put("eVar12","inicio");
					TrackingHelper.trackInicioSimulador(click_paso1_operacion);
					delegate.redirectToView(item.getRedirect());
				}
			}
		// Abajo PPI
		}else if(v.getId() == R.id.pGlobalBotonSegundaFilaSeccionPPI){
			if(item != null){
				delegate.addProductId(item.getArrayIndex(),true);
				if(stateList.get(v.getId()).equals(State.DELETE)){
					CreditItem itC = getCreditItemContratadoById(v.getId());
					CreditItem itO = getCreditItemOfertadoById(v.getId());
					if(itC!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation, new OwnClickListener(itC, true,this));
					}else if(itO!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation,  new OwnClickListener(itO, false,this));
					}
				}else{
					Map<String,Object> click_paso1_operacion = new HashMap<String, Object>();
					click_paso1_operacion.put("evento_realizada","event45");
					click_paso1_operacion.put("&&products","simulador;simulador:simulador prestamo personal liquidacion");
					click_paso1_operacion.put("eVar12","simulacion realizada");
					TrackingHelper.trackSimulacionRealizada(click_paso1_operacion);
					delegate.redirectToView(item.getRedirect());
				}
			}
		// Abajo Auto
		}else if(v.getId() == R.id.pGlobalBotonSegundaFilaLineaCredito2){
			if(item != null){
				delegate.addProductId(item.getArrayIndex(),true);
				if(stateList.get(v.getId()).equals(State.DELETE)){
					CreditItem itC = getCreditItemContratadoById(v.getId());
					CreditItem itO = getCreditItemOfertadoById(v.getId());
					if(itC!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation, new OwnClickListener(itC, true,this));
					}else if(itO!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation,  new OwnClickListener(itO, false,this));
					}
				}else{
					Map<String,Object> click_paso1_operacion = new HashMap<String, Object>();
					click_paso1_operacion.put("evento_realizada","event45");
					click_paso1_operacion.put("&&products","simulador;simulador:simulador credito auto liquidacion");
					click_paso1_operacion.put("eVar12","simulacion realizada");
					TrackingHelper.trackSimulacionRealizada(click_paso1_operacion);
					delegate.redirectToView(item.getRedirect());
				}
			}
		// Abajo Hipo
		}else if(v.getId() == R.id.pGlobalBotonSegundaFilaTarjetaCredito2){
			if(item != null){
				delegate.addProductId(item.getArrayIndex(),true);
				if(stateList.get(v.getId()).equals(State.DELETE)){
					CreditItem itC = getCreditItemContratadoById(v.getId());
					CreditItem itO = getCreditItemOfertadoById(v.getId());
					if(itC!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation, new OwnClickListener(itC, true,this));
					}else if(itO!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation,  new OwnClickListener(itO, false,this));
					}
				}else{
					Map<String,Object> click_paso1_operacion = new HashMap<String, Object>();
					click_paso1_operacion.put("evento_realizada","event45");
					click_paso1_operacion.put("&&products","simulador;simulador:simulador credito hipotecario liquidacion");
					click_paso1_operacion.put("eVar12","simulacion realizada");
					TrackingHelper.trackSimulacionRealizada(click_paso1_operacion);
					delegate.redirectToView(item.getRedirect());
				}
			}
		// Abajo Nomina
		}else if(v.getId() == R.id.pGlobalBotonSegundaFilaSeccionPPI2){
			if(item != null){
				delegate.addProductId(item.getArrayIndex(),true);
				if(stateList.get(v.getId()).equals(State.DELETE)){
					CreditItem itC = getCreditItemContratadoById(v.getId());
					CreditItem itO = getCreditItemOfertadoById(v.getId());
					if(itC!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation, new OwnClickListener(itC, true,this));
					}else if(itO!=null){

						showYesNoAlertInverseButtons(R.string.label_information, R.string.label_alert_information_deleteSimulation,  new OwnClickListener(itO, false,this));
					}
				}else{
					Map<String,Object> click_paso1_operacion = new HashMap<String, Object>();
					click_paso1_operacion.put("evento_realizada","event45");
					click_paso1_operacion.put("&&products","simulador;simulador:simulador prestamo nomina liquidacion");
					click_paso1_operacion.put("eVar12","simulacion realizada");
					TrackingHelper.trackSimulacionRealizada(click_paso1_operacion);
					delegate.redirectToView(item.getRedirect());
				}
			}
		// Menu Abajo
		}else if(v.getId() == R.id.pGlobalBottomMenuRC){
			delegate.redirectToView(ResumenActivity.class);
			
		}else if(v.getId() == R.id.pGlobalBottomMenuGuardar){
			if(delegate.getRegresar()){
				// Bloqueamos la pantalla
				MainController.getInstance().muestraIndicadorActividad("operacion", "conectando");
				delegate.doGuardar(this);
			}
	    }else if(v.getId() == R.id.pGlobalHeaderRegresar){
			if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//flujo bmovil
				//timeout off
				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
				ActivityCompat.finishAffinity(this);
				this.finish();
				Intent i=new Intent(this, StartBmovilInBack.class);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				//i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				this.startActivity(i);
				//return;
			}else {
				delegate.redirectToView(MenuPrincipalViewController.class);
			}
	    }
			
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		// Bloqueamos la pantalla
		MainController.getInstance().muestraIndicadorActividad("operacion", "conectando");
		
		delegate.doEliminar(this);
		
	}

	@Override
	public boolean onLongClick(View v) {
		checkBadges(v,true);
		return true;
	}
	
	private class OwnClickListener implements DialogInterface.OnClickListener{
		
		CreditItem it;
		Boolean isCont = false;
		MenuPrincipalActivity act;

		public OwnClickListener(CreditItem it, Boolean isCont, MenuPrincipalActivity act) {
			super();
			this.it = it;
			this.isCont = isCont;
			this.act = act;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
			// TODO Auto-generated method stub
			if(isCont){

				delegate.changeContSimulated(it.getArrayIndex(),act);
			}else{

				delegate.changeOfertSimulated(it.getArrayIndex(),act);
			}
		}
		
	}
	
	private void changeBadge(ImageView img, int id){
		
		if(stateList.get(id) == State.OK){
			img.setBackgroundResource(R.drawable.icon_producto_desaprobado);
			stateList.put(id, State.DELETE);
			
		}else if(stateList.get(id) == State.DELETE){
			
			
		}/*else{
			img.setBackgroundResource(android.graphics.Color.TRANSPARENT);
			stateList.put(id, State.NORMAL);
		}*/
	}
	
	private void changeBadgeOnResponse(ImageView img, int id){
		
		if(stateList.get(id) == State.OK){
			img.setBackgroundResource(R.drawable.icon_producto_aprobado);			
		}else if(stateList.get(id) == State.DELETE){
			img.setBackgroundResource(R.drawable.icon_producto_desaprobado);
		}else{
			img.setBackgroundResource(android.graphics.Color.TRANSPARENT);
		}
	}
	
	private void checkBadges(View v, Boolean isClick){
		ImageView img;
		// Arriba Linea Credito
		if(v.getId() == R.id.pGlobalBotonLineaCredito){
			img = (ImageView)findViewById(R.id.pGlobalLineaCreditoBadgeImg);
			if(isClick){

				changeBadge(img, R.id.pGlobalBotonLineaCredito);
			}else{

				changeBadgeOnResponse(img, R.id.pGlobalBotonLineaCredito);
			}

		// Arriba Tarjeta de credito
		}else if(v.getId() == R.id.pGlobalBotonTarjetaCredito)
		{
			img = (ImageView)findViewById(R.id.pGlobalTarjetaCreditoBadgeImg);
			if(isClick){

				changeBadge(img, R.id.pGlobalBotonTarjetaCredito);
			}else{

				changeBadgeOnResponse(img, R.id.pGlobalBotonTarjetaCredito);
			}
			
		// Arriba PPI
		}else if(v.getId() == R.id.pGlobalBotonSeccionPPI){
			img = (ImageView)findViewById(R.id.pGlobalSeccionPPIBadgeImg);
			if(isClick){

				changeBadge(img, R.id.pGlobalBotonSeccionPPI);
			}else{

				changeBadgeOnResponse(img, R.id.pGlobalBotonSeccionPPI);
			}

		// Arriba Auto
		}else if(v.getId() == R.id.pGlobalBotonLineaCredito2){
			img = (ImageView)findViewById(R.id.pGlobalLineaCredito2BadgeImg);
			if(isClick){

				changeBadge(img, R.id.pGlobalBotonLineaCredito2);
			}else{

				changeBadgeOnResponse(img, R.id.pGlobalBotonLineaCredito2);
			}

		// Arriba Hipot
		}else if(v.getId() == R.id.pGlobalBotonTarjetaCredito2){
			img = (ImageView)findViewById(R.id.pGlobalTarjCredito2BadgeImg);
			if(isClick){

				changeBadge(img, R.id.pGlobalBotonTarjetaCredito2);
			}else{

				changeBadgeOnResponse(img, R.id.pGlobalBotonTarjetaCredito2);
			}

		// Arriba Nomina
		}else if(v.getId() == R.id.pGlobalBotonSeccionPPI2){
			img = (ImageView)findViewById(R.id.pGlobalSeccionPPI2BadgeImg);
			if(isClick){

				changeBadge(img, R.id.pGlobalBotonSeccionPPI2);
			}else{

				changeBadgeOnResponse(img, R.id.pGlobalBotonSeccionPPI2);
			}

		// Abajo PPI
		}else if(v.getId() == R.id.pGlobalBotonSegundaFilaSeccionPPI){
			img = (ImageView)findViewById(R.id.pGlobalSegundaFilaPPIBadgeImg);
			if(isClick){

				changeBadge(img,R.id.pGlobalBotonSegundaFilaSeccionPPI);
			}else{

				changeBadgeOnResponse(img, R.id.pGlobalBotonSegundaFilaSeccionPPI);
			}
		
		// Abajo Auto
		}else if(v.getId() == R.id.pGlobalBotonSegundaFilaLineaCredito2){
			img = (ImageView)findViewById(R.id.pGlobalSegundaFilaLineaCredito2BadgeImg);
			if(isClick){

				changeBadge(img, R.id.pGlobalBotonSegundaFilaLineaCredito2);
			}else{

				changeBadgeOnResponse(img, R.id.pGlobalBotonSegundaFilaLineaCredito2);
			}
		
		// Abajo Hipo
		}else if(v.getId() == R.id.pGlobalBotonSegundaFilaTarjetaCredito2){
			img = (ImageView)findViewById(R.id.pGlobalSegundaFilaTarjetaCredito2BadgeImg);
			if(isClick){

				changeBadge(img, R.id.pGlobalBotonSegundaFilaTarjetaCredito2);
			}else{

				changeBadgeOnResponse(img, R.id.pGlobalBotonSegundaFilaTarjetaCredito2);
			}
		
		// Abajo Nomina
		}else if(v.getId() == R.id.pGlobalBotonSegundaFilaSeccionPPI2){
			img = (ImageView)findViewById(R.id.pGlobalSegundaFilaSeccionPPI2BadgeImg);
			if(isClick){

				changeBadge(img, R.id.pGlobalBotonSegundaFilaSeccionPPI2);
			}else{

				changeBadgeOnResponse(img, R.id.pGlobalBotonSegundaFilaSeccionPPI2);
			}
			
		}
	}
	

	@Override    
    public boolean onKeyDown(int keyCode, KeyEvent event) { 

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			isOnForeground = false;
				if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session

					SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
					ActivityCompat.finishAffinity(this);

					Intent i=new Intent(this, StartBmovilInBack.class);
					//i.putExtra(bancomer.api.common.commons.Constants.RESULT_STRING, Constants.FALSE_STRING);
					//i.putExtra( bancomer.api.common.commons.Constants.TIMEOUT_STRING, Constants.FALSE_STRING);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
					finish();
				}else {
    				delegate.redirectToView(MenuPrincipalViewController.class);
				}
            	return true; 	
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}		
    }
	

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if(!isScreenOn() || (!this.isFinishing()&&isOnForeground))
		{

			SuiteApp.getInstance().closeBmovilAppSession();
		}
	}
	
}
