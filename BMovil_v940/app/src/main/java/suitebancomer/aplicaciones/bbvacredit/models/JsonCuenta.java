package suitebancomer.aplicaciones.bbvacredit.models;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import android.content.res.Resources;

import com.bancomer.mbanking.R;

public class JsonCuenta {

	private String numeroTarjeta;
	
	private String balance;
	
	private String visible;
	
	private String moneda;
	
	private String tipoCuenta;
	
	private String concepto;
	
	private String alias;
	
	private String celularAsociado;
	
	private String codigoCompania;
	
	private String descripcionCompania;
	
	private String fechaUltimaModificacion;
	
	private String indicadorSPEI;
	
	private String ocurrencia;
	
	
	public JsonCuenta() {
	}

	public JsonCuenta(String numeroTarjeta, String balance, String visible,
			String moneda, String tipoCuenta, String concepto, String alias,
			String celularAsociado, String codigoCompania,
			String descripcionCompania, String fechaUltimaModificacion,
			String indicadorSPEI, String ocurrencia) {
		super();
		this.numeroTarjeta = numeroTarjeta;
		this.balance = balance;
		this.visible = visible;
		this.moneda = moneda;
		this.tipoCuenta = tipoCuenta;
		this.concepto = concepto;
		this.alias = alias;
		this.celularAsociado = celularAsociado;
		this.codigoCompania = codigoCompania;
		this.descripcionCompania = descripcionCompania;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.indicadorSPEI = indicadorSPEI;
		this.ocurrencia = ocurrencia;
	}


	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}


	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}


	public String getBalance() {
		return balance;
	}


	public void setBalance(String balance) {
		this.balance = balance;
	}


	public String getVisible() {
		return visible;
	}


	public void setVisible(String visible) {
		this.visible = visible;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getTipoCuenta() {
		return tipoCuenta;
	}


	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}


	public String getConcepto() {
		return concepto;
	}


	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}


	public String getAlias() {
		return alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public String getCelularAsociado() {
		return celularAsociado;
	}


	public void setCelularAsociado(String celularAsociado) {
		this.celularAsociado = celularAsociado;
	}


	public String getCodigoCompania() {
		return codigoCompania;
	}


	public void setCodigoCompania(String codigoCompania) {
		this.codigoCompania = codigoCompania;
	}


	public String getDescripcionCompania() {
		return descripcionCompania;
	}


	public void setDescripcionCompania(String descripcionCompania) {
		this.descripcionCompania = descripcionCompania;
	}


	public String getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}


	public void setFechaUltimaModificacion(String fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}


	public String getIndicadorSPEI() {
		return indicadorSPEI;
	}


	public void setIndicadorSPEI(String indicadorSPEI) {
		this.indicadorSPEI = indicadorSPEI;
	}


	public String getOcurrencia() {
		return ocurrencia;
	}


	public void setOcurrencia(String ocurrencia) {
		this.ocurrencia = ocurrencia;
	}


	public String getNombreTipoCuenta(Resources res) {
    	String typeName = "";
    	if (ConstantsCredit.CHECK_TYPE.equals(tipoCuenta)) {
            typeName = res.getString(R.string.accounttype_check);
        } else if (ConstantsCredit.CREDIT_TYPE.equals(tipoCuenta)) {
            typeName = res.getString(R.string.accounttype_credit);
        } else if (ConstantsCredit.LIBRETON_TYPE.equals(tipoCuenta)) {
            typeName = res.getString(R.string.accounttype_libreton);
        } else if (ConstantsCredit.SAVINGS_TYPE.equals(tipoCuenta)) {
            typeName = res.getString(R.string.accounttype_savings);
        } else if (ConstantsCredit.EXPRESS_TYPE.equals(tipoCuenta)) {
            typeName = res.getString(R.string.accounttype_express);
        } else if (ConstantsCredit.CLABE_TYPE_ACCOUNT.equals(tipoCuenta)) {
	       	typeName = res.getString(R.string.accounttype_clabe);
        } else if (ConstantsCredit.PREPAID_TYPE.equals(tipoCuenta)) {
        	typeName = res.getString(R.string.accounttype_prepaid);
        }
    	
    	return typeName;
    }
}
