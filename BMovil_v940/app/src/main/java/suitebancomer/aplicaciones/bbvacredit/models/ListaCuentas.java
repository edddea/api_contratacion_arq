package suitebancomer.aplicaciones.bbvacredit.models;

import java.util.List;

public class ListaCuentas {
	
	private List<Cuenta> lista;

	public List<Cuenta> getLista() {
		return lista;
	}

	public void setLista(List<Cuenta> lista) {
		this.lista = lista;
	}

	public ListaCuentas(List<Cuenta> lista) {
		super();
		this.lista = lista;
	}

	public ListaCuentas() {
		// TODO Auto-generated constructor stub
	}
}
