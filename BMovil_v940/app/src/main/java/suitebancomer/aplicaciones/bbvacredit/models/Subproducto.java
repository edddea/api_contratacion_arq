package suitebancomer.aplicaciones.bbvacredit.models;

import java.util.ArrayList;

public class Subproducto {
	
	private String cveSubp;
	
	private String desSubp;
	
	private String numTDC;
	
	private Integer monMax;
	
	private Integer monMin;
	
	private ArrayList<Plazo> plazo;
	
	private Integer tasa;
	
	private Double CAT;
	
	private String fechCat;

	public Subproducto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Subproducto(String cveSubp, String desSubp, String numTDC,
			Integer monMax, Integer monMin, ArrayList<Plazo> plazo,
			Integer tasa, Double cAT, String fechCat) {
		super();
		this.cveSubp = cveSubp;
		this.desSubp = desSubp;
		this.numTDC = numTDC;
		this.monMax = monMax;
		this.monMin = monMin;
		this.plazo = plazo;
		this.tasa = tasa;
		CAT = cAT;
		this.fechCat = fechCat;
	}

	public String getCveSubp() {
		return cveSubp;
	}

	public void setCveSubp(String cveSubp) {
		this.cveSubp = cveSubp;
	}

	public String getDesSubp() {
		return desSubp;
	}

	public void setDesSubp(String desSubp) {
		this.desSubp = desSubp;
	}

	public String getNumTDC() {
		return numTDC;
	}

	public void setNumTDC(String numTDC) {
		this.numTDC = numTDC;
	}

	public Integer getMonMax() {
		return monMax;
	}

	public void setMonMax(Integer monMax) {
		this.monMax = monMax;
	}

	public Integer getMonMin() {
		return monMin;
	}

	public void setMonMin(Integer monMin) {
		this.monMin = monMin;
	}

	public ArrayList<Plazo> getPlazo() {
		return plazo;
	}

	public void setPlazo(ArrayList<Plazo> plazo) {
		this.plazo = plazo;
	}

	public Integer getTasa() {
		return tasa;
	}

	public void setTasa(Integer tasa) {
		this.tasa = tasa;
	}

	public Double getCAT() {
		return CAT;
	}

	public void setCAT(Double cAT) {
		CAT = cAT;
	}

	public String getFechCat() {
		return fechCat;
	}

	public void setFechCat(String fechCat) {
		this.fechCat = fechCat;
	}

}
