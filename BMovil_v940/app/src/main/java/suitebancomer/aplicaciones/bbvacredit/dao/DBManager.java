package suitebancomer.aplicaciones.bbvacredit.dao;

import java.util.HashMap;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import android.database.Cursor;

public class DBManager {
	
	private static DBManager theInstance = null;
	private DataBase dataBase;
	
	private DBManager() {
		dataBase = new DataBase(MainController.getInstance().getContext());
	}

	public static DBManager getInstance() {
		if (theInstance == null) {
			theInstance = new DBManager();
		}
		
		return theInstance;
	}
	
	/**
	 * se guardan los datos en la tabla
	 */
	public void guardarDatosDB(String identificador, String valor, String table) {

		dataBase.open();
		dataBase.insertGenericEntry(table, identificador, valor);
		dataBase.close();

	}

	/**
	 * se cargan los datos de la tabla
	 */
	public String cargarDatosDB(String identificador, String table) {

		String perfil = "";
		dataBase.open();
		Cursor consulta = dataBase.selectGenericEntry(table, identificador);
		if (consulta.moveToNext()) {
			consulta.moveToNext();
			perfil = consulta.getString(consulta
					.getColumnIndex(ConstantsCredit.VALOR));
		}
		dataBase.close();

		return perfil;

	}

	/**
	 * se actualizan los datos de la tabla
	 */
	public void actualizarDatosDB(String identificador, String nuevoValor, String table) {

		dataBase.open();
		dataBase.updateGenericEntry(table, identificador, nuevoValor);
		dataBase.close();

	}
	
	/**
	 * se elimina los datos de la tabla para un identificador
	 */
	public void eliminarDatosDB(String identificador, String table) {

		dataBase.open();
		dataBase.deleteGenericEntry(identificador, table);
		dataBase.close();

	}

	/**
	 * se guarda la lista de datos en la tabla
	 */
	public void guardarListaDatosDB(HashMap<String, Object> mapNameValue,
			String table) {
		
		dataBase.open();
		dataBase.insertAllGenericEntries(table, mapNameValue);
		dataBase.close();
		
	}

	
	/**
	 * se carga la lista de datos de la tabla
	 */
	public HashMap<String, Object> cargarListaDatosDB(String table) {

	
		
		HashMap<String, Object> mapNameValue = new HashMap<String, Object>();
		dataBase.open();
		Cursor consulta = dataBase.selectGenericAllEntry(table);
		
		while(consulta.moveToNext()){

			mapNameValue.put(consulta.getString(consulta
					.getColumnIndex(ConstantsCredit.IDENTIFICADOR)), consulta.getString(consulta
					.getColumnIndex(ConstantsCredit.VALOR)));
		
		}
		dataBase.close();

		return mapNameValue;
	}

	/**
	 * se eliminan todos los datos de la tabla
	 */
	public void eliminarTodosDatosDB(String table) {
		
		dataBase.open();
		dataBase.deleteAllGenericEntries(table);
		dataBase.close();
		
	}

	
	/**
	 * se actualiza una lista de datos en la tabla
	 */
	public void actualizarListaDatosDB(HashMap<String, Object> mapNameValue,
			String table) {
		dataBase.open();
		dataBase.updateAllGenericEntries(table, mapNameValue);
		dataBase.close();
		
	}
}
