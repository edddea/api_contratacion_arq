package suitebancomer.aplicaciones.bbvacredit.gui.delegates;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.activities.LineaCreditoActivity;
import suitebancomer.aplicaciones.bbvacredit.io.AuxConectionFactoryCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerResponseCredit;
import suitebancomer.aplicaciones.bbvacredit.models.CalculoData;
import suitebancomer.aplicaciones.bbvacredit.models.ConsultaDatosTDCData;
import suitebancomer.aplicaciones.bbvacredit.models.ObjetoCreditos;
import suitebancomer.aplicaciones.bbvacredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import android.content.SharedPreferences;
import android.util.Log;

import com.bancomer.mbanking.R;

public class LineaCreditoDelegate extends BaseDelegateOperacion{
	
	private Session session;
	
	private boolean isSaveOperation = false;
	
	private Boolean ocultarInfo = false;
	
	private ObjetoCreditos data;
	
	private Producto producto = null;

	private Integer montoMin = 0;
	
	private Integer montoMax = 0;
	
	private Integer montoProgress = 0;
	
	// Indice de producto dentro del array
	private Integer productIndex = 0;
	
	private LineaCreditoActivity actContrato; 

	private ArrayList<Producto> productTable = new ArrayList<Producto>();
	
	private ConsultaDatosTDCData dataTDC;
	
	public Integer getMontoProgress() {
		return montoProgress;
	}

	public void setMontoProgress(Integer montoProgress) {
		this.montoProgress = montoProgress;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public ArrayList<Producto> getProductTable() {
		return productTable;
	}

	public void setProductTable(ArrayList<Producto> productTable) {
		this.productTable = productTable;
	}

	public LineaCreditoActivity getActContrato() {
		return actContrato;
	}

	public void setActContrato(LineaCreditoActivity actContrato) {
		this.actContrato = actContrato;
	}

	public Integer getMontoMin() {
		return montoMin;
	}

	public void setMontoMin(Integer montoMin) {
		this.montoMin = montoMin;
	}

	public Integer getMontoMax() {
		return montoMax;
	}

	public void setMontoMax(Integer montoMax) {
		this.montoMax = montoMax;
	}

	public Boolean getOcultarInfo() {
		return ocultarInfo;
	}

	public void setOcultarInfo(Boolean ocultarInfo) {
		this.ocultarInfo = ocultarInfo;
	}

	public ConsultaDatosTDCData getDataTDC() {
		return dataTDC;
	}

	public void setDataTDC(ConsultaDatosTDCData dataTDC) {
		this.dataTDC = dataTDC;
	}

	private void ocultarInfo(){
		// TODO: duda
		if(producto.getIndSimBoolean()){
			ocultarInfo = true;
		}else{
			ocultarInfo = false;
		}
	}
	
	private void setProducto(){
		SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
		productIndex = sp.getInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX, 0);
		
		producto = data.getProductos().get(productIndex);
	}

	public LineaCreditoDelegate() {
		session = Tools.getCurrentSession();
		actContrato = session.getLineaAct();
		// Bloqueamos la pantalla
		MainController.getInstance().muestraIndicadorActividad("operacion", "conectando");
		
		// Mapeamos los datos
		data = session.getDataFromCreditos();
		dataTDC = session.getDataTDC();
		
		setProducto();
		if(producto != null){
			if(!producto.getIndSimBoolean()){
				ocultarInfo();
				doPeticionTDC();
				montoProgress = Double.valueOf(producto.getSubproducto().get(0).getMonMax()).intValue();
			}else{
				productTable = data.getProductos();
				montoProgress = Double.valueOf(producto.getMontoDeseS()).intValue();
			}
			
			// Establecer max y min monto
			montoMin = Double.valueOf(producto.getSubproducto().get(0).getMonMin()).intValue();
			montoMax = Double.valueOf(producto.getSubproducto().get(0).getMonMax()).intValue();
			
		}
		// Desbloqueamos la pantalla
		MainController.getInstance().ocultaIndicadorActividad();
	}
	
	private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}else{
			paramTable.put(cnt, "");
			Log.d(param.getClass().getName(), param.toString()+" empty or null");
		}
	}
	
	private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}
	}
	
	public void doPeticionTDC(){
		// Mapeamos el usuario y la contrase�a
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		// NT
		paramTable.put(ServerConstantsCredit.USERNAME_PARAM, session.getNumCelular());
		// NP
		//se comenta porque arq de servicio no envia el password y se comento en video conferencia, que este parametro ya no se envia
		//paramTable.put(ServerConstantsCredit.PASSWORD_PARAM, session.getPassword());
		// IU - Generamos el IUM
		paramTable.put(ServerConstantsCredit.IUM_ETIQUETA, session.getIum());
		// TODO: duda Tipo de cuenta
		paramTable.put(ServerConstantsCredit.TP, "TC");
		// Numero de cuenta
		paramTable.put(ServerConstantsCredit.AS, producto.getNumTDCS());
		// TODO: periodo ... duda
		paramTable.put(ServerConstantsCredit.PE, "0");

		this.doNetworkOperation(Server.CONSULTA_TDC, paramTable);
	}
	
	public void doRecalculo(){
		// Mapeamos el usuario y la contrase�a
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		
		// Mapeamos el codigo de operacion
		addParametroObligatorio(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);
		
		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
		
		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
		
		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

		// Mapeamos el tipo de operacion -> 2
		addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);
		
		// Falta mapear los parametros no obligatorios

		Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.calculo(paramTable);
		this.doNetworkOperation(getCodigoOperacion(), paramTable2, true , new CalculoData(), MainController.getInstance().getContext());
	}

	public void doRecalculoContract(LineaCreditoActivity actContrato){
		// Mapeamos el activity
		this.actContrato = actContrato;
		
		// Mapeamos el usuario y la contrase�a
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		
		// Mapeamos el codigo de operacion
		addParametroObligatorio(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);
		
		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
		
		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
		
		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
		
		// Mapeamos el tipo de operacion -> 2
		addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);

		// Tomamos el credito contratado y el subproducto asociado
		Producto ccr = data.getProductos().get(productIndex);

		int montoSel = actContrato.getSeekbar().getProgress();

		addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM,paramTable);
		addParametro(ccr.getSubproducto().get(0).getCveSubp(), ServerConstantsCredit.CVESUBP_PARAM,paramTable);
		addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM,paramTable);

		Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.calculo(paramTable);
		this.doNetworkOperation(getCodigoOperacion(), paramTable2,true,new CalculoData(),MainController.getInstance().getContext());
	}
	
	/**
	 * Created June 8th,2015.
	 * @param actContrato
	 */
	
	
	public void saveOrDeleteSimulationRequest(LineaCreditoActivity actContrato, boolean flag)
	{
		// Mapeamos el activity
				this.actContrato = actContrato;
				this.isSaveOperation = flag;
				// Mapeamos el usuario y la contrase�a
				Hashtable<String, String> paramTable = new Hashtable<String, String>();
				
				// Mapeamos el codigo de operacion
				addParametroObligatorio(this.getCodigoOperacionGuardarEliminar(),ServerConstantsCredit.OPERACION_PARAM, paramTable);
				
				// Mapeamos el id de cliente tomado de la sesion
				addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
				
				// Mapeamos el IUM tomado de la sesion
				addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
				
				// Mapeamos el numeroCelular tomado de la sesion
				addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
				
				
				if(isSaveOperation)
				{	// Mapeamos el tipo de operacion -> 3
					addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);
				}
				else
				{	// Mapeamos el tipo de operacion -> 4
					addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);
				}
		Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
		this.doNetworkOperation(getCodigoOperacionGuardarEliminar(), paramTable2,true,new CalculoData(),MainController.getInstance().getContext());
	}
	
	/****/
	private void setSimLooking4Cve(String cve){
		Iterator<Producto> it = data.getProductos().iterator();
		
		while(it.hasNext()){
			Producto pr = it.next();
			if(pr.getCveProd().equals(cve)) pr.setIndSimBoolean(true);
		}
	}
	
	public void analyzeResponse(String operationId, ServerResponseCredit response) {
		if(getCodigoOperacion().equals(operationId)){
    		if(response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL){
    			// Llenar creditos
    			CalculoData respuesta = (CalculoData) response.getResponse();
    			
    			// Informamos el obj creditos con la informaci�n necesaria
    			data.setCreditos(respuesta.getCreditos());
    			data.setEstado(respuesta.getEstado());
    			data.setMontotSol(respuesta.getMontotSol());
    			data.setPagMTot(respuesta.getPagMTot());
    			data.setPorcTotal(respuesta.getPorcTotal());
    			data.setProductos(respuesta.getProductos());
    			
    			// Setear indsim
				productIndex = session.getProductoIndexByCve(ConstantsCredit.INCREMENTO_LINEA_CREDITO);
    			producto = data.getProductos().get(productIndex);
    			if(data.getProductos().get(productIndex).getCveProd().equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){

    				data.getProductos().get(productIndex).setIndSimBoolean(true);
				}else{
					setSimLooking4Cve(ConstantsCredit.INCREMENTO_LINEA_CREDITO);
				}
    			
    			// Desbloqueamos la pantalla
    			MainController.getInstance().ocultaIndicadorActividad();
    			
    			Tools.getCurrentSession().setVisibilityButton(actContrato, R.id.incLCredBottomMenuRC);
    			//actContrato.pintarInfoTabla(null);
    			actContrato.pintarInfoTabla();
    			
    		}
    	}else if(Server.CONSULTA_TDC.equals(operationId)){
    		if(response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL){
    			// Llenar creditos
    			ConsultaDatosTDCData respuesta = (ConsultaDatosTDCData)response.getResponse();
    			session.setDataTDC(respuesta);
    			setDataTDC(respuesta);
    			//actContrato.pintarInfoTabla(respuesta.getSC());
    			actContrato.muestraSaldos(respuesta.getSC());
    			// TODO: que hacemos con el saldo al corte ? 
//    			Log.d("TDC >> PE >> ",respuesta.getPE());
//    			Log.d("TDC >> SC >> ",respuesta.getSC());
    		}
    	}else if(getCodigoOperacionGuardarEliminar().equals(operationId))
    	{
    		if(response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL){
    			if(isSaveOperation)
    				actContrato.oneClickILC();
    			else
    				actContrato.doRecalculo();
    		}
    	}
	}

	public <T> void redirectToView(Class<T> c){
		MainController.getInstance().showScreen(c);
	}

	@Override
	protected String getCodigoOperacion() {
		// TODO Auto-generated method stub
		return Server.CALCULO_OPERACION;
	}
	
	
	protected String getCodigoOperacionGuardarEliminar()
	{
		return Server.CALCULO_ALTERNATIVAS_OPERACION;
	}
	
	
	
	
}
