package suitebancomer.aplicaciones.bbvacredit.io;

public class ServerConstantsCredit {
	public static final int NUM_PROVEEDORES = 3;
	
	public static final String PARAMETER_SEPARATOR = "*";
	public static final String OPERATION_CODE_PARAMETER = "OPERACION";
	public static final String OPERATION_CODE_VALUE = "BAN2O01";
	public static final String OPERATION_CODE_VALUE_BCDR = "BCRD001";
	public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";
	public static final String OPERATION_LOCALE_VALUE = "es_ES";
	public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";
	
	public static final String JSON_ESTADO_RESPUESTA = "estado";
	public static final String JSON_CODIGO_MENSAJE_ETIQUETA = "codigoMensaje";
	public static final String JSON_MENSAJE_INFORMATIVO_ETIQUETA = "descripcionMensaje";
	
	public static final String JSON_ID_OPERACION_ETIQUETA = "operacion";
	public static final String JSON_ID_USUARIO_ETIQUETA = "idUsuario";
	public static final String JSON_CONTRASENA_ETIQUETA = "contrasena";
	public static final String JSON_PERFIL_USUARIO_ETIQUETA = "perfilUsuario";
	
	/* Parametros Login */
	public static final String USERNAME_PARAM = "NT";
	public static final String PASSWORD_PARAM = "NP";
	public static final String IUM_ETIQUETA = "IU";
	public static final String APPLICATION_VERSION = "VM";
	public static final String VERSION_C1 = "C1";
	public static final String VERSION_C4 = "C4";
	public static final String VERSION_C5 = "C5";
	public static final String VERSION_C8 = "C8";
	public static final String MARCA_MODELO = "MM";
	public static final String VERSION_TA = "TA";
	public static final String VERSION_DM = "DM";
	public static final String VERSION_SV = "SV";
	public static final String VERSION_MS = "MS";
	public static final String VERSION_AU = "AU";
	public static final String VERSION_VM = "VM";
	public static final String VERSION_HC = "HC";
	
	/* Parametros calculo */
	public static final String OPERACION_PARAM = "operacion";
	public static final String CLIENTE_PARAM = "cliente";
	public static final String IUM_PARAM = "IUM";
	public static final String NUMERO_CEL_PARAM = "numeroCelular";
	public static final String CVEPROD_PARAM = "CveProd";
	public static final String CVESUBP_PARAM = "CveSubp";
	public static final String CVEPLAZO_PARAM = "CvePalzo";
	public static final String TIPO_OP_PARAM = "tipoOp";
	public static final String MON_DESE_PARAM = "MonDese";
	public static final String CONTRATO_PARAM = "Contrato";
	public static final String PAGO_MENSUAL_PARAM = "PagoMensual";
	public static final String PRODUCTO_PARAM = "producto";
	
	/* Envio Correo */
	
	public static final String IND_S = "S";
	public static final String IND_N = "N";
	
	//contratacion
	public static final String SCN = "SCN";
	public static final String CN_IMPORTE = "CN_IMPORTE";
	public static final String CN_TASA_ANUAL = "CN_TASA_ANUAL";
	public static final String CN_PLAZO = "CN_PLAZO";
	public static final String CN_PAGO_FIJO_MENSUAL = "CN_PAGO_FIJO_MENSUAL";
	
	public static final String SPPI = "SPPI";
	public static final String PP_IMPORTE = "PP_IMPORTE";
	public static final String PP_TASA_ANUAL = "PP_TASA_ANUAL";
	public static final String PP_PLAZO = "PP_PLAZO";
	public static final String PP_PAGO_FIJO_MENSUAL = "PP_PAGO_FIJO_MENSUAL";
	
	public static final String STDC = "STDC";
	public static final String TC_LIMITE_DE_CREDITO_AUTORIZADO = "TC_LIMITE_DE_CREDITO_AUTORIZADO";
	public static final String TC_LIMITE_DE_CREDITO_SIMULADO = "TC_LIMITE_DE_CREDITO_SIMULADO";
	
	public static final String SILDC = "SILDC";
	public static final String IL_TARJETA_DE_CREDITO = "IL_TARJETA_DE_CREDITO";
	public static final String IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA = "IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA";
	public static final String IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA = "IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA";
	
	public static final String SCA = "SCA";
	public static final String CA_IMPORTE = "CA_IMPORTE";
	public static final String CA_TASA_ANUAL = "CA_TASA_ANUAL";
	public static final String CA_PLAZO = "CA_PLAZO";
	public static final String CA_PAGO_FIJO_MENSUAL = "CA_PAGO_FIJO_MENSUAL";
	
	public static final String SCH = "SCH";
	public static final String CH_IMPORTE = "CH_IMPORTE";
	public static final String CH_TASA_ANUAL = "CH_TASA_ANUAL";
	public static final String CH_PLAZO = "CH_PLAZO";
	public static final String CH_PAGO_FIJO_MENSUAL = "CH_PAGO_FIJO_MENSUAL";
	
	public static final String CON_CAT = "CON_CAT";
	public static final String CON_FECHA_CALCULO = "CON_FECHA_CALCULO";
	
	//liquidacion	
	public static final String SCNL = "SCNL";
	public static final String CN_DESC = "CN_DESC";
	public static final String CN_DESC_TITLE = "SIMULACION CREDITO NOMINA";
	public static final String CN_CANT = "CN_CANT";
	
	public static final String SPPIL = "SPPIL";
	public static final String PP_DESC = "PP_DESC";
	public static final String PP_DESC_TITLE = "SIMULACION PRESTAMO PERSONAL INMEDIATO";
	public static final String PP_CANT = "PP_CANT";
	
	public static final String SCHL = "SCHL";
	public static final String CH_DESC = "CH_DESC";
	public static final String CH_DESC_TITLE = "SIMULACION CREDITO HIPOTECARIO";
	public static final String CH_CANT = "CH_CANT";
	
	public static final String SCAL = "SCAL";
	public static final String CA_DESC = "CA_DESC";
	public static final String CA_DESC_TITLE = "SIMULACION CREDITO DE AUTO";
	public static final String CA_CANT = "CA_CANT";
	
	public static final String SP5 = "SP5";
	public static final String P5_DESC = "P5_DESC";
	public static final String P5_CANT = "P5_CANT";
	
	public static final String SP6 = "SP6";
	public static final String P6_DESC = "P6_DESC";
	public static final String P6_CANT = "P6_CANT";
	
	//alternativas
	public static final String SCR ="SCR"; //Indicador de visualización de los campos de nuevas alternativas
	
	public static final String SAN ="SAN";
	public static final String CN = "CN";
	
	public static final String SAP ="SAP";
	public static final String PP = "PP";
	
	public static final String SAI ="SAI";
	public static final String IL = "IL";
	
	public static final String SAH ="SAH";
	public static final String CH = "CH";
	
	public static final String SAA ="SAA";
	public static final String CA = "CA";
	
	public static final String SAT ="SAT";
	public static final String TC = "TC";
	
	//YA NO SON NECESARIOS
	//public static final String LIQ_CAT = "LIQ_CAT";
	//public static final String LIQ_FECHA_CALCULO = "LIQ_FECHA_CALCULO";
	
	public static final String IND_CORREO = "IND_CORREO";
	public static final String email = "email";
	
	/* Parametros envio correo */
	public static final String TP = "TP";
	public static final String AS = "AS";
	public static final String PE = "PE";
	
	/* Parametros TDC */
	
}
