package suitebancomer.aplicaciones.bbvacredit.gui.activities;

import suitebancomer.aplicaciones.bmovil.classes.io.Server;

import com.bancomer.mbanking.R;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;

public class PopUpContactameActivity extends BaseActivity{

  public static void muestraPop(Context context, String params){
	
    final ProgressDialog pd = ProgressDialog.show(context, "", "Cargando..." , true);

	final Dialog dialog = new Dialog(context);
	dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	dialog.setCancelable(false);
	dialog.setContentView(R.layout.pop_contactame);
			
	//TextView txtMensaje = (TextView) dialog.findViewById(R.id.popContactameTitle);
	Button btnAceptar = (Button) dialog.findViewById(R.id.popContactameBtnAceptar);
	
	final WebView mWebView = (WebView) dialog.findViewById(R.id.popContactameWebView);	
	mWebView.getSettings().setJavaScriptEnabled(true);

	
	String url = Server.URL_CONTACTAME + "?" + params;
	mWebView.loadUrl(url);
	
	mWebView.setWebViewClient(new WebViewClient() {
				
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url)
        {
			return false;

		}
		
		@Override
        public void onPageStarted(WebView view, String url, Bitmap favicon)
        {
            pd.show();
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            pd.dismiss();

               String webUrl = mWebView.getUrl();

        }

	
	});
	
	btnAceptar.setOnClickListener(new OnClickListener() {
		@Override
		public void onClick(View v) {
			dialog.cancel();
		}	
		
	});
  
	dialog.show();
   }

}
