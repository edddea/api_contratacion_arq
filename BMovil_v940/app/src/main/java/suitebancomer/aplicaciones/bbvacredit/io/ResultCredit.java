package suitebancomer.aplicaciones.bbvacredit.io;

/**
 * Result wraps the result data of a response (status, error code, error message).
 */
public class ResultCredit {
	/**
     * The status code.
     */
    private String status;

    /**
     * The error code.
     */
    private String code;

    /**
     * The error message.
     */
    private String message;

    /**
     * The URL for mandatory application update.
     */
    private String updateURL = null;

    /**
     * Default constructor.
     * @param stat the status code
     * @param cod the error code
     * @param msg the error message
     */
    public ResultCredit(String stat, String cod, String msg) {
        this.status = stat;
        this.code = cod;
        this.message = msg;
    }

    /**
     * Default constructor.
     * @param st the status code
     * @param cod the error code
     * @param msg the error message
     * @param urlUpd the URL for mandatory application updating
     */
    public ResultCredit(String st, String cod, String msg, String urlUpd) {
        this.status = st;
        this.code = cod;
        this.message = msg;
        this.updateURL = urlUpd;
    }

    /**
     * Get the status code.
     * @return status code
     */
    public String getStatus() {
        return status;
    }

    /**
     * Get the error code.
     * @return the error code
     */
    public String getCode() {
        return code;
    }

    /**
     * Get the error message.
     * @return the error message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Get the update URL.
     * @return the update URL for mandatory updating
     */
    public String getUpdateURL() {
        return updateURL;
    }
}
