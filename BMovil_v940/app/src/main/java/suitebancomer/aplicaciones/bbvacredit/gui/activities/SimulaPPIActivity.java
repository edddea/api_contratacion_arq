package suitebancomer.aplicaciones.bbvacredit.gui.activities;

import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bbvacredit.common.GuiTools;
import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.DetalleDeAlternativaDelegate;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.SimulaPPIDelegate;
import suitebancomer.aplicaciones.bbvacredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilConstants;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import tracking.TrackingHelper;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class SimulaPPIActivity extends BaseActivity implements OnClickListener, OnSeekBarChangeListener{
	
	private SimulaPPIDelegate delegate;
	private ImageButton refreshBtn;
    private ImageButton btnSimular;
	
	// Menu Button
	private Button resumenContratacionBtn;
	private Button btnAdquirir;
	
	
	// Botones Seekbar
	private Button seekPlus;
	private Button seekMinus;
	private SeekBar seekbar;
	private Spinner spinner;
	
	private boolean isOcultaInfoEnabled;

	public Button getSeekPlus() {
		return seekPlus;
	}

	public void setSeekPlus(Button seekPlus) {
		this.seekPlus = seekPlus;
	}

	public Button getSeekMinus() {
		return seekMinus;
	}

	public void setSeekMinus(Button seekMinus) {
		this.seekMinus = seekMinus;
	}

	public SeekBar getSeekbar() {
		return seekbar;
	}

	public void setSeekbar(SeekBar seekbar) {
		this.seekbar = seekbar;
	}

	public Spinner getSpinner() {
		return spinner;
	}

	public void setSpinner(Spinner spinner) {
		this.spinner = spinner;
	}
	private BmovilViewsController parentManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.activity_simula_ppi);
		isOnForeground = true;
		isOcultaInfoEnabled=false;
		MainController.getInstance().setCurrentActivity(this);
		Log.e("Cr�dito","Personal");
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("creditsimPPICont", parentManager.estados);
		init();
	}
	
	private void init(){
		Tools.setIsContratacionPreference(true);
		ocultaInfoNC();
		Tools.getCurrentSession().setPpiCAct(this);
		delegate = new SimulaPPIDelegate();
		inicializaBotones();
		mapearBotones();	
		
		if((delegate.getProducto()!=null) && delegate.getProducto().getIndSimBoolean()) this.pintarInfoTabla();
		
		scaleToCurrentScreen();
		setEventToSpinner();
		//Tools.getCurrentSession().setVisibilityButton(this, R.id.simPPIBottomMenuRC);
	}
	
	private void scaleToCurrentScreen(){
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.simPPIBtnSimular));
		guiTools.scale(findViewById(R.id.simPPILayout));
		guiTools.scale(findViewById(R.id.simPPIBodyLayout));
		guiTools.scale(findViewById(R.id.simPPITitle),true);
		guiTools.scale(findViewById(R.id.simPPILayoutSubTitle));
		guiTools.scale(findViewById(R.id.simPPISubTitleTCImg));
		guiTools.scale(findViewById(R.id.simPPISubTitleMiddleTxt),true);
		guiTools.scale(findViewById(R.id.simPPILinearLayout));
		guiTools.scale(findViewById(R.id.simPPILText),true);
		guiTools.scale(findViewById(R.id.simPPILMon),true);
		guiTools.scale(findViewById(R.id.simPPIMTextSL),true);
		guiTools.scale(findViewById(R.id.simPPILineaSeparacion));
		guiTools.scale(findViewById(R.id.simPPIPlazo),true);
		guiTools.scale(findViewById(R.id.simPPISpinner));
		guiTools.scale(findViewById(R.id.simPPIBottomLayout));
		guiTools.scale(findViewById(R.id.simPPIBottomMenuRC),true);
		guiTools.scale(findViewById(R.id.simPPIBottomAdquirir),true);
		guiTools.scale(findViewById(R.id.simPPIlblResultadoSimulacion),true);
		guiTools.scale(findViewById(R.id.simPPIlblResultadoSimulacion2),true);
		guiTools.scale(findViewById(R.id.simPPIlblResultadoSimulacion3),true);
		guiTools.scale(findViewById(R.id.simPPIlblResultadoPago),true);
		guiTools.scale(findViewById(R.id.simPPIlblResultadoPlazo),true);
		guiTools.scale(findViewById(R.id.simPPIlblResultadoTasa),true);
		guiTools.scale(findViewById(R.id.simPPILinearLayout2));
		guiTools.scale(findViewById(R.id.simPPILinearLayout3));
		guiTools.scale(findViewById(R.id.simPPILinearLayoutTexto));
		guiTools.scale(findViewById(R.id.simPPICatFecha),true);
		guiTools.scale(findViewById(R.id.simPPICatInfo));
		guiTools.scale(findViewById(R.id.simPPICatPorc),true);
		guiTools.scale(findViewById(R.id.simPPICatText),true);
		guiTools.scale(findViewById(R.id.simPPICCat),true);
		guiTools.scale(findViewById(R.id.simPPISeekbarText),true);
		guiTools.scale(findViewById(R.id.simPPISeekbarText2),true);
		guiTools.scale(findViewById(R.id.simPPISeekBar));
		guiTools.scale(findViewById(R.id.simPPISeekbarLayout));
		guiTools.scale(findViewById(R.id.simPPISeekbarPlus));
		guiTools.scale(findViewById(R.id.simPPISeekbarMin));
	}
	
	private void inicializaBotones(){
		
		refreshBtn = (ImageButton)findViewById(R.id.headerRefresh);
		resumenContratacionBtn = (Button)findViewById(R.id.simPPIBottomMenuRC);
		btnAdquirir = (Button)findViewById(R.id.simPPIBottomAdquirir);
		seekPlus = (Button)findViewById(R.id.simPPISeekbarMin);
		seekMinus = (Button)findViewById(R.id.simPPISeekbarPlus);
		seekbar = (SeekBar)findViewById(R.id.simPPISeekBar);
		spinner = (Spinner)findViewById(R.id.simPPISpinner);
		btnSimular = (ImageButton)findViewById(R.id.simPPIBtnSimular);

		

	}
	
	private void mapearBotones(){
	
		
		btnSimular = (ImageButton)findViewById(R.id.simPPIBtnSimular);
		btnSimular.setOnClickListener(this);
		
		refreshBtn = (ImageButton)findViewById(R.id.headerRefresh);
		refreshBtn.setOnClickListener(this);
		
		resumenContratacionBtn = (Button)findViewById(R.id.simPPIBottomMenuRC);
		resumenContratacionBtn.setOnClickListener(this);
		
		btnAdquirir = (Button)findViewById(R.id.simPPIBottomAdquirir);
		btnAdquirir.setOnClickListener(this);
		
		seekPlus = (Button)findViewById(R.id.simPPISeekbarMin);
		seekPlus.setOnClickListener(this);
		
		seekMinus = (Button)findViewById(R.id.simPPISeekbarPlus);
		seekMinus.setOnClickListener(this);
		
		seekbar = (SeekBar)findViewById(R.id.simPPISeekBar);
		seekbar.setMax(delegate.getMontoMax());
		seekbar.setProgress(delegate.getMontoProgress());
		seekbar.setOnSeekBarChangeListener(this);		
		seekbar.refreshDrawableState();
		
		TextView valueSeekBar =(TextView) findViewById(R.id.simPPISeekbarText2);
		valueSeekBar.setText(GuiTools.getMoneyString(delegate.getMontoProgress().toString()));

		TextView disponible = (TextView)findViewById(R.id.simPPILMon);
		if(delegate.getProducto() != null){
			disponible.setText(GuiTools.getMoneyString(String.valueOf(delegate.getProducto().getSubproducto().get(0).getMonMax())));
		}else{
			disponible.setText(GuiTools.getMoneyString(String.valueOf(0)));
		}
		
		spinner = (Spinner)findViewById(R.id.simPPISpinner);
		ArrayAdapter<String> dataAdapter = 
				new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, delegate.getPlazos());		
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);	
		/*
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
			public void onItemSelected(AdapterView<?> parent, View view, int pos, long id){
				if(spinner.getId() == R.id.simPPISpinner)
			     {	
					     ocultaInfoNC();
										
			     }
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {				
			}
		});*/
	}
	
	
	
	
	public void subir()
	{
		

		LinearLayout p=(LinearLayout)findViewById(R.id.simPPIBottomLayout);
		ImageButton bn=(ImageButton)findViewById(R.id.simPPIBtnSimular);
	     //p.setY();
		//Log.d("verifica", obtiene()+"");
	//	float h=alturah();
		//Log.d("altura", h+"");
	   
	}
	public void bajar()
	{
		LinearLayout d=(LinearLayout)findViewById(R.id.simPPIBodyLayout);
		LinearLayout p=(LinearLayout)findViewById(R.id.simPPIBottomLayout);
		p.setY(Tools.getPosicionBarraInferiorB(d));

	}
	private void muestraInfoNC(){
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simPPILinearLayoutTexto);
		tlayout.setVisibility(View.VISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.simPPICatInfo);
		tableLayout.setVisibility(View.VISIBLE);
		
		btnAdquirir = (Button)findViewById(R.id.simPPIBottomAdquirir);
		if(suitebancomer.aplicaciones.bmovil.classes.common.Session.getInstance(this).getClientProfile() == bancomer.api.common.commons.Constants.Perfil.avanzado)
			btnAdquirir.setVisibility(View.VISIBLE);
		bajar();
	}
	
	public void mockData(){
		Spinner spinner = (Spinner) findViewById(R.id.simPPISpinner);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.plazo_array, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {
		isOnForeground = false;
		if(v.getId() == R.id.headerRefresh){
			delegate.setRet();
			delegate.redirectToView(MenuPrincipalActivity.class);
		}else if(v.getId() == R.id.simPPIBtnSimular)
		{
			isOcultaInfoEnabled=false;
	    //	delegate.doRecalculoContract(this);
            Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
			click_paso2_operacion.put("evento_realizada","event52");
			click_paso2_operacion.put("&&products","simulador;simulador:simulador prestamo personal");
			click_paso2_operacion.put("eVar12","simulacion realizada");
			TrackingHelper.trackSimulacionRealizada(click_paso2_operacion);
			deleteSimulation();
	    }else if(v.getId() == R.id.simPPIBottomMenuRC){
		    delegate.redirectToView(ResumenActivity.class);
	    }else if(v.getId() == R.id.simPPIBottomAdquirir)
	    {
		     //nuevo flujo para adquirir
	    	isOnForeground = false;
			//consumoOneClick();
		    Log.e("Selecciono adquirir","...");
            Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();

			click_paso2_operacion.put("inicio","event45");
			click_paso2_operacion.put("&&products","simulador;simulador:simulador prestamo personal");
			click_paso2_operacion.put("eVar12","seleccion adquirir");

			TrackingHelper.trackPaso1Operacion(click_paso2_operacion);
			saveSimulation();
			//agm delegate.getCorreo(this);
			//this.showAlert("Para contratar acude a tu sucursal más cercana.",this);
			
		}else if(v.getId() == R.id.simPPISeekbarPlus){
			seekbar = (SeekBar)findViewById(R.id.simPPISeekBar);
			if(seekbar.getProgress() < seekbar.getMax()){
				Integer val = seekbar.getMax() - seekbar.getProgress(); 
				if(val>=1000){
					seekbar.setProgress(seekbar.getProgress()+1000);
				}else{
					seekbar.setProgress(seekbar.getProgress()+val);
				}
			}
		}else if(v.getId() == R.id.simPPISeekbarMin){
			seekbar = (SeekBar)findViewById(R.id.simPPISeekBar);
			if(seekbar.getProgress() > delegate.getMontoMin()){
				if((seekbar.getProgress() >= 1000)&&(delegate.getMontoMin() <= (seekbar.getProgress()-1000))){
					seekbar.setProgress(seekbar.getProgress()-1000);
				}else{
					seekbar.setProgress(delegate.getMontoMin());
				}
			}
		}
	}
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		
		TextView text = (TextView)findViewById(R.id.simPPISeekbarText2);
		text.setText(GuiTools.getMoneyString(String.valueOf(seekbar.getProgress())));
		
		ocultaInfoNC();
		
		if(seekbar.getProgress() == delegate.getMontoMax()){
			seekbar.setProgress(seekbar.getProgress());
		}else{
			seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
		}
//		if(seekbar.getProgress()>=delegate.getMontoMin()){ 
//		delegate.doRecalculoContract(this);
//		}
//		if(seekbar.getProgress()>=delegate.getMontoMin()) {
//			delegate.doRecalculoContract(this);		
//		}
		
	}
	
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
//		seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
//		if(seekbar.getProgress()<delegate.getMontoMin()){
//			seekbar.setProgress(delegate.getMontoMin());
//		}
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
//		seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
//		if(seekbar.getProgress()<delegate.getMontoMin()){
//			seekbar.setProgress(delegate.getMontoMin());
//		}
		if(seekbar.getProgress()>=delegate.getMontoMin()){ 
			//delegate.doRecalculoContract(this);
		}else{
			seekbar.setProgress(delegate.getMontoMin());
			   //delegate.doRecalculoContract(this);
		}
	}
	
	public void pintarInfoTabla(){
		
		//Quitar bloqueo
		MainController.getInstance().ocultaIndicadorActividad();
		
		ocultaInfoNC();		
			
		TextView porcentajeCat = (TextView)findViewById(R.id.simPPICatPorc);
		TextView fechaCat = (TextView)findViewById(R.id.simPPICatFecha);
		TextView resultadoPago = (TextView)findViewById(R.id.simPPIlblResultadoPago);
		TextView resultadoPlazo = (TextView)findViewById(R.id.simPPIlblResultadoPlazo);
		TextView resultadoTasa = (TextView)findViewById(R.id.simPPIlblResultadoTasa);
		
		String plazoSel = null;
		if(spinner!= null && spinner.getSelectedItem() != null){			
			plazoSel = spinner.getSelectedItem().toString();
		}
		
		Producto p = delegate.getProducto();

		if ( (delegate.getProducto()!=null) && delegate.getProducto().getIndSimBoolean() ){
			plazoSel = p.getDesPlazoE();
			spinner.setSelection(getPosSelectedSpinner(spinner, plazoSel));
			
			//seekbar.setProgress(Integer.parseInt(p.getMontoDeseS()));
		}
		
		String textoPorcCat = p.getCATS(); 
		porcentajeCat.setText(textoPorcCat);
		fechaCat.setText(p.getFechaCatS());
		
		resultadoPago.setText(GuiTools.getMoneyString(String.valueOf(p.getPagMProdS())));
		resultadoPlazo.setText(plazoSel);
		resultadoTasa.setText(p.getTasaS()+"%");			

		muestraInfoNC();
	}

	private void ocultaInfoNC(){
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simPPILinearLayoutTexto);
		tlayout.setVisibility(View.INVISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.simPPICatInfo);
		tableLayout.setVisibility(View.INVISIBLE);
		
		btnAdquirir = (Button)findViewById(R.id.simPPIBottomAdquirir);
		btnAdquirir.setVisibility(View.INVISIBLE);
	}
	
	
	
	@Override    
    public boolean onKeyDown(int keyCode, KeyEvent event) { 

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			isOnForeground = false;
    			delegate.redirectToView(MenuPrincipalActivity.class);
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}		
    }
	
	public void consumoOneClick()
	{		
		Log.d("Salv� Simulaci�n","ok");
		
		//OK
		/** Modified: April 22th, 2015. Author:OOS.*/    	
		//Promociones promocion = new Promociones();
		Producto producto = delegate.getProducto();
		
		//Bmovil Session variables to be used to determine which flow the "one click flow" is going to take.
		suitebancomer.aplicaciones.bmovil.classes.common.Session.getInstance(MainController.getInstance().getContext()).setOfertaDelSimulador(true);
		suitebancomer.aplicaciones.bmovil.classes.common.Session.getInstance(MainController.getInstance().getContext()).setCveCamp(producto.getCveProd());
				
		
		//OfertaConsumo oferta = new OfertaConsumo();
		
		//oferta.setImporte(Tools.formatterForBmovil(producto.getMontoDeseS()));
		//oferta.setPlazoDes(producto.getDesPlazoE());
		
		//String bscPagar = new String(producto.getDessubpE());
		//oferta.setTipoSeg(bscPagar.substring(0, 16));
		//oferta.setTotalPagos(Tools.getTotalPagos(producto.getDesPlazoE()));
		Session session = Session.getInstance();
		
		DetalleDeAlternativaDelegate delegate = new DetalleDeAlternativaDelegate(session.getIdUsuario(), session.getIum(), session.getNumCelular(), producto.getCveProd());
		//delegate.setOfertaConsumo(oferta);
		//delegate.setPromocion(promocion);
		delegate.consultaDetalleAlternativasTask();
		
		
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if(!isScreenOn() || (!this.isFinishing()&&isOnForeground))
		{
			SuiteApp.getInstance().closeBmovilAppSession();
		}
	}
	
	private void setEventToSpinner()
	{
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) { 
		    	if(isOcultaInfoEnabled)
		    		ocultaInfoNC();
		    	isOcultaInfoEnabled=true;
		    } 

		    public void onNothingSelected(AdapterView<?> adapterView) {
		        return;
		    } 
		}); 
	}
	
	/**
	 * Created June 8th, 2015,
	 */
	
	private void saveSimulation()
	{
		delegate.saveOrDeleteSimulationRequest(this,true);
	}
	private void deleteSimulation()
	{
		delegate.saveOrDeleteSimulationRequest(this,false);
	}
	
	public void doRecalculo()
	{
		Log.d("Elimin� Simulaci�n","ok");
		delegate.doRecalculoContract(this);
	}
	
	public void showAlert(String texto, SimulaPPIActivity act){
		final SimulaPPIActivity  activity = act;
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setMessage(texto);

		alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {         
	        } 
		});
     
     AlertDialog dialog = alert.show();
     
     TextView messageView = (TextView)dialog.findViewById(android.R.id.message);
     messageView.setGravity(Gravity.CENTER);
	}
	
}
