package suitebancomer.aplicaciones.bbvacredit.gui.activities;

import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bbvacredit.common.GuiTools;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.SimulaTarjetaCreditoDelegate;
import suitebancomer.aplicaciones.bbvacredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import tracking.TrackingHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class SimulaTarjetaCreditoActivity extends BaseActivity implements OnClickListener,OnSeekBarChangeListener{
	
	private SimulaTarjetaCreditoDelegate delegate;
	private ImageButton backButton;
	private ImageButton btnSimular;
	
	// Menu Button
	private Button resumenBtn;
	private Button btnContacto;
	
	// Botones Seekbar
	private Button seekPlus;
	private Button seekMinus;
	private SeekBar seekbar;
	private BmovilViewsController parentManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState,SHOW_HEADER|SHOW_TITLE, R.layout.activity_simula_tarjeta_credito);
		isOnForeground = true;
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("creditsimtdccont", parentManager.estados);
		MainController.getInstance().setCurrentActivity(this);
		
		init();
	}
	
	private void init(){
		Tools.setIsContratacionPreference(true);
		ocultaInfoNC();
		Tools.getCurrentSession().setTdcAct(this);
		delegate = new SimulaTarjetaCreditoDelegate();
		inicializaBotones();
		mapearBotones();
		
		if((delegate.getProducto()!=null) && delegate.getProducto().getIndSimBoolean()) this.pintarInfoTabla();
		
		scaleToCurrentScreen();
		//Tools.getCurrentSession().setVisibilityButton(this, R.id.simTCBottomMenuRC);
	}
	
	private void inicializaBotones(){
		
		backButton = (ImageButton)findViewById(R.id.headerRefresh);
		btnSimular = (ImageButton)findViewById(R.id.simTCBtnSimular);
		resumenBtn = (Button)findViewById(R.id.simTCBottomMenuRC);
		btnContacto = (Button)findViewById(R.id.simTCBottomContacto);
		seekPlus = (Button)findViewById(R.id.simTCSeekbarMin);
		seekMinus = (Button)findViewById(R.id.simTCSeekbarPlus);
		seekbar = (SeekBar)findViewById(R.id.simTCSeekBar);

	}
	
	private void mapearBotones(){		
		backButton = (ImageButton)findViewById(R.id.headerRefresh);
		backButton.setOnClickListener(this);
		
		btnSimular = (ImageButton)findViewById(R.id.simTCBtnSimular);
		btnSimular.setOnClickListener(this);
		
		resumenBtn = (Button)findViewById(R.id.simTCBottomMenuRC);
		resumenBtn.setOnClickListener(this);
		
		btnContacto = (Button)findViewById(R.id.simTCBottomContacto);
		btnContacto.setOnClickListener(this);
		
		seekPlus = (Button)findViewById(R.id.simTCSeekbarMin);
		seekPlus.setOnClickListener(this);
		
		seekMinus = (Button)findViewById(R.id.simTCSeekbarPlus);
		seekMinus.setOnClickListener(this);
		
		seekbar = (SeekBar)findViewById(R.id.simTCSeekBar);
		seekbar.setMax(delegate.getMontoMax());
		seekbar.setProgress(delegate.getMontoProgress());
		seekbar.setOnSeekBarChangeListener(this);		
		seekbar.refreshDrawableState();
		
		TextView valueSeekBar =(TextView) findViewById(R.id.simTCSeekbarText2);
		valueSeekBar.setText(GuiTools.getMoneyString(delegate.getMontoProgress().toString()));

		TextView disponible = (TextView)findViewById(R.id.simTCMoney);
		if(delegate.getProducto() != null){
			disponible.setText(GuiTools.getMoneyString(String.valueOf(delegate.getProducto().getSubproducto().get(0).getMonMax())));
		}else{
			disponible.setText(GuiTools.getMoneyString(String.valueOf(0)));
		}
		
	}
	
	/**
	 * Escala los elementos de la pantalla para la resoluci�n actual.
	 */
	private void scaleToCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.simTCLayout));
		guiTools.scale(findViewById(R.id.simTCBodyLayout));
		guiTools.scale(findViewById(R.id.simTCTitle),true);
		guiTools.scale(findViewById(R.id.simTCLayoutSubTitle));
		guiTools.scale(findViewById(R.id.simTCSubTitleTCImg));
		guiTools.scale(findViewById(R.id.simTCSubTitleMiddleTxt),true);
		guiTools.scale(findViewById(R.id.simTCSubTitleMiddleTxt2),true);
		guiTools.scale(findViewById(R.id.simTCSubTitleMiddleTxt3),true);
//		guiTools.scale(findViewById(R.id.simTCLinearTextLayout));
		
//		guiTools.scale(findViewById(R.id.simTCMText6),true);
		guiTools.scale(findViewById(R.id.simTCMoney),true);
		guiTools.scale(findViewById(R.id.simTCBotText),true);
		guiTools.scale(findViewById(R.id.simTCBotMoney),true);
//		guiTools.scale(findViewById(R.id.simTCCatInfo));
//		guiTools.scale(findViewById(R.id.simTCCat),true);
//		guiTools.scale(findViewById(R.id.simTCCatPorc),true);

//		guiTools.scale(findViewById(R.id.simTCCatText),true);
//		guiTools.scale(findViewById(R.id.simTCCatFecha),true);
		guiTools.scale(findViewById(R.id.simTCBottomLayout));
		guiTools.scale(findViewById(R.id.simTCBtnSimular));
		guiTools.scale(findViewById(R.id.simTCBottomMenuRC),true);
		guiTools.scale(findViewById(R.id.simTCBottomContacto),true);
		guiTools.scale(findViewById(R.id.simTCSeekbarText),true);
		guiTools.scale(findViewById(R.id.simTCSeekbarText2),true);
		guiTools.scale(findViewById(R.id.simTCSeekBar));
		guiTools.scale(findViewById(R.id.simTCSeekbarLayout));
		guiTools.scale(findViewById(R.id.simTCSeekbarPlus));
		guiTools.scale(findViewById(R.id.simTCSeekbarMin));
	}

	@Override
	public void onClick(View v) {
		isOnForeground = false;	
		if(v.getId() == R.id.headerRefresh){			
			delegate.redirectToView(MenuPrincipalActivity.class);
		}else if(v.getId() == R.id.simTCBtnSimular)
		{
		//	delegate.doRecalculoContract(this);
			Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
			click_paso2_operacion.put("evento_realizada","event52");
			click_paso2_operacion.put("&&products","simulador;simulador:simulador tarjeta credito");
			click_paso2_operacion.put("eVar12","simulacion realizada");
			TrackingHelper.trackSimulacionRealizada(click_paso2_operacion);
			deleteSimulation();
		}else if(v.getId() == R.id.simTCBottomMenuRC){
			delegate.redirectToView(ResumenActivity.class);
		}else if(v.getId() == R.id.simTCBottomContacto){
			String params = "";
			Session sesion = Session.getInstance(this);
			params = "nombredepila=" + sesion.getNombreCliente().split(" ")[0] + "&segmento=TDC&credito=" + seekbar.getProgress() + "&idpagina=simulacion";
			Log.d("Credit","params = " + params);
			Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
			click_paso2_operacion.put("inicio","event45");
			click_paso2_operacion.put("&&products","simulador;simulador:simulador tarjeta credito");
			click_paso2_operacion.put("eVar12","seleccion contactame");
			TrackingHelper.trackPaso1Operacion(click_paso2_operacion);
			PopUpContactameActivity.muestraPop(this, params);
			isOnForeground = false;
			//Log.e("Selecciono contactame","....");
		}else if(v.getId() == R.id.simTCSeekbarPlus){
			seekbar = (SeekBar)findViewById(R.id.simTCSeekBar);
			if(seekbar.getProgress() < seekbar.getMax()){
				Integer val = seekbar.getMax() - seekbar.getProgress(); 
				if(val>=1000){
					seekbar.setProgress(seekbar.getProgress()+1000);
				}else{
					seekbar.setProgress(seekbar.getProgress()+val);
				}
				if(seekbar.getProgress()<=delegate.getMontoMax()){ 
					//delegate.doRecalculoContract(this);
					}
			}
		}else if(v.getId() == R.id.simTCSeekbarMin){
			seekbar = (SeekBar)findViewById(R.id.simTCSeekBar);
			if(seekbar.getProgress() > delegate.getMontoMin()){
				if((seekbar.getProgress() >= 1000)&&(delegate.getMontoMin() <= (seekbar.getProgress()-1000))){
					seekbar.setProgress(seekbar.getProgress()-1000);
				}else{
					seekbar.setProgress(delegate.getMontoMin());
				}
				if(seekbar.getProgress()>=delegate.getMontoMin()){ 
					//delegate.doRecalculoContract(this);
					}
			}
		}
		
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		TextView text = (TextView)findViewById(R.id.simTCSeekbarText2);
		text.setText(GuiTools.getMoneyString(String.valueOf(seekbar.getProgress())));
		
		ocultaInfoNC();
		if(seekbar.getProgress() == delegate.getMontoMax()){
			seekbar.setProgress(seekbar.getProgress());
		}else{
			seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
		}
		
//		if(seekbar.getProgress()>=delegate.getMontoMin()) 
//			delegate.doRecalculoContract(this);
//		
	}
	
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
//		seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
//		if(seekbar.getProgress()<delegate.getMontoMin()){
//			seekbar.setProgress(delegate.getMontoMin());
//		}
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
//		seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
//		if(seekbar.getProgress()<delegate.getMontoMin()){
//			seekbar.setProgress(delegate.getMontoMin());
//		}
		if(seekbar.getProgress()>=delegate.getMontoMin()){ 
		//delegate.doRecalculoContract(this);
		}else{
			seekbar.setProgress(delegate.getMontoMin());
			//delegate.doRecalculoContract(this);
		}
	}

	public void pintarInfoTabla(){
		
		//Quitar bloqueo
		MainController.getInstance().ocultaIndicadorActividad();
		
		ocultaInfoNC();		
		
//		TextView porcentajeCat = (TextView)findViewById(R.id.simTCCatPorc);
//		TextView fechaCat = (TextView)findViewById(R.id.simTCCatFecha);
		TextView resultadoPago = (TextView)findViewById(R.id.simTCBotMoney);
		
		Producto p = delegate.getProducto();
		
		if ( (delegate.getProducto()!=null) && delegate.getProducto().getIndSimBoolean() ){			
			//seekbar.setProgress(Integer.parseInt(p.getMontoDeseS()));
		}
		
		String textoPorcCat = p.getCATS(); 
//		porcentajeCat.setText(textoPorcCat);
//		fechaCat.setText(p.getFechaCatS());
		
//		resultadoPago.setText(GuiTools.getMoneyString(String.valueOf(seekbar.getProgress())));		
		resultadoPago.setText(GuiTools.getMoneyString(String.valueOf(p.getMontoDeseS())));
	
		muestraInfoNC();
	}

	private void ocultaInfoNC(){
		
		TextView tviewTexto = (TextView)findViewById(R.id.simTCBotText);
		tviewTexto.setVisibility(View.INVISIBLE);
		
		TextView tviewImporte = (TextView)findViewById(R.id.simTCBotMoney);
		tviewImporte.setVisibility(View.INVISIBLE);
		
		btnContacto = (Button)findViewById(R.id.simTCBottomContacto);
		btnContacto.setVisibility(View.INVISIBLE);
		
//		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.simTCCatInfo);
//		tableLayout.setVisibility(View.INVISIBLE);
	}
	
	private void muestraInfoNC(){
		
		TextView tviewTexto = (TextView)findViewById(R.id.simTCBotText);
		tviewTexto.setVisibility(View.VISIBLE);
		
		TextView tviewImporte = (TextView)findViewById(R.id.simTCBotMoney);
		tviewImporte.setVisibility(View.VISIBLE);
		
		btnContacto = (Button)findViewById(R.id.simTCBottomContacto);
		btnContacto.setVisibility(View.VISIBLE);
		
//		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.simTCCatInfo);
//		tableLayout.setVisibility(View.VISIBLE);
	}

	public SeekBar getSeekbar() {
		return seekbar;
	}

	public void setSeekbar(SeekBar seekbar) {
		this.seekbar = seekbar;
	}		
	
	@Override    
    public boolean onKeyDown(int keyCode, KeyEvent event) { 

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			isOnForeground = false;
    			delegate.redirectToView(MenuPrincipalActivity.class);
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}		
    }
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if(!isScreenOn() || (!this.isFinishing()&&isOnForeground))
		{
			SuiteApp.getInstance().closeBmovilAppSession();
		}
	}
	
	/**
	 * Created June 8th, 2015,
	 */
	
	private void deleteSimulation()
	{
		delegate.deleteSimulationRequest(this);
	}
	
	public void doRecalculo()
	{
		Log.d("Eliminó Simulación","ok");
		delegate.doRecalculoContract(this);
	}
	
	
}
