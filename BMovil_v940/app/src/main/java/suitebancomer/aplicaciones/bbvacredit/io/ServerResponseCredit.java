package suitebancomer.aplicaciones.bbvacredit.io;

import android.util.Log;

import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.me.JSONAble;

import bancomer.api.alertasdigitales.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaConsumo;

/**
 * ServerResponse wraps the response from the server, containing attributes
 * that. define if the response has been successful or not, and the data content
 * if the response was successful
 * 
 * @author CGI.
 */
public class ServerResponseCredit implements ParsingHandlerCredit, ParsingHandlerJSONCredit {

	// ///////////////////////////////////////////////////////////////////////////
	// Status code definitions //
	// ///////////////////////////////////////////////////////////////////////////

	/**
	 * Default UID
	 */
//	private static final long serialVersionUID = 1L;

	/**
	 * Operation successful.
	 */
	public static final int OPERATION_SUCCESSFUL = 0;

	/**
	 * Operation with warning.
	 */
	public static final int OPERATION_WARNING = 1;

	/**
	 * Operation failed.
	 */
	public static final int OPERATION_ERROR = 2;

	/**
	 * Operation succesful but asks for optinal updating.
	 */
	public static final int OPERATION_OPTIONAL_UPDATE = 3;

	/**
	 * Session expired.
	 */
	public static final int OPERATION_SESSION_EXPIRED = -100;

	/**
	 * Unknown operation result.
	 */
	public static final int OPERATION_STATUS_UNKNOWN = -1000;

	/**
	 * Status of the current response.
	 */
	private int status = 0;

	/**
	 * Response error code.
	 */
	private String messageCode = null;

	/**
	 * Error message.
	 */
	private String messageText = null;

	/**
	 * URL for mandatory updating(the mandatory updating order is received as.
	 * error with code MBANK1111)
	 */
	private String updateURL = null;

	/**
	 * Response handler, containing the data content of the response.
	 */
	private Object responseHandler = null;

	/**
	 * Default constructor.
	 */
	public ServerResponseCredit() {
	}

	/**
	 * Constructor.
	 * 
	 * @param handler
	 *            the handler of the content
	 */
	public ServerResponseCredit(Object handler) {
		this.responseHandler = handler;
	}

	/**
	 * Constructor with parameters.
	 * 
	 * @param stat
	 *            the response status
	 * @param msgCode
	 *            the error code
	 * @param msgText
	 *            the error text
	 * @param handler
	 *            the handler of the content
	 */
	public ServerResponseCredit(int stat, String msgCode, String msgText,
			Object handler) {
		this.status = stat;
		this.messageCode = msgCode;
		this.messageText = msgText;
		this.responseHandler = handler;
	}

	/**
	 * Get the status code.
	 * 
	 * @return the status code
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * Get the error code.
	 * 
	 * @return the error code
	 */
	public String getMessageCode() {
		return messageCode;
	}

	/**
	 * Get the error text.
	 * 
	 * @return the error text
	 */
	public String getMessageText() {
		return messageText;
	}

	/**
	 * Get the response.
	 * 
	 * @return the content response
	 */
	public Object getResponse() {
		return responseHandler;
	}

	/**
	 * Gets the value of the URL for mandatory application update.
	 * 
	 * @return SOMETHING
	 */
	public String getUpdateURL() {
		return updateURL;
	}

	@Override
	public void process(ParserCredit parser) throws IOException, ParsingExceptionCredit {
		if (parser != null) {

            status = OPERATION_STATUS_UNKNOWN;
            ResultCredit result = parser.parseResult();
            String statusText = result.getStatus();
            if (ParserCredit.STATUS_OK.equals(statusText)) {
                status = OPERATION_SUCCESSFUL;
            } else if (ParserCredit.STATUS_OPTIONAL_UPDATE.equals(statusText)) {
                status = OPERATION_OPTIONAL_UPDATE;
            } else if (ParserCredit.STATUS_WARNING.equals(statusText)) {
                status = OPERATION_WARNING;
            } else if (ParserCredit.STATUS_ERROR.equals(statusText)) {
                status = OPERATION_ERROR;
                updateURL = result.getUpdateURL();
            }

            messageCode = result.getCode();
            messageText = result.getMessage();

            if ((responseHandler != null) && (responseHandler instanceof ParsingHandlerCredit)
            		&& ((status == OPERATION_SUCCESSFUL)
            		|| (status == OPERATION_OPTIONAL_UPDATE))) {
                ((ParsingHandlerCredit) responseHandler).process(parser);
            }

        } else {

            status = OPERATION_STATUS_UNKNOWN;
            messageCode = null;
            messageText = null;
        }
	}

	@Override
	public void processJSON(String jsonString) throws JSONException {
		if (jsonString != null) {
            JSONObject objectJSON = new JSONObject(jsonString);
            String statusString = objectJSON.getString(ServerConstantsCredit.JSON_ESTADO_RESPUESTA);
            
            if (ParserCredit.STATUS_OK.equals(statusString)) {
                status = OPERATION_SUCCESSFUL;
            } else if (ParserCredit.STATUS_OPTIONAL_UPDATE.equals(statusString)) {
                status = OPERATION_OPTIONAL_UPDATE;
            } else if (ParserCredit.STATUS_WARNING.equals(statusString)) {
                status = OPERATION_WARNING;
            } else if (ParserCredit.STATUS_ERROR.equals(statusString)) {
                status = OPERATION_ERROR;
                
                messageCode = objectJSON.getString(ServerConstantsCredit.JSON_CODIGO_MENSAJE_ETIQUETA);
                messageText = objectJSON.getString(ServerConstantsCredit.JSON_MENSAJE_INFORMATIVO_ETIQUETA);
            }
            
            if ((responseHandler != null) && (responseHandler instanceof JSONAble) &&
                    ((status == OPERATION_SUCCESSFUL) || (status == OPERATION_OPTIONAL_UPDATE)) ) {
                ((JSONAble)responseHandler).fromJSON(jsonString);
            }else if(responseHandler instanceof AceptaOfertaConsumo && status == OPERATION_SUCCESSFUL ){
				try {
					ParserJSON pj=new ParserJSON(jsonString);
					((AceptaOfertaConsumo) responseHandler).process(pj);
				} catch (IOException e) {
					if(Server.ALLOW_LOG)
						Log.e(this.getClass().toString(), e.getMessage());
				} catch (ParsingException e) {
					if(Server.ALLOW_LOG)
						Log.e(this.getClass().toString(),e.getMessage());
				}
			}
            
        } else {
            status = OPERATION_STATUS_UNKNOWN;
            messageCode = null;
            messageText = null;
        }
	}

//	@Override
//	public void process(ParserJSON parser) throws IOException, ParsingException {
//		if (parser != null) {
//
//            status = OPERATION_STATUS_UNKNOWN;
//            Result result = parser.parseResult();
//            String statusText = result.getStatus();
//            if (Parser.STATUS_OK.equals(statusText)) {
//                status = OPERATION_SUCCESSFUL;
//            } else if (Parser.STATUS_OPTIONAL_UPDATE.equals(statusText)) {
//                status = OPERATION_OPTIONAL_UPDATE;
//            } else if (Parser.STATUS_WARNING.equals(statusText)) {
//                status = OPERATION_WARNING;
//            } else if (Parser.STATUS_ERROR.equals(statusText)) {
//                status = OPERATION_ERROR;
//                updateURL = result.getUpdateURL();
//            }
//
//            messageCode = result.getCode();
//            messageText = result.getMessage();
//
//            if ((responseHandler != null) && (responseHandler instanceof ParsingHandlerJSON)
//            		&& ((status == OPERATION_SUCCESSFUL)
//            		|| (status == OPERATION_OPTIONAL_UPDATE))) {
//                ((ParsingHandlerJSON) responseHandler).process(parser);
//            }
//
//        } else {
//
//            status = OPERATION_STATUS_UNKNOWN;
//            messageCode = null;
//            messageText = null;
//        }
//	}
}
