package suitebancomer.aplicaciones.bbvacredit.models;

import java.util.ArrayList;

public class Producto {
	
	public static final String ADQUISICION_CODE = "ADQUISICION";
	public static final String LIQUIDEZ_CODE = "LIQUIDEZ";
	
	private String cveProd;
	
	private String desProd;
	
	private String parainfo;
	
	private ArrayList<Subproducto> subproducto;
	
	private String subprodE;
	
	private String dessubpE;
	
	private String cvePzoE;
	
	private String desPlazoE;
	
	private String montoMaxS;
	
	private String montoMinS;
	
	private String numTDCS;
	
	private String montoDeseS;
	
	private String pagMProdS;
	
	private String porcProdS;
	
	private String tasaS;
	
	private String CATS;
	
	private String fechaCatS;
	
	private String indSim;
	
	private String indInfo;
	
	private String mensaje;

	public String getCveProd() {
		return cveProd;
	}

	public void setCveProd(String cveProd) {
		this.cveProd = cveProd;
	}

	public String getDesProd() {
		return desProd;
	}

	public void setDesProd(String desProd) {
		this.desProd = desProd;
	}

	public String getParainfo() {
		return parainfo;
	}

	public void setParainfo(String parainfo) {
		this.parainfo = parainfo;
	}

	public ArrayList<Subproducto> getSubproducto() {
		return subproducto;
	}

	public void setSubproducto(ArrayList<Subproducto> subproducto) {
		this.subproducto = subproducto;
	}

	public String getSubprodE() {
		return subprodE;
	}

	public void setSubprodE(String subprodE) {
		this.subprodE = subprodE;
	}

	public String getDessubpE() {
		return dessubpE;
	}

	public void setDessubpE(String dessubpE) {
		this.dessubpE = dessubpE;
	}

	public String getCvePzoE() {
		return cvePzoE;
	}

	public void setCvePzoE(String cvePzoE) {
		this.cvePzoE = cvePzoE;
	}

	public String getDesPlazoE() {
		return desPlazoE;
	}

	public void setDesPlazoE(String desPlazoE) {
		this.desPlazoE = desPlazoE;
	}

	public String getMontoMaxS() {
		return montoMaxS;
	}

	public void setMontoMaxS(String montoMaxS) {
		this.montoMaxS = montoMaxS;
	}

	public String getMontoMinS() {
		return montoMinS;
	}

	public void setMontoMinS(String montoMinS) {
		this.montoMinS = montoMinS;
	}

	public String getNumTDCS() {
		return numTDCS;
	}

	public void setNumTDCS(String numTDCS) {
		this.numTDCS = numTDCS;
	}

	public String getMontoDeseS() {
		return montoDeseS;
	}

	public void setMontoDeseS(String montoDeseS) {
		this.montoDeseS = montoDeseS;
	}

	public String getPagMProdS() {
		return pagMProdS;
	}

	public void setPagMProdS(String pagMProdS) {
		this.pagMProdS = pagMProdS;
	}

	public String getPorcProdS() {
		return porcProdS;
	}

	public void setPorcProdS(String porcProdS) {
		this.porcProdS = porcProdS;
	}

	public String getTasaS() {
		return tasaS;
	}

	public void setTasaS(String tasaS) {
		this.tasaS = tasaS;
	}

	public String getCATS() {
		return CATS;
	}

	public void setCATS(String cATS) {
		CATS = cATS;
	}

	public String getFechaCatS() {
		return fechaCatS;
	}

	public void setFechaCatS(String fechaCatS) {
		this.fechaCatS = fechaCatS;
	}

	public String getIndSim() {
		return indSim;
	}

	public void setIndSim(String indSim) {
		this.indSim = indSim;
	}

	public String getIndInfo() {
		return indInfo;
	}

	public void setIndInfo(String indInfo) {
		this.indInfo = indInfo;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public Boolean getIndSimBoolean(){
		if(this.getIndSim().equals("S")){
			return true;
		}else{
			return false;
		}
	}
	
	public void setIndSimBoolean(Boolean value){
		if(value){
			this.setIndSim("S");
		}else{
			this.setIndSim("N");
		}
	}

	/**
	 *  Este m�todo compara dos productos.
	 *  Seg�n documentaci�n los productos ofertados no pueden repetirse, por lo que dos productos con el mismo codigo de producto deben ser iguales
	 */
	@Override
	public boolean equals(Object o) {
		// TODO Auto-generated method stub
		if(o instanceof Producto){
			Producto inputProduct = (Producto)o;
			
			if(this.getCveProd().equalsIgnoreCase(inputProduct.getCveProd())){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

}
