package suitebancomer.aplicaciones.bbvacredit.gui.activities;

import suitebancomer.aplicaciones.bbvacredit.common.GuiTools;
import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public abstract class BaseActivity extends Activity {
	
	
	public final static int DONTSHOW_HEADERBUTTON = 1;
	
	/**
     * To use as parameter in aplicaEstilo method in order to show the header
     */
    public final static int SHOW_HEADER = 2;
    
    /**
     * To use as parameter in aplicaEstilo method in order to show the title
     */
    public final static int SHOW_TITLE = 4;
	
	/**
	 * Par�metros con los que se construir� la pantalla b�sica
	 */
	private int activityParameters;
	
	/**
     * Referencia al imagebutton del cabecero
     */
	private ImageButton headerRefresh;
	
	/**
     * Referencia al layout del cabecero
     */
    private LinearLayout mHeaderLayout;
    
    /**
     * Referencia al layout del t�tulo
     */
    private LinearLayout mTitleLayout;
    
    /**
     * Referencia al separador del t�tulo
     */
    private ImageView mTitleDivider;
	
	/**
	 * Layout where each view will be drawn.
	 */
	private ViewGroup mBodyLayout;
	
	/**
	 * Defines when the last click occurred
	 */
	private long touchDownTime;
	
	/**
     * A progress dialog for long waiting processes.
     */
    protected ProgressDialog mProgressDialog;
	
	/**
     * The alert error or information dialog.
     */
    private AlertDialog mAlertDialog;
    
    private boolean habilitado = true;
	
    
    public boolean isOnForeground=true;
    
	/**
     * Current view edit fields' references. Each view should implement this variable.
     */
	protected static EditText[] sFields;

	protected int getPosSelectedSpinner(Spinner spinner, String valueSel){
		int pos = 0;
		boolean cont = true;
		
		valueSel = valueSel.trim();
		if (spinner != null){
			for (int i=0; i< spinner.getAdapter().getCount() && cont; i++){
				if ( spinner.getAdapter().getItem(i).toString().trim().equalsIgnoreCase(valueSel) ){
					pos = i;
					cont = false;
				}
			}
		}
		return pos;
	}
	
	public void onCreate(Bundle savedInstanceState, int parameters, int layoutID) {
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		activityParameters = parameters;
		setContentView(R.layout.activity_base);
		
		// references to ui elements
        mHeaderLayout = (LinearLayout) findViewById(R.id.header_layout);
//        mTitleLayout = (LinearLayout) findViewById(R.id.title_layout);
//        mTitleDivider = (ImageView) findViewById(R.id.title_divider);
        mBodyLayout = (ViewGroup) findViewById(R.id.body_layout);
        
        // set header visibility
        mHeaderLayout.setVisibility(((activityParameters&SHOW_HEADER)==SHOW_HEADER)?View.VISIBLE:View.GONE);
        
        headerRefresh = (ImageButton) findViewById(R.id.headerRefresh);
        
        headerRefresh.setVisibility(((activityParameters&DONTSHOW_HEADERBUTTON)==DONTSHOW_HEADERBUTTON)?View.GONE:View.VISIBLE);
        //set title visibility, by default is gone until a call to aplicaEstilo is made
        //mTitleLayout.setVisibility(View.GONE);
        //mTitleDivider.setVisibility(View.GONE);
        
        // load received layout id in the body
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(layoutID, mBodyLayout, true);
        
        scaleForCurrentScreen();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return false;
	}
	
	/**
     * Places the title string and respective icon on screen.
     * @param titleText the screen title
     * @param iconResource the resource of the screen icon
     */
    public void setTitle(final int titleText, final int iconResource) {
    	setTitle(titleText, iconResource, R.color.segundo_rojo);
    }
    
    /**
     * Places the title string and respective icon on screen with the desired color
     * @param titleText the screen title
     * @param iconResource the resource of the screen icon
     */
    public void setTitle(final int titleText, final int iconResource, final int colorResource) {
    	
    	if ((activityParameters&SHOW_TITLE)==SHOW_TITLE) {
        	//mTitleLayout.setVisibility(View.VISIBLE);
        	//ImageView icon = (ImageView)mTitleLayout.findViewById(R.id.title_icon);
//        	if(iconResource > 0){
//        		icon.setImageDrawable(getResources().getDrawable(iconResource));
//        	}
//        	if(titleText > 0){
//        		String title = getString(titleText);
//        		TextView titleTextView = (TextView)mTitleLayout.findViewById(R.id.title_text);
//        		titleTextView.setTextColor(getResources().getColor(colorResource));
//        		titleTextView.setText(title);
//        	}
        	//mTitleDivider.setVisibility(View.VISIBLE);
        } else {
//        	mTitleLayout.setVisibility(View.GONE);
        	//mTitleDivider.setVisibility(View.GONE);
        }
    }
    
    /**
     * Overrides and cancels android's back button, if the button is visible then it does the same
     * as clicking on the application's back button.
     */
    @Override    
    public boolean onKeyDown(int keyCode, KeyEvent event) { 
    	onUserInteraction();
    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
//    			goBack();
    			MainController.getInstance().goBack();
            	return super.onKeyDown(keyCode, event);
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}
    }
    
    /*
	 * Called to process touch screen events. 
	 */
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		
	    switch (ev.getAction()){
		    case MotionEvent.ACTION_DOWN:
		        touchDownTime = SystemClock.elapsedRealtime();
		        break;
		        
		    case MotionEvent.ACTION_UP:
		    	//to avoid drag events
		        if (SystemClock.elapsedRealtime() - touchDownTime <= 150){	
		        	
		        	EditText[] textFields = this.getFields();
		        	if(textFields != null && textFields.length > 0){
		        		boolean clickIsOutsideEditTexts = true;
		        		
		        		for(EditText field : textFields){
		        			if(isPointInsideView(ev.getRawX(), ev.getRawY(), field)){
		        				clickIsOutsideEditTexts = false;
		        				break;
		        			}
		        		}
		        		
		        		if(clickIsOutsideEditTexts){
		        			this.hideSoftKeyboard();
		        		}        		
		        	} else {
		        		this.hideSoftKeyboard();
		        	}
		        }
		        break;
	    }
		
		return super.dispatchTouchEvent(ev);
	}
	
	/**
	 * Override this from children to return the view's fields.
	 * @return the current screen fields.
	 */
	protected EditText[] getFields(){
		return sFields;
	}
	
	/**
     * Determines if given points are inside view
     * @param x - x coordinate of point
     * @param y - y coordinate of point
     * @param view - view object to compare
     * @return true if the points are within view bounds, false otherwise
     */
    private boolean isPointInsideView(float x, float y, View view){
    	int location[] = new int[2];
		view.getLocationOnScreen(location);
		int viewX = location[0];
		int viewY = location[1];

		//point is inside view bounds
		if(( x > viewX && x < (viewX + view.getWidth())) &&
				( y > viewY && y < (viewY + view.getHeight()))){
			return true;
		} else {
			return false;
		}
    }
    
    /**
     * Hides the soft keyboard if any editbox called it.
     */
    public void hideSoftKeyboard() {
    	InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
    	View focusedView = getCurrentFocus();
    	if(imm != null && focusedView != null){
    		imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
    	}
    }
    
    /**
     * Places a progress dialog, for long waiting processes.
     * 
     * @param strTitle the dialog title
     * @param strMessage the message to show in the while
     */
    public void muestraIndicadorActividad(String strTitle, String strMessage) {
    	if(mProgressDialog != null){
    		ocultaIndicadorActividad();
    	}	
		mProgressDialog = ProgressDialog.show(this, strTitle, 
    			strMessage, true);
		mProgressDialog.setCancelable(false);
    }
    
    /**
     * Hides the progress dialog.
     */
    public void ocultaIndicadorActividad() {
    	if(mProgressDialog != null){
    		mProgressDialog.dismiss();
    	}
    }
    
    public void hideCurrentDialog() {
		try {
			if (mAlertDialog != null) {
				mAlertDialog.dismiss();
				mAlertDialog = null;
			}
		} catch(Exception ex) {
			
		}
	}
    
    public void setCurrentDialog(AlertDialog dialog) {
		this.mAlertDialog = dialog;
	}
    
    private ScrollView getScrollView(){
		return(ScrollView)findViewById(R.id.body_layout);		
	}

	protected void moverScroll() {
		getScrollView().post(new Runnable() {

			@Override
			public void run() {
				getScrollView().fullScroll(ScrollView.FOCUS_UP);
				Log.d(getClass().getName(), "Mover scroll");
			}
		});		
	}
    
    /**
     * Shows an error message if the message length is greater than 0. The alert
     * view title will be "Error", and it will have one button to accept.
     *
     * @param errorMessage the error message text to show
     *
     */
    public void showErrorMessage(String errorMessage, OnClickListener listener){
    	
    	if(errorMessage.length() > 0){ 		
    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    		alertDialogBuilder.setTitle(R.string.label_error);
    		alertDialogBuilder.setMessage(errorMessage);
    		alertDialogBuilder.setPositiveButton(R.string.label_ok, listener);
    		alertDialogBuilder.setCancelable(false);
        	mAlertDialog = alertDialogBuilder.show();		
    	}
    }
    
    /**
     * Shows an error message if the provided resource exists. The alert
     * view title will be "Error", and it will have one button to accept.
     *
     * @param errorMessage the error message resource to show
     */
    public void showErrorMessage(int errorMessage) {
    	if (errorMessage > 0) {	
    		showErrorMessage(getString(errorMessage), null);
    	}
    }
    
    public void showErrorMessage(String errorMessage) {
    	showErrorMessage(errorMessage, null);
    }


/***********************************************************************************************************/
	/**
	 * Muestra una alerta con el mensaje dado.
	 * @param message el mensaje a mostrar
	 */
	public void showInformationAlertPL(int message) {
		if (message > 0) {
			showInformationAlertPL(getString(message));
		}
	}


	/**
	 * Shows an information alert with the given message text. No action is performed after this alert is closed.
	 * @param message the message text to show
	 */
	public void showInformationAlertPL(String message) {
		if (message.length() > 0) {
			showInformationAlertPL(message, null);
		}
	}

	/**
	 * Shows an information alert with the given message.
	 * @param message the message resource to show
	 */
	public void showInformationAlertPL(int message, OnClickListener listener) {
		if (message > 0) {
			showInformationAlertPL(getString(message), listener);
		}
	}

	/**
	 * Shows an information alert with the given message text. A listener is
	 * passed to perform action on close.
	 * @param message the message text to show
	 * @param listener onClickListener to perform action on close
	 */
	public void showInformationAlertPL(String message, OnClickListener listener) {
		showInformationAlertPL(getString(R.string.label_information), message, listener);
	}

	/**
	 * Shows an alert dialog.
	 * @param title The alert title resource id.
	 * @param message The alert message resource id.
	 * @param listener The listener for the "Ok" button.
	 */
	public void showInformationAlertPL(int title, int message, OnClickListener listener) {
		if(message > 0) {
			showInformationAlertPL(getString(title), getString(message), listener);
		}
	}

	/**
	 * Shows an information alert with the given message text. A listener is
	 * passed to perform action on close.
	 * @param title The alert title resource id.
	 * @param message the message text to show
	 * @param listener onClickListener to perform action on close
	 */
	public void showInformationAlertPL(String title, String message, OnClickListener listener) {
		showInformationAlertPL(title, message, getString(R.string.label_ok), listener);
	}

	/**
	 * Shows an alert dialog.
	 * @param title The alert title resource id.
	 * @param message The alert message resource id.
	 * @param okText the ok text
	 * @param listener The listener for the "Ok" button.
	 */
	public void showInformationAlertPL(int title, int message, int okText, OnClickListener listener) {
		if(message > 0) {
			showInformationAlertPL(getString(title), getString(message), getString(okText), listener);
		}
	}

	/**
	 * Shows an alert dialog.
	 * @param title The alert title.
	 * @param message The alert message.
	 * @param okText the ok text
	 * @param listener The listener for the "Ok" button.
	 */
	public void showInformationAlertPL(String title, String message, String okText, OnClickListener listener) {
		//if(!habilitado)
		//	return;
		habilitado = false;
		if(message.length() > 0){
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
			alertDialogBuilder.setTitle(title);
			alertDialogBuilder.setMessage(message);
			if (null == listener) {
				listener = new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						setHabilitado(true);
						dialog.dismiss();
					}
				};
			}

			alertDialogBuilder.setPositiveButton(okText, listener);
			alertDialogBuilder.setCancelable(false);
			mAlertDialog = alertDialogBuilder.create();
			mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					habilitado = true;
				}
			});
			mAlertDialog.show();
		}
	}


/***********************************************************************************************************/


	/**
     * Muestra una alerta con el mensaje dado.
     * @param message el mensaje a mostrar
     */
    public void showInformationAlert(int message) {
    	if (message > 0) {		
    		showInformationAlert(getString(message));
    	}
    }
    
    /**
     * Shows an information alert with the given message text. No action is performed after this alert is closed.
     * @param message the message text to show
     */
    public void showInformationAlert(String message) {
    	if (message.length() > 0) {
    		showInformationAlert(message, null);
    	}
    }
    
    /**
     * Shows an information alert with the given message.
     * @param message the message resource to show
     */
    public void showInformationAlert(int message, OnClickListener listener) {
    	if (message > 0) {
    		showInformationAlert(getString(message), listener);
    	}
    }
    
	/**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(String message, OnClickListener listener) {
    	showInformationAlert(getString(R.string.label_information), message, listener);
    }
    
    /**
	 * Shows an alert dialog.
	 * @param title The alert title resource id.
	 * @param message The alert message resource id.
	 * @param listener The listener for the "Ok" button. 
	 */
    public void showInformationAlert(int title, int message, OnClickListener listener) {
    	if(message > 0) {
    		showInformationAlert(getString(title), getString(message), listener);
    	}
    }
    
    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param title The alert title resource id.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(String title, String message, OnClickListener listener) {
    	showInformationAlert(title, message, getString(R.string.label_ok), listener);
    }
    
    /**
	 * Shows an alert dialog.
	 * @param title The alert title resource id.
	 * @param message The alert message resource id.
	 * @param okText the ok text
	 * @param listener The listener for the "Ok" button. 
	 */
    public void showInformationAlert(int title, int message, int okText, OnClickListener listener) {
    	if(message > 0) {
    		showInformationAlert(getString(title), getString(message), getString(okText), listener);
    	}
    }
    
    /**
	 * Shows an alert dialog.
	 * @param title The alert title.
	 * @param message The alert message.
	 * @param okText the ok text
	 * @param listener The listener for the "Ok" button. 
	 */
	public void showInformationAlert(String title, String message, String okText, OnClickListener listener) {
		if(!habilitado)
			return;		
		habilitado = false;
    	if(message.length() > 0){
    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    		alertDialogBuilder.setTitle(title);
    		alertDialogBuilder.setMessage(message);
    		if (null == listener) {
    			listener = new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				};
    		}

    		alertDialogBuilder.setPositiveButton(okText, listener);
    		alertDialogBuilder.setCancelable(false);
    		mAlertDialog = alertDialogBuilder.create();
    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					habilitado = true;
				}
			});
    		mAlertDialog.show();
    	}
    }
	
	public void showYesNoAlert(String message, OnClickListener positiveListener) {
		showYesNoAlert(getString(R.string.label_information), 
					   message, 
					   getString(R.string.common_alert_yesno_positive_button), 
					   getString(R.string.common_alert_yesno_negative_button), 
					   positiveListener, 
					   null);
	}
	
	public void showYesNoAlert(int message, OnClickListener positiveListener) {
		showYesNoAlert(R.string.label_information, 
					   message, 
					   R.string.common_alert_yesno_positive_button, 
					   R.string.common_alert_yesno_negative_button, 
					   positiveListener, 
					   null);
	}
	
	public void showYesNoAlert(int message, OnClickListener positiveListener, OnClickListener negativeListener) {
		showYesNoAlert(R.string.label_information, 
					   message, 
					   R.string.common_alert_yesno_positive_button, 
					   R.string.common_alert_yesno_negative_button, 
					   positiveListener, 
					   negativeListener);
	}
	
	public void showYesNoAlert(String title, String message, OnClickListener positiveListener) {
		showYesNoAlert(title, 
					   message, 
					   getString(R.string.common_alert_yesno_positive_button), 
					   getString(R.string.common_alert_yesno_negative_button), 
					   positiveListener, 
					   null);
	}
	
	public void showYesNoAlert(int title, int message, OnClickListener positiveListener) {
		showYesNoAlert(title, 
					   message, 
					   R.string.common_alert_yesno_positive_button, 
					   R.string.common_alert_yesno_negative_button, 
					   positiveListener, 
					   null);
	}
	
	public void showYesNoAlertInverseButtons(int title, int message, OnClickListener positiveListener) {
		showYesNoAlert(title, 
					   message, 
					   R.string.common_alert_yesno_negative_button,
					   R.string.common_alert_yesno_positive_button,  
					   null, 
					   positiveListener);
	}
	
	public void showYesNoAlert(String title, String message, String okText, OnClickListener positiveListener) {
		showYesNoAlert(title, message, okText, getString(R.string.common_alert_yesno_negative_button), positiveListener, null);
	}
	
	public void showYesNoAlert(int title, int message, int okText, OnClickListener positiveListener) {
		showYesNoAlert(title, message, okText, R.string.common_alert_yesno_negative_button, positiveListener, null);
	}
	
	public void showYesNoAlert(String title, String message, String okText, String calcelText, OnClickListener positiveListener) {
		showYesNoAlert(title, message, okText, calcelText, positiveListener, null);
	}
	
	public void showYesNoAlert(int title, int message, int okText, int calcelText, OnClickListener positiveListener) {
		showYesNoAlert(title, message, okText, calcelText, positiveListener, null);
	}
	
	public void showYesNoAlert(int title, 
			   				   int message, 
			   				   int okText, 
			   				   int calcelText, 
			   				   OnClickListener positiveListener, 
			   				   OnClickListener negativeListener) {
		showYesNoAlert(getString(title), getString(message), getString(okText), getString(calcelText), positiveListener, negativeListener);
	}
	
	public void showYesNoAlert(String title, 
							   String message, 
							   String okText, 
							   String calcelText, 
							   OnClickListener positiveListener, 
							   OnClickListener negativeListener) {
		if(!habilitado)
			return;		
		habilitado = false;
    	if(message.length() > 0){
    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    		alertDialogBuilder.setTitle(title);
    		alertDialogBuilder.setMessage(message);
    		alertDialogBuilder.setPositiveButton(okText, positiveListener);
    		
    		if(null == negativeListener) {
    			alertDialogBuilder.setNegativeButton(calcelText, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						habilitado = true;
						dialog.dismiss();
					}
				});
    		} else {
    			alertDialogBuilder.setNegativeButton(calcelText, negativeListener);
    		}
    		
    		alertDialogBuilder.setCancelable(false);
    		mAlertDialog = alertDialogBuilder.create();
    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					habilitado = true;
				}
			});
    		mAlertDialog.show();
	    	
    	}
    }
	/** Revisar **/
	public void showMailAlert(String texto, 
			   OnClickListener positiveListener, 
			   OnClickListener negativeListener){  
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Te enviaremos el resumen \nde tu simulaci�n");

        final EditText input = new EditText(this);
        input.setText(texto);
        alert.setView(input);

        if(null == negativeListener) {
            alert.setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
    	        public void onClick(DialogInterface dialog, int whichButton) {
    	         String srt = input.getEditableText().toString();            
    	        } 
            });
		} else {
			alert.setPositiveButton("Cancelar", negativeListener);
		}
        
        alert.setNegativeButton("Aceptar", positiveListener);
        	
        AlertDialog dialog = alert.show();
        
        TextView messageView = (TextView)dialog.findViewById(android.R.id.message);
        messageView.setGravity(Gravity.CENTER);
        //AlertDialog alertDialog = alert.create();
        //alertDialog.show();
	}
	
	public void showNoButtonAlert(String title, String message){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setMessage(message);
		
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}

	/**
	 * Establece la l�gica a seguir para el redimensionamiento de la pantalla.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        
        guiTools.scale(mHeaderLayout);
        guiTools.scale(mHeaderLayout.findViewById(R.id.header_layout_in));
        guiTools.scale(mHeaderLayout.findViewById(R.id.simTCHeaderLogo));
        guiTools.scale(mHeaderLayout.findViewById(R.id.simTCHeaderBtn));
        guiTools.scale(mHeaderLayout.findViewById(R.id.headerRefresh));
        /*
        guiTools.scale(mHeaderLayout.findViewById(R.id.firstitem));
        guiTools.scale(mHeaderLayout.findViewById(R.id.seconditem));
        guiTools.scale(mHeaderLayout.findViewById(R.id.thirditem));*/
		//guiTools.scale(mTitleLayout);
		//guiTools.scale(mTitleLayout.findViewById(R.id.title_icon));
		//guiTools.scale(mTitleLayout.findViewById(R.id.title_text), true);
		//guiTools.scale(mTitleDivider);
		guiTools.scale(mBodyLayout);
	}
	
	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}
	
	public boolean isHabilitado() {
		return habilitado;
	}
	
	
	@Override
	public void onUserInteraction() {
		if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session
			if(MainController.getInstance().getStartBmovilInBack()!=null)
			MainController.getInstance().getStartBmovilInBack().onUserInteraction();
		}else{
			if(MainController.getInstance().getMenuSuiteController()!=null)
				MainController.getInstance().getMenuSuiteController().getParentViewsController().onUserInteraction();
		}
		super.onUserInteraction();
	}
	
	public boolean isScreenOn()
	{
		 PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		 return powerManager.isScreenOn();
	}
	
}
