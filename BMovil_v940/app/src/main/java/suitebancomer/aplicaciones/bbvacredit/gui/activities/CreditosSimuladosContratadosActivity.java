package suitebancomer.aplicaciones.bbvacredit.gui.activities;

import java.util.ArrayList;
import java.util.Iterator;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.common.GuiTools;
import suitebancomer.aplicaciones.bbvacredit.common.ListaDatosController;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.CreditosSimuladosContratadosDelegate;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.MenuPrincipalCreditDelegate;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.ResumenDelegate;
import suitebancomer.aplicaciones.bbvacredit.models.CreditoContratado;
import suitebancomer.aplicaciones.bbvacredit.models.Producto;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.R.layout;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ImageView;

public class CreditosSimuladosContratadosActivity extends BaseActivity implements OnClickListener {
	private Button enviarMailBtn;
	private Button regresarBtn;
	private CreditosSimuladosContratadosDelegate delegate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState,SHOW_HEADER|SHOW_TITLE|DONTSHOW_HEADERBUTTON, R.layout.activity_creditos_simulados_contratados);
		isOnForeground=true;
		MainController.getInstance().setCurrentActivity(this);
		
		init();
	}
	
	private void init() {
		delegate = new CreditosSimuladosContratadosDelegate();
		delegate.llenarTablas(this);
		scaleToCurrentScreen();
		mapearBotones();
	
		
	}
	
	private void mapearBotones() {
		
		enviarMailBtn = (Button)findViewById(R.id.ActCredSimContbtnEnvioEmail);
		enviarMailBtn.setOnClickListener(this);
		
		regresarBtn = (Button)findViewById(R.id.ActCredSimContbtnRegresar);
		regresarBtn.setOnClickListener(this);
		
	}
	
	public void showMailAlert(String texto, CreditosSimuladosContratadosActivity act){
		final CreditosSimuladosContratadosActivity activity = act;
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setMessage("Te enviaremos el resumen \nde tu simulaci�n");

		final EditText input = new EditText(this);
		input.setText(texto);
		alert.setView(input);

		alert.setPositiveButton("Cancelar", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
	         String srt = input.getEditableText().toString();            
	        } 
		});
     
     
		alert.setNegativeButton("Aceptar", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {
		         String srt = input.getEditableText().toString();   
		         delegate.sendEmail(activity, srt);
		    } 
     });
     	
     AlertDialog dialog = alert.show();
     
     TextView messageView = (TextView)dialog.findViewById(android.R.id.message);
     messageView.setGravity(Gravity.CENTER);
	}
	
	@Override
	public void onClick(View v) {
		
		
		if(v.getId()==R.id.ActCredSimContbtnEnvioEmail){
			// Datos de correo electronico de usuario
			delegate.getCorreo(this);
//			showMailAlert(delegate.getEmail(),this);
		}
		if(v.getId()==R.id.ActCredSimContbtnRegresar){
			isOnForeground=false;
			delegate.redirectToView(MenuPrincipalActivity.class);
		}
	
		}
		
	private void scaleToCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.pActCredSimContMainLayout));
		guiTools.scale(findViewById(R.id.pActCredSimContScrollLayout));
		guiTools.scale(findViewById(R.id.pActCredSimContLinearScroll));
		guiTools.scale(findViewById(R.id.pActCredSimContRelativeScroll));
		guiTools.scale(findViewById(R.id.ActCredSimContbtnEnvioEmail));
		guiTools.scale(findViewById(R.id.ActCredSimContbtnRegresar));
		guiTools.scale(findViewById(R.id.ActCredSimContimgFondo));
		guiTools.scale(findViewById(R.id.ActCredSimContimgResumen));
		guiTools.scale(findViewById(R.id.ActCredSimContimgResumen2));
		guiTools.scale(findViewById(R.id.ActCredSimContlblCredContratados),true);
		guiTools.scale(findViewById(R.id.ActCredSimContlblResumen),true);
		guiTools.scale(findViewById(R.id.ActCredSimContlblTituloCredSimulados),true);
		
		guiTools.scale(findViewById(R.id.creditosContratadosDetalle));
		guiTools.scale(findViewById(R.id.creditosSimuladosDetalle));
		guiTools.scale(findViewById(R.id.resumenSubTitle),true);
		
	
	}
	
	private void ocultaCreditosContratados(){
		((ImageView)findViewById(R.id.ActCredSimContimgResumen2)).setVisibility(View.GONE);
		((TextView)findViewById(R.id.ActCredSimContlblCredContratados)).setVisibility(View.GONE);;
		((LinearLayout)findViewById(R.id.creditosContratadosDetalle)).setVisibility(View.GONE);;
	}
	
	public void pintarTablaContratados(){
		if(delegate.getHaySimulacionSim()){
			ArrayList<CreditoContratado> lista = delegate.getCreditos();
			Iterator<CreditoContratado> it = lista.iterator();
			Integer cont = 0;
		
			ListaDatosController listaController;
			Boolean firstobj = true;
			while(it.hasNext()){
				CreditoContratado p = it.next();
				
				if(p.getIndicadorSim()){
					listaController = new ListaDatosController((LinearLayout)findViewById(R.id.creditosContratadosDetalle), this, this, R.id.creditosContratadosDetalle);
					
					listaController.addCreditoContratadoElement("Producto:", p.getDesProd(), firstobj, true);
					listaController.addCreditoContratadoElement("Número de contrato:",p.getContrato() , false, false);
					listaController.addCreditoContratadoElement("Pago mensual:", GuiTools.getMoneyString(p.getPagoMen().toString()), false, false);
					listaController.addCreditoContratadoElement("Total de tu crédito:", GuiTools.getMoneyString(p.getSaldo().toString()), false, false);
					
					++cont;
				}
			}
		}else{
			// Si no existen creditos simulados ocultamos
			ocultaCreditosContratados();
		}
	}
	
	private void ocultaCreditosSimulados(){
		((ImageView)findViewById(R.id.ActCredSimContimgResumen)).setVisibility(View.GONE);
		((TextView)findViewById(R.id.ActCredSimContlblTituloCredSimulados)).setVisibility(View.GONE);;
		((LinearLayout)findViewById(R.id.creditosSimuladosDetalle)).setVisibility(View.GONE);;
	}
	
	public void pintarTablaSimulados(){
		if(delegate.getHaySimulacion()){
			ArrayList<Producto> lista = delegate.getProductos();
			Iterator<Producto> it = lista.iterator();
		
			ListaDatosController listaController;
			while(it.hasNext()){
				Producto p = it.next();
		
				if(p.getIndSimBoolean()){
					
					if(p.getCveProd().equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
						listaController = new ListaDatosController((LinearLayout)findViewById(R.id.creditosSimuladosDetalle), this, this, R.id.creditosSimuladosDetalle);
						listaController.addCreditoContratadoElement("Producto:", p.getDesProd(), true, true);
						listaController.addCreditoContratadoElement("Tarjeta de crédito:",GuiTools.getTDCString(p.getSubproducto().get(0).getNumTDC()) , false, false);
						listaController.addCreditoContratadoElement("Línea anterior:", GuiTools.getMoneyString(delegate.getLineaCredito(p.getCveProd())), false, false);
						listaController.addCreditoContratadoElement("Crédito adicional:", GuiTools.getMoneyString(p.getSubproducto().get(0).getMonMax().toString()), false, false);
					}else if(p.getCveProd().equals(ConstantsCredit.TARJETA_CREDITO)){
						listaController = new ListaDatosController((LinearLayout)findViewById(R.id.creditosSimuladosDetalle), this, this, R.id.creditosSimuladosDetalle);
						listaController.addCreditoContratadoElement("Producto:", p.getDesProd(), true, true);
						listaController.addCreditoContratadoElement("Línea adicional:", GuiTools.getMoneyString(p.getSubproducto().get(0).getMonMax().toString()), false, false);
					}else{
						listaController = new ListaDatosController((LinearLayout)findViewById(R.id.creditosSimuladosDetalle), this, this, R.id.creditosSimuladosDetalle);
						listaController.addCreditoContratadoElement("Producto:", p.getDesProd(), true, true);
						listaController.addCreditoContratadoElement("Crédito disponible:", GuiTools.getMoneyString(p.getSubproducto().get(0).getMonMax().toString()), false, false);
						listaController.addCreditoContratadoElement("Crédito simulado:", GuiTools.getMoneyString(p.getMontoDeseS().toString()), false, false);
						listaController.addCreditoContratadoElement("Plazo:", p.getDesPlazoE(), false, false);
						listaController.addCreditoContratadoElement("Tasa:", p.getTasaS()+"%", false, false);
					}
				}
			}
		}else{
			// Si no existen creditos simulados ocultamos
			ocultaCreditosSimulados();
		}
	}
	
	@Override    
    public boolean onKeyDown(int keyCode, KeyEvent event) { 

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			isOnForeground = false;
    			delegate.redirectToView(MenuPrincipalActivity.class);
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}		
    }
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if(!isScreenOn() || (!this.isFinishing()&&isOnForeground))
		{
			SuiteApp.getInstance().closeBmovilAppSession();
		}
	}

}