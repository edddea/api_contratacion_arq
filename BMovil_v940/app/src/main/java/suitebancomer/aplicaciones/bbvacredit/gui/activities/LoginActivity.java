package suitebancomer.aplicaciones.bbvacredit.gui.activities;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.common.GuiTools;
import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.LoginDelegate;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.bancomer.mbanking.R;

public class LoginActivity extends BaseActivity implements OnClickListener {
	
	
	private EditText mUser;
	private EditText mPass;
	private LoginDelegate loginDelegate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_TITLE|SHOW_HEADER|DONTSHOW_HEADERBUTTON, R.layout.activity_login);
		MainController.getInstance().setCurrentActivity(this);
		
		init();
	}
	
	private void init() {
		
		loginDelegate = new LoginDelegate();
		configurarEditTextUsuario();
		configurarEditTextContrasena();
		configurarBotonEntrar();
		scaleToCurrentScreen();
	}
	
	private void configurarEditTextUsuario() {
		mUser = (EditText)findViewById(R.id.login_user_input);
		InputFilter[] userFilterArray = new InputFilter[1];
		userFilterArray[0] = new InputFilter.LengthFilter(ConstantsCredit.LONGITUD_USUARIO);
		mUser.setFilters(userFilterArray);
		loginDelegate.establecerUsuario(mUser);
	}
	
	private void configurarEditTextContrasena() {
		mPass = (EditText)findViewById(R.id.login_pass_input);
		mPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
		InputFilter[] passFilterArray = new InputFilter[1];
		passFilterArray[0] = new InputFilter.LengthFilter(ConstantsCredit.LONGITUD_CONTRASENA);
		mPass.setFilters(passFilterArray);
	}
	
	private void configurarBotonEntrar() {
		Button entrar = (Button)findViewById(R.id.login_access_button);
		entrar.setOnClickListener(this);
	}
	
	@Override 
	public void onClick (View arg0) {
            
		if (validarDatos()) {
			this.muestraIndicadorActividad("", "Procesando");
			loginDelegate.realizarLogin(loginDelegate.getUser(), mPass.getText().toString(), null);
			this.ocultaIndicadorActividad();
		}
	}
	
	public void clearPass(){
		mPass.setText("");
	}
	
	private boolean validarDatos() {
		boolean datosValidos = false;
		
		if (mPass.length() < ConstantsCredit.LONGITUD_CONTRASENA) {
			String message = getString(R.string.label_alert_information_lenght_pass_1)
					+ ConstantsCredit.ESPACIO
					+ ConstantsCredit.LONGITUD_CONTRASENA
					+ ConstantsCredit.ESPACIO
					+ getString(R.string.label_alert_information_lenght_pass_2);

			showInformationAlert(message);

		} else {
			datosValidos = true;
		}
		
		return datosValidos;
	}
	
	/**
	 * Escala los elementos de la pantalla para la resoluci�n actual.
	 */
	private void scaleToCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

        /*guiTools.scale(findViewById(R.id.header_layout));
        guiTools.scale(findViewById(R.id.header_layout_in));
        guiTools.scale(findViewById(R.id.simTCHeaderLogo));
        guiTools.scale(findViewById(R.id.simTCHeaderBtn));
        guiTools.scale(findViewById(R.id.headerRefresh));*/
		
		guiTools.scale(findViewById(R.id.loginLoginLayout));
		guiTools.scale(findViewById(R.id.login_login), true);
		
		guiTools.scale(findViewById(R.id.loginMiddleLayout));
		
		guiTools.scale(findViewById(R.id.loginNumeroCelularLayout));
		guiTools.scale(findViewById(R.id.login_user_label), true);
		guiTools.scale(findViewById(R.id.login_user_input));
		
		guiTools.scale(findViewById(R.id.loginContrasenaLayout));
		guiTools.scale(findViewById(R.id.login_pass_label), true);
		guiTools.scale(findViewById(R.id.login_pass_input));
		
		guiTools.scale(findViewById(R.id.btnEntrarLayout));
		guiTools.scale(findViewById(R.id.login_access_button));
		
		guiTools.scale(findViewById(R.id.loginFooterLayout));
		guiTools.scale(findViewById(R.id.imgLoginFooter));
		//guiTools.scale(findViewById(R.id.lblComoContratar), true);
	}
}
