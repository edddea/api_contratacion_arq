package suitebancomer.aplicaciones.bbvacredit.models;

import java.util.List;

public class ListaDineroMovil {
	
	private String versionCatalogo;
	
	private Integer ocurrencia;
	
	private List<DineroMovil> lista;

	public ListaDineroMovil(String versionCatalogo, Integer ocurrencia,
			List<DineroMovil> lista) {
		super();
		this.versionCatalogo = versionCatalogo;
		this.ocurrencia = ocurrencia;
		this.lista = lista;
	}

	public ListaDineroMovil() {
		// TODO Auto-generated constructor stub
	}

	public String getVersionCatalogo() {
		return versionCatalogo;
	}

	public void setVersionCatalogo(String versionCatalogo) {
		this.versionCatalogo = versionCatalogo;
	}

	public Integer getOcurrencia() {
		return ocurrencia;
	}

	public void setOcurrencia(Integer ocurrencia) {
		this.ocurrencia = ocurrencia;
	}

	public List<DineroMovil> getLista() {
		return lista;
	}

	public void setLista(List<DineroMovil> lista) {
		this.lista = lista;
	}
	
}
