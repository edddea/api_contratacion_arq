package suitebancomer.aplicaciones.bbvacredit.gui.activities;

import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bbvacredit.common.GuiTools;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.SimulaCreditoHipotecarioDelegate;
import suitebancomer.aplicaciones.bbvacredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import tracking.TrackingHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class SimulaCreditoHipotecarioContratoActivity extends BaseActivity  implements OnClickListener, OnSeekBarChangeListener{

	private SimulaCreditoHipotecarioDelegate delegate;
	private ImageButton backButton;
	private ImageButton btnSimular;
	
	// Menu Button
	private Button resumenBtn;
	private Button btnContacto;
	
	// Botones help
	private ImageButton helpAdqBtn;
	private ImageButton helpLiqBtn;
	
	// Botones Seekbar
	private Button seekPlus;
	private Button seekMinus;
	private SeekBar seekbar;
	private Spinner spinner;
	private CheckBox checkAdquisicion;
	private CheckBox checkLiquidez;
	
	//Label monto disponible
	private TextView disponible;
	
	private boolean isOcultaInfoEnabled;

	public SeekBar getSeekbar() {
		return seekbar;
	}

	public void setSeekbar(SeekBar seekbar) {
		this.seekbar = seekbar;
	}

	public Spinner getSpinner() {
		return spinner;
	}

	public void setSpinner(Spinner spinner) {
		this.spinner = spinner;
	}

	public CheckBox getCheckAdquisicion() {
		return checkAdquisicion;
	}

	public void setCheckAdquisicion(CheckBox checkAdquisicion) {
		this.checkAdquisicion = checkAdquisicion;
	}

	public CheckBox getCheckLiquidez() {
		return checkLiquidez;
	}

	public void setCheckLiquidez(CheckBox checkLiquidez) {
		this.checkLiquidez = checkLiquidez;
	}
	private BmovilViewsController parentManager;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState,SHOW_HEADER|SHOW_TITLE, R.layout.activity_simula_credito_hipotecario_contrato);
		isOnForeground=true;
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("creditsimHipoCont", parentManager.estados);
		MainController.getInstance().setCurrentActivity(this);
		isOcultaInfoEnabled=false;
		
		init();
	}
	
	private void init(){
		Tools.setIsContratacionPreference(true);
		ocultaInfoNC();
		Tools.getCurrentSession().setHipoCAct(this);
		delegate = new SimulaCreditoHipotecarioDelegate();
		inicializaBotones();
		mapearBotones();	
		
		if((delegate.getProducto()!=null) && delegate.getProducto().getIndSimBoolean()) this.pintarInfoTabla();
				
		scaleToCurrentScreen();
		checkAdquisicion.setChecked(true);
		setEventToSpinner();
		//Tools.getCurrentSession().setVisibilityButton(this, R.id.simCHipoCBottomMenuRC);
	}
	
	private void inicializaBotones(){
		
		backButton = (ImageButton)findViewById(R.id.headerRefresh);
		helpAdqBtn = (ImageButton)findViewById(R.id.simCHipoCasqAdq);
		helpLiqBtn = (ImageButton)findViewById(R.id.simCHipoCasqLiq);
		resumenBtn = (Button)findViewById(R.id.simCHipoCBottomMenuRC);
		btnContacto = (Button)findViewById(R.id.simCHipoCBottomContacto);
		btnSimular = (ImageButton)findViewById(R.id.simCHipoCBtnSimular);
		seekPlus = (Button)findViewById(R.id.simCHipoCSeekbarMin);
		seekMinus = (Button)findViewById(R.id.simCHipoCSeekbarPlus);
		seekbar = (SeekBar)findViewById(R.id.simCHipoCSeekBar);
		spinner = (Spinner)findViewById(R.id.simCHipoCSpinner);
		checkAdquisicion = (CheckBox)findViewById(R.id.simCHipoCradioAdq);
		checkLiquidez = (CheckBox)findViewById(R.id.radioButton2);
		disponible = (TextView)findViewById(R.id.simCHipoCLMon);
		
	}
	
	private void mapearBotones(){		
		backButton = (ImageButton)findViewById(R.id.headerRefresh);
		backButton.setOnClickListener(this);
		
		helpAdqBtn = (ImageButton)findViewById(R.id.simCHipoCasqAdq);
		helpAdqBtn.setOnClickListener(this);
		
		helpLiqBtn = (ImageButton)findViewById(R.id.simCHipoCasqLiq);
		helpLiqBtn.setOnClickListener(this);
		
		resumenBtn = (Button)findViewById(R.id.simCHipoCBottomMenuRC);
		resumenBtn.setOnClickListener(this);
		
		btnContacto = (Button)findViewById(R.id.simCHipoCBottomContacto);
		btnContacto.setOnClickListener(this);
		
		btnSimular = (ImageButton)findViewById(R.id.simCHipoCBtnSimular);
		btnSimular.setOnClickListener(this);
		
		seekPlus = (Button)findViewById(R.id.simCHipoCSeekbarMin);
		seekPlus.setOnClickListener(this);
		
		seekMinus = (Button)findViewById(R.id.simCHipoCSeekbarPlus);
		seekMinus.setOnClickListener(this);
		
		seekbar = (SeekBar)findViewById(R.id.simCHipoCSeekBar);
		seekbar.setMax(delegate.getMontoMax());
		seekbar.setProgress(delegate.getMontoProgress());
		seekbar.setOnSeekBarChangeListener(this);		
		seekbar.refreshDrawableState();
		
		TextView valueSeekBar =(TextView) findViewById(R.id.simCHipoCSeekbarText2);
		valueSeekBar.setText(GuiTools.getMoneyString(delegate.getMontoMax().toString()));

		//TextView disponible = (TextView)findViewById(R.id.simCHipoCLMon);
		if(delegate.getProducto() != null){
			disponible.setText(GuiTools.getMoneyString(String.valueOf(delegate.getProducto().getSubproducto().get(0).getMonMax())));
		}else{
			disponible.setText(GuiTools.getMoneyString(String.valueOf(0)));
		}
		
		spinner = (Spinner)findViewById(R.id.simCHipoCSpinner);
		setPlazos();
		
		Boolean setliquidinit = false;
		checkAdquisicion = (CheckBox)findViewById(R.id.simCHipoCradioAdq);
		checkAdquisicion.setOnClickListener(this);
		if(!delegate.mostrarCombo(Producto.ADQUISICION_CODE)){
			((LinearLayout)findViewById(R.id.simCHipoCRadioRow1)).setVisibility(View.GONE);
			setliquidinit = true;
		}else{
			((LinearLayout)findViewById(R.id.simCHipoCRadioRow1)).setVisibility(View.VISIBLE);
		}

		checkLiquidez = (CheckBox)findViewById(R.id.radioButton2);
		checkLiquidez.setOnClickListener(this);	
		
		if(!delegate.mostrarCombo(Producto.LIQUIDEZ_CODE)){
			((LinearLayout)findViewById(R.id.simCHipoCRadioRow2)).setVisibility(View.GONE);
		}else{
			((LinearLayout)findViewById(R.id.simCHipoCRadioRow2)).setVisibility(View.VISIBLE);
			if(setliquidinit) checkLiquidez.setChecked(true);
		}
	}
	
	public void setPlazos(){
		ArrayAdapter<String> dataAdapter = 
				new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, delegate.getPlazos());		
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
	}
	
	public void mockData(){
		Spinner spinner = (Spinner) findViewById(R.id.simCHipoCSpinner);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.plazo_array, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {
		isOnForeground = false;
		if(v.getId() == R.id.headerRefresh){			
			delegate.redirectToView(MenuPrincipalActivity.class);
		// Boton pregunta Adq
		}else if(v.getId() == R.id.simCHipoCasqAdq){
			showInformationAlert(R.string.simCHipoC_dialog_label_adquisicion);
		// Boton pregunta Liq
		}else if(v.getId() == R.id.simCHipoCasqLiq){
			showInformationAlert(R.string.simCHipoC_dialog_label_liquidez);
		}else if(v.getId() == R.id.simCHipoCBottomMenuRC){
			delegate.redirectToView(ResumenActivity.class);
		}else if(v.getId() == R.id.simCHipoCBottomContacto)
		{
			isOnForeground=false;
			String params = "";
			Session sesion = Session.getInstance(this);
			params = "nombredepila=" + sesion.getNombreCliente().split(" ")[0] + "&segmento=HIP&credito=" + seekbar.getProgress() + "&idpagina=simulacion";
			Log.d("Credit","params = " + params);
			Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
			click_paso2_operacion.put("inicio","event45");
			click_paso2_operacion.put("&&products","simulador;simulador:simulador credito hipotecario");
			click_paso2_operacion.put("eVar12", "seleccion contactame");
			TrackingHelper.trackPaso1Operacion(click_paso2_operacion);
			PopUpContactameActivity.muestraPop(this, params);
			//JQH PopUpContactameActivity.muestraPop(this);
			//Log.e("Selecciono contacto","....");
		}else if(v.getId() == R.id.simCHipoCBtnSimular)
		{
			isOcultaInfoEnabled=false;
		//	delegate.doRecalculoContract(this);	
			Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
			click_paso2_operacion.put("evento_realizada","event52");
			click_paso2_operacion.put("&&products","simulador;simulador:simulador credito hipotecario");
			click_paso2_operacion.put("eVar12","simulacion realizada");
			TrackingHelper.trackSimulacionRealizada(click_paso2_operacion);
			deleteSimulation();
		
		}else if(v.getId() == R.id.simCHipoCSeekbarPlus){
			seekbar = (SeekBar)findViewById(R.id.simCHipoCSeekBar);
			if(seekbar.getProgress() < seekbar.getMax()){
				Integer val = seekbar.getMax() - seekbar.getProgress(); 
				if(val>=1000){
					seekbar.setProgress(seekbar.getProgress()+1000);
				}else{
					seekbar.setProgress(seekbar.getProgress()+val);
				}
				if(seekbar.getProgress()<=delegate.getMontoMax()){ 
					//delegate.doRecalculoContract(this);
					}
			}
		}else if(v.getId() == R.id.simCHipoCSeekbarMin){
			seekbar = (SeekBar)findViewById(R.id.simCHipoCSeekBar);
			if(seekbar.getProgress() > delegate.getMontoMin()){
				if((seekbar.getProgress() >= 1000)&&(delegate.getMontoMin() <= (seekbar.getProgress()-1000))){
					seekbar.setProgress(seekbar.getProgress()-1000);
				}else{
					seekbar.setProgress(delegate.getMontoMin());
				}
				if(seekbar.getProgress()>=delegate.getMontoMin()){ 
					//delegate.doRecalculoContract(this);
					}
			}
		}else if(v.getId() == R.id.simCHipoCradioAdq){
			checkAdquisicion.setChecked(true);
			if (checkLiquidez.isChecked()){
				checkLiquidez.setChecked(false);
			}
			// recarga de plazos
			Producto p = delegate.getProducto();
			delegate.cargarPlazosOnCode(Producto.ADQUISICION_CODE);
			setPlazos();
			setMontos( p, Producto.ADQUISICION_CODE);
		}else if(v.getId() == R.id.radioButton2){
			checkLiquidez.setChecked(true);
			if (checkAdquisicion.isChecked()){
				checkAdquisicion.setChecked(false);
			}
			Producto p = delegate.getProducto();
			setMontos( p, Producto.LIQUIDEZ_CODE);
			// recarga de plazos
			delegate.cargarPlazosOnCode(Producto.LIQUIDEZ_CODE);
			setPlazos();
		}			
	}
	
	private void scaleToCurrentScreen(){
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.simCHipoCLayout));
		guiTools.scale(findViewById(R.id.simCHipoCBodyLayout));
		guiTools.scale(findViewById(R.id.simCHipoCTitle));
		guiTools.scale(findViewById(R.id.simCHipoCLayoutSubTitle));
		guiTools.scale(findViewById(R.id.simCHipoCSubTitleTCImg));
		guiTools.scale(findViewById(R.id.simCHipoCSubTitleMiddleTxt));
		guiTools.scale(findViewById(R.id.simCHipoCLinearLayout));
		guiTools.scale(findViewById(R.id.simCHipoCLText));
		guiTools.scale(findViewById(R.id.simCHipoCLMon));
		guiTools.scale(findViewById(R.id.simCHipoCMTextSL));
		guiTools.scale(findViewById(R.id.simCHipoCLineaSeparacion));
		guiTools.scale(findViewById(R.id.simCHipoCMTextSL2));
		guiTools.scale(findViewById(R.id.simCHipoCLineaSeparacion2));
		guiTools.scale(findViewById(R.id.simCHipoCRadioRow1));
		guiTools.scale(findViewById(R.id.simCHipoCRadioRow2));
		guiTools.scale(findViewById(R.id.simCHipoCradioAdq));
		guiTools.scale(findViewById(R.id.radioButton2));
		guiTools.scale(findViewById(R.id.simCHipoCasqAdq));
		guiTools.scale(findViewById(R.id.simCHipoCasqLiq));
		guiTools.scale(findViewById(R.id.simCHipoCEstado));
		guiTools.scale(findViewById(R.id.simCHipoCSpinner));
		guiTools.scale(findViewById(R.id.simCHipoCBottomLayout));
		guiTools.scale(findViewById(R.id.simCHipoCBottomMenuRC));
		guiTools.scale(findViewById(R.id.simCHipoCBottomContacto));
		guiTools.scale(findViewById(R.id.simCHipoCBtnSimular));
		guiTools.scale(findViewById(R.id.simCHipoCRadiosLayout));

		guiTools.scale(findViewById(R.id.simCHipoCSeekBar));
		guiTools.scale(findViewById(R.id.simChipoCSeekbarLayout));
		guiTools.scale(findViewById(R.id.simCHipoCSeekbarPlus));
		guiTools.scale(findViewById(R.id.simCHipoCSeekbarMin));
		guiTools.scale(findViewById(R.id.simCHipoCCatFecha),true);
		guiTools.scale(findViewById(R.id.simCHipoCCatText),true);
		guiTools.scale(findViewById(R.id.simCHipoCCatPorc),true);
		guiTools.scale(findViewById(R.id.simCHipoCCat),true);
		guiTools.scale(findViewById(R.id.simCHipoCCatInfo));
		guiTools.scale(findViewById(R.id.simCHipoCInfo));
		guiTools.scale(findViewById(R.id.simCHipoClblDuracion),true);
		guiTools.scale(findViewById(R.id.simCHipoClblResultadoTasa),true);
		guiTools.scale(findViewById(R.id.simCHipoClblResultadoSimulacion2),true);
		guiTools.scale(findViewById(R.id.simCHipoCLinearLayout2));
		guiTools.scale(findViewById(R.id.simCHipoClblResultadoPago),true);
		guiTools.scale(findViewById(R.id.simCHipoClblResultadoSimulacion),true);
		guiTools.scale(findViewById(R.id.simCHipoCLinearLayoutTexto));

		guiTools.scale(findViewById(R.id.simCHipoCSeekbarText),true);
		guiTools.scale(findViewById(R.id.simCHipoCSeekbarText2),true);
		
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		TextView text = (TextView)findViewById(R.id.simCHipoCSeekbarText2);
		text.setText(GuiTools.getMoneyString(String.valueOf(seekbar.getProgress())));
		
		ocultaInfoNC();
		
		if(seekbar.getProgress() == delegate.getMontoMax()){
			seekbar.setProgress(seekbar.getProgress());
		}else{
			seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
		}
		
//		if(seekbar.getProgress()>=delegate.getMontoMin()) 
//			delegate.doRecalculoContract(this);		
	}

	public void pintarInfoTabla(){
		
		//Quitar bloqueo
		MainController.getInstance().ocultaIndicadorActividad();
		
		ocultaInfoNC();		
		
		TextView porcentajeCat = (TextView)findViewById(R.id.simCHipoCCatPorc);
		TextView fechaCat = (TextView)findViewById(R.id.simCHipoCCatFecha);
		TextView resultadoPago = (TextView)findViewById(R.id.simCHipoClblResultadoPago);
		TextView resultadoTasa = (TextView)findViewById(R.id.simCHipoClblResultadoTasa);

		
		Producto p = delegate.getProducto();
		
		TextView text = (TextView)findViewById(R.id.simCHipoCSeekbarText2);
		text.setText(GuiTools.getMoneyString(String.valueOf(seekbar.getProgress())));
		
		
		if ( (p!=null) && p.getIndSimBoolean() ){
			//seekbar.setProgress(Double.parseDouble(p.getMontoDeseS()));
			
			if (p.getDessubpE().equalsIgnoreCase(Producto.LIQUIDEZ_CODE)){
				checkAdquisicion.setChecked(false);
				checkLiquidez.setChecked(true);
				delegate.cargarPlazosOnCode(Producto.LIQUIDEZ_CODE);
				setPlazos();
				setMontos(p, Producto.LIQUIDEZ_CODE);
				
			}else{
				checkAdquisicion.setChecked(true);
				checkLiquidez.setChecked(false);
				delegate.cargarPlazosOnCode(Producto.ADQUISICION_CODE);
				setPlazos();
				setMontos(p, Producto.ADQUISICION_CODE);
			}
			
			if(!delegate.mostrarCombo(Producto.ADQUISICION_CODE)){
				((LinearLayout)findViewById(R.id.simCHipoCRadioRow1)).setVisibility(View.GONE);
			}else{
				((LinearLayout)findViewById(R.id.simCHipoCRadioRow1)).setVisibility(View.VISIBLE);
			}
			
			if(!delegate.mostrarCombo(Producto.LIQUIDEZ_CODE)){
				((LinearLayout)findViewById(R.id.simCHipoCRadioRow2)).setVisibility(View.GONE);
			}else{
				((LinearLayout)findViewById(R.id.simCHipoCRadioRow2)).setVisibility(View.VISIBLE);
			}

			spinner.setSelection(getPosSelectedSpinner(spinner, p.getDesPlazoE()));
		}
		
		String textoPorcCat = p.getCATS(); 
		porcentajeCat.setText(textoPorcCat);
		fechaCat.setText("Fecha de calculo al "+p.getFechaCatS().toLowerCase());
		
		resultadoPago.setText(GuiTools.getMoneyString(String.valueOf(p.getPagMProdS())));
		resultadoTasa.setText(p.getTasaS()+"%");		
		
		muestraInfoNC();
	}

	private void setMontos(Producto p, String code) {
		for(int i=0;i<p.getSubproducto().size();i++){
			if(p.getSubproducto().get(i).getDesSubp().equalsIgnoreCase(code)){
				//if(p.getSubproducto().get(i).getMonMax()<seekbar.getMax() && seekbar.getProgress()>p.getSubproducto().get(i).getMonMax()){
				seekbar.setMax(p.getSubproducto().get(i).getMonMax());
					seekbar.setProgress(p.getSubproducto().get(i).getMonMax());
					delegate.setMontoMax(p.getSubproducto().get(i).getMonMax());
					delegate.setMontoMin(p.getSubproducto().get(i).getMonMin());

				//}
				//seekbar.setMax(p.getSubproducto().get(i).getMonMax());
				disponible.setText(GuiTools.getMoneyString(String.valueOf(p.getSubproducto().get(i).getMonMax())));

			}
		}

		
		
	}

	private void ocultaInfoNC(){
		
		TextView info = (TextView)findViewById(R.id.simCHipoCInfo);
		info.setVisibility(View.INVISIBLE);
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCHipoCLinearLayoutTexto);
		tlayout.setVisibility(View.INVISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.simCHipoCCatInfo);
		tableLayout.setVisibility(View.INVISIBLE);
		
		btnContacto = (Button)findViewById(R.id.simCHipoCBottomContacto);
		btnContacto.setVisibility(View.INVISIBLE);
	}
	
	public void subir()
	{

		LinearLayout p=(LinearLayout)findViewById(R.id.simCHipoCBottomLayout);
		ImageButton bn=(ImageButton)findViewById(R.id.simCHipoCBtnSimular);
		p.setY(Tools.getPosicionBarraInferiorS(bn));


	}
	
	public void bajar()
	{
		LinearLayout d=(LinearLayout)findViewById(R.id.simCHipoCBodyLayout);
		LinearLayout p=(LinearLayout)findViewById(R.id.simCHipoCBottomLayout);
		p.setY(Tools.getPosicionBarraInferiorB(d));

	}
	private void muestraInfoNC(){
		
		TextView info = (TextView)findViewById(R.id.simCHipoCInfo);
		info.setVisibility(View.VISIBLE);
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCHipoCLinearLayoutTexto);
		tlayout.setVisibility(View.VISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.simCHipoCCatInfo);
		tableLayout.setVisibility(View.VISIBLE);
		
		btnContacto = (Button)findViewById(R.id.simCHipoCBottomContacto);
		btnContacto.setVisibility(View.VISIBLE);
		bajar();
		
	}
	
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
//		seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
//		if(seekbar.getProgress()<delegate.getMontoMin()){
//			seekbar.setProgress(delegate.getMontoMin());
//		}
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
//		seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
//		if(seekbar.getProgress()<delegate.getMontoMin()){
//			seekbar.setProgress(delegate.getMontoMin());
//		}
		if(seekbar.getProgress()>=delegate.getMontoMin()){ 
		//delegate.doRecalculoContract(this);
		}else{
			seekbar.setProgress(delegate.getMontoMin());
			//delegate.doRecalculoContract(this);
		}
	}
	
	@Override    
    public boolean onKeyDown(int keyCode, KeyEvent event) { 

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			isOnForeground = false;
    			delegate.redirectToView(MenuPrincipalActivity.class);
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}		
    }
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if(!isScreenOn() || (!this.isFinishing()&&isOnForeground))
		{
			SuiteApp.getInstance().closeBmovilAppSession();
		}
	}
	
	
	private void setEventToSpinner()
	{
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) { 
		    	if(isOcultaInfoEnabled)
		    		ocultaInfoNC();
		    	isOcultaInfoEnabled=true;
		    } 

		    public void onNothingSelected(AdapterView<?> adapterView) {
		        return;
		    } 
		}); 
	}
	
	/**
	 * Created June 8th, 2015,
	 */
	
	private void deleteSimulation()
	{
		delegate.deleteSimulationRequest(this);
	}
	
	public void doRecalculo()
	{
		Log.d("Elimina Simulacion","ok");
		delegate.doRecalculoContract(this);
	}
	
}
