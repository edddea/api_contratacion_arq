package suitebancomer.aplicaciones.bbvacredit.gui.activities;

import java.util.Timer;
import java.util.TimerTask;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bancomer.mbanking.R;

public class SplashActivity extends BaseActivity {
	
	/**
	 * The timer for presenting the splash
	 */
	private Timer splashTimer;
	
	/**
	 * The action performed when the timer expires
	 */
	private TimerTask splashTimerTask;
	
	private boolean isAppCanceled;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, 0, R.layout.activity_splash);
		
		MainController.getInstance().setCurrentActivity(this);
		MainController.getInstance().setContext(this);
		
		ImageView img = (ImageView)findViewById(R.id.splashImage);
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		img.getLayoutParams().height = metrics.heightPixels;
	
	}
	
	/**
	 * Method used when this activity awakes from pause state
	 */
	@Override
	protected void onResume() {
		super.onResume();
		
		splashTimer = new Timer();
		splashTimerTask = new SplashTask();
		
		splashTimer.schedule(splashTimerTask, ConstantsCredit.SPLASH_VIEW_CONTROLLER_DURATION);
	}
	
	@Override
	protected void onPause() {
		//goBack();
		super.onPause();
		splashTimer.cancel();
	}
	
	/**
     * Overrides and cancels android's back button, if the button is visible then it does the same
     * as clicking on the application's back button.
     */
    @Override    
    public boolean onKeyDown(int keyCode, KeyEvent event) { 

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			splashTimer.cancel();
    			splashTimerTask.cancel();
    			isAppCanceled = true;
    			android.os.Process.killProcess(android.os.Process.myPid());
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}		
    }
    
    /**
	 * Use handler to respond to splash timeout.
	 */
	private Handler mApplicationHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (!isAppCanceled) {
				MainController.getInstance().showScreen(LoginActivity.class);
			}
		}
	};
	
	/**
	 * Class that indicates when to start the application
	 */
	private class SplashTask extends TimerTask {

		/**
		 * The actions to be performed
		 */
		@Override
		public void run() {
			Message msg = new Message();
			mApplicationHandler.sendMessage(msg);
		}
		
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		return true;
	}
}
