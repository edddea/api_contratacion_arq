package suitebancomer.aplicaciones.bbvacredit.gui.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import suitebancomer.aplicaciones.bbvacredit.common.GuiTools;
import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.DetalleDeAlternativaDelegate;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.LineaCreditoDelegate;
import suitebancomer.aplicaciones.bbvacredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import tracking.TrackingHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class LineaCreditoActivity extends BaseActivity implements OnClickListener, OnSeekBarChangeListener{
	
	private LineaCreditoDelegate delegate;
	private ImageButton refreshBtn;
	private ImageButton btnSimular;
	
	// Menu Button
	private Button resumenBtn;
	private Button btnAdquirir;
	
	// Botones Seekbar
	private Button seekPlus;
	private Button seekMinus;
	private SeekBar seekbar;

	private Double saldo;
	public SeekBar getSeekbar() {
		return seekbar;
	}

	public void setSeekbar(SeekBar seekbar) {
		this.seekbar = seekbar;
	}
	private BmovilViewsController parentManager;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.activity_linea_credito);
		Log.e("ILC","Credit");
		isOnForeground=true; 
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("creditsimILCCont", parentManager.estados);
		MainController.getInstance().setCurrentActivity(this);
		init();
	}
	
	private void init(){
		Tools.setIsContratacionPreference(true);
		ocultaInfoNC();
		Tools.getCurrentSession().setLineaAct(this);
		delegate = new LineaCreditoDelegate();
		
		TextView textTDC = (TextView)findViewById(R.id.incLCredSubTitleEndText);
		textTDC.setText(GuiTools.getTDCString2Asteriscos(delegate.getProducto().getNumTDCS()));
		if((delegate.getProducto()!=null) && delegate.getProducto().getIndSimBoolean()){
			//this.pintarInfoTabla( (delegate.getDataTDC() == null)?null:(delegate.getDataTDC().getSC())  );			
			this.pintarInfoTabla();
		}
		
		scaleToCurrentScreen();
		mapearBotones();
		
		//Tools.getCurrentSession().setVisibilityButton(this, R.id.incLCredBottomMenuRC);

		
		/*
		TextView text3 = (TextView)findViewById(R.id.incLCredMText2);
		//text3.setVisibility(View.GONE);

		TextView text4 = (TextView)findViewById(R.id.incLCredMCred2);
		//text4.setVisibility(View.GONE);
		
		TextView text3 = (TextView)findViewById(R.id.incLCredMText3);
		text3.setVisibility(View.GONE);
		
		TextView cred3 = (TextView)findViewById(R.id.incLCredMCred3);
		cred3.setVisibility(View.GONE);
		
		TextView text4 = (TextView)findViewById(R.id.incLCredMText4);
		text4.setVisibility(View.GONE);
		
		TextView text5 = (TextView)findViewById(R.id.incLCredMText5);
		text5.setVisibility(View.GONE);
		*/
		
	} 
	
	public void muestraSaldos(String saldo){
		this.saldo = Double.parseDouble(saldo);
		TextView textSaldo = (TextView)findViewById(R.id.incLCredMCred1);
		TextView disponible = (TextView)findViewById(R.id.incLCredMCred2);
		
		if (saldo != null){
			this.saldo = this.saldo/100;
			textSaldo.setText(GuiTools.getMoneyString(String.valueOf(this.saldo)));
		}

		if(delegate.getProducto() != null){
			
			disponible.setText(GuiTools.getMoneyString(String.valueOf(delegate.getProducto().getSubproducto().get(0).getMonMax())));
		}else{
			disponible.setText(GuiTools.getMoneyString(String.valueOf(0)));
		}
	}
	
	private void mapearBotones(){
		refreshBtn = (ImageButton)findViewById(R.id.headerRefresh);
		refreshBtn.setOnClickListener(this);
		
		btnSimular = (ImageButton)findViewById(R.id.incLCredBtnSimular);
		btnSimular.setOnClickListener(this);
		
		resumenBtn = (Button)findViewById(R.id.incLCredBottomMenuRC);
		resumenBtn.setOnClickListener(this);
		
		btnAdquirir = (Button)findViewById(R.id.incLCredBottomAdquirir);
		btnAdquirir.setOnClickListener(this);
		
		seekPlus = (Button)findViewById(R.id.incLCredSeekbarMin);
		seekPlus.setOnClickListener(this);
		
		seekMinus = (Button)findViewById(R.id.incLCredSeekbarPlus);
		seekMinus.setOnClickListener(this);
		
		seekbar = (SeekBar)findViewById(R.id.incLCredSeekBar);
		seekbar.setMax(delegate.getMontoMax());
		seekbar.setProgress(delegate.getMontoProgress());
		seekbar.setOnSeekBarChangeListener(this);
		
		TextView text = (TextView)findViewById(R.id.incLCredSeekbarText2);
		text.setText(GuiTools.getMoneyString(String.valueOf(seekbar.getProgress())));
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.linea_credito, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Escala los elementos de la pantalla para la resoluci�n actual.
	 */
	private void scaleToCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.incLCredLayout));
		
		guiTools.scale(findViewById(R.id.incLCredBodyLayout));
		guiTools.scale(findViewById(R.id.incLCredTitle),true);
		guiTools.scale(findViewById(R.id.incLCredLayoutSubTitle));
		guiTools.scale(findViewById(R.id.incLCredSubTitleTCImg));
		guiTools.scale(findViewById(R.id.incLCredSubTitleMiddleTxt),true);
		guiTools.scale(findViewById(R.id.incLCredSubTitleEndText),true);
		
		guiTools.scale(findViewById(R.id.incLCredMText1),true);
		guiTools.scale(findViewById(R.id.incLCredMCred1),true);
		guiTools.scale(findViewById(R.id.incLCredMText2),true);
		guiTools.scale(findViewById(R.id.incLCredMCred2),true);
//		guiTools.scale(findViewById(R.id.incLCredMText3),true);
		
//		guiTools.scale(findViewById(R.id.incLCredLinearTextLayout));
//		guiTools.scale(findViewById(R.id.incLCredMText4),true);
//		guiTools.scale(findViewById(R.id.incLCredMCred3),true);
//		guiTools.scale(findViewById(R.id.incLCredMText5),true);
		
		guiTools.scale(findViewById(R.id.incLCredLText),true);
		
		guiTools.scale(findViewById(R.id.incLCredBottomLayout));
		guiTools.scale(findViewById(R.id.incLCredBottomMenuRC),true);
		guiTools.scale(findViewById(R.id.incLCredBottomAdquirir),true);
		
		guiTools.scale(findViewById(R.id.incLCredSeekbarText),true);
		guiTools.scale(findViewById(R.id.incLCredSeekbarText2),true);
		guiTools.scale(findViewById(R.id.incLCredSeekBar));
		guiTools.scale(findViewById(R.id.incLCredSeekbarLayout));
		guiTools.scale(findViewById(R.id.incLCredSeekbarPlus));
		guiTools.scale(findViewById(R.id.incLCredSeekbarMin));
		
		guiTools.scale(findViewById(R.id.incLCredBtnSimular));
		guiTools.scale(findViewById(R.id.incLCredResMensaje),true);
		guiTools.scale(findViewById(R.id.incLCredResul),true);
		guiTools.scale(findViewById(R.id.incLCredCondiciones),true);
		
		
		
	}

	@Override
	public void onClick(View v) {
		isOnForeground = false;
		if(v.getId() == R.id.headerRefresh){		
			delegate.redirectToView(MenuPrincipalActivity.class);
		}else if(v.getId() == R.id.incLCredBtnSimular)
		{
		//	delegate.doRecalculoContract(this);
Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
			
			click_paso2_operacion.put("evento_realizada","event52");
			click_paso2_operacion.put("&&products","simulador;simulador:simulador incremento linea credito");
			click_paso2_operacion.put("eVar12","simulacion realizada");
			TrackingHelper.trackSimulacionRealizada(click_paso2_operacion);
			deleteSimulation();
			
		}else if(v.getId() == R.id.incLCredBottomMenuRC){
			delegate.redirectToView(ResumenActivity.class);
		}else if(v.getId() == R.id.incLCredBottomAdquirir)
		{
			//muestra opcion adquirir
			Log.e("Selecciono adquirir", "....");
			isOnForeground=false; 
			//oneClickILC();
Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
			click_paso2_operacion.put("evento_paso1","event45");
			click_paso2_operacion.put("&&products","simulador;simulador:simulador incremento linea credito");
			click_paso2_operacion.put("eVar12","seleccion adquirir");
			TrackingHelper.trackPaso1Operacion(click_paso2_operacion);
			saveSimulation();
		}else if(v.getId() == R.id.incLCredSeekbarPlus){
			seekbar = (SeekBar)findViewById(R.id.incLCredSeekBar);
			if(seekbar.getProgress() < seekbar.getMax()){
				Integer val = seekbar.getMax() - seekbar.getProgress(); 
				if(val>=1000){
					seekbar.setProgress(seekbar.getProgress()+1000);
				}else{
					seekbar.setProgress(seekbar.getProgress()+val);
				}
				if(seekbar.getProgress()<=delegate.getMontoMax()){ 
					//delegate.doRecalculoContract(this);
					}
			}
		}else if(v.getId() == R.id.incLCredSeekbarMin){
			seekbar = (SeekBar)findViewById(R.id.incLCredSeekBar);
			if(seekbar.getProgress() > delegate.getMontoMin()){
				if((seekbar.getProgress() >= 1000)&&(delegate.getMontoMin() <= (seekbar.getProgress()-1000))){
					seekbar.setProgress(seekbar.getProgress()-1000);
				}else{
					seekbar.setProgress(delegate.getMontoMin());
				}
				if(seekbar.getProgress()>=delegate.getMontoMin()){ 
					//delegate.doRecalculoContract(this);
					}
			}
		}
		
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		TextView text = (TextView)findViewById(R.id.incLCredSeekbarText2);
		text.setText(GuiTools.getMoneyString(String.valueOf(seekbar.getProgress())));
		
		ocultaInfoNC(); 
		
		if(seekbar.getProgress() == delegate.getMontoMax()){
			seekbar.setProgress(seekbar.getProgress());
		}else{
			seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
		}
		
//		if(seekbar.getProgress()>=delegate.getMontoMin()){ 
//			delegate.doRecalculoContract(this);
//		}
	}

	//public void pintarInfoTabla(String saldoTDC){
	public void pintarInfoTabla(){
		
		//Quitar bloqueo
		MainController.getInstance().ocultaIndicadorActividad();
		
		ocultaInfoNC();	
		
		Producto p = delegate.getProducto();
		TextView resultadoPago = (TextView)findViewById(R.id.incLCredResul);
		
		
		//resultadoPago.setText(GuiTools.getMoneyString(String.valueOf(seekbar.getProgress()+saldo)));				
		resultadoPago.setText(GuiTools.getMoneyString(String.valueOf(p.getMontoDeseS())));		
		
		muestraInfoNC();
		
//		TextView textSaldo = (TextView)findViewById(R.id.incLCredMCred1);
//		TextView textLimiteCred = (TextView)findViewById(R.id.incLCredMCred2);
//		TextView textLineaCredMax = (TextView)findViewById(R.id.incLCredMCred3);
	
/*		
		if (saldoTDC != null){
			textSaldo.setText(GuiTools.getMoneyString(String.valueOf(saldoTDC)));	
		}

		// Limite de credito actual
		textLimiteCred.setText(GuiTools.getMoneyString(String.valueOf(p.getMontoMaxS()))); 
*/		
		//TODO Pendiente de duda
		//textLimiteCred.setText(GuiTools.getMoneyString(String.valueOf(p.getMontoMaxS())));
		//textLineaCredMax.setText(GuiTools.getMoneyString(String.valueOf(p.getSubproducto().get(0).getMonMax())));

		
	}
	
	private void ocultaInfoNC(){
		/*
		TextView text1 = (TextView)findViewById(R.id.incLCredMText1);
		text1.setVisibility(View.GONE);

		TextView text2 = (TextView)findViewById(R.id.incLCredMCred1);
		text2.setVisibility(View.GONE);

		TextView text3 = (TextView)findViewById(R.id.incLCredMText2);
		text3.setVisibility(View.GONE);

		TextView text4 = (TextView)findViewById(R.id.incLCredMCred2);
		text4.setVisibility(View.GONE);
        
		TextView text5 = (TextView)findViewById(R.id.incLCredMText3);
		text5.setVisibility(View.GONE);
		
		TextView cred3 = (TextView)findViewById(R.id.incLCredMCred3);
		cred3.setVisibility(View.GONE);
		
		TextView text6 = (TextView)findViewById(R.id.incLCredMText4);
		text6.setVisibility(View.GONE);
		
		TextView text7 = (TextView)findViewById(R.id.incLCredMText5);
		text7.setVisibility(View.GONE);
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.incLCredLinearTextLayout);
		tlayout.setVisibility(View.GONE);
        */
		TextView textMensaje = (TextView)findViewById(R.id.incLCredResMensaje);
		textMensaje.setVisibility(View.GONE);
		
		TextView textResult = (TextView)findViewById(R.id.incLCredResul);
		textResult.setVisibility(View.GONE);
		
		TextView textCondiciones = (TextView)findViewById(R.id.incLCredCondiciones);
		textCondiciones.setVisibility(View.GONE);
		
		btnAdquirir = (Button)findViewById(R.id.incLCredBottomAdquirir);
		btnAdquirir.setVisibility(View.GONE);
		
	}
	
	private void muestraInfoNC(){
		/*
		TextView text1 = (TextView)findViewById(R.id.incLCredMText1);
		text1.setVisibility(View.VISIBLE);

		TextView text2 = (TextView)findViewById(R.id.incLCredMCred1);
		text2.setVisibility(View.VISIBLE);

		TextView text3 = (TextView)findViewById(R.id.incLCredMText2);
		text3.setVisibility(View.VISIBLE);

		TextView text4 = (TextView)findViewById(R.id.incLCredMCred2);
		text4.setVisibility(View.VISIBLE);

//		TextView text5 = (TextView)findViewById(R.id.incLCredMText3);
//		text5.setVisibility(View.VISIBLE);

//		LinearLayout tlayout = (LinearLayout)findViewById(R.id.incLCredLinearTextLayout);
//		tlayout.setVisibility(View.VISIBLE);

 */
		TextView textMensaje = (TextView)findViewById(R.id.incLCredResMensaje);
		textMensaje.setVisibility(View.VISIBLE);
		
		TextView textResult = (TextView)findViewById(R.id.incLCredResul);
		textResult.setVisibility(View.VISIBLE);
		
		TextView textCondiciones = (TextView)findViewById(R.id.incLCredCondiciones);
		textCondiciones.setVisibility(View.VISIBLE);
		
		btnAdquirir = (Button)findViewById(R.id.incLCredBottomAdquirir);
		btnAdquirir.setVisibility(View.VISIBLE);
	}
	
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
//		seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
//		if(seekbar.getProgress()<delegate.getMontoMin()){
//			seekbar.setProgress(delegate.getMontoMin());
//		}
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
//		seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
//		if(seekbar.getProgress()<delegate.getMontoMin()){
//			seekbar.setProgress(delegate.getMontoMin());
//		}
		if(seekbar.getProgress()>=delegate.getMontoMin()){ 
		//delegate.doRecalculoContract(this);
		}else{
			seekbar.setProgress(delegate.getMontoMin());
			//delegate.doRecalculoContract(this);
		}

	}
	
	/**
	 * Created June 8th, 2015,
	 */
	
	private void saveSimulation()
	{
		delegate.saveOrDeleteSimulationRequest(this,true);
	}
	
	
	
	private void deleteSimulation()
	{
		delegate.saveOrDeleteSimulationRequest(this,false);
	}
	
	
	public void doRecalculo()
	{
		delegate.doRecalculoContract(this);
	}
	
	
	public void oneClickILC()
	{
		
		//OK
		//Promociones promocion = new Promociones();
		//OfertaILC oferta = new OfertaILC();
		Producto producto = delegate.getProducto();
	
		//oferta.setCat(producto.getCATS());
		//oferta.setFechaCat(producto.getFechaCatS());
		
		//Bmovil Session variables to be used to determine which flow the "one click flow" is going to take.
		suitebancomer.aplicaciones.bmovil.classes.common.Session.getInstance(MainController.getInstance().getContext()).setOfertaDelSimulador(true);
		suitebancomer.aplicaciones.bmovil.classes.common.Session.getInstance(MainController.getInstance().getContext()).setCveCamp(producto.getCveProd());
		
		Session session = Session.getInstance(MainController.getInstance().getContext());
		
		DetalleDeAlternativaDelegate delegate = new DetalleDeAlternativaDelegate(session.getIdUsuario(), session.getIum(), session.getNumCelular(), producto.getCveProd());
		//delegate.setOferta(oferta);
		//delegate.setPromocion(promocion);
		delegate.consultaDetalleAlternativasTask();
	}
	
	@Override    
    public boolean onKeyDown(int keyCode, KeyEvent event) { 

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			isOnForeground = false;
    			delegate.redirectToView(MenuPrincipalActivity.class);
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}		
    }
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if(!isScreenOn() || (!this.isFinishing()&&isOnForeground))
		{
			SuiteApp.getInstance().closeBmovilAppSession();
		}
	}
}

