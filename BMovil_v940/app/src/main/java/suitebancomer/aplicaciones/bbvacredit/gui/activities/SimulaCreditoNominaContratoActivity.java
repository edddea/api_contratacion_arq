package suitebancomer.aplicaciones.bbvacredit.gui.activities;

import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bbvacredit.common.GuiTools;
import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.DetalleDeAlternativaDelegate;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.SimulaCreditoNominaDelegate;
import suitebancomer.aplicaciones.bbvacredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilConstants;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import tracking.TrackingHelper;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class SimulaCreditoNominaContratoActivity extends BaseActivity  implements OnClickListener, OnSeekBarChangeListener{
	
	private SimulaCreditoNominaDelegate delegate;
	private ImageButton refreshBtn;
	private ImageButton btnSimular;
	
	// Menu Button
	private Button resumenBtn;
	private Button btnAdquirir;
	
	// Botones Seekbar
	private Button seekPlus;
	private Button seekMinus;
	private SeekBar seekbar;
	private Spinner spinner;

	private boolean isOcultaInfoEnabled;
	private BmovilViewsController parentManager;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.activity_simula_credito_nomina_contrato);
		isOnForeground = true;
		isOcultaInfoEnabled=false;
		MainController.getInstance().setCurrentActivity(this);
		Log.e("Cr�dito de n�mina","cargado");
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("creditsimNominaCont", parentManager.estados);
	
		init();
	}
	
	private void init(){
		Tools.setIsContratacionPreference(true);
		ocultaInfoNC();
		Tools.getCurrentSession().setNomCAct(this);
		delegate = new SimulaCreditoNominaDelegate();
		inicializaBotones();
		mapearBotones();
		
		if((delegate.getProducto()!=null) && delegate.getProducto().getIndSimBoolean()) this.pintarInfoTabla();

		scaleToCurrentScreen();
		setEventToSpinner();
		//Tools.getCurrentSession().setVisibilityButton(this, R.id.simCNomCBottomMenuRC);
	}
	
	private void scaleToCurrentScreen(){
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.simCNomCBtnSimular));
		guiTools.scale(findViewById(R.id.simCNomCLayout));
		guiTools.scale(findViewById(R.id.simCNomCBodyLayout));
		guiTools.scale(findViewById(R.id.simCNomCTitle),true);
		guiTools.scale(findViewById(R.id.simCNomCLayoutSubTitle));
		guiTools.scale(findViewById(R.id.simCNomCSubTitleTCImg));
		guiTools.scale(findViewById(R.id.simCNomCSubTitleMiddleTxt),true);
		guiTools.scale(findViewById(R.id.simCNomCLinearLayout));
		guiTools.scale(findViewById(R.id.simCNomCLText),true);
		guiTools.scale(findViewById(R.id.simCNomCLMon),true);
		guiTools.scale(findViewById(R.id.simCNomCMTextSL),true);
		guiTools.scale(findViewById(R.id.simCNomCLineaSeparacion));
		guiTools.scale(findViewById(R.id.simCNomCPlazo),true);
		guiTools.scale(findViewById(R.id.simCNomCSpinner));
		guiTools.scale(findViewById(R.id.simCNomCBottomLayout));
		guiTools.scale(findViewById(R.id.simCNomCBottomMenuRC),true);
		guiTools.scale(findViewById(R.id.simCNomCBottomAdquirir),true);
		guiTools.scale(findViewById(R.id.simCNomCCatFecha),true);
		guiTools.scale(findViewById(R.id.simCNomCCatText),true);
		guiTools.scale(findViewById(R.id.simCNomCCatPorc),true);
		guiTools.scale(findViewById(R.id.simCNomCCat),true);
		guiTools.scale(findViewById(R.id.simCNomCatInfo));
		guiTools.scale(findViewById(R.id.simCNomClblResultadoTasa),true);
		guiTools.scale(findViewById(R.id.simCNomClblResultadoSimulacion3),true);
		guiTools.scale(findViewById(R.id.simCNomCLinearLayout3));
		guiTools.scale(findViewById(R.id.simCNomClblResultadoPlazo),true);
		guiTools.scale(findViewById(R.id.simCNomClblResultadoSimulacion2),true);
		guiTools.scale(findViewById(R.id.simCNomCLinearLayout2));
		guiTools.scale(findViewById(R.id.simCNomClblResultadoPago),true);
		guiTools.scale(findViewById(R.id.simCNomClblResultadoSimulacion),true);
		guiTools.scale(findViewById(R.id.simCNomCLinearLayoutTexto));
		
		guiTools.scale(findViewById(R.id.simCNomCSeekbarText),true);
		guiTools.scale(findViewById(R.id.simCNomCSeekbarText2),true);
		guiTools.scale(findViewById(R.id.simCNomCSeekBar));
		guiTools.scale(findViewById(R.id.simCNomCSeekbarLayout));
		guiTools.scale(findViewById(R.id.simCNomCSeekbarPlus));
		guiTools.scale(findViewById(R.id.simCNomCSeekbarMin));
		
		
	}
	
	private void inicializaBotones(){
		
		refreshBtn = (ImageButton)findViewById(R.id.headerRefresh);
		btnSimular = (ImageButton)findViewById(R.id.simCNomCBtnSimular);
		resumenBtn = (Button)findViewById(R.id.simCNomCBottomMenuRC);
		btnAdquirir = (Button)findViewById(R.id.simCNomCBottomAdquirir);
		seekPlus = (Button)findViewById(R.id.simCNomCSeekbarMin);
		seekMinus = (Button)findViewById(R.id.simCNomCSeekbarPlus);
		seekbar = (SeekBar)findViewById(R.id.simCNomCSeekBar);
		spinner = (Spinner)findViewById(R.id.simCNomCSpinner);
	}
	
	private void mapearBotones(){
		refreshBtn = (ImageButton)findViewById(R.id.headerRefresh);
		refreshBtn.setOnClickListener(this);
		
		btnSimular = (ImageButton)findViewById(R.id.simCNomCBtnSimular);
		btnSimular.setOnClickListener(this);
		
		resumenBtn = (Button)findViewById(R.id.simCNomCBottomMenuRC);
		resumenBtn.setOnClickListener(this);
		
		btnAdquirir = (Button)findViewById(R.id.simCNomCBottomAdquirir);
		btnAdquirir.setOnClickListener(this);
		
		seekPlus = (Button)findViewById(R.id.simCNomCSeekbarMin);
		seekPlus.setOnClickListener(this);
		
		seekMinus = (Button)findViewById(R.id.simCNomCSeekbarPlus);
		seekMinus.setOnClickListener(this);
		
		seekbar = (SeekBar)findViewById(R.id.simCNomCSeekBar);
		seekbar.setMax(delegate.getMontoMax());
		seekbar.setProgress(delegate.getMontoProgress());
		seekbar.setOnSeekBarChangeListener(this);		
		seekbar.refreshDrawableState();
		
		TextView valueSeekBar =(TextView) findViewById(R.id.simCNomCSeekbarText2);
		valueSeekBar.setText(GuiTools.getMoneyString(delegate.getMontoProgress().toString()));

		TextView disponible = (TextView)findViewById(R.id.simCNomCLMon);
		if(delegate.getProducto() != null){
			disponible.setText(GuiTools.getMoneyString(String.valueOf(delegate.getProducto().getSubproducto().get(0).getMonMax())));
		}else{
			disponible.setText(GuiTools.getMoneyString(String.valueOf(0)));
		}
		
		spinner = (Spinner)findViewById(R.id.simCNomCSpinner);
		ArrayAdapter<String> dataAdapter = 
				new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, delegate.getPlazos());		
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
					
	}
	
	public void mockData(){
		Spinner spinner = (Spinner) findViewById(R.id.simCNomCSpinner);
		// Create an ArrayAdapter using the string array and a default spinner layout
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.plazo_array, android.R.layout.simple_spinner_item);
		// Specify the layout to use when the list of choices appears
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		// Apply the adapter to the spinner
		spinner.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {
		isOnForeground = false;
		if(v.getId() == R.id.headerRefresh){			
			delegate.setRet();
			delegate.redirectToView(MenuPrincipalActivity.class);
		}else if(v.getId() == R.id.simCNomCBtnSimular)
		{
			isOcultaInfoEnabled=false;
		//	delegate.doRecalculoContract(this);
			Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();
			click_paso2_operacion.put("evento_realizada","event52");
			click_paso2_operacion.put("&&products","simulador;simulador:simulador credito nomina");
			click_paso2_operacion.put("eVar12","simulacion realizada");
			TrackingHelper.trackSimulacionRealizada(click_paso2_operacion);
			Log.e("Selecciono Simular", "...");
			deleteSimulation();
		}else if(v.getId() == R.id.simCNomCBottomMenuRC){
			delegate.redirectToView(ResumenActivity.class);
		}else if(v.getId() == R.id.simCNomCBottomAdquirir){
			isOnForeground=false;
			//consumoOneClick();
			//nuevo flujo para adquirir
			Log.e("Selecciono adquirir", "...");
            Map<String,Object> click_paso2_operacion = new HashMap<String, Object>();


			click_paso2_operacion.put("inicio","event45");
			click_paso2_operacion.put("&&products","simulador;simulador:simulador credito nomina");
			click_paso2_operacion.put("eVar12","seleccion adquirir");

			TrackingHelper.trackPaso1Operacion(click_paso2_operacion);
			saveSimulation();
			//this.showAlert("Para contratar acude a tu sucursal más cercana.",this);
		}else if(v.getId() == R.id.simCNomCSeekbarPlus){
			seekbar = (SeekBar)findViewById(R.id.simCNomCSeekBar);
			if(seekbar.getProgress() < seekbar.getMax()){
				Integer val = seekbar.getMax() - seekbar.getProgress(); 
				if(val>=1000){
					seekbar.setProgress(seekbar.getProgress()+1000);
				}else{
					seekbar.setProgress(seekbar.getProgress()+val);
				}
				if(seekbar.getProgress()<=delegate.getMontoMin()){ 
					//delegate.doRecalculoContract(this);
					}
			}
		}else if(v.getId() == R.id.simCNomCSeekbarMin){
			seekbar = (SeekBar)findViewById(R.id.simCNomCSeekBar);
			if(seekbar.getProgress() > delegate.getMontoMin()){
				if((seekbar.getProgress() >= 1000)&&(delegate.getMontoMin() <= (seekbar.getProgress()-1000))){
					seekbar.setProgress(seekbar.getProgress()-1000);
				}else{
					seekbar.setProgress(delegate.getMontoMin());
				}
				if(seekbar.getProgress()>=delegate.getMontoMin()){ 
					//delegate.doRecalculoContract(this);
					}
			}
		}
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
		TextView text = (TextView)findViewById(R.id.simCNomCSeekbarText2);
		text.setText(GuiTools.getMoneyString(String.valueOf(seekbar.getProgress())));
		
		ocultaInfoNC();
		
		if(seekbar.getProgress() == delegate.getMontoMax()){
			seekbar.setProgress(seekbar.getProgress());
		}else{
			seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
		}
//		if(seekbar.getProgress()>=delegate.getMontoMin()){ 
//		delegate.doRecalculoContract(this);
//		}
//		if(seekbar.getProgress()>=delegate.getMontoMin()) {
//			delegate.doRecalculoContract(this);		
//		}
	}

	
	public void pintarInfoTabla(){
		
		//Quitar bloqueo
		MainController.getInstance().ocultaIndicadorActividad();
		
		ocultaInfoNC();		
		
		TextView porcentajeCat = (TextView)findViewById(R.id.simCNomCCatPorc);
		TextView fechaCat = (TextView)findViewById(R.id.simCNomCCatFecha);
		TextView resultadoPago = (TextView)findViewById(R.id.simCNomClblResultadoPago);
		TextView resultadoPlazo = (TextView)findViewById(R.id.simCNomClblResultadoPlazo);
		TextView resultadoTasa = (TextView)findViewById(R.id.simCNomClblResultadoTasa);
		
		Producto p = delegate.getProducto();
		
		if ( (delegate.getProducto()!=null) && delegate.getProducto().getIndSimBoolean() ){
			spinner.setSelection(getPosSelectedSpinner(spinner, p.getDesPlazoE()));
			//seekbar.setProgress(Integer.parseInt(p.getMontoDeseS()));
		}
		String textoPorcCat = p.getCATS(); 
		porcentajeCat.setText(textoPorcCat);
		fechaCat.setText("Fecha de c�lculo al "+p.getFechaCatS().toLowerCase());
		
		resultadoPago.setText(GuiTools.getMoneyString(String.valueOf(p.getPagMProdS())));
		resultadoPlazo.setText(p.getDesPlazoE());
		resultadoTasa.setText(p.getTasaS()+"%");		
		
		muestraInfoNC();
	}

	private void ocultaInfoNC(){
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCNomCLinearLayoutTexto);
		tlayout.setVisibility(View.INVISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.simCNomCatInfo);
		tableLayout.setVisibility(View.INVISIBLE);
		
		btnAdquirir = (Button)findViewById(R.id.simCNomCBottomAdquirir);
		btnAdquirir.setVisibility(View.INVISIBLE);
	}
	
	public void subir()
	{

		LinearLayout p=(LinearLayout)findViewById(R.id.simCNomCBottomLayout);
		ImageButton bn=(ImageButton)findViewById(R.id.simCNomCBtnSimular);
		p.setY(Tools.getPosicionBarraInferiorS(bn));

	}
	public void bajar()
	{
		LinearLayout d=(LinearLayout)findViewById(R.id.simCNomCBodyLayout);
		LinearLayout p=(LinearLayout)findViewById(R.id.simCNomCBottomLayout);
		p.setY(Tools.getPosicionBarraInferiorB(d));

	}
	private void muestraInfoNC(){
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCNomCLinearLayoutTexto);
		tlayout.setVisibility(View.VISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.simCNomCatInfo);
		tableLayout.setVisibility(View.VISIBLE);
		
		btnAdquirir = (Button)findViewById(R.id.simCNomCBottomAdquirir);
		btnAdquirir.setVisibility(View.VISIBLE);
		if(suitebancomer.aplicaciones.bmovil.classes.common.Session.getInstance(this).getClientProfile() !=  bancomer.api.common.commons.Constants.Perfil.avanzado)
			btnAdquirir.setVisibility(View.INVISIBLE);
		//bajar();
	}
	
	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
//		seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
//		if(seekbar.getProgress()<delegate.getMontoMin()){
//			seekbar.setProgress(delegate.getMontoMin());
//		}
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
//		seekbar.setProgress(GuiTools.getAmountThFormatted(seekbar.getProgress()));
//		if(seekbar.getProgress()<delegate.getMontoMin()){
//			seekbar.setProgress(delegate.getMontoMin());
//		}
		if(seekbar.getProgress()>=delegate.getMontoMin()){ 
		//delegate.doRecalculoContract(this);
		}else{
			seekbar.setProgress(delegate.getMontoMin());
			//delegate.doRecalculoContract(this);
		}
	}

	public SeekBar getSeekbar() {
		return seekbar;
	}

	public void setSeekbar(SeekBar seekbar) {
		this.seekbar = seekbar;
	}

	public Spinner getSpinner() {
		return spinner;
	}

	public void setSpinner(Spinner spinner) {
		this.spinner = spinner;
	}
	
	
	@Override    
    public boolean onKeyDown(int keyCode, KeyEvent event) { 

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			isOnForeground = false;
    			delegate.redirectToView(MenuPrincipalActivity.class);
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}		
    }
	
	
	
	public void consumoOneClick()
	{		
		
		Log.d("Salv� Simulaci�n","ok");
		
		//OK
		/** Modified: April 22th, 2015. Author:OOS.*/
		/** Modificacion 50986.*/

		//Promociones promocion = new Promociones();
		Producto producto = delegate.getProducto();
		
		//Bmovil Session variables to be used to determine which flow the "one click flow" is going to take.
		suitebancomer.aplicaciones.bmovil.classes.common.Session.getInstance(MainController.getInstance().getContext()).setOfertaDelSimulador(true);
		suitebancomer.aplicaciones.bmovil.classes.common.Session.getInstance(MainController.getInstance().getContext()).setCveCamp(producto.getCveProd());
				
		
		//OfertaConsumo oferta = new OfertaConsumo();
		
		//oferta.setImporte(Tools.formatterForBmovil(producto.getMontoDeseS()));
		//oferta.setPlazoDes(producto.getDesPlazoE());
		
		//String bscPagar = new String(producto.getDessubpE());
		//oferta.setTipoSeg(bscPagar.substring(0, 16));
		//oferta.setTotalPagos(Tools.getTotalPagos(producto.getDesPlazoE()));
		
		Session session = Session.getInstance();
		
		DetalleDeAlternativaDelegate delegate = new DetalleDeAlternativaDelegate(session.getIdUsuario(), session.getIum(), session.getNumCelular(), producto.getCveProd());
		//delegate.setOfertaConsumo(oferta);
		//delegate.setPromocion(promocion);
		delegate.consultaDetalleAlternativasTask();
		
		
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if(!isScreenOn() || (!this.isFinishing()&&isOnForeground))
		{
			SuiteApp.getInstance().closeBmovilAppSession();
		}
	}
	
	
	private void setEventToSpinner()
	{
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
		    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) { 
		    	if(isOcultaInfoEnabled)
		    		ocultaInfoNC();
		    	//subir();
		    	isOcultaInfoEnabled=true;
		    } 

		    public void onNothingSelected(AdapterView<?> adapterView) {
		        return;
		    } 
		}); 
	}
	
	
	/**
	 * Created June 8th, 2015,
	 */
	
	private void saveSimulation()
	{
		delegate.saveOrDeleteSimulationRequest(this,true);
	}
	private void deleteSimulation()
	{
		delegate.saveOrDeleteSimulationRequest(this,false);
	}
	
	public void doRecalculo()
	{
		Log.d("Elimin� Simulaci�n","ok");
		delegate.doRecalculoContract(this);
	}
	public void showAlert(String texto, SimulaCreditoNominaContratoActivity act){
		final SimulaCreditoNominaContratoActivity activity = act;
		AlertDialog.Builder alert = new AlertDialog.Builder(this);
		alert.setMessage(texto);

		
		alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int whichButton) {           
	        } 
		});
     
     AlertDialog dialog = alert.show();
     
     TextView messageView = (TextView)dialog.findViewById(android.R.id.message);
     messageView.setGravity(Gravity.CENTER);
	}
	
}
