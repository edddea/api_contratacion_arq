package suitebancomer.aplicaciones.bbvacredit.models;

public class ConvenioPagoServicios {
	
	private String convenioCIE;
	
	private String nombreLogo;
	
	private String nombreServicio;
	
	private String orden;

	public ConvenioPagoServicios(String convenioCIE, String nombreLogo,
			String nombreServicio, String orden) {
		super();
		this.convenioCIE = convenioCIE;
		this.nombreLogo = nombreLogo;
		this.nombreServicio = nombreServicio;
		this.orden = orden;
	}

	public ConvenioPagoServicios() {
		// TODO Auto-generated constructor stub
	}

	public String getConvenioCIE() {
		return convenioCIE;
	}

	public void setConvenioCIE(String convenioCIE) {
		this.convenioCIE = convenioCIE;
	}

	public String getNombreLogo() {
		return nombreLogo;
	}

	public void setNombreLogo(String nombreLogo) {
		this.nombreLogo = nombreLogo;
	}

	public String getNombreServicio() {
		return nombreServicio;
	}

	public void setNombreServicio(String nombreServicio) {
		this.nombreServicio = nombreServicio;
	}

	public String getOrden() {
		return orden;
	}

	public void setOrden(String orden) {
		this.orden = orden;
	}
	
}
