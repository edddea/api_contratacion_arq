package suitebancomer.aplicaciones.bbvacredit.gui.delegates;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.activities.SimulaCreditoAutoActivity;
import suitebancomer.aplicaciones.bbvacredit.gui.activities.SimulaCreditoAutoContratoActivity;
import suitebancomer.aplicaciones.bbvacredit.io.AuxConectionFactoryCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerResponseCredit;
import suitebancomer.aplicaciones.bbvacredit.models.CalculoData;
import suitebancomer.aplicaciones.bbvacredit.models.CreditoContratado;
import suitebancomer.aplicaciones.bbvacredit.models.ObjetoCreditos;
import suitebancomer.aplicaciones.bbvacredit.models.Plazo;
import suitebancomer.aplicaciones.bbvacredit.models.Producto;
import suitebancomer.aplicaciones.bbvacredit.models.Subproducto;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import android.content.SharedPreferences;
import android.util.Log;

import com.bancomer.mbanking.R;

public class SimulaCreditoAutoDelegate extends BaseDelegateOperacion{
	
	private Session session;
	
	private Boolean ocultaInfo = true;
	
	// Indice de producto dentro del array
	private Integer productIndex = 0;

	private Producto producto = null;
	
	private ArrayList<Producto> productTable = new ArrayList<Producto>();

	private Boolean ret = false;
	
	// Activity para hacer los cambios en vistas en return de server
	private SimulaCreditoAutoActivity act;
	
	private SimulaCreditoAutoContratoActivity actContrato;
	
	private ObjetoCreditos data;

	private Integer montoMin = 0;
	
	private Integer montoMax = 0;
	
	private Integer montoProgress = 0;
	
	private List<String> estados;

	private List<String> plazos;
	
	private boolean isLiquidacion = false;
	
	public Integer getMontoMin() {
		return montoMin;
	}

	public Integer getMontoProgress() {
		return montoProgress;
	}

	public void setMontoProgress(Integer montoProgress) {
		this.montoProgress = montoProgress;
	}

	public void setMontoMin(Integer montoMin) {
		this.montoMin = montoMin;
	}

	public Integer getMontoMax() {
		return montoMax;
	}

	public void setMontoMax(Integer montoMax) {
		this.montoMax = montoMax;
	}

	public void setRet(){
		session.setRegresar(ret);
	}
	
	public ArrayList<Producto> getProductTable() {
		return productTable;
	}

	public void setProductTable(ArrayList<Producto> productTable) {
		this.productTable = productTable;
	}

	public Boolean getOcultaInfo() {
		return ocultaInfo;
	}

	public void setOcultaInfo(Boolean ocultaInfo) {
		this.ocultaInfo = ocultaInfo;
	}
	
	public Integer getSaldo(){
		return data.getCreditosContratados().get(productIndex).getSaldo();
	}
	
	private void setProducto(){
		SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
		productIndex = sp.getInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX, 0);
		
		producto = data.getProductos().get(productIndex);
	}
	
	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public List<String> getEstados() {
		return estados;
	}

	public void setEstados(List<String> estados) {
		this.estados = estados;
	}

	public List<String> getPlazos() {
		return plazos;
	}

	public void setPlazos(List<String> plazos) {
		this.plazos = plazos;
	}

	public SimulaCreditoAutoDelegate() {
		session = Tools.getCurrentSession();
		actContrato = session.getAutoCAct();
		// Datos de sesion
		data = session.getCreditos();
		// Bloqueamos la pantalla
		MainController.getInstance().muestraIndicadorActividad("operacion", "conectando");
		
		if(Tools.getIsContratacionPreference()){
			plazos = new ArrayList<String>();
			estados = new ArrayList<String>();

			setProducto();
			if(producto != null){
				
				if(producto.getIndSimBoolean()){
					productTable = data.getProductos();
					ocultaInfo = true;
					montoProgress = Double.valueOf(producto.getMontoDeseS()).intValue();
				}else{
					ocultaInfo = false;
					montoProgress = Double.valueOf(producto.getSubproducto().get(0).getMonMax()).intValue();
				}
				
				// Establecer max y min monto
				montoMin = Double.valueOf(producto.getSubproducto().get(0).getMonMin()).intValue();
				montoMax = Double.valueOf(producto.getSubproducto().get(0).getMonMax()).intValue();	
				
				//llena los listados de estado de veh�culo y plazos
				
				Set<String> plazosProd = new HashSet<String>();
				Set<String> estadosProd = new HashSet<String>();
				
				for (Subproducto subP : producto.getSubproducto()) {
					//for (Plazo plazo : subP.getPlazo()) {
					//	plazosProd.add(plazo.getDesPlazo());
					//}
					estadosProd.add(subP.getDesSubp());
				}
			/*	if (!plazosProd.isEmpty()){
					plazos.addAll(plazosProd);
					java.util.Collections.sort(plazos);
				}*/
				cargarPlazosOnCode(producto.getSubproducto().get(0).getDesSubp());
				
				if (!estadosProd.isEmpty()){
					estados.addAll(estadosProd);
					java.util.Collections.sort(estados);
				}
			}			

			// desbloqueamos
			MainController.getInstance().ocultaIndicadorActividad();
		}else{

			SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
			productIndex = sp.getInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX_LIQ, 0);
			// Ocultamos la info
			CreditoContratado crr = data.getCreditosContratados().get(productIndex);
			if(crr.getIndicadorSim()){
				ocultaInfo = false;
				productTable = data.getProductos();
			}else{
				ocultaInfo = true;
			}
			
			// desbloqueamos
			MainController.getInstance().ocultaIndicadorActividad();
		}
	}
	
	private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}else{
			paramTable.put(cnt, "");
			Log.d(param.getClass().getName(), param.toString()+" empty or null");
		}
	}
	
	private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}
	}

	public void doRecalculoContract(SimulaCreditoAutoContratoActivity actContrato){
		// Mapeamos el activity
		this.actContrato = actContrato;
		
		// Mapeamos el usuario y la contrase�a
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		
		// Mapeamos el codigo de operacion
		addParametroObligatorio(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);
		
		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
		
		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
		
		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
		
		// Mapeamos el tipo de operacion -> 2
		addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);

		// Tomamos el credito contratado y el subproducto asociado
		Producto ccr = data.getProductos().get(productIndex);

		String plazoSel = actContrato.getSpinnerPlazo().getSelectedItem().toString();
		int montoSel = actContrato.getSeekbar().getProgress();
		String estadoSel = actContrato.getSpinnerEstado().getSelectedItem().toString();
		
		String cvePlazoSel = getCvPlazoByValue(ccr, plazoSel);
		String cveEstadoSel = getCvSubPByValue(ccr, estadoSel);

		addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM,paramTable);
		addParametro(cveEstadoSel, ServerConstantsCredit.CVESUBP_PARAM,paramTable);
		addParametro(cvePlazoSel, ServerConstantsCredit.CVEPLAZO_PARAM,paramTable);
		addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM,paramTable);

		Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.calculo(paramTable);
		this.doNetworkOperation(getCodigoOperacion(), paramTable2, true, new CalculoData(), MainController.getInstance().getContext());

	}
	
	public void doRecalculoNC(SimulaCreditoAutoActivity act){
		// Mapeamos el activity
		this.act = act;
		
		// Mapeamos el usuario y la contrase�a
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		
		// Mapeamos el codigo de operacion
		addParametroObligatorio(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);
		
		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
		
		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
		
		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
		
		// Mapeamos el tipo de operacion -> 2
		addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);

		// Tomamos el credito contratado y el subproducto asociado
		CreditoContratado ccr = data.getCreditosContratados().get(productIndex);

		addParametro(ccr.getContrato(), ServerConstantsCredit.CONTRATO_PARAM,paramTable);

		addParametro(ccr.getPagoMen(), ServerConstantsCredit.PAGO_MENSUAL_PARAM,paramTable);

		Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.calculo(paramTable);
		this.doNetworkOperation(getCodigoOperacion(), paramTable2, true, new CalculoData(), MainController.getInstance().getContext());
	}
	
	/****/
	private void setSimLooking4Cve(String cve){
		Iterator<Producto> it = data.getProductos().iterator();
		
		while(it.hasNext()){
			Producto pr = it.next();
			if(pr.getCveProd().equals(cve)) pr.setIndSimBoolean(true);
		}
	}
	
	private void setContSimLooking4Cve(String cve){
		Iterator<CreditoContratado> it = data.getCreditosContratados().iterator();
		
		while(it.hasNext()){
			CreditoContratado pr = it.next();
			if(pr.getCveProd().equals(cve)) pr.setIndicadorSim(true);
		}
	}
	
	public void analyzeResponse(String operationId, ServerResponseCredit response) {
		if(getCodigoOperacion().equals(operationId)){
    		if(response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL){
    			if(Tools.getIsContratacionPreference()){
    				// Llenar creditos
    				CalculoData respuesta = (CalculoData) response.getResponse();
    				ret = true;
    				
    				// Informamos el obj creditos con la informaci�n necesaria
    				data.setCreditos(respuesta.getCreditos());
    				data.setEstado(respuesta.getEstado());
    				data.setMontotSol(respuesta.getMontotSol());
    				data.setPagMTot(respuesta.getPagMTot());
    				data.setPorcTotal(respuesta.getPorcTotal());
    				data.setProductos(respuesta.getProductos());
    				
    				// Setear indsim
    				productIndex = session.getProductoIndexByCve(ConstantsCredit.CREDITO_AUTO);
    				producto = data.getProductos().get(productIndex);
    				
    				if(data.getProductos().get(productIndex).getCveProd().equals(ConstantsCredit.CREDITO_AUTO)){

        				data.getProductos().get(productIndex).setIndSimBoolean(true);
    				}else{
    					setSimLooking4Cve(ConstantsCredit.CREDITO_AUTO);
    				}
    				
    				Tools.getCurrentSession().setVisibilityButton(actContrato, R.id.simCAutoCBottomMenuRC);
    				actContrato.pintarInfoTabla();		     				
    			}else{
	    			// Llenar creditos
	    			CalculoData respuesta = (CalculoData) response.getResponse();
	    			ret = true;
	    			
	    			// Informamos el obj creditos con la informaci�n necesaria
	    			data.setCreditos(respuesta.getCreditos());
	    			data.setEstado(respuesta.getEstado());
	    			data.setMontotSol(respuesta.getMontotSol());
	    			data.setPagMTot(respuesta.getPagMTot());
	    			data.setPorcTotal(respuesta.getPorcTotal());
	    			data.setProductos(respuesta.getProductos());
	    			
	    			// Setear indsim
	    			
	    			productTable = data.getProductos();
	    			
	    			if(data.getCreditosContratados().get(productIndex).getCveProd().equals(ConstantsCredit.CREDITO_AUTO)){

		    			data.getCreditosContratados().get(productIndex).setIndicadorSim(true);
	    			}else{
    					setContSimLooking4Cve(ConstantsCredit.CREDITO_AUTO);
    				}
	    			
    				Tools.getCurrentSession().setVisibilityButton(act, R.id.simCAutoBottomMenuRC);
	    			act.pintarInfoTabla();
    			}
    		}
    	}else if(getCodigoOperacionGuardarEliminar().equals(operationId))
    	{
    		if(response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL)
    		{
    			if(isLiquidacion){
    			act.doRecalculo();	
    			}else
    			{
    				actContrato.doRecalculo();
    			}
    			
    		}
    	}
	}

	public <T> void redirectToView(Class<T> c){
		MainController.getInstance().showScreen(c);
	}

	@Override
	protected String getCodigoOperacion() {
		return Server.CALCULO_OPERACION;
	}
	
	//Nuevo
	public void cargarPlazosOnCode(String code){
		Set<String> plazosProd = new HashSet<String>();
		
		for (Subproducto subP : producto.getSubproducto()) {
			if(subP.getDesSubp().equalsIgnoreCase(code)){
				for (Plazo plazo : subP.getPlazo()) {
					plazosProd.add(plazo.getDesPlazo());
				}
			}
		}
		
		if (!plazosProd.isEmpty()){
			plazos = new ArrayList<String>();
			plazos.addAll(plazosProd);
			java.util.Collections.sort(plazos);
		}else{
			Log.i("Carga de plazos", "No se ha encontrado subproducto");
		}
	}
	
	/**
	 * Created June 8th,2015,
	 */
	
	
	public void deleteSimulationRequest(SimulaCreditoAutoContratoActivity actContrato)
	{
		// Mapeamos el activity
				this.actContrato = actContrato;
				// Mapeamos el usuario y la contrase�a
				Hashtable<String, String> paramTable = new Hashtable<String, String>();
				
				// Mapeamos el codigo de operacion
				addParametroObligatorio(this.getCodigoOperacionGuardarEliminar(),ServerConstantsCredit.OPERACION_PARAM, paramTable);
				
				// Mapeamos el id de cliente tomado de la sesion
				addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
				
				// Mapeamos el IUM tomado de la sesion
				addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
				
				// Mapeamos el numeroCelular tomado de la sesion
				addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
				
				
				// Mapeamos el tipo de operacion -> 4
				addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);

		Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
		this.doNetworkOperation(getCodigoOperacionGuardarEliminar(), paramTable2, true, new CalculoData(), MainController.getInstance().getContext());
	}
	

	protected String getCodigoOperacionGuardarEliminar()
	{
		return Server.CALCULO_ALTERNATIVAS_OPERACION;
	}
	
	public void deleteSimulationRequest(SimulaCreditoAutoActivity actContrato,boolean flag)
	{
		// Mapeamos el activity
				this.act = actContrato;
				this.isLiquidacion = flag;
				// Mapeamos el usuario y la contrase�a
				Hashtable<String, String> paramTable = new Hashtable<String, String>();
				
				// Mapeamos el codigo de operacion
				addParametroObligatorio(this.getCodigoOperacionGuardarEliminar(),ServerConstantsCredit.OPERACION_PARAM, paramTable);
				
				// Mapeamos el id de cliente tomado de la sesion
				addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
				
				// Mapeamos el IUM tomado de la sesion
				addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
				
				// Mapeamos el numeroCelular tomado de la sesion
				addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
				
				
				// Mapeamos el tipo de operacion -> 4
				addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);

		Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
		this.doNetworkOperation(getCodigoOperacionGuardarEliminar(), paramTable2, true, new CalculoData(), MainController.getInstance().getContext());
	}
	
}
