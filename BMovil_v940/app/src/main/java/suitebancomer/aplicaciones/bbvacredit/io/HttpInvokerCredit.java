package suitebancomer.aplicaciones.bbvacredit.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Hashtable;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.me.JSONTools;

import android.util.Log;
import suitebancomer.aplicaciones.bbvacredit.common.NameValuePair;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class HttpInvokerCredit {

	/*
	 * Read buffer size.
	 */
	public static final int READBUFFER_SIZE = 2048;

	/**
	 * Flag to indicate HTTP POST usage.
	 */
	public static final boolean USE_HTTP_POST = true;

	/**
	 * Flag to indicate HTTP response dump usage.
	 */
	public static final boolean DUMP_HTTP_RESPONSE = true;

	/**
	 * HTTP connection methods.
	 */
	public static final String POST = "POST";
	public static final String GET = "GET";

	/**
	 * HTTP protocols.
	 */
	public static final String HTTP = "HTTP";
	public static final String HTTPS = "HTTPS";

	/**
	 * HTTP answers.
	 */
	public static final int UNDEFINED = 0;
	public static final int HTTP_CONTINUE = 100;
	public static final int HTTP_OK = 200;
	public static final int HTTP_NOT_FOUND = 404;
	public static final int HTTP_UNAUTHORIZED = 401;
	public static final int HTTP_FORBIDDEN = 403;
	public static final int HTTP_BAD_REQUEST = 400;
	public static final int HTTP_INTERNAL_ERROR = 500;
	public static final int HTTP_UNAVAILABLE = 503;

	/**
	 * The current connection.
	 */
	private HttpURLConnection connection = null;

	/**
	 * Cookie from server.
	 */
	private Hashtable<String, String> cookies = new Hashtable<String, String>();

	private static StringBuffer oP = new StringBuffer();

	/**
	 * Default constructor.
	 */


	public HttpInvokerCredit() {
		httpClient = new DefaultHttpClient();
		HttpParams params = new BasicHttpParams();

		// Establece el timeout de la conexion y del socket a 30 segundos
		HttpConnectionParams.setConnectionTimeout(params, 30 * 1000);
		HttpConnectionParams.setSoTimeout(params, 30 * 1000);
		httpClient.setParams(params);
	}


	/**
	 * Simulate a network operation.
	 *
	 * @param response
	 *            the simulated response
	 * @param handler
	 *            instance capable of parsing the result
	 * @throws IOException
	 * @throws ParsingExceptionCredit
	 * @throws JSONException
	 */
	public void simulateOperation(String response, Object handler, Boolean checkJSON)
			throws IOException, ParsingExceptionCredit, JSONException {

		if (JSONTools.isJSONValid(response)) {
			parseJSON(response, (ParsingHandlerJSONCredit) handler);
		} else {
			parse(response, (ParsingHandlerCredit) handler);
		}
	}



	public void simulateOperationTest(String response, Object handler, Boolean checkJSON)
			throws IOException, ParsingExceptionCredit, JSONException {

		parseJSON(response, (ParsingHandlerJSONCredit) handler);

	}

	public void simulateOperation(String response, Object handler)
			throws IOException, ParsingExceptionCredit, JSONException {

		parse(response, (ParsingHandlerCredit) handler);
	}

	/**
	 * Invoke a network operation.
	 * 
	 * @param operation
	 *            operation code
	 * @param parameters
	 *            parameters
	 * @param handler
	 *            instance capable of parsing and storing the result
	 * @throws IOException
	 * @throws ParsingExceptionCredit
	 * @throws JSONException 
	 * @throws URISyntaxException 
	 * @throws ParsingException 
	 */
	public void invokeOperation(String operation, Object parameters,
			ParsingHandlerCredit handler) throws IOException, ParsingExceptionCredit, JSONException, ParsingException, URISyntaxException {

		Log.d("Entra a invoker","Credit");

		invokeOperation(operation, parameters, handler, false);
	}

	/**
	 *  
	 * @return
	 */
	private String getOperationCodeValue(String operation){
		String ret = "";
		if(operation.equals(Server.OPERATION_CODES_CREDIT.get(Server.LOGIN_OPERACION)) 
				|| (operation.equals(Server.OPERATION_CODES_CREDIT.get(Server.CONSULTA_TDC)))){
			ret = ServerConstantsCredit.OPERATION_CODE_VALUE;
		}else{
			ret = ServerConstantsCredit.OPERATION_CODE_VALUE_BCDR;
		}

		return ret;
	}

	/**
	 * Invoke a network operation.
	 * 
	 * @param operation
	 *            operation code
	 * @param parameters
	 *            parameters
	 * @param handler
	 *            instance capable of parsing the result
	 * @param clearCookies
	 *            true if session cookies must be deleted, false if not
	 * @throws IOException
	 * @throws ParsingExceptionCredit
	 * @throws JSONException 
	 * @throws URISyntaxException 
	 * @throws ParsingException 
	 */
	public void invokeOperation(String operation, Object parameters,
			ParsingHandlerCredit handler, boolean clearCookies) throws IOException,
			ParsingExceptionCredit, JSONException, ParsingException, URISyntaxException {
		if (clearCookies) {
			this.cookies.clear();
		}
		if(Server.ALLOW_LOG){
			Log.e("invokeOperation","...");		
			Log.e("operation",operation);
		}

		String operationCode = getOperationCodeValue(operation);
		if(Server.ALLOW_LOG) Log.e("operationCode",operationCode);

		String url = getUrl(operation);
		String params;

		if ( parameters instanceof NameValuePair[] ) {
			params = getURLOperacionBan(operationCode).concat(getParameterString(operation, (NameValuePair[]) parameters));
		} else {
			Log.d("Bloque invokeOperation","Non NAmeValuePair option");
			oP = new StringBuffer("OP").append(operation);
			params = getURLOperacionBan(operationCode).concat(encode(parameters.toString()));
		}
		char separator = ((url.indexOf('?') == -1) ? '?' : '&');
		url = new StringBuffer(url).append(separator).append(params)
				.toString();
		if(Server.ALLOW_LOG) Log.e("url if",url);
		//JQH retrieve(url, method, type, contents, handler);
		retrieve(url, null, null, handler);
	}

	/**
	 * Get the URL to invoke the server.
	 * 
	 * @param operation
	 *            operation code
	 * @return a string with the response
	 */
	public static String getUrl(String operation) {
		return Server.getOperationUrlCredit(operation);
	}

	/**
	 * Get the URL to invoke the server.
	 * 
	 * @param operation
	 *            operation code
	 * @param parameters
	 *            parameters to pass to the server
	 * @return a string with the response
	 */
	public static String getParameterString(String operation,
			NameValuePair[] parameters) {

		StringBuffer sb = new StringBuffer("OP").append(operation);
		oP = new StringBuffer("OP").append(operation);
		System.out.println("Operacion=" + oP);
		if (parameters != null) {
			NameValuePair parameter;
			int size = parameters.length;
			for (int i = 0; i < size; i++) {
				parameter = parameters[i];
				String name = parameter.getName();
				String value = parameter.getValue();
				sb.append(ServerConstantsCredit.PARAMETER_SEPARATOR);
				sb.append(name);
				sb.append(value);
			}
		}

		String params = sb.toString();
		params = encode(params);

		return params;

	}

	private String getURLOperacionBan(String opCodeValue){
		StringBuffer buffer = new StringBuffer();

		buffer.append(ServerConstantsCredit.OPERATION_CODE_PARAMETER);
		buffer.append('=');
		buffer.append(opCodeValue);

		buffer.append('&');

		buffer.append(ServerConstantsCredit.OPERATION_LOCALE_PARAMETER);
		buffer.append('=');
		buffer.append(ServerConstantsCredit.OPERATION_LOCALE_VALUE);

		buffer.append('&');

		buffer.append(ServerConstantsCredit.OPERATION_DATA_PARAMETER);
		buffer.append('=');

		return buffer.toString();
	}

	/**
	 * Abort the operation.
	 */
	public void abort() {
		try {
			close();
		} catch (Throwable t) {
		}
	}

	/**
	 * Read and parse the response.
	 * 
	 * @param response
	 *            the response from the server
	 * @param handler
	 *            instance capable of parsing the result
	 * @throws IOException
	 * @throws ParsingExceptionCredit
	 */
	private static void parse(String response, ParsingHandlerCredit handler)
			throws IOException, ParsingExceptionCredit {
		if (DUMP_HTTP_RESPONSE) {
			// System.out.println("Response contents: " + response);
		}
		Reader reader = null;
		try {
			reader = new StringReader(response);
			ParserCredit parser = new ParserCredit(reader);
			handler.process(parser);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (Throwable ignored) {
				}
			}
		}
	}

	private static void parseJSON(String response, ParsingHandlerJSONCredit handler)
			throws IOException, ParsingExceptionCredit, JSONException {

		Reader reader = null;
		try {
			Log.e("inicializa reader","ok");
			reader = new StringReader(response);
			// ParserJSON parser = new ParserJSON(response);
			Log.e("incializa process Json","ok");
			handler.processJSON(response);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (Throwable ignored) {
				}
			}
		}
	}
	
	/**
	 * Close the http operation.
	 */
	private void close() {
		if (connection != null) {
			connection.disconnect();
			connection = null;
		}
	}

	/**
	 * URL encoding.
	 * 
	 * @param s
	 *            the input string
	 * @return the encodec string
	 */
	public static String encode(String s) {
		String encoded = s;
		if (s != null) {
			try {
				StringBuffer sb = new StringBuffer(s.length() * 3);
				char c;
				int size = s.length();
				for (int i = 0; i < size; i++) {
					c = s.charAt(i);
					if (c == '&') {
						sb.append("%26");
					} else if (c == ' ') {
						sb.append('+');
					} else if ((c >= ',' && c <= ';') || (c >= 'A' && c <= 'Z')
							|| (c >= 'a' && c <= 'z') || c == '_' || c == '?'
							|| c == '*') {
						sb.append(c);
					} else {
						sb.append('%');
						if (c > 15) {
							sb.append(Integer.toHexString((int) c));
						} else {
							sb.append("0" + Integer.toHexString((int) c));
						}
					}
				}
				encoded = sb.toString();
			} catch (Exception ex) {
				encoded = null;
			}
		}
		return (encoded);
	}


	//INICIO JQH
	DefaultHttpClient httpClient;
	org.apache.http.client.CookieStore cookiesCRD;

	public void setCookies(org.apache.http.client.CookieStore cookiesBM) {
		this.cookiesCRD = cookiesBM;
	}

	public void printCookies()
	{
		org.apache.http.client.CookieStore cookieStore = httpClient.getCookieStore();
		List<Cookie> cookies = cookieStore.getCookies();

		for(int i=0;i<cookies.size();i++)
		{
			Log.d("Name= ",cookies.get(i).getName());
			Log.d("Value= ",cookies.get(i).getValue());
			Log.d("Domain= ",cookies.get(i).getDomain());
		}
	}

	private static void read(String response,
			ParsingHandlerCredit handler) throws IOException, ParsingExceptionCredit, JSONException {
		if (JSONTools.isJSONValid(response)) {
			parseJSON(response, (ParsingHandlerJSONCredit) handler);
		} else {
			parse(response, handler);
		}
	}

	private void retrieve(String url, String type, byte[] body,
			ParsingHandlerCredit handler) throws IOException, ParsingException, URISyntaxException, ParsingExceptionCredit, JSONException {

		BufferedReader in = null;
		String result = null;

		if(cookiesCRD!=null){
			//se setean las cookies locales
			httpClient.setCookieStore(cookiesCRD);
		}else {
			//se setean las cookies del api server
			httpClient.setCookieStore(suitebancomer.aplicaciones.commservice.httpcomm.HttpInvoker.getCookieStore());
		}
		try {

			HttpPost request = new HttpPost();

			request.setURI(new URI(url) );
			request.addHeader("Lan", "android");

			HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, "OK");

			printCookies();
			httpClient.getConnectionManager().closeExpiredConnections();

			response = httpClient.execute(request);
			printCookies();

			in = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "ISO-8859-1"));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}

			in.close();
			result = sb.toString(); //respuesta
			Log.d("Credit", result);

		} catch (Exception e) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Ha ocurrido una excepción en la respuesta.", e);
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		if (result.length() > Tools.TAM_FRAGMENTO_TEXTO) {
			Tools.trazaTexto("RESULT", result);
		} else {
			if(Server.ALLOW_LOG) Log.e("RESULT", "result: " + result);
		}

		read(result, handler);

	}
	//FIN JQH




}



