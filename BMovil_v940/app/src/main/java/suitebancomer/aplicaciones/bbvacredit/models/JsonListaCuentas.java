package suitebancomer.aplicaciones.bbvacredit.models;

import java.util.List;

public class JsonListaCuentas {
	
	private String cuentas;
	
	private List<JsonCuenta> lista;

	public JsonListaCuentas(String cuentas, List<JsonCuenta> lista) {
		super();
		this.cuentas = cuentas;
		this.lista = lista;
	}

	public JsonListaCuentas() {
		// TODO Auto-generated constructor stub
	}

	public String getCuentas() {
		return cuentas;
	}

	public void setCuentas(String cuentas) {
		this.cuentas = cuentas;
	}

	public List<JsonCuenta> getLista() {
		return lista;
	}

	public void setLista(List<JsonCuenta> lista) {
		this.lista = lista;
	}
	
}
