package suitebancomer.aplicaciones.bbvacredit.dao;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;


public class DataBase {
	
	private static final int DATABASE_VERSION = 3;

	// The index (key) column name for use in where clauses.
	public static final String KEY_ID="_id";

	// The name and column index of each column in your database.
	public static final String KEY_IDENTIFICADOR=ConstantsCredit.IDENTIFICADOR;
	public static final String KEY_VALOR=ConstantsCredit.VALOR;

	
	

	// TO-DO: Create public field for each column in your table.

	// SQL Statement to create a new table.
	private static final String CREATE_TABLE1 = "create table " + 
			ConstantsCredit.TABLE_USERLOGIN + " (" + KEY_ID + " integer primary key autoincrement, " +
			KEY_IDENTIFICADOR + " text not null, " +
			KEY_VALOR + " text not null);";


	//SQL Statement to delete a content of the table
	private static final String DELETE_TABLE ="delete from ";


	// Variable to hold the database instance
	private SQLiteDatabase db;

	// Context of the application using the database.
	private final Context context;

	/**
	 * Database open/upgrade helper.
	 */
	private myDbHelper dbHelper;


	/**
	 * Opens and connects to the specified socket TCP/IP connection.
	 * @param _context Context of the application using the database.
	 */
	public DataBase(Context _context) {
		context = _context; 
	}

	/**
	 * Open and return a readable/writable instance of the database.
	 * 
	 */
	public DataBase open(){
		dbHelper = new myDbHelper(context, ConstantsCredit.DATABASE_NAME, null,DATABASE_VERSION); 
		try {
			db = dbHelper.getWritableDatabase();
		}
		catch (SQLiteException ex){
			db = dbHelper.getReadableDatabase();
		}

		return this;
	}

	/**
	 * Open and return a readable/writable instance of the database. 
	 * 
	 */      
	public void close() {
		try {
			db.close();
			dbHelper.close();
		} catch (SQLiteException e) {
			Log.i("DataBase.java", "Error close()");
			e.printStackTrace();
		}finally
		{
			db=null;
			dbHelper=null;
		}

	}

	

	

	/**
	 * Delete a table row.
	 * @param _rowIndex
	 * @param table
	 */
	public boolean removeEntry(long _rowIndex,String table) {
		return db.delete(table, KEY_ID + "=" + _rowIndex, null) > 0;
	}

	/**
	 * Get all the records in the table
	 * @param table 
	 */
	public Cursor getAllEntries (String table) {
		return db.query(table, new String[] {KEY_ID, KEY_IDENTIFICADOR, KEY_VALOR},null, null, null, null, null);
	}

	
	/**
	 * Get the number of rows in the table
	 * @param table 
	 */
	public int getNumRecords (String table) {
		Cursor c=null;
		int total=0;
		try{
			c=db.query(table, new String[] {KEY_ID, KEY_IDENTIFICADOR},null, null, null, null, null);
			total=c.getCount();
		}catch (SQLiteException e) {
			Log.i("DataBase,java", "Error getNumRecords()");
		}finally{
			if(c!=null)
				c.close();
		}
		return total;
	}

	/**
	 * Delete the contents of the table
	 * @param table 
	 */
	public void deleteContent(String table) {
		db.execSQL(DELETE_TABLE + table);
	}


	public long insertAllGenericEntries(String table, HashMap<String, Object> mapNameValue) {
		long numUpdatedRows = 0L;
		Iterator<Entry<String, Object>> it = mapNameValue.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
	        if (insertGenericEntry(table, (String)pairs.getKey(), pairs.getValue()) == -1L) {
				return -1L;
			}
	        it.remove();
	    }
		
		return numUpdatedRows;
	}
	
	/**
	 * Actualiza todas las entradas de una tabla.
	 * @param table el nombre de la tabla
	 * @param mapNameValue un mapa con los nombres y los valores de los atributos
	 * @return el numero de filas actualizadas
	 */
	public int updateAllGenericEntries(String table, HashMap<String, Object> mapNameValue) {
		int numUpdatedRows = 0;
		Iterator<Entry<String, Object>> it = mapNameValue.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
	        numUpdatedRows += updateGenericEntry(table, (String)pairs.getKey(), pairs.getValue());
	        it.remove();
	    }
		
		return numUpdatedRows;
	}
	
	/**
	 * Borra todas las entradas de una tabla.
	 * @return el numero de filas borradas
	 */
	public int deleteAllGenericEntries(String table) {		
		return db.delete(table, null, null);
	}
	
	/**
	 * Selecciona un atributo de una tabla concreta.
	 * @param name el nombre del atributo
	 * @return el cursor con el contenido de la fila buscada
	 */
	public Cursor selectGenericEntry(String table, String name) {
		String selection = KEY_IDENTIFICADOR + " = '" + name + "'";
		return db.query(table, null, selection, null, null, null, null);
	}
	
	/**
	 * Inserta una tributo en la tabla concreta.
	 * @param name el nombre del atributo
	 * @param value el valor del atributo
	 * @return el numero de filas insertadas
	 */
	public long insertGenericEntry(String table, String name, Object value) {
		if(null == name || null == value) {
			return -1L;
		}

		ContentValues contentValues = new ContentValues();
		contentValues.put(KEY_IDENTIFICADOR, name);
		
		if (value instanceof String) {
			contentValues.put(KEY_VALOR, (String)value);
		} else if (value instanceof Integer) {
			contentValues.put(KEY_VALOR, (Integer)value);
		}

		return db.insert(table, null, contentValues);
	}
	
	/**
	 * Actualiza un atributo en la tabla concreta.
	 * @param name el nombre del atributo
	 * @param value el valor del atributo
	 * @return el numero de filas actualizadas
	 */
	public int updateGenericEntry(String table, String name, Object value) {
		if(null == name || null == value) {
			return -1;
		}

		String selection = KEY_IDENTIFICADOR + " = '" + name + "'";

		ContentValues contentValues = new ContentValues();
		contentValues.put(KEY_IDENTIFICADOR, name);
		
		if (value instanceof String) {
			contentValues.put(KEY_VALOR, (String)value);
		} else if (value instanceof Integer) {
			contentValues.put(KEY_VALOR, (Integer)value);
		}

		return db.update(table, contentValues, selection, null);
	}
	
	
	
	/**
	 * Delete a row by name
	 */
	public int deleteGenericEntry(String user,String table) {
		if(null == user)
			return -1;

		String selection = KEY_IDENTIFICADOR + " = '" + user +"'";		
		return db.delete(table, selection, null);
	}
	
	
	
	/**
	 * Selecciona una lista de atributos de una tabla concreta.
	 * @param table
	 * @return el cursor con el contenido de las filas buscadas
	 */
	public Cursor selectGenericAllEntry(String table) {
		return db.query(table, null, null, null, null, null, null);
	}
	
	
	/**
	 * 
	 * 
	 * FIN: METODOS GENERICOS PARA ESCRIBIR EN TABLAS
	 * 
	 * 
	 */
	
	/**
	 * Hide the logic used to decide if a database needs to be created or 
	 * upgraded before its opened.
	 */ 
	private static class myDbHelper extends SQLiteOpenHelper {
		/**
		 * Hide the logic used to decide if a database needs to be created or 
		 * upgraded before its opened.
		 * @param context of the application using the database.
		 * @param name of the database.
		 * @param null CursorFactory
		 * @param version database
		 */
		public myDbHelper(Context context, String name, CursorFactory factory, int version) {
			super(context, name, factory, version);
		}

		/** Called when no database exists in 
		 * disk and the helper class needs
		 * to create a new one.
		 * @param _db database.
		 */ 
		@Override
		public void onCreate(SQLiteDatabase _db) {
			_db.execSQL(CREATE_TABLE1);
		}

		/** Called when there is a database version mismatch meaning that 
		 * the version of the database on disk needs to be upgraded to 
		 * the current version.
		 * @param _db database.
		 * @param _oldVersion of the database.
		 * @param _newVersion of the database.
		 */
		@Override
		public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion) {
			// Log the version upgrade.
			Log.w("TaskDataBase", "Upgrading from version " + _oldVersion + " to " + _newVersion);

			// If the old version is not one of the expected, delete all the data.	
				Log.w(this.getClass().getSimpleName(), "No se esperaba esta version de la bd, se eliminar� todo el contenido.");
				_db.execSQL("DROP TABLE IF EXISTS " + ConstantsCredit.TABLE_USERLOGIN);;
				onCreate(_db);  
			
		}

	
	}


}
