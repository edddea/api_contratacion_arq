package suitebancomer.aplicaciones.bbvacredit.models;

import java.util.ArrayList;
import java.util.Iterator;

public class ObjetoCreditos {
	
	private String estado;
	
	private String montotSol;
	
	private Integer porcTotal;
	
	private String pagMTot;
	
	private ArrayList<Producto> productos;

	private ArrayList<Credito> creditos;
	
	private ArrayList<CreditoContratado> creditosContratados;

	public ObjetoCreditos() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ObjetoCreditos(String estado, String montotSol, Integer porcTotal,
			String pagMTot, ArrayList<Producto> productos,
			ArrayList<Credito> creditos,
			ArrayList<CreditoContratado> creditosContratados) {
		super();
		this.estado = estado;
		this.montotSol = montotSol;
		this.porcTotal = porcTotal;
		this.pagMTot = pagMTot;
		this.productos = productos;
		this.creditos = creditos;
		this.creditosContratados = creditosContratados;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getMontotSol() {
		return montotSol;
	}

	public void setMontotSol(String montotSol) {
		this.montotSol = montotSol;
	}

	public Integer getPorcTotal() {
		return porcTotal;
	}

	public void setPorcTotal(Integer porcTotal) {
		this.porcTotal = porcTotal;
	}

	public String getPagMTot() {
		return pagMTot;
	}

	public void setPagMTot(String pagMTot) {
		this.pagMTot = pagMTot;
	}

	public ArrayList<Producto> getProductos() {
		return productos;
	}
	
	private void preprocProducts(ArrayList<Producto> productos){
		if(this.productos != null){
			Iterator<Producto> it = productos.iterator();
			while(it.hasNext()){
				Producto pr = it.next();
				if(this.productos.contains(pr)){
					Boolean sim = this.productos.get(this.productos.indexOf(pr)).getIndSimBoolean();
					pr.setIndSimBoolean(sim);
				}else{
					pr.setIndSimBoolean(false);
				}
			}
		}
	}

	public void setProductos(ArrayList<Producto> productos) {
		preprocProducts(productos);
		this.productos = productos;
	}

	public ArrayList<Credito> getCreditos() {
		return creditos;
	}

	public void setCreditos(ArrayList<Credito> creditos) {
		this.creditos = creditos;
	}

	public ArrayList<CreditoContratado> getCreditosContratados() {
		return creditosContratados;
	}

	public void setCreditosContratados(
			ArrayList<CreditoContratado> creditosContratados) {
		this.creditosContratados = creditosContratados;
	}
}
