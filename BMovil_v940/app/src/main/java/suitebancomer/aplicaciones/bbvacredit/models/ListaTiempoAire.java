package suitebancomer.aplicaciones.bbvacredit.models;

import java.util.List;

public class ListaTiempoAire {
	
	private String versionCatalogo;
	
	private Integer ocurrencia;
	
	private List<TiempoAire> lista;

	public ListaTiempoAire(String versionCatalogo, Integer ocurrencia,
			List<TiempoAire> lista) {
		super();
		this.versionCatalogo = versionCatalogo;
		this.ocurrencia = ocurrencia;
		this.lista = lista;
	}

	public ListaTiempoAire() {
		// TODO Auto-generated constructor stub
	}

	public String getVersionCatalogo() {
		return versionCatalogo;
	}

	public void setVersionCatalogo(String versionCatalogo) {
		this.versionCatalogo = versionCatalogo;
	}

	public Integer getOcurrencia() {
		return ocurrencia;
	}

	public void setOcurrencia(Integer ocurrencia) {
		this.ocurrencia = ocurrencia;
	}

	public List<TiempoAire> getLista() {
		return lista;
	}

	public void setLista(List<TiempoAire> lista) {
		this.lista = lista;
	}
		
}
