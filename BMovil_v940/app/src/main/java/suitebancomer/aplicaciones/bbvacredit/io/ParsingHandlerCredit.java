package suitebancomer.aplicaciones.bbvacredit.io;

import java.io.IOException;

public interface ParsingHandlerCredit {
	/**
     * Process a server response
     * @param parser
     * @throws IOException 
     * @throws ParsingExceptionCredit
     */
    public void process(ParserCredit parser) throws IOException, ParsingExceptionCredit;
}
