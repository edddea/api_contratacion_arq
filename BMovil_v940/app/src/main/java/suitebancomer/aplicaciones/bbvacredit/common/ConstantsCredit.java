package suitebancomer.aplicaciones.bbvacredit.common;

public class ConstantsCredit {
	public static final int SPLASH_VIEW_CONTROLLER_DURATION = 2000;
	
	public static final int LONGITUD_CONTRASENA = 6;
	
	public static final int LONGITUD_USUARIO = 10;
	
	/**
	 * SharedPrefs
	 */
	public static final String SHARED_POSICION_GLOBAL = "PosicionGlobalProductID";
	public static final String SHARED_POSICION_GLOBAL_INDEX = "PosicionGlobalIndex";
	public static final String SHARED_POSICION_GLOBAL_INDEX_LIQ = "PosicionGlobalIndexLiquidacion";
	public static final String SHARED_IS_CONTRATACION = "isContratacion";
	
	/**
	 * The number of visible digits of the user name (the cellular number).
	 */
	public static final int VISIBLE_NUMBER_ACCOUNT = 5;
	
	public static final int VISIBLE_NUMBER_CHARCOUNT = 5;

	public static final String ESPACIO = " ";
	
	public static final String SEPARADOR_MONTOS_COMPANIA = ",";
	

	/**
	 * The application version.
	 */
	public static final String APPLICATION_VERSION = "900";
	
	/**
	 * The other amount value
	 */
	public static final String OTHER_AMOUNT = "Otro";

	/**
      * 
      */
	public static final String STATUS_APP_ACTIVE = "A1";

	/**
      * 
      */
	public static final String STATUS_PASS_BLOCKED = "BI";

	/**
      * 
      */
	public static final String STATUS_USER_CANCELED = "C4";

	/**
      * 
      */
	public static final String STATUS_BANK_CANCELED = "CN";

	/**
      * 
      */
	public static final String STATUS_PENDING_ACTIVATION = "PA";

	/**
      * 
      */
	public static final String STATUS_NIP_BLOCKED = "PB";

	/**
      * 
      */
	public static final String STATUS_PENDING_SEND = "PE";

	/**
      * 
      */
	public static final String STATUS_APP_SUSPENDED = "S4";

	/**
	 * Estatus de ium incorrecto.
	 */
	public static final String STATUS_WRONG_IUM = "II";

	/**
	 * Cliente no existe.
	 */
	public static final String STATUS_CLIENT_NOT_FOUND = "NE";

	/**
	 * Cliente inexistente.
	 */
	public static final String STATUS_CLIENT_NOT_EXISTS = "CNE0007";

	/**
	 * Contrataci�n pendiente.
	 */
	public static final String STATUS_ENGAGEMENT_UNCOMPLETE = "PS";

	/**
      * 
      */
	public static final String PROFILE_BASIC_00 = "MF00";

	/**
      * 
      */
	public static final String PROFILE_BASIC_01 = "MF01";

	/**
      * 
      */
	public static final String PROFILE_RECORTADO_02 = "MF02";

	/**
      * 
      */
	public static final String PROFILE_ADVANCED_03 = "MF03";

	/**
	 * Type of account: Check.
	 */
	public static final String CHECK_TYPE = "CH";

	/**
	 * Type of account: Libreton.
	 */
	public static final String LIBRETON_TYPE = "LI";

	/**
	 * Type of account: Savings.
	 */
	public static final String SAVINGS_TYPE = "AH";

	/**
	 * Type of account: Credit.
	 */
	public static final String CREDIT_TYPE = "TC";

	/**
	 * Type of account: Debit.
	 */
	public static final String DEBIT_TYPE = "TD";

	/**
	 * Type of account: Prepaid.
	 */
	public static final String PREPAID_TYPE = "TP";

	/**
	 * Type of account: Express.
	 */
	public static final String EXPRESS_TYPE = "CE";
	
	//SPEI
	
	/**
     * Type of account: SPEI.
     */
    public static final String SPEI_TYPE = "SP";
    //Termina SPEI

	/**
	 * Type of concept for an account: charge (origin).
	 */
	public static final String ORIGIN_CONCEPT = "C";

	/**
	 * Type of concept for an account: deposit (destination).
	 */
	public static final String DESTINATION_CONCEPT = "A";

	/**
	 * Type of account: Clabe
	 */
	public final static String CLABE_TYPE_ACCOUNT = "CL";
	
	//SPEI
	//para tipo celular
	public final static String PHONE_TYPE_ACCOUNT = "TL";
	
	
	/** Perfiles de usuario */
	public static final String PERFIL_AVANZADO = "avanzado";
	
	public static final String PERFIL_BASICO = "basico"; 
	

	/** Perfiles de usuario */
	public static final String IDENTIFICADOR = "identificador";
	
	public static final String VALOR = "valor";
	
	public static final String TABLE_USERLOGIN = "userlogin";
	
	public static final String DATABASE_NAME = "ProyectoBase.db";
	
	/** Valores Midlet Simulacion **/
	public static final String VM300 = "300";
	public static final String VM350 = "350";
	public static final String VM411 = "411";
	public static final String VM500 = "500";
	public static final String VM910 = "910";
	
	/** Valores Tipo de Operacion **/
	public static final String OP_CALCULO_DE_ALTERNATIVAS_STR = "Calculo de alternativas";
	public static final String OP_GUARDAR_ALTERNATIVAS_STR = "Guardar alternativas";
	public static final String OP_ELIMINAR_STR = "Eliminar";
	public static final Integer OP_CALCULO_DE_ALTERNATIVAS = 2;
	public static final Integer OP_GUARDAR_ALTERNATIVAS = 3;
	public static final Integer OP_ELIMINAR = 4;
	
	/** Claves Creditos **/
	public static final String INCREMENTO_LINEA_CREDITO = "0ILC";
	public static final String TARJETA_CREDITO = "0TDC";
	public static final String CREDITO_HIPOTECARIO = "0HIP";
	public static final String CREDITO_AUTO = "AUTO";
	public static final String PRESTAMO_PERSONAL_INMEDIATO = "0PPI";
	public static final String CREDITO_NOMINA = "0NOM";
	
	/**  R�pidos  **/
	/**
	 * N�mero m�ximo de r�pidos que pueden mostrarse en la p�gina, contando el
	 * bot�n de m�s o de agregar en caso de que aparescan.
	 */
	public final static int MAX_RAPIDOS_POR_PAGINA = 5;

	/**
	 * C�digo de una operaci�n r�pida de pago de servicios.
	 */
	public final static String RAPIDOS_CODIGO_OPERACION_TIEMPO_AIRE = "compraTiempoAire";

	/**
	 * C�digo de una operaci�n r�pida de recargas.
	 */
	public final static String RAPIDOS_CODIGO_OPERACION_DINERO_MOVIL = "dineroMovil";

	/**
	 * C�digo de una operaci�n r�pida de otros bancomer.
	 */
	public final static String RAPIDOS_CODIGO_OPERACION_OTROS_BBVA = "otrosBancomer";
	
	/** Valor de IUM est�tico **/
	public final static String IUM_ESTATICO = "5223723F10497C2B4928D7E73FC79453";//Marisol 785EB73D1466E3FF7737DBBAA8CA90D9";//JQH"5223723F10497C2B4928D7E73FC79453";
	
	/** Valor de n�mero celular est�tico **/
	public final static String CELULAR = "5559521022";//5532275090";//"5559521022";
	
	
	
	/**
	 * BBVA CREDIT INTEGRATIONS CONTANST FOR CONSUMO JSON
	 */
	
	
	public final static String JSON_CONSUMO_TAG="Consumo";
	public final static String CVESUBP="CveSubp";
	public final static String PAGOMILS="pagoMilS";
	public final static String PRODUCTO="producto";
	public final static String DESCDIASPAGO="1RO";
	
	
	
	
	
}
