package suitebancomer.aplicaciones.bbvacredit.gui.activities;

import java.util.ArrayList;
import java.util.Iterator;

import suitebancomer.aplicaciones.bbvacredit.common.GuiTools;
import suitebancomer.aplicaciones.bbvacredit.common.ListaDatosController;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.LineaCreditoDelegate;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.SimulaCreditoAutoDelegate;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.SimulaPPIDelegate;
import suitebancomer.aplicaciones.bbvacredit.models.Producto;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.R.layout;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SimulaPPILiquidacionActivity extends BaseActivity implements OnClickListener{

	private SimulaPPIDelegate delegate;
	private ImageButton refreshBtn;
	private Button resumenLiquidacionBtn;
	
	//private ImageView simulaBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.activity_simula_ppiliquidacion);
		isOnForeground = true;
		MainController.getInstance().setCurrentActivity(this);
		init();
		//delegate.doRecalculoNC(this);
		deleteSimulation();
	}
	
	private void init(){
		// le indicamos al delegate que no es contratacion
		Tools.setIsContratacionPreference(false);
		delegate = new SimulaPPIDelegate();
		setSaldo();
		// Ocultamos la info si es necesario
		if(delegate.getOcultaInfo()){
			ocultaInfoNC();
		}else{
			pintarInfoTabla();
		}
		scaleToCurrentScreen();
		mapearBotones();
		Tools.getCurrentSession().setVisibilityButton(this, R.id.simPPILBottomMenuRC);
	}
	
	private void setSaldo(){
		TextView saldo = (TextView)findViewById(R.id.simPPILILMtxt);
		
		saldo.setText(GuiTools.getMoneyString(delegate.getSaldo().toString()));
	}
	
	// Oculta Info
	private void ocultaInfoNC(){
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCAuto);
		tlayout.setVisibility(View.INVISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.RelativeLayoutText1);
		tableLayout.setVisibility(View.INVISIBLE);
	}
	
	private void muestraInfoNC(){
		
		LinearLayout tlayout = (LinearLayout)findViewById(R.id.simCAuto);
		tlayout.setVisibility(View.VISIBLE);
		
		LinearLayout tableLayout = (LinearLayout)findViewById(R.id.RelativeLayoutText1);
		tableLayout.setVisibility(View.VISIBLE);
	}
	// End Oculta Info
	
	private void mapearBotones(){
		refreshBtn = (ImageButton)findViewById(R.id.headerRefresh);
		refreshBtn.setOnClickListener(this);
		
		resumenLiquidacionBtn = (Button)findViewById(R.id.simPPILBottomMenuRC);
		resumenLiquidacionBtn.setOnClickListener(this);
		
		//simulaBtn = (ImageView)findViewById(R.id.simPPILHeaderLogo);
		//simulaBtn.setOnClickListener(this);
	}
	
	private void scaleToCurrentScreen(){
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scale(findViewById(R.id.simPPILLayout));
		guiTools.scale(findViewById(R.id.simPPILBodyLayout));
		guiTools.scale(findViewById(R.id.simPPILTitle),true);
		guiTools.scale(findViewById(R.id.simPPILSubTitleTCImg));
		//guiTools.scale(findViewById(R.id.simPPILLText),true);
		guiTools.scale(findViewById(R.id.simPPILILMtxt),true);
		//guiTools.scale(findViewById(R.id.simPPILHeaderLogo));
		guiTools.scale(findViewById(R.id.simPPILBottomLayout));
		guiTools.scale(findViewById(R.id.simPPILBottomMenuRC),true);

		guiTools.scale(findViewById(R.id.simCAuto));
		guiTools.scale(findViewById(R.id.simCAutoLlblAuxiliar),true);
		
		//guiTools.scale(findViewById(R.id.simCAutoLblInfo),true);
		guiTools.scale(findViewById(R.id.RelativeLayoutText1));
	}
	
	private void ocultaCamposTabla(){
		LinearLayout layout = (LinearLayout)findViewById(R.id.RelativeLayoutText1);
		layout.removeAllViews();
		
	}
	
	public void pintarInfoTabla(){
		ocultaCamposTabla();
		
		muestraInfoNC();
		
		ArrayList<Producto> lista = delegate.getProductTable();
		Iterator<Producto> it = lista.iterator();
		Integer cont = 0;

		ListaDatosController listaController;
		 String prodDesc ="";
		while(it.hasNext()){
			Producto p = it.next();
			
			if(p.getCveProd().equals("0NOM") || p.getCveProd().equals("0PPI")) 
			{
	  prodDesc = "PRESTAMO PERSONAL";
			                       } 
			else {
			                               prodDesc = p.getDesProd();
			                       }

			listaController = new ListaDatosController((LinearLayout)findViewById(R.id.RelativeLayoutText1), this, this, R.id.RelativeLayoutText1);
			listaController.addElement(prodDesc, GuiTools.getMoneyString(p.getSubproducto().get(0).getMonMax().toString()), false);
			++cont;
		}
	}

	@Override
	public void onClick(View v) {
		isOnForeground = false;
		if(v.getId() == R.id.headerRefresh){
			delegate.setRet();
			delegate.redirectToView(MenuPrincipalActivity.class);
		}else if(v.getId() == R.id.simPPILBottomMenuRC){
			delegate.redirectToView(ResumenActivity.class);
		//}else if(v.getId() == R.id.simPPILHeaderLogo){
			//delegate.doRecalculoNC(this);
			
		}
	}
	
	@Override    
    public boolean onKeyDown(int keyCode, KeyEvent event) { 

    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			isOnForeground = false;
    			delegate.redirectToView(MenuPrincipalActivity.class);
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}		
    }
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if(!isScreenOn() || (!this.isFinishing()&&isOnForeground))
		{
			SuiteApp.getInstance().closeBmovilAppSession();
		}
	}
	
	
	
	
	/**
	 * June 8th,2015,
	 */
	
	private void deleteSimulation()
	{
		delegate.deleteSimulationRequest(this,true);
	}
	
	public void doRecalculo()
	{
		Log.d("Elimina Simulacion","ok");
		delegate.doRecalculoNC(this);
	}
	
	
}
