package suitebancomer.aplicaciones.bbvacredit.controllers;

import suitebancomer.aplicaciones.bbvacredit.gui.activities.BaseActivity;
import android.content.Intent;

public class ActivityController {
	private static ActivityController theInstance = null;
	
	private BaseActivity currentActivity;

	public BaseActivity getCurrentActivity() {
		return currentActivity;
	}

	void setCurrentActivity(BaseActivity currentActivity) {
		this.currentActivity = currentActivity;
	}
	
	void showScreen(Class<?> activity) {
		Intent intent = new Intent(currentActivity, activity);
		currentActivity.startActivity(intent);
	}
}
