package suitebancomer.aplicaciones.bbvacredit.models;

public class CreditoContratado {

	private String cveProd;
	
	private String desProd;
	
	private String contrato;
	
	private Integer montCre;
	
	private Integer saldo;
	
	private Integer pagoMen;
	
	private Boolean indicadorSim;

	public CreditoContratado() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CreditoContratado(String cveProd, String desProd, String contrato,
			Integer montCre, Integer saldo, Integer pagoMen,
			Boolean indicadorSim) {
		super();
		this.cveProd = cveProd;
		this.desProd = desProd;
		this.contrato = contrato;
		this.montCre = montCre;
		this.saldo = saldo;
		this.pagoMen = pagoMen;
		this.indicadorSim = indicadorSim;
	}

	public String getCveProd() {
		return cveProd;
	}

	public void setCveProd(String cveProd) {
		this.cveProd = cveProd;
	}

	public String getDesProd() {
		return desProd;
	}

	public void setDesProd(String desProd) {
		this.desProd = desProd;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public Integer getMontCre() {
		return montCre;
	}

	public void setMontCre(Integer montCre) {
		this.montCre = montCre;
	}

	public Integer getSaldo() {
		return saldo;
	}

	public void setSaldo(Integer saldo) {
		this.saldo = saldo;
	}

	public Integer getPagoMen() {
		return pagoMen;
	}

	public void setPagoMen(Integer pagoMen) {
		this.pagoMen = pagoMen;
	}

	public Boolean getIndicadorSim() {
		return indicadorSim;
	}

	public void setIndicadorSim(Boolean indicadorSim) {
		this.indicadorSim = indicadorSim;
	}
	
}
