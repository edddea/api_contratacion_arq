package suitebancomer.aplicaciones.bbvacredit.controllers;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Vector;

import suitebancomer.aplicaciones.bbvacredit.common.AbstractSuitePowerManager;
import suitebancomer.aplicaciones.bbvacredit.gui.activities.BaseActivity;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.BaseDelegate;
import suitebancomer.aplicaciones.bbvacredit.io.NetworkOperationCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ParsingExceptionCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerResponseCredit;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuPrincipalViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomer.aplicaciones.bbvacredit.io.ConnectionFactoryCredit;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.StartBmovilInBack;
import com.bancomer.mbanking.SuiteApp;

public class MainController {
	
	private static MainController theInstance = null;
	
	private ActivityController activityController;
	private Server server;
	private Context context;
	private Vector<BaseActivity> screenStack;

	
	//BBVA INTEGRATION
	private MenuPrincipalViewController menuSuiteController;
	
	/**
	 * Pending network operations.
	 */
	protected Hashtable<String, NetworkOperationCredit> networkOperationsTable = null;
	
	/**
	 * Reference to a pending network operation, in case that an
	 * operation launched a session expired error. This operation
	 * will be relaunched after a successful login
	 */
	protected NetworkOperationCredit pendingOperation = null;
	
	/**
     * A progress dialog for long waiting processes.
     */
    protected ProgressDialog mProgressDialog;
	
	private MainController() {
		activityController = new ActivityController();
		networkOperationsTable = new Hashtable<String, NetworkOperationCredit>();
		server = new Server();
		screenStack = new Vector<BaseActivity>();
	}

	public static MainController getInstance() {
		if (theInstance == null) {
			theInstance = new MainController();
		}
		
		return theInstance;
	}
	
	public void setCurrentActivity(BaseActivity currentActivity) {
		screenStack.add(currentActivity);
		activityController.setCurrentActivity(currentActivity);
	}
	
	public void goBack() {
		if (screenStack.size() > 0) {
			screenStack.removeElementAt(screenStack.size() - 1);
		}
		
		if (screenStack.size() > 0) {
			activityController.setCurrentActivity(screenStack.elementAt(screenStack.size() - 1));
		} else {
			activityController.setCurrentActivity(null);
		}
	}
	
	public void showScreen(Class<?> activity) {
		activityController.showScreen(activity);
	}
	
	
	/**
	 * These methods are going to  be used to call methods from suite Bancomer.
	 * */

	public void setMenuSuiteController(MenuPrincipalViewController controller)
	{
		menuSuiteController=controller;
	}
	
	public MenuPrincipalViewController getMenuSuiteController()
	{
		return menuSuiteController;
	}

	/**
	 * These methods are going to  be used to call methods from suite Bancomer when other app start session.
	 * */
	StartBmovilInBack startBmovilInBack;
	public void setStartBmovilInBack(StartBmovilInBack controller)
	{
		startBmovilInBack=controller;
	}

	public StartBmovilInBack getStartBmovilInBack()
	{
		return startBmovilInBack;
	}
	
	
	
	//End of region.
	

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

	public Server getServer() {
		return server;
	}
	
	public ActivityController getActivityController() {
		return activityController;
	}
	
	/**
	 * Determines if the requested operation is not being called already
	 * @param opId the operation id
	 * @return true if the operation isn't already waiting for a response
	 */
	private boolean isNotAlreadyCalling(String opId){
		return !this.networkOperationsTable.containsKey(opId);
	}
	
	/**
     * Places a progress dialog, for long waiting processes.
     * 
     * @param strTitle the dialog title
     * @param strMessage the message to show in the while
     */
    public void muestraIndicadorActividad(String strTitle, String strMessage) {
    	if(mProgressDialog != null){
    		ocultaIndicadorActividad();
    	}	
//		mProgressDialog = ProgressDialog.show(getContext(), strTitle, 
//    			strMessage, true);
    	mProgressDialog = ProgressDialog.show(this.getActivityController().getCurrentActivity(), strTitle, strMessage, true);
		mProgressDialog.setCancelable(false);
    }
    
    /**
     * Hides the progress dialog.
     */
    public void ocultaIndicadorActividad() {
    	if(mProgressDialog != null){
    		mProgressDialog.dismiss();
    	}
    }

	/**
	 * metodo para llamar el api server
	 * @param operationId
	 * @param params
	 * @param caller
	 * @param progressLabel
	 * @param progressMessage
	 * @param isJson
	 * @param hadler
	 * @param contex
	 */
	public void invokeNetworkOperation(final String operationId, final Hashtable<String, ?> params,
			  final BaseDelegate caller, 
			  String progressLabel, 
			  String progressMessage,
		   final Boolean isJson, final Object hadler, final Context contex) {
              
		
		Log.e("Código en invoke",operationId);
             		
		if (isNotAlreadyCalling(operationId)) {

			try {

				NetworkOperationCredit operation = new NetworkOperationCredit(operationId, params, caller);
				this.networkOperationsTable.put(operationId, operation);
				this.pendingOperation = operation;
				
				if (caller != null) {
					if(AbstractSuitePowerManager.getSuitePowerManager().isScreenOn())
						// TODO: Fix para no bloquear dos veces el login. Corregir.
						if(!operationId.equals(Server.CONSULTA_ALTERNATIVAS)) muestraIndicadorActividad(progressLabel, progressMessage);
					else
						Log.d(this.getClass().getSimpleName(), "La aplicaci�n estaba bloqueada.");
				}

				Thread thread = new Thread(new Runnable() {
					public void run() {
						try {
							//ServerResponseCredit response = getServer().doNetworkOperationCredit(operationId, params);
							int opId=0;
							if(Server.CALCULO_ALTERNATIVAS_OPERACION.equals(operationId) || Server.CALCULO_OPERACION.equals(operationId) ){
								opId=Server.CALCULO_ALTERNATIVAS_OPERACION_ID;
							}else if(Server.CONSULTA_CORREO_OPERACION.equals(operationId)){
								opId=Server.CONSULTA_CORREO_OPERACION_ID;
							}else if(Server.ENVIO_CORREO_OPERACION.equals(operationId)){
								opId=Server.ENVIO_CORREO_OPERACION_ID;
							}else if(Server.CONSULTA_ALTERNATIVAS.equals(operationId)){
								opId=Server.CONSULTA_ALTERNATIVAS_ID;
							}else if(Server.CONSULTA_TDC.equals(operationId)){
								opId=Server.CONSULTA_TDC_ID;
							}else if(Server.DETALLE_ALTERNATIVA.equals(operationId)){
								opId=Server.DETALLE_ALTERNATIVA_ID;
							}else if(Server.CONTRATA_ALTERNATIVA_CONSUMO.equals(operationId)){
								opId=Server.CONTRATA_ALTERNATIVA_CONSUMO_ID;
							}

							ServerResponseCredit responseNew=null;
							ParametersTO parameters=new ParametersTO();
							parameters.setSimulation(Server.SIMULATION);
							parameters.setDevelopment(Server.DEVELOPMENT);
							parameters.setOperationId(opId);
							parameters.setParameters(params.clone());
							parameters.setJson(isJson);
							IResponseService resultado = new ResponseServiceImpl() ;
							//Convirtiendo el Enum al esperado por API SERVER

							suitebancomer.aplicaciones.commservice.commons.ApiConstants.isJsonValueCode isJsonTransformValue= ApiConstants.isJsonValueCode.NONE;
							Hashtable<String, ?> parameters2 = ((Hashtable<String, ?>)parameters.getParameters());
							ConnectionFactoryCredit cf= new ConnectionFactoryCredit(parameters.getOperationId(), parameters2, parameters.isJson(), hadler,contex,isJsonTransformValue);
							resultado=cf.processConnectionWithParams();
							responseNew=cf.parserConnection(resultado, hadler);

							returnFromNetworkOperation(operationId, responseNew, caller, null);
						} catch (Throwable t) {
							returnFromNetworkOperation(operationId, null, caller, t);
						}
					}

				});

				// Starts the worker thread to perform network operation
				thread.start();

			} catch (Throwable th) {
				returnFromNetworkOperation(operationId, null, caller, th);
			}

		} else {
			// unable to process request, show error and return
			return;
		}
	}

	/**
	 * Invoke a network operation. It shows a popup indicating progress with
	 * custom texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param progressLabel text with the title of the progress popup
	 * @param progressMessage text with the content of the progress popup
	 */
	public void invokeNetworkOperation(final String operationId, final Hashtable<String, ?> params,
			  final BaseDelegate caller,
			  String progressLabel,
			  String progressMessage) {


		Log.e("Código en invoke",operationId);

		if (isNotAlreadyCalling(operationId)) {

			try {

				NetworkOperationCredit operation = new NetworkOperationCredit(operationId, params, caller);
				this.networkOperationsTable.put(operationId, operation);
				this.pendingOperation = operation;

				if (caller != null) {
					if(AbstractSuitePowerManager.getSuitePowerManager().isScreenOn())
						// TODO: Fix para no bloquear dos veces el login. Corregir.
						if(!operationId.equals(Server.CONSULTA_ALTERNATIVAS)) muestraIndicadorActividad(progressLabel, progressMessage);
					else
						Log.d(this.getClass().getSimpleName(), "La aplicaci�n estaba bloqueada.");
				}

				Thread thread = new Thread(new Runnable() {
					public void run() {
						try {
							ServerResponseCredit response = getServer().doNetworkOperationCredit(operationId, params);
							returnFromNetworkOperation(operationId, response, caller, null);
						} catch (Throwable t) {
							returnFromNetworkOperation(operationId, null, caller, t);
						}
					}

				});

				// Starts the worker thread to perform network operation
				thread.start();

			} catch (Throwable th) {
				returnFromNetworkOperation(operationId, null, caller, th);
			}

		} else {
			// unable to process request, show error and return
			return;
		}
	}

	/**
	 * Called internally after a network operation has returned. It ends the progress
	 * popup and invokes method to analyze the result
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message from the server or method should handle in the default
	 * implementation
	 */
	protected void returnFromNetworkOperation(final String operationId, 
											  final ServerResponseCredit response, 
											  final BaseDelegate caller, 
											  final Throwable throwable) {

		if (caller != null) {
			//TODO: Fix para no bloquear dos veces el login. Corregir.
			if (operationId.equals(Server.CONSULTA_ALTERNATIVAS)) {
				ocultaIndicadorActividad();
				if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session
					MainController.getInstance().getStartBmovilInBack().habilitarVista();
				}else{
					MainController.getInstance().getMenuSuiteController().habilitarVista();
				}
			} else if(!operationId.equals(Server.LOGIN_OPERACION)) ocultaIndicadorActividad();
			getActivityController().getCurrentActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						analyzeNetworkResponse(operationId, response, throwable, caller);
					} catch (Throwable t) {
						Log.d(this.getClass().getSimpleName(), "Exception on analyzeNetworkResponse.", t);
						if (caller != null) {
							String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
							getActivityController().getCurrentActivity().showErrorMessage(message);
						}
					}
				}
			});
		} else { //TODO session timer expired, go to login screen
			Thread thread = new Thread(new Runnable() {
				public void run() {
					try {
						analyzeNetworkResponse(operationId, response, throwable, null);
					} catch (Throwable t) {
						String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
						Log.d(this.getClass().getSimpleName() + " :: returnFromNetworkOperation", message);
					}
				}
			});
			thread.run();
		}
	}
	
	/**
	 * Get the operation error message
	 * @param throwable the throwable
	 * @return the operation error message 
	 */
	private String getErrorMessage(Throwable throwable) {
		StringBuffer sb = new StringBuffer();
		if (throwable != null) {
			if (throwable instanceof NumberFormatException) {
				sb.append(this.getContext().getString(R.string.error_format));
			} else if (throwable instanceof ParsingExceptionCredit) {
				sb.append(this.getContext().getString(R.string.error_format));
			} else if (throwable instanceof IOException) {
				sb.append(this.getContext().getString(R.string.error_communications));
			} else {
				sb.append(this.getContext().getString(R.string.error_communications));
			}
		}
		return sb.toString();
	}
	
	/**
	 * Analyze the network response obtained from the server
	 * @param operationId network identifier. See Server class.
	 * @param response the ServerResponse instance built from the server response. It
	 * contains the type of result (success, failure), and the real content for
	 * the application business.
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	protected void analyzeNetworkResponse(String operationId, 
										  ServerResponseCredit response, 
										  Throwable throwable, 
										  final BaseDelegate caller) {
		// get the caller           
		NetworkOperationCredit operation = this.networkOperationsTable.get(operationId);
		if (operation != null) {
			// remove the operation id from the table
			this.networkOperationsTable.remove(operationId);
			if (operation.isActive()) {
				if (response != null) {
					BaseDelegate baseDelegate = operation.getCaller();
					int resultCode = response.getStatus();
					switch (resultCode) {
					case ServerResponseCredit.OPERATION_SUCCESSFUL:
					case ServerResponseCredit.OPERATION_WARNING:
						baseDelegate.analyzeResponse(operationId, response);						
											
						break;
					case ServerResponseCredit.OPERATION_OPTIONAL_UPDATE:
						baseDelegate.analyzeResponse(operationId, response);
						break;
					case ServerResponseCredit.OPERATION_ERROR:
						this.pendingOperation = null;
						String errorCode = response.getMessageCode();						
						
//						if ((operationError == Server.LOGIN_OPERATION) && (Constants.DEACTIVATION_ERROR_CODE.equals(errorCode))) {
//							Session session = Session.getInstance(SuiteApp.appContext);
//							String username = session.getUsername();
//							String password = session.getPassword();
//							Session.getInstance(suiteApp.getApplicationContext()).setValidity(Session.UNSET_STATUS);
//
//							Bundle params = new Bundle();
//							params.putString(Server.USERNAME_PARAM, username);
//							params.putString(Server.PASSWORD_PARAM, password);					
//								
//						} else {
						if(bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin  inicio la session
							this.getActivityController().getCurrentActivity().showErrorMessage(errorCode + "\n" + response.getMessageText());
							baseDelegate.analyzeResponse(operationId, response);
						}else{
							MainController.getInstance().getStartBmovilInBack().error(errorCode,response.getMessageText());
						}

//						}
						break;
					case ServerResponseCredit.OPERATION_SESSION_EXPIRED:
						this.pendingOperation = operation;
//						Session.getInstance(this.getContext()).setValidity(Session.INVALID_STATUS);
//						suiteApp.getSuiteViewsController().showMenuSuite(true);
						break;
					default:
						ocultaIndicadorActividad();
						this.getActivityController().getCurrentActivity().showErrorMessage(R.string.error_format);
						break;
					}
				} else {
					if (throwable != null) {
						ocultaIndicadorActividad();
						//this.getActivityController().getCurrentActivity().showErrorMessage("error");
						this.getActivityController().getCurrentActivity().showErrorMessage(getErrorMessage(throwable));
					} else {
						ocultaIndicadorActividad();
						this.getActivityController().getCurrentActivity().showErrorMessage(R.string.error_communications);
					}
				}
			}
		} else {
			ocultaIndicadorActividad();
			this.getActivityController().getCurrentActivity().showErrorMessage(R.string.error_unexpected);
		}		
	}
}
