package suitebancomer.aplicaciones.bbvacredit.models;

public class Plazo {
	
	private String cvePlazo;
	
	private String desPlazo;

	public String getCvePlazo() {
		return cvePlazo;
	}

	public void setCvePlazo(String cvePlazo) {
		this.cvePlazo = cvePlazo;
	}

	public String getDesPlazo() {
		return desPlazo;
	}

	public void setDesPlazo(String desPlazo) {
		this.desPlazo = desPlazo;
	}
	
}
