package suitebancomer.aplicaciones.bbvacredit.gui.delegates;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.activities.SimulaTarjetaCreditoActivity;
import suitebancomer.aplicaciones.bbvacredit.io.AuxConectionFactoryCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerResponseCredit;
import suitebancomer.aplicaciones.bbvacredit.models.CalculoData;
import suitebancomer.aplicaciones.bbvacredit.models.ObjetoCreditos;
import suitebancomer.aplicaciones.bbvacredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import android.content.SharedPreferences;
import android.util.Log;

import com.bancomer.mbanking.R;

public class SimulaTarjetaCreditoDelegate extends BaseDelegateOperacion{

	private Session session;

	private Boolean ocultaInfo = true;

	// Indice de producto dentro del array
	private Integer productIndex = 0;

	private Integer montoMin = 0;

	private Integer montoMax = 0;

	private Integer montoProgress = 0;

	private Producto producto = null;

	private ArrayList<Producto> productTable =  new ArrayList<Producto>();

	private SimulaTarjetaCreditoActivity actContrato;

	private ObjetoCreditos data;

	private Boolean ret = false;

	public Integer getMontoProgress() {
		return montoProgress;
	}

	public void setMontoProgress(Integer montoProgress) {
		this.montoProgress = montoProgress;
	}

	public ArrayList<Producto> getProductTable() {
		return productTable;
	}

	public void setProductTable(ArrayList<Producto> productTable) {
		this.productTable = productTable;
	}

	public Boolean getOcultaInfo() {
		return ocultaInfo;
	}

	public void setOcultaInfo(Boolean ocultaInfo) {
		this.ocultaInfo = ocultaInfo;
	}

	public Boolean getRet() {
		return ret;
	}

	public void setRet(Boolean ret) {
		this.ret = ret;
	}

	public Integer getProductIndex() {
		return productIndex;
	}

	public void setProductIndex(Integer productIndex) {
		this.productIndex = productIndex;
	}

	public Integer getMontoMin() {
		return montoMin;
	}

	public void setMontoMin(Integer montoMin) {
		this.montoMin = montoMin;
	}

	public Integer getMontoMax() {
		return montoMax;
	}

	public void setMontoMax(Integer montoMax) {
		this.montoMax = montoMax;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public void setProducto() {
		SharedPreferences sp = MainController.getInstance().getContext().getSharedPreferences(ConstantsCredit.SHARED_POSICION_GLOBAL, 0);
		productIndex = sp.getInt(ConstantsCredit.SHARED_POSICION_GLOBAL_INDEX, 0);

		producto = data.getProductos().get(productIndex);
	}

	public SimulaTarjetaCreditoDelegate() {
		session = Tools.getCurrentSession();
		actContrato = session.getTdcAct();
		// Datos de sesion
		data = session.getCreditos();
		// Bloqueamos la pantalla
		MainController.getInstance().muestraIndicadorActividad("operacion", "conectando");

		if(Tools.getIsContratacionPreference()){
			setProducto();
			if(producto != null){
				if(producto.getIndSimBoolean()){
					productTable = data.getProductos();
					ocultaInfo = true;
					montoProgress = Double.valueOf(producto.getMontoDeseS()).intValue();
				}else{
					ocultaInfo = false;
					montoProgress = Double.valueOf(producto.getSubproducto().get(0).getMonMax()).intValue();
				}

				// Establecer max y min monto
				montoMin = Double.valueOf(producto.getSubproducto().get(0).getMonMin()).intValue();
				montoMax = Double.valueOf(producto.getSubproducto().get(0).getMonMax()).intValue();

			}
		}

		// desbloqueamos
		MainController.getInstance().ocultaIndicadorActividad();

	}

	private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}else{
			paramTable.put(cnt, "");
			Log.d(param.getClass().getName(), param.toString()+" empty or null");
		}
	}

	private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}
	}

	public void doRecalculoContract(SimulaTarjetaCreditoActivity actContrato){
		// Mapeamos el activity
		this.actContrato = actContrato;

		// Mapeamos el usuario y la contrase�a
		Hashtable<String, String> paramTable = new Hashtable<String, String>();

		// Mapeamos el codigo de operacion
		addParametroObligatorio(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);

		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);

		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);

		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

		// Mapeamos el tipo de operacion -> 2
		addParametroObligatorio(ConstantsCredit.OP_CALCULO_DE_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);

		// Tomamos el credito contratado y el subproducto asociado
		Producto ccr = data.getProductos().get(productIndex);

		int montoSel = actContrato.getSeekbar().getProgress();

		addParametro(ccr.getCveProd(), ServerConstantsCredit.CVEPROD_PARAM,paramTable);
		addParametro(ccr.getSubproducto().get(0).getCveSubp(), ServerConstantsCredit.CVESUBP_PARAM,paramTable);
		addParametro(montoSel, ServerConstantsCredit.MON_DESE_PARAM, paramTable);

		Hashtable<String, String> paramTable2 = AuxConectionFactoryCredit.calculo(paramTable);
		this.doNetworkOperation(getCodigoOperacion(), paramTable2, true, new CalculoData(), MainController.getInstance().getContext());
	}

	/****/
	private void setSimLooking4Cve(String cve){
		Iterator<Producto> it = data.getProductos().iterator();

		while(it.hasNext()){
			Producto pr = it.next();
			if(pr.getCveProd().equals(cve)) pr.setIndSimBoolean(true);
		}
	}

	public void analyzeResponse(String operationId, ServerResponseCredit response) {
		if(getCodigoOperacion().equals(operationId)){
    		if(response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL){
    			if(Tools.getIsContratacionPreference()){
    				// Llenar creditos
    				CalculoData respuesta = (CalculoData) response.getResponse();
    				setRet(true);

    				// Informamos el obj creditos con la informaci�n necesaria
    				data.setCreditos(respuesta.getCreditos());
    				data.setEstado(respuesta.getEstado());
    				data.setMontotSol(respuesta.getMontotSol());
    				data.setPagMTot(respuesta.getPagMTot());
    				data.setPorcTotal(respuesta.getPorcTotal());
    				data.setProductos(respuesta.getProductos());

    				// Setear indsim

    				productIndex = session.getProductoIndexByCve(ConstantsCredit.TARJETA_CREDITO);
    				producto = data.getProductos().get(productIndex);

    				if(data.getProductos().get(productIndex).getCveProd().equals(ConstantsCredit.TARJETA_CREDITO)){

        				data.getProductos().get(productIndex).setIndSimBoolean(true);
    				}else{
    					setSimLooking4Cve(ConstantsCredit.TARJETA_CREDITO);
    				}
    				Tools.getCurrentSession().setVisibilityButton(actContrato, R.id.simTCBottomMenuRC);
    				actContrato.pintarInfoTabla();
    			}
    		}
    	}else if(getCodigoOperacionGuardarEliminar().equals(operationId))
    	{
    		if(response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL){
    			actContrato.doRecalculo();
    		}
    	}
	}

	public <T> void redirectToView(Class<T> c){
		MainController.getInstance().showScreen(c);
	}

	@Override
	protected String getCodigoOperacion() {
		return Server.CALCULO_OPERACION;
	}



	/**
	 * Created June 8th,2015,
	 */


	public void deleteSimulationRequest(SimulaTarjetaCreditoActivity actContrato)
	{
		// Mapeamos el activity
				this.actContrato = actContrato;
				// Mapeamos el usuario y la contrase�a
				Hashtable<String, String> paramTable = new Hashtable<String, String>();

				// Mapeamos el codigo de operacion
				addParametroObligatorio(this.getCodigoOperacionGuardarEliminar(),ServerConstantsCredit.OPERACION_PARAM, paramTable);

				// Mapeamos el id de cliente tomado de la sesion
				addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);

				// Mapeamos el IUM tomado de la sesion
				addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);

				// Mapeamos el numeroCelular tomado de la sesion
				addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);


				// Mapeamos el tipo de operacion -> 4
				addParametroObligatorio(ConstantsCredit.OP_ELIMINAR, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);

		Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
		this.doNetworkOperation(getCodigoOperacionGuardarEliminar(), paramTable2, true, new CalculoData(), MainController.getInstance().getContext());
	}


	protected String getCodigoOperacionGuardarEliminar()
	{
		return Server.CALCULO_ALTERNATIVAS_OPERACION;
	}



}
