/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomer.aplicaciones.bbvacredit.models;

import java.io.Serializable;
import java.util.Vector;

import suitebancomer.aplicaciones.bbvacredit.common.NameValuePair;

/**
 *
 * @author Stefanini IT Solutions.
 * Catalog wraps a catalog, which is a list of items composed of
 * a key and a value
 *
 */
public class Catalog implements Serializable {


    /**
     * The list of items.
     */
    private Vector<NameValuePair> items;

    /**
     * Default constructor.
     */
    public Catalog() {
        items = new Vector<NameValuePair>();
    }

    /**
     * Creates a new catalog from an array or pairs Name-Value.
     * @param nameValuePairs do something
     */
    public Catalog(NameValuePair[] nameValuePairs) {
        items = new Vector<NameValuePair>();
        for (int i = 0; i < nameValuePairs.length; i++) {
        	if(nameValuePairs[i]!=null){
            items.addElement(nameValuePairs[i]);
        	}
        }
    }

    /**
     * Add an item to the catalog.
     * @param code the key to identify the item
     * @param value the value of the item
     */
    public void add(String code, String value) {
        items.addElement(new NameValuePair(code, value));
    }

    /**
     * Returns de NameValuePair in the index position of the items vector.
     * @param index Index inside the items vector
     * @return NameValuePair in the defined position
     */
    public NameValuePair getItem(int index) {
        if (index >= 0 && index < items.size()) {
            return ((NameValuePair) items.elementAt(index));
        }

        return null;

    }

    /**
     * Returns the size of the items vector.
     * @return Size of the items vector
     */
    public int size() {
        return items.size();
    }


    /**
     * Returns and string array containing all the codes for the catalog.
     * @return the codes
     */
    public String[] getCodes() {
        if (items != null && items.size() > 0) {
            String names[] = new String [items.size()];
            for (int i = 0; i < items.size(); i++) {
                names[i] = ((NameValuePair) items.elementAt(i)).getName();
            }
            return names;
        } else {
        	return null;
        }
    }


     /**
     * Returns and string array containing all the values for the catalog.
     * @return the values
     */
    public String[] getValues() {
        if (items != null && items.size() > 0) {
            String values[] = new String [items.size()];
            for (int i = 0; i < items.size(); i++) {
                values[i] = ((NameValuePair) items.elementAt(i)).getValue();
            }

            return values;
        } else {
        	return null;
        }
    }
    
    /**
     * Returns the code of name-value pair for the given name value.
     * 
     * @param name the name matching the code.
     * @return the code for the name or null if not found.
     */
    public String getCode(String name) {
    	
    	for (int index = 0; index < items.size(); index++) {
    		NameValuePair valuePair =  items.get(index);
    		if (valuePair.getValue().equals(name)){
    			return valuePair.getName();
    		}
    	}
    	
    	return null;
    }
    
    /**
     * Returns the name value pair whose code ends with the given suffix.
     * @param codeSuffix the searched code suffix.
     * @return a name value pair corresponding to the code or null if not found.
     */
    public NameValuePair getNameValuePair(String codeSuffix){
    	// TODO: Remover.
    	if(null == codeSuffix)
    		codeSuffix = null;
    	
    	
    	for (int index = 0; index < items.size(); index++) {
    		NameValuePair valuePair =  items.get(index);
    		if (valuePair.getName().endsWith(codeSuffix)){
    			return valuePair;
    		}
    		
    	}
    	return null;
    }
}
