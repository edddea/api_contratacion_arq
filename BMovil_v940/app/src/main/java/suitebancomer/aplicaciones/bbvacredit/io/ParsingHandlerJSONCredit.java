package suitebancomer.aplicaciones.bbvacredit.io;

import java.io.IOException;

import org.json.JSONException;

public interface ParsingHandlerJSONCredit {
	/**
     * Process a server response
     * @param parser
     * @throws IOException 
     * @throws ParsingExceptionCredit
     */
//	public void process(ParserJSON parser) throws IOException, ParsingException;
	public void processJSON(String jsonString) throws JSONException;
}
