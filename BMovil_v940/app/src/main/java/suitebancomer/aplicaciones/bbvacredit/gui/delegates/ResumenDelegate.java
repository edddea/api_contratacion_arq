package suitebancomer.aplicaciones.bbvacredit.gui.delegates;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.common.ListaDatosResumenController;
import suitebancomer.aplicaciones.bbvacredit.common.Session;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.activities.ResumenActivity;
import suitebancomer.aplicaciones.bbvacredit.gui.activities.SimulaCreditoNominaContratoActivity;
import suitebancomer.aplicaciones.bbvacredit.io.AuxConectionFactoryCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerResponseCredit;
import suitebancomer.aplicaciones.bbvacredit.models.CalculoData;
import suitebancomer.aplicaciones.bbvacredit.models.ConsultaCorreoData;
import suitebancomer.aplicaciones.bbvacredit.models.CreditoContratado;
import suitebancomer.aplicaciones.bbvacredit.models.EnvioCorreoData;
import suitebancomer.aplicaciones.bbvacredit.models.ObjetoCreditos;
import suitebancomer.aplicaciones.bbvacredit.models.Producto;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import android.util.Log;

public class ResumenDelegate  extends BaseDelegateOperacion{

	private Session session;
	
	private ObjetoCreditos data;
	
	private Boolean haySimulacion = false;
	
	private Boolean haySimulacionCont = false;
	
	private Boolean sendSecond = true;
	
	private ResumenActivity act = null;

	//Modificacion 50986
	private ListaDatosResumenController actListaResumen = null;

	private Boolean isConsumo;
	
	private static final String EMAIL_PATTERN = 
			"^([_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*){1,64}@"
			+ "([_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*(\\.[_A-Za-z0-9-]+)){1,255}$";
	
	public Boolean getHaySimulacionCont() {
		return haySimulacionCont;
	}

	public void setHaySimulacionCont(Boolean haySimulacionCont) {
		this.haySimulacionCont = haySimulacionCont;
	}

	public Boolean getHaySimulacion() {
		return haySimulacion;
	}

	public void setHaySimulacion(Boolean haySimulacion) {
		this.haySimulacion = haySimulacion;
	}
	
	public String getEmail(){
		return session.getEmail();
	}
	
	public ResumenDelegate() {
		session = Tools.getCurrentSession();
		// Datos de sesion
		data = session.getCreditos();
		// Bloqueamos la pantalla
		MainController.getInstance().muestraIndicadorActividad("operacion", "conectando");

	}
	
	public ArrayList<Producto> getProductos(){
		return data.getProductos();
	}
	
	public ArrayList<CreditoContratado> getCreditos(){
		return data.getCreditosContratados();
	}
	
	private void checkForSim(){
		
		Iterator<Producto> it = data.getProductos().iterator();
		
		while(it.hasNext()){
			if(it.next().getIndSimBoolean()) haySimulacion = true;
		}
		
		
	}
	
	private void checkForSimContratados(){
		
		Iterator<CreditoContratado> it = data.getCreditosContratados().iterator();
		
		while(it.hasNext()){
			if(it.next().getIndicadorSim()) haySimulacionCont = true;
		}
		
		
	}
	
	public void llenarTablas(ResumenActivity act){
		Integer resumen = 0;
		
		checkForSim();
		checkForSimContratados();
		if(data.getCreditosContratados() != null){
			resumen = act.pintarTablaContratados();
		}
		if(data.getProductos() != null){
			if(resumen > 0 ) act.pintarTablaProductos();
			act.pintarTablaSimulados();
		}
		act.pintarTotalMensual();
		MainController.getInstance().ocultaIndicadorActividad();
	}
	
	/**
	 * Peticiones
	 */
	
	public Boolean validaEmail(String email){
		Pattern pattern;
		Matcher matcher;
		
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(email);
		
		return matcher.matches();
		
	}
	
	private <T> void addParametroObligatorio(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}else{
			paramTable.put(cnt, "");
		}
	}
	
	private <T> void addParametro(T param, String cnt, Hashtable<String, String> paramTable){
		if(!Tools.isEmptyOrNull(param.toString())){
			paramTable.put(cnt, param.toString());
		}
	}
	
	private Boolean containsProductOperation(String operation){
		Iterator<Producto> it = data.getProductos().iterator();
		Boolean ret = false;
		while(it.hasNext()){
			Producto pr = it.next();
			if((pr.getCveProd().equals(operation))&&(pr.getIndSimBoolean())) ret = true;
		}
		
		return ret;
	}
	
	private Producto getProducto(String operation){
		Iterator<Producto> it = data.getProductos().iterator();
		Producto ret = null;
		while(it.hasNext()){
			Producto pr = it.next();
			if(pr.getCveProd().equals(operation)) ret = pr;
		}
		
		return ret;
	}
	
	private Boolean containsProductCreditoContratado(String operation){
		Iterator<CreditoContratado> it = data.getCreditosContratados().iterator();
		Boolean ret = false;
		while(it.hasNext()){
			CreditoContratado pr = it.next();
			if(pr.getCveProd().equals(operation)) ret = true;
		}
		
		return ret;
	}
	
	private CreditoContratado getCreditoContratado(String operation){
		Iterator<CreditoContratado> it = data.getCreditosContratados().iterator();
		CreditoContratado ret = null;
		while(it.hasNext()){
			CreditoContratado pr = it.next();
			if((pr.getCveProd().equals(operation))&&pr.getIndicadorSim()) ret = pr;
		}
		
		return ret;
	}
	
	private void sendEmailContratacion(){		
		//Modified by: WRGC; May 6th,2015.
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		
		// Mapeamos el codigo de operacion
		addParametro(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);
		
		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
		
		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
		
		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
		
		if(containsProductOperation(ConstantsCredit.CREDITO_NOMINA)){
			 
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getIndSim(),ServerConstantsCredit.SCN, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getMontoDeseS(),ServerConstantsCredit.CN_IMPORTE, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getMontoDeseS(),ServerConstantsCredit.CN_IMPORTE, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getTasaS(),ServerConstantsCredit.CN_TASA_ANUAL, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getDesPlazoE(),ServerConstantsCredit.CN_PLAZO, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getPagMProdS(),ServerConstantsCredit.CN_PAGO_FIJO_MENSUAL, paramTable);
		}else{
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCN, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CN_IMPORTE, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CN_TASA_ANUAL, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CN_PLAZO, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CN_PAGO_FIJO_MENSUAL, paramTable);
		}
		
		if(containsProductOperation(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
			addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getIndSim(),ServerConstantsCredit.SPPI, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getMontoDeseS(),ServerConstantsCredit.PP_IMPORTE, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getTasaS(),ServerConstantsCredit.PP_TASA_ANUAL, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getDesPlazoE(),ServerConstantsCredit.PP_PLAZO, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getPagMProdS(),ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL, paramTable);
		}else{
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SPPI, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.PP_IMPORTE, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.PP_TASA_ANUAL, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.PP_PLAZO, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL, paramTable);
		}
		
		if(containsProductOperation(ConstantsCredit.TARJETA_CREDITO)){
			addParametroObligatorio(getProducto(ConstantsCredit.TARJETA_CREDITO).getIndSim(),ServerConstantsCredit.STDC, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.TARJETA_CREDITO).getMontoMaxS(),ServerConstantsCredit.TC_LIMITE_DE_CREDITO_AUTORIZADO, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.TARJETA_CREDITO).getMontoDeseS(),ServerConstantsCredit.TC_LIMITE_DE_CREDITO_SIMULADO, paramTable);
		}else{
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.STDC, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.TC_LIMITE_DE_CREDITO_AUTORIZADO, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.TC_LIMITE_DE_CREDITO_SIMULADO, paramTable);
		}
		
		if(containsProductOperation(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
			addParametroObligatorio(getProducto(ConstantsCredit.INCREMENTO_LINEA_CREDITO).getIndSim(),ServerConstantsCredit.SILDC, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.INCREMENTO_LINEA_CREDITO).getNumTDCS(),ServerConstantsCredit.IL_TARJETA_DE_CREDITO, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.INCREMENTO_LINEA_CREDITO).getMontoMaxS(),ServerConstantsCredit.IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.INCREMENTO_LINEA_CREDITO).getMontoDeseS(),ServerConstantsCredit.IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA, paramTable);
		}else{
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SILDC, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.IL_TARJETA_DE_CREDITO, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA, paramTable);
		}
		
		if(containsProductOperation(ConstantsCredit.CREDITO_AUTO)){
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getIndSim(),ServerConstantsCredit.SCA, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getMontoDeseS(),ServerConstantsCredit.CA_IMPORTE, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getTasaS(),ServerConstantsCredit.CA_TASA_ANUAL, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getDesPlazoE(),ServerConstantsCredit.CA_PLAZO, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getPagMProdS(),ServerConstantsCredit.CA_PAGO_FIJO_MENSUAL, paramTable);
		}else{
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCA, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CA_IMPORTE, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CA_TASA_ANUAL, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CA_PLAZO, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CA_PAGO_FIJO_MENSUAL, paramTable);
		}
		
		if(containsProductOperation(ConstantsCredit.CREDITO_HIPOTECARIO)){
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getIndSim(),ServerConstantsCredit.SCH, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getMontoDeseS(),ServerConstantsCredit.CH_IMPORTE, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getTasaS(),ServerConstantsCredit.CH_TASA_ANUAL, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getDesPlazoE(),ServerConstantsCredit.CH_PLAZO, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getPagMProdS(),ServerConstantsCredit.CH_PAGO_FIJO_MENSUAL, paramTable);
		}else{
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCH, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CH_IMPORTE, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CH_TASA_ANUAL, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CH_PLAZO, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CH_PAGO_FIJO_MENSUAL, paramTable);
		}
		
		if((data.getProductos() != null) && (!data.getProductos().isEmpty())){
			addParametroObligatorio(data.getProductos().get(0).getCATS(),ServerConstantsCredit.CON_CAT, paramTable);
			try {
				addParametroObligatorio(java.net.URLEncoder.encode(data.getProductos().get(0).getFechaCatS(), "UTF-8").replace("+", "%20"),ServerConstantsCredit.CON_FECHA_CALCULO, paramTable);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				addParametroObligatorio(data.getProductos().get(0).getFechaCatS(),ServerConstantsCredit.CON_FECHA_CALCULO, paramTable);
				Log.e("EncodeURL",data.getProductos().get(0).getFechaCatS() );
			}
		}else{
			addParametroObligatorio("",ServerConstantsCredit.CON_CAT, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CON_FECHA_CALCULO, paramTable);
		}
		
		addParametroObligatorio("C",ServerConstantsCredit.IND_CORREO, paramTable);
		addParametroObligatorio(session.getEmail(),ServerConstantsCredit.email, paramTable);

		Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.envioCorreo(paramTable);
		this.doNetworkOperation(getCodigoOperacion(), paramTable2, true, new EnvioCorreoData(), MainController.getInstance().getContext());
	}
	
	private void sendEmailLiquidacion(){
		
		//Modified by: OOS,WRGC; May 6th,2015.
		boolean flagSCR=false;
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		
		// Mapeamos el codigo de operacion
		addParametro(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);
		
		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
		
		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
		
		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);
		

		if(containsProductCreditoContratado(ConstantsCredit.CREDITO_NOMINA)){
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.CREDITO_NOMINA).getIndicadorSim(),ServerConstantsCredit.SCNL, paramTable);
			//addParametroObligatorio(ServerConstantsCredit.CN_DESC_TITLE,ServerConstantsCredit.CN_DESC, paramTable);
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.CREDITO_NOMINA).getPagoMen(),ServerConstantsCredit.CN_CANT, paramTable);
			
			if(containsProductOperation(ConstantsCredit.CREDITO_NOMINA)){
				flagSCR=true;
				addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getMontoMaxS(),ServerConstantsCredit.CN, paramTable);
				addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_NOMINA).getIndSim(),ServerConstantsCredit.SAN, paramTable);				
			}else{
				addParametroObligatorio("",ServerConstantsCredit.CN, paramTable);
				addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAN, paramTable);
			}
		}else{
			//addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCNL, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.CN_DESC, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.CN_CANT, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CN, paramTable);
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAN, paramTable);
		}

		if(containsProductCreditoContratado(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getIndicadorSim(),ServerConstantsCredit.SPPIL, paramTable);
			//addParametroObligatorio(ServerConstantsCredit.PP_DESC_TITLE,ServerConstantsCredit.PP_DESC, paramTable);
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getPagoMen(),ServerConstantsCredit.PP_CANT, paramTable);
			
			if(containsProductOperation(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO)){
				flagSCR=true;
				addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getMontoMaxS(),ServerConstantsCredit.PP, paramTable);
				addParametroObligatorio(getProducto(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO).getIndSim(),ServerConstantsCredit.SAP, paramTable);
			}else{
				addParametroObligatorio("",ServerConstantsCredit.PP, paramTable);
				addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAP, paramTable);
			}
		}else{
			//addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SPPIL, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.PP_DESC, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.PP_CANT, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.PP, paramTable);
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAP, paramTable);
		}
		
		if(containsProductCreditoContratado(ConstantsCredit.CREDITO_HIPOTECARIO)){
		//	addParametroObligatorio(getCreditoContratado(ConstantsCredit.CREDITO_HIPOTECARIO).getIndicadorSim(),ServerConstantsCredit.SCHL, paramTable);
			//addParametroObligatorio(ServerConstantsCredit.CH_DESC_TITLE,ServerConstantsCredit.CH_DESC, paramTable);
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.CREDITO_HIPOTECARIO).getPagoMen(),ServerConstantsCredit.CH_CANT, paramTable);
			
			if(containsProductOperation(ConstantsCredit.CREDITO_HIPOTECARIO)){
				flagSCR=true;
				addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getMontoMaxS(),ServerConstantsCredit.CH, paramTable);
				addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_HIPOTECARIO).getIndSim(),ServerConstantsCredit.SAH, paramTable);
			}else{
				addParametroObligatorio("",ServerConstantsCredit.CH, paramTable);
				addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAH, paramTable);
			}
		}else{
			//addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCHL, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.CH_DESC, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.CH_CANT, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CH, paramTable);
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAH, paramTable);
		}

		if(containsProductCreditoContratado(ConstantsCredit.CREDITO_AUTO)){
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.CREDITO_AUTO).getIndicadorSim(),ServerConstantsCredit.SCAL, paramTable);
			//addParametroObligatorio(ServerConstantsCredit.CA_DESC_TITLE,ServerConstantsCredit.CA_DESC, paramTable);
			//addParametroObligatorio(getCreditoContratado(ConstantsCredit.CREDITO_AUTO).getPagoMen(),ServerConstantsCredit.CA_CANT, paramTable);
			
			if(containsProductOperation(ConstantsCredit.CREDITO_AUTO)){
				flagSCR=true;
				addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getMontoMaxS(),ServerConstantsCredit.CA, paramTable);
				addParametroObligatorio(getProducto(ConstantsCredit.CREDITO_AUTO).getIndSim(),ServerConstantsCredit.SAA, paramTable);
			}else{
				addParametroObligatorio("",ServerConstantsCredit.CA, paramTable);
				addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAA, paramTable);
			}
		}else{
			//addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCAL, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.CA_DESC, paramTable);
			//addParametroObligatorio("",ServerConstantsCredit.CA_CANT, paramTable);
			addParametroObligatorio("",ServerConstantsCredit.CA, paramTable);
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAA, paramTable);
		}
		
		
		//New Region || Created by: OOS; May 6th,2015.
		
		
		Iterator<CreditoContratado> it = data.getCreditosContratados().iterator();
		String tituloCredito;
		int counterAux=0;
		
		
		while(it.hasNext()){
			CreditoContratado pr = it.next();

			if(pr.getCveProd().equals(ConstantsCredit.CREDITO_NOMINA))
				tituloCredito=ServerConstantsCredit.CN_DESC_TITLE;
			else if (pr.getCveProd().equals(ConstantsCredit.PRESTAMO_PERSONAL_INMEDIATO))
				tituloCredito=ServerConstantsCredit.PP_DESC_TITLE;
			else if (pr.getCveProd().equals(ConstantsCredit.CREDITO_HIPOTECARIO))
				tituloCredito=ServerConstantsCredit.CH_DESC_TITLE;
			else
				tituloCredito=ServerConstantsCredit.CA_DESC_TITLE;

			switch(counterAux)
			{
			case 0:
				addParametroObligatorio(pr.getIndicadorSim(),ServerConstantsCredit.SCNL, paramTable);
				addParametroObligatorio(tituloCredito,ServerConstantsCredit.CN_DESC, paramTable);
				addParametroObligatorio(pr.getPagoMen(),ServerConstantsCredit.CN_CANT, paramTable);
				break;
			case 1:
				addParametroObligatorio(pr.getIndicadorSim(),ServerConstantsCredit.SPPIL, paramTable);
				addParametroObligatorio(tituloCredito,ServerConstantsCredit.PP_DESC, paramTable);
				addParametroObligatorio(pr.getPagoMen(),ServerConstantsCredit.PP_CANT, paramTable);
				break;
			case 2:
				addParametroObligatorio(pr.getIndicadorSim(),ServerConstantsCredit.SCHL, paramTable);
				addParametroObligatorio(tituloCredito,ServerConstantsCredit.CH_DESC, paramTable);
				addParametroObligatorio(pr.getPagoMen(),ServerConstantsCredit.CH_CANT, paramTable);
				break;
			case 3:
				addParametroObligatorio(pr.getIndicadorSim(),ServerConstantsCredit.SCAL, paramTable);
				addParametroObligatorio(tituloCredito,ServerConstantsCredit.CA_DESC, paramTable);
				addParametroObligatorio(pr.getPagoMen(),ServerConstantsCredit.CA_CANT, paramTable);
				break;
			case 4:
				addParametroObligatorio(pr.getIndicadorSim(),ServerConstantsCredit.SP5, paramTable);
				addParametroObligatorio(tituloCredito,ServerConstantsCredit.P5_DESC, paramTable);
				addParametroObligatorio(pr.getPagoMen(),ServerConstantsCredit.P5_CANT, paramTable);
				break;
			case 5:
				addParametroObligatorio(pr.getIndicadorSim(),ServerConstantsCredit.SP6, paramTable);
				addParametroObligatorio(tituloCredito,ServerConstantsCredit.P6_DESC, paramTable);
				addParametroObligatorio(pr.getPagoMen(),ServerConstantsCredit.P6_CANT, paramTable);
				break;
			}	
			counterAux++;
		}
		
		if(counterAux<5)
		{
			do
			{
				switch(counterAux)
				{
				case 0:
					addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCNL, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.CN_DESC, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.CN_CANT, paramTable);
					break;
				case 1:
					addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SPPIL, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.PP_DESC, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.PP_CANT, paramTable);
					break;
				case 2:
					addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCHL, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.CH_DESC, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.CH_CANT, paramTable);
					break;
				case 3:
					addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCAL, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.CA_DESC, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.CA_CANT, paramTable);
					break;
				case 4:
					addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SP5, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.P5_DESC, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.P5_CANT, paramTable);
					break;
				case 5:
					addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SP6, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.P6_DESC, paramTable);
					addParametroObligatorio("",ServerConstantsCredit.P6_CANT, paramTable);
					break;
				}		
				counterAux++;
			}while(counterAux<6);
		}
	   //End Region
		
		
		
		if(containsProductOperation(ConstantsCredit.INCREMENTO_LINEA_CREDITO)){
			flagSCR=true;
			addParametroObligatorio(getProducto(ConstantsCredit.INCREMENTO_LINEA_CREDITO).getMontoMaxS(),ServerConstantsCredit.IL, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.INCREMENTO_LINEA_CREDITO).getIndSim(),ServerConstantsCredit.SAI, paramTable);
		}else{
			addParametroObligatorio("",ServerConstantsCredit.IL, paramTable);
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAI, paramTable);
		}
		
		if(containsProductOperation(ConstantsCredit.TARJETA_CREDITO)){
			flagSCR=true;
			addParametroObligatorio(getProducto(ConstantsCredit.TARJETA_CREDITO).getMontoMaxS(),ServerConstantsCredit.TC, paramTable);
			addParametroObligatorio(getProducto(ConstantsCredit.TARJETA_CREDITO).getIndSim(),ServerConstantsCredit.SAT, paramTable);
		}else{
			addParametroObligatorio("",ServerConstantsCredit.TC, paramTable);
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SAT, paramTable);
		}

		
		//Modified May 6th,2015.
		if(flagSCR)
			addParametroObligatorio(ServerConstantsCredit.IND_S,ServerConstantsCredit.SCR, paramTable);
		else
			addParametroObligatorio(ServerConstantsCredit.IND_N,ServerConstantsCredit.SCR, paramTable);
		//End Region.
		
		//YA NO SON NECESARIOS
		//addParametroObligatorio("",ServerConstantsCredit.LIQ_CAT, paramTable);
		//addParametroObligatorio("",ServerConstantsCredit.LIQ_FECHA_CALCULO, paramTable);

		addParametroObligatorio("L",ServerConstantsCredit.IND_CORREO, paramTable);
		addParametroObligatorio(session.getEmail(),ServerConstantsCredit.email, paramTable);

		Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.envioCorreo(paramTable);
		this.doNetworkOperation(getCodigoOperacion(), paramTable2, true, new EnvioCorreoData(), MainController.getInstance().getContext());
	}
	
	public void getCorreo(ResumenActivity act){
		// Establecer activity controladora
		this.act = act;
		// Consulta para obtener de correo
		consultaCorreo();
	}
	
	public void consultaCorreo(){
		
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		
		// Mapeamos el codigo de operacion
		addParametroObligatorio(this.getCodigoOperacion(),ServerConstantsCredit.OPERACION_PARAM, paramTable);
				
		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);
				
		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);
				
		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

		Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.consultaCorreo(paramTable);
		this.doNetworkOperation(Server.CONSULTA_CORREO_OPERACION, paramTable2, true, new ConsultaCorreoData(), MainController.getInstance().getContext());
	}
	
	
	public void sendEmail(ResumenActivity act,String email){
		if(validaEmail(email))
		{
			// Bloqueamos la pantalla
			MainController.getInstance().muestraIndicadorActividad("operacion", "conectando");
			this.act = act;
			sendSecond = true;
			//agm
			session.setEmail(email);
			//
			if(act.isSimulaOfertados())
				sendEmailContratacion();
			else if (act.isSimulaContratados())
				sendEmailLiquidacion();
			else
				MainController.getInstance().ocultaIndicadorActividad();

		}else{
			Log.d("validaEmail", "Email no v�lido");
		}
	}
	
	/****/
	
	public void analyzeResponse(String operationId, ServerResponseCredit response) {
    	
    	if(getCodigoOperacion().equals(operationId)){
    		if((response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL)){

				if((act.isSimulaContratados() && !act.isSimulaOfertados()) ||
						(!act.isSimulaContratados() && act.isSimulaOfertados()) ||
						!sendSecond){
        			// Desbloqueamos la pantalla
        			MainController.getInstance().ocultaIndicadorActividad();
    				act.showInformationAlert("Su mensaje ha sido enviado con �xito");
    			}

				if(sendSecond && act.isSimulaContratados()){
    				sendEmailLiquidacion();
    			}
    			
    			sendSecond = false;
    		}
    	}else if(Server.CONSULTA_CORREO_OPERACION.equals(operationId))
    	{
    		if((response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL)){
    			ConsultaCorreoData data = (ConsultaCorreoData) response.getResponse();	
    			// En caso de email vacio muestra el alert vacio
    			if(data.getCorreoclie().equals("")){
    				act.showMailAlert("", act);
    			}else{
    				// El correo no es vacio, se mete en sesion y se muestra
    				session.setEmail(data.getCorreoclie());
        			act.showMailAlert(session.getEmail(), act);
    			}
    		}else{
    			// En caso de error se muestra el alert de email vacio
    			//act.showMailAlert("", act);
    		}
    	//Modificacion 50986
    	}else if(getCodigoOperacionGuardar().equals(operationId))
		{
			if(response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL){
				if(isConsumo)
				   actListaResumen.consumoOneClick();
				else
					actListaResumen.oneClickILC();
			}
		}
	}

	public <T> void redirectToView(Class<T> c){
		MainController.getInstance().showScreen(c);
	}

	@Override
	protected String getCodigoOperacion() {
		// TODO Auto-generated method stub
		return Server.ENVIO_CORREO_OPERACION;
	}

	//Modificacion 50986
	public void saveSimulationRequest(ListaDatosResumenController actListaResumen, boolean isConsumo )
	{   // Mapeamos el activity
		this.actListaResumen = actListaResumen;
		this.isConsumo = isConsumo;

		// Mapeamos el usuario y la contrase�a
		Hashtable<String, String> paramTable = new Hashtable<String, String>();

		// Mapeamos el codigo de operacion
		addParametroObligatorio(this.getCodigoOperacionGuardar(),ServerConstantsCredit.OPERACION_PARAM, paramTable);

		// Mapeamos el id de cliente tomado de la sesion
		addParametroObligatorio(session.getIdUsuario(),ServerConstantsCredit.CLIENTE_PARAM, paramTable);

		// Mapeamos el IUM tomado de la sesion
		addParametroObligatorio(session.getIum(),ServerConstantsCredit.IUM_PARAM, paramTable);

		// Mapeamos el numeroCelular tomado de la sesion
		addParametroObligatorio(session.getNumCelular(),ServerConstantsCredit.NUMERO_CEL_PARAM, paramTable);

		// Mapeamos el tipo de operacion -> 3
		addParametroObligatorio(ConstantsCredit.OP_GUARDAR_ALTERNATIVAS, ServerConstantsCredit.TIPO_OP_PARAM,paramTable);

		Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.guardarEliminarSimulacion(paramTable);
		this.doNetworkOperation(getCodigoOperacionGuardar(), paramTable2, true, new CalculoData(), MainController.getInstance().getContext());
	}


	//Modificacion 50986
	protected String getCodigoOperacionGuardar()
	{
		return Server.CALCULO_ALTERNATIVAS_OPERACION;
	}

}
