package suitebancomer.aplicaciones.bbvacredit.models;

public class Campania {
	private String claveCampania;
	private String descripcionOferta;
	private String monto;
	
	public Campania(String claveCampania, String descripcionOferta, String monto) {
		this.claveCampania = claveCampania;
		this.descripcionOferta = descripcionOferta;
		this.monto = monto;
	}

	public String getClaveCampania() {
		return claveCampania;
	}

	public void setClaveCampania(String claveCampania) {
		this.claveCampania = claveCampania;
	}

	public String getDescripcionOferta() {
		return descripcionOferta;
	}

	public void setDescripcionOferta(String descripcionOferta) {
		this.descripcionOferta = descripcionOferta;
	}

	public String getMonto() {
		return monto;
	}

	public void setMonto(String monto) {
		this.monto = monto;
	}
}
