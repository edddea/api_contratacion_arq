package suitebancomer.aplicaciones.bbvacredit.models;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.me.JSONAble;

import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.io.ParsingHandlerJSONCredit;
import android.util.Log;

public class EnvioCorreoData implements ParsingHandlerJSONCredit, JSONAble{
	
	private String estado;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public void fromJSON(String jsonString) {
		// TODO Auto-generated method stub
		try{
			JSONObject obj = new JSONObject(jsonString);
		
			this.estado = Tools.getJSONChecked(obj,"estado", String.class); //obj.getString("estado");
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			Log.e(this.getClass().toString(), "Error en fromJSON");
			e.printStackTrace();
		}
	}

	@Override
	public String toJSON() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void processJSON(String jsonString) throws JSONException {
		// TODO Auto-generated method stub
		JSONObject obj = new JSONObject(jsonString);
		
		this.estado = Tools.getJSONChecked(obj,"estado", String.class); //obj.getString("estado");
	}

}
