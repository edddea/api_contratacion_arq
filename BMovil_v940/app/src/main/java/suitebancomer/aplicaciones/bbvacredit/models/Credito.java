package suitebancomer.aplicaciones.bbvacredit.models;

public class Credito {
	
	private String contrato;

	public Credito() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Credito(String contrato) {
		super();
		this.contrato = contrato;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}
	
}
