package suitebancomer.aplicaciones.bbvacredit.models;

public class Cuenta {
	
	private String tipo;
	
	private String alias;
	
	private String divisa;
	
	private String asunto;
	
	private String importe;
	
	private String concepto;
	
	private String visivble;
	
	private String ocurrencia;

	public Cuenta(String tipo, String alias, String divisa, String asunto,
			String importe, String concepto, String visivble) {
		super();
		this.tipo = tipo;
		this.alias = alias;
		this.divisa = divisa;
		this.asunto = asunto;
		this.importe = importe;
		this.concepto = concepto;
		this.visivble = visivble;
	}

	public Cuenta() {
		// TODO Auto-generated constructor stub
	}

	public String getOcurrencia() {
		return ocurrencia;
	}

	public void setOcurrencia(String ocurrencia) {
		this.ocurrencia = ocurrencia;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getDivisa() {
		return divisa;
	}

	public void setDivisa(String divisa) {
		this.divisa = divisa;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getVisivble() {
		return visivble;
	}

	public void setVisivble(String visivble) {
		this.visivble = visivble;
	}
	
	
}
