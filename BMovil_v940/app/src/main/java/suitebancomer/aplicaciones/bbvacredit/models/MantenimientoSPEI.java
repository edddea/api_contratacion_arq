package suitebancomer.aplicaciones.bbvacredit.models;

public class MantenimientoSPEI {

	private String nombreComp;
	
	private String nombreLogo;
	
	private String claveComp;
	
	private String orden;

	public MantenimientoSPEI(String nombreComp, String nombreLogo,
			String claveComp, String orden) {
		super();
		this.nombreComp = nombreComp;
		this.nombreLogo = nombreLogo;
		this.claveComp = claveComp;
		this.orden = orden;
	}

	public MantenimientoSPEI() {
		// TODO Auto-generated constructor stub
	}

	public String getNombreComp() {
		return nombreComp;
	}

	public void setNombreComp(String nombreComp) {
		this.nombreComp = nombreComp;
	}

	public String getNombreLogo() {
		return nombreLogo;
	}

	public void setNombreLogo(String nombreLogo) {
		this.nombreLogo = nombreLogo;
	}

	public String getClaveComp() {
		return claveComp;
	}

	public void setClaveComp(String claveComp) {
		this.claveComp = claveComp;
	}

	public String getOrden() {
		return orden;
	}

	public void setOrden(String orden) {
		this.orden = orden;
	}
	
}
