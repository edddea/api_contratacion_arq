package suitebancomer.aplicaciones.bbvacredit.models;

public class JsonHorarioOperacion {
	
	private String spei;
	
	private String servicios;
	
	private String speiCelular;

	public JsonHorarioOperacion(String spei, String servicios,
			String speiCelular) {
		super();
		this.spei = spei;
		this.servicios = servicios;
		this.speiCelular = speiCelular;
	}

	public String getSpei() {
		return spei;
	}

	public void setSpei(String spei) {
		this.spei = spei;
	}

	public String getServicios() {
		return servicios;
	}

	public void setServicios(String servicios) {
		this.servicios = servicios;
	}

	public String getSpeiCelular() {
		return speiCelular;
	}

	public void setSpeiCelular(String speiCelular) {
		this.speiCelular = speiCelular;
	}

}
