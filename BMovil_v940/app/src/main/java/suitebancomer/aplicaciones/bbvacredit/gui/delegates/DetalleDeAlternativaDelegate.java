package suitebancomer.aplicaciones.bbvacredit.gui.delegates;
/** Created: April 14th, 2015. Author:OOS.*/    	

import android.util.Log;
import android.content.DialogInterface;

import com.bancomer.mbanking.SuiteApp;
import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.common.Tools;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.io.AuxConectionFactoryCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerResponseCredit;
import suitebancomer.aplicaciones.bbvacredit.models.ConsumoCredit;
import suitebancomer.aplicaciones.bbvacredit.models.DetalleAlternativa;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ContratoConsumoViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;

public class DetalleDeAlternativaDelegate extends BaseDelegateOperacion {
	
	private String cliente;
	private String ium;
	private String numeroCelular;
	private String producto;
	
	
	private OfertaILC oferta;
	private Promociones promocion;
	private OfertaConsumo ofertaConsumo;
	
	private ContratoConsumoViewController controladorContratoConsumoView;
	
	/**
	 * Default constructor
	 */
	public DetalleDeAlternativaDelegate(String cliente, String ium, String numeroCelular, String producto) {
		this.cliente=cliente;
		this.ium=ium;
		this.numeroCelular=numeroCelular;
		this.producto=producto;
	}
	
	@Override
	protected String getCodigoOperacion() {
		return Server.DETALLE_ALTERNATIVA;
	}
	
	
	public OfertaILC getOferta()
	{
		return this.oferta;
	}
		
	
	
	public Promociones getPromocion()
	{
		return this.promocion;
	}
	
	
	
	public void setOferta(OfertaILC oferta)
	{
		this.oferta=oferta;
	}	
	

	public void setPromocion(Promociones promocion)
	{
		this.promocion=promocion;
	}
	
	
	
	public OfertaConsumo getOfertaConsumo()
	{
		return this.ofertaConsumo;
	}
	
	
	public void setOfertaConsumo(OfertaConsumo ofertaConsumo)
	{
		this.ofertaConsumo=ofertaConsumo;
	}
	
	
	
	
	
		
	public void consultaDetalleAlternativasTask(){
		
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		
		paramTable.put(ServerConstantsCredit.OPERACION_PARAM, getCodigoOperacion());
		paramTable.put(ServerConstantsCredit.CLIENTE_PARAM, cliente);
		paramTable.put(ServerConstantsCredit.IUM_PARAM, ium);
		paramTable.put(ServerConstantsCredit.NUMERO_CEL_PARAM, numeroCelular);
		paramTable.put(ServerConstantsCredit.PRODUCTO_PARAM, producto);

		Hashtable<String, String> paramTable2  =AuxConectionFactoryCredit.consultaDetalleAlternativa(paramTable);
		this.doNetworkOperation(getCodigoOperacion(), paramTable2,true,new DetalleAlternativa(),MainController.getInstance().getContext());
	}
	
    
    public void analyzeResponse(String operationId, ServerResponseCredit response) {

		/** Modified: April 22th, 2015. Author:OOS.*/
		/** Modificacion 50986.*/
		if (Server.DETALLE_ALTERNATIVA.equals(operationId)) {
			if ((response.getStatus() == ServerResponseCredit.OPERATION_SUCCESSFUL) || (response.getStatus() == ServerResponseCredit.OPERATION_OPTIONAL_UPDATE)) {
				DetalleAlternativa data = (DetalleAlternativa) response.getResponse();

				promocion = new Promociones();
				promocion.setCveCamp(data.getIdCamCRM());
				//promocion.setMonto(Tools.formatterForBmovil(data.getMonDese()));
				promocion.setDesOferta("");

				//String numCelular=Session.getInstance().getNumCelular();

				if (producto.equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)) {
					oferta = new OfertaILC();
					promocion.setMonto(data.getMonDese());
					oferta.setCat(data.getCAT());
					oferta.setFechaCat(data.getFechCat());

					oferta.setImporte((int) (Float.parseFloat(data.getMonDese())) + "");
					oferta.setLineaActual((int) (Float.parseFloat(data.getLinCredAct())) + "");
					oferta.setLineaFinal((int) (Float.parseFloat(data.getLinCredFinal())) + "");
					oferta.setContrato("");

					Account cuenta = new Account();
					cuenta.setNumber(data.getCuenta());
					cuenta.setAlias("");
					oferta.setAccount(cuenta);

    				iniciarFlujoOneClickILC();
    				//MainController.getInstance().getMenuSuiteController().delegate.realizaConsumoILC(Server.CONSULTA_DETALLE_OFERTA, MainController.getInstance().getMenuSuiteController(), numCelular, promocion.getCveCamp(),this);   	
    			}
    			else
    			{
					ofertaConsumo = new OfertaConsumo();

					ofertaConsumo.setImporte(data.getImporte());
					ofertaConsumo.setPlazoDes(data.getPlazoDes());
					String bscPagar = new String(data.getBscPagar());
					ofertaConsumo.setTipoSeg(bscPagar.length() > 0 ? bscPagar.substring(0, 16) : "");
					ofertaConsumo.setTotalPagos(data.getTotalPagos());
					//ofertaConsumo.setDescDiasPago(ConstantsCredit.DESCDIASPAGO);
					ofertaConsumo.setDescDiasPago(data.getDescDiasPago());
					//ofertaConsumo.setFolioUG(data.getContrato());
					//ofertaConsumo.setFolioUG("");
					ofertaConsumo.setFolioUG(data.getFolioUG());
					//ofertaConsumo.setProducto(data.getCveProd());
					ofertaConsumo.setProducto(data.getProducto());
					ofertaConsumo.setCat(data.getCAT());
					ofertaConsumo.setCuentaVinc(data.getCtaVinc());
					//ofertaConsumo.setTasaAnual(data.getTasa());
					//ofertaConsumo.setTasaMensual(data.getTasa());
					ofertaConsumo.setTasaAnual(data.getTasaAnual());
					ofertaConsumo.setTasaMensual(data.getTasaMensual());
					//ofertaConsumo.setPagoMenFijo(Tools.formatterForBmovil(data.getPagMens()));
					ofertaConsumo.setPagoMenFijo(data.getPagoMenFijo());
					//if(data.getFechCat()!=null && !data.getFechCat().equals(""))
					ofertaConsumo.setFechaCat(data.getFechCat());
					//else
					//	ofertaConsumo.setFechaCat(data.getFechSol());
					//ofertaConsumo.setPlazo(data.getCvePlazo());
					ofertaConsumo.setPlazo(data.getPlazo());
					//ofertaConsumo.setEstatusOferta(BmovilConstants.PREABPROBADO);
					ofertaConsumo.setEstatusOferta(data.getEstatusOferta());
					//setValuesFromJson(data.getCveSubp());
					ofertaConsumo.setProducto(data.getProducto());
					ofertaConsumo.setImpSegSal(data.getImpSegSal());

					iniciarFlujoOneClickConsumo();
					//MainController.getInstance().getMenuSuiteController().delegate.realizaCONSUMO(Server.CONSULTA_DETALLE_OFERTA, MainController.getInstance().getMenuSuiteController(), numCelular, promocion.getCveCamp(),this);

				}

			} else {
				MainController.getInstance().ocultaIndicadorActividad();
			}
		} else if (Server.CONTRATA_ALTERNATIVA_CONSUMO.equals(operationId)) {
			AceptaOfertaConsumo aceptaOferta;
			controladorContratoConsumoView.ocultaIndicadorActividad();
			if(response.getStatus() == ServerResponse.OPERATION_ERROR){
				aceptaOferta= (AceptaOfertaConsumo)response.getResponse();
				if(aceptaOferta.getPromociones()!=null){
					Session session = Session.getInstance(SuiteApp.appContext);
					session.setPromocion(aceptaOferta.getPromociones());
					controladorContratoConsumoView.showInformationAlert("Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {
									((BmovilViewsController) controladorContratoConsumoView.getParentViewsController()).showMenuPrincipal();
								}
							});
				}else{

					controladorContratoConsumoView.showInformationAlert("Aviso", response.getMessageCode() + "\n" + response.getMessageText(),
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {
								}
							});

				}
			}else{
				aceptaOferta= (AceptaOfertaConsumo)response.getResponse();
				Session session = Session.getInstance(SuiteApp.appContext);
				session.setPromocion(aceptaOferta.getPromociones());
				if(ofertaConsumo.getEstatusOferta().equals(Constants.PREFORMALIZADO)){
					((BmovilViewsController) controladorContratoConsumoView.getParentViewsController()).showExitoConsumo(aceptaOferta, ofertaConsumo);//pantalla exito
				}else if(ofertaConsumo.getEstatusOferta().equals(Constants.PREABPROBADO)){
					DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int arg1) {
							dialog.dismiss();
							((BmovilViewsController) controladorContratoConsumoView.getParentViewsController()).showMenuPrincipal();
						}
					};
					if(aceptaOferta.getScoreSal().equals("RE")){
						controladorContratoConsumoView.showInformationAlert("Su crédito fue rechazado por buró.", listener);

					}else if(aceptaOferta.getScoreSal().equals("DI")){
						controladorContratoConsumoView.showInformationAlert("Su crédito no fue autorizado, favor de acudir a sucursal.", listener);

					}else if(aceptaOferta.getScoreSal().equals("AP")||aceptaOferta.getScoreSal().equals("NB")){
						((BmovilViewsController) controladorContratoConsumoView.getParentViewsController()).showExitoConsumo(aceptaOferta,ofertaConsumo);//pantalla exito
					}
				}
			}
		}
	}
    
    
    
    private void iniciarFlujoOneClickILC()
    {
		if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session
			MainController.getInstance().getStartBmovilInBack().ofertaILC(oferta, promocion);
		}else{
    		MainController.getInstance().getMenuSuiteController().ofertaILC(oferta, promocion);
		}
    }
    
    
    private void iniciarFlujoOneClickConsumo()
    {
		if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session
			MainController.getInstance().getStartBmovilInBack().ofertaConsumo(ofertaConsumo, promocion);
		}else{
    		MainController.getInstance().getMenuSuiteController().ofertaConsumo(ofertaConsumo, promocion);
		}
    }
    
    /**** Modificacion 50986.
	 * El siguiente metodo ya no es necesario ya que en la respuesta de DetalleAlternativa
	 * ya vienen estipulados los datos que se obtenian con este metodo.

    private void setValuesFromJson(String cveSubP)
    {
    	// Created: April 22th, 2015. Author:OOS.
    	
    	ArrayList<ConsumoCredit> items = Tools.getJsonCreditParsed();
    	ConsumoCredit auxInstance=null;
    	
    	for(int i=0;i<items.size();i++)
    	{
    		auxInstance = items.get(i);
    		if(auxInstance.getCveSubP().equals(cveSubP))
    			break;
    	}
    	
    	if(auxInstance!=null)
    	{
    		ofertaConsumo.setProducto(auxInstance.getProducto());
        	ofertaConsumo.setImpSegSal(Tools.getImporteDelSeguro(auxInstance.getPagMilS(), ofertaConsumo.getImporte()));
        	
    	}


    }*/

	public void contrataAlternativa(Hashtable<String,String> map){

		Hashtable<String, String> paramTable2  = AuxConectionFactoryCredit.contrataAlternativaConsumo(map);
		doNetworkOperation(Server.CONTRATA_ALTERNATIVA_CONSUMO,paramTable2,true,new AceptaOfertaConsumo(),MainController.getInstance().getContext());
	}

	public ContratoConsumoViewController getControladorContratoView() {
		return controladorContratoConsumoView;
	}

	public void setControladorContratoView(ContratoConsumoViewController controladorContratoView) {
		this.controladorContratoConsumoView = controladorContratoView;
	}
}