package suitebancomer.aplicaciones.bbvacredit.io;

import android.content.Context;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.me.JSONAble;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Map;
import java.util.Objects;

import bancomer.api.apiventatdc.io.ParserImpl;
import bancomer.api.apiventatdc.io.ParserJSONImpl;
import bancomer.api.apiventatdc.io.Server;
import bancomer.api.apiventatdc.io.ServerResponseImpl;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;
import suitebancomer.aplicaciones.commservice.service.ICommService;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public class ConnectionFactoryCredit {

    private int operationId;
    private Hashtable<String, ?> params;
    private Object responseObject;
    private ParametersTO parameters;
    private boolean isJson;
    private Context context;
    private ApiConstants.isJsonValueCode isJsonValueCode;

    public ConnectionFactoryCredit(final int operationId, final Hashtable<String, ?> params,
                                   final boolean isJson, final Object responseObject, Context context, final ApiConstants.isJsonValueCode isJsonValueCode) {
        super();
        this.operationId = operationId;
        this.params = params;
        this.responseObject = responseObject;
        this.isJson = isJson;
        this.isJsonValueCode = isJsonValueCode;
        this.context=context;
    }

    /**
     * realiza la peticon a server
     * @return
     * @throws Exception
     */
    public IResponseService processConnectionWithParams() throws Exception {

        final Integer opId = Integer.valueOf(operationId);
        parameters = new ParametersTO();
        parameters.setSimulation(ServerCommons.SIMULATION);
        if (!ServerCommons.SIMULATION) {
            parameters.setProduction(!ServerCommons.DEVELOPMENT);
            parameters.setDevelopment(ServerCommons.DEVELOPMENT);
        }
        parameters.setOperationId(opId.intValue());
        final Hashtable<String, String> mapa= new Hashtable<String, String>();
        for (final Map.Entry<String, ?> entry : params.entrySet()) {
            final String key = entry.getKey();
            final Object value = entry.getValue();
            mapa.put(key, (String) (value==null?"":value));
        }
        //
        parameters.setParameters(mapa);
        parameters.setJson(isJson);
        parameters.setIsJsonValue(this.isJsonValueCode);
        IResponseService resultado = new ResponseServiceImpl();
        final ICommService serverproxy = new CommServiceProxy(context);
        try {
            resultado = serverproxy.request(parameters,
                    responseObject == null ? new Object().getClass()
                            : responseObject.getClass());
        } catch (UnsupportedEncodingException e) {
            throw new Exception(e);
        } catch (ClientProtocolException e) {
            throw new Exception(e);
        } catch (IllegalStateException e) {
            throw new Exception(e);
        } catch (IOException e) {
            throw new Exception(e);
        } catch (JSONException e) {
            throw new Exception(e);
        }

        return resultado;

    }

    /**
     * convierte la respuesta del server del tipo IResponseService
     * a uns ServerResponseImpl originario del api
     * @param resultado
     * @param handler
     * @return
     * @throws Exception
     */
    public ServerResponseCredit parserConnection(final IResponseService resultado,
                                           final Object handler) throws Exception {
        ServerResponseCredit response=null;
        ParsingHandlerJSONCredit handlerP=null;
        suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler handlerPOld=null;
        //si la peticon es por ParsingHandler
        if(resultado.getObjResponse() instanceof suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler){
            if (ApiConstants.OPERATION_SUCCESSFUL == resultado.getStatus()) {
                if(resultado.getObjResponse() instanceof ParsingHandler){
                    handlerPOld= (suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler) resultado.getObjResponse();
                }
                if (parameters.isJson()) {
                    if (handler != null) {
                        try {
                            if(handlerPOld!=null){
                                handlerPOld.process(new suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON(resultado.getResponseString()));
                            }else{
                                handlerPOld=(suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler) handler.getClass().newInstance();
                                handlerPOld.process(new suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON(resultado.getResponseString()));
                            }
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            throw new Exception(e);
                        }
                    }
                    response = new ServerResponseCredit(resultado.getStatus(),
                            resultado.getMessageCode(), resultado.getMessageText(), handlerPOld);
                }

            }
        }else if (ApiConstants.OPERATION_SUCCESSFUL == resultado.getStatus()) {

            if(resultado.getObjResponse() instanceof ParsingHandlerJSONCredit){
                handlerP= (ParsingHandlerJSONCredit) resultado.getObjResponse();

                if (parameters.isJson() ) {
                    if (handler != null) {
                        try {
                            if(handlerP==null){
                                handlerP=(ParsingHandlerJSONCredit)handler;
                            }
                            ((JSONAble)handlerP).fromJSON(resultado.getResponseString());
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                            throw new Exception(e);
                        }
                    }
                    response = new ServerResponseCredit(resultado.getStatus(),
                            resultado.getMessageCode(), resultado.getMessageText(), handlerP);
                }
            }
		}else{
			try {
				handlerP=null;
                handlerPOld=null;
                response= new ServerResponseCredit(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),null);
			} catch (Exception e) {
				// TODO: handle exception
				throw new Exception(e);
			}

		}

		return response;
	}

}
