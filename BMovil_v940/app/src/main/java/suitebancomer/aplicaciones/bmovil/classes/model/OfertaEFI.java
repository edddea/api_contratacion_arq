package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;

public class OfertaEFI implements ParsingHandler{
	
	String importe;
	String fecFinOferta;
	String comisionEFIPorcentaje;
	String comisionEFIMonto;
	String plazoMeses;
	String tasaAnual;
	//String montoUtilizado;
	String montoDisponible;
	String montoDisponibleFijo; //Nuevo monto
	//String montoDisposicion;
	//String pagoMensual;
	String pagoMensualSimulador;
	String disponibleTDC;
	String celula;
	String edicion;
	Account accountDestino= new Account();
	Account accountOrigen= new Account();
	String numContrato;
	String fechaCat;
	String cat;
	String montoMaximo;
	Account accountTipoCuenta= new Account(); //Tipo de cuenta
	public ArrayList<Account> accountEFI=new ArrayList<Account>();
	
	
//Tipo de cuenta
	public Account getTipoCuenta() {
		return accountTipoCuenta;
	}


	public void setTipoCuenta(Account tipocuenta) {
		this.accountTipoCuenta = tipocuenta;
	}

//End Tipo de cuenta	
	
	
	
	public String getImporte() {
		return importe;
	}



	public void setImporte(String importe) {
		this.importe = importe;
	}



	public String getFecFinOferta() {
		return fecFinOferta;
	}



	public void setFecFinOferta(String fecFinOferta) {
		this.fecFinOferta = fecFinOferta;
	}



	public String getComisionEFIPorcentaje() {
		return comisionEFIPorcentaje;
	}



	public void setComisionEFIPorcentaje(String comisionEFIPorcentaje) {
		this.comisionEFIPorcentaje = comisionEFIPorcentaje;
	}



	public String getComisionEFIMonto() {
		return comisionEFIMonto;
	}



	public void setComisionEFIMonto(String comisionEFIMonto) {
		this.comisionEFIMonto = comisionEFIMonto;
	}



	public String getPlazoMeses() {
		return plazoMeses;
	}



	public void setPlazoMeses(String plazoMeses) {
		this.plazoMeses = plazoMeses;
	}



	public String getTasaAnual() {
		return tasaAnual;
	}



	public void setTasaAnual(String tasaAnual) {
		this.tasaAnual = tasaAnual;
	}



	/*public String getMontoUtilizado() {
		return montoUtilizado;
	}



	public void setMontoUtilizado(String montoUtilizado) {
		this.montoUtilizado = montoUtilizado;
	}*/



	public String getMontoDisponible() {
		return montoDisponible;
	}



	public void setMontoDisponible(String montoDisponible) {
		this.montoDisponible = montoDisponible;
	}

	
	//Modificacion
	public String getMontoDisponibleFijo() {
		return montoDisponibleFijo;
	}



	public void setMontoDisponibleFijo(String montoDisponibleFijo) {
		this.montoDisponibleFijo = montoDisponibleFijo;
	}


	/*public String getMontoDisposicion() {
		return montoDisposicion;
	}



	public void setMontoDisposicion(String montoDisposicion) {
		this.montoDisposicion = montoDisposicion;
	}*/



	/*public String getPagoMensual() {
		return pagoMensual;
	}



	public void setPagoMensual(String pagoMensual) {
		this.pagoMensual = pagoMensual;
	}*/



	public String getPagoMensualSimulador() {
		return pagoMensualSimulador;
	}



	public void setPagoMensualSimulador(String pagoMensualSimulador) {
		this.pagoMensualSimulador = pagoMensualSimulador;
	}



	/*public String getMontoDispTDC() {
		return montoDispTDC;
	}



	public void setMontoDispTDC(String montoDispTDC) {
		this.montoDispTDC = montoDispTDC;
	}*/



	public String getCelula() {
		return celula;
	}



	public void setCelula(String celula) {
		this.celula = celula;
	}



	public String getEdicion() {
		return edicion;
	}



	public void setEdicion(String edicion) {
		this.edicion = edicion;
	}



	public Account getAccountDestino() {
		return accountDestino;
	}



	public void setAccountDestino(Account accountDestino) {
		this.accountDestino = accountDestino;
	}



	public Account getAccountOrigen() {
		return accountOrigen;
	}



	public void setAccountOrigen(Account accountOrigen) {
		this.accountOrigen = accountOrigen;
	}



	public String getNumContrato() {
		return numContrato;
	}



	public void setNumContrato(String numContrato) {
		this.numContrato = numContrato;
	}



	public String getFechaCat() {
		return fechaCat;
	}



	public void setFechaCat(String fechaCat) {
		this.fechaCat = fechaCat;
	}



	public String getCat() {
		return cat;
	}



	public void setCat(String cat) {
		this.cat = cat;
	}
	
	

	public String getMontoMaximo() {
		return montoMaximo;
	}



	public void setMontoMaximo(String montoMaximo) {
		this.montoMaximo = montoMaximo;
	}



	public ArrayList<Account> getAccountEFI() {
		return accountEFI;
	}



	public void setAccountEFI(ArrayList<Account> accountEFI) {
		this.accountEFI = accountEFI;
	}



	public String getDisponibleTDC() {
		return disponibleTDC;
	}



	public void setDisponibleTDC(String disponibleTDC) {
		this.disponibleTDC = disponibleTDC;
	}



	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub}
		importe=parser.parseNextValue("importe");
		fecFinOferta=parser.parseNextValue("fecFinOferta");
		comisionEFIPorcentaje=parser.parseNextValue("comisionEFIPorcentaje");
		comisionEFIMonto=parser.parseNextValue("comisionEFIMonto");
		plazoMeses=parser.parseNextValue("plazoMeses");
		tasaAnual=parser.parseNextValue("tasaAnual");
		//montoUtilizado=parser.parseNextValue("montoUtilizado");
		montoDisponible=parser.parseNextValue("montoDisponible");
		montoMaximo=parser.parseNextValue("montoDisponible");
		montoDisponibleFijo=parser.parseNextValue("montoDisponible");
		//montoDisposicion=parser.parseNextValue("montoDisposicion");
		//pagoMensual=parser.parseNextValue("pagoMensual")+"00";
		pagoMensualSimulador=parser.parseNextValue("pagoMensualSimulador");
		disponibleTDC=parser.parseNextValue("disponibleTDC");
		celula=parser.parseNextValue("celula");
		edicion=parser.parseNextValue("edicion");
		fechaCat=parser.parseNextValue("fechaCat");
		cat=parser.parseNextValue("Cat");
		accountOrigen.setAlias(parser.parseNextValue("alias"));
		accountOrigen.setNumber(parser.parseNextValue("numeroTarjeta"));
		numContrato=parser.parseNextValue("numContrato");
	//	accountDestino.setNumber(parser.parseNextValue("cuentaEje"));
			
		accountEFI.add(accountOrigen);
	}



	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	} 

}
