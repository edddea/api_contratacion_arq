package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ConsultarEstatusEnvioEC implements ParsingHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<CuentaEC> cuentas=null;
	
	
	
	public ArrayList<CuentaEC> getCuentas() {
		return cuentas;
	}

	public void setCuentas(ArrayList<CuentaEC> cuentas) {
		this.cuentas = cuentas;
	}
	
	/**
	 * Default Construtor
	 */
	
	public ConsultarEstatusEnvioEC()
	{
		cuentas = new ArrayList<CuentaEC>();
	}
	

	private ArrayList<CuentaEC> parseCuentas(ParserJSON parser)throws IOException, ParsingException 
	{
		ArrayList<CuentaEC> objetos =  null;
		JSONObject root=null;
		JSONArray elements=null;
		JSONObject actual=null;
		CuentaEC auxiliar = null;
		
		root= parser.parserNextObject("arrCuentas");
		try {
			elements = root.getJSONArray("ocCuentas");
			objetos = new ArrayList<CuentaEC>();
			for(int i=0;i<elements.length();i++){
				actual = elements.getJSONObject(i);
				auxiliar = new CuentaEC();
				auxiliar.setEstadoEnvio(actual.getString("estatusEnvio"));
				auxiliar.setNumeroCuenta(actual.getString("numeroCuenta"));
				auxiliar.setFlag(false);
				objetos.add(auxiliar);
			}
		} catch (JSONException e) {
			Log.e(this.getClass().getSimpleName(), "No se pudo interpretar el json.", e);
			return null;
		}


		return objetos;
		
	}
	
	
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		
		this.cuentas = parseCuentas(parser);
	}

}
