package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class ConsultaInterbancariaResult implements ParsingHandler{

	ArrayList<DetalleInterbancaria> arrTransferenciaSPEI;
	
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	
	
	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		arrTransferenciaSPEI=new ArrayList<DetalleInterbancaria>();
		try{
			JSONArray arrTransferencias=parser.parseNextValueWithArray("transferencias");
			for(int i=0;i<arrTransferencias.length();i++){
				JSONObject jsonTransferencia=arrTransferencias.getJSONObject(i);
				DetalleInterbancaria transferenciaSPEI=new DetalleInterbancaria();
				transferenciaSPEI.setNombreBanco(jsonTransferencia.getString("nombreBanco"));
				transferenciaSPEI.setImporte(jsonTransferencia.getString("importe"));
				transferenciaSPEI.setFechaAplicacion(jsonTransferencia.getString("fechaAplicacion"));
				transferenciaSPEI.setIva(jsonTransferencia.getString("IVA"));
				transferenciaSPEI.setCuentaOrigen(jsonTransferencia.getString("cuentaOrigen"));
				transferenciaSPEI.setCuentaDestino(jsonTransferencia.getString("cuentaDestino"));
				transferenciaSPEI.setDescripcion(jsonTransferencia.getString("descripcion"));
				transferenciaSPEI.setTipoServicio(jsonTransferencia.getString("tipoServicio"));
				transferenciaSPEI.setReferencia(jsonTransferencia.getString("referencia"));
				transferenciaSPEI.setClaveRastreo(jsonTransferencia.getString("claveRastreo"));
				transferenciaSPEI.setFolio(jsonTransferencia.getString("Folio"));
				transferenciaSPEI.setEstatusOperacion(jsonTransferencia.getString("estatusOperacion"));
				transferenciaSPEI.setNombreBeneficiario(jsonTransferencia.getString("nombreBeneficiario"));
				transferenciaSPEI.setDetalleDevolicion(jsonTransferencia.getString("DetalleDevolucion"));
				transferenciaSPEI.setHoraDevolucion(jsonTransferencia.getString("horaDevolucion"));
				transferenciaSPEI.setHoraAcuse(jsonTransferencia.getString("horaAcuse"));
				transferenciaSPEI.setHoraLiquidacion(jsonTransferencia.getString("horaLiquidacion"));
				transferenciaSPEI.setHoraAprobacion(jsonTransferencia.getString("horaAprobacion"));
				transferenciaSPEI.setHoraAlta(jsonTransferencia.getString("horaAlta"));
				transferenciaSPEI.setFechaDevolucion(jsonTransferencia.getString("fechaDevolucion"));
				arrTransferenciaSPEI.add(transferenciaSPEI);
			}
		}catch(Exception e){
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
		}
	}



	public ArrayList<DetalleInterbancaria> getArrTransferenciaSPEI() {
		return arrTransferenciaSPEI;
	}



	public void setArrTransferenciaSPEI(
			ArrayList<DetalleInterbancaria> arrTransferenciaSPEI) {
		this.arrTransferenciaSPEI = arrTransferenciaSPEI;
	}

}
