package suitebancomer.aplicaciones.bmovil.classes.common;

import java.util.Hashtable;

import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class ServerMapperUtil {

	public static Hashtable<String, String> mapperBaja(Hashtable<String, String> params){
		Hashtable<String, String> parameter = new Hashtable<String, String>();
		parameter.put("NT",  params.get(Server.USERNAME_PARAM)!=null?(String) params.get(Server.USERNAME_PARAM):"");
		 parameter.put("NP", params.get(Server.PASSWORD_PARAM) !=null?(String) params.get(Server.PASSWORD_PARAM):"");
		 parameter.put("IU", params.get(ServerConstants.IUM_ETIQUETA) !=null?(String) params.get(ServerConstants.IUM_ETIQUETA):"");
		 parameter.put("TE", params.get(Server.NUMERO_CLIENTE_PARAM) !=null?(String) params.get(Server.NUMERO_CLIENTE_PARAM):"");
		 parameter.put("DE", params.get(Server.DESCRIPCION_PARAM)!=null?(String) params.get(Server.DESCRIPCION_PARAM):"");
		 parameter.put("CC", "");
		 parameter.put("CA", params.get(Server.PAYMENT_ACCOUNT_PARAM)!=null?(String) params.get(Server.PAYMENT_ACCOUNT_PARAM):"");
		parameter.put("BF", params.get(Server.BENEFICIARIO_PARAM) != null ? (String) params.get(Server.BENEFICIARIO_PARAM):"");
		parameter.put("CB", params.get(Server.BANK_CODE_PARAM) == null ? "" : (String) params.get(Server.BANK_CODE_PARAM));
		 parameter.put("IM", "");
		 parameter.put("RF", params.get(Server.REFERENCIA_NUMERICA_PARAM) != null ? (String) params.get(Server.REFERENCIA_NUMERICA_PARAM) : "");
		 parameter.put("CP", "");
		 parameter.put("IT", params.get(Server.ID_TOKEN)!=null?(String) params.get(Server.ID_TOKEN):"");
		 parameter.put("TP", params.get(Server.TIPO_CONSULTA_PARAM)!=null?(String) params.get(Server.TIPO_CONSULTA_PARAM):"");
		parameter.put("OA", params.get(Server.OPERADORA_PARAM) != null ? (String) params.get(Server.OPERADORA_PARAM) : "");
		 parameter.put("CN",  params.get(Server.ID_CANAL)!=null?(String) params.get(Server.ID_CANAL):"");
		parameter.put("US",  params.get(Server.USUARIO)!=null?(String) params.get(Server.USUARIO):"");
		 parameter.put("AP", "");
		 parameter.put("NI",  params.get(Server.CARD_NIP_PARAM)!=null?(String) params.get(Server.CARD_NIP_PARAM):"");
		 parameter.put("CV",  params.get(Server.CARD_CVV2_PARAM)!=null?(String) params.get(Server.CARD_CVV2_PARAM):"");
		 parameter.put("OT",  params.get(ServerConstants.CODIGO_OTP)!=null?(String) params.get(ServerConstants.CODIGO_OTP):"");
		 parameter.put("VA",  params.get(Server.VA_PARAM)!=null?(String) params.get(Server.VA_PARAM):"");
		parameter.put("order","NT*NP*IU*TE*DE*CC*CA*BF*CB*IM*RF*CP*IT*TP*OA*CN*US*AP*NI*CV*OT*VA");
		 return parameter;
		
	}
	
}
