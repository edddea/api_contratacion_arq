package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DepositosRecibidosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ObtenerComprobantesDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaDatosViewController;
import tracking.TrackingHelper;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class DetalleDepositosRecibidosViewController extends BaseViewController implements View.OnClickListener {
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	/**
	 * Delegado de la vista.
	 */
	private DepositosRecibidosDelegate delegate;
	
	/**
	 * Contenedor principal de la vista.
	 */
	private LinearLayout rootLayout;
	
	/**
	 * Contenedor para l lista de detalles del movimiento de dinero movil.
	 */
	private LinearLayout listaDetllesLayout;
	
	/**
	 * Lista de detalles del movimiento de dinero movil.
	 */
	private ListaDatosViewController listaDetalles;
	
	private ImageButton menuButton;
	private TextView textoEspecialResultados;
	
	public DetalleDepositosRecibidosViewController() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * No javadoc.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_consulta_despositos_recibidos_detalles);
		setTitle(R.string.bmovil_consultar_depositosrecibidos_titulo, R.drawable.bmovil_consultar_icono);		
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("detalle depositos", parentManager.estados);
		
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID));
		delegate = (DepositosRecibidosDelegate)getDelegate();
		delegate.setViewController(this);
		
		init();
	}
	
	/**
	 * Inicializa el controlador de la ventana.
	 */
	private void init() {
		findViews();
		cargaListaDatos();
		configurarPantalla();
		scaleForCurrentScreen();
		
		
	}

	public void configurarPantalla() {
	
		menuButton.setOnClickListener(this);
		textoEspecialResultados.setText(R.string.bmovil_consultar_obtenercomprobante_detalles_texto_ayuda);
		textoEspecialResultados.setVisibility(View.GONE);
	}
	
	/**
	 * Busca las vistas usadas y guarda sus referencias.
	 */
	private void findViews() {
		rootLayout = (LinearLayout)findViewById(R.id.rootLayout);
		listaDetllesLayout = (LinearLayout)findViewById(R.id.listaDetallesLayout);
		textoEspecialResultados = (TextView) findViewById(R.id.resultado_texto_especial);
		menuButton = (ImageButton)findViewById(R.id.resultados_menu_button);
	}
	
	/**
	 * Redimensiona las vistas para que mantegan la relacion de aspecto en cualquier pantalla.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scalePaddings(rootLayout);
		guiTools.scale(listaDetllesLayout);
		guiTools.scale(textoEspecialResultados, true);
		guiTools.scale(menuButton);
	}
	
	/**
	 * Carga el componente de lista de detalles.
	 */
	private void cargaListaDatos() {
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		ArrayList<Object> detalles = delegate.getDatosTablaDetalleMovimientoDR();
		
		listaDetalles = new ListaDatosViewController(this, params, parentViewsController);
		
		listaDetalles.setNumeroCeldas(2);
		listaDetalles.setLista(detalles);
		listaDetalles.setNumeroFilas(detalles.size());
		listaDetalles.setTitulo(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_titulo);
		listaDetalles.showLista();
		listaDetllesLayout.addView(listaDetalles);
	}
	
	public void botonMenuClick() {
		//AMZ
				((BmovilViewsController)parentViewsController).touchMenu();

		parentViewsController.removeDelegateFromHashMap(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID);
		((BmovilViewsController)parentViewsController).showMenuPrincipal(true);
	}
	
	@Override
	public void onClick(View v) {
		if (v == menuButton) {
			botonMenuClick();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_bmovil_resultados, menu);
		//AMZ
		
				int opc = parentManager.estados.size()-1;
				int opc2 = parentManager.estados.size()-2;
				if(parentManager.estados.get(opc) == "opciones"){
					String rem = parentManager.estados.remove(opc);
				}else if(parentManager.estados.get(opc2) == "sms" || parentManager.estados.get(opc2) == "correo" 
							|| parentManager.estados.get(opc2) == "alta frecuentes"){
					String rem = parentManager.estados.remove(opc2);
					 rem = parentManager.estados.remove(parentManager.estados.size()-1);
				}else{
					TrackingHelper.trackState("opciones", parentManager.estados);
				}
				/***RHO***/
		//return true;
		return false;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		Boolean showMenu = true;
		
		menu.removeItem(R.id.save_menu_sms_button);
//		menu.removeItem(R.id.save_menu_email_button);
		menu.removeItem(R.id.save_menu_pdf_button);
		menu.removeItem(R.id.save_menu_frecuente_button);
		menu.removeItem(R.id.save_menu_rapida_button);
		menu.removeItem(R.id.save_menu_borrar_button);
		
		/***RHO***/
		//return showMenu;
		return false;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		//ARR
				Map<String,Object> envioConfirmacionMap = new HashMap<String, Object>();
		switch(item.getItemId()) {
			case R.id.save_menu_email_button:
				//enviaEmail
				//AMZ
				delegate.enviaEmail();
				//AMZ
				int correo = parentManager.estados.size()-1;
				if(parentManager.estados.get(correo)=="resul"){
					String rem = parentManager.estados.remove(correo);
					rem = parentManager.estados.remove(parentManager.estados.size()-1);
				}
				
				return true;
			default:
				return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}

}
