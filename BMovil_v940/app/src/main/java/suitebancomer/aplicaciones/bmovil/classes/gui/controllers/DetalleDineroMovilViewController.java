package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;



import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DineroMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaDatosViewController;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelper;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;


public class DetalleDineroMovilViewController extends BaseViewController {
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	/**
	 * Delegado de la vista.
	 */
	private DineroMovilDelegate delegate;
	
	/**
	 * Contenedor principal de la vista.
	 */
	private LinearLayout rootLayout;
	
	/**
	 * Contenedor para l lista de detalles del movimiento de dinero movil.
	 */
	private LinearLayout listaDetllesLayout;
	
	/**
	 * Boton para cancelr un envio de dinero movil.
	 */
	private ImageButton botonCancelar;
	
	/**
	 * Lista de detalles del movimiento de dinero movil.
	 */
	private ListaDatosViewController listaDetalles;
	
	public DetalleDineroMovilViewController() {
		// TODO Auto-generated constructor stub
	}

	/*
	 * No javadoc.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_consulta_dinero_movil_detalles);
		setTitle(R.string.bmovil_consultar_dineromovil_titulo, R.drawable.bmovil_consultar_icono);		
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("detalle", parentManager.estados);
		
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID));
		delegate = (DineroMovilDelegate)getDelegate();
		delegate.setViewController(this);
		delegate.setTipoOperacion(Constants.Operacion.cancelarDineroMovil);
		
		init();
	}
	
	/**
	 * Inicializa el controlador de la ventana.
	 */
	private void init() {
		findViews();
		cargaListaDatos();
		scaleForCurrentScreen();
		
		if(delegate.isMovimientoDMVigente()) {
			botonCancelar.setVisibility(View.VISIBLE);
		}
	}
	
	/**
	 * Busca las vistas usadas y guarda sus referencias.
	 */
	private void findViews() {
		rootLayout = (LinearLayout)findViewById(R.id.rootLayout);
		listaDetllesLayout = (LinearLayout)findViewById(R.id.listaDetallesLayout);
		botonCancelar = (ImageButton)findViewById(R.id.btnCancelar);
	}
	
	/**
	 * Redimensiona las vistas para que mantegan la relacion de aspecto en cualquier pantalla.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scalePaddings(rootLayout);
		guiTools.scale(listaDetllesLayout);
		guiTools.scale(botonCancelar);
	}
	
	/**
	 * Carga el componente de lista de detalles.
	 */
	private void cargaListaDatos() {
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		ArrayList<Object> detalles = delegate.getDatosTablaDetalleMovimientoDM();
		
		listaDetalles = new ListaDatosViewController(this, params, parentViewsController);
		
		listaDetalles.setNumeroCeldas(2);
		listaDetalles.setLista(detalles);
		listaDetalles.setNumeroFilas(detalles.size());
		listaDetalles.setTitulo(R.string.bmovil_consultar_dineromovil_detalles_datos_titulo);
		listaDetalles.showLista();
		
		listaDetllesLayout.addView(listaDetalles);
	}
	
	/**
	 * Evento para el click del boton de cancelar env�o.
	 * @param view La vista que invoca esté método.
	 */
	public void onBotonCancelarClick(View view) {
		//AMZ
		Map<String,Object> inicioCancelarMap = new HashMap<String, Object>();
		//AMZ
		inicioCancelarMap.put("evento_inicio","event45");
		inicioCancelarMap.put("&&products","operaciones;cancelar dinero movil");
		inicioCancelarMap.put("eVar12","inicio_cancelar");
		TrackingHelper.trackInicioOperacion(inicioCancelarMap);


		if(Server.ALLOW_LOG) Log.i("DetalleDineroMovilViewController", "Se intenta cancelar el envío");
		
		delegate.setEsBajaDM(true);
		delegate.setBajaFrecuente(false);
		
		delegate.showConfirmacionAutenticacion();
//		delegate.getDatosTablaConfirmacion();
//		
//		delegate.cancelarMovimiento();
	}

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}
	
	
}
