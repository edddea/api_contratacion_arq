package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ConsultarEstatusEnvioECResult implements ParsingHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 /**
	     * The fecha
	     */
	    private static String fecha = null;
	    
	    /**
	     * The hora
	     */
	    private static String hora = null;
	    
	    /**
	     * The folio
	     */
	    private static String folio = null;
	    

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
	   setFecha(parser.parseNextValue("fecha"));
	   setHora(parser.parseNextValue("hora"));
	   setFolio(parser.parseNextValue("folio"));
	}

	public static String getFecha() {
	    return fecha;
	}

	public void setFecha(String fecha) {
	    this.fecha = fecha;
	}

	public static String getHora() {
	    return hora;
	}

	public void setHora(String hora) {
	    this.hora = hora;
	}

	public static String getFolio() {
	    return folio;
	}

	public void setFolio(String folio) {
	    this.folio = folio;
	}


}
