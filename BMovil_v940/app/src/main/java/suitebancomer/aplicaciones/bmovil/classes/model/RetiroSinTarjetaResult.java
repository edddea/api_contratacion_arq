package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Result;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class RetiroSinTarjetaResult  implements ParsingHandler {
	
	
	/**
	 * modelo de respuesta para la operacion de retiro sin tarjeta
	 */
	private static final long serialVersionUID = 1L;

	private String mensajeInformativo;
	
	private String claveRst4;
	
	private String importe;
	
	private String fechaAlta;
	
	private String vigencia;
	
	private String horaOperacion;
	
	private String folio;
	
	private String claveRetiro;


	

	public RetiroSinTarjetaResult() {
		super();

		this.mensajeInformativo = "";
		this.claveRst4 = "";
		this.importe = "";
		this.fechaAlta = "";
		this.vigencia = "";
		this.horaOperacion = "";
		this.folio = "";
		this.claveRetiro = "";
		
		
		
	}

	public RetiroSinTarjetaResult(String mensajeInformativo, String claveRst4,
			String importe, String fechaAlta, String vigencia,
			String horaOperacion, String folio, String claveRetiro) {
		super();
		this.mensajeInformativo = mensajeInformativo;
		this.claveRst4 = claveRst4;
		this.importe = importe;
		this.fechaAlta = fechaAlta;
		this.vigencia = vigencia;
		this.horaOperacion = horaOperacion;
		this.folio = folio;
		this.claveRetiro = claveRetiro;
	}

	public String getMensajeInformativo() {
		return mensajeInformativo;
	}

	public void setMensajeInformativo(String mensajeInformativo) {
		this.mensajeInformativo = mensajeInformativo;
	}

	public String getClaveRst4() {
		return claveRst4;
	}

	public void setClaveRst4(String claveRst4) {
		this.claveRst4 = claveRst4;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getHoraOperacion() {
		return horaOperacion;
	}

	public void setHoraOperacion(String horaOperacion) {
		this.horaOperacion = horaOperacion;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getClaveRetiro() {
		return claveRetiro;
	}

	public void setClaveRetiro(String claveRetiro) {
		this.claveRetiro = claveRetiro;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		try{
			
				this.mensajeInformativo = parser.parseNextValue("mensajeInformativo");
				this.claveRst4 =  parser.parseNextValue("claveRst4");
				this.importe =parser.parseNextValue("importe");
				this.fechaAlta = parser.parseNextValue("fechaAlta");
				this.vigencia = parser.parseNextValue("vigencia");
				this.horaOperacion = parser.parseNextValue("horaOperacion");
				this.folio = parser.parseNextValue("folio");
				this.claveRetiro = parser.parseNextValue("claveRetiro");

		
		}catch(Exception e){
			if (Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
