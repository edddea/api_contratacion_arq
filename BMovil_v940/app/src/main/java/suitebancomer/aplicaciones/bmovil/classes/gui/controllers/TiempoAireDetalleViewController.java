package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.bmovil.classes.common.AbstractContactMannager;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TiempoAireDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import suitebancomer.classes.gui.views.SeleccionHorizontalViewController;
import tracking.TrackingHelper;

public class TiempoAireDetalleViewController extends BaseViewController implements OnClickListener {

	/**
	 * Codigo de la petici�n de un contacto desde la agenda.
	 */
	public static final int ContactsRequestCode = 1;

	TiempoAireDelegate delegate;
	RelativeLayout vista;
	LinearLayout vistaCtaOrigen;
	LinearLayout vistaSelecHorizontal;
	CuentaOrigenViewController componenteCtaOrigen;
	SeleccionHorizontalViewController seleccionHorizontal;
	ListaSeleccionViewController listaSeleccion;
	public EditText numCelular;
	ImageButton btnAgenda;
	public EditText numCelularConfirmacion;
	public Button importe;
	Dialog comboImporte;
	ImageButton btnContinuar;
	TextView fecha;
	boolean pedirContacto;
	private boolean btnImporteDesactivado;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_compra_tiempo_aire_detalle_view);
		setTitle(R.string.tiempo_aire_title,R.drawable.bmovil_tiempo_aire_icono);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("nuevo", parentManager.estados);
		
		btnImporteDesactivado = false;
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID));
		delegate = (TiempoAireDelegate)getDelegate();
		delegate.setcontroladorTiempoAire(this);
//		vista = (RelativeLayout)findViewById(R.id.tiempo_aire_view_controller_layout);
		vistaCtaOrigen = (LinearLayout)findViewById(R.id.tiempo_aire_cuenta_origen_view);
		vistaSelecHorizontal = (LinearLayout)findViewById(R.id.tiempo_aire_seleccion_horizontal_view);
		
		btnContinuar = (ImageButton)findViewById(R.id.tiempo_aire_boton_continuar);
		btnContinuar.setOnClickListener(this);
		
		btnAgenda = (ImageButton)findViewById(R.id.tiempo_aire_boton_agenda);
		btnAgenda.setOnClickListener(this);
		
		importe = (Button) findViewById(R.id.tiempo_aire_importe_txt);
		importe.setOnClickListener(this);
		
		numCelular = (EditText)findViewById(R.id.tiempo_aire_numero_celular_text);
		numCelularConfirmacion = (EditText)findViewById(R.id.tiempo_aire_confirma_numero_txt);
		importe = (Button) findViewById(R.id.tiempo_aire_importe_txt);
		fecha = (TextView)findViewById(R.id.tiempo_aire_fecha_operacion_text);
		fecha.setText(Tools.getCurrentDate());
		pedirContacto = false;

		scaleForCurrentScreen();
		
		if(delegate.isFrecuente())
			configuraPantallaParaFrecuente();
		else if(delegate.isOperacionRapida())
			configurarPantallaParaRapidos();
		else
			init();
		
		numCelular.addTextChangedListener(new BmovilTextWatcher(this));
		numCelularConfirmacion.addTextChangedListener(new BmovilTextWatcher(this));
	}

	private void configuraPantallaParaFrecuente() {
		cargarCuentas();
		cargaSeleccionHorizontal();
    	numCelular.setText(delegate.getCompraTiempoAire().getCelularDestino());
    	numCelularConfirmacion.setText(delegate.getCompraTiempoAire().getCelularDestino());
		btnAgenda.setEnabled(false);
    	numCelular.setEnabled(false);
    	
		ArrayList<Object> listaServicios = delegate.getListaServicios();
		for (int i = 0; i < listaServicios.size(); i++) {
			Compania compania = (Compania)listaServicios.get(i);
			if (delegate.getCompraTiempoAire().getNombreCompania().endsWith(compania.getNombre())) {
				seleccionHorizontal.setSelectedItem(compania);
				delegate.getCompraTiempoAire().setClaveCompania(compania.getClave());
			}
		}
		numCelularConfirmacion.setEnabled(false);
		seleccionHorizontal.bloquearComponente();
	}
	
	public void init(){
    	// fija el tama�o del campo adecuados para numero telefonico
    	InputFilter[] FilterArray = new InputFilter[1];
    	FilterArray[0] = new InputFilter.LengthFilter(Constants.NUMERO_TELEFONO_LENGTH);
    	numCelular.setFilters(FilterArray);
    	numCelularConfirmacion.setFilters(FilterArray);

		cargarCuentas();
		cargaSeleccionHorizontal();
	}
	
	@SuppressWarnings("deprecation")
	public void cargarCuentas(){
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
//		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.topMargin = 8;
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
//		params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
		GuiTools guiTools = GuiTools.getCurrent();
		
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, guiTools.getEquivalenceInPixels(92.0));
//		params.bottomMargin = guiTools.getEquivalenceInPixels(20.0);
//		params.leftMargin = guiTools.getEquivalenceInPixels(20.0);

		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
		componenteCtaOrigen.setDelegate(delegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(delegate.getCompraTiempoAire().getCuentaOrigen()));
		componenteCtaOrigen.init();
		vistaCtaOrigen.addView(componenteCtaOrigen);
	}

	@SuppressWarnings("deprecation")
	public void cargaSeleccionHorizontal() {
		LinearLayout.LayoutParams params1 = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		ArrayList<Object> listaServicios = delegate.getListaServicios();
		
		seleccionHorizontal = new SeleccionHorizontalViewController(this, params1, 
													listaServicios, getDelegate(), false);
		vistaSelecHorizontal.addView(seleccionHorizontal);
		
		if(!delegate.isOperacionRapida())
			delegate.setConvenioSeleccionado((Compania)listaServicios.get(0));
	}

	@SuppressWarnings("deprecation")
	public void cargarImportes() {
		ArrayList<Object> listaMontos = delegate.getImportes();
		comboImporte = new Dialog(this);
		comboImporte.setOnDismissListener(new DialogInterface.OnDismissListener() {
			
			@Override
			public void onDismiss(DialogInterface dialog) {
				btnImporteDesactivado = false;
			}
		});
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.topMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.rightMargin =  getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
		
		listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
		listaSeleccion.setDelegate(delegate);
		listaSeleccion.setNumeroColumnas(1);
		listaSeleccion.setLista(listaMontos);
		listaSeleccion.setOpcionSeleccionada(-1);
		listaSeleccion.setSeleccionable(false);
		listaSeleccion.setAlturaFija(false);
		listaSeleccion.setNumeroFilas(7);
		listaSeleccion.setExisteFiltro(false);
		listaSeleccion.cargarTabla();
		comboImporte.setTitle(getString(R.string.nipperAirtimePurchase_selectAmount));
		comboImporte.addContentView(listaSeleccion, params);
		comboImporte.show();
	}

	@Override
	public void onClick(View v) {
		if (v == btnAgenda) {
			pedirContacto = true;
			AbstractContactMannager.getContactManager().requestContactData(this);
		}else if (v == importe && !btnImporteDesactivado) {
			btnImporteDesactivado = true;
			cargarImportes();
		}else if (v == btnContinuar && !parentViewsController.isActivityChanging()) {
			delegate.validarDatos();
			//ARR
			if(delegate.res)
			{
				//ARR
				Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
				
				//ARR
				paso2OperacionMap.put("evento_paso2", "event47");
				paso2OperacionMap.put("&&products", "operaciones;comprar+tiempo aire");
				paso2OperacionMap.put("eVar12", "paso2:cuenta e importe");

				TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
			}
		}
	}
	
	public void actualizarImporte(String importeElegido){
		StringBuffer sb = new StringBuffer(importeElegido);
		sb.append("00");
		importe.setText(Tools.formatAmountFromServer(sb.toString()));
		delegate.getCompraTiempoAire().setImporte(sb.toString());
		//btnImporteDesactivado = false;
		comboImporte.dismiss();
	}
	
	public void reactivarCtaOrigen(){
		delegate.getCompraTiempoAire().setCuentaOrigen(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled(componenteCtaOrigen.getCuentaOrigenDelegate().getListaCuentaOrigen().size() > 1);
	}
	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		setHabilitado(true);
		delegate.analyzeResponse(operationId, response);
	}

	/*
	 * No javadoc. 
	 */
	@Override  
	public void onActivityResult(int reqCode, int resultCode, Intent data) {  
	    super.onActivityResult(reqCode, resultCode, data);
	    
	    if (resultCode == Activity.RESULT_OK) {
	    	switch (reqCode) {
			case ContactsRequestCode:
				obtenerContacto(data);
				break;
			}
	    }
	}
	
	/**
	 * Obtiene los datos del contacto del contacto seleccionado.
	 * @param data Los datos del Intent.
	 */
	public void obtenerContacto(Intent data) {
		AbstractContactMannager manager = AbstractContactMannager.getContactManager();
		manager.getContactData(this, data);
		
		if(manager.getNumeroDeTelefono().equalsIgnoreCase(""))
			return;

	    numCelular.setText(manager.getNumeroDeTelefono());
	    numCelularConfirmacion.setText(manager.getNumeroDeTelefono());
	    pedirContacto = false;
	}

	
	@Override
	protected void onResume() {
		super.onResume();
		if (!pedirContacto) {
			if (parentViewsController.consumeAccionesDeReinicio()) {
				return;
			}
		}
		setHabilitado(true);
		getParentViewsController().setCurrentActivityApp(this);
		if (delegate != null) {
			delegate.setcontroladorTiempoAire(this);
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if (!pedirContacto) {
			parentViewsController.consumeAccionesDePausa();
		}
	}
	
	@Override
	public void goBack() {
		super.goBack();
	}	
	
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.layoutRoot));
		guiTools.scale(findViewById(R.id.tiempo_aire_titulo_seleccionHorizontal));
		guiTools.scale(findViewById(R.id.tiempo_aire_numero_celular));
		guiTools.scale(numCelular,true);
		guiTools.scale(btnAgenda);
		guiTools.scale(findViewById(R.id.tiempo_aire_confirma_numero));
		guiTools.scale(numCelularConfirmacion,true);
		guiTools.scale(findViewById(R.id.tiempo_aire_importe));
		guiTools.scale(importe,true);
		guiTools.scale(findViewById(R.id.tiempo_aire_fecha_operacion_label));
		guiTools.scale(fecha,true);
		guiTools.scale(btnContinuar);

	}
	
	private void configurarPantallaParaRapidos() {
		cargarCuentas();
		cargaSeleccionHorizontal();
		
		importe.setText(Tools.formatAmountFromServer(delegate.getCompraTiempoAire().getImporte()));
    	numCelular.setText(delegate.getCompraTiempoAire().getCelularDestino());
    	numCelularConfirmacion.setText(delegate.getCompraTiempoAire().getCelularDestino());
		
		ArrayList<Object> listaServicios = delegate.getListaServicios();
		for (int i = 0; i < listaServicios.size(); i++) {
			Compania compania = (Compania)listaServicios.get(i);
			if (delegate.getCompraTiempoAire().getNombreCompania().equalsIgnoreCase(compania.getNombre())) {
				seleccionHorizontal.setSelectedItem(compania);
				delegate.getCompraTiempoAire().setClaveCompania(compania.getClave());
			}
		}
		
		btnAgenda.setEnabled(false);
    	numCelular.setEnabled(false);
    	numCelularConfirmacion.setEnabled(false);
		//seleccionHorizontal.bloquearComponente();
	}
}
