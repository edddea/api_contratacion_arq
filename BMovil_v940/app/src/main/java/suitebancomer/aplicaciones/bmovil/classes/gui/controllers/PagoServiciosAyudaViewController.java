package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;


import java.util.Hashtable;

import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagoServiciosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Catalog;
import suitebancomer.aplicaciones.bmovil.classes.model.NameValuePair;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.classes.gui.controllers.BaseViewController;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import tracking.TrackingHelper;

/**
 * Services payment helper view controller. Downloads a helper image from the server 
 * and displays it to the user. A not available service message is shown in the
 * event of a download error
 *
 */
public class PagoServiciosAyudaViewController extends BaseViewController {

	/**
	 * Constant for reference help
	 */
	public static final String REFERENCE_HELP_ID = "REF";
	
	/**
	 * Constant for reason help
	 */
	public static final String REASON_HELP_ID = "CON";
	//AMZ
		public BmovilViewsController parentManager;
		//AMZ

	/**
	 * Called when the activity is first created. Initializes the layout views
	 * @param savedInstanceState state
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_pago_servicios_ayuda);
		setTitle(R.string.servicesPayment_title, R.drawable.icono_pagar_servicios);
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
																
		
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(getParentViewsController().getBaseDelegateForKey(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID));
	}
    
    /**
	 * Called before the view is shown. Format is applied to subviews.
	 */
    @Override
    protected void onStart() {
    	super.onStart();

    	// Retrieves the specified image from server, once layout has been setup
    	Bundle extras = getIntent().getExtras();
		Hashtable<String, String> par = new Hashtable<String, String>();

        par.put(Server.SCREEN_SIZE_PARAM, extras.getString(Server.SCREEN_SIZE_PARAM));
        par.put(Server.HELP_IMAGE_TYPE_PARAM, extras.getString(Server.HELP_IMAGE_TYPE_PARAM));
        par.put(Server.SERVICE_PROVIDER_PARAM, extras.getString(Server.SERVICE_PROVIDER_PARAM));
		//Este param sirve para identificarlo que es procesamiento de imagen
		par.put(ApiConstants.IMAGEN_BMOVIL, extras.getString(Server.SERVICE_PROVIDER_PARAM));
        //Revisar ya que se descarga una imagen
        SuiteApp.getInstance().getBmovilApplication().invokeNetworkOperation(Server.HELP_IMAGE_OPERATION, par,false,null,Server.isJsonValueCode.NONE,
		this);
    }
    
    @Override
    protected void onResume() {
    	super.onResume();
    	if (getParentViewsController().consumeAccionesDeReinicio()) {
    		return;
    	}
    	getParentViewsController().setCurrentActivityApp(this);
    	((PagoServiciosDelegate)getDelegate()).setControladorPagos(this);
    }
    
    @Override
    protected void onPause() {
    	super.onPause();
    	getParentViewsController().consumeAccionesDePausa();
    }
    
    /**
	 * Analyzes the server response. Result and page code are extracted from the server response
	 *
	 * @param response The server response to analyze
	 */
    public void processNetworkResponse(int operationId, ServerResponse response) {
    	((PagoServiciosDelegate)getDelegate()).analyzeResponse(operationId, response);
    }

    /**
     * If the image was found at server, then its shown
     * 
     * @param image the bitmap image
     */
	public void showData(Bitmap image) {
		ImageView img = (ImageView) findViewById(R.id.pagarServicios_imagen_ayuda);
        img.setImageBitmap(image);
	}
	
	/**
	 * If image was not found, an error message is shown
	 */
	public void showUnfoundImage() {
		findViewById(R.id.pagarServicios_imagen_ayuda).setVisibility(View.GONE);
		findViewById(R.id.pagarServicios_sin_ayuda).setVisibility(View.VISIBLE);
		
		Bundle params = this.getIntent().getExtras();
		String aux = params.getString(Server.HELP_IMAGE_TYPE_PARAM);
		
		TextView tv = (TextView)findViewById(R.id.pagarServicios_sin_ayuda);
		tv.setWidth( (params.getInt("TEXT_WIDTH")) - 50 );
		
		Catalog catalog_ayudas = (Session.getInstance(getApplicationContext())).getHelps();
		
		if( aux.equals(REASON_HELP_ID) ){
			NameValuePair dato = catalog_ayudas.getNameValuePair("APSCON");
			tv.setText(dato.getValue());
		}
		
		if( aux.equals(REFERENCE_HELP_ID) ){
			NameValuePair dato = catalog_ayudas.getNameValuePair("APSREF");
			tv.setText(dato.getValue());
		}
	}
}
