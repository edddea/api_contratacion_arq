package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;



import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TipoConsultaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import tracking.TrackingHelper;

public class TipoConsultaViewController extends BaseViewController {
	private LinearLayout contenedorMenu;
	private ListaSeleccionViewController menuFrecuentes;
	TipoConsultaDelegate delegate;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_tipo_consulta);
		setTitle(R.string.bmovil_consultar_frecuentes_title_cabecera, R.drawable.bmovil_consultar_icono);
		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		delegate = (TipoConsultaDelegate) parentViewsController.getBaseDelegateForKey(TipoConsultaDelegate.TIPO_CONSULTA_DELEGATE_ID);
		setDelegate(delegate);
		delegate.setTipoConsultaViewController(this);
		incializarPantalla();
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
			
				TrackingHelper.trackState("frecuentes", parentManager.estados);
		
	}

	@SuppressWarnings("deprecation")
	private void incializarPantalla() {
		contenedorMenu = (LinearLayout)findViewById(R.id.bmovil_tipo_consulta_frecuentes);
		GuiTools.getCurrent().init(getWindowManager());
		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		//params.topMargin = GuiTools.getCurrent().getEquivalenceFromScaledPixels( getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin));
		params.leftMargin = GuiTools.getCurrent().getEquivalenceFromScaledPixels( getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin));
		params.rightMargin = GuiTools.getCurrent().getEquivalenceFromScaledPixels( getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin));

		ArrayList<Object> lista = delegate.getDatosTablaMenuTipoFrecuentes();		

		if (menuFrecuentes == null) {
			menuFrecuentes = new ListaSeleccionViewController(this, params,	parentViewsController);
			menuFrecuentes.setDelegate(delegate);
			menuFrecuentes.setNumeroColumnas(1);
			menuFrecuentes.setEncabezado(null);
			menuFrecuentes.setLista(lista);
			menuFrecuentes.setOpcionSeleccionada(-1);
			menuFrecuentes.setSeleccionable(false);
			menuFrecuentes.setAlturaFija(false);
			menuFrecuentes.setNumeroFilas(lista.size());
			menuFrecuentes.setExisteFiltro(false);
			menuFrecuentes.setTitle("Selecciona");
			menuFrecuentes.cargarTabla();
			contenedorMenu.addView(menuFrecuentes);
			
		}
		
	}

	public void opcionSeleccionada(String selected) {
		if(Server.ALLOW_LOG) Log.d(getLocalClassName().trim(), "Entraste a consultarFrecuentes");
		if(delegate.validaHorarioOperacion(selected)){
//AMZ
			
			Map<String,Object> OperacionRealizadaMap = new HashMap<String, Object>();
			if(selected.equals(Constants.tipoCFOtrosBBVA)){
				if (delegate.validarUsuarioTDC()) {
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","consulta;frecuentes");
				OperacionRealizadaMap.put("eVar11","otra cuenta bbva bancomer");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				} else {
					showInformationAlert(R.string.alert_unallowed_operation);
					return;
				}
			}else if(selected.equals(Constants.tipoCFCExpress)){
				if (delegate.validarUsuarioTDC()) {
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","consulta;frecuentes");
				OperacionRealizadaMap.put("eVar11","cuenta express");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
			TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				} else {
					showInformationAlert(R.string.alert_unallowed_operation);
					return;
				}
			}else if(selected.equals(Constants.tipoCFOtrosBancos)){
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","consulta;frecuentes");
				OperacionRealizadaMap.put("eVar11","otros bancos");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
			TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
			}else if(selected.equals(Constants.tipoCFDineroMovil)){
				if (delegate.validarUsuarioTDC()) {
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","consulta;frecuentes");
				OperacionRealizadaMap.put("eVar11","dinero movil");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
			TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				} else {
					showInformationAlert(R.string.alert_unallowed_operation);
					return;
				}
			}else if(selected.equals(Constants.tipoCFTiempoAire)){
				if (delegate.validarUsuarioTDC()) {
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","consulta;frecuentes");
				OperacionRealizadaMap.put("eVar11","tiempo aire");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
			TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				} else {
					showInformationAlert(R.string.alert_unallowed_operation);
					return;
				}
			}else if(selected.equals(Constants.tipoCFPagosCIE)){
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","consulta;frecuentes");
				OperacionRealizadaMap.put("eVar11","servicios");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
			TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
			}
			
			((BmovilViewsController)parentViewsController).showConsultaFrecuentes(selected);
			
		}else{
			
			showInformationAlert(delegate.getTextoMensajeAlertaHorario(selected));	
		}		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		if (delegate != null) {
			delegate.setTipoConsultaViewController(this);
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {		
		super.goBack();
	}
	
	@Override
	protected void onDestroy() {
		parentViewsController.removeDelegateFromHashMap(TipoConsultaDelegate.TIPO_CONSULTA_DELEGATE_ID);
		super.onDestroy();
	}

	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		delegate.analyzeResponse(operationId, response);
	}

}
