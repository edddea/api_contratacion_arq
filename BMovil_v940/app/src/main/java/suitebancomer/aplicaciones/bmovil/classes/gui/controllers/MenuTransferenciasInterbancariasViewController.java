package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.widget.LinearLayout;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuTransferenciasInterbancariasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;

/**
 * Created by Nicolas Arriaza on 04/08/2015.
 */
public class MenuTransferenciasInterbancariasViewController  extends BaseViewController {

    private MenuTransferenciasInterbancariasDelegate delegate;

    private LinearLayout vista;

    private ListaSeleccionViewController listaSeleccion;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_menu_transferencias_interbancarias);
        setTitle(R.string.bmovil_spei_title, R.drawable.bmovil_consultar_icono);
        SuiteApp suiteApp = (SuiteApp)getApplication();

        setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());

        delegate = (MenuTransferenciasInterbancariasDelegate)parentViewsController.getBaseDelegateForKey(MenuTransferenciasInterbancariasDelegate.MENU_TRANSFERENCIAS_INTERBANCARIAS_DELEGATE_ID);
        if(delegate == null){
            delegate = new MenuTransferenciasInterbancariasDelegate();
            parentViewsController.addDelegateToHashMap(MenuTransferenciasInterbancariasDelegate.MENU_TRANSFERENCIAS_INTERBANCARIAS_DELEGATE_ID, delegate);
        }
        setDelegate(delegate);
        delegate.setViewController(this);

        vista = (LinearLayout)findViewById(R.id.contenedor_menu_transferencias_interbancarias);
        init();
    }

    public void init(){
        cargaListaSeleccionComponent();
    }

    public void cargaListaSeleccionComponent(){
        ArrayList<Object> lista = ((ArrayList<Object>) delegate.getDatosTablaMenu());

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        GuiTools.getCurrent().init(getWindowManager());
        params.leftMargin 	= GuiTools.getCurrent().getEquivalenceFromScaledPixels(getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin));
        params.rightMargin 	= GuiTools.getCurrent().getEquivalenceFromScaledPixels(getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin));


        listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
        listaSeleccion.setDelegate(delegate);
        listaSeleccion.setLista(lista);
        listaSeleccion.setNumeroColumnas(1);
        listaSeleccion.setTitle(getString(R.string.bmovil_spei_menuTransferencias_table_title));
        listaSeleccion.setOpcionSeleccionada(-1);
        listaSeleccion.setSeleccionable(false);
        listaSeleccion.setAlturaFija(false);
        listaSeleccion.setNumeroFilas(lista.size());
        listaSeleccion.cargarTabla();
        vista.addView(listaSeleccion);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
        if (delegate != null) {
            delegate.setViewController(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    public void goBack() {
        super.goBack();
    }

    @Override
    protected void onDestroy() {
        parentViewsController.removeDelegateFromHashMap(MenuTransferenciasInterbancariasDelegate.MENU_TRANSFERENCIAS_INTERBANCARIAS_DELEGATE_ID);
        super.onDestroy();
    }

    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }


}
