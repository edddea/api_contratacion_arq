package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;
import suitebancomer.aplicaciones.bmovil.classes.common.AbstractContactMannager;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.InterbancariosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaInterbancaria;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.AmountField;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import tracking.TrackingHelper;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class InterbancariosViewController extends BaseViewController implements OnClickListener, DialogInterface.OnDismissListener {

	
	LinearLayout vista;
	LinearLayout vistaCtaOrigen;
	public CuentaOrigenViewController componenteCtaOrigen;
	ImageButton btnContinuar;
	private InterbancariosDelegate delegate;
	public Button tipoCuenta;
	public Button bancoDestino;
	public EditText txtNumeroCuenta;
	public EditText txtBeneficiario;
	public EditText txtReferencia;
	public EditText txtConcepto;
	ListaSeleccionViewController listaSeleccion;
	Dialog combo;
	boolean selectedTipoCuenta;
	public AmountField textImporte;
	TextView fecha;
	protected static final int SHOW_LISTA_BANCOS = 100;
	private boolean skipParentValidation;
	
	//AMZ
	public BmovilViewsController parentManager;
	// AMZ

	// boton para seleccionar contactos de la agenda del dispositivo
	private ImageButton btnAgendaInterbancarios;

	// Codigo de respuesta de contactos
	public static final int ContactsRequestCode = 1;

	// Bandera para indicar si se estan pidiendo contactos.
	private boolean pidiendoContactos;
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_transfer_interbancario_view);
		setTitle(R.string.opcionesTransfer_menu_otrosbancos,R.drawable.bmovil_transferir_icono);
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
				TrackingHelper.trackState("nuevo", parentManager.estados);
				

		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID));
		delegate = (InterbancariosDelegate)getDelegate();
		if(delegate==null){
			delegate = new InterbancariosDelegate();
			parentViewsController.addDelegateToHashMap(InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID,delegate);
			setDelegate(delegate);
		}
		delegate.setInterbancariosViewController(this);
		vista = (LinearLayout)findViewById(R.id.transfer_interbancario_view_controller_layout);
		vistaCtaOrigen = (LinearLayout)findViewById(R.id.transfer_interbancario_cuenta_origen_view);
		btnContinuar = (ImageButton)findViewById(R.id.transfer_interbancario_boton_continuar);
		btnContinuar.setOnClickListener(this);
		
		tipoCuenta = (Button) findViewById(R.id.transfer_interbancario_tipo_cuenta_txt);
		tipoCuenta.setOnClickListener(this);
		bancoDestino = (Button) findViewById(R.id.transfer_interbancario_banco_destino_txt);;
		bancoDestino.setOnClickListener(this);
		txtNumeroCuenta = (EditText)findViewById(R.id.transfer_interbancario_numero_tarjeta_txt);
		textImporte = (AmountField)findViewById(R.id.transfer_interbancario_importe_txt);
		txtBeneficiario = (EditText)findViewById(R.id.transfer_interbancario_beneficiario_txt);
		txtReferencia = (EditText)findViewById(R.id.transfer_interbancario_referencia_txt);
		txtConcepto = (EditText)findViewById(R.id.transfer_interbancario_concepto_txt);
		fecha = (TextView)findViewById(R.id.transfer_interbancario_fecha_operacion_text);
		fecha.setText(Tools.getCurrentDate());
		
		btnAgendaInterbancarios = (ImageButton)findViewById(R.id.btnAgendaInterbancarios);
		btnAgendaInterbancarios.setOnClickListener(this); 
		
		//bandera contactos
		pidiendoContactos = false;
		
		if(delegate.getEsFrecuente()){
			configuraPantallaParaFrecuente();
		}
		else
			init();
		InputFilter[] filters = new InputFilter[1];
		filters[0] = new InputFilter.LengthFilter(Constants.TRANSFER_OTROS_BANCOS_REFERENCE_LENGTH);
		txtReferencia.setFilters(filters);
		InputFilter[] filterBeneficiario ={new InputFilter.LengthFilter(Constants.TRANSFER_INTERBANCARIO_BENEFICIARIO_LENGTH)};
		InputFilter[] filterConcepto ={new InputFilter.LengthFilter(Constants.TRANSFER_INTERBANCARIO_CONCEPTO_LENGTH)};
		txtBeneficiario.setFilters(filterBeneficiario);
		txtConcepto.setFilters(filterConcepto);
		InputFilter[] filterImporte = {new InputFilter.LengthFilter(Constants.AMOUNT_LENGTH)};
		textImporte.setFilters(filterImporte);
		configurarPantalla();
		
		txtBeneficiario.addTextChangedListener(new BmovilTextWatcher(this));
		txtConcepto.addTextChangedListener(new BmovilTextWatcher(this));
		txtNumeroCuenta.addTextChangedListener(new BmovilTextWatcher(this));
		txtReferencia.addTextChangedListener(new BmovilTextWatcher(this));
	}
	
	private void configurarPantalla(){
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(findViewById(R.id.transfer_interbancario_tipo_cuenta),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_tipo_cuenta_txt),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_banco_destino),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_banco_destino_txt),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_numero_tarjeta),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_numero_tarjeta_txt),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_beneficiario),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_beneficiario_txt),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_importe),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_importe_txt),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_referencia),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_referencia_txt),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_concepto),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_concepto_txt),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_fecha_operacion_label),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_fecha_operacion_text),true);
		gTools.scale(findViewById(R.id.transfer_interbancario_boton_continuar));
		gTools.scale(findViewById(R.id.transfer_interbancario_contenedor_principal));
		//gTools.scale(findViewById(R.id.transfer_interbancario_view_controller_layout));
		gTools.scale(findViewById(R.id.transfer_interbancario_cuenta_origen_view));		
		
		gTools.scale(findViewById(R.id.btnAgendaInterbancarios));
	}

	public void configuraPantallaParaFrecuente() {
		tipoCuenta.setEnabled(false);
		bancoDestino.setEnabled(false);
		txtNumeroCuenta.setEnabled(false);
		txtBeneficiario.setEnabled(false);
		String tipo = delegate.getTransferenciaInterbancaria().getTipoCuentaDestino();
		String tituloTipoCuenta = "";
		   if(tipo.equals(Constants.DEBIT_TYPE)){
			   tituloTipoCuenta = SuiteApp.appContext.getString(R.string.cardType_debit);
			   InputFilter[] filter = {new InputFilter.LengthFilter(16)};
				txtNumeroCuenta.setFilters(filter);
		   }else if(tipo.equals(Constants.CREDIT_TYPE)){
			   tituloTipoCuenta = SuiteApp.appContext.getString(R.string.cardType_credit);
			   InputFilter[] filter = {new InputFilter.LengthFilter(16)};
				txtNumeroCuenta.setFilters(filter);
		   }else if(tipo.equals(Constants.CLABE_TYPE_ACCOUNT)){
			   tituloTipoCuenta = SuiteApp.appContext.getString(R.string.cardType_clabe);
			   InputFilter[] filter = {new InputFilter.LengthFilter(18)};
				txtNumeroCuenta.setFilters(filter);
		   }
		   //SPEI
		   else if(tipo.equals(Constants.PHONE_TYPE_ACCOUNT)){
			   tituloTipoCuenta = SuiteApp.appContext.getString(R.string.cardType_phone);
			   InputFilter[] filter = {new InputFilter.LengthFilter(Constants.TELEPHONE_NUMBER_LENGTH)};
			   txtNumeroCuenta.setFilters(filter);
			   TextView accountTitleLabel = (TextView)findViewById(R.id.transfer_interbancario_numero_tarjeta);
			   accountTitleLabel.setText(getString(R.string.transferir_interbancario_numero_celular));
		   }
		   //termina SPEI
		  
		
		tipoCuenta.setText(tituloTipoCuenta);
		bancoDestino.setText(delegate.getTransferenciaInterbancaria().getNombreBanco());
		delegate.numeroTarjeta();
		txtNumeroCuenta.setText(delegate.getTransferenciaInterbancaria().getNumeroCuentaDestino());
		txtBeneficiario.setText(delegate.getTransferenciaInterbancaria().getBeneficiario());
		txtConcepto.setText(delegate.getTransferenciaInterbancaria().getConcepto());
		txtReferencia.setText(delegate.getTransferenciaInterbancaria().getReferencia());
		Session session = Session.getInstance(SuiteApp.appContext);		
		String fechaActual = Tools.dateForReference(session.getServerDate());
		txtReferencia.setText(fechaActual);
		
		cargarCuentas();
	}

	public void init(){
		//frecuntes correo electronico
		//delegate.setTransferenciaInterbancaria(((TransferirDelegate)parentViewsController.getBaseDelegateForKey(TransferirDelegate.TRANSFERIR_DELEGATE_ID)).getTransferenciaInterbancaria());
		if (!delegate.getEsFrecuente()) {
			TransferenciaInterbancaria transferenciaInterbancaria = new TransferenciaInterbancaria();
			transferenciaInterbancaria.setCuentaOrigen(((TransferirDelegate) parentViewsController.getBaseDelegateForKey(TransferirDelegate.TRANSFERIR_DELEGATE_ID)).getTransferenciaInterbancaria().getCuentaOrigen());
			delegate.setTransferenciaInterbancaria(transferenciaInterbancaria);
		} else {
			delegate.setTransferenciaInterbancaria(((TransferirDelegate) parentViewsController.getBaseDelegateForKey(TransferirDelegate.TRANSFERIR_DELEGATE_ID)).getTransferenciaInterbancaria());
		}
		cargarCuentas();
		bancoDestino.setEnabled(false);
		Session session = Session.getInstance(SuiteApp.appContext);		
		String fechaActual = Tools.dateForReference(session.getServerDate());
		txtReferencia.setText(fechaActual);
    }
	
	@SuppressWarnings("deprecation")
	public void cargarCuentas(){
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.topMargin = 8;
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
//		params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);

		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
		componenteCtaOrigen.setDelegate(delegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(delegate.getTransferenciaInterbancaria().getCuentaOrigen()));
		componenteCtaOrigen.init();
		vistaCtaOrigen.addView(componenteCtaOrigen);
	}
	
	@SuppressWarnings("deprecation")
	public void cargarComboTipoCuentas(){
		ArrayList<Object> listaTiposCuenta = delegate.getListaTipoCuentaDestino();
		combo = new Dialog(this){
			@Override
			public boolean onTouchEvent(MotionEvent event) {
				//Log.d("Dialog", "onTouchEvent");
				return super.onTouchEvent(event);
			}
		};
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.topMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.rightMargin =  getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
		
		listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
		listaSeleccion.setDelegate(delegate);
		listaSeleccion.setNumeroColumnas(1);
		listaSeleccion.setLista(listaTiposCuenta);
		listaSeleccion.setOpcionSeleccionada(-1);
		listaSeleccion.setSeleccionable(false);
		listaSeleccion.setAlturaFija(false);
		listaSeleccion.setNumeroFilas(7);
		listaSeleccion.setExisteFiltro(false);
		listaSeleccion.setSingleLine(true);
		listaSeleccion.cargarTabla();
		combo.setTitle(getString(R.string.combo_selectAccountType));
		combo.addContentView(listaSeleccion, params);
		combo.show();
	}
	
	@SuppressWarnings("deprecation")
	public void cargarComboBancos(){
		ArrayList<Object> listaTiposCuenta = delegate.getListadoBancos();
		combo = new Dialog(this);
		combo.setOnDismissListener(this);

		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.topMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.rightMargin =  getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
	
		listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
		listaSeleccion.setDelegate(delegate);
		listaSeleccion.setNumeroColumnas(1);
		listaSeleccion.setLista(listaTiposCuenta);
		listaSeleccion.setOpcionSeleccionada(-1);
		listaSeleccion.setSeleccionable(false);
		listaSeleccion.setAlturaFija(false);
		listaSeleccion.setNumeroFilas(7);
		listaSeleccion.setExisteFiltro(false);
		listaSeleccion.setSingleLine(true);
		listaSeleccion.cargarTabla();
		combo.setTitle(getString(R.string.combo_selectBank));
		combo.addContentView(listaSeleccion, params);
		combo.show();
	}
	
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}
	 
	
	@Override
	public void onClick(View v) {
		if (v == btnContinuar && !parentViewsController.isActivityChanging()) {
			
			//se valida para cuando el tipo de cuenta destino es TC ya que no debe 
			//realizar operaciones en fin de semana aun cuando la cuenta cargo tenga un celular asociado
			if (Tools.isEmptyOrNull(delegate.getTransferenciaInterbancaria().getTipoCuentaDestino())){
				InterbancariosViewController.this.showInformationAlert(R.string.transferir_interbancario_alerta_tipo_tarjeta);
			}
			else {
				boolean flag;

				if(((delegate.getTransferenciaInterbancaria().getTipoCuentaDestino().equals(Constants.TP_TC_VALUE))))
					flag=delegate.isOpenHoursTC();
				else
					flag=delegate.isOpenHours();

				if(flag)
					delegate.validaDatos();
				else
				{
					if(((delegate.getTransferenciaInterbancaria().getTipoCuentaDestino().equals(Constants.TP_TC_VALUE))))
						this.showInformationAlert(delegate.textoMensajeAlertaTC());
					else
						this.showInformationAlert(delegate.textoMensajeAlerta());
				}
			}
		}else if (v == tipoCuenta) {
			//Log.d("Interbancariosviewcontroller", "Presionaste tipo de cuenta");
			
				selectedTipoCuenta = true;
			
				cargarComboTipoCuentas();
		
			}else if (v == bancoDestino) {
			//Log.d("Interbancariosviewcontroller", "Presionaste banco destino");
			
				selectedTipoCuenta = false;
			
				bancoDestino.setEnabled(false);
			//cargarComboBancos();
			
				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showListaBancos(SHOW_LISTA_BANCOS);
			skipParentValidation = true;
		}	
		else if(v == btnAgendaInterbancarios){
				if(pidiendoContactos)
					return;
				pidiendoContactos = true;
				AbstractContactMannager.getContactManager().requestContactData(this);
		}
    }
	
	/**
	 * Obtiene los datos del contacto seleccionado.
	 * @param data Los datos del Intent.
	 */
	public void obtenerContacto(Intent data) {
		AbstractContactMannager manager = AbstractContactMannager.getContactManager();
		manager.getContactData(this, data);
		
		if(manager.getNumeroDeTelefono().equalsIgnoreCase(""))
			return;

	    txtNumeroCuenta.setText(manager.getNumeroDeTelefono());
	    txtBeneficiario.setText(manager.getNombreContacto());
	}

	public void actualizarSeleccion(Object select){
		if (select instanceof Account) {
			delegate.getTransferenciaInterbancaria().setCuentaOrigen((Account)select);
			componenteCtaOrigen.getImgDerecha().setEnabled(true);
			componenteCtaOrigen.getImgIzquierda().setEnabled(true);
			componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
			if (!delegate.getEsFrecuente()) {
				tipoCuenta.setText("");
				bancoDestino.setText("");
				delegate.getTransferenciaInterbancaria().setTipoCuentaDestino("");
				delegate.getTransferenciaInterbancaria().setInstitucionBancaria("");
			}
		}else{
			if (selectedTipoCuenta) {
				//SPEI
				if(delegate.getTransferenciaInterbancaria().getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE) && 
						   ((String)select).equals(Constants.PHONE_TYPE_ACCOUNT)) {					       
							combo.dismiss();
							showInformationAlert(R.string.transferir_interbancario_error_tdc_to_phone);
							tipoCuenta.setText("");
							return;
				//Termina SPEI
				}else if (delegate.getTransferenciaInterbancaria().getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE) && 
						((String)select).equals(Constants.DEBIT_TYPE)) {
					combo.dismiss();
					showInformationAlert(R.string.transferir_interbancario_alerta_tdc_tdd);
					tipoCuenta.setText("");
					return;
				}else if (delegate.getTransferenciaInterbancaria().getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE) && 
						((String)select).equals(Constants.CLABE_TYPE_ACCOUNT)) {
					combo.dismiss();
					showInformationAlert(R.string.transferir_interbancario_alerta_tdc_clabe);
					tipoCuenta.setText("");
					return;
				}else {
					delegate.getTransferenciaInterbancaria().setTipoCuentaDestino((String)select);
					delegate.getTransferenciaInterbancaria().setMonedaCuentaDestino(delegate.getTransferenciaInterbancaria().getCuentaOrigen().getCurrency());
					//Log.d("InterbancariosViewController", "Regrese de seleccionar algo del combo y recibi "+ (String)select);
					tipoCuenta.setText(getTipoCuenta(getResources(), (String)select));
					bancoDestino.setEnabled(true);
					bancoDestino.setText("");
					txtNumeroCuenta.setText("");
				delegate.getTransferenciaInterbancaria().setInstitucionBancaria("");
				//SPEI
				String accountTitle = "";
				if(delegate.getTransferenciaInterbancaria().getTipoCuentaDestino().equals(Constants.PHONE_TYPE_ACCOUNT)) {
					accountTitle = getString(R.string.transferir_interbancario_numero_celular);
					//boton contactos de agenda visible
					btnAgendaInterbancarios.setVisibility(View.VISIBLE);
				} else {
					accountTitle = getString(R.string.transferir_interbancario_numero_tarjeta_cuenta);
					//boton contactos de agenda invisible
					btnAgendaInterbancarios.setVisibility(View.GONE);
				}
				((TextView)findViewById(R.id.transfer_interbancario_numero_tarjeta)).setText(accountTitle);

				//Termina SPEI
				}
			}
//			else if (!selectedTipoCuenta) {
//				delegate.getTransferenciaInterbancaria().setInstitucionBancaria(((NameValuePair)select).getName());
//				String nombreBanco = ((NameValuePair)select).getValue();
//				delegate.getTransferenciaInterbancaria().setNombreBanco(nombreBanco);
//				//Log.d("InterbancariosViewController", "Regrese de seleccionar algo del combo y recibi "+ ((NameValuePair)select).getName());
//				bancoDestino.setText(((NameValuePair)select).getValue());
//				bancoDestino.setEnabled(true);
//			}
			delegate.numeroTarjeta();
			combo.dismiss();
		}
	}
	
	public String getTipoCuenta(Resources res, String type)
	{
		String tipoCuenta = "";
		if (Constants.CREDIT_TYPE.equals(type)) {
        	tipoCuenta = res.getString(R.string.cardType_credit);
        } else if (Constants.CLABE_TYPE_ACCOUNT.equals(type)) {
        	tipoCuenta = res.getString(R.string.cardType_clabe);
        } else if (Constants.DEBIT_TYPE.equals(type)) {
			tipoCuenta = res.getString(R.string.cardType_debit);
		}//SPEI
          else if (Constants.PHONE_TYPE_ACCOUNT.equals(type)) {
		tipoCuenta = res.getString(R.string.cardType_phone);
          }
		//Termina SPEI
        return tipoCuenta;
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		if (!pidiendoContactos) {
			if (!skipParentValidation && parentViewsController.consumeAccionesDeReinicio()) {
				return;
			}					
		}
		getParentViewsController().setCurrentActivityApp(this);
		skipParentValidation = false;
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		if (!pidiendoContactos) {
		     if (!skipParentValidation) {
		         parentViewsController.consumeAccionesDePausa();
		     } 
		}
	}
	
	@Override
	public void goBack() {
		if(!delegate.getEsFrecuente() || !pidiendoContactos)
		    parentViewsController.removeDelegateFromHashMap(InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID);
		super.goBack();
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		//Log.d("Interbancarios", "entro al dismiss del dialog");
		bancoDestino.setEnabled(true);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {	
		pidiendoContactos = false;
		
		super.onActivityResult(requestCode, resultCode, data);
		
		if(requestCode == SHOW_LISTA_BANCOS  && resultCode == RESULT_OK){
			Bundle extras = data.getExtras();
			String institucionBancaria = extras.getString("institucionBancaria");
			String nombreBanco = extras.getString("nombreBanco");
			delegate.getTransferenciaInterbancaria().setInstitucionBancaria(institucionBancaria);
			delegate.getTransferenciaInterbancaria().setNombreBanco(nombreBanco);			
			bancoDestino.setText(nombreBanco);
			//Log.d(getLocalClassName(), "Banco seleccionado: "+nombreBanco);
	    }
		else if(requestCode == ContactsRequestCode && resultCode == Activity.RESULT_OK ){
			obtenerContacto(data);
		}
		else if(requestCode == RESULT_CANCELED){
	    	
	    }
		bancoDestino.setEnabled(true);
		delegate.numeroTarjeta();
	}

}
