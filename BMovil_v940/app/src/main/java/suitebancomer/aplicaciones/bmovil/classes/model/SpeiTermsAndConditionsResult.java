package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class SpeiTermsAndConditionsResult implements ParsingHandler {
	/**
	 * The terms and conditions.
	 */
	private String terms;
	
	/**
	 * @return The terms and conditions.
	 */
	public String getTerms() {
		return terms;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		terms = null;
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		terms = parser.parseNextValue("textoTerminosCondiciones");
	}
}
