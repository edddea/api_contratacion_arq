package suitebancomer.aplicaciones.bmovil.classes.model;

public class ConsultaDepositosRecibidosClientesBancomer {
	private String fecha;
	private String hora;
	private String importe;
	private String nombreTitularCuentaCargo;
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getnombreTitularCuentaCargo() {
		return nombreTitularCuentaCargo;
	}
	public void setnombreTitularCuentaCargo(String nombreTitularCuentaCargo) {
		this.nombreTitularCuentaCargo = nombreTitularCuentaCargo;
	}
	ConsultaDepositosRecibidosClientesBancomer(){
		fecha="";
		hora="";
		importe="";
		nombreTitularCuentaCargo="";
	}
}
