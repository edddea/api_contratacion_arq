package suitebancomer.aplicaciones.bmovil.classes.model;

public class InhibirEEC {
	private String operacion;
	private String contrasena;
	private String token;
	private String cvv2;
	private String nip;
	private String registro;
	private String operar;
	private String visible;
	
	public InhibirEEC(){
		
	}
	
	
	public String getOperacion() {
		return operacion;
	}
	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}
	public String getNip() {
		return nip;
	}
	public void setNip(String nip) {
		this.nip = nip;
	}
	public String getRegistro() {
		return registro;
	}
	public void setRegistro(String registro) {
		this.registro = registro;
	}
	public String getOperar() {
		return operar;
	}
	public void setOperar(String operar) {
		this.operar = operar;
	}
	public String getVisible() {
		return visible;
	}
	public void setVisible(String visible) {
		this.visible = visible;
	}
	
	
}
