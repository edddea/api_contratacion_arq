package suitebancomer.aplicaciones.bmovil.classes.model;

public class Schedule {
	
	/**
	 * 	D�a de servicio en formato operaciónd�a (e.g. speidomingo)
	 */
	private String day;
	
	/**
	 * Horas de operación (e.g. 00000000)
	 */
	private String hour;
	
	
	/**
	 * 
	 * @return d�a de operación.
	 */
	public String getDay() {
		return day;
	}

	/**
	 * 
	 * @param day
	 * 			El d�a de operación.
	 */
	
	public void setDay(String day) {
		this.day = day;
	}
	
	/**
	 * 
	 * @return  horas de servicio.
	 */

	public String getHour() {
		return hour;
	}
	
	/**
	 * 
	 * @param hour
	 * 			Las horas de servicio.
	 */

	public void setHour(String hour) {
		this.hour = hour;
	}
	

}
