package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class AceptaOfertaILC implements ParsingHandler{
	String numeroTarjeta;
	String lineaActual;
	String importe;
	String lineaFinal;
	String fechaOperacion;
	String folioInternet;
	String email;
	String hora;
	String beneficiario;
	String ofertaCruzada;
	Promociones promocion;
	Promociones[] promociones;
	String PM;

		public Promociones getPromocion() {
		return promocion;
	}

	public void setPromocion(Promociones promocion) {
		this.promocion = promocion;
	}

	public Promociones[] getPromociones() {
		return promociones;
	}

	public void setPromociones(Promociones[] promociones) {
		this.promociones = promociones;
	}

	public String getOfertaCruzada() {
		return ofertaCruzada;
	}

	public void setOfertaCruzada(String ofertaCruzada) {
		this.ofertaCruzada = ofertaCruzada;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}

	public String getLineaActual() {
		return lineaActual;
	}

	public void setLineaActual(String lineaActual) {
		this.lineaActual = lineaActual;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getLineaFinal() {
		return lineaFinal;
	}

	public void setLineaFinal(String lineaFinal) {
		this.lineaFinal = lineaFinal;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getFolioInternet() {
		return folioInternet;
	}

	public void setFolioInternet(String folioInternet) {
		this.folioInternet = folioInternet;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPM() {
		return PM;
	}

	public void setPM(String pM) {
		PM = pM;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		numeroTarjeta=parser.parseNextValue("numeroTarjeta",false);
		lineaActual=parser.parseNextValue("lineaActual",false);
		importe=parser.parseNextValue("importe",false);
		lineaFinal=parser.parseNextValue("lineaFinal",false);
		fechaOperacion=parser.parseNextValue("fechaOperacion",false);
		folioInternet=parser.parseNextValue("folioInternet",false);
		email=parser.parseNextValue("email",false);
		hora=parser.parseNextValue("hora",false);
		beneficiario=parser.parseNextValue("beneficiario",false); 
		ofertaCruzada=parser.parseNextValue("ofertaCruzada",false); 
		
		PM=parser.parseNextValue("PM");
		try {
			if(ofertaCruzada!=null){
				if(!ofertaCruzada.equals("NO")){
					JSONObject jsonObject= new JSONObject(parser.parseNextValue("LO"));
					promocion= new Promociones();
					String cveCamp=jsonObject.getString("cveCamp");
					promocion.setCveCamp(cveCamp);
					String desOferta= jsonObject.getString("desOferta");
					promocion.setDesOferta(desOferta);
					String monto=jsonObject.getString("monto");
					promocion.setMonto(monto);
				}
			}
			
			if(PM.equals("SI")){
			JSONObject jsonObjectPromociones= new JSONObject(parser.parseNextValue("LP"));			
			JSONArray arrPromocionesJson =jsonObjectPromociones.getJSONArray("campanias");
			ArrayList<Promociones> arrPromociones = new ArrayList<Promociones>();
			for (int i = 0; i < arrPromocionesJson.length(); i++) {
				JSONObject jsonPromocion = (JSONObject) arrPromocionesJson.get(i);
				Promociones promo = new Promociones();
				promo.setCveCamp(jsonPromocion.getString("cveCamp"));
				promo.setDesOferta(jsonPromocion.getString("desOferta"));
				promo.setMonto(jsonPromocion.getString("monto"));
				arrPromociones.add(promo);
			}
			
			this.promociones = arrPromociones.toArray(new Promociones[arrPromociones.size()]);
			}			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			if(Server.ALLOW_LOG) e.printStackTrace();
		}
		
	}

}
