package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.AltaFrecuenteDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaDatosViewController;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class AltaFrecuenteViewController extends BaseViewController implements OnClickListener {

	EditText nombreCorto;
	//frecuente:correo electronico
	EditText correoElectronico;
	EditText contrasena;
	EditText nip;
	EditText asm;
	LinearLayout contenedorContrasena, contenedorNIP, contenedorASM;
	LinearLayout contenedorListaDatos;
	TextView instruccionesContrasena;
	TextView instruccionesNIP;
	TextView instruccionesASM;
	private TextView campoContrasena;
	private TextView campoNIP;
	private TextView campoASM;
	ListaDatosViewController listaDatos;
	ImageButton botonConfirmar;
	AltaFrecuenteDelegate altaFrecuenteDelegate;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_alta_frecuente);
		setTitle(R.string.altafrecuente_titulo, R.drawable.bmovil_alta_frecuente_icono);		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(getParentViewsController().getBaseDelegateForKey(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID));
		altaFrecuenteDelegate = (AltaFrecuenteDelegate)getDelegate();
		altaFrecuenteDelegate.setViewController(this);
		inicializar();
		configuraPantalla();
		
		nombreCorto.addTextChangedListener(new BmovilTextWatcher(this));
		contrasena.addTextChangedListener(new BmovilTextWatcher(this));
		nip.addTextChangedListener(new BmovilTextWatcher(this));
		asm.addTextChangedListener(new BmovilTextWatcher(this));
	}
	
	void inicializar(){
		nombreCorto = (EditText)findViewById(R.id.alta_frecuente_nombre_corto_edittext);
		//frecuente correo electronico
		correoElectronico = (EditText)findViewById(R.id.alta_frecuente_correo_electronico_edittext);
		contrasena = (EditText)findViewById(R.id.alta_frecuente_contrasena_edittext);
		nip = (EditText)findViewById(R.id.alta_frecuente_nip_edittext);
		asm = (EditText)findViewById(R.id.alta_frecuente_asm_edittext);
		instruccionesContrasena = (TextView)findViewById(R.id.alta_frecuente_contrasena_instrucciones_label);
		instruccionesNIP = (TextView)findViewById(R.id.alta_frecuente_nip_instrucciones_label);
		instruccionesASM = (TextView)findViewById(R.id.alta_frecuente_asm_instrucciones_label);
		contenedorContrasena = (LinearLayout)findViewById(R.id.alta_frecuente_contrasena_layout);
		contenedorNIP = (LinearLayout)findViewById(R.id.alta_frecuente_nip_layout);
		contenedorASM = (LinearLayout)findViewById(R.id.alta_frecuente_asm_layout);
		contenedorListaDatos = (LinearLayout)findViewById(R.id.alta_frecuente_lista_datos);
		
		campoContrasena = (TextView)findViewById(R.id.alta_frecuente_contrasena_label);
		campoNIP = (TextView)findViewById(R.id.alta_frecuente_nip_label);
		campoASM = (TextView)findViewById(R.id.alta_frecuente_asm_label);
		
		InputFilter[] filtroNombreCorto = {new InputFilter.LengthFilter(Constants.NOMBRE_CORTO_LENGTH)};	
		nombreCorto.setFilters(filtroNombreCorto);
		
		InputFilter[] filtroContrasena = {new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH)};	
		contrasena.setFilters(filtroContrasena);
		
		InputFilter[] filtroNIP = {new InputFilter.LengthFilter(Constants.NIP_LENGTH)};	
		nip.setFilters(filtroNIP);
		
		InputFilter[] filtroASM = {new InputFilter.LengthFilter(Constants.ASM_LENGTH)};	
		asm.setFilters(filtroASM);
		
		botonConfirmar = (ImageButton)findViewById(R.id.alta_frecuente_confirmar_button);
		botonConfirmar.setOnClickListener(this);
		nombreCorto.setSingleLine();
		//frecuente: correo electronico
		correoElectronico.setSingleLine();



	}
	
	void configuraPantalla(){		
		ArrayList<Object> lista = altaFrecuenteDelegate.getDatosTablaAltaFrecuentes();//new ArrayList<Object>();
		@SuppressWarnings("deprecation")
		LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		//params.topMargin = 18;
		params.leftMargin = getResources().getDimensionPixelOffset( R.dimen.alta_frecuente_side_margin);
		params.rightMargin =getResources().getDimensionPixelOffset( R.dimen.alta_frecuente_side_margin);

		if (listaDatos == null) {
			listaDatos = new ListaDatosViewController(this, params, parentViewsController);
			listaDatos.setNumeroCeldas(2);
			listaDatos.setLista(lista);
			listaDatos.setNumeroFilas(lista.size());
			listaDatos.setTitulo(R.string.confirmation_subtitulo);
			listaDatos.showLista();
			contenedorListaDatos.addView(listaDatos);
		}
		//elementos de seguridad		
		mostrarContrasena(altaFrecuenteDelegate.mostrarContrasenia());
		mostrarNIP(altaFrecuenteDelegate.mostrarNIP());
		mostrarASM(altaFrecuenteDelegate.tokenAMostrar());
		
		LinearLayout contenedorPadre = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);
		
		if (contenedorContrasena.getVisibility() == View.GONE &&
			contenedorNIP.getVisibility() == View.GONE &&
			contenedorASM.getVisibility() == View.GONE) {
			contenedorPadre.setBackgroundColor(0);
			nombreCorto.setImeOptions(EditorInfo.IME_ACTION_DONE);
			
		}else{
			nombreCorto.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
		
		
		//Guitools
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(nombreCorto,true);
		//frecuente correo electronico
		gTools.scale(correoElectronico, true);
		gTools.scale(contrasena,true); 
		gTools.scale(nip,true);
		gTools.scale(asm,true);
		gTools.scale(findViewById(R.id.alta_frecuente_nombre_corto_label),true);
		//frecuente correo electronico
		gTools.scale(findViewById(R.id.alta_frecuente_correo_electronico_label),true);
		gTools.scale(findViewById(R.id.alta_frecuente_contrasena_label),true);
		gTools.scale(instruccionesContrasena,true);
		gTools.scale(findViewById(R.id.alta_frecuente_nip_label),true);
		gTools.scale(instruccionesNIP,true);		
		gTools.scale(findViewById(R.id.alta_frecuente_asm_label),true);
		gTools.scale(instruccionesASM,true);
		gTools.scale(botonConfirmar);
		gTools.scale(findViewById(R.id.cambiar_password_resultado_contenedor_superior));
	}
	
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		super.goBack();
	}
	
	
	@Override
	public void onClick(View v) {
		if(v == botonConfirmar){
			//if(altaFrecuenteDelegate.validaDatos())
				altaFrecuenteDelegate.realizaAltaDeFrecuente();			
		}
	}
	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		altaFrecuenteDelegate.analyzeResponse(operationId, response);
	}
	
	/*
	* configura el campo contrasena
	*/
	public void mostrarContrasena(boolean visibility){
		contenedorContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		instruccionesContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		contrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoContrasena.setText(altaFrecuenteDelegate.getEtiquetaCampoContrasenia());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
			contrasena.setFilters(userFilterArray);
			contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		instruccionesContrasena.setVisibility(View.GONE);
	}
	
	/*
	* configura el campo nip
	*/
	public void mostrarNIP(boolean visibility){
		contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		nip.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoNIP.setText(altaFrecuenteDelegate.getEtiquetaCampoNip());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
			nip.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
			String instrucciones = altaFrecuenteDelegate.getTextoAyudaNIP();
			if (instrucciones.equals("")) {
				instruccionesNIP.setVisibility(View.GONE);
			} else {
				instruccionesNIP.setVisibility(View.VISIBLE);
				instruccionesNIP.setText(instrucciones);
			}
		} else {
			nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}
	
	/*
	 * muestra el asm con el texto correspondiente
	 */
	public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){
		switch(tipoOTP) {
			case ninguno:
				contenedorASM.setVisibility(View.GONE);
				campoASM.setVisibility(View.GONE);
				asm.setVisibility(View.GONE);
				asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
				break;
			case codigo:
			case registro:
				contenedorASM.setVisibility(View.VISIBLE);
				campoASM.setVisibility(View.VISIBLE);
				asm.setVisibility(View.VISIBLE);
				InputFilter[] userFilterArray = new InputFilter[1];
				userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
				asm.setFilters(userFilterArray);
				cambiarAccionTexto(contrasena);
				cambiarAccionTexto(nip);
				asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
		}
		
		Constants.TipoInstrumento tipoInstrumento = altaFrecuenteDelegate.consultaTipoInstrumentoSeguridad();

		switch (tipoInstrumento) {
			case OCRA:
				campoASM.setText(altaFrecuenteDelegate.getEtiquetaCampoOCRA());
				//asm.setTransformationMethod(null);
				break;
			case DP270:
				campoASM.setText(altaFrecuenteDelegate.getEtiquetaCampoDP270());
				//asm.setTransformationMethod(null);
				break;
			case SoftToken:
				if (SuiteApp.getSofttokenStatus()) {
					asm.setText(Constants.DUMMY_OTP);
					asm.setEnabled(false);
					campoASM.setText(altaFrecuenteDelegate.getEtiquetaCampoSoftokenActivado());
				} else {
					campoASM.setText(altaFrecuenteDelegate.getEtiquetaCampoSoftokenDesactivado());
					//asm.setTransformationMethod(null);
				}
				break;
			default:
				break;
		}

		String instrucciones = altaFrecuenteDelegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);


		if (instrucciones.equals("")) {
			instruccionesASM.setVisibility(View.GONE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}		
	}
	
	/*
	 * 
	 *
	 */
	private void cambiarAccionTexto(EditText campo) {
		if (campo.getVisibility() == View.VISIBLE) {
			campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
	}
	
	/*
	 * retorna el texto ingresado en el editText contrasena o empty segun su visibilidad 
	 */
	public String pideContrasena() {
		if (contrasena.getVisibility() == View.GONE) {
			return "";
		} else {
			return contrasena.getText().toString();
		}
	}
	
	/*
	 * retorna el texto ingresado en el editText nip o empty segun su visibilidad
	 */
	public String pideNIP() {
		if (nip.getVisibility() == View.GONE) {
			return "";
		} else {
			return nip.getText().toString();
		}
	}
	
	/*
	 * retorna el texto ingresado en el editText asm o empty segun su visibilidad
	 */
	public String pideASM() {
		if (asm.getVisibility() == View.GONE) {
			return "";
		} else {
			return asm.getText().toString();
		}
	}
	
	public String getNombreCorto(){
		return (nombreCorto.length() > 0 ) ? nombreCorto.getText().toString() : "";
	}

	//frecuente: correo electronico

	public String getCorreoElectronico() {
		return (correoElectronico.length() > 0 ) ? correoElectronico.getText().toString() : "";
	}
		
}
