package suitebancomer.aplicaciones.bmovil.classes.model;

public class FrecuenteMulticanalData {
	
	 /**
     * 
     */
//    private String numCliente;
//    
    /**
     * Id del canal
     */
    private String idCanal;
    
    /**
     * Usuario 
     */
    private String usuario;

	//frecuente correo electronico

	private String correoFrecuente;

	/**
     * 
     */
//    private String funcion;
    
    /**
     * 
     */
//    private String subcanal;
    
    /**
     * 
     */
//    private String entidad;
    
    /**
     * 
     */
//    private String nombrePago;
    
    /**
	 * @return the numCliente
	 */
//	public String getNumCliente() {
//		return numCliente;
//	}

	/**
	 * @param numCliente the numCliente to set
	 */
//	public void setNumCliente(String numCliente) {
//		this.numCliente = numCliente;
//	}

    /**
     * 
     * @return CN
     */
	public String getIdCanal() {
		return idCanal;
	}

	public void setIdCanal(String idCanal) {
		this.idCanal = idCanal;
	}

	/**
	 * 
	 * @return US
	 */
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	//frecuente correo electronico
	public String getCorreoFrecuente() {
		return correoFrecuente;
	}

	public void setCorreoFrecuente(String correoFrecuente) {
		this.correoFrecuente = correoFrecuente;
	}

//	public String getFuncion() {
//		return funcion;
//	}
//
//	public void setFuncion(String funcion) {
//		this.funcion = funcion;
//	}
//
//	public String getSubcanal() {
//		return subcanal;
//	}
//
//	public void setSubcanal(String subcanal) {
//		this.subcanal = subcanal;
//	}
//
//	public String getEntidad() {
//		return entidad;
//	}
//
//	public void setEntidad(String entidad) {
//		this.entidad = entidad;
//	}
//
//	public String getNombrePago() {
//		return nombrePago;
//	}
//
//	public void setNombrePago(String nombrePago) {
//		this.nombrePago = nombrePago;
//	}
//
//	public FrecuenteMulticanalData() {
//		numCliente = "";
//	    idCanal = "";
//	    usuario = "";
//	    funcion = "";
//	    subcanal = "";
//	    entidad = "";
//	    nombrePago = "";
//	}
}
