package suitebancomer.aplicaciones.bmovil.classes.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import android.content.Context;
import android.util.Log;

import com.bancomer.mbanking.SuiteApp;

public class TemporalSTFileManager {
	// #region Constantes.
	/**
	 * Nombre del archivo de propiedades.
	 */
	private static final String TEMPORALST_FILE_NAME = "TemporalST.prop";
	
	/**
	 * Nombre de la propiedad que determina el Celular
	 */
	public static final String PROP_TEMPORALST_CELULAR = "celular";
	
	/**
	 * Nombre de la propiedad que determina el numero de tarjeta
	 */
	public static final String PROP_TEMPORALST_TARJETA = "tarjeta";
	
	/**
	 * Nombre de la propiedad que determina la compa�ia
	 */
	public static final String PROP_TEMPORALST_COMPANIA = "compania";
	
	/**
	 * Nombre de la propiedad que determina la contraseña
	 */
	public static final String PROP_TEMPORALST_CONTRASENA = "contrasena";
	
	/**
	 * Nombre de la propiedad que determina el correo
	 */
	public static final String PROP_TEMPORALST_CORREO = "correo";
	
	/**
	 * Nombre de la propiedad que determina el tipo de perfil
	 */
	public static final String PROP_TEMPORALST_PERFIL = "perfil";
	
	// #endregion
	
	// #region Variables.
	/**
	 * Propiedades de la aplicacion.
	 */
	private Properties properties;
	// #endregion
	
	// #region Singleton.
	/**
	 * La instancia de la clase.
	 */
	private static TemporalSTFileManager manager = null;
	
	/**
	 * @return La instancia de la clase.
	 */
	public static TemporalSTFileManager getCurrent() {
		if(null == manager)
			manager = new TemporalSTFileManager();
		return manager;
	}
	
	/**
	 * Inicializa el administrador de propiedades. 
	 */
	private TemporalSTFileManager() {
		properties = null;
		File file = new File(SuiteApp.appContext.getFilesDir(), TEMPORALST_FILE_NAME);
		
		if(!file.exists())
			initTemporalSTFile(file);
		else
			loadTemporalSTFile();
	}
	// #endregion
	
	// #region Carga del archivo de propiedades.
	/**
	 * Inicializa el archivo de propiedades con los valores iniciales para cada propiedad.
	 * @param file
	 */
	private void initTemporalSTFile(File file) {
		try {
			file.createNewFile();
		} catch(IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al crear el archivo TemporalST.", ioEx);
			return;
		}
				
		loadTemporalSTFile();
		
		if(null != properties) {
			setPropertynValue(PROP_TEMPORALST_CELULAR, "");
			setPropertynValue(PROP_TEMPORALST_TARJETA, "");
			setPropertynValue(PROP_TEMPORALST_COMPANIA, "");
			setPropertynValue(PROP_TEMPORALST_CONTRASENA, "");
			setPropertynValue(PROP_TEMPORALST_CORREO, "");
			setPropertynValue(PROP_TEMPORALST_PERFIL, "");
		}
	}
	
	/**
	 * Carga el archivo de propiedades.
	 */
	private void loadTemporalSTFile() {
		InputStream input;
		try {
			input = SuiteApp.appContext.openFileInput(TEMPORALST_FILE_NAME);
		} catch(FileNotFoundException fnfEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargar el archivo TemporalST para lectura.", fnfEx);
			return;
		}

		properties = new Properties();
		try {
			properties.load(input);
		} catch(IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargar TemporalST.", ioEx);
			properties = null;
			return;
		}
	}
	// #endregion
	
	// #region Generic getters and setters for properties.
	/**
	 * Establece el valor especificado a la propiedad indicada.
	 * @param propertyName El nombre de la propiedad.
	 * @param value El valor a establecer.
	 */
	private void setPropertynValue(String propertyName, String value) {
		if(null == properties || Tools.isEmptyOrNull(propertyName))
			return;
		if(null == value)
			value = "";
		
		properties.setProperty(propertyName, value);
		storeFileForProperty(propertyName, value);
	}
	
	/**
	 * Obtiene el valor de la propiedad especificada.
	 * @param propertyName El nombre de la propiedad.
	 * @return El valor de la propiedad.
	 */
	private String getPropertynValue(String propertyName) {
		if(null == properties)
			return null;
		
		String value = properties.getProperty(propertyName);
		return value;
	}
	// #endregion
	
	// #region Setters y Getters del estatus de activación para las aplicaciones.	
	public String getTemporalSTCelular(){
		return getPropertynValue(PROP_TEMPORALST_CELULAR);
	}
	
	public String getTemporalSTTarjeta(){
		return getPropertynValue(PROP_TEMPORALST_TARJETA);
	}
	
	public String getTemporalSTCompania(){
		return getPropertynValue(PROP_TEMPORALST_COMPANIA);
	}
	
	public String getTemporalSTContrasena(){
		return getPropertynValue(PROP_TEMPORALST_CONTRASENA);
	}
	
	public String getTemporalSTCorreo(){
		return getPropertynValue(PROP_TEMPORALST_CORREO);
	}
	
	public String getTemporalSTPerfil(){
		return getPropertynValue(PROP_TEMPORALST_PERFIL);
	}
		
	public void setTemporalSTCelular(String value){
		setPropertynValue(PROP_TEMPORALST_CELULAR, value);
	}
	
	public void setTemporalSTTarjeta(String value){
		setPropertynValue(PROP_TEMPORALST_TARJETA, value);
	}
	
	public void setTemporalSTCompania(String value){
		setPropertynValue(PROP_TEMPORALST_COMPANIA, value);
	}
	
	public void setTemporalSTContrasena(String value){
		setPropertynValue(PROP_TEMPORALST_CONTRASENA, value);
	}
	
	public void setTemporalSTCorreo(String value){
		setPropertynValue(PROP_TEMPORALST_CORREO, value);
	}
	
	public void setTemporalSTPerfil(String value){
		setPropertynValue(PROP_TEMPORALST_PERFIL, value);
	}
	// #endregion	
	
	/**
	 * Guarda el archivo de propiedades para preservar el cambio en alguna de las propiedades. 
	 * El nombre y valor de la propiedad modificada con usados para registrar el proceso mediante el log.  
	 * @param propertyName El nombre de la propiedad a guardar.
	 * @param propertyValue El valor de la propiedad a guardar.
	 * @return True si el archivo fue guardado exitosamente, False de otro modo.
	 */
	private boolean storeFileForProperty(String propertyName, String propertyValue)	{
		if(Tools.isEmptyOrNull(propertyName))
			propertyName = "No name indicated.";
		if(Tools.isEmptyOrNull(propertyValue))
			propertyName = "No value indicated.";
		
		OutputStream output;
		try {	
			output = SuiteApp.appContext.openFileOutput(TEMPORALST_FILE_NAME, Context.MODE_PRIVATE);
		} catch(FileNotFoundException fnfEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al abrir el archivo para guardar la propiedad " + propertyName, fnfEx);
			return false;
		}
		
		try {
			properties.store(output, null);
		} catch (IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al guardar en el archivo TemporalST los valores: " + propertyName + " - " + propertyValue, ioEx);
			return false;
		}
		
		if(Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Archivo actualizado con los siguientes cambios: " + propertyName + " - " + propertyValue);
		return true;
	}

	public static void reloadFile(){
		manager=null;
	}
}
