package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetalleOfertaILCDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuPrincipalDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import tracking.TrackingHelper;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

public class DetalleILCViewController extends BaseViewController {

	private TextView lblTexto2;
	private TextView lblTexto3;
	private TextView lblTexto4;
	private TextView lblTexto5;
	private TextView lblTexto6;
	private TextView lblTexto7;
	private TextView lblTexto8;	
	private ImageButton btnAceptar;
	BaseViewController me;
	private DetalleOfertaILCDelegate detalleOfertaILCDelegate;
	public CuentaOrigenViewController componenteCtaOrigen;
	LinearLayout vista;
	LinearLayout vistaCtaOrigen;
	boolean isNavegarPantallaSig=false;
	boolean isBackground=false;
	private TextView lblTitleCuentaOrigen;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	//ARR
	Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_detalleilc);
		me=this;
		
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("detalle ilc", parentManager.estados);
		//AMZ		
		
		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE));
		detalleOfertaILCDelegate = (DetalleOfertaILCDelegate)getDelegate();
		detalleOfertaILCDelegate.setControladorDetalleILCView(this);	
		setTitle(R.string.bmovil_pantallailc_title, R.drawable.icono_pagar_servicios);
		vista = (LinearLayout)findViewById(R.id.detalleofertaILC_view_controller_layout);
		vistaCtaOrigen = (LinearLayout)findViewById(R.id.ofertaILC_cuenta_origen_view);
		detalleOfertaILCDelegate.formatoFechaCatMostrar();
		
		lblTexto2 = (TextView) findViewById(R.id.lblTexto2);
		lblTexto3 = (TextView) findViewById(R.id.lblTexto3);
		lblTexto4 = (TextView) findViewById(R.id.lblTexto4);
		lblTexto5 = (TextView) findViewById(R.id.lblTexto5);
		lblTexto6 = (TextView) findViewById(R.id.lblTexto6);
		lblTexto7 = (TextView) findViewById(R.id.lblTexto7);
		lblTexto8 = (TextView) findViewById(R.id.lblTexto8);		
		
		lblTexto8.setText(R.string.bmovil_pantallailc_texto1);
		String mystring = Tools.formatAmount(detalleOfertaILCDelegate.getOfertaILC().getImporte()+"00",false);
		SpannableString content = new SpannableString(mystring);
		content.setSpan(new UnderlineSpan(), 0, mystring.length(), 0);
		lblTexto7.setText(content);
		lblTexto6.setText(R.string.bmovil_pantallailc_texto2);
		lblTexto5.setText(Tools.formatAmount(detalleOfertaILCDelegate.getOfertaILC().getLineaFinal()+"00",false));
		lblTexto4.setText(R.string.bmovil_pantallailc_texto3);
		lblTexto3.setText(R.string.bmovil_pantallailc_texto4);
		lblTexto2.setText("CAT "+ detalleOfertaILCDelegate.getOfertaILC().getCat()+"%"+" Sin I.V.A informativo.\nFecha de cálculo al "+ detalleOfertaILCDelegate.fechaCat+ "\nConsulta términos y condiciones en \nwww.bancomer.com.");
		btnAceptar = (ImageButton)findViewById(R.id.detalleOferta_boton_continuar);
		btnAceptar.setOnClickListener(clickListener);
		cargarCuentas();
		configurarPantalla();
		
		}
	
	OnClickListener clickListener=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			//AMZ
			Map<String,Object> OperacionMap = new HashMap<String, Object>();
			// TODO Auto-generated method stub
			if(v==btnAceptar){
				isNavegarPantallaSig=true;
				
				//AMZ
				OperacionMap.put("evento_realizada","event52");
				OperacionMap.put("&&products","promociones;detalle ilc+exito ilc");
				OperacionMap.put("eVar12","operacion realizada oferta ilc");
				TrackingHelper.trackOperacionRealizada(OperacionMap);
				
				detalleOfertaILCDelegate.realizaOperacion(Server.ACEPTACION_OFERTA, me);
			}
		}
	};
	
	private void configurarPantalla() {
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());			
		gTools.scale(lblTexto2, true);
		gTools.scale(lblTexto3, true);
		gTools.scale(lblTexto4, true);
		gTools.scale(lblTexto5, true);
		gTools.scale(lblTexto6, true);
		gTools.scale(lblTexto7, true);
		gTools.scale(lblTexto8, true);
		gTools.scale(btnAceptar);
		gTools.scale(findViewById(R.id.ofertaILC_cuenta_origen_view));
		
	}
	
	@SuppressWarnings("deprecation")
	public void cargarCuentas(){
		ArrayList<Account> listaCuetasAMostrar = detalleOfertaILCDelegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.setDelegate(detalleOfertaILCDelegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(detalleOfertaILCDelegate.getOfertaILC().getAccount()));
		componenteCtaOrigen.init();
		componenteCtaOrigen.actualizaComponente(true);
		lblTitleCuentaOrigen=componenteCtaOrigen.getTituloComponenteCtaOrigen();
		lblTitleCuentaOrigen.setVisibility(View.GONE);
		vistaCtaOrigen.addView(componenteCtaOrigen);
	}
	
	public void goBack() {
		
		//ARR
		paso1OperacionMap.put("evento_paso1", "event46");
		paso1OperacionMap.put("&&products", "promociones;detalle ilc");
		paso1OperacionMap.put("eVar12", "paso1:rechazo oferta ilc");

		TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
		// TODO Auto-generated method stub
		isNavegarPantallaSig=true;
		detalleOfertaILCDelegate.realizaOperacion(Server.RECHAZO_OFERTA, me);	
		
		

	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);

	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if(!isNavegarPantallaSig){
		detalleOfertaILCDelegate.realizaOperacion(Server.RECHAZO_OFERTA, me);
		parentViewsController.consumeAccionesDePausa();
		isBackground=true;
		
		}
	}
	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		detalleOfertaILCDelegate.analyzeResponse(operationId, response);
	}
	
}
