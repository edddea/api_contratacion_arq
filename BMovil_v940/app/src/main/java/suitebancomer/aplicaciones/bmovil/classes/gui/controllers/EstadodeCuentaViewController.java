package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.common.AbstractContactMannager;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.EstadodeCuentaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MovimientosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaEC;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaECExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.Movement;
import suitebancomer.aplicaciones.bmovil.classes.model.MovementExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.Periodo;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaDatosViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import tracking.TrackingHelper;

import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.R;

/**
 * @author Carlos Santoyo
 */

public class EstadodeCuentaViewController extends BaseViewController implements OnClickListener {

	private LinearLayout vista;
	private LinearLayout contenedorPrincipal;
	private TextView lblPeriodo;
	public  Button comboPeriodo;
	private ImageButton btnContinuar;
	private TextView lblCorreo;
	//public TextView txtCorreoElectronico;
	private EstadodeCuentaDelegate estadodeCuentaDelegate;
	public CuentaOrigenViewController componenteCtaOrigen;
	public BmovilViewsController parentManager;
	private Periodo periodoSeleccionado = null;

	ArrayList<Periodo> periodos;
	
	/**
	 * Default constructor for this activity
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_consulta_estado_de_cuenta);
		setTitle(R.string.consultar_estados_de_cuenta_title, R.drawable.bmovil_consultar_icono);
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
			
				TrackingHelper.trackState("movimientos", parentManager.estados);

		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(EstadodeCuentaDelegate.ESTADODECUENTA_DELEGATE_ID));
		estadodeCuentaDelegate = (EstadodeCuentaDelegate)getDelegate(); 
		estadodeCuentaDelegate.setEstadodeCuentaViewController(this);
		init();
		showCuentaOrigen();
		estadodeCuentaDelegate.cargarPeriodos();
		
	}
	

	/**
	 * Busca las vistas usadas y guarda sus referencias.
	 */
	private void findViews(){
		lblPeriodo = (TextView)findViewById(R.id.lblPeriodo);
		comboPeriodo = (Button)findViewById(R.id.CboPeriodo);
		btnContinuar = (ImageButton)findViewById(R.id.estado_de_cuenta_boton_continuar);
		btnContinuar.setOnClickListener(this);
		lblCorreo = (TextView)findViewById(R.id.lblCorreo);
		//txtCorreoElectronico = (TextView)findViewById(R.id.txtCorreoElectronico);
		vista = (LinearLayout)findViewById(R.id.estado_de_cuenta_layout);
		contenedorPrincipal = (LinearLayout)findViewById(R.id.contenedorPrincipal);
	}
	
	/**
	 * Redimensiona las vistas para que mantegan la relacion de aspecto en cualquier pantalla.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scalePaddings(contenedorPrincipal);
		guiTools.scale(lblPeriodo, true);
		guiTools.scale(comboPeriodo, true);
		guiTools.scale(lblCorreo, true);
		//guiTools.scale(txtCorreoElectronico, true);
		guiTools.scale(btnContinuar);
		guiTools.scale(vista);
	}
	
	private void init() {
		findViews();
		scaleForCurrentScreen();

		/*
		txtCorreoElectronico.setSelected(true);
		
		txtCorreoElectronico.setText(Session.getInstance(SuiteApp.appContext).getEmail());
		if(0 == lblCorreo.getText().toString().length()) {
			View correoLayout = findViewById(R.id.txtCorreoElectronico);
			if(null != correoLayout)
				correoLayout.setVisibility(View.GONE);
		}
		*/
	
	}
	
	@SuppressWarnings("deprecation")
	public void showCuentaOrigen(){
	    ArrayList<Account> listaCuetasAMostrar = estadodeCuentaDelegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
		componenteCtaOrigen.setDelegate(estadodeCuentaDelegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.init();
		vista.addView(componenteCtaOrigen);
	}
	

	
	public void onPeriodoClick(View view) {
		estadodeCuentaDelegate.setEstadodeCuentaViewController(this);
		//estadodeCuentaDelegate.cargarPeriodos();
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		int numPeriodos = 0;
		if(periodos != null) 
			numPeriodos = periodos.size();
		final String[] period = new String[numPeriodos];
		if(numPeriodos > 0 ){
			for(int i = 0; i < numPeriodos; i++)
				period[i] = Tools.parsePeriodoFecha(periodos.get(i).getPeriodo()) + " - " + periodos.get(i).getFechaCorte();
			
			builder.setTitle(R.string.bmovil_consultar_dineromovil_movimientos_tipo_titulo).setItems(period, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					comboPeriodo.setText(period[which]);
					Log.d("WHIC"," " + which );
					periodoSeleccionado = periodos.get(which);
					Log.d("WaHIC", periodoSeleccionado.getReferencia());
				}
			}).show();
		}else{
			showInformationAlertEspecial("Aviso", "arreglo vacio", "Aun no se generan periodos", null);
		}
	}
	
	@SuppressWarnings("deprecation")
	public void llenaListaDatos(){
		if (estadodeCuentaDelegate.getTotalPeriodos() != null) {
			this.anadirPeriodos(estadodeCuentaDelegate.getTotalPeriodos());
		}
		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
		ocultaIndicadorActividad();
	}
	
	private void anadirPeriodos(ConsultaECExtract ec) {
		//if (periodos == null) {
			periodos = ec.getPeriodos();
		//}
	}
	public CuentaOrigenViewController getComponenteCtaOrigen() {
		return componenteCtaOrigen;
	}
	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		estadodeCuentaDelegate.analyzeResponse(operationId, response);
	}
		
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		setHabilitado(true);
		getParentViewsController().setCurrentActivityApp(this);
		if (estadodeCuentaDelegate != null) {
		    estadodeCuentaDelegate.setCallerController(this);
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		super.goBack();
	}


	public void borrarListaPeriodos() {
		comboPeriodo.setText("");
		
	}

	@Override
	public void onClick(View v) {
	   
			if  (v == btnContinuar && !parentViewsController.isActivityChanging() ) {
				if(!estadodeCuentaDelegate.validarDatos()) return;
				
			    ConsultaEC consultaEC = new ConsultaEC ();
			    consultaEC.setCuentaOrigen(estadodeCuentaDelegate.getCuentaSeleccionada());
			    //consultaEC.setPeriodo(periodoSeleccionado.getPeriodo());
			    consultaEC.setPeriodo(Tools.parsePeriodoFecha(periodoSeleccionado.getPeriodo()));
			    consultaEC.setReferencia(periodoSeleccionado.getReferencia());
			    consultaEC.setFechaCorte(periodoSeleccionado.getFechaCorte());
			    //consultaEC.seteMail(txtCorreoElectronico.getText().toString());
			    consultaEC.seteMail(Session.getInstance(SuiteApp.appContext).getEmail());
			    
			    estadodeCuentaDelegate.setConsultaEC(consultaEC);
			    estadodeCuentaDelegate.setCallerController(this);
			    setHabilitado(false);
				estadodeCuentaDelegate.showConfirmacion();
	   
			} if(estadodeCuentaDelegate.res) {
				//ARR
				Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
				
				//ARR
				paso2OperacionMap.put("evento_paso2", "event47");
				paso2OperacionMap.put("&&products", "operaciones;consultar estado de cuenta");
				paso2OperacionMap.put("eVar12", "paso2:revisa y autoriza");

				TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
			}
	}
	
	
	
	
}

