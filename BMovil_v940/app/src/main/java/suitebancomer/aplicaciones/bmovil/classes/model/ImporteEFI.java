package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ImporteEFI implements ParsingHandler {
	String pagoMensual;
	String comisionEFIMonto;
	
	
	public String getPagoMensual() {
		return pagoMensual;
	}

	public void setPagoMensual(String pagoMensual) {
		this.pagoMensual = pagoMensual;
	}

	public String getComisionEFIMonto() {
		return comisionEFIMonto;
	}

	public void setComisionEFIMonto(String comisionEFIMonto) {
		this.comisionEFIMonto = comisionEFIMonto;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		pagoMensual=parser.parseNextValue("pagoMensual"); 
		comisionEFIMonto=parser.parseNextValue("ComisionEFIMonto");
		
	}

}
