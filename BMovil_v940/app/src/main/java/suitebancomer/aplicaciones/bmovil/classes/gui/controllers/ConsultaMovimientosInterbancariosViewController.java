package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.ErrorData;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaMovimientosInterbancariosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaSPEIData;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;

/**
 * Created by Nicolas Arriaza on 04/08/2015.
 */
public class ConsultaMovimientosInterbancariosViewController extends BaseViewController implements View.OnClickListener{

    private ConsultaMovimientosInterbancariosDelegate delegate;

    private CuentaOrigenViewController componenteCtaOrigen;

    private LinearLayout layoutCuentaOrigen;
    private LinearLayout contenedorListaInterbancarios;
    private TextView btnConsultar;

    private ListaSeleccionViewController listaFrecuentes;

    public CuentaOrigenViewController getComponenteCtaOrigen() {
        return componenteCtaOrigen;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_consulta_movimientos_interbancarios);
        setTitle(R.string.bmovil_consulta_movimientos_interbancarios_title, R.drawable.bmovil_consultar_icono);

        setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());

        delegate = (ConsultaMovimientosInterbancariosDelegate)parentViewsController.getBaseDelegateForKey(ConsultaMovimientosInterbancariosDelegate.CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID);
        if(delegate == null){
            delegate = new ConsultaMovimientosInterbancariosDelegate();
            parentViewsController.addDelegateToHashMap(ConsultaMovimientosInterbancariosDelegate.CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID, delegate);
        }
        setDelegate(delegate);
        delegate.setViewController(this);

        initViews();
        configurarPantalla();

        muestraComponenteCuentaOrigen();
        muestraListaMovimientos();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
        if (delegate != null) {
            delegate.setViewController(this);

            if (delegate.getErrorData() != null) {
                ErrorData errorData = delegate.getErrorData();
                showInformationAlertEspecial(errorData.getErrorTitle(), errorData.getErrorCode(), errorData.getErrorDescription(), null);
                delegate.setErrorData(null);
                ocultaMasMovimientos();
            } else {
                delegate.isPeriodoExceded(String.valueOf(delegate.getPeriodo()));
                ConsultaSPEIData data = delegate.getConsultaSPEIData();

                if ((data == null) ||
                        (data != null && data.getTransferencias() == null) ||
                        (data != null && data.getTransferencias() != null && data.getTransferencias().isEmpty())) {

                    ocultaMasMovimientos();
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    public void goBack() {
        super.goBack();
    }

    @Override
    protected void onDestroy() {
        parentViewsController.removeDelegateFromHashMap(ConsultaMovimientosInterbancariosDelegate.CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID);
        super.onDestroy();
    }

    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }

    private void initViews(){
        layoutCuentaOrigen = (LinearLayout)findViewById(R.id.contenedor_lista_cuentas_movimientos_interbancarios);
        contenedorListaInterbancarios = (LinearLayout)findViewById(R.id.contenedor_lista_movimientos_interbancarios);
        btnConsultar=(TextView)findViewById(R.id.btnConsultarMovimientosInterbancarios);
        btnConsultar.setOnClickListener(this);
    }

    private void configurarPantalla(){
        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(findViewById(R.id.consultar_movimientos_interbancarios_contenedor_principal));
        gTools.scale(findViewById(R.id.btnConsultarMovimientosInterbancarios));
    }

    public void muestraComponenteCuentaOrigen(){
        delegate.cargaCuentasOrigen();
        ArrayList<Account> listaCuetasAMostrar = delegate.getListaCuentaOrigen();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
        componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.bmovil_consulta_movimientos_interbancarios_cuentaorigen_title));
        componenteCtaOrigen.setDelegate(delegate);
        componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);

        if(delegate.getCuentaSeleccionada() != null){
            componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(delegate.getCuentaSeleccionada()));
        }else{
            componenteCtaOrigen.setIndiceCuentaSeleccionada(0);
        }

        componenteCtaOrigen.init();
        //FIXME niko do it
        componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.transferir);

        layoutCuentaOrigen.addView(componenteCtaOrigen);
    }

    public void actualizaCuentaOrigen(Account cuenta){
        if(Server.ALLOW_LOG) Log.d("TransferirMisCuentasDetalleViewController", "Actualizar la cuenta origen actual por  " + cuenta.getNumber());
        delegate.setCuentaSeleccionada(cuenta);
        componenteCtaOrigen.getImgDerecha().setEnabled(true);
        componenteCtaOrigen.getImgIzquierda().setEnabled(true);
        componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
    }

    public void muestraListaCuentas(){
        componenteCtaOrigen.getImgDerecha().setEnabled(true);
        componenteCtaOrigen.getImgIzquierda().setEnabled(true);
        componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
    }

    private void muestraListaMovimientos() {
        if (listaFrecuentes == null) {
            cargarLista();
        }
    }

    public void cargarLista(){

        contenedorListaInterbancarios.removeAllViews();

        LinearLayout.LayoutParams params;
        params = new LayoutParams(LayoutParams.FILL_PARENT,	LayoutParams.WRAP_CONTENT);

        listaFrecuentes = new ListaSeleccionViewController(this, params,parentViewsController);
        listaFrecuentes.setDelegate(delegate);

        ArrayList<Object> listaEncabezado = delegate.getDatosHeaderTablaInterbancarios();
        ArrayList<Object> listaDatos = delegate.getInterbancarios();
        listaFrecuentes.setEncabezado(listaEncabezado);
        listaFrecuentes.setLista(listaDatos);
        listaFrecuentes.setNumeroColumnas(listaEncabezado.size() - 1) ;
        if (listaDatos.size() == 0) {
            listaFrecuentes.setTextoAMostrar("");
            listaFrecuentes.setForzarListaVacia(true);
        }else {
            listaFrecuentes.setTextoAMostrar(null);
            listaFrecuentes.setNumeroFilas(listaDatos.size());
            listaFrecuentes.setForzarListaVacia(false);
        }
        listaFrecuentes.setOpcionSeleccionada(-1);
        listaFrecuentes.setSeleccionable(false);
        listaFrecuentes.setAlturaFija(true);
        listaFrecuentes.setNumeroFilas(listaDatos.size());
        listaFrecuentes.setExisteFiltro(true);
        listaFrecuentes.setTitle(getString(R.string.bmovil_consulta_movimientos_interbancarios_movimientos_title));
        listaFrecuentes.setSubTitle(getString(R.string.bmovil_consulta_movimientos_interbancarios_movimientos_subtitle));
        listaFrecuentes.setSingleLine(true);
        listaFrecuentes.cargarTabla();
        contenedorListaInterbancarios.addView(listaFrecuentes);
    }

    public void ocultaMasMovimientos(){
        btnConsultar.setVisibility(View.GONE);
    }

    public void muestraMasMovimientos(){
        btnConsultar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        if(v.equals(btnConsultar)){
            delegate.masMovimientosAction();
        }
    }
}
