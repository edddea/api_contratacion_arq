package suitebancomer.aplicaciones.bmovil.classes.common;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.Catalog;
import android.util.Log;

import com.bancomer.mbanking.SuiteApp;

public class CatFileManager {
	
	/**
	 * Nombre del archivo de catalogos.
	 */
	private static final String FILE_NAME = "Cat.prop";
	
	/**
	 * Manejador del archivo de catalogos.
	 */
	private File file;
			
	/**
	 * La instancia de la clase.
	 */
	private static CatFileManager manager = null;
	
	/**
	 * @return La instancia de la clase.
	 */
	public static CatFileManager getCurrent() {
		if(null == manager)
			manager = new CatFileManager();
		return manager;
	}
	
	/**
	 * Inicializa el administrador de propiedades. 
	 */
	private CatFileManager() {
		this.file = new File(SuiteApp.appContext.getFilesDir(), FILE_NAME);
		
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch(IOException ioEx) {
				if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error al crear el archivo Cat.", ioEx);
				return;
			}
		}
	}
	
	/**
	 * Guarda el archivo de catálogos.
	 */
	public void guardaCatalogos(Catalog[] catalogs) {
		ObjectOutputStream os = null;
		try {
			os = new ObjectOutputStream(new FileOutputStream(this.file));		
			os.writeObject(catalogs);
			if(Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Catalogos guardados.");
		} catch (Exception e) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error en guardaCatalogos.", e);
		} finally{
			try {
				if(null != os){
					os.close();
				}
			} catch (Exception e) {
				if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error al cerrar en guardaCatalogos.", e);
			}
		}
	}
	
	/**
	 * Carga el archivo de catalogos.
	 */
	public Catalog[] cargaCatalogos(){
		Catalog[] catalogs = null;
		ObjectInputStream is = null;
		try {
			is = new ObjectInputStream(new FileInputStream(file));
			catalogs = (Catalog[]) is.readObject();
			if(Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Catalogos cargados.");
		} catch (EOFException e) {
			if(Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "El archivo de Catalogos esta vacio.");
		} catch (StreamCorruptedException e) {
			if(Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "El archivo de Catalogos es de una version antigua e ilegible.");
		} catch (Exception e) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error en cargaCatalogos.", e);
		} finally {
			try{
				if(null != is){
					is.close();
				}
			} catch (Exception e) {
				if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error al cerrar en cargaCatalogos.", e);
			}
		}
		return catalogs;
	}
	
	/**
	 * Vacía el archivo de catalogos.
	 */
	public void vaciaArchivoCatalogos() {
		ObjectOutputStream os = null;
		try {
			os = new ObjectOutputStream(new FileOutputStream(this.file));
			if(Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Catalogos vaciados.");
		} catch (Exception e) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error en vaciaArchivoCatalogos.", e);
		} finally{
			try {
				if(null != os){
					os.close();
				}
			} catch (Exception e) {
				if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error al cerrar en vaciaArchivoCatalogos.", e);
			}
		}
		
	}
	/*
	* Metodo que sete el manager a nulo
	* con la finalidad de que la proxima vez que
	* se llame el archivo este se vea obligado a leer el archivo nuevamente
	* */
	public static void reloadFile(){
		manager=null;
	}
}
