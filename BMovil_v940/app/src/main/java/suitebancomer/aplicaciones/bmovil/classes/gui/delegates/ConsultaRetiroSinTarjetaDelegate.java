package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultaRetiroSinTarjetaViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.BajaDineroMovilData;
import suitebancomer.aplicaciones.bmovil.classes.model.Clave12DigitosResult;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDineroMovil;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaRetiroSinTarjeta;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaRetiroSinTarjetaData;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjetaResult;
import suitebancomer.classes.gui.controllers.BaseViewController;
import tracking.TrackingHelper;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class ConsultaRetiroSinTarjetaDelegate extends DelegateBaseOperacion {
	/**
	 * Identificador del delegado.
	 */
	public static final long RETIRO_SIN_TARJETA_DELEGATE_ID = 6L;

	private final Constants.Operacion tipoOperacion;

	private final Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

	private final Session session = Session.getInstance(SuiteApp.appContext);

	private RetiroSinTarjetaResult retiroSinTarjetaResult;
	
	/**
	 * El controlador actual de la pantalla.
	 */
	private BaseViewController viewController;

	private ConsultaRetiroSinTarjetaData movimientosSTR;

	private String claveRetiroActual;

	public ConsultaRetiroSinTarjetaDelegate () {
		this.tipoOperacion = Constants.Operacion.consultaRetiroSinTarjeta;
	}

	public void setViewController(BaseViewController viewController) {
		this.viewController = viewController;
	}
	public RetiroSinTarjetaResult getRetiroSinTarjetaResult() {
		return retiroSinTarjetaResult;
	}


	public void setRetiroSinTarjetaResult(
			RetiroSinTarjetaResult retiroSinTarjetaResult) {
		this.retiroSinTarjetaResult = retiroSinTarjetaResult;
	}
	public ArrayList<Object> getHeaderListaMovimientos() {
		final ArrayList<Object> result = new ArrayList<Object>();

		result.add(null);
		result.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_dineromovil_movimientos_lista_codigo));
		result.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_dineromovil_movimientos_lista_beneficiario));
		result.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_dineromovil_movimientos_lista_vencimiento));
		result.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_dineromovil_movimientos_lista_importe));

		return result;
	}

	public void consultaRetiroSinTarjeta(Constants.TipoEstatusRST tipo) {
		final Hashtable<String, String> paramTable = new Hashtable<String, String>();
//		final Session session = Session.getInstance(SuiteApp.appContext);
        int operacion = 0;

     	operacion = Server.CONSULTA_SIN_TARJETA;
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, 						session.getIum());
		paramTable.put(ServerConstants.NUMERO_TELEFONO, 			session.getUsername());
		paramTable.put("estatusOperacion",			tipo.codigo);
		//JAIG SI
		doNetworkOperation(operacion, paramTable, true, new ConsultaRetiroSinTarjetaData(),isJsonValueCode.NONE, viewController);
		//  ((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operacion, paramTable, viewController,true);
	}

	@Override
	public void performAction(Object obj) {
		
		if(viewController instanceof ConsultaRetiroSinTarjetaViewController) {
			movimientoSeleccionadoDM = ((ConsultaRetiroSinTarjetaViewController) viewController).getListaEnvios().getOpcionSeleccionada();
			if (Server.ALLOW_LOG) Log.e("ConsultaRST", "Seleccionado el movimiento en la posición: "+movimientoSeleccionadoDM);

			//AMZ inicio
			final Map<String,Object> ConsultaRealizadaMap = new HashMap<String, Object>();

			ConsultaRealizadaMap.put("evento_realizada","event52");
			ConsultaRealizadaMap.put("&&products","consulta;dinero movil");
			ConsultaRealizadaMap.put("eVar12","operacion realizada");
			TrackingHelper.trackConsultaRealizada(ConsultaRealizadaMap);

		//AMZ fin
//			showDetallesMovimientoDM();

			peticionClave12Digitos();
		}else{
			
		}
	}




		@Override
		public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson, ParsingHandler handler,isJsonValueCode isJsonValueCode, BaseViewController caller) {
			if( viewController != null) {
				((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
			}
		}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (operationId == Server.BAJA_OPSINTARJETA) {
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				// Object parsedres =
				// response.getResponse().toString().replaceAll("/^<.>$/", "");
				//retiroSinTarjetaResult = (RetiroSinTarjetaResult) response.getResponse();

				operacionExistosa();

				// ((AltaRetiroSinTarjetaViewController)altaRetiroSinTarjetaViewController).setDataFromTDC();

			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING){
				
				viewController.showInformationAlert(response.getMessageText());
				
			}else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				
				viewController.ocultaIndicadorActividad();
				viewController.showInformationAlert(response.getMessageText());

			}

		} else if (operationId == Server.CONSULTA_SIN_TARJETA) {
			this.movimientosSTR = (ConsultaRetiroSinTarjetaData) response
					.getResponse();
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				
				actualizarConsultaSTR();
			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING){
				
				
				actualizarConsultaSTR();
				//viewController.showInformationAlert(response.getMessageText());
				
			}else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
			
				
				viewController.ocultaIndicadorActividad();
				actualizarConsultaSTR();
				viewController.showInformationAlert(response.getMessageText());

			}
		} else if (operationId == Server.OP_RETIRO_SIN_TAR_12_DIGITOS) {
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				final Clave12DigitosResult result = (Clave12DigitosResult) response
						.getResponse();

				if (result.getFolioCanal().equalsIgnoreCase(movimientosSTR.getRetiros().get(movimientoSeleccionadoDM).getFolioOperacion())) {
					if (Server.ALLOW_LOG) Log.v(null, "folioOperación = folioCanal");
				
				showDetallesMovimientoDM();
				claveRetiroActual=result.getClaveRetiro();
				
				} else {
					viewController.showInformationAlert(viewController.getString(R.string.label_error), "Respuesta no corresponde con el dato solicitado", null);
				
				}
			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING){
				
				viewController.showInformationAlert(response.getMessageText());
				
			}else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				
				viewController.ocultaIndicadorActividad();
				viewController.showInformationAlert(response.getMessageText());
			}
		}
	}

		public void actualizarConsultaSTR() {
			if(!(viewController instanceof ConsultaRetiroSinTarjetaViewController)) {
				return;
			}
			final ConsultaRetiroSinTarjetaViewController controller = (ConsultaRetiroSinTarjetaViewController)viewController;

			final ArrayList<Object> movimientos = getDatosTablaSTR();

			controller.getListaEnvios().setNumeroFilas(movimientos.size());
			controller.getListaEnvios().setLista(movimientos);

			if(movimientos.isEmpty()) {
				controller.getListaEnvios().setTextoAMostrar(viewController.getString(R.string.bmovil_consultar_dineromovil_sin_envios));
			} else{
				controller.getListaEnvios().setTextoAMostrar(null);
				controller.getListaEnvios().setNumeroColumnas(4);
			}

			controller.getListaEnvios().cargarTabla();
		}

		public ArrayList<Object> getDatosTablaSTR() {
			final ArrayList<Object> datos = new ArrayList<Object>();
			ArrayList<String> registro;

			if((null == movimientosSTR) || movimientosSTR.getRetiros()==null ||movimientosSTR.getRetiros().isEmpty()) {
				registro = new ArrayList<String>();
				registro.add(null);
				registro.add("");
				registro.add("");
				registro.add("");
				registro.add("");
			} else {
				for(final ConsultaRetiroSinTarjeta consulta : movimientosSTR.getRetiros()) {
					registro = new ArrayList<String>();
					registro.add(null);
					registro.add(consulta.getCodigoSeguridad());
					registro.add((Constants.MOVEMENT_SHORT_DESCRIPTION < consulta.getBeneficiario().length()) ?
								 consulta.getBeneficiario().substring(0, Constants.MOVEMENT_SHORT_DESCRIPTION) :
								 consulta.getBeneficiario() );
					registro.add(Tools.formatDate(consulta.getFechaOperacion()));
					registro.add(Tools.formatAmount(consulta.getImporte(), false));
					datos.add(registro);
				}
			}

			return datos;
		}
		private int movimientoSeleccionadoDM;

		private void showDetallesMovimientoDM() {
			SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showDetalleRetiroSinTarjeta();
		}

		public boolean isMovimientoDMVigente() {
			final ConsultaRetiroSinTarjeta movimiento = movimientosSTR.getRetiros().get(movimientoSeleccionadoDM);
			return Constants.TipoMovimientoDM.VIGENTES.codigo.equalsIgnoreCase(movimiento.getEstatusOperacion());
		}

		public ArrayList<Object> getDatosTablaDetalleMovimientoDM() {
			final ArrayList<Object> datos = new ArrayList<Object>();

			final ConsultaRetiroSinTarjeta movimiento = movimientosSTR.getRetiros().get(movimientoSeleccionadoDM);
			ArrayList<String> fila;

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_retirosintarjeta_detalles_clave_retiro));
			fila.add(Tools.formatClaveRetiroGuiones(claveRetiroActual));
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_otp));
			fila.add(movimiento.getCodigoSeguridad());
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_origen));
			fila.add(Tools.hideAccountNumber(movimiento.getCuentaCargo()));
			datos.add(fila);

//			fila = new ArrayList<String>();
//			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_celular));
//			fila.add(movimiento.getNumeroCelular());
//			datos.add(fila);

//			fila = new ArrayList<String>();
//			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_compania));
//			fila.add(movimiento.getCompaniaCelular());
//			datos.add(fila);

//			fila = new ArrayList<String>();
//			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_beneficiario));
//			fila.add(movimiento.getBeneficiario());
//			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_importe));
			fila.add(Tools.formatAmount(movimiento.getImporte(), false));
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_concepto));
			fila.add(movimiento.getConcepto());
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_alta));
			fila.add(Tools.formatDate(movimiento.getFechaOperacion()));
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_vigencia));
			fila.add(Tools.formatDate(movimiento.getFechaExpiracion()));
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_folio));
			fila.add(movimiento.getFolioOperacion());
			datos.add(fila);

			return datos;
		}

		private void peticionClave12Digitos() {

			final Hashtable<String, String> paramTable = new Hashtable<String, String>();
//			final Session session = Session.getInstance(SuiteApp.appContext);
	        int operacion = 0;

	     	operacion = Server.OP_RETIRO_SIN_TAR_12_DIGITOS;
			paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
			paramTable.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
			paramTable.put("folioOperacion", this.movimientosSTR.getRetiros().get(movimientoSeleccionadoDM).getFolioOperacion());
			//JAIG SI
			doNetworkOperation(operacion, paramTable,true, new Clave12DigitosResult(),isJsonValueCode.NONE, viewController);
			//  ((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operacion, paramTable, viewController,true);

		}

		public void showConfirmacionAutenticacion(){
			//final BmovilViewsController bmovilParentController = ((BmovilViewsController)viewController.getParentViewsController());
//			bmovilParentController.showConfirmacionAutenticacionViewController(this, getNombreImagenEncabezado(),
//					getTextoEncabezado(),
//					R.string.confirmation_subtitulo);

			SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(viewController);
			SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacion(this);/*(this,
																			   R.drawable.bmovil_consultar_icono,
																			   R.string.bmovil_consultar_retirosintarjeta_titulo,
																			   R.string.confirmation_subtitulo);*/
		}

		/* (non-Javadoc)
		 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getDatosTablaConfirmacion()
		 */
		@Override
		public ArrayList<Object> getDatosTablaConfirmacion() {
			final ArrayList<Object> datos = new ArrayList<Object>();

			final ConsultaRetiroSinTarjeta movimiento = movimientosSTR.getRetiros().get(movimientoSeleccionadoDM);
			ArrayList<String> fila;

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_retirosintarjeta_detalles_clave_retiro));
			fila.add(Tools.formatClaveRetiroGuiones(claveRetiroActual));
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_otp));
			fila.add(movimiento.getCodigoSeguridad());
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_origen));
			fila.add(Tools.hideAccountNumber(movimiento.getCuentaCargo()));
			datos.add(fila);

//			fila = new ArrayList<String>();
//			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_celular));
//			fila.add(movimiento.getNumeroCelular());
//			datos.add(fila);

//			fila = new ArrayList<String>();
//			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_compania));
//			fila.add(movimiento.getCompaniaCelular());
//			datos.add(fila);

//			fila = new ArrayList<String>();
//			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_beneficiario));
//			fila.add(movimiento.getBeneficiario());
//			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_importe));
			fila.add(Tools.formatAmount(movimiento.getImporte(), false));
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_concepto));
			fila.add(movimiento.getConcepto());
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_alta));
			fila.add(Tools.formatDate(movimiento.getFechaOperacion()));
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_vigencia));
			fila.add(Tools.formatDate(movimiento.getFechaExpiracion()));
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_folio));
			fila.add(movimiento.getFolioOperacion());
			datos.add(fila);

			return datos;
		}
		@Override
		public TipoOtpAutenticacion tokenAMostrar() {
			TipoOtpAutenticacion value = null;
			Operacion tipoOperacionCancelar = Constants.Operacion.cancelarDineroMovil;
			final Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

			value = Autenticacion.getInstance().tokenAMostrar(tipoOperacionCancelar, perfil,Tools.getDoubleAmountFromServerString(movimientosSTR.getRetiros().get(movimientoSeleccionadoDM).getImporte()));

			return value;
		}

		@Override
		public boolean mostrarNIP() {
			//Modificación 48480 - Se cambia la operación cancelarRetirosintarjeta a cancelarDineroMovil
			Operacion tipoOperacionCancelar = Constants.Operacion.cancelarDineroMovil;
			boolean mostrar = false;
			final Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

			mostrar = Autenticacion.getInstance().mostrarNIP(tipoOperacionCancelar, perfil,Tools.getDoubleAmountFromServerString(movimientosSTR.getRetiros().get(movimientoSeleccionadoDM).getImporte()));

			return mostrar;
		}

		/* (non-Javadoc)
		 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#mostrarContrasenia()
		 */
		@Override
		public boolean mostrarContrasenia() {
			//Modificación 48480 - Se cambia la operación cancelarRetirosintarjeta a cancelarDineroMovil
			Operacion tipoOperacionCancelar = Constants.Operacion.cancelarDineroMovil;
			boolean mostrar = false;

			mostrar = Autenticacion.getInstance().mostrarContrasena(tipoOperacionCancelar, perfil,Tools.getDoubleAmountFromServerString(movimientosSTR.getRetiros().get(movimientoSeleccionadoDM).getImporte()));

			return mostrar;
		}

		@Override
		public boolean mostrarCVV() {
			//Modificación 48480 - Se cambia la operación cancelarRetirosintarjeta a cancelarDineroMovil
			Operacion tipoOperacionCancelar = Constants.Operacion.cancelarDineroMovil;
			boolean mostrar = false;
			final Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

			mostrar = Autenticacion.getInstance().mostrarCVV(tipoOperacionCancelar, perfil,Tools.getDoubleAmountFromServerString(movimientosSTR.getRetiros().get(movimientoSeleccionadoDM).getImporte()));

			return mostrar;
		}

		/**
		 * @return the perfil
		 */
		public Constants.Perfil getPerfil() {
			return perfil;
		}
		@Override
		public int getNombreImagenEncabezado() {

			return R.drawable.bmovil_consultar_icono;
		}
		
		@Override
		public int getTextoEncabezado() {
			return (R.string.bmovil_consultar_retirosintarjeta_titulo);

		}
		

		@Override
		public void realizaOperacion(
				ConfirmacionViewController confirmacionViewController,
				String contrasenia, String nip, String token, String campoTarjeta, String cvv) {
			Operacion tipoOperacionCancelar = Constants.Operacion.cancelarDineroMovil; //Modificación 48480 - Se cambia la operación cancelarRetirosintarjeta a cancelarDineroMovil
			Hashtable<String, String> paramTable = new Hashtable<String, String>();
			Session session = Session.getInstance(SuiteApp.appContext);
	        int operacion = 0;
			//Toast.makeText(SuiteApp.getInstance(),"realizaOperacion en AltaRetiroSinTarjetaDelegate", Toast.LENGTH_SHORT).show();
			//se llama al metodo que realiza la peticion de baja al servidor
			if(Server.ALLOW_LOG) Log.d("baja consulta Retiro Sin tarjeta", "Cancele consulta retiro sin tarjeta movil");
			ConsultaRetiroSinTarjeta movimiento = movimientosSTR.getRetiros().get(movimientoSeleccionadoDM);
			operacion = Server.BAJA_OPSINTARJETA;

			paramTable.put(ServerConstants.PARAMS_TEXTO_NT, 					session.getUsername());
			paramTable.put(ServerConstants.PARAMS_TEXTO_NP, 					Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia);
			paramTable.put(ServerConstants.PARAMS_TEXTO_IU, 						session.getIum());
			paramTable.put("TE", 			session.getClientNumber());
			paramTable.put("FO", 			movimiento.getFolioOperacion());
			paramTable.put("CG", 				movimiento.getCodigoSeguridad());
			String fecha = movimiento.getFechaOperacion();
			   try {
				    fecha = new SimpleDateFormat("yyyy-MM-dd").format(new SimpleDateFormat("ddMMyyyy").parse(fecha));
				} catch (ParseException e) {
					if(Server.ALLOW_LOG) e.printStackTrace();
				}
			paramTable.put("FE", 					fecha);
			
			if(!Tools.isEmptyOrNull(nip))
				paramTable.put("NI",					nip);
			if(!Tools.isEmptyOrNull(token))
				paramTable.put("OT",					token);
			if(!Tools.isEmptyOrNull(cvv))
				paramTable.put("CV",					cvv);
			
//			paramTable.put(Server.NIP_PARAM, 						Tools.isEmptyOrNull(nip) ? "" : nip);
//			paramTable.put(ServerConstants.CODIGO_OTP, 						Tools.isEmptyOrNull(token) ? "" : token);
//			paramTable.put(Server.CVV2_PARAM, 						Tools.isEmptyOrNull(cvv) ?	"" : cvv);
			paramTable.put("VA",	Autenticacion.getInstance().getCadenaAutenticacion(tipoOperacionCancelar,
																													   Session.getInstance(SuiteApp.appContext).getClientProfile()));
			//JAIG SI
        doNetworkOperation(operacion, paramTable,false, new BajaDineroMovilData(),isJsonValueCode.NONE, confirmacionViewController);

		}
		
		public void showResultados() {
			SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showResultadosViewController(this, 
																													R.string.transferir_retirosintarjeta_resultados_titulo, 
																												  R.drawable.bmovil_dinero_movil_icono);
		}
		
		public ArrayList<Object> getDatosTablaResultados() {
			final ArrayList<Object> datos = new ArrayList<Object>();

			final ConsultaRetiroSinTarjeta movimiento = movimientosSTR.getRetiros().get(movimientoSeleccionadoDM);
			ArrayList<String> fila;

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_retirosintarjeta_detalles_clave_retiro));
			fila.add(Tools.formatClaveRetiroGuiones(claveRetiroActual));
			datos.add(fila);


			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_otp));
			fila.add(movimiento.getCodigoSeguridad());
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_origen));
			fila.add(Tools.hideAccountNumber(movimiento.getCuentaCargo()));
			datos.add(fila);

//			fila = new ArrayList<String>();
//			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_celular));
//			fila.add(movimiento.getNumeroCelular());
//			datos.add(fila);

//			fila = new ArrayList<String>();
//			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_compania));
//			fila.add(movimiento.getCompaniaCelular());
//			datos.add(fila);

//			fila = new ArrayList<String>();
//			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_beneficiario));
//			fila.add(movimiento.getBeneficiario());
//			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_importe));
			fila.add(Tools.formatAmount(movimiento.getImporte(), false));
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_concepto));
			fila.add(movimiento.getConcepto());
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_alta));
			fila.add(Tools.formatDate(movimiento.getFechaOperacion()));
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_vigencia));
			fila.add(Tools.formatDate(movimiento.getFechaExpiracion()));
			datos.add(fila);

			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_folio));
			fila.add(movimiento.getFolioOperacion());
			datos.add(fila);

			return datos;
		}
		
		private void operacionExistosa() {
			if(Server.ALLOW_LOG) Log.d(getClass().getName(), "operacionExistosa");
			showResultados();
			
		}
		
		@Override
		public String getTextoTituloResultado() {

			return SuiteApp.appContext.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_operacion_cancelada);
		}
		
		
		@Override
		public int getColorTituloResultado() {

			return R.color.magenta;
			
		}
		
		
}

