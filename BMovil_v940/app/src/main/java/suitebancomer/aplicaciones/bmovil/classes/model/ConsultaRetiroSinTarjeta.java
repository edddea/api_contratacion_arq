package suitebancomer.aplicaciones.bmovil.classes.model;

public class ConsultaRetiroSinTarjeta {
	
	private String folioOperacion;
	
	private String importe;
	
	private String cuentaCargo;
	
	private String estatusOperacion;
	
	private String numeroCelular;
	
	private String companiaCelular;
	
	private String beneficiario;
	
	private String concepto;
	
	private String fechaOperacion;
	
	private String fechaExpiracion;
	
	private String horaOperacionExp;
	
	private String codigoSeguridad;
	
	private String indicador;

	public String getFolioOperacion() {
		return folioOperacion;
	}

	public void setFolioOperacion(String folioOperacion) {
		this.folioOperacion = folioOperacion;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getCuentaCargo() {
		return cuentaCargo;
	}

	public void setCuentaCargo(String cuentaCargo) {
		this.cuentaCargo = cuentaCargo;
	}

	public String getEstatusOperacion() {
		return estatusOperacion;
	}

	public void setEstatusOperacion(String estatusOperacion) {
		this.estatusOperacion = estatusOperacion;
	}

	public String getNumeroCelular() {
		return numeroCelular;
	}

	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	public String getCompaniaCelular() {
		return companiaCelular;
	}

	public void setCompaniaCelular(String companiaCelular) {
		this.companiaCelular = companiaCelular;
	}

	public String getBeneficiario() {
		return beneficiario;
	}

	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getFechaOperacion() {
		return fechaOperacion;
	}

	public void setFechaOperacion(String fechaOperacion) {
		this.fechaOperacion = fechaOperacion;
	}

	public String getFechaExpiracion() {
		return fechaExpiracion;
	}

	public void setFechaExpiracion(String fechaExpiracion) {
		this.fechaExpiracion = fechaExpiracion;
	}

	public String getHoraOperacionExp() {
		return horaOperacionExp;
	}

	public void setHoraOperacionExp(String horaOperacionExp) {
		this.horaOperacionExp = horaOperacionExp;
	}

	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	public String getIndicador() {
		return indicador;
	}

	public void setIndicador(String indicador) {
		this.indicador = indicador;
	}

	public ConsultaRetiroSinTarjeta(String folioOperacion, String importe,
			String cuentaCargo, String estatusOperacion, String numeroCelular,
			String companiaCelular, String beneficiario, String concepto,
			String fechaOperacion, String fechaExpiracion,
			String horaOperacionExp, String codigoSeguridad, String indicador) {
		super();
		this.folioOperacion = folioOperacion;
		this.importe = importe;
		this.cuentaCargo = cuentaCargo;
		this.estatusOperacion = estatusOperacion;
		this.numeroCelular = numeroCelular;
		this.companiaCelular = companiaCelular;
		this.beneficiario = beneficiario;
		this.concepto = concepto;
		this.fechaOperacion = fechaOperacion;
		this.fechaExpiracion = fechaExpiracion;
		this.horaOperacionExp = horaOperacionExp;
		this.codigoSeguridad = codigoSeguridad;
		this.indicador = indicador;
	}
	
	
	

}
