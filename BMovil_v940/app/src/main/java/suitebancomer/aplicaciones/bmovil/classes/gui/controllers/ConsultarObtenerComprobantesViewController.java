package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ObtenerComprobantesDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaPagoServiciosData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTransferenciasCuentasBBVAData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTransferenciasMisCuentasData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTransferenciasOtrosBancosData;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import tracking.TrackingHelper;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class ConsultarObtenerComprobantesViewController extends BaseViewController {
	// AMZ
	public BmovilViewsController parentManager;
	// AMZ
	/**
	 * Layout base, contenedor principal dela vista.
	 */
	private LinearLayout rootLayout;

	/**
	 * Etiqueta de t�tulo para el filtro se movimientos.
	 */
	private TextView lblSelecciona;

	/**
	 * Combobox de selecci�n de movimientos.
	 */
	private TextView comboFiltro;

	/**
	 * Layout para el componente que mostrara la lista de movimientos.
	 */
	private LinearLayout listaEnviosLayout;

	/**
	 * Componente de lista de movimientos de obtener comprobantes.
	 */
	private ListaSeleccionViewController listaEnvios;

	/**
	 * Delegado de la vista.
	 */
	private ObtenerComprobantesDelegate delegate;

	private TextView lblTitulo;
	
	TextView verMasMovimientos;
	
	/*
	 * No javadoc.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_consulta_obtener_comprobante);
		setTitle(R.string.bmovil_consultar_obtenercomprobante_titulo, R.drawable.bmovil_consultar_icono);
		// AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();

		TrackingHelper.trackState("comprobante", parentManager.estados);

		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID));
		delegate = (ObtenerComprobantesDelegate) getDelegate();
		delegate.setViewController(this);

		init();
	}

	/**
	 * Inicializa el controlador de la ventana.
	 */
	private void init() {

		findViews();
		cargaListaDatos();
		scaleForCurrentScreen();
	}

	/**
	 * Busca las vistas usadas y guarda sus referencias.
	 */
	private void findViews() {
		rootLayout = (LinearLayout) findViewById(R.id.rootLayout);
		lblSelecciona = (TextView) findViewById(R.id.lblSelecciona);
		comboFiltro = (TextView) findViewById(R.id.comboboxFiltroEstado);
		listaEnviosLayout = (LinearLayout) findViewById(R.id.layoutListaEnvios);
		lblTitulo = (TextView) findViewById(R.id.lblTitulo);
	}

	/**
	 * Redimensiona las vistas para que mantegan la relacion de aspecto en
	 * cualquier pantalla.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		guiTools.scalePaddings(rootLayout);
		guiTools.scale(lblSelecciona, true);
		guiTools.scale(comboFiltro, true);
		guiTools.scale(listaEnviosLayout);
		guiTools.scale(lblTitulo);
	}

	/**
	 * Carga la lista movimientos de dinero movil.
	 */
	private void cargaListaDatos() {
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		
		ArrayList<Object> encabezados = delegate.getHeaderListaMovimientos();
		ArrayList<Object> datos = new ArrayList<Object>();

		listaEnvios = new ListaSeleccionViewController(this, params,
				parentViewsController);
		listaEnvios.setDelegate(delegate);
		listaEnvios.setEncabezado(encabezados);

		listaEnvios.setNumeroColumnas(2);
		listaEnvios.setTitle(getString(R.string.bmovil_consultar_dineromovil_movimientos_lista_titulo));
		listaEnvios.setOpcionSeleccionada(-1);
		listaEnvios.setSeleccionable(false);
		listaEnvios.setAlturaFija(true);
		listaEnvios.setNumeroFilas(datos.size());
		listaEnvios.setSingleLine(true);

		listaEnvios.setLista(datos);
		listaEnvios.setTextoAMostrar(" ");
		listaEnvios.cargarTabla();
		delegate.setTipoOperacion(Server.CONSULTA_TRANSFERENCIAS_MIS_CUENTAS);
//		delegate.consultarMovimientosOC();
		
		listaEnvios.setVisibility(View.GONE);
		listaEnviosLayout.addView(listaEnvios);
	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		this.delegate.analyzeResponse(operationId, response);
	}


	public void onTipoOperacionSeleccionado(Constants.TipoOperacionOC tipo) {
		String tipoOperacion = "";
		int operacion = 0;
		switch (tipo) {
		case TRANSMISCUENTAS:
			tipoOperacion = getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_tipooperacion_transmiscuentas);
			operacion= Server.CONSULTA_TRANSFERENCIAS_MIS_CUENTAS;
			delegate.parsingHandler = new ConsultaTransferenciasMisCuentasData();
			break;
		case TRANSCUENTASBBVA:
			tipoOperacion = getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_tipooperacion_transcuentasbbva);
			operacion= Server.CONSULTA_TRANSFERENCIAS_CUENTA_BBVA;
			delegate.parsingHandler = new ConsultaTransferenciasCuentasBBVAData();
			break;
		case TRANSOTROSBANCOS:
			tipoOperacion = getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_tipooperacion_transotrosbancos);
			operacion= Server.CONSULTA_TRANSFERENCIAS_A_OTROS_BANCOS;
			delegate.parsingHandler = new ConsultaTransferenciasOtrosBancosData();
			break;
		case PAGOSERV:
			tipoOperacion = getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_tipooperacion_pagoservicios);
			operacion= Server.CONSULTA_PAGO_SERVICIOS;
			delegate.parsingHandler = new ConsultaPagoServiciosData();
			break;
		default:
			tipoOperacion = "";
			break;
		}
		comboFiltro.setText(tipoOperacion);
		delegate.setPeriodoActual(0);
		delegate.setTipoOperacion(operacion);
		delegate.setMovimientosOC(null);
		delegate.actualizarMovimientosOC();
		muestraVerMasMovimientos(false);
		listaEnvios.setOpcionSeleccionada(-1);
		delegate.consultarMovimientosOC();
	}

	public void onTipoOperacionClick(View view) {
		delegate.setViewController(this);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		final String[] importes = new String[] {
				getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_tipooperacion_transmiscuentas),
				getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_tipooperacion_transcuentasbbva),
				getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_tipooperacion_transotrosbancos),
				getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_tipooperacion_pagoservicios)};

		builder.setTitle(
				R.string.bmovil_consultar_dineromovil_movimientos_tipo_titulo)
				.setItems(importes, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						if (Constants.TipoOperacionOC.TRANSMISCUENTAS.value == which) {
							onTipoOperacionSeleccionado(Constants.TipoOperacionOC.TRANSMISCUENTAS);
						} else if (Constants.TipoOperacionOC.TRANSCUENTASBBVA.value == which) {
							onTipoOperacionSeleccionado(Constants.TipoOperacionOC.TRANSCUENTASBBVA);
						} else if (Constants.TipoOperacionOC.TRANSOTROSBANCOS.value == which) {
							onTipoOperacionSeleccionado(Constants.TipoOperacionOC.TRANSOTROSBANCOS);
						} else if (Constants.TipoOperacionOC.PAGOSERV.value == which) {
							onTipoOperacionSeleccionado(Constants.TipoOperacionOC.PAGOSERV);
			
						}

					}
				}).show();
	}

	/**
	 * @return the listaEnvios
	 */
	public ListaSeleccionViewController getListaEnvios() {
		return listaEnvios;
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (null != listaEnvios) {
			setDelegate(parentViewsController
					.getBaseDelegateForKey(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID));
			delegate = (ObtenerComprobantesDelegate) getDelegate();
			delegate.setViewController(this);
			listaEnvios.setDelegate(this.delegate);
		}

		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if (MotionEvent.ACTION_DOWN == ev.getAction()) {
			// if(Server.ALLOW_LOG) Log.d(this.getClass().getName(),
			// "Touch Event Action: ACTION_DOWN");
			listaEnvios.setMarqueeEnabled(false);
		} else if (MotionEvent.ACTION_UP == ev.getAction()) {
			// if(Server.ALLOW_LOG) Log.d(this.getClass().getName(),
			// "Touch Event Action: ACTION_UP");
			listaEnvios.setMarqueeEnabled(true);
		}

		return super.dispatchTouchEvent(ev);
	}
	public void muestraVerMasMovimientos(boolean visible){
		if(visible && null == this.verMasMovimientos){
			this.verMasMovimientos = (TextView) getLayoutInflater().inflate(R.layout.layout_bmovil_ver_mas_movimientos, null);
			
			listaEnviosLayout.addView(verMasMovimientos);
			GuiTools guiTools = GuiTools.getCurrent();
			guiTools.init(getWindowManager());
			guiTools.scale(verMasMovimientos, true);
			verMasMovimientos.setPadding(0, 0, 0, 10);
		}
		
		if(null != verMasMovimientos){
			verMasMovimientos.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
	}
	public void onLinkVerMasMovimientos(View sender) {
		delegate.verMasMovimientos();
	}

}
