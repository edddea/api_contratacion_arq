package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.AltaRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjeta;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.AmountField;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaDatosViewController;
import tracking.TrackingHelper;

public class AltaRetiroSinTarjetaViewController extends BaseViewController implements OnClickListener{

	
	
	private AltaRetiroSinTarjetaDelegate delegate;
	//AMZ
	public BmovilViewsController parentManager;
	
	
	private RetiroSinTarjeta datosBeneficiario;
	
	
	private CuentaOrigenViewController componenteCtaOrigen;
	
	
	private LinearLayout vistaCompCuentaRetiro;
	
//	private TextView numCelular;
//	private TextView numeroCelular;
//	private TextView compCelular;
//	private TextView companiaCelular;
//	private TextView benef_sintarjeta;
//	private TextView beneficiario_sintarjeta;
	private HashMap<String, Compania> listaCompanias;
	
	
	private TextView labelImporte;
	private TextView campoImporte;
	private TextView labelOtroImporte;
	private AmountField campoOtroImporte;
	
	
	private TextView lblConcepto;
	private EditText textConcepto;
	private ImageButton btnContinuar;
	
	
	private Session session;



	private ListaDatosViewController listaDatos;
	private LinearLayout datosBeneficiarioLayout;
	
	
	
	
	public RetiroSinTarjeta getDatosBeneficiario() {
		return datosBeneficiario;
	}


	public void setDatosBeneficiario(RetiroSinTarjeta datosBeneficiario) {
		this.datosBeneficiario = datosBeneficiario;
	}


	public CuentaOrigenViewController getComponenteCtaOrigen() {
		return componenteCtaOrigen;
	}


	public void setComponenteCtaOrigen(
			CuentaOrigenViewController componenteCtaOrigen) {
		this.componenteCtaOrigen = componenteCtaOrigen;
	}

	
	/**
	 * Default constructor for this activity
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.activity_retiro_sin_tarjeta);
		//marqueeEnabled = true;
		setTitle(R.string.transferir_alta_retiro_sintar_title, R.drawable.bmovil_dinero_movil_icono);
		//AMZ
		
		//Toast.makeText(SuiteApp.getInstance(),"onCreate AltaRetiroSinTarjetaViewController", Toast.LENGTH_SHORT).show();

		
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("retiro sin tarjeta", parentManager.estados);
		
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		delegate = (AltaRetiroSinTarjetaDelegate)parentViewsController.getBaseDelegateForKey(AltaRetiroSinTarjetaDelegate.ALTA_RETIRO_SINTAR_DELEGATE_ID);
		if(delegate == null){
			delegate = new AltaRetiroSinTarjetaDelegate();
			parentViewsController.addDelegateToHashMap(AltaRetiroSinTarjetaDelegate.ALTA_RETIRO_SINTAR_DELEGATE_ID, delegate);
		}
		delegate.setAltaRetiroSinTarjetaViewController(this);
		
		
		datosBeneficiario=new RetiroSinTarjeta();
		
		//Toast.makeText(SuiteApp.getInstance(),"onCreate PAgo Tdc", Toast.LENGTH_LONG).show();

		

		
		init();
	}
	
	
	public void init(){
		
		
		session= Session.getInstance(SuiteApp.appContext);

		findViews();
		scaleForCurrentScreen();
		muestraComponenteCuentaOrigen();
		
		//ESTA PETICION IRA MAS ADELANTE
		//delegate.consultaAltaRetiroSinTarjeta();
		
		setDataBeneficiario();
		
		
		campoOtroImporte.addTextChangedListener(new BmovilTextWatcher(this));

		// Set TDC number
	//	textCuentaDestino.setText(delegate.getCuentaTDC().getNumber());
		
		//InputFilter[] filterImporte = {new InputFilter.LengthFilter(Constants.AMOUNT_LENGTH_PAGO_TDC)};
	//	textImporte.setFilters(filterImporte);
//		textCuentaDestino.addTextChangedListener(new BmovilTextWatcher(this));
//		textImporte.addTextChangedListener(new BmovilTextWatcher(this));
//		txtMotivo.addTextChangedListener(new BmovilTextWatcher(this));

	}
	
	public void muestraComponenteCuentaOrigen(){
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
		componenteCtaOrigen.setDelegate(delegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		if(delegate.getCuentaOrigenSeleccionada() != null){
			componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(delegate.getCuentaOrigenSeleccionada()));
		}else{
			componenteCtaOrigen.setIndiceCuentaSeleccionada(0);
		}
		componenteCtaOrigen.init();
		componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.transferir);
		vistaCompCuentaRetiro.addView(componenteCtaOrigen);
//		delegate.setCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		cargaCuentaSeleccionada();
	}
	
	
	
	public void cargaCuentaSeleccionada(){
//		textCuentaDestino.setText(Tools.enmascaraCuentaDestino(delegate.getInnerTransaction().getCuentaDestino().getNumber()));
	}

	
	private void findViews() {

		vistaCompCuentaRetiro = (LinearLayout)findViewById(R.id.transfer_mis_cuentas_layout);
		
		
		
//		numCelular = (TextView)findViewById(R.id.numCelular);
//		numeroCelular = (TextView)findViewById(R.id.numeroCelular);
//		compCelular = (TextView)findViewById(R.id.compCelular);
//		companiaCelular = (TextView)findViewById(R.id.companiaCelular);
//		benef_sintarjeta = (TextView)findViewById(R.id.benef_sintarjeta);
//		beneficiario_sintarjeta = (TextView)findViewById(R.id.beneficiario_sintarjeta);
		
		labelImporte = (TextView)findViewById(R.id.lblImporte_retirosintar);
		campoImporte = (TextView)findViewById(R.id.comboImporte_retiro_sintar);
		labelOtroImporte = (TextView)findViewById(R.id.lblOtroImporte_retirosintar);
		campoOtroImporte = (AmountField)findViewById(R.id.textOtroImporte_retirosintar);
		
		lblConcepto = (TextView)findViewById(R.id.lblConcepto_retirosintar);
		textConcepto = (EditText)findViewById(R.id.textConcepto_retirosintar);
		
		
		textConcepto = (EditText)findViewById(R.id.textConcepto_retirosintar);
		
		btnContinuar = (ImageButton)findViewById(R.id.btnContinuar_retirosintar);
	
		datosBeneficiarioLayout = (LinearLayout)findViewById(R.id.datosBeneficiario_retsintarjeta);



	}
	
	
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.rootLayout));
//		guiTools.scale(findViewById(R.id.linear00));
//		guiTools.scale(findViewById(R.id.linear01));
//		guiTools.scale(findViewById(R.id.linear02));
		
		guiTools.scale(findViewById(R.id.datosBeneficiario_retsintarjeta));

		
		guiTools.scale(findViewById(R.id.transfer_mis_cuentas_layout));
		
		guiTools.scale(findViewById(R.id.detalle_transfer_importe_edit), true);
		
//		guiTools.scale(findViewById(R.id.numCelular), true);
//		guiTools.scale(findViewById(R.id.numeroCelular), true);
//		guiTools.scale(findViewById(R.id.compCelular), true);
//		guiTools.scale(findViewById(R.id.companiaCelular), true);
//		guiTools.scale(findViewById(R.id.benef_sintarjeta), true);
//		guiTools.scale(findViewById(R.id.beneficiario_sintarjeta), true);
		
		
		guiTools.scale(findViewById(R.id.lblImporte_retirosintar), true);
		guiTools.scale(findViewById(R.id.comboImporte_retiro_sintar), true);
		guiTools.scale(findViewById(R.id.lblOtroImporte_retirosintar), true);
		guiTools.scale(campoOtroImporte, true);
		
		guiTools.scale(lblConcepto, true);
		guiTools.scale(textConcepto, true);
		
		guiTools.scale(btnContinuar);


	


		
		
		/*guiTools.scale(findViewById(R.id.continuarlayout));		
		guiTools.scale(continuarButton);*/
	}
	
	public ArrayList<Object> getListaBeneficiario(){
		
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(getString(R.string.bmovil_retiro_sintarjeta_numcelular));
		fila.add(datosBeneficiario.getCelularBeneficiario());
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(getString(R.string.bmovil_retiro_sintarjeta_compcelular));
		fila.add(datosBeneficiario.getCompania().getNombre());
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(getString(R.string.bmovil_retiro_sintarjeta_beneficiario));
		fila.add(datosBeneficiario.getBeneficiario());
		tabla.add(fila);	


		return tabla;
	}
	
	public void setDataBeneficiario(){
		
		session = Session.getInstance(SuiteApp.appContext);
		
		datosBeneficiario.setBeneficiario(session.getNombreCliente());
		datosBeneficiario.setCelularBeneficiario(session.getUsername());
		session.getCatalogoDineroMovil();
		datosBeneficiario.setCuentaOrigen(delegate.getCuentaOrigenSeleccionada());
		listaCompanias=delegate.cargarListaCompanias();
		datosBeneficiario.setCompania(listaCompanias.get(session.getCompaniaUsuario()));
//		numeroCelular.setText(datosBeneficiario.getCelularBeneficiario());
//		companiaCelular.setText(datosBeneficiario.getCompania().getNombre());
//		beneficiario_sintarjeta.setText(datosBeneficiario.getBeneficiario());
		muestraTDCData(getListaBeneficiario());
		
	}
	
	
	

	@SuppressWarnings("deprecation")
	public void muestraListaCuentas(){
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
	}
	

	
	
	public void actualizaCuentaOrigen(Account cuenta){

		if(Server.ALLOW_LOG) Log.d("AltaRSTViewController", "Actualizar la cuenta origen actual por  "+cuenta.getNumber());
		delegate.setCuentaOrigenSeleccionada(cuenta);
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
		
		datosBeneficiario.setCuentaOrigen(delegate.getCuentaOrigenSeleccionada());

		
	}
	
	
	/**
	 * Muestra el Dialog para seleccionar el importe.
	 * @param view La vista que invoca esté método.
	 */
	public void onComboImporteClick(View view) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		session = Session.getInstance(SuiteApp.appContext);

		
		Compania companiaSeleccionado = (Compania)listaCompanias.get(session.getCompaniaUsuario());
		final String[] importes = delegate.obtenerCatalogoDeImportes(companiaSeleccionado.getNombre());
		
		builder.setTitle(R.string.transferir_dineromovil_titulo_importe).setItems(importes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String importe = importes[which];
				campoImporte.setText(importe);
				
				if(getResources().getString(R.string.transferir_dineromovil_datos_importe_otro).equalsIgnoreCase(importe)) {
					campoOtroImporte.setText(R.string.transferir_dineromovil_datos_importe_inicial);
					
					labelOtroImporte.setVisibility(View.VISIBLE);
					campoOtroImporte.setVisibility(View.VISIBLE);
				} else {
					labelOtroImporte.setVisibility(View.GONE);
					campoOtroImporte.setVisibility(View.GONE);
				}
			}
		}).show();
	}
	
	/**
	 * Restablece los importes a 0.0 y oculta los campor de otro importe.
	 */
	public void limpiarImportes() {
		campoImporte.setText(R.string.bmovil_common_hint_combo);
		//campoOtroImporte.setText(R.string.transferir_dineromovil_datos_importe_inicial);
		//campoOtroImporte.setAmount(getString(R.string.transferir_dineromovil_datos_importe_inicial));
		campoOtroImporte.reset();
		
		labelOtroImporte.setVisibility(View.GONE);
		campoOtroImporte.setVisibility(View.GONE);
	}
	
	
	
	public void botonContinuarRetiroClick(View view) {
		if(!isHabilitado())
			return;
		setHabilitado(false);
		/*if(parentViewsController.isActivityChanging())
			return;*/
		
		//consulta de limites para montos mayores
		//delegate.consultarLimites();
		
		
		datosBeneficiario.setCuentaOrigen(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		datosBeneficiario.setConcepto(Tools.removeSpecialCharacters(textConcepto.getText().toString()));
		
		Compania compania = listaCompanias.get(session.getCompaniaUsuario());
	//	mapa.put(compania.getClave(), compania.getNombre());
		datosBeneficiario.setCompania(compania);
//		datosBeneficiario.setCelularBeneficiario(numeroCelular.getText().toString());
		
		//se pasa la instancia al delegate para que no falle en el validador
		delegate.setModeloRetiroSinTarjeta(datosBeneficiario);
		

		delegate.validaDatos(datosBeneficiario.getCelularBeneficiario(), 
							 (View.VISIBLE == campoOtroImporte.getVisibility()) ? null : campoImporte.getText().toString(), 
							 (View.VISIBLE == campoOtroImporte.getVisibility()) ? campoOtroImporte.getAmount() : null, 
							 Tools.removeSpecialCharacters(datosBeneficiario.getBeneficiario()));
	}
	
	
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	
	
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
//		delegate.setCallerController(this);
		campoOtroImporte.reset();
		
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		
		//this.ocultaIndicadorActividad();
		super.goBack();
	}
	
	
	@SuppressLint("InflateParams") 
	public void muestraTDCData(ArrayList<Object> tabla){
		
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(datosBeneficiarioLayout);
		
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		listaDatos = new ListaDatosViewController(this, params, parentViewsController);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(tabla);
		listaDatos.setNumeroFilas(tabla.size());
		listaDatos.showLista();
		datosBeneficiarioLayout.addView(listaDatos);
	}

}
