/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomer.aplicaciones.bmovil.classes.io;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.util.Log;

import com.bancomer.mbanking.SuiteApp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.session.CommonSession;
import suitebancomer.aplicaciones.bbvacredit.common.ConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.common.CreditConstantsPropertyList;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.io.CreditPropertyList;
import suitebancomer.aplicaciones.bbvacredit.io.HttpInvokerCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ParsingExceptionCredit;
import suitebancomer.aplicaciones.bbvacredit.io.PathsOperacionCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerConstantsCredit;
import suitebancomer.aplicaciones.bbvacredit.io.ServerResponseCredit;
import suitebancomer.aplicaciones.bbvacredit.models.CalculoData;
import suitebancomer.aplicaciones.bbvacredit.models.ConsultaAlternativasData;
import suitebancomer.aplicaciones.bbvacredit.models.ConsultaCorreoData;
import suitebancomer.aplicaciones.bbvacredit.models.ConsultaDatosTDCData;
import suitebancomer.aplicaciones.bbvacredit.models.DetalleAlternativa;
import suitebancomer.aplicaciones.bbvacredit.models.EnvioCorreoData;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilConstants;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.ConstantsPropertyList;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.ActivacionResult;
import suitebancomer.aplicaciones.bmovil.classes.model.ActualizarCuentasResult;
import suitebancomer.aplicaciones.bmovil.classes.model.ActualizarPreregistroResult;
import suitebancomer.aplicaciones.bmovil.classes.model.BajaDineroMovilData;
import suitebancomer.aplicaciones.bmovil.classes.model.BeneficiarioBancomerData;
import suitebancomer.aplicaciones.bmovil.classes.model.CambiarLimitesResult;
import suitebancomer.aplicaciones.bmovil.classes.model.CambiarPasswordData;
import suitebancomer.aplicaciones.bmovil.classes.model.CambioCuentaResult;
import suitebancomer.aplicaciones.bmovil.classes.model.CambioDePerfilData;
import suitebancomer.aplicaciones.bmovil.classes.model.CambioTelefonoResult;
import suitebancomer.aplicaciones.bmovil.classes.model.CampaniaPaperlessResult;
import suitebancomer.aplicaciones.bmovil.classes.model.Clave12DigitosResult;
import suitebancomer.aplicaciones.bmovil.classes.model.Comision;
import suitebancomer.aplicaciones.bmovil.classes.model.CompraTiempoAireResult;
import suitebancomer.aplicaciones.bmovil.classes.model.ConfigurarCorreoData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultThirdsAccountBBVAResult;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDepositosBBVABancomerYEfectivoData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDepositosChequesData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDepositosEfectivoData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDepositosRecibidosChequesEfectivo;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDineroMovilData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaECExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaECResult;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaEstatusMantenimientoData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaInterbancariaResult;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaPagoServiciosData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaPoliza;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultarEstatusEnvioEC;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaRetiroSinTarjetaData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaSPEIData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTarjetaContratacionData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTerminosDeUsoData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTransferenciasClientesBancomerData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTransferenciasCuentasBBVAData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTransferenciasDeOtrosBancosData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTransferenciasMisCuentasData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTransferenciasOtrosBancosData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultarEstatusEnvioECResult;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultarLimitesResult;
import suitebancomer.aplicaciones.bmovil.classes.model.ContratacionBmovilData;
import suitebancomer.aplicaciones.bmovil.classes.model.DesbloqueoResult;
import suitebancomer.aplicaciones.bmovil.classes.model.EstatusDelServicioData;
import suitebancomer.aplicaciones.bmovil.classes.model.FrecuenteMulticanalResult;
import suitebancomer.aplicaciones.bmovil.classes.model.ImporteEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.ImportesTDCData;
import suitebancomer.aplicaciones.bmovil.classes.model.LoginData;
import suitebancomer.aplicaciones.bmovil.classes.model.MantenimientoAlertasResultado;
import suitebancomer.aplicaciones.bmovil.classes.model.MovementExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.NameValuePair;
import suitebancomer.aplicaciones.bmovil.classes.model.NombreCIEData;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.PagoServicio;
import suitebancomer.aplicaciones.bmovil.classes.model.PagoServicioResult;
import suitebancomer.aplicaciones.bmovil.classes.model.PaymentExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.RegistrarOperacionResult;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjetaResult;
import suitebancomer.aplicaciones.bmovil.classes.model.SincroExportSTData;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.aplicaciones.bmovil.classes.model.SpeiTermsAndConditionsResult;
import suitebancomer.aplicaciones.bmovil.classes.model.SuspenderCancelarResult;
import suitebancomer.aplicaciones.bmovil.classes.model.TextoPaperless;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferResult;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovilData;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferirCuentasPropiasData;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
//import suitebancomer.aplicaciones.softtoken.classes.model.AutenticacionSTRespuesta;
//import suitebancomer.aplicaciones.softtoken.classes.model.ConsultaTarjetaSTRespuesta;

//SPEI


/**
 * Server is responsible of acting as an interface between the application
 * business logic and the HTTP operations. It receive the network operation
 * requests and bservservuilds a network operation suitable for HttpInvoker class, which
 * eventually will perform the network connection. Server returns the server
 * response as a ServerResponse object, so that the application logic can
 * perform high level checks and processes.
 * 
 * @author Stefanini IT Solutions.
 */
public class Server {
    public static PropertyList oReadDocument = new PropertyList();
	public static CreditPropertyList oReadDocumentCredit = new CreditPropertyList();
	/**
	 * Defines if the application show logs
	 */
	public static boolean ALLOW_LOG = ServerCommons.ALLOW_LOG;

	/**
	 * Defines if the application is running on the emulator or in the device.
	 */
	public static boolean EMULATOR = ServerCommons.EMULATOR;

	/**
	 * Indicates simulation usage.
	 */
	public static boolean SIMULATION = ServerCommons.SIMULATION;

	/**
	 * Indicates if the base URL points to development server or production
	 */
	//public static boolean DEVELOPMENT = false;
	 public static boolean DEVELOPMENT = ServerCommons.DEVELOPMENT; //TODO remove for TEST

	/**
	 * Indicates simulation usage.
	 */
	public static final long SIMULATION_RESPONSE_TIME = 5;

	/**
	 * Bancomer Infrastructure.
	 */
	public static final int BANCOMER = 0;

	/**
	 * Stefanini Infrastructure.
	 */
	public static final int STEFANINI = 1;

	/**
	 * Last operation provider.
	 */

	// Change this value to bancomer for production setup
	// public static int PROVIDER = STEFANINI;
	public static int PROVIDER = BANCOMER; // TODO remove for TEST

	// Simulates profile change status
    public static String INSTRUMENT_TYPE = Constants.IS_TYPE_OCRA;

	//public static String CURRENT_PROFILE_STRING = PROFILE_CHANGE_BASIC_TO_ADVANCED;
    public static String CURRENT_PROFILE_STRING;


	// SIMULATES DIFFERENT ACCOUNT SETS
	public static String CURRENT_ACCOUNT_SET;

	// Simulates diferent app status.
	public static String CODIGO_ESTATUS_APLICACION = "PA";

    // Simulate diferent fastpayment sets.
	public static String CURRENT_FAST_PAYMENT_SET;

	public static String LISTA_OPERACIONES;

	// ///////////////////////////////////////////////////////////////////////////
	// Operations identifiers //
	// ///////////////////////////////////////////////////////////////////////////

	/**
	 * Activation step 1 operation.
	 */
	public static final int ACTIVATION_STEP1_OPERATION = 1;

	/**
	 * Acivation step 2 operation.
	 */
	public static final int ACTIVATION_STEP2_OPERATION = 2;

	/**
	 * Login oepration.
	 */
	public static final int LOGIN_OPERATION = 3;

	/**
	 * Movements operation.
	 */
	public static final int MOVEMENTS_OPERATION = 4;

	/**
	 * Transfer money operation between the user's accounts.
	 */
	public static final int SELF_TRANSFER_OPERATION = 5;

	/**
	 * Transfer money operation to someone else's account in Bancomer.
	 */
	public static final int BANCOMER_TRANSFER_OPERATION = 6;

	/**
	 * External transfer operation.
	 */
	public static final int EXTERNAL_TRANSFER_OPERATION = 7;

	/**
	 * Nipper store purchase operation.
	 */
	public static final int NIPPER_STORE_PURCHASE_OPERATION = 8;

	/**
	 * Nipper airtime purchase operation.
	 */
	public static final int NIPPER_AIRTIME_PURCHASE_OPERATION = 9;

	/**
	 * Change password operation.
	 */
	public static final int CHANGE_PASSWORD_OPERATION = 10;

	/**
	 * Close session operation.
	 */
	public static final int CLOSE_SESSION_OPERATION = 11;

	/**
	 * Get the owner of a credit card.
	 */
	public static final int CARD_OWNER_OPERATION = 12;

	/**
	 * Calculate fee.
	 */
	public static final int CALCULATE_FEE_OPERATION = 13;

	/**
	 * Calculate fee 2.
	 */
	public static final int CALCULATE_FEE_OPERATION2 = 14;

	/**
	 * Service payment.
	 */
	public static final int SERVICE_PAYMENT_OPERATION = 15;

	/**
	 * Help image for service payment.
	 */
	public static final int HELP_IMAGE_OPERATION = 16;

	/**
	 * Request frequent service payment operation list.
	 */
	public static final int FAVORITE_PAYMENT_OPERATION = 18;

	/**
	 * Fast Payment service.
	 */
	public static final int FAST_PAYMENT_OPERATION = 19;

	/**
	 * Service enterprise name operation.
	 */
	public static final int SERVICE_NAME_OPERATION = 20;

	/**
	 * Alta de operaciones sin tarjeta
	 */
	public static final int ALTA_OPSINTARJETA = 23;

	/**
	 * Consulta de operaciones sin tarjeta
	 */
	public static final int CONSULTA_OPSINTARJETA = 24;

	/**
	 * Baja de operaciones sin tarjeta
	 */
	public static final int BAJA_OPSINTARJETA = 25;

	/**
	 * Consulta status del servicio
	 */
	public static final int CONSULTA_ESTATUSSERVICIO = 26;

	/**
	 * Cambia perfil del cliente
	 */
	public static final int CAMBIA_PERFIL = 27;

	/**
	 * Consulta comision para transferir interbancario
	 */
	public static final int CONSULTAR_COMISION_I = 28;

	/**
	 * Consulta comision para transferir interbancario
	 */
	public static final int CONSULTAR_CODIGO_PAGO_SERVICIOS = 29;

	/**
	 * Preregistro de pago de servicios
	 */
	public static final int PREREGISTRAR_PAGO_SERVICIOS = 30;

	/**
	 * Compra de tiempo aire
	 */
	public static final int COMPRA_TIEMPO_AIRE = 31;

	/**
	 * Alta de frecuente
	 */
	public static final int ALTA_FRECUENTE = 32;

	/**
	 * Consulta del beneficiario
	 */
	public static final int CONSULTA_BENEFICIARIO = 33;

	/**
	 * Baja de frecuente
	 */
	public static final int BAJA_FRECUENTE = 34; // OP144

	/**
	 * Baja de frecuente
	 */
	public static final int CONSULTA_CIE = 35;

	/**
	 * 
	 */
	public static final int ACTUALIZAR_FRECUENTE = 36;

	/**
	 * Actualizacion de preregistrado a Frecuente
	 */
	public static final int ACTUALIZAR_PREREGISTRO_FRECUENTE = 37;

	/**
	 * Cambio de telefono asociado
	 * 
	 */
	public static final int CAMBIO_TELEFONO = 38;

	/**
	 * Cambio de cuenta Asociada
	 */
	public static final int CAMBIO_CUENTA = 39;

	/**
	 * Suspencion Temporal
	 */
	public static final int SUSPENDER_CANCELAR = 40;

	/**
	 * Actualizacion de cuentas del usuario
	 */
	public static final int ACTUALIZAR_CUENTAS = 41;

	public static final int CONSULTA_TARJETA_OPERATION = 42;

	/**
	 * Consulta de limites de operacion
	 */
	public static final int CONSULTAR_LIMITES = 43;

	/**
	 * Cambio de limites de operacion
	 */
	public static final int CAMBIAR_LIMITES = 44;

	/**
	 * Consulta de estatus mantenimiento.
	 */
	public static final int CONSULTA_MANTENIMIENTO = 45;

	/**
	 * Desbloqueo de contraseñas
	 */
	public static final int DESBLOQUEO = 46;

	/**
	 * Quitar suspension
	 */
	public static final int QUITAR_SUSPENSION = 47;

	/**
	 * Reactivacion
	 */
	public static final int OP_ACTIVACION = 48;

	/**
	 * Operación de contratación final para bmovil.
	 */
	public static final int OP_CONTRATACION_BMOVIL_ALERTAS = 49;

	/**
	 * Operación de consulta de terminos y condiciones de uso.
	 */
	public static final int OP_CONSULTAR_TERMINOS = 50;

	public static final int OP_ENVIO_CLAVE_ACTIVACION = 51;

	public static final int OP_CONFIGURAR_CORREO = 52;

	public static final int OP_ENVIO_CORREO = 53;

	public static final int OP_VALIDAR_CREDENCIALES = 54;

	public static final int OP_FINALIZAR_CONTRATACION_ALERTAS = 55;

	/** Activacion softtoken - Consulta de tipo de solicitud. */
	public static final int CONSULTA_TARJETA_ST = 56;

	/** Activacion softtoken - Autenticacion */
	public static final int AUTENTICACION_ST = 57;

	/**
	 * Activacion softtoken
	 */
	public static final int EXPORTACION_SOFTTOKEN = 58;
	/**
	 * Activacion softtoken
	 */
	public static final int SINCRONIZACION_SOFTTOKEN = 59;

	/** Activacion softtoken - Contratacion enrolamiento. */
	public static final int CONTRATACION_ENROLAMIENTO_ST = 60;

	/** Activacion softtoken - Finalizar contratacion. */
	public static final int FINALIZAR_CONTRATACION_ST = 61;

	/** Activacion softtoken - Cambio telefono asociado. */
	public static final int CAMBIO_TELEFONO_ASOCIADO_ST = 62;

	/** Activacion softtoken - Solicitud. */
	public static final int SOLICITUD_ST = 63;

	/** Mantenimiento Alertas. */
	public static final int MANTENIMIENTO_ALERTAS = 64;
	
	/** Consulta de Terminos y Condiciones Sesion */
	
	public static final int OP_CONSULTAR_TERMINOS_SESION = 65;
	
	/** Solicitar Alertas */
	
	public static final int OP_SOLICITAR_ALERTAS = 66;
	
	//Empieza codigo de SPEI revisar donde se usan las constantes
		/* Identifier for the request spei accounts operation.*/
		public static final int SPEI_ACCOUNTS_REQUEST = 67;//66

		/**
		 * Identifier for the request spei terms and conditions.
		 */
		public static final int SPEI_TERMS_REQUEST = 68; //67
		
		/**
		 * The SPEI maintenance operation.
		 */
		public static final int SPEI_MAINTENANCE = 69;//68
		
		/**
		 * The request beneficiary account number operation.
		 */
		public static final int CONSULT_BENEFICIARY_ACCOUNT_NUMBER = 70;//69
	//Termina Codigo de SPEI	
		/** Consulta Interbancarios */
		
		

		
		//One CLick
		/**
		 * Operation codes.
		 */
		/** consulta detalle ofertas ilc*/
		public static final int CONSULTA_DETALLE_OFERTA=71;//67;//66
		public static final int ACEPTACION_OFERTA=72;//68;//67
		public static final int EXITO_OFERTA=73;//69;//68
		public static final int RECHAZO_OFERTA=74;//70;//69
		/**consulta detalle ofertas EFI*/
		public static final int CONSULTA_DETALLE_OFERTA_EFI=75;//71;//70
		public static final int ACEPTACION_OFERTA_EFI=76;//71
		public static final int EXITO_OFERTA_EFI=77;//72
		public static final int SIMULADOR_EFI=78;//73
		/** consulta detalle consumo*/
		public static final int CONSULTA_DETALLE_OFERTA_CONSUMO=79;
		public static final int POLIZA_OFERTA_CONSUMO=80;
		public static final int TERMINOS_OFERTA_CONSUMO=81;
		public static final int EXITO_OFERTA_CONSUMO=82;
		public static final int DOMICILIACION_OFERTA_CONSUMO=83;
		public static final int CONTRATO_OFERTA_CONSUMO=84;
		public static final int RECHAZO_OFERTA_CONSUMO=85;
		public static final int SMS_OFERTA_CONSUMO=86;
		public static final int OP_CONSULTA_INTERBANCARIOS =87;
	public static final int OP_GLOBAL = 141;
		
		/**
		 * Request frequent service payment operation list for BBVA accounts.
		 */
		public static final int FAVORITE_PAYMENT_OPERATION_BBVA = 88;
		
		//Depositos Movil
		public static final int CONSULTA_DEPOSITOS_CHEQUES = 89;//87
		public static final int CONSULTA_DEPOSITOS_EFECTIVO = 90; // Ya no se usa 88
		public static final int CONSULTA_PAGO_SERVICIOS = 91;//89
		public static final int CONSULTA_TRANSFERENCIAS_CUENTA_BBVA = 92;//90
		public static final int CONSULTA_TRANSFERENCIAS_MIS_CUENTAS = 93;//91
		public static final int CONSULTA_TRANSFERENCIAS_A_OTROS_BANCOS = 94;//92
		public static final int CONSULTA_TRANSFERENCIAS_CLIENTES_BANCOMER = 95; // Ya no se usa 93
		public static final int CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS = 96;//94
		public static final int CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO = 97;//95
		public static final int CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO_DETALLE = 98;//96
		public static final int CONSULTA_DEPOSITOS_CHEQUES_DETALLE = 99;//97

		/** consulta importes tarjeta de crédito **/
		public static final int OP_CONSULTA_TDC = 100;
		
		public static final int OP_RETIRO_SIN_TAR = 101;
		
		public static final int CONSULTA_SIN_TARJETA = 102;
		

		/** consulta alta de retiro sin tarjeta**/
		public static final int OP_RETIRO_SIN_TAR_12_DIGITOS = 103;

		/** sincronizaExportaToken **/
		public static final int OP_SINC_EXP_TOKEN = 104;

		public static final int OP_CONSULTA_OTROS_CREDITOS = 105;

	//agregadas paperless
	/** operacion actualizarEstatusEnvioEC*/
	public static final int ACTUALIZAR_ESTATUS_EC=106;
	/** operacion consultarEstadoCuenta*/
	public static final int CONSULTAR_ESTADO_CUENTA=107;
	/** operacion consultarEstatusEnvioEC*/
	public static final int CONSULTAR_ESTATUS_EC=108;
	/** operacion consultaTextoPaperles*/
	public static final int CONSULTAR_TEXTO_PAPERLESS=109;
	/** operacion inhibirEnvioEstadoCuenta*/
	public static final int INHIBIR_ENVIO_EC=110;
	/** operacion obtenerPeriodosEC*/
	public static final int OBTENER_PERIODO_EC=111;

	// SPEI
	public static final int OP_CONSULTA_SPEI = 123;
	public static final int OP_ENVIOCORREO_SPEI = 124;

	//para simular errores en alta de retiro sin tarjeta
	public Boolean errorComunicacionesRetSinTarjeta=false;
	public Boolean errorMontosRetiroSinTarjeta=false;

	
	/**
	 * Base URL for providers.
	 */
	public static String[] BASE_URL_CREDIT;
	/**
	 *  ZERO
	 */
	private static final String ZERO = "0";

	/** OPERACIONES */
	public static final String LOGIN_OPERACION = "login";
	public static final String CALCULO_OPERACION = "calculo";

	//Modified June 8th,2015,
	public static final String CALCULO_ALTERNATIVAS_OPERACION="calculoAlternativas";
	public static final String CONSULTA_CORREO_OPERACION = "consultaCorreo";
	public static final String ENVIO_CORREO_OPERACION = "envioCorreo";
	public static final String CONSULTA_ALTERNATIVAS = "consultaAlternativas";
	public static final String CONSULTA_TDC = "consultaTDC";
	public static final String DETALLE_ALTERNATIVA = "detalleAlternativa";
	//ids migraciona a api server
	public static final int CALCULO_ALTERNATIVAS_OPERACION_ID=142;
	public static final int CONSULTA_CORREO_OPERACION_ID = 143;
	public static final int ENVIO_CORREO_OPERACION_ID = 144;
	public static final int CONSULTA_ALTERNATIVAS_ID = 145;
	public static final int CONSULTA_TDC_ID = 146;
	public static final int DETALLE_ALTERNATIVA_ID = 147;
	public static final int CONTRATA_ALTERNATIVA_CONSUMO_ID =148;
	//Modificacion 50870
	public static final String CONTRATA_ALTERNATIVA_CONSUMO = "contrataAlternativaConsumo ";

	/** Simulaciones de error */
	public static boolean SIMULACION_RESPUESTA_ERROR_LOGIN = false;
	public static boolean SIMULACION_RESPUESTA_ERROR_CALCULO = false;
	public static boolean SIMULACION_RESPUESTA_ERROR_CONSULTA_CORREO = false;
	public static boolean SIMULACION_RESPUESTA_ERROR_ENVIO_CORREO = false;
	public static boolean SIMULACION_RESPUESTA_ERROR_CONSULTA_ALTERNATIVAS = false;
	public static boolean SIMULACION_RESPUESTA_ERROR_CONSULTA_TDC = false;
	public static boolean SIMULACION_RESPUESTA_ERROR_CONSULTA_DETALLE_ALTERNATIVAS = false;


	/**
	 * url de consumo para popup contactame
	 */
	public static final String URL_CONTACTAME = "https://www.bancomermovil.com/mbank/mbank/Descargas/BBVACredit/TLM/webview24042015Prod.html";//"https://www.bancomermovil.net:11443/mbank/mbank/Descargas/BBVACredit/form.html";


	/**
	 * Operation codes.
	 */
	public static final Hashtable<String, String> OPERATION_CODES_CREDIT = new Hashtable<String, String>();

	private static void setupOperationCodesCredit(){
		OPERATION_CODES_CREDIT.put(LOGIN_OPERACION, "103");
		OPERATION_CODES_CREDIT.put(CALCULO_OPERACION, "calculoAlternativas");
		OPERATION_CODES_CREDIT.put(CONSULTA_CORREO_OPERACION, "consultaCorreo");
		OPERATION_CODES_CREDIT.put(ENVIO_CORREO_OPERACION, "envioCorreo");
		OPERATION_CODES_CREDIT.put(CONSULTA_ALTERNATIVAS, "consultaAlternativas");
		OPERATION_CODES_CREDIT.put(CONSULTA_TDC, "104");
		OPERATION_CODES_CREDIT.put(DETALLE_ALTERNATIVA, "detalleAlternativa");
		//Modificacion 50870
		OPERATION_CODES_CREDIT.put(CONTRATA_ALTERNATIVA_CONSUMO, "contrataAlternativaConsumo");
	}

	/**
	 * Path table for provider/operation URLs.
	 */
	public static Hashtable<String, PathsOperacionCredit> OPERATIONS_PATH_CREDIT = new Hashtable<String, PathsOperacionCredit>();

	/**
	 * Operation URLs map.
	 */
	public static Hashtable<String, String> OPERATIONS_MAP_CREDIT = new Hashtable<String, String>();


	/**
	 * Operation providers map.
	 */
	public static Hashtable<String, Integer> OPERATIONS_PROVIDER_CREDIT = new Hashtable<String, Integer>();



	/**
	 * Setup operation for a provider.
	 *
	 * @param operation
	 *            the operation
	 * @param provider
	 *            the provider
	 */

	private static void setupOperationCredit(String operation, int provider) {
		OPERATIONS_MAP_CREDIT.put(
				OPERATION_CODES_CREDIT.get(operation),
				new StringBuffer(BASE_URL_CREDIT[provider]).append(((PathsOperacionCredit) OPERATIONS_PATH_CREDIT.get(operation)).getPathProveedor(provider)).toString());
		OPERATIONS_PROVIDER_CREDIT.put(operation, Integer.valueOf(provider));
	}

	/**
	 * Setup operations for a provider
	 *
	 * @param provider the provider
	 */

	private static void setupOperationsCredit(int provider) {
		setupOperationCredit(LOGIN_OPERACION, provider);
		setupOperationCredit(CALCULO_OPERACION, provider);
		setupOperationCredit(CONSULTA_CORREO_OPERACION, provider);
		setupOperationCredit(ENVIO_CORREO_OPERACION, provider);
		setupOperationCredit(CONSULTA_ALTERNATIVAS, provider);
		setupOperationCredit(CONSULTA_TDC, provider);
		setupOperationCredit(DETALLE_ALTERNATIVA, provider);
		//Modificacion 50870
		setupOperationCredit(CONTRATA_ALTERNATIVA_CONSUMO, provider);
	}

	/**
	 * Setup production server paths
	 */
	private static void setupProductionServerCredit() {
		BASE_URL_CREDIT = new String[]{
				"https://www.bancomermovil.com",
				"http://10.0.3.10:8080/servermobile/Servidor",
				"http://movilok.net"
		};

		OPERATIONS_PATH_CREDIT.put(LOGIN_OPERACION, new PathsOperacionCredit("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		OPERATIONS_PATH_CREDIT.put(CALCULO_OPERACION, new PathsOperacionCredit("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		OPERATIONS_PATH_CREDIT.put(CONSULTA_CORREO_OPERACION, new PathsOperacionCredit("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		OPERATIONS_PATH_CREDIT.put(ENVIO_CORREO_OPERACION, new PathsOperacionCredit("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		OPERATIONS_PATH_CREDIT.put(CONSULTA_ALTERNATIVAS, new PathsOperacionCredit("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		OPERATIONS_PATH_CREDIT.put(CONSULTA_TDC, new PathsOperacionCredit("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		OPERATIONS_PATH_CREDIT.put(DETALLE_ALTERNATIVA, new PathsOperacionCredit("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		//Modificacion 50870
		OPERATIONS_PATH_CREDIT.put(CONTRATA_ALTERNATIVA_CONSUMO, new PathsOperacionCredit("/mbhxp_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

	}

	/**
	 * Setup production server paths
	 */
	private static void setupDevelopmentServerCredit() {
		BASE_URL_CREDIT = new String[]{
				"https://www.bancomermovil.net:11443",
				"http://10.0.3.10:8080/servermobile/Servidor",
				"http://movilok.net"
		};

		OPERATIONS_PATH_CREDIT.put(LOGIN_OPERACION, new PathsOperacionCredit("/eexd_mx_web/servlet/ServletOperacionWeb", "/eexd_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		OPERATIONS_PATH_CREDIT.put(CALCULO_OPERACION, new PathsOperacionCredit("/eexd_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		OPERATIONS_PATH_CREDIT.put(CONSULTA_CORREO_OPERACION, new PathsOperacionCredit("/eexd_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		OPERATIONS_PATH_CREDIT.put(ENVIO_CORREO_OPERACION, new PathsOperacionCredit("/eexd_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		OPERATIONS_PATH_CREDIT.put(CONSULTA_ALTERNATIVAS, new PathsOperacionCredit("/eexd_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		OPERATIONS_PATH_CREDIT.put(CONSULTA_TDC, new PathsOperacionCredit("/eexd_mx_web/servlet/ServletOperacionWeb", "/mbhxp_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		OPERATIONS_PATH_CREDIT.put(DETALLE_ALTERNATIVA, new PathsOperacionCredit("/eexd_mx_web/servlet/ServletOperacionWeb", "/eexd_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));

		//Modificacion 50870
		OPERATIONS_PATH_CREDIT.put(CONTRATA_ALTERNATIVA_CONSUMO, new PathsOperacionCredit("/eexd_mx_web/servlet/ServletOperacionWeb", "/eexd_mx_web/servlet/ServletOperacionWeb","/bancomer/activation1"));
	}

	/**
	 * Get the operation URL.
	 *
	 * @param operation
	 *            the operation id
	 * @return the operation URL
	 */
	public static String getOperationUrlCredit(String operation) {
		String url = (String) OPERATIONS_MAP_CREDIT.get(operation);
		return url;
	}

	/** PROVEEDORES */
	public static final int MOVILOK = 2;
	//end BBVACredit
	/**
	 * Operation codes.
	 */
	public static final String[] OPERATION_CODES = { "00", "01", "02", "103",
			"104", "105", "106", "107", "08", "09", "110", "111", "12", "13",
			"14", "115", "16", "17", "141", "19", "20", "21", "22", "123",
			"124", "125", "26", "cambioPerfil", "113", "130", "145", "109", "118",
			"112", "144", "120", "143", "146", "cambioNumCeluar",
			"cambioCuentaAsociada", "suspenderCancelarServicio",
			"actualizarCuentas", "consultaTarjetaContratacionE",
			"consultaLimites", "configuracionLimites",
			"consultaEstatusMantenimiento", "desbloqueoContrasena",
			"quitarSuspension", "102", "contratacionBMovilAlertas",
			"consultaTerminosCondiciones", "envioClaveActivacion",
			"configuracionCorreo", "envioCorreo", "valCredenciales",
			"finalizarContratacionAlertas", "consultaTarjetaST", "autenticacionToken", "204", "203",
			"contratacionE", "finalizarContratacionST",
			"cambioTelefonoAsociadoE", "solicitudST", "mantenimientoAlertas", 
			"consultaTerminosCondicionesSesion","solicitarAlertas",/* SPEI*/"ConsultaCuentasSPEI",
			"consultaTerminosCondicionesSPEI", "MantenimientoSpei", "ConsultaCuentaTerceros",
			/*OneClick*/"cDetalleOfertaBMovil","aceptacionILCBmovil","exitoILC","noAceptacionBMovil",
			"cDetalleOfertaBMovil","aceptaOfertaEFI","exitoEFI","SimulacionEFI","detalleConsumoBMovil",
			"polizaConsumoBMovil","consultaContratoConsumoBMovil","oneClickBmovilConsumo",
			"consultaDomiciliacionBovedaConsumoBMovil","consultaContratoBovedaConsumoBmovil",
			"noAceptacionBMovil","exitoConsumoBMovil","consultaTransferenciaSPEI","consultaFrecuentesTercerosCE",
			"consultarDepositosCheques","consultarDepositosEfectivo",
			"consultarTransfPagoServicios","consultarTransfBancomer","consultarTransfMisCuentas",
			"consultarTransfOtrosBancos","consultarClientesBancomer","consultarOtrosBancos","consultarDepositosEfectivo",
			"detalleDepositosEfectivo", "detalleDepositosCheques","importesTDC","retiroSinTarjeta",
			"consultaRetiroSinTarjeta", "claveRetiroSinTarjeta", "sincronizaExportaToken", "consultarCreditos",
			/*paperles*/"actualizarEstatusEnvioEC","consultarEstadoCuenta","consultarEstatusEnvioEC","consultaTextoPaperless","inhibirEnvioEstadoCuenta","obtenerPeriodosEC",
			/*relleno*/"", "", "", "", "", "", "", "", "", "", "",
			/*SPEI*/"consultaMovInterbancarios", "envioCorreoSPEI"};

	
	//one Click
			/**
			 * clase de enum para cambio de url;
			 */
			public enum isJsonValueCode{
				ONECLICK, NONE,CONSUMO,DEPOSITOS,PAPERLESS;
			}
			
			public static isJsonValueCode isjsonvalueCode= isJsonValueCode.NONE;
			//Termina One CLick


	/**
	 * Base URL for providers.
	 */
	public static String[] BASE_URL;

	/**
	 * Path table for provider/operation URLs.
	 */
	public static String[][] OPERATIONS_PATH;

	/**
	 * Operation URLs map.
	 */
	public static Hashtable<String, String> OPERATIONS_MAP = new Hashtable<String, String>();

	/**
	 * Operation providers map.
	 */
	public static Hashtable<String, Integer> OPERATIONS_PROVIDER = new Hashtable<String, Integer>();

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_PARAMETER = "OPERACION";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_VALUE = "BAN2O01";
	
	/**
	 * Operation code parameter for RECORTADO.
	 */
	public static final String OPERATION_CODE_VALUE_RECORTADO = "BREC001";

	/**
	 * Operation code parameter for BMOVIL MEJORADO.
	 */
	public static final String OPERATION_CODE_VALUE_BMOVIL_MEJORADO = "MJRS001";
	
	/**
	 * Operation code parameter for DEPOSITOS
	 */
	public static final String JSON_OPERATION_CODE_VALUE_DEPOSITOS = "O300001";

	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE = "BAN2O05";
	

	//one CLick
		/**
		 * Operation code parameter for new ops.
		 */
		public static final String JSON_OPERATION_CODE_VALUE_ONECLICK = "ONEC001";
		/**
		 * 
		 */
		
		//one CLick
		/**
		 * Operation code parameter for new ops.
		 */
		public static final String JSON_OPERATION_CODE_VALUE_CONSUMO = "ONEC002";
		/**
		 * 
		 */
	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE_PAPERLESS = "PPLS001";/***/
	/**
	 *
	 */
	 //one CLick
	/**
	 * Operation code parameter for new ops "contrataAlternativaConsumo" CREDIT .
	 */
	public static final String JSON_OPERATION_CODE_VALUE_CONSUMO_CREDIT = "BCRD001";
	/**
	 *
	 */
	
	/**
	 * Operation code parameter for new ops.
	 */
//	public static final String JSON_OPERATION_SOLICITAR_ALERTAS_CODE_VALUE = "BREC001";
	public static final String JSON_OPERATION_RECORTADO_CODE_VALUE = "BREC001";

	/**
	 * Operation code for mejoras
	 */
	public static final String JSON_OPERATION_MEJORAS_CODE_VALUE = "MEJO001";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_VALUE = "es_ES";

	/**
	 * Determines if operation is fast or frequent payment.
	 */
	public static final String FASTORFRECUENTOPERATION = "FastOrFrequentOperation";

	/**
	 * Charge account text.
	 */
	public static final String CUENTA_CARGO = "cuenta cargo";

	static {
		if (DEVELOPMENT) {
			setupDevelopmentServerCredit();
			setupDevelopmentServer();
		} else {
			setupProductionServerCredit();
			setupProductionServer();
		}

		//BBVACredit
		setupOperationCodesCredit();
		setupOperationsCredit(PROVIDER);
		// SuiteBancomer
		setupOperations(PROVIDER);
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupProductionServer() {
		BASE_URL = new String[] { "https://www.bancomermovil.com",
				// "https://172.17.100.173",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Produccion"
						: "" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "", "/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP01
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP02
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP103
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP104
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP105
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP106
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP107
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP08
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP09
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP110
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP11
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP12
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP13
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP14
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP115
						"/mbhxp_mx_web/img", // OP16
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP17
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP141
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP19
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP20
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP21
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP22
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP123
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP124
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP125
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP26
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP140: cambioPerfil
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP113
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP30
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP146
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP109
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP118
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP112
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP144
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP120
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP143
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP146
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPcambioNumCelular
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPcambioCuentaAsociada
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPsuspenderCancelarServicio
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPactualizarCuentas
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPconsultaTarjetaParaContratacion
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPconsultaLimites
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPcambiarLimites
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPconsultaEstatusMantenimiento
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPdesbloqueo
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPquitarsuspension
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OP102
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPContratacionBmovilAlertas
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondiciones
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPEnvioClaveActivacion
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPConfigurarCorreo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPEnvioCorreo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPvalCredenciales
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb",// OPfinalizaContratacion
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // CONSULTA_TARJETA_ST
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP202
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP203
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP204
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // CONTRATACION_ENROLAMIENTO_ST
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // FINALIZAR_CONTRATACION_ST 				
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // CAMBIO_TELEFONO_ASOCIADO_ST 			
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // SOLICITUD_ST
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // MANTENIMIENTO_ALERTAS
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondicionesSesion
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Solicitud Alertas
						//SPEI revisar
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentasSPEI
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // consultaTerminosCondicionesSPEI
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // MantenimientoSpei
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentaTerceros
						//Termina SPEI
						//One Clcik
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // detalle oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // acepta oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // rechazo oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // detalle oferta efi promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // acepta oferta efi promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito oferta efi promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // simulador efi
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // detalle oferta consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // poliza consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // terminos consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // domiciliacion consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // contrato consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // rechazo consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // sms consumo promociones
						//Termina One click
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consulta Interbancario
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//consultaTercerosCE
						//Depositos movil
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos cheques
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos efectivos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Pago Servicios
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia a cuentas BBVA
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencias a mis cuentas
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia a otros Bancos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencias a clientes Bancomer
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia de otros Bancos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP95: Consulta de depósitos BBVA Bancomer y Efectivo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP96: Consulta de depósitos BBVA Bancomer y Efectivo Detalle
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP97: Consulta de depositos cheques Detalle
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consulta Importes TDC
						//Retiro sin tarjeta
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP alta retiro sin tarjeta
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consulta Tarjeta sin retiro
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consulta Clave 12 Digitos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Sinc Exp Token
						//inicia PAPERLESS
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//actualizarEstatusEnvioEC
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//consultarEstadoCuenta
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//consultarEstatusEnvioEC
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//consultaTextoPaperless
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//inhibirEnvioEstadoCuenta
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//obtenerPeriodosEC
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//
						"", "", "", "", "", "", "", "", "",	"", "", // Relleno
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", //OP Consulta SPEI
						"/mbhxp_mx_web/servlet/ServletOperacionWeb" // OP_ENVIOCORREO_SPEI

				},
				new String[] { "", "/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP01
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP02
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP103
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP104
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP105
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP106
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP107
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP08
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP09
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP110
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP11
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP12
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP13
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP14
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP115
						"/mbhxp_mx_web/img", // OP16
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP17
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP141
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP19
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP20
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP21
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP22
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP123
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP124
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP125
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP26
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP140: cambioPerfil
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP113
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP30
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP146
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP109
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP118
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP112
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP144
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP120
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP143
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP146
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPcambioNumCelular
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPcambioCuentaAsociada
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPsuspenderCancelarServicio
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPactualizarCuentas
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPconsultaTarjetaParaContratacion
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPconsultaLimites
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPcambiarLimites
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPconsultaEstatusMantenimiento
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPdesbloqueo
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPquitarsuspension
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OP102
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPContratacionBmovilAlertas
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondiciones
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPEnvioClaveActivacion
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPConfigurarCorreo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPEnvioCorreo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPValCredenciales
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb",// OPfinalizaContratacion
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // CONSULTA_TARJETA_ST_SOFTTOKEN
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP202
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP203
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP204
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // CONTRATACION_ENROLAMIENTO_SOFTTOKEN
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // FINALIZAR_CONTRATACION_ST				
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // CAMBIO_TELEFONO_ASOCIADO_ST 			
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // SOLICITUD_ST
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // MANTENIMIENTO_ALERTAS
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondicionesSesion
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Solicitud Alertas
						//SPEI revisar
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentasSPEI
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // consultaTerminosCondicionesSPEI
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // MantenimientoSpei
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentaTerceros
						//SPEI Termina
						//One click
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // oferta ilc promociones bmovil
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // rechazo oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // detalle oferta efi promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // acepta oferta efi promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito oferta efi promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // simulador efi
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // detalle oferta consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // poliza consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // terminos consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // domiciliacion consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // contrato consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // rechazo consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // sms consumo promociones
						//Termina one click
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",// OP Consultar Interbancarios
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", //consultaTercerosCE
						//Depositos movil
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos cheques
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos efectivos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Pago Servicios
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia a cuentas BBVA
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencias a mis cuentas
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia a otros Bancos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencias a clientes Bancomer
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia de otros Bancos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP95: Consulta de depósitos BBVA Bancomer y Efectivo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP96: Consulta de depósitos BBVA Bancomer y Efectivo Detalle
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP97: Consulta de depositos cheques Detalle
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consulta Importes TDC
						//Retiro sin tarjeta
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP alta retiro sin tarjeta
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consulta Tarjeta sin retiro
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consulta Clave 12 Digitos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP Sinc Exp Token
						//inicia PAPERLESS
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//actualizarEstatusEnvioEC
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//consultarEstadoCuenta
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//consultarEstatusEnvioEC
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//consultaTextoPaperless
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//inhibirEnvioEstadoCuenta
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//obtenerPeriodosEC
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//
						"", "", "", "", "", "", "", "", "",	"", "", // Relleno
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", //OP Consulta SPEI
						"/mbhxp_mx_web/servlet/ServletOperacionWeb" // OP_ENVIOCORREO_SPEI
				} };
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupDevelopmentServer() {
		BASE_URL = new String[] {
				"https://www.bancomermovil.net:11443",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Desarrollo"
						: "http://189.254.77.54:8080/servermobile/Servidor/Desarrollo" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "", "/eexd_mx_web/servlet/ServletOperacionWeb", // OP01
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP02
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP103
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP104
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP105
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP106
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP107
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP08
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP09
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP110
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP11
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP12
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP13
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP14
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP115
						"/eexd_mx_web/img", // OP16
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP17
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP141
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP19
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP20
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP21
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP22
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP123
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP124
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP125
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP26
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP140: cambioPerfil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP113
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP30
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP146
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP109
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP118
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP112
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP144
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP120
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP143
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP146
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPcambioNumCelular
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPcambioCuentaAsociada
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPsuspenderCancelarServicio
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPactualizarCuentas
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPconsultaTarjetaParaContratacion
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPconsultaLimites
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPcambiarLimites
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPconsultaEstatusMantenimiento
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPdesbloqueo
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPquitarSuspension
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OP102
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPContratacionBmovilAlertas
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondiciones
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPEnvioClaveActivacion
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPConfigurarCorreo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPEnvioCorreo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPValCredenciales
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPfinalizarContratacionAlertas
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // CONSULTA_TARJETA_ST_SOFTTOKEN
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP202
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP203
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP204
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // CONTRATACION_ENROLAMIENTO_SOFTTOKEN
						"/eexd_mx_web/servlet/ServletOperacionWeb", // FINALIZAR_CONTRATACION_ST				
						"/eexd_mx_web/servlet/ServletOperacionWeb", // CAMBIO_TELEFONO_ASOCIADO_ST 			
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // SOLICITUD_ST
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // MANTENIMIENTO_ALERTAS
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondicionesSesion
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Solicitud Alertas
						//SPEI revisar
						"/eexd_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentasSPEI
						"/eexd_mx_web/servlet/ServletOperacionWeb", // consultaTerminosCondicionesSPEI
						"/eexd_mx_web/servlet/ServletOperacionWeb", // MantenimientoSpei
						"/eexd_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentaTerceros
						//Termina SPEI
						//One  click
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // acepta oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // rechazo oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta efi promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // acepta oferta efi promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta efi promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // simulador efi
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // poliza oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // terminos oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // domiciliacion oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // contrato oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // rechazo oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // sms oferta consumo promocion bmovil
						//Termina one click
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Consultar Interbancarios
						"/eexd_mx_web/servlet/ServletOperacionWeb", //consultaTercerosCE
						//Deposito Movil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos con cheques
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos con efectivo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de Pago de Servicios
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia a cuentas BBVA
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias a mis cuentas
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias a otros Bancos
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias a clientes Bancomer
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias de otros Bancos
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP95: Consulta de depósitos BBVA Bancomer y Efectivo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP96: Consulta de depósitos BBVA Bancomer y Efectivo Detalle
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP97: Consulta de depositos con cheques Detalle
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Consultar Importes TDC
						//Retiro sin tarjeta
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP alta retiro sin tarjeta
						"/eexd_mx_web/servlet/ServletOperacionWeb",	// OP Consultar Tarjeta Sin Retiro
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP Consulta Clave 12 Digitos
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Sinc Exp Token
						//inicia PAPERLESS
						"/eexd_mx_web/servlet/ServletOperacionWeb",//actualizarEstatusEnvioEC
						"/eexd_mx_web/servlet/ServletOperacionWeb",//consultarEstadoCuenta
						"/eexd_mx_web/servlet/ServletOperacionWeb",//consultarEstatusEnvioEC
						"/eexd_mx_web/servlet/ServletOperacionWeb",//consultaTextoPaperless
						"/eexd_mx_web/servlet/ServletOperacionWeb",//inhibirEnvioEstadoCuenta
						"/eexd_mx_web/servlet/ServletOperacionWeb",//obtenerPeriodosEC
						"/eexd_mx_web/servlet/ServletOperacionWeb",//
						"/eexd_mx_web/servlet/ServletOperacionWeb",//
						"/eexd_mx_web/servlet/ServletOperacionWeb",//
						"", "", "", "", "", "", "", "", "",	"", "", // Relleno
						"/eexd_mx_web/servlet/ServletOperacionWeb", //OP Consulta SPEI
						"/eexd_mx_web/servlet/ServletOperacionWeb" // OP_ENVIOCORREO_SPEI

				},
				new String[] { "", "/eexd_mx_web/servlet/ServletOperacionWeb", // OP01
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP02
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP103
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP104
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP105
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP106
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP107
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP08
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP09
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP110
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP11
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP12
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP13
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP14
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP115
						"/eexd_mx_web/img", // OP16
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP17
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP141
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP19
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP20
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP21
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP22
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP123
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP124
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP125
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP26
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP140: cambioPerfil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP113
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP30
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP146
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP109
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP118
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP112
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP144
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP120
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP143
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP146
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPcambioNumCelular
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPcambioCuentaAsociada
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPsuspenderCancelarServicio
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPactualizarCuentas
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPconsultaTarjetaParaContratacion
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPconsultaLimites
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPcambiarLimites
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPconsultaEstatusMantenimiento
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPdesbloqueo
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPquitarSuspension
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OP102
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPContratacionBmovilAlertas
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondiciones
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPEnvioClaveActivacion
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPConfigurarCorreo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPEnvioCorreo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPValCredenciales
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPfinalizarContratacionAlertas
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // CONSULTA_TARJETA_ST_SOFTTOKEN
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP202
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP203
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP204
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // CONTRATACION_ENROLAMIENTO_SOFTTOKEN
						"/eexd_mx_web/servlet/ServletOperacionWeb", // FINALIZAR_CONTRATACION_ST				
						"/eexd_mx_web/servlet/ServletOperacionWeb", // CAMBIO_TELEFONO_ASOCIADO_ST 			
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // SOLICITUD_ST
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // MANTENIMIENTO_ALERTAS
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondiciones
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Solicitud Alertas
						//SPEI revisar
						"/eexd_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentasSPEI
						"/eexd_mx_web/servlet/ServletOperacionWeb", // consultaTerminosCondicionesSPEI
						"/eexd_mx_web/servlet/ServletOperacionWeb", // MantenimientoSpei
						"/eexd_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentaTerceros
						//Termina SPEI
						//One Click
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // acepta oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // rechazo oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta efi promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // acepta oferta efi promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta efi promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // simulador efi
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // poliza oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // terminos oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // domiciliacion oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // contrato oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // rechazo oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // sms oferta consumo promocion bmovil
						//Termnina one click
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Consultar Interbancarios
						"/eexd_mx_web/servlet/ServletOperacionWeb", //consultaTercerosCE
						//Deposito Movil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos con cheques
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos con efectivo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de Pago de Servicios
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia a cuentas BBVA
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias a mis cuentas
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias a otros Bancos
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias a clientes Bancomer
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias de otros Bancos
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP95: Consulta de depósitos BBVA Bancomer y Efectivo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP96: Consulta de depósitos BBVA Bancomer y Efectivo Detalle
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP97: Consulta de depositos con cheques Detalle
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Consultar Importes TDC
						//Retiro sin tarjeta
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP alta retiro sin tarjeta
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Consultar Tarjeta sin retiro
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP Consulta Clave 12 Digitos
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Sinc Exp Token
						//inicia PAPERLESS
						"/eexd_mx_web/servlet/ServletOperacionWeb",//actualizarEstatusEnvioEC
						"/eexd_mx_web/servlet/ServletOperacionWeb",//consultarEstadoCuenta
						"/eexd_mx_web/servlet/ServletOperacionWeb",//consultarEstatusEnvioEC
						"/eexd_mx_web/servlet/ServletOperacionWeb",//consultaTextoPaperless
						"/eexd_mx_web/servlet/ServletOperacionWeb",//inhibirEnvioEstadoCuenta
						"/eexd_mx_web/servlet/ServletOperacionWeb",//obtenerPeriodosEC
						"/eexd_mx_web/servlet/ServletOperacionWeb",//
						"/eexd_mx_web/servlet/ServletOperacionWeb",//
						"/eexd_mx_web/servlet/ServletOperacionWeb",//
						"", "", "", "", "", "", "", "", "",	"", "", // Relleno
						"/eexd_mx_web/servlet/ServletOperacionWeb", //OP Consulta SPEI
						"/eexd_mx_web/servlet/ServletOperacionWeb" // OP_ENVIOCORREO_SPEI


				} };
	}

	/**
	 * Setup operations for a provider.
	 * 
	 * @param provider
	 *            the provider
	 */
	private static void setupOperations(int provider) {
		setupOperation(ACTIVATION_STEP1_OPERATION, provider); // OP01
		setupOperation(ACTIVATION_STEP2_OPERATION, provider); // OP02
		setupOperation(LOGIN_OPERATION, provider); // OP103
		setupOperation(MOVEMENTS_OPERATION, provider); // OP104
		setupOperation(SELF_TRANSFER_OPERATION, provider); // OP105
		setupOperation(BANCOMER_TRANSFER_OPERATION, provider); // OP106
		setupOperation(EXTERNAL_TRANSFER_OPERATION, provider); // OP107
		setupOperation(NIPPER_STORE_PURCHASE_OPERATION, provider); // OP08
		setupOperation(NIPPER_AIRTIME_PURCHASE_OPERATION, provider); // OP09
		setupOperation(CHANGE_PASSWORD_OPERATION, provider); // OP110
		setupOperation(CLOSE_SESSION_OPERATION, provider); // OP11
		setupOperation(CARD_OWNER_OPERATION, provider); // OP12
		setupOperation(CALCULATE_FEE_OPERATION, provider); // OP13
		setupOperation(CALCULATE_FEE_OPERATION2, provider); // OP14
		setupOperation(SERVICE_PAYMENT_OPERATION, provider); // OP15
		setupOperation(HELP_IMAGE_OPERATION, provider); // OP16
		setupOperation(17, 0); // OP17
		setupOperation(FAVORITE_PAYMENT_OPERATION, provider); // OP141
		setupOperation(FAST_PAYMENT_OPERATION, provider); // OP19
		setupOperation(SERVICE_NAME_OPERATION, provider); // OP20
		setupOperation(ALTA_OPSINTARJETA, provider); // OP123
		setupOperation(BAJA_OPSINTARJETA, provider); // OP124
		setupOperation(CONSULTA_OPSINTARJETA, provider); // OP125
		setupOperation(CONSULTA_ESTATUSSERVICIO, 0); // OP26
		setupOperation(CAMBIA_PERFIL, provider); // OP140: cambioPerfil
		setupOperation(CONSULTAR_COMISION_I, provider); // OP113
		setupOperation(CONSULTAR_CODIGO_PAGO_SERVICIOS, provider); // OP30
		setupOperation(PREREGISTRAR_PAGO_SERVICIOS, provider); // OP146
		setupOperation(COMPRA_TIEMPO_AIRE, provider); // OP109
		setupOperation(ALTA_FRECUENTE, provider); // OP118
		setupOperation(CONSULTA_BENEFICIARIO, provider); // OP112
		setupOperation(BAJA_FRECUENTE, provider); // OP144
		setupOperation(CONSULTA_CIE, provider); // OP120
		setupOperation(ACTUALIZAR_FRECUENTE, provider); // OP143
		setupOperation(ACTUALIZAR_PREREGISTRO_FRECUENTE, provider);// OP146
		setupOperation(CAMBIO_TELEFONO, provider);// OPcambioNumCelular
		setupOperation(CAMBIO_CUENTA, provider);// OPcambioCuentaAsociada
		setupOperation(SUSPENDER_CANCELAR, provider);// OPsuspenderCancelar
		setupOperation(ACTUALIZAR_CUENTAS, provider);// OPactualizarCuentas
		setupOperation(CONSULTA_TARJETA_OPERATION, provider);// OPconsultaTarjetaParaContratacion
		setupOperation(CONSULTAR_LIMITES, provider); // OPconsultaLimites
		setupOperation(CAMBIAR_LIMITES, provider);// OPcambiarLimites
		setupOperation(CONSULTA_MANTENIMIENTO, provider);// OPconsultaMantenimiento
		setupOperation(DESBLOQUEO, provider);
		setupOperation(QUITAR_SUSPENSION, provider);
		setupOperation(OP_ACTIVACION, provider);
		setupOperation(OP_CONTRATACION_BMOVIL_ALERTAS, provider);
		setupOperation(OP_CONSULTAR_TERMINOS, provider);
		setupOperation(OP_ENVIO_CLAVE_ACTIVACION, provider);
		setupOperation(OP_CONFIGURAR_CORREO, provider);
		setupOperation(OP_ENVIO_CORREO, provider);
		setupOperation(OP_VALIDAR_CREDENCIALES, provider);
		setupOperation(OP_FINALIZAR_CONTRATACION_ALERTAS, provider);
		setupOperation(CONSULTA_TARJETA_ST, provider);
		setupOperation(AUTENTICACION_ST, provider);// OP202
		setupOperation(SINCRONIZACION_SOFTTOKEN, provider);// OP203
		setupOperation(EXPORTACION_SOFTTOKEN, provider);// OP204
		setupOperation(CONTRATACION_ENROLAMIENTO_ST, provider);
		setupOperation(FINALIZAR_CONTRATACION_ST, provider);
		setupOperation(CAMBIO_TELEFONO_ASOCIADO_ST, provider);
		setupOperation(SOLICITUD_ST, provider);
		setupOperation(MANTENIMIENTO_ALERTAS, provider);
		setupOperation(OP_CONSULTAR_TERMINOS_SESION, provider);
		setupOperation(OP_SOLICITAR_ALERTAS, provider);
		//SPEI 
		setupOperation(SPEI_ACCOUNTS_REQUEST, provider);// SpeiAccountsRequest
		setupOperation(SPEI_TERMS_REQUEST, provider);// SpeiTermsRequest
		setupOperation(SPEI_MAINTENANCE, provider);// MantenimientoSpei
		setupOperation(CONSULT_BENEFICIARY_ACCOUNT_NUMBER, provider);// MantenimientoSpei
		//Termina SPEI
		//One click
		setupOperation(CONSULTA_DETALLE_OFERTA, provider);
		setupOperation(ACEPTACION_OFERTA, provider);
		setupOperation(EXITO_OFERTA, provider);
		setupOperation(RECHAZO_OFERTA, provider);
		setupOperation(ACEPTACION_OFERTA_EFI, provider);
		setupOperation(CONSULTA_DETALLE_OFERTA_EFI, provider);
		setupOperation(SIMULADOR_EFI, provider);
		setupOperation(EXITO_OFERTA_EFI, provider);
		setupOperation(CONSULTA_DETALLE_OFERTA_CONSUMO, provider);
		setupOperation(POLIZA_OFERTA_CONSUMO, provider);
		setupOperation(TERMINOS_OFERTA_CONSUMO, provider);
		setupOperation(EXITO_OFERTA_CONSUMO, provider);
		setupOperation(DOMICILIACION_OFERTA_CONSUMO, provider);
		setupOperation(CONTRATO_OFERTA_CONSUMO, provider);
		setupOperation(RECHAZO_OFERTA_CONSUMO, provider);
		setupOperation(SMS_OFERTA_CONSUMO, provider);
		//Termina One click
		setupOperation(OP_CONSULTA_INTERBANCARIOS, provider);
		setupOperation(FAVORITE_PAYMENT_OPERATION_BBVA, provider);
		//Deposito movil
		setupOperation(CONSULTA_DEPOSITOS_CHEQUES, provider);
		setupOperation(CONSULTA_DEPOSITOS_EFECTIVO, provider);
		setupOperation(CONSULTA_PAGO_SERVICIOS, provider);
		setupOperation(CONSULTA_TRANSFERENCIAS_CUENTA_BBVA, provider);
		setupOperation(CONSULTA_TRANSFERENCIAS_MIS_CUENTAS, provider);
		setupOperation(CONSULTA_TRANSFERENCIAS_A_OTROS_BANCOS, provider);
		setupOperation(CONSULTA_TRANSFERENCIAS_CLIENTES_BANCOMER, provider);
		setupOperation(CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS, provider);
		setupOperation(CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO, provider);
		setupOperation(CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO_DETALLE, provider);
		setupOperation(CONSULTA_DEPOSITOS_CHEQUES_DETALLE, provider);
		setupOperation(OP_CONSULTA_TDC,provider);
		//Retiro sin tarjeta
		setupOperation(OP_RETIRO_SIN_TAR,provider);
		setupOperation(CONSULTA_SIN_TARJETA,provider);
		setupOperation(OP_RETIRO_SIN_TAR_12_DIGITOS, provider);
		setupOperation(OP_SINC_EXP_TOKEN, provider);
		//PAPERLESS
		setupOperation(ACTUALIZAR_ESTATUS_EC, provider);
		setupOperation(CONSULTAR_ESTADO_CUENTA, provider);
		setupOperation(CONSULTAR_ESTATUS_EC, provider);
		setupOperation(CONSULTAR_TEXTO_PAPERLESS, provider);
		setupOperation(INHIBIR_ENVIO_EC, provider);
		setupOperation(OBTENER_PERIODO_EC, provider);
		//SPEI
		setupOperation(OP_CONSULTA_SPEI, provider);
		setupOperation(OP_ENVIOCORREO_SPEI, provider);
	}

	/**
	 * Setup operation for a provider.
	 * @param operation the operation
	 * @param provider the provider
	 */
	private static void setupOperation(int operation, int provider) {
		String code = OPERATION_CODES[operation];
		OPERATIONS_MAP.put(
				code,
				new StringBuffer(BASE_URL[provider]).append(
						OPERATIONS_PATH[provider][operation]).toString());
		OPERATIONS_PROVIDER.put(code, new Integer(provider));
	}

	/**
	 * Get the operation URL.
	 * @param operation the operation id
	 * @return the operation URL
	 */
	public static String getOperationUrl(String operation) {
		String url = (String) OPERATIONS_MAP.get(operation);
		return url;
	}

	// ///////////////////////////////////////////////////////////////////////////
	// Parameter definition for application interface //
	// ///////////////////////////////////////////////////////////////////////////

	/**
	 * The username.
	 */
	public static final String USERNAME_PARAM = "username";

	/**
	 * The password.
	 */
	public static final String PASSWORD_PARAM = "password";
	
	/**
	 * The IUM. SPEI
	 */
	public static final String IUM_PARAM = "ium";

	/**
	 * The version of catalog 1.
	 */
	public static final String VERSION_C1_PARAM = "version_c1";

	/**
	 * The version of catalog 4.
	 */
	public static final String VERSION_C4_PARAM = "version_c4";

	/**
	 * The version of catalog 5.
	 */
	public static final String VERSION_C5_PARAM = "version_c5";

	/**
	 * The version of catalog 8.
	 */
	public static final String VERSION_C8_PARAM = "version_c8";

	/**
	 * The version of catalog Tiempo aire.
	 */
	public static final String VERSION_TA_PARAM = "version_ta";

	/**
	 * The version of catalog Dinero movil.
	 */
	public static final String VERSION_DM_PARAM = "version_dm";
	
	
	/**
	 * The version of catalog Servicios.
	 */
	public static final String VERSION_SV_PARAM = "version_sv";

	public static final String VERSION_AU_PARAM = "version_au";
	//SPEI revisar
		/* The params for the SPEI catalog version.
		 */
		public static final String VERSION_MS_PARAM = "version_ms";
		//Termina SPEI

	/**
	 * Vía 1 o 2.
	 */
	public static final String VIA_PARAM = "via";
	/**
	 * The bancomer token.
	 */
	public static final String BANCOMER_TOKEN_PARAM = "sello_bancomer";

	/**
	 * The account.
	 */
	public static final String ACCOUNT_PARAM = "account";

	/**
	 * Origin account.
	 */
	public static final String ORIGIN_PARAM = "origin";

	/**
	 * Destination account.
	 */
	public static final String DESTINATION_PARAM = "destination";

	/**
	 * Money to transfer.
	 */
	public static final String AMOUNT_PARAM = "amount";

	/**
	 * Destination card number.
	 */
	public static final String CARDNUMBER_PARAM = "cardowner";

	/**
	 * Store.
	 */
	public static final String STORE_PARAM = "store";

	/**
	 * Cellular operator.
	 */
	public static final String OPERATOR_PARAM = "operator";
	
	//SPEI revisar

		/**
		 * Telephone number.
		 */
		public static final String PHONENUMBER_PARAM = "phonenumber";

		/**
		 * Old telephone number.
		 */
		public static final String OLD_PHONENUMBER_PARAM = "oldphonenumber";
	    //Termina SPEI

	/**
	 * Old password.
	 */
	public static final String OLD_PASSWORD_PARAM = "oldpassword";

	/**
	 * New password.
	 */
	public static final String NEW_PASSWORD_PARAM = "newpassword";

	/**
	 * Activation code.
	 */
	public static final String ACTIVATION_CODE_PARAM = "activation";

	/**
	 * User identifier.
	 */
	public static final String USER_ID_PARAM = "userid";

	/**
	 * Account type.
	 */
	public static final String ACCOUNT_TYPE_PARAM = "account_type";

	/**
	 * User identifier.
	 */
	public static final String BANK_PARAM = "userid2";

	/**
	 * Transfer receiver name identifier.
	 */
	public static final String RECEIVER_PARAM = "receiver";

	/**
	 * Transfer reference identifier.
	 */
	public static final String REFERENCE_PARAM = "reference";

	/**
	 * Transfer reason identifier.
	 */
	public static final String REASON_PARAM = "reason";

	/**
	 * Card type identifier.
	 */
	public static final String CARD_TYPE_PARAM = "card_type";

	/**
	 * Product type identifier.
	 */
	public static final String PRODUCT_TYPE_PARAM = "product_type";

	/**
	 * screen size identifier.
	 */
	public static final String SCREEN_SIZE_PARAM = "screen_size";

	/**
	 * Type of help image identifier.
	 */
	public static final String HELP_IMAGE_TYPE_PARAM = "help_image_type";

	/**
	 * Service provider identifier.
	 */
	public static final String SERVICE_PROVIDER_PARAM = "service_provider";

	/**
	 * CIE Agreement identifier.
	 */
	public static final String CIE_AGREEMENT_PARAM = "cie_agreement";

	/**
	 * Operation type.
	 */
	public static final String TYPE_OPER = "type_oper";

	/**
	 * Nick name.
	 */
	public static final String NICK_NAME_PARAM = "nickname";

	/**
	 * AP.
	 */
	public static final String IDNUMBER = "idNumber";

	/**
	 * CA.
	 */
	public static final String CUENTA_ABONO = "cuentaAbono";

	/**
	 * Payment operation code.
	 */
	public static final String PAYMENT_OPER_PARAM = "payment_oper";

	/**
	 * Payment ID.
	 */
	public static final String PAYMENT_ID_PARAM = "payment_id";

	/**
	 * Marca y modelo que se envía en Login, se concatenan las cadenas.
	 */
	public static final String MARCA_MODELO = "Marca Modelo";

	/**
	 * Plataforma en la que corre el midlet, en este caso es Android.
	 */
	public static final String PLATAFORMA = "Plataforma";

	/**
	 * Estatus de la operación sin tarjeta para efectivo m�vil, se utiliza en la
	 * consulta de Efectivo m�vil. Los estatus pueden ser VG=vigente,
	 * CN=Cancelada, CD=Expirada
	 */
	public static final String ESTATUS_OPSINTARJETA_PARAM = "estatus";

	/**
	 * Código de la operación de efectivo m�vil.
	 */
	public static final String FOLIO_OPERACION_PARAM="FolioOperacion";
	/**
	 * Código de la operación de CLAVE 12 DIGITOS RETIRO SIN TARJETA.
	 */
	public static final String FOLIO_OPERACION_RETIRO_SIN_TAR = "folioOperacion";

	/**
	 * Código del canal de efectivo m�vil.
	 */
	public static final String CODIGO_CANAL_PARAM = "CodigoCanal";

	/**
	 * Fecha de alta de la operación, efectivo m�vil.
	 */
	public static final String FECHA_OPERACION = "FechaAlta";

	public static final String NUMERO_CLIENTE_PARAM = "NumeroCliente";

	public static final String BANK_CODE_PARAM = "CodigoBanco";

	public static final String BENEFICIARIO_PARAM = "Beneficiario";

	public static final String MOTIVO_PAGO_PARAM = "MotivoPago";
	
	//SPEI Revisar
		public static final String OLD_COMPANY_NAME_PARAM = "companiaVieja";
		
		public static final String NEW_COMPANY_NAME_PARAM = "companiaNueva";
		//Termina SPEI

	public static final String REFERENCIA_NUMERICA_PARAM = "ReferenciaNumeria";
	public static final String NIP_PARAM = "Nip";
	public static final String CVV2_PARAM = "cvv2";
	public static final String VA_PARAM = "InstruccionValidacion";
	public static final String PAYMENT_CODE_PARAM = "QR";
	public static final String MARCA = "marca";
	public static final String MODELO = "modelo";

	public static final String DESCRIPCION_PARAM = "descripcion";
	public static final String CONCEPTO_PARAM = "concepto";
	public static final String TIPO_CONSULTA_PARAM = "tipoconsulta";
	public static final String OPERADORA_PARAM = "operadora";
	public static final String ID_TOKEN = "idToken";
	public static final String TIPO_COMISION_PARAM = "tipocomision";

	public static final String C1_PARAM = "C1";
	public static final String CN_PARAM = "CN";
	public static final String DE_PARAM = "DE";
	public static final String C2_PARAM = "C2";
	public static final String C3_PARAM = "C3";

	public static final String AUTH_CAT_PARAM = "authenticationVersion";
	public static final String COMPANIES_PARAM = "companiesVersion";

	public static final String ALERTSTATUS_PARAM = "estadoAlertas";

	/**
	 * Tags JSON
	 */
	public static final String J_NIP = "codigoNIP";
	public static final String J_CVV2 = "codigoCVV2";
	public static final String J_AUT = "cadenaAutenticacion";
	public static final String J_CUENTA_N = "cuentaNueva";
	public static final String J_CUENTA_A = "cuentaAnterior";
	public static final String J_CUENTA = "cuenta";
	
	//SPEI
	public static final String J_NUM_TELEFONO = "numeroTelefono";	
	public static final String J_NUM_CLIENTE = "numeroCliente";
	public static final String J_CVE_ACCESO = "cveAcceso";
	public static final String J_OTP = "codigoOTP";

	public static final String VOUCHER_TYPE_PARAM = "voucherType";
	public static final String SUBJECT_PARAM = "subject";
	public static final String CLIENTNAME_PARAM = "clientName";
	public static final String CELLPHONENUMBER_PARAM = "cellphoneNumber";
	public static final String OPERATION_DATE_PARAM = "operationDate";
	public static final String OPERATION_HOUR_PARAM = "operationHour";
	public static final String SECURITY_CODE_PARAM = "securityCode";
	public static final String REGISTER_DATE_PARAM = "registerDate";
	public static final String VALIDITY_DATE_PARAM = "validityDate";
	public static final String AGREEMENT_PARAM = "agreement";
	public static final String SERVICE_PARAM = "service";
	public static final String MSG_PARAM = "mensajeA";
	
	//SPEI revisar
		/**
		 * Last digists of the card for authentication prupose.
		 */
		public static final String LAST_CARD_DIGITS_PARAM = "tarjeta5Dig";
		/**
		 * The param for the maintenance type.
		 */
		public static final String SPEI_MAINTENANCE_TYPE_PARAM = "tipoMantenimientoSpei";
		
		//termina SPEI

	/**
	 * Reference to HttpInvoker, which performs the network connection.
	 */
	private HttpInvoker httpClient = null;

	/**
	 * simulation option for the login.
	 */
	private static boolean simulateDeactivation = false;
	
	public static String JSON_OPERATION_CONSULTA_INTERBANCARIOS="BXCO001";


	/**
	 * Referencia a ClienteHttp, en lugar de HttpInvoker.
	 */
	private ClienteHttp clienteHttp = null;

	// protected Context mContext;

	public ClienteHttp getClienteHttp() {
		return clienteHttp;
	}

	/**
	 * BBVACredit
	 * Reference to HttpInvoker, which performs the network connection
	 */
	private HttpInvokerCredit httpClientCredit = null;
	/**
	 * Default constructor.
	 * Default constructor.
	 */
	public Server() {
		httpClient = new HttpInvoker();
		clienteHttp = new ClienteHttp();
		if(ALLOW_LOG) Log.d("CLIENTE HTTP BMOVIL", clienteHttp.getClient().toString());
		httpClientCredit = new HttpInvokerCredit();

		/**
		 * Configuracion de los paramtros comunes en API Commons.
		 * Esto es necesario hacerlo aquí ya que solo se debe hacer una vez y no en la configuración de la API
		 * porque se repetiría código.
		 */
		CommonSession cs = CommonSession.getInstance();
		cs.getCommonSessionService().setAllowLog(ALLOW_LOG);
	}

	/**
	 * Perform the login operation.
	 * @param params parameters for the login operation
	 * @return the response from the server
	 * @throws IOException Input or output exception
	 * @throws ParsingException parsing exception
	 */
	private ServerResponse register(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

		ServerResponse response = new ServerResponse();

		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.OK), response);
		} else {
			NameValuePair[] parameters = new NameValuePair[5];
			parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
			parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
			parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[3] = new NameValuePair("MR", (String) params.get(MARCA_MODELO));
			parameters[4] = new NameValuePair("CT", "ANDROID");

			clienteHttp.invokeOperation(OPERATION_CODES[ACTIVATION_STEP1_OPERATION], parameters, response, true,false);
		}
		return response;
	}

	/**
	 * Perform the activate application operations.
	 * @param params the params to activation
	 * @return the response from the server
	 * @throws IOException exception
	 * @throws ParsingException exception
	 */
	private ServerResponse activate(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();

		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.OK), response);
		} else {
			NameValuePair[] parameters = new NameValuePair[4];
			parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
			parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
			parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[3] = new NameValuePair("AC", (String) params.get(ACTIVATION_CODE_PARAM));

			clienteHttp.invokeOperation(OPERATION_CODES[ACTIVATION_STEP2_OPERATION], parameters,response,false,false);
		}

		return response;
	}

	/**
	 * Perform the login operations.
	 * @param params the params to login
	 * @return the response from the server
	 * @throws IOException exception
	 * @throws ParsingException exception
	 */
	private ServerResponse login(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

		LoginData data = new LoginData();
		ServerResponse response = new ServerResponse(data);

		if (SIMULATION) {
			if (simulateDeactivation) {
				simulateDeactivation = false;
                this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.LOGIN_OK),response);
			} else {
                obtenerValores();
				String message1 = CURRENT_ACCOUNT_SET;
				String versionC1 = (String) params.get(VERSION_C1_PARAM);
				String versionC4 = (String) params.get(VERSION_C4_PARAM);
				String versionC5 = (String) params.get(VERSION_C5_PARAM);
				String versionC8 = (String) params.get(VERSION_C8_PARAM);
				String versionTA = (String) params.get(VERSION_TA_PARAM);
				String versionDM = (String) params.get(VERSION_DM_PARAM);
				String versionSV = (String) params.get(VERSION_SV_PARAM);
				String versionSPEI = (String) params.get("MS");

				if (versionC1 != null && versionC4 != null && versionC5 != null
						&& versionC8 != null && versionTA != null
						&& versionDM != null && versionSV != null) {
					// if the currently received catalog version is the same as
					// the version that we have in the simulated server, don't
					// send this catalog
					boolean msgRefAdded = false;

					if (!versionC1.equals("6")) {
                        message1 += oReadDocument.read(ConstantsPropertyList.VERSION_C1);
					}
					if (!versionC4.equals("6")) {
                        message1 += oReadDocument.read(ConstantsPropertyList.VERSION_C4);
					}
					if (!versionC5.equals("6")) {
                        message1 += oReadDocument.read(ConstantsPropertyList.VERSION_C5);
					}
					if (!versionC8.equals("1")) {// Se Agrego un if comparando 1
                        message1 += oReadDocument.read(ConstantsPropertyList.VERSION_C8);
					}
					if (!versionTA.equals("1")) {// Se Agrego un if comparando 1
						if (!msgRefAdded) {
							msgRefAdded = true;
                            message1 += oReadDocument.read(ConstantsPropertyList.VERSION_TA1);
                            if (CURRENT_ACCOUNT_SET.equals(oReadDocument.read(ConstantsPropertyList.OPTIONAL_UPDATE))) {
                                message1 += oReadDocument.read(ConstantsPropertyList.VERSION_TA2);
							}
                            message1 += oReadDocument.read(ConstantsPropertyList.VERSION_TA3)
                                    + oReadDocument.read(ConstantsPropertyList.CORREO_EJEMPLO) + CURRENT_PROFILE_STRING
                                    + CURRENT_FAST_PAYMENT_SET + oReadDocument.read(ConstantsPropertyList.VERSION_TA4);
						}
                            message1 += oReadDocument.read(ConstantsPropertyList.VERSION_TA5);
					}
					if (!versionDM.equals("1")) {// Se Agrego un if comparando 1
						if (!msgRefAdded) {
							msgRefAdded = true;
                            message1 += oReadDocument.read(ConstantsPropertyList.VERSION_DM1);
                            if (CURRENT_ACCOUNT_SET.equals(oReadDocument.read(ConstantsPropertyList.OPTIONAL_UPDATE))) {
                                message1 += oReadDocument.read(ConstantsPropertyList.VERSION_DM2);
							}
                                message1 += oReadDocument.read(ConstantsPropertyList.VERSION_DM3)
                                        + oReadDocument.read(ConstantsPropertyList.CORREO_EJEMPLO) + CURRENT_PROFILE_STRING
                                        + CURRENT_FAST_PAYMENT_SET + oReadDocument.read(ConstantsPropertyList.VERSION_DM4);
						}
                            message1 += oReadDocument.read(ConstantsPropertyList.VERSION_DM5);
					}
					if (!versionSV.equals("1")) {// Se Agrego un if comparando 1
						if (!msgRefAdded) {
							msgRefAdded = true;
                            message1 += oReadDocument.read(ConstantsPropertyList.VERSION_SV1);
                            if (CURRENT_ACCOUNT_SET.equals(oReadDocument.read(ConstantsPropertyList.OPTIONAL_UPDATE))) {
                                message1 += oReadDocument.read(ConstantsPropertyList.VERSION_SV2);
							}
                            message1 += oReadDocument.read(ConstantsPropertyList.VERSION_SV3)
                                    + oReadDocument.read(ConstantsPropertyList.CORREO_EJEMPLO) + CURRENT_PROFILE_STRING
                                    + CURRENT_FAST_PAYMENT_SET + oReadDocument.read(ConstantsPropertyList.VERSION_SV4);
                        }
                        message1 += oReadDocument.read(ConstantsPropertyList.VERSION_SV5);
					}
					if (!versionSPEI.equals("1")) {
						if (!msgRefAdded) {
							msgRefAdded = true;
                            message1 += oReadDocument.read(ConstantsPropertyList.VERSION_SPEI1);
                            if (CURRENT_ACCOUNT_SET.equals(oReadDocument.read(ConstantsPropertyList.OPTIONAL_UPDATE))) {
                                message1 += oReadDocument.read(ConstantsPropertyList.VERSION_SPEI2);
							}
                            message1 += oReadDocument.read(ConstantsPropertyList.VERSION_SPEI3)
                                    + oReadDocument.read(ConstantsPropertyList.CORREO_EJEMPLO) + CURRENT_PROFILE_STRING
                                    + CURRENT_FAST_PAYMENT_SET + oReadDocument.read(ConstantsPropertyList.VERSION_SPEI4);
						}
                        message1 += oReadDocument.read(ConstantsPropertyList.VERSION_SPEI5);
					}
					if (!msgRefAdded) {
						msgRefAdded = true;
                        message1 += oReadDocument.read(ConstantsPropertyList.VERSION_OTRA1);
                        if (CURRENT_ACCOUNT_SET.equals(oReadDocument.read(ConstantsPropertyList.OPTIONAL_UPDATE))) {
                            message1 += oReadDocument.read(ConstantsPropertyList.VERSION_OTRA2);
						}
                        message1 += oReadDocument.read(ConstantsPropertyList.VERSION_OTRA3) + oReadDocument.read(ConstantsPropertyList.CORREO_EJEMPLO)
								+ CURRENT_PROFILE_STRING
                                + CURRENT_FAST_PAYMENT_SET + oReadDocument.read(ConstantsPropertyList.VERSION_OTRA4);
					}
                        message1 += oReadDocument.read(ConstantsPropertyList.COMPLETA_VERSION1);
                        message1 += oReadDocument.read(ConstantsPropertyList.COMPLETA_VERSION2);
                        message1 += oReadDocument.read(ConstantsPropertyList.COMPLETA_VERSION3) + LISTA_OPERACIONES
                                + oReadDocument.read(ConstantsPropertyList.COMPLETA_VERSION4);
				} else {
					message1 = CURRENT_ACCOUNT_SET
                            + oReadDocument.read(ConstantsPropertyList.NO_VERSION1);
                        if (CURRENT_ACCOUNT_SET.equals(oReadDocument.read("OPTIONAL_UPDATE"))) {
                            message1 += oReadDocument.read(ConstantsPropertyList.NO_VERSION2);
					}

                        message1 += oReadDocument.read(ConstantsPropertyList.NO_VERSION3)
                                + oReadDocument.read(ConstantsPropertyList.CORREO_EJEMPLO)
							+ CURRENT_PROFILE_STRING
							+ CURRENT_FAST_PAYMENT_SET
                                + oReadDocument.read(ConstantsPropertyList.NO_VERSION4);
                        message1 += oReadDocument.read(ConstantsPropertyList.NO_VERSION5) + LISTA_OPERACIONES
                                + oReadDocument.read(ConstantsPropertyList.NO_VERSION6);
				}
                        message1 += oReadDocument.read(ConstantsPropertyList.DATE_LOGIN1);
				        //one click
                        message1 += oReadDocument.read(ConstantsPropertyList.DATE_LOGIN2);
				
				        //Adding CS Data
                        message1+=oReadDocument.read(ConstantsPropertyList.DATE_LOGIN3);
				
				        //Adding HS Data
                        message1+=oReadDocument.read(ConstantsPropertyList.DATE_LOGIN4);

				if (Constants.STATUS_APP_ACTIVE != CODIGO_ESTATUS_APLICACION)
                    message1 += oReadDocument.read(ConstantsPropertyList.DATE_LOGIN5);

                        if (CURRENT_ACCOUNT_SET.equals(oReadDocument.read(ConstantsPropertyList.MANDATORY_UPDATE))) {
				}
				
				//Trama con ST != A1
				if(Server.ALLOW_LOG) Log.d("Login request", params.toString());
				Tools.trazaTexto("Login response", message1);
				
				this.httpClient.simulateOperation(message1, response);
			}

		} else {

			// String ium = "13A03B213E85AE4C7874CF6AB553A780"; //TODO remove
			// for TEST
			// String ium = "D8E66DE684DA4CCD3019E106F61BAC69"; //TODO Cuenta de
			// Adri

			NameValuePair[] parameters = new NameValuePair[16];
			parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));

			parameters[1] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[2] = new NameValuePair("VM", BmovilConstants.APPLICATION_VERSION);
			parameters[3] = new NameValuePair("C1", (String) params.get(VERSION_C1_PARAM));
			parameters[4] = new NameValuePair("C4", (String) params.get(VERSION_C4_PARAM));
			parameters[5] = new NameValuePair("C5", (String) params.get(VERSION_C5_PARAM));
			parameters[6] = new NameValuePair("C8", (String) params.get(VERSION_C8_PARAM));
			parameters[7] = new NameValuePair("MM", (String) params.get(MARCA_MODELO));
			parameters[8] = new NameValuePair("TA", (String) params.get(VERSION_TA_PARAM));
			parameters[9] = new NameValuePair("DM", (String) params.get(VERSION_DM_PARAM));
			parameters[10] = new NameValuePair("SV", (String) params.get(VERSION_SV_PARAM));
			parameters[11] = new NameValuePair("MS", (String) params.get("MS"));
			//SPEI revisar
			//parameters[12] = new NameValuePair("MS",(String) params.get(VERSION_MS_PARAM));
			parameters[12] = new NameValuePair("AU", (String) params.get(VERSION_AU_PARAM));
			parameters[13] = new NameValuePair("HS",(String) params.get("HS"));
			//Termina SPEI

			parameters[14] = new NameValuePair("NP", (String) params.get("NP"));
			parameters[15] = new NameValuePair("EN", (String) params.get("EN"));

			clienteHttp.invokeOperation(OPERATION_CODES[LOGIN_OPERATION],parameters, response, true,false);
			//BBVA CREDIT INTEGRATION
			Session.getInstance(SuiteApp.appContext).setCookiesBM(this.clienteHttp.getCookiesFromBM());
		}
		return response;
	}

	/**
	 * Perform the close session operation
	 * @param params the client identifier
	 * @return the response from the server
	 * @throws IOException
	 * @throws ParsingException
	 */
	private ServerResponse closeSession(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();

		if (SIMULATION) {	
			if(Server.ALLOW_LOG) Log.d("Close session", params.toString());
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.OK), response);

		} else {
			NameValuePair[] parameters = new NameValuePair[3];
			parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
			parameters[1] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[2] = new NameValuePair("TE", (String) params.get(USER_ID_PARAM));

			clienteHttp.invokeOperation(OPERATION_CODES[CLOSE_SESSION_OPERATION], parameters,response,false,false);
		}

		return response;
	}

	/**
	 * Get the movements of a user's account
	 * @param //account the account number whose last movements will be requested
	 * @return the response from the server
	 * @throws ParsingException
	 */
	private ServerResponse getMovements(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		MovementExtract data = new MovementExtract();

		String accountNumber = (String) params.get(ACCOUNT_PARAM);
		String accountType = (String) params.get(ACCOUNT_TYPE_PARAM);
		String periodo = (String) params.get(ServerConstants.PERIODO_ETIQUETA);
		
		data.setAccountNumber(accountNumber);
		data.setAccountType(accountType);
		ServerResponse response = new ServerResponse(data);

		NameValuePair[] parameters = new NameValuePair[6];
		parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
		parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
		parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[3] = new NameValuePair("TP", accountType);
		parameters[4] = new NameValuePair("AS", accountNumber);
		parameters[5] = new NameValuePair("PE", periodo);
		
		if (SIMULATION) {
			String PE = "0";
			if(periodo.equals(Constants.MOVIMIENTOS_MES_ACTUAL)) {
				PE="0";
			} else if (periodo.equals(Constants.MOVIMIENTOS_MES_ANTERIOR)) {
				PE="1";
			} else if (periodo.equals(Constants.MOVIMIENTOS_DOS_MESES_ATRAS)) {
				PE="2";
			}
			
			if (accountType.equals("LI")) {
				
				if ("0".equals(PE)) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.MOVEMENTSLI_OK1)+PE+oReadDocument.read(ConstantsPropertyList.MOVEMENTSLI_OK2), response);
				} else {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.MOVEMENTSLI_ERROR),response);
				}
				
			}else if (accountType.equals("TC")){
				// Simulamos movs tc
                final String msg = oReadDocument.read(ConstantsPropertyList.MOVEMENTSTC_OK);
				this.httpClient.simulateOperation(msg,response);
			} else {
				String movimientos = "";

				for (int i = 0; i < 10; i++) {
                    movimientos += oReadDocument.read(ConstantsPropertyList.NO_MOVIMIENTOS1)+PE+oReadDocument.read(ConstantsPropertyList.NO_MOVIMIENTOS2);
				}
                this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.NO_MOVIMIENTOS_ERROR), response);
			}
			// this.httpClient.simulateOperation("ESERROR*COMBANK0003*MENo%20hay%20movimientos%20registrados", response);
			/* Remove comment from next line to use the error operation */
			// this.httpClient.simulateOperation("ESERROR*CO043210*MEClave%20de%20activación%20erroneo,%20favor%20de%20verificar",response);
			
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[MOVEMENTS_OPERATION],parameters, response,false,false);
		}

		return response;
	}

	/**
	 * Consulta el estado del cliente
	 * @param //account the account number whose last movements will be requested
	 * @return the response from the server
	 * @throws ParsingException
	 */
	private ServerResponse consultaEstatusDelServicio(Hashtable<String, ?> params) throws IOException, ParsingException,URISyntaxException {
		EstatusDelServicioData data = new EstatusDelServicioData();
		ServerResponse response = new ServerResponse(data);

		String username = (String) params.get(USERNAME_PARAM);
		NameValuePair[] parameters = new NameValuePair[1];
		parameters[0] = new NameValuePair("NT", username);

		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONESTATUS_SERVICIO_OK), response);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[MOVEMENTS_OPERATION],parameters, response,false,false);
		}

		return response;
	}

	/**
	 * Cambia el perfil del cliente.
	 * @param params los parametros a enviar al servidor
	 * @return la respuesta del servidor
	 * @throws IOException excepciones de entrada/salida
	 * @throws ParsingException excepciones de parseos
	 * @throws URISyntaxException excepciones de URIs
	 */
	private ServerResponse cambiaPerfil(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		
		CambioDePerfilData data = new CambioDePerfilData();
		ServerResponse response = new ServerResponse(data);

		NameValuePair[] parameters = new NameValuePair[10];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair(ServerConstants.JSON_IUM_ETIQUETA, (String) params.get(ServerConstants.JSON_IUM_ETIQUETA));
		parameters[2] = new NameValuePair(ServerConstants.NUMERO_CLIENTE, (String) params.get(ServerConstants.NUMERO_CLIENTE));
		
		parameters[3] = new NameValuePair(ServerConstants.PERFIL_NUEVO_ETIQUETA, (String) params.get(ServerConstants.PERFIL_NUEVO_ETIQUETA));
		parameters[4] = new NameValuePair(ServerConstants.CODIGO_NIP, (String) params.get(ServerConstants.CODIGO_NIP));
		parameters[5] = new NameValuePair(ServerConstants.CVE_ACCESO, (String) params.get(ServerConstants.CVE_ACCESO));
		
		parameters[6] = new NameValuePair(ServerConstants.CODIGO_CVV2, (String) params.get(ServerConstants.CODIGO_CVV2));
		parameters[7] = new NameValuePair(ServerConstants.TARJETA_5DIG, (String) params.get(ServerConstants.TARJETA_5DIG));
		parameters[8] = new NameValuePair(ServerConstants.CODIGO_OTP, (String) params.get(ServerConstants.CODIGO_OTP));
		
		parameters[9] = new NameValuePair(ServerConstants.CADENA_AUTENTICACION, (String) params.get(ServerConstants.CADENA_AUTENTICACION));

		if (SIMULATION) {
            clienteHttp.simulateOperation(OPERATION_CODES[CAMBIA_PERFIL],parameters, response, oReadDocument.read(ConstantsPropertyList.CAMBIO_PERFIL), true);

		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CAMBIA_PERFIL],parameters, response, false, true);
		}

		return response;
	}

	/**
	 * Retrieves the name of the enterprise
	 * @param cieAgreement String number
	 * @return the response from the server
	 * @throws ParsingException
	 */
	/*
	 * private ServerResponse getServiceName(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
	 * Service data = new Service(); ServerResponse response = new ServerResponse(data);
	 * 
	 * if (SIMULATION) {
	 * this.httpClient.simulateOperation("ESOK*DEStefanini%20IT%20Solutions", response);
	 * 
	 * } else {
	 * 
	 * NameValuePair[] parameters = new NameValuePair[3];
	 * parameters[0] = new NameValuePair("NT", (String) params.get(PHONENUMBER_PARAM));
	 * parameters[1] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM));
	 * parameters[2] = new NameValuePair("NU", (String) params.get(CIE_AGREEMENT_PARAM));
	 * 
	 * clienteHttp.invokeOperation(OPERATION_CODES[SERVICE_NAME_OPERATION], parameters, response);
	 * 
	 * }
	 * return response;
	 * }
	 */	

	/**
	 * Perform a preregister service payment operation
	 * @param params
	 * @return
	 * @throws IOException
	 * @throws ParsingException
	 */
	private ServerResponse preregistrarServicio(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		PagoServicioResult data = new PagoServicioResult(PagoServicioResult.RESULT_PREREGISTER);
		ServerResponse response = new ServerResponse(data);

		NameValuePair[] parameters = null;
		parameters = new NameValuePair[14];
		parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
		parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
		parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[3] = new NameValuePair("CC", "");
		parameters[4] = new NameValuePair("FN", PAYMENT_SERVICES_PREREGISTER_FUNCTION);
		parameters[5] = new NameValuePair("TP", PAYMENT_SERVICES_PREREGISTER_REASON_TYPE);
		parameters[6] = new NameValuePair("CI", (String) params.get(CIE_AGREEMENT_PARAM));
		parameters[7] = new NameValuePair("IT", (String) params.get(IDENTIFICADOR_TOKEN_PARAM));
		parameters[8] = new NameValuePair("BF", (String) params.get(BENEFICIARIO_PARAM));
		parameters[9] = new NameValuePair("RF", (String) params.get(REFERENCE_PARAM));
		parameters[10] = new NameValuePair("NI", (String) params.get(NIP_PARAM));
		parameters[11] = new NameValuePair("CV", (String) params.get(CVV2_PARAM));
		parameters[12] = new NameValuePair("OT", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[13] = new NameValuePair("VA", (String) params.get(VALIDATION_INSTRUCTIONS_PARAM));

		if (SIMULATION) {
            this.clienteHttp.simulateOperation(OPERATION_CODES[PREREGISTRAR_PAGO_SERVICIOS], parameters, response, oReadDocument.read(ConstantsPropertyList.PREG_SERVICIO_OK));
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[PREREGISTRAR_PAGO_SERVICIOS], parameters, response,false,false);
		}

		return response;
	}

	/**
	 * Gets the fee for an operation.
	 * @param params the params needed to do operation
	 * @return A response with the payment fee
	 * @throws IOException
	 * @throws ParsingException
	 */
	/*
	 * private ServerResponse getFee(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
	 * Fee data = new Fee(); ServerResponse response = new ServerResponse(data);
	 * 
	 * if (SIMULATION) {
	 * this.httpClient.simulateOperation("ESOK*CD5000", response);
	 * } else {
	 * 
	 * NameValuePair[] parameters = new NameValuePair[5];
	 * parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
	 * parameters[1] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM));
	 * parameters[2] = new NameValuePair("CC", (String) params.get(ORIGIN_PARAM));
	 * parameters[3] = new NameValuePair("CA", (String) params.get(DESTINATION_PARAM));
	 * parameters[4] = new NameValuePair("IM", (String) params.get(AMOUNT_PARAM));
	 * 
	 * clienteHttp.invokeOperation(OPERATION_CODES[CALCULATE_FEE_OPERATION], parameters, response);
	 * 
	 * }
	 * return response;
	 * }
	 */

	/**
	 * Gets the fee2 for an operation.
	 * @param params the params needed to do operation
	 * @return A response with the payment fee2
	 * @throws IOException
	 * @throws ParsingException
	 */
	/*
	 * private ServerResponse getFee2(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
	 * Fee data = new Fee();
	 * ServerResponse response = new ServerResponse(data);
	 * 
	 * if (SIMULATION) {
	 * this.httpClient.simulateOperation("ESOK*CD5000", response);
	 * } else {
	 * 
	 * NameValuePair[] parameters = new NameValuePair[5];
	 * parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
	 * parameters[1] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM));
	 * parameters[2] = new NameValuePair("CC", (String) params.get(ORIGIN_PARAM));
	 * parameters[3] = new NameValuePair("CA", (String) params.get(DESTINATION_PARAM));
	 * parameters[4] = new NameValuePair("IM", (String) params.get(AMOUNT_PARAM));
	 * 
	 * clienteHttp.invokeOperation(OPERATION_CODES[CALCULATE_FEE_OPERATION2], parameters, response);
	 * 
	 * }
	 * return response;
	 * }
	 */

	/**
	 * Retrieves the name of the owner of an specified credit card
	 * @param cardNumber String number of the credit card
	 * @return the response from the server
	 * @throws ParsingException
	 */
	/*
	 * private ServerResponse getCardOwner(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
	 * CardOwner data = new CardOwner();
	 * ServerResponse response = new ServerResponse(data);
	 * 
	 * if (SIMULATION) {
	 * 
	 * // this.httpClient.simulateOperation("ESOK*NOJose%20Antonio*PPMoreno*PMGalvez*TPLI*DVMX*AS45673284098123", response);
	 * this.httpClient.simulateOperation("ESOK*NOJose%20Antonio*PPMoreno*PMGalvez*TP*DV*AS", response);
	 * 
	 * } else {
	 * 
	 * NameValuePair[] parameters = new NameValuePair[4];
	 * parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
	 * parameters[1] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM));
	 * parameters[2] = new NameValuePair("TN", (String) params.get(CARDNUMBER_PARAM));
	 * parameters[3] = new NameValuePair("TP", (String) params.get(PRODUCT_TYPE_PARAM));
	 * 
	 * clienteHttp .invokeOperation(OPERATION_CODES[CARD_OWNER_OPERATION], parameters, response);
	 * 
	 * }
	 * return response;
	 * }
	 */

	/**
	 * Realiza la consulta de nombre de beneficiario para transferencias
	 * Bancomer
	 * return la respuesta del servidor
	 */
	private ServerResponse consultaBeneficiario(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		BeneficiarioBancomerData data = new BeneficiarioBancomerData();
		ServerResponse response = new ServerResponse(data);

		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONBENEFICIARIO_OK), response);
		} else {
			NameValuePair[] parameters = new NameValuePair[5];
			parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
			parameters[1] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[2] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE_PARAM));
			parameters[3] = new NameValuePair("TN", (String) params.get(ACCOUNT_PARAM));
			parameters[4] = new NameValuePair("TP", (String) params.get(ACCOUNT_TYPE_PARAM));

			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_BENEFICIARIO], parameters, response,false,false);
		}
		return response;
	}

	/**
	 * Perform a store purchase
	 * @param store the store id
	 * @param amount the amount
	 * @return the response from the server
	 * @throws IOException
	 * @throws ParsingException
	 */
	/*
	 * private ServerResponse performStorePurchase(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
	 * NipperResult data = new NipperResult();
	 * ServerResponse response = new ServerResponse(data);
	 * PaymentObject payment = MBankingApp.getPaymentObject();
	 * 
	 * if (SIMULATION) {
	 * this.httpClient.simulateOperation("ESOK*AS5509*DECAFE%20LA%20SELVA*FO5343515478", response);
	 * } else {
	 * NameValuePair[] parameters = new NameValuePair[6];
	 * parameters[0] = new NameValuePair("NT", payment.getNT());
	 * parameters[1] = new NameValuePair("NP", payment.getNP());
	 * parameters[2] = new NameValuePair("IU", payment.getIU());
	 * parameters[3] = new NameValuePair("TE", payment.getTE());
	 * parameters[4] = new NameValuePair("CN", payment.getCN());
	 * parameters[5] = new NameValuePair("IM", payment.getIM());
	 * 
	 * clienteHttp.invokeOperation(OPERATION_CODES[NIPPER_STORE_PURCHASE_OPERATION], parameters, response);
	 * }
	 * return response;
	 * }
	 */

	/**
	 * Gets and image from the server. Used for getting help in services payment
	 * @params Size, type and provider data
	 * @return the response from the server
	 * @throws IOException
	 * @throws ParsingException
	 */
	private ServerResponse getHelpImage(Hashtable<String, ?> params) throws IOException, ParsingException {
		ServerResponse response = null;

		StringBuffer sb = new StringBuffer();
		sb.append((String) params.get(SCREEN_SIZE_PARAM)).append("_");
		sb.append((String) params.get(SERVICE_PROVIDER_PARAM)).append("_");
		sb.append((String) params.get(HELP_IMAGE_TYPE_PARAM)).append(".png");

		StringBuffer url = new StringBuffer();
		url.append(Server.getOperationUrl(String.valueOf(Server.HELP_IMAGE_OPERATION)));
		url.append("/").append(sb.toString());

		try {
			Bitmap image = this.clienteHttp.getImageFromServer(url.toString());
			response = new ServerResponse(ServerResponse.OPERATION_SUCCESSFUL, null, null, image);
		} catch (Throwable th) {
			response = new ServerResponse(ServerResponse.OPERATION_SUCCESSFUL, null, null, null);
		}

		return response;
	}
	
	/**
	 * Performs a cash transfer defined by amount, from de origin to the
	 * destination.
	 * @param params The params for the transaction.
	 * @return The response from the server.
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse selfTransfer(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		
		NameValuePair[] parameters = new NameValuePair[12];
		parameters[0] = new NameValuePair("NT", (String) params.get(USER_ID_PARAM));
		parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
		parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[3] = new NameValuePair("CC", (String) params.get(CHARGE_ACCOUNT_PARAM));
		parameters[4] = new NameValuePair("CA", (String) params.get(PAYMENT_ACCOUNT_PARAM));
		parameters[5] = new NameValuePair("IM", (String) params.get(AMOUNT_PARAM));
		parameters[6] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE_PARAM));
		parameters[7] = new NameValuePair("NI", (String) params.get(CARD_NIP_PARAM));
		parameters[8] = new NameValuePair("CV", (String) params.get(CARD_CVV2_PARAM));
		parameters[9] = new NameValuePair("OT", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[10] = new NameValuePair("VA", (String) params.get(VALIDATION_INSTRUCTIONS_PARAM));
		parameters[11] = new NameValuePair("MP", (String) params.get(CONCEPTO_PARAM));


		TransferirCuentasPropiasData data = new TransferirCuentasPropiasData();
		ServerResponse response = new ServerResponse(data);

		if (SIMULATION) {
            String simulationResponse = oReadDocument.read(ConstantsPropertyList.SELFTRANFER_OK);
			
			if(Server.ALLOW_LOG) Log.d("SIMULACION", "TRANSFERENCIAS CUENTAS PROPIAS: " + simulationResponse);
						
			clienteHttp.simulateOperation(OPERATION_CODES[SELF_TRANSFER_OPERATION], parameters,response, simulationResponse);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[SELF_TRANSFER_OPERATION], parameters,response,false,false);
		}

		return response;
	}
	
	/**
	 * Perform air-time purchase
	 * @param //store the store id
	 * @param //amount the amount
	 * @return the response from the server
	 * @throws IOException
	 * @throws ParsingException
	 */
	private ServerResponse performAirtimePurchase(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		String operation = OPERATION_CODES[COMPRA_TIEMPO_AIRE];

		CompraTiempoAireResult data = new CompraTiempoAireResult();
		ServerResponse handler = new ServerResponse(data);

		NameValuePair[] parameters = null;
		parameters = new NameValuePair[11];
		parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
		parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
		parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[3] = new NameValuePair("NU", (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[4] = new NameValuePair("CC", (String) params.get(ORIGIN_PARAM));
		parameters[5] = new NameValuePair("OA", (String) params.get(OPERATOR_PARAM));
		parameters[6] = new NameValuePair("IM", (String) params.get(AMOUNT_PARAM));
		parameters[7] = new NameValuePair("NI", (String) params.get(NIP_PARAM));
		parameters[8] = new NameValuePair("CV", (String) params.get(CVV2_PARAM));
		parameters[9] = new NameValuePair("OT", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[10] = new NameValuePair("VA", (String) params.get(VA_PARAM));
		
		if (SIMULATION) {
            String simulationResponse = oReadDocument.read(ConstantsPropertyList.PERFPURCHASE_ERROR2);
			
			if(Server.ALLOW_LOG) Log.d("SIMULACION", "TRANSFERENCIAS COMPRA AIRE: " + simulationResponse);
			
			clienteHttp.simulateOperation(operation, parameters, handler, simulationResponse);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[COMPRA_TIEMPO_AIRE], parameters, handler,false,false);

		}
		return handler;
	}
	
	/**
	 * Perform a service payment operation
	 * @param params
	 * @return
	 * @throws IOException
	 * @throws ParsingException
	 */
	private ServerResponse servicePayment(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		PagoServicioResult data = new PagoServicioResult(PagoServicioResult.RESULT_PAYMENT);
		ServerResponse response = new ServerResponse(data);

		if (SIMULATION) {
            String simulationResponse = oReadDocument.read(ConstantsPropertyList.SERVICEPAYMENT_ERROR2);
			
			if(Server.ALLOW_LOG) Log.d("SIMULACION", "TRANSFERENCIAS PAGO SERVICIOS: " + simulationResponse);
			
			this.httpClient.simulateOperation(simulationResponse, response);
		} else {

			NameValuePair[] parameters = null;
			parameters = new NameValuePair[12];
			parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
			parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
			parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[3] = new NameValuePair("CC", (String) params.get(ORIGIN_PARAM));
			parameters[4] = new NameValuePair("CA", (String) params.get(CIE_AGREEMENT_PARAM));
			parameters[5] = new NameValuePair("RF", (String) params.get(REFERENCE_PARAM));
			parameters[6] = new NameValuePair("CP", (String) params.get(REASON_PARAM));
			parameters[7] = new NameValuePair("IM", (String) params.get(AMOUNT_PARAM));
			parameters[8] = new NameValuePair("NI", (String) params.get(NIP_PARAM));
			parameters[9] = new NameValuePair("CV", (String) params.get(CVV2_PARAM));
			parameters[10] = new NameValuePair("OT", (String) params.get(ServerConstants.CODIGO_OTP));
			parameters[11] = new NameValuePair("VA", (String) params.get(VALIDATION_INSTRUCTIONS_PARAM));

			clienteHttp.invokeOperation(OPERATION_CODES[SERVICE_PAYMENT_OPERATION], parameters, response,false,false);
		}

		return response;
	}
	
	/**
	 * Performs a cash transfer defined by amount, from de origin to the
	 * destination
	 * @return the response from the server
	 */
	private ServerResponse bancomerTransfer(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		TransferResult data = new TransferResult();
		ServerResponse response = new ServerResponse(data);

		if (SIMULATION) {
            String simulationResponse = oReadDocument.read(ConstantsPropertyList.BANCTRANSFER_ERROR1);
			
			if(Server.ALLOW_LOG) Log.d("SIMULACION", "TRANSFERENCIAS CUENTAS TERCEROS: " + simulationResponse);
			
			this.httpClient.simulateOperation(simulationResponse, response);
			
		} else {
			NameValuePair[] parameters = new NameValuePair[13];
			parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
			parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
			parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[3] = new NameValuePair("CC", (String) params.get(ORIGIN_PARAM));
			parameters[4] = new NameValuePair("CA", (String) params.get(DESTINATION_PARAM));
			parameters[5] = new NameValuePair("IM", (String) params.get(AMOUNT_PARAM));
			parameters[6] = new NameValuePair("TP", (String) params.get(ACCOUNT_TYPE_PARAM));
			parameters[7] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE_PARAM));
			parameters[8] = new NameValuePair("MP",(String) params.get(CONCEPTO_PARAM));
			parameters[9] = new NameValuePair("NI",(String) params.get(NIP_PARAM));
			parameters[10] = new NameValuePair("CV",(String) params.get(CVV2_PARAM));
			parameters[11] = new NameValuePair("OT",(String) params.get(ServerConstants.CODIGO_OTP));
			parameters[12] = new NameValuePair("VA",(String) params.get(VALIDATION_INSTRUCTIONS_PARAM));

			clienteHttp.invokeOperation(OPERATION_CODES[BANCOMER_TRANSFER_OPERATION], parameters,response, true,false);
		}
		return response;
	}
	
	/**
	 * Realiza el alta de una operación de Efectivo m�vil
	 * @return the response from the server
	 * @throws IOException
	 * @throws ParsingException
	 */
	private ServerResponse altaOperacionSinTarjeta(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		TransferenciaDineroMovilData data = new TransferenciaDineroMovilData();
		ServerResponse response = new ServerResponse(data);

		int indexCounter = 0;

		NameValuePair[] parameters = new NameValuePair[params.size()];
		parameters[indexCounter++] = new NameValuePair(ServerConstants.NUMERO_TELEFONO_ETIQUETA, (String) params.get(ServerConstants.NUMERO_TELEFONO_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair(ServerConstants.CONTRASENA_ETIQUETA, (String) params.get(ServerConstants.CONTRASENA_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair(ServerConstants.IUM_ETIQUETA, (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair(ServerConstants.NUMERO_CLIENTE_ETIQUETA, (String) params.get(ServerConstants.NUMERO_CLIENTE_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair(ServerConstants.CUENTA_CARGO_ETIQUETA, (String) params.get(ServerConstants.CUENTA_CARGO_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair(ServerConstants.IMPORTE_ETIQUETA, (String) params.get(ServerConstants.IMPORTE_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair(ServerConstants.NOMBRE_BENEFICIARIO_ETIQUETA, (String) params.get(ServerConstants.NOMBRE_BENEFICIARIO_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair(ServerConstants.CONCEPTO_ETIQUETA, (String) params.get(ServerConstants.CONCEPTO_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair(ServerConstants.NUMERO_CELULAR_TERCERO_ETIQUETA, (String) params.get(ServerConstants.NUMERO_CELULAR_TERCERO_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair(ServerConstants.OPERADORA_TELEFONIA_CELULAR_ETIQUETA, (String) params.get(ServerConstants.OPERADORA_TELEFONIA_CELULAR_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair(ServerConstants.NIP_ETIQUETA, (String) params.get(ServerConstants.NIP_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair(ServerConstants.CVV_ETIQUETA, (String) params.get(ServerConstants.CVV_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair(ServerConstants.OTP_TOKEN_ETIQUETA, (String) params.get(ServerConstants.OTP_TOKEN_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair(ServerConstants.INSTRUCCION_VALIDACION_ETIQUETA, (String) params.get(ServerConstants.INSTRUCCION_VALIDACION_ETIQUETA));

		if (SIMULATION) {
            String simulationResponse = oReadDocument.read(ConstantsPropertyList.ALTAOPSIN_TRAJETAOK);
			
			if(Server.ALLOW_LOG) Log.d("SIMULACION", "TRANSFERENCIAS ALTA EFECTIVO MOVIL: " + simulationResponse);
			
			this.clienteHttp.simulateOperation(OPERATION_CODES[ALTA_OPSINTARJETA], parameters, response, simulationResponse);
			
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[ALTA_OPSINTARJETA], parameters, response,false,false);
		}
		return response;
	}

	/**
	 * Performs a cash transfer defined by amount, from de origin to the
	 * destination
	 * @param //origin String The number of the origin account
	 * @param //destination String The number of the destination account/credit card
	 * @param //amount String Amount of the transfer
	 * @return the response from the server
	 * @throws ParsingException
	 */
	private ServerResponse externalTransfer(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		TransferResult data = new TransferResult();
		ServerResponse response = new ServerResponse(data);

		NameValuePair[] parameters = null;
		parameters = new NameValuePair[16];
		parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
		parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
		parameters[2] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE_PARAM));
		parameters[3] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[4] = new NameValuePair("CC", (String) params.get(ORIGIN_PARAM));
		parameters[5] = new NameValuePair("CA", (String) params.get(DESTINATION_PARAM));
		parameters[6] = new NameValuePair("TP", (String) params.get(ACCOUNT_TYPE_PARAM));
		parameters[7] = new NameValuePair("IM", (String) params.get(AMOUNT_PARAM));
		parameters[8] = new NameValuePair("CB", (String) params.get(BANK_CODE_PARAM));
		parameters[9] = new NameValuePair("MP", (String) params.get(MOTIVO_PAGO_PARAM));
		parameters[10] = new NameValuePair("BF", (String) params.get(BENEFICIARIO_PARAM));
		parameters[11] = new NameValuePair("RF", (String) params.get(REFERENCIA_NUMERICA_PARAM));
		parameters[12] = new NameValuePair("NI", (String) params.get(NIP_PARAM));
		parameters[13] = new NameValuePair("CV", (String) params.get(CVV2_PARAM));
		parameters[14] = new NameValuePair("OT", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[15] = new NameValuePair("VA", (String) params.get(VA_PARAM));

		if (SIMULATION) {
            String simulationResponse = oReadDocument.read(ConstantsPropertyList.EXTERNALTRANSFER_OK3);
			
			if(Server.ALLOW_LOG) Log.d("SIMULACION", "TRANSFERENCIAS OTROS BANCOS: " + simulationResponse);
			
			this.clienteHttp.simulateOperation(OPERATION_CODES[EXTERNAL_TRANSFER_OPERATION], parameters, response, simulationResponse);
			
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[EXTERNAL_TRANSFER_OPERATION], parameters, response,false,false);
		}
		return response;
	}

	/**
	 * Performs a fast payment operation that can be either adding, deleting or
	 * consulting the payments. It may return a payment extract object when
	 * consulting the payments.
	 * @param params
	 * @return a server response with a payment extract if consulting, or else empty data
	 * @throws IOException
	 * @throws ParsingException
	 */
	/*
	 * private ServerResponse fastPaymentOperation(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
	 * PaymentObject payment = MBankingApp.getPaymentObject();
	 * 
	 * ServerResponse response = (payment.getNU().equals("C")) ? new ServerResponse( new PaymentExtract()) : new ServerResponse();
	 * 
	 * NameValuePair[] parameters = new NameValuePair[15];
	 * parameters[0] = new NameValuePair("NT", payment.getNT());
	 * parameters[1] = new NameValuePair("NP", payment.getNP());
	 * parameters[2] = new NameValuePair("IU", payment.getIU());
	 * parameters[3] = new NameValuePair("TE", payment.getTE());
	 * 
	 * if (payment.getNU().equals("C")) {
	 * parameters[4] = new NameValuePair("DE", "");
	 * parameters[5] = new NameValuePair("CC", "");
	 * parameters[6] = new NameValuePair("CA", "");
	 * parameters[7] = new NameValuePair("BF", "");
	 * parameters[8] = new NameValuePair("CB", "");
	 * parameters[9] = new NameValuePair("IM", "");
	 * parameters[10] = new NameValuePair("RF", "");
	 * parameters[11] = new NameValuePair("CP", "");
	 * parameters[12] = new NameValuePair("TP", "");
	 * parameters[13] = new NameValuePair("NU", "C");
	 * parameters[14] = new NameValuePair("AP", "");
	 * 
	 * } else if (payment.getNU().equals("A")) {
	 * parameters[4] = new NameValuePair("DE", payment.getDE());
	 * parameters[5] = new NameValuePair("CC", payment.getCC());
	 * parameters[6] = new NameValuePair("CA", payment.getCA());
	 * parameters[7] = new NameValuePair("BF", payment.getBF());
	 * parameters[8] = new NameValuePair("CB", "");
	 * parameters[9] = new NameValuePair("IM", payment.getIM());
	 * parameters[10] = new NameValuePair("RF", payment.getRF());
	 * parameters[11] = new NameValuePair("CP", payment.getCP());
	 * parameters[12] = new NameValuePair("TP", payment.getTP());
	 * parameters[13] = new NameValuePair("NU", "A");
	 * parameters[14] = new NameValuePair("AP", "");
	 * 
	 * } else if (payment.getNU().equals("B")) {
	 * parameters[4] = new NameValuePair("DE", payment.getDE());
	 * parameters[5] = new NameValuePair("CC", "");
	 * parameters[6] = new NameValuePair("CA", "");
	 * parameters[7] = new NameValuePair("BF", "");
	 * parameters[8] = new NameValuePair("CB", "");
	 * parameters[9] = new NameValuePair("IM", "");
	 * parameters[10] = new NameValuePair("RF", "");
	 * parameters[11] = new NameValuePair("CP", "");
	 * parameters[12] = new NameValuePair("TP", "");
	 * parameters[13] = new NameValuePair("NU", "B");
	 * parameters[14] = new NameValuePair("AP", payment.getAP());
	 * }
	 * 
	 * if (!SIMULATION) {
	 * 
	 * clienteHttp.invokeOperation(OPERATION_CODES[FAST_PAYMENT_OPERATION], parameters, response);
	 * 
	 * } else {
	 * 
	 * String responseString = "ESOK";
	 * //"ESERROR*COMBANK00003*ME NO%20Hay%20datos%20a%20listar"
	 * if (payment.getNU().equals("C")) {
	 * 
	 * responseString = "ESOK*OC4*NKTELMEX%201*CCLI0054123443212134*CA1212122323*IM342*BFTELMEX*RFPAGO%20DE%20TEL*CP55122334455*DEPAGO%20TELMEX*CB0001*TO01*AP20100623*NKCABLEVISION*CCCE0054123445628974*CA7625378965*IM10*BFCABLEVISION*RFPLAN%20BASICO%202*CP55122126321*DERENTA%20MENSUAL%20CABLE*CB0001*TO01*AP20100630*NKRenta%20de%20Depa*CCAH0054147280962134*CA7653429871*IM5*BFJOSE%20FERNANDO%20VELAZQUEZ%20HERNANDEZ*RFPAGO%20DE%20RENTA%20DE%20DEPARTAMENTO*CP55112312335*DERENTA%20MENSUAL*CB0001*TO01*AP20100805*NKTiempo%20aire*CC*CA5531197500*IM10050*BFTELCEL*RFRECARGA%20DE%20CEL*CP1234556789*DEMI%20RECARGA*CB*TO06*AP20100803";
	 * }
	 * 
	 * this.httpClient.simulateOperation(responseString, response);
	 * }
	 * return response;
	 * }
	 */

	/**
	 * Change the user's password
	 * @param params the operation parameters
	 * @return the response from the server
	 * @throws IOException on communication errors
	 * @throws ParsingException
	 */
	/* TODO AMB
	public ServerResponse changePassword(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		CambiarPasswordData data = new CambiarPasswordData();
		ServerResponse response = new ServerResponse(data);

		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CHANGEPASSWORD_OK), response);
			
		} else {
			NameValuePair[] parameters = new NameValuePair[4];
			parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
			parameters[1] = new NameValuePair("NP", (String) params.get(OLD_PASSWORD_PARAM));
			parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[3] = new NameValuePair("NN", (String) params.get(NEW_PASSWORD_PARAM));

			clienteHttp.invokeOperation(OPERATION_CODES[CHANGE_PASSWORD_OPERATION], parameters, response,false,false);
		}

		return response;
	}*/

	/**
	 * Performs a favorite payment operation that can be either adding, deleting
	 * or consulting the user payment. It may return a payment extract object
	 * when consulting the payments.
	 * @param params
	 * @return a server response with a payment extract if consulting, or else empty data
	 * @throws IOException
	 * @throws ParsingException
	 */
	private ServerResponse favoritePaymentOperation(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		String tipoConsulta = (String) params.get(TIPO_CONSULTA_PARAM);

		ServerResponse response = null;
		response = new ServerResponse(new PaymentExtract());
		
		/*NameValuePair[] parameters = new NameValuePair[5];
		parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
		parameters[1] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[2] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE_PARAM));
		parameters[3] = new NameValuePair("TP", tipoConsulta);
		parameters[4] = new NameValuePair("AP", "");*/
		
		//frecuentes Nueva
		NameValuePair[] parameters;
		if(tipoConsulta.equals(Constants.tipoCFOtrosBBVA))
		{
			parameters = new NameValuePair[3];
			parameters[0] = new NameValuePair("numeroTelefono", (String) params.get(USERNAME_PARAM));
			parameters[1] = new NameValuePair("IUM", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[2] = new NameValuePair("numeroCliente", (String) params.get(NUMERO_CLIENTE_PARAM));
		}
		else
		{
			parameters = new NameValuePair[5];
			parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
			parameters[1] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[2] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE_PARAM));
			parameters[3] = new NameValuePair("TP", tipoConsulta);
			parameters[4] = new NameValuePair("AP", "");
		}

		if (SIMULATION) {
            String responseString = oReadDocument.read(ConstantsPropertyList.OK);

			if (tipoConsulta.equals(Constants.tipoCFPagosCIE)) {
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFPAGOS_CIE1);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFPAGOS_CIE2);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFPAGOS_CIE3);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFPAGOS_CIE4);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFPAGOS_CIE5);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFPAGOS_CIE6);

			} else if (tipoConsulta.equals(Constants.tipoCFTiempoAire)) {
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCF_TIEMPOAIRE1);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCF_TIEMPOAIRE2);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCF_TIEMPOAIRE3);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCF_TIEMPOAIRE4);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCF_TIEMPOAIRE5);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCF_TIEMPOAIRE6);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCF_TIEMPOAIRE7);

			} else if (tipoConsulta.equals(Constants.tipoCFOtrosBBVA)) {
				//aqui se debe invocar.

                responseString = oReadDocument.read(ConstantsPropertyList.TIPOCFOTROS_BBVA1);

			} else if (tipoConsulta.equals(Constants.tipoCFOtrosBancos)) {
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFOTROS_BANCOS1);

                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFOTROS_BANCOS2);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFOTROS_BANCOS3);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFOTROS_BANCOS4);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFOTROS_BANCOS5);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFOTROS_BANCOS6);

                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFOTROS_BANCOS7);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFOTROS_BANCOS8);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCFOTROS_BANCOS9);

			} else if (tipoConsulta.equals(Constants.tipoCFDineroMovil)) {
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCF_DINEROMOVIL1);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCF_DINEROMOVIL2);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCF_DINEROMOVIL3);

			} else if (tipoConsulta.equals(Constants.tipoCFCExpress)) {
                responseString = oReadDocument.read(ConstantsPropertyList.TIPOCF_CEXPRESS1);

			} else if (tipoConsulta.equals(Constants.tipoCFTarjetasCredito)) {
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCF_TRAJETASCREDITO1);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCF_TRAJETASCREDITO2);
                responseString += oReadDocument.read(ConstantsPropertyList.TIPOCF_TRAJETASCREDITO3);
			}

		}else {	
			
			if (tipoConsulta.equals(Constants.tipoCFOtrosBBVA)) 
				clienteHttp.invokeOperation(OPERATION_CODES[FAVORITE_PAYMENT_OPERATION_BBVA], parameters, response,false,true);
			
			else
				clienteHttp.invokeOperation(OPERATION_CODES[FAVORITE_PAYMENT_OPERATION], parameters, response,false,false);
		}

		return response;
	}

	/**
	 * Perform a store purchase
	 * @param //store the store id
	 * @param //amount the amount
	 * @return the response from the server
	 * @throws IOException
	 * @throws ParsingException
	 */
	private ServerResponse consultaOperacionesSinTarjeta(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ConsultaDineroMovilData data = new ConsultaDineroMovilData();
		ServerResponse response = new ServerResponse(data);

		String estatus = (String) params.get(OPERATION_CODE_PARAMETER);

		NameValuePair[] parameters = new NameValuePair[5];
		parameters[0] = new NameValuePair("NT", (String) params.get(USER_ID_PARAM));
		parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
		parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[3] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE_PARAM));
		parameters[4] = new NameValuePair("EO", estatus);

		if (SIMULATION) {
			String respuesta = null;

			if (estatus.equalsIgnoreCase(Constants.TipoMovimientoDM.VIGENTES.codigo)) {

                respuesta = oReadDocument.read(ConstantsPropertyList.VIGENTES_OK1)
                        + oReadDocument.read(ConstantsPropertyList.VIGENTES_OK2)
                        + oReadDocument.read(ConstantsPropertyList.VIGENTES_OK3)
                        + oReadDocument.read(ConstantsPropertyList.VIGENTES_OK4)
                        + oReadDocument.read(ConstantsPropertyList.VIGENTES_OK5);

			} else if (estatus.equalsIgnoreCase(Constants.TipoMovimientoDM.CANCELADOS.codigo)) {

                respuesta = oReadDocument.read(ConstantsPropertyList.CANCELADOS_OK1) + oReadDocument.read(ConstantsPropertyList.CANCELADOS_OK2);

			} else if (estatus.equalsIgnoreCase(Constants.TipoMovimientoDM.VENCIDOS.codigo)) {
                respuesta = oReadDocument.read(ConstantsPropertyList.VENCIDOS_ERROR)
                        + oReadDocument.read(ConstantsPropertyList.VENCIDOS_OK1)
                        + oReadDocument.read(ConstantsPropertyList.VENCIDOS_OK2)
                        + oReadDocument.read(ConstantsPropertyList.VENCIDOS_OK3);

			}

			this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_OPSINTARJETA], parameters, response, respuesta);

		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_OPSINTARJETA], parameters, response,false,false);
		}

		return response;
	}	

	/**
	 * Realiza la cancelaci�n de una operación de Efectivo m�vil previamente dada de alta
	 * @return the response from the server
	 * @throws IOException
	 * @throws ParsingException
	 */
	private ServerResponse bajaOperacionSinTarjeta(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		BajaDineroMovilData data = new BajaDineroMovilData();
		ServerResponse response = new ServerResponse(data);

		int indexCounter = 0;
		NameValuePair[] parameters = new NameValuePair[params.size()];

		parameters[indexCounter++] = new NameValuePair("NT", (String) params.get(USER_ID_PARAM));
		parameters[indexCounter++] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
		parameters[indexCounter++] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[indexCounter++] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE_PARAM));
		parameters[indexCounter++] = new NameValuePair("FO", (String) params.get(FOLIO_OPERACION_PARAM));
		parameters[indexCounter++] = new NameValuePair("CG", (String) params.get(CODIGO_CANAL_PARAM));
		parameters[indexCounter++] = new NameValuePair("FE", (String) params.get(FECHA_OPERACION));
		
		if (params.containsKey(NIP_PARAM)) {
			parameters[indexCounter++] = new NameValuePair("NI", (String) params.get(NIP_PARAM));
		}
		if (params.containsKey(CVV2_PARAM)) {
			parameters[indexCounter++] = new NameValuePair("CV", (String) params.get(CVV2_PARAM));
		}
		if (params.containsKey(ServerConstants.CODIGO_OTP)) {
			parameters[indexCounter++] = new NameValuePair("OT", (String) params.get(ServerConstants.CODIGO_OTP));
		}
		parameters[indexCounter++] = new NameValuePair("VA", (String) params.get(VALIDATION_INSTRUCTIONS_PARAM));

		if (SIMULATION) {
            this.clienteHttp.simulateOperation(OPERATION_CODES[BAJA_OPSINTARJETA], parameters, response, oReadDocument.read(ConstantsPropertyList.BAJAOPSINTARJETAOK));
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[BAJA_OPSINTARJETA], parameters, response,false,false);
		}
		return response;
	}

	/**
	 * Gets the data for a bar/qr code scan
	 * @param params the params needed to do operation
	 * @return A response with the data belonging to the bar/qr code
	 * @throws IOException
	 * @throws ParsingException
	 */
	public ServerResponse getPaymentCodeData(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		PagoServicio pagoServicio = new PagoServicio();
		ServerResponse response = new ServerResponse(pagoServicio);

		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.GETPAYMENTCODEDATAOK), response);
			
		} else {
			NameValuePair[] parameters = new NameValuePair[2];
			parameters[0] = new NameValuePair("CM", (String) params.get(CIE_AGREEMENT_PARAM));
			parameters[1] = new NameValuePair("QR", (String) params.get(PAYMENT_CODE_PARAM));

			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTAR_CODIGO_PAGO_SERVICIOS], parameters, response,false,false);
		}
		return response;
	}

	/**
	 * perform the network operation identifier by operationId with the
	 * parameters passed.
	 * @param operationId the identifier of the operation
	 * @param params Hashtable with the parameters as key-value pairs.
	 * @return the response from the server
	 * @throws IOException exception
	 * @throws ParsingException exception
	 */
	public ServerResponse doNetworkOperation(int operationId, Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
			Log.e("userinterface", "sim:" + String.valueOf( SIMULATION));
		ServerResponse response = null;
		long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
		Integer provider = (Integer) OPERATIONS_PROVIDER.get(OPERATION_CODES[operationId]);
		if (provider != null) {
			PROVIDER = provider.intValue();
		}

		switch (operationId) {
		case LOGIN_OPERATION:
			response = login(params);
			sleep = sleep * 3;
			break;
		case MOVEMENTS_OPERATION:
			response = getMovements(params);
			break;
		/*
		 * case CARD_OWNER_OPERATION: response = getCardOwner(params); break;
		 */
		case SELF_TRANSFER_OPERATION:
			response = selfTransfer(params);
			break;
		case BANCOMER_TRANSFER_OPERATION:
			response = bancomerTransfer(params);
			break;
		/*
		 * case NIPPER_STORE_PURCHASE_OPERATION: response = performStorePurchase(params); break;
		 */
		case COMPRA_TIEMPO_AIRE:
			response = performAirtimePurchase(params);
			break;
		case CHANGE_PASSWORD_OPERATION:
//			response = changePassword(params);
			break;
		case EXTERNAL_TRANSFER_OPERATION:
			response = externalTransfer(params);
			break;
		case ACTIVATION_STEP1_OPERATION:
			response = register(params);
			break;
		case ACTIVATION_STEP2_OPERATION:
			response = activate(params);
			break;
		case CLOSE_SESSION_OPERATION:
			response = closeSession(params);
			break;
		/*
		 * case CALCULATE_FEE_OPERATION: response = getFee(params); break;
		 * case CALCULATE_FEE_OPERATION2: response = getFee2(params); break;
		 */
		case SERVICE_PAYMENT_OPERATION:
			response = servicePayment(params);
			break;
		case HELP_IMAGE_OPERATION:
			response = getHelpImage(params);
			break;
		/*
		 * case SERVICE_NAME_OPERATION: response = getServiceName(params); break;
		 * case FAST_PAYMENT_OPERATION: response = fastPaymentOperation(params); break;
		 * case FAVORITE_PAYMENT_OPERATION: response = favoritePaymentOperation(params); break;
		 */
		case ALTA_OPSINTARJETA:
			response = altaOperacionSinTarjeta(params);
			break;
		case BAJA_OPSINTARJETA:
			response = bajaOperacionSinTarjeta(params);
			break;
		case CONSULTA_OPSINTARJETA:
			response = consultaOperacionesSinTarjeta(params);
			break;
		case CONSULTA_ESTATUSSERVICIO:
			response = consultaEstatusDelServicio(params);
			break;
		case CAMBIA_PERFIL:
			response = cambiaPerfil(params);
			break;
		case CONSULTAR_COMISION_I:
			response = consultaComision(params);
			break;
		case FAVORITE_PAYMENT_OPERATION:
			response = favoritePaymentOperation(params);
			break;
		case CONSULTAR_CODIGO_PAGO_SERVICIOS:
			response = getPaymentCodeData(params);
			break;
		case PREREGISTRAR_PAGO_SERVICIOS:
			response = preregistrarServicio(params);
			break;
		case ALTA_FRECUENTE:
			response = altaDeFrecuente(params);
			break;
		case CONSULTA_BENEFICIARIO:
			response = consultaBeneficiario(params);
			break;
		case BAJA_FRECUENTE:
			response = bajaDeFrecuente(params);
			break;
		case CONSULTA_CIE:
			response = consultaCIE(params);
			break;
		case ACTUALIZAR_FRECUENTE:
			response = actualizarFrecuente(params);
			break;
		case ACTUALIZAR_PREREGISTRO_FRECUENTE:
			response = actualizarPregistradoAFrecuente(params);
			break;
		case CONSULTA_TARJETA_OPERATION:
			response = consultaTarjetaParaContratacion(params);
			break;
		case CAMBIO_TELEFONO:
//			response = cambioTelefonoAsociado(params);
			break;
		case CAMBIO_CUENTA:
			response = cambioCuentaAsociada(params);
			break;
		case SUSPENDER_CANCELAR:
//			response = suspenderCancelarServicio(params);
			break;
		case ACTUALIZAR_CUENTAS:
			response = actualizarCuentas(params);
			break;
		case CONSULTAR_LIMITES:
			response = consultarLimites(params);
			break;
		case CAMBIAR_LIMITES:
			response = cambiarLimites(params);
			break;
		case CONSULTA_MANTENIMIENTO:
			response = consultaEstatusMantenimiento(params);
			break;
		case DESBLOQUEO:
			response = desbloqueoContrasena(params);
			break;
		case QUITAR_SUSPENSION:
			response = quitarSuspension(params);
			break;
		case OP_ACTIVACION:
			response = realizarActivacion(params);
			break;
		case OP_CONTRATACION_BMOVIL_ALERTAS:
			response = contratacionBmovilAlertas(params);
			break;
		case OP_CONSULTAR_TERMINOS:
			response = consultarTerminos(params);
			break;
		case OP_ENVIO_CLAVE_ACTIVACION:
			response = envioClaveActivacion(params);
			break;
		case OP_CONFIGURAR_CORREO:
//			response = configurarCorreo(params);
			break;
		case OP_ENVIO_CORREO:
			response = enviarCorreo(params);
			break;
		case OP_VALIDAR_CREDENCIALES:
			response = validarCredenciales(params);
			break;
		case OP_FINALIZAR_CONTRATACION_ALERTAS:
			response = finalizarContratacionAlertas(params);
			break;
		case EXPORTACION_SOFTTOKEN:
			response = exportacionST(params);
			break;
		case SINCRONIZACION_SOFTTOKEN:
			response = sincronizacionST(params);
			break;
		case CONTRATACION_ENROLAMIENTO_ST:
			response = contratacionEnrolamiento(params);
			break;
		case FINALIZAR_CONTRATACION_ST:
			response = finalizarContratacionST(params);
			break;
		case CAMBIO_TELEFONO_ASOCIADO_ST:
			response = cambioTelefonoAsociadoST(params);
			break;
		case SOLICITUD_ST:
			response = solicitudST(params);
			break;
		case MANTENIMIENTO_ALERTAS:
			response = mantenimientoAlertas(params);
			break;
		case OP_CONSULTAR_TERMINOS_SESION:
			response = consultarTerminosSesion(params);
			break;
		case OP_SOLICITAR_ALERTAS:
			response = solictarAlertas(params);
			break;
		//SPEI revisar
		//case SPEI_ACCOUNTS_REQUEST:
		//	response = requestSpeiAccounts(params);
		//	break;
		case SPEI_TERMS_REQUEST:
			response = requestSpeiTermsAndConditions(params);
			break;
		case SPEI_MAINTENANCE:
			response = doSpeiMaintenance(params);
			break;
		case CONSULT_BENEFICIARY_ACCOUNT_NUMBER:
			response = consultThirdsAccountBBVA(params);
			break;
			//One click
		case CONSULTA_DETALLE_OFERTA:
			response = consultaDetalleOferta(params);
			break;
		case ACEPTACION_OFERTA:
			response = aceptaOfertaILC(params);
			break;
		case EXITO_OFERTA:
			response = exitoOfertaILC(params);
			break;
		case RECHAZO_OFERTA:
			response = rechazoOfertaILC(params);
			break;
		case CONSULTA_DETALLE_OFERTA_EFI:
			response = consultaDetalleOfertaEFI(params);
			break;
		case ACEPTACION_OFERTA_EFI:
			response = consultaAceptaOfertaEFI(params);
			break;
		case EXITO_OFERTA_EFI:
			response = exitoOfertaEFI(params);
			break;
		case SIMULADOR_EFI:
			response = simuladorEFI(params);
			break;
		case CONSULTA_DETALLE_OFERTA_CONSUMO:
			response = consultaDetalleConsumo(params);
			break;			
		case POLIZA_OFERTA_CONSUMO:
			response = polizaConsumo(params);
			break;
		case TERMINOS_OFERTA_CONSUMO:
			response = terminosConsumo(params);
			break;
		case EXITO_OFERTA_CONSUMO:
			response = exitoConsumo(params);
			break;					
		case DOMICILIACION_OFERTA_CONSUMO:
			response = domiciliacionConsumo(params);
			break;
		case CONTRATO_OFERTA_CONSUMO:
			response = contratoConsumo(params);
			break;
		case RECHAZO_OFERTA_CONSUMO:
			response = rechazoOfertaConsumo(params);
			break;
		case SMS_OFERTA_CONSUMO:
			response = envioSMSOfertaConsumo(params);
			break;
		case OP_CONSULTA_INTERBANCARIOS:
			response = consultaInterbancarios(params);
			break;
			//Termina One click
			//Deposito movil
			case CONSULTA_DEPOSITOS_CHEQUES:
				response = consultaDepositoCheques(params);
				break;
			case CONSULTA_DEPOSITOS_CHEQUES_DETALLE:
				response = consultaDepositoChequesDetalle(params);
				break;
			case CONSULTA_DEPOSITOS_EFECTIVO:
				response = consultaDepositoEfectivo(params);
				break;
			case CONSULTA_PAGO_SERVICIOS:
				response = consultaPagoServicios(params);
				break;
			case CONSULTA_TRANSFERENCIAS_CUENTA_BBVA:
				response = consultaTransferenciasCuentaBBVA(params);
				break;
			case CONSULTA_TRANSFERENCIAS_MIS_CUENTAS:
				response = consultaTransferenciasMisCuentas(params);
				break;
			case CONSULTA_TRANSFERENCIAS_A_OTROS_BANCOS:
				response = consultaTransferenciasOtrosBancos(params);
				break;
			case CONSULTA_TRANSFERENCIAS_CLIENTES_BANCOMER:
				response = consultaTransferenciasClientesBancomer(params);
				break;
			case CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS:
				response = consultaTransferenciasDeOtrosBancos(params);
				break;
			case CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO:
				response = consultaDepositosBBVABancomerYEfectivo(params);
				break;
			case CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO_DETALLE:
				response = consultaDepositosBBVABancomerYEfectivoDetalle(params);
				break;
			case OP_CONSULTA_TDC:
				response = importesTDC(params);
				break;
			case OP_RETIRO_SIN_TAR:
				response = retiroSinTarjeta(params);
				break;
			case CONSULTA_SIN_TARJETA:
				response = consultaRetiroSinTarjeta(params);
				break;
			case OP_RETIRO_SIN_TAR_12_DIGITOS:
				response = clave12DigitosRetiroSinTarjeta(params);
				break;

			case OP_SINC_EXP_TOKEN:
				response = SincronizacionExportacionST(params);
				break;
			//inicia paperless
			case ACTUALIZAR_ESTATUS_EC:
				response = actualizarEstatusEnvioEC(params);
				break;
			case CONSULTAR_ESTADO_CUENTA:
				response = consultarEstadoCuenta(params);
				break;
			case CONSULTAR_ESTATUS_EC:
				response = consultarEstatusEnvioEC(params);
				break;
			case CONSULTAR_TEXTO_PAPERLESS:
				response = consultaTextoPaperles(params);
				break;
			case INHIBIR_ENVIO_EC:
				response = inhibirEnvioEstadoCuenta(params);
				break;
			case OBTENER_PERIODO_EC:
				response = obtenerPeriodosEC(params);
				break;

			case OP_CONSULTA_SPEI:
				response = consultaSPEI(params);
				break;

			case OP_ENVIOCORREO_SPEI:
				response = enviarCorreoSPEI(params);
				break;
		
	}
		//Termina SPEI	
		if (sleep > 0) {
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
			}
		}
		return response;
	}
	
	//O3
	private ServerResponse consultaTransferenciasDeOtrosBancos(
			Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		 ConsultaTransferenciasDeOtrosBancosData data= new ConsultaTransferenciasDeOtrosBancosData();
			ServerResponse response = new ServerResponse(data);
			isjsonvalueCode=isJsonValueCode.DEPOSITOS;

			NameValuePair[] parameters = new NameValuePair[6];
			parameters[0] = new NameValuePair(ServerConstants.CUENTA, (String) params.get(ServerConstants.CUENTA));
			parameters[1] = new NameValuePair(ServerConstants.TIPO_CUENTA, (String) params.get(ServerConstants.TIPO_CUENTA));
			parameters[2] = new NameValuePair(ServerConstants.TIPO_OPERACION, "otrosBancos"); //RN6
			parameters[3] = new NameValuePair(ServerConstants.PERIODO, (String) params.get(ServerConstants.PERIODO));
			parameters[4] = new NameValuePair(ServerConstants.NUMERO_CELULAR, (String) params.get(ServerConstants.NUMERO_CELULAR));
			parameters[5] = new NameValuePair(ServerConstants.IUM, (String) params.get(ServerConstants.IUM));
			
			if (SIMULATION) {
				
				String responseJson = null;
				
				if ("AH".equals((String) params.get(ServerConstants.TIPO_CUENTA))) {
                    responseJson = oReadDocument.read(ConstantsPropertyList.TIPOCUENTA);
				} else {
					if ("0".equals((String) params.get(ServerConstants.PERIODO))) {
                        responseJson = oReadDocument.read(ConstantsPropertyList.PERIODO_1);
						
					} else if ("1".equals((String) params.get(ServerConstants.PERIODO))) {
                        responseJson = oReadDocument.read(ConstantsPropertyList.PERIODO_2);
						
					} else if ("2".equals((String) params.get(ServerConstants.PERIODO))) {
                        responseJson = oReadDocument.read(ConstantsPropertyList.PERIODO_3);
					}
				}
				
				clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS], parameters, response, responseJson, true);
				
			} else {
				clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS], parameters, response, false, true);
			}
			
			return response;
		
	}

	private ServerResponse consultaTransferenciasClientesBancomer(
			Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
				 ConsultaTransferenciasClientesBancomerData data= new ConsultaTransferenciasClientesBancomerData();
					ServerResponse response = new ServerResponse(data);

					String cuenta=(String) params.get(ServerConstants.CUENTA);
					String tipoCuenta=(String) params.get(ServerConstants.TIPO_CUENTA);
					String tipoOperacion="clientesBancomer";
					String periodo=(String) params.get(ServerConstants.PERIODO);
					String numeroCelular=(String) params.get(ServerConstants.NUMERO_CELULAR);
					String ium=(String) params.get(ServerConstants.IUM);
					NameValuePair[] parameters = new NameValuePair[6];
					parameters[0] = new NameValuePair(ServerConstants.CUENTA, cuenta);
					parameters[1] = new NameValuePair(ServerConstants.TIPO_CUENTA, tipoCuenta );
					parameters[2] = new NameValuePair(ServerConstants.TIPO_OPERACION, tipoOperacion );
					parameters[3] = new NameValuePair(ServerConstants.PERIODO, periodo);
					parameters[4] = new NameValuePair(ServerConstants.NUMERO_CELULAR, numeroCelular);
					parameters[5] = new NameValuePair(ServerConstants.IUM, ium);
					
					if (SIMULATION) {

                        String responseJson = oReadDocument.read(ConstantsPropertyList.CONTRANSCLIENTBANCOMER);

                        clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_TRANSFERENCIAS_CLIENTES_BANCOMER], parameters, response, responseJson, true);
						
					} else {
						clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_TRANSFERENCIAS_CLIENTES_BANCOMER], parameters, response, false, true);
					}
					
					return response;
	}

	private ServerResponse consultaTransferenciasOtrosBancos (
		Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ConsultaTransferenciasOtrosBancosData data= new ConsultaTransferenciasOtrosBancosData();
		ServerResponse response = new ServerResponse(data);
		isjsonvalueCode=isJsonValueCode.DEPOSITOS;

		NameValuePair[] parameters = new NameValuePair[4];
		parameters[0] = new NameValuePair(ServerConstants.TIPO_OPERACION, "transfOtrosBancos"); //RN3
		parameters[1] = new NameValuePair(ServerConstants.PERIODO, (String) params.get(ServerConstants.PERIODO));
		parameters[2] = new NameValuePair(ServerConstants.NUMERO_CELULAR, (String) params.get(ServerConstants.NUMERO_CELULAR));
		parameters[3] = new NameValuePair(ServerConstants.IUM, (String) params.get(ServerConstants.IUM));
			
		if (SIMULATION) {
				
			String responseJson = null;

			if ("0".equals((String) params.get(ServerConstants.PERIODO))) {
                responseJson = oReadDocument.read(ConstantsPropertyList.CONOTROSBANCOSPERIDO_1);

			} else if ("1".equals((String) params.get(ServerConstants.PERIODO))) {
                responseJson = oReadDocument.read(ConstantsPropertyList.CONOTROSBANCOSPERIDO_2);

			} else if ("2".equals((String) params.get(ServerConstants.PERIODO))) {
                responseJson = oReadDocument.read(ConstantsPropertyList.CONOTROSBANCOSPERIDO_3);
			}
						 
			clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_TRANSFERENCIAS_A_OTROS_BANCOS], parameters, response, responseJson, true);
				
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_TRANSFERENCIAS_A_OTROS_BANCOS], parameters, response, false, true);
		}
			
			return response;
	}

	private ServerResponse consultaTransferenciasMisCuentas(
		Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		 ConsultaTransferenciasMisCuentasData data= new ConsultaTransferenciasMisCuentasData();
		ServerResponse response = new ServerResponse(data);
		isjsonvalueCode=isJsonValueCode.DEPOSITOS;

		NameValuePair[] parameters = new NameValuePair[4];
		parameters[0] = new NameValuePair(ServerConstants.TIPO_OPERACION, "transfMisCuentas"); //RN3
		parameters[1] = new NameValuePair(ServerConstants.PERIODO, (String) params.get(ServerConstants.PERIODO));
		parameters[2] = new NameValuePair(ServerConstants.NUMERO_CELULAR, (String) params.get(ServerConstants.NUMERO_CELULAR));
		parameters[3] = new NameValuePair(ServerConstants.IUM, (String) params.get(ServerConstants.IUM));
			
		if (SIMULATION) {
				
			String responseJson = null;

			if ("0".equals((String) params.get(ServerConstants.PERIODO))) {
                responseJson = oReadDocument.read(ConstantsPropertyList.CONTRANSMISCUENTASPERIODO_1);

			} else if ("1".equals((String) params.get(ServerConstants.PERIODO))) {
                responseJson = oReadDocument.read(ConstantsPropertyList.CONTRANSMISCUENTASPERIODO_2);

			} else if ("2".equals((String) params.get(ServerConstants.PERIODO))) {
                responseJson = oReadDocument.read(ConstantsPropertyList.CONTRANSMISCUENTASPERIODO_3);
			}

            clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_TRANSFERENCIAS_MIS_CUENTAS], parameters, response, responseJson, true);
				
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_TRANSFERENCIAS_MIS_CUENTAS], parameters, response, false, true);
		}
			
			return response;
	}

	private ServerResponse consultaTransferenciasCuentaBBVA(
			Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ConsultaTransferenciasCuentasBBVAData data= new ConsultaTransferenciasCuentasBBVAData();
			ServerResponse response = new ServerResponse(data);
			isjsonvalueCode=isJsonValueCode.DEPOSITOS;

			NameValuePair[] parameters = new NameValuePair[4];
			parameters[0] = new NameValuePair(ServerConstants.TIPO_OPERACION, "transfBancomer"); //RN3
			parameters[1] = new NameValuePair(ServerConstants.PERIODO, (String) params.get(ServerConstants.PERIODO));
			parameters[2] = new NameValuePair(ServerConstants.NUMERO_CELULAR, (String) params.get(ServerConstants.NUMERO_CELULAR));
			parameters[3] = new NameValuePair(ServerConstants.IUM, (String) params.get(ServerConstants.IUM));
			
		if (SIMULATION) {

            String responseJson=oReadDocument.read(ConstantsPropertyList.CONTRANSCUENTABBVA_AVISO);

			clienteHttp.simulateOperation(
					OPERATION_CODES[CONSULTA_TRANSFERENCIAS_CUENTA_BBVA],
					parameters, response, responseJson, true);

		} else {
			clienteHttp.invokeOperation(
					OPERATION_CODES[CONSULTA_TRANSFERENCIAS_CUENTA_BBVA],
					parameters, response, false, true);
		}
			
		return response;
	}

	private ServerResponse consultaPagoServicios(Hashtable<String, ?> params)  throws IOException, ParsingException, URISyntaxException {
		 ConsultaPagoServiciosData data= new ConsultaPagoServiciosData();
		 ServerResponse response = new ServerResponse(data);
		 isjsonvalueCode=isJsonValueCode.DEPOSITOS;

		NameValuePair[] parameters = new NameValuePair[4];
		parameters[0] = new NameValuePair(ServerConstants.TIPO_OPERACION, "transfPagoServicios"); //RN3
		parameters[1] = new NameValuePair(ServerConstants.PERIODO, (String) params.get(ServerConstants.PERIODO));
		parameters[2] = new NameValuePair(ServerConstants.NUMERO_CELULAR, (String) params.get(ServerConstants.NUMERO_CELULAR));
		parameters[3] = new NameValuePair(ServerConstants.IUM, (String) params.get(ServerConstants.IUM));

		if (SIMULATION) {

			String responseJson = null;

			if ("0".equals((String) params.get(ServerConstants.PERIODO))) {
                responseJson = oReadDocument.read(ConstantsPropertyList.CONPAGOSERVPERIODO_1);

			} else if ("1".equals((String) params.get(ServerConstants.PERIODO))) {
                responseJson = oReadDocument.read(ConstantsPropertyList.CONPAGOSERVPERIODO_2);

			} else if ("2".equals((String) params.get(ServerConstants.PERIODO))) {
                responseJson = oReadDocument.read(ConstantsPropertyList.CONPAGOSERVPERIODO_3);
			}

			clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_PAGO_SERVICIOS], parameters, response, responseJson, true);

		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_PAGO_SERVICIOS], parameters, response, false, true);
		}

		return response;
	}

	private ServerResponse consultaDepositoEfectivo(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		 ConsultaDepositosEfectivoData data= new ConsultaDepositosEfectivoData();
			ServerResponse response = new ServerResponse(data);
			isjsonvalueCode=isJsonValueCode.DEPOSITOS;

			String cuenta=(String) params.get(ServerConstants.CUENTA);
			String tipoCuenta=(String) params.get(ServerConstants.TIPO_CUENTA);
			String tipoOperacion="depositoEfectivo";
			String periodo=(String) params.get(ServerConstants.PERIODO);
			String numeroCelular=(String) params.get(ServerConstants.NUMERO_CELULAR);
			String ium=(String) params.get(ServerConstants.IUM);
			NameValuePair[] parameters = new NameValuePair[6];
			parameters[0] = new NameValuePair(ServerConstants.CUENTA, cuenta);
			parameters[1] = new NameValuePair(ServerConstants.TIPO_CUENTA, tipoCuenta );
			parameters[2] = new NameValuePair(ServerConstants.TIPO_OPERACION, tipoOperacion );
			parameters[3] = new NameValuePair(ServerConstants.PERIODO, periodo);
			parameters[4] = new NameValuePair(ServerConstants.NUMERO_CELULAR, numeroCelular);
			parameters[5] = new NameValuePair(ServerConstants.IUM, ium);
			
			if (SIMULATION) {

                String responseJson = oReadDocument.read(ConstantsPropertyList.CONDEPEFECTIVOOK);

				clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_DEPOSITOS_EFECTIVO], parameters, response, responseJson, true);
				
			} else {
				clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DEPOSITOS_EFECTIVO], parameters, response, false, true);
			}
			
			return response;
	}

	private ServerResponse consultaDepositoCheques(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		 ConsultaDepositosChequesData data= new ConsultaDepositosChequesData();
		ServerResponse response = new ServerResponse(data);
		isjsonvalueCode=isJsonValueCode.DEPOSITOS;

		NameValuePair[] parameters = new NameValuePair[6];
		parameters[0] = new NameValuePair(ServerConstants.CUENTA, (String) params.get(ServerConstants.CUENTA));
		parameters[1] = new NameValuePair(ServerConstants.TIPO_CUENTA, (String) params.get(ServerConstants.TIPO_CUENTA));
		parameters[2] = new NameValuePair(ServerConstants.TIPO_OPERACION, "depositoCheques"); //RN6
		parameters[3] = new NameValuePair(ServerConstants.PERIODO, (String) params.get(ServerConstants.PERIODO));
		parameters[4] = new NameValuePair(ServerConstants.NUMERO_CELULAR, (String) params.get(ServerConstants.NUMERO_CELULAR));
		parameters[5] = new NameValuePair(ServerConstants.IUM, (String) params.get(ServerConstants.IUM));
		
		if (SIMULATION) {
			
			String responseJson = null;
			
			if ("AH".equals((String) params.get(ServerConstants.TIPO_CUENTA))) {

                responseJson = oReadDocument.read(ConstantsPropertyList.CONDEPCHEQUESTIPOCUENTAAVISO);
			} else {
				if ("0".equals((String) params.get(ServerConstants.PERIODO))) {
                    responseJson = oReadDocument.read(ConstantsPropertyList.CONDEPCHEQUESPERIODO_1);
				
				} else if ("1".equals((String) params.get(ServerConstants.PERIODO))) {
                    responseJson = oReadDocument.read(ConstantsPropertyList.CONDEPCHEQUESPERIODO_2);
				
				} else if ("2".equals((String) params.get(ServerConstants.PERIODO))) {
                    responseJson = oReadDocument.read(ConstantsPropertyList.CONDEPCHEQUESPERIODO_3);
				}
			}

			clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_DEPOSITOS_CHEQUES], parameters, response, responseJson, true);
			
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DEPOSITOS_CHEQUES], parameters, response, false, true);
		}
		
		return response;
		
		
	}
	
	private ServerResponse consultaDepositoChequesDetalle(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ConsultaDepositosRecibidosChequesEfectivo data= new ConsultaDepositosRecibidosChequesEfectivo();
		ServerResponse response = new ServerResponse(data);
		isjsonvalueCode=isJsonValueCode.DEPOSITOS;
		
		NameValuePair[] parameters = new NameValuePair[4];
		parameters[0] = new NameValuePair(ServerConstants.CUENTA, (String) params.get(ServerConstants.CUENTA));
		parameters[1] = new NameValuePair(ServerConstants.NUM_MOVTO, (String) params.get(ServerConstants.NUM_MOVTO));
		parameters[2] = new NameValuePair(ServerConstants.NUMERO_CELULAR, (String) params.get(ServerConstants.NUMERO_CELULAR));
		parameters[3] = new NameValuePair(ServerConstants.IUM, (String) params.get(ServerConstants.IUM));
		
		if (SIMULATION) {

            String responseJson = oReadDocument.read(ConstantsPropertyList.CONDEPCHEQUEDETALLEOK);

			clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_DEPOSITOS_CHEQUES_DETALLE], parameters, response, responseJson, true);
			
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DEPOSITOS_CHEQUES_DETALLE], parameters, response, false, true);
		}
		
		return response;
	}
	
	private ServerResponse consultaDepositosBBVABancomerYEfectivo(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ConsultaDepositosBBVABancomerYEfectivoData data= new ConsultaDepositosBBVABancomerYEfectivoData();
		ServerResponse response = new ServerResponse(data);
		isjsonvalueCode=isJsonValueCode.DEPOSITOS;

		NameValuePair[] parameters = new NameValuePair[6];
		parameters[0] = new NameValuePair(ServerConstants.CUENTA, (String) params.get(ServerConstants.CUENTA));
		parameters[1] = new NameValuePair(ServerConstants.TIPO_CUENTA, (String) params.get(ServerConstants.TIPO_CUENTA));
		parameters[2] = new NameValuePair(ServerConstants.TIPO_OPERACION, "depositoEfectivo"); //RN6
		parameters[3] = new NameValuePair(ServerConstants.PERIODO, (String) params.get(ServerConstants.PERIODO));
		parameters[4] = new NameValuePair(ServerConstants.NUMERO_CELULAR, (String) params.get(ServerConstants.NUMERO_CELULAR));
		parameters[5] = new NameValuePair(ServerConstants.IUM, (String) params.get(ServerConstants.IUM));
		
		if (SIMULATION) {
			
			String responseJson = null;

			if ("AH".equals((String) params.get(ServerConstants.TIPO_CUENTA))) {
                responseJson = oReadDocument.read(ConstantsPropertyList.CONDEPBBVAEFECTIVOTIPOCUENTAERROR);
			} else {
				if ("0".equals((String) params.get(ServerConstants.PERIODO))) {

                    responseJson = oReadDocument.read(ConstantsPropertyList.CONDEPBBVAEFECTIVOPERIODO_1);
					
				} else if ("1".equals((String) params.get(ServerConstants.PERIODO))) {

                    responseJson = oReadDocument.read(ConstantsPropertyList.CONDEPBBVAEFECTIVOPERIODO_2);
					
				} else if ("2".equals((String) params.get(ServerConstants.PERIODO))) {

                    responseJson = oReadDocument.read(ConstantsPropertyList.CONDEPBBVAEFECTIVOPERIODO_3);
				}
			}

			clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO], parameters, response, responseJson, true);
			
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO], parameters, response, false, true);
		}
		
		return response;
	}
	
	private ServerResponse consultaDepositosBBVABancomerYEfectivoDetalle(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ConsultaDepositosRecibidosChequesEfectivo data= new ConsultaDepositosRecibidosChequesEfectivo();
		ServerResponse response = new ServerResponse(data);
		isjsonvalueCode=isJsonValueCode.DEPOSITOS;

		NameValuePair[] parameters = new NameValuePair[6];
		parameters[0] = new NameValuePair(ServerConstants.CUENTA, (String) params.get(ServerConstants.CUENTA));
		parameters[1] = new NameValuePair(ServerConstants.NUM_MOVTO, (String) params.get(ServerConstants.NUM_MOVTO));
		parameters[2] = new NameValuePair(ServerConstants.REFERENCIA_INTERNA, (String) params.get(ServerConstants.REFERENCIA_INTERNA));
		parameters[3] = new NameValuePair(ServerConstants.REFERENCIA_AMPLIADA, (String) params.get(ServerConstants.REFERENCIA_AMPLIADA));
		parameters[4] = new NameValuePair(ServerConstants.NUMERO_CELULAR, (String) params.get(ServerConstants.NUMERO_CELULAR));
		parameters[5] = new NameValuePair(ServerConstants.IUM, (String) params.get(ServerConstants.IUM));
		
		if (SIMULATION) {

            String responseJson = oReadDocument.read(ConstantsPropertyList.CONDEPBBVAEFECTIVODETALLEOK);

			clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO_DETALLE], parameters, response, responseJson, true);
			
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO_DETALLE], parameters, response, false, true);
		}
		
		return response;
	}


	private ServerResponse consultaComision(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		Comision data = new Comision();
		ServerResponse response = new ServerResponse(data);

		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONSULTACOMISIONOK), response);
			
		} else {
			NameValuePair[] parameters = new NameValuePair[6];
			parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
			parameters[1] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[2] = new NameValuePair("CC", (String) params.get(ORIGIN_PARAM));
			parameters[3] = new NameValuePair("CA", (String) params.get(DESTINATION_PARAM));
			parameters[4] = new NameValuePair("IM", (String) params.get(AMOUNT_PARAM));
			parameters[5] = new NameValuePair("TO", (String) params.get(TIPO_COMISION_PARAM));
			
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTAR_COMISION_I], parameters, response,false,false);
		}
		return response;
	}

	/**
	 * Cancel the ongoing network operation.
	 */
	public void cancelNetworkOperations() {
		httpClient.abort();
		clienteHttp.abort();
	}

	/*
	 * public void setContext(Context context) { mContext = context;
	 * clienteHttp.setContext(mContext); }
	 * 
	 * public Context getContext() { return mContext; }
	 */

	/*
	 * Own accounts transaction operation.
	 */
	// public static final int OWN_ACCOUNTS_TRANSFER_OPERATION = 28;

	/**
	 * Card NIP parm.
	 */
	public static final String CARD_NIP_PARAM = "nip";

	/**
	 * Card CVV2 param.
	 */
	public static final String CARD_CVV2_PARAM = "cvv2";

	/**
	 * Transaction charge account param.
	 */
	public static final String CHARGE_ACCOUNT_PARAM = "charge_account";

	/**
	 * Transaction payment account pram.
	 */
	public static final String PAYMENT_ACCOUNT_PARAM = "payment_account";

	/**
	 * Validation instructions param.
	 */
	public static final String VALIDATION_INSTRUCTIONS_PARAM = "validation_instructions";

	/**
	 * Identificador del token param.
	 */
	public static final String IDENTIFICADOR_TOKEN_PARAM = "identificador_token";

	/**
	 * Funcion para pago de servicios.
	 */
	public static final String PAYMENT_SERVICES_PREREGISTER_FUNCTION = "I380";

	/**
	 * Funcion para pago de servicios.
	 */
	public static final String PAYMENT_SERVICES_PREREGISTER_REASON_TYPE = "CV";

	/**
	 * Tipo token para pago de servicios.
	 */
	public static final String PAYMENT_SERVICES_TOKEN_TYPE = "T";
	public static final String ID_CANAL = "canal";
	public static final String USUARIO = "usuario";

	/**
	 * Perform a bank transaction betwen self accounts.
	 * @param params The params for the transaction.
	 * @return The server response.
	 */
	/*
	 * public ServerResponse transferirCuentasPropias(Hashtable<String, ?>
	 * params) throws IOException, ParsingException, URISyntaxException {
	 * TransferirCuentasPropiasData data = new TransferirCuentasPropiasData();
	 * ServerResponse response = new ServerResponse(data);
	 * 
	 * if (SIMULATION) {
	 * this.httpClient.simulateOperation("ESOK*FO5343515478*CT*OC2*AS00741800000151951234*IM178964*AS00741800000151951234*IM314192*FE21062012*HR085700", response);
	 * } else {
	 * 
	 * NameValuePair[] parameters = new NameValuePair[11];
	 * parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
	 * parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
	 * parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM));
	 * parameters[3] = new NameValuePair("CC", (String) params.get(CHARGE_ACCOUNT_PARAM));
	 * parameters[4] = new NameValuePair("CA", (String) params.get(PAYMENT_ACCOUNT_PARAM));
	 * parameters[5] = new NameValuePair("IM", (String) params.get(AMOUNT_PARAM));
	 * parameters[6] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE_PARAM));
	 * parameters[7] = new NameValuePair("NI", (String) params.get(CARD_NIP_PARAM));
	 * parameters[8] = new NameValuePair("CV", (String) params.get(CARD_CVV2_PARAM));
	 * parameters[9] = new NameValuePair("OT", (String) params.get(ServerConstants.CODIGO_OTP));
	 * parameters[10] = new NameValuePair("VA", (String) params.get(VALIDATION_INSTRUCTIONS_PARAM));
	 * 
	 * clienteHttp.invokeOperation(OPERATION_CODES[SELF_TRANSFER_OPERATION], parameters, response);
	 * }
	 * 
	 * return response;
	 * }
	 */

	private ServerResponse altaDeFrecuente(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();

		NameValuePair[] parameters = new NameValuePair[20];
		parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
		parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
		parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[3] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE_PARAM));
		parameters[4] = new NameValuePair("DE", (String) params.get(DESCRIPCION_PARAM));
		parameters[5] = new NameValuePair("CC", "");
		parameters[6] = new NameValuePair("CA", Tools.isEmptyOrNull((String) params.get(PAYMENT_ACCOUNT_PARAM)) ? "" : (String) params.get(PAYMENT_ACCOUNT_PARAM));
		parameters[7] = new NameValuePair("BF", (String) params.get(BENEFICIARIO_PARAM));
		parameters[8] = new NameValuePair("CB", Tools.isEmptyOrNull((String) params.get(BANK_CODE_PARAM)) ? "" : (String) params.get(BANK_CODE_PARAM));
		parameters[9] = new NameValuePair("IM", "");
		parameters[10] = new NameValuePair("RF", params.get(REFERENCE_PARAM) != null ? (String) params.get(REFERENCE_PARAM) : "");
		parameters[11] = new NameValuePair("CP", (String) params.get(CONCEPTO_PARAM));
		parameters[12] = new NameValuePair("TP", (String) params.get(TIPO_CONSULTA_PARAM));
		parameters[13] = new NameValuePair("OA", params.get(OPERADORA_PARAM) != null ? (String) params.get(OPERADORA_PARAM) : "");
		parameters[14] = new NameValuePair("AP", "");
		parameters[15] = new NameValuePair("IT", (String) params.get(ID_TOKEN));
		parameters[16] = new NameValuePair("NI", (String) params.get(CARD_NIP_PARAM));
		parameters[17] = new NameValuePair("CV", (String) params.get(CARD_CVV2_PARAM));
		parameters[18] = new NameValuePair("OT", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[19] = new NameValuePair("VA", (String) params.get(VA_PARAM));

		if (SIMULATION) {
            clienteHttp.simulateOperation(OPERATION_CODES[ALTA_FRECUENTE], parameters, response, oReadDocument.read(ConstantsPropertyList.OK));
		} else {

			clienteHttp.invokeOperation(OPERATION_CODES[ALTA_FRECUENTE], parameters, response,false,false);
		}

		return response;
	}
	
	public ServerResponse bajaDeFrecuente(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();
		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.OK), response);
			
		} else {
			NameValuePair[] parameters = new NameValuePair[22];
			parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
			parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
			parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[3] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE_PARAM));
			parameters[4] = new NameValuePair("DE", (String) params.get(DESCRIPCION_PARAM));
			parameters[5] = new NameValuePair("CC", "");
			parameters[6] = new NameValuePair("CA", (String) params.get(PAYMENT_ACCOUNT_PARAM));
			parameters[7] = new NameValuePair("BF", params.get(BENEFICIARIO_PARAM) != null ? (String) params.get(BENEFICIARIO_PARAM) : "");
			parameters[8] = new NameValuePair("CB", params.get(BANK_CODE_PARAM) == null ? "" : (String) params.get(BANK_CODE_PARAM));
			parameters[9] = new NameValuePair("IM", "");
			parameters[10] = new NameValuePair("RF", params.get(REFERENCIA_NUMERICA_PARAM) != null ? (String) params.get(REFERENCIA_NUMERICA_PARAM) : "");
			parameters[11] = new NameValuePair("CP", "");
			parameters[12] = new NameValuePair("IT", (String) params.get(ID_TOKEN));
			parameters[13] = new NameValuePair("TP", (String) params.get(TIPO_CONSULTA_PARAM));
			parameters[14] = new NameValuePair("OA", params.get(OPERADORA_PARAM) != null ? (String) params.get(OPERADORA_PARAM) : "");
			parameters[15] = new NameValuePair("CN", (String) params.get(ID_CANAL));
			parameters[16] = new NameValuePair("US", (String) params.get(USUARIO));
			parameters[17] = new NameValuePair("AP", "");
			parameters[18] = new NameValuePair("NI", (String) params.get(CARD_NIP_PARAM));
			parameters[19] = new NameValuePair("CV", (String) params.get(CARD_CVV2_PARAM));
			parameters[20] = new NameValuePair("OT", (String) params.get(ServerConstants.CODIGO_OTP));
			parameters[21] = new NameValuePair("VA", (String) params.get(VA_PARAM));

			clienteHttp.invokeOperation(OPERATION_CODES[BAJA_FRECUENTE], parameters, response,false,false);

		}
		return response;
	}
	
	
	private ServerResponse solictarAlertas(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		SolicitarAlertasData data = new SolicitarAlertasData();
		ServerResponse response = new ServerResponse(data);

		NameValuePair[] parameters = new NameValuePair[4];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair(ServerConstants.JSON_IUM_ETIQUETA, (String) params.get(ServerConstants.JSON_IUM_ETIQUETA));
		parameters[2] = new NameValuePair(ServerConstants.NUMERO_TARJETA, (String) params.get(ServerConstants.NUMERO_TARJETA));
		parameters[3] = new NameValuePair(ServerConstants.COMPANIA_CELULAR, (String) params.get(ServerConstants.COMPANIA_CELULAR));
		
		if (SIMULATION) {
            String responseJson = oReadDocument.read(ConstantsPropertyList.SOLICITARALERTASOK_1);

			clienteHttp.simulateOperation(OPERATION_CODES[OP_SOLICITAR_ALERTAS], parameters, response, responseJson, true);
			
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[OP_SOLICITAR_ALERTAS], parameters, response, false, true);
		}
		
		return response;
	}
	
	private ServerResponse consultaCIE(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		NombreCIEData data = new NombreCIEData();
		ServerResponse response = new ServerResponse(data);
		
		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONSULTACIE_OK), response);
			
		} else {
			NameValuePair[] parameters = new NameValuePair[3];
			parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
			parameters[1] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[2] = new NameValuePair("NU", (String) params.get(CIE_AGREEMENT_PARAM));

			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_CIE], parameters, response,false,false);
		}
		return response;
	}

	private ServerResponse actualizarFrecuente(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse(new FrecuenteMulticanalResult());

		NameValuePair[] parameters = new NameValuePair[12];
		parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
		parameters[1] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[2] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE_PARAM));
		parameters[3] = new NameValuePair("TP", (String) params.get(TIPO_CONSULTA_PARAM));
		parameters[4] = new NameValuePair("CA", (String) params.get(DESTINATION_PARAM));
		parameters[5] = new NameValuePair("DE", (String) params.get(DE_PARAM));
		parameters[6] = new NameValuePair("RF", (String) params.get(REFERENCE_PARAM));
		parameters[7] = new NameValuePair("BF", (String) params.get(BENEFICIARIO_PARAM));
		parameters[8] = new NameValuePair("CB", params.get(BANK_CODE_PARAM) == null ? "" : (String) params.get(BANK_CODE_PARAM));
		parameters[9] = new NameValuePair("CN", (String) params.get(CN_PARAM));
		parameters[10] = new NameValuePair("US", (String) params.get("US"));
		parameters[11] = new NameValuePair("OA", params.get(OPERADORA_PARAM) == null ? "" : (String) params.get(OPERADORA_PARAM));

		if (SIMULATION) {
            clienteHttp.simulateOperation(OPERATION_CODES[ACTUALIZAR_FRECUENTE], parameters, response, oReadDocument.read(ConstantsPropertyList.OK));
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[ACTUALIZAR_FRECUENTE], parameters, response,false,false);
		}

		return response;

	}

	/**
	 * Actualizacion de un preregistrado en pago de servicios a frecuente.
	 * @param params
	 * @return
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse actualizarPregistradoAFrecuente(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse(new ActualizarPreregistroResult());

		NameValuePair[] parameters = new NameValuePair[14];
		parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
		parameters[1] = new NameValuePair("NP", (String) params.get(PASSWORD_PARAM));
		parameters[2] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[3] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE_PARAM));
		parameters[4] = new NameValuePair("C1", (String) params.get(C1_PARAM));
		parameters[5] = new NameValuePair("FN", "I380");
		parameters[6] = new NameValuePair("NK", (String) params.get(NICK_NAME_PARAM));
		parameters[7] = new NameValuePair("CA", (String) params.get(PAYMENT_ACCOUNT_PARAM));
		parameters[8] = new NameValuePair("BF", (String) params.get(BENEFICIARIO_PARAM));
		parameters[9] = new NameValuePair("RF", (String) params.get(REFERENCE_PARAM));
		parameters[10] = new NameValuePair("NI", (String) params.get(CARD_NIP_PARAM));
		parameters[11] = new NameValuePair("CV", (String) params.get(CARD_CVV2_PARAM));
		parameters[12] = new NameValuePair("OT", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[13] = new NameValuePair("VA", (String) params.get(VA_PARAM));

		if (SIMULATION) {
            clienteHttp.simulateOperation(OPERATION_CODES[ACTUALIZAR_PREREGISTRO_FRECUENTE], parameters, response, oReadDocument.read(ConstantsPropertyList.ACTPREGISTRADOFRECUENTEOK));
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[ACTUALIZAR_PREREGISTRO_FRECUENTE], parameters, response,false,false);
		}
		
		return response;
	}

	/**
	 * Ejecuta la operación para verificar si el número de tarjeta para la
	 * contratación es v�lido.
	 * @param params Los parmetros de la consulta.
	 * @return La respuesta del servidor.
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse consultaTarjetaParaContratacion(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse(new ConsultaTarjetaContratacionData());

		NameValuePair[] parameters = new NameValuePair[5];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(USERNAME_PARAM));
		parameters[1] = new NameValuePair("numeroTarjeta", (String) params.get(CARDNUMBER_PARAM));
		parameters[2] = new NameValuePair(ServerConstants.COMPANIA_CELULAR, (String) params.get(ServerConstants.COMPANIA_CELULAR));
		parameters[3] = new NameValuePair(ServerConstants.PERFIL_CLIENTE, (String) params.get(ServerConstants.PERFIL_CLIENTE));
		parameters[4] = new NameValuePair(ServerConstants.VERSION_FLUJO, Constants.VERSION_FLUJO_CONTRATACION);
		
		if (SIMULATION) {
            String responseJson = oReadDocument.read(ConstantsPropertyList.CONTARJETACONTRATACIONOK);
			
			clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_TARJETA_OPERATION], parameters, response, responseJson, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_TARJETA_OPERATION], parameters, response, false, true);
		}
		
		return response;
	}

	/**
	 * Ejecuta la operacion para hacer el cambio de telefono asociado
	 * @param params
	 * @return La respuesta del servidor.
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	/* TODO AMB
	private ServerResponse cambioTelefonoAsociado(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse(new CambioTelefonoResult());

		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CAMBIOTELASOCIADOOK), response, true);
			
		} else {
			NameValuePair[] parameters = new NameValuePair[12];

			parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
			parameters[1] = new NameValuePair(ServerConstants.NUMERO_CLIENTE, (String) params.get(ServerConstants.NUMERO_CLIENTE));
			parameters[2] = new NameValuePair(ServerConstants.JSON_IUM_ETIQUETA, (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[3] = new NameValuePair(ServerConstants.CVE_ACCESO, (String) params.get(ServerConstants.CVE_ACCESO));
			parameters[4] = new NameValuePair(ServerConstants.NUMERO_CELULAR_NUEVO, (String) params.get("numeroCelNuevo"));
			parameters[5] = new NameValuePair(ServerConstants.COMPANIA_CELULAR_NUEVO, (String) params.get("companiaCelularNueva"));
			parameters[6] = new NameValuePair(ServerConstants.VERSION, (String) params.get(ServerConstants.VERSION));
			parameters[7] = new NameValuePair(ServerConstants.CODIGO_NIP, (String) params.get(J_NIP));
			parameters[8] = new NameValuePair(ServerConstants.CODIGO_CVV2, (String) params.get(J_CVV2));
			parameters[9] = new NameValuePair(ServerConstants.CODIGO_OTP, (String) params.get(ServerConstants.CODIGO_OTP));
			parameters[10] = new NameValuePair(ServerConstants.CADENA_AUTENTICACION, (String) params.get(J_AUT));
			parameters[11] = new NameValuePair(ServerConstants.TARJETA_5DIG, (String) params.get(ServerConstants.TARJETA_5DIG));
			
			clienteHttp.invokeOperation(OPERATION_CODES[CAMBIO_TELEFONO], parameters, response, false, true);
		}
		return response;
	}*/

	/**
	 * Realiza la peticion al server para efectuar el cambio de cuenta asociada
	 * @param params
	 * @return
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse cambioCuentaAsociada(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse(new CambioCuentaResult());

		NameValuePair[] parameters = new NameValuePair[12];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair("numeroCliente", (String) params.get(ServerConstants.NUMERO_CLIENTE));
		parameters[2] = new NameValuePair("IUM", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[3] = new NameValuePair("cveAcceso", (String) params.get(ServerConstants.CVE_ACCESO));
		parameters[4] = new NameValuePair("companiaCelular", (String) params.get(ServerConstants.COMPANIA_CELULAR));
		parameters[5] = new NameValuePair("cuentaAnterior", (String) params.get(J_CUENTA_A));
		parameters[6] = new NameValuePair("cuentaNueva", (String) params.get(J_CUENTA_N));
		parameters[7] = new NameValuePair("codigoNIP", (String) params.get(J_NIP));
		parameters[8] = new NameValuePair("codigoCVV2", (String) params.get(J_CVV2));
		parameters[9] = new NameValuePair("codigoOTP", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[10] = new NameValuePair("cadenaAutenticacion", (String) params.get(J_AUT));
		parameters[11] = new NameValuePair("tarjeta5Dig", (String) params.get("tarjeta5Dig"));

		if (SIMULATION) {
            String strResponse = oReadDocument.read(ConstantsPropertyList.CAMBIOCUENTAASOCIADAOK_2);
			clienteHttp.simulateOperation(OPERATION_CODES[CAMBIO_CUENTA], parameters, response, strResponse, true);
			
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CAMBIO_CUENTA], parameters, response, false, true);
		}
		return response;
	}

	/**
	 * Invoca la operacion en el server de suspender o cancelar
	 * @param params
	 * @return
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	/*TODO AMB
	private ServerResponse suspenderCancelarServicio(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		SuspenderCancelarResult data = new SuspenderCancelarResult();
		ServerResponse response = new ServerResponse(data);

		NameValuePair[] parameters = new NameValuePair[11];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair("numeroCliente", (String) params.get(ServerConstants.NUMERO_CLIENTE));
		parameters[2] = new NameValuePair("IUM", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[3] = new NameValuePair("cveAcceso", (String) params.get(ServerConstants.CVE_ACCESO));
		parameters[4] = new NameValuePair("companiaCelular", (String) params.get("companiaCelular"));
		parameters[5] = new NameValuePair("idOperacion", (String) params.get("idOperacion"));
		parameters[6] = new NameValuePair("codigoNIP", (String) params.get(J_NIP));
		parameters[7] = new NameValuePair("codigoCVV2", (String) params.get(J_CVV2));
		parameters[8] = new NameValuePair("codigoOTP", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[9] = new NameValuePair("cadenaAutenticacion", (String) params.get(J_AUT));
		parameters[10] = new NameValuePair("tarjeta5Dig", (String) params.get("tarjeta5Dig"));

		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.SUSPENDERCANCELARSERVOK), response, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[SUSPENDER_CANCELAR], parameters, response, false, true);
		}
		
		return response;
	}*/

	/**
	 * Invoca al server para actualizar las cuentas del usuario
	 * @param params
	 * @return
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse actualizarCuentas(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ActualizarCuentasResult data = new ActualizarCuentasResult();
		ServerResponse response = new ServerResponse(data);

		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.ACTUALIZACUENTAOK),response, true);
		} else {
			NameValuePair[] parameters = new NameValuePair[3];
			parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
			parameters[1] = new NameValuePair("numeroCliente", (String) params.get(ServerConstants.NUMERO_CLIENTE));
			parameters[2] = new NameValuePair("IUM", (String) params.get(ServerConstants.IUM_ETIQUETA));

			clienteHttp.invokeOperation(OPERATION_CODES[ACTUALIZAR_CUENTAS], parameters, response, false, true);
		}
		
		return response;
	}

	/**
	 * Invoca la operacion al server para obtener los montos limites del usuario
	 * actualmente.
	 * @param params
	 * @return Los montos de operacion actualmente
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse consultarLimites(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ConsultarLimitesResult data = new ConsultarLimitesResult();
		ServerResponse response = new ServerResponse(data);

		final NameValuePair[] parameters = new NameValuePair[9];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair("IUM", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[2] = new NameValuePair("cveAcceso", (String) params.get(ServerConstants.CVE_ACCESO));
		parameters[3] = new NameValuePair("tarjeta5Dig", (String) params.get(ServerConstants.TARJETA_5DIG));
		parameters[4] = new NameValuePair("codigoNIP", (String) params.get(ServerConstants.CODIGO_NIP));
		parameters[5] = new NameValuePair("codigoCVV2", (String) params.get(ServerConstants.CODIGO_CVV2));
		parameters[6] = new NameValuePair("codigoOTP", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[7] = new NameValuePair("cadenaAutenticacion", (String) params.get(ServerConstants.CADENA_AUTENTICACION));
		parameters[8] = new NameValuePair("versionFlujo", (String) params.get(ServerConstants.VERSION_FLUJO));
		
		
		
		if (SIMULATION) {
            String responseJson = oReadDocument.read(ConstantsPropertyList.CONSULTARLIMITESOK);

			this.clienteHttp.simulateOperation(OPERATION_CODES[CONSULTAR_LIMITES], parameters, response, responseJson, true);
			
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTAR_LIMITES], parameters, response, false, true);
		}

		return response;
	}

	/**
	 * Realiza la actualizacion de los limites de montos de operacion
	 * @param params
	 * @return La respuesta del server al actualizar los montos de operacion.
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse cambiarLimites(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		CambiarLimitesResult data = new CambiarLimitesResult();
		ServerResponse handler = new ServerResponse(data);

		NameValuePair[] parameters = new NameValuePair[12];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair("numeroCliente", (String) params.get(ServerConstants.NUMERO_CLIENTE));
		parameters[2] = new NameValuePair("IUM", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[3] = new NameValuePair("limusuOp", (String) params.get("limusuOp"));
		parameters[4] = new NameValuePair("limusuDiario", (String) params.get("limusuDiario"));
		parameters[5] = new NameValuePair("limusuMensual", (String) params.get("limusuMensual"));
		parameters[6] = new NameValuePair("cveAcceso", (String) params.get(ServerConstants.CVE_ACCESO));
		parameters[7] = new NameValuePair("codigoNIP", (String) params.get(J_NIP));
		parameters[8] = new NameValuePair("codigoCVV2", (String) params.get(J_CVV2));
		parameters[9] = new NameValuePair("codigoOTP", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[10] = new NameValuePair("cadenaAutenticacion", (String) params.get(J_AUT));
		parameters[11] = new NameValuePair("tarjeta5Dig", (String) params.get("tarjeta5Dig"));

		if (SIMULATION) {
			String response = oReadDocument.read(ConstantsPropertyList.CAMBIARLIMITESOK);
			clienteHttp.simulateOperation(OPERATION_CODES[CAMBIAR_LIMITES], parameters, handler, response, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CAMBIAR_LIMITES], parameters, handler, false, true);
		}
		
		return handler;
	}

	private ServerResponse consultaEstatusMantenimiento(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ConsultaEstatusMantenimientoData data = new ConsultaEstatusMantenimientoData();
		ServerResponse response = new ServerResponse(data);

		NameValuePair[] parameters = new NameValuePair[4];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair(ServerConstants.VERSION_MIDLET, ServerConstants.APP_VERSION_CONSULTA);
		parameters[2] = new NameValuePair("verCatTelefonicas", (String) params.get(COMPANIES_PARAM));
		parameters[3] = new NameValuePair("verCatAutenticacion", (String) params.get(AUTH_CAT_PARAM));
		System.out.println("MTE: "+SIMULATION);
		if (SIMULATION) {
			System.out.println("MTE");
			String responseJson = "";
            String estatusInstrumento = "";

//            responseJson = oReadDocument.read(ConstantsPropertyList.CONESTATUSMANTENIMIENTOOK_1) + CODIGO_ESTATUS_APLICACION + oReadDocument.read(ConstantsPropertyList.CONESTATUSMANTENIMIENTOOK_2) + oReadDocument.read(ConstantsPropertyList.MANTENIMIENTOPERFIL) + oReadDocument.read(ConstantsPropertyList.CONESTATUSMANTENIMIENTOOK_3) + oReadDocument.read(ConstantsPropertyList.MANTENIMIENTOTIPOINSTRUMENTO) + oReadDocument.read(ConstantsPropertyList.CONESTATUSMANTENIMIENTOOK_4) + estatusInstrumento + oReadDocument.read(ConstantsPropertyList.CONESTATUSMANTENIMIENTOOK_5)
//                    + oReadDocument.read(ConstantsPropertyList.CORREO_EJEMPLO)
//                    + oReadDocument.read(ConstantsPropertyList.CONESTATUSMANTENIMIENTOOK_6)
//					+ LISTA_OPERACIONES
//                    + oReadDocument.read(ConstantsPropertyList.CONESTATUSMANTENIMIENTOOK_7);


            responseJson = oReadDocument.read(ConstantsPropertyList.CONESTATUSMANTENIMIENTOOK_0);
			//CODIGO_ESTATUS_APLICACION = "PA";
			/*String tipoInstrumento = "";
			String perfil = "MF02";
			String CORREO_EJEMPLO = "ejemplo@gonet.us";

			responseJson = "{\"estado\":\"OK\",\"numeroCliente\":\"D0027970\",\"estatusServicio\":\"" + CODIGO_ESTATUS_APLICACION + "\",\"companiaCelular\":\"\",\"fechaContratacion\":\"02-04-2014\",\"fechaSistema\":\"\",\"perfilCliente\":\"" + perfil + "\",\"tipoInstrumento\":\"" + tipoInstrumento + "\",\"estatusInstrumento\":\"" + estatusInstrumento + "\",\"emailCliente\":\""
					+ CORREO_EJEMPLO
					+ "\",\"nombreCliente\":\"VICENTE HERNADEZ OROPEZA\",\"estatusAlertas\":\"SI\",\"autenticacion\":"
					+ LISTA_OPERACIONES
					+ ",\"telefonicasMantenimiento\":[{\"companiaCelular\":\"TELCEL\",\"nombreImagen\":\"telcel.png\"},{\"companiaCelular\":\"MOVISTAR\",\"nombreImagen\":\"movistar.png\"},{\"companiaCelular\":\"IUSACELL\",\"nombreImagen\":\"iusacell.png\"},{\"companiaCelular\":\"UNEFON\",\"nombreImagen\":\"unefon.png\"},{\"companiaCelular\":\"NEXTEL\",\"nombreImagen\":\"nextel.png\"}],\"versionCatTelefonico\":\"8\"}";
			*/clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_MANTENIMIENTO], parameters, response, responseJson, true);
			
		} else {
			System.out.println("MTE No simulacion");
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_MANTENIMIENTO], parameters, response, false, true);
		}
		return response;
	}

	/**
	 * @param params
	 * @return
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse desbloqueoContrasena(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		DesbloqueoResult data = new DesbloqueoResult();
		ServerResponse response = new ServerResponse(data);

		NameValuePair[] parameters = new NameValuePair[9];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair("companiaCelular", (String) params.get("companiaCelular"));
		parameters[2] = new NameValuePair("numeroCliente", (String) params.get(ServerConstants.NUMERO_CLIENTE));
		parameters[3] = new NameValuePair("cveAccesoNueva", (String) params.get(ServerConstants.CVE_ACCESO));
		parameters[4] = new NameValuePair("codigoNIP", (String) params.get(J_NIP));
		parameters[5] = new NameValuePair("codigoCVV2", (String) params.get(J_CVV2));
		parameters[6] = new NameValuePair("codigoOTP", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[7] = new NameValuePair("cadenaAutenticacion", (String) params.get(J_AUT));
		parameters[8] = new NameValuePair("tarjeta5Dig", (String) params.get("tarjeta5Dig"));


		if (SIMULATION) {
			String res = oReadDocument.read(ConstantsPropertyList.DESBLOQUEOCONTRASENAOK);
			clienteHttp.simulateOperation(OPERATION_CODES[DESBLOQUEO], parameters, response, res, true);
			
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[DESBLOQUEO], parameters, response, false, true);
		}
		return response;
	}

	/**
	 * Con esta operación se quita el estatus de suspendido del cliente y se deja en estatus PE
	 * @param params
	 * @return
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse quitarSuspension(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();

		NameValuePair[] parameters = new NameValuePair[9];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair("numeroCliente", (String) params.get(ServerConstants.NUMERO_CLIENTE));
		parameters[2] = new NameValuePair("companiaCelular", (String) params.get("companiaCelular"));
		parameters[3] = new NameValuePair("cveAcceso", (String) params.get(ServerConstants.CVE_ACCESO));
		parameters[4] = new NameValuePair("codigoNIP", (String) params.get(J_NIP));
		parameters[5] = new NameValuePair("codigoCVV2", (String) params.get(J_CVV2));
		parameters[6] = new NameValuePair("codigoOTP", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[7] = new NameValuePair("cadenaAutenticacion", (String) params.get(J_AUT));
		parameters[8] = new NameValuePair("tarjeta5Dig", (String) params.get("tarjeta5Dig"));

		if (SIMULATION) {
            clienteHttp.simulateOperation(OPERATION_CODES[QUITAR_SUSPENSION], parameters, response, oReadDocument.read(ConstantsPropertyList.OK_2), true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[QUITAR_SUSPENSION], parameters, response, false, true);
		}
		return response;
	}

	private ServerResponse realizarActivacion(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse(new ActivacionResult());

		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.REALIZARACTIVACIONOK), response);

		} else {
			NameValuePair[] parameters = new NameValuePair[4];
			parameters[0] = new NameValuePair("NT", (String) params.get(USERNAME_PARAM));
			parameters[1] = new NameValuePair("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
			parameters[2] = new NameValuePair("AC", (String) params.get(ACTIVATION_CODE_PARAM));
			parameters[3] = new NameValuePair("PR", (String) params.get(ServerConstants.PERFIL_CLIENTE));

			clienteHttp.invokeOperation(OPERATION_CODES[OP_ACTIVACION], parameters, response, false,false);
		}

		return response;
	}

	private ServerResponse contratacionBmovilAlertas(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse(new ContratacionBmovilData());
		
		NameValuePair[] parameters = new NameValuePair[16];		
		
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair("cveAcceso", (String) params.get(PASSWORD_PARAM));
		parameters[2] = new NameValuePair("numeroTarjeta", (String) params.get(CARDNUMBER_PARAM));
		parameters[3] = new NameValuePair("perfilCliente", (String) params.get(ServerConstants.PERFIL_CLIENTE));
		parameters[4] = new NameValuePair("companiaCelular", (String) params.get(ServerConstants.COMPANIA_CELULAR));
		parameters[5] = new NameValuePair("emailCliente", (String) params.get(ServerConstants.EMAIL_CLIENTE));
		parameters[6] = new NameValuePair("aceptoTerminosCondiciones", (String) params.get(ServerConstants.ACEPTO_TERMINOS_CONDICIONES));
		parameters[7] = new NameValuePair("estatusAlertas", (String) params.get(ALERTSTATUS_PARAM));
		parameters[8] = new NameValuePair("codigoNIP", (String) params.get(NIP_PARAM));
		parameters[9] = new NameValuePair("codigoCVV2", (String) params.get(CVV2_PARAM));
		parameters[10] = new NameValuePair("codigoOTP", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[11] = new NameValuePair(ServerConstants.VERSION_FLUJO, Constants.VERSION_FLUJO_CONTRATACION);
		parameters[12] = new NameValuePair("cadenaAutenticacion", (String) params.get(VA_PARAM));
		parameters[13] = new NameValuePair("marca", (String) params.get(MARCA));
		parameters[14] = new NameValuePair("modelo", (String) params.get(MODELO));		
		parameters[15] = new NameValuePair("tarjeta5Dig", (String) params.get("tarjeta5Dig"));

		if (SIMULATION) {
            String resp = oReadDocument.read(ConstantsPropertyList.CONTRATACIONBMOVILALERTAOK_1);
			clienteHttp.simulateOperation(OPERATION_CODES[OP_CONTRATACION_BMOVIL_ALERTAS], parameters, response, resp, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[OP_CONTRATACION_BMOVIL_ALERTAS], parameters, response, false, true);
		}
		
		return response;
	}
	
	
	/**
	 * Consulta los terminos, condiciones sesion de uso para bmovil.
	 * @param params Parametros de la trama de env�o.
	 * @return Respuesta del servidor.
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse consultarTerminosSesion(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse(new ConsultaTerminosDeUsoData());

		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONSTERMINOSESIONOK), response, true);
			
		} else {
			NameValuePair[] parameters = new NameValuePair[1];
			parameters[0] = new NameValuePair(ServerConstants.PERFIL_CLIENTE, (String) params.get(ServerConstants.PERFIL_CLIENTE));
			clienteHttp.invokeOperation(OPERATION_CODES[OP_CONSULTAR_TERMINOS_SESION], parameters, response, false, true);
		}
		
		return response;
	}
	
	/**
	 * Consulta los terminos y condiciones de uso para bmovil.
	 * @param params Parametros de la trama de env�o.
	 * @return Respuesta del servidor.
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse consultarTerminos(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse(new ConsultaTerminosDeUsoData());

		if (SIMULATION) {
            this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONSULTARTERMINOSOK), response, true);
			
		} else {
			NameValuePair[] parameters = new NameValuePair[1];
			parameters[0] = new NameValuePair(ServerConstants.PERFIL_CLIENTE, (String) params.get(ServerConstants.PERFIL_CLIENTE));
			clienteHttp.invokeOperation(OPERATION_CODES[OP_CONSULTAR_TERMINOS], parameters, response, false, true);
		}
		
		return response;
	}

	private ServerResponse envioClaveActivacion(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();

		NameValuePair[] parameters = new NameValuePair[9];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair("numeroCliente", (String) params.get(ServerConstants.NUMERO_CLIENTE));
		parameters[2] = new NameValuePair("companiaCelular", (String) params.get(ServerConstants.COMPANIA_CELULAR));
		parameters[3] = new NameValuePair("cveAcceso", (String) params.get(PASSWORD_PARAM));
		parameters[4] = new NameValuePair("codigoNIP", (String) params.get(NIP_PARAM));
		parameters[5] = new NameValuePair("codigoCVV2", (String) params.get(CVV2_PARAM));
		parameters[6] = new NameValuePair("codigoOTP", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[7] = new NameValuePair("cadenaAutenticacion", (String) params.get(VA_PARAM));
		parameters[8] = new NameValuePair("tarjeta5Dig", (String) params.get("tarjeta5Dig"));

		if (SIMULATION) {
            clienteHttp.simulateOperation(OPERATION_CODES[OP_ENVIO_CLAVE_ACTIVACION], parameters, response, oReadDocument.read(ConstantsPropertyList.OK_2), true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[OP_ENVIO_CLAVE_ACTIVACION], parameters, response, false, true);
		}
		return response;
	}

	/**
	 * Configuracion de nuevo correo electrónico.
	 * @param params Parametros de la operación.
	 * @return Respuesta del servidor.
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	/*TODO AMB
	private ServerResponse configurarCorreo(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse(new ConfigurarCorreoData());

		NameValuePair[] parameters = new NameValuePair[10];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(USERNAME_PARAM));
		parameters[1] = new NameValuePair("email", (String) params.get(ServerConstants.EMAIL));
		parameters[2] = new NameValuePair("numeroCliente", (String) params.get(ServerConstants.NUMERO_CLIENTE));
		parameters[3] = new NameValuePair("IUM", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[4] = new NameValuePair("cveAcceso", (String) params.get(PASSWORD_PARAM));
		parameters[5] = new NameValuePair("codigoNIP", (String) params.get(NIP_PARAM));
		parameters[6] = new NameValuePair("codigoCVV2", (String) params.get(CVV2_PARAM));
		parameters[7] = new NameValuePair("codigoOTP", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[8] = new NameValuePair("cadenaAutenticacion", (String) params.get(VA_PARAM));
		parameters[9] = new NameValuePair("tarjeta5Dig", (String) params.get("tarjeta5Dig"));

		if (SIMULATION) {
            String msg = oReadDocument.read(ConstantsPropertyList.CONFIGURARCORREOOK);
			clienteHttp.simulateOperation(OPERATION_CODES[OP_CONFIGURAR_CORREO], parameters, response, msg, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[OP_CONFIGURAR_CORREO], parameters, response, false, true);
		}
		return response;
	}*/

	/**
	 * Configuracion de nuevo correo electrónico.
	 * @param params Parametros de la operación.
	 * @return Respuesta del servidor.
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse enviarCorreo(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();

		final NameValuePair[] parameters = new NameValuePair[11];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair("IUM", (String) params.get(ServerConstants.IUM_ETIQUETA));
		parameters[2] = new NameValuePair("tipoComprobante", (String) params.get(VOUCHER_TYPE_PARAM));
		parameters[3] = new NameValuePair("folioOperacion", (String) params.get(FOLIO_OPERACION_PARAM));
		parameters[4] = new NameValuePair("beneficiario", (String) params.get(BENEFICIARIO_PARAM));
		parameters[5] = new NameValuePair("concepto", (String) params.get(CONCEPTO_PARAM));
		parameters[6] = new NameValuePair(ServerConstants.BANCO_DESTINO, (String) params.get(ServerConstants.BANCO_DESTINO));
		parameters[7] = new NameValuePair("correoDestino", (String) params.get(ServerConstants.EMAIL));
		parameters[8] = new NameValuePair("mensajeA", (String) params.get(MSG_PARAM));
		parameters[9] = new NameValuePair("codTrans", (String) params.get(ServerConstants.CODIGO_TRANSACCION));
		parameters[10] = new NameValuePair("versionFlujo", BmovilConstants.APPLICATION_VERSION);

		if (SIMULATION) {
            String responseJson = oReadDocument.read(ConstantsPropertyList.OK_2);

			this.clienteHttp.simulateOperation(OPERATION_CODES[OP_ENVIO_CORREO], parameters, response, responseJson, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[OP_ENVIO_CORREO], parameters, response, false, true);
		}
		
		return response;
	}

	/**
	 * Realiza la operacion de validaCredenciales para Registro de Operacion
	 * @param params
	 * @return
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse validarCredenciales(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse(new RegistrarOperacionResult());

		NameValuePair[] parameters = new NameValuePair[9];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair("numeroCliente", (String) params.get(ServerConstants.NUMERO_CLIENTE));
		parameters[2] = new NameValuePair("cveAcceso", (String) params.get(ServerConstants.CVE_ACCESO));
		parameters[3] = new NameValuePair("codigoNIP", (String) params.get(J_NIP));
		parameters[4] = new NameValuePair("codigoCVV2", (String) params.get(J_CVV2));
		parameters[5] = new NameValuePair("codigoOTP", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[6] = new NameValuePair("cadenaAutenticacion", (String) params.get(J_AUT));
		parameters[7] = new NameValuePair("cuentaDestino", (String) params.get("cuentaDestino"));
		parameters[8] = new NameValuePair("tarjeta5Dig", "");

		if (SIMULATION) {
            String resp = oReadDocument.read(ConstantsPropertyList.OK_2);
			clienteHttp.simulateOperation(OPERATION_CODES[OP_VALIDAR_CREDENCIALES], parameters, response, resp, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[OP_VALIDAR_CREDENCIALES], parameters, response, false, true);
		}
		return response;
	}

	/**
	 * Realiza la operacion de finalizarContrtcion el flujo de Contratacion.
	 * @param params Parametros de env�o al servidor.
	 * @return La respuesta del servidor.
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse finalizarContratacionAlertas(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();
		
		NameValuePair[] parameters = new NameValuePair[10];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair("cveAcceso", (String) params.get(PASSWORD_PARAM));
		parameters[2] = new NameValuePair("numeroTarjeta", (String) params.get(CARDNUMBER_PARAM));
		parameters[3] = new NameValuePair("companiaCelular", (String) params.get(ServerConstants.COMPANIA_CELULAR));
		parameters[4] = new NameValuePair("estatusAlertas", (String) params.get(ALERTSTATUS_PARAM));
		parameters[5] = new NameValuePair("codigoNIP", (String) params.get(NIP_PARAM));
		parameters[6] = new NameValuePair("codigoCVV2", (String) params.get(CVV2_PARAM));
		parameters[7] = new NameValuePair("codigoOTP", (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[8] = new NameValuePair("cadenaAutenticacion", (String) params.get(VA_PARAM));
		parameters[9] = new NameValuePair("tarjeta5Dig", (String) params.get("tarjeta5Dig"));

		if (SIMULATION) {
            String resp = oReadDocument.read(ConstantsPropertyList.FINCONTRALERTAOK_2);
			clienteHttp.simulateOperation(OPERATION_CODES[OP_FINALIZAR_CONTRATACION_ALERTAS], parameters, response, resp, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[OP_FINALIZAR_CONTRATACION_ALERTAS], parameters, response, false, true);
		}
		return response;
	}

	//SPEI
	
	/**
	 * Sends a request to the server to retrieve the info of the accounts available for spei operations.
	 * @param params Params to send to the server.
	 * @return The server response.
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	/*
	private ServerResponse requestSpeiAccounts(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse(new RequestSpeiAccountsResult());
		int index = 0;
		NameValuePair[] parameters = new NameValuePair[3];
		
		parameters[index++] = new NameValuePair("numeroTelefono", 		(String)params.get(USERNAME_PARAM));
		parameters[index++] = new NameValuePair("IUM", 					(String) params.get(ServerConstants.JSON_IUM_ETIQUETA));//(String)params.get(IUM_PARAM));
		parameters[index++] = new NameValuePair("numeroCliente", 		(String) params.get(ServerConstants.NUMERO_CLIENTE));//(String)params.get(CLIENTNUMBER_PARAM));
		
		if (SIMULATION) {
			String resp = "{\"estado\":\"OK\",\"switch\":\"FALSE\",\"cuentas\":[{\"numeroCliente\":\"B1316895\",\"numeroCuenta\":\"00743616002700628034\",\"telefono\":\"5515861861\",\"codigoCompania\":\"02\",\"descripcionCompania\":\"MOVISTAR\",\"fechaUltimaModificacion\":\"2014-02-15\",\"indicadorSPEI\":\"S\"},{\"numeroCliente\":\"B1316896\",\"numeroCuenta\":\"00743616002700894637\",\"telefono\":\"\",\"codigoCompania\":\"\",\"descripcionCompania\":\"\",\"fechaUltimaModificacion\":\"\",\"indicadorSPEI\":\"N\"},{\"numeroCliente\":\"B1316897\",\"numeroCuenta\":\"00743616002700846372\",\"telefono\":\"5555555555\",\"codigoCompania\":\"04\",\"descripcionCompania\":\"IUSACELL\",\"fechaUltimaModificacion\":\"2014-02-15\",\"indicadorSPEI\":\"S\"}]}";
			clienteHttp.simulateOperation(OPERATION_CODES[SPEI_ACCOUNTS_REQUEST], parameters, response, resp, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[SPEI_ACCOUNTS_REQUEST], parameters, response, false, true);
		}
		return response;
	}
	*/
	
	/**
	 * Sends a request to the server to retrieve the terms and conditions for spei operations.
	 * @param params Params to send to the server.
	 * @return The server response.
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse requestSpeiTermsAndConditions(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		//SIMULATION = false;
		
		ServerResponse response = new ServerResponse(new SpeiTermsAndConditionsResult());
		
		int index = 0;
		NameValuePair[] parameters = new NameValuePair[1];
		parameters[index++] = new NameValuePair("idProducto",	(String)params.get(SPEI_MAINTENANCE_TYPE_PARAM));
		
		if (SIMULATION) {
            String resp = oReadDocument.read(ConstantsPropertyList.REQUESTSPEITERMSCONDITIONSOK_2);
            clienteHttp.simulateOperation(OPERATION_CODES[SPEI_TERMS_REQUEST], parameters, response, resp, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[SPEI_TERMS_REQUEST], parameters, response, false, true);
		}
		return response;
	} 
	
	/**
	 * Sends a request to the server to retrieve the terms and conditions for spei operations.
	 * @param params Params to send to the server.
	 * @return The server response.
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse doSpeiMaintenance(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();
		int index = 0;
		NameValuePair[] parameters = new NameValuePair[16];

		parameters[index++] = new NameValuePair("numeroTelefono", 		(String)params.get(USERNAME_PARAM));
		parameters[index++] = new NameValuePair("asunto", 				(String) params.get(ACCOUNT_PARAM));
		parameters[index++] = new NameValuePair("numeroTelefonoA", 		(String)params.get(OLD_PHONENUMBER_PARAM));
		parameters[index++] = new NameValuePair("companiaCelularVieja", (String)params.get(OLD_COMPANY_NAME_PARAM));
		parameters[index++] = new NameValuePair("numeroCelNuevo", 		(String)params.get(PHONENUMBER_PARAM));
		parameters[index++] = new NameValuePair("companiaCelularNueva", (String)params.get(NEW_COMPANY_NAME_PARAM));
		parameters[index++] = new NameValuePair("companiaCelular", 		(String)params.get(COMPANIA_TELEFONICA));
		parameters[index++] = new NameValuePair("tipoMantenimiento", 	(String)params.get(SPEI_MAINTENANCE_TYPE_PARAM));
		parameters[index++] = new NameValuePair("IUM", 					(String) params.get(ServerConstants.JSON_IUM_ETIQUETA));//(String)params.get(IUM_PARAM));
		parameters[index++] = new NameValuePair("numeroCliente", 		(String) params.get(ServerConstants.NUMERO_CLIENTE)); //(String)params.get(CLIENTNUMBER_PARAM));
		parameters[index++] = new NameValuePair("codigoNIP", 			(String)params.get(NIP_PARAM));
		parameters[index++] = new NameValuePair("codigoCVV2", 			(String)params.get(CVV2_PARAM));
		parameters[index++] = new NameValuePair("codigoOTP", 			(String) params.get(ServerConstants.CODIGO_OTP));
		parameters[index++] = new NameValuePair("cadenaAutenticacion",	(String)params.get(VA_PARAM));
		parameters[index++] = new NameValuePair("cveAcceso", 			(String)params.get(PASSWORD_PARAM));
		parameters[index++] = new NameValuePair("tarjeta5Dig", 			(String)params.get(LAST_CARD_DIGITS_PARAM));
		
		if (SIMULATION) {
            String resp = oReadDocument.read(ConstantsPropertyList.OK_2);
			clienteHttp.simulateOperation(OPERATION_CODES[SPEI_MAINTENANCE], parameters, response, resp, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[SPEI_MAINTENANCE], parameters, response, false, true);
		}		
		return response;
	}

	/**
	 * Sends a request to the server to retrieve the beneficiary account related to a phone number.
	 * @param params Params to send to the server.
	 * @return The server response.
	 * @throws IOException
	 * @throws ParsingException
	 * @throws URISyntaxException
	 */
	private ServerResponse consultThirdsAccountBBVA(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse(new ConsultThirdsAccountBBVAResult());
		int index = 0;
		NameValuePair[] parameters = new NameValuePair[3];

		parameters[index++] = new NameValuePair("numeroTelefono", 	(String)params.get(USERNAME_PARAM));
		parameters[index++] = new NameValuePair("IUM",(String) params.get(ServerConstants.JSON_IUM_ETIQUETA));				//(String)params.get(IUM_PARAM));
		parameters[index++] = new NameValuePair("telefonoDeposito",(String)params.get(PHONENUMBER_PARAM)); //(String)params.get(CELLPHONENUMBER_PARAM)
		
		if (SIMULATION) {
            String resp = oReadDocument.read(ConstantsPropertyList.CONTHIRDSACCOUNTBBVA_OK);
			clienteHttp.simulateOperation(OPERATION_CODES[CONSULT_BENEFICIARY_ACCOUNT_NUMBER], parameters, response, resp, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULT_BENEFICIARY_ACCOUNT_NUMBER], parameters, response, false, true);
		}
		return response;
	}
	
	//Termina SPEI
	
	// #region Softtoken
	public static final String OTP_TOKEN = "otp_token";
	public static final String OTP1 = "otp_gene1";
	public static final String OTP2 = "otp_gene2";
	public static final String NUMERO_CLIENTE = "Numerocliente";
	public static final String COMPANIA_TELEFONICA = "Compañiatelefonica";
	public static final String TIPO_SOLICITUD_ST = "tiposolicitudst";
	public static final String INSTRUMENTO_SEGURIDAD_ST = "instrumentoseguridadst";
	public static final String NIP_CAJERO = "nipcajeros";
	public static final String NUMERO_SERIE_ST = "numeroserie";
	public static final String EMAIL_ST = "mailst";
	public static final String NOMBRE_TOKEN = "nombreToken";
	public static final String NOMBRE_CLIENTE = "Nombrecliente";
	//CÛdigo Karen
	public static final String PERFIL_PARAM = "PerfilCliente";
	public static final String OTP_PARAM = "OTP";

	public ServerResponse sincronizacionST(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();

		NameValuePair[] parameters = new NameValuePair[3];
		parameters[0] = new NameValuePair("NA", (String) params.get(NOMBRE_TOKEN));
		parameters[1] = new NameValuePair("O1", (String) params.get(OTP1));
		parameters[2] = new NameValuePair("O2", (String) params.get(OTP2));

		if (SIMULATION) {
            String message = oReadDocument.read(ConstantsPropertyList.OK);
			httpClient.simulateOperation(message, response);
			clienteHttp.simulateOperation(OPERATION_CODES[SINCRONIZACION_SOFTTOKEN], parameters, response, message);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[SINCRONIZACION_SOFTTOKEN], parameters, response,false,false);
		}
		return response;
	}

	public ServerResponse exportacionST(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();

		NameValuePair[] parameters = new NameValuePair[6];
		parameters[0] = new NameValuePair("NJ", (String) params.get(ServerConstants.NUMERO_TARJETA));
		parameters[1] = new NameValuePair("NT", (String) params.get(ServerConstants.NUMERO_TELEFONO));
//		parameters[2] = new NameValuePair("TE", (String) params.get(NUMERO_CLIENTE));
		parameters[3] = new NameValuePair("TS", (String) params.get(TIPO_SOLICITUD_ST));
		parameters[4] = new NameValuePair("NA", (String) params.get(NOMBRE_TOKEN));
//		parameters[2] = new NameValuePair("TE", "AAAAAAAA");
		parameters[2] = new NameValuePair("TE", (String) params.get(Constants.EMPTY_STRING));
		parameters[5] = new NameValuePair("AV", (String) params.get(ServerConstants.VERSION_APP));

		if (SIMULATION) {
            String message = oReadDocument.read(ConstantsPropertyList.OK);
			httpClient.simulateOperation(message, response);
			clienteHttp.simulateOperation(OPERATION_CODES[EXPORTACION_SOFTTOKEN], parameters, response, message);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[EXPORTACION_SOFTTOKEN], parameters, response,false,false);
		}
		return response;
	}

	/**
	 * Operacion del proceso de activacion de softToken - Contratacion
	 * enrolamiento.
	 * @param params los parametros a enviar al servidor
	 * @return la respuesta del servidor
	 * @throws IOException excepciones de entrada/salida
	 * @throws ParsingException excepciones de parseos
	 * @throws URISyntaxException excepciones de URIs
	 */
	public ServerResponse contratacionEnrolamiento(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();

		NameValuePair[] parameters = new NameValuePair[8];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair(ServerConstants.CVE_ACCESO, (String) params.get(ServerConstants.CVE_ACCESO));
		parameters[2] = new NameValuePair(ServerConstants.NUMERO_TARJETA, (String) params.get(ServerConstants.NUMERO_TARJETA));
		parameters[3] = new NameValuePair(ServerConstants.PERFIL_CLIENTE, (String) params.get(ServerConstants.PERFIL_CLIENTE));
		parameters[4] = new NameValuePair(ServerConstants.COMPANIA_CELULAR, (String) params.get(ServerConstants.COMPANIA_CELULAR));
		parameters[5] = new NameValuePair(ServerConstants.EMAIL, (String) params.get(ServerConstants.EMAIL));
		parameters[6] = new NameValuePair(ServerConstants.ACEPTO_TERMINOS_CONDICIONES, (String) params.get(ServerConstants.ACEPTO_TERMINOS_CONDICIONES));
		parameters[7] = new NameValuePair(ServerConstants.VERSION_FLUJO, Constants.VERSION_FLUJO_CONTRATACION);

		if (SIMULATION) {
            String responseJson = oReadDocument.read(ConstantsPropertyList.CONTENROLAMIENTOOK);
			clienteHttp.simulateOperation(OPERATION_CODES[CONTRATACION_ENROLAMIENTO_ST], parameters, response, responseJson, true);
			
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CONTRATACION_ENROLAMIENTO_ST], parameters, response, false, true);
		}

		return response;
	}

	/**
	 * Operacion del proceso de activacion de softToken - Finalizar contratacion Bmovil ST.
	 * @param params los parametros a enviar al servidor
	 * @return la respuesta del servidor
	 * @throws IOException excepciones de entrada/salida
	 * @throws ParsingException excepciones de parseos
	 * @throws URISyntaxException excepciones de URIs
	 */
	public ServerResponse finalizarContratacionST(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();

		NameValuePair[] parameters = new NameValuePair[5];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TARJETA, (String) params.get(ServerConstants.NUMERO_TARJETA));
		parameters[1] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[2] = new NameValuePair(ServerConstants.JSON_IUM_ETIQUETA, (String) params.get(ServerConstants.JSON_IUM_ETIQUETA));
		parameters[3] = new NameValuePair(ServerConstants.COMPANIA_CELULAR, (String) params.get(ServerConstants.COMPANIA_CELULAR));
		parameters[4] = new NameValuePair(ServerConstants.CODIGO_OTP, (String) params.get(ServerConstants.CODIGO_OTP));

		if (SIMULATION) {
            String responseJson = oReadDocument.read(ConstantsPropertyList.OK_2);
			clienteHttp.simulateOperation(OPERATION_CODES[FINALIZAR_CONTRATACION_ST], parameters, response, responseJson, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[FINALIZAR_CONTRATACION_ST], parameters, response, false, true);
		}

		return response;
	}

	/**
	 * Operacion del proceso de activacion de softToken - Cambio Telefono
	 * Asociado ST.
	 * @param params los parametros a enviar al servidor
	 * @return la respuesta del servidor
	 * @throws IOException excepciones de entrada/salida
	 * @throws ParsingException excepciones de parseos
	 * @throws URISyntaxException excepciones de URIs
	 */
	public ServerResponse cambioTelefonoAsociadoST(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();

		NameValuePair[] parameters = new NameValuePair[5];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[1] = new NameValuePair(ServerConstants.COMPANIA_CELULAR, (String) params.get(ServerConstants.COMPANIA_CELULAR));
		parameters[2] = new NameValuePair(ServerConstants.CODIGO_OTP, (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[3] = new NameValuePair(ServerConstants.NUMERO_CLIENTE, (String) params.get(ServerConstants.NUMERO_CLIENTE));
		parameters[4] = new NameValuePair(ServerConstants.NUMERO_TARJETA, (String) params.get(ServerConstants.NUMERO_TARJETA));

		if (SIMULATION) {
            String responseJson = oReadDocument.read(ConstantsPropertyList.OK_2);
			clienteHttp.simulateOperation(OPERATION_CODES[CAMBIO_TELEFONO_ASOCIADO_ST], parameters, response, responseJson, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[CAMBIO_TELEFONO_ASOCIADO_ST], parameters, response, false, true);
		}

		return response;
	}

	/**
	 * Operacion del proceso de activacion de softToken - Solicitud ST.
	 * @param params los parametros a enviar al servidor
	 * @return la respuesta del servidor
	 * @throws IOException excepciones de entrada/salida
	 * @throws ParsingException excepciones de parseos
	 * @throws URISyntaxException excepciones de URIs
	 */
	public ServerResponse solicitudST(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();

		NameValuePair[] parameters = new NameValuePair[5];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TARJETA, (String) params.get(ServerConstants.NUMERO_TARJETA));
		parameters[1] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[2] = new NameValuePair(ServerConstants.COMPANIA_CELULAR, (String) params.get(ServerConstants.COMPANIA_CELULAR));
		parameters[3] = new NameValuePair("email", (String) params.get(ServerConstants.CORREO_ELECTRONICO));
		parameters[4] = new NameValuePair(ServerConstants.NUMERO_CLIENTE, (String) params.get(ServerConstants.NUMERO_CLIENTE));

		if (SIMULATION) {
            String responseJson = oReadDocument.read(ConstantsPropertyList.OK_2);
			clienteHttp.simulateOperation(OPERATION_CODES[SOLICITUD_ST], parameters, response, responseJson, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[SOLICITUD_ST], parameters, response, false, true);
		}

		return response;
	}

	/**
	 * Operacion de mantenimiento de alertas.
	 * @param params los parametros a enviar al servidor
	 * @return la respuesta del servidor
	 * @throws IOException excepciones de entrada/salida
	 * @throws ParsingException excepciones de parseos
	 * @throws URISyntaxException excepciones de URIs
	 */
	public ServerResponse mantenimientoAlertas(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		MantenimientoAlertasResultado data = new MantenimientoAlertasResultado();
		ServerResponse response = new ServerResponse(data);

		NameValuePair[] parameters = new NameValuePair[11];
		parameters[0] = new NameValuePair(ServerConstants.NUMERO_TARJETA, (String) params.get(ServerConstants.NUMERO_TARJETA));
		parameters[1] = new NameValuePair(ServerConstants.INDICADOR, (String) params.get(ServerConstants.INDICADOR));
		parameters[2] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[3] = new NameValuePair(ServerConstants.COMPANIA_CELULAR, (String) params.get(ServerConstants.COMPANIA_CELULAR));
		parameters[4] = new NameValuePair(ServerConstants.CVE_ACCESO, (String) params.get(ServerConstants.CVE_ACCESO));
		parameters[5] = new NameValuePair(ServerConstants.CODIGO_NIP, (String) params.get(ServerConstants.CODIGO_NIP));
		parameters[6] = new NameValuePair(ServerConstants.CODIGO_OTP, (String) params.get(ServerConstants.CODIGO_OTP));
		parameters[7] = new NameValuePair(ServerConstants.CODIGO_CVV2, (String) params.get(ServerConstants.CODIGO_CVV2));
		parameters[8] = new NameValuePair(ServerConstants.PERFIL_CLIENTE, (String) params.get(ServerConstants.PERFIL_CLIENTE));
		parameters[9] = new NameValuePair(ServerConstants.CADENA_AUTENTICACION, (String) params.get(ServerConstants.CADENA_AUTENTICACION));
		parameters[10] = new NameValuePair(ServerConstants.TARJETA_5DIG, (String) params.get(ServerConstants.TARJETA_5DIG));

		if (SIMULATION) {
            String responseJson = oReadDocument.read(ConstantsPropertyList.MANTENIMIENTOALERTASOK);
			clienteHttp.simulateOperation(OPERATION_CODES[MANTENIMIENTO_ALERTAS], parameters, response, responseJson, true);
		} else {
			clienteHttp.invokeOperation(OPERATION_CODES[MANTENIMIENTO_ALERTAS], parameters, response, false, true);
		}

		return response;
	}
	//One CLick

			private ServerResponse consultaDetalleOferta(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				
				OfertaILC ofertaILC=new OfertaILC();
				ServerResponse response = new ServerResponse(ofertaILC);
				isjsonvalueCode=isJsonValueCode.ONECLICK;
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONDETALLEOFERTAOK),response, true);
				}else{
					String numeroCelular = (String) params.get("numeroCelular");
					String cveCamp = (String) params.get("cveCamp");
					String IUM = (String) params.get("IUM");
								
					NameValuePair[] parameters = new NameValuePair[3];
					parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
					parameters[1] = new NameValuePair("cveCamp", cveCamp);
					parameters[2] = new NameValuePair("IUM",IUM);

					clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DETALLE_OFERTA], parameters, response, false, true);
				}
				return response;		
			}
			
			private ServerResponse aceptaOfertaILC(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				
				AceptaOfertaILC aceptaOfertaILC=new AceptaOfertaILC();
				ServerResponse response = new ServerResponse(aceptaOfertaILC);
				isjsonvalueCode=isJsonValueCode.ONECLICK;
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.ACEPTAOFERTAILC_OK1),response, true);

				}else{
								
					String numeroTarjeta = (String) params.get("numeroTarjeta");
					String lineaActual = (String) params.get("lineaActual");
					String importe = (String) params.get("importe");
					String lineaFinal = (String) params.get("lineaFinal");
					String cveCamp = (String) params.get("cveCamp");
					String numeroCelular = (String) params.get("numeroCelular");
					String IUM = (String) params.get("IUM");
					String Cat = (String) params.get("Cat");
					String fechaCat = (String) params.get("fechaCat");
			
					NameValuePair[] parameters = new NameValuePair[9];
					parameters[0] = new NameValuePair("numeroTarjeta", numeroTarjeta);
					parameters[1] = new NameValuePair("lineaActual", lineaActual);
					parameters[2] = new NameValuePair("importe",importe);
					parameters[3] = new NameValuePair("lineaFinal",lineaFinal);
					parameters[4] = new NameValuePair("cveCamp",cveCamp);
					parameters[5] = new NameValuePair("numeroCelular",numeroCelular);
					parameters[6] = new NameValuePair("IUM",IUM);
					parameters[7] = new NameValuePair("Cat",Cat);
					parameters[8] = new NameValuePair("fechaCat",fechaCat);

					clienteHttp.invokeOperation(OPERATION_CODES[ACEPTACION_OFERTA], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponse exitoOfertaILC(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
					//ResultExitoILC resultExitoILC= new ResultExitoILC();
					ServerResponse response = new ServerResponse();
					isjsonvalueCode=isJsonValueCode.ONECLICK;
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.OK_2),response, true);
				}else{
					String numeroTarjeta = (String) params.get("numeroTarjeta");
					String lineaActual = (String) params.get("lineaActual");
					String Importe = (String) params.get("importe");
					String lineaFinal = (String) params.get("lineaFinal");
					String numeroCelular = (String) params.get("numeroCelular");
					String email = (String) params.get("email");
					String folioInternet = (String) params.get("folioInternet");
					String Cat = (String) params.get("Cat");
					String fechaCat = (String) params.get("fechaCat");
					String mensajeA = (String) params.get("mensajeA");
					String IUM = (String) params.get("IUM");
								
					NameValuePair[] parameters = new NameValuePair[11];
					parameters[0] = new NameValuePair("numeroTarjeta", numeroTarjeta);
					parameters[1] = new NameValuePair("lineaActual", lineaActual);
					parameters[2] = new NameValuePair("importe",Importe);
					parameters[3] = new NameValuePair("lineaFinal",lineaFinal);
					parameters[4] = new NameValuePair("numeroCelular",numeroCelular);
					parameters[5] = new NameValuePair("email",email);
					parameters[6] = new NameValuePair("folioInternet",folioInternet);
					parameters[7] = new NameValuePair("Cat",Cat);
					parameters[8] = new NameValuePair("fechaCat",fechaCat);
					parameters[9] = new NameValuePair("mensajeA",mensajeA);
					parameters[10] = new NameValuePair("IUM",IUM);

					clienteHttp.invokeOperation(OPERATION_CODES[EXITO_OFERTA], parameters, response, false, true);
				}
				return response;
			}

			
			
			private ServerResponse rechazoOfertaILC(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				ServerResponse response = new ServerResponse();
				isjsonvalueCode=isJsonValueCode.ONECLICK;
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.RECHAZOOFERTAILC_OK),response, true);
				}else{
					String numeroCelular = (String) params.get("numeroCelular");
					String cveCamp = (String) params.get("cveCamp");
					String descripcionMensaje = (String) params.get("descripcionMensaje");
					String contrato = (String) params.get("contrato");
					String IUM = (String) params.get("IUM");
								
					NameValuePair[] parameters = new NameValuePair[5];
					parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
					parameters[1] = new NameValuePair("cveCamp", cveCamp);
					parameters[2] = new NameValuePair("descripcionMensaje",descripcionMensaje);
					parameters[3] = new NameValuePair("contrato",contrato);
					parameters[4] = new NameValuePair("IUM",IUM);

					clienteHttp.invokeOperation(OPERATION_CODES[RECHAZO_OFERTA], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponse consultaDetalleOfertaEFI(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				isjsonvalueCode=isJsonValueCode.ONECLICK;
				OfertaEFI ofertaEFI=new OfertaEFI();
				ServerResponse response = new ServerResponse(ofertaEFI);
				
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONDETALLEOFERTAEFI_OK),response, true);
				}else{
					
					
					String numeroCelular = (String) params.get("numeroCelular");
					String cveCamp = (String) params.get("cveCamp");
					String IUM = (String) params.get("IUM");
								
					NameValuePair[] parameters = new NameValuePair[3];
					parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
					parameters[1] = new NameValuePair("cveCamp", cveCamp);
					parameters[2] = new NameValuePair("IUM",IUM);

					clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DETALLE_OFERTA_EFI], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponse consultaAceptaOfertaEFI(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				
				AceptaOfertaEFI aceptaOfertaEFI=new AceptaOfertaEFI();
				ServerResponse response = new ServerResponse(aceptaOfertaEFI);
				isjsonvalueCode=isJsonValueCode.ONECLICK;
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONACEPTAOFERTAEFI_OK2),response, true);
				}else{
					
					
					String numeroCelular = (String) params.get("numeroCelular");
					String cveCamp = (String) params.get("cveCamp");
					String comisionEFIPorcentaje = (String) params.get("comisionEFIPorcentaje");
					String comisionEFIMonto = (String) params.get("comisionEFIMonto");
					String celula = (String) params.get("celula");
					String edicion = (String) params.get("edicion");
					String numeroTarjeta = (String) params.get("numeroTarjeta");
					String numeroCuenta = (String) params.get("numeroCuenta");
					String tipoCuenta = (String) params.get("tipoCuenta"); // **Tipo de cuenta EFI
					String importe = (String) params.get("importe");
					String impDispuesto = (String) params.get("impDispuesto");
					String tasaAnual = (String) params.get("tasaAnual");
					String fechaCat = (String) params.get("fechaCat");
					String Cat = (String) params.get("Cat");
					String pagoMensualSimulador= (String) params.get("pagoMensualSimulador");
					String IUM = (String) params.get("IUM");

								
					NameValuePair[] parameters = new NameValuePair[16];
					parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
					parameters[1] = new NameValuePair("cveCamp", cveCamp);
					parameters[2] = new NameValuePair("comisionEFIPorcentaje",comisionEFIPorcentaje);
					parameters[3] = new NameValuePair("comisionEFIMonto",comisionEFIMonto);
					parameters[4] = new NameValuePair("celula",celula);
					parameters[5] = new NameValuePair("edicion",edicion);
					parameters[6] = new NameValuePair("numeroTarjeta",numeroTarjeta);
					parameters[7] = new NameValuePair("numeroCuenta",numeroCuenta);
					parameters[8] = new NameValuePair("tipoCuenta",tipoCuenta); // **Tipo de Tarjeta EFI
					parameters[9] = new NameValuePair("importe",importe);
					parameters[10] = new NameValuePair("impDispuesto",impDispuesto);
					parameters[11] = new NameValuePair("tasaAnual",tasaAnual);
					parameters[12] = new NameValuePair("fechaCat",fechaCat);
					parameters[13] = new NameValuePair("Cat",Cat);
					parameters[14] = new NameValuePair("pagoMensualSimulador",pagoMensualSimulador);
					parameters[15] = new NameValuePair("IUM",IUM);
					

					clienteHttp.invokeOperation(OPERATION_CODES[ACEPTACION_OFERTA_EFI], parameters, response, false, true);
				}
				return response;
			}
			

			private ServerResponse exitoOfertaEFI(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
					//ResultExitoILC resultExitoILC= new ResultExitoILC();
					ServerResponse response = new ServerResponse();
					isjsonvalueCode=isJsonValueCode.ONECLICK;
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.OK_2),response, true);
				}else{
					String numeroCelular = (String) params.get("numeroCelular");
					String cuentaDeposito = (String) params.get("cuentaDeposito");
					String numeroTarjeta = (String) params.get("numeroTarjeta");
					String importe = (String) params.get("importe");
					String tasaAnual = (String) params.get("tasaAnual");
					String plazo = (String) params.get("plazo");
					String pagoFijo = (String) params.get("pagoFijo");
					String comisionDisp = (String) params.get("comisionDisp");
					String email = (String) params.get("email");
					String fechaOperacion = (String) params.get("fechaOperacion");
					String horaOperacion = (String) params.get("horaOperacion");
					String folioInternet = (String) params.get("folioInternet");
					String Cat = (String) params.get("Cat");
					String fechaCat = (String) params.get("fechaCat");
					String mensajeA = (String) params.get("mensajeA");
					String IUM = (String) params.get("IUM");
								
					NameValuePair[] parameters = new NameValuePair[16];
					parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
					parameters[1] = new NameValuePair("cuentaDeposito", cuentaDeposito);
					parameters[2] = new NameValuePair("numeroTarjeta",numeroTarjeta);
					parameters[3] = new NameValuePair("importe",importe);
					parameters[4] = new NameValuePair("tasaAnual",tasaAnual);
					parameters[5] = new NameValuePair("plazo",plazo);
					parameters[6] = new NameValuePair("pagoFijo",pagoFijo);
					parameters[7] = new NameValuePair("comisionDisp",comisionDisp);
					parameters[8] = new NameValuePair("email",email);
					parameters[9] = new NameValuePair("fechaOperacion",fechaOperacion);
					parameters[10] = new NameValuePair("horaOperacion",horaOperacion);
					parameters[11] = new NameValuePair("folioInternet",folioInternet);
					parameters[12] = new NameValuePair("Cat",Cat);
					parameters[13] = new NameValuePair("fechaCat",fechaCat);
					parameters[14] = new NameValuePair("mensajeA",mensajeA);
					parameters[15] = new NameValuePair("IUM",IUM);

					clienteHttp.invokeOperation(OPERATION_CODES[EXITO_OFERTA_EFI], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponse simuladorEFI(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				
				ImporteEFI importeEFI=new ImporteEFI();
				ServerResponse response = new ServerResponse(importeEFI);
				isjsonvalueCode=isJsonValueCode.ONECLICK;
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.SIMULADOREFI_OK),response, true);
				}else{
					
					
					String numeroCelular = (String) params.get("numeroCelular");
					String nuevoImporte = (String) params.get("nuevoImporte");
					String plazoMeses = (String) params.get("plazoMeses");
					String tasaAnual = (String) params.get("tasaAnual");
					String comisionEFIPorcentaje = (String) params.get("comisionEFIPorcentaje");
					String IUM = (String) params.get("IUM");


								
					NameValuePair[] parameters = new NameValuePair[6];
					parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
					parameters[1] = new NameValuePair("nuevoImporte", nuevoImporte);
					parameters[2] = new NameValuePair("plazoMeses",plazoMeses);
					parameters[3] = new NameValuePair("tasaAnual",tasaAnual);
					parameters[4] = new NameValuePair("comisionEFIPorcentaje",comisionEFIPorcentaje);
					parameters[5] = new NameValuePair("IUM",IUM);
					clienteHttp.invokeOperation(OPERATION_CODES[SIMULADOR_EFI], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponse consultaDetalleConsumo(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				
				OfertaConsumo ofertaConsumo=new OfertaConsumo();
				ServerResponse response = new ServerResponse(ofertaConsumo);
				isjsonvalueCode=isJsonValueCode.CONSUMO;
				if (SIMULATION) {
					
					// trama CLIENTE PREAPROBADO
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONDETALLECONSUMOOK_1),response, true);
				}else{
					
					String numeroCelular = (String) params.get("numeroCelular");
					String claveCamp = (String) params.get("claveCamp");
					String IUM = (String) params.get("IUM");
					String importePar = (String) params.get("importePar");
								
					NameValuePair[] parameters = new NameValuePair[4];
					parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
					parameters[1] = new NameValuePair("claveCamp", claveCamp);
					parameters[2] = new NameValuePair("IUM",IUM);
					parameters[3] = new NameValuePair("importePar",importePar);

					
					clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_DETALLE_OFERTA_CONSUMO], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponse polizaConsumo(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				ServerResponse response = new ServerResponse(new ConsultaPoliza());
				isjsonvalueCode=isJsonValueCode.CONSUMO;
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.POLIZACONSUMOOK),response, true);
				}else{
					
					
					String IdProducto = (String) params.get("IdProducto");			
					NameValuePair[] parameters = new NameValuePair[1];
					parameters[0] = new NameValuePair("IdProducto", IdProducto);
					
					clienteHttp.invokeOperation(OPERATION_CODES[POLIZA_OFERTA_CONSUMO], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponse terminosConsumo(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				ServerResponse response = new ServerResponse(new ConsultaPoliza());
				isjsonvalueCode=isJsonValueCode.CONSUMO;
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.TERMINOSCONSUMOOK),response, true);
				}else{

					String opcion = (String) params.get("opcion");
					String IdProducto = (String) params.get("IdProducto");	
					String versionE = (String) params.get("versionE");	
					String numeroCelular = (String) params.get("numeroCelular");	
					String IUM = (String) params.get("IUM"); 
					
					NameValuePair[] parameters = new NameValuePair[5];
					parameters[0] = new NameValuePair("opcion", opcion);
					parameters[1] = new NameValuePair("IdProducto", IdProducto);
					parameters[2] = new NameValuePair("versionE", versionE);
					parameters[3] = new NameValuePair("numeroCelular", numeroCelular);
					parameters[4] = new NameValuePair("IUM", IUM);
					
					clienteHttp.invokeOperation(OPERATION_CODES[TERMINOS_OFERTA_CONSUMO], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponse exitoConsumo(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				AceptaOfertaConsumo aceptaofertaConsumo=new AceptaOfertaConsumo();
				ServerResponse response = new ServerResponse(aceptaofertaConsumo);
				Session session = Session.getInstance(SuiteApp.appContext);
				session.setImporteParcial("");
				isjsonvalueCode=isJsonValueCode.CONSUMO;
				if (SIMULATION) {

                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.EXITOCONSUMOOK_2),response, true);
				}else{

					String numeroCelular = (String) params.get("numeroCelular");	
					String claveCamp = (String) params.get("claveCamp");
					String estatus = (String) params.get("estatus");
					//String IdProductoBuro = (String) params.get("IdProductoBuro");
					String codigoOTP = (String) params.get("codigoOTP");
					String cadenaAutenticacion = (String) params.get("cadenaAutenticacion");
					String seguroObli = (String) params.get("seguroObli");
					String IUM = (String) params.get("IUM");
					//Modif. 63685
					String importePar = (String) params.get("importePar");

					String codigoNIP = (String) params.get("codigoNIP");
					String codigoCVV2 = (String) params.get("codigoCVV2");
					String cveAcceso = (String) params.get("cveAcceso");
					String tarjeta5Dig = (String) params.get("tarjeta5Dig");

					//Modificacion 50870
					if(!Session.getInstance(SuiteApp.appContext).getOfertaDelSimulador()){

						//String cadenaAutenticacion = (String) params.get("cadenaAutenticacion");
						 cadenaAutenticacion = (String) params.get("cadenaAutenticacion");

						NameValuePair[] parameters = new NameValuePair[12];
						parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
						parameters[1] = new NameValuePair("claveCamp", claveCamp);
						parameters[2] = new NameValuePair("estatus", estatus);
						parameters[3] = new NameValuePair("cadenaAutenticacion", cadenaAutenticacion);
						parameters[4] = new NameValuePair("seguroObli", seguroObli);
						parameters[5] = new NameValuePair("IUM", IUM);
						parameters[6] = new NameValuePair("codigoNIP", codigoNIP);
						parameters[7] = new NameValuePair("codigoCVV2", codigoCVV2);
						parameters[8] = new NameValuePair("codigoOTP", codigoOTP);
						parameters[9] = new NameValuePair("cveAcceso", cveAcceso);
						parameters[10] = new NameValuePair("tarjeta5Dig", tarjeta5Dig);
						parameters[11] = new NameValuePair("importePar ", importePar);

						clienteHttp.invokeOperation(OPERATION_CODES[EXITO_OFERTA_CONSUMO], parameters, response, false, true);

					}
					else {

						NameValuePair[] parameters = new NameValuePair[6];
						parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
						parameters[1] = new NameValuePair("claveCamp", claveCamp);
						parameters[2] = new NameValuePair("estatus", estatus);
						parameters[3] = new NameValuePair("cadenaAutenticacion", cadenaAutenticacion);
						parameters[4] = new NameValuePair("seguroObli", seguroObli);
						parameters[5] = new NameValuePair("IUM", IUM);
						parameters[6] = new NameValuePair("codigoNIP", codigoNIP);
						parameters[7] = new NameValuePair("codigoCVV2", codigoCVV2);
						parameters[8] = new NameValuePair("codigoOTP", codigoOTP);
						parameters[9] = new NameValuePair("cveAcceso", cveAcceso);
						parameters[10] = new NameValuePair("tarjeta5Dig", tarjeta5Dig);
						parameters[11] = new NameValuePair("importePar ", importePar);


						clienteHttp.invokeOperation(OPERATION_CODES_CREDIT.get(CONTRATA_ALTERNATIVA_CONSUMO), parameters, response, false, true);

					}
				}
				return response;
			}
			
			private ServerResponse domiciliacionConsumo(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				ServerResponse response = new ServerResponse(new ConsultaPoliza());
				isjsonvalueCode=isJsonValueCode.CONSUMO;
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.DOMICILIACIONCONSUMOOK),response, true);
				}else{

					String IdProducto = (String) params.get("IdProducto");		
					String numeroCelular = (String) params.get("numeroCelular");
					String numCredito = (String) params.get("numCredito");
					String indicad = (String) params.get("indicad");
					String bscPagar = (String) params.get("bscPagar");
					String IUM = (String) params.get("IUM"); 
					
					NameValuePair[] parameters = new NameValuePair[6];
					parameters[0] = new NameValuePair("IdProducto", IdProducto);
					parameters[1] = new NameValuePair("numeroCelular", numeroCelular);
					parameters[2] = new NameValuePair("numCredito", numCredito);
					parameters[3] = new NameValuePair("indicad", indicad);
					parameters[4] = new NameValuePair("bscPagar", bscPagar);
					parameters[5] = new NameValuePair("IUM", IUM);
					
					clienteHttp.invokeOperation(OPERATION_CODES[DOMICILIACION_OFERTA_CONSUMO], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponse contratoConsumo(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				ServerResponse response = new ServerResponse(new ConsultaPoliza());
				isjsonvalueCode=isJsonValueCode.CONSUMO;
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONTRATOCONSUMOOK),response, true);
				}else{

					String IdProducto = (String) params.get("IdProducto");		
					String numeroCelular = (String) params.get("numeroCelular");
					String numCredito = (String) params.get("numCredito");
					String indicad = (String) params.get("indicad");
					String plazoSal = (String) params.get("plazoSal");
					String IUM = (String) params.get("IUM"); 
					
					NameValuePair[] parameters = new NameValuePair[6];
					parameters[0] = new NameValuePair("IdProducto", IdProducto);
					parameters[1] = new NameValuePair("numeroCelular", numeroCelular);
					parameters[2] = new NameValuePair("numCredito", numCredito);
					parameters[3] = new NameValuePair("indicad", indicad);
					parameters[4] = new NameValuePair("plazoSal", plazoSal);
					parameters[5] = new NameValuePair("IUM", IUM);
					
					clienteHttp.invokeOperation(OPERATION_CODES[CONTRATO_OFERTA_CONSUMO], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponse rechazoOfertaConsumo(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				ServerResponse response = new ServerResponse();
				isjsonvalueCode=isJsonValueCode.ONECLICK;
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.RECHAZOOFERTACONSUMOOK),response, true);
				}else{
					String numeroCelular = (String) params.get("numeroCelular");
					String cveCamp = (String) params.get("cveCamp");
					String descripcionMensaje = (String) params.get("descripcionMensaje");
					String folioUG = (String) params.get("folioUG");
					String IUM = (String) params.get("IUM");
								
					NameValuePair[] parameters = new NameValuePair[5];
					parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
					parameters[1] = new NameValuePair("cveCamp", cveCamp);
					parameters[2] = new NameValuePair("descripcionMensaje",descripcionMensaje);
					parameters[3] = new NameValuePair("folioUG",folioUG);
					parameters[4] = new NameValuePair("IUM",IUM);

					clienteHttp.invokeOperation(OPERATION_CODES[RECHAZO_OFERTA_CONSUMO], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponse envioSMSOfertaConsumo(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				ServerResponse response = new ServerResponse();
				isjsonvalueCode=isJsonValueCode.CONSUMO;
				if (SIMULATION) {
                    this.httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.ENVIOSMSOFERTACONSUMOERROR),response, true);
				}else{
					String numeroCelular = (String) params.get("numeroCelular");
					String numCredito = (String) params.get("numCredito");
					String idProducto = (String) params.get("idProducto");
					String estatusContratacion = (String) params.get("estatusContratacion");
					String cuentaVinc = (String) params.get("cuentaVinc");
					String fechaCat = (String) params.get("fechaCat");
					String Importe = (String) params.get("Importe");
					String plazoDes = (String) params.get("plazoDes");
					String pagoMensualFijo = (String) params.get("pagoMensualFijo");
					String email = (String) params.get("email");
					String Cat = (String) params.get("Cat");
					String mensajeA = (String) params.get("mensajeA");
					String IUM = (String) params.get("IUM");
								
					NameValuePair[] parameters = new NameValuePair[13];
					parameters[0] = new NameValuePair("numeroCelular", numeroCelular);
					parameters[1] = new NameValuePair("numCredito", numCredito);
					parameters[2] = new NameValuePair("idProducto", idProducto);
					parameters[3] = new NameValuePair("estatusContratacion",estatusContratacion);
					parameters[4] = new NameValuePair("cuentaVinc", cuentaVinc);
					parameters[5] = new NameValuePair("fechaCat",fechaCat);
					parameters[6] = new NameValuePair("Importe",Importe);
					parameters[7] = new NameValuePair("plazoDes",plazoDes);
					parameters[8] = new NameValuePair("pagoMensualFijo",pagoMensualFijo);
					parameters[9] = new NameValuePair("email",email);
					parameters[10] = new NameValuePair("Cat",Cat);
					parameters[11] = new NameValuePair("mensajeA",mensajeA);
					parameters[12] = new NameValuePair("IUM",IUM);

					clienteHttp.invokeOperation(OPERATION_CODES[SMS_OFERTA_CONSUMO], parameters, response, false, true);
				}
				return response;
			}
			/**
			 * Operacion de Consulta Interbancario
			 * 
			 * @param params
			 *            los parametros a enviar al servidor
			 * @return la respuesta del servidor
			 * @throws IOException
			 *             excepciones de entrada/salida
			 * @throws ParsingException
			 *             excepciones de parseos
			 * @throws URISyntaxException
			 *             excepciones de URIs
			 */

			
			/**
			 * Operacion de Consulta Interbancario
			 * 
			 * @param params
			 *            los parametros a enviar al servidor
			 * @return la respuesta del servidor
			 * @throws IOException
			 *             excepciones de entrada/salida
			 * @throws ParsingException
			 *             excepciones de parseos
			 * @throws URISyntaxException
			 *             excepciones de URIs
			 */
			public ServerResponse consultaInterbancarios(Hashtable<String, ?> params)
					throws IOException, ParsingException, URISyntaxException {
				ConsultaInterbancariaResult data = new ConsultaInterbancariaResult();
				ServerResponse response = new ServerResponse(data);
				
				int i = 0;
				NameValuePair[] parameters = new NameValuePair[9];
				
				parameters[i++] = new NameValuePair("numeroCelular",
						(String) params.get(ServerConstants.NUMERO_TELEFONO));
				parameters[i++] = new NameValuePair(ServerConstants.CVE_ACCESO,
						(String) params.get(ServerConstants.CVE_ACCESO));
				parameters[i++] = new NameValuePair(ServerConstants.CODIGO_NIP,
						(String) params.get(ServerConstants.CODIGO_NIP));
				parameters[i++] = new NameValuePair(ServerConstants.CODIGO_OTP,
						(String) params.get(ServerConstants.CODIGO_OTP));
				parameters[i++] = new NameValuePair(ServerConstants.CODIGO_CVV2,
						(String) params.get(ServerConstants.CODIGO_CVV2));
				parameters[i++] = new NameValuePair(ServerConstants.CADENA_AUTENTICACION,
						(String) params.get(ServerConstants.CADENA_AUTENTICACION));
				parameters[i++] = new NameValuePair(ServerConstants.TARJETA_5DIG,
						(String) params.get(ServerConstants.TARJETA_5DIG));

				parameters[i++] = new NameValuePair(ServerConstants.PERIODO,
						(String) params.get(ServerConstants.PERIODO));
				parameters[i++] = new NameValuePair(ServerConstants.JSON_IUM_ETIQUETA,
						(String) params.get(ServerConstants.IUM_ETIQUETA));
				
				if (SIMULATION) {

                    String responseJson = oReadDocument.read(ConstantsPropertyList.CONINTERBANCARIOSOK_2);
					
					clienteHttp.simulateOperation(
							OPERATION_CODES[OP_CONSULTA_INTERBANCARIOS], parameters,
							response, responseJson, true);
				} else {
					clienteHttp.invokeOperation(OPERATION_CODES[OP_CONSULTA_INTERBANCARIOS],
							parameters, response, false, true);
				}

				return response;
			}

			/**
			 * Petiion Importes tarjeta de crédito
			 * @param params
			 * @return
			 * @throws IOException
			 * @throws ParsingException
			 * @throws URISyntaxException
			 */
			private ServerResponse importesTDC(Hashtable<String, ?> params)	throws IOException, ParsingException, URISyntaxException {
				ImportesTDCData data = new ImportesTDCData();
				ServerResponse response = new ServerResponse(data);
//				isjsonvalueCode=isJsonValueCode.CONSUMO;

				String IUM = (String) params.get(ServerConstants.IUM_ETIQUETA);
				String numeroCelular = (String) params.get("numeroCelular");
				String tipoCuenta = (String) params.get("tipoCuenta");
				String numeroTarjeta = (String) params.get("numeroTarjeta");
				String periodo = (String) params.get("periodo");

				NameValuePair[] parameters = new NameValuePair[5];
				parameters[0] = new NameValuePair("IUM", IUM);
				parameters[1] = new NameValuePair("numeroCelular", numeroCelular);
				parameters[2] = new NameValuePair("tipoCuenta", tipoCuenta);
				parameters[3] = new NameValuePair("numeroTarjeta",numeroTarjeta);
				parameters[4] = new NameValuePair("periodo", periodo);

				if (SIMULATION) {
					//aquí va una simulación impts tdc
				}else{

					clienteHttp.invokeOperation(OPERATION_CODES[OP_CONSULTA_TDC], parameters, response, false, true);
				}
				return response;
			}
			
			/**
			 * Peticion de alta para retiro sin tarjeta
			 * @param params
			 * @return
			 * @throws IOException
			 * @throws ParsingException
			 * @throws URISyntaxException
			 */
			private ServerResponse retiroSinTarjeta(Hashtable<String, ?> params)	throws IOException, ParsingException, URISyntaxException {


				final RetiroSinTarjetaResult data = new RetiroSinTarjetaResult();

				final ServerResponse response = new ServerResponse(data);


				final String numeroTelefono = (String) params.get("numeroTelefono");

				final String cveAcceso = (String)  params.get(ServerConstants.CVE_ACCESO);
				final String codigoNIP = (String)  params.get(ServerConstants.CODIGO_NIP);
				final String codigoCVV2 = (String)  params.get(ServerConstants.CODIGO_CVV2);

				final String cadenaAutenticacion= (String)  params.get(ServerConstants.CADENA_AUTENTICACION);
				final String IUM = (String) params.get(ServerConstants.JSON_IUM_ETIQUETA);
				final String codigoOTP = (String) params.get(ServerConstants.CODIGO_OTP);

				final String tipoCuenta = (String) params.get("tipoCuenta");
				final String numeroCuenta = (String) params.get("numeroCuenta");
				final String cuentaDestino = (String) params.get("cuentaDestino");


				final String perfilCliente = (String) params.get(ServerConstants.PERFIL_CLIENTE);
				final String tarjeta5Dig = (String) params.get(ServerConstants.TARJETA_5DIG);
				final String importe = (String) params.get("importe");
				final String concepto = (String) params.get("concepto");


				final NameValuePair[] parameters = new NameValuePair[14];

				parameters[0] = new NameValuePair("IUM", IUM);
				parameters[1] = new NameValuePair("numeroCelular", numeroTelefono);
				parameters[2] = new NameValuePair("cveAcceso", cveAcceso);
				parameters[3] = new NameValuePair("codigoNIP", codigoNIP);
				parameters[4] = new NameValuePair("codigoCVV2", codigoCVV2);
				parameters[5] = new NameValuePair("cadenaAutenticacion", cadenaAutenticacion);
				parameters[6] = new NameValuePair("codigoOTP", codigoOTP);
				parameters[7] = new NameValuePair("tipoCuenta", tipoCuenta);
				parameters[8] = new NameValuePair("numeroCuenta", numeroCuenta);
				parameters[9] = new NameValuePair("cuentaDestino", cuentaDestino);

				parameters[10] = new NameValuePair("perfilCliente", perfilCliente);
				parameters[11] = new NameValuePair("tarjeta5Dig", tarjeta5Dig);
				parameters[12] = new NameValuePair("importe", importe);
				parameters[13] = new NameValuePair("concepto", concepto);




				if (SIMULATION) {
					//the good one
					if(Server.ALLOW_LOG) Log.d("RetiroSinTarjeta","Entra en la peticion");
					//Boolean errorComunicaciones = false;
					//Boolean errorContrato = false;
					//errorComunicaciones=true;

					if(errorComunicacionesRetSinTarjeta){

						/*
						errorComunicaciones=false;
						errorContrato=true;
						*/

						// error
						clienteHttp.simulateOperation(OPERATION_CODES[OP_RETIRO_SIN_TAR], parameters, response, "{\"estado\":\"ERROR\",\"codigoMensaje\":\"MBANK00090\",\"mensajeInformativo\":\"ERROR DE COMUNICACIONES\"}", true);

					}else if (errorMontosRetiroSinTarjeta){

						clienteHttp.simulateOperation(OPERATION_CODES[OP_RETIRO_SIN_TAR], parameters, response, "{\"estado\":\"ERROR\",\"codigoMensaje\":\"CNE0234\",\"mensajeInformativo\":\"IMPORTE DE TRANSACCION EXCEDE EL LIMITE DEFINIDO PARA ESTA OPERACION\"}", true);

					}

					else{

						final String result = "{\"estado\": \"OK\", \"mensajeInformativo\": \"Operación exitosa\", \"claveRst4\": \"3684\", \"importe\": \"10000\", \"fechaAlta\": \"20012015\", \"vigencia\": \"23012015\", \"horaOperacion\": \"19:35\" , \"folio\": \"0001340005\" , \"claveRetiro\": \"162345678901\"}";
//						String result = "<meta http-equiv=\"Content-type\" content=\"text/html; charset=iso-8859-1\">{\"estado\":\"OK\",\"mensajeInformativo\":\"\",\"datos\":[{\"fechaPago\":\"2013-01-09\",\"pagoMinimo\":\"442771\",\"pagoNoGeneraInt\":\"7767921\",\"saldoAlCorte\":\"7767921\"}]}";
//						Integer indice = result.indexOf("{");
//						String res = result.substring(indice);
						//String res = result.replaceAll("\\<.*?>", "");

						if(Server.ALLOW_LOG) Log.d("debugRespuesta", result);
						//Log.d("debugRespuestaParseada", res);

						clienteHttp.simulateOperation(OPERATION_CODES[OP_RETIRO_SIN_TAR], parameters, response, result, true);

						//errorContrato = true;
					}
				}else{

					clienteHttp.invokeOperation(OPERATION_CODES[OP_RETIRO_SIN_TAR], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponse consultaRetiroSinTarjeta(Hashtable<String, ?> params)	throws IOException, ParsingException, URISyntaxException {
				ConsultaRetiroSinTarjetaData data = new ConsultaRetiroSinTarjetaData();
				
			
				ServerResponse response = new ServerResponse(data);
				String IUM = (String) params.get(ServerConstants.IUM_ETIQUETA);
				String numeroTelefono = (String) params.get(ServerConstants.NUMERO_TELEFONO);
				String estatusOperacion = (String) params.get(Server.OPERATION_CODE_PARAMETER);

				if (Server.ALLOW_LOG)Log.d("Datos de la consulta", "IUM:"+IUM+"\nNumero Telefono: "+numeroTelefono+"\nEstatus de la Operacion: "+estatusOperacion);
							
				NameValuePair[] parameters = new NameValuePair[3];
				parameters[0] = new NameValuePair("IUM", IUM);
				parameters[1] = new NameValuePair("numeroTelefono", numeroTelefono);
				parameters[2] = new NameValuePair("estatusOperacion", estatusOperacion);
				
				if (SIMULATION) {
					Boolean errorComunicacionesSR=false;//simular un error o no
					//the good one
					if(Server.ALLOW_LOG) Log.d("ConsultaRetSinTarjeta", "Entra en la peticion");


					if(errorComunicacionesSR){

						// error
						clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_SIN_TARJETA], parameters, response, "{\"estado\":\"ERROR\",\"codigoMensaje\":\"MBANK00090\",\"mensajeInformativo\":\"ERROR DE COMUNICACIONES\"}", true);

					}
					else if (estatusOperacion.equalsIgnoreCase(Constants.TipoEstatusRST.VIGENTE.codigo)){

						String result = "{\"estado\":\"OK\",\"mensajeInformativo\":\"OperacionExitosa\",\"retiro\":[{\"folioOperacion\":\"0080087008\",\"importe\":\"10000\",\"cuentaCargo\":\"00740001002800120995\",\"estatusOperacion\":\"VG\",\"numeroCelular\":\"5521101113\",\"companiaCelular\":\"TELCEL\",\"beneficiario\":\"LUIS\",\"concepto\":\"PRUEBA\",\"fechaOperacion\":\"20032015\",\"fechaExpiracion\":\"27032015\",\"horaOperacionExp\":\"12:45\",\"codigoSeguridad\":\"0126\",\"indicador\":\"S\"},{\"folioOperacion\":\"0040132007\",\"importe\":\"10000\",\"cuentaCargo\":\"00740001002800120995\",\"estatusOperacion\":\"VG\",\"numeroCelular\":\"5521101113\",\"companiaCelular\":\"TELCEL\",\"beneficiario\":\"LUIS\",\"concepto\":\"PRUEBAS\",\"fechaOperacion\":\"17032015\",\"fechaExpiracion\":\"24032015\",\"horaOperacionExp\":\"16:58\",\"codigoSeguridad\":\"0246\",\"indicador\":\"S\"},{\"folioOperacion\":\"0040132031\",\"importe\":\"10000\",\"cuentaCargo\":\"00740001002800120995\",\"estatusOperacion\":\"VG\",\"numeroCelular\":\"5521101113\",\"companiaCelular\":\"TELCEL\",\"beneficiario\":\"LUIS\",\"concepto\":\"PRUEBA\",\"fechaOperacion\":\"17032015\",\"fechaExpiracion\":\"22032015\",\"horaOperacionExp\":\"18:58\",\"codigoSeguridad\":\"0249\",\"indicador\":\"S\"}]}";
						String res = result.replaceAll("\\<.*?>", "");

						if(Server.ALLOW_LOG) Log.d("debugRespuesta", result);
						if(Server.ALLOW_LOG) Log.d("debugRespuestaParseada", res);

						clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_SIN_TARJETA], parameters, response, res, true);


					}else if(estatusOperacion.equalsIgnoreCase(Constants.TipoEstatusRST.VENCIDO.codigo)){
						String result = "{\"estado\":\"OK\",\"mensajeInformativo\":\"OperacionExitosa\",\"retiro\":[{\"folioOperacion\":\"0080087008\",\"importe\":\"10000\",\"cuentaCargo\":\"00740001002800120995\",\"estatusOperacion\":\"VG\",\"numeroCelular\":\"5521101113\",\"companiaCelular\":\"TELCEL\",\"beneficiario\":\"PEDRO\",\"concepto\":\"PRUEBA\",\"fechaOperacion\":\"20032015\",\"fechaExpiracion\":\"27032015\",\"horaOperacionExp\":\"12:45\",\"codigoSeguridad\":\"0126\",\"indicador\":\"S\"},{\"folioOperacion\":\"0040132007\",\"importe\":\"10000\",\"cuentaCargo\":\"00740001002800120995\",\"estatusOperacion\":\"VG\",\"numeroCelular\":\"5521101113\",\"companiaCelular\":\"TELCEL\",\"beneficiario\":\"ALBERTO\",\"concepto\":\"PRUEBAS\",\"fechaOperacion\":\"17032015\",\"fechaExpiracion\":\"24032015\",\"horaOperacionExp\":\"16:58\",\"codigoSeguridad\":\"0246\",\"indicador\":\"S\"},{\"folioOperacion\":\"0040132031\",\"importe\":\"10000\",\"cuentaCargo\":\"00740001002800120995\",\"estatusOperacion\":\"VG\",\"numeroCelular\":\"5521101113\",\"companiaCelular\":\"TELCEL\",\"beneficiario\":\"LUIS\",\"concepto\":\"PRUEBA\",\"fechaOperacion\":\"17032015\",\"fechaExpiracion\":\"22032015\",\"horaOperacionExp\":\"18:58\",\"codigoSeguridad\":\"0249\",\"indicador\":\"S\"}]}";
						String res = result.replaceAll("\\<.*?>", "");

						Log.d("debugRespuesta", result);
						Log.d("debugRespuestaParseada", res);

						clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_SIN_TARJETA], parameters, response, res, true);
					}else if(estatusOperacion.equalsIgnoreCase(Constants.TipoEstatusRST.CANCELADO.codigo)){
						String result = "{\"estado\":\"OK\",\"mensajeInformativo\":\"OperacionExitosa\",\"retiro\":[{\"folioOperacion\":\"0080087008\",\"importe\":\"10000\",\"cuentaCargo\":\"00740001002800120995\",\"estatusOperacion\":\"VG\",\"numeroCelular\":\"5521101113\",\"companiaCelular\":\"TELCEL\",\"beneficiario\":\"JORGE\",\"concepto\":\"PRUEBA\",\"fechaOperacion\":\"20032015\",\"fechaExpiracion\":\"27032015\",\"horaOperacionExp\":\"12:45\",\"codigoSeguridad\":\"0126\",\"indicador\":\"S\"},{\"folioOperacion\":\"0040132007\",\"importe\":\"10000\",\"cuentaCargo\":\"00740001002800120995\",\"estatusOperacion\":\"VG\",\"numeroCelular\":\"5521101113\",\"companiaCelular\":\"TELCEL\",\"beneficiario\":\"MARIA\",\"concepto\":\"PRUEBAS\",\"fechaOperacion\":\"17032015\",\"fechaExpiracion\":\"24032015\",\"horaOperacionExp\":\"16:58\",\"codigoSeguridad\":\"0246\",\"indicador\":\"S\"},{\"folioOperacion\":\"0040132031\",\"importe\":\"10000\",\"cuentaCargo\":\"00740001002800120995\",\"estatusOperacion\":\"VG\",\"numeroCelular\":\"5521101113\",\"companiaCelular\":\"TELCEL\",\"beneficiario\":\"NICO\",\"concepto\":\"PRUEBA\",\"fechaOperacion\":\"17032015\",\"fechaExpiracion\":\"22032015\",\"horaOperacionExp\":\"18:58\",\"codigoSeguridad\":\"0249\",\"indicador\":\"S\"}]}";
						String res = result.replaceAll("\\<.*?>", "");

						Log.d("debugRespuesta", result);
						Log.d("debugRespuestaParseada", res);
						clienteHttp.simulateOperation(OPERATION_CODES[CONSULTA_SIN_TARJETA], parameters, response, res, true);
					}
				}else{
					clienteHttp.invokeOperation(OPERATION_CODES[CONSULTA_SIN_TARJETA], parameters, response, false, true);
				}
				return response;
			}

			
			
			private ServerResponse clave12DigitosRetiroSinTarjeta(Hashtable<String, ?> params)	throws IOException, ParsingException, URISyntaxException {

				final Clave12DigitosResult data = new Clave12DigitosResult();

				final ServerResponse response = new ServerResponse(data);

				final String IUM = (String) params.get(ServerConstants.IUM_ETIQUETA);
				final String numeroTelefono = (String) params.get(ServerConstants.NUMERO_TELEFONO);
				final String folioOperacion = (String) params.get(Server.FOLIO_OPERACION_RETIRO_SIN_TAR);

				if (Server.ALLOW_LOG) Log.d("Datos de la consulta", "IUM:"+IUM+"\nNumero Telefono: "+numeroTelefono+"\nFolio de Operación: "+folioOperacion);

				final NameValuePair[] parameters = new NameValuePair[3];
				parameters[0] = new NameValuePair("IUM", IUM);
				parameters[1] = new NameValuePair("numeroTelefono", numeroTelefono);
				parameters[2] = new NameValuePair("folioOperacion", folioOperacion);

				if (SIMULATION) {
					Boolean errorComunicacionesSR=false;//simular un error o no
					//the good one
					Log.d("Consulta 12 Digitos Retiro Sin Tarjeta - SIMULATION >> ","Entra en la peticion");

					if (folioOperacion.equalsIgnoreCase("0040132031")) {
						errorComunicacionesSR = true;
					}

					if(errorComunicacionesSR){

						// error
						clienteHttp.simulateOperation(OPERATION_CODES[OP_RETIRO_SIN_TAR_12_DIGITOS], parameters, response, "{\"estado\":\"ERROR\",\"codigoMensaje\":\"MBANK00090\",\"mensajeInformativo\":\"ERROR DE COMUNICACIONES\"}", true);

					}
					else {

						String result = "";

						if (folioOperacion.equalsIgnoreCase("0080087008")) {
							result = "{\"estado\":\"OK\",\"mensajeInformativo\":\"Operación Exitosa\",\"claveRetiro\":\"123456789012\",\"folioCanal\":\"0080087008\"}";
						} else if (folioOperacion.equalsIgnoreCase("0040132007")) {
							result = "{\"estado\":\"OK\",\"mensajeInformativo\":\"Operación Exitosa\",\"claveRetiro\":\"000000000000\",\"folioCanal\":\"0040132007\"}";
						}

						final String res = result.replaceAll("\\<.*?>", "");

						Log.d("debugRespuesta", result);
						Log.d("debugRespuestaParseada", res);

						clienteHttp.simulateOperation(OPERATION_CODES[OP_RETIRO_SIN_TAR_12_DIGITOS], parameters, response, res, true);

						//errorContrato = true;
					}
				}else{

					clienteHttp.invokeOperation(OPERATION_CODES[OP_RETIRO_SIN_TAR_12_DIGITOS], parameters, response, false, true);
				}
				return response;
			}

	private ServerResponse SincronizacionExportacionST(Hashtable<String, ?> params)	throws IOException, ParsingException, URISyntaxException {
		SincroExportSTData data = new SincroExportSTData();

		ServerResponse response = new ServerResponse(data);

		NameValuePair[] parameters = new NameValuePair[8];

		parameters[0] = new NameValuePair("IUM", (String) params.get(ServerConstants.JSON_IUM_ETIQUETA));
		parameters[1] = new NameValuePair("numeroTelefono", (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[2] = new NameValuePair("numeroTarjeta", (String) params.get(ServerConstants.NUMERO_TARJETA));
		parameters[3] = new NameValuePair("tipoSolicitud", (String) params.get(ServerConstants.TIPO_SOLICITUD));
		parameters[4] = new NameValuePair("OTP1", (String) params.get(ServerConstants.OTP1));
		parameters[5] = new NameValuePair("OTP2", (String) params.get(ServerConstants.OTP2));
		parameters[6] = new NameValuePair("nombreToken", (String) params.get(ServerConstants.NOMBRE_TOKEN));
		parameters[7] = new NameValuePair("versionApp", (String) params.get(ServerConstants.VERSION_APP));

		if (SIMULATION) {
			//aquí va una simulacion
		}else{

			clienteHttp.invokeOperation(OPERATION_CODES[OP_SINC_EXP_TOKEN], parameters, response, false, true);
		}
		return response;
	}

    private void obtenerValores(){
        CURRENT_ACCOUNT_SET = oReadDocument.read(ConstantsPropertyList.ACCOUNTS_ALL_ACCOUNTS);
        CURRENT_PROFILE_STRING = oReadDocument.read(ConstantsPropertyList.PROFILE_ADVANCED_OCRA);
        CURRENT_FAST_PAYMENT_SET = oReadDocument.read(ConstantsPropertyList.NO_FAST_PAYMENT);
        LISTA_OPERACIONES = oReadDocument.read(ConstantsPropertyList.OPERACIONES_CAT_AU);
    }

	/**
	 * INTEGRACION BBVACredit
	 * perform the network operation identifier by operationId with the
	 * parameters passed
	 *
	 * @param operationId the identifier of the operation
	 * @param params Hashtable with the parameters as key-value pairs.
	 * @return the response from the server
	 * @throws IOException
	 * @throws ParsingExceptionCredit
	 * @throws URISyntaxException
	 * @throws ParsingException
	 */
	public ServerResponseCredit doNetworkOperationCredit(String operationId, Hashtable<String, ?> params) throws IOException, ParsingExceptionCredit, JSONException, ParsingException, URISyntaxException {
		if (Server.ALLOW_LOG) System.out.println("doNetworkOperation - operationId = " + operationId);
		ServerResponseCredit response = null;
		long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);

		Integer provider = (Integer) OPERATIONS_PROVIDER_CREDIT.get(operationId);
		if (provider != null) {
			PROVIDER = provider.intValue();
		}

		if (LOGIN_OPERACION.equals(operationId)) {
			if (Server.ALLOW_LOG) System.out.println("Operacion de login");
			response = loginCredit(params);
		}else if(CALCULO_OPERACION.equals(operationId)){
			if (Server.ALLOW_LOG) System.out.println("Operacion de posicion global");
			response = calculo(params);
		}else if(CONSULTA_CORREO_OPERACION.equals(operationId)){
			if (Server.ALLOW_LOG) System.out.println("Operacion de consulta de correo");
			response = consultaCorreo(params);
		}else if(ENVIO_CORREO_OPERACION.equals(operationId)){
			if (Server.ALLOW_LOG) System.out.println("Operacion de envio de correo");
			response = envioCorreo(params);
		}else if(CONSULTA_ALTERNATIVAS.equals(operationId)){
			if (Server.ALLOW_LOG) Log.e("cve Operation",operationId);
			if (Server.ALLOW_LOG) System.out.println("Operacion de consulta alternativas");
			response = consultaAlternativas(params);
		}else if(CONSULTA_TDC.equals(operationId)){
			if (Server.ALLOW_LOG) System.out.println("Operacion de consulta TDC");
			response = consultaTDC(params);

		}else if(DETALLE_ALTERNATIVA.equals(operationId)){
			if (Server.ALLOW_LOG) System.out.println("Operacion detalle alternativa");
			response = consultaDetalleAlternativa(params);
		}else if(CALCULO_ALTERNATIVAS_OPERACION.equals(operationId))
		{
			if(Server.ALLOW_LOG) Log.e("Invoking ","ok");
			response=guardarEliminarSimulacion(params);
		}else if(CONTRATA_ALTERNATIVA_CONSUMO.equals(operationId)){
			if (Server.ALLOW_LOG) System.out.println("Operacion contrata alternativa");
			response = contrataAlternativaConsumo(params);
		}

		if (sleep > 0) {
			//#debug debug
			if (Server.ALLOW_LOG) System.out.println("Sleeping extra time: " + sleep);
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				//e.printStackTrace();
			}
		}

		if (Server.ALLOW_LOG) System.out.println("End of doNetworkOperation");
		return response;
	}

	/**
	 * inicia metodos BBVACredit
	 *
	 */

	/**
	 * Realiza la operacion de login.
	 *
	 * @param params un mapa de parametros para la operacion
	 * @return la respuesta del servidor
	 * @throws IOException            control de excepciones de entrada/salida
	 * @throws ParsingExceptionCredit control de excepciones de parseo
	 * @throws JSONException
	 * @throws URISyntaxException
	 * @throws ParsingException
	 */
	private ServerResponseCredit loginCredit(Hashtable<String, ?> params) throws IOException, ParsingExceptionCredit, JSONException, ParsingException, URISyntaxException {

		// Se devuelve los distintos estados de respuesta en funcion de la version del sistema - VM
		LoginData data = new LoginData();
		ServerResponseCredit response = new ServerResponseCredit(data);

		if (SIMULATION) {

			if (SIMULACION_RESPUESTA_ERROR_LOGIN) {
				this.httpClientCredit.simulateOperation("ESERROR*COCNE0011*MECLAVE DE ACCESO INCORRECTA", response);

			} else {

				String versionVM = (String) params.get(ServerConstantsCredit.VERSION_VM);
				String versionAU = (String) params.get(ServerConstantsCredit.VERSION_AU);
				String mensaje = "";

				if (versionVM.equals(ConstantsCredit.VM500)) {
					mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM500);
				} else if (versionVM.equals(ConstantsCredit.VM300)) {
					mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM300);
				} else if (versionVM.equals(ConstantsCredit.VM350)) {
					mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM350);
				} else if (versionVM.equals(ConstantsCredit.VM411)) {
					if (versionAU.equals(ZERO)) {
						mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM411_0);
					} else if (((Boolean) params.get(ServerConstantsCredit.VERSION_HC))) {
						mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM411_HC);
					} else {
						mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM411);
					}
				} else if (versionVM.equals(ConstantsCredit.VM910)) {
					mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.LOGIN_VM910);
				}

				logNetworkOperation("loginCredit", params.toString(), mensaje);
				this.httpClientCredit.simulateOperation(mensaje, response);
			}

		} else {
			NameValuePair[] parameters = new NameValuePair[14];
			parameters[0] = new NameValuePair(ServerConstantsCredit.USERNAME_PARAM, (String) params.get(ServerConstantsCredit.USERNAME_PARAM));
			parameters[1] = new NameValuePair(ServerConstantsCredit.PASSWORD_PARAM, (String) params.get(ServerConstantsCredit.PASSWORD_PARAM));
			parameters[2] = new NameValuePair(ServerConstantsCredit.IUM_ETIQUETA, (String) params.get(ServerConstantsCredit.IUM_ETIQUETA));
			parameters[3] = new NameValuePair(ServerConstantsCredit.VERSION_VM, (String) params.get(ServerConstantsCredit.VERSION_VM));
			parameters[4] = new NameValuePair(ServerConstantsCredit.VERSION_C1, (String) params.get(ServerConstantsCredit.VERSION_C1));
			parameters[5] = new NameValuePair(ServerConstantsCredit.VERSION_C4, (String) params.get(ServerConstantsCredit.VERSION_C4));
			parameters[6] = new NameValuePair(ServerConstantsCredit.VERSION_C5, (String) params.get(ServerConstantsCredit.VERSION_C5));
			parameters[7] = new NameValuePair(ServerConstantsCredit.VERSION_C8, (String) params.get(ServerConstantsCredit.VERSION_C8));
			parameters[8] = new NameValuePair(ServerConstantsCredit.MARCA_MODELO, (String) params.get(ServerConstantsCredit.MARCA_MODELO));
			parameters[9] = new NameValuePair(ServerConstantsCredit.VERSION_TA, (String) params.get(ServerConstantsCredit.VERSION_TA));
			parameters[10] = new NameValuePair(ServerConstantsCredit.VERSION_DM, (String) params.get(ServerConstantsCredit.VERSION_DM));
			parameters[11] = new NameValuePair(ServerConstantsCredit.VERSION_SV, (String) params.get(ServerConstantsCredit.VERSION_SV));
			parameters[12] = new NameValuePair(ServerConstantsCredit.VERSION_MS, (String) params.get(ServerConstantsCredit.VERSION_MS));
			parameters[13] = new NameValuePair(ServerConstantsCredit.VERSION_AU, (String) params.get(ServerConstantsCredit.VERSION_AU));
//					parameters[13] = new NameValuePair(ServerConstants.VERSION_VM, Constants.APPLICATION_VERSION);

			this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(LOGIN_OPERACION), parameters, response);
		}
		return response;

	}

	/**
	 * Realiza la operacion de consulta TDC.
	 *
	 * @param params un mapa de parametros para la operacion
	 * @return la respuesta del servidor
	 * @throws IOException            control de excepciones de entrada/salida
	 * @throws ParsingExceptionCredit control de excepciones de parseo
	 * @throws JSONException
	 * @throws URISyntaxException
	 * @throws ParsingException
	 */
	private ServerResponseCredit consultaTDC(Hashtable<String, ?> params) throws IOException, ParsingExceptionCredit, JSONException, ParsingException, URISyntaxException {

		// Se devuelve los distintos estados de respuesta en funcion de la version del sistema - VM
		ConsultaDatosTDCData data = new ConsultaDatosTDCData();
		ServerResponseCredit response = new ServerResponseCredit(data);

		if (SIMULATION) {

			if (SIMULACION_RESPUESTA_ERROR_CONSULTA_TDC) {
				String mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CONSULTA_TDC_ERROR);

				logNetworkOperation("consultaTDC", params.toString(), mensaje);
				this.httpClientCredit.simulateOperation(mensaje, response);

			} else {

				String mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CONSULTA_TDC);

				if(Server.ALLOW_LOG) Log.d("Consulta TDC request", params.toString());
				Tools.trazaTexto("Consulta TDC response", mensaje);

				this.httpClientCredit.simulateOperation(mensaje, response);
						/*
						NameValuePair[] parameters = new NameValuePair[6];
						parameters[0] = new NameValuePair(ServerConstantsCredit.USERNAME_PARAM, (String) params.get(ServerConstantsCredit.USERNAME_PARAM));
						parameters[1] = new NameValuePair(ServerConstantsCredit.PASSWORD_PARAM, (String) params.get(ServerConstantsCredit.PASSWORD_PARAM));
						parameters[2] = new NameValuePair(ServerConstantsCredit.IUM_ETIQUETA, (String) params.get(ServerConstantsCredit.IUM_ETIQUETA));
						parameters[3] = new NameValuePair(ServerConstantsCredit.TP, (String) params.get(ServerConstantsCredit.TP));
						parameters[4] = new NameValuePair(ServerConstantsCredit.AS, (String) params.get(ServerConstantsCredit.AS));
						if(params.containsKey(ServerConstantsCredit.PE))
							parameters[5] = new NameValuePair(ServerConstantsCredit.PE, (String) params.get(ServerConstantsCredit.PE));

						this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CONSULTA_TDC), parameters, response);
				        */

			}

		} else {
			suitebancomer.aplicaciones.bbvacredit.common.NameValuePair[] parameters = new suitebancomer.aplicaciones.bbvacredit.common.NameValuePair[6];
			parameters[0] = new suitebancomer.aplicaciones.bbvacredit.common.NameValuePair(ServerConstantsCredit.USERNAME_PARAM, (String) params.get(ServerConstantsCredit.USERNAME_PARAM));
			parameters[1] = new suitebancomer.aplicaciones.bbvacredit.common.NameValuePair(ServerConstantsCredit.PASSWORD_PARAM, (String) params.get(ServerConstantsCredit.PASSWORD_PARAM));
			parameters[2] = new suitebancomer.aplicaciones.bbvacredit.common.NameValuePair(ServerConstantsCredit.IUM_ETIQUETA, (String) params.get(ServerConstantsCredit.IUM_ETIQUETA));
			parameters[3] = new suitebancomer.aplicaciones.bbvacredit.common.NameValuePair(ServerConstantsCredit.TP, (String) params.get(ServerConstantsCredit.TP));
			parameters[4] = new suitebancomer.aplicaciones.bbvacredit.common.NameValuePair(ServerConstantsCredit.AS, (String) params.get(ServerConstantsCredit.AS));
			if (params.containsKey(ServerConstantsCredit.PE))
				parameters[5] = new suitebancomer.aplicaciones.bbvacredit.common.NameValuePair(ServerConstantsCredit.PE, (String) params.get(ServerConstantsCredit.PE));

			this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CONSULTA_TDC), parameters, response);
		}
		return response;

	}

	/**
	 * Realiza la operacion de calculo.
	 *
	 * @param params un mapa de parametros para la operacion
	 * @return la respuesta del servidor
	 * @throws IOException            control de excepciones de entrada/salida
	 * @throws ParsingExceptionCredit control de excepciones de parseo
	 * @throws JSONException
	 * @throws URISyntaxException
	 * @throws ParsingException
	 */
	private ServerResponseCredit calculo(Hashtable<String, ?> params) throws IOException, ParsingExceptionCredit, JSONException, ParsingException, URISyntaxException {
		CalculoData data = new CalculoData();
		ServerResponseCredit response = new ServerResponseCredit(data);

		if (SIMULATION) {
			String mensaje = "";
			if (SIMULACION_RESPUESTA_ERROR_CALCULO) {
				mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CALCULO_ERROR);
				this.httpClientCredit.simulateOperation(mensaje, response, true);
			} else {

				//Creditos ofertados 6
				mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CALCULO);

				logNetworkOperation("calculo", params.toString(), mensaje);
				this.httpClientCredit.simulateOperation(mensaje, response, true);
			}
		} else {
			JSONObject json = new JSONObject();

			// Se distingue entre los par�metros que vayamos a meter ? para diferenciar las peticiones de liquidaci�n y contrataci�n; por tanto no meter par�metros vacios
			if (params.containsKey(ServerConstantsCredit.CVESUBP_PARAM) || params.containsKey(ServerConstantsCredit.CVEPROD_PARAM) || params.containsKey(ServerConstantsCredit.CVEPLAZO_PARAM)) {
				// Contrataci�n
				json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CALCULO_OPERACION));
				json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));
				json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
				json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));
				json.put(ServerConstantsCredit.CVEPROD_PARAM, params.get(ServerConstantsCredit.CVEPROD_PARAM));
				json.put(ServerConstantsCredit.CVESUBP_PARAM, params.get(ServerConstantsCredit.CVESUBP_PARAM));
				json.put(ServerConstantsCredit.CVEPLAZO_PARAM, params.get(ServerConstantsCredit.CVEPLAZO_PARAM));
				json.put(ServerConstantsCredit.MON_DESE_PARAM, params.get(ServerConstantsCredit.MON_DESE_PARAM));
				json.put(ServerConstantsCredit.TIPO_OP_PARAM, params.get(ServerConstantsCredit.TIPO_OP_PARAM));

			} else {
				// Liquidacion
				json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CALCULO_OPERACION));
				json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));
				json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
				json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));
				json.put(ServerConstantsCredit.CONTRATO_PARAM, params.get(ServerConstantsCredit.CONTRATO_PARAM));
				json.put(ServerConstantsCredit.PAGO_MENSUAL_PARAM, params.get(ServerConstantsCredit.PAGO_MENSUAL_PARAM));
				json.put(ServerConstantsCredit.TIPO_OP_PARAM, params.get(ServerConstantsCredit.TIPO_OP_PARAM));

			}

			this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CALCULO_OPERACION), json, response);
		}

		return response;
	}

	/**
	 * Realiza la operacion de consulta de correo.
	 *
	 * @param params un mapa de parametros para la operacion
	 * @return la respuesta del servidor
	 * @throws IOException            control de excepciones de entrada/salida
	 * @throws ParsingExceptionCredit control de excepciones de parseo
	 * @throws JSONException
	 * @throws URISyntaxException
	 * @throws ParsingException
	 */
	public ServerResponseCredit consultaCorreo(Hashtable<String, ?> params) throws IOException, ParsingExceptionCredit, JSONException, ParsingException, URISyntaxException {
		ConsultaCorreoData data = new ConsultaCorreoData();
		ServerResponseCredit response = new ServerResponseCredit(data);

		if (SIMULATION) {
			String mensaje = "";
			if (SIMULACION_RESPUESTA_ERROR_CONSULTA_CORREO) {
				mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CONSULTA_CORREO_ERROR);
			} else {
				mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CONSULTA_CORREO);
			}

			logNetworkOperation("consultaCorreo", params.toString(), mensaje);
			this.httpClientCredit.simulateOperation(mensaje, response, true);

		} else {
			JSONObject json = new JSONObject();

			json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CONSULTA_CORREO_OPERACION));
			json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));
			json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
			json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));

			this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CONSULTA_CORREO_OPERACION), json, response);
		}

		return response;
	}

	/**
	 * Realiza la operacion de envio de correo.
	 *
	 * @param params un mapa de parametros para la operacion
	 * @return la respuesta del servidor
	 * @throws IOException            control de excepciones de entrada/salida
	 * @throws ParsingExceptionCredit control de excepciones de parseo
	 * @throws JSONException
	 * @throws URISyntaxException
	 * @throws ParsingException
	 */
	private ServerResponseCredit envioCorreo(Hashtable<String, ?> params) throws IOException, ParsingExceptionCredit, JSONException, ParsingException, URISyntaxException {
		EnvioCorreoData data = new EnvioCorreoData();
		ServerResponseCredit response = new ServerResponseCredit(data);

		if (SIMULATION) {
			String mensaje = "";
			if (SIMULACION_RESPUESTA_ERROR_ENVIO_CORREO) {
				//mensaje = "{\"estado\":\"ERROR\"}";
				mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.ENVIO_CORREO_ERROR);
			} else {
				mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.ENVIO_CORREO);
			}

			logNetworkOperation("envioCorreo", params.toString(), mensaje);
			this.httpClientCredit.simulateOperation(mensaje, response, true);
		} else {
			JSONObject json = new JSONObject();

			json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.ENVIO_CORREO_OPERACION));
			json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));
			json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
			json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));

			String indicador = (String) params.get(ServerConstantsCredit.IND_CORREO);

			if (indicador.equals("C")) {
				json.put(ServerConstantsCredit.SCN, params.get(ServerConstantsCredit.SCN));
				json.put(ServerConstantsCredit.CN_IMPORTE, params.get(ServerConstantsCredit.CN_IMPORTE));
				json.put(ServerConstantsCredit.CN_TASA_ANUAL, params.get(ServerConstantsCredit.CN_TASA_ANUAL));
				json.put(ServerConstantsCredit.CN_PLAZO, params.get(ServerConstantsCredit.CN_PLAZO));
				json.put(ServerConstantsCredit.CN_PAGO_FIJO_MENSUAL, params.get(ServerConstantsCredit.CN_PAGO_FIJO_MENSUAL));

				json.put(ServerConstantsCredit.SPPI, params.get(ServerConstantsCredit.SPPI));
				json.put(ServerConstantsCredit.PP_IMPORTE, params.get(ServerConstantsCredit.PP_IMPORTE));
				json.put(ServerConstantsCredit.PP_TASA_ANUAL, params.get(ServerConstantsCredit.PP_TASA_ANUAL));
				json.put(ServerConstantsCredit.PP_PLAZO, params.get(ServerConstantsCredit.PP_PLAZO));
				json.put(ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL, params.get(ServerConstantsCredit.PP_PAGO_FIJO_MENSUAL));

				json.put(ServerConstantsCredit.STDC, params.get(ServerConstantsCredit.STDC));
				json.put(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_AUTORIZADO, params.get(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_AUTORIZADO));
				json.put(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_SIMULADO, params.get(ServerConstantsCredit.TC_LIMITE_DE_CREDITO_SIMULADO));

				json.put(ServerConstantsCredit.SILDC, params.get(ServerConstantsCredit.SILDC));
				json.put(ServerConstantsCredit.IL_TARJETA_DE_CREDITO, params.get(ServerConstantsCredit.IL_TARJETA_DE_CREDITO));
				json.put(ServerConstantsCredit.IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA, params.get(ServerConstantsCredit.IL_LINEA_DE_CREDITO_ADICIONAL_AUTORIZADA));
				json.put(ServerConstantsCredit.IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA, params.get(ServerConstantsCredit.IL_NUEVA_LINEA_DE_CREDITO_SOLICITADA));

				json.put(ServerConstantsCredit.SCA, params.get(ServerConstantsCredit.SCA));
				json.put(ServerConstantsCredit.CA_IMPORTE, params.get(ServerConstantsCredit.CA_IMPORTE));
				json.put(ServerConstantsCredit.CA_TASA_ANUAL, params.get(ServerConstantsCredit.CA_TASA_ANUAL));
				json.put(ServerConstantsCredit.CA_PLAZO, params.get(ServerConstantsCredit.CA_PLAZO));
				json.put(ServerConstantsCredit.CA_PAGO_FIJO_MENSUAL, params.get(ServerConstantsCredit.CA_PAGO_FIJO_MENSUAL));

				json.put(ServerConstantsCredit.SCH, params.get(ServerConstantsCredit.SCH));
				json.put(ServerConstantsCredit.CH_IMPORTE, params.get(ServerConstantsCredit.CH_IMPORTE));
				json.put(ServerConstantsCredit.CH_TASA_ANUAL, params.get(ServerConstantsCredit.CH_TASA_ANUAL));
				json.put(ServerConstantsCredit.CH_PLAZO, params.get(ServerConstantsCredit.CH_PLAZO));
				json.put(ServerConstantsCredit.CH_PAGO_FIJO_MENSUAL, params.get(ServerConstantsCredit.CH_PAGO_FIJO_MENSUAL));

				json.put(ServerConstantsCredit.CON_CAT, params.get(ServerConstantsCredit.CON_CAT));
				json.put(ServerConstantsCredit.CON_FECHA_CALCULO, params.get(ServerConstantsCredit.CON_FECHA_CALCULO));

			} else if (indicador.equals("L")) {

				//liquidacion
				json.put(ServerConstantsCredit.SCNL, params.get(ServerConstantsCredit.SCNL));
				json.put(ServerConstantsCredit.CN_DESC, params.get(ServerConstantsCredit.CN_DESC));
				json.put(ServerConstantsCredit.CN_CANT, params.get(ServerConstantsCredit.CN_CANT));

				json.put(ServerConstantsCredit.SPPIL, params.get(ServerConstantsCredit.SPPIL));
				json.put(ServerConstantsCredit.PP_DESC, params.get(ServerConstantsCredit.PP_DESC));
				json.put(ServerConstantsCredit.PP_CANT, params.get(ServerConstantsCredit.PP_CANT));

				json.put(ServerConstantsCredit.SCHL, params.get(ServerConstantsCredit.SCHL));
				json.put(ServerConstantsCredit.CH_DESC, params.get(ServerConstantsCredit.CH_DESC));
				json.put(ServerConstantsCredit.CH_CANT, params.get(ServerConstantsCredit.CH_CANT));

				json.put(ServerConstantsCredit.SCAL, params.get(ServerConstantsCredit.SCAL));
				json.put(ServerConstantsCredit.CA_DESC, params.get(ServerConstantsCredit.CA_DESC));
				json.put(ServerConstantsCredit.CA_CANT, params.get(ServerConstantsCredit.CA_CANT));

				json.put(ServerConstantsCredit.SP5, params.get(ServerConstantsCredit.SP5));
				json.put(ServerConstantsCredit.P5_DESC, params.get(ServerConstantsCredit.P5_DESC));
				json.put(ServerConstantsCredit.P5_CANT, params.get(ServerConstantsCredit.P5_CANT));

				json.put(ServerConstantsCredit.SP6, params.get(ServerConstantsCredit.SP6));
				json.put(ServerConstantsCredit.P6_DESC, params.get(ServerConstantsCredit.P6_DESC));
				json.put(ServerConstantsCredit.P6_CANT, params.get(ServerConstantsCredit.P6_CANT));


				//alternativas
				json.put(ServerConstantsCredit.SCR, params.get(ServerConstantsCredit.SCR));

				json.put(ServerConstantsCredit.SAN, params.get(ServerConstantsCredit.SAN));
				json.put(ServerConstantsCredit.CN, params.get(ServerConstantsCredit.CN));

				json.put(ServerConstantsCredit.SAP, params.get(ServerConstantsCredit.SAP));
				json.put(ServerConstantsCredit.PP, params.get(ServerConstantsCredit.PP));

				json.put(ServerConstantsCredit.SAI, params.get(ServerConstantsCredit.SAI));
				json.put(ServerConstantsCredit.IL, params.get(ServerConstantsCredit.IL));

				json.put(ServerConstantsCredit.SAH, params.get(ServerConstantsCredit.SAH));
				json.put(ServerConstantsCredit.CH, params.get(ServerConstantsCredit.CH));

				json.put(ServerConstantsCredit.SAA, params.get(ServerConstantsCredit.SAA));
				json.put(ServerConstantsCredit.CA, params.get(ServerConstantsCredit.CA));

				json.put(ServerConstantsCredit.SAT, params.get(ServerConstantsCredit.SAT));
				json.put(ServerConstantsCredit.TC, params.get(ServerConstantsCredit.TC));

				//YA NO SON NECESARIOS
				//json.put(ServerConstantsCredit.LIQ_CAT, params.get(ServerConstantsCredit.LIQ_CAT));
				//json.put(ServerConstantsCredit.LIQ_FECHA_CALCULO, params.get(ServerConstantsCredit.LIQ_FECHA_CALCULO));
			}

			json.put(ServerConstantsCredit.IND_CORREO, indicador);
			json.put(ServerConstantsCredit.email, params.get(ServerConstantsCredit.email));

			this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(ENVIO_CORREO_OPERACION), json, response);
		}

		return response;
	}

	/**
	 * Realiza la operacion de envio de correo.
	 *
	 * @param params un mapa de parametros para la operacion
	 * @return la respuesta del servidor
	 * @throws IOException            control de excepciones de entrada/salida
	 * @throws ParsingExceptionCredit control de excepciones de parseo
	 * @throws JSONException
	 * @throws URISyntaxException
	 * @throws ParsingException
	 */
	private ServerResponseCredit consultaAlternativas(Hashtable<String, ?> params) throws IOException, ParsingExceptionCredit, JSONException, ParsingException, URISyntaxException {
		ConsultaAlternativasData data = new ConsultaAlternativasData();
		ServerResponseCredit response = new ServerResponseCredit(data);

		if (SIMULATION) {
			String mensaje = "";
			SIMULACION_RESPUESTA_ERROR_CONSULTA_ALTERNATIVAS=true;
			if (SIMULACION_RESPUESTA_ERROR_CONSULTA_ALTERNATIVAS) {
				mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CONSULTA_ALTERNATIVAS_ERROR);

			} else {
				if(Server.ALLOW_LOG) Log.e("consultaAlternativas", "OK");

				//Todas las posibles opciones para creditos simulados y contratados en diccionario
				mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CONSULTA_ALTERNATIVAS);
			}

			logNetworkOperation("consultaAlternativas", params.toString(), mensaje);
			this.httpClientCredit.simulateOperation(mensaje, response, true);

		} else {
			JSONObject json = new JSONObject();

			json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CONSULTA_ALTERNATIVAS));
			json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));

			json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
			json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));

			//Starts BBVA CREDIT Cookies.
			if (Session.getInstance(SuiteApp.appContext).areCookiesSet()) {
				this.httpClientCredit.setCookies(Session.getInstance(SuiteApp.appContext).getCookieManager());
				Session.getInstance(SuiteApp.appContext).setCookiesFlag(true);
				this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CONSULTA_ALTERNATIVAS), json, response);
			}else{
				this.httpClientCredit.setCookies(Session.getInstance(SuiteApp.appContext).getCookieManager());
				Session.getInstance(SuiteApp.appContext).setCookiesFlag(true);
				this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CONSULTA_ALTERNATIVAS), json, response);
			}

			//Log.d("INVOKER CERDIT", this.httpClientCredit.getHttpClient().toString());

		}

		return response;
	}

	private ServerResponseCredit consultaDetalleAlternativa(Hashtable<String, ?> params) throws IOException, ParsingExceptionCredit, JSONException, ParsingException, URISyntaxException {
		DetalleAlternativa data = new DetalleAlternativa();
		ServerResponseCredit response = new ServerResponseCredit(data);

		if(Server.ALLOW_LOG) Log.e("Llega a server method", "ok");
		if(Server.ALLOW_LOG) Log.e("valor bandera", String.valueOf(SIMULATION));
		SIMULACION_RESPUESTA_ERROR_CONSULTA_DETALLE_ALTERNATIVAS=true;
		if (SIMULATION) {
			String mensaje = "";
			if (SIMULACION_RESPUESTA_ERROR_CONSULTA_DETALLE_ALTERNATIVAS) {
				mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.DETALLE_ALTERNATIVA_ERROR);
			} else {

				if (Session.getInstance(MainController.getInstance().getContext()).getCveCamp().equals(ConstantsCredit.INCREMENTO_LINEA_CREDITO)) {

					if(Server.ALLOW_LOG) Log.e("Entra ILC", "ok");
					//ILC
					mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.DETALLE_ALTERNATIVA_ILC);

				} else {
					if(Server.ALLOW_LOG) Log.e("Entra Consumo", "ok");
					//CONSUMO
					mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.DETALLE_ALTERNATIVA_CONSUMO);
				}
			}

			logNetworkOperation("consultaDetalleAlternativa", params.toString(), mensaje);
			this.httpClientCredit.simulateOperation(mensaje, response, true);

		} else {
			JSONObject json = new JSONObject();

			json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.DETALLE_ALTERNATIVA));
			json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));

			json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
			json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));
			json.put(ServerConstantsCredit.PRODUCTO_PARAM, params.get(ServerConstantsCredit.PRODUCTO_PARAM));

			this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(DETALLE_ALTERNATIVA), json, response);
		}

		return response;
	}

	private ServerResponseCredit contrataAlternativaConsumo(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException, ParsingExceptionCredit, JSONException {
		AceptaOfertaConsumo aceptaofertaConsumo=new AceptaOfertaConsumo();
		ServerResponseCredit response = new ServerResponseCredit(aceptaofertaConsumo);
		isjsonvalueCode=isJsonValueCode.CONSUMO;
		if (SIMULATION) {
			String mensaje = oReadDocumentCredit.read(ConstantsPropertyList.EXITOCONSUMOOK_2);
			this.httpClientCredit.simulateOperation(mensaje, response, true);
			//this.httpClientCredit.simulateOperation(oReadDocument.read(ConstantsPropertyList.EXITOCONSUMOOK_2),response, true);
		}else{

			//Modificacion 50870
			JSONObject json = new JSONObject();
			json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CONTRATA_ALTERNATIVA_CONSUMO));
			json.put("numeroCelular", params.get("numeroCelular"));
			json.put("claveCamp", params.get("claveCamp"));
			json.put("estatus", params.get("estatus"));
			json.put("codigoOTP", params.get("codigoOTP"));
			json.put("seguroObli", params.get("seguroObli"));
			json.put("IUM", params.get("IUM"));
			json.put("producto", Session.getInstance(SuiteApp.appContext).getCveCamp());

			this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CONTRATA_ALTERNATIVA_CONSUMO), json, response);
		}
		return response;
	}

	//INICIA PAPERLESS
	//metodos paperless
	private ServerResponse actualizarEstatusEnvioEC(Hashtable<String, ?> params)

			throws IOException, ParsingException, URISyntaxException {
		ConsultarEstatusEnvioECResult data = new ConsultarEstatusEnvioECResult();
		ServerResponse response = new ServerResponse(data);
		isjsonvalueCode=isJsonValueCode.PAPERLESS;
		if (SIMULATION) {
			//	PLESS_SIM1
			this.httpClient.simulateOperation("{\"estado\":\"OK\",\"codigoMensaje\":\"\",\"descripcionMensaje\":\"\",\"folio\":\"5343515478\",\"fecha\":\"18122014\",\"hora\":085700\"}",response, true);

		}else{

			String numeroCelular = (String) params.get("numeroCelular");
			String IUM = (String) params.get("IUM");

			String cadAut = (String) params.get("cadenaAutenticacion");
			String cveAcceso = (String) params.get("cveAcceso");
			String codigoNIP = (String) params.get("codigoNIP");
			String codigoOTP = (String) params.get("codigoOTP");
			String codigoOTPReg = (String) params.get("codigoOTPReg");
			String codigoCVV2 = (String) params.get("codigoCVV2");

			JSONObject arrCuentas = (JSONObject) params.get("arrCuentas");

			NameValuePair[] parameters = new NameValuePair[9];
			parameters[0] = new NameValuePair("numeroCelular",numeroCelular);
			parameters[1] = new NameValuePair("IUM",IUM);
			parameters[2] = new NameValuePair("cadenaAutenticacion",cadAut);
			parameters[3] = new NameValuePair("cveAcceso",cveAcceso);
			parameters[4] = new NameValuePair("codigoNIP",codigoNIP);
			parameters[5] = new NameValuePair("codigoOTP",codigoOTP);
			parameters[6] = new NameValuePair("codigoOTPReg",codigoOTPReg);
			parameters[7] = new NameValuePair("codigoCVV2",codigoCVV2);
			parameters[8] = new NameValuePair("arrCuentas",arrCuentas);

			clienteHttp.invokeOperation(OPERATION_CODES[ACTUALIZAR_ESTATUS_EC], parameters, response, false, true);
		}
		return response;
	}
	private ServerResponse consultarEstadoCuenta(Hashtable<String, ?> params)

			throws IOException, ParsingException, URISyntaxException {
		ConsultaECResult data = new ConsultaECResult();
		ServerResponse response = new ServerResponse(data);
		isjsonvalueCode=isJsonValueCode.PAPERLESS;
		if (SIMULATION) {
			//PLESS_SIM2
			this.httpClient.simulateOperation("{\"estado\":\"OK\",\"codigoMensaje\":\"\",\"descripcionMensaje\":\"\",\"pdf\":\"aHR0cDovL3d3dy51bmFsLmVkdS5jby9kbnAvQXJjaGl2b3NfYmFzZS9Gb3JtYXRvX1VuaWNvX2RlX0hvamFfZGVfVmlkYS1QZXJzb25hX25hdHVyYWwtREFGUC5wZGY=\"}",response, true);
		}else{
			String numeroCuenta = (String) params.get("numeroCuenta");
			String periodo = (String) params.get("periodo");
			String fechaCorte = (String) params.get("fechaCorte");
			String referencia = (String) params.get("referencia");
			String email = (String) params.get("email");
			String numeroCelular = (String) params.get("numeroCelular");
			String IUM = (String) params.get("IUM");

			String cadAut = (String) params.get("cadenaAutenticacion");
			String cveAcceso = (String) params.get("cveAcceso");
			String codigoNIP = (String) params.get("codigoNIP");
			String codigoOTP = (String) params.get("codigoOTP");
			String codigoOTPReg = (String) params.get("codigoOTPReg");
			String codigoCVV2 = (String) params.get("codigoCVV2");

			NameValuePair[] parameters = new NameValuePair[13];
			parameters[0] = new NameValuePair("numeroCuenta",numeroCuenta);
			parameters[1] = new NameValuePair("fechaCorte",fechaCorte);
			parameters[2] = new NameValuePair("periodo",periodo);
			parameters[3] = new NameValuePair("referencia",referencia);
			parameters[4] = new NameValuePair("email",email);
			parameters[5] = new NameValuePair("numeroCelular",numeroCelular);
			parameters[6] = new NameValuePair("IUM",IUM);
			parameters[7] = new NameValuePair("cadenaAutenticacion",cadAut);
			parameters[8] = new NameValuePair("cveAcceso",cveAcceso);
			parameters[9] = new NameValuePair("codigoNIP",codigoNIP);
			parameters[10] = new NameValuePair("codigoOTP",codigoOTP);
			parameters[11] = new NameValuePair("codigoOTPReg",codigoOTPReg);
			parameters[12] = new NameValuePair("codigoCVV2",codigoCVV2);


			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTAR_ESTADO_CUENTA], parameters, response, false, true);
		}

		return response;
	}

	private ServerResponse consultarEstatusEnvioEC(Hashtable<String, ?> params)

			throws IOException, ParsingException, URISyntaxException {
		ConsultarEstatusEnvioEC data = new ConsultarEstatusEnvioEC();
		ServerResponse response = new ServerResponse(data);
		isjsonvalueCode=isJsonValueCode.PAPERLESS;
		if (SIMULATION) {
			//PLESS_SIM3
			this.httpClient.simulateOperation("{\"estado\":\"OK\",\"codigoMensaje\":\"\",\"descripcionMensaje\":\"\",\"arrCuentas\":{\"ocCuentas\": [{\"numeroCuenta\":\"00743616000178463778\",\"estatusEnvio\":\"D\"},{\"numeroCuenta\":\"00749964732300923874\",\"estatusEnvio\":\"I\"}]}}",response, true);

		}else{
			String numeroCelular = (String) params.get(ServerConstants.NUMERO_CELULAR);
			String IUM = (String) params.get(ServerConstants.JSON_IUM_ETIQUETA);
			NameValuePair[] parameters = new NameValuePair[2];
			parameters[0] = new NameValuePair(ServerConstants.NUMERO_CELULAR,numeroCelular);
			parameters[1] = new NameValuePair(ServerConstants.JSON_IUM_ETIQUETA,IUM);
			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTAR_ESTATUS_EC], parameters, response, false, true);
		}
		return response;
	}
	private ServerResponse consultaTextoPaperles(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {
		TextoPaperless textoPaperless=new TextoPaperless();
		ServerResponse response = new ServerResponse(textoPaperless);
		isjsonvalueCode=isJsonValueCode.PAPERLESS;
		if (SIMULATION) {
			//PLESS_SIM4
			this.httpClient.simulateOperation("{\"estado\":\"OK\",\"tcTitulo\":\"&#161;Piensa Verde&#33;\",\"tcSubtitulo\":\"&#191;Quieres dejar de recibir tu estado de cuenta en tu domicilio&#63;\",\"tcDescripcion\":\" BBVA Bancomer te informa que puedes consultar tu estado de cuenta a trav&eacute;s de www.bancomer.com en Acceso a clientes &oacute; Servicios Digitales y en cualquiera de nuestras sucursales.\",\"tcPieDeMensaje\":\"V&aacute;lido para cuentas de Cheques, Ahorro y TDC.\",\"tcFraseDelDia\":\"Cuidemos el medio ambiente \"}", response, true);
		}else{
			String idProducto = (String) params.get("idProducto");
			String indicadorPagina = (String) params.get("indicadorPagina");
			String indicadorSubseccion = (String) params.get("indicadorSubseccion");
			String Versión = (String) params.get("version"); /***/
			String ultimaSeccionConsultada = (String) params.get("ultimaSeccionConsultada");
			String numeroCelular = (String) params.get("numeroCelular");
			String IUM = (String) params.get("IUM");

			NameValuePair[] parameters = new NameValuePair[7];
			parameters[0] = new NameValuePair("idProducto",idProducto);
			parameters[1] = new NameValuePair("indicadorPagina",indicadorPagina);
			parameters[2] = new NameValuePair("indicadorSubseccion",indicadorSubseccion);
			parameters[3] = new NameValuePair("version",Versión);/***/
			parameters[4] = new NameValuePair("ultimaSeccionConsultada",ultimaSeccionConsultada);
			parameters[5] = new NameValuePair("numeroCelular",numeroCelular);
			parameters[6] = new NameValuePair("IUM",IUM);

			clienteHttp.invokeOperation(OPERATION_CODES[CONSULTAR_TEXTO_PAPERLESS], parameters, response, false, true);
		}
		return response;
	}
	private ServerResponse inhibirEnvioEstadoCuenta(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {
		CampaniaPaperlessResult data = new CampaniaPaperlessResult();
		ServerResponse response = new ServerResponse(data);
		isjsonvalueCode=isJsonValueCode.PAPERLESS;
		if (SIMULATION) {
			//PLESS_SIM5
			this.httpClient.simulateOperation("{\"estado\":\"OK\",\"codigoMensaje\":\"\",\"descripcionMensaje\":\"\",\"folio\":\"5343515478\",\"fecha\":\"18122014\",\"hora\":\"085700\"}",response, true);
		}else{
			String idProducto = (String) params.get("idProducto");
			String claveContratacion = (String) params.get("claveContratacion");
			String claveRespuesta = (String) params.get("claveRespuesta");
			String idCampaña = (String) params.get("idCampana"); /**/
			String email = (String) params.get("email");
			String causaInhibicion = (String) params.get("causaInhibicion");
			String numeroCelular = (String) params.get("numeroCelular");
			String IUM = (String) params.get("IUM");

			String cadAut = (String) params.get("cadenaAutenticacion");
			String cveAcceso = (String) params.get("cveAcceso");
			String codigoNIP = (String) params.get("codigoNIP");
			String codigoOTP = (String) params.get("codigoOTP");
			String codigoOTPReg = (String) params.get("codigoOTPReg");
			String codigoCVV2 = (String) params.get("codigoCVV2");

			NameValuePair[] parameters = new NameValuePair[14];
			parameters[0] = new NameValuePair("idProducto",idProducto);
			parameters[1] = new NameValuePair("claveContratacion",claveContratacion);
			parameters[2] = new NameValuePair("claveRespuesta",claveRespuesta);
			parameters[3] = new NameValuePair("idCampana",idCampaña); /****/
			parameters[4] = new NameValuePair("email",email);
			parameters[5] = new NameValuePair("causaInhibicion",causaInhibicion);
			parameters[6] = new NameValuePair("numeroCelular",numeroCelular);
			parameters[7] = new NameValuePair("IUM",IUM);
			parameters[8] = new NameValuePair("cadenaAutenticacion",cadAut);
			parameters[9] = new NameValuePair("cveAcceso",cveAcceso);
			parameters[10] = new NameValuePair("codigoNIP",codigoNIP);
			parameters[11] = new NameValuePair("codigoOTP",codigoOTP);
			parameters[12] = new NameValuePair("codigoOTPReg",codigoOTPReg);
			parameters[13] = new NameValuePair("codigoCVV2",codigoCVV2);

			clienteHttp.invokeOperation(OPERATION_CODES[INHIBIR_ENVIO_EC], parameters, response, false, true);
		}
		return response;
	}

	private ServerResponse obtenerPeriodosEC(Hashtable<String, ?> params)

			throws IOException, ParsingException, URISyntaxException {
		ConsultaECExtract data = new ConsultaECExtract();
		ServerResponse response = new ServerResponse(data);
		isjsonvalueCode=isJsonValueCode.PAPERLESS;
		if (SIMULATION) {
			//PLESS_SIM6
		}else{
			String numeroCuenta = (String) params.get("numeroCuenta");
			String numeroCelular = (String) params.get("numeroCelular");
			String IUM = (String) params.get("IUM");


			NameValuePair[] parameters = new NameValuePair[3];
			parameters[0] = new NameValuePair("numeroCuenta",numeroCuenta);
			parameters[1] = new NameValuePair("numeroCelular",numeroCelular);
			parameters[2] = new NameValuePair("IUM",IUM);
			clienteHttp.invokeOperation(OPERATION_CODES[OBTENER_PERIODO_EC], parameters, response, false, true);
		}
		return response;
	}
	//TERMINA PAPERLESS

	// INICIA SPEI
	public ServerResponse consultaSPEI(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ConsultaSPEIData data = new ConsultaSPEIData();
		ServerResponse response = new ServerResponse(data);

		Boolean error = false;

		NameValuePair[] parameters = new NameValuePair[params.size()];
		//Parametros obligatorios
		Integer cont = 0;
		parameters[cont++] = new NameValuePair(ServerConstants.PERIODO, (String) params.get(ServerConstants.PERIODO));
		parameters[cont++] = new NameValuePair(ServerConstants.NUMERO_CELULAR, (String) params.get(ServerConstants.NUMERO_CELULAR));
		parameters[cont++] = new NameValuePair(ServerConstants.JSON_IUM_ETIQUETA, (String) params.get(ServerConstants.JSON_IUM_ETIQUETA));
		parameters[cont++] = new NameValuePair(ServerConstants.CUENTA, (String) params.get(ServerConstants.CUENTA));
		parameters[cont++] = new NameValuePair(ServerConstants.CADENA_AUTENTICACION, (String) params.get(ServerConstants.CADENA_AUTENTICACION));

		// Parametros no obligatorios
		if(params.containsKey(ServerConstants.CODIGO_NIP)){
			parameters[cont++] = new NameValuePair(ServerConstants.CODIGO_NIP, (String) params.get(ServerConstants.CODIGO_NIP));
		}
		if(params.containsKey(ServerConstants.CODIGO_CVV2)) {
			parameters[cont++] = new NameValuePair(ServerConstants.CODIGO_CVV2, (String) params.get(ServerConstants.CODIGO_CVV2));
		}
		if(params.containsKey(ServerConstants.CVE_ACCESO)) {
			parameters[cont++] = new NameValuePair(ServerConstants.CVE_ACCESO, (String) params.get(ServerConstants.CVE_ACCESO));
		}
		if(params.containsKey(ServerConstants.TARJETA_5DIG)) {
			parameters[cont++] = new NameValuePair(ServerConstants.TARJETA_5DIG, (String) params.get(ServerConstants.TARJETA_5DIG));
		}
		if(params.containsKey(ServerConstants.CODIGO_OTP)) {
			parameters[cont++] = new NameValuePair(ServerConstants.CODIGO_OTP, (String) params.get(ServerConstants.CODIGO_OTP));
		}

		if (SIMULATION) {
			if(error){
				httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONSULTASPEI_ERROR), response, true);
			}else {
				String periodo = (String) params.get(ServerConstants.PERIODO);
				if (periodo.equals("0")) {
					/*if(contadorEjecuciones == 0){
						contadorEjecuciones++;
						httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONSULTASPEI_OK_P1), response, true);
					}else{*/
					httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONSULTASPEI_OK_P0), response, true);
					//}
				} else if (periodo.equals("1")){

					httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONSULTASPEI_OK_P1), response, true);
				}else{

					httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.CONSULTASPEI_OK_P2), response, true);
				}
			}
		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[OP_CONSULTA_SPEI], parameters, response, false, true);
		}

		return response;
	}

	public ServerResponse enviarCorreoSPEI(Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {
		ServerResponse response = new ServerResponse();

		Boolean error = false;

		NameValuePair[] parameters = new NameValuePair[params.size()];
		//Parametros obligatorios
		Integer cont = 0;
		parameters[cont++] = new NameValuePair(ServerConstants.CUENTA, (String) params.get(ServerConstants.CUENTA));
		parameters[cont++] = new NameValuePair(ServerConstants.NUMERO_TELEFONO, (String) params.get(ServerConstants.NUMERO_TELEFONO));
		parameters[cont++] = new NameValuePair(ServerConstants.JSON_IUM_ETIQUETA, (String) params.get(ServerConstants.JSON_IUM_ETIQUETA));
		parameters[cont++] = new NameValuePair(ServerConstants.TIPO_COMPROBANTE, (String) params.get(ServerConstants.TIPO_COMPROBANTE));
		parameters[cont++] = new NameValuePair(ServerConstants.PERIODO, (String) params.get(ServerConstants.PERIODO));
		parameters[cont++] = new NameValuePair(ServerConstants.NUMERO_DE_REFERENCIA, (String) params.get(ServerConstants.NUMERO_DE_REFERENCIA));
		parameters[cont++] = new NameValuePair(ServerConstants.CORREO_DESTINO, (String) params.get(ServerConstants.CORREO_DESTINO));

		// Parametros no obligatorios
		if(params.containsKey(ServerConstants.MENSAJE_A)){
			parameters[cont++] = new NameValuePair(ServerConstants.MENSAJE_A, (String) params.get(ServerConstants.MENSAJE_A));
		}

		if (SIMULATION) {
			if(error){
				httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.ENVIARCORREOSPEI_ERROR), response, true);
			}else{
				httpClient.simulateOperation(oReadDocument.read(ConstantsPropertyList.ENVIARCORREOSPEI_OK), response, true);
			}
		}else{
			clienteHttp.invokeOperation(OPERATION_CODES[OP_ENVIOCORREO_SPEI], parameters, response, false, true);
		}

		return response;
	}
	// TERMINA SPEI

	/**
	 * Created June 8th,2015,
	 *
	 * @throws URISyntaxException
	 * @throws ParsingException
	 */


	private ServerResponseCredit guardarEliminarSimulacion(Hashtable<String, ?> params) throws IOException, ParsingExceptionCredit, JSONException, ParsingException, URISyntaxException {
		CalculoData data = new CalculoData();
		ServerResponseCredit response = new ServerResponseCredit(data);
		if(Server.ALLOW_LOG) Log.d("Entra a proceso de Server:", "Guardar o Eliminar Simulación");


		if (SIMULATION) {
			String mensaje = "";
			if (SIMULACION_RESPUESTA_ERROR_CONSULTA_DETALLE_ALTERNATIVAS) {
				mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CALCULO_ALTERNATIVAS_ERROR);
			} else {
				mensaje = oReadDocumentCredit.read(CreditConstantsPropertyList.CALCULO_ALTERNATIVAS);
			}

			logNetworkOperation("guardarEliminarSimulacion", params.toString(), mensaje);
			this.httpClientCredit.simulateOperation(mensaje, response, true);

		} else {
			JSONObject json = new JSONObject();

			json.put(ServerConstantsCredit.OPERACION_PARAM, Server.OPERATION_CODES_CREDIT.get(Server.CALCULO_OPERACION));
			json.put(ServerConstantsCredit.CLIENTE_PARAM, params.get(ServerConstantsCredit.CLIENTE_PARAM));

			json.put(ServerConstantsCredit.IUM_PARAM, params.get(ServerConstantsCredit.IUM_PARAM));
			json.put(ServerConstantsCredit.NUMERO_CEL_PARAM, params.get(ServerConstantsCredit.NUMERO_CEL_PARAM));
			json.put(ServerConstantsCredit.TIPO_OP_PARAM, params.get(ServerConstantsCredit.TIPO_OP_PARAM));

			this.httpClientCredit.invokeOperation(OPERATION_CODES_CREDIT.get(CALCULO_OPERACION), json, response);
		}

		return response;
	}

	private static void logNetworkOperation(String operationTag, String requestParams, String responseMessage) {
		if (ALLOW_LOG) {
			Log.d(operationTag + " request params", requestParams);
			Tools.trazaTexto(operationTag + " response", responseMessage);
		}
	}

}