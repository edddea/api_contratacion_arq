package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.content.res.Resources;
import android.text.Editable;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.TransferMisCuentasDetalleViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.TransferirMisCuentasSeleccionViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Comision;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaInterna;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferirCuentasPropiasData;
import suitebancomer.classes.gui.controllers.BaseViewController;


public class TransferirMisCuentasDelegate extends DelegateBaseOperacion {
	public final static long TRANSFERIR_MIS_CUENTAS_DELEGATE_ID = 0xb3c703909bd19831L;
	protected SolicitarAlertasData sa;
	public TransferirMisCuentasDelegate() {
		innerTransaction = new TransferenciaInterna();
		this.operacion = Constants.Operacion.transferirPropias;
	}
	
	public void showTransferMisCuentasDetalle() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showTransferirMisCuentasDetalle();
	}
	
	public void showConfirmacion() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacion(this);
	}
	
	public void showResultados() {
		this.actualizarMontoCuentas();
		
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showResultadosViewController(this, getNombreImagenEncabezado(), getTextoEncabezado());
	}

	public void analyzeResponse(int operationId, ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			
			if(operationId == Server.OP_SOLICITAR_ALERTAS){
				
			
				setSa((SolicitarAlertasData) response.getResponse());
				analyzeAlertasRecortado();
				return;
			}
			
			if (response.getResponse() instanceof Comision){
				comision = (Comision) response.getResponse();
				showConfirmacion();
			}else {
				resultado = (TransferirCuentasPropiasData)response.getResponse();
				showResultados();
			}
			
			
			
			
		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
			
		} else if (response.getStatus() == ServerResponse.OPERATION_ERROR){
			
			
			Session session = Session.getInstance(SuiteApp.appContext);
			Constants.Perfil perfil = session.getClientProfile();
			
			if(Constants.Perfil.recortado.equals(perfil)){
					
					setTransferErrorResponse(response);
					solicitarAlertas(ownerController);

			}
		}
	
	}
	
	@Override
	public void realizaOperacion(
			ConfirmacionViewController confirmacionViewController,
			String contrasenia, String nip, String token, String campoTarjeta) {
		this.ownerController = confirmacionViewController;

		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		Session session = Session.getInstance(SuiteApp.appContext);

		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
		paramTable.put(ServerConstants.PARAMS_TEXTO_NP, (null == contrasenia) ? ""
				: contrasenia);
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());
		paramTable.put("CC", innerTransaction
				.getCuentaOrigen().getType()
				+ innerTransaction.getCuentaOrigen().getNumber());
		paramTable.put("CA", innerTransaction
				.getCuentaDestino().getType()
				+ innerTransaction.getCuentaDestino().getNumber());
		paramTable.put("IM",
				Tools.formatAmountForServer(innerTransaction.getImporte()));
		paramTable.put("TE", session.getClientNumber());
		paramTable.put("NI", (null == nip) ? "" : nip);
		paramTable.put("CV", "");
		paramTable
				.put("OT", (null == token) ? "" : token);
		paramTable.put(
				"VA",
				Autenticacion.getInstance().getCadenaAutenticacion(
						this.operacion,
						Session.getInstance(SuiteApp.appContext)
								.getClientProfile(),
						Tools.getDoubleAmountFromServerString(innerTransaction
								.getImporte())));

		paramTable.put("MP", innerTransaction.getMotivoPago());
		//JAIG
		doNetworkOperation(Server.SELF_TRANSFER_OPERATION, paramTable,false,new TransferirCuentasPropiasData(),isJsonValueCode.NONE,
				confirmacionViewController);
	}

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#doNetworkOperation(int, java.util.Hashtable, suitebancomer.classes.gui.controllers.BaseViewController)
	 */
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {
		((BmovilViewsController)caller.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
	}
	

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#getDelegateIdentifier()
	 */
	@Override
	public long getDelegateIdentifier() {
		// TODO Auto-generated method stub
		return super.getDelegateIdentifier();
	}
	

	/**
	 * Actualiza los montos de las cuentas de origen y destino.
	 */
	@SuppressWarnings("unchecked")
	public void actualizarMontoCuentas() {
		ArrayList<String> datosCuentaOrigen;
		ArrayList<String> datosCuentaDestino;
		
		try {
			datosCuentaOrigen = (ArrayList<String>)resultado.getAccountList().get(Constants.SELF_TRANSFER_ORIGIN_ACCOUNT_INDEX);
			datosCuentaDestino = (ArrayList<String>)resultado.getAccountList().get(Constants.SELF_TRANSFER_DESTINATION_ACCOUNT_INDEX);
			
			Session.getInstance(SuiteApp.appContext).actualizaMonto(innerTransaction.getCuentaOrigen(), 
																	datosCuentaOrigen.get(Constants.SELF_TRANSFER_ACCOUNT_BALANCE_INDEX));
			Session.getInstance(SuiteApp.appContext).actualizaMonto(innerTransaction.getCuentaDestino(), 
																	datosCuentaDestino.get(Constants.SELF_TRANSFER_ACCOUNT_BALANCE_INDEX));
		} catch(Exception ex) {
			if(Server.ALLOW_LOG) Log.e("actualizarMontoCuentas", "Error al interpretar los datos de cuentas del servidor.", ex);
		}
	}
	
	public int getOpcionesMenuResultados() {
		return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF;
	}

	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.result_lista_cuenta_origen));
		fila.add(Tools.hideAccountNumber(innerTransaction.getCuentaOrigen().getNumber()));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.result_lista_cuenta_destino));
		fila.add(Tools.hideAccountNumber(innerTransaction.getCuentaDestino().getNumber()));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.result_lista_importe));
		fila.add("$ " + Tools.formatUserAmount(innerTransaction.getImporte()));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.transferir_detalle_motivo));
		fila.add(innerTransaction.getMotivoPago());
		tabla.add(fila);
		
		if(innerTransaction.getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE)){
			
			if(!Tools.isEmptyOrNull(comision.getComision()))
			{
				fila = new ArrayList<String>();
				fila.add(ownerController.getString(R.string.transferir_interbancario_tabla_resultados_comision));
				fila.add(Tools.formatAmount(comision.getComision(),false));
				tabla.add(fila);
			}
		}

		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.result_lista_fecha));
		fila.add(Tools.formatDate(Tools.parseDateTime(resultado.getServerDate(), resultado.getServerTime())));
		tabla.add(fila);

		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.result_lista_hora));
		fila.add(Tools.formatTime(resultado.getServerTime()));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.result_lista_folio));
		fila.add(resultado.getReference());
		tabla.add(fila);
		
		return tabla;
	}
	
	private TransferirCuentasPropiasData resultado;
	
	public TransferirCuentasPropiasData getResultado() {
		return resultado;
	}
	
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.confirmation_lista_cuenta_origen));
		fila.add(Tools.hideAccountNumber(innerTransaction.getCuentaOrigen().getNumber()));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.confirmation_lista_cuenta_destino));
		fila.add(Tools.hideAccountNumber(innerTransaction.getCuentaDestino().getNumber()));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.confirmation_lista_importe));
		fila.add("$ " + Tools.formatUserAmount(innerTransaction.getImporte()));
		tabla.add(fila);
		

		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.transferir_detalle_motivo));
		fila.add(innerTransaction.getMotivoPago());
		tabla.add(fila);

		if(innerTransaction.getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE)){
			
			if(!Tools.isEmptyOrNull(comision.getComision()))
			{
				fila = new ArrayList<String>();
				fila.add(ownerController.getString(R.string.transferir_interbancario_tabla_resultados_comision));
				fila.add(Tools.formatAmount(comision.getComision(),false));
				tabla.add(fila);
			}
		}
		
		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.confirmation_lista_fecha));
		fila.add(Tools.formatDate(Session.getInstance(SuiteApp.appContext).getServerDate().getTime()));
		tabla.add(fila);
		
		return tabla;
	}

	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.bmovil_transferir_icono;
	}

	@Override
	public int getTextoEncabezado() {
		return R.string.mis_cuentas_title;
	}

	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		TipoOtpAutenticacion value = null;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		value = Autenticacion.getInstance().tokenAMostrar(this.operacion, perfil,Tools.getDoubleAmountFromServerString(innerTransaction.getImporte()));
		
		return value;
	}

	@Override
	public boolean mostrarNIP() {
		boolean mostrar = false;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		mostrar = Autenticacion.getInstance().mostrarNIP(this.operacion, perfil,Tools.getDoubleAmountFromServerString(innerTransaction.getImporte()));
		
		return mostrar;
	}

	@Override
	public boolean mostrarContrasenia() {
		boolean mostrar = false;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		mostrar = Autenticacion.getInstance().mostrarContrasena(this.operacion, perfil,Tools.getDoubleAmountFromServerString(innerTransaction.getImporte()));
		
		return mostrar;
	}

	/**
	 * La operacion de este delegado.
	 */
	private Constants.Operacion operacion;
	
	/**
	 * Regresa el tipo de operación.
	 * @return El tipo de operación.
	 */
	public Operacion getOperationContext() {
		return Operacion.transferirPropias;
	}
	
	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada seg�n sea reguerido.
	 * @return
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		//Account[] accounts = Session.getAccountsMC();
				
		if(profile == Constants.Perfil.avanzado) {
			if(ownerController instanceof TransferirMisCuentasSeleccionViewController) {
				for(Account acc : accounts) {
					if(acc.isVisible()) {
						accountsArray.add(acc);
						break;
					}
				}
				
				for(Account acc : accounts) {
					if(!acc.isVisible())
						accountsArray.add(acc);
				}
			}
			
			else if (ownerController instanceof TransferMisCuentasDetalleViewController) {
				accountsArray.add(innerTransaction.getCuentaOrigen());
				for(Account acc : accounts) {
					if(!acc.getNumber().equalsIgnoreCase(innerTransaction.getCuentaDestino().getNumber()) && !acc.getNumber().equalsIgnoreCase(innerTransaction.getCuentaOrigen().getNumber()))
						accountsArray.add(acc);
				}
			}
			
		} else {
			for(Account acc : accounts) {
				if(acc.isVisible())
					accountsArray.add(acc);
			}
		}
		
		return accountsArray;
	}
	
	/**
	 * Obtiene la lista de cuentas a mostrar en el componente ListaSeleccion, omite la cuenta seleccionada en el componente CuentaOrigen.
	 * @return La lista de cuentas a mostrar.
	 */
	public ArrayList<Account> cargaCuntasListaSeleccion() {
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		//Account[] accounts = Session.getAccountsMC();
				
		for(Account acc : accounts) {
			if(!acc.getNumber().equalsIgnoreCase(innerTransaction.getCuentaOrigen().getNumber())) {
//				if(!acc.getType().equals(innerTransaction.getCuentaOrigen().getType()) || !innerTransaction.getCuentaOrigen().getType().equalsIgnoreCase("TC"))
					accountsArray.add(acc);
			}
		}
		
		return accountsArray;
	}
	
	/**
	 * Set the source account for the transaction.
	 * @param sourceAccount The source account
	 * @throws InvalidParameterException If the source account is null.
	 */
	public void setCuentaSeleccionada(Account sourceAccount) throws InvalidParameterException {
		if(null == sourceAccount) {
			InvalidParameterException ex = new InvalidParameterException("The source account can not be null.");
			if(Server.ALLOW_LOG) Log.e("TransferirMisCuentasDelegate", "The source account can not be null.", ex);
			throw ex;
		} else {
			innerTransaction.setCuentaOrigen(sourceAccount);
		}
	}
	
	/**
	 * The owner view controller.
	 */
	private BaseViewController ownerController;
	
	/**
	 * @return The view controller associated to this delegate.
	 */
	public BaseViewController getCallerController() {
		return ownerController;
	}

	/**
	 * @param callerController The view controller to associate to this delegate.
	 */
	public void setCallerController(BaseViewController callerController) {
		this.ownerController = callerController;
	}
	
	/**
	 * Stores the source and destination accounts.
	 * @param source The source account.
	 * @param destination The destination account.
	 */
	public void guardaTransferenciaInterna(Account destination) {
		innerTransaction.setCuentaDestino(destination);
	}
	
	/**
	 * Stores the amount value.
	 * @param amount
	 */
	public void guardaImporte(String amount) {
		innerTransaction.setImporte(amount);
	}
	
	/**
	 * Validates if the amount is correct.
	 * @param amount The amount for the transaction.
	 * @return True if the amount is correct.
	 */
	public boolean validaCampos(String amount) {
		long value = -1;
		
		try {
			value = Long.valueOf(amount);
		} catch (NumberFormatException ex) {
			if(Server.ALLOW_LOG) Log.e("TransferirMisCuentasDelegate", "Error while parsing the amount as an int.", ex);
			return false;
		}
		
		return (value > 0);	
	}
	
	/**
	 * Inner transaction model object.
	 */
	TransferenciaInterna innerTransaction;
	
	public TransferenciaInterna getInnerTransaction() {
		return innerTransaction;
	}
	
	public String OpcionSeleccionada;

	public void setOpcionSeleccionada(String opcionSeleccionada) {
		OpcionSeleccionada = opcionSeleccionada;
	}

	@Override
	public void performAction(Object obj) {
		if(Server.ALLOW_LOG) Log.d("TransferirMisCuentasDelegate","Regrese de lista Seleccion");
		if (ownerController instanceof TransferirMisCuentasSeleccionViewController) {
			if (((TransferirMisCuentasSeleccionViewController)ownerController).componenteCtaOrigen.isSeleccionado()) {
				((TransferirMisCuentasSeleccionViewController)ownerController).muestraListaCuentas();
				((TransferirMisCuentasSeleccionViewController)ownerController).componenteCtaOrigen.setSeleccionado(false);
			}else {
				((TransferirMisCuentasSeleccionViewController)ownerController).opcionSeleccionada((Account)obj);
			}
		}else if (ownerController instanceof TransferirMisCuentasSeleccionViewController) {
			((TransferirMisCuentasSeleccionViewController)ownerController).componenteCtaOrigen.setSeleccionado(false);
		}else if (ownerController instanceof TransferMisCuentasDetalleViewController) {
			((TransferMisCuentasDetalleViewController)ownerController).actualizaCuentaOrigen((Account)obj);
		}
	}
	
	@Override
	public String getTextoTituloResultado() {
		return ownerController.getString(R.string.transferir_detalle_operacion_exitosaTitle);
	}
	
	@Override
	public int getColorTituloResultado() {
		return R.color.verde_limon;
	}
	
	@Override
	public String getTextoAyudaResultados() {
		// TODO Auto-generated method stub
		return "Para guardar esta información oprime el botón de opciones";
	}

	/**
	 * De <nombre de cuenta, ej. Libret�n> <últimos 5 dígitos de la cuenta
	 * origen> a <nombre de cuenta, RN4> <moneda a 2 caracteres, ej US> <
	 * últimos 4 dígitos de la cuenta depósito> por <importe, $#.##> Folio
	 * <Folio de la operación> < Fecha y hora devueltas en la operación
	 * DD/MM/YYYY hh:mm:ss>
	 */
	@Override
	public String getTextoSMS() {
		StringBuilder builder = new StringBuilder("De ");
		
		Resources res = SuiteApp.appContext.getResources();
		builder.append(innerTransaction.getCuentaOrigen().getPublicName(res, true));
		builder.append(" a ");
		builder.append(innerTransaction.getCuentaOrigen().getPublicName(res, false).replaceAll("\n", " "));
		builder.append(" por ");
		builder.append("$" + innerTransaction.getImporte()); 
		builder.append(" Folio ");
		builder.append(resultado.getReference());
		builder.append(" Fecha y hora devueltas en la operación ");
		builder.append(Tools.formatDate(resultado.getServerDate()));
		builder.append(" ");
		builder.append(Tools.formatTime(resultado.getServerTime()));
						
		return builder.toString();
	}


	Comision comision;

	public void consultaComision(){
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String,String> paramTable = null;
		paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());//NT
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());//IU
		paramTable.put("CC", innerTransaction.getCuentaOrigen().getFullNumber());//CC
		paramTable.put("CA", innerTransaction.getCuentaDestino().getFullNumber());//CA
		paramTable.put("IM", innerTransaction.getImporte()==null?"": Tools.formatAmountForServer(innerTransaction.getImporte()));//IM
		paramTable.put("TO", Constants.COMISION_B);//TO
		doNetworkOperation(Server.CONSULTAR_COMISION_I, paramTable,false,new Comision(),isJsonValueCode.NONE, ownerController);
	}
	public void guardaMotivo(Editable text) {
		// TODO Auto-generated method stub
		innerTransaction.setMotivoPago(text.toString());
	}
}
