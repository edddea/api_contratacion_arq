package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;



import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DineroMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.InterbancariosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.OtrosBBVADelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagoServiciosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TiempoAireDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelper;

public class ConsultarFrecuentesViewController extends BaseViewController implements OnClickListener {

	TextView titulo;
	ListaSeleccionViewController listaFrecuentes;
	Button operarFrecuente;
	Button eliminarFrecuente;
	DelegateBaseOperacion delegate;
	LinearLayout contenedorListaFrecuentes;
	private Bundle extras;
	public String tipoFrecuente;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ	


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_consultar_frecuentes);
		setTitle(R.string.bmovil_consultar_frecuentes_title_cabecera, R.drawable.bmovil_consultar_icono);
		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		extras = getIntent().getExtras();
		long delegateID = extras.getLong(Constants.FREQUENT_REQUEST_SCREEN_DELEGATE_KEY);
		delegate = (DelegateBaseOperacion) parentViewsController.getBaseDelegateForKey(delegateID);
		setDelegate(delegate);
			//AMZ
			parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
			//AMZ
		if (delegate instanceof InterbancariosDelegate) {
			((InterbancariosDelegate) delegate).setViewController(this);
			//AMZ
			TrackingHelper.trackState("otros bancos", parentManager.estados);
			//AMZ
		} else if (delegate instanceof PagoServiciosDelegate) {
			((PagoServiciosDelegate) delegate).setControladorPagos(this);
			//AMZ
			TrackingHelper.trackState("pago servicio", parentManager.estados);
			//AMZ
		}else if (delegate instanceof TiempoAireDelegate) {
			((TiempoAireDelegate) delegate).setcontroladorTiempoAire(this);
			//AMZ
			TrackingHelper.trackState("tiempo aire", parentManager.estados);
			//AMZ
		}else if (delegate instanceof OtrosBBVADelegate) {
			((OtrosBBVADelegate) delegate).setViewController(this);
			//AMZ por aqui
			if(((OtrosBBVADelegate) delegate).isEsExpress()==true){
				TrackingHelper.trackState("express", parentManager.estados);
			}else{
				TrackingHelper.trackState("otras cuentas", parentManager.estados);
			}
			//AMZ
		}else if (delegate instanceof DineroMovilDelegate) {
			((DineroMovilDelegate) delegate).setViewController(this);
			//AMZ
			TrackingHelper.trackState("movil", parentManager.estados);
			//AMZ
		}
		inicializar();
		configurarPantalla();
	}

	private void configurarPantalla(){
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(findViewById(R.id.consultar_frecuentes_boton_operar));	
		gTools.scale(findViewById(R.id.consultar_frecuentes_boton_eliminar));
		gTools.scale(findViewById(R.id.consultar_frecuentes_contenedor_principal));

	}

	@SuppressWarnings("deprecation")
	void inicializar() {
		operarFrecuente = (Button) findViewById(R.id.consultar_frecuentes_boton_operar);
		operarFrecuente.setOnClickListener(this);
		eliminarFrecuente = (Button) findViewById(R.id.consultar_frecuentes_boton_eliminar);
		eliminarFrecuente.setOnClickListener(this);
		contenedorListaFrecuentes = (LinearLayout)findViewById(R.id.contenedor_lista_frecuentes);

		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT,	LayoutParams.WRAP_CONTENT);

		if (listaFrecuentes == null) {
			listaFrecuentes = new ListaSeleccionViewController(this, params,parentViewsController);
			listaFrecuentes.setDelegate(delegate);

			ArrayList<Object> listaEncabezado = delegate.getDatosHeaderTablaFrecuentes();
			ArrayList<Object> listaDatos = delegate.getDatosTablaFrecuentes();
			listaFrecuentes.setEncabezado(listaEncabezado);
			listaFrecuentes.setLista(listaDatos);
			listaFrecuentes.setNumeroColumnas(listaEncabezado.size() - 1) ;
			if (listaDatos.size() == 0) {
				listaFrecuentes.setTextoAMostrar(getString(R.string.bmovil_consultar_frecuentes_emptylist));
			}else {
				listaFrecuentes.setTextoAMostrar(null);
				listaFrecuentes.setNumeroFilas(listaDatos.size());		
			}	
			listaFrecuentes.setOpcionSeleccionada(-1);
			listaFrecuentes.setSeleccionable(true);
			listaFrecuentes.setAlturaFija(true);
			listaFrecuentes.setNumeroFilas(listaDatos.size());
			listaFrecuentes.setExisteFiltro(true);
			listaFrecuentes.setTitle(extras.getString(Constants.FREQUENT_REQUEST_SCREEN_TITLE_KEY));
			listaFrecuentes.setSingleLine(true);
			listaFrecuentes.cargarTabla();
			contenedorListaFrecuentes.addView(listaFrecuentes);
		}

		if (delegate instanceof InterbancariosDelegate) {
			tipoFrecuente = Constants.tipoCFOtrosBancos;
		} else if (delegate instanceof PagoServiciosDelegate) {
			tipoFrecuente = Constants.tipoCFPagosCIE;
		} else if (delegate instanceof TiempoAireDelegate) {
			tipoFrecuente = Constants.tipoCFTiempoAire;
		} else if (delegate instanceof OtrosBBVADelegate){
			tipoFrecuente = Constants.tipoCFOtrosBBVA;
		} else if (delegate instanceof DineroMovilDelegate){
			tipoFrecuente = Constants.tipoCFDineroMovil;
		}

		delegate.consultarFrecuentes(tipoFrecuente);
	}

	@Override
	public void onClick(View v) {

		if (v == operarFrecuente) {
			operarFrecuente();
		} else if (v == eliminarFrecuente) {
			eliminarFrecuente();
		}
	}

	void operarFrecuente() {
		if (listaFrecuentes.getOpcionSeleccionada() != -1) {
			delegate.operarFrecuente(listaFrecuentes.getOpcionSeleccionada());	
			//AMZ
			Map<String,Object> InicioReducidoMap = new HashMap<String, Object>();
			if (delegate instanceof InterbancariosDelegate) {
				//AMZ
				InicioReducidoMap.put("evento_inicio","event45");
				InicioReducidoMap.put("&&products","operaciones;transferencias+otros bancos");
				InicioReducidoMap.put("eVar12","inicio_reducido");
				TrackingHelper.trackInicioOperacion(InicioReducidoMap);
			} else if (delegate instanceof PagoServiciosDelegate) {
				//AMZ
				InicioReducidoMap.put("evento_inicio","event45");
				InicioReducidoMap.put("&&products","operaciones;pagar+servicio");
				InicioReducidoMap.put("eVar12","inicio_reducido");
				TrackingHelper.trackInicioOperacion(InicioReducidoMap);
			} else if (delegate instanceof TiempoAireDelegate) {
				//AMZ
				InicioReducidoMap.put("evento_inicio","event45");
				InicioReducidoMap.put("&&products","operaciones;comprar+tiempo aire");
				InicioReducidoMap.put("eVar12","inicio_reducido");
				TrackingHelper.trackInicioOperacion(InicioReducidoMap);
			} else if (delegate instanceof OtrosBBVADelegate){
				//AMZ
				if(((OtrosBBVADelegate) delegate).isEsExpress()){
					InicioReducidoMap.put("evento_inicio","event45");
					InicioReducidoMap.put("&&products","operaciones;transferencias+cuenta express");
					InicioReducidoMap.put("eVar12","inicio_reducido");
				}else{
					InicioReducidoMap.put("evento_inicio","event45");
					InicioReducidoMap.put("&&products","operaciones;transferencias+otra cuenta bbva bancomer");
					InicioReducidoMap.put("eVar12","inicio_reducido");
				}
				TrackingHelper.trackInicioOperacion(InicioReducidoMap);
			} else if (delegate instanceof DineroMovilDelegate){
				//AMZ
				InicioReducidoMap.put("evento_inicio","event45");
				InicioReducidoMap.put("&&products","operaciones;transferencias+dinero movil");
				InicioReducidoMap.put("eVar12","inicio_reducido");
				TrackingHelper.trackInicioOperacion(InicioReducidoMap);
			}
		} else {
			showInformationAlert(R.string.bmovil_tipo_consulta_frecuentes_alerta_operar);
		}
	}

	void eliminarFrecuente() {
		if (listaFrecuentes.getOpcionSeleccionada() != -1) {

			delegate.eliminarFrecuente(listaFrecuentes.getOpcionSeleccionada());

		} else {
			showInformationAlert(R.string.bmovil_tipo_consulta_frecuentes_alerta_eliminar);
		}

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public void goBack() {
		// parentViewsController.removeDelegateFromHashMap(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
		super.goBack();
	}


	public void muestraFrecuentes() {
		//ArrayList<Object> listaEncabezado = null;
		ArrayList<Object> listaDatos = null;
		listaDatos = delegate.getDatosTablaFrecuentes();

		if (listaDatos.size() > 0) {
			listaFrecuentes.setTextoAMostrar(null);
			listaFrecuentes.setLista(listaDatos);
			listaFrecuentes.setNumeroColumnas(((ArrayList<Object>) listaDatos.get(0)).size() - 1) ;
			listaFrecuentes.setSeleccionable(true);
			listaFrecuentes.setAlturaFija(true);
			listaFrecuentes.setNumeroFilas(listaDatos.size());
			listaFrecuentes.getFiltroLista().setListaOriginal(listaDatos);
			listaFrecuentes.setSingleLine(true);
			listaFrecuentes.cargarTabla();
			if (delegate instanceof PagoServiciosDelegate)
			listaFrecuentes.setOverridedColumnWidths(new float[]{0.85f});
		}else {
			listaFrecuentes.setLista(listaDatos);
			listaFrecuentes.setAlturaFija(true);
			listaFrecuentes.setNumeroFilas(listaDatos.size());
			listaFrecuentes.setTextoAMostrar(getString(R.string.bmovil_consultar_frecuentes_emptylist));
			if (delegate instanceof PagoServiciosDelegate)
			listaFrecuentes.setOverridedColumnWidths(new float[]{1.0f});
			listaFrecuentes.cargarTabla();
		}

	}

	public ListaSeleccionViewController getListaFrecuentes() {
		return listaFrecuentes;
	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if(MotionEvent.ACTION_DOWN == ev.getAction()) {
			if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Touch Event Action: ACTION_DOWN");
			listaFrecuentes.setMarqueeEnabled(false);
		}else if(MotionEvent.ACTION_UP == ev.getAction()) {
			if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Touch Event Action: ACTION_UP");
			listaFrecuentes.setMarqueeEnabled(true);
		}

		return super.dispatchTouchEvent(ev);
	}
}
