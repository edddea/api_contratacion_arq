package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.HashMap;
import java.util.Map;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.InputFilter;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.StartBmovilInBack;
import com.bancomer.mbanking.SuiteApp;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ExitoILCDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuAdministrarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import tracking.TrackingHelper;

public class ExitoOfertaILCViewController extends BaseViewController {
	private ExitoILCDelegate exitoILCDelegate;
	LinearLayout vista;
	BaseViewController me;
	private TextView lblTextoOferta;
	private TextView lblTextoImporte;
	private TextView lblTextoAyudaOferta;
	private TextView lblCorreoOferta;
	private EditText editTextemail;
	private TextView lblsmsOferta;
	private EditText editTextNumero;
	private ImageButton btnComprobante;
	private ImageButton btnmMenu;
	private LinearLayout layout_btnpromocion;
	private Button btnClickpromociones;
	private TextView txtofertasclic;
	private ImageView imgIconoclic;
	//AMZ
	private BmovilViewsController parentManager;
	
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_acepta_oferta_ilc);
		me=this;
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("exito ilc", parentManager.estados);
		
		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		exitoILCDelegate = (ExitoILCDelegate) parentViewsController.getBaseDelegateForKey(ExitoILCDelegate.EXITO_OFERTA_DELEGATE);	
		exitoILCDelegate.setcontroladorExitoILCView(this);
		setTitle(R.string.bmovil_pantallailc_title, R.drawable.icono_pagar_servicios);
		vista = (LinearLayout)findViewById(R.id.aceptaofertaILC_view_controller_layout);
		layout_btnpromocion = (LinearLayout)findViewById(R.id.layout_btnpromocion);

		
		lblTextoOferta = (TextView) findViewById(R.id.lblTextoOferta);
		lblTextoImporte = (TextView) findViewById(R.id.lblTextoImporte);
		lblTextoAyudaOferta = (TextView) findViewById(R.id.lblTextoAyudaOferta);
		lblCorreoOferta = (TextView) findViewById(R.id.lblCorreoOferta);
		lblsmsOferta = (TextView) findViewById(R.id.lblsmsOferta);
		
		
		editTextNumero=(EditText)findViewById(R.id.editTextNumero);
		editTextemail=(EditText) findViewById(R.id.editTextemail);
		txtofertasclic=(TextView)findViewById(R.id.txtofertasclic);
		imgIconoclic=(ImageView)findViewById(R.id.imgIconoclic);
		btnComprobante = (ImageButton)findViewById(R.id.btnComprobante);
		btnComprobante.setOnClickListener(clickListener);
		btnmMenu = (ImageButton)findViewById(R.id.btnMenu);
		btnmMenu.setOnClickListener(clickListener);
		
		if(exitoILCDelegate.getAceptaiOfertaILC().getPromocion()!=null){
			btnClickpromociones= new Button(this);
			String cveCamp=exitoILCDelegate.getAceptaiOfertaILC().getPromocion().getCveCamp().substring(0,4);
			
			DisplayMetrics metrics = getResources().getDisplayMetrics();
			float density = metrics.density;
			int w = (int) (250 * density);
			int h = (int) (55* density);
			LayoutParams lyParam = new LayoutParams(w,h);
			lyParam.gravity= Gravity.CENTER;
			btnClickpromociones.setText(Tools.formatAmount(exitoILCDelegate.getAceptaiOfertaILC().getPromocion().getMonto(),false));
			/*String prueba=Tools.formatAmount("99999900",false);
			btnClickpromociones.setText(prueba);*/
			btnClickpromociones.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10);
			btnClickpromociones.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
			if(cveCamp.equals("0130")){
				btnClickpromociones.setBackgroundResource(R.drawable.anbtnilc_promo);
				btnClickpromociones.setTextColor(Color.rgb(246,137,30));
				int t=(int)(10*density);
				int r=(int)(55*density);
				btnClickpromociones.setPadding(0,t,r,0);
			}else if(cveCamp.equals("0377")){
				btnClickpromociones.setBackgroundResource(R.drawable.anbtn_efi_promocion);
				btnClickpromociones.setTextColor(Color.rgb(62,182,187));
				int t=(int)(10*density);
				int r=(int)(75*density);
				btnClickpromociones.setPadding(0,t,r,0);
			}
			btnClickpromociones.setLayoutParams(lyParam);
			btnClickpromociones.setOnClickListener(clickListener);
			GuiTools guiTools = GuiTools.getCurrent();
			guiTools.init(getWindowManager());
			guiTools.scale(btnClickpromociones, true);
			layout_btnpromocion.addView(btnClickpromociones);
		}else{
			layout_btnpromocion.setVisibility(LinearLayout.GONE);	
		}
		configurarPantalla();
		
		lblTextoImporte.setText(Tools.formatAmount(exitoILCDelegate.getAceptaiOfertaILC().getLineaFinal().replace(",",""), false));
		
		InputFilter[] FilterArray = new InputFilter[1];
    	FilterArray[0] = new InputFilter.LengthFilter(Constants.NUMERO_TELEFONO_LENGTH);
    	editTextNumero.setFilters(FilterArray);
    	if(exitoILCDelegate.getAceptaiOfertaILC().getEmail().compareTo("")==0){    		
    		lblTextoAyudaOferta.setText(R.string.bmovil_texto_extoILC3);
    		lblCorreoOferta.setVisibility(View.GONE);   		
    		editTextemail.setVisibility(View.GONE);
    		
    	}else{
    		
    		lblTextoAyudaOferta.setText(R.string.bmovil_texto_extoILC2);
    		editTextemail.setText(exitoILCDelegate.getAceptaiOfertaILC().getEmail());
    		editTextemail.setEnabled(false);
    		editTextemail.setFocusable(false);
    	}
		Session session = Session.getInstance(SuiteApp.appContext);
		String user = session.getUsername();
		editTextNumero.setText(user);
		editTextNumero.setEnabled(false);
		
		editTextNumero.setFocusable(false);
	
	}
	
	OnClickListener clickListener=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			//AMZ
			//Map<String,Object> OperacionMap = new HashMap<String, Object>();
			Map<String,Object> click_bannerMap = new HashMap<String, Object>();
			// TODO Auto-generated method stub
			if(v==btnComprobante){
				exitoILCDelegate.showResultados();
			}else if(v==btnmMenu){
				if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session
					SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
					ActivityCompat.finishAffinity(me);

					Intent i=new Intent(me, StartBmovilInBack.class);
					i.putExtra(bancomer.api.common.commons.Constants.RESULT_STRING, Constants.TRUE_STRING);
					i.putExtra( bancomer.api.common.commons.Constants.TIMEOUT_STRING, Constants.FALSE_STRING);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
					finish();
				}else {
					exitoILCDelegate.showMenu();
				}
			}else if(v==btnClickpromociones){
				click_bannerMap.put("evento_banner", "event32");
				click_bannerMap.put("products", "oferta cruzada efi");
				click_bannerMap.put("eVar12", "promociones:detalle ilc:exito ilc");
				TrackingHelper.trackClickBanner(click_bannerMap, click_bannerMap);
				
				exitoILCDelegate.showDetallePromocion();
			}
		}
	};
	
	private void configurarPantalla() {
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(lblTextoOferta, true);
		gTools.scale(lblTextoImporte, true);
		gTools.scale(lblTextoAyudaOferta, true);
		gTools.scale(lblCorreoOferta, true);
		gTools.scale(lblsmsOferta, true);
		gTools.scale(editTextemail,true);
		gTools.scale(editTextNumero,true);
		gTools.scale(btnComprobante);
		gTools.scale(btnmMenu);
		gTools.scale(txtofertasclic,true);
		gTools.scale(imgIconoclic);
		gTools.scale(layout_btnpromocion);
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);

	}
	
	@Override
	protected void onPause() {
		super.onPause();
			parentViewsController.consumeAccionesDePausa();
		
	}
	
	public void goBack() {
		// TODO Auto-generated method stub
	}
		
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		exitoILCDelegate.analyzeResponse(operationId, response);
	}
	
	
	
}
