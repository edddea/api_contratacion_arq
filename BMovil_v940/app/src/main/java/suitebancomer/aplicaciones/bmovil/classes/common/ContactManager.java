package suitebancomer.aplicaciones.bmovil.classes.common;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;
import android.util.Log;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.DineroMovilViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

/**
 * Clase que obtiene los datos de un contacto para la version 2.0 de Android (Api 5) o posteriores.
 */
@TargetApi(Build.VERSION_CODES.ECLAIR)
public class ContactManager extends AbstractContactMannager {
	/**
	 * Constructor por defecto.
	 */
	public ContactManager() {
		super();
	}

	@Override
	public void getContactData(Activity callerActivity, Intent data) {
		String telefono = "";
	    String contacto = "";
	    Cursor c = null;
		
	    if(Server.ALLOW_LOG) Log.v(this.getClass().getSimpleName(), data.getData().getLastPathSegment());
	    
		c = callerActivity.getContentResolver().query(android.provider.ContactsContract.CommonDataKinds.Phone.CONTENT_URI, 
				   null, 
				   android.provider.ContactsContract.CommonDataKinds.Phone._ID + "=" + data.getData().getLastPathSegment(), 
				   null, 
				   null);

		int phoneIdx = c.getColumnIndex(android.provider.ContactsContract.CommonDataKinds.Phone.NUMBER);
		int nameIdx = c.getColumnIndex(android.provider.ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
		
		if(Server.ALLOW_LOG) Log.v(this.getClass().getSimpleName(), "El cursor tiene " + c.getCount() + " resultados.");
		
		if(c.moveToFirst()) {
			telefono = c.getString(phoneIdx);
			contacto = c.getString(nameIdx);
			if(Server.ALLOW_LOG) Log.v("ContactManager", "Telefono seleccionado: " + telefono);
		} else {
			if(Server.ALLOW_LOG) Log.w("ContactManager", "No results");
		}
		
		telefono = Tools.formatPhoneNumberFromContact(telefono);
		
		if(Constants.TELEPHONE_NUMBER_LENGTH == telefono.length()) {
	    	this.numeroDeTelefono = telefono;
	    	this.nombreContacto = contacto;
	    } else {
	    	this.numeroDeTelefono = "";
	    	this.nombreContacto = "";
	    }
	}

	@Override
	public void requestContactData(Activity callerActivity) {
		Intent intent;
		intent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
		callerActivity.startActivityForResult(intent, DineroMovilViewController.ContactsRequestCode);
	}
}
