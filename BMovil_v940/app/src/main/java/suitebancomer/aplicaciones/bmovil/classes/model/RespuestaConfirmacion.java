package suitebancomer.aplicaciones.bmovil.classes.model;

public class RespuestaConfirmacion {
	// #region Variables.
	/**
	 * Contraseña de bmovil.
	 */
	private String pwd;
	
	/**
	 * Cvv de la tarjeta.
	 */
	private String cvv;
	
	/**
	 * Nip del cajero.
	 */
	private String nip;
	
	/**
	 * Token de seguridad.
	 */
	private String otp;
	
	/**
	 * tarjeta num
	 */
	private String numTarjeta;
	// #endregion
	
	// #region Getters y Setters.
	/**
	 * @return Contraseña de bmovil.
	 */
	public String getPwd() {
		return pwd;
	}

	/**
	 * @param pwd Contraseña de bmovil.
	 */
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	/**
	 * @return Cvv de la tarjeta.
	 */
	public String getCvv() {
		return cvv;
	}

	/**
	 * @param cvv Cvv de la tarjeta.
	 */
	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	/**
	 * @return Nip del cajero.
	 */
	public String getNip() {
		return nip;
	}

	/**
	 * @param nip Nip del cajero.
	 */
	public void setNip(String nip) {
		this.nip = nip;
	}

	/**
	 * @return Token de seguridad.
	 */
	public String getOtp() {
		return otp;
	}

	/**
	 * @param otp Token de seguridad.
	 */
	public void setOtp(String otp) {
		this.otp = otp;
	}
	
	public String getNumTarjeta() {
		return numTarjeta;
	}
	
	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}
	// #endregion
	
	/**
	 * Constructor básico, crea un objeto con valores nulos.
	 */
	public RespuestaConfirmacion() {
		pwd = null;
		cvv = null;
		nip = null;
		otp = null;
		numTarjeta = null;
	}

	/**
	 * Contructor.
	 * @param pwd Contraseña de bmovil.
	 * @param cvv Cvv de la tarjeta.
	 * @param nip Nip del cajero.
	 * @param otp Token de seguridad.
	 * @param numTarjeta TODO
	 */
	public RespuestaConfirmacion(String pwd, String cvv, String nip, String otp, String numTarjeta) {
		super();
		this.pwd = pwd;
		this.cvv = cvv;
		this.nip = nip;
		this.otp = otp;
		this.numTarjeta = numTarjeta;
	}

}
