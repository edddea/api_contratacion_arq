package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.model.Rapido;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class ConsultaRapidosDelegate extends BaseDelegate {
	// #region Variables.
	/**
	 * Identificador para el delegado.
	 */
	public static final long CONSULTA_RAPIDOS_DELEGATE_ID = -2749987068355262540L;

	/**
	 * Controlador actual que consume las acciones de este delegado.
	 */
	private BaseViewController ownerController;
	// #endregion

	// #region Getters y Setters
	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#getDelegateIdentifier()
	 */
	@Override
	public long getDelegateIdentifier() {
		return CONSULTA_RAPIDOS_DELEGATE_ID;
	}

	/**
	 * @return Controlador actual que consume las acciones de este delegado.
	 */
	public BaseViewController getOwnerController() {
		return ownerController;
	}

	/**
	 * @param ownerController Controlador actual que consume las acciones de este delegado.
	 */
	public void setOwnerController(BaseViewController ownerController) {
		this.ownerController = ownerController;
	}
	// #endregion

	public ArrayList<Object> cargarDatosListaSeleccion() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		Rapido[] rapidos = Session.getInstance(SuiteApp.appContext).getRapidos();
		
		if(null != rapidos) {
			for(Rapido rapido : rapidos) {
				fila = new ArrayList<String>();
				
				fila.add("");
				fila.add(rapido.getNombreCorto());
				fila.add(rapido.getNombreOperacionAMostrar());
				fila.add(Tools.formatToCurrencyAmount(rapido.getImporte()));
				
				tabla.add(fila);
			}
		}
		
		return tabla;
	}
	
	public ArrayList<Object> cargarEncabezadosListaSeleccion() {
		ArrayList<Object> registros = new ArrayList<Object>();
		registros.add("");
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_rapidos_titulo_lista_seleccion_encabezado1));
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_rapidos_titulo_lista_seleccion_encabezado2));
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_rapidos_titulo_lista_seleccion_encabezado3));
		return registros;
	}
	
	public void operarRapido(int selectedIndex) {
		BmovilViewsController bmovilViewsController = (BmovilViewsController)ownerController.getParentViewsController();
		Rapido selected = Session.getInstance(SuiteApp.appContext).getRapidos()[selectedIndex];
		
		if(Constants.RAPIDOS_CODIGO_OPERACION_OTROS_BBVA.equals(selected.getTipoRapido())) {
			bmovilViewsController.showOtrosBBVAViewController(selected);
		} else if(Constants.RAPIDOS_CODIGO_OPERACION_TIEMPO_AIRE.equals(selected.getTipoRapido())) {
			bmovilViewsController.showTiempoAireViewController(selected);
		} else if(Constants.RAPIDOS_CODIGO_OPERACION_DINERO_MOVIL.equals(selected.getTipoRapido())) {
			bmovilViewsController.showTransferirDineroMovil(selected);
		}
	}
	
	// #region
	// #endregion
	// #region
	// #endregion
	// #region
	// #endregion
}
