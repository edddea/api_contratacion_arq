package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import bancomer.api.common.commons.Constants;
import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.OpcionesPagosViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.classes.gui.controllers.BaseViewController;

public class PagarDelegate extends DelegateBaseAutenticacion {

	public final static long PAGAR_DELEGATE_ID = 0x3297680b39ea662dL;

	OpcionesPagosViewController controladorPagos;
	private String tipoOperacion;

	private ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController;

	public ConfirmacionAutenticacionViewController getConfirmacionAutenticacionViewController(){
		return confirmacionAutenticacionViewController;
	}

	public void setConfirmacionAutenticacionViewController(ConfirmacionAutenticacionViewController c){
		this.confirmacionAutenticacionViewController = c;
	}

	public BaseViewController getControladorPagos() {
		return controladorPagos;
	}

	public void setControladorPagos(OpcionesPagosViewController controladorPagos) {
		this.controladorPagos = controladorPagos;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void performAction(Object obj) {
		controladorPagos.opcionSeleccionada((String)obj);
	}

	/*
	 * M�todo que valida que opciones se mostr�n en la lista selecci�n
	 * dependiendo del caso en el que entre que puede ser Transferir, comprar,
	 * pagar o cunsultar.
	 */
	public ArrayList<Object> cargarListasMenu() {
		ArrayList<Object> listaOpcionesMenu = new ArrayList<Object>();
		ArrayList<Object> registros;
		Session session = Session.getInstance(SuiteApp.getInstance());
		if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.pagoServicios,	session.getClientProfile())) {
			registros = new ArrayList<Object>(2);
			registros.add(Constants.SERVICE_PAYMENT_TYPE);
			registros.add(SuiteApp.appContext.getString(R.string.pagar_service));
			listaOpcionesMenu.add(registros);
		}
		if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.transferirBancomer,	session.getClientProfile())) {
			registros = new ArrayList<Object>(2);
			registros.add(Constants.INTERNAL_TRANSFER_CREDIT_TYPE);
			registros.add(SuiteApp.appContext.getString(R.string.pagar_creditCard));
			listaOpcionesMenu.add(registros);
		}
		// TODO: opcion nueva
		registros = new ArrayList<Object>(2);
		registros.add(Constants.OPERACION_PAGAR_OTRAS_TDC);
		registros.add(SuiteApp.appContext.getString(R.string.pagar_otherCreditCard));
		listaOpcionesMenu.add(registros);


		registros = new ArrayList<Object>(2);
		registros.add(Constants.OPERACION_PAGAR_OTROS_CREDITOS);
		registros.add(SuiteApp.appContext.getString(R.string.pagar_misCreditos));
		listaOpcionesMenu.add(registros);
		return listaOpcionesMenu;
	}

	/**
	    * Check if the current server date is between the available
	    * operation time, which is "hard coded" to Monday to Friday
	    * from 6:00 to 17:20
	    * @return true if the current server date is between the
	    * available operation hours, false if not
	    */
	   public boolean isOpenHours() {
	       return Session.getInstance(SuiteApp.appContext).isOpenHoursForServicePayments();
	   }

	/**
	 * Displays a text that indicates that external transfers cannot be
	 * performed during current time.
	 */
	public String textoMensajeAlerta() {
		return Session.getInstance(SuiteApp.appContext)
				.getOpenHoursForServicePaymentsMessage();
	}

	/**
	 * Invoca la operacion para hacer el cambio de cuenta asociada
	 */
	@Override
	public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController, String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
		confirmacionAutenticacionViewController.muestraIndicadorActividad(confirmacionAutenticacionViewController.getString(R.string.alert_operation), confirmacionAutenticacionViewController.getString(R.string.alert_connecting));
		setConfirmacionAutenticacionViewController(confirmacionAutenticacionViewController);
		// Llamada al modulo de Consulta Creditos
		Session session = Session.getInstance(SuiteApp.getInstance());
		// Inicializamos la clase de configuracion
		InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance(controladorPagos);//(menuConsultarViewController, ((BmovilViewsController)menuConsultarViewController.getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient());
		// Establecemos los parametros necesarios
		init.getConsultaSend().setIUM(session.getIum());
		init.getConsultaSend().setUsername(session.getUsername());
		init.getConsultaSend().setCallBackModule(controladorPagos);
		init.getConsultaSend().setCveAcceso(contrasenia == null ? "" : contrasenia);
		init.getConsultaSend().setTarjeta5Dig(campoTarjeta == null ? "" : campoTarjeta);
		init.getConsultaSend().setCodigoNip(nip == null ? "" : nip);
		init.getConsultaSend().setCodigoCvv2(cvv == null ? "" : cvv);
		init.getConsultaSend().setCodigoOtp(token == null ? "" : token);
		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultarCreditos, session.getClientProfile());
		init.getConsultaSend().setCadenaAutenticacion(cadenaAutenticacion);
		init.getConsultaSend().setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR);
		init.setActivity(controladorPagos);
		// Realizamos la llamada a Consulta Otros Creditos
		((BmovilViewsController)controladorPagos.getParentViewsController()).setCurrentActivityApp(confirmacionAutenticacionViewController);
		((BmovilViewsController)controladorPagos.getParentViewsController()).setActivityChanging(true);
		init.preShowConsultarOtrosCreditos();

	}

	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {

		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;

		fila = new ArrayList<String>();
		fila.add("Consulta");
		fila.add("Creditos Otorgados");
		tabla.add(fila);
		fila = new ArrayList<String>();
		fila.add("Fecha de operación");

		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		fila.add(formatter.format(date));

		tabla.add(fila);

		return tabla;
	}

	@Override
	public boolean mostrarContrasenia() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.consultarCreditos,
				perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarCVV(Constants.Operacion.consultarCreditos, perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.consultarCreditos,
				perfil);
		return value;
	}

	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public Constants.TipoOtpAutenticacion tokenAMostrar() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.consultarCreditos,
					perfil);
		} catch (Exception ex) {
			Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
			tipoOTP = null;
		}
		return tipoOTP;
	}

	@Override
	public int getTextoEncabezado() {
		int resTitle;
		resTitle = R.string.bmovil_menu_miscuentas_otroscreditos_otrosCredito;
		return resTitle;
	}

	@Override
	public int getNombreImagenEncabezado() {
		int resIcon = R.drawable.bmovil_configurarmontos_icono;
		return resIcon;
	}


	@Override
	public String getTextoTituloResultado() {
		int txtTitulo = R.string.transferir_detalle_operacion_exitosaTitle;
		return controladorPagos.getString(txtTitulo);
	}


}
