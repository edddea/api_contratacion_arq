package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.security.InvalidParameterException;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.OpcionesTransferViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.TransferViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Payment;
import suitebancomer.aplicaciones.bmovil.classes.model.PaymentExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovil;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaInterbancaria;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaOtrosBBVA;
import suitebancomer.classes.gui.controllers.BaseViewController;

import android.util.Log;


import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
//import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.OpcionesTransferViewController;

public class TransferirDelegate extends DelegateBaseOperacion {

	public final static long TRANSFERIR_DELEGATE_ID = 0x317600e7880d8b66L;

	BaseViewController controladorTransferencias;
	private String tipoOperacion;
	TransferenciaInterbancaria transferenciaInterbancaria;
	TransferenciaOtrosBBVA transferenciaOtrosBBVA;
	private boolean isExpress;
	private boolean isTDC;
	TransferenciaDineroMovil transferenciaDineroMovil;

	public Boolean opcionRetiroSinTarjeta = false;

	private Payment[] frecuentes;

	public TransferenciaInterbancaria getTransferenciaInterbancaria() {
		return transferenciaInterbancaria;
	}

	public void setTransferenciaInterbancaria(
			TransferenciaInterbancaria transferenciaInterbancaria) {
		this.transferenciaInterbancaria = transferenciaInterbancaria;
	}

	public TransferenciaOtrosBBVA getTransferenciaOtrosBBVA() {
		return transferenciaOtrosBBVA;
	}

	public void setTransferenciaOtrosBBVA(TransferenciaOtrosBBVA transferenciaOtrosBBVA) {
		this.transferenciaOtrosBBVA = transferenciaOtrosBBVA;
	}

	public TransferenciaDineroMovil getTransferenciaDineroMovil() {
		return transferenciaDineroMovil;
	}

	public void setTransferenciaDineroMovil(TransferenciaDineroMovil transferenciaDineroMovil) {
		this.transferenciaDineroMovil = transferenciaDineroMovil;
	}

	public BaseViewController getControladorTransferencias() {
		return controladorTransferencias;
	}

	public void setControladorTransferencias(BaseViewController controladorTransferencias) {
		this.controladorTransferencias = controladorTransferencias;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public String getTextoTitulo() {
		return "";
	}

	public Object getListaFrecuentes() {
		return null;
	}

	public Operacion getOperationContext() {
		return Operacion.transferir;
	}

	public boolean isExpress() {
		return isExpress;
	}

	public void setExpress(boolean isExpress) {
		this.isExpress = isExpress;
	}

	public boolean isTDC() {
		return isTDC;
	}

	public void setTDC(boolean isTDC) {
		this.isTDC = isTDC;
	}


	public Object invocaDelegateOperacion() {
		return null;
	}

	public void performAction(Object obj) {
		if (controladorTransferencias instanceof OpcionesTransferViewController && obj instanceof String) {
			((OpcionesTransferViewController) controladorTransferencias).opcionSeleccionada((String) obj);
		} else if (controladorTransferencias instanceof TransferViewController && obj instanceof Account) {
			((TransferViewController) controladorTransferencias).actualizaCombo();
		} else if (obj instanceof Payment) {
			if (Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Eligio un frecuente");
			Payment selectedPayment = (Payment) obj;

			if (selectedPayment.getOperationCode().equals(Constants.tipoCFCExpress)) {
				transferenciaOtrosBBVA.setCuentaDestino(selectedPayment.getBeneficiaryAccount().replaceAll("[^0-9]", ""));
				if (transferenciaOtrosBBVA.getCuentaDestino().length() > 10) {
					selectedPayment.setOperationCode(Constants.tipoCFOtrosBBVA);
				}
			}

			if (selectedPayment.getOperationCode().equals(Constants.tipoCFOtrosBancos)) {
				String typeCard = selectedPayment.getBeneficiaryTypeAccount();
				String nombreBanco = Session.getInstance(SuiteApp.appContext).obtenerNombreBanco(selectedPayment.getBankCode(), typeCard);
				transferenciaInterbancaria.setInstitucionBancaria(selectedPayment.getBankCode());
				transferenciaInterbancaria.setNumeroCuentaDestino(selectedPayment.getBeneficiaryAccount());
				transferenciaInterbancaria.setBeneficiario(selectedPayment.getBeneficiary());
				transferenciaInterbancaria.setReferencia(selectedPayment.getReference());
				transferenciaInterbancaria.setConcepto(selectedPayment.getConcept());
				transferenciaInterbancaria.setImporte(selectedPayment.getAmount());
				transferenciaInterbancaria.setAliasFrecuente(selectedPayment.getNickname());
				transferenciaInterbancaria.setTipoOperacion(Constants.TPTransferInterbancarias);
				transferenciaInterbancaria.setNombreBanco(nombreBanco);
				transferenciaInterbancaria.setTipoCuentaDestino(typeCard);
				//multicanal
				transferenciaInterbancaria.setFrecuenteMulticanal(selectedPayment.getIdToken());
				transferenciaInterbancaria.setIdCanal(selectedPayment.getIdCanal());
				transferenciaInterbancaria.setUsuario(selectedPayment.getUsuario());
				transferenciaInterbancaria.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());

				((TransferViewController) controladorTransferencias).frecuenteSeleccionado(transferenciaInterbancaria, false, false);
			} else if (selectedPayment.getOperationCode().equals(Constants.tipoCFOtrosBBVA)) {
				//TODO llenar desde frecuente)
				transferenciaOtrosBBVA.setCuentaDestino(selectedPayment.getBeneficiaryAccount().replaceAll("[^0-9]", ""));
				transferenciaOtrosBBVA.setBeneficiario(selectedPayment.getBeneficiary());
				transferenciaOtrosBBVA.setImporte(selectedPayment.getAmount());
				transferenciaOtrosBBVA.setAliasFrecuente(selectedPayment.getNickname());
				transferenciaOtrosBBVA.setTipoCuenta(selectedPayment.getBeneficiaryTypeAccount());
				transferenciaOtrosBBVA.setTipoOperacion(null);

				//multicanal
				transferenciaOtrosBBVA.setFrecuenteMulticanal(selectedPayment.getIdToken());
				transferenciaOtrosBBVA.setIdCanal(selectedPayment.getIdCanal());
				transferenciaOtrosBBVA.setUsuario(selectedPayment.getUsuario());
				transferenciaOtrosBBVA.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());

				((TransferViewController) controladorTransferencias).frecuenteSeleccionado(transferenciaOtrosBBVA, false, false);
			} else if (selectedPayment.getOperationCode().equals(Constants.tipoCFCExpress)) {
				transferenciaOtrosBBVA.setCuentaDestino(selectedPayment.getBeneficiaryAccount().replaceAll("[^0-9]", ""));
				transferenciaOtrosBBVA.setBeneficiario(selectedPayment.getBeneficiary());
				transferenciaOtrosBBVA.setImporte(selectedPayment.getAmount());
				transferenciaOtrosBBVA.setAliasFrecuente(selectedPayment.getNickname());
				transferenciaOtrosBBVA.setTipoCuenta(Constants.EXPRESS_TYPE);
				transferenciaOtrosBBVA.setTipoOperacion(Constants.tipoCFCExpress);
				//multicanal
				transferenciaOtrosBBVA.setFrecuenteMulticanal(selectedPayment.getIdToken());
				transferenciaOtrosBBVA.setIdCanal(selectedPayment.getIdCanal());
				transferenciaOtrosBBVA.setUsuario(selectedPayment.getUsuario());

				((TransferViewController) controladorTransferencias).frecuenteSeleccionado(transferenciaOtrosBBVA, true, false);
			} else if (selectedPayment.getOperationCode().equals(Constants.tipoCFTarjetasCredito)) {
				transferenciaOtrosBBVA.setCuentaDestino(selectedPayment.getBeneficiaryAccount().substring(2));
				transferenciaOtrosBBVA.setBeneficiario(selectedPayment.getBeneficiary());
				transferenciaOtrosBBVA.setImporte(selectedPayment.getAmount());
				transferenciaOtrosBBVA.setAliasFrecuente(selectedPayment.getNickname());
				transferenciaOtrosBBVA.setTipoCuenta(Constants.CREDIT_TYPE);
				transferenciaOtrosBBVA.setTipoOperacion(Constants.tipoCFTarjetasCredito);
				//multicanal
				transferenciaOtrosBBVA.setFrecuenteMulticanal(selectedPayment.getIdToken());
				transferenciaOtrosBBVA.setIdCanal(selectedPayment.getIdCanal());
				transferenciaOtrosBBVA.setUsuario(selectedPayment.getUsuario());
				transferenciaOtrosBBVA.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());


				((TransferViewController) controladorTransferencias).frecuenteSeleccionado(transferenciaOtrosBBVA, false, true);
			} else if (selectedPayment.getOperationCode().equals(Constants.tipoCFDineroMovil)) {
				// Cuenta origen???
				transferenciaDineroMovil.setCelularbeneficiario(selectedPayment.getBeneficiaryAccount());
				transferenciaDineroMovil.setNombreCompania(selectedPayment.getOperadora());
				transferenciaDineroMovil.setBeneficiario(selectedPayment.getBeneficiary());
				transferenciaDineroMovil.setConcepto(selectedPayment.getConcept());
				//multicanal
				transferenciaDineroMovil.setFrecuenteMulticanal(selectedPayment.getIdToken());
				transferenciaDineroMovil.setIdCanal(selectedPayment.getIdCanal());
				transferenciaDineroMovil.setUsuario(selectedPayment.getUsuario());
				transferenciaDineroMovil.setAliasFrecuente(selectedPayment.getNickname());
				transferenciaDineroMovil.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());

				((TransferViewController) controladorTransferencias).frecuenteSeleccionado(transferenciaDineroMovil, false, false);
			}

		}
	}

	public void crearModeloDeOperacion() {
		if (tipoOperacion.equals(Constants.Operacion.transferirInterbancaria.value)) {
			transferenciaInterbancaria = new TransferenciaInterbancaria();
		} else if (tipoOperacion.equals(Constants.Operacion.transferirBancomer.value)) {
			transferenciaOtrosBBVA = new TransferenciaOtrosBBVA();
			if (isExpress) {
				transferenciaOtrosBBVA.setTipoCuenta(Constants.EXPRESS_TYPE);
				transferenciaOtrosBBVA.setTipoOperacion(Constants.tipoCFCExpress);
			} else if (isTDC) {
				transferenciaOtrosBBVA.setTipoCuenta(Constants.CREDIT_TYPE);
				transferenciaOtrosBBVA.setTipoOperacion(Constants.tipoCFTarjetasCredito);
			} else {
				transferenciaOtrosBBVA.setTipoCuenta("");
				transferenciaOtrosBBVA.setTipoOperacion(null);
			}
		} else if (tipoOperacion.equals(Constants.Operacion.dineroMovil.value)) {
			transferenciaDineroMovil = new TransferenciaDineroMovil();
		}
	}

	@Override

	public int getOpcionesMenuResultados() {
		return 0;
	}

	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada según sea reguerido.
	 *
	 * @return
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();

		ArrayList<Account> accountsArray = new ArrayList<Account>();
		//Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		//Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccountsIB();
		Account[] accounts;
		if ((tipoOperacion.equals(Constants.Operacion.transferirInterbancaria.value)) && ((profile == Constants.Perfil.avanzado) || (profile == Constants.Perfil.basico))) {//modif 28072015
//		if((tipoOperacion.equals(Constants.Operacion.transferirInterbancaria.value)) && (profile == Constants.Perfil.basico)){//modif 28072015
			if (Server.ALLOW_LOG) System.out.println("Cuentas Spei");
			accounts = Session.getInstance(SuiteApp.appContext).getAccountsIB();
			for (Account acc : accounts) {
				if ((acc.isVisible()) && !acc.getType().equals(Constants.CREDIT_TYPE)) {
					accountsArray.add(acc);
					break;
				}
			}

		} else {
			if (Server.ALLOW_LOG) System.out.println("Cuentas No Spei");
			accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		}

		if (tipoOperacion.equals(Constants.Operacion.dineroMovil.value)) {
			if (profile == Constants.Perfil.avanzado) {
				if (controladorTransferencias instanceof TransferViewController) {
					for (Account acc : accounts) {
						if (acc.isVisible() && !acc.getType().equals(Constants.CREDIT_TYPE)) {
							accountsArray.add(acc);
							break;
						}
					}

					for (Account acc : accounts) {
						if (!acc.isVisible() && !acc.getType().equals(Constants.CREDIT_TYPE))
							accountsArray.add(acc);
					}
				}

			} else {
				for (Account acc : accounts) {
					if (acc.isVisible())
						accountsArray.add(acc);
				}
			}


		} else if (tipoOperacion.equals(Constants.Operacion.transferirBancomer.value)) {
			if (profile == Constants.Perfil.avanzado) {
				if (controladorTransferencias instanceof TransferViewController) {

					for (Account acc : accounts) {
						if (acc.isVisible() && !acc.getType().equals(Constants.CREDIT_TYPE)) {
							accountsArray.add(acc);
							break;
						}
					}

					for (Account acc : accounts) {
						if (!acc.isVisible() && !acc.getType().equals(Constants.CREDIT_TYPE))
							accountsArray.add(acc);
					}
				}
			} else {
				for (Account acc : accounts) {
					if (acc.isVisible() && !acc.getType().equals(Constants.CREDIT_TYPE))
						accountsArray.add(acc);
				}
			}
		} else {
			if (profile == Constants.Perfil.avanzado) {
				if (controladorTransferencias instanceof TransferViewController) {
					for (Account acc : accounts) {
						if (acc.isVisible()) {
							accountsArray.add(acc);
							break;
						}
					}

					for (Account acc : accounts) {
						if (!acc.isVisible())
							accountsArray.add(acc);
					}
				}

			} else {
				for (Account acc : accounts) {
					if (acc.isVisible())
						accountsArray.add(acc);
				}
			}

		}
		return accountsArray;
	}

	/**
	 * Set the source account for the transaction.
	 *
	 * @param sourceAccount The source account
	 * @throws InvalidParameterException If the source account is null.
	 */
	public void setCuentaSeleccionada(Account sourceAccount) throws InvalidParameterException {
		if (null == sourceAccount) {
			InvalidParameterException ex = new InvalidParameterException("The source account can not be null.");
			if (Server.ALLOW_LOG)
				Log.e("TransferirMisCuentasDelegate", "The source account can not be null.", ex);
			throw ex;
		} else {
			if (tipoOperacion.equals(Constants.Operacion.transferirInterbancaria.value)) {
				transferenciaInterbancaria.setCuentaOrigen(sourceAccount);
			} else if (tipoOperacion.equals(Constants.Operacion.transferirBancomer.value)) {
				transferenciaOtrosBBVA.setCuentaOrigen(sourceAccount);
			} else if (tipoOperacion.equals(Constants.Operacion.dineroMovil.value)) {
				transferenciaDineroMovil.setCuentaOrigen(sourceAccount);
			}

		}
	}

	/*
     * �todo para cargar la lista de frecuentes
     */
/*	public ArrayList<Frecuente> cargaFrecuentes(){

	}
*/
	/*
	 * M�todo que valida que opciones se mostr�n en la lista selecci�n dependiendo del caso en el
	 * que entre que puede ser Transferir, comprar, pagar o cunsultar.
	 */
	public ArrayList<Object> cargarListasMenu() {
		ArrayList<Object> listaOpcionesMenu = new ArrayList<Object>();
		ArrayList<Object> registros;
		Session session = Session.getInstance(SuiteApp.getInstance());
		if (tipoOperacion.equals(Operacion.transferir.value)) {
			if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.transferirPropias, session.getClientProfile())) {

				registros = new ArrayList<Object>(2);
				registros.add(Constants.OPERACION_TRANSFERENCIA_MIS_CUENTAS_TYPE);
				registros.add(SuiteApp.appContext.getString(R.string.opcionesTransfer_menu_miscuentas));
				listaOpcionesMenu.add(registros);
			}
			if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.transferirBancomer, session.getClientProfile())) {
				registros = new ArrayList<Object>(2);
				registros.add(Constants.INTERNAL_TRANSFER_DEBIT_TYPE);
				registros.add(SuiteApp.appContext.getString(R.string.opcionesTransfer_menu_otrascuentasbbva));
				listaOpcionesMenu.add(registros);
			}
			/*if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.transferirBancomer,	session.getClientProfile())) {
			registros = new ArrayList<Object>(2);
			registros.add(Constants.INTERNAL_TRANSFER_EXPRESS_TYPE);
			registros.add(SuiteApp.appContext.getString(R.string.opcionesTransfer_menu_cuentaexpress));
			listaOpcionesMenu.add(registros);
			}*/
			if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.transferirInterbancaria, session.getClientProfile())) {
				registros = new ArrayList<Object>(2);
				registros.add(Constants.EXTERNAL_TRANSFER_TYPE);
				registros.add(SuiteApp.appContext.getString(R.string.opcionesTransfer_menu_otrosbancos));
				listaOpcionesMenu.add(registros);
			}
			if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.dineroMovil, session.getClientProfile())) {
				registros = new ArrayList<Object>(2);
				registros.add(Constants.OPERACION_EFECTIVO_MOVIL_TYPE);
				registros.add(SuiteApp.appContext.getString(R.string.opcionesTransfer_menu_dineromovil));
				listaOpcionesMenu.add(registros);
			}
			if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.retiroSinTarjeta, session.getClientProfile())) {
				registros = new ArrayList<Object>(2);
				registros.add(Constants.OPERACION_RETIRO_SINTARJETA);
				registros.add(SuiteApp.appContext.getString(R.string.opcionesTransfer_menu_retirosintarjeta));
				listaOpcionesMenu.add(registros);
			}
		} else {
			/*
			 * se validar�n las opciones que se mostraran para comprar, pagar y consultar
			 */
		}
		return listaOpcionesMenu;
	}


	/**
	 * Check if the current server date is between the available
	 * operation time, which is "hard coded" to Monday to Friday
	 * from 6:00 to 17:20
	 *
	 * @return true if the current server date is between the
	 * available operation hours, false if not
	 */
	public boolean isOpenHours() {
		return Session.getInstance(SuiteApp.getInstance()).isOpenHoursForExternalTransfers();
	}

	public boolean isOpenHoursTC() {
		return Session.getInstance(SuiteApp.getInstance()).isOpenHoursForExternalTransfersUsingTC();
	}

	/**
	 * Displays a text that indicates that external transfers cannot be performed
	 * during current time.
	 */
	public String textoMensajeAlerta() {
		return Session.getInstance(SuiteApp.getInstance()).getOpenHoursForExternalTransfersMessage();
	}

	public String textoMensajeAlertaTC() {
		return Session.getInstance(SuiteApp.getInstance()).getOpenHoursForExternalTransfersMessageUsingTC();
	}

	public void consultarFrecuentes(String tipoFrecuente) {
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> paramTable = null;
		paramTable = new Hashtable<String, String>();
		if("01".equals(tipoFrecuente)){
			paramTable.put("numeroTelefono", 	session.getUsername());//NT
			paramTable.put("IUM",			session.getIum());//IU
			paramTable.put("numeroCliente",session.getClientNumber());//TE
			if (controladorTransferencias instanceof TransferViewController) {
//			doNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable, controladorTransferencias);
				//JAIG CHECK
				((BmovilViewsController) controladorTransferencias.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION_BBVA, paramTable, true,new PaymentExtract(),isJsonValueCode.NONE, controladorTransferencias, true);
			}
		}else {

			paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());//NT
			paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());//IU
			paramTable.put("TE", session.getClientNumber());//TE
			if (isExpress) {
				paramTable.put("TP", Constants.tipoCFCExpress);//TP
			} else if (isTDC) {
				paramTable.put("TP", Constants.tipoCFTarjetasCredito);//TP
			} else {
				paramTable.put("TP", tipoFrecuente);//TP
			}
			frecuentes = null;
			paramTable.put("AP", "");
			//freceuntes correo
			paramTable.put("VM", Constants.APPLICATION_VERSION);

			if (controladorTransferencias instanceof TransferViewController) {
//			doNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable, controladorTransferencias);
				//JAIG CHECK
				((BmovilViewsController) controladorTransferencias.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable, false, new PaymentExtract(), isJsonValueCode.NONE, controladorTransferencias, true);
			}
		}

	}


	@Override
	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson, ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if (controladorTransferencias instanceof TransferViewController) {
			((BmovilViewsController) controladorTransferencias.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode, caller);
		}
		if (controladorTransferencias instanceof OpcionesTransferViewController) {
			((BmovilViewsController) controladorTransferencias.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode, caller, true);
		}

	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {

			if (operationId == Server.OP_SOLICITAR_ALERTAS) {
				SolicitarAlertasData validacionalertas = (SolicitarAlertasData) response.getResponse();
				if (Constants.ALERT02.equals(validacionalertas.getValidacionAlertas())) {
					// 02 no tiene alertas

					SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
					if (opcionRetiroSinTarjeta) {
						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_retiroSinTarjeta_recortado_sin_alertas));

					} else {
						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_dineromovil_recortado_sin_alertas));

					}
				} else {

					setSa(validacionalertas);
					analyzeAlertasRecortadoSinError();
				}
				return;
			}

			if (response.getResponse() instanceof PaymentExtract) {
				PaymentExtract extract = (PaymentExtract) response.getResponse();
				setFrecuentes(extract.getPayments());
				mostrarListaFrecuentes();
			}

		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
			if (operationId != Server.FAVORITE_PAYMENT_OPERATION)
				mostrarAlert(response.getMessageText());

		} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
			if (operationId == Server.FAVORITE_PAYMENT_OPERATION) {
				frecuentes = null;
				mostrarListaFrecuentes();
			}
		}
	}

	/**
	 * Muestra una alerta por pantalla
	 */
	private void mostrarAlert(String mensaje) {
		((BmovilViewsController) controladorTransferencias.getParentViewsController())
				.getCurrentViewControllerApp().showInformationAlert(
				mensaje);
	}

	private void mostrarListaFrecuentes() {
		((TransferViewController) controladorTransferencias).muestraFrecuentes();

	}

	public Payment[] getFrecuentes() {
		return frecuentes;
	}

	public void setFrecuentes(Payment[] frecuentes) {
		this.frecuentes = frecuentes;
	}

	public ArrayList<Object> getDatosTablaFrecuentes() {
		ArrayList<Object> listaDatos = new ArrayList<Object>();
		ArrayList<Object> registro;

		if (tipoOperacion.equals(Constants.Operacion.dineroMovil.value)) {
			Payment este = new Payment();
			este.setNK(SuiteApp.appContext.getString(R.string.bmovil_common_este_celular));
			este.setBeneficiary(SuiteApp.appContext.getString(R.string.bmovil_common_titular_de_la_cuenta));
			este.setBeneficiaryAccount(Session.getInstance(SuiteApp.appContext).getUsername());
			este.setConcept("");
			este.setAmount("0.0");
			este.setOperadora(Session.getInstance(SuiteApp.appContext).getCompaniaUsuario());
			este.setOperationCode(Constants.tipoCFDineroMovil);
			este.setIdToken(este.getNickname());


			registro = new ArrayList<Object>();
			registro = new ArrayList<Object>();
			registro.add(este);
			registro.add(este.getNickname());
			registro.add(este.getBeneficiaryAccount());
			listaDatos.add(registro);
		}

		if (frecuentes != null) {
			for (Payment payment : frecuentes) {
				registro = new ArrayList<Object>();
				registro.add(payment);
				registro.add(payment.getNickname());
				if (tipoOperacion.equals(Constants.Operacion.transferirInterbancaria.value)) {
					String typeCard = payment.getBeneficiaryTypeAccount();
					String nombreBanco;
					if(payment.getBankCode()!=null)
					{
						nombreBanco = Session.getInstance(SuiteApp.appContext).obtenerNombreBanco(payment.getBankCode(), typeCard);
					}
					else
					{
						nombreBanco = "";
					}
					if (Tools.isEmptyOrNull(nombreBanco))
						continue;
					registro.add(nombreBanco);
					registro.add(Tools.hideAccountNumber(payment.getBeneficiaryAccount()));
				} else if (tipoOperacion.equals(Constants.Operacion.transferirBancomer.value)) {
					if (isExpress) {
						registro.add(payment.getBeneficiaryAccount());
					} else {
						registro.add(Tools.hideAccountNumber(payment.getBeneficiaryAccount()));
					}
				} else if (tipoOperacion.equals(Constants.Operacion.dineroMovil.value)) {
					registro.add(payment.getBeneficiaryAccount());
				}

				listaDatos.add(registro);
			}
		}
		return listaDatos;
	}

	public void comprobarRecortado() {
//		Session session = Session.getInstance(SuiteApp.appContext);
//		Constants.Perfil perfil = session.getClientProfile();
//		
//		if(Constants.Perfil.recortado.equals(perfil)){
//				
//					if(Constants.siEstatusAlertas.equals(session.getEstatusAlertas())){
		// tiene ALERTAS

		solicitarAlertas(controladorTransferencias);

//					} else {
//						// NO tiene ALERTAS
//						
//						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
//						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_dineromovil_recortado_sin_alertas));
//						
//					}
//				}

	}

}
