package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.base.callback.CallBackSession;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.SincronizarSesion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.MenuHamburguesaViewsControllers;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuConsultarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import tracking.TrackingHelper;

public class MenuConsultarViewController extends BaseViewController  implements CallBackModule, CallBackSession {
	
	LinearLayout vista;
	ListaSeleccionViewController listaSeleccion;
	ArrayList<Object> lista;
	MenuConsultarDelegate delegate;
	//AMZ
		public BmovilViewsController parentManager;
		//AMZ
	
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_menu_consultar);
		setTitle(R.string.consultar_title, R.drawable.bmovil_consultar_icono);
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(MenuConsultarDelegate.MENU_CONSULTAR_DELEGATE_ID));
		delegate = (MenuConsultarDelegate)getDelegate();
		if(delegate==null){
			delegate = new MenuConsultarDelegate();
			parentViewsController.addDelegateToHashMap(MenuConsultarDelegate.MENU_CONSULTAR_DELEGATE_ID,delegate);
			setDelegate(delegate);
		}
		delegate.setMenuConsultarViewController(this);
		vista = (LinearLayout)findViewById(R.id.contenedor_menu_consultar);
		init();
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("consultar", parentManager.estados);
	}
	
	public void init(){
		cargaListaSeleccionComponent();
	}
	
	@SuppressWarnings({ "unchecked","deprecation" })
	public void cargaListaSeleccionComponent(){
		lista = ((ArrayList<Object>) delegate.getDatosTablaMenu());
		
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		GuiTools.getCurrent().init(getWindowManager());
		//params.topMargin 	= GuiTools.getCurrent().getEquivalenceFromScaledPixels(getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_border_margin));
		params.leftMargin 	= GuiTools.getCurrent().getEquivalenceFromScaledPixels(getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin));
		params.rightMargin 	= GuiTools.getCurrent().getEquivalenceFromScaledPixels(getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin));
	

		listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
		listaSeleccion.setDelegate(delegate);
		listaSeleccion.setLista(lista);
		listaSeleccion.setNumeroColumnas(1);
		listaSeleccion.setTitle("Selecciona el tipo de operación"); //getString(extras.getInt(Constants.PANTALLA_BASE_SUBTITULO));
		listaSeleccion.setOpcionSeleccionada(-1);
		listaSeleccion.setSeleccionable(false);
		listaSeleccion.setAlturaFija(false);
		listaSeleccion.setNumeroFilas(lista.size());
		listaSeleccion.cargarTabla();		
		vista.addView(listaSeleccion);
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		if (delegate != null) {
			delegate.setMenuConsultarViewController(this);
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {		
		super.goBack();
	}
	
	@Override
	protected void onDestroy() {
		parentViewsController.removeDelegateFromHashMap(MenuConsultarDelegate.MENU_CONSULTAR_DELEGATE_ID);
		super.onDestroy();
	}
	
	/*public void opcionSeleccionada(String opcionSeleccionada) {
		//AMZ
				Map<String,Object> inicioConsultaMap = new HashMap<String, Object>();
				
		Session session = Session.getInstance(SuiteApp.getInstance());
		if (opcionSeleccionada.equals("0")) {
			//Consultar movimientos
			//AMZ
			inicioConsultaMap.put("evento_inicio","event45");
			inicioConsultaMap.put("&&products","consulta;movimientos");
			inicioConsultaMap.put("eVar12","inicio");
			TrackingHelper.trackInicioConsulta(inicioConsultaMap);
			if(Server.ALLOW_LOG) Log.d(getLocalClassName().trim(), "Entraste a consultar movimientos");
			((BmovilViewsController)parentViewsController).showConsultarMovimientos();
		}else if (opcionSeleccionada.equals("1")) {
			//consultar envios DM
			//AMZ
			inicioConsultaMap.put("evento_inicio","event45");
			inicioConsultaMap.put("&&products","consulta;dinero movil");
			inicioConsultaMap.put("eVar12","inicio");
			TrackingHelper.trackInicioConsulta(inicioConsultaMap);
			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.dineroMovil,	session.getClientProfile())) {
				if(Server.ALLOW_LOG) Log.d(getLocalClassName().trim(), "Entraste a consultar envios DM");
				((BmovilViewsController)parentViewsController).showConsultarDineroMovil();
			}else{
				// EA#9 Caso de Uso Transferir Dinero Movil
//				DineroMovilDelegate dineroMovilDelegate = new DineroMovilDelegate();
//				dineroMovilDelegate.comprobarRecortado();
				delegate.comprobarRecortado();
			}
		}else if (opcionSeleccionada.equals("2")) {
			//AMZ
			inicioConsultaMap.put("evento_inicio","event45");
			inicioConsultaMap.put("&&products","consulta;frecuentes");
			inicioConsultaMap.put("eVar12","inicio");
			TrackingHelper.trackInicioConsulta(inicioConsultaMap);
			if(Server.ALLOW_LOG) Log.d(getLocalClassName().trim(), "Entraste a consultar Frecuentes");
			((BmovilViewsController)parentViewsController).showTipoConsultaFrecuentes();			
		}else if (opcionSeleccionada.equals("3")) {
			//rapidos
			if(Server.ALLOW_LOG) Log.d(getLocalClassName().trim(), "Entraste a consultar rapidos");
			((BmovilViewsController)parentViewsController).showConsultarRapidas();
		}else if (opcionSeleccionada.equals("4")) {
			//rapidos
			if(Server.ALLOW_LOG) Log.d(getLocalClassName().trim(), "Entraste a consultar interbancarios");
			((BmovilViewsController)parentViewsController).showConsultarInterbancarios();
		}
		
	}*/
	
	public void opcionSeleccionada(String opcionSeleccionada) {
		//AMZ
				Map<String,Object> inicioConsultaMap = new HashMap<String, Object>();
				
		Session session = Session.getInstance(SuiteApp.getInstance());
		if (opcionSeleccionada.equals(BmovilConstants.MenuConsultar_movimientos_opcion)) {
			//Consultar movimientos
			//AMZ
			inicioConsultaMap.put("evento_inicio","event45");
			inicioConsultaMap.put("&&products","consulta;movimientos");
			inicioConsultaMap.put("eVar12","inicio");
			TrackingHelper.trackInicioConsulta(inicioConsultaMap);
			//	if(Server.ALLOW_LOG) Log.d(getLocalClassName().trim(), "Entraste a consultar movimientos");
			((BmovilViewsController)parentViewsController).showConsultarMovimientos();
		}else if (opcionSeleccionada.equals(BmovilConstants.MenuConsultar_enviosdm_opcion)) {
			//consultar envios DM
			//AMZ
			inicioConsultaMap.put("evento_inicio","event45");
			inicioConsultaMap.put("&&products","consulta;dinero movil");
			inicioConsultaMap.put("eVar12","inicio");
			TrackingHelper.trackInicioConsulta(inicioConsultaMap);
			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.dineroMovil,	session.getClientProfile())) {
				//	if(Server.ALLOW_LOG) Log.d(getLocalClassName().trim(), "Entraste a consultar envios DM");
				((BmovilViewsController)parentViewsController).showConsultarDineroMovil();
			}else{
				// EA#9 Caso de Uso Transferir Dinero Movil
//				DineroMovilDelegate dineroMovilDelegate = new DineroMovilDelegate();
//				dineroMovilDelegate.comprobarRecortado();
				delegate.comprobarRecortado();
				delegate.setOpcionRetiroSinTarjeta(false);
			}
		}else if (opcionSeleccionada.equals(BmovilConstants.MenuConsultar_opfrecuentes_opcion)) {
			//AMZ
			inicioConsultaMap.put("evento_inicio","event45");
			inicioConsultaMap.put("&&products","consulta;frecuentes");
			inicioConsultaMap.put("eVar12","inicio");
			TrackingHelper.trackInicioConsulta(inicioConsultaMap);
			//	if(Server.ALLOW_LOG) Log.d(getLocalClassName().trim(), "Entraste a consultar Frecuentes");
			((BmovilViewsController)parentViewsController).showTipoConsultaFrecuentes();			
		}
		/*else if (opcionSeleccionada.equals("3")) {
			//rapidos
			//	if(Server.ALLOW_LOG) Log.d(getLocalClassName().trim(), "Entraste a consultar rapidos");
			((BmovilViewsController)parentViewsController).showConsultarRapidas();
		}*/
		else if (opcionSeleccionada.equals("4")) {
			//consultar interbancarios
			if(Server.ALLOW_LOG) Log.d(getLocalClassName().trim(), "Entraste a consultar interbancarios");
			((BmovilViewsController)parentViewsController).showMenuTransferenciasInterbancariasViewController();
		}
		else if (opcionSeleccionada.equals(BmovilConstants.MenuConsultar_depositosrecibidos_opcion)) {
			//Depositos Recibidos
			((BmovilViewsController)parentViewsController).showConsultarDepositosRecibidos();
		}
		else if (opcionSeleccionada.equals(BmovilConstants.MenuConsultar_obtenercomprobante_opcion)) {
			//Obtener Comprobante
			
			((BmovilViewsController)parentViewsController).showConsultarObtenerComprobante();
		}
		else if(opcionSeleccionada.equals(BmovilConstants.MenuConsultar_retirosintarjeta_opcion)){
			//Retiro sin tarjeta
			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.retiroSinTarjeta,	session.getClientProfile())) {
				if (Server.ALLOW_LOG) Log.d(getLocalClassName().trim(), "Entraste a consultar retiros sin tarjeta");
				((BmovilViewsController)parentViewsController).showConsultarRetiroSinTarjeta();
			}else{
				delegate.setOpcionRetiroSinTarjeta(true);
				delegate.comprobarRecortado();
			}
		}else if (opcionSeleccionada.equals(BmovilConstants.MenuConsultar_otroscreditos_opcion)) {

			//JMT
			inicioConsultaMap.put("evento_inicio","event45");
			inicioConsultaMap.put("&&products","consulta;mis creditos");
			inicioConsultaMap.put("eVar12","inicio");
			TrackingHelper.trackInicioConsulta(inicioConsultaMap);

			if (!bancomer.api.common.commons.Tools.isNetworkConnected(this)) {
				showErrorMessage(getString(R.string.error_communications));
				return;
			}

			Autenticacion aut = Autenticacion.getInstance();
			Constants.Perfil perfil = session.getClientProfile();

			if(Autenticacion.getInstance().operarOperacion(Constants.Operacion.consultarCreditos,	session.getClientProfile())){

			String cadAutenticacion =  aut.getCadenaAutenticacion(Constants.Operacion.consultarCreditos, perfil);
			if(Server.ALLOW_LOG) {
				Log.d(">> CGI", "Cadena de autenticacion >> " + cadAutenticacion);
				Log.d(">> CGI", "Perfil autenticacion >> " + perfil.name());
			}
			if(("00000").equals(cadAutenticacion)){

				muestraIndicadorActividad(getString(R.string.alert_operation),getString(R.string.alert_connecting));
				// Call to modulo
				InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance(this);//, ((BmovilViewsController)getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient());
				init.getConsultaSend().setIUM(session.getIum());
				init.getConsultaSend().setCallBackModule(this);
			//	init.getConsultaSend().setCallBackSession(this);
				init.getConsultaSend().setUsername(session.getUsername());
				init.getConsultaSend().setCadenaAutenticacion(cadAutenticacion);
				init.getConsultaSend().setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR);

				parentViewsController.setActivityChanging(true);
				init.preShowConsultarOtrosCreditos();


			}else{
				if(Server.ALLOW_LOG) {
					Log.d(">> CGI", "Go to confirmacion!");
				}
				MenuConsultarDelegate delegate = (MenuConsultarDelegate) ((BmovilViewsController)getParentViewsController()).getBaseDelegateForKey(MenuConsultarDelegate.MENU_CONSULTAR_DELEGATE_ID);
				if (delegate == null) {
					delegate = new MenuConsultarDelegate();
					((BmovilViewsController)getParentViewsController()).addDelegateToHashMap(MenuConsultarDelegate.MENU_CONSULTAR_DELEGATE_ID,
							delegate);
				}
				showConfirmacion(delegate);
			}
			}else{
				showInformationAlert(R.string.error_operacion_no_permitida);
			}
		}
	}

	public void showConfirmacion(MenuConsultarDelegate delegate){
		int resSubtitle = 0;
		int resTitleColor = 0;
		int resIcon = R.drawable.bmovil_configurarmontos_icono;
		int resTitle = R.string.bmovil_menu_miscuentas_otroscreditos_otrosCredito;
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(delegate, resIcon, resTitle, resSubtitle, resTitleColor);
	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		delegate.analyzeResponse(operationId, response);
	}


	@Override
	public void returnToPrincipal() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(true);
	}

	@Override
	public void returnDataFromModule(String operation, bancomer.api.common.io.ServerResponse response) {

		ocultaIndicadorActividad();
		if(delegate.getConfirmacionAutenticacionViewController() != null) delegate.getConfirmacionAutenticacionViewController().ocultaIndicadorActividad();
		if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

			InitConsultaOtrosCreditos.getInstance().showConsultarOtrosCreditos();

		}else{
			parentViewsController.setActivityChanging(false);
			if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				showInformationAlertEspecial(getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);

			}else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
				showInformationAlertEspecial(getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
			}

		}
	}


	@Override
	public void userInteraction() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();
	}

	@Override
	public void cierraSesionBackground(Boolean isChanging) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		parentViewsController.setActivityChanging(isChanging);
		if (parentManager.consumeAccionesDePausa()) {
			finish();
			if(MenuHamburguesaViewsControllers.getIsOterApp()){
				MenuHamburguesaViewsControllers.setIsOterApp(Boolean.FALSE);
			}
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	public void cierraSesion() {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication()
				.setApplicationInBackground(Boolean.FALSE);
		SuiteApp.getInstance().getBmovilApplication().logoutApp(Boolean.FALSE);
	}

	@Override
	public void returnMenuPrincipal() {
		Session.getInstance(SuiteApp.appContext).reloadFiles();
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(Boolean.TRUE);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(true);
	}

	@Override
	public void closeSession(String userName, String ium, String client) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getBmovilApp().closeSession(userName, ium, client);
	}
}
