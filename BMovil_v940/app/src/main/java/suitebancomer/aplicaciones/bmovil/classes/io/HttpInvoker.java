package suitebancomer.aplicaciones.bmovil.classes.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;

import suitebancomer.aplicaciones.bmovil.classes.model.NameValuePair;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;

/**
 * HttpInvoker allows to send challenge operations using HTTP protocol.
 *
 * @author Stefanini IT Solutions.
 */
public class HttpInvoker {
	/*
	 * Read buffer size.
	 */
	public static final int READBUFFER_SIZE = 2048;

	/**
     * Flag to indicate HTTP POST usage.
     */
    public static final boolean USE_HTTP_POST = true;

    /**
     * Flag to indicate HTTP response dump usage.
     */
    public static final boolean DUMP_HTTP_RESPONSE = true;

    /**
     * The field separator in the response.
     */
    private static final String PARAMETER_SEPARATOR = "*";
    
    /////////////////////////////////////////////////////////////////////////////
    //                Constants				                                   //
    /////////////////////////////////////////////////////////////////////////////
	/**
	 * HTTP connection methods.
	 */
	public static final String POST = "POST";
	public static final String GET = "GET";

	/**
     * HTTP protocols.
     */
    public static final String HTTP = "HTTP";
    public static final String HTTPS = "HTTPS";
	
	/**
	 * HTTP answers.
	 */
	public static final int UNDEFINED = 0;
	public static final int HTTP_CONTINUE = 100;
	public static final int HTTP_OK = 200;
	public static final int HTTP_NOT_FOUND = 404;
	public static final int HTTP_UNAUTHORIZED = 401;
	public static final int HTTP_FORBIDDEN = 403;
    public static final int HTTP_BAD_REQUEST = 400;
	public static final int HTTP_INTERNAL_ERROR = 500;
	public static final int HTTP_UNAVAILABLE = 503;

    /**
     * The current connection.
     */
    private HttpURLConnection connection = null;

    /**
     * Cookie from server.
     */
    private Hashtable<String, String> cookies = new Hashtable<String, String>();

    /**
     * Default constructor.
     */
    public HttpInvoker() {
    	
    }

    /**
     * Get the response from the server.
     * @param url URL to invoke
     * @return byte array obtained from the server
     * @throws IOException 
     */
    public byte[] get(String url) throws IOException {
        return retrieve(url);
    }

    /**
     * Simulate a network operation.
     * @param response the simulated response
     * @param handler instance capable of parsing the result
     * @throws IOException
     * @throws ParsingException
     */
    public void simulateOperation(String response, ParsingHandler handler)
        	throws IOException, ParsingException {
            parse(response, handler);
        }

    public void simulateOperation(String response, ParsingHandler handler, boolean isJSON)
        	throws IOException, ParsingException {
    	if (isJSON) {
			parseJSON(response, handler);
		}else {
            parse(response, handler);
		}
    }

    /**
     * Invoke a network operation.
     * @param operation operation code
     * @param parameters parameters
     * @param handler instance capable of parsing and storing the result
     * @throws IOException
     * @throws ParsingException 
     */
    public void invokeOperation(String operation, NameValuePair[] parameters,
    		ParsingHandler handler) throws IOException, ParsingException {
        invokeOperation(operation, parameters, handler, false);
    }

    /**
     * Invoke a network operation.
     * @param operation operation code
     * @param parameters parameters
     * @param handler instance capable of parsing the result
     * @param clearCookies true if session cookies must be deleted, false if not
     * @throws IOException
     * @throws ParsingException
     */
    public void invokeOperation(String operation, NameValuePair[] parameters,
    		ParsingHandler handler, boolean clearCookies) throws IOException,
    		ParsingException {
        if (clearCookies) {
            this.cookies.clear();
        }
        if (USE_HTTP_POST) {
            String method = HttpInvoker.POST;
            String url = getUrl(operation);
            String type = "application/x-www-form-urlencoded; charset=ISO-8859-1";
            String params = getParameterString(operation, parameters);
            byte[] contents = params.getBytes("ISO-8859-1");
            retrieve(url, method, type, contents, handler);
        } else {
          //  String method = HttpInvoker.GET;
        	 String method = HttpInvoker.POST;
            String url = getUrl(operation);
            String params = getParameterString(operation, parameters);
            char separator = ((url.indexOf('?') == -1) ? '?' : '&');
            url = new StringBuffer(url).append(separator).append(params).toString();
            retrieve(url, method, null, null, handler);
        }
    }

    /**
     * Get the URL to invoke the server.
     * @param operation operation code
     * @return a string with the response
     */
    public static String getUrl(String operation) {
        return Server.getOperationUrl(operation);
    }

    /**
     * Get the URL to invoke the server.
     * @param operation operation code
     * @param parameters parameters to pass to the server
     * @return a string with the response
     */
    public static String getParameterString(String operation, NameValuePair[] parameters) {

        StringBuffer sb = new StringBuffer("OP").append(operation);
        if (parameters != null) {
            NameValuePair parameter;
            int size = parameters.length;
            for (int i = 0; i < size; i++) {
                parameter = parameters[i];
                String name = parameter.getName();
                String value = parameter.getValue();
                sb.append(PARAMETER_SEPARATOR);
                sb.append(name);
                sb.append(value);
            }
        }
        
        String params = sb.toString();
        params = encode(params);

        sb.setLength(0);

        
        sb.append(Server.OPERATION_CODE_PARAMETER);
        sb.append('=');
        if(Server.OPERATION_CODES[Server.OP_CONSULTA_TDC].equals(operation)){
        	sb.append(Server.OPERATION_CODE_VALUE_BMOVIL_MEJORADO);
        	
        }
        else if(Server.OPERATION_CODES[Server.OP_SOLICITAR_ALERTAS].equals(operation)){
        	sb.append(Server.OPERATION_CODE_VALUE_RECORTADO);
        }else{
            sb.append(Server.OPERATION_CODE_VALUE);

        }

        sb.append('&');

        sb.append(Server.OPERATION_LOCALE_PARAMETER);
        sb.append('=');
        sb.append(Server.OPERATION_LOCALE_VALUE);

        sb.append('&');

        sb.append(Server.OPERATION_DATA_PARAMETER);
        sb.append('=');
        sb.append(params);

        return sb.toString();

    }

    /**
     * Abort the operation.
     */
    public void abort() {
        try {
            close();
        } catch (Throwable t) {}
    }

    /**
     * Retrieve the response from the server and store it in the ParsingHandler.
     * @param url the URL to invoke
     * @param method the HTTP method
     * @param type the POST body content type
     * @param handler instance capable of parsing the result
     * @throws IOException
     * @throws ParsingException
     */
    private void retrieve(String url, String method, String type, byte[] body,
    		ParsingHandler handler) throws IOException, ParsingException {
        try {
            open(url, method, type, body);
            read(handler);
        } finally {
            close();
        }
    }

    /**
     * Get the response from the server.
     * @param url the URL to invoke
     * @return the byte array obtained from the server
     * @throws IOException 
     */
    private byte[] retrieve(String url) throws IOException {
        try {
         //   open(url, HttpInvoker.GET, null, null);
        	open(url, HttpInvoker.POST, null, null);
            return read();
        } finally {
            close();
        }
    }

    /**
     * Open the connection with the server.
     * @param url the URL to connect to
     * @param method the HTTP method
     * @throws IOException 
     */
    private void open(String url, String method, String type, byte[] body) throws IOException {
        close();
        URL objUrl;

        OutputStream output = null;
        try {
        	objUrl = new URL(url);
	        connection = (HttpURLConnection)objUrl.openConnection();
	        connection.setAllowUserInteraction(false);
	        connection.setDoOutput(true);
            if (HttpInvoker.POST.equals(method)) {
                connection.setRequestMethod(HttpInvoker.POST);
                setCookies(connection, url);
                if (body != null) {
                    if (type != null) {
                        connection.setRequestProperty("Content-Type", type);
                    }
                    String length = Integer.toString(body.length);
                    connection.setRequestProperty("Content-Length", length);
                	connection.connect();
                    output = connection.getOutputStream();
                    output.write(body);
                    // Debugging - quitar
                   // System.out.println("------- REQUEST ------");
                    for (int x = 0; x < body.length; x++) {
                    	//	System.out.print((char)body[x]);
                    }
                    //  System.out.println("\n---------------------");
                }
            } else {
               // connection.setRequestMethod(HttpInvoker.GET);
            	 connection.setRequestMethod(HttpInvoker.POST);
                setCookies(connection, url);
            }
        } finally {
            if (output != null) {
                output.close();
            }
        }
    }

    /**
     * Read the response from the server.
     * @return the bytes read
     * @throws IOException
     */
    private byte[] read() throws IOException {
        byte[] data = null;
        int code = connection.getResponseCode();
        
        if ((code != HttpURLConnection.HTTP_OK) && (code != 100)) {
            StringBuffer message = new StringBuffer();
            message.append("Response code = ");
            message.append(code);
            message.append(" (");
            message.append(connection.getResponseMessage());
            message.append(")");
            throw new IOException(message.toString());
        }
        getCookies(connection);
        data = read(connection);
        return data;
    }

    /**
     * Read the result from the server.
     * @param handler instance capable of parsing the result
     * @throws IOException
     * @throws ParsingException
     */
    private void read(ParsingHandler handler) throws IOException, ParsingException {
        int code = connection.getResponseCode();
        // System.out.println("HttpInvoker: response code = " + code);
        if ((code != HttpInvoker.HTTP_OK) && (code != HttpInvoker.HTTP_CONTINUE)) {
            StringBuffer message = new StringBuffer();
            message.append("Response code = ");
            message.append(code);
            message.append(" (");
            message.append(connection.getResponseMessage());
            message.append(")");
            throw new IOException(message.toString());
        }
        getCookies(connection);
        read(connection, handler);
    }

    /**
     * Get the input reader.
     * @param connection the connection
     * @param handler the handler
     * @throws IOException on communication errors
     * @throws ParsingException
     */
    private static void read(HttpURLConnection connection,
    		ParsingHandler handler) throws IOException, ParsingException {
        if (DUMP_HTTP_RESPONSE) {
            byte[] data = read(connection);
            String response = new String(data, "ISO-8859-1");
            // System.out.println("------- RESPONSE ------");
            //System.out.println(response);
            //System.out.println("\n----------------------");
            parse(response, handler);
        } else {
            parse(connection, handler);
        }
    }

    /**
     * Read and parse the response.
     * @param connection is the response from the server
     * @param handler instance capable of parsing the result
     * @throws IOException
     * @throws ParsingException
     */
    private static void parse(HttpURLConnection connection, ParsingHandler handler) throws IOException, ParsingException {
        Reader reader = null;
        InputStream stream = null;
        try {
        	stream = connection.getInputStream();
            reader = new InputStreamReader(stream, "ISO-8859-1");
            Parser parser = new Parser(reader);
            handler.process(parser);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                    stream = null;
                } catch (Throwable ignored) {
                }
            }
            if (stream != null) {
                try {
                    stream.close();
                } catch (Throwable ignored) {
                }
            }
        }
    }

    /**
     * Read and parse the response.
     * @param response the response from the server
     * @param handler instance capable of parsing the result
     * @throws IOException
     * @throws ParsingException
     */
    private static void parse(String response, ParsingHandler handler) throws IOException, ParsingException {
        if (DUMP_HTTP_RESPONSE) {
            //System.out.println("Response contents: " + response);
        }
        Reader reader = null;
        try {
            reader = new StringReader(response);
            Parser parser = new Parser(reader);
            handler.process(parser);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Throwable ignored) {
                }
            }
        }
    }

    private static void parseJSON(String response, ParsingHandler handler) throws IOException, ParsingException {

        Reader reader = null;
        try {
//            reader = new StringReader(response);
            ParserJSON parser = new ParserJSON(response);
            handler.process(parser);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Throwable ignored) {
                }
            }
        }
    }

    /**
     * Read the response from the server.
     * @param connection the connection
     * @return the bytes read
     * @throws IOException
     */
    private static byte[] read(HttpURLConnection connection) throws IOException {
        byte[] data = null;
        byte[] dataAux = null;
        InputStream stream = null;
        try {
        	stream = connection.getInputStream();
            int len = (int)connection.getContentLength();
            if (len > 0) {
                int actual = 0;
                int bytesread = 0;
                data = new byte[len];
                while ((bytesread != len) && (actual != -1)) {
                    actual = stream.read(data, bytesread, len - bytesread);
                    bytesread += actual;
                }
            } else {
                int ch;
                int i = 0;
                int j = 1;
                data = new byte[READBUFFER_SIZE];
                while ((ch = stream.read()) != -1) {
                    if (i >= data.length) {
                        j++;
                        dataAux = new byte[READBUFFER_SIZE * j];
                        System.arraycopy(data, 0, dataAux, 0, data.length);
                        data = null;
                        data = dataAux;
                    }
                    data[i] = (byte) ch;
                    i++;
                }
                dataAux = null;
                dataAux = new byte[i];
                System.arraycopy(data, 0, dataAux, 0, i);
                data = dataAux;
                dataAux = null;
            }
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (Throwable ignored) {
                }
            }
        }
        return data;
    }

    /**
     * Close the http operation.
     */
    private void close() {
        if (connection != null) {
            connection.disconnect();
            connection = null;
        }
    }

    /**
     * URL encoding.
     * @param s the input string
     * @return the encodec string
     */
    public static String encode(String s) {
        String encoded = s;
        if (s != null) {
            try {
                StringBuffer sb = new StringBuffer(s.length() * 3);
                char c;
                int size = s.length();
                for (int i = 0; i < size; i++) {
                    c = s.charAt(i);
                    if (c == '&') {
                        sb.append("%26");
                    } else if (c == ' ') {
                        sb.append('+');
                    } else if ((c >= ',' && c <= ';')
                    		|| (c >= 'A' && c <= 'Z')
                    		|| (c >= 'a' && c <= 'z')
                    		|| c == '_' || c == '?' || c == '*') {
                        sb.append(c);
                    } else {
                        sb.append('%');
                        if (c > 15) {
                            sb.append(Integer.toHexString((int)c));
                        } else {
                            sb.append("0" + Integer.toHexString((int) c));
                        }
                    }
                }
                encoded = sb.toString();
            } catch (Exception ex) {
                encoded = null;
            }
        }
        return (encoded);
    }


    /**
     * Retreives the cookie from the connection.
     * @param connection connection the connection
     */
    private void getCookies(HttpURLConnection connection) throws IOException {
        try {
            String key = null;
            String value = null;
            String cookie = null;
            String domain = null;
            int k = 1;
            while (connection.getHeaderFieldKey(k) != null) {
                key = connection.getHeaderFieldKey(k);
                value = connection.getHeaderField(k);
                key = key.toLowerCase();
                if (key.equals("set-cookie")) {
                    int j = value.indexOf(";");
                    cookie = value.substring(0, j);
                    domain = connection.getURL().getHost();
                    if (domain == null) {
                        cookie = null;
                    } else {
                        domain = domain.toLowerCase();
                        cookies.put(cookie, domain);
                    }
                }
                k++;
            }
        } catch (Exception e) {
        	if(Server.ALLOW_LOG) throw new IOException(e.getMessage());
        }
        return;

    }

    /**
     * Set the related cookies to this connection.
     * @param connection the connection
     * @param url the target url
     */
    private void setCookies(HttpURLConnection connection, String url) throws IOException {

        try {
            String domain;
            String target = url.trim();
            int lenprotocol = (target.startsWith("https", 0)) ? 8 : 7;
            int indexSlash = target.indexOf("/", lenprotocol);
            int indexColon = target.indexOf(":", lenprotocol);
            if ((indexSlash == -1) && (indexColon == -1)) {
                domain = target.substring(lenprotocol, target.length());
            } else if (indexSlash == -1) {
                domain = target.substring(lenprotocol, indexColon);
            } else if (indexColon == -1) {
                domain = target.substring(lenprotocol, indexSlash);
            } else if ((indexSlash < indexColon)) {
                domain = target.substring(lenprotocol, indexSlash);
            } else {
                domain = target.substring(lenprotocol, indexColon);
            }
            domain = domain.toLowerCase();
            StringBuffer buff = new StringBuffer();
            String cookie = "", cookieDomain = "";
            Enumeration<String> e = cookies.keys();
            while (e.hasMoreElements()) {
                cookie = (String) e.nextElement();
                cookieDomain = (String) cookies.get(cookie);
                if (domain.endsWith(cookieDomain)) {
                    buff.append(cookie);
                    buff.append("; ");
                }
            }
            String cookieStr = buff.toString();
            if ((cookieStr != null) && (!cookieStr.equals(""))) {
            	//  System.out.println("Setting cookie: " + cookieStr);
                connection.setRequestProperty("cookie", cookieStr);
            }
        } catch (Exception ex) {
        	if(Server.ALLOW_LOG) throw new IOException(ex.getMessage());
        }
        return;

    }
    

}