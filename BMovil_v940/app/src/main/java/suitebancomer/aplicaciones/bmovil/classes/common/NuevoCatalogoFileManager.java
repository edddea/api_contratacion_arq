package suitebancomer.aplicaciones.bmovil.classes.common;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Map.Entry;

import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import android.content.Context;
import android.util.Log;

import com.bancomer.mbanking.SuiteApp;

public class NuevoCatalogoFileManager {
	// #region Constantes.
	/**
	 * Nombre del archivo de propiedades.
	 */
	private static final String NUEVO_CATALOGO_FILE_NAME = "NuevoCatalogo.prop";
		
	private static final int POSICION_IDENTIFICADOR = 0;
	private static final int POSICION_NAME = 1;
	private static final int POSICION_IMAGE = 2;
	private static final int POSICION_CLAVE = 3;
	private static final int POSICION_AMOUNTS = 4;
	private static final int POSICION_ORDER = 5;
	private static final String CARACTER_SEPARACION = "?";
	
	// #endregion
	
	// #region Variables.
	/**
	 * Propiedades de la aplicacion.
	 */
	private Properties properties;
	// #endregion
	
	// #region Singleton.
	/**
	 * La instancia de la clase.
	 */
	private static NuevoCatalogoFileManager manager = null;
	
	/**
	 * @return La instancia de la clase.
	 */
	public static NuevoCatalogoFileManager getCurrent() {
		if(null == manager)
			manager = new NuevoCatalogoFileManager();
		return manager;
	}
	
	/**
	 * Inicializa el administrador de propiedades. 
	 */
	private NuevoCatalogoFileManager() {
		properties = null;
		File file = new File(SuiteApp.appContext.getFilesDir(), NUEVO_CATALOGO_FILE_NAME);
		
		if(!file.exists())
			initNuevoCatalogoFile(file);
		else
			loadNuevoCatalogoFile();
	}
	// #endregion
	
	// #region Carga del archivo de propiedades.
	/**
	 * Inicializa el archivo de propiedades con los valores iniciales para cada propiedad.
	 * @param file
	 */
	private void initNuevoCatalogoFile(File file) {
		try {
			file.createNewFile();
		} catch(IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al crear el archivo NuevoCatalogo.", ioEx);
			return;
		}
				
		loadNuevoCatalogoFile();
		
		if(null != properties) {
			setPropertynValue("", null);
		}
	}
	
	/**
	 * Carga el archivo de propiedades.
	 */
	private void loadNuevoCatalogoFile() {
		InputStream input;
		try {
			input = SuiteApp.appContext.openFileInput(NUEVO_CATALOGO_FILE_NAME);
		} catch(FileNotFoundException fnfEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargar el archivo NuevoCatalogo para lectura.", fnfEx);
			return;
		}

		properties = new Properties();
		try {
			properties.load(input);
		} catch(IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargar NuevoCatalogo.", ioEx);
			properties = null;
			return;
		}
	}
	// #endregion
	
	// #region Generic getters and setters for properties.
	/**
	 * Establece el valor especificado a la propiedad indicada.
	 * @param propertyName El nombre de la propiedad.
	 * @param value El valor a establecer.
	 */
	private void setPropertynValue(String propertyName, Vector<String> value) {
		if(null == properties || Tools.isEmptyOrNull(propertyName))
			return;
		if(null == value){
			value= new Vector<String>();
			value.add("");
		}
		String valor=value.get(POSICION_IDENTIFICADOR)+CARACTER_SEPARACION
				+value.get(POSICION_NAME)+CARACTER_SEPARACION
				+value.get(POSICION_IMAGE)+CARACTER_SEPARACION
				+value.get(POSICION_CLAVE)+CARACTER_SEPARACION
				+value.get(POSICION_AMOUNTS)+CARACTER_SEPARACION
				+value.get(POSICION_ORDER);
		properties.setProperty(propertyName, valor);
		storeFileForProperty(propertyName, value);
	}
	
	/**
	 * Obtiene el valor de la propiedad especificada.
	 * @param propertyName El nombre de la propiedad.
	 * @return El valor de la propiedad.
	 */
	
	public Vector<String> getPropertynValue(String propertyName) {
		if(null == properties)
			return null;
		
		String values = (String) properties.get(propertyName);
		Vector<String> result= new Vector<String>();
				
		StringTokenizer tokens=new StringTokenizer(values, CARACTER_SEPARACION);
        int nDatos=tokens.countTokens();
        String[] datos=new String[nDatos];
        int i=0;
        while(tokens.hasMoreTokens()){
            datos[i]=tokens.nextToken();
            i++;
        }
		
		
		result.add(datos[0]); // id
		result.add(datos[1]); // name
		result.add(datos[2]); // image
		result.add(datos[3]); // clave
		result.add(datos[4]); // amounts
		result.add(datos[5]); // order
		return result;
	}
	// #endregion
	
	// #region Setters y Getters del estatus de activación para las aplicaciones.
	
	public String getIdentificador(Vector<String> values){
		return values.get(POSICION_IDENTIFICADOR);
	}
	public String getName(Vector<String> values){
		return values.get(POSICION_NAME);
	}	
	public String getImage(Vector<String> values){
		return values.get(POSICION_IMAGE);
	}
	public String getClave(Vector<String> values){
		return values.get(POSICION_CLAVE);
	}
	public String getAmounts(Vector<String> values){
		return values.get(POSICION_AMOUNTS);
	}
	public String getOrder(Vector<String> values){
		return values.get(POSICION_ORDER);
	}	
	
	public Collection<Entry<Object,Object>> getAllCatalog(){
		Collection<Entry<Object,Object>> es=properties.entrySet();
		return es;
	}	
		
	public void setCat(String key,Vector<String> value){
		setPropertynValue(key, value);
	}
	
	
	public void borrarCat(){
		properties.clear();
	}
	
	public void insertarCatalogo(String id, int identificador, String nombre,
			String imagen, String clave, String importes, int orden){
		
		
		Vector<String> values= new Vector<String>();
		values.add(String.valueOf(identificador));
		values.add(nombre);
		values.add(imagen);
		values.add(clave);
		// importes, puede ser nulo!
		values.add(importes);
		values.add(String.valueOf(orden));
		
		if(Server.ALLOW_LOG) Log.i("Nuevo catalogo " + id, values.toString());
		
		setPropertynValue(id,values);		
		
	}	
	
	// #endregion	
	
	/**
	 * Guarda el archivo de propiedades para preservar el cambio en alguna de las propiedades. 
	 * El nombre y valor de la propiedad modificada con usados para registrar el proceso mediante el log.  
	 * @param propertyName El nombre de la propiedad a guardar.
	 * @param propertyValue El valor de la propiedad a guardar.
	 * @return True si el archivo fue guardado exitosamente, False de otro modo.
	 */
	private boolean storeFileForProperty(String propertyName, Vector<String> propertyValue)	{
		if(Tools.isEmptyOrNull(propertyName))
			propertyName = "No name indicated.";
		if(Tools.isEmptyOrNull(propertyValue.toString()))
			propertyName = "No value indicated.";
		
		OutputStream output;
		try {	
			output = SuiteApp.appContext.openFileOutput(NUEVO_CATALOGO_FILE_NAME, Context.MODE_PRIVATE);
		} catch(FileNotFoundException fnfEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al abrir el archivo para guardar la propiedad " + propertyName, fnfEx);
			return false;
		}
		
		try {
			properties.store(output, null);
		} catch (IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al guardar en el archivo NuevoCatalogo los valores: " + propertyName + " - " + propertyValue, ioEx);
			return false;
		}
		
		if(Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Archivo actualizado con los siguientes cambios: " + propertyName + " - " + propertyValue);
		return true;
	}

	public static void reloadFile(){
		manager=null;
	}
}
