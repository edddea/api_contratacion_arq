package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaInterbancariosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaRapidosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import tracking.TrackingHelper;

public class ConsultaInterbancariosViewController extends BaseViewController implements OnClickListener{

	
	private ConsultaInterbancariosDelegate delegate;
	private LinearLayout contenedorListaInterbancarios;
	private ListaSeleccionViewController listaFrecuentes;
	private TextView btnConsultar;
	//AMZ
		public BmovilViewsController parentManager;
		//AMZ

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_consulta_interbancarios);
		setTitle(R.string.bmovil_consultar_interbancarios_titulo, R.drawable.bmovil_consultar_icono);
		
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("consulta interbancaria", parentManager.estados);
		
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		delegate = (ConsultaInterbancariosDelegate)parentViewsController.getBaseDelegateForKey(ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID);
		delegate.setOwnerController(this);
		setDelegate(delegate);
		
		init();
		configurarPantalla();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		parentViewsController.setActivityChanging(false);
		delegate.setOwnerController(this);
		parentViewsController.setCurrentActivityApp(this);
	}
	
	private void configurarPantalla(){
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(findViewById(R.id.consultar_interbancarios_contenedor_principal));
		gTools.scale(findViewById(R.id.btnConsultar));
	}

	private void init() {
		contenedorListaInterbancarios = (LinearLayout)findViewById(R.id.contenedor_lista_interbancarios);

		btnConsultar=(TextView)findViewById(R.id.btnConsultar);
		btnConsultar.setOnClickListener(this);
		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT,	LayoutParams.WRAP_CONTENT);

		if (listaFrecuentes == null) {
			listaFrecuentes = new ListaSeleccionViewController(this, params,parentViewsController);
			listaFrecuentes.setDelegate(delegate);

			ArrayList<Object> listaEncabezado = delegate.getDatosHeaderTablaInterbancarios();
			ArrayList<Object> listaDatos = delegate.getInterbancarios();
			listaFrecuentes.setEncabezado(listaEncabezado);
			listaFrecuentes.setLista(listaDatos);
			listaFrecuentes.setNumeroColumnas(listaEncabezado.size() - 1) ;
			if (listaDatos.size() == 0) {
				listaFrecuentes.setTextoAMostrar(getString(R.string.bmovil_consultar_frecuentes_emptylist));
			}else {
				listaFrecuentes.setTextoAMostrar(null);
				listaFrecuentes.setNumeroFilas(listaDatos.size());		
			}	
			listaFrecuentes.setOpcionSeleccionada(-1);
			listaFrecuentes.setSeleccionable(false);
			listaFrecuentes.setAlturaFija(false);
			listaFrecuentes.setNumeroFilas(listaDatos.size());
			listaFrecuentes.setExisteFiltro(true);
			listaFrecuentes.setTitle("Envíos realizados");
			listaFrecuentes.setSingleLine(true);
			listaFrecuentes.cargarTabla();
			contenedorListaInterbancarios.addView(listaFrecuentes);
		}
		delegate.consultarInterbancarios(ConsultaInterbancariosDelegate.getPeriodoConsulta());
	}
	
	void cargarLista(){
		contenedorListaInterbancarios.removeAllViews();
		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT,	LayoutParams.WRAP_CONTENT);
		
		listaFrecuentes = new ListaSeleccionViewController(this, params,parentViewsController);
		listaFrecuentes.setDelegate(delegate);

		ArrayList<Object> listaEncabezado = delegate.getDatosHeaderTablaInterbancarios();
		ArrayList<Object> listaDatos = delegate.getInterbancarios();
		listaFrecuentes.setEncabezado(listaEncabezado);
		listaFrecuentes.setLista(listaDatos);
		listaFrecuentes.setNumeroColumnas(listaEncabezado.size() - 1) ;
		if (listaDatos.size() == 0) {
			listaFrecuentes.setTextoAMostrar(getString(R.string.bmovil_consultar_frecuentes_emptylist));
		}else {
			listaFrecuentes.setTextoAMostrar(null);
			listaFrecuentes.setNumeroFilas(listaDatos.size());		
		}	
		listaFrecuentes.setOpcionSeleccionada(-1);
		listaFrecuentes.setSeleccionable(false);
		listaFrecuentes.setAlturaFija(true);
		listaFrecuentes.setNumeroFilas(listaDatos.size());
		listaFrecuentes.setExisteFiltro(true);
		listaFrecuentes.setTitle("Envíos realizados");
		listaFrecuentes.setSingleLine(true);
		listaFrecuentes.cargarTabla();
		contenedorListaInterbancarios.addView(listaFrecuentes);
	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
		cargarLista();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v==btnConsultar){
			int periodo=ConsultaInterbancariosDelegate.getPeriodoConsulta();
			delegate.consultarInterbancarios(periodo);
			if(periodo==2){
				btnConsultar.setVisibility(View.GONE);
				delegate.setPeriodoConsulta(0);
			}
			
			
		}
	}
}
