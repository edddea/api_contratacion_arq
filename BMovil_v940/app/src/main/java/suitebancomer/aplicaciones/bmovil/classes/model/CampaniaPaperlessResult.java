package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.io.Serializable;

import android.util.Log;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class CampaniaPaperlessResult implements ParsingHandler {

	//"fecha":"18122014","hora":"085700"
	
	private String codigoMensaje;	
	private String descripcionMensaje;	
	private static String folio;	
	private static String fecha;	
	private static String hora;
	
		
	
	public String getCodigoMensaje() {
		return codigoMensaje;
	}


	public String getDescripcionMensaje() {
		return descripcionMensaje;
	}


	public static String getFolio() {
		return folio;
	}


	public static String getFecha() {
		
		return  Tools.formatDate(fecha);
	}


	public static String getHora() {
		return Tools.formatTime(hora);
	}


	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		throw new ParsingException("Invalid process");
	}

	
	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		codigoMensaje = parser.parseNextValue("codigoMensaje");	
		descripcionMensaje = parser.parseNextValue("descripcionMensaje");	
		folio = parser.parseNextValue("folio");		
		fecha = parser.parseNextValue("fecha");
		hora = parser.parseNextValue("hora");
		
		Log.e("entra a process", String.valueOf(folio));
		
	}

}
