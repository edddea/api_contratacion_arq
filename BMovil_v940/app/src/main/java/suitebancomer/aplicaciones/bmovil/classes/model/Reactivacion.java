package suitebancomer.aplicaciones.bmovil.classes.model;

import bancomer.api.common.commons.Constants.Perfil;

public class Reactivacion {
	private Perfil perfil;
	private String perfilAST;
	private String estatus;
	private String numCelular;
	private String companiaCelular;
	private String numCliente;
	
	/**
	 * @return the perfil
	 */
	public Perfil getPerfil() {
		return perfil;
	}
	/**
	 * @param perfil the perfil to set
	 */
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the numCelular
	 */
	public String getNumCelular() {
		return numCelular;
	}
	/**
	 * @param numCelular the numCelular to set
	 */
	public void setNumCelular(String numCelular) {
		this.numCelular = numCelular;
	}
	/**
	 * @return the companiaCelular
	 */
	public String getCompaniaCelular() {
		return companiaCelular;
	}
	/**
	 * @param companiaCelular the companiaCelular to set
	 */
	public void setCompaniaCelular(String companiaCelular) {
		this.companiaCelular = companiaCelular;
	}
	/**
	 * @return the numCliente
	 */
	public String getNumCliente() {
		return numCliente;
	}
	/**
	 * @param numCliente the numCliente to set
	 */
	public void setNumCliente(String numCliente) {
		this.numCliente = numCliente;
	}
	
	public void setPerfilAST(String perfilAST) {
		this.perfilAST = perfilAST;
	}
	
	public String getPerfilAST() {
		return perfilAST;
	}

}
