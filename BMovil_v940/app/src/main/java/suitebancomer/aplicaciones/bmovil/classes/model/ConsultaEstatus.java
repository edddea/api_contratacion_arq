package suitebancomer.aplicaciones.bmovil.classes.model;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;

public class ConsultaEstatus {
	// #region Variables.
	/**
	 * Estatus.
	 */
	private String estatus;
	
	/**
	 * Perfil del cliente.
	 */
	private Constants.Perfil perfil;
	
	/**
	 * Nombre del cliente.
	 */
	private String nombreCliente;
	
	/**
	 * Numero de tarjeta
	 */
	private String numTarjeta;
	
	/**
	 * Tipo de instrumento de seguridad del cliente.
	 */
	private String instrumento;
	
	/**
	 * Estatus del instrumento de seguridad del cliente.
	 */
	private String estatusInstrumento;
	
	/**
	 * Número de cliente.
	 */
	private String numCliente;
	
	/**
	 * Numero de celular del cliente.
	 */
	private String numCelular;
	
	/**
	 * Compa�ia celular del cliente.
	 */
	private String companiaCelular;
	
	/**
	 * Email del cliente.
	 */
	private String emailCliente;
	
	/**
	 * Perfil del cliente (MFxx).
	 */
	private String perfilAST;
	
	/**
	 * Estatus de las alertas Bancomer.
	 */
	private String estatusAlertas;
	// #endregion
	
	// #region Getters y Setters.
	/**
	 * @return Estatus.
	 */
	public String getEstatus() {
		return estatus;
	}

	/**
	 * @param estatus Estatus.
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return Perfil del cliente.
	 */
	public Constants.Perfil getPerfil() {
		return perfil;
	}

	/**
	 * @param perfil Perfil del cliente.
	 */
	public void setPerfil(Constants.Perfil perfil) {
		this.perfil = perfil;
	}

	/**
	 * @return Nombre del cliente.
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}

	/**
	 * @param nombreCliente Nombre del cliente.
	 */
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}

	/**
	 * @return Tipo de instrumento de seguridad del cliente.
	 */
	public String getInstrumento() {
		return instrumento;
	}

	/**
	 * @param instrumento Tipo de instrumento de seguridad del cliente.
	 */
	public void setInstrumento(String instrumento) {
		this.instrumento = instrumento;
	}

	/**
	 * @return Estatus del instrumento de seguridad del cliente.
	 */
	public String getEstatusInstrumento() {
		return estatusInstrumento;
	}

	/**
	 * @param estatusInstrumento Estatus del instrumento de seguridad del cliente.
	 */
	public void setEstatusInstrumento(String estatusInstrumento) {
		this.estatusInstrumento = estatusInstrumento;
	}

	/**
	 * @return Número de cliente.
	 */
	public String getNumCliente() {
		return numCliente;
	}

	/**
	 * @param numCliente Número de cliente.
	 */
	public void setNumCliente(String numCliente) {
		this.numCliente = numCliente;
	}

	/**
	 * @return Numero de celular del cliente.
	 */
	public String getNumCelular() {
		return numCelular;
	}

	/**
	 * @param numCelular Numero de celular del cliente.
	 */
	public void setNumCelular(String numCelular) {
		this.numCelular = numCelular;
	}

	/**
	 * @return Compa�ia celular del cliente.
	 */
	public String getCompaniaCelular() {
		return companiaCelular;
	}

	/**
	 * @param companiaCelular Compa�ia celular del cliente.
	 */
	public void setCompaniaCelular(String companiaCelular) {
		this.companiaCelular = companiaCelular;
	}

	/**
	 * @return Email del cliente.
	 */
	public String getEmailCliente() {
		return emailCliente;
	}

	/**
	 * @param emailCliente Email del cliente.
	 */
	public void setEmailCliente(String emailCliente) {
		this.emailCliente = emailCliente;
	}
	
	/**
	 * @param perfilAST Perfil del cliente (MFxx).
	 */
	public void setPerfilAST(String perfilAST) {
		this.perfilAST = perfilAST;
	}
	
	/**
	 * @return Perfil del cliente (MFxx).
	 */
	public String getPerfilAST() {
		return perfilAST;
	}
	
	/**
	 * @return Estatus de las alertas Bancomer.
	 */
	public String getEstatusAlertas() {
		return estatusAlertas;
	}

	/**
	 * @param estatusAlertas Estatus de las alertas Bancomer.
	 */
	public void setEstatusAlertas(String estatusAlertas) {
		this.estatusAlertas = estatusAlertas;
	}
	// #endregion

	//#regions Constructores.
	public ConsultaEstatus() {
		this.estatus = null;
		this.perfil = null;
		this.nombreCliente = null;
		this.instrumento = null;
		this.estatusInstrumento = null;
		this.numCliente = null;
		this.numCelular = null;
		this.companiaCelular = null;
		this.emailCliente = null;
		this.estatusAlertas = null;
	}

	/**
	 * @param estatus Estatus.
	 * @param perfil Perfil del cliente.
	 * @param nombreCliente nombre del cliente.
	 * @param instrumento Tipo de instrumento de seguridad del cliente.
	 * @param estatusInstrumento Estatus del instrumento de seguridad del cliente.
	 * @param numCliente Número de cliente.
	 * @param numCelular Numero de celular del cliente.
	 * @param companiaCelular Compa�ia celular del cliente.
	 * @param emailCliente Email del cliente.
	 */
	public ConsultaEstatus(String estatus, Perfil perfil, String nombreCliente,	String instrumento, String estatusInstrumento, String numCliente,
						   String numCelular, String companiaCelular, String emailCliente, String estatusAlertas) {
		super();
		this.estatus = estatus;
		this.perfil = perfil;
		this.nombreCliente = nombreCliente;
		this.instrumento = instrumento;
		this.estatusInstrumento = estatusInstrumento;
		this.numCliente = numCliente;
		this.numCelular = numCelular;
		this.companiaCelular = companiaCelular;
		this.emailCliente = emailCliente;
		this.estatusAlertas = estatusAlertas;
	}
	//#endregion
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ConsultaEstatus [estatus=" + estatus + ", perfil=" + perfil
				+ ", nombreCliente=" + nombreCliente + ", instrumento="
				+ instrumento + ", estatusInstrumento=" + estatusInstrumento
				+ ", numCliente=" + numCliente + ", numCelular=" + numCelular
				+ ", companiaCelular=" + companiaCelular + ", emailCliente="
				+ emailCliente + ", perfilAST=" + perfilAST + ", estatusAlertas=" + estatusAlertas + "]";
	}

	public String getNumTarjeta() {
		return numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}
}
