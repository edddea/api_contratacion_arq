package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.Operacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuAdministrarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
//import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.SofttokenViewsController;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import tracking.TrackingHelper;

/**
 * @author Francisco.Garcia
 * 
 */
public class MenuAdministrarViewController extends BaseViewController {

	private MenuAdministrarDelegate menuAdministrarDelegate;
	private ListaSeleccionViewController listaSeleccion;
	private LinearLayout vista;
	private boolean isRunningTask;
	//AMZ
	private BmovilViewsController parentManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE,
				R.layout.layout_bmovil_menu_administrar);
		setTitle(R.string.administrar_menu_titulo,
				R.drawable.bmovil_administrar_icono);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("administrar", parentManager.estados);
		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(suiteApp.getBmovilApplication()
				.getBmovilViewsController());
		setDelegate(parentViewsController
				.getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID));
		menuAdministrarDelegate = (MenuAdministrarDelegate) getDelegate();
		menuAdministrarDelegate.setMenuAdministrarViewController(this);
		vista = (LinearLayout) findViewById(R.id.menu_administrar_layout);
		inicializaMenu();
	}

	@SuppressWarnings("deprecation")
	protected void inicializaMenu() {

		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT,
				LayoutParams.WRAP_CONTENT);
		GuiTools.getCurrent().init(getWindowManager());
		// params.topMargin =
		// GuiTools.getCurrent().getEquivalenceFromScaledPixels(getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin));
		params.leftMargin = GuiTools.getCurrent()
				.getEquivalenceFromScaledPixels(
						getResources().getDimensionPixelOffset(
								R.dimen.resultados_side_margin));
		params.rightMargin = GuiTools.getCurrent()
				.getEquivalenceFromScaledPixels(
						getResources().getDimensionPixelOffset(
								R.dimen.resultados_side_margin));

		ArrayList<Object> lista = menuAdministrarDelegate.getDatosMenu();// new
																			// ArrayList<Object>();
		// ArrayList<Object> registros;
		// registros = new ArrayList<Object>(2);
		// registros.add(Constants.MENU_ADMINISTRAR_CAMBIAR_CONTRASENA_OPCION);
		// registros.add(SuiteApp.appContext.getString(R.string.administrar_menu_cambiar_contrasena_titulo));
		// lista.add(registros);
		//
		// registros = new ArrayList<Object>(2);
		// registros.add(Constants.MENU_ADMINISTRAR_ACERCADE_OPCION);
		// registros.add(SuiteApp.appContext.getString(R.string.administrar_acercade_titulo));
		// lista.add(registros);

		if (listaSeleccion == null) {
			listaSeleccion = new ListaSeleccionViewController(this, params,
					parentViewsController);
			listaSeleccion.setDelegate(menuAdministrarDelegate);
			listaSeleccion.setNumeroColumnas(1);
			listaSeleccion.setEncabezado(null);
			listaSeleccion.setLista(lista);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(false);
			listaSeleccion.setNumeroFilas(7);
			listaSeleccion.setExisteFiltro(false);
			listaSeleccion.cargarTabla();
			vista.addView(listaSeleccion);
		}

	}

	/*
	 * Estos de cajon! :|
	 */
	@Override
	protected void onResume() {
		super.onResume();

		/*if (this.parentViewsController instanceof SofttokenViewsController) {

			parentViewsController = SuiteApp.getInstance()
					.getBmovilApplication().getBmovilViewsController();
		}*/

		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		isRunningTask = false;
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public void goBack() {
		parentViewsController
				.removeDelegateFromHashMap(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
		super.goBack();
	}

	// ----------------------------------------------
	/***/

	public void opcionSeleccionada(String opcionSeleccionada) {
		if(Server.ALLOW_LOG) Log.d("MenuAdministrar", "opcion rebotar?" + isRunningTask);
		if (isRunningTask){
			return;
		}

		BmovilViewsController parent = null;

		if (this.parentViewsController instanceof BmovilViewsController) {

			parent = ((BmovilViewsController) this.parentViewsController);
		} else {

			parent = SuiteApp.getInstance().getBmovilApplication()
					.getBmovilViewsController();
		}


		//AMZ
				Map<String,Object> OperacionMap = new HashMap<String, Object>();
		Session session = Session.getInstance(SuiteApp.getInstance());
		if(opcionSeleccionada.equals(BmovilConstants.MADMINISTRAR_NOVEDADES)) {
			//AMZ
			OperacionMap.put("evento_realizada","event52");
			OperacionMap.put("&&products","operaciones;admin+novedades");
			OperacionMap.put("eVar12","operacion realizada");
			TrackingHelper.trackOperacionRealizada(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a Novedades");
			parent.showNovedades();
		} else if (opcionSeleccionada.equals(BmovilConstants.MADMINISTRAR_ACERCADE)) {
			//AMZ
			OperacionMap.put("evento_realizada","event52");
			OperacionMap.put("&&products","operaciones;admin+acerca");
			OperacionMap.put("eVar12","operacion realizada");
			TrackingHelper.trackOperacionRealizada(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a Acerca De");
			parent.showAcercaDe();
		} else if (opcionSeleccionada
				.equals(BmovilConstants.MADMINISTRAR_CAMBIAR_CONTRASENA)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+cambia password");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a Cambiar contrasena");
//			parent.showCambiarPassword();
		} else if (opcionSeleccionada
				.equals(BmovilConstants.MADMINISTRAR_CAMBIO_TELEFONO)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+cambio celular");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a: cambioTelefono");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.cambioTelefono,
					session.getClientProfile())) {
//				parent.showCambioDeTelefono();
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
		} else if (opcionSeleccionada
				.equals(BmovilConstants.MADMINISTRAR_ACTUALIZAR_CUENTAS)) {
			//AMZ
			OperacionMap.put("evento_realizada","event52");
			OperacionMap.put("&&products","operaciones;admin+actualiza cuentas");
			OperacionMap.put("eVar12","operacion realizada");
			TrackingHelper.trackOperacionRealizada(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a actualizarCuentas");

			actualizarCuentasSelected();

		} else if (opcionSeleccionada
				.equals(BmovilConstants.MADMINISTRAR_CAMBIO_CUENTA)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+cambio cuenta");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a cambioCuenta");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.cambioCuenta,
					session.getClientProfile())) {
//				parent.showCambioCuentaAsociada();
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
		} else if (opcionSeleccionada
				.equals(BmovilConstants.MADMINISTRAR_SUSPENDER_CANCELAR)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+suspender o cancelar");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a suspenderCancelar");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.suspenderCancelar,
					session.getClientProfile())) {
//				parent.showMenuSuspenderCancelar();
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}

		} else if (opcionSeleccionada
				.equals(BmovilConstants.MADMINISTRAR_CONFIGURAR_MONTOS)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+configura montos");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a CONFIGURAR_MONTOS");
			if(Constants.Perfil.avanzado.equals(session.getClientProfile())){
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.cambioLimites,
					session.getClientProfile())) {
				consultarLimites();
				isRunningTask = true;
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
			}else{
				if (Autenticacion.getInstance().operarOperacion(
						Constants.Operacion.consultaLimites,
						session.getClientProfile())) {
					consultarLimites();
					isRunningTask = true;
				} else {
					// EA#13 del caso de uso de Menu Administrar
				}
			}
		} else if (opcionSeleccionada
				.equals(BmovilConstants.MADMINISTRAR_CONFIGURAR_ALERTAS)) {
			if(Server.ALLOW_LOG) Log.d(getClass().getName(), "Entraste a configurar alertas");
			// parent.showConfigurarAlertas();
		} else if (opcionSeleccionada
				.equals(BmovilConstants.MADMINISTRAR_CONFIGURAR_CORREO)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+configura correo");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a suspenderCancelar");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.configurarCorreo,
					session.getClientProfile())) {
//				parent.showConfigurarCorreo();
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
		} else if (opcionSeleccionada
				.equals(BmovilConstants.MADMINISTRAR_OPERAR_SIN_TOKEN)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+operar sin token");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a operarSinToken");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.cambioPerfil,
					session.getClientProfile())) {
//				parent.showCambioPerfil();
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
		}else if(opcionSeleccionada
				.equals(BmovilConstants.MADMINISTRAR_OPERAR_RECORTADO)){
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a operar Recortado");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.cambioPerfil,
					session.getClientProfile())) {
//				parent.showCambioPerfil();
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
		} else if (opcionSeleccionada
				.equals(BmovilConstants.MADMINISTRAR_OPERAR_CON_TOKEN)) {
			//AMZ
			OperacionMap.put("evento_inicio","event45");
			OperacionMap.put("&&products","operaciones;admin+operar con token");
			OperacionMap.put("eVar12","inicio");
			TrackingHelper.trackInicioOperacion(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a operarConToken");
			if (Autenticacion.getInstance().operarOperacion(
					Constants.Operacion.cambioPerfil,
					session.getClientProfile())) {

				Perfil perfil = Session.getInstance(SuiteApp.appContext)
						.getClientProfile();

				/* TODO AMB
				CambioPerfilDelegate cambioPerfilDelegate = new CambioPerfilDelegate();
				if (cambioPerfilDelegate.verificarCambioPerfil(perfil)) {
					parent.showCambioPerfil();
				} else {
					cambioPerfilDelegate.setOwnerController(this);
					cambioPerfilDelegate.cambioPerfilSinToken();

				}*/
			} else {
				// EA#13 del caso de uso de Menu Administrar
			}
		} else if (opcionSeleccionada
				.equals(BmovilConstants.MADMINISTRAR_CONSULTAR_CONTRATO)) {
			//AMZ
			OperacionMap.put("evento_realizada","event52");
			OperacionMap.put("&&products","operaciones;admin+consulta contrato");
			OperacionMap.put("eVar12","operacion realizada");
			TrackingHelper.trackOperacionRealizada(OperacionMap);
			if(Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a consultarContrato");
			showTerminosDeUso();
		}//SPEI
		  else if (opcionSeleccionada.equals(BmovilConstants.MADMINISTRAR_ASOCIAR_CELULAR)) {
			  if(cuentasValidasSPEI()>0){
				  if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Entraste a consultar asociaci'on SPEI");
				  ((BmovilViewsController)parentViewsController).showAsociacionCuentaTelefono();
			  }
			  else
			       alertSinCuentasSPEI(); 
			 
		 //Termina SPEI 
		  }	  
		  else if (opcionSeleccionada
				.equals(BmovilConstants.MADMINISTRAR_CONSULTA_ESTATUS_ENVIO_ESTADO_DE_CUENTA)) {

			Autenticacion aut = Autenticacion.getInstance();
			Perfil perfil = Session.getInstance(SuiteApp.appContext)
					.getClientProfile();
			Operacion operacion;
			operacion = Operacion.actualizarEstatusEnvioEC;
			if(aut.isOperable(operacion, perfil)) {
				//AMZ
				OperacionMap.put("evento_inicio","event45");
				OperacionMap.put("&&products","operaciones;admin+consulta de envio de estado de cuenta");
				OperacionMap.put("eVar12","inicio");
				TrackingHelper.trackInicioOperacion(OperacionMap);
				if (Server.ALLOW_LOG) Log.d("" + getClass().getName(), "Entraste a envio de estado de cuenta");

				((BmovilViewsController)parentViewsController).showConsultarEstatusEnvioEstadodeCuenta();

			}else {
				if (Server.ALLOW_LOG) Log.d("hcf", "Alert cambio de perfil");
				showNoYesAlert(R.string.administrar_menu_envio_estados_cuenta, new OnClickListener() {
//				showYesNoAlert(R.string.administrar_menu_envio_estados_cuenta, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						cambioPerfil();
					}
				}, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				} );
			}

		}
	    
	}

	protected void cambioPerfil(){
		BmovilViewsController parent = ((BmovilViewsController) this.parentViewsController);
		//TODO AMB
		// parent.showCambioPerfil();
	}
	/**
	 * cambio de cuentas
	 */
	private void actualizarCuentasSelected() {
		isRunningTask = true;
		menuAdministrarDelegate.actualizarCuentas();
	}

	/**
	 * mostrar alert de cuentas actualizadas
	 */
	public void muestraCuentasActualizadas() {
		String title = SuiteApp.appContext
				.getString(R.string.bmovil_actualizar_cuentas_exitoso);
		showInformationAlert(title, " ", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				isRunningTask = false;
			}
		});
	}
	
	public int cuentasValidasSPEI(){
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		int accountsTemp = 0;
		    
		  for(Account acc : accounts)
			{
			 // if(acc.getType().equals(Constants.SAVINGS_TYPE)||acc.getType().equals(Constants.CHECK_TYPE)||acc.getType().equals(Constants.LIBRETON_TYPE)){
				if(acc.getType().equals("AH")||acc.getType().equals("CH")||acc.getType().equals("LI")){

				    accountsTemp+=1;	
				    if(Server.ALLOW_LOG) Log.e("cuentasSPEI",String.valueOf(accountsTemp));
		        }
			}
		return accountsTemp;
		
	}
	
	public void alertSinCuentasSPEI() {
		String title = SuiteApp.appContext
				.getString(R.string.bmovil_asociar_cuenta_telefono_alternative_title);
		
		String message = SuiteApp.appContext
				.getString(R.string.bmovil_asociar_cuenta_telefono_alert);
		showInformationAlert(title, message, new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				isRunningTask = false;
			}
		});
	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		menuAdministrarDelegate.analyzeResponse(operationId, response);
		if(Server.ALLOW_LOG) Log.d("MenuAdministar", "processNetworkResponse" + isRunningTask);
	}

	private void consultarLimites() {
		if (isRunningTask)
			return;

		// Get aut.string
		Session session = Session.getInstance(SuiteApp.appContext);
		Autenticacion aut = Autenticacion.getInstance();
		Perfil perfil = session.getClientProfile();
		String cadAutenticacion =  aut.getCadenaAutenticacion(Constants.Operacion.consultaLimites, perfil);
		//if(Server.ALLOW_LOG) Log.d(">> CGI", "Cadena de autenticacion >> "+cadAutenticacion);
		//if(Server.ALLOW_LOG) Log.d(">> CGI", "Perfil autenticacion >> "+perfil.name());
		if(("00000").equals(cadAutenticacion)){
			//if(Server.ALLOW_LOG) Log.d(">> CGI", "Go to config montos!");
			menuAdministrarDelegate.conusltarLimitesJumpOp(menuAdministrarDelegate.getMenuAdministrarViewController(), null, null, null, null, null);
		}else{
			//if(Server.ALLOW_LOG) Log.d(">> CGI", "Go to confirmacion!");
			menuAdministrarDelegate.showConfirmacion();
		}
		//if(Server.ALLOW_LOG) Log.d("MenuAdministar", "consultarLimites" + isRunningTask);
		
		//menuAdministrarDelegate.consultarLimites();
		//if(Server.ALLOW_LOG) Log.d("MenuAdministar", "consultarLimites" + isRunningTask);
	}

	/*public void showConfigurarMontos(ConfigurarMontos cm) {
		if(Server.ALLOW_LOG) Log.d("MenuAdministar", "showConfigurarMontos" + isRunningTask);
		BmovilViewsController parent = ((BmovilViewsController) this.parentViewsController);
//		parent.showConfigurarMontos(cm);
	}*/

	public void showTerminosDeUso() {
		menuAdministrarDelegate.showTerminosDeUso();
	}

	public void showTerminosDeUso(String msg) {
		if(Server.ALLOW_LOG) Log.d("MenuAdministrar", "showTerminosDeUso" + isRunningTask);
		BmovilViewsController parent = ((BmovilViewsController) this.parentViewsController);
		parent.showTerminosDeUso(msg);
	}

	public void setRunningTask(boolean isRunningTask) {
		this.isRunningTask = isRunningTask;
	}

}
