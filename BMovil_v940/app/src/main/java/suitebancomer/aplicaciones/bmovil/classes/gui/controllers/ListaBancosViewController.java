package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.InterbancariosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.NameValuePair;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class ListaBancosViewController extends BaseViewController {
	
	private ListaSeleccionViewController listaSeleccion;
	private DelegateBaseOperacion delegate;
	private LinearLayout contenedorPrincipal;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_bmovil_lista_bancos_view);
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		delegate = (DelegateBaseOperacion) parentViewsController.getBaseDelegateForKey(InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID);
		setTitle(getString(R.string.combo_selectBank));
		contenedorPrincipal = (LinearLayout) findViewById(R.id.contenedor_bancos);	
		scaleToScreenSize();
		ArrayList<Object> bancos = ((InterbancariosDelegate)delegate).getListadoBancos();
		inicializarLista(bancos);
	}
	
	private void scaleToScreenSize(){
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());		
		gTools.scale(contenedorPrincipal);		
	}
	
	private void inicializarLista(ArrayList<Object> bancos){		
		@SuppressWarnings("deprecation")
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
		DelegateBaseOperacion delegateProv = new DelegateBaseOperacion(){			
			@Override
			public void performAction(Object selected) {
				if(selected instanceof NameValuePair)
					notificarResultado((NameValuePair)selected);
			}
		};
		listaSeleccion.setDelegate(delegateProv);
		listaSeleccion.setNumeroColumnas(1);
		listaSeleccion.setLista(bancos);
		listaSeleccion.setOpcionSeleccionada(-1);
		listaSeleccion.setSeleccionable(false);
		listaSeleccion.setAlturaFija(false);
		listaSeleccion.setNumeroFilas(7);
		listaSeleccion.setExisteFiltro(false);
		listaSeleccion.setSingleLine(true);
		listaSeleccion.cargarTabla();
		contenedorPrincipal.addView(listaSeleccion);
	}
	
	void notificarResultado(NameValuePair selected){
		Intent intent = new Intent();
		int result = 0;
		if (selected == null) {
			result = RESULT_CANCELED;
		} else {
			result = RESULT_OK;
			String institucionBancaria = selected.getName();
			String nombreBanco = selected.getValue();
			intent.putExtra("institucionBancaria", institucionBancaria);
			intent.putExtra("nombreBanco", nombreBanco);
		}
		setResult(result, intent);
		finish();
		parentViewsController.setActivityChanging(true);
	}
	
    @Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) 
			return;
		parentViewsController.setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	public void goBack() {
		notificarResultado(null);
		super.goBack();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(Server.ALLOW_LOG) Log.d(getLocalClassName(), "onTouchEvent");
		return super.onTouchEvent(event);
	}
	
}
