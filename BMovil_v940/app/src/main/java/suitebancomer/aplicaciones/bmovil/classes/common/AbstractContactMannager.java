package suitebancomer.aplicaciones.bmovil.classes.common;

import android.app.Activity;
import android.content.Intent;

/**
 * Clase base y abstracta para el control de contactos.
 * @see DonutContactManager
 */
public abstract class AbstractContactMannager {

	/**
	 * Número de teléfono del contacto.
	 */
	protected String numeroDeTelefono;
	
	/**
	 * Nombre del contacto.
	 */
	protected String nombreContacto;
	
	/**
	 * @return El número de teléfono del contacto.
	 */
	public String getNumeroDeTelefono() {
		return numeroDeTelefono;
	}
	
	/**
	 * @return El nombre del contacto.
	 */
	public String getNombreContacto() {
		return nombreContacto;
	}
	
	// Solo visible para clases hijas.
	/**
	 * Contructor por defecto.
	 */
	protected AbstractContactMannager() {
		numeroDeTelefono = "";
		nombreContacto = "";
	}

	/**
	 * Obtiene el manejador de contactos especifico de por plataforma.
	 * @return
	 */
	public static AbstractContactMannager getContactManager() {
		return (android.os.Build.VERSION_CODES.DONUT == android.os.Build.VERSION.SDK_INT) ? new DonutContactManager() : new ContactManager();
	}

	/**
	 * Obtiene los datos de contacto a partir del resultado de un Intent.
	 * @param callerActivity La actividad que invoca esté método.
	 * @param data El resultado del Intent.
	 */
	public abstract void getContactData(Activity callerActivity, Intent data);
	
	public abstract void requestContactData(Activity callerActivity);
}
