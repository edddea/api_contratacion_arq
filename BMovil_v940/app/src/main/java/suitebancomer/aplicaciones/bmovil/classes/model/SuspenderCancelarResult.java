/**
 * Parser para obtener la respuesta de nacar al realizar
 * la suspencion temporal del servicio � 
 * la cancelacion definitiva.
 */
package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * @author Francisco.Garcia
 *
 */
public class SuspenderCancelarResult implements ParsingHandler {

	/**
	 * Folio de la arquitectura que devuelve la trasacci�n
	 */
	private String folio;
	
	/**
	 * 
	 * @return  Folio de la arquitectura que devuelve la trasacci�n
	 */
	public String getFolio() {
		return folio;
	}
	
	/** 
	 * 
	 */
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		throw new ParsingException("Invalid process");
	}

	
	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		folio = parser.parseNextValue("folioArq");
	}

}
