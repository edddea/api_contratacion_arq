package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuComprarViewController;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class MenuComprarDelegate extends BaseDelegate{

	public final static long MENU_COMPRAR_DELEGATE_ID = 0xb35981680eb5fe06L;
	
	private BaseViewController controladorMenuComprar;

	public BaseViewController getControladorMenuComprar() {
		return controladorMenuComprar;
	}


	public void setControladorMenuComprar(BaseViewController controladorMenuComprar) {
		this.controladorMenuComprar = controladorMenuComprar;
	}

	public void performAction(Object obj) {
		((MenuComprarViewController)controladorMenuComprar).opcionSeleccionada((String)obj);
	}
	
	/*
	 * M�todo que valida que opciones se mostr�n en la lista selecci�n dependiendo del caso en el
	 * que entre que puede ser Transferir, comprar, pagar o cunsultar.
	 */
	public ArrayList<Object> cargarListasMenu(){
		ArrayList<Object> listaOpcionesMenu = new ArrayList<Object>(); 
		ArrayList<Object> registros;
		Session session = Session.getInstance(SuiteApp.getInstance());
		if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.compraTiempoAire,	session.getClientProfile())) {
			registros = new ArrayList<Object>(2);
			registros.add(Constants.OPERACION_COMPRAR_TIEMPO_AIRE);
			registros.add(SuiteApp.appContext.getString(R.string.menu_comprar_tiempo_aire));
			listaOpcionesMenu.add(registros);
		}
		return listaOpcionesMenu;
	}
	
}
