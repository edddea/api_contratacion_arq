package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;



import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirMisCuentasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.AmountField;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelper;

public class TransferMisCuentasDetalleViewController extends BaseViewController implements OnClickListener {
	
	private LinearLayout vista;
	private TransferirMisCuentasDelegate delegate;
	private CuentaOrigenViewController componenteCtaOrigen;
	private EditText textCuentaDestino;
	private ImageButton continuarButton;
	private AmountField textImporte;
	private EditText txtMotivo;

	public BmovilViewsController parentManager;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_transferir_mis_cuentas_detalle_view);
		setTitle(R.string.opcionesTransfer_menu_miscuentas,R.drawable.bmovil_transferir_icono);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("nuevo", parentManager.estados);
		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID));
		delegate = (TransferirMisCuentasDelegate)getDelegate();
		delegate.setCallerController(this);
		
		init();
	}
	
	public void init(){
		findViews();
		scaleForCurrentScreen();
		muestraComponenteCuentaOrigen();
		textCuentaDestino.addTextChangedListener(new BmovilTextWatcher(this));
		textImporte.addTextChangedListener(new BmovilTextWatcher(this));
		txtMotivo.addTextChangedListener(new BmovilTextWatcher(this));

	}
	
	@SuppressWarnings("deprecation")
	public void muestraComponenteCuentaOrigen(){
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
		componenteCtaOrigen.setDelegate(delegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(delegate.getInnerTransaction().getCuentaOrigen()));
		componenteCtaOrigen.init();
		componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.transferir);
		vista.addView(componenteCtaOrigen);
		delegate.setCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		cargaCuentaSeleccionada();
	}
	
	public void cargaCuentaSeleccionada(){
		textCuentaDestino.setText(Tools.enmascaraCuentaDestino(delegate.getInnerTransaction().getCuentaDestino().getNumber()));
	}
	
	@Override
	public void onClick(View v) {
		if (v == continuarButton) {
			if (!delegate.validaCampos(Tools.formatAmountForServer(textImporte.getAmount()))) {
				this.showInformationAlert(R.string.transferir_detalle_importe_validacion_alerta);
			} else  {
				if (delegate.getInnerTransaction().getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE)) {
					this.delegate.guardaImporte(textImporte.getAmount());
					this.delegate.guardaMotivo(txtMotivo.getText());
					delegate.consultaComision();
				}else {
					this.delegate.guardaImporte(textImporte.getAmount());
					this.delegate.guardaMotivo(txtMotivo.getText());

					//ARR
					Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
					
					//ARR
					paso2OperacionMap.put("evento_paso2", "event47");
					paso2OperacionMap.put("&&products", "operaciones;transferencias+mis cuentas");
					paso2OperacionMap.put("eVar12", "paso2:cuenta e importe");

					TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
					this.delegate.showConfirmacion();
				}
			}
		}
	}

	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		delegate.setCallerController(this);
		textImporte.reset();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		super.goBack();
	}
	
	public void actualizaCuentaOrigen(Account cuenta){
		if(Server.ALLOW_LOG) Log.d("TransferirMisCuentasDetalleViewController", "Actualizar la cuenta origen actual por  "+cuenta.getNumber());
		delegate.setCuentaSeleccionada(cuenta);
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled(true);
	}

	private void findViews() {
		vista = (LinearLayout)findViewById(R.id.transfer_mis_cuentas_detalle_layout);
		textCuentaDestino = (EditText)findViewById(R.id.detalle_transfer_cuenta_destino_edit);
		textImporte = (AmountField)findViewById(R.id.detalle_transfer_importe_edit);
		txtMotivo = (EditText)findViewById(R.id.txtMotivo);

		
		continuarButton = (ImageButton)findViewById(R.id.detalle_transferir_boton_continuar);
		continuarButton.setOnClickListener(this);
	}
	
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.rootLayout));
		
		guiTools.scale(findViewById(R.id.detalle_transfer_cuenta_destino_text), true);
		guiTools.scale(textCuentaDestino, true);
		
		guiTools.scale(findViewById(R.id.detalle_transfer_importe_text), true);
		guiTools.scale(textImporte, true);
		
		guiTools.scale(findViewById(R.id.lblMotivo), true);
		guiTools.scale(txtMotivo, true);
		
		guiTools.scale(continuarButton);
	}
}
