package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.bmovil.classes.common.AbstractContactMannager;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DineroMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovil;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.AmountField;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.SeleccionHorizontalViewController;
import tracking.TrackingHelper;


public class DineroMovilViewController extends BaseViewController {
	/**
	 * Codigo de la petici�n de un contacto desde la agenda.
	 */
	public static final int ContactsRequestCode = 1;
	
	/**
	 * Contenedor para el componente CuentaOrigen.
	 */
	private LinearLayout ctaOrigenLayout;
	
	/**
	 * Contenedor para el componente SeleccionHorizontal.
	 */
	private LinearLayout seleccionHorizontalLayout;
	
	/**
	 * El componente CuentaOrigen
	 */
	private CuentaOrigenViewController ctaOrigen;
	
	/**
	 * El componente SeleccionHorizontal.
	 */
	private SeleccionHorizontalViewController seleccionHorizontal;
	
	/**
	 * El delegado para este controlador.
	 */
	private DineroMovilDelegate delegate;
	
	/**
	 * Campo del número de telefono.
	 */
	private EditText campoNumeroTelefono;
	
	/**
	 * Campo de confirmaci�n para el número de telefono.
	 */
	private EditText campoConfirmarNumero;
	
	/**
	 * Campo de beneficiario.
	 */
	private EditText campoBeneficiario;
	
	/**
	 * Combo de importe.
	 */
	private TextView campoImporte;
	
	/**
	 * Campo de texto para otro importe;
	 */
	private TextView labelOtroImporte;
	
	/**
	 * Campo de otro importe.
	 */
	private AmountField campoOtroImporte;
	
	/**
	 * Campo de concepto.
	 */
	private EditText campoConcepto;
	
	/**
	 * Bandera para indicar si se estan pidiendo contactos.
	 */
	private boolean pidiendoContactos;
	//AMZ
			public BmovilViewsController parentManager;
			//AMZ
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_transferir_dinero_movil);
		setTitle(R.string.transferir_dineromovil_titulo, R.drawable.bmovil_dinero_movil_icono);		
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("nuevo", parentManager.estados);
		
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID));
		delegate = (DineroMovilDelegate)getDelegate();
		
		init();
	}
	
	/**
	 * Inicializa la vista.
	 */
	private void init() {
		if(!delegate.esFrecuente() && ! delegate.isOperacionRapida()){
			TransferirDelegate transferDelegate = (TransferirDelegate)parentViewsController.getBaseDelegateForKey(TransferirDelegate.TRANSFERIR_DELEGATE_ID);
			//freceuntes: correo electronico
			//TransferenciaDineroMovil transferencia = null;
			//transferencia = transferDelegate.getTransferenciaDineroMovil();
			TransferenciaDineroMovil transferencia = new TransferenciaDineroMovil();
			transferencia.setCuentaOrigen(transferDelegate.getTransferenciaDineroMovil().getCuentaOrigen());
			delegate.setTransferencia(transferencia);
		}
		
		pidiendoContactos = false;
		delegate.setViewController(this);
		delegate.setTipoOperacion(Constants.Operacion.dineroMovil);
		findViews();
		scaleForCurrentScreen();
		cargarComponenteCuentaOrigen();
		cargarSeleccionHorizontal();
		
		if(delegate.esFrecuente()) {
			cargarDatosDeFrecuente();
		} else if(delegate.isOperacionRapida()) {
			cargarDatosDeRapido();
		}
		
		campoBeneficiario.addTextChangedListener(new BmovilTextWatcher(this));
		campoConcepto.addTextChangedListener(new BmovilTextWatcher(this));
		campoConfirmarNumero.addTextChangedListener(new BmovilTextWatcher(this));
		campoNumeroTelefono.addTextChangedListener(new BmovilTextWatcher(this));
		campoOtroImporte.addTextChangedListener(new BmovilTextWatcher(this));
	}
	
	/**
	 * Busca las vistas en los layouts.
	 */
	private void findViews() {
		ctaOrigenLayout = (LinearLayout)findViewById(R.id.ctaOrigenLayout);
		seleccionHorizontalLayout = (LinearLayout)findViewById(R.id.seleccionHorizontalLayout);
		
		campoNumeroTelefono = (EditText)findViewById(R.id.textNumeroCel);
		campoConfirmarNumero = (EditText)findViewById(R.id.textConfirma);
		campoBeneficiario = (EditText)findViewById(R.id.textBeneficiario);
		campoImporte = (TextView)findViewById(R.id.comboImporte);   
		labelOtroImporte = (TextView)findViewById(R.id.lblOtroImporte);
		campoOtroImporte = (AmountField)findViewById(R.id.textOtroImporte);
		campoConcepto = (EditText)findViewById(R.id.textConcepto);
	}
	
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		guiTools.scale(findViewById(R.id.rootLayout));
		
		guiTools.scale(findViewById(R.id.lblCompania), true);
		guiTools.scale(seleccionHorizontalLayout);
		
		guiTools.scale(findViewById(R.id.lblNumeroCel), true);
		guiTools.scale(findViewById(R.id.btnAgenda));
		guiTools.scale(campoNumeroTelefono, true);
		
		guiTools.scale(findViewById(R.id.lblConfirma), true);
		guiTools.scale(campoConfirmarNumero, true);
		
		guiTools.scale(findViewById(R.id.lblBeneficiario), true);
		guiTools.scale(campoBeneficiario, true);
		
		guiTools.scale(findViewById(R.id.lblImporte), true);
		guiTools.scale(campoImporte, true);
		
		guiTools.scale(labelOtroImporte, true);
		guiTools.scale(campoOtroImporte, true);
		
		guiTools.scale(findViewById(R.id.lblConcepto), true);
		guiTools.scale(campoConcepto, true);
		
		guiTools.scale(findViewById(R.id.btnContinuar));
	}
	
	/**
	 * Carga el componente CuentaOrigen.
	 */
	private void cargarComponenteCuentaOrigen() {
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		@SuppressWarnings("deprecation")
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		ctaOrigen = new CuentaOrigenViewController(this, params, parentViewsController, this);
		ctaOrigen.setDelegate(delegate);
		ctaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		ctaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_dineromovil_titulo_cuentaretiro));
		ctaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(delegate.getTransferencia().getCuentaOrigen()));
		ctaOrigen.init();
		
		ctaOrigenLayout.addView(ctaOrigen);
	}
	
	/**
	 * Carga el componente SeleccionHorizontal.
	 */
	private void cargarSeleccionHorizontal() {
		//tituloSeleccionHorizontal.setText(getString(R.string.servicesPayment_servicesComponentTitle));
		@SuppressWarnings("deprecation")
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		ArrayList<Object> companias = delegate.cargarListaCompanias();
		seleccionHorizontal = new SeleccionHorizontalViewController(this, params, companias, getDelegate(), false);
		seleccionHorizontalLayout.addView(seleccionHorizontal);
	}
	
	/**
	 * Intenta obtener un contacto desde la agenda del dispoitivo.
	 * @param view La vista que manda a llamar este método.
	 */
	public void onBotonContactoClick(View view) {
		if(pidiendoContactos)
			return;
		pidiendoContactos = true;
		AbstractContactMannager.getContactManager().requestContactData(this);
	}
	
	/*
	 * No javadoc. 
	 */
	@Override  
	public void onActivityResult(int reqCode, int resultCode, Intent data) {  
	    super.onActivityResult(reqCode, resultCode, data);
	    pidiendoContactos = false;
	    if (resultCode == Activity.RESULT_OK) {
	    	switch (reqCode) {
			case ContactsRequestCode:
				obtenerContacto(data);
				break;
			}
	    }
	}
	
	/**
	 * Obtiene los datos del contacto del contacto seleccionado.
	 * @param data Los datos del Intent.
	 */
	public void obtenerContacto(Intent data) {
		AbstractContactMannager manager = AbstractContactMannager.getContactManager();
		manager.getContactData(this, data);
		
		if(manager.getNumeroDeTelefono().equalsIgnoreCase(""))
			return;

	    campoNumeroTelefono.setText(manager.getNumeroDeTelefono());
	    campoConfirmarNumero.setText(manager.getNumeroDeTelefono());
	    campoBeneficiario.setText(manager.getNombreContacto());
	}
	
	/**
	 * Muestra el Dialog para seleccionar el importe.
	 * @param view La vista que invoca esté método.
	 */
	public void onComboImporteClick(View view) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		Compania companiaSeleccionado = (Compania)seleccionHorizontal.getSelectedItem();
		final String[] importes = delegate.obtenerCatalogoDeImportes(companiaSeleccionado.getNombre());
		
		builder.setTitle(R.string.transferir_dineromovil_titulo_importe).setItems(importes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String importe = importes[which];
				campoImporte.setText(importe);
				
				if(getResources().getString(R.string.transferir_dineromovil_datos_importe_otro).equalsIgnoreCase(importe)) {
					campoOtroImporte.setText(R.string.transferir_dineromovil_datos_importe_inicial);
					
					labelOtroImporte.setVisibility(View.VISIBLE);
					campoOtroImporte.setVisibility(View.VISIBLE);
				} else {
					labelOtroImporte.setVisibility(View.GONE);
					campoOtroImporte.setVisibility(View.GONE);
				}
			}
		}).show();
	}
	
	/**
	 * Restablece los importes a 0.0 y oculta los campor de otro importe.
	 */
	public void limpiarImportes() {
		campoImporte.setText(R.string.bmovil_common_hint_combo);
		//campoOtroImporte.setText(R.string.transferir_dineromovil_datos_importe_inicial);
		//campoOtroImporte.setAmount(getString(R.string.transferir_dineromovil_datos_importe_inicial));
		campoOtroImporte.reset();
		
		labelOtroImporte.setVisibility(View.GONE);
		campoOtroImporte.setVisibility(View.GONE);
	}
	
	/**
	 * Bandera que indica si se esta operando con un frecuente.
	 */
	private boolean desdeFrecuente = false;
	
	/**
	 * @return La bandera que indica si se esta operando con un frecuente.
	 */
	public boolean isDesdeFrecuente() {
		return desdeFrecuente;
	}

	/**
	 * @param desdeFrecuente Valor a establecer para bandera que indica si se esta operando con un frecuente.
	 */
	public void setDesdeFrecuente(boolean desdeFrecuente) {
		this.desdeFrecuente = desdeFrecuente;
	}

	/**
	 * Carga y bloquea los campos obtenidos desde un frecuente.
	 */
	private void cargarDatosDeFrecuente() {
		seleccionHorizontal.setSelectedItem(delegate.getCompaniaSeleccionada());
		campoNumeroTelefono.setText(delegate.getTransferencia().getCelularbeneficiario());
		campoConfirmarNumero.setText(delegate.getTransferencia().getCelularbeneficiario());
		campoBeneficiario.setText(delegate.getTransferencia().getBeneficiario());
		campoConcepto.setText(delegate.getTransferencia().getConcepto());
		campoImporte.setText(R.string.bmovil_common_hint_combo);
		(findViewById(R.id.btnAgenda)).setEnabled(false);
		//ctaOrigen.setEnabled(false);
		seleccionHorizontal.bloquearComponente();
		campoNumeroTelefono.setEnabled(false);
		campoConfirmarNumero.setEnabled(false);
		campoBeneficiario.setEnabled(false);
		//campoConcepto.setEnabled(false);
		//campoImporte.setEnabled(false);
	}
	
	public void botonContinuarClick(View view) {
		if(!isHabilitado())
			return;
		setHabilitado(false);
		if(parentViewsController.isActivityChanging())
			return;
		TransferenciaDineroMovil model = delegate.getTransferencia();
		
		model.setCuentaOrigen(ctaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		model.setConcepto(Tools.removeSpecialCharacters(campoConcepto.getText().toString()));
		
		HashMap<String, String> mapa = new HashMap<String, String>();
		// TODO Verificar este cast.
		Compania compania = (Compania)seleccionHorizontal.getSelectedItem();
		mapa.put(compania.getClave(), compania.getNombre());
		model.setCompania(mapa);
		model.setNombreCompania(compania.getNombre());
		
		if(!delegate.esFrecuente()){
		// TODO Quitar hardcodes
			model.setTipoOperacion("TP09");
			model.setFrecuenteMulticanal("");
			model.setAliasFrecuente("");
		}
		
		//delegate.setTransferencia(model);
		
		delegate.validaDatos(campoNumeroTelefono.getText().toString(), 
							 campoConfirmarNumero.getText().toString(), 
							 (View.VISIBLE == campoOtroImporte.getVisibility()) ? null : campoImporte.getText().toString(), 
							 (View.VISIBLE == campoOtroImporte.getVisibility()) ? campoOtroImporte.getAmount() : null, 
							 Tools.removeSpecialCharacters(campoBeneficiario.getText().toString()));
	}

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}
		
	@Override
	protected void onResume() {	
		super.onResume();
		setHabilitado(true);
		if(delegate != null)
			delegate.setViewController(this);
		if(!pidiendoContactos) {
			if (parentViewsController.consumeAccionesDeReinicio()) {
				return;
			}
			getParentViewsController().setCurrentActivityApp(this);
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if(!pidiendoContactos) {
			parentViewsController.consumeAccionesDePausa();
		}
	}
	
	@Override
	public void goBack() {
		if(!pidiendoContactos) {
			parentViewsController.removeDelegateFromHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
		}
		super.goBack();
	}

	public void reactivarCtaOrigen(Account cuenta){
		delegate.getTransferencia().setCuentaOrigen(cuenta);
		ctaOrigen.getImgDerecha().setEnabled(true);
		ctaOrigen.getImgIzquierda().setEnabled(true);
		ctaOrigen.getVistaCtaOrigen().setEnabled(ctaOrigen.getCuentaOrigenDelegate().getListaCuentaOrigen().size() > 1);
	}
	
	/**
	 * Carga los elementos desde un r�pido.
	 */
	private void cargarDatosDeRapido() {
		// Establece datos.
		seleccionHorizontal.setSelectedItem(delegate.getCompaniaSeleccionada());
		campoNumeroTelefono.setText(delegate.getTransferencia().getCelularbeneficiario());
		campoConfirmarNumero.setText(delegate.getTransferencia().getCelularbeneficiario());
		campoBeneficiario.setText(delegate.getTransferencia().getBeneficiario());
		campoConcepto.setText(delegate.getTransferencia().getConcepto());
		campoImporte.setText(getString(R.string.transferir_dineromovil_datos_importe_otro));
		campoOtroImporte.setAmount(delegate.getTransferencia().getImporte());
		labelOtroImporte.setVisibility(View.VISIBLE);
		campoOtroImporte.setVisibility(View.VISIBLE);
		
		
		
		// Deshbilita los campos precargados.
		(findViewById(R.id.btnAgenda)).setEnabled(false);
		seleccionHorizontal.bloquearComponente();
		campoNumeroTelefono.setEnabled(false);
		campoConfirmarNumero.setEnabled(false);
		campoBeneficiario.setEnabled(false);
		campoImporte.setEnabled(false);
		campoOtroImporte.setEnabled(false);
	}
}
