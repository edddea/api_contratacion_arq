/*
 * Copyright (c) 2009 BBVA. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * ConsultaECExtract wraps an account periods, the list of the last 12 periods
 * 
 * @author Carlos Santoyo
 */
public class ConsultaECResult  implements ParsingHandler {

    /**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 3272545845918202838L;

	   /**
	     * The pdf link
	     */
	    private static String pdf = null;
	    
	   
	    
  
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
	   setPdf(parser.parseNextValue("pdf"));
	  
	}

	public String getPdf() {
		return pdf;
	}

	public void setPdf(String pdf) {
		ConsultaECResult.pdf = pdf;
	}

	
    
}
