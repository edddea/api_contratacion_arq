package suitebancomer.aplicaciones.bmovil.classes.model;

import bancomer.api.common.commons.Constants;


public class CambioDePerfil {

	private Constants.Perfil perfilACambiar;

	public Constants.Perfil getPerfilACambiar() {
		return perfilACambiar;
	}

	public void setPerfilACambiar(Constants.Perfil perfilACambiar) {
		this.perfilACambiar = perfilACambiar;
	}

}

