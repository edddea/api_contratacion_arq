package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;

import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultaMovimientosViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.PagoTdcViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.ImportesTDCData;
import suitebancomer.aplicaciones.bmovil.classes.model.Movement;
import suitebancomer.aplicaciones.bmovil.classes.model.MovementExtract;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;
import android.view.View;

import com.bancomer.mbanking.R;

import com.bancomer.mbanking.SuiteApp;
import java.util.HashMap;
import java.util.Map;
import tracking.TrackingHelper;

public class MovimientosDelegate extends BaseDelegate{

	public final static long MOVIMIENTOS_DELEGATE_ID = 0x4fd454ce67b39f1L;
	

	Account cuentaActual;
	ConsultaMovimientosViewController consultaMovimientosViewController;
	MovementExtract totalMovementsExtract;
	Movement movimiento;
	ImportesTDCData resImpTDC;
	
	// control peticiones
	private Boolean errorGenerico = false;
	private ServerResponse errorImportesSR = null;
	
	boolean listaMoviemientosVaciaError=false;
	
	public ImportesTDCData getResImpTDC() {
		return resImpTDC;
	}

	public void setResImpTDC(ImportesTDCData resImpTDC) {
		this.resImpTDC = resImpTDC;
	}
	
	private int indiceMovimientoSeleccionado;

	public ConsultaMovimientosViewController getConsultaMovimientosViewController() {
		return consultaMovimientosViewController;
	}

	public void setConsultaMovimientosViewController(ConsultaMovimientosViewController consultaMovimientosViewController) {
		this.consultaMovimientosViewController = consultaMovimientosViewController;
	}
	
	public Account getCuentaSeleccionada(){
		return consultaMovimientosViewController.getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada();
	}
	
	public void showDetalleMovimientosViewController(){
		((BmovilViewsController)consultaMovimientosViewController.getParentViewsController()).showDetalleMovimientosViewController();
	}
	
	public String getDescripcionDetalle(){
		return "";
	}

	public MovementExtract getTotalMovementsExtract() {
		return totalMovementsExtract;
	}
	
	public Movement getMovimiento() {
		return movimiento;
	}

	public void setMovimiento(Movement movimiento) {
		this.movimiento = movimiento;
	}

	public void consultaMovimientos(String periodo) {
		if (cuentaActual == null) {
			cuentaActual = getCuentaSeleccionada();
		}
		indiceMovimientoSeleccionado = -1;
		
		Session session = Session.getInstance(SuiteApp.appContext);
		
		//prepare data
        Hashtable<String,String> paramTable = new Hashtable<String,String>();
        paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
        paramTable.put(ServerConstants.PARAMS_TEXTO_NP, "");
        paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());
        paramTable.put("TP", cuentaActual.getType());
        paramTable.put("AS", cuentaActual.getNumber());
        paramTable.put(ServerConstants.PERIODO_ETIQUETA,periodo);
		paramTable.put("order", "NT*NP*IU*TP*AS*PE");
        //JAIG
		doNetworkOperation(Server.MOVEMENTS_OPERATION, paramTable, false,new MovementExtract(),isJsonValueCode.NONE, consultaMovimientosViewController);
	}

	public void consultaMovimientos2(String periodo) {
		if (cuentaActual == null) {
			cuentaActual = getCuentaSeleccionada();
		}
		indiceMovimientoSeleccionado = -1;

		Session session = Session.getInstance(SuiteApp.appContext);

		//prepare data
		Hashtable<String,String> paramTable = new Hashtable<String,String>();
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
		paramTable.put(ServerConstants.PARAMS_TEXTO_NP, "");
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());
		paramTable.put("TP", cuentaActual.getType());
		paramTable.put("AS", cuentaActual.getNumber());
		paramTable.put(ServerConstants.PERIODO_ETIQUETA,periodo);
		paramTable.put("order", "NT*NP*IU*TP*AS*PE");
		//JAIG
		doNetworkOperation(Server.MOVEMENTS_OPERATION, paramTable, false,totalMovementsExtract,isJsonValueCode.NONE, consultaMovimientosViewController);
	}
	
	public void consultaImportesTDC() {
		if (cuentaActual == null) {
			cuentaActual = getCuentaSeleccionada();
		}
//		indiceMovimientoSeleccionado = -1;
		
		Session session = Session.getInstance(SuiteApp.appContext);
		
		//prepare data
        Hashtable<String,String> paramTable = new Hashtable<String,String>();       
        
        //paramTable.put(ServerConstants.OPERACION, Server.OPERATION_CODES[Server.OP_CONSULTA_TDC]);
        paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());//cuentaActual.getCelularAsociado());
        paramTable.put(ServerConstants.TIPO_CUENTA,"TC");
        paramTable.put(ServerConstants.NUMERO_TARJETA,cuentaActual.getNumber());
        paramTable.put(ServerConstants.PERIODO, "1");
		//JAIG
		doNetworkOperation(Server.OP_CONSULTA_TDC, paramTable,true, new ImportesTDCData(),isJsonValueCode.NONE, consultaMovimientosViewController);
	}

	public void consultaMovimientosTransito() {
		if (cuentaActual == null) {
			cuentaActual = getCuentaSeleccionada();
		}

		Session session = Session.getInstance(SuiteApp.appContext);

		//prepare data
		Hashtable<String,String> paramTable = new Hashtable<String,String>();
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
		paramTable.put(ServerConstants.TIPO_CUENTA,"TC");
		paramTable.put(ServerConstants.NUMERO_TARJETA,cuentaActual.getNumber());

		//JAIG
		doNetworkOperation(Server.MOVEMENTS_OPERATION, paramTable, true,new ImportesTDCData(),isJsonValueCode.NONE, consultaMovimientosViewController);
	}
	
	public void performAction(Object obj) {
		if (obj instanceof Account) {
			//cuentaActual = (Account)obj;
			  Account nuevaCuenta = (Account) obj;
			  		if (!cuentaActual.equals(nuevaCuenta)) {
			  			// Debo borrar los movimientos que ya hubiera de la cuenta anterior
			  				totalMovementsExtract = null;
			  				movimiento = null;
			  				consultaMovimientosViewController.borrarListaMovimientos();
			  				consultaMovimientosViewController.borrarTDCDataBlock();
			}
			cuentaActual = nuevaCuenta;
			if(cuentaActual.getType().equals(Constants.CREDIT_TYPE)){
				consultaImportesTDC();
			}else{
			consultaMovimientos(Constants.MOVIMIENTOS_MES_ACTUAL);
			}
		}else if (obj instanceof Movement) {
			setMovimiento((Movement)obj);
			indiceMovimientoSeleccionado = totalMovementsExtract.getMovements().indexOf(getMovimiento());
			//AMZ inicio
			Map<String,Object> ConsultaRealizadaMap = new HashMap<String, Object>();
			
			ConsultaRealizadaMap.put("evento_realizada","event52");
			ConsultaRealizadaMap.put("&&products","consulta;movimientos");
			ConsultaRealizadaMap.put("eVar12","operacion realizada");
			TrackingHelper.trackConsultaRealizada(ConsultaRealizadaMap);
		
		//AMZ fin
			showDetalleMovimientosViewController();
		}
	}
	
	public int getIndiceMovimientoSeleccionado() {
		return indiceMovimientoSeleccionado;
	}
	
	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode,BaseViewController caller) {
		((BmovilViewsController) consultaMovimientosViewController
				.getParentViewsController()).getBmovilApp()
				.invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode,caller, true);
	}
	
	public void analyzeResponse(int operationId, ServerResponse response) {
		/*if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			totalMovementsExtract = (MovementExtract) response.getResponse();
			consultaMovimientosViewController.llenaListaDatos();
			consultaMovimientosViewController.muestraVerMasMovimientos(!totalMovementsExtract.getMovements().isEmpty() 
						&& !totalMovementsExtract.getPeriodo().equals(Constants.MOVIMIENTOS_DOS_MESES_ATRAS));
		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING || 
				response.getStatus() == ServerResponse.OPERATION_ERROR) {
			//totalMovementsExtract = null; 
			//consultaMovimientosViewController.llenaListaDatos();

			if (totalMovementsExtract != null) {
				consultaMovimientosViewController.ocultaIndicadorActividad();
			} else {
				consultaMovimientosViewController.llenaListaDatos();
			}
			consultaMovimientosViewController.muestraVerMasMovimientos(false);
			consultaMovimientosViewController.showInformationAlertEspecial(consultaMovimientosViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
		}*/
		if(operationId == Server.MOVEMENTS_OPERATION){
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				totalMovementsExtract = (MovementExtract) response.getResponse();
				totalMovementsExtract.setAccountNumber(cuentaActual.getNumber());
				totalMovementsExtract.setAccountType(cuentaActual.getType());
				
				if (cuentaActual.getType().equals(Constants.CREDIT_TYPE)){
				consultaMovimientosViewController.llenaListaDatos(listaMoviemientosVaciaError);
				
				if (!listaMoviemientosVaciaError){
					consultaMovimientosViewController.muestraVerMasMovimientos(!totalMovementsExtract.getMovements().isEmpty() 
								&& !totalMovementsExtract.getPeriodo().equals(Constants.MOVIMIENTOS_DOS_MESES_ATRAS));
				}else {
					consultaMovimientosViewController.ocultaIndicadorActividad();
					if(errorGenerico){
						consultaMovimientosViewController.muestraErrorGenericoTDC();
						//errorGenerico=false;
					}else{
						consultaMovimientosViewController.showInformationAlertEspecial(consultaMovimientosViewController.getString(R.string.label_error), errorImportesSR.getMessageCode(), errorImportesSR.getMessageText(), null);
					}
				}

				
				}else {
					consultaMovimientosViewController.llenaListaDatos(false);
					consultaMovimientosViewController.muestraVerMasMovimientos(!totalMovementsExtract.getMovements().isEmpty() 
							&& !totalMovementsExtract.getPeriodo().equals(Constants.MOVIMIENTOS_DOS_MESES_ATRAS));

				}
				

			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING || 
					response.getStatus() == ServerResponse.OPERATION_ERROR) {
				//totalMovementsExtract = null; 
				//consultaMovimientosViewController.llenaListaDatos();
	
				if (totalMovementsExtract != null) {
					consultaMovimientosViewController.ocultaIndicadorActividad();
				}
				//por si viene de error TDC por contrato y tambien hay error de movimientos 
				else if (cuentaActual.getType().equals(Constants.CREDIT_TYPE)){
					consultaMovimientosViewController.llenaListaDatos(listaMoviemientosVaciaError);
				}
				else {
					consultaMovimientosViewController.llenaListaDatos(false);
				}
				
				
				
				consultaMovimientosViewController.muestraVerMasMovimientos(false);
				consultaMovimientosViewController.showInformationAlertEspecial(consultaMovimientosViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
			}
		}else if(operationId == Server.OP_CONSULTA_TDC){
			totalMovementsExtract = new MovementExtract();
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {

				resImpTDC = (ImportesTDCData)response.getResponse();
				totalMovementsExtract.setAccountNumber(cuentaActual.getNumber());
				totalMovementsExtract.setAccountType(cuentaActual.getType());
				listaMoviemientosVaciaError=false;

				
			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING){
				listaMoviemientosVaciaError=false;
				
			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				listaMoviemientosVaciaError=true;

				consultaMovimientosViewController.ocultaIndicadorActividad();

				if (((response.getMessageCode()!=null) && (!response.getMessageCode().equals("MPE1245"))) ||(response.getMessageCode()==null)){
							
					  // pagoTdcViewController.showInformationAlert(pagoTdcViewController.getString(R.string.label_error), "La información no se encuentra disponible por el momento, consulte mas tarde", null);
					errorGenerico = true;
					errorImportesSR = null;
//					consultaMovimientosViewController.muestraErrorGenericoTDC();
					   
				}else {
					errorGenerico = false;
					errorImportesSR = response;
//					consultaMovimientosViewController.showInformationAlertEspecial(consultaMovimientosViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
					
					if (consultaMovimientosViewController.getVerMasMovimientos()!=null){
						
						consultaMovimientosViewController.getVerMasMovimientos().setVisibility(View.GONE);

					}
				}
				
				
				
			}
			
			consultaMovimientos2(Constants.MOVIMIENTOS_MES_ACTUAL);

		}
	}

	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada seg�n sea requerido.
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();		
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		if(profile ==  Constants.Perfil.avanzado ){
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		
//		if(Constants.PROFILE_ADVANCED_03.equalsIgnoreCase(profile)) {
			for(Account acc : accounts) {
				if(acc.isVisible()) {
					accountsArray.add(acc);
					break;
				}
			}
			
			for(Account acc : accounts) {
				if(!acc.isVisible())
					accountsArray.add(acc);
			}
/*		} else {
			for(Account acc : accounts) {
				if(acc.isVisible())
					accountsArray.add(acc);
			}
		}*/
		}else{
			accountsArray.add(Tools.obtenerCuentaEje());
		}
		return accountsArray;
	}

	public void verMasMovimientos() {
		int periodoProximoInt = Integer.valueOf(this.totalMovementsExtract.getPeriodo()).intValue() + 1;
		if(cuentaActual.getType().equals(Constants.CREDIT_TYPE)){
		consultaMovimientos2(String.valueOf(periodoProximoInt));
		}
		else{
			consultaMovimientos(String.valueOf(periodoProximoInt));
		}
	}

	public void verMovimientosTransito() {
		if(cuentaActual.getType().equals(Constants.CREDIT_TYPE)){
			consultaMovimientosTransito();
		}
	}
}
