package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;
import java.util.Hashtable;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.StartBmovilInBack;
import com.bancomer.mbanking.SuiteApp;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;

public class DetalleOfertaEFIDelegate extends BaseDelegate{
	AceptaOfertaEFI aceptaOfertaEFI;
	OfertaEFI ofertaEFI;
	Promociones promocion;
	String fechaCat="";
	String leyenda="";
	String leyenda1= "";
	String leyenda2= " ";
	String cuentadeposito="";
	
	
	public static final long DETALLE_OFERTA_EFI_DELEGATE = 631453334566765687L;
	private BaseViewController controladorDetalleILCView;


	public DetalleOfertaEFIDelegate(OfertaEFI ofertaEFI, Promociones promocion){
		this.ofertaEFI= ofertaEFI;
		this.promocion= promocion;

	}
	
	public String getLeyendaInicial() {
		return leyenda;
	}
	
	public String getLeyendaInicial1() {
		return leyenda1;
	}
	
	public String getLeyendaInicial2() {
		return leyenda2;
	}
	
	public void setleyenda(String ley) {
	
		this.leyenda=ley;
		
	}
	
	//Cuentas
	
	public String getCuentaDeposito() {
		
		return cuentadeposito;
	}
	
	public void setCuentaDeposito(String cuenta) {
	
		this.cuentadeposito=cuenta;
		
	}
	
		
	public OfertaEFI getOfertaEFI() {
		return ofertaEFI;
	}
	
	public void setOfertaEFI(OfertaEFI ofertaEFI) {
		this.ofertaEFI = ofertaEFI;
	}
	
	public Promociones getPromocion() {
		return promocion;
	}

	public void setPromocion(Promociones promocion) {
		this.promocion = promocion;
	}

	public void realizaOperacion(int idOperacion,
			BaseViewController baseViewController) {
		if (idOperacion == Server.ACEPTACION_OFERTA_EFI){
			Session session = Session.getInstance(SuiteApp.appContext);
			String user = session.getUsername();
			String ium= session.getIum();
			formatoFechaCat();
			Hashtable<String, String> params = new Hashtable<String, String>();
			//params.put("operacion", "aceptaOfertaEFI");
			params.put("numeroCelular", user);
			params.put("cveCamp",promocion.getCveCamp());
			String comisionEFIPorcentaje= ofertaEFI.getComisionEFIPorcentaje().replace(".", ",");
			params.put("comisionEFIPorcentaje",comisionEFIPorcentaje);
			//para agregar una coma al valor
			int starcomision=ofertaEFI.getComisionEFIMonto().length()-2;
			String comisionEFIMonto= ofertaEFI.getComisionEFIMonto().substring(0,starcomision)+","+ofertaEFI.getComisionEFIMonto().substring(starcomision);
			params.put("comisionEFIMonto",comisionEFIMonto);
			params.put("celula",ofertaEFI.getCelula());
			params.put("edicion",ofertaEFI.getEdicion());
			params.put("numeroTarjeta",ofertaEFI.getAccountOrigen().getNumber());
			
			//int start=ofertaEFI.getAccountDestino().getNumber().length()-10;  //Muestra 10 ultimos digitos de la tarjeta ()-10;
			//int start=ofertaEFI.getAccountDestino().getNumber().length()-20;
			//String numeroCuenta=ofertaEFI.getAccountDestino().getNumber().substring(start);
			String numeroCuenta=ofertaEFI.getAccountDestino().getNumber();
			params.put("numeroCuenta",numeroCuenta);
			String tipoCuenta = ofertaEFI.getAccountDestino().getType();
			params.put("tipoCuenta", tipoCuenta);  //Tipo de cuenta
			//
			params.put("importe",promocion.getMonto());//promociones modelo
			params.put("impDispuesto",ofertaEFI.getMontoDisponible());//montodisponible
			String tasaAnual= ofertaEFI.getTasaAnual().replace(".", ",");
			params.put("tasaAnual",tasaAnual);
			params.put("fechaCat",fechaCat);// con o sin formato?
			String Cat= ofertaEFI.getCat().replace(".", ",");
			params.put("Cat", Cat);
			int starpagoMensual=ofertaEFI.getPagoMensualSimulador().length()-2;//getComisionEFIMonto().length()-2;
			String pagoMensualSimulador=ofertaEFI.getPagoMensualSimulador().substring(0,starpagoMensual)+","+ofertaEFI.getPagoMensualSimulador().substring(starpagoMensual);
			params.put("pagoMensualSimulador", pagoMensualSimulador);
			params.put("IUM", ium);
			//JAIG SI
			doNetworkOperation(Server.ACEPTACION_OFERTA_EFI, params,true, new AceptaOfertaEFI(),isJsonValueCode.ONECLICK,
					baseViewController);
		}else if(idOperacion == Server.RECHAZO_OFERTA){
			Session session = Session.getInstance(SuiteApp.appContext);
			String user = session.getUsername();
			String ium= session.getIum();
			Hashtable<String, String> params = new Hashtable<String, String>();
			//Tiene otro nombre
			//params.put("operacion", "noAceptacionBMovil");
			params.put("numeroCelular", user);
			params.put("cveCamp",promocion.getCveCamp());
			params.put("descripcionMensaje","0002");
			params.put("contrato",ofertaEFI.getNumContrato());
			params.put("IUM", ium);
			//JAIG SI
			doNetworkOperation(Server.RECHAZO_OFERTA, params,true,null,isJsonValueCode.ONECLICK,
					baseViewController);
		}
	
	}
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson, ParsingHandler handler,isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if( controladorDetalleILCView != null)
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
	}
	
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (operationId == Server.ACEPTACION_OFERTA_EFI) {
			if(response.getStatus() == ServerResponse.OPERATION_ERROR){
				aceptaOfertaEFI = (AceptaOfertaEFI) response.getResponse();
				Session session = Session.getInstance(SuiteApp.appContext);
				session.setPromocion(aceptaOfertaEFI.getPromociones());
				controladorDetalleILCView.showInformationAlert("Aviso",response.getMessageCode()+"\n"+response.getMessageText(),
						new OnClickListener() {
							@Override
							public void onClick(
									DialogInterface dialog,
									int which) {
								dialog.dismiss();

								if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session

									SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
									ActivityCompat.finishAffinity(controladorDetalleILCView);

									Intent i=new Intent(controladorDetalleILCView, StartBmovilInBack.class);
									i.putExtra(bancomer.api.common.commons.Constants.RESULT_STRING, Constants.FALSE_STRING);
									i.putExtra( bancomer.api.common.commons.Constants.TIMEOUT_STRING, Constants.FALSE_STRING);
									i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									controladorDetalleILCView.startActivity(i);
									controladorDetalleILCView.finish();
								}else {
									((BmovilViewsController) controladorDetalleILCView.getParentViewsController()).showMenuPrincipal();
								}
							}
						});
			}else{
			aceptaOfertaEFI = (AceptaOfertaEFI) response.getResponse();
			Session session = Session.getInstance(SuiteApp.appContext);
			session.setPromocion(aceptaOfertaEFI.getPromociones());
			//para guardar el nuevo saldo del cliente
            Account[] accounts =session.getAccounts();
            
            for(int i=0; i< accounts.length; i++){
                if(ofertaEFI.getAccountOrigen().getNumber().equals(accounts[i].getNumber())){
                    double montoDisponible= Double.parseDouble(ofertaEFI.getMontoDisponible());
                    double saldoActualizado= montoDisponible+accounts[i].getBalance();
                    accounts[i].setBalance(saldoActualizado);            
                    break;
                }
            }   
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showExitoEFI(aceptaOfertaEFI, ofertaEFI);
			}
		}else if(operationId== Server.RECHAZO_OFERTA){
			if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session

				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
				ActivityCompat.finishAffinity(controladorDetalleILCView);

				Intent i=new Intent(controladorDetalleILCView, StartBmovilInBack.class);
				i.putExtra(bancomer.api.common.commons.Constants.RESULT_STRING, Constants.TRUE_STRING);
				i.putExtra( bancomer.api.common.commons.Constants.TIMEOUT_STRING, Constants.FALSE_STRING);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				controladorDetalleILCView.startActivity(i);
				controladorDetalleILCView.finish();
			}else {
				((BmovilViewsController) controladorDetalleILCView.getParentViewsController()).showMenuPrincipal();
			}
		}		
	}
	

			
	public AceptaOfertaEFI getAceptaOfertaEFI() {
		return aceptaOfertaEFI;
	}

	public void setAceptaOfertaEFI(AceptaOfertaEFI aceptaOfertaEFI) {
		this.aceptaOfertaEFI = aceptaOfertaEFI;
	}

	public BaseViewController getControladorDetalleILCView() {
		return controladorDetalleILCView;
	}
	public void setControladorDetalleILCView(
			BaseViewController controladorDetalleILCView) {
		this.controladorDetalleILCView = controladorDetalleILCView;
	}
	
	public ArrayList<Account> cargaCuentasOrigen() {
		cuentaOrigen();
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		accountsArray= ofertaEFI.getAccountEFI();		
		return accountsArray;
	}
	
	public void cuentaOrigen(){
		Session session = Session.getInstance(SuiteApp.appContext);
		Account[] accounts=session.getAccounts();
		ArrayList<Account> accountEFI;
		accountEFI=new ArrayList<Account>();
		for(int i=0; i<accounts.length;i++){
			if(accounts[i].getNumber().compareTo(ofertaEFI.getAccountOrigen().getNumber())==0){
				if(!accounts[i].getAlias().equals(""))
					ofertaEFI.getAccountOrigen().setAlias(accounts[i].getAlias());				
					ofertaEFI.getAccountOrigen().setBalance(accounts[i].getBalance());
					ofertaEFI.getAccountOrigen().setType(Constants.CREDIT_TYPE);
					ofertaEFI.getAccountOrigen().setCelularAsociado(accounts[i].getCelularAsociado());
				 	accountEFI.add(ofertaEFI.getAccountOrigen());
					ofertaEFI.setAccountEFI(accountEFI);
					break;
			}
		}
		if(ofertaEFI.getAccountEFI()==null){
			ofertaEFI.getAccountOrigen().setType(Constants.CREDIT_TYPE);
			accountEFI.add(ofertaEFI.getAccountOrigen());
			ofertaEFI.setAccountEFI(accountEFI);
		}
	}
	
	/*public boolean validarDatos(){
		double  montoDisp= Double.parseDouble(ofertaEFI.getMontoDisponible());
		double  montoDispTDC= Double.parseDouble(ofertaEFI.getMontoDispTDC());
		if(montoDisp > montoDispTDC){
			controladorDetalleILCView.showInformationAlert(
					R.string.bmovil_alert_saldo_novalido,
					new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog,
								int which) {
							showModificarImporte();
						}
					});
			return false;
		}
		return true;
	}*/
	
	public void showModificarImporte(){
		((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showModificaImporteEFI(ofertaEFI);
	}
	
	public void formatoFechaCat(){
		try{	
			String[] meses = { "enero", "febrero", "marzo", "abril", "mayo", 
			      "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };		
			String fechaCatM= ofertaEFI.getFechaCat();
			String fechaC=fechaCatM.replaceAll("de+","");
			String fechaCa=fechaC.replaceAll("\\s","");
			String mesSelec=fechaCa.substring(2,fechaCa.length()-4);
			String dia= fechaCatM.substring(0,2);
			String anio= fechaCatM.substring(fechaCatM.length()-4);
			int mes = -1;
		      for (int i=0; i<meses.length; i++) {
		         if ( meses[i].equalsIgnoreCase(mesSelec ) ) {
		            // El mes coincide con el elemento actual del array
		            mes = i+1;
		            break;
		         }
		      }
		     if(mes==10||mes==11||mes==12){ 
		    	 fechaCat=anio+"-"+mes+"-"+dia;
		     }else{
		    	 fechaCat=anio+"-0"+mes+"-"+dia;
		     }
				}catch(Exception ex){
					if(Server.ALLOW_LOG) Log.e("","error formato"+ex);
			}
		}
	
	public void cuentaDestino(){
		Session session = Session.getInstance(SuiteApp.appContext);
		Account[] accounts =session.getAccounts();
		if(ofertaEFI.getAccountDestino().getNumber()==null || ofertaEFI.getAccountDestino().getNumber().equals("")){
		for(int i=0; i< accounts.length; i++){
			if(accounts[i].getType().compareTo(Constants.CREDIT_TYPE)!=0 || accounts[i].getType().compareTo(Constants.EXPRESS_TYPE)!=0){
				ofertaEFI.setAccountDestino(accounts[i]);
					break;
				}
			}
		}
	}	
	
	
	
}
