package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import suitebancomer.aplicaciones.bmovil.classes.common.AbstractContactMannager;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MantenimientoSpeiMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.MantenimientoSpei.TiposOperacionSpei;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.SeleccionHorizontalViewController;

public class DetalleCuentaSpeiMovilViewController extends BaseViewController {
	//#region Class fileds.
	/**
	 * Activity request code for load a contact from the phone.
	 */
	public static final int CONTACTS_REQUEST_CODE = 1;
	
	/**
	 * The delegate for this screen.
	 */
	private MantenimientoSpeiMovilDelegate ownDelegate; 
	
	/**
	 * The origin account component.
	 */
	private CuentaOrigenViewController originAccount;
	
	/**
	 * The ListaSeleccion component to show the accounts list.
	 */
	private SeleccionHorizontalViewController companiesList;
	
	/**
	 * The text field to indicate the new phone number to associate.
	 */
	private EditText phoneNumberEdittext;
	
	/**
	 * The text field to indicate the new phone number to associate.
	 */
	private EditText confirmPhoneNumberEdittext;
	
	/**
	 * The linear layout for hold the accounts list.
	 */
	private LinearLayout companiesListLayout;
	
	/**
	 * Flag to indicate if the activity is loading contacts.
	 */
	private boolean loadingContact;
	//#endregion
	
	//#region Life-cycle management.
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_detalle_asociacion_cuenta_telefono);
		setTitle(R.string.bmovil_asociar_cuenta_telefono_alternative_title, R.drawable.icono_spei);
		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(MantenimientoSpeiMovilDelegate.DELEGATE_ID));
		ownDelegate = (MantenimientoSpeiMovilDelegate) getDelegate();
		ownDelegate.setOwnerController(this);
		
		loadingContact = false;
		
		findViews();
		scaleForCurrentScreen();
		initialize();	
	}

	/**
	 * Search the required views for the operation of this screen.
	 */
	private void findViews() {
//		speiToggleButton = (CheckBox)findViewById(R.id.speiAvailableToogleButton);
		phoneNumberEdittext = (EditText)findViewById(R.id.phoneEdittext);
		confirmPhoneNumberEdittext = (EditText)findViewById(R.id.phoneConfirmEdittext);
		companiesListLayout = (LinearLayout)findViewById(R.id.companiesListLayout);
	}
	
	/**
	 * Scale the shown views to fit the current screen resolution.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.mainLayout));
		guiTools.scale(findViewById(R.id.accountLayout), false);
		guiTools.scale(findViewById(R.id.companiesListTitle), true);
		guiTools.scale(findViewById(R.id.companiesListLayout), false);
		guiTools.scale(findViewById(R.id.phoneLabel), true);
		guiTools.scale(findViewById(R.id.phoneEdittext), true);
		guiTools.scale(findViewById(R.id.phoneConfirmLabel), true);
		guiTools.scale(findViewById(R.id.phoneConfirmEdittext), true);
		guiTools.scale(findViewById(R.id.searchContactsButton), false);
		guiTools.scale(findViewById(R.id.confirmButton));
	}
	
	/**
	 * Initialize the screen values.
	 */
	private void initialize() {
		originAccount = ownDelegate.loadOriginAccountComponent();
		if(null != originAccount)
			((LinearLayout)findViewById(R.id.accountLayout)).addView(originAccount);
		else
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while loading the origin account component.");
		
		companiesList = ownDelegate.loadCompaniesList();
		if(null != companiesList)
			companiesListLayout.addView(companiesList);
		else
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while loading the phone companies list.");
		
		if(ownDelegate.getSpeiModel().getTipoOperacion() == TiposOperacionSpei.Modify) {
			//phoneNumberEdittext.setText(ownDelegate.getSelectedSpeiAccount().getTelefonoAsociado());
			phoneNumberEdittext.setText(ownDelegate.getSelectedSpeiAccount().getCelularAsociado());
			//confirmPhoneNumberEdittext.setText(ownDelegate.getSelectedSpeiAccount().getTelefonoAsociado());
			confirmPhoneNumberEdittext.setText(ownDelegate.getSelectedSpeiAccount().getCelularAsociado());
		}
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		ownDelegate.setOwnerController(this);
		getParentViewsController().setCurrentActivityApp(this);
		if(!loadingContact)
			parentViewsController.consumeAccionesDeReinicio();
		
//		if(!ownDelegate.isAssociate())
//			onConfirmButtonClicked(null);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if(!loadingContact)
			parentViewsController.consumeAccionesDePausa();
	}
	//#endregion
	
	//#region Network.
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		ownDelegate.analyzeResponse(operationId, response);
	}
	//#endregion
	
	//#region OnClick Handlers
	/**
	 * Procedure when the confirm button is clicked.
	 * @param sender The clicked view.
	 */
	public void onConfirmButtonClicked(View sender) {
		ownDelegate.validaDatos(phoneNumberEdittext.getText().toString(), 
								confirmPhoneNumberEdittext.getText().toString(), 
								companiesList.getSelectedItem());
	}
	
	/**
	 * Procedure when the contacts button is clicked.
	 * @param sender The clicked view.
	 */
	public void onContactsButtonClicked(View sender) {
		if(loadingContact)
			return;
		loadingContact = true;
		AbstractContactMannager.getContactManager().requestContactData(this);
	}
	//#endregion

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == CONTACTS_REQUEST_CODE) {
			loadingContact = false;
			if(Activity.RESULT_OK == resultCode) {
				obtainContactData(data);
			}
		} else {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}
	
	/**
	 * Obtains and set the contact data.
	 * @param data The activity result data.
	 */
	private void obtainContactData(Intent data) {
		AbstractContactMannager manager = AbstractContactMannager.getContactManager();
		manager.getContactData(this, data);
		
		if(manager.getNumeroDeTelefono().equalsIgnoreCase(""))
			return;

		phoneNumberEdittext.setText(manager.getNumeroDeTelefono());
		confirmPhoneNumberEdittext.setText(manager.getNumeroDeTelefono());
	}
	
	/**
	 * Updates the origin account component with the selected account.
	 * @param currentAccount The selected account.
	 */
	public void updateOriginAccount(Account currentAccount) {
		if(originAccount.isSeleccionado()) {
			originAccount.setSeleccionado(false);
		}
		
		originAccount.getImgDerecha().setEnabled(true);
		originAccount.getImgIzquierda().setEnabled(true);
		originAccount.getVistaCtaOrigen().setEnabled(true);
		
		phoneNumberEdittext.setText(ownDelegate.getSpeiModel().getCelularAsociado());
		confirmPhoneNumberEdittext.setText(ownDelegate.getSpeiModel().getCelularAsociado());
		companiesList.setSelectedItem(ownDelegate.getSpeiModel().getCompany());
		
		phoneNumberEdittext.invalidate();
		confirmPhoneNumberEdittext.invalidate();
	}
}
