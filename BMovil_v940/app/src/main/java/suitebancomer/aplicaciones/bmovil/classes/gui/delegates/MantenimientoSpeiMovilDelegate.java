package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultaCuentaSpeiMovilViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.DetalleCuentaSpeiMovilViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.MantenimientoSpei;
import suitebancomer.aplicaciones.bmovil.classes.model.MantenimientoSpei.TiposOperacionSpei;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import suitebancomer.classes.gui.views.SeleccionHorizontalViewController;

//import suitebancomer.aplicaciones.bmovil.classes.model.RequestSpeiAccountsResult;
//import suitebancomer.aplicaciones.bmovil.classes.model.SpeiAccount;

public class MantenimientoSpeiMovilDelegate extends DelegateBaseAutenticacion {
	//#region Class fields.
	/**
	 * The unique identifier for the delegate.
	 */
	public final static long DELEGATE_ID = 0x4ed434c61ca109f2L;
	
	/**
	 * The view controller for this delegate.
	 */
	private BaseViewController ownerController;
	
	/**
	 * The operation to perform.
	 */
	private Constants.Operacion operation;
	
	/**
	 * The list of accounts returned by the server.
	 */
	private ArrayList<Account> speiAccounts;
	
	/**
	 * The SPEI switch indicator.
	 */
	private boolean switchSpeiActivated;
	
	/**
	 * The SpeiAccount to operate.
	 */
	private Account selectedSpeiAccount;
	
	/**
	 * The SPEI operation data model.
	 */
	private MantenimientoSpei speiModel;
	//#endregion
	
	//#region Getters and Setters.
	@SuppressWarnings("static-access")
	@Override
	public long getDelegateIdentifier() {
		return this.DELEGATE_ID;
	}
	
	/**
	 * @return The owner controller
	 */
	public BaseViewController getOwnerController() {
		return ownerController;
	}
	
	/**
	 * @param ownerController the owner controller to set.
	 */
	public void setOwnerController(BaseViewController ownerController) {
		this.ownerController = ownerController;
	}

	/**
	 * @return The operation to perform.
	 */
	public Constants.Operacion getOperation() {
		return operation;
	}

	/**
	 * @param operation The operation to perform.
	 */
	public void setOperation(Constants.Operacion operation) {
		this.operation = operation;
	}
	
	/**
	 * @return The SpeiAccount to operate.
	 */
	public Account getSelectedSpeiAccount() {
		return selectedSpeiAccount;
	}

	/**
	 * @return The current operation model.
	 */
	public MantenimientoSpei getSpeiModel() {
		return speiModel;
	}
	//#endregion

	//#region Network.
	//@Override
	
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson,ParsingHandler handler, Server.isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if( ownerController != null)
		   SuiteApp.getInstance().getBmovilApplication().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller, true);
		
	}
    

	@Override 
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			//if(operationId == Server.SPEI_ACCOUNTS_REQUEST){
				//analyzeRequestSpeiAccountsResponse(response);
			//} else 
			if(operationId == Server.SPEI_MAINTENANCE) {
				actualizaListasCuentas(operationId);					
				showResults();
			} else {
				super.analyzeResponse(operationId, response);
			}
		} else if(response.getStatus() == ServerResponse.OPERATION_WARNING) {
			if(ownerController != null)
				ownerController.showInformationAlert(response.getMessageText());
		}else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
			if(ownerController != null)
				ownerController.showInformationAlert(response.getMessageText());
		}
	}	
	//#endregion
	
	//#region Confirmation
	
public void actualizaListasCuentas(int operationId){
		
		if(operationId == Server.SPEI_MAINTENANCE) {
			// Actualiza listas de cuentas 	       	
			 //   Session session = Session.getInstance(SuiteApp.appContext);
			
				String account = speiModel.getCuentaAsignada();
				String tipoMantenimiento = speiModel.getTipoOperacion().getOperationCodeForServer();
				String newPhone = speiModel.getCelularAsociado();
				String codeCompany = speiModel.getCompany().getClave();
				String nameCompany = speiModel.getCompany().getNombre();
				
				ArrayList<Account> accountsListB = Session.getListB();
				ArrayList<Account> accountsListA = Session.getListA();
							
				if(tipoMantenimiento.equals(BmovilConstants.SPEI_OPERATION_TYPE_FOR_SERVER_ASSOCIATE) ){
						
					for(Account acc: accountsListB)	
					{		
						
						if(acc.getNumber().toString().equals(account)){
											
							Account obAccount = new Account();
							
							String number = acc.getNumber();
							double balance = acc.getBalance();					
							boolean visible = acc.isVisible();
							String currency = acc.getCurrency();
							String type = acc.getType();
							String concept = acc.getConcept();
							String alias = acc.getAlias();
							String fecha = Tools.formatDateFromServer();
							
							obAccount.setNumber(number);
							obAccount.setBalance(balance);					
							obAccount.setVisible(visible);
							obAccount.setCurrency(currency);
							obAccount.setType(type);
							obAccount.setConcept(concept);
							obAccount.setAlias(alias);
							obAccount.setCelularAsociado(newPhone);
							obAccount.setCodigoCompania(codeCompany);
							obAccount.setDescripcionCompania(nameCompany);
							obAccount.setIndicadorSPEI("S");
							obAccount.setFechaUltimaModificacion(fecha);
							
							Session.getListA().add(obAccount);
							Session.getListB().remove(acc);
							
							break;

						}
							
					}
				}else if(tipoMantenimiento.equals(BmovilConstants.SPEI_OPERATION_TYPE_FOR_SERVER_DIASSOCIATE)){
					
					for(Account acc: accountsListA)
					{

						if(acc.getNumber().toString().equals(account)){
		                    Account obAccount = new Account();
							
							String number = acc.getNumber();
							double balance = acc.getBalance();					
							boolean visible = acc.isVisible();
							String currency = acc.getCurrency();
							String type = acc.getType();
							String concept = acc.getConcept();
							String alias = acc.getAlias();
							String fecha = Tools.formatDateFromServer();
							
							obAccount.setNumber(number);
							obAccount.setBalance(balance);					
							obAccount.setVisible(visible);
							obAccount.setCurrency(currency);
							obAccount.setType(type);
							obAccount.setConcept(concept);
							obAccount.setAlias(alias);
							obAccount.setCelularAsociado("");
							obAccount.setCodigoCompania("");
							obAccount.setDescripcionCompania("");
							obAccount.setIndicadorSPEI("N");
							obAccount.setFechaUltimaModificacion(fecha);
							
							Session.getListB().add(obAccount);
													
							Session.getListA().remove(acc);
							break;
							
						}
					}
					
				}else{
					
					for(Account acc: accountsListA)
					{
						if(acc.getNumber().toString().equals(account)){
							
							String fecha = Tools.formatDateFromServer();
							
							acc.setCelularAsociado(newPhone);;
							acc.setCodigoCompania(codeCompany);	
							acc.setDescripcionCompania(nameCompany);
							acc.setIndicadorSPEI("S");;
							acc.setFechaUltimaModificacion(fecha);
							break;
						}
					}
					
					
				}
		}
	}
	
	//#endregion
	
	//#region Confirmation
	@Override
	public boolean mostrarContrasenia() {
		boolean value = Autenticacion.getInstance().mostrarContrasena(operation, Session.getInstance(SuiteApp.appContext).getClientProfile());
		return value;
	}

	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(operation, Session.getInstance(SuiteApp.appContext).getClientProfile());
		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error on Autenticacion.mostrarNIP execution.", ex);
			tipoOTP = null;
		}
		
		return tipoOTP;
	}

	@Override
	public boolean mostrarNIP() {
		boolean value = Autenticacion.getInstance().mostrarNIP(operation, Session.getInstance(SuiteApp.appContext).getClientProfile());

		return value;
	}
	
	@Override
	public boolean mostrarCVV() {
		boolean value = Autenticacion.getInstance().mostrarCVV(operation, Session.getInstance(SuiteApp.appContext).getClientProfile());
		return value;
	}
	
	@Override
	public boolean mostrarCampoTarjeta() {
		return mostrarNIP() || mostrarCVV(); 
	}
	
	@Override
	public String getEtiquetaCampoContrasenia() {
		return ownerController.getString(R.string.confirmation_aut_contrasena);
	}
	
	@Override
	public String getEtiquetaCampoTarjeta() {
		return ownerController.getString(R.string.confirmation_componente_digitos_tarjeta);
	}
	
	@Override
	public String getTextoAyudaTarjeta() {
		return ownerController.getString(R.string.confirmation_ayuda_componente_digitos_tarjeta);
	}
	
	@Override
	public String getEtiquetaCampoNip() {
		return ownerController.getString(R.string.confirmation_nip);
	}
	
	@Override
	public String getTextoAyudaNIP() {
		return ownerController.getString(R.string.confirmation_autenticacion_ayudaNip);
	}
	
	@Override
	public String getEtiquetaCampoCVV() {
		return ownerController.getString(R.string.confirmation_CVV);
	}
	
	@Override
	public String getTextoAyudaCVV() {
		return ownerController.getString(R.string.confirmation_CVV_ayuda);
	}

	/**
	 * Gets the OTP label for the specified instrument.
	 * @param tipoInstrumento The instrument type.
	 * @return The label associated to the instrument.
	 */
	public String getEtiquetaCampoOTP(TipoInstrumento tipoInstrumento) {
		String label = Constants.EMPTY_STRING;
		
		switch (tipoInstrumento) {
		case DP270:
			label = ownerController.getString(R.string.confirmation_dp270);
			break;
		case OCRA:
			label = ownerController.getString(R.string.confirmation_ocra);
			break;
		case SoftToken:
			label = ownerController.getString(SuiteApp.getSofttokenStatus() ? 
											  R.string.confirmation_softtokenActivado : 
											  R.string.confirmation_softtokenDesactivado);
			break;

		default:
			label = Constants.EMPTY_STRING;
			break;
		}
		
		return label;
	}
	
	@Override
	public String getTextoAyudaInstrumentoSeguridad(TipoInstrumento tipoInstrumento) {
		String helpText;
		TipoOtpAutenticacion tokenType = tokenAMostrar();
		
		if (tokenType == TipoOtpAutenticacion.ninguno) {
			helpText = Constants.EMPTY_STRING;
		} else if (tokenType == TipoOtpAutenticacion.registro) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (SuiteApp.getSofttokenStatus())
						helpText = ownerController.getString(R.string.confirmation_ayudaRegistroSofttokenActivado);
					else
						helpText = ownerController.getString(R.string.confirmation_ayudaRegistroSofttokenDesactivado);
				case OCRA:
					helpText = ownerController.getString(R.string.confirmation_ayudaRegistroOCRA);
				case DP270:
					helpText = ownerController.getString(R.string.confirmation_ayudaRegistroDP270);
				case sinInstrumento:
				default:
					helpText = Constants.EMPTY_STRING;
			}
		} else if (tokenType == TipoOtpAutenticacion.codigo) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (SuiteApp.getSofttokenStatus()) {
						helpText = ownerController.getString(R.string.confirmation_ayudaCodigoSofttokenActivado);
					} else {
						helpText = ownerController.getString(R.string.confirmation_ayudaCodigoSofttokenDesactivado);
					}					
				case OCRA:
					helpText = ownerController.getString(R.string.confirmation_ayudaCodigoOCRA);
				case DP270:
					return ownerController.getString(R.string.confirmation_ayudaCodigoDP270);
				case sinInstrumento:
				default:
					helpText = Constants.EMPTY_STRING;
			}
		} else {
			helpText = Constants.EMPTY_STRING;
		}
		
		return helpText;
	}
	
	/**
	 * Show the confirmation screen.
	 */
	public void showConfirmation() {
		boolean showHelpImage = speiModel.getTipoOperacion() == TiposOperacionSpei.Diassociate;
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showSpeiConfirmation(this, showHelpImage);
	}
	
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.bmovil_confirm_asociar_cuenta_telefono_operation));
		fila.add(speiModel.getTipoOperacion().getOperationCode());
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.bmovil_confirm_asociar_cuenta_telefono_account));
		//fila.add(Tools.hideAccountNumber(speiModel.getCuentaAsignada().getNumber()));
		fila.add(Tools.hideAccountNumber(speiModel.getCuentaAsignada()));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.bmovil_confirm_asociar_cuenta_telefono_phone));
		fila.add(speiModel.getCelularAsociado());
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.bmovil_confirm_asociar_cuenta_telefono_company));
		fila.add(speiModel.getCompany().getNombre());
		tabla.add(fila);

		fila = new ArrayList<String>();
		fila.add(ownerController.getString(R.string.bmovil_confirm_asociar_cuenta_telefono_fecha));
		java.util.Date currentDate = Session.getInstance(SuiteApp.appContext).getServerDate();
		fila.add(Tools.dateToString(currentDate));
		tabla.add(fila);
		
		return tabla;
	}

	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.icono_spei;
	}

	@Override
	public int getTextoEncabezado() {
		return R.string.bmovil_asociar_cuenta_telefono_alternative_title;
	}
	
	/**
	 * Starts the operation process.
	 * @param confirmationController The confirmation controller currently shown.
	 * @param pwd The user's application password.
	 * @param nip The user's NIP.
	 * @param otp The user's OTP.
	 * @param cvv The user's card CVV.
	 * @param card The user's card last digits.
	 */
	public void realizeOperation(BaseViewController confirmationController, String pwd, String nip, String otp, String cvv, String card) {
		ownerController = confirmationController;
		String va = Autenticacion.getInstance().getCadenaAutenticacion(operation, Session.getInstance(SuiteApp.appContext).getClientProfile());
		
		doSpeiMaintenance(card, nip, pwd, cvv, otp, va);
	}
	
	/**
	 * Sends the required information to the server to attempt to realize an SPEI account maintenance operation.
	 * @param card The user's card last digits.
	 * @param nip The user's NIP.
	 * @param pwd The user's application password.
	 * @param cvv The user's card CVV.
	 * @param otp The user's OTP.
	 * @param va The authentication string.
	 */
	public void doSpeiMaintenance(String card, String nip, String pwd, String cvv, String otp, String va) {
		Session session = Session.getInstance(SuiteApp.appContext);
		boolean isDiassociate = speiModel.getTipoOperacion() == TiposOperacionSpei.Diassociate;
		String newPhone = isDiassociate ? Constants.EMPTY_STRING : speiModel.getCelularAsociado();
		//////Mostrar compania
		
		String phoneCompany = isDiassociate ? Constants.EMPTY_STRING : speiModel.getCompany().getClave();
		//Inicia Código Karen
				//String account = speiModel.getCuentaAsignada().getNumber();
		
		String account = speiModel.getCuentaAsignada();
		  
				if (account.length()>19) {
		            account = account.substring(0,8) + account.substring(10); 
				}
		//Termina Código Karen
				
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put("numeroTelefono",				session.getUsername());
		//Código Karen comentado
		//params.put(Server.ACCOUNT_PARAM, 				speiModel.getCuentaAsignada().getNumber());
		params.put("asunto", 				account);
		//params.put(Server.OLD_PHONENUMBER_PARAM,		selectedSpeiAccount.getTelefonoAsociado());
		params.put("numeroTelefonoA",		selectedSpeiAccount.getCelularAsociado());
		params.put("numeroCelNuevo", 			newPhone);
		params.put("companiaCelular", 			phoneCompany);
		params.put("tipoMantenimiento", 	speiModel.getTipoOperacion().getOperationCodeForServer());
		params.put(ServerConstants.JSON_IUM_ETIQUETA,				session.getIum());  /*Server.IUM_PARAM*/	
		params.put("numeroCliente",				session.getClientNumber());/*Server.CLIENTNUMBER_PARAM*/
		params.put("codigoNIP", 					nip);
		params.put("codigoCVV2", 					cvv);
		params.put("codigoOTP", 					otp); /*Server.OTP_PARAM,*/
		params.put("cadenaAutenticacion", 					va);
		params.put("cveAcceso", 				pwd);
		params.put("tarjeta5Dig", 		card);
		params.put("companiaCelularVieja",
				   (speiModel.getTipoOperacion() == TiposOperacionSpei.Associate) ? "" : selectedSpeiAccount.getDescripcionCompania());
		//params.put(Server.OLD_COMPANY_NAME_PARAM, 
				   //(speiModel.getTipoOperacion() == TiposOperacionSpei.Associate) ? selectedSpeiAccount.getNombreCompania() : "");
		////////////////
		//System.out.print("************compañia: "+speiModel.getCompany().getNombre());
		
		params.put("companiaCelularNueva",
				   (speiModel.getTipoOperacion() == TiposOperacionSpei.Diassociate) ? "" : speiModel.getCompany().getNombre());
                                                                                                                                                                                                                        
		//JAIG
		doNetworkOperation(Server.SPEI_MAINTENANCE, params,false,null, Server.isJsonValueCode.NONE, ownerController);
		
	}
	//#endregion
	
	//#region Results.
	private void showResults() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController()
			.showResultadosAutenticacionViewController(this, getNombreImagenEncabezado(), getTextoEncabezado());
	}
	
	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		return getDatosTablaConfirmacion();
	}
	
	@Override
	public String getTextoTituloResultado() {
		if(speiModel.getTipoOperacion() == TiposOperacionSpei.Diassociate)
			return ownerController.getString(R.string.bmovil_asociar_cuenta_telefono_resultados_baja);
		else
			return ownerController.getString(R.string.transferir_detalle_operacion_exitosaTitle);
	}
	
	@Override
	public int getColorTituloResultado() {
		return (speiModel.getTipoOperacion() == TiposOperacionSpei.Diassociate) ? R.color.magenta : R.color.verde_limon;
	}
	
	@Override
	public int getOpcionesMenuResultados() {
		return 0;
	}

	//#endregion

	//#region Check accounts.
	@SuppressWarnings("deprecation")
	/**
	 * Loads the ListaSeleccion element with the needed data.
	 */
	public void loadAccountsList() {
		ListaSeleccionViewController accountsList;
		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT,	LayoutParams.WRAP_CONTENT);

		accountsList = new ListaSeleccionViewController(ownerController, params, ownerController.getParentViewsController());
		accountsList.setDelegate(this);

		ArrayList<Object> listaEncabezado = getAccountsListHeaderData();
		ArrayList<Object> listaDatos = getAccountsListData();
		
		accountsList.setEncabezado(listaEncabezado);
		accountsList.setLista(listaDatos);
		accountsList.setNumeroColumnas(listaEncabezado.size() - 1) ;
		
		if (listaDatos.size() == 0) {
			accountsList.setTextoAMostrar(ownerController.getString(R.string.bmovil_consultar_frecuentes_emptylist));
		} else {
			accountsList.setTextoAMostrar(null);		
		}
		
		accountsList.setOpcionSeleccionada(-1);
		accountsList.setSeleccionable(true);
		accountsList.setAlturaFija(true);
		accountsList.setNumeroFilas(listaDatos.size());
		accountsList.setExisteFiltro(false);
		accountsList.setTitle(ownerController.getString(R.string.bmovil_asociar_cuenta_telefono_table_title));
		accountsList.setSingleLine(true);
		accountsList.setInformActionEvenIfSelectable(true);
		accountsList.cargarTabla();
		
		((ConsultaCuentaSpeiMovilViewController)ownerController).setAccountsList(accountsList);
	}

	/**
	 * Gets the headers to show in the accounts list table.
	 * @return The headers.
	 */
	private ArrayList<Object> getAccountsListHeaderData() {
		ArrayList<Object> registros = new ArrayList<Object>();
		registros.add("");
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_asociar_cuenta_telefono_table_header_account));
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_asociar_cuenta_telefono_table_header_phone));
		return registros;
	}

	/**
	 * Gets the accounts list data to show.
	 * @return The accounts data.
	 */
	private ArrayList<Object> getAccountsListData() {
		ArrayList<Object> listaDatos = new ArrayList<Object>();
		ArrayList<Object> registro;
		Object[] data;
		
		boolean isAssociated ;
		for(/*SpeiAccount*/ Account	acc : speiAccounts) {
			registro = new ArrayList<Object>();
			
			
			//isAssociated = acc.getIndicadorSpeiActivado().equals(Constants.SPEI_PHONE_ASSOCIATED_CODE);
			isAssociated = acc.getIndicadorSPEI().equals(BmovilConstants.SPEI_PHONE_ASSOCIATED_CODE);
			data = new Object[3];
			data[0] = acc;
			data[1] = isAssociated;
			//data[2] = isAssociated ? acc.getTelefonoAsociado() : null; 
			data[2] = isAssociated ? acc.getCelularAsociado() : null; 
			registro.add(data);
			//registro.add(Tools.hideAccountNumber(acc.getNumeroCuenta().getNumber()));
			registro.add(Tools.hideAccountNumber(acc.getNumber()));
			//registro.add(isAssociated ? 
			//			 acc.getTelefonoAsociado() : 
			//			 ownerController.getString(R.string.bmovil_asociar_cuenta_telefono_table_header_empty_phone));
			
			registro.add(isAssociated ? 
						 acc.getCelularAsociado() : 
						 ownerController.getString(R.string.bmovil_asociar_cuenta_telefono_table_header_empty_phone));
			
			listaDatos.add(registro);
		}
		
		return listaDatos;
	}
	
	/**
	 * Shows the account details screen.
	 */
	public void showAccountDetailsScreen() {
		((BmovilViewsController)ownerController.getParentViewsController()).showDetailAsociacionCuentaTelefono();
	}
	
	/**
	 * Behavior when the user selects to edit or delete an SPEI association.
	 * @param selectedData The data from the seledted item.
	 * @param isEditing True if the user clicked edit button, false if clicked delete button.
	 */
	public void elementSelected(Object[] selectedData, boolean isEditing) {
		// If the user did not select an element the data will be null.
		if(null == selectedData) {
			ownerController.showInformationAlert(isEditing ? 
												 R.string.bmovil_asociar_cuenta_telefono_error_invalid_operation_unselected_for_edit :
												 R.string.bmovil_asociar_cuenta_telefono_error_invalid_operation_unselected_for_delete);
			return;
		}
		
		boolean hasPhoneAssociated = (Boolean)selectedData[1];
		//selectedSpeiAccount = (SpeiAccount)selectedData[0];
		selectedSpeiAccount = (Account)selectedData[0];
				
		addressFlow(isEditing, hasPhoneAssociated);
	}
	
	/**
	 * Request to the server the list of accounts available for the SPEI operations.
	 */
	
	public void requestSpeiAccounts() {
		/*
		Session session = Session.getInstance(SuiteApp.appContext);
		
		Hashtable<String, String> params = new Hashtable<String, String>();
		
		params.put(Server.USERNAME_PARAM, session.getUsername());
		params.put(ServerConstants.JSON_IUM_ETIQUETA,session.getIum());/*Server.IUM_PARAM,*/ 
	/*  params.put(ServerConstants.NUMERO_CLIENTE,session.getClientNumber());/*Server.CLIENTNUMBER_PARAM,*/
    
	/*	doNetworkOperation(Server.SPEI_ACCOUNTS_REQUEST, params, ownerController);
		*/		
		analyzeRequestSpeiAccountsResponse();
	}
	
	/**
	 * Analyze the server response for the SPEI accounts request.
	 * @param response The server response.
	 */
	private void analyzeRequestSpeiAccountsResponse(/*ServerResponse response*/) {
		/*
		if(null == response)
			return;
		*/
		
		//RequestSpeiAccountsResult result = (RequestSpeiAccountsResult)response.getResponse();
		//switchSpeiActivated = result.isSpeiSwitch();
		//speiAccounts = result.getSpeiAccounts();
		
		/*switchSpeiActivated = Session.getSwitchSPEI();
		
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		speiAccounts = new ArrayList<Account>();
		
		
		for(Account acc : accounts)
		{
			speiAccounts.add(acc);
			
			if(Server.ALLOW_LOG) Log.e("NC",String.valueOf(acc.getNumber()));
			if(Server.ALLOW_LOG) Log.e("Balance",String.valueOf(acc.getBalance()));
			if(Server.ALLOW_LOG) Log.e("Visible",String.valueOf(acc.isVisible()));
			if(Server.ALLOW_LOG) Log.e("Currency",String.valueOf(acc.getCurrency()));
			if(Server.ALLOW_LOG) Log.e("Type",String.valueOf(acc.getType()));
			if(Server.ALLOW_LOG) Log.e("Concept",String.valueOf(acc.getConcept()));
			if(Server.ALLOW_LOG) Log.e("Alias",String.valueOf(acc.getAlias()));
			if(Server.ALLOW_LOG) Log.e("CelularAsociado",String.valueOf(acc.getCelularAsociado()));
			if(Server.ALLOW_LOG) Log.e("CodigoCompania",String.valueOf(acc.getCodigoCompania()));
			if(Server.ALLOW_LOG) Log.e("DescripcionCompania",String.valueOf(acc.getDescripcionCompania()));
			if(Server.ALLOW_LOG) Log.e("IndicadorSPEI",String.valueOf(acc.getIndicadorSPEI()));
			if(Server.ALLOW_LOG) Log.e("FechaUltimaModificacion",String.valueOf(acc.getFechaUltimaModificacion()));
			
		}
		
		
		loadAccountsList();*/
		/*
		if(null == response)
			return;
		*/
		
		//RequestSpeiAccountsResult result = (RequestSpeiAccountsResult)response.getResponse();
		//switchSpeiActivated = result.isSpeiSwitch();
		//speiAccounts = result.getSpeiAccounts();
		
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		speiAccounts = new ArrayList<Account>();
		switchSpeiActivated = Session.getSwitchSPEI();
		
		for(Account acc : accounts)
		{
			if(acc.getType().equals("AH")||acc.getType().equals("CH")||acc.getType().equals("LI")){
				
				speiAccounts.add(acc);
				
			}
			else{
			
				if(Server.ALLOW_LOG) Log.e("CUENTAS INVALIDAS ",String.valueOf(acc.getNumber()));
			
		   }
		}
		
		loadAccountsList();
	}
	
	/**
	 * Address the application flow to the corresponding case.
	 * @param isEditOperation Flag to indicate if the user is editing or deleting the phone number. 
	 * @param hasPhoneAssociated Flag to indicate if the account already has an associated phone.
	 */
	private void addressFlow(boolean isEditOperation, boolean hasPhoneAssociated) {
		speiModel = new MantenimientoSpei();
		
		if(switchSpeiActivated) {
			if(isEditOperation) {
				if(hasPhoneAssociated)
					processSwitchOnPhoneAssignedEdit();
				else
					processSwitchOnPhoneUnassignedEdit();
			} else {
				if(hasPhoneAssociated)
					processSwitchOnPhoneAssignedRemove();
				else
					processSwitchOnPhoneUnassignedRemove();
			}
		} else {
			if(isEditOperation) {
				if(hasPhoneAssociated)
					processSwitchOffPhoneAssignedEdit();
				else
					processSwitchOffPhoneUnassignedEdit();
			} else {
				if(hasPhoneAssociated)
					processSwitchOffPhoneAssignedRemove();
				else
					processSwitchOffPhoneUnassignedRemove();
			}
		}
	}
	
	/**
	 * Process the data and continue with the application flow, this method covers the cases when the SPEI switch is off, 
	 * the account has a phone assigned and the user clicked the edit button.
	 * <br/>
	 * EA#2
	 */
	private void processSwitchOffPhoneAssignedEdit() {
		//speiModel.setCuentaAsignada(selectedSpeiAccount.getNumeroCuenta());
		speiModel.setCuentaAsignada(selectedSpeiAccount.getNumber());
		speiModel.setTipoOperacion(MantenimientoSpei.TiposOperacionSpei.Modify);
		showAccountDetailsScreen();
	}

	/**
	 * Process the data and continue with the application flow, this method covers the cases when the SPEI switch is off, 
	 * the account has a phone assigned and the user clicked the remove button.
	 * <br/>
	 * EA#3
	 */
	private void processSwitchOffPhoneAssignedRemove() {
		//speiModel.setCuentaAsignada(selectedSpeiAccount.getNumeroCuenta());
		//speiModel.setCelularAsociado(selectedSpeiAccount.getTelefonoAsociado());
		speiModel.setCuentaAsignada(selectedSpeiAccount.getNumber());
		speiModel.setCelularAsociado(selectedSpeiAccount.getCelularAsociado());
		speiModel.setTipoOperacion(MantenimientoSpei.TiposOperacionSpei.Diassociate);
		//speiModel.setCompany(new Compania(selectedSpeiAccount.getNombreCompania(), 
		speiModel.setCompany(new Compania(selectedSpeiAccount.getDescripcionCompania(),
										  Constants.EMPTY_STRING, 
										  selectedSpeiAccount.getCodigoCompania(), 
										  Constants.EMPTY_STRING, 
										  Constants.EMPTY_STRING));
		
		showConfirmation();
	}

	/**
	 * Process the data and continue with the application flow, this method covers the cases when the SPEI switch is off, 
	 * the account has not a phone assigned and the user clicked the edit button.
	 * <br/>
	 * Main scenario
	 */
	private void processSwitchOffPhoneUnassignedEdit() {
		//speiModel.setCuentaAsignada(selectedSpeiAccount.getNumeroCuenta());
		speiModel.setCuentaAsignada(selectedSpeiAccount.getNumber());
		speiModel.setTipoOperacion(MantenimientoSpei.TiposOperacionSpei.Associate);
		showAccountDetailsScreen();
	}

	/**
	 * Process the data and continue with the application flow, this method covers the cases when the SPEI switch is off, 
	 * the account has not a phone assigned and the user clicked the remove button.
	 * <br/>
	 * EA#1 
	 */
	private void processSwitchOffPhoneUnassignedRemove() {
		ownerController.showInformationAlert(R.string.bmovil_asociar_cuenta_telefono_error_invalid_operation_unassigned_phone);
		((ConsultaCuentaSpeiMovilViewController)ownerController).clearAccountsListSelection();
	}

	/**
	 * Process the data and continue with the application flow, this method covers the cases when the SPEI switch is on, 
	 * the account has a phone assigned and the user clicked the edit button.
	 * <br/>
	 * EA#4
	 * <br/>
	 * EA#6
	 */
	private void processSwitchOnPhoneAssignedEdit() {
		//if(Session.getInstance(SuiteApp.appContext).getUsername().equals(selectedSpeiAccount.getTelefonoAsociado())) {
		if(Session.getInstance(SuiteApp.appContext).getUsername().equals(selectedSpeiAccount.getCelularAsociado())) {
			ownerController.showInformationAlert(R.string.bmovil_asociar_cuenta_telefono_error_invalid_operation_edit_associated_bmovil_phone);
		} else {
			ownerController.showInformationAlert(R.string.bmovil_asociar_cuenta_telefono_error_invalid_operation_edit_unassociated_bmovil_phone);
		}
		((ConsultaCuentaSpeiMovilViewController)ownerController).clearAccountsListSelection();
	}

	/**
	 * Process the data and continue with the application flow, this method covers the cases when the SPEI switch is on, 
	 * the account has a phone assigned and the user clicked the remove button.
	 * <br/>
	 * EA#5
	 * <br/>
	 * EA#9
	 */
	private void processSwitchOnPhoneAssignedRemove() {
		//if(Session.getInstance(SuiteApp.appContext).getUsername().equals(selectedSpeiAccount.getTelefonoAsociado())) {
		  if(Session.getInstance(SuiteApp.appContext).getUsername().equals(selectedSpeiAccount.getCelularAsociado())) {
			//speiModel.setCuentaAsignada(selectedSpeiAccount.getNumeroCuenta());
			//speiModel.setCelularAsociado(selectedSpeiAccount.getTelefonoAsociado());
			speiModel.setCuentaAsignada(selectedSpeiAccount.getNumber());
			speiModel.setCelularAsociado(selectedSpeiAccount.getCelularAsociado());
			speiModel.setTipoOperacion(MantenimientoSpei.TiposOperacionSpei.Diassociate);
			//speiModel.setCompany(new Compania(selectedSpeiAccount.getNombreCompania(),
			speiModel.setCompany(new Compania(selectedSpeiAccount.getDescripcionCompania(),
											  Constants.EMPTY_STRING, 
											  selectedSpeiAccount.getCodigoCompania(), 
											  Constants.EMPTY_STRING, 
											  Constants.EMPTY_STRING));
			showConfirmation();
		} else {
			ownerController.showInformationAlert(R.string.bmovil_asociar_cuenta_telefono_error_invalid_operation_edit_unassociated_bmovil_phone);
			((ConsultaCuentaSpeiMovilViewController)ownerController).clearAccountsListSelection();
		}
	}

	/**
	 * Process the data and continue with the application flow, this method covers the cases when the SPEI switch is on, 
	 * the account has not a phone assigned and the user clicked the edit button.
	 * <br/>
	 * EA#7
	 * <br/>
	 * EA#9
	 */
	private void processSwitchOnPhoneUnassignedEdit() {
		if(!isBmovilNumberAssociated()) {
			speiModel.setCelularAsociado(Session.getInstance(SuiteApp.appContext).getUsername());
			
			//speiModel.setCuentaAsignada(selectedSpeiAccount.getNumeroCuenta());
			speiModel.setCuentaAsignada(selectedSpeiAccount.getNumber());
			speiModel.setCelularAsociado(Session.getInstance(SuiteApp.appContext).getUsername());
			speiModel.setTipoOperacion(MantenimientoSpei.TiposOperacionSpei.Associate);
			
			String companyName = Session.getInstance(SuiteApp.appContext).getCompaniaUsuario();
			String empty = Constants.EMPTY_STRING;
			Compania company = new Compania(empty, empty, empty, empty, empty);
			Compania selectedCompany = new Compania(empty, empty, empty, empty, empty);
			
			for(Object obj : Session.getInstance(SuiteApp.appContext).getCatalogoMantenimientoSPEI().getObjetos()) {
				company = (Compania)obj;
				if(company.getNombre().equalsIgnoreCase(companyName)) {
					selectedCompany = company;
					//////////
					//	System.out.println("******************compania"+company);
					break;
				}
			}
			
			speiModel.setCompany(selectedCompany);
			
			showConfirmation();
		} else {
			ownerController.showInformationAlert(R.string.bmovil_asociar_cuenta_telefono_error_invalid_operation_edit_deactivated_spei);
			((ConsultaCuentaSpeiMovilViewController)ownerController).clearAccountsListSelection();
		}
	}

	/**
	 * Process the data and continue with the application flow, this method covers the cases when the SPEI switch is on, 
	 * the account has not a phone assigned and the user clicked the remove button.
	 * <br/>
	 * EA#8
	 * <br/>
	 * EA#9
	 */
	private void processSwitchOnPhoneUnassignedRemove() {
		if(!isBmovilNumberAssociated()) {
			processSwitchOffPhoneUnassignedRemove();
		} else {
			ownerController.showInformationAlert(R.string.bmovil_asociar_cuenta_telefono_error_invalid_operation_edit_deactivated_spei);
			((ConsultaCuentaSpeiMovilViewController)ownerController).clearAccountsListSelection();
		}
	}

	/**
	 * Analyze if the phone number associated to the Bmovil service is set on one of the SPEI accounts.
	 * @return True if the phone number associated with Bmovil is set in at least one account, false otherwise.
	 */
	private boolean isBmovilNumberAssociated() {
		boolean result = false;
		String bmovilNumber = Session.getInstance(SuiteApp.appContext).getUsername();
		
		for(/*SpeiAccount*/ Account speiAcc : speiAccounts) {
			if(speiAcc.getCelularAsociado().equals(bmovilNumber)) {
				result = true;
				break;
			}
		}
		
		return result;
		
	}
	//#endregion
	
	//#region Accounts details.
	/**
	 * Validate the selected values.
	 * @param phone The selected phone.
	 * @param phoneConfirmation The phone confirmation
	 * @param associate Flag to indicate if is associate or disassociate.
	 * @param company The Phone company.
	 */
	public void validaDatos(String phone, String phoneConfirmation, Object company) {
		if(Constants.TELEPHONE_NUMBER_LENGTH != phone.length()) {
			ownerController.showInformationAlert(R.string.bmovil_detalle_asociar_cuenta_telefono_error_telefono_corto);
		} 
//		else if (Constants.TELEPHONE_NUMBER_LENGTH != phoneConfirmation.length()) {
//			ownerController.showInformationAlert(R.string.bmovil_detalle_asociar_cuenta_telefono_error_confirmacion_corto);
//		} 
		else if (!phone.equalsIgnoreCase(phoneConfirmation)) {
			ownerController.showInformationAlert(R.string.bmovil_detalle_asociar_cuenta_telefono_error_telefono_diferente);
		} else {
			Compania comp = (Compania)company;
			speiModel.setCompany(comp);
			speiModel.setCelularAsociado(phone);
			
			showConfirmation();
		}
	}
	
	/**
	 * Loads the SeleccionHorizontal component to show in the account details screen.
	 * @return The companies list component.
	 */
	public SeleccionHorizontalViewController loadCompaniesList() {
		SeleccionHorizontalViewController companiesList;
		@SuppressWarnings("deprecation")
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		ArrayList<Object> companies = loadCompaniesForSeleccionHorizontal();
		companiesList = new SeleccionHorizontalViewController(ownerController, params, companies, this, false);
		
		if(speiModel.getTipoOperacion() == TiposOperacionSpei.Modify) {
			Object company = null;
			for(Object comp : companies) {
				if(((Compania)comp).getClave().equals(selectedSpeiAccount.getCodigoCompania()))
					company = comp;
			}
			
			if(null != company)
				companiesList.setSelectedItem(company);
		}
		
		return companiesList;
	}
	
	/**
	 * Loads the CuentaOrigen component to show in the account details screen.
	 * @return The origin account component.
	 */
	public CuentaOrigenViewController loadOriginAccountComponent() {
		int selectedIndex = 0;
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		
		for(int i = 0; i < speiAccounts.size(); i++) {
			//accountsArray.add(speiAccounts.get(i).getNumeroCuenta());
			accountsArray.add(speiAccounts.get(i));
			//if(selectedSpeiAccount.getNumeroCuenta().getNumber().equals(speiAccounts.get(i).getNumeroCuenta().getNumber()))
			//if(selectedSpeiAccount.getNumeroCuenta().getNumber().equals(speiAccounts.get(i).getNumber()))
			if(selectedSpeiAccount.getNumber().equals(speiAccounts.get(i).getNumber()))
			  selectedIndex = i;
		}
		
//		accountsArray.add(selectedAccount);
		@SuppressWarnings("deprecation")
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		CuentaOrigenViewController cuentaOrigen = new CuentaOrigenViewController(ownerController, 
																				 params, 
																				 ownerController.getParentViewsController(), 
																				 ownerController);
		cuentaOrigen.setDelegate(this);
		cuentaOrigen.setListaCuetasAMostrar(accountsArray);
		cuentaOrigen.getTituloComponenteCtaOrigen().setText(ownerController.getString(R.string.bmovil_detalle_asociar_cuenta_telefono_label_account));
		cuentaOrigen.setIndiceCuentaSeleccionada(selectedIndex);
		
		cuentaOrigen.init();
		
		
		return cuentaOrigen;
	}
	
	/**
	 * Loads the companies to be shown in the SeleccionHorizontal component.
	 * @return The companies list.
	 */
	private ArrayList<Object> loadCompaniesForSeleccionHorizontal() {
		Vector<Object> companiesVector = Session.getInstance(SuiteApp.appContext).getCatalogoMantenimientoSPEI().getObjetos();
		ArrayList<Object> companiesList = new ArrayList<Object>(companiesVector);
		
		return companiesList;
	}
	//#endregion
	
	public MantenimientoSpeiMovilDelegate() {
		this.operation = Constants.Operacion.MantenimientoSpeimovil;
		speiModel = new MantenimientoSpei();
	}

	@Override
	public void performAction(Object obj) {
		if(null == obj)
			return;
		
		if(obj instanceof Object[]) {
			ConsultaCuentaSpeiMovilViewController controller = (ConsultaCuentaSpeiMovilViewController)ownerController;
			Object[] data = (Object[])obj;
			//SpeiAccount accountData = (SpeiAccount)data[0];
			Account accountData = (Account)data[0];
			boolean hasPhoneAssociated = (Boolean)data[1];
			
			if(switchSpeiActivated) {
				controller.setEditButtonEnabled(!hasPhoneAssociated && !isBmovilNumberAssociated());
				controller.setDeleteButtonEnabled(hasPhoneAssociated && 
				//								  Session.getInstance(SuiteApp.appContext).getUsername().equals(accountData.getTelefonoAsociado()));
						                          Session.getInstance(SuiteApp.appContext).getUsername().equals(accountData.getCelularAsociado()));
			} else {
				controller.setEditButtonEnabled(true);
				controller.setDeleteButtonEnabled(hasPhoneAssociated);
			}
		} else if(obj instanceof Account) {
			Account acc = (Account)obj;
			
			String acc2 = acc.getNumber();
			if(Server.ALLOW_LOG) Log.w(this.getClass().getSimpleName(), "Cuenta seleccionada: " + acc.getNumber());
			
			//speiModel.setCuentaAsignada(acc);
			
			speiModel.setCuentaAsignada(acc2);
			
			//for(SpeiAccount speiAccount : speiAccounts) {
			for(Account speiAccount : speiAccounts) {	
				//if(speiModel.getCuentaAsignada().getNumber().equals(speiAccount.getNumber())) {
				if(speiModel.getCuentaAsignada().equals(speiAccount.getNumber())) {
					selectedSpeiAccount = speiAccount;
					break;
				}
			}
			
			//if(selectedSpeiAccount.getIndicadorSpeiActivado().equals(Constants.SPEI_PHONE_ASSOCIATED_CODE)) {
			if(selectedSpeiAccount.getIndicadorSPEI().equals(BmovilConstants.SPEI_PHONE_ASSOCIATED_CODE)) {
				speiModel.setTipoOperacion(TiposOperacionSpei.Modify);
				speiModel.setCelularAsociado(selectedSpeiAccount.getCelularAsociado());
				
				Compania company = null;
				for(Object comp : loadCompaniesForSeleccionHorizontal()) {
					if(((Compania)comp).getClave().equals(selectedSpeiAccount.getCodigoCompania()))
						company = (Compania)comp;
				}
				speiModel.setCompany(company);
			} else {
				speiModel.setTipoOperacion(TiposOperacionSpei.Associate);
				speiModel.setCelularAsociado(Constants.EMPTY_STRING);
				speiModel.setCompany((Compania)loadCompaniesForSeleccionHorizontal().get(0));
			}
			
			((DetalleCuentaSpeiMovilViewController)ownerController).updateOriginAccount(acc);
			
		} else {
			if(Server.ALLOW_LOG) Log.v(this.getClass().getSimpleName(), obj.toString());
		}
	}
	
}