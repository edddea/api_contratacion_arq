package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import android.annotation.SuppressLint;
import android.util.Log;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaILC;
import suitebancomer.classes.gui.controllers.BaseViewController;

@SuppressLint("NewApi")
public class ExitoEFIDelegate extends DelegateBaseOperacion {
	private BaseViewController viewController;
	AceptaOfertaEFI aceptaOfertaEFI;
	OfertaEFI ofertaEFI;
	String fechaEnviada;
	public static final long EXITO_OFERTA_EFI_DELEGATE = 631453334566765888L;


	public ExitoEFIDelegate(AceptaOfertaEFI aceptaOfertaEFI, OfertaEFI ofertaEFI){
		this.aceptaOfertaEFI= aceptaOfertaEFI;
		this.ofertaEFI= ofertaEFI;

	}
		
	public AceptaOfertaEFI getAceptaOfertaEFI() {
		return aceptaOfertaEFI;
	}



	public void setAceptaOfertaEFI(AceptaOfertaEFI aceptaOfertaEFI) {
		this.aceptaOfertaEFI = aceptaOfertaEFI;
	}



	public OfertaEFI getOfertaEFI() {
		return ofertaEFI;
	}



	public void setOfertaEFI(OfertaEFI ofertaEFI) {
		this.ofertaEFI = ofertaEFI;
	}



	@Override
	public int getOpcionesMenuResultados() {
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL ;
	}
	
	public void realizaOperacion(int idOperacion,
			BaseViewController baseViewController, boolean isEmail, boolean isSMS){
		if(idOperacion == Server.EXITO_OFERTA_EFI){
			Session session = Session.getInstance(SuiteApp.appContext);
			String user = session.getUsername();
			String ium= session.getIum();
			Hashtable<String, String> params = new Hashtable<String, String>();
			//params.put("operacion", "exitoEFI");
			params.put("numeroCelular",user);
			//params.put("cuentaDeposito",ofertaEFI.getAccountDestino().getNumber());
			params.put("numeroTarjeta",aceptaOfertaEFI.getNumeroTarjeta());
			
			//params.put("importe", aceptaOfertaEFI.getImpDispuesto()+".00");
			String importe2 = aceptaOfertaEFI.getImpDispuesto().replace(",", "")+"00";
			importe2 = Tools.formatAmount(importe2, false);
			params.put("importe", importe2.replace("$", "").replace(".00", ""));
			
			
			
			params.put("plazo",ofertaEFI.getPlazoMeses()+" Meses");
			
			//params.put("pagoFijo",aceptaOfertaEFI.getPagoMensual());
			String pagoFijo2 = aceptaOfertaEFI.getPagoMensual().replace(",", "");
			pagoFijo2 = Tools.formatAmount(pagoFijo2, false);
			params.put("pagoFijo", pagoFijo2.replace("$", ""));
						
			//params.put("comisionDisp",aceptaOfertaEFI.getComisionEFIMonto());
			String comisionDisp2 = aceptaOfertaEFI.getComisionEFIMonto().replace(",", "");
			comisionDisp2 = Tools.formatAmount(comisionDisp2, false);
			params.put("comisionDisp", comisionDisp2.replace("$", ""));
					
			params.put("email",aceptaOfertaEFI.getEmail());
			params.put("fechaOperacion",fechaEnviada);
			params.put("horaOperacion",aceptaOfertaEFI.getHoraOperacion());
			params.put("folioInternet",aceptaOfertaEFI.getFolioAST());
			params.put("Cat",aceptaOfertaEFI.getCat().replace(",", "."));
			params.put("fechaCat",aceptaOfertaEFI.getFechaCat());
			String mensajeA="";
			if(isEmail && isSMS){
				params.put("tasaAnual",aceptaOfertaEFI.getTasaMensual());
			
				params.put("cuentaDeposito",ofertaEFI.getAccountDestino().getNumber());
				
				mensajeA="011";
			}
			else if(isEmail){
				params.put("tasaAnual",ofertaEFI.getTasaAnual());
				
				int start=ofertaEFI.getAccountDestino().getNumber().length()-20;
				//String numeroCuentaDestino=ofertaEFI.getAccountDestino().getNumber().substring(start);
				params.put("cuentaDeposito",ofertaEFI.getAccountDestino().getNumber().substring(start));
				
				mensajeA="010";
			}
			else if(isSMS){
				/*double tasaMensual = Double.parseDouble(ofertaEFI.getTasaAnual()) / 12;
				DecimalFormat df = new DecimalFormat("#.####"); 
				df.setRoundingMode(RoundingMode.DOWN);
				params.put("tasaAnual",df.format(tasaMensual).replace(",", "."));
				mensajeA="001";*/
				params.put("tasaAnual",aceptaOfertaEFI.getTasaMensual());
				
				int start=ofertaEFI.getAccountDestino().getNumber().length()-20;
				//String numeroCuentaDestino=ofertaEFI.getAccountDestino().getNumber().substring(start);
				params.put("cuentaDeposito",ofertaEFI.getAccountDestino().getNumber().substring(start));
				mensajeA="001";
				
			}
			params.put("mensajeA",mensajeA);
			params.put("IUM",ium);
			//JAIG SI isjsonvalueCode=isJsonValueCode.ONECLICK;
			doNetworkOperation(Server.EXITO_OFERTA_EFI, params,true,null,isJsonValueCode.ONECLICK,
					baseViewController);
		}else if(idOperacion == Server.CONSULTA_DETALLE_OFERTA_EFI){
			Session session = Session.getInstance(SuiteApp.appContext);
			String user = session.getUsername();
			String ium= session.getIum();		
			Hashtable<String, String> params = new Hashtable<String, String>();
			//params.put("operacion", "cDetalleOfertaBMovil");
			params.put("numeroCelular",user );
			params.put("cveCamp",aceptaOfertaEFI.getPromocion().getCveCamp());
			params.put("IUM", ium);
			//JAIG NO  isjsonvalueCode=isJsonValueCode.ONECLICK;
			doNetworkOperation(Server.CONSULTA_DETALLE_OFERTA_EFI, params,true, new OfertaEFI(),isJsonValueCode.ONECLICK,
					baseViewController);
		}else if(idOperacion == Server.CONSULTA_DETALLE_OFERTA){
			Session session = Session.getInstance(SuiteApp.appContext);
			String user = session.getUsername();
			String ium= session.getIum();		
			Hashtable<String, String> params = new Hashtable<String, String>();
			//params.put("operacion", "cDetalleOfertaBMovil");
			params.put("numeroCelular",user );
			params.put("cveCamp",aceptaOfertaEFI.getPromocion().getCveCamp());
			params.put("IUM", ium);
			//JAIG isjsonvalueCode=isJsonValueCode.ONECLICK;
			doNetworkOperation(Server.CONSULTA_DETALLE_OFERTA, params,true, new OfertaILC(),isJsonValueCode.ONECLICK,
					baseViewController);
		}
		
	}
	

	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params, boolean isJson, ParsingHandler handler,isJsonValueCode isJsonValueCode,BaseViewController caller) {
		if( viewController != null)
			((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,false);
	}
		
		public void analyzeResponse(int operationId, ServerResponse response) {
			if(operationId==Server.CONSULTA_DETALLE_OFERTA){
				OfertaILC ofertaILC= (OfertaILC)response.getResponse();
				((BmovilViewsController)viewController.getParentViewsController()).showDetalleILC(ofertaILC, aceptaOfertaEFI.getPromocion());
				if(Server.ALLOW_LOG) Log.d("", "estoy entrando analyze");
				}else if(operationId==Server.CONSULTA_DETALLE_OFERTA_EFI){
					OfertaEFI ofertaEFI= (OfertaEFI)response.getResponse();
					((BmovilViewsController)viewController.getParentViewsController()).showDetalleEFI(ofertaEFI, aceptaOfertaEFI.getPromocion());
					
				}

		}

	
	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		ArrayList<Object> datosResultados = new ArrayList<Object>();
		ArrayList<String> registro;	
			formatFecha();
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_credito));
			registro.add(Tools.hideAccountNumber(aceptaOfertaEFI.getNumeroTarjeta()));
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_cuenta_deposito));
			registro.add(Tools.hideAccountNumber(ofertaEFI.getAccountDestino().getNumber()));
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_importe));
			registro.add(Tools.formatAmount(aceptaOfertaEFI.getImpDispuesto().replace(",","")+"00",false));
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_tasaanual));
			registro.add(ofertaEFI.getTasaAnual()+"%");
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_plazo));
			registro.add(aceptaOfertaEFI.getPlazoMeses()+" meses");
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_plazo_fijo));
			registro.add(Tools.formatAmount(aceptaOfertaEFI.getPagoMensual().replace(",",""),false));
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_comision));
			registro.add(Tools.formatAmount(aceptaOfertaEFI.getComisionEFIMonto().replace(",",""),false));
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_fecha));
			registro.add(aceptaOfertaEFI.getFechaOperacion());
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_hora));		
			registro.add(aceptaOfertaEFI.getHoraOperacion());
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_folio));
			registro.add(aceptaOfertaEFI.getFolioAST());
			datosResultados.add(registro);
		
		return datosResultados;
		
	}
	
	@Override
	public int getTextoEncabezado() {
		return R.string.bmovil_promocion_title_efi;
	}
	
	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.icono_pagar_servicios;
	}
	
	public void showMenu() {
		// TODO Auto-generated method stub
		((BmovilViewsController)viewController.getParentViewsController()).showMenuPrincipal();
		
	}
	
	public void showResultados(){
		((BmovilViewsController)viewController.getParentViewsController()).showResultadosViewController(this, -1, -1);
	}
	
	public void showDetallePromocion(){
		String cveCamp=aceptaOfertaEFI.getPromocion().getCveCamp().substring(0,4);
		if(cveCamp.equals("0130"))
			realizaOperacion(Server.CONSULTA_DETALLE_OFERTA,viewController,false,false );
		else if(cveCamp.equals("0377"))		
			realizaOperacion(Server.CONSULTA_DETALLE_OFERTA_EFI,viewController,false,false);		
		
	}
	
	public BaseViewController getcontroladorExitoILC() {
		return viewController;
	}

	public void setcontroladorExitoILCView(BaseViewController viewController) {
		this.viewController = viewController;
	}
	
	@Override
   	public String getTextoAyudaResultados() {
    	return "CAT "+ofertaEFI.getCat()+" sin I.V.A Informativo.\nFecha de Cálculo al "+ ofertaEFI.getFechaCat();
	}
	
	@Override
	public String getTituloTextoEspecialResultados() {
		return "";
	}
	
	@Override
	public String getTextoEspecialResultados() {
		return viewController.getString(R.string.bmovil_result_textoAyuda);
	}
	
	public void formatFecha(){	
		try{
		String fecha= aceptaOfertaEFI.getFechaOperacion();
		String fechaFormat= fecha.substring(0, 11);
		String horaFormat= fecha.substring(11);
		aceptaOfertaEFI.setHoraOperacion(horaFormat);
		String dia=fechaFormat.substring(8,10);
		String mes= fechaFormat.substring(5,7);
		String anio = fechaFormat.substring(0,4);
		String fechaFormateada=dia+"/"+mes+"/"+anio;
		aceptaOfertaEFI.setFechaOperacion(fechaFormateada);
		fechaEnviada=anio+"-"+mes+"-"+dia;
		}catch(Exception ex){
			if(Server.ALLOW_LOG) Log.e("","error formato"+ex);
		}
	}
}
