package suitebancomer.aplicaciones.bmovil.classes.model;

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Nicolas Arriaza on 31/07/2015.
 */
public class TransferenciaSPEI{

    private String bancoOrdenante;

    private String cuentaOrdenante;

    private String nombreOrdenante;

    private String fechaCalendario;

    private String montoDelPago;

    private String bancoBeneficiario;

    private String cuentaBeneficiaria;

    private String nombreDelBeneficiario;

    private String conceptoDelPago;

    private String numeroDeReferencia;

    private String claveDeRastreo;

    public String getBancoOrdenante() {
        return bancoOrdenante;
    }

    public void setBancoOrdenante(String bancoOrdenante) {
        this.bancoOrdenante = bancoOrdenante;
    }

    public String getCuentaOrdenante() {
        return cuentaOrdenante;
    }

    public void setCuentaOrdenante(String cuentaOrdenante) {
        this.cuentaOrdenante = cuentaOrdenante;
    }

    public String getNombreOrdenante() {
        return nombreOrdenante;
    }

    public void setNombreOrdenante(String nombreOrdenante) {
        this.nombreOrdenante = nombreOrdenante;
    }

    public String getFechaCalendario() {
        return fechaCalendario;
    }

    public void setFechaCalendario(String fechaCalendario) {
        this.fechaCalendario = fechaCalendario;
    }

    public String getMontodelPago() {
        return montoDelPago;
    }

    public void setMontodelPago(String montodelPago) {
        this.montoDelPago = montodelPago;
    }

    public String getBancoBeneficiario() {
        return bancoBeneficiario;
    }

    public void setBancoBeneficiario(String bancoBeneficiario) {
        this.bancoBeneficiario = bancoBeneficiario;
    }

    public String getCuentaBeneficiaria() {
        return cuentaBeneficiaria;
    }

    public void setCuentaBeneficiaria(String cuentaBeneficiaria) {
        this.cuentaBeneficiaria = cuentaBeneficiaria;
    }

    public String getNombreDelBeneficiario() {
        return nombreDelBeneficiario;
    }

    public void setNombreDelBeneficiario(String nombreDelBeneficiario) {
        this.nombreDelBeneficiario = nombreDelBeneficiario;
    }

    public String getConceptoDelPago() {
        return conceptoDelPago;
    }

    public void setConceptoDelPago(String conceptoDelPago) {
        this.conceptoDelPago = conceptoDelPago;
    }

    public String getNumeroDeReferencia() {
        return numeroDeReferencia;
    }

    public void setNumeroDeReferencia(String numeroDeReferencia) {
        this.numeroDeReferencia = numeroDeReferencia;
    }

    public String getClaveDeRastreo() {
        return claveDeRastreo;
    }

    public void setClaveDeRastreo(String claveDeRastreo) {
        this.claveDeRastreo = claveDeRastreo;
    }


    public static class ClaveRastreoComparator implements Comparator<TransferenciaSPEI> {
        @Override
        public int compare(TransferenciaSPEI transfer1, TransferenciaSPEI transfer2) {
            return transfer2.getClaveDeRastreo().compareTo(transfer1.getClaveDeRastreo());
        }
    }

    public static class FechaComparator implements Comparator<TransferenciaSPEI> {
        @Override
        public int compare(TransferenciaSPEI transfer1, TransferenciaSPEI transfer2) {
            DateFormat format = new SimpleDateFormat("dd/MM/yy");
            try {
                Date date1 = format.parse(transfer1.getFechaCalendario());
                Date date2 = format.parse(transfer2.getFechaCalendario());
                int result = date2.compareTo(date1);
                return result;
            } catch (ParseException e) {
                Log.w("FechaComparator", e);
                return -1;
            }
        }
    }
}
