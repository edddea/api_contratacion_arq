package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;


import java.util.Hashtable;

import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerMapperUtil;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionRegistroViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultarFrecuentesViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.OtrosBBVAViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.RegistrarOperacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.BeneficiarioBancomerData;
import suitebancomer.aplicaciones.bmovil.classes.model.Comision;
import suitebancomer.aplicaciones.bmovil.classes.model.FrecuenteMulticanalResult;
import suitebancomer.aplicaciones.bmovil.classes.model.Payment;
import suitebancomer.aplicaciones.bmovil.classes.model.PaymentExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.Rapido;
import suitebancomer.aplicaciones.bmovil.classes.model.RegistrarOperacionResult;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferResult;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaInterna;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaOtrosBBVA;
import suitebancomer.classes.gui.controllers.BaseViewController;
import android.text.Editable;
import android.util.Log;
//SPEI
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultThirdsAccountBBVAResult;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelper;

public class OtrosBBVADelegate extends DelegateBaseAutenticacion {
	
	public final static long OTROS_BBVA_DELEGATE_ID = 0x317600e7880d6352L;
	
	Constants.Operacion tipoOperacion;
	OtrosBBVAViewController otrosBBVAViewController;
	TransferenciaOtrosBBVA transferenciaOtrosBBVA;
	TransferResult result;//interbancariosResult
	
	boolean esFrecuente;
	boolean esExpress;
	boolean esTDC;
	private boolean bajaFrecuente;
	BaseViewController frecuenteViewController;
	
	Payment[] frecuentes;
	protected String tempPassword;
	Payment selectedPayment;
	
	
	public boolean getEsFrecuente() {
		return esFrecuente;
	}
	
	public void setEsFrecuente(boolean esFrecuente){
		this.esFrecuente = esFrecuente;
		this.bajaFrecuente = false;
		if (esFrecuente) {
			tipoOperacion = Constants.Operacion.transferirBancomerF;
		} else {
			tipoOperacion = Constants.Operacion.transferirBancomer;
		}
	}

	int longitudNumeroCuenta;
	Comision comision;
	
	public OtrosBBVADelegate(boolean esExpress, boolean esTDC) {
		longitudNumeroCuenta = 0;
		this.esExpress = esExpress;
		this.esTDC = esTDC;
	}
	
	public OtrosBBVADelegate(boolean esExpress, boolean esTDC, boolean esFrecuente) {
		this.esFrecuente = esFrecuente;
		this.esExpress = esExpress;
		this.esTDC = esTDC;
		if(esFrecuente)
			tipoOperacion = Constants.Operacion.transferirBancomerF;
	}

	public TransferenciaOtrosBBVA getTransferenciaOtrosBBVA() {
		return transferenciaOtrosBBVA;
	}

	public void setTransferenciaOtrosBBVA(TransferenciaOtrosBBVA transferenciaOtrosBBVA) {
		this.transferenciaOtrosBBVA = transferenciaOtrosBBVA;
		//frecuente correo electronico
		this.aliasFrecuente = null;
		this.correoFrecuente = null;
	}

	public boolean isEsExpress() {
		return esExpress;
	}
	
	public void setEsExpress(boolean esExpress) {
		this.esExpress = esExpress;
	
	}
	
	public boolean isEsTDC() {
		return esTDC;
	}
		
	public void setEsTDC(boolean esTDC) {
		this.esTDC = esTDC;
	}

	public boolean isBajaFrecuente() {
		return bajaFrecuente;
	}

	public void setBajaFrecuente(boolean bajaFrecuente) {
		this.bajaFrecuente = bajaFrecuente;
	}

	@Override
	public int getOpcionesMenuResultados() {
		if(esFrecuente)
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF | SHOW_MENU_RAPIDA  ;
		else if (bajaFrecuente) {
			return super.getOpcionesMenuResultados();
		}
		else 
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF | SHOW_MENU_FRECUENTE | SHOW_MENU_RAPIDA ;
	}

	@Override
	public String getTextoSMS() {
		// TODO Auto-generated method stub
		String folio = result.getFolio();
		String importe =Tools.formatAmount( transferenciaOtrosBBVA.getImporte(),false);
		String fecha = Tools.formatDate(result.getFecha());
		String hora = Tools.formatTime(result.getHora());
		String cuenta = transferenciaOtrosBBVA.getCuentaOrigen().getNombreTipoCuenta(otrosBBVAViewController.getResources());

		StringBuilder msg = new StringBuilder();
		msg.append(SuiteApp.appContext.getString(R.string.smsText_firstFrom));
		msg.append(" ");
		msg.append(cuenta);
		msg.append(" ");
		msg.append(Tools.hideAccountNumber(transferenciaOtrosBBVA.getCuentaOrigen().getNumber()));
		msg.append(" ");
		msg.append(SuiteApp.appContext.getString(R.string.smsText_a));
		msg.append(" ");
		msg.append(Tools.hideAccountNumber( transferenciaOtrosBBVA.getCuentaDestino()));
		msg.append(" ");
		msg.append(SuiteApp.appContext.getString(R.string.smsText_secondFrom));
		msg.append(" ");
		msg.append(transferenciaOtrosBBVA.getBeneficiario().substring(0, transferenciaOtrosBBVA.getBeneficiario().length()>Constants.MESSAGE_NAME_MAX_LENGTH?
																	  Constants.MESSAGE_NAME_MAX_LENGTH : 
																	  transferenciaOtrosBBVA.getBeneficiario().length()));
		msg.append(" ");
		msg.append(SuiteApp.appContext.getString(R.string.smsText_byQuantity));
		msg.append(" ");
		msg.append(importe);
		msg.append(" ");
		msg.append(SuiteApp.appContext.getString(R.string.smsText_folio));
		msg.append(" ");
		msg.append(folio);
		msg.append(" ");
		msg.append(fecha);
		msg.append(" ");
		msg.append(hora);
		String msgText = msg.toString().replaceAll("\n", "");
		
		return msgText;
	}

	@Override
	public String getTextoEmail() {
		// TODO Auto-generated method stub
		//El correo se compondrá del contenido del componente �ListaDatosViewController�
		//mostrado en la pantalla de resultados.
		return super.getTextoEmail();
	}

	@Override
	public String getTextoPDF() {
		// TODO Auto-generated method stub
		//El PDF se compondrá del contenido del componente �ListaDatosViewController� 
		//mostrado en la pantalla de resultados
		return super.getTextoPDF();
	}

	@Override
	public String getTextoPantallaResultados() {
		// TODO Auto-generated method stub
		return "";
		//return otrosBBVAViewController.getString(R.string.transferir_interbancario_texto_titulo_resultados);
	}

	@Override
	public String getTextoTituloResultado() {
		if (bajaFrecuente) {
			return frecuenteViewController.getString(R.string.baja_frecuentes_titulo_tabla_resultados);
		} else {
			return otrosBBVAViewController.getString(R.string.transferir_detalle_operacion_exitosaTitle);
		}
	}

	@Override
	public String getTextoTituloConfirmacion() {
		// TODO Auto-generated method stub
		return otrosBBVAViewController.getString(R.string.confirmation_subtitulo);
	}

	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
//		Resources res = SuiteApp.appContext.getResources();
		ArrayList<Object> datosConfirmacion = new ArrayList<Object>();
		ArrayList<String> registro;

		if (bajaFrecuente) {
			
			//Alias frecuente
			registro = new ArrayList<String>();
			registro.add(frecuenteViewController.getString(R.string.altafrecuente_nombrecorto));
			registro.add(transferenciaOtrosBBVA.getAliasFrecuente());
			datosConfirmacion.add(registro);
			
			//Cuenta destino
			registro = new ArrayList<String>();
			//registro.add(SuiteApp.appContext.getString(esExpress ? R.string.transferir_otrosBBVA_destiny_express : R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta));
			//SPEI
			registro.add(SuiteApp.appContext.getString(esExpress ? R.string.transferir_otrosBBVA_destiny_express : R.string.transferir_interbancario_tabla_resultados_cuenta_deposito));
			registro.add(esExpress ? transferenciaOtrosBBVA.getCuentaDestino() : Tools.hideAccountNumber(transferenciaOtrosBBVA.getCuentaDestino()));
			datosConfirmacion.add(registro);
			//Beneficiario
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.common_beneficiaryLabel));
			registro.add(transferenciaOtrosBBVA.getBeneficiario().toUpperCase());
			//String bene = transferenciaOtrosBBVA.getBeneficiario().toUpperCase();
			//registro.add(bene.replace("%20"," "));
			datosConfirmacion.add(registro);
			
			ArrayList<Object> entryList = datosConfirmacion;
			for(int i = entryList.size() - 1; i >= 0; i--) {
				try {
					@SuppressWarnings("unchecked")
					ArrayList<String> entry = (ArrayList<String>)entryList.get(i);
					if(entry.get(1).trim().equals(Constants.EMPTY_STRING)) 
						entryList.remove(i);
				} catch(Exception ex) {
					if(Server.ALLOW_LOG) Log.i(getClass().getSimpleName(), "Error al quitar el elemento.", ex);
				}
			}
			
		} else {

			//Cuenta de retiro
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
			registro.add(Tools.hideAccountNumber(transferenciaOtrosBBVA.getCuentaOrigen().getNumber()));
			datosConfirmacion.add(registro);
			
			//Número celular (SPEI)
			if(phoneNumber && !esExpress) {
				registro = new ArrayList<String>();
				registro.add(SuiteApp.appContext.getString(R.string.transferir_otrosBBVA_destiny_express));
				registro.add(transferenciaOtrosBBVA.getNumeroCelular());
				datosConfirmacion.add(registro);
			}
			
			//Cuenta destino
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(esExpress ? R.string.transferir_otrosBBVA_destiny_express : R.string.transferir_otrosBBVA_destiny_account));
			//registro.add(SuiteApp.appContext.getString(esExpress ? R.string.transferir_otrosBBVA_destiny_express : R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta));
			registro.add(esExpress ? transferenciaOtrosBBVA.getCuentaDestino() : Tools.hideAccountNumber(transferenciaOtrosBBVA.getCuentaDestino()));
			datosConfirmacion.add(registro);
			//Beneficiario
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.common_beneficiaryLabel));
			registro.add(transferenciaOtrosBBVA.getBeneficiario().toUpperCase());
			datosConfirmacion.add(registro);
			//Importe
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_importe));
			registro.add(Tools.formatAmount(transferenciaOtrosBBVA.getImporte(),false));
			datosConfirmacion.add(registro);
//			//Concepto
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_otrosBBVA_concepto));
			registro.add(Tools.removeSpecialCharacters(transferenciaOtrosBBVA.getConcepto()));
		//	registro.add(transferenciaOtrosBBVA.getConcepto());
			datosConfirmacion.add(registro);
			//registro.add(Tools.formatDate(result.getFecha()));
			
			//fecha
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_fechaoperacion));
			java.util.Date currentDate = Session.getInstance(SuiteApp.appContext).getServerDate();
			registro.add(Tools.dateToString(currentDate));
			datosConfirmacion.add(registro);

		}
		
		return datosConfirmacion;
	}

	@Override
	public ArrayList<Object> getDatosTablaResultados() {
//		Resources res = SuiteApp.appContext.getResources();
		ArrayList<Object> datosResultados = new ArrayList<Object>();
		ArrayList<String> registro;

		if (bajaFrecuente) {
			
			//Alias frecuente
			registro = new ArrayList<String>();
			registro.add(frecuenteViewController.getString(R.string.altafrecuente_nombrecorto));
			registro.add(transferenciaOtrosBBVA.getAliasFrecuente());
			datosResultados.add(registro);
			
			//Número celular (SPEI)
			if(phoneNumber && !esExpress) {
				registro = new ArrayList<String>();
				registro.add(SuiteApp.appContext.getString(R.string.transferir_otrosBBVA_destiny_express));
				registro.add(transferenciaOtrosBBVA.getNumeroCelular());
				datosResultados.add(registro);
			}
			
			//Cuenta destino
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(esExpress ? R.string.transferir_otrosBBVA_destiny_express : R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta));
			registro.add(esExpress ? transferenciaOtrosBBVA.getCuentaDestino() : Tools.hideAccountNumber(transferenciaOtrosBBVA.getCuentaDestino()));
			datosResultados.add(registro);
			//Beneficiario
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.common_beneficiaryLabel));
			registro.add(transferenciaOtrosBBVA.getBeneficiario().toUpperCase());
			datosResultados.add(registro);
			
		} else {

			registro = new ArrayList<String>();
			//Cuenta de retiro
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
			registro.add(Tools.hideAccountNumber(transferenciaOtrosBBVA.getCuentaOrigen().getNumber()));
			datosResultados.add(registro);
			//Cuenta destino
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(esExpress ? R.string.transferir_otrosBBVA_destiny_express : R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta));
			registro.add(esExpress ? transferenciaOtrosBBVA.getCuentaDestino() : Tools.hideAccountNumber(transferenciaOtrosBBVA.getCuentaDestino()));
			datosResultados.add(registro);
			//Beneficiario
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.common_beneficiaryLabel));
			registro.add(transferenciaOtrosBBVA.getBeneficiario().toUpperCase());
			datosResultados.add(registro);
			//Importe
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_importe));
			registro.add(Tools.formatAmount(transferenciaOtrosBBVA.getImporte(),false));
			datosResultados.add(registro);
//			//Concepto
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_otrosBBVA_concepto));
			registro.add(transferenciaOtrosBBVA.getConcepto());
			datosResultados.add(registro);
			//fecha
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_fechaoperacion));
			registro.add(Tools.formatDate(result.getFecha()));
			datosResultados.add(registro);
			//hora
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_horaoperacion));
			registro.add(Tools.formatTime(result.getHora()));
			datosResultados.add(registro);
			//folio
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_folio));
			registro.add(result.getFolio());//agregar format day
			datosResultados.add(registro);
			//frecuente correo electronico
			//alias frecuente
			//aliasFrecuente = transferenciaOtrosBBVA.getAliasFrecuente() != null && !transferenciaOtrosBBVA.getAliasFrecuente().isEmpty() ? transferenciaOtrosBBVA.getAliasFrecuente() : aliasFrecuente;
			if(aliasFrecuente != null && !aliasFrecuente.isEmpty()){
				registro = new ArrayList<String>();
				registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_alias_frecuente));
				registro.add(aliasFrecuente);
				datosResultados.add(registro);
			}

			//correo frecuente
			//correoFrecuente = transferenciaOtrosBBVA.getCorreoFrecuente() != null && !transferenciaOtrosBBVA.getCorreoFrecuente().isEmpty() ? transferenciaOtrosBBVA.getCorreoFrecuente() : correoFrecuente;
			if(correoFrecuente !=null && !correoFrecuente.isEmpty()){
				registro = new ArrayList<String>();
				registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_correo_electronico_frecuente));
				registro.add(correoFrecuente);
				datosResultados.add(registro);
			}
			correoFrecuente = transferenciaOtrosBBVA.getCorreoFrecuente() != null && !transferenciaOtrosBBVA.getCorreoFrecuente().isEmpty() ? transferenciaOtrosBBVA.getCorreoFrecuente() : correoFrecuente;

		}
		
		return datosResultados;
	}

	@Override
	public boolean mostrarContrasenia() {
		boolean value;
		if (bajaFrecuente) {
			value =  Autenticacion.getInstance().mostrarContrasena(this.tipoOperacion, 
					Session.getInstance(SuiteApp.appContext).getClientProfile());
		} else {
			value =  Autenticacion.getInstance().mostrarContrasena(this.tipoOperacion, 
					Session.getInstance(SuiteApp.appContext).getClientProfile(), 
					Tools.getDoubleAmountFromServerString(transferenciaOtrosBBVA.getImporte()));
		}
		
		return value;
	}

	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			if (bajaFrecuente) {
				tipoOTP = Autenticacion.getInstance().tokenAMostrar(this.tipoOperacion,
						Session.getInstance(SuiteApp.appContext).getClientProfile());
			} else {
				tipoOTP = Autenticacion.getInstance().tokenAMostrar(this.tipoOperacion,
						Session.getInstance(SuiteApp.appContext).getClientProfile(),
						Tools.getDoubleAmountFromServerString(transferenciaOtrosBBVA.getImporte()));
			}
		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.mostrarNIP execution.", ex);
			tipoOTP = null;
		}
		
		return tipoOTP;
	}

	@Override
	public boolean mostrarNIP() {
		boolean value;
		if (bajaFrecuente) {
			value =  Autenticacion.getInstance().mostrarNIP(this.tipoOperacion, 
					Session.getInstance(SuiteApp.appContext).getClientProfile());
		} else {
			value =  Autenticacion.getInstance().mostrarNIP(this.tipoOperacion, 
					Session.getInstance(SuiteApp.appContext).getClientProfile(),
					Tools.getDoubleAmountFromServerString(transferenciaOtrosBBVA.getImporte()));
		}
	
		return value;
	}
	
	@Override
	public boolean mostrarCVV() {
		boolean value;
		if (bajaFrecuente) {
			value =  Autenticacion.getInstance().mostrarCVV(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile());
		} else {
			value =  Autenticacion.getInstance().mostrarCVV(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile(),
					Tools.getDoubleAmountFromServerString(transferenciaOtrosBBVA.getImporte()));
		}		
	
		return value;
	}

	@Override
	public ArrayList<Object> getDatosTablaAltaFrecuentes() {
		ArrayList<Object> datosAltaFrecuente = new ArrayList<Object>();
		ArrayList<String> registro;
		//Cuenta de retiro
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
		registro.add(Tools.hideAccountNumber(transferenciaOtrosBBVA.getCuentaOrigen().getNumber()));
		datosAltaFrecuente.add(registro);
		//Cuenta destino
		registro = new ArrayList<String>();
		//SPEI
		registro.add(SuiteApp.appContext.getString(esExpress ? R.string.transferir_otrosBBVA_destiny_express : R.string.transferir_otrosBBVA_destiny_account));
		//registro.add(SuiteApp.appContext.getString(esExpress ? R.string.login_userLabel : R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta));
		registro.add(esExpress ? transferenciaOtrosBBVA.getCuentaDestino() : Tools.hideAccountNumber(transferenciaOtrosBBVA.getCuentaDestino()));
		datosAltaFrecuente.add(registro);	
		//Beneficiario
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.common_beneficiaryLabel));
		registro.add(transferenciaOtrosBBVA.getBeneficiario().toUpperCase());
		datosAltaFrecuente.add(registro);
		
		return datosAltaFrecuente;
	}

	@Override
	public int getNombreImagenEncabezado() {
		if (esTDC) {
			return R.drawable.icono_pagar_servicios;
		} else {
			return R.drawable.bmovil_transferir_icono;
		}
	}

	@Override
	public int getTextoEncabezado() {
		if (esExpress) {
			return R.string.opcionesTransfer_menu_cuentaexpress;
		} else if (esTDC) {
			return R.string.transferir_otrosBBVA_TDC_title;
		} else {
			return R.string.opcionesTransfer_menu_otrascuentasbbva;
		}
	}

	@Override
	public String getTextoBotonOperacionNueva() {
		return SuiteApp.appContext.getString(R.string.transferir_otrosBBVA_texto_boton_operacion_nueva);
	}

	@Override
	public void realizaOperacion(ConfirmacionViewController confirmacionViewController,	String contrasenia, String nip, String token, String campoTarjeta) {
		//prepare data
        Hashtable<String,String> paramTable = null;
        tempPassword = Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia;
        int operacion = 0;
     	Session session = Session.getInstance(SuiteApp.appContext);
     	operacion = Server.BANCOMER_TRANSFER_OPERATION;
			paramTable = new Hashtable<String, String>();
			String tipoOperacion = "";
			String cuentaDestino = "";
			cuentaDestino = transferenciaOtrosBBVA.getCuentaDestino();
	    	paramTable.put(ServerConstants.ID_OPERACION, ServerConstants.TRANSFERIR_BANCOMER);
			paramTable.put("NT", session.getUsername());//"NT"
			paramTable.put("NP", Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia );//NP
			paramTable.put("IU", session.getIum());//IU
			paramTable.put("CC", transferenciaOtrosBBVA.getCuentaOrigen().getFullNumber());//CC
			paramTable.put("CA", esExpress ? Constants.EXPRESS_TYPE + transferenciaOtrosBBVA.getCuentaDestino() : transferenciaOtrosBBVA.getCuentaDestino());//CA
			
			//	System.out.println(esExpress ? Constants.EXPRESS_TYPE + transferenciaOtrosBBVA.getCuentaDestino() : transferenciaOtrosBBVA.getCuentaDestino());
			
			paramTable.put("IM", transferenciaOtrosBBVA.getImporte() == null ? "" : transferenciaOtrosBBVA.getImporte());//IM
			
			/*if(transferenciaOtrosBBVA.getCuentaDestino().length() == Constants.CHECK_ACCOUNT_NUMBER_LENGTH2){
				paramTable.put(Server.ACCOUNT_TYPE_PARAM, Constants.EXPRESS_TYPE);//TP
				paramTable.put(Server.DESTINATION_PARAM,Constants.EXPRESS_TYPE+transferenciaOtrosBBVA.getCuentaDestino());
			}
			else{*/
				paramTable.put("TP", tipoOperacion);//IU
			//}
				paramTable.put("TE", session.getClientNumber());//TE

				paramTable.put("MP", transferenciaOtrosBBVA.getConcepto());//MP


			paramTable.put("NI", "");
			paramTable.put("CV", "");
			paramTable.put("OT", Tools.isEmptyOrNull(token)?"":token);
			String va =  Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile(),
					Tools.getDoubleAmountFromServerString(transferenciaOtrosBBVA.getImporte()));
			paramTable.put("VA", Tools.isEmptyOrNull(va) ? "" : va);//validar
//		}
     	//JAIG
		doNetworkOperation(operacion, paramTable, false, new TransferResult(), isJsonValueCode.NONE, confirmacionViewController);

	}

	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params, boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode,BaseViewController caller) {
		if(otrosBBVAViewController != null)
			((BmovilViewsController)otrosBBVAViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
		else if(frecuenteViewController != null)
		  ((BmovilViewsController)frecuenteViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			if (operationId == Server.OP_VALIDAR_CREDENCIALES) {
				showConfirmacionRegistro();
				return;
			}
			if (operationId == Server.OP_SOLICITAR_ALERTAS) {

				setSa((SolicitarAlertasData) response.getResponse());
				analyzeAlertasRecortado();
				return;
			}
			//SPEI
			if(operationId == Server.CONSULT_BENEFICIARY_ACCOUNT_NUMBER) {
				analyzeConsultThirdsAccountBBVAResponse(response);
			} else if (operationId == Server.CONSULTA_BENEFICIARIO) {
				BeneficiarioBancomerData data = (BeneficiarioBancomerData)response.getResponse();
				transferenciaOtrosBBVA.setBeneficiario(data.getNombreBeneficiarioCompleto());
				//CÛdigo Ale y Marco DyD
				if(transferenciaOtrosBBVA.getCuentaDestino().length() == Constants.CHECK_ACCOUNT_NUMBER_LENGTH2){
					esExpress = true;
					spei = false;
				}else{
				esExpress = false;
				spei = false;
				}
				//Termina CÛdigo Ale y Marco
				showConfirmacion();
			} else if (response.getResponse() instanceof TransferResult) {
				result = (TransferResult) response.getResponse();
				if ( esFrecuente && (tokenAMostrar() == Constants.TipoOtpAutenticacion.codigo 
						|| tokenAMostrar() == Constants.TipoOtpAutenticacion.registro)
						&& Tools.isEmptyOrNull(transferenciaOtrosBBVA.getFrecuenteMulticanal())) {
					actualizaFrecuenteAMulticanal();
				} else {
					transferenciaExitosa();
				}
			
			} else if (response.getResponse() instanceof Comision) {
				comision = (Comision)response.getResponse();
				showConfirmacion();
			} else if (response.getResponse() instanceof PaymentExtract) {
				PaymentExtract extract = (PaymentExtract) response.getResponse();
				frecuentes = extract.getPayments();
				mostrarListaFrecuentes();
			} else if (response.getResponse() instanceof FrecuenteMulticanalResult) {
				transferenciaExitosa();
			} else {
				((BmovilViewsController)frecuenteViewController.getParentViewsController()).showResultadosViewController(this, -1, -1);
			}
			
		} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
			
			if (operationId == Server.BANCOMER_TRANSFER_OPERATION) {
				if(Server.ALLOW_LOG) Log.i("OtrosBBVADelegate: ", "EA#8 Montos Mayores paso 2: recibe msg ERROR");
				
				Session session = Session.getInstance(SuiteApp.appContext);
				Constants.Perfil perfil = session.getClientProfile();
				
				if (Constants.Perfil.recortado.equals(perfil)) {		
					if(Server.ALLOW_LOG) Log.i("OtrosBBVADelegate: ", "EA#11 Usuario con perfil Recortado CON alertas");
					setTransferErrorResponse(response);
					solicitarAlertas(otrosBBVAViewController);
					
				} else {
					BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
					BmovilViewsController bmvc = bmApp.getBmovilViewsController(); 
					BaseViewController current = bmvc.getCurrentViewControllerApp();
					current.showInformationAlert(response.getMessageText());
				}
				
			} else {
				
				if (operationId != Server.FAVORITE_PAYMENT_OPERATION) {
					BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
					BmovilViewsController bmvc = bmApp.getBmovilViewsController(); 
					BaseViewController current = bmvc.getCurrentViewControllerApp();
					current.showInformationAlert(response.getMessageText());
				}
			}
		}
	}

	public Constants.Operacion getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(Constants.Operacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public void setOtrosBBVAViewController(OtrosBBVAViewController otrosBBVAViewController) {
		this.otrosBBVAViewController = otrosBBVAViewController;
	}
	
	public TransferResult getResult(){		
		return result;
	}
	
	public void validaDatos(String cuenta, String importe){

		//ARR variable para realizar el paso 2 del etiquetado
		boolean valida = false;
		
		if (transferenciaOtrosBBVA.getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE)) {
			otrosBBVAViewController.showInformationAlert(R.string.transferir_otrosBBVA_invalid_origin);
			return;
		}
		
		if (cuenta == null || cuenta.equals("")) {
			//if(esExpress)
			//	otrosBBVAViewController.showInformationAlert( R.string.transferir_otrosBBVA_expressNumberCannotBeEmpty );
		if(esExpress || phoneNumber)
			otrosBBVAViewController.showInformationAlert(R.string.transferir_otrosBBVA_error_empty_phone);
			else
				//otrosBBVAViewController.showInformationAlert(R.string.transferir_otrosBBVA_cardNumberCannotBeEmpty);
				otrosBBVAViewController.showInformationAlert(R.string.transferir_otrosBBVA_error_empty_card);

				return;
		} else if (cuenta.length() != Constants.CHECK_ACCOUNT_NUMBER_LENGTH2) {
			if (cuenta.length() != Constants.CHECK_ACCOUNT_NUMBER_LENGTH){
				if (cuenta.length() != Constants.CARD_NUMBER_LENGTH) {
				otrosBBVAViewController.showInformationAlert(R.string.transferir_otrosBBVA_invalid_account);
				return;
				}
				else{
					valida = true;
					transferenciaOtrosBBVA.setCuentaDestino(cuenta);
				}
				} else {
					valida = true;
				transferenciaOtrosBBVA.setCuentaDestino(cuenta);
			}
		} else {
			//if (isEsExpress()) {
				//cuenta = Constants.EXPRESS_TYPE + cuenta;
			//}
			valida = true;
			transferenciaOtrosBBVA.setCuentaDestino(cuenta);
			esExpress = true;
		}

		
		if (validaMonto(importe)) {
			transferenciaOtrosBBVA.setImporte(importe);
		} else {
			otrosBBVAViewController.showInformationAlert(R.string.transferir_otrosBBVA_invalid_amount);
			return;
		}
		//SPEI
		if(!phoneNumber){
		getNombreBeneficiario();
		}
		else{
			consultThirdsAccountBBVA(cuenta, importe);
//				showConfirmacion();
			}
		if(valida)
		{
			//ARR
			Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
			
			if(esExpress)
			{
				//ARR
				paso2OperacionMap.put("evento_paso2", "event47");
				paso2OperacionMap.put("&&products", "operaciones;transferencias+cuenta express");
				paso2OperacionMap.put("eVar12", "paso2:cuenta e importe");

				TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
			}
			else if(esTDC)
			{
				//ARR
				paso2OperacionMap.put("evento_paso2", "event47");
				paso2OperacionMap.put("&&products", "operaciones;pagar+tarjeta credito");
				paso2OperacionMap.put("eVar12", "paso2:cuenta e importe");

				TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
			}
			else
			{
				//ARR
				paso2OperacionMap.put("evento_paso2", "event47");
				paso2OperacionMap.put("&&products", "operaciones;transferencias+otra cuenta bbva bancomer");
				paso2OperacionMap.put("eVar12", "paso2:cuenta e importe");

				TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
			}
			
			
		}
			
	}
	
	private void getNombreBeneficiario() {
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> paramTable = null;
		paramTable = new Hashtable<String, String>();
		paramTable.put("NT", session.getUsername());// NT
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());// IU
		paramTable.put("TE", session.getClientNumber());// TE
		
		paramTable.put("TN", esExpress ? transferenciaOtrosBBVA.getCuentaDestino() : transferenciaOtrosBBVA.getCuentaDestino()); //TN
		if (esExpress) {
			paramTable.put("TP", Constants.EXPRESS_TYPE); //TP
		} else {
			if(transferenciaOtrosBBVA.getCuentaDestino().length() == Constants.CHECK_ACCOUNT_NUMBER_LENGTH2)
			{
				paramTable.put("TP", Constants.EXPRESS_TYPE); //TP
				//transferenciaOtrosBBVA.setCuentaDestino(Constants.EXPRESS_TYPE+transferenciaOtrosBBVA.getCuentaDestino());
				esExpress = true;
			}
			else{
			paramTable.put("TP", ""); //TP
			}

		}
		//JAIG
		doNetworkOperation(Server.CONSULTA_BENEFICIARIO, paramTable,false,new BeneficiarioBancomerData(),isJsonValueCode.NONE, otrosBBVAViewController);
	}
	
	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada seg�n sea reguerido.
	 * @return
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		//Account[] accounts = Session.getAccountsMC();
		
		if(profile == Constants.Perfil.avanzado) {
//			if(otrosBBVAViewController instanceof InterbancariosViewController) {
				for(Account acc : accounts) {
					if(acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE)) {
						accountsArray.add(acc);
						break;
					}
				}
				
				for(Account acc : accounts) {
					if(!acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE))
						accountsArray.add(acc);
				}
		//	}
			
		} else {
			for(Account acc : accounts) {
				if(acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE))
					accountsArray.add(acc);
			}
		}
		
		return accountsArray;
	}

	public void showConfirmacion() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		/*if (registrarOperacion())
			bMovilVC.showRegistrarOperacion(this);
		else
			bMovilVC.showConfirmacion(this);*/
		if (registrarOperacion()) {			
			bMovilVC.showRegistrarOperacion(this, spei);
		} else {
			bMovilVC.showConfirmacion(this);
	}
	}

	@Override
	public int getColorTituloResultado() {
		if (bajaFrecuente) {
			return R.color.magenta;
		} else {
			return R.color.verde_limon;
		}
	}
    
	/**
	 * Validates if the amount is correct.
	 * @param amount The amount for the transaction.
	 * @return True if the amount is correct.
	 */
	public boolean validaMonto(String amount) {
		long value = -1;
		
		try {
			value = Long.valueOf(amount);
		} catch (NumberFormatException ex) {
			if(Server.ALLOW_LOG) Log.e("TransferirMisCuentasDelegate", "Error while parsing the amount as an int.", ex);
			return false;
		}
		
		return (value > 0);	
	}

    
    @Override
    public void performAction(Object obj) {
    	if(otrosBBVAViewController != null) {
    		otrosBBVAViewController.actualizarSeleccion(obj);
    	}
    }
    
   @Override
   	public String getTextoAyudaResultados() {
  		if (bajaFrecuente) {
   			return super.getTextoAyudaResultados();
   		} else {
   			return SuiteApp.appContext.getString(R.string.transferir_interbancario_texto_ayuda_resultados);
   		}
   }
   
   //Consulta de frecuentes 
   /**
    * @return the viewController
    */
   public BaseViewController getViewController() {
	   return frecuenteViewController;
   }

   /**
    * @param viewController the viewController to set
    */
   public void setViewController(BaseViewController viewController) {
	   this.frecuenteViewController = viewController;
   }
   
   @Override
   public void consultarFrecuentes(String tipoFrecuente){
	   esFrecuente = true;
	   Session session = Session.getInstance(SuiteApp.appContext);
	   Hashtable<String,String> paramTable = null;
	   paramTable = new Hashtable<String, String>();
	  /* paramTable.put(Server.USERNAME_PARAM, 	session.getUsername());//NT
	   paramTable.put(ServerConstants.IUM_ETIQUETA,			session.getIum());//IU
	   paramTable.put(Server.NUMERO_CLIENTE_PARAM,session.getClientNumber());//TE
	   */

	   if(esExpress)
		   tipoFrecuente = Constants.tipoCFCExpress;


	   if(tipoFrecuente.equals(Constants.tipoCFOtrosBBVA))
		{
			/*
		   		paramTable.put("numeroTelefono", 	session.getUsername());//NT
			   paramTable.put("IUM",			session.getIum());//IU
			   paramTable.put("numeroCliente",session.getClientNumber());//TE
			   //JAIG
				  ((BmovilViewsController)frecuenteViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION_BBVA, paramTable,true,new PaymentExtract(),isJsonValueCode.NONE, frecuenteViewController,true);
		*/
			paramTable.put("NT", 	session.getUsername());//NT
			paramTable.put("IU",			session.getIum());//IU
			paramTable.put("TE",session.getClientNumber());//TE
			paramTable.put("TP",tipoFrecuente);//TP
			paramTable.put("AP", "");
			//correo frecuentes
			paramTable.put("VM", Constants.APPLICATION_VERSION);
			((BmovilViewsController)frecuenteViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable,  false, new PaymentExtract(), isJsonValueCode.NONE, frecuenteViewController, true);

		}
		else
		{
			paramTable.put("NT", 	session.getUsername());//NT
			paramTable.put("IU",			session.getIum());//IU
			paramTable.put("TE",session.getClientNumber());//TE
			paramTable.put("TP",tipoFrecuente);//TP
			paramTable.put("AP", "");
			//correo frecuentes
			paramTable.put("VM", Constants.APPLICATION_VERSION);
			   //JAIG
			  ((BmovilViewsController)frecuenteViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable,false,new PaymentExtract(),isJsonValueCode.NONE, frecuenteViewController,true);
		}
	  // paramTable.put(Server.TIPO_CONSULTA_PARAM,tipoFrecuente);//TP
//	   doNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable, frecuenteViewController);

   }
   
   private void mostrarListaFrecuentes(){
	  ((ConsultarFrecuentesViewController)frecuenteViewController).muestraFrecuentes();
   }
   
   @SuppressWarnings("unchecked")
public void operarFrecuente(int indexSelected){
	   ArrayList<Object> arlPayments = null;
	   if (frecuenteViewController instanceof ConsultarFrecuentesViewController) {
		   arlPayments = ((ConsultarFrecuentesViewController)frecuenteViewController).getListaFrecuentes().getLista();
	   }
	   
	   Payment selectedPayment = frecuentes[indexSelected];
	   if (arlPayments != null && arlPayments.size() > 0) {
		   selectedPayment = (Payment)((ArrayList<Object>)arlPayments.get(indexSelected)).get(0);
	   }
	   ArrayList<Account> cuentas = cargaCuentasOrigen();
	   Account cuentaOrigen = null ;
	   if(cuentas.size() > 0)
		   cuentaOrigen = cuentas.get(0);
	   
	   transferenciaOtrosBBVA = new TransferenciaOtrosBBVA();
	   transferenciaOtrosBBVA.setAliasFrecuente(selectedPayment.getNickname());
	   transferenciaOtrosBBVA.setBeneficiario(selectedPayment.getBeneficiary());
	   transferenciaOtrosBBVA.setCuentaDestino(selectedPayment.getBeneficiaryAccount());
	   transferenciaOtrosBBVA.setTipoCuenta(selectedPayment.getBeneficiaryTypeAccount());
	   transferenciaOtrosBBVA.setCuentaOrigen(cuentaOrigen);
	   transferenciaOtrosBBVA.setTipoOperacion(selectedPayment.getOperationCode());
	   transferenciaOtrosBBVA.setFrecuenteMulticanal(selectedPayment.getIdToken());
	   transferenciaOtrosBBVA.setIdCanal(selectedPayment.getIdCanal());
	   transferenciaOtrosBBVA.setUsuario(selectedPayment.getUsuario());
	   //frecuente correo electronico
	   transferenciaOtrosBBVA.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());
	   esFrecuente = true;


		if (frecuenteViewController != null) {
//			if (transferenciaOtrosBBVA.getTipoOperacion() == Constants.tipoCFCExpress) {
			if (selectedPayment.getBeneficiaryTypeAccount().equals(Constants.EXPRESS_TYPE) && !(selectedPayment.getBeneficiaryAccount().length()>10)) {
				((BmovilViewsController) frecuenteViewController
						.getParentViewsController())
						.showOtrosBBVAViewController(transferenciaOtrosBBVA, true, false);
			} else if (transferenciaOtrosBBVA.getTipoOperacion() == Constants.tipoCFTarjetasCredito) {
				((BmovilViewsController) frecuenteViewController
						.getParentViewsController())
						.showOtrosBBVAViewController(transferenciaOtrosBBVA, false, true);
			} else {
				((BmovilViewsController) frecuenteViewController
						.getParentViewsController())
						.showOtrosBBVAViewController(transferenciaOtrosBBVA, false, false);
			}
		}
   }
   
	@SuppressWarnings("unchecked")
	@Override
	public void eliminarFrecuente(int indexSelected) {
		setBajaFrecuente(true);
		esFrecuente = false;
		ArrayList<Object> arlPayments = null;
		if (frecuenteViewController instanceof ConsultarFrecuentesViewController) {
			arlPayments = ((ConsultarFrecuentesViewController)frecuenteViewController).getListaFrecuentes().getLista();
		}
		   
		Payment selectedPayment = frecuentes[indexSelected];
		if (arlPayments != null && arlPayments.size() > 0) {
			selectedPayment = (Payment)((ArrayList<Object>)arlPayments.get(indexSelected)).get(0);
		}
		this.selectedPayment = selectedPayment;
		ArrayList<Account> cuentas = cargaCuentasOrigen();
		Account cuentaOrigen = null ;
		if(cuentas.size() > 0)
			cuentaOrigen = cuentas.get(0);
		   
		transferenciaOtrosBBVA = new TransferenciaOtrosBBVA();
		transferenciaOtrosBBVA.setAliasFrecuente(selectedPayment.getNickname());
		transferenciaOtrosBBVA.setBeneficiario(selectedPayment.getBeneficiary());
		if (Server.ALLOW_LOG) System.out.println("Frecuente<----->Beneficario: "+Tools.removeSpecialCharacters(selectedPayment.getBeneficiary()));
		transferenciaOtrosBBVA.setCuentaDestino(selectedPayment.getBeneficiaryAccount());
		transferenciaOtrosBBVA.setTipoCuenta(selectedPayment.getBeneficiaryTypeAccount());
		transferenciaOtrosBBVA.setCuentaOrigen(cuentaOrigen);
//		transferenciaOtrosBBVA.setTipoOperacion(selectedPayment.getOperationCode());
		transferenciaOtrosBBVA.setFrecuenteMulticanal(selectedPayment.getIdToken());
		transferenciaOtrosBBVA.setIdCanal(selectedPayment.getIdCanal());
		transferenciaOtrosBBVA.setUsuario(selectedPayment.getUsuario());
		//frecuente correo electronico
		transferenciaOtrosBBVA.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());

		setTipoOperacion(Constants.Operacion.bajaFrecuente);
		esExpress = (selectedPayment.getBeneficiaryTypeAccount().equals(Constants.EXPRESS_TYPE));
		showConfirmacionAutenticacion();
	}
      
   @Override
	public ArrayList<Object> getDatosTablaFrecuentes() {
		ArrayList<Object> listaDatos = new ArrayList<Object>();
		if (frecuentes != null) {
			for (Payment payment : frecuentes) {
				ArrayList<Object> registro = new ArrayList<Object>();
				registro.add(payment);
				registro.add(payment.getNickname());
				if (esExpress) {
					registro.add(payment.getBeneficiaryAccount());
				} else {
					registro.add(Tools.hideAccountNumber(payment.getBeneficiaryAccount()));
				}
				listaDatos.add(registro);
			}
		} /*else {
			ArrayList<Object> registro = new ArrayList<Object>();
			registro.add("");
			registro.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_frecuentes_emptylist));
			registro.add("");
			listaDatos.add(registro);
		}*/
		return listaDatos;
	}
   
	@Override
	public ArrayList<Object> getDatosHeaderTablaFrecuentes() {
		ArrayList<Object> registros = new ArrayList<Object>();
		registros.add("");
		registros.add(SuiteApp.appContext.getString(R.string.altafrecuente_nombrecorto));
		if(esExpress)
			registros.add(SuiteApp.appContext.getString(R.string.transferir_otrosBBVA_destiny_express));
		else
			registros.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_frecuentes_header_cuenta));
		return registros;
	}

	/**
	 * Displays a text that indicates that external transfers cannot be
	 * performed during current time.
	 */
	public String textoMensajeAlerta() {
		return Session.getInstance(SuiteApp.getInstance())
				.getOpenHoursForExternalTransfersMessage();
	}
   
	/* 
	 * 
	 */
	@Override
	public Hashtable<String, String> getParametrosAltaFrecuentes() {
		 Session session = Session.getInstance(SuiteApp.appContext);
		 Hashtable<String,String> paramTable = null;		
		 paramTable = new Hashtable<String, String>();
		 paramTable.put("NT", session.getUsername());
		 paramTable.put("IU", session.getIum());
		 paramTable.put("TE", session.getClientNumber());
		 // System.out.println("Bandera Expres: "+esExpress);
		 if (esExpress) {

			 //	 System.out.println("Tipo cuenta: "+transferenciaOtrosBBVA.getTipoCuenta());
			 if(Tools.isEmptyOrNull(transferenciaOtrosBBVA.getTipoCuenta())){
				 transferenciaOtrosBBVA.setTipoCuenta(Constants.EXPRESS_TYPE);
			 }

			 paramTable.put("CA", transferenciaOtrosBBVA.getTipoCuenta() + transferenciaOtrosBBVA.getCuentaDestino());


			 // System.out.println(transferenciaOtrosBBVA.getTipoCuenta() + transferenciaOtrosBBVA.getCuentaDestino());
		 }else {

            /* if(transferenciaOtrosBBVA.getCuentaDestino().length() == Constants.CHECK_ACCOUNT_NUMBER_LENGTH2)
             {
                 paramTable.put(Server.PAYMENT_ACCOUNT_PARAM, Constants.EXPRESS_TYPE + transferenciaOtrosBBVA.getCuentaDestino() ); //CA
             }
             else{*/
            	 paramTable.put("CA", transferenciaOtrosBBVA.getCuentaDestino());
             //}
		 }
		 paramTable.put("BF", transferenciaOtrosBBVA.getBeneficiario());
		 if(Tools.isEmptyOrNull(transferenciaOtrosBBVA.getTipoOperacion())){
			 
			 /*if(transferenciaOtrosBBVA.getCuentaDestino().length() == Constants.CHECK_ACCOUNT_NUMBER_LENGTH2)
             {
                 paramTable.put(Server.TIPO_CONSULTA_PARAM, "08"); //CA
             }*/
			 
			 // no expreess 
			 paramTable.put("TP", Constants.TPTransferBBVA);
		 }else{	
			 if (esTDC)
				transferenciaOtrosBBVA.setTipoOperacion(Constants.TPTransferBBVA);
			 paramTable.put("TP", transferenciaOtrosBBVA.getTipoOperacion());
		 }
		 if(esExpress){
			 transferenciaOtrosBBVA.setTipoOperacion(Constants.TPTransferExpress);
			 paramTable.put("TP", transferenciaOtrosBBVA.getTipoOperacion());
		 }
		 paramTable.put("OA", "");
		 paramTable.put("CC", "");
		 paramTable.put("CB", "");
		 paramTable.put("IM", "");
		 paramTable.put("RF", "");
		 paramTable.put("CP", "");
		 paramTable.put("AP", "");
		 paramTable.put("CV", "");
		 return paramTable;
	}
	
	/**
	 * envia la actualizacion del frecuente
	 */
	private void actualizaFrecuenteAMulticanal() {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), "actualizaFrecuenteAMulticanal");
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> paramTable = null;
		paramTable = new Hashtable<String, String>();
		paramTable.put("NT", session.getUsername());
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());
		paramTable.put("TE", session.getClientNumber());
		String tipoConsultaParam = "";
		if(esTDC){
			tipoConsultaParam  = Constants.TPTarjetaCredito;
		}else if(esExpress){
			tipoConsultaParam = Constants.TPTransferExpress;
			
		}else{
			tipoConsultaParam = Constants.TPTransferBBVA2;
		}
		paramTable.put("TP", tipoConsultaParam);
		paramTable.put("CA",transferenciaOtrosBBVA.getTipoCuenta() + transferenciaOtrosBBVA.getCuentaDestino());
		paramTable.put("DE", transferenciaOtrosBBVA.getAliasFrecuente());
		paramTable.put("RF", "");
		paramTable.put("BF", transferenciaOtrosBBVA.getBeneficiario());
		paramTable.put("CB", "");
		paramTable.put(Server.CN_PARAM, transferenciaOtrosBBVA.getIdCanal());
		paramTable.put("US", transferenciaOtrosBBVA.getUsuario());
		paramTable.put("OA", "");
		//frecuente correo electronico
		paramTable.put("CE", transferenciaOtrosBBVA.getCorreoFrecuente());

		//JAIG
		doNetworkOperation(Server.ACTUALIZAR_FRECUENTE, paramTable, false, new FrecuenteMulticanalResult(), isJsonValueCode.NONE, otrosBBVAViewController);
	}
	
	/**
	 * continuacion del flujo despues de actulizar el frecuente 
	 * continuacion del flujo normal para mostrar resultados
	 */
	private void transferenciaExitosa(){
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), "transferenciaExitosa");
		Session session = Session.getInstance(SuiteApp.appContext);
		session.actualizaMonto(transferenciaOtrosBBVA.getCuentaOrigen(), result.getSaldoActual());		
		((BmovilViewsController)otrosBBVAViewController.getParentViewsController()).showResultadosViewController(this, -1, -1);		
	}

	public void showConfirmacionAutenticacion(){
		BmovilViewsController bmovilParentController = ((BmovilViewsController)frecuenteViewController.getParentViewsController());
		bmovilParentController.showConfirmacionAutenticacionViewController(this, getNombreImagenEncabezado(),
				getTextoEncabezado(),
				R.string.confirmation_subtitulo);
	}
	
	@Override
	public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController,String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
		//prepare data
        Hashtable<String,String> paramTable = null;
        tempPassword = Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia;
        int operacion = 0;
     	Session session = Session.getInstance(SuiteApp.appContext);
     	
    		operacion = Server.BAJA_FRECUENTE;
			//completar hashtable
			paramTable = new Hashtable<String, String>();
			String tipoOperacion = Tools.isEmptyOrNull(transferenciaOtrosBBVA.getTipoOperacion())? "" : transferenciaOtrosBBVA.getTipoOperacion();

			paramTable.put(Server.USERNAME_PARAM, session.getUsername());//"NT"
			paramTable.put(Server.PASSWORD_PARAM, Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia );//NP
			paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());//IU
			paramTable.put(Server.NUMERO_CLIENTE_PARAM, session.getClientNumber());//TE
			paramTable.put(Server.DESCRIPCION_PARAM, transferenciaOtrosBBVA.getAliasFrecuente());//DE
			 if (esExpress) {
				 paramTable.put(Server.PAYMENT_ACCOUNT_PARAM, transferenciaOtrosBBVA.getTipoCuenta() + transferenciaOtrosBBVA.getCuentaDestino());
			 }else {
				 paramTable.put(Server.PAYMENT_ACCOUNT_PARAM, transferenciaOtrosBBVA.getCuentaDestino());
			 }
//			paramTable.put(Server.BENEFICIARIO_PARAM, compraTiempoAire.getNombreCompania());//BF
//			paramTable.put(Server.REFERENCIA_NUMERICA_PARAM, compraTiempoAire.getCelularDestino());//RF
//			String idToken = (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) ? "T" : "";
			paramTable.put(Server.ID_TOKEN, selectedPayment.getIdToken());
			paramTable.put(Server.TIPO_CONSULTA_PARAM, esExpress ? Constants.TPTransferExpress:Constants.TPTransferBBVA);
			String va =  Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile());
			paramTable.put(Server.VA_PARAM,Tools.isEmptyOrNull(va)?"":va);//validar
			paramTable.put(Server.ID_CANAL, transferenciaOtrosBBVA.getIdCanal());
			paramTable.put(Server.USUARIO, transferenciaOtrosBBVA.getUsuario());
			Hashtable<String, String> paramTable2 = new Hashtable<String, String>();

			//paramTable2= mapperBaja(paramTable);
			paramTable2= ServerMapperUtil.mapperBaja(paramTable);
			doNetworkOperation(operacion, paramTable2, false, null, isJsonValueCode.NONE, confirmacionAutenticacionViewController);

	}
/*
	private Hashtable<String, String> mapperBaja(Hashtable<String, String> params){
		Hashtable<String, String> parameter = new Hashtable<String, String>();
		parameter.put("NT", (String) params.get(Server.USERNAME_PARAM));
		 parameter.put("NP", (String) params.get(Server.PASSWORD_PARAM));
		 parameter.put("IU", (String) params.get(ServerConstants.IUM_ETIQUETA));
		 parameter.put("TE", (String) params.get(Server.NUMERO_CLIENTE_PARAM));
		 parameter.put("DE", (String) params.get(Server.DESCRIPCION_PARAM));
		 parameter.put("CC", "");
		 parameter.put("CA", (String) params.get(Server.PAYMENT_ACCOUNT_PARAM));
		parameter.put("BF", params.get(Server.BENEFICIARIO_PARAM) != null ? (String) params.get(Server.BENEFICIARIO_PARAM) : "");
		parameter.put("CB", params.get(Server.BANK_CODE_PARAM) == null ? "" : (String) params.get(Server.BANK_CODE_PARAM));
		 parameter.put("IM", "");
		 parameter.put("RF", params.get(Server.REFERENCIA_NUMERICA_PARAM) != null ? (String) params.get(Server.REFERENCIA_NUMERICA_PARAM) : "");
		 parameter.put("CP", "");
		 parameter.put("IT", (String) params.get(Server.ID_TOKEN));
		 parameter.put("TP", (String) params.get(Server.TIPO_CONSULTA_PARAM));
		parameter.put("OA", params.get(Server.OPERADORA_PARAM) != null ? (String) params.get(Server.OPERADORA_PARAM) : "");
		 parameter.put("CN", (String) params.get(Server.ID_CANAL));
		parameter.put("US", (String) params.get(Server.USUARIO));
		 parameter.put("AP", "");
		 parameter.put("NI", (String) params.get(Server.CARD_NIP_PARAM));
		 parameter.put("CV", (String) params.get(Server.CARD_CVV2_PARAM));
		 parameter.put("OT", (String) params.get(ServerConstants.CODIGO_OTP));
		 parameter.put("VA", (String) params.get(Server.VA_PARAM));
		 return parameter;

	}
*/
	private boolean operacionRapida;
	
	public boolean isOperacionRapida() {
		return this.operacionRapida;
	}
	
	public void setOperacionRapida(boolean operacionRapida) {
		this.operacionRapida = operacionRapida;
	}
	
	public void cargarRapido(Rapido rapido) {
		operacionRapida = (null != rapido);
		if(null == rapido)
			return;
		
		esFrecuente = false;
		tipoOperacion = Operacion.transferirBancomerR;
		
		if(null == this.transferenciaOtrosBBVA)
			transferenciaOtrosBBVA = new TransferenciaOtrosBBVA();
		
		transferenciaOtrosBBVA.setBeneficiario(rapido.getBeneficiario());
		transferenciaOtrosBBVA.setImporte(Tools.formatAmountForServer(String.valueOf(rapido.getImporte())));
		transferenciaOtrosBBVA.setCuentaDestino(rapido.getCuentaDestino());
		
		Account cuenta = Tools.obtenerCuenta(rapido.getCuentaOrigen());
		if(!Constants.INVALID_ACCOUNT_NUMBER.equals(cuenta.getNumber()))
			transferenciaOtrosBBVA.setCuentaOrigen(cuenta);
		else
			transferenciaOtrosBBVA.setCuentaOrigen(Tools.obtenerCuentaEje());
		
		esExpress = Constants.EXPRESS_TYPE.equals(cuenta.getType());
		esTDC = Constants.CREDIT_TYPE.equals(cuenta.getType());
	}
	
	/**
	 * @category RegistrarOperacion
	 * @return
	 */
	public boolean registrarOperacion(){
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		Operacion operacion;
		if(esFrecuente)
			operacion = Operacion.transferirBancomerF;
		else
			operacion = Operacion.transferirBancomer;
		Autenticacion aut = Autenticacion.getInstance();
		boolean value = aut.validaRegistro(operacion, perfil);		
		if(Server.ALLOW_LOG) Log.d("RegistroOP",value+" transferirBancomerF? "+esFrecuente);
		return value;
	}
	/**
	 *  @category RegistrarOperacion
	 * @return
	 */
	protected ArrayList<Object> getDatosRegistroOp() {
		ArrayList<Object> datosConfirmacion = new ArrayList<Object>();
		ArrayList<String> registro;
		
		//Cuenta de retiro
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
		registro.add(Tools.hideAccountNumber(transferenciaOtrosBBVA.getCuentaOrigen().getNumber()));
		datosConfirmacion.add(registro);
		//Cuenta destino
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(esExpress ? R.string.transferir_otrosBBVA_destiny_express : R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta));
		registro.add(esExpress ? transferenciaOtrosBBVA.getCuentaDestino() : Tools.hideAccountNumber(transferenciaOtrosBBVA.getCuentaDestino()));
		datosConfirmacion.add(registro);
		//Beneficiario
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.common_beneficiaryLabel));
		registro.add(transferenciaOtrosBBVA.getBeneficiario().toUpperCase());
		datosConfirmacion.add(registro);
		//fecha
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_fechaoperacion));
		java.util.Date currentDate = Session.getInstance(SuiteApp.appContext).getServerDate();
		registro.add(Tools.dateToString(currentDate));
		datosConfirmacion.add(registro);
		
		return datosConfirmacion;
	}
	
	/**
	 * @category RegistrarOperacion
	 */
	@Override
	public ArrayList<Object> getDatosRegistroExitoso() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> registro;
		
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_op));
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_exito));
		tabla.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_cuenta));
		registro.add(esExpress ? transferenciaOtrosBBVA.getCuentaDestino() : Tools.hideAccountNumber(transferenciaOtrosBBVA.getCuentaDestino()));
		tabla.add(registro);
				
		return tabla;
	}
	/**
	 * @category RegistrarOperacion
	 * realiza el registro de operacion
	 * @param rovc
	 * @param token
	 */
	@Override
	protected void realizaOperacion(RegistrarOperacionViewController rovc,	String token) {

		int operationId = Server.OP_VALIDAR_CREDENCIALES;
		Hashtable<String, String> params = new Hashtable<String, String>();		
		Session session = Session.getInstance(SuiteApp.appContext);
     	String cuentaDestino = transferenciaOtrosBBVA.getCuentaDestino();

		params.put(ServerConstants.ID_OPERACION, ServerConstants.TRANSFERIR_BANCOMER);
		params.put(ServerConstants.TITULAR_CUENTA_TERCERO, transferenciaOtrosBBVA.getBeneficiario());
		params.put(ServerConstants.CUENTA_TERCERO, transferenciaOtrosBBVA.getCuentaDestino());

		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		params.put(ServerConstants.CVE_ACCESO, "" );
		params.put(Server.J_NIP, "");
		params.put(Server.J_CVV2, "");
		params.put(ServerConstants.CODIGO_OTP, token == null ? "":token);
		params.put(Server.J_AUT, "00010");
		params.put(ServerConstants.IUM, session.getIum());
		params.put(ServerConstants.VERSION, Constants.APPLICATION_VERSION);
		params.put("tarjeta5Dig", "");
		//JAIG
		doNetworkOperation(operationId, params,true,new RegistrarOperacionResult(),isJsonValueCode.NONE, rovc);
	}
	
	 /**
	  * @category RegistrarOperacion
	  * Realiza la operacion de tranferencia desde la pantalla de confirmacionRegistro
	  */
	@Override
	public void realizaOperacion(ConfirmacionRegistroViewController viewController,	String contrasenia, String nip, String token) {
		//prepare data
        Hashtable<String,String> paramTable = null;
        tempPassword = Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia;
        int operacion = 0;
		Session session = Session.getInstance(SuiteApp.appContext);
		operacion = Server.BANCOMER_TRANSFER_OPERATION;		
		String tipoOperacion = "";
		String va = Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,	session.getClientProfile(), Tools.getDoubleAmountFromServerString(transferenciaOtrosBBVA.getImporte()));
		
		paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.ID_OPERACION, ServerConstants.TRANSFERIR_BANCOMER);
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());// "NT"
		paramTable.put(ServerConstants.PARAMS_TEXTO_NP, Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia);// NP
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());// IU
		paramTable.put("CC", transferenciaOtrosBBVA.getCuentaOrigen().getFullNumber());// CC
		paramTable.put("CA", esExpress ? Constants.EXPRESS_TYPE + transferenciaOtrosBBVA.getCuentaDestino()	: transferenciaOtrosBBVA.getCuentaDestino());// CA
		paramTable.put("IM",	transferenciaOtrosBBVA.getImporte() == null ? "" : transferenciaOtrosBBVA.getImporte());// IM
		
		//System.out.println(esExpress ? Constants.EXPRESS_TYPE + transferenciaOtrosBBVA.getCuentaDestino() : transferenciaOtrosBBVA.getCuentaDestino());
		/*if(transferenciaOtrosBBVA.getCuentaDestino().length() == Constants.CHECK_ACCOUNT_NUMBER_LENGTH2){
			paramTable.put(Server.ACCOUNT_TYPE_PARAM, Constants.EXPRESS_TYPE);//TP
			paramTable.put(Server.DESTINATION_PARAM,Constants.EXPRESS_TYPE+transferenciaOtrosBBVA.getCuentaDestino());
		}
		else
			paramTable.put(Server.ACCOUNT_TYPE_PARAM, tipoOperacion);//IU
		}*/
		paramTable.put("TP", tipoOperacion);// IU
		paramTable.put("TE", session.getClientNumber());// TE
		paramTable.put("MP", transferenciaOtrosBBVA.getConcepto()); //MP
		paramTable.put("NI", "");
		paramTable.put("CV", "");
		paramTable.put("OT", Tools.isEmptyOrNull(token) ? "" : token);
		paramTable.put("VA", Tools.isEmptyOrNull(va) ? "" : va);// validar
     	//JAIG
		doNetworkOperation(operacion, paramTable,false,new TransferResult(),isJsonValueCode.NONE, viewController);
	}
	
	/**
	 * @category RegistrarOperacion
	 * Muestra la pantalla de confirmacion registro
	 */
	public void showConfirmacionRegistro() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		bMovilVC.showConfirmacionRegistro(this);		
	}
	
	@Override
	public String getNumeroCuentaParaRegistroOperacion() {
		String cuentaDestino = transferenciaOtrosBBVA.getCuentaDestino();
		return cuentaDestino.substring(cuentaDestino.length() - 5);
	}
	
	//SPEI
private boolean spei;
	
	public boolean isSpei() {
		return spei;
	}
	
	public void setSpei(boolean spei) {
		this.spei = spei;
	}
	
	public void consultThirdsAccountBBVA(String phone, String amount) {
//		if (phone.length() != Constants.CHECK_ACCOUNT_NUMBER_LENGTH) {
//			otrosBBVAViewController.showInformationAlert(R.string.transferir_otrosBBVA_invalid_account);
//			return;
//		}
		
		transferenciaOtrosBBVA.setImporte(amount);
		transferenciaOtrosBBVA.setNumeroCelular(phone);
		
        Hashtable<String,String> paramTable = new Hashtable<String, String>();
        int operacion = Server.CONSULT_BENEFICIARY_ACCOUNT_NUMBER;
        Session session = Session.getInstance(SuiteApp.appContext);
		
		paramTable.put("numeroTelefono", session.getUsername());
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		paramTable.put("telefonoDeposito", phone);
     	
		doNetworkOperation(operacion, paramTable,true,new ConsultThirdsAccountBBVAResult(),isJsonValueCode.NONE, this.otrosBBVAViewController);
	}
	
	private void analyzeConsultThirdsAccountBBVAResponse(ServerResponse response) {
		ConsultThirdsAccountBBVAResult result = (ConsultThirdsAccountBBVAResult)response.getResponse();
		
		
		String fullName = "";
		if(!Tools.isEmptyOrNull(result.getNombreB())) {
			fullName += result.getNombreB();
		}
		if(!Tools.isEmptyOrNull(result.getApellidoPaternoB())) {
			fullName += " " + result.getApellidoPaternoB();
		}
		if(!Tools.isEmptyOrNull(result.getApellidoMaternoB())) {
			fullName += " " + result.getApellidoMaternoB();
		}
		transferenciaOtrosBBVA.setBeneficiario(fullName);
		
		if(result.getTipoCuenta().equals("E")) {
			esExpress = true;
			spei = false;
			transferenciaOtrosBBVA.setTipoCuenta(Constants.EXPRESS_TYPE);
		} else {
			esExpress = false;
			spei = true;
			transferenciaOtrosBBVA.setNumeroCelular(transferenciaOtrosBBVA.getCuentaDestino());
			transferenciaOtrosBBVA.setCuentaDestino(result.getNumeroCuentaB());
			transferenciaOtrosBBVA.setTipoCuenta(Constants.SPEI_TYPE);
		}
		
		showConfirmacion();
	}

	private boolean phoneNumber = false;

	public boolean isPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(boolean phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	//public void guardaMotivo(Editable text) {
		// TODO Auto-generated method stub
//		innerTransaction.setMotivoPago(text.toString());
	//}
	
	
}
