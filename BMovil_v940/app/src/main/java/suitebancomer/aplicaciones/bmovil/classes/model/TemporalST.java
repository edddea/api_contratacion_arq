package suitebancomer.aplicaciones.bmovil.classes.model;

/**
 * Modelo para el contenido de la tabla TemporalST.
 * 
 * @author CGI
 */
public class TemporalST {

	/** El numero de celular. */
	private String celular;

	/** El numero de tarjeta. */
	private String tarjeta;

	/** La compannia telefonica. */
	private String compannia;

	/** La contrasenna. */
	private String contrasenna;

	/** El correo electronico. */
	private String correo;

	/** El perfil. */
	private String perfil;

	/**
	 * Constructor por defecto.
	 */
	public TemporalST() {
		super();
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param celular
	 *            el numero de celular
	 * @param tarjeta
	 *            el numero de tarjeta
	 * @param compannia
	 *            la compannia telefonica
	 * @param contrasenna
	 *            la contrasenna
	 * @param correo
	 *            el correo electronico
	 * @param perfil
	 *            el perfil
	 */
	public TemporalST(String celular, String tarjeta, String compannia,
			String contrasenna, String correo, String perfil) {
		super();
		this.celular = celular;
		this.tarjeta = tarjeta;
		this.compannia = compannia;
		this.contrasenna = contrasenna;
		this.correo = correo;
		this.perfil = perfil;
	}

	/**
	 * Obtiene el numero de celular.
	 * 
	 * @return el numero de celular.
	 */
	public String getCelular() {
		return celular;
	}

	/**
	 * Establece el numero de celular.
	 * 
	 * @param celular
	 *            el numero de celular a establecer
	 */
	public void setCelular(String celular) {
		this.celular = celular;
	}

	/**
	 * Obtiene el numero de tarjeta.
	 * 
	 * @return el numero de tarjeta
	 */
	public String getTarjeta() {
		return tarjeta;
	}

	/**
	 * Establece el numero de tarjeta.
	 * 
	 * @param tarjeta
	 *            el numero de tarjeta a establecer
	 */
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}

	/**
	 * Obtiene la compannia telefonica.
	 * 
	 * @return la compannia telefonica
	 */
	public String getCompannia() {
		return compannia;
	}

	/**
	 * Establece la compannia telefonica.
	 * 
	 * @param compannia
	 *            la compannia telefonica a establecer
	 */
	public void setCompannia(String compannia) {
		this.compannia = compannia;
	}

	/**
	 * Obtiene la contrasenna.
	 * 
	 * @return la contrasenna
	 */
	public String getContrasenna() {
		return contrasenna;
	}

	/**
	 * Establece la contrasenna.
	 * 
	 * @param contrasenna
	 *            la contrasenna a establecer
	 */
	public void setContrasenna(String contrasenna) {
		this.contrasenna = contrasenna;
	}

	/**
	 * Obtiene el correo electrico.
	 * 
	 * @return el correo electrico
	 */
	public String getCorreo() {
		return correo;
	}

	/**
	 * Establece el correo electrico.
	 * 
	 * @param correo
	 *            el correo electrico a establecer
	 */
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	
	/**
	 * Obtiene el perfil.
	 * 
	 * @return el perfil
	 */
	public String getPerfil() {
		return perfil;
	}

	/**
	 * Establece el perfil.
	 * 
	 * @param perfil
	 *            el perfil a establecer
	 */
	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

}