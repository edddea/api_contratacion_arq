package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import android.util.Log;



import com.bancomer.mbanking.SuiteApp;

public class CambioCuentaResult implements ParsingHandler{
	private String folio;
	private Account[] asuntos;
	
	public String getFolio() {
		return folio;
	};
	
	public Account[] getAsuntos() {
		return asuntos;
	}
	
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
	
		throw new ParsingException("Invalid process.");
		
	}
	
	@Override
public void process(ParserJSON parser) throws IOException, ParsingException {
		if(Server.ALLOW_LOG) Log.w("Change","ok");

		
		folio = parser.parseNextValue("folioArq");
		JSONArray arrayAsuntos = parser.parseNextValueWithArray("asuntos", false);
		int numAsuntos = arrayAsuntos.length();
		asuntos = new Account[numAsuntos];
		for(int i = 0;i < numAsuntos; i++ ){
		try {
			JSONObject asuntoObj = arrayAsuntos.getJSONObject(i);
			String tipoCuenta = asuntoObj.getString("tipoCuenta");
			String alias = asuntoObj.getString("alias");
			String divisa = asuntoObj.getString("divisa");
			String asunto = asuntoObj.getString("asunto");
			String saldo = asuntoObj.getString("saldo");
			String concepto = asuntoObj.getString("concepto");
			String visible = asuntoObj.getString("visible");
			/*String celularAsociado = asuntoObj.getString("celularAsociado");
			String codigoCompania = asuntoObj.getString("codigoCompania");
			String descripcionCompania = asuntoObj.getString("descripcionCompania");
			String fechaUltimaModificacion = asuntoObj.getString("fechaUltimaModificacion");
			String indicadorSPEI = asuntoObj.getString("indicadorSPEI");
			*/
			String celularAsociado =""; 
			if(asuntoObj.has("celularAsociado"))
				celularAsociado = asuntoObj.getString("celularAsociado");
			String codigoCompania ="";
			if(asuntoObj.has("ocdigoCompania"))
				codigoCompania= asuntoObj.getString("codigoCompania");
			String descripcionCompania ="";
			if(asuntoObj.has("descripcionCompania"))
				descripcionCompania= asuntoObj.getString("descripcionCompania");
			String fechaUltimaModificacion ="";
			if(asuntoObj.has("fechaUltimaModificacion"))
				fechaUltimaModificacion = asuntoObj.getString("fechaUltimaModificacion");
			String indicadorSPEI ="";
			if(asuntoObj.has("indicadorSPEI"))
				indicadorSPEI = asuntoObj.getString("indicadorSPEI");
			Session session = Session.getInstance(SuiteApp.appContext);
			String date = Tools.dateForReference(session.getServerDate());
			asuntos[i] = new Account(asunto,
					Tools.getDoubleAmountFromServerString(saldo),
					Tools.formatDate(date), "S".equals(visible), divisa,
					tipoCuenta, concepto, alias, celularAsociado, 
					codigoCompania, descripcionCompania, fechaUltimaModificacion , indicadorSPEI);
				
		} catch (JSONException e) {
			//throw new ParsingException("Error formato");
			if(Server.ALLOW_LOG) throw new ParsingException("Esto es lo que se muestra en el alert");

		}
			
		}		
	}
	
}
