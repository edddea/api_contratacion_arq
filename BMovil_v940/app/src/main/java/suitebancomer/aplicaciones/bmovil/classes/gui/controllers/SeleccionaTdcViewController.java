package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;







import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagoTdcDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirMisCuentasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelper;

public class SeleccionaTdcViewController extends BaseViewController {

	public LinearLayout vista;
	public PagoTdcDelegate delegate;
	public CuentaOrigenViewController componenteCtaOrigen;
	public ListaSeleccionViewController listaSeleccion;
	public BmovilViewsController parentManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_transfer_mis_cuentas_seleccion_view);
		setTitle(R.string.transferir_otrosBBVA_TDC_title,R.drawable.bmovil_mis_cuentas_icono);
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		
			
				
				TrackingHelper.trackState("miscuentas", parentManager.estados);
		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		
		//Toast.makeText(SuiteApp.getInstance(),"onCreate selecciona PAgo Tdc", Toast.LENGTH_LONG).show();

		
		setDelegate(parentViewsController.getBaseDelegateForKey(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID));
		delegate = (PagoTdcDelegate)getDelegate();
		if(delegate==null){
			delegate =  new PagoTdcDelegate();
			parentViewsController.addDelegateToHashMap(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID,delegate);
			setDelegate(delegate);
		}
		delegate.setPagoTdcViewController(this);
		vista = (LinearLayout)findViewById(R.id.transfer_mis_cuentas_layout);
		muestraComponenteCuentaOrigen();
		

	}
	
	@SuppressWarnings("deprecation")
	public void muestraComponenteCuentaOrigen(){
		GuiTools.getCurrent().init(getWindowManager());
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		params.topMargin = 8;
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
//		params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
		

			
			//this.showInformationAlert("No es posible realizar transferencias con una sola cuenta.");
		
		

		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
		componenteCtaOrigen.setDelegate(delegate);
		
		if(delegate.getCuentaOrigenSeleccionada() != null){
			componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(delegate.getCuentaOrigenSeleccionada()));
		}else{
			componenteCtaOrigen.setIndiceCuentaSeleccionada(0);
		}

		if (listaCuetasAMostrar.isEmpty()){
			
//			this.showInformationAlert("Aviso","error tdc", accionCerrar());
			
			
			
			/*Intent intent = new Intent(this, OpcionesPagosViewController.class);
			intent.putExtra("avisoAlert", R.string.error_cuenta_eje_credito);
			startActivity(intent);
			*/
		    
			
			
			//this.showInformationAlert("alert");
			//Toast.makeText(SuiteApp.getInstance(),"alert", Toast.LENGTH_LONG).show();

			  //this.showInformationAlert(R.string.error_cuenta_eje_credito);
		     // this.finish();
		      
		    //  Intent intent = new Intent(this, SettingsActivity.class);
		    //  startActivity(intent);
		
		}
		else {
         componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);

		componenteCtaOrigen.init();
		componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.transferir);
		vista.addView(componenteCtaOrigen);
		muestraListaCuentas();
		}
		
	}
	
//	private OnClickListener accionCerrar() {
//		
////		try {
////			Thread.sleep(3000);
////		} catch (InterruptedException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
////	     this.finish();
////		return null;
//	}

	public ArrayList<Account> agruparCuentas(){
		
        ArrayList<Account> tarjetas = new ArrayList<Account>();
        
        for(Account a : Session.getInstance(SuiteApp.appContext).getAccounts()){
      	  if (a.getType().equals(Constants.CREDIT_TYPE)) {
      		  tarjetas.add(a);
      	  }
        }
        
		return tarjetas;
	}
	
	@SuppressWarnings("deprecation")
	public void muestraListaCuentas(){
		delegate.setCuentaOrigenSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		
		ArrayList<Account> listaCuetasAMostrar = agruparCuentas();
		
		ArrayList<Object> registros;
		ArrayList<Object> lista = new ArrayList<Object>();
		ArrayList<Object> encabezado = new ArrayList<Object>();
		encabezado.add(null);
		encabezado.add(getString(R.string.transferir_lista_seleccion_tipo_cuenta));
		encabezado.add(getString(R.string.transferir_lista_seleccion_cuenta));

		if (listaCuetasAMostrar != null) {
			for (int i = 0; i < listaCuetasAMostrar.size(); i++) {
				registros = new ArrayList<Object>();
				registros.add(listaCuetasAMostrar.get(i));
				registros.add(getTipoCuenta(getResources(), listaCuetasAMostrar.get(i).getType()));
				registros.add(Tools.hideAccountNumber(listaCuetasAMostrar.get(i).getNumber()));
				lista.add(registros);
			}
		}else if (listaCuetasAMostrar == null){
			registros = new ArrayList<Object>();
			registros.add("");
			registros.add("");
			lista.add(registros);
		}
		
		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.topMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_border_margin);
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.mis_cuentas_table_side_margin);
//		params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.mis_cuentas_table_side_margin);
		params.bottomMargin = GuiTools.getCurrent().getEquivalenceFromScaledPixels( getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_border_margin));		

		if (listaSeleccion == null) {
			listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
			listaSeleccion.setDelegate(delegate);
			listaSeleccion.setNumeroColumnas(2);
			listaSeleccion.setEncabezado(encabezado);
			listaSeleccion.setTitle(getString(R.string.transferir_lista_seleccion_titulo));
			listaSeleccion.setLista(lista);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(7);
			listaSeleccion.setExisteFiltro(false);
			listaSeleccion.cargarTabla();
			vista.addView(listaSeleccion);

		}else {
			listaSeleccion.setLista(lista);
			listaSeleccion.setOpcionSeleccionada(lista.indexOf(delegate.getCuentaOrigenSeleccionada()));
			listaSeleccion.cargarTabla();
		}
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
	}
	
	public void opcionSeleccionada(Account cuenta){
		//ARR
		Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
		parentManager.showPagoTDCCuenta(cuenta, componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
				
//		if (delegate.getInnerTransaction().getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE)) {
//			if (delegate.getInnerTransaction().getCuentaOrigen().getType().equals(cuenta.getType())) {
//				this.showInformationAlert(R.string.transferir_validacion_tdc_a_tdc);
//			}else {
//				delegate.guardaTransferenciaInterna(cuenta);
//				listaSeleccion.cargarTabla();
//				delegate.showTransferMisCuentasDetalle();
//			}
//		} else {
//
//			//ARR
//			paso1OperacionMap.put("evento_paso1", "event46");
//			paso1OperacionMap.put("&&products", "operaciones;transferencias+mis cuentas");
//			paso1OperacionMap.put("eVar12", "paso1:eleccion cuenta");
//
//			TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
//			delegate.guardaTransferenciaInterna(cuenta);
//			listaSeleccion.cargarTabla();
//			delegate.showTransferMisCuentasDetalle();
//		}
	}
	
	public String getTipoCuenta(Resources res, String type)
	{
		String tipoCuenta = "";
        if (Constants.CHECK_TYPE.equals(type)) {
        	tipoCuenta = res.getString(R.string.accounttype_check);
        } else if (Constants.CREDIT_TYPE.equals(type)) {
        	tipoCuenta = res.getString(R.string.accounttype_credit);
        } else if (Constants.LIBRETON_TYPE.equals(type)) {
        	tipoCuenta = res.getString(R.string.accounttype_libreton);
        } else if (Constants.SAVINGS_TYPE.equals(type)) {
        	tipoCuenta = res.getString(R.string.accounttype_savings);
        } else if (Constants.EXPRESS_TYPE.equals(type)) {
        	tipoCuenta = res.getString(R.string.accounttype_express);
        } else if (Constants.CLABE_TYPE_ACCOUNT.equals(type)) {
        	tipoCuenta = res.getString(R.string.accounttype_clabe);
        }else if (Constants.PREPAID_TYPE.equals(type)) {
			tipoCuenta = res.getString(R.string.accounttype_prepaid);
		}
        
        return tipoCuenta;
	}
//	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		if (delegate != null) {
			delegate.setPagoTdcViewController(this);
			componenteCtaOrigen.setIndiceCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getListaCuentaOrigen().indexOf(delegate.getCuentaOrigenSeleccionada()));
			//componenteCtaOrigen.actualizaComponente();
			componenteCtaOrigen.actualizaComponente(false);
			muestraListaCuentas();
		}
	}
//	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		super.goBack();
	}
//	
	public void actualizaComponenteCtaOrigen(){
		componenteCtaOrigen.getCuentaOrigenDelegate().setCuentaSeleccionada(delegate.getCuentaOrigenSeleccionada());
		//componenteCtaOrigen.actualizaComponente();
		componenteCtaOrigen.actualizaComponente(false);

	}
//
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if(MotionEvent.ACTION_DOWN == ev.getAction()) {
			if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Touch Event Action: ACTION_DOWN");
			listaSeleccion.setMarqueeEnabled(false);
		}else if(MotionEvent.ACTION_UP == ev.getAction()) {
			if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Touch Event Action: ACTION_UP");
			listaSeleccion.setMarqueeEnabled(true);
		}
		
		return super.dispatchTouchEvent(ev);
	}
}
