package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;


import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionRegistroDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaDatosViewController;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import java.util.HashMap;
import java.util.Map;
import tracking.TrackingHelper;

public class ConfirmacionRegistroViewController extends BaseViewController implements OnClickListener{
	private LinearLayout contenedorPrincipal;
	private LinearLayout contenedorContrasena;
	private LinearLayout contenedorNIP;
	private LinearLayout contenedorASM;
	private LinearLayout contenedorCVV;	
	private TextView campoContrasena;
	private TextView campoNIP;
	private TextView campoASM;
	private TextView campoCVV;
	private EditText contrasena;
	private EditText nip;	
	private EditText asm;
	private EditText cvv;
	private TextView instruccionesContrasena;
	private TextView instruccionesNIP;
	private TextView instruccionesASM;
	private TextView instruccionesCVV;	
	private ImageButton confirmarButton;	
	private ConfirmacionRegistroDelegate delegate;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	
	
	//Nuevo Campo
//		private TextView campoTarjeta;
//		private LinearLayout contenedorCampoTarjeta;
//		//private EditText tarjeta;
//		private TextView instruccionesTarjeta;
		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_confirmacion_registro);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("confreg", parentManager.estados);

		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(getParentViewsController().getBaseDelegateForKey(ConfirmacionRegistroDelegate.CONFIRMACION_REGISTRO_DELEGATE_DELEGATE_ID));
		setTitulo();		
		delegate = (ConfirmacionRegistroDelegate)getDelegate();
		delegate.setViewController(this);
		findViews();
		scaleToScreenSize();		
		delegate.consultaDatosLista();		
		configuraPantalla();
		moverScroll();
		
		contrasena.addTextChangedListener(new BmovilTextWatcher(this));
		nip.addTextChangedListener(new BmovilTextWatcher(this));
		asm.addTextChangedListener(new BmovilTextWatcher(this));
		cvv.addTextChangedListener(new BmovilTextWatcher(this));
		//Otarjeta.addTextChangedListener(new BmovilTextWatcher(this));
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	private void configuraPantalla() {
		mostrarDatosRegistro();
		mostrarContrasena(delegate.consultaDebePedirContrasena());
		mostrarNIP(delegate.consultaDebePedirNIP());
		mostrarASM(delegate.consultaInstrumentoSeguridad());
		mostrarCVV(delegate.consultaDebePedirCVV());
		
		LinearLayout contenedorPadre = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);
		
		if (contenedorContrasena.getVisibility() == View.GONE &&
			contenedorNIP.getVisibility() == View.GONE &&
			contenedorASM.getVisibility() == View.GONE &&
			contenedorCVV.getVisibility() == View.GONE) {
			contenedorPadre.setBackgroundColor(0);			
		}		
		confirmarButton.setOnClickListener(this);
	}
	
	/**
	 * muestra los datos del registor exitoso de la operacion.
	 */
	@SuppressWarnings("deprecation")
	private void mostrarDatosRegistro(){
		ArrayList<Object> datos = delegate.getDatosRegistroExitoso();
		LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		ListaDatosViewController listaDatos = new ListaDatosViewController(this, params, parentViewsController);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.setTitulo(R.string.confirmation_subtitulo);
		listaDatos.showLista();
		LinearLayout layoutRegistro = (LinearLayout)findViewById(R.id.confirmacion_lista_datos_reg_op);
		layoutRegistro.addView(listaDatos);
		
	}
	
	/**
	 * Pone el titulo y la imagen de encabezado para la pantalla
	 */
	public void setTitulo() {
		ConfirmacionRegistroDelegate confirmacionDelegate = (ConfirmacionRegistroDelegate)getDelegate();		
		setTitle(confirmacionDelegate.getTextoEncabezado(),
				 confirmacionDelegate.getNombreImagenEncabezado());
	}
	
	@SuppressWarnings("deprecation")
	public void setListaDatos(ArrayList<Object> datos) {
		LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		ListaDatosViewController listaDatos = new ListaDatosViewController(this, params, parentViewsController);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.setTitulo(R.string.confirmation_subtitulo_op);
		listaDatos.showLista();
		LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.confirmacion_lista_datos);
		layoutListaDatos.addView(listaDatos);
	}

	/**
	 * Se encarga de hacer visible el campo de contrasena y configurarlo con el
	 * mensaje y limite de caracteres
	 * @param visibility
	 */
	public void mostrarContrasena(boolean visibility){
		contenedorContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		contrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoContrasena.setText(delegate.getEtiquetaCampoContrasenia());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
			contrasena.setFilters(userFilterArray);
			contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		instruccionesContrasena.setVisibility(View.GONE);
	}
	
	/**
	 * Se encarga de hacer visible el campo de nip y configurarlo con el
	 * mensaje y limite de caracteres 
	 */
	public void mostrarNIP(boolean visibility){
		contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		nip.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoNIP.setText(delegate.getEtiquetaCampoNip());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
			nip.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
			String instrucciones = delegate.getTextoAyudaNIP();
			if (instrucciones.equals("")) {
				instruccionesNIP.setVisibility(View.GONE);
			} else {
				instruccionesNIP.setVisibility(View.VISIBLE);
				instruccionesNIP.setText(instrucciones);
			}
		} else {
			nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}
	
	/**
	 * Se encarga de hacer visible el campo de asm y configurarlo con el
	 * mensaje y limite de caracteres 
	 */
	public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){
		switch(tipoOTP) {
			case ninguno:
				contenedorASM.setVisibility(View.GONE);
				campoASM.setVisibility(View.GONE);
				asm.setVisibility(View.GONE);
				asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
				break;
			case codigo:
			case registro:
				contenedorASM.setVisibility(View.VISIBLE);
				campoASM.setVisibility(View.VISIBLE);
				asm.setVisibility(View.VISIBLE);
				InputFilter[] userFilterArray = new InputFilter[1];
				userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
				asm.setFilters(userFilterArray);
				cambiarAccionTexto(contrasena);
				cambiarAccionTexto(nip);
				asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
		}
		
		Constants.TipoInstrumento tipoInstrumento = delegate.consultaTipoInstrumentoSeguridad();

		switch (tipoInstrumento) {
			case OCRA:
				campoASM.setText(delegate.getEtiquetaCampoOCRA());
				//asm.setTransformationMethod(null);
				break;
			case DP270:
				campoASM.setText(delegate.getEtiquetaCampoDP270());
				//asm.setTransformationMethod(null);
				break;
			case SoftToken:
				if(SuiteApp.getSofttokenStatus()) {
					asm.setText(Constants.DUMMY_OTP);
					asm.setEnabled(false);
					campoASM.setText(delegate.getEtiquetaCampoSoftokenActivado());
				} else {
					asm.setText("");
					asm.setEnabled(true);
					campoASM.setText(delegate.getEtiquetaCampoSoftokenDesactivado());
					//asm.setTransformationMethod(null);
				}
				break;
			default:
				break;
		}
		String instrucciones = delegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
		if (instrucciones.equals("")) {
			instruccionesASM.setVisibility(View.GONE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}		
	}
	
	private void cambiarAccionTexto(EditText campo) {
		if (campo.getVisibility() == View.VISIBLE) {
			campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
	}
	
	/**
	 * Si el campo es visible se obtiene el valor ingresado, de lo contrario retorna empty
	 * @return valor de la contrasena.
	 */
	public String pideContrasena() {
		if (contrasena.getVisibility() == View.GONE) {
			return "";
		} else {
			return contrasena.getText().toString();
		}
	}
	
	/**
	 * Si el campo es visible se obtiene el valor ingresado, de lo contrario retorna empty
	 * @return valor del nip
	 */
	public String pideNIP() {
		if (nip.getVisibility() == View.GONE) {
			return "";
		} else {
			return nip.getText().toString();
		}
	}
	
	/**
	 * 
	 * @return el valor de asm si es visible o empty
	 */
	public String pideASM() {
		if (asm.getVisibility() == View.GONE) {
			return "";
		} else {
			return asm.getText().toString();
		}
	}
	
	/**
	 * 
	 * @return el valor de cvv si es visible o empty
	 */
	public String pideCVV() {
		if (cvv.getVisibility() == View.GONE) {
			return "";
		} else {
			return cvv.getText().toString();
		}
	}
	
	/**
	 * 
	 * @param visibility
	 */
	public void mostrarCVV(boolean visibility){
		contenedorCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		cvv.setVisibility(visibility ? View.VISIBLE:View.GONE);
		instruccionesCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		
		if (visibility) {
			campoCVV.setText(delegate.getEtiquetaCampoCVV());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
			cvv.setFilters(userFilterArray);
			String instrucciones = delegate.getTextoAyudaCVV();
			instruccionesCVV.setText(instrucciones);
			instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(nip);
			cambiarAccionTexto(asm);
			cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}

	@Override
	public void onClick(View v) {
		if (v == confirmarButton && !parentViewsController.isActivityChanging()) {
			botonConfirmarClick();
		}
	}
	
	/**
	 * Se confirma la operacion y se hace la peticion
	 */
	public void botonConfirmarClick() {
		if(delegate.validaDatos()){
			String contrasena = pideContrasena();
			String nip = pideNIP();
			String asm = pideASM();
			String cvv = pideCVV();
			delegate.enviaPeticionOperacion(contrasena, nip, asm, cvv);
			//ARR
			
			Map<String,Object> operacionRealizadaMap = new HashMap<String, Object>();

			//Comprobacion de titulos
			
			if(getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_miscuentas))
			{
				//ARR
				operacionRealizadaMap.put("evento_realizada", "event52");
				operacionRealizadaMap.put("&&products", "operaciones;transferencias+mis cuentas");
				operacionRealizadaMap.put("eVar12", "operacion realizada");

				TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
			}
			else if(getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrascuentasbbva))
			{
				//ARR
				operacionRealizadaMap.put("evento_realizada", "event52");
				operacionRealizadaMap.put("&&products", "operaciones;transferencias+otra cuenta bbva bancomer");
				operacionRealizadaMap.put("eVar12", "operacion realizada");

				TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
			}
			else if(getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_cuentaexpress))
			{
				//ARR
				operacionRealizadaMap.put("evento_realizada", "event52");
				operacionRealizadaMap.put("&&products", "operaciones;transferencias+cuenta express");
				operacionRealizadaMap.put("eVar12", "operacion realizada");

				TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
			}
			else if(getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrosbancos))
			{
				//ARR
				operacionRealizadaMap.put("evento_realizada", "event52");
				operacionRealizadaMap.put("&&products", "operaciones;transferencias+otros bancos");
				operacionRealizadaMap.put("eVar12", "operacion realizada");

				TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
			}
			else if(getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_dineromovil))
			{
				//ARR
				operacionRealizadaMap.put("evento_realizada", "event52");
				operacionRealizadaMap.put("&&products", "operaciones;transferencias+dinero movil");
				operacionRealizadaMap.put("eVar12", "operacion realizada");

				TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
			}
			else if(getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.transferir_otrosBBVA_TDC_title))
			{
				//ARR
				operacionRealizadaMap.put("evento_realizada", "event52");
				operacionRealizadaMap.put("&&products", "operaciones;pagar+tarjeta credito");
				operacionRealizadaMap.put("eVar12", "operacion realizada");

				TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
			}
			else if(getString(delegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.tiempo_aire_title))
			{
				//ARR
				operacionRealizadaMap.put("evento_realizada", "event52");
				operacionRealizadaMap.put("&&products", "operaciones;comprar+tiempo aire");
				operacionRealizadaMap.put("eVar12", "operacion realizada");

				TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
			}
		}
	} 
		
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}
	
	private void findViews() {
		contenedorPrincipal		= (LinearLayout)findViewById(R.id.confirmacion_lista_datos);
		contenedorContrasena	= (LinearLayout)findViewById(R.id.campo_confirmacion_contrasena_layout);
		contenedorNIP 			= (LinearLayout)findViewById(R.id.campo_confirmacion_nip_layout);
		contenedorASM 			= (LinearLayout)findViewById(R.id.campo_confirmacion_asm_layout);
		contenedorCVV 			= (LinearLayout)findViewById(R.id.campo_confirmacion_cvv_layout);
		
		contrasena 				= (EditText)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
		nip 					= (EditText)contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
		asm						= (EditText)contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
		cvv						= (EditText)contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);
		
		campoContrasena			= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_label);
		campoNIP				= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_label);
		campoASM				= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_label);
		campoCVV				= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_label);
		
		
		instruccionesContrasena	= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
		instruccionesNIP		= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
		instruccionesASM		= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
		instruccionesCVV		= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);
		
		confirmarButton 		= (ImageButton)findViewById(R.id.confirmacion_confirmar_button);		
	}
	
	private void scaleToScreenSize() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(contenedorPrincipal);
		guiTools.scale(findViewById(R.id.confirmacion_campos_layout));
		
		guiTools.scale(contenedorContrasena);
		guiTools.scale(contenedorNIP);
		guiTools.scale(contenedorASM);
		guiTools.scale(contenedorCVV);
		
		guiTools.scale(contrasena, true);
		guiTools.scale(nip, true);
		guiTools.scale(asm, true);
		guiTools.scale(cvv, true);
		
		guiTools.scale(campoContrasena, true);
		guiTools.scale(campoNIP, true);
		guiTools.scale(campoASM, true);
		guiTools.scale(campoCVV, true);
		
		guiTools.scale(instruccionesContrasena, true);
		guiTools.scale(instruccionesNIP, true);
		guiTools.scale(instruccionesASM, true);
		guiTools.scale(instruccionesCVV, true);
		
		guiTools.scale(confirmarButton);
		
	}
	
	public void limpiarCampos(){
		contrasena.setText("");
		nip.setText("");
		asm.setText("");
		cvv.setText("");
	}
}