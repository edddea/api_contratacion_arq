package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Clase para parsear el resultado de la operacion Desbloqueo
 * @author Francisco.Garcia
 *
 */
@SuppressWarnings("serial")
public class DesbloqueoResult implements ParsingHandler {

	private String folio;
	
	/**
	 * 
	 * @return Folio de la arquitectura que devuelve la trasacci�n
	 */
	public String getFolio() {
		return folio;
	}
	
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		folio = parser.parseNextValue("folioArq");

	}

}
