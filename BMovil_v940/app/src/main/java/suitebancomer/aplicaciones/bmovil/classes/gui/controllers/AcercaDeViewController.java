/**
 * 
 */
package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuAdministrarDelegate;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import android.os.Bundle;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import tracking.TrackingHelper;


/**
 * @author Francisco.Garcia
 *
 */
public class AcercaDeViewController extends BaseViewController {
	
	MenuAdministrarDelegate menuAdministrarDelegate;
	TextView lblVersion;
	TextView lblIdentificador;
	TextView lblTerminal;
	//AMZ
			private BmovilViewsController parentManager;
		
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_acerca_de);
		setTitle(R.string.administrar_acercade_titulo, R.drawable.bmovil_acercade_icono);	
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();						
				TrackingHelper.trackState("acerca", parentManager.estados);
		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID));
		menuAdministrarDelegate = (MenuAdministrarDelegate)getDelegate(); 
		menuAdministrarDelegate.setAcercaDeViewController(this);
		lblVersion = (TextView)findViewById(R.id.acerca_de_version_value_label);
		lblIdentificador = (TextView)findViewById(R.id.acerca_de_identificador_value_label);
		lblTerminal = (TextView)findViewById(R.id.acerca_de_terminal_value_label);
		showDescripcion();
		configurarPantalla();
	}
	
	private void configurarPantalla(){
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(lblVersion,true);
		gTools.scale(lblIdentificador,true);
		gTools.scale(lblTerminal,true);
		gTools.scale(findViewById(R.id.acerca_de_legal_label),true);
		gTools.scale(findViewById(R.id.acerca_de_info_sistema_label),true);		
	}
	
	/*
	 * Estos de cajon! :|  
	 * */
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
		super.goBack();
	}
	
	/**
	 * 
	 */
	private void showDescripcion() {
		// TODO Auto-generated method stub
		menuAdministrarDelegate.inicializaAcercaDe();
		
	}
	
	/**
	 * 
	 */
	public TextView getLblVersion() {
		return lblVersion;
	}

	
	/**
	 * 
	 */
	public TextView getLblIdentificador() {
		return lblIdentificador;
	}
	
	/**
	 * 
	 */
	public TextView getLblTerminal() {
		return lblTerminal;
	}

}
