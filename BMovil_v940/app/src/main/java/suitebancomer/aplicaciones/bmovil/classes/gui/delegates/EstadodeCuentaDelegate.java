package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Hashtable;

import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.EstadodeCuentaViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaEC;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaECExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaECResult;
import suitebancomer.classes.gui.controllers.BaseViewController;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.R;

public class EstadodeCuentaDelegate extends DelegateBaseAutenticacion{

	public final static long ESTADODECUENTA_DELEGATE_ID = 9022323031661701077L;
	protected SuiteApp suiteApp = null;
        
	Account cuentaActual;
	EstadodeCuentaViewController estadodecuentaViewController;
	ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController;
	ConsultaECExtract totalPeriodos;
	ConsultaEC consultaEC;
	String pdfUrl;
	
	public boolean res = false;
	
	private int indicePeriodoSeleccionado;
	
	
	//NEW CODE
	public void setConfirmacionAutenticacionViewController(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController)
	{
	    this.confirmacionAutenticacionViewController=confirmacionAutenticacionViewController;
	}
	
	public ConsultaECExtract getTotalPeriodos() {
		return totalPeriodos;
	}
	
	public EstadodeCuentaViewController getEstadodeCuentaViewController() {
		return estadodecuentaViewController;
	}

	public void setEstadodeCuentaViewController(EstadodeCuentaViewController estadodecuentaViewController) {
		this.estadodecuentaViewController = estadodecuentaViewController;
	}
	
	public Account getCuentaSeleccionada(){
		return estadodecuentaViewController.getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada();
	}
	
	/**
	 * The owner view controller.
	 */
	private BaseViewController ownerController;
	
	/**
	 * @return The view controller associated to this delegate.
	 */
	public BaseViewController getCallerController() {
		return ownerController;
	}

	/**
	 * @param callerController The view controller to associate to this delegate.
	 */
	public void setCallerController(BaseViewController callerController) {
		this.ownerController = callerController;
	}
	
	public ConsultaEC getconsultaEC() {
		return consultaEC;
	}

	public void setConsultaEC(ConsultaEC consultaEC) {
		this.consultaEC = consultaEC;
	}

	@Override
	public void performAction(Object obj) {
		if (obj instanceof Account) {
		    estadodecuentaViewController.borrarListaPeriodos();
		    cuentaActual = getCuentaSeleccionada();
		    cargarPeriodos();
		}

	}
	
	public int getIndicePeriodoSeleccionado() {
		return indicePeriodoSeleccionado;
	}
	

	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada según sea requerido.
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();		
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		
		if(profile ==  Constants.Perfil.avanzado ){
		 
			for(Account acc : accounts) {
				if(acc.isVisible()) {
					accountsArray.add(acc);
					break;
				}
			}
			
			for(Account acc : accounts) {
				if(!acc.isVisible())
					accountsArray.add(acc);
			}

		}else{
			accountsArray.add(Tools.obtenerCuentaEje());
		}
		return accountsArray;
	}
	
	/**
	 * Carga los periodos
	 * 
	 */
	public void cargarPeriodos() {
		int operationId = Server.OBTENER_PERIODO_EC;
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put("numeroCelular", session.getUsername());
		//params.put("numeroCuenta",getCuentaSeleccionada().getNumber());
		String cuentaR = getCuentaSeleccionada().getNumber();
		params.put("numeroCuenta",cuentaR.substring(cuentaR.length()-10)); /***AMBIENTACION***/
		params.put("IUM", session.getIum());
		//JAIG
		doNetworkOperation(operationId, params,true, new ConsultaECExtract(), Server.isJsonValueCode.PAPERLESS, estadodecuentaViewController);
	}
	
	public void doNetworkOperation(int operationId, Hashtable<String, ?> params,boolean isJson, ParsingHandler handler, Server.isJsonValueCode isJsonValueCode, BaseViewController caller) {
		((BmovilViewsController) estadodecuentaViewController
				.getParentViewsController()).getBmovilApp()
				.invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller);
	}
	
	
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
		    if(response.getResponse() instanceof ConsultaECExtract){
		    	totalPeriodos = (ConsultaECExtract) response.getResponse();
		    	Log.e("tamano", ""+totalPeriodos.getPeriodos().size());
		    	estadodecuentaViewController.llenaListaDatos();			
		    }else if (response.getResponse() instanceof ConsultaECResult) {
		    	ConsultaECResult data = (ConsultaECResult) response.getResponse();
		    	//showPdf(Tools.DecodeBase64(data.getPdf()));
		    	showPdf(data.getPdf());
		    }
			
		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING || 
				response.getStatus() == ServerResponse.OPERATION_ERROR) {
			totalPeriodos = null; 
			estadodecuentaViewController.showInformationAlertEspecial(estadodecuentaViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
		}
	}
	
	/*
	public void validarDatos() {
	    
		if (((EstadodeCuentaViewController) ownerController).comboPeriodo
				.getText().toString().equals("")) {
			((EstadodeCuentaViewController) ownerController)
					.showInformationAlert(R.string.bmovil_Estado_de_cuenta_periodoInvalido);
		} else {
			if (!ownerController.isHabilitado())
				return;
			ownerController.setHabilitado(false);
					
			/*consultaEC.setPeriodo(((EstadodeCuentaViewController) ownerController).comboPeriodo
							.getText().toString());*/
			/*
			String texto = ((EstadodeCuentaViewController) ownerController).comboPeriodo.getText().toString();
			consultaEC.setPeriodo(texto.substring(0,texto.indexOf("-")-1));
			consultaEC.setFechaCorte(texto.substring(texto.indexOf("-")+2));
			
			showConfirmacion();
			
			res = true;
		}
	} */
	
	public boolean validarDatos() {
	    
		if (((EstadodeCuentaViewController) ownerController).comboPeriodo
				.getText().toString().equals("")) {
			((EstadodeCuentaViewController) ownerController)
					.showInformationAlert(R.string.bmovil_Estado_de_cuenta_periodoInvalido);
			return false;
		} else if (!ownerController.isHabilitado())
				return false;
		else {
			res = true;
			return true;
		}
	} 
	
	@SuppressLint("NewApi")
	public void showPdf(String pdfData){
		byte[] data = Base64.decode(pdfData, Base64.DEFAULT);
		File edoctaFile = new File(Environment.getExternalStorageDirectory().toString() + "/edocta.pdf");
		OutputStream out = null;
		   try {
		     out = new BufferedOutputStream(new FileOutputStream(edoctaFile));
			 out.write(data);
		   } catch (Exception e) {
			   if(Server.ALLOW_LOG) Log.d("FILE", Log.getStackTraceString(e));
		}finally {
		     if (out != null) {
		       try {
				out.close();
			} catch (IOException e) {
				   if(Server.ALLOW_LOG) Log.d("CLOSE", Log.getStackTraceString(e));
			}
		     }
		   }
		
		Uri path = Uri.fromFile(edoctaFile);
		Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
		//pdfIntent.setData(path);
		//Log.d("URI",path.toString());
		pdfIntent.setDataAndType(path, "application/pdf");
		//pdfIntent.setType("application/pdf");
		pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

		try{
			estadodecuentaViewController.startActivity(pdfIntent);
		}catch(ActivityNotFoundException e){
			Toast.makeText(ownerController, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
	    }catch(Exception e){
			if(Server.ALLOW_LOG) Log.d("ACT",Log.getStackTraceString(e));
	    }
	}
	
	
	

	/**
	 * Obtiene el texto para el encabezado de la confirmacion
	 */
	@Override
	public int getTextoEncabezado() {
		int textoEncabezado = 0;
		
		textoEncabezado = R.string.consultar_estados_de_cuenta_title;
		
		return textoEncabezado;
	}
	
	/**
	 * Obtiene la imagen para mostrar encabezado
	 */
	@Override
	public int getNombreImagenEncabezado() {
		int imgEncabezado = 0;
		
		imgEncabezado = R.drawable.bmovil_consultar_icono;
		
		return imgEncabezado;
	}
	
	public int getColorTituloResultado() {
		int colorRecurso = 0;
		
		colorRecurso = R.color.verde_bmovil_detalle;
		
		return colorRecurso;
	}
	
	public void showConfirmacion() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		if (registrarOperacion())
			bMovilVC.showRegistrarOperacion(this);
		else
			bMovilVC.showConfirmacionAutenticacionViewController(this,
				getNombreImagenEncabezado(), 
				getTextoEncabezado(),
				R.string.confirmation_subtitulo, getColorTituloResultado());
	}
	
	/**
	 * @category RegistrarOperacion
	 * @return
	 */
	public boolean registrarOperacion() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext)
				.getClientProfile();
		Operacion operacion;
			operacion = Operacion.consultarEstadoCuenta;
		
		Autenticacion aut = Autenticacion.getInstance();
		boolean value = aut.validaRegistro(operacion, perfil);
		if(Server.ALLOW_LOG) Log.d("RegistroOP", value + " consultarEstadoCuenta? " );
		return value;
	}
	/**
	 * @return true si se debe mostrar contrasena, false en caso contrario.
	 */
	@Override
	
	public boolean mostrarContrasenia() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile(); 
		boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.consultarEstadoCuenta,
				perfil);
		
		Log.e("mostrarContrasenia()",String.valueOf(value));
		return value;
	}
	
	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile(); 
		boolean value =  Autenticacion.getInstance().mostrarCVV(Constants.Operacion.consultarEstadoCuenta, perfil);
		Log.e("mostrarCVV()",String.valueOf(value));
		return value;
	}
	
	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.consultarEstadoCuenta, 
				perfil);
		Log.e("mostrarNIP()",String.valueOf(value));
		return value;
	}
	
	@Override
	public boolean mostrarCampoTarjeta() {
		return (mostrarCVV() || mostrarNIP());
	}
	
	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
	    Perfil perfil = Session.getInstance(SuiteApp.appContext)
			.getClientProfile();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
		    tipoOTP = Autenticacion.getInstance().tokenAMostrar(Operacion.consultarEstadoCuenta,perfil);
			
		} catch (Exception ex) {
			Log.e(this.getClass().getName(),
					"Error on Autenticacion.mostrarNIP execution.", ex);
			tipoOTP = null;
		}
		
		return tipoOTP;
	}
	
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {

		ArrayList<Object> datosConfirmacion = new ArrayList<Object>();
		ArrayList<String> registro;	
		
		registro = new ArrayList<String>();
		registro.add(ownerController
				.getString(R.string.bmovil_Estado_de_cuenta_lblCuenta));
		registro.add(Tools.hideAccountNumber(consultaEC
				.getCuentaOrigen().getNumber()));
		datosConfirmacion.add(registro);

		registro = new ArrayList<String>();
		registro.add(ownerController
				.getString(R.string.bmovil_Estado_de_cuenta_lblPeriodo));
		registro.add(consultaEC.getPeriodo());
		datosConfirmacion.add(registro);
		
		/*
		registro = new ArrayList<String>();
		registro.add(ownerController
				.getString(R.string.bmovil_Estado_de_cuenta_lblCorreo));
		registro.add(consultaEC.geteMail());
		datosConfirmacion.add(registro);
		*/

	return datosConfirmacion;
}
/*	public ArrayList<Object> getDatosTablaResultados(){
		
		ArrayList<Object> tabla = new ArrayList<Object>();
		
		ArrayList<String> fila1;		
		fila1 = new ArrayList<String>();
		fila1.add(ownerController.getString(R.string.bmovil_Estado_de_cuenta_lblCuenta));
		fila1.add(Tools.hideAccountNumber(consultaEC.getCuentaOrigen().getNumber()));
		
		ArrayList<String> fila2;
		fila2 = new ArrayList<String>();
		fila2.add(ownerController.getString(R.string.bmovil_Estado_de_cuenta_lblPeriodo));
		fila2.add(consultaEC.getPeriodo());
		
		ArrayList<String> fila3;
		fila3 = new ArrayList<String>();
		fila3.add(ownerController.getString(R.string.bmovil_Estado_de_cuenta_lblCorreo));
		fila3.add(consultaEC.geteMail());
		
		ArrayList<String> fila4;		
		fila4 = new ArrayList<String>();
		fila4.add(ownerController.getString(R.string.bmovil_result_fecha));
		String fecha = Tools.formatDate(ConsultaECResult.getFecha());
		fila4.add(fecha);
		
		ArrayList<String> fila5;		
		fila5 = new ArrayList<String>();
		fila5.add(ownerController.getString(R.string.bmovil_result_hora));
		String hora = Tools.parsetime(ConsultaECResult.getHora());
		fila5.add(hora);
		
		ArrayList<String> fila6;		
		fila6 = new ArrayList<String>();
		fila6.add(ownerController.getString(R.string.bmovil_result_folio));
		String folio = ConsultaECResult.getFolio();
		fila6.add(folio);

		tabla.add(fila1);
		tabla.add(fila2);
		tabla.add(fila3);
		tabla.add(fila4);
		tabla.add(fila5);
		tabla.add(fila6);

		return tabla;
	}*/
	
	
	/**
	 * Invoka la conexion al server para consultar el estado de cuenta
	 * 
	 */
	public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAut,String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
		Session session = Session.getInstance(SuiteApp.appContext);
		Autenticacion aut = Autenticacion.getInstance();
		Perfil perfil = session.getClientProfile();
		
		String cadAutenticacion =  aut.getCadenaAutenticacion(Constants.Operacion.consultarEstadoCuenta, perfil);
		
		int operationId = Server.CONSULTAR_ESTADO_CUENTA;
		Hashtable<String, String> params = new Hashtable<String, String>();
		
		String cuentaR = getCuentaSeleccionada().getNumber();
		params.put("numeroCuenta",cuentaR.substring(cuentaR.length()-10)); /***AMBIENTACION***/
		params.put(ServerConstants.PERIODO,consultaEC.getPeriodo());
		params.put(ServerConstants.REFERENCIA ,consultaEC.getReferencia());		
		params.put(ServerConstants.EMAIL,  session.getEmail());
		params.put(ServerConstants.FECHA_CORTE,  consultaEC.getFechaCorte());		
		params.put(ServerConstants.NUMERO_CELULAR, session.getUsername() );
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		
		
			
		
		/**VERIFICAR VALIDAR 10/06**/
		
		params.put(ServerConstants.CADENA_AUTENTICACION, "00100"); //validar
		params.put(ServerConstants.CVE_ACCESO, contrasenia);
		params.put(ServerConstants.CODIGO_NIP, nip);
		params.put(ServerConstants.CODIGO_OTP, token);
		params.put(ServerConstants.TARJETA_5DIG, "¿?");
		params.put(ServerConstants.CODIGO_CVV2, cvv);
		
		
		/**VERIFICAR VALIDAR 10/06**/
		
		params.put(ServerConstants.CVE_ACCESO, contrasenia == null ? "" : contrasenia);			
		params.put(Server.J_NIP, nip == null ? "" : nip);
		params.put(Server.J_CVV2, cvv== null ? "" : cvv);
		params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
		params.put(Server.J_AUT, cadAutenticacion);	
		params.put("tarjeta5Dig", campoTarjeta == null ? "" : campoTarjeta);
		
		/*		
		//params.put(ServerConstants.CADENA_AUTENTICACION, cadAutenticacion); //validar
		Log.e("cadAutenticacion",cadAutenticacion);	
		Log.e("numeroCuenta",cuentaR.substring(cuentaR.length()-10));	
		Log.e("PERIODO",consultaEC.getPeriodo());	
		//Log.e("REFERENCIA",consultaEC.getReferencia());	
		Log.e("EMAIL",session.getEmail());	
		//Log.e("FECHA_CORTE",consultaEC.getFechaCorte());	
		Log.e("NUMERO_CELULAR",session.getUsername());	
		Log.e("JSON_IUM_ETIQUETA",session.getIum());
		Log.e("CVE_ACCESO",contrasenia == null ? "no hay" : contrasenia);	
		Log.e("J_NIP",nip == null ? "no hay" : nip);	
		Log.e("J_CVV2",cvv== null ? "no hay" : cvv);	
		Log.e("CODIGO_OTP",token == null ? "no hay" : token);	
		Log.e("J_AUT",cadAutenticacion);	
		Log.e("tarjeta5Dig",campoTarjeta == null ? "no hay" : campoTarjeta);	
		*/
		//JAIG
		doNetworkOperation(operationId, params,true, new ConsultaECResult(), Server.isJsonValueCode.PAPERLESS, confirmacionAut);
	}
	
		
	public String getTextoTituloResultado() {

	    String textoTituloResultado = SuiteApp.appContext.getString(R.string.bmovil_common_resultado_operacion_exitosa);	
	    return textoTituloResultado;
	    }
	
	public String getTextoEspecialResultados() {

	    String textoEspecialResultados = SuiteApp.appContext.getString(R.string.bmovil_Estado_de_cuenta_lbltextoEspecial);	
	    return textoEspecialResultados;
	    }
	
//	private void showResultados(){
//		BmovilViewsController bmovilParentController = ((BmovilViewsController) estadodecuentaViewController.getParentViewsController());
//		bmovilParentController.showResultadosViewController(this, getNombreImagenEncabezado(), getTextoEncabezado());
//	}
	
	@Override
	public int getOpcionesMenuResultados() {
	// TODO Auto-generated method stub
	return SHOW_MENU_EMAIL;
	}
	
	
}
