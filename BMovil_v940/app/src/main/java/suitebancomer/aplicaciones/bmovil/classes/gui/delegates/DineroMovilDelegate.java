package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;


import android.util.Log;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.model.CatalogoVersionado;
import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerMapperUtil;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionRegistroViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultarDineroMovilViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultarFrecuentesViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.DineroMovilViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.RegistrarOperacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.BajaDineroMovilData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDineroMovil;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDineroMovilData;
import suitebancomer.aplicaciones.bmovil.classes.model.FrecuenteMulticanalResult;
import suitebancomer.aplicaciones.bmovil.classes.model.Payment;
import suitebancomer.aplicaciones.bmovil.classes.model.PaymentExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.Rapido;
import suitebancomer.aplicaciones.bmovil.classes.model.RegistrarOperacionResult;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovil;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovilData;
import suitebancomer.classes.gui.controllers.BaseViewController;
import tracking.TrackingHelper;

public class DineroMovilDelegate extends DelegateBaseAutenticacion {
	/**
	 * Identificador del delegado.
	 */
	public static final long DINERO_MOVIL_DELEGATE_ID = 0x0abc8cabc5b0b3d3L;
	
	/**
	 * Modelo de transferencia de dinero m�vil.
	 */
	private TransferenciaDineroMovil transferencia;
	
	/**
	 * El controlador actual de la pantalla.
	 */
	private BaseViewController viewController;
	
	/**
	 * Bandera que indica si se esta operando con un frecuente.
	 */
	private boolean esFrecuente;

	/**
	 * Bandera que indica si se esta realizando una baja de frecuente.
	 */
	private boolean bajaFrecuente;
	
	protected SolicitarAlertasData sa;
	

	/**
	 * @return the esFrecuente
	 */
	public boolean esFrecuente() {
		return esFrecuente;
	}
	
	/**
	 * @param esFrecuente the esFrecuente to set
	 */
	public void esFrecuente(boolean esFrecuente) {
		bajaFrecuente = false;
		this.esFrecuente = esFrecuente;
	}
	
	/**
	 * @return El controlador de la pantalla.
	 */
	public BaseViewController getViewController() {
		return viewController;
	}

	/**
	 * @param viewController El controlador de la pantalla a establecer.
	 */
	public void setViewController(BaseViewController viewController) {
		this.viewController = viewController;
	}
	
	/**
	 * @return Los datos de la tranaferencia.
	 */
	public TransferenciaDineroMovil getTransferencia() {
		if(transferencia == null)
			transferencia = new TransferenciaDineroMovil();
		return transferencia;
	}

	/**
	 * @param transferencia Los datos de la transferencia a establecer.
	 */
	public void setTransferencia(TransferenciaDineroMovil transferencia) {
		this.transferencia = transferencia;
		//frecuente correo electronico
		aliasFrecuente = null;
		correoFrecuente = null;
	}

	/**
	 * Constructor por defecto
	 */
	public DineroMovilDelegate() {
		operacionRapida = false;
		this.tipoOperacion = Constants.Operacion.dineroMovil;
		transferencia = new TransferenciaDineroMovil();
	}

	public boolean isBajaFrecuente() {
		return bajaFrecuente;
	}

	public void setBajaFrecuente(boolean bajaFrecuente) {
		this.bajaFrecuente = bajaFrecuente;
	}

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#performAction(java.lang.Object)
	 */
	@Override
	public void performAction(Object obj) {
		if(obj instanceof Compania) {
			if(viewController instanceof DineroMovilViewController) {
				DineroMovilViewController controller = (DineroMovilViewController)viewController;
				controller.limpiarImportes();
			}	
		} else if(viewController instanceof ConsultarDineroMovilViewController) {
			ConsultarDineroMovilViewController controller = (ConsultarDineroMovilViewController)viewController;
			movimientoSeleccionadoDM = controller.getListaEnvios().getOpcionSeleccionada();
			if(Server.ALLOW_LOG) Log.e("DineroMovilDelegate", "Seleccionado el movimiento en la posición: " + movimientosDM);

			//AMZ inicio
			Map<String,Object> ConsultaRealizadaMap = new HashMap<String, Object>();
			
			ConsultaRealizadaMap.put("evento_realizada","event52");
			ConsultaRealizadaMap.put("&&products","consulta;dinero movil");
			ConsultaRealizadaMap.put("eVar12","operacion realizada");
			TrackingHelper.trackConsultaRealizada(ConsultaRealizadaMap);
		
		//AMZ fin
			showDetallesMovimientoDM();
		}else if (viewController instanceof DineroMovilViewController) {
			((DineroMovilViewController)viewController).reactivarCtaOrigen((Account) obj);
		}
	}
	
	/**
	 * Obtiene el catálogo de importes para la compa�ia especificada.
	 * @param identificadorCompania Identificador de la compa�ia a buscar.
	 * @return La lista de importes.
	 */
	public String[] obtenerCatalogoDeImportes(String identificadorCompania) {
		ArrayList<String> importes = new ArrayList<String>();
		
		CatalogoVersionado catalogoDineroMovil = Session.getInstance(SuiteApp.appContext).getCatalogoDineroMovil();
		Vector<Object> companias = catalogoDineroMovil.getObjetos();
		for (Object comp : companias) {
			Compania compania = (Compania)comp;
			if(identificadorCompania == compania.getNombre()) {
				for (int monto : compania.getMontosValidos()) {
					importes.add(String.valueOf(monto)); 
				}
				break;
			}
		}
		importes.add(SuiteApp.appContext.getResources().getString(R.string.transferir_dineromovil_datos_importe_otro));
		
		return importes.toArray(new String[importes.size()]);
	}
	
	/**
	 * Carga la lista de convenios para las transferencias de dinero m�vil.
	 * @return La lista de Convenios.
	 */
	public ArrayList<Object> cargarListaCompanias() {
		ArrayList<Object> listaCompanias = new ArrayList<Object>();
		CatalogoVersionado catalogoDineroMovil = Session.getInstance(SuiteApp.appContext).getCatalogoDineroMovil();
		for (Object comp : catalogoDineroMovil.getObjetos()) {
			listaCompanias.add((Compania)comp);
		}
		return listaCompanias;
	}
	
	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada seg�n sea reguerido.
	 * @return Lista de cuentas para el componente CuentaOrigen.
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		
		if(profile == Constants.Perfil.avanzado) {
			for(Account acc : accounts) {
				if(acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE)) {
					accountsArray.add(acc);
					break;
				}
			}
			for(Account acc : accounts) {
				if(!acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE))
					accountsArray.add(acc);
			}
		} else {
			for(Account acc : accounts) {
				if(acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE))
					accountsArray.add(acc);
			}
		}
		
		return accountsArray;
	}
	

	@Override
	public boolean mostrarContrasenia() {
		boolean value = false;
		
		if(esBajaDM || bajaFrecuente)
			value = Autenticacion.getInstance().mostrarContrasena(this.tipoOperacion, 
																  Session.getInstance(SuiteApp.appContext).getClientProfile());
		else
			value = Autenticacion.getInstance().mostrarContrasena(this.tipoOperacion, 
																  Session.getInstance(SuiteApp.appContext).getClientProfile(),
																  Tools.getDoubleAmountFromServerString(transferencia.getImporte()));
		return value;
	}

	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			if(esBajaDM)
				tipoOTP = Autenticacion.getInstance().tokenAMostrar(this.tipoOperacion,
						Session.getInstance(SuiteApp.appContext).getClientProfile());
			else
				tipoOTP = Autenticacion.getInstance().tokenAMostrar(this.tipoOperacion,
						Session.getInstance(SuiteApp.appContext).getClientProfile(),
						Tools.getDoubleAmountFromServerString(transferencia.getImporte()));
		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.mostrarNIP execution.", ex);
			tipoOTP = null;
		}
		
		return tipoOTP;
	}

	@Override
	public boolean mostrarNIP() {
		boolean value;
		
		if(esBajaDM)
			value = Autenticacion.getInstance().mostrarNIP(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile());
		else
			value = Autenticacion.getInstance().mostrarNIP(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile(),
					Tools.getDoubleAmountFromServerString(transferencia.getImporte()));
		return value;
	}
	
	@Override
	public boolean mostrarCVV() {
		boolean value;
		
		if(esBajaDM)
			value = Autenticacion.getInstance().mostrarCVV(this.tipoOperacion, 
																  Session.getInstance(SuiteApp.appContext).getClientProfile());
		else
			value = Autenticacion.getInstance().mostrarCVV(this.tipoOperacion, 
																  Session.getInstance(SuiteApp.appContext).getClientProfile(),
																  Tools.getDoubleAmountFromServerString(transferencia.getImporte()));
		return value;
	}
	
	/**
	 * Valida los datos para la tranferencia.
	 * @param numeroTelefono
	 * @param confirmaTelefono
	 * @param importe
	 */
	public void validaDatos(String telefono, String confirmacion, String importe, String otroImporte, String beneficiario) {
		double monto; 
		double otroMonto;
		DineroMovilViewController controller = null;
		
		try {
			controller = (DineroMovilViewController)viewController;
		} catch (ClassCastException ex) {
			if(Server.ALLOW_LOG) Log.e("DineroMovilDelegate", "Error al convertir el view controller a un DineroMovilViewController", ex);
			return;
		}
		
		try {
			monto = Double.parseDouble(importe); 
		} catch (Exception ex) {
			monto = 0.0;
		}
		try {
			otroMonto = Double.parseDouble(otroImporte); 
		} catch (Exception ex) {
			otroMonto = 0.0;
		}
		
		viewController.setHabilitado(true);
		if(Constants.TELEPHONE_NUMBER_LENGTH != telefono.length()) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_telefono_corto);
		} else if (Constants.TELEPHONE_NUMBER_LENGTH != confirmacion.length()) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_confirmar_cotro);
		} else if (!telefono.equalsIgnoreCase(confirmacion)) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_confirmar_mal);
		} else if (0 == beneficiario.length()) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_beneficiario_nulo);
		} else if (null != importe && Constants.BALANCE_EN_CEROS == monto) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_importe_vacio);
		} else if (null == importe && Constants.BALANCE_EN_CEROS == otroMonto) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_otro_vacio);
		} else if (null == importe && 0 != (otroMonto % 100.0)) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_otro_invalido);		
		} else {
			transferencia.setCelularbeneficiario(telefono);
			transferencia.setImporte(Tools.formatAmountForServer(String.valueOf((null == importe) ? otroMonto : monto)));
			transferencia.setBeneficiario(beneficiario);
			if(esFrecuente)
				tipoOperacion = Operacion.dineroMovilF;
			else
				tipoOperacion = Operacion.dineroMovil;
			//ARR
			Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
			
			//ARR
			paso2OperacionMap.put("evento_paso2", "event47");
			paso2OperacionMap.put("&&products", "operaciones;transferencias+dinero movil");
			paso2OperacionMap.put("eVar12", "paso2:cuenta e importe");

			TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
				
			showConfirmacion();			
		}
	}
	
	/**
	 * Muestra la pntalla de confirmacion.
	 */
	public void showConfirmacion() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		if (registrarOperacion())
			bMovilVC.showRegistrarOperacion(this);
		else
			bMovilVC.showConfirmacion(this);
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getDatosTablaConfirmacion()
	 */
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		if (bajaFrecuente) {
			
			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.altafrecuente_nombrecorto));
			fila.add(transferencia.getAliasFrecuente());
			tabla.add(fila);
			
			fila = new ArrayList<String>();
			fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_celular));
			fila.add(transferencia.getCelularbeneficiario());
			tabla.add(fila);
			
			fila = new ArrayList<String>();
			fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_compania));
			fila.add(transferencia.getNombreCompania());
			tabla.add(fila);
			
			fila = new ArrayList<String>();
			fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_beneficiario));
			fila.add(transferencia.getBeneficiario());
			tabla.add(fila);

			fila = new ArrayList<String>();
			fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_concepto));
			fila.add(transferencia.getConcepto());
			tabla.add(fila);
			
			ArrayList<Object> entryList = tabla;
			for(int i = entryList.size() - 1; i >= 0; i--) {
				try {
					@SuppressWarnings("unchecked")
					ArrayList<String> entry = (ArrayList<String>)entryList.get(i);
					if(entry.get(1).trim().equals(Constants.EMPTY_STRING)) 
						entryList.remove(i);
				} catch(Exception ex) {
					if(Server.ALLOW_LOG) Log.i(getClass().getSimpleName(), "Error al quitar el elemento.", ex);
				}
			}
			
		} else if(esBajaDM)	{
			tabla = this.getDatosTablaDetalleMovimientoDM();
		} else {

			fila = new ArrayList<String>();
			fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_cuenta));
			fila.add(Tools.hideAccountNumber(transferencia.getCuentaOrigen().getNumber()));
			tabla.add(fila);
			
			fila = new ArrayList<String>();
			fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_celular));
			fila.add(transferencia.getCelularbeneficiario());
			tabla.add(fila);
			
			fila = new ArrayList<String>();
			fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_compania));
			fila.add(transferencia.getCompania().values().toArray()[0].toString());
			tabla.add(fila);
			
			fila = new ArrayList<String>();
			fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_beneficiario));
			fila.add(transferencia.getBeneficiario());
			tabla.add(fila);
			
			fila = new ArrayList<String>();
			fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_importe));
			fila.add(Tools.formatAmount(transferencia.getImporte(), false));
			tabla.add(fila);
			
			fila = new ArrayList<String>();
			fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_concepto));
			fila.add(transferencia.getConcepto());
			tabla.add(fila);
		}
		
		return tabla;
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getTextoEspecialResultados()
	 */
	@Override
	public String getTextoEspecialResultados() {
		if (bajaFrecuente) {
			return super.getTextoEspecialResultados();
		}else if (esBajaDM) {
			return super.getTextoEspecialResultados();
		}else {
			return SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_especial);
		}
	}
	
	@Override
	public String getTituloTextoEspecialResultados() {
		if (esBajaDM || bajaFrecuente) {
			return super.getTituloTextoEspecialResultados();
		}else {
			return  SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_titulo_texto_especial);
		}
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getNombreImagenEncabezado()
	 */
	@Override
	public int getNombreImagenEncabezado() {
		if(esBajaDM)
			return R.drawable.bmovil_consultar_icono;
		else
			return R.drawable.bmovil_dinero_movil_icono;
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getTextoEncabezado()
	 */
	@Override
	public int getTextoEncabezado() {
		if(esBajaDM)
			return R.string.bmovil_consultar_dineromovil_titulo;
		else
			return R.string.transferir_dineromovil_titulo;
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getTextoAyudaResultados()
	 */
	@Override
	public String getTextoAyudaResultados() {
   		if (bajaFrecuente) {
   			return super.getTextoAyudaResultados();
   		} else if (esBajaDM) {
   			return super.getTextoAyudaResultados();
		} else {
   			return SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_ayuda);
   		}
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getColorTituloResultado()
	 */
	@Override
	public int getColorTituloResultado() {
		if (bajaFrecuente || esBajaDM) {
			return R.color.magenta;
		} else {
			return R.color.verde_limon;
		}
	}

	/**
	 * El tipo de operación.
	 */
	private Constants.Operacion tipoOperacion;
	
	/**
	 * @return El tipo de operación.
	 */
	public Constants.Operacion getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	 * @param tipoOperacion El tipo de operación a estblecer.
	 */
	public void setTipoOperacion(Constants.Operacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	private Hashtable<String, String> informarPeticionAltaFrecuente(String contrasenia, String nip, String token) {
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		Session session = Session.getInstance(SuiteApp.appContext);

		String cAut = Autenticacion.getInstance().getCadenaAutenticacion(tipoOperacion, session.getClientProfile(), Tools.getDoubleAmountFromServerString(transferencia.getImporte()));
		paramTable.put(ServerConstants.ID_OPERACION, ServerConstants.DINERO_MOVIL);
		paramTable.put(ServerConstants.NUMERO_TELEFONO_ETIQUETA, session.getUsername());
		paramTable.put(ServerConstants.CONTRASENA_ETIQUETA, (null == contrasenia) ? "" : contrasenia);
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());
		paramTable.put(ServerConstants.NUMERO_CLIENTE_ETIQUETA, session.getClientNumber());
		paramTable.put(ServerConstants.CUENTA_CARGO_ETIQUETA, transferencia.getCuentaOrigen().getType() + transferencia.getCuentaOrigen().getNumber());
		paramTable.put(ServerConstants.IMPORTE_ETIQUETA, transferencia.getImporte());
		paramTable.put(ServerConstants.NOMBRE_BENEFICIARIO_ETIQUETA, transferencia.getBeneficiario());
		paramTable.put(ServerConstants.CONCEPTO_ETIQUETA, transferencia.getConcepto());
		paramTable.put(ServerConstants.NUMERO_CELULAR_TERCERO_ETIQUETA, transferencia.getCelularbeneficiario());
		paramTable.put(ServerConstants.OPERADORA_TELEFONIA_CELULAR_ETIQUETA, transferencia.getCompania().values().toArray()[0].toString());
		paramTable.put(ServerConstants.NIP_ETIQUETA, Tools.isEmptyOrNull(nip) ? "" : nip);
		paramTable.put(ServerConstants.CVV_ETIQUETA, "");
		paramTable.put(ServerConstants.OTP_TOKEN_ETIQUETA, Tools.isEmptyOrNull(token) ? "" : token);
		paramTable.put(ServerConstants.INSTRUCCION_VALIDACION_ETIQUETA,	cAut);
		
		return paramTable;
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#realizaOperacion(suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionViewController, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void realizaOperacion(ConfirmacionViewController confirmacionViewController,	String contrasenia, String nip, String token, String campoTarjeta) {
		//this.ownerController = confirmacionViewController;

		Hashtable<String, String> paramTable;
		Session session = Session.getInstance(SuiteApp.appContext);
        int operacion = 0;

     	if (bajaFrecuente) {

			operacion = Server.BAJA_FRECUENTE;
			//completar hashtable
			paramTable = new Hashtable<String, String>();

			paramTable.put(Server.USERNAME_PARAM, session.getUsername());//"NT"
			paramTable.put(Server.PASSWORD_PARAM, Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia );//NP
			paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());//IU
			paramTable.put(Server.NUMERO_CLIENTE_PARAM, session.getClientNumber());//TE
			paramTable.put(Server.DESCRIPCION_PARAM, transferencia.getAliasFrecuente());//DE
			// CC
			paramTable.put(Server.PAYMENT_ACCOUNT_PARAM, transferencia.getCelularbeneficiario());//CA
//			paramTable.put(Server.BENEFICIARIO_PARAM, compraTiempoAire.getNombreCompania());//BF
			//CB
			//IM
//			paramTable.put(Server.REFERENCIA_NUMERICA_PARAM, compraTiempoAire.getCelularDestino());//RF
			//CP
			paramTable.put(Server.TIPO_CONSULTA_PARAM, Constants.TPDineroMovil);//TP
			String va =  Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile());
			paramTable.put(Server.VA_PARAM, Tools.isEmptyOrNull(va) ? "" : va);//validar

			Hashtable<String, String> paramTable2=new Hashtable<String, String>();
			paramTable2=ServerMapperUtil.mapperBaja(paramTable);
			//JAIG SI
			doNetworkOperation(operacion, paramTable2, false, null, isJsonValueCode.NONE, confirmacionViewController);

		} 
//     	else if(esBajaDM) {
//			ConsultaDineroMovil movimiento = movimientosDM.getMovimientos().get(movimientoSeleccionadoDM);
//			operacion = Server.BAJA_OPSINTARJETA;
//
//			paramTable.put(Server.USER_ID_PARAM, 					session.getUsername());
//			paramTable.put(Server.PASSWORD_PARAM, 					Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia);
//			paramTable.put(ServerConstants.IUM, 						session.getIum());
//			paramTable.put(Server.NUMERO_CLIENTE_PARAM, 			session.getClientNumber());
//			paramTable.put(Server.FOLIO_OPERACION_PARAM, 			movimiento.getFolioOperacion());
//			paramTable.put(Server.CODIGO_CANAL_PARAM, 				movimiento.getCodigoCanal());
//			paramTable.put(Server.FECHA_OPERACION, 					movimiento.getFechaAlta());
//			if(!Tools.isEmptyOrNull(nip))
//				paramTable.put(Server.NIP_PARAM,					nip);
//			if(!Tools.isEmptyOrNull(token))
//				paramTable.put(ServerConstants.CODIGO_OTP, 					token);
//			// TODO Pendiente para modificacion posterior.
//	//		if(!Tools.isEmptyOrNull(cvv2))
//	//			paramTable.put(Server.CVV2_PARAM, 						"");
//			paramTable.put(Server.VALIDATION_INSTRUCTIONS_PARAM,	Autenticacion.getInstance().getCadenaAutenticacion(tipoOperacion, 
//																													   Session.getInstance(SuiteApp.appContext).getClientProfile()));
//			
//		} 
     	else {
	        operacion = Server.ALTA_OPSINTARJETA;
	        paramTable = informarPeticionAltaFrecuente(contrasenia, nip, token);
			//JAIG SI
			doNetworkOperation(operacion, paramTable,false,new TransferenciaDineroMovilData(),isJsonValueCode.NONE, confirmacionViewController);
		}

//		SuiteApp.getInstance().getBmovilApplication().invokeNetworkOperation(operacion, paramTable, viewController);
		
	}
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson, ParsingHandler handler,isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if( viewController != null)
			((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
	}

	/**
	 * La respuesta del servidor.
	 */
	private TransferenciaDineroMovilData serverResponse;
	
	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#analyzeResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			if(operationId == Server.OP_SOLICITAR_ALERTAS){
				
				analyzeAlertasRecortadoSinError();
				return;
			}
			if(operationId == Server.OP_VALIDAR_CREDENCIALES){
				showConfirmacionRegistro();
				return;
			}
			if(response.getResponse() instanceof TransferenciaDineroMovilData) {
				serverResponse = (TransferenciaDineroMovilData)response.getResponse();
				
				if ( esFrecuente && (tokenAMostrar() == Constants.TipoOtpAutenticacion.codigo 
						|| tokenAMostrar() == Constants.TipoOtpAutenticacion.registro)
						&& Tools.isEmptyOrNull(transferencia.getFrecuenteMulticanal())) {
					actualizaFrecuenteAMulticanal();
				} else {
					transferenciaExitosa();
				}
			} else if(response.getResponse() instanceof PaymentExtract){
				PaymentExtract extract = (PaymentExtract) response.getResponse();
				frecuentes = extract.getPayments();
				mostrarListaFrecuentes();
			} else if(response.getResponse() instanceof ConsultaDineroMovilData) {
				this.movimientosDM = (ConsultaDineroMovilData)response.getResponse();
				actualizarMovimientosDM();
			} else {
				((BmovilViewsController)viewController.getParentViewsController()).showResultadosViewController(this, -1, -1);
			}
		} else if(response.getStatus() == ServerResponse.OPERATION_WARNING) {
			if(viewController != null)
				((BmovilViewsController)viewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
			else if(frecuenteController != null)
				((BmovilViewsController)frecuenteController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
			else{
				BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
				BmovilViewsController bmvc = bmApp.getBmovilViewsController(); 
				BaseViewController current = bmvc.getCurrentViewControllerApp();
				current.showInformationAlert(response.getMessageText());
			}
		}else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
			
			if(operationId == Server.OP_VALIDAR_CREDENCIALES){
				BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
				BmovilViewsController bmvc = bmApp.getBmovilViewsController(); 
				BaseViewController current = bmvc.getCurrentViewControllerApp();
				current.showInformationAlert(response.getMessageText());
				return;
			}
			this.movimientosDM = null;
				actualizarMovimientosDM();
			
		}
	}
	
	/**
	 * Muestra la pantalla de resultados.
	 */
	public void showResultados() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showResultadosViewController(this, 
																												R.string.transferir_dineromovil_titulo, 
																											  R.drawable.bmovil_dinero_movil_icono);
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getDatosTablaResultados()
	 */
	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		ArrayList<Object> datosTabla =  new ArrayList<Object>();
		ArrayList<String> registro = null;
		
		if (bajaFrecuente) {
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.altafrecuente_nombrecorto));
			registro.add(transferencia.getAliasFrecuente());
			datosTabla.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_celular));
			registro.add(transferencia.getCelularbeneficiario());
			datosTabla.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_compania));
			registro.add(transferencia.getNombreCompania());
			datosTabla.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_beneficiario));
			registro.add(transferencia.getBeneficiario());
			datosTabla.add(registro);

			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_concepto));
			registro.add(transferencia.getConcepto());
			datosTabla.add(registro);
			
		} else if(esBajaDM) {
			datosTabla = this.getDatosTablaDetalleMovimientoDM();
		} else {

			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_codigo));
			registro.add(serverResponse.getClaveOTP());
			datosTabla.add(registro);
	
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_cuenta));
			registro.add(Tools.hideAccountNumber(transferencia.getCuentaOrigen().getNumber()));
			datosTabla.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_celular));
			registro.add(transferencia.getCelularbeneficiario());
			datosTabla.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_compania));
			registro.add(transferencia.getCompania().values().toArray()[0].toString());
			datosTabla.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_beneficiario));
			registro.add(transferencia.getBeneficiario());
			datosTabla.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_importe));
			registro.add(Tools.formatAmount(transferencia.getImporte(), false));
			datosTabla.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_concepto));
			registro.add(transferencia.getConcepto());
			datosTabla.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_alta));
			registro.add(Tools. formatDate(serverResponse.getFechaOperacion()));
			datosTabla.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_vigencia));
			registro.add(Tools.formatDate(serverResponse.getFechaExpiracion()) + " " + serverResponse.getHoraOperacion() + " " + "hrs");
			datosTabla.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_folio));
			registro.add(serverResponse.getFolio());
			datosTabla.add(registro);
		}

		//frecuentes correo electronico
		//alias frecuente
		//aliasFrecuente = transferencia.getAliasFrecuente() != null && !transferencia.getAliasFrecuente().isEmpty() ? transferencia.getAliasFrecuente() : aliasFrecuente;
		if(aliasFrecuente != null && !aliasFrecuente.isEmpty()){
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_alias_frecuente));
			registro.add(aliasFrecuente);
			datosTabla.add(registro);
		}

		//correo frecuente
		//correoFrecuente = transferencia.getCorreoFrecuente() != null && !transferencia.getCorreoFrecuente().isEmpty() ? transferencia.getCorreoFrecuente() : correoFrecuente;
		if(correoFrecuente !=null && !correoFrecuente.isEmpty()){
			registro = new ArrayList<String>();
			registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_correo_electronico_frecuente));
			registro.add(correoFrecuente);
			datosTabla.add(registro);
		}
		correoFrecuente = transferencia.getCorreoFrecuente() != null && !transferencia.getCorreoFrecuente().isEmpty() ? transferencia.getCorreoFrecuente() : correoFrecuente;


		return datosTabla;
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getTextoTituloResultado()
	 */
	@Override
	public String getTextoTituloResultado() {
		if (bajaFrecuente) {
			return SuiteApp.appContext.getString(R.string.baja_frecuentes_titulo_tabla_resultados);
		}else if (esBajaDM) {
			return SuiteApp.appContext.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_operacion_cancelada);
		} else {
			return SuiteApp.appContext.getString(R.string.transferir_dineromovil_resultados_titulo);
		}
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getTextoTituloConfirmacion()
	 */
	@Override
	public String getTextoTituloConfirmacion() {
		return SuiteApp.appContext.getString(R.string.transferir_dineromovil_titulo);
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getTextoSMS()
	 */
	@Override
	public String getTextoSMS() {
		StringBuilder builder = new StringBuilder();
		builder.append("Dinero movil por ");
		builder.append(Tools.formatAmount(transferencia.getImporte(), false));
		builder.append(" al ");
		builder.append(transferencia.getNombreCompania());
		builder.append(" ");
		builder.append(Tools.hideAccountNumber(transferencia.getCelularbeneficiario()));
		builder.append(" de ");
		builder.append(transferencia.getBeneficiario());
		builder.append(" Folio: ");
		builder.append(serverResponse.getFolio());
		builder.append(" Codigo seg: ");
		builder.append(serverResponse.getClaveOTP());
		builder.append(" Informe a Beneficiario Vence: ");
		builder.append(serverResponse.getFechaExpiracion() + " " + serverResponse.getHoraOperacion());
		builder.append("h Concepto: ");
		builder.append(transferencia.getConcepto());		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getOpcionesMenuResultados()
	 */
	@Override
	public int getOpcionesMenuResultados() {
		if(esBajaDM || bajaFrecuente) {
			return super.getOpcionesMenuResultados();
		} else if(esFrecuente) {
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF;
		} else { 
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF | SHOW_MENU_FRECUENTE;
		}
	}
	
	/*
	 * M�todo que se utilizar� para la eliminaci�n de frecuentes
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eliminarFrecuente(int indexSelected) {
		ArrayList<Object> arlPayments = null;
		if (viewController instanceof ConsultarFrecuentesViewController) {
			   arlPayments = ((ConsultarFrecuentesViewController)viewController).getListaFrecuentes().getLista();
		}
		
		Payment selectedPayment = frecuentes[indexSelected];
		if (arlPayments != null && arlPayments.size() > 0) {
			   selectedPayment = (Payment)((ArrayList<Object>)arlPayments.get(indexSelected)).get(0);
		}
		this.selectedPayment = selectedPayment;
//		ArrayList<Account> cuentas = cargaCuentasOrigen();
//		if(cuentas.size() > 0)
//			   getCompraTiempoAire().setCuentaOrigen(cuentas.get(0));
		
		if(viewController != null) {
			   esFrecuente = false;
			   setBajaFrecuente(true);
			   this.selectedPayment = selectedPayment;
			   setTipoOperacion(Constants.Operacion.dineroMovilF);
			   transferencia.setAliasFrecuente(selectedPayment.getNickname());
			   transferencia.setCelularbeneficiario(selectedPayment.getBeneficiaryAccount());
			   transferencia.setBeneficiario(selectedPayment.getBeneficiary());
			   transferencia.setImporte(selectedPayment.getAmount());
			   transferencia.setNombreCompania(selectedPayment.getOperadora());
			   transferencia.setIdCanal(selectedPayment.getIdCanal());
			   transferencia.setUsuario(selectedPayment.getUsuario());
			   
		}
		setTipoOperacion(Constants.Operacion.bajaFrecuente);
		showConfirmacionAutenticacion();
	}


	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getTextoBotonOperacionNueva()
	 */
	@Override
	public String getTextoBotonOperacionNueva() {
		return SuiteApp.appContext.getString(R.string.transferir_dineromovil_nueva_operacion);
	}
	
	/**
	 * Obtiene la compa�ia seleccionada desde un frecuente.
	 * @return La compa�ia seleccionada.
	 */
	public Object getCompaniaSeleccionada() {
		if(!esFrecuente)
			return null;
		
		Object cuentaSeleccionada = null;
		
		CatalogoVersionado catalogoDineroMovil = Session.getInstance(SuiteApp.appContext).getCatalogoDineroMovil();
		for (Object comp : catalogoDineroMovil.getObjetos()) {
			Compania compania = (Compania)comp;
			
			if(compania.getNombre().equalsIgnoreCase(transferencia.getNombreCompania())) {
				cuentaSeleccionada = comp;
				break;
			}
		}
		
		return cuentaSeleccionada;
	}
	

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getDatosTablaAltaFrecuentes()
	 */
	@Override
	public ArrayList<Object> getDatosTablaAltaFrecuentes() {
		ArrayList<Object> datosAltaFrecuente = new ArrayList<Object>();
		ArrayList<String> registro;
		
		registro = new ArrayList<String>();
		registro.add(viewController.getString(R.string.transferir_dineromovil_confirmacion_cuenta));
		registro.add(Tools.hideAccountNumber(transferencia.getCuentaOrigen().getNumber()));
		datosAltaFrecuente.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(viewController.getString(R.string.transferir_dineromovil_confirmacion_celular));
		registro.add(transferencia.getCelularbeneficiario());
		datosAltaFrecuente.add(registro);

		registro = new ArrayList<String>();
		registro.add(viewController.getString(R.string.transferir_dineromovil_confirmacion_compania));
		registro.add(transferencia.getNombreCompania());
		datosAltaFrecuente.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(viewController.getString(R.string.transferir_dineromovil_confirmacion_beneficiario));
		registro.add(transferencia.getBeneficiario());
		datosAltaFrecuente.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(viewController.getString(R.string.transferir_dineromovil_confirmacion_concepto));
		registro.add(transferencia.getConcepto());
		datosAltaFrecuente.add(registro);
		
		return datosAltaFrecuente;
	}
	

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getDatosHeaderTablaFrecuentes()
	 */
	@Override
	public ArrayList<Object> getDatosHeaderTablaFrecuentes() {
		ArrayList<Object> registros = new ArrayList<Object>();
		registros.add("");
		registros.add(SuiteApp.appContext.getString(R.string.altafrecuente_nombrecorto));
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_frecuentes_header_cuenta));
		return registros;
	}
	

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getDatosTablaFrecuentes()
	 */
	@Override
	public ArrayList<Object> getDatosTablaFrecuentes() {
		ArrayList<Object> listaDatos = new ArrayList<Object>();
		ArrayList<Object> registro;
		
		if (frecuentes != null) {
			for (Payment payment : frecuentes) {
				registro = new ArrayList<Object>();
				registro.add(payment);
				registro.add(payment.getNickname());
				registro.add(payment.getBeneficiaryAccount());
				listaDatos.add(registro);
			}
		}/* else {
			ArrayList<Object> registro = new ArrayList<Object>();
			registro.add("");
			registro.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_frecuentes_emptylist));
			registro.add("");
			listaDatos.add(registro);
		}*/
		return listaDatos;
	}
	

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#operarFrecuente(int)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void operarFrecuente(int indexSelected) {
		ArrayList<Object> arlPayments = null;
		if (viewController instanceof ConsultarFrecuentesViewController) {
			   arlPayments = ((ConsultarFrecuentesViewController)viewController).getListaFrecuentes().getLista();
		}
		
		Payment selectedPayment = frecuentes[indexSelected];
		if (arlPayments != null && arlPayments.size() > 0) {
			   selectedPayment = (Payment)((ArrayList<Object>)arlPayments.get(indexSelected)).get(0);
		}
		
//		ArrayList<Account> cuentas = cargaCuentasOrigen();
//		if(cuentas.size() > 0)
//			   getCompraTiempoAire().setCuentaOrigen(cuentas.get(0));
		if(viewController != null) {
			   esFrecuente = true;
			   bajaFrecuente = false;
			   this.selectedPayment = selectedPayment;
			   setTipoOperacion(Constants.Operacion.dineroMovilF);
			   transferencia.setCelularbeneficiario(selectedPayment.getBeneficiaryAccount());
			   transferencia.setBeneficiario(selectedPayment.getBeneficiary());
			   transferencia.setImporte(selectedPayment.getAmount());
			   transferencia.setNombreCompania(selectedPayment.getOperadora());
			   transferencia.setIdCanal(selectedPayment.getIdCanal());
			   transferencia.setUsuario(selectedPayment.getUsuario());
			   //frecuente correo electronico
			transferencia.setAliasFrecuente(selectedPayment.getNickname());
			transferencia.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());

			ArrayList<Account> cuentas = cargaCuentasOrigen();
			   if(cuentas.size() > 0)
				   transferencia.setCuentaOrigen(cuentas.get(0));
			   
			   ((BmovilViewsController)viewController.getParentViewsController()).showTransferirDineroMovil(transferencia,false);
		}
		
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#consultarFrecuentes(java.lang.String)
	 */
	@Override
	public void consultarFrecuentes(String tipoFrecuente) {
		esFrecuente = true;
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String,String> paramTable = null;
		paramTable = new Hashtable<String, String>();

		paramTable.put("TP",	tipoFrecuente);//TP
		paramTable.put("TE",	session.getClientNumber());//TE
		paramTable.put(ServerConstants.PARAMS_TEXTO_IU,			session.getIum());//IU
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, 		session.getUsername());//NT

		//CHECK INVOCACION
		String tipoConsulta = tipoFrecuente;
		if(tipoConsulta.equals(Constants.tipoCFOtrosBBVA))
		{
			//paramTable.put("NT", 		session.getUsername());//NT
			//paramTable.put("IU",			session.getIum());//IU
			//paramTable.put("numeroCliente",	session.getClientNumber());//TE
			//JAIG SI
			paramTable.put("order","TP*TE*IU*NT");
			((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION_BBVA, paramTable,true, new PaymentExtract(), isJsonValueCode.NONE,viewController,true);

		}
		else
		{
			paramTable.put("AP", "");
			//Correo freceuntes
			paramTable.put("VM", Constants.APPLICATION_VERSION);
			//JAIG SI
			paramTable.put("order","TP*TE*IU*VM*AP*NT");
			((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable,false, new PaymentExtract(),isJsonValueCode.NONE, viewController,true);

		}
//		doNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable, viewController);

	}
	
	public void comprobarRecortado(){
		Session session = Session.getInstance(SuiteApp.appContext);
		Constants.Perfil perfil = session.getClientProfile();
		
		if(Constants.Perfil.recortado.equals(perfil)){
			
			solicitarAlertas(getViewController());
			
		}
		
	}
	

	private BaseViewController frecuenteController;

	/**
	 * @return the frecuenteController
	 */
	public BaseViewController getFrecuenteController() {
		return frecuenteController;
	}

	/**
	 * @param frecuenteController the frecuenteController to set
	 */
	public void setFrecuenteController(BaseViewController frecuenteController) {
		this.frecuenteController = frecuenteController;
	}
	
	private Payment[] frecuentes;
	
	private void mostrarListaFrecuentes(){
		((ConsultarFrecuentesViewController)viewController).muestraFrecuentes();
	}

	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getParametrosAltaFrecuentes()
	 */
	@Override
	public Hashtable<String, String> getParametrosAltaFrecuentes() {
		//JAIG
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String,String> paramTable = null;		
		paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
		paramTable.put(ServerConstants.PARAMS_TEXTO_IU, session.getIum());
		paramTable.put("TE", session.getClientNumber());
		paramTable.put("CC", "");
		paramTable.put("CA",  transferencia.getCelularbeneficiario());
		paramTable.put("BF", transferencia.getBeneficiario());
		paramTable.put("CP", transferencia.getConcepto());
		paramTable.put("TP", Constants.TPDineroMovil);
		paramTable.put("OA", transferencia.getNombreCompania());
		paramTable.put("CB", "");
		paramTable.put("IM", "");
		paramTable.put("RF", "");
		paramTable.put("AP", "");
		paramTable.put("CV", "");
		return paramTable;
	}

	private Payment selectedPayment;

	/**
	 * @return the selectedPayment
	 */
	public Payment getSelectedPayment() {
		return selectedPayment;
	}

	/**
	 * @param selectedPayment the selectedPayment to set
	 */
	public void setSelectedPayment(Payment selectedPayment) {
		this.selectedPayment = selectedPayment;
	}
	
	/**
	 * envia la actualizacion del frecuente
	 */
	private void actualizaFrecuenteAMulticanal() {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), "actualizaFrecuenteAMulticanal");
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> paramTable = null;
		paramTable = new Hashtable<String, String>();
		
		
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
		paramTable.put(ServerConstants.PARAMS_TEXTO_IU, session.getIum());
		paramTable.put("TE", session.getClientNumber());
		paramTable.put("TP", Constants.TPDineroMovil);
		paramTable.put("CA", transferencia.getAliasFrecuente());
		paramTable.put("DE", transferencia.getAliasFrecuente());
		paramTable.put("RF", "");
		paramTable.put("BF", transferencia.getBeneficiario());
		paramTable.put("OA", transferencia.getNombreCompania());
		paramTable.put("CN", transferencia.getIdCanal());
		paramTable.put("CB", "");
		paramTable.put("US", transferencia.getUsuario());
		//frecuente correo electronico
		paramTable.put("CE", transferencia.getCorreoFrecuente());


		doNetworkOperation(Server.ACTUALIZAR_FRECUENTE, paramTable,false,new FrecuenteMulticanalResult(), isJsonValueCode.NONE, viewController);
	}
	
	/**
	 * continuacion del flujo despues de actulizar el frecuente 
	 * continuacion del flujo normal para mostrar resultados
	 */
	private void transferenciaExitosa(){
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), "transferenciaExitosa");
		showResultados();
	}

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#getDelegateIdentifier()
	 */
	@SuppressWarnings("static-access")
	@Override
	public long getDelegateIdentifier() {
		return this.DINERO_MOVIL_DELEGATE_ID;
	}
	
	/* ####################################################################################################################### */
	
	

	private ConsultaDineroMovilData movimientosDM;
	
	public ArrayList<Object> getHeaderListaMovimientos() {
		ArrayList<Object> result = new ArrayList<Object>();
		
		result.add(null);
		result.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_dineromovil_movimientos_lista_codigo));
		result.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_dineromovil_movimientos_lista_beneficiario));
		result.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_dineromovil_movimientos_lista_vencimiento));
		result.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_dineromovil_movimientos_lista_importe));
		
		return result;
	}
	
	public ArrayList<Object> getDatosTablaMovimientosDM() {
		ArrayList<Object> datos = new ArrayList<Object>();
		ArrayList<String> registro;
		
		if(null == movimientosDM || movimientosDM.getMovimientos().isEmpty()) {
			registro = new ArrayList<String>();
			registro.add(null);
			registro.add("");
			registro.add("");
			registro.add("");
			registro.add("");
		} else {
			for(ConsultaDineroMovil consulta : movimientosDM.getMovimientos()) {
				registro = new ArrayList<String>();
				registro.add(null);
				registro.add(consulta.getCodigoSeguridad());
				registro.add((Constants.MOVEMENT_SHORT_DESCRIPTION < consulta.getBeneficiario().length()) ? 
							 consulta.getBeneficiario().substring(0, Constants.MOVEMENT_SHORT_DESCRIPTION) : 
							 consulta.getBeneficiario() );
				registro.add(Tools.formatShortDate(consulta.getFechaVigencia()));
				registro.add(Tools.formatAmount(consulta.getImporte(), false));
				datos.add(registro);
			}
		}
		
		return datos;
	}
	
	public void consultarMovimientosDM(Constants.TipoMovimientoDM tipo) {
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		Session session = Session.getInstance(SuiteApp.appContext);
        int operacion = 0;

     	operacion = Server.CONSULTA_OPSINTARJETA;
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, 					session.getUsername());
		paramTable.put(ServerConstants.PARAMS_TEXTO_NP, 					"");
		paramTable.put(ServerConstants.PARAMS_TEXTO_IU, 						session.getIum());
		paramTable.put("TE", 			session.getClientNumber());
		paramTable.put("EO",			tipo.codigo);
		
		//doNetworkOperation(operacion, paramTable, viewController);
		  ((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operacion, paramTable,false,new ConsultaDineroMovilData(),isJsonValueCode.NONE, viewController,true);
	}
	
	public void actualizarMovimientosDM() {
		if(!(viewController instanceof ConsultarDineroMovilViewController))
			return;
		ConsultarDineroMovilViewController controller = (ConsultarDineroMovilViewController)viewController;
		
		ArrayList<Object> movimientos = getDatosTablaMovimientosDM();
		
		controller.getListaEnvios().setNumeroFilas(movimientos.size());
		controller.getListaEnvios().setLista(movimientos);
		
		if(movimientos.isEmpty())
			controller.getListaEnvios().setTextoAMostrar(viewController.getString(R.string.bmovil_consultar_dineromovil_sin_envios));
		else{
			controller.getListaEnvios().setTextoAMostrar(null);
			controller.getListaEnvios().setNumeroColumnas(4);
		}
		
		controller.getListaEnvios().cargarTabla();
	}
	
	private int movimientoSeleccionadoDM;
	
	private void showDetallesMovimientoDM() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showDetalleDineroMovil();
	}
	
	public boolean isMovimientoDMVigente() {
		ConsultaDineroMovil movimiento = movimientosDM.getMovimientos().get(movimientoSeleccionadoDM);
		return Constants.TipoMovimientoDM.VIGENTES.codigo.equalsIgnoreCase(movimiento.getEstatus());
	}
	
	public ArrayList<Object> getDatosTablaDetalleMovimientoDM() {
		ArrayList<Object> datos = new ArrayList<Object>();
		
		ConsultaDineroMovil movimiento = movimientosDM.getMovimientos().get(movimientoSeleccionadoDM);
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_otp));
		fila.add(movimiento.getCodigoSeguridad());
		datos.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_origen));
		fila.add(Tools.hideAccountNumber(movimiento.getCuentaOrigen().getNumber()));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_celular));
		fila.add(movimiento.getCelularBeneficiario());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_compania));
		fila.add(movimiento.getCompaniaBeneficiario());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_beneficiario));
		fila.add(movimiento.getBeneficiario());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_importe));
		fila.add(Tools.formatAmount(movimiento.getImporte(), false));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_concepto));
		fila.add(movimiento.getConcepto());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_alta));
		fila.add(Tools.formatDate(movimiento.getFechaAlta()));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_vigencia));
		fila.add(Tools.formatDate(movimiento.getFechaVigencia()) + " " + Tools.formatTime(movimiento.getHoraExpiracion()));
		datos.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_folio));
		fila.add(movimiento.getFolioOperacion());
		datos.add(fila);
		
		return datos;
	}
	
	private boolean esBajaDM = false;
	
	public void setEsBajaDM(boolean esBaja) {
		this.esBajaDM = esBaja;
	}
	
	public void showConfirmacionAutenticacion(){
		BmovilViewsController bmovilParentController = ((BmovilViewsController)viewController.getParentViewsController());
//		bmovilParentController.showConfirmacionAutenticacionViewController(this, getNombreImagenEncabezado(), 
//				getTextoEncabezado(), 
//				R.string.confirmation_subtitulo);
		
		bmovilParentController.showConfirmacionAutenticacionViewController(this, 
																		   R.drawable.bmovil_consultar_icono, 
																		   R.string.bmovil_consultar_dineromovil_titulo, 
																		   R.string.confirmation_subtitulo);
	}
	
	@Override
	public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController,String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
		//this.ownerController = confirmacionViewController;
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		Session session = Session.getInstance(SuiteApp.appContext);
        int operacion = 0;

        if (bajaFrecuente) {

			operacion = Server.BAJA_FRECUENTE;
			//completar hashtable
			paramTable = new Hashtable<String, String>();

			paramTable.put(Server.USERNAME_PARAM, session.getUsername());//"NT"
			paramTable.put(Server.PASSWORD_PARAM, Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia );//NP
			paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());//IU
			paramTable.put(Server.NUMERO_CLIENTE_PARAM, session.getClientNumber());//TE
			paramTable.put(Server.DESCRIPCION_PARAM, transferencia.getAliasFrecuente());//DE
			paramTable.put(Server.PAYMENT_ACCOUNT_PARAM, transferencia.getCelularbeneficiario());//CA
			//			paramTable.put(Server.BENEFICIARIO_PARAM, compraTiempoAire.getNombreCompania());//BF
			//			paramTable.put(Server.REFERENCIA_NUMERICA_PARAM, compraTiempoAire.getCelularDestino());//RF
//			String idToken = (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) ? "T" : "";
			paramTable.put(Server.ID_TOKEN, selectedPayment.getIdToken());
			paramTable.put(Server.TIPO_CONSULTA_PARAM, Constants.TPDineroMovil);//TP
			paramTable.put(Server.OPERADORA_PARAM, transferencia.getNombreCompania());//OA
			String va =  Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile());
			paramTable.put(Server.VA_PARAM,Tools.isEmptyOrNull(va)?"":va);//validar
			paramTable.put(Server.ID_CANAL, transferencia.getIdCanal());
			paramTable.put(Server.USUARIO, transferencia.getUsuario());

			Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
			paramTable2=ServerMapperUtil.mapperBaja(paramTable);
			doNetworkOperation(operacion, paramTable2, false, new BajaDineroMovilData(), isJsonValueCode.NONE, confirmacionAutenticacionViewController);

        }else if(esBajaDM) {

        	if(Server.ALLOW_LOG) Log.d("Dinero movil", "Cancele envio dinero movil");
			ConsultaDineroMovil movimiento = movimientosDM.getMovimientos().get(movimientoSeleccionadoDM);
			operacion = Server.BAJA_OPSINTARJETA;

			paramTable.put(ServerConstants.PARAMS_TEXTO_NT, 					session.getUsername());
			paramTable.put(ServerConstants.PARAMS_TEXTO_NP, 					Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia);
			paramTable.put(ServerConstants.PARAMS_TEXTO_IU, 						session.getIum());
			paramTable.put("TE", 			session.getClientNumber());
			paramTable.put("FO", 			movimiento.getFolioOperacion());
			paramTable.put("CG", 				movimiento.getCodigoCanal());
			String[] fechaAlta = movimiento.getFechaAlta().split("/");
			String fecha = fechaAlta[2]+"-"+fechaAlta[1]+"-"+fechaAlta[0];
			paramTable.put("FE", 					fecha);
			
			if(!Tools.isEmptyOrNull(nip))
				paramTable.put("NI",					nip);
			if(!Tools.isEmptyOrNull(token))
				paramTable.put("OT",					token);
			if(!Tools.isEmptyOrNull(cvv))
				paramTable.put("CV",					cvv);
			
//			paramTable.put(Server.NIP_PARAM, 						Tools.isEmptyOrNull(nip) ? "" : nip);
//			paramTable.put(ServerConstants.CODIGO_OTP, 						Tools.isEmptyOrNull(token) ? "" : token);
//			paramTable.put(Server.CVV2_PARAM, 						Tools.isEmptyOrNull(cvv) ?	"" : cvv);
			
			paramTable.put("VA",	Autenticacion.getInstance().getCadenaAutenticacion(tipoOperacion,
																													   Session.getInstance(SuiteApp.appContext).getClientProfile()));

			doNetworkOperation(operacion, paramTable, false, new BajaDineroMovilData(), isJsonValueCode.NONE, confirmacionAutenticacionViewController);

		}
        //JAIG SI

	}

	public TransferenciaDineroMovilData getServerResponse() {
		return serverResponse;
	}
	
	// #region Rapido.
	/**
	 * Rapido a operar.
	 */
	private boolean operacionRapida;


	/**
	 * @return Rapido a operar.
	 */
	public boolean isOperacionRapida() {
		return this.operacionRapida;
	}

	/**
	 * @param rapido Rapido a operar.
	 */
	public void cargarRapido(Rapido rapido) {
		operacionRapida = (null != rapido); 
		
		if(null == rapido)
			return;
		
		esFrecuente = false;
		if(null == transferencia)
			transferencia = new TransferenciaDineroMovil();
		
		transferencia.setCelularbeneficiario(rapido.getTelefonoDestino());
		transferencia.setBeneficiario(rapido.getBeneficiario());
		transferencia.setConcepto(rapido.getConcepto());
		transferencia.setImporte(Tools.formatAmountForServer(String.valueOf(rapido.getImporte())));
		
		// Cargar compa�ia de celular
		Compania compania = null;
		for (Object comp : Session.getInstance(SuiteApp.appContext).getCatalogoDineroMovil().getObjetos()) {
			compania = (Compania)comp;
			if(compania.getNombre().equalsIgnoreCase(rapido.getCompaniaCelular()))
				break;
			compania = null;
		}
		
		// Agregar la compa�ia si se encontro en los catálogos.
		if(null != compania) {
			HashMap<String, String> mapa = new HashMap<String, String>();
			mapa.put(compania.getClave(), compania.getNombre());
			transferencia.setCompania(mapa);
			transferencia.setNombreCompania(compania.getNombre());
		}
		
		transferencia.setCuentaOrigen(Tools.obtenerCuentaEje());
	}
	
	// #endregion
	
	/**
	 * @category RegistrarOperacion
	 * @return
	 */
	public boolean registrarOperacion(){
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		Operacion operacion;
		if(esFrecuente)
			operacion = Operacion.dineroMovilF;
		else
			operacion = Operacion.dineroMovil;
		Autenticacion aut = Autenticacion.getInstance();
		boolean value = aut.validaRegistro(operacion, perfil);		
		if(Server.ALLOW_LOG) Log.d("RegistroOP",value+" dineroMovilF? "+esFrecuente);
		return value;
	}
	
		
	
	/**
	 *  @category RegistrarOperacion
	 * @return
	 */
	protected ArrayList<Object> getDatosRegistroOp() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_cuenta));
		fila.add(Tools.hideAccountNumber(transferencia.getCuentaOrigen().getNumber()));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_celular));
		fila.add(transferencia.getCelularbeneficiario());
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_compania));
		fila.add(transferencia.getCompania().values().toArray()[0].toString());
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_beneficiario));
		fila.add(transferencia.getBeneficiario());
		tabla.add(fila);
		
//		fila = new ArrayList<String>();
//		fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_importe));
//		fila.add(Tools.formatAmount(transferencia.getImporte(), false));
//		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(SuiteApp.appContext.getString(R.string.transferir_dineromovil_confirmacion_concepto));
		fila.add(transferencia.getConcepto());
		tabla.add(fila);
		
		return tabla;
	}
	
	/**
	 * @category RegistrarOperacion
	 */
	@Override
	public ArrayList<Object> getDatosRegistroExitoso() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> registro;
		
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_op));
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_exito));
		tabla.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_cuenta));
		registro.add(transferencia.getCelularbeneficiario());
		tabla.add(registro);
				
		return tabla;
	}
	
	/**
	 * @category RegistrarOperacion
	 * realiza el registro de operacion
	 * @param rovc
	 * @param token
	 */
	@Override
	protected void realizaOperacion(RegistrarOperacionViewController rovc,	String token) {

		int operationId = Server.OP_VALIDAR_CREDENCIALES;
		Hashtable<String, String> params = new Hashtable<String, String>();		
		Session session = Session.getInstance(SuiteApp.appContext);
     	String cuentaDestino = transferencia.getCelularbeneficiario();

		params.put(ServerConstants.ID_OPERACION, ServerConstants.DINERO_MOVIL);
		params.put(ServerConstants.CELULAR_BENEFICIARIO, transferencia.getCelularbeneficiario());
		params.put(ServerConstants.COMPANIA_BENEFICIARIO, transferencia.getNombreCompania());

		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put("numeroCliente", session.getClientNumber());
		params.put("cveAcceso", "" );
		params.put(Server.J_NIP, "");
		params.put(Server.J_CVV2, "");
		params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
		params.put(Server.J_AUT, "00010");
		params.put("tarjeta5Dig", "");
		params.put(ServerConstants.IUM, session.getIum());
		params.put(ServerConstants.VERSION, Constants.APPLICATION_VERSION);
		//JAIG SI
		doNetworkOperation(operationId, params,true, new RegistrarOperacionResult(),isJsonValueCode.NONE, rovc);
	}
	
	 /**
	  * @category RegistrarOperacion
	  * Realiza la operacion de tranferencia desde la pantalla de confirmacionRegistro
	  */
	@Override
	public void realizaOperacion(ConfirmacionRegistroViewController viewController,	String contrasenia, String nip, String token) {
		int operacion = Server.ALTA_OPSINTARJETA;
		Hashtable<String, String> paramTable = informarPeticionAltaFrecuente(contrasenia, nip, token);
		//JAIG SI
		doNetworkOperation(operacion, paramTable,false, new TransferenciaDineroMovilData(),isJsonValueCode.NONE, viewController);
	}
		
	/**
	 * @category RegistrarOperacion
	 * Muestra la pantalla de confirmacion registro
	 */
	public void showConfirmacionRegistro() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		bMovilVC.showConfirmacionRegistro(this);		
	}
	
	@Override
	public String getNumeroCuentaParaRegistroOperacion() {
		String cuentaDestino = transferencia.getCelularbeneficiario();
		return cuentaDestino.substring(cuentaDestino.length() - 5);
	}
}
