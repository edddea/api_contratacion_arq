
/*
 * Copyright (c) 2009 BBVA. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.Serializable;

import bancomer.api.common.commons.Constants;

/**
 * Bean like class that represents a frequent payment.
 * This class is used by FrequentPaymentExtract parser to wrap individually 
 * each service of the response.
 *
 * @author 
 *
 */
public class Payment extends FrecuenteMulticanalData implements Serializable {
	
    /**
     * Operation nickname, assigned by client (NK)
     */
    private String strNK;

    /**
     * Charge account (CC)
     */
    private String strChargeAccount;

    /**
     *  Beneficiary account (CA)
     */
    private String strBeneficiaryAccount;

    /**
     *  Operation amount (IM)
     */
    private String strAmount;

    /**
     *  Beneficiary entity (BF)
     */
    private String strBeneficiary;
    
    /**
     *  Beneficiary entity (BF)
     */
    private String strOperadora;

    /**
     *  Reference (RF)
     */
    private String strReference;

    /**
     *  Concept (CP)
     */
    private String strConcept;

    /**
     *  Description (DE)
     */
    private String strDescription;

    /**
     *  Bank code (CB)
     */
    private String strBankCode;

    /**
     *  Operation code (TO)
     */
    private String strOperationCode;

    /**
     *  Application date (AP)
     */
    private String idNumber;
    
    /**
     * 
     */
    private String indicadorFrecuente;
    
    /**
     * 
     */
    private String idToken;
    
    private boolean esInterbancario;
    /**
     * Default constructor
     */
    public Payment() {
    }

    /**
     * Constructor with parameters
     * @param strNK                 NK
     * @param strChargeAccount      CC
     * @param strBeneficiaryAccount CA
     * @param strAmount             IM
     * @param strBeneficiary        BF
     * @param strReference          RF
     * @param strConcept            CP
     * @param strDescription        DE
     * @param strBankCode           CB
     * @param strOperationCode      TO
     * @param strApplicationDate    AP
     */
    public Payment(String strNK, String strChargeAccount, String strBeneficiaryAccount,
            String strAmount, String strBeneficiary, String strReference, String strConcept,
            String strDescription, String strBankCode, String strOperationCode,
            String idNumber) {

        this.strNK = strNK;
        this.strChargeAccount = strChargeAccount;
        this.strBeneficiaryAccount = strBeneficiaryAccount;
        this.strAmount = strAmount;
        this.strBeneficiary = strBeneficiary;
        this.strReference = strReference;
        this.strConcept = strConcept;
        this.strDescription = strDescription;
        this.strBankCode = strBankCode;
        this.strOperationCode = strOperationCode;
        this.idNumber = idNumber;
        esInterbancario =  strOperationCode.equals(Constants.tipoCFOtrosBancos);
        if(strOperationCode.equals(Constants.tipoCFOtrosBBVA ) || strOperationCode.equals(Constants.tipoCFCExpress ) || esInterbancario)
        	this.strBeneficiaryAccount = obtenerCuentaOrigen(strBeneficiaryAccount);
    }

    /**
     * Constructor with parameters
     * @param strNK                 NK
     * @param strChargeAccount      CC
     * @param strBeneficiaryAccount CA
     * @param strAmount             IM
     * @param strBeneficiary        BF
     * @param strOperadora			OA
     * @param strReference          RF
     * @param strConcept            CP
     * @param strDescription        DE
     * @param strBankCode           CB
     * @param strOperationCode      TO
     * @param strApplicationDate    AP
     */
    public Payment(String strNK, String strChargeAccount, String strBeneficiaryAccount,
            String strAmount, String strBeneficiary, String strReference, String strConcept,
            String strDescription, String strBankCode, String strOperationCode,
            String idNumber, String strOperadora) {

        this.strNK = strNK;
        this.strChargeAccount = strChargeAccount;
        this.strBeneficiaryAccount = strBeneficiaryAccount;
        this.strAmount = strAmount;
        this.strBeneficiary = strBeneficiary;
        this.strReference = strReference;
        this.strConcept = strConcept;
        this.strDescription = strDescription;
        this.strBankCode = strBankCode;
        this.strOperationCode = strOperationCode;
        this.idNumber = idNumber;
        this.strOperadora = strOperadora;
        esInterbancario =  strOperationCode.equals(Constants.tipoCFOtrosBancos);
        if(strOperationCode.equals(Constants.tipoCFOtrosBBVA ) || strOperationCode.equals(Constants.tipoCFCExpress ) || esInterbancario)
        	this.strBeneficiaryAccount = obtenerCuentaOrigen(strBeneficiaryAccount);
    }

    /*
     * Returns all the payments in a string
     * For debugging purposes only.
     * This function CAN be deleted
     
    public String toString() {

        String strResult = "";
        strResult+= "NK: " + getNK();
        strResult+= "\nCharge account: " + getChargeAccount();
        strResult+= "\nBenef account: " + getBeneficiaryAccount();
        strResult+= "\nAmount: " + getAmount();
        strResult+= "\nBenef: " + getBeneficiary();
        strResult+= "\nRefer: " + getReference();
        strResult+= "\nConcept: " + getConcept();
        strResult+= "\nDesc: " + getDescription();
        strResult+= "\nBank Code: " + getBankCode();
        strResult+= "\nOp Code: " + getOperationCode();
        strResult+= "\nAp IdNumber: " + getIdNumber();
        strResult+= "\n";
        return strResult;
    }*/

    /* ATTRIBUTE GETTERS */

    public String getNickname() {
        return strNK;
    }

    public String getChargeAccount() {
        return strChargeAccount;
    }

    public String getBeneficiaryAccount() {
        return strBeneficiaryAccount;
    }

    public String getAmount() {
        return strAmount;
    }

    public String getBeneficiary() {
        return strBeneficiary;
    }
    
    public String getOperadora() {
        return strOperadora;
    }

    public String getReference() {
        return strReference;
    }

    public String getConcept() {
        return strConcept;
    }

    public String getDescription() {
        return strDescription;
    }

    public String getBankCode() {
        return strBankCode;
    }

    public String getOperationCode() {
        return strOperationCode;
    }

    public String getIdNumber() {
        return idNumber;
    }

    /* ATTRIBUTE SETTERS */

    public void setNK(String strNK) {
        this.strNK = strNK;
    }

    public void setChargeAccount(String strChargeAccount) {
        this.strChargeAccount = strChargeAccount;
    }

    public void setBeneficiaryAccount(String strBeneficiaryAccount) {
        this.strBeneficiaryAccount = strBeneficiaryAccount;
    }

    public void setAmount(String strAmount) {
        this.strAmount = strAmount;
    }

    public void setBeneficiary(String strBeneficiary) {
        this.strBeneficiary = strBeneficiary;
    }
    
    public void setOperadora(String strOperadora) {
        this.strOperadora = strOperadora;
    }

    public void setReference(String strReference) {
        this.strReference = strReference;
    }

    public void setConcept(String strConcept) {
        this.strConcept = strConcept;
    }

    public void setDescription(String strDescription) {
        this.strDescription = strDescription;
    }

    public void setBankCode(String strBankCode) {
        this.strBankCode = strBankCode;
    }

    public void setOperationCode(String strOperationCode) {
        this.strOperationCode = strOperationCode;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }
    
    public String toString(){
    	return (this.getNickname().trim().length() == 0)
    			? this.getConcept()
    			: this.getNickname().trim();
    }

	/**
	 * @return the indicadorFrecuente
	 */
	public String getIndicadorFrecuente() {
		return indicadorFrecuente;
	}

	/**
	 * @param indicadorFrecuente the indicadorFrecuente to set
	 */
	public void setIndicadorFrecuente(String indicadorFrecuente) {
		this.indicadorFrecuente = indicadorFrecuente;
	}

	/**
	 * @return the idToken
	 */
	public String getIdToken() {
		return idToken;
	}

	/**
	 * @param idToken the idToken to set
	 */
	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}

	private String beneficiaryTypeAccount;
	
	public String getBeneficiaryTypeAccount() {
		return beneficiaryTypeAccount;
	}
	
	public void setBeneficiaryTypeAccount(String beneficiaryTypeAccount) {
		this.beneficiaryTypeAccount = beneficiaryTypeAccount;
	}
	
	protected String obtenerCuentaOrigen(String cuenta){
		this.beneficiaryTypeAccount = "";
		boolean esTC = false;
	if(cuenta != null){
		if ((cuenta.substring(0, 2).equals("10"))&&(cuenta.length() == 12)){
	           this.beneficiaryTypeAccount = "10";
	           return cuenta.substring(2);
	       }
		if((!Character.isDigit(cuenta.charAt(0)) && !Character.isDigit(cuenta.charAt(1)))
				||(esTC = cuenta.substring(0, 2).equals("20"))){
			if(esInterbancario)
			this.beneficiaryTypeAccount = esTC ? Constants.CREDIT_TYPE : cuenta.substring(0, 2);
			else
				this.beneficiaryTypeAccount = cuenta.substring(0, 2);
			return cuenta.substring(2);
		}
		else
			return cuenta;
	}
	return cuenta;
	}
	
    public Payment(String strNK, String strChargeAccount, String strBeneficiaryAccount,
            String strAmount, String strBeneficiary, String strReference, String strConcept,
            String strDescription, String strBankCode, String strOperationCode) {

        this.strNK = strNK;
        this.strChargeAccount = strChargeAccount;
        this.strBeneficiaryAccount = strBeneficiaryAccount;
        this.strAmount = strAmount;
        this.strBeneficiary = strBeneficiary;
        this.strReference = strReference;
        this.strConcept = strConcept;
        this.strDescription = strDescription;
        this.strBankCode = strBankCode;
        this.strOperationCode = strOperationCode;
        this.idNumber = "";
        this.strOperadora = "";
        esInterbancario =  strOperationCode.equals(Constants.tipoCFOtrosBancos);
        if(strOperationCode.equals(Constants.tipoCFOtrosBBVA ) || strOperationCode.equals(Constants.tipoCFCExpress ) || esInterbancario)
        	this.strBeneficiaryAccount = obtenerCuentaOrigen(strBeneficiaryAccount);
    }
	
	
	
}
