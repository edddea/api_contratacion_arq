package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.common.BmovilConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.CampaniaPaperlessDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuPrincipalDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PopUpPaperlessDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.TextoPaperless;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomer.classes.common.GuiTools;
//import suitebancomer.classes.common.ScaleTool;
import suitebancomer.classes.common.ScaleTool;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.controllers.MenuSuiteViewController;
import suitebancomer.classes.gui.delegates.MenuSuiteDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import tracking.TrackingHelper;


import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.softtoken.SofttokenApp;
import com.bancomer.mbanking.softtoken.SuiteAppApi;
import com.google.zxing.client.android.CaptureActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class PopUpPaperlessController extends BaseViewController {
	
	private ImageView logo;
	
	private RelativeLayout mainLayout;
	
	private BmovilViewsController parentManager;
	private CampaniaPaperlessDelegate delegateCP;
	// #region Variables.
	/**
	 * Delegado asociado con el controlador.
	 */
	private PopUpPaperlessDelegate ppDelegate;
	private PopUpPaperlessController ppView;
	
	private TextView tcTitulo;
	private TextView tcSubtitulo;
	private TextView tcDescripcion;
	private TextView tcFraseDelDia;
	private TextView tcPieDeMensaje;
	
	/**
	 * Boton Rechazar.
	 */
	private ImageButton btnRechazar;
	
	/**
	 * Boton Aceptar.
	 */
	private ImageButton btnAceptar;
	private Session session;
	
	public PopUpPaperlessController() {
		super();
		ppDelegate = null;
	}
	private MenuSuiteDelegate menuSuiteDelegate;
	
	// #region Ciclo de vida.
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_paperless_pop_up);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();						
		TrackingHelper.trackState("TetxoPaperles", parentManager.estados);

		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(PopUpPaperlessDelegate.TEXTO_PAPERLESS_ID));
		ppDelegate = (PopUpPaperlessDelegate)getDelegate(); 
		ppDelegate.setOwnerController(this);
		
		
		
		init();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		
	}
	// #endregion
	
	// #region Configurar pantalla.
	/**
	 * Inicializa la pantalla.
	 */
	private void init() {
		findViews();
		setTextoCampanaPaperless();
		
		scaleForCurrentScreen();
		scale();
		btnAceptar.setOnClickListener(clickListener);
		btnRechazar.setOnClickListener(clickListener);
	}
	
	private void setTextoCampanaPaperless() {
		TextoPaperless texto = ppDelegate.getTextopaperless();
		
		tcTitulo.setText(texto.getTcTitulo());
		tcSubtitulo.setText(texto.getTcSubtitulo());
		tcDescripcion.setText(texto.getTcDescripcion());
		tcFraseDelDia.setText(texto.getTcFraseDelDia());
		tcPieDeMensaje.setText(texto.getTcPieDeMensaje());
		
	}

	OnClickListener clickListener=new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(v==btnAceptar){
				session = Session.getInstance(SuiteApp.appContext);				
				if (Server.ALLOW_LOG) Log.w("getSecurityInstrument-->", session.getSecurityInstrument());
				if (Server.ALLOW_LOG) Log.w("getEstatusIS-->", session.getEstatusIS());
				if(((session.getSecurityInstrument().equals(Constants.TYPE_SOFTOKEN.S1.value) || session.getSecurityInstrument().equals(Constants.IS_TYPE_DP270) || session.getSecurityInstrument().equals(Constants.IS_TYPE_OCRA)) && (session.getEstatusIS().equals(Constants.STATUS_APP_ACTIVE)))){
					parentManager.showConfirmarPaperless();
				}else{
					muestraAlertClienteSinToken();					
				}				
				//parentManager.showConfirmarPaperless();
			}else if(v==btnRechazar){
				parentViewsController.setActivityChanging(true);
				ppDelegate.rechazoPaperless(); /**LLAMADA NACAR**/
				//ppDelegate.guardaNoAceptacion(); //
				MenuPrincipalDelegate delegate = (MenuPrincipalDelegate) parentViewsController.getBaseDelegateForKey(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID);
				delegate.restoreMenuPrincipal();
	//			finish();
/*
		    	parentViewsController.setActivityChanging(true);
		    	finish();
		    	parentViewsController.overrideScreenBackTransition();*/				
			}
		}
	};


	/**
	 * Busca la referencia a todas las subvistas necesarias.
	 */
	private void findViews() {
		tcTitulo = (TextView) findViewById(R.id.tcTitulo);
		tcSubtitulo = (TextView) findViewById(R.id.tcSubtitulo);
		tcDescripcion = (TextView) findViewById(R.id.tcDescripcion);
		tcFraseDelDia = (TextView) findViewById(R.id.tcFraseDelDia);
		tcPieDeMensaje = (TextView) findViewById(R.id.tcPieDeMensaje);
		
		btnRechazar = (ImageButton) findViewById(R.id.imgBtnRechazar);
		btnAceptar = (ImageButton) findViewById(R.id.imgBtnAceptar);
		logo = (ImageView) findViewById(R.id.imgLogo);
		mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
	}
	
	private void scale() {
		ScaleTool scaleTool = new ScaleTool(640, 640, getBaseContext());
		scaleTool.fixScaleHeight(logo, 178);
		scaleTool.fixScaleWidth(logo, 128);
		
		scaleTool.fixScaleWidth(mainLayout, 600);
		scaleTool.fixScaleHeight(mainLayout, 750);
	}
	
	/**
	 * Escala las subvistas para la pantalla actual.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		

		guiTools.scale(findViewById(R.layout.layout_paperless_pop_up));
		guiTools.scale(tcTitulo);
		guiTools.scale(tcSubtitulo);
		guiTools.scale(tcDescripcion);
		guiTools.scale(tcFraseDelDia);
		guiTools.scale(tcPieDeMensaje);
	}
	
	
	public void processNetworkResponse(int operationId, ServerResponse response) {
		ppDelegate.analyzeResponse(operationId, response);
	}
	
	public void muestraAlertClienteSinToken(){		
		String clientProfile = String.valueOf(session.getClientProfile());
		if (Server.ALLOW_LOG) Log.w("clientProfile-->", clientProfile);
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(PopUpPaperlessController.this);
	    alertDialog.setTitle(R.string.common_confirmation);	     	
	    	
	    if(clientProfile.equals(Constants.AU_TAG_BASICO) || clientProfile.equals(Constants.AU_TAG_AVANZADO))
	    	alertDialog.setMessage(getResources().getString(R.string.popup_paperless_alert_client_basic_advanced));
	    else
	    	alertDialog.setMessage(getResources().getString(R.string.popup_paperless_alert_client_recortado));

	    alertDialog.setPositiveButton(R.string.common_cancel, null);
		alertDialog.setNegativeButton(R.string.common_accept, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				SofttokenApp softtokenApp = SuiteApp.getInstance().getSofttokenApplication();
				SofttokenViewsController viewsController = softtokenApp.getSottokenViewsController();
				MenuSuiteDelegate delegate = Session.takeViewToken;

				if (SuiteApp.getSofttokenStatus()) {
					//se descomenta al migrar a administracion api
					//viewsController.setCurrentActivityApp(PopUpPaperlessController.this);

		/* invocar el api de softoken  para generar otps*/
					SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/
					ServerCommons.ALLOW_LOG= Server.ALLOW_LOG;
					ServerCommons.DEVELOPMENT=Server.DEVELOPMENT;
					ServerCommons.SIMULATION=Server.SIMULATION;
					sftoken.onCreate(SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController());
					sftoken.setIntentToReturn(new MenuSuiteViewController());



					suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate delegate2 = (suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate) sftoken.getSuiteViewsControllerApi().getBaseDelegateForKey(suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
					if (null == delegate2) {
						delegate2 = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate();
						sftoken.getSuiteViewsControllerApi().addDelegateToHashMap(
								suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,
								delegate2);
						sftoken.getSuiteViewsControllerApi().addDelegateToHashMap(suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID,
								delegate2.generaTokendelegate);
					}
					SuiteAppApi.setActivityQrToken(new CaptureActivity());
					sftoken.showViewController(suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.GeneraOTPSTViewController.class, Intent.FLAG_ACTIVITY_CLEAR_TOP, false, null, null, SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController());


					/*
					viewsController.showPantallaGeneraOTPST(false);*/
				} else {
					if(menuSuiteDelegate==null)
						menuSuiteDelegate=(MenuSuiteDelegate)SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
					if (menuSuiteDelegate.existePendienteDescarga()) {
						menuSuiteDelegate.showActivacionSTEA12();

					} else {
						menuSuiteDelegate.showActivacionSTEP();

					}
				}
			}
		});

		alertDialog.show();
	}	
}