package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class ConsultaPagoServiciosData implements ParsingHandler {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<ConsultaObtenerComprobante> movimientos;
	private String estado;
	private String codigoMensaje;
	private String descripcionMensaje;
	private String periodo;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodigoMensaje() {
		return codigoMensaje;
	}

	public void setCodigoMensaje(String codigoMensaje) {
		this.codigoMensaje = codigoMensaje;
	}

	public String getDescripcionMensaje() {
		return descripcionMensaje;
	}

	public void setDescripcionMensaje(String descripcionMensaje) {
		this.descripcionMensaje = descripcionMensaje;
	}

	public String getPeriodo() {
		return periodo;
	}

	public ArrayList<ConsultaObtenerComprobante> getMovimientos() {
		return movimientos;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	/**
	 * Constructor por defecto.
	 */
	public ConsultaPagoServiciosData() {

		estado = null;
		codigoMensaje = null;
		descripcionMensaje = null;
		periodo = null;
		movimientos = new ArrayList<ConsultaObtenerComprobante>();
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {

		estado = parser.parseNextValue("estado");
		codigoMensaje = parser.parseNextValue("codigoMensaje");
		descripcionMensaje = parser.parseNextValue("descripcionMensaje");
		if (estado.equals("OK")) {
			periodo = parser.parseNextValue("periodo");
			JSONObject aux = parser.parserNextObject("arrMovtos");
			JSONArray aux1;
			try {

				aux1 = aux.getJSONArray("ocMovtos");
				for (int i = 0; i < aux1.length(); i++) {
					ConsultaObtenerComprobante cob = new ConsultaObtenerComprobante();
					cob.setCuentaAbonoOGuia(aux1.getJSONObject(i).getString(
							"folioCIE"));
					cob.setCuentaCargoOConvenio(aux1.getJSONObject(i).getString(
							"convenio"));
					//cob.setCuentaCargoOConvenio(aux1.getJSONObject(i).getString(
						//	"cuentaAbono"));
					cob.setFecha(aux1.getJSONObject(i).getString("fecha"));
					cob.setFolio(aux1.getJSONObject(i).getString("folio"));
					cob.setHora(aux1.getJSONObject(i).getString("hora"));
					cob.setImporte(aux1.getJSONObject(i).getString("importe"));
					movimientos.add(cob);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				if(Server.ALLOW_LOG) e.printStackTrace();
			}
		}

	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub

	}

}
