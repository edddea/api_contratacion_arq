package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;



import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetalleOfertaEFIDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuPrincipalDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import tracking.TrackingHelper;


public class DetalleEFIViewController extends BaseViewController {

	private TextView lblTexto2;
	private TextView lblTexto3;
	private TextView lblDisponibletdc;
	private TextView lblTexto4;
	private TextView lblTexto5;	
	private TextView lblTexto6;
	private TextView lblTexto7;
	public TextView lblTexto8;	
	private TextView lblLinkImporte;
	private ImageButton btnAceptar;
	BaseViewController me;
	public DetalleOfertaEFIDelegate detalleOfertaEFIDelegate;
	public CuentaOrigenViewController componenteCtaOrigen;
	LinearLayout vista;
	LinearLayout vistaCtaOrigen;
	boolean isNavegarPantallaSig=false;
	boolean isBackground=false;
	private TextView lblTitleCuentaOrigen;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	//ARR
	Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_detalleefi);
		me=this;
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
			
				TrackingHelper.trackState("detalle efi", parentManager.estados);
		//AMZ		
		
		
		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(DetalleOfertaEFIDelegate.DETALLE_OFERTA_EFI_DELEGATE));
		detalleOfertaEFIDelegate = (DetalleOfertaEFIDelegate)getDelegate();
		detalleOfertaEFIDelegate.setControladorDetalleILCView(this);	
		setTitle(R.string.bmovil_promocion_title_efi, R.drawable.an_icono_efi);
		vista = (LinearLayout)findViewById(R.id.detalleofertaEFI_view_controller_layout);
		vistaCtaOrigen = (LinearLayout)findViewById(R.id.ofertaEFI_cuenta_origen_view);

		
		lblTexto2 = (TextView) findViewById(R.id.lblTexto2);
		lblTexto3 = (TextView) findViewById(R.id.lblTexto3);
		lblDisponibletdc= (TextView) findViewById(R.id.lblDisponibletdc);
		lblTexto4 = (TextView) findViewById(R.id.lblTexto4);
		lblTexto5 = (TextView) findViewById(R.id.lblTexto5);
		lblTexto6 = (TextView) findViewById(R.id.lblTexto6);
		lblTexto7 = (TextView) findViewById(R.id.lblTexto7);
		lblTexto8 = (TextView) findViewById(R.id.lblTexto8);
		lblLinkImporte=(TextView) findViewById(R.id.lblLinkImporte);
		detalleOfertaEFIDelegate.cuentaDestino();
		//modificacion
		//String txt8= "De acuerdo al saldo disponible \nque tienes en este momento en \ntu Tarjeta de Crédito, tu \nEfectivo Inmediato es de:";
		//String txt8;
		if (detalleOfertaEFIDelegate.getLeyendaInicial()==""){
			//txt8="De acuerdo al saldo disponible que\ntienes en este momento en tu\nTarjeta de Crédito, tu Efectivo\nInmediato es de:";
			//lblTexto8.setText(txt8);
			lblTexto8.setText(R.string.bmovil_detalleoferta_leyendainicial);
		}
		else{
			//txt8= "Monto del crédito solicitado:";
            //lblTexto8.setText(txt8);
			lblTexto8.setText(R.string.bmovil_detalleoferta_leyendasimulador);
		}
		
		String mystring = Tools.formatAmount(detalleOfertaEFIDelegate.getOfertaEFI().getMontoDisponible()+"00",false);
		SpannableString content = new SpannableString(mystring);
		content.setSpan(new UnderlineSpan(), 0, mystring.length(), 0);
		lblTexto7.setText(content);
		
		//String text2 ="Que se depositar� en tu cuenta \nterminaci�n "+Tools.hideAccountNumber(detalleOfertaEFIDelegate.getOfertaEFI().getAccountDestino().getNumber())+ //Cuenta de deposito
		String text2 ="A pagar en "+detalleOfertaEFIDelegate.getOfertaEFI().getPlazoMeses()+" mensualidades de\n$"+(Tools.formatAmountFromServer(detalleOfertaEFIDelegate.getOfertaEFI().getPagoMensualSimulador()) 
				+"\nque se depositará en tu cuenta \nterminación "+Tools.hideAccountNumber(detalleOfertaEFIDelegate.getOfertaEFI().getAccountDestino().getNumber()));
		String pagoM = detalleOfertaEFIDelegate.getOfertaEFI().getPlazoMeses();
		String pagoM2="$"+Tools.formatAmountFromServer(detalleOfertaEFIDelegate.getOfertaEFI().getPagoMensualSimulador());
		int startpagoM= text2.indexOf(pagoM);
		int startpagoM2=text2.indexOf(pagoM2);
		SpannableString pagomensual = new SpannableString(text2);
		pagomensual.setSpan(new ForegroundColorSpan(Color.rgb(134,200,45)),startpagoM,startpagoM+ pagoM.length(),0);
		pagomensual.setSpan(new ForegroundColorSpan(Color.rgb(134,200,45)),startpagoM2,startpagoM2+ pagoM2.length(),0);
		pagomensual.setSpan(new RelativeSizeSpan(1.5f), startpagoM, startpagoM+ pagoM.length(), 0);
		pagomensual.setSpan(new RelativeSizeSpan(1.5f), startpagoM2, startpagoM2+ pagoM2.length(), 0);
		lblTexto6.setText(pagomensual);
		
		//String txt5= "que se depositar� en tu cuenta \nterminaci�n "+Tools.hideAccountNumber(detalleOfertaEFIDelegate.getOfertaEFI().getAccountDestino().getNumber());
		//lblTexto5.setText(txt5);
		
		String textImporte= "Modificar monto";
		SpannableString textImp = new SpannableString(textImporte);
		textImp.setSpan(new UnderlineSpan(), 0, textImporte.length(), 0);

		
		lblLinkImporte.setText(textImp);
		lblLinkImporte.setOnClickListener(clickListener);
		
		
		
		
		//modificacion
				/*
				
				dispTDC = dispTDC + " que podr�s\nutilizar cuando liberes disponible\nen tu línea de crédito";
				String saldo=Tools.formatAmount(detalleOfertaEFIDelegate.getOfertaEFI().getDisponibleTDC(),false);
				int startSaldo= dispTDC.indexOf(saldo);
				SpannableString disponibleTDC = new SpannableString(dispTDC);
		
				disponibleTDC.setSpan(new RelativeSizeSpan(1.5f),startSaldo,startSaldo+saldo.length(),0);
				lblDisponibletdc.setText(disponibleTDC);
				 */
				
				//modificacion: mostrar importe de la oferta por disponible en TDC
		    	String importe= "Recuerda que tu Oferta máxima es \nhasta por "+Tools.formatAmount(detalleOfertaEFIDelegate.getOfertaEFI().getImporte()+"00", false) ;
			    importe = importe + " que podrás\nutilizar cuando liberes disponible\nen tu línea de crédito";
				String saldo=Tools.formatAmount(detalleOfertaEFIDelegate.getOfertaEFI().getImporte()+"00",false);
				int startSaldo= importe.indexOf(saldo);
				SpannableString importeOferta = new SpannableString(importe);
				importeOferta.setSpan(new RelativeSizeSpan(1.5f),startSaldo,startSaldo+saldo.length(),0);
				lblDisponibletdc.setText(importeOferta);
		
		lblTexto4.setText(R.string.bmovil_pantallaefi_texto3);		
		lblTexto3.setText("Comisión por disposición de efectivo $"+Tools.formatAmountFromServer(detalleOfertaEFIDelegate.getOfertaEFI().getComisionEFIMonto())+"\nTasa fija anual "+detalleOfertaEFIDelegate.getOfertaEFI().getTasaAnual()+"%\nCAT "+ detalleOfertaEFIDelegate.getOfertaEFI().getCat()+"%"+" Sin I.V.A informativo.\nFecha de cálculo al "+detalleOfertaEFIDelegate.getOfertaEFI().getFechaCat()+ "\nConsulta términos y condiciones en \nwww.bancomer.com.");	
		btnAceptar = (ImageButton)findViewById(R.id.detalleOferta_boton_continuar);
		btnAceptar.setOnClickListener(clickListener);
		cargarCuentas();
		configurarPantalla();
		
		}
	
	OnClickListener clickListener=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(v==btnAceptar){
				isNavegarPantallaSig=true;
				//AMZ
				Map<String,Object> OperacionMap = new HashMap<String, Object>();
				// TODO Auto-generated method stub

			
				//AMZ
				OperacionMap.put("evento_realizada","event52");
				OperacionMap.put("&&products","promociones;detalle efi+exito ilc");
				OperacionMap.put("eVar12","operacion realizada oferta efi");
				TrackingHelper.trackOperacionRealizada(OperacionMap);
				
				detalleOfertaEFIDelegate.realizaOperacion(Server.ACEPTACION_OFERTA_EFI, me);
				
				detalleOfertaEFIDelegate.setleyenda("");
			
				
				
			}else if(v==lblLinkImporte){
				//ARR
Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
				
				//ARR
				paso2OperacionMap.put("evento_paso2", "event47");
				paso2OperacionMap.put("&&products", "promociones;detalle efi");
				paso2OperacionMap.put("eVar12", "paso2:modificar importe efi");
				
				TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
				detalleOfertaEFIDelegate.showModificarImporte();
				detalleOfertaEFIDelegate.setleyenda((detalleOfertaEFIDelegate.getLeyendaInicial2()));
				
				detalleOfertaEFIDelegate.setCuentaDeposito(detalleOfertaEFIDelegate.getOfertaEFI().getAccountDestino().getNumber());
				
			}
		}
	};
	
	private void configurarPantalla() {
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());			
		gTools.scale(lblTexto2, true);
		gTools.scale(lblTexto3, true);
		gTools.scale(lblTexto4, true);
		gTools.scale(lblTexto5, true);
		gTools.scale(lblTexto6, true);
		gTools.scale(lblTexto7, true);
		gTools.scale(lblTexto8, true);
		gTools.scale(lblDisponibletdc, true);
		gTools.scale(btnAceptar);
		gTools.scale(findViewById(R.id.ofertaILC_cuenta_origen_view));
		gTools.scale(findViewById(R.id.lblLinkImporte),true);
		
	}
	
	@SuppressWarnings("deprecation")
	public void cargarCuentas(){
		ArrayList<Account> listaCuetasAMostrar = detalleOfertaEFIDelegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.setDelegate(detalleOfertaEFIDelegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(detalleOfertaEFIDelegate.getOfertaEFI().getAccountOrigen()));
		componenteCtaOrigen.init();
		componenteCtaOrigen.actualizaComponente(true);
		lblTitleCuentaOrigen=componenteCtaOrigen.getTituloComponenteCtaOrigen();
		lblTitleCuentaOrigen.setVisibility(View.GONE);
		vistaCtaOrigen.addView(componenteCtaOrigen);
	}
	
	public void goBack() {
		//ARR
		paso1OperacionMap.put("evento_paso1", "event46");
		paso1OperacionMap.put("&&products", "promociones;detalle efi");
		paso1OperacionMap.put("eVar12", "paso1:rechazo oferta efi");
		TrackingHelper.trackPaso1Operacion(paso1OperacionMap);

		// TODO Auto-generated method stub
		isNavegarPantallaSig=true;
		detalleOfertaEFIDelegate.realizaOperacion(Server.RECHAZO_OFERTA, me);
		detalleOfertaEFIDelegate.setleyenda("");
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);

	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if(!isNavegarPantallaSig){
		parentViewsController.consumeAccionesDePausa();
		isBackground=true;
		
		}
	}
	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		detalleOfertaEFIDelegate.analyzeResponse(operationId, response);
	}
	
}
