package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import android.util.Log;

import com.bancomer.mbanking.SuiteApp;

@SuppressWarnings("serial")
public class ActualizarCuentasResult  implements ParsingHandler{

	private Account[] asuntos;
	
	/**
	 * 
	 * @return lista de Accounts del usuario
	 */
	public Account[] getAsuntos() {
		return asuntos;
	}
	
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		throw new ParsingException("Invalid process.");
	}
	
	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {

		JSONArray arrayAsuntos = parser.parseNextValueWithArray("asuntos", false);
		int numAsuntos = arrayAsuntos.length();
		asuntos = new Account[numAsuntos];
		for (int i = 0; i < numAsuntos; i++) {
			
			
			try {
				JSONObject asuntoObj = arrayAsuntos.getJSONObject(i);
				String tipoCuenta = asuntoObj.getString("tipoCuenta");
				String alias = asuntoObj.getString("alias");
				String divisa = asuntoObj.getString("divisa");
				String asunto = asuntoObj.getString("asunto");
				String saldo = asuntoObj.getString("saldo");
				

				String concepto = asuntoObj.getString("concepto");
				String visible = asuntoObj.getString("visible");
				String celularAsociado = asuntoObj.getString("celularAsociado");
				String codigoCompania = asuntoObj.getString("codigoCompania");
				String descripcionCompania = asuntoObj.getString("descripcionCompania");
				String fechaUltimaModificacion = asuntoObj.getString("fechaUltimaModificacion");
				String indicadorSPEI = asuntoObj.getString("indicadorSPEI");
				
					//if(tipoCuenta =="LI" || tipoCuenta =="AH" || tipoCuenta =="CH"
						//|| tipoCuenta =="CE" || tipoCuenta =="TC" || tipoCuenta =="TP" ){
				Session session = Session.getInstance(SuiteApp.appContext);
				String date = Tools.dateForReference(session.getServerDate());
				if(Server.ALLOW_LOG) Log.e("value of this parser",String.valueOf(Double.valueOf(saldo)));
				
				asuntos[i] = new Account(asunto,
						Tools.getDoubleAmountFromServerString(saldo), 						
						Tools.formatDate(date), "S".equals(visible), divisa,
						tipoCuenta, concepto, alias, celularAsociado, 
						codigoCompania, descripcionCompania, fechaUltimaModificacion , indicadorSPEI);
				
				//System.out.println("array"+asuntos);

					//}
			} catch (JSONException e) {
				if(Server.ALLOW_LOG) throw new ParsingException("Error formato");
			}

		}
	}

}
