package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.Hashtable;
import java.util.regex.Pattern;

import suitebancomer.aplicaciones.bmovil.classes.common.BmovilConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagoTdcDelegate.PagoDeTarjeta;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.CompraTiempoAire;
import suitebancomer.aplicaciones.bmovil.classes.model.CompraTiempoAireResult;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDepositosRecbidosTransDeOtrosBancos;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaDepositosRecibidosChequesEfectivo;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaObtenerComprobante;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaSPEIData;
import suitebancomer.aplicaciones.bmovil.classes.model.DetalleInterbancaria;
import suitebancomer.aplicaciones.bmovil.classes.model.EnvioEmail;
import suitebancomer.aplicaciones.bmovil.classes.model.PagoServicio;
import suitebancomer.aplicaciones.bmovil.classes.model.PagoServicioResult;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjeta;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjetaResult;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferResult;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovil;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovilData;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaInterbancaria;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaInterna;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaOtrosBBVA;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaSPEI;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferirCuentasPropiasData;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;

import static suitebancomer.aplicaciones.bmovil.classes.common.Tools.formatDate;
import static suitebancomer.aplicaciones.bmovil.classes.common.Tools.formatTime;

public class EnviarCorreoDelegate extends DelegateBaseAutenticacion {
	// #region Variables.
	/**
	 * Identificador �nico del delegado. 
	 */
	public static long ENVIAR_CORREO_DELEGATE_ID= 5769131963703731822L;
	
	/**
	 * Email matching pattern regex.
	 */
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	/**
	 * Controlador asociado al delegado.
	 */
	private BaseViewController ownerController;
	
	/**
	 * Delegado de la pantalla de resultados.
	 */
	private ResultadosDelegate resultadosDelegate;
	//O3
	private DelegateBaseOperacion operationDelegate;
	
	/**
	 * Modelo de datos para la transaccion a NACAR.
	 */
	private EnvioEmail model;
	
	/**
	 * Bandera para indicar si el correo se deben enviar tanto al beneficiario como al usuario.
	 */
	private boolean enviarParaAmbos;
	
	/**
	 * Bandera para determinar si hubo alg�n error en el env�o del mensaje.
	 */
	private boolean errorEnEnvio;
	// #endregion
	
	// #region Getters y Setters.
	/**
	 * @return Controlador asociado al delegado.
	 */
	public BaseViewController getOwnerController() {
		return ownerController;
	}

	/**
	 * @param ownerController Controlador asociado al delegado.
	 */
	public void setOwnerController(BaseViewController ownerController) {
		this.ownerController = ownerController;
	}

	@Override
	public long getDelegateIdentifier() {
		return ENVIAR_CORREO_DELEGATE_ID;
	}
	
	/**
	 * @return Delegado de la pantalla de resultados.
	 */
	public ResultadosDelegate getResultadosDelegate() {
		return resultadosDelegate;
		//return operationDelegate;
	}

	/**
	 * @param resultadosDelegate Delegado de la pantalla de resultados.
	 */
	public void setResultadosDelegate(ResultadosDelegate resultadosDelegate) {
		this.resultadosDelegate = resultadosDelegate;
	}
	// #endregion

	public EnviarCorreoDelegate() {
		model = null;
		ownerController = null;
//		resultadosDelegate = 
//				(ResultadosDelegate)SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().
//				getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
	}
	
	/**
	 * 
	 */
	public void validaDatos(boolean enviarABeneficiario, boolean enviarAMi, String correoBeneficiario, String mensaje) {
		if(null == resultadosDelegate || null == correoBeneficiario || null == mensaje)
		//if(null == operationDelegate || null == correoBeneficiario || null == mensaje)
			return;
		
		if(!enviarABeneficiario && !enviarAMi) {
			ownerController.showInformationAlert(R.string.bmovil_enviar_correo_error_sin_opciones_de_envio);
			return;
		} else if(enviarABeneficiario) {
			if(0 == correoBeneficiario.length()) {
				ownerController.showInformationAlert(R.string.bmovil_enviar_correo_error_beneficiario_vacio);
				return;
			} else if(!Pattern.compile(EMAIL_PATTERN).matcher(correoBeneficiario).matches()) {
				ownerController.showInformationAlert(R.string.bmovil_enviar_correo_error_beneficiario_invalido);
				return;
			}
		}
		
		enviarParaAmbos = false;
		
		model = new EnvioEmail();
		model.setCorreoBeneficiario(enviarABeneficiario ? correoBeneficiario : "");
		model.setCorreoUsuario(enviarAMi ? Session.getInstance(SuiteApp.appContext).getEmail() : "");
		model.setMensaje(mensaje);
		
		Hashtable<String, String> params = null;
		
		if(enviarABeneficiario && enviarAMi) {
			params = armarTramaDeEnvioDecider(model.getCorreoBeneficiario(), mensaje);
			enviarParaAmbos = true;
		} else if(enviarABeneficiario) {
			params = armarTramaDeEnvioDecider(model.getCorreoBeneficiario(), mensaje);
		} else if(enviarAMi) {
			params = armarTramaDeEnvioDecider(model.getCorreoUsuario(), mensaje);
		}
		
		if (Server.ALLOW_LOG) Log.d(getClass().getSimpleName(), "Parametros para envio de Email: " + ((params == null) ? null :  params.toString()));
		
		errorEnEnvio = false;
		if (null != params) {
			if (resultadosDelegate.getOperationDelegate() instanceof ConsultaMovimientosInterbancariosDelegate) {
				doNetworkOperation(Server.OP_ENVIOCORREO_SPEI, params,true, null,isJsonValueCode.NONE, ownerController);
			} else{
				doNetworkOperation(Server.OP_ENVIO_CORREO, params,true, null,isJsonValueCode.NONE, ownerController);
			}
		}
	}

	private Hashtable<String, String> armarTramaDeEnvioDecider(String email, String mensaje){
		if(resultadosDelegate.getOperationDelegate() instanceof ConsultaMovimientosInterbancariosDelegate){
			return armarTramaDeEnvioSPEI( email, mensaje);
		}else{
			return armarTramaDeEnvio(email, mensaje);
		}
	}

	private Hashtable<String, String> armarTramaDeEnvioSPEI(String email, String mensaje) {
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> params = new Hashtable<String, String>();

		ConsultaMovimientosInterbancariosDelegate delegate = (ConsultaMovimientosInterbancariosDelegate)resultadosDelegate.getOperationDelegate();
		ConsultaSPEIData data = delegate.getConsultaSPEIData();
		TransferenciaSPEI transfer = delegate.getTransferenciaSeleccionada();

		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());

		params.put(ServerConstants.PERIODO, data.getPeriodoConsulta());
		params.put(ServerConstants.NUMERO_DE_REFERENCIA, transfer.getNumeroDeReferencia());
		params.put(ServerConstants.CORREO_DESTINO, email);
		params.put(ServerConstants.CUENTA, delegate.getCuentaSeleccionada().getFullNumber());
		params.put(ServerConstants.TIPO_COMPROBANTE, "consultaSPEI");
		params.put(ServerConstants.MENSAJE_A, Tools.removeSpecialCharacters(mensaje));

		return params;
	}
	
	/**
	 * Arma la trama de env�o para el servidor.
	 * @param email El email a utilizar.
	 * @param mensaje TODO
	 * @return La lista de parametros para el servidor.
	 */
	private Hashtable<String, String> armarTramaDeEnvio(String email, String mensaje) {
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> params = new Hashtable<String, String>();
		
		// Comunes
		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		//params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		params.put("tipoComprobante", "");
		//params.put(Server.SUBJECT_PARAM, "");
		params.put("correoDestino", email);
		//params.put(Server.CLIENTNAME_PARAM, session.getNombreCliente());
		params.put("mensajeA", Tools.removeSpecialCharacters(mensaje));
		
		// Especificos de cada operación.
		/*params.put(Server.ORIGIN_PARAM, "");
		params.put(Server.CELLPHONENUMBER_PARAM, "");
		params.put(ServerConstants.COMPANIA_CELULAR, "");
		params.put(Server.AMOUNT_PARAM, "");
		params.put(Server.OPERATION_DATE_PARAM, "");
		params.put(Server.OPERATION_HOUR_PARAM, "");*/
		params.put("folioOperacion", "");
		params.put("codTrans", "");
		params.put("beneficiario", "");
		params.put("concepto", "");
		params.put("versionFlujo", Constants.APPLICATION_VERSION);
		
		if(resultadosDelegate.getOperationDelegate() instanceof InterbancariosDelegate) {
			//if(operationDelegate.getOperationDelegate() instanceof InterbancariosDelegate) {
			CargarDatosDeInterbancaria(params);
		}else if(resultadosDelegate.getOperationDelegate() instanceof AltaRetiroSinTarjetaDelegate) {
			CargarDatosDeAltaRetiroSinTarjeta(params); 
		}else if(resultadosDelegate.getOperationDelegate() instanceof DineroMovilDelegate) {
		//} else if(operationDelegate.getOperationDelegate() instanceof DineroMovilDelegate) {
			CargarDatosDeDineroMovil(params);
		} else if(resultadosDelegate.getOperationDelegate() instanceof TiempoAireDelegate) {
		//} else if(operationDelegate.getOperationDelegate() instanceof TiempoAireDelegate) {
			CargarDatosDeCompraDeTA(params);
		} else if(resultadosDelegate.getOperationDelegate() instanceof PagoServiciosDelegate) {
			//} else if(operationDelegate.getOperationDelegate() instanceof PagoServiciosDelegate) {
			CargarDatosDePagoDeServicios(params);
		} else if(resultadosDelegate.getOperationDelegate() instanceof OtrosBBVADelegate) {
			//} else if(operationDelegate.getOperationDelegate() instanceof OtrosBBVADelegate) {
			OtrosBBVADelegate tempDel = (OtrosBBVADelegate)resultadosDelegate.getOperationDelegate();
			if(tempDel.isEsExpress())
				CargarDatosDeCuentaExpress(params);
			else
				CargarDatosDeTerceros(params);
		}else if(resultadosDelegate.getOperationDelegate() instanceof PagoTdcDelegate) {
			CargarDatosPagoTDC(params);	
		}else if(resultadosDelegate.getOperationDelegate() instanceof TransferirMisCuentasDelegate) {
			//}else if(operationDelegate.getOperationDelegate() instanceof TransferirMisCuentasDelegate) {
			CargarDatosDeEntreMisCuentas(params);
		}else if(resultadosDelegate.getOperationDelegate() instanceof PagoTdcDelegate) {//modificacion
			CargarDatosDeMisTarjetas(params); 
		}
		else if(resultadosDelegate.getOperationDelegate() instanceof ConsultaInterbancariosDelegate) {
		//} else if(operationDelegate.getOperationDelegate() instanceof ConsultaInterbancariosDelegate) {
			CargarDatosConsultaInterbancaria(params);
		}else { 
			return null;
		}
		
		if(!Tools.isEmptyOrNull(params.get(Server.ORIGIN_PARAM))) {
			params.put(Server.ORIGIN_PARAM, Tools.hideAccountNumber(params.get(Server.ORIGIN_PARAM)));
		}
		if(!Tools.isEmptyOrNull(params.get(Server.ACCOUNT_PARAM))) {
			params.put(Server.ACCOUNT_PARAM, Tools.hideAccountNumber(params.get(Server.ACCOUNT_PARAM)));
		}
		
		ajustarLongitudDeParametros(params);
		
		return params;
	}
	
	/**
	 * Ajusta la longitud de todos los parametros para que no rebasen lo establecido.
	 * @param params La tabla de parametros por ajustar.
	 */
	private void ajustarLongitudDeParametros(Hashtable<String, String> params) {
		ajustarLongitudDeParametro(params, ServerConstants.NUMERO_TELEFONO,10);
		ajustarLongitudDeParametro(params, "folioOperacion",10);
//		ajustarLongitudDeParametro(params, Server.SECURITY_CODE_PARAM, 	4);
		ajustarLongitudDeParametro(params, "beneficiario", 	16);
		ajustarLongitudDeParametro(params, "concepto", 		16);
		ajustarLongitudDeParametro(params, "correoDestino", 			50);
		ajustarLongitudDeParametro(params, "mensajeA", 			250);
	}
	
	/**
	 * Ajusta la longitud del parametro indicado en la tabla de parametros, si la longitud actual es mayor a la indicada  el campo es truncado.
	 * @param params La tabla de par�metros.
	 * @param property La propiedad a verificar.
	 * @param maxLenght La longitud m�xima.
	 */
	private void ajustarLongitudDeParametro(Hashtable<String, String> params, String property, int maxLenght) {
		String value = "";
		value = params.get(property);
		// Dejar los primeros caracteres.
		if(value.length() > maxLenght)
			params.put(property, value.substring(0, maxLenght));
		
		// Dejar los ultimos caracteres.
//		if(value.length() > maxLenght)
//			params.put(property, value.substring(value.length() - maxLenght));
	}

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#doNetworkOperation(int, java.util.Hashtable, suitebancomer.classes.gui.controllers.BaseViewController)
	 */
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson, ParsingHandler handler,isJsonValueCode isJsonValueCode, BaseViewController caller) {
		SuiteApp.getInstance().getBmovilApplication().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller);
	}

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#analyzeResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if(ServerResponse.OPERATION_SUCCESSFUL == response.getStatus()) {
			if((Server.OP_ENVIO_CORREO == operationId)||(Server.OP_ENVIOCORREO_SPEI == operationId)) {
				if(enviarParaAmbos) {
					enviarParaAmbos = false;
					Hashtable<String, String> params = armarTramaDeEnvioDecider(model.getCorreoUsuario(), model.getMensaje());
					if(null != params) {
						if(resultadosDelegate.getOperationDelegate() instanceof ConsultaMovimientosInterbancariosDelegate){
							doNetworkOperation(Server.OP_ENVIOCORREO_SPEI, params,true, null,isJsonValueCode.NONE, ownerController);
						}else{
							doNetworkOperation(Server.OP_ENVIO_CORREO, params,true, null,isJsonValueCode.NONE, ownerController);
						}
					}
				} else {
					if(errorEnEnvio) {
						ownerController.showInformationAlert(R.string.bmovil_enviar_correo_error);
					} else {
						ownerController.showInformationAlert(R.string.bmovil_enviar_correo_exito, new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog, int which) {
								regresarAResultados();
							}
						});
					}
				}
			}
		} else {
			if((Server.OP_ENVIO_CORREO == operationId)||(Server.OP_ENVIOCORREO_SPEI == operationId)){
				if(enviarParaAmbos) {
					enviarParaAmbos = false;
					Hashtable<String, String> params = armarTramaDeEnvioDecider(model.getCorreoUsuario(), model.getMensaje());
					if(null != params) {
						if (resultadosDelegate.getOperationDelegate() instanceof ConsultaMovimientosInterbancariosDelegate) {
							doNetworkOperation(Server.OP_ENVIOCORREO_SPEI, params,true, null,isJsonValueCode.NONE, ownerController);
						} else {
							doNetworkOperation(Server.OP_ENVIO_CORREO, params,true, null,isJsonValueCode.NONE, ownerController);
						}
					}
				}
				errorEnEnvio = true;
			} else {
				ownerController.showInformationAlert(response.getMessageText());
			}
		}
	}
	
	public void regresarAResultados() {
		ownerController.goBack();
	}
	
	/**
	 * Carga los datos especificos de una Consulta Interbancaria
	 * @param params La tabla de parametros a llenar.
	 */
	private void CargarDatosConsultaInterbancaria(Hashtable<String, String> params) {
		ConsultaInterbancariosDelegate opDelegate = (ConsultaInterbancariosDelegate)resultadosDelegate.getOperationDelegate();
		DetalleInterbancaria operation = opDelegate.getDetalleInterbancaria();
//		TransferenciaInterbancariaResult result = opDelegate.getResult();
		
		params.put("tipoComprobante", "interbancaria");
/*		params.put(Server.ORIGIN_PARAM, operation.getCuentaOrigen());
		params.put(Server.AMOUNT_PARAM, Tools.formatAmount(operation.getImporte(),false));
		params.put(Server.OPERATION_DATE_PARAM, formatDate(operation.getFechaAplicacion()));
		params.put(Server.OPERATION_HOUR_PARAM, formatTime(operation.getHoraAlta()));*/
		params.put("folioOperacion", operation.getFolio());
		params.put("beneficiario", operation.getNombreBeneficiario());
		params.put("concepto", operation.getDescripcion());
/*		params.put(Server.BANK_PARAM, operation.getNombreBanco());
		params.put(Server.ACCOUNT_PARAM, operation.getCuentaDestino());
		params.put(Server.REFERENCE_PARAM, operation.getReferencia());*/
	}

	/**
	 * Carga los datos especificos de una Transferencia Interbancaria
	 * @param params La tabla de parametros a llenar.
	 */
	private void CargarDatosDeInterbancaria(Hashtable<String, String> params) {
		InterbancariosDelegate opDelegate = (InterbancariosDelegate)resultadosDelegate.getOperationDelegate();
		TransferenciaInterbancaria operation = opDelegate.getTransferenciaInterbancaria();
		TransferResult result = opDelegate.getResult();

		params.put("bancoDestino", operation.getNombreBanco());
		params.put("tipoComprobante", "interbancaria");
/*		params.put(Server.ORIGIN_PARAM, operation.getCuentaOrigen().getNumber());
		params.put(Server.AMOUNT_PARAM, Tools.formatAmount(operation.getImporte(),false));
		params.put(Server.OPERATION_DATE_PARAM, formatDate(result.getFecha()));
		params.put(Server.OPERATION_HOUR_PARAM, formatTime(result.getHora()));*/
		params.put("folioOperacion", result.getFolio());
		//SPEI
		params.put("beneficiario", Tools.removeSpecialCharacters(operation.getBeneficiario()));
		params.put("concepto", Tools.removeSpecialCharacters(operation.getConcepto()));
		//Termina SPEI
		params.put(ServerConstants.CODIGO_TRANSACCION, result.getCodigoTrans());
	}
	//O3
	private void CargarDatosDeEntreDepositosRecibidos(Hashtable<String, String> params) {
		// TODO: A falta de duda para el correcto seteo de parametros
		DepositosRecibidosDelegate delegate = (DepositosRecibidosDelegate)operationDelegate;
		
		if((delegate.getMovimientosDR() != null) && (!delegate.getMovimientosDR().isEmpty())){
			Object movimiento = delegate.getMovimientosDR().get(delegate.getMovimientoSeleccionadoDR());
			
			if (delegate.getTipoOperacionActual() == Server.CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO) {
				params.put(Server.AMOUNT_PARAM, Tools.formatAmount(((ConsultaDepositosRecibidosChequesEfectivo) movimiento).getImporte(),false));
				params.put(Server.OPERATION_DATE_PARAM, formatDate(((ConsultaDepositosRecibidosChequesEfectivo) movimiento).getFecha()));
				params.put(Server.OPERATION_HOUR_PARAM, formatTime(((ConsultaDepositosRecibidosChequesEfectivo) movimiento).getHora()));
				params.put(Server.FOLIO_OPERACION_PARAM, "");
				params.put(Server.ACCOUNT_PARAM, "");
				
			} else if (delegate.getTipoOperacionActual() == Server.CONSULTA_DEPOSITOS_CHEQUES) {
				params.put(Server.AMOUNT_PARAM, Tools.formatAmount( ((ConsultaDepositosRecibidosChequesEfectivo)movimiento).getImporte(),false));
				params.put(Server.OPERATION_DATE_PARAM, formatDate(((ConsultaDepositosRecibidosChequesEfectivo)movimiento).getFecha()));
				params.put(Server.OPERATION_HOUR_PARAM, formatTime(((ConsultaDepositosRecibidosChequesEfectivo)movimiento).getHora()));
				params.put(Server.FOLIO_OPERACION_PARAM, "");
				params.put(Server.ACCOUNT_PARAM,"");			
				
			} else if (delegate.getTipoOperacionActual() == Server.CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS) {
				params.put(Server.AMOUNT_PARAM, Tools.formatAmount(((ConsultaDepositosRecbidosTransDeOtrosBancos)movimiento).getImporte(),false));
				params.put(Server.OPERATION_DATE_PARAM, formatDate(((ConsultaDepositosRecbidosTransDeOtrosBancos)movimiento).getFecha()));
				params.put(Server.OPERATION_HOUR_PARAM, formatTime(((ConsultaDepositosRecbidosTransDeOtrosBancos)movimiento).getHora()));
				params.put(Server.FOLIO_OPERACION_PARAM, "");
				params.put(Server.ACCOUNT_PARAM, "");		
			}
			
		}
	}
	
	private void CargarDatosDeEntreObtenerComprobantes(Hashtable<String, String> params){
		ObtenerComprobantesDelegate delegate = (ObtenerComprobantesDelegate)operationDelegate;
		
		if((delegate.getMovimientosOC() != null) && (!delegate.getMovimientosOC().isEmpty())){
			//TODO: A falta de duda para el correcto seteo de parametros
			ConsultaObtenerComprobante movimiento = delegate.getMovimientosOC().get(delegate.getMovimientoSeleccionadoOC());
			params.put(Server.AMOUNT_PARAM, Tools.formatAmount(movimiento.getImporte(),false));
			params.put(Server.OPERATION_DATE_PARAM, formatDate(movimiento.getFecha()));
			params.put(Server.OPERATION_HOUR_PARAM, formatTime(movimiento.getHora()));
			params.put(Server.FOLIO_OPERACION_PARAM, movimiento.getFolio());
			
			if(delegate.isTransferencia()){
				params.put(Server.ACCOUNT_PARAM, movimiento.getCuentaCargoOConvenio());
			}else{
				params.put(Server.ACCOUNT_PARAM, movimiento.getCuentaAbonoOGuia());
			}
		}
	}
	
	/**
	 * Carga los datos especificos de una Transferencia De Dinero Movil
	 * @param params La tabla de parametros a llenar.
	 */
	private void CargarDatosDeDineroMovil(Hashtable<String, String> params) {
		DineroMovilDelegate opDelegate = (DineroMovilDelegate)resultadosDelegate.getOperationDelegate();
		TransferenciaDineroMovil operation = opDelegate.getTransferencia();
		TransferenciaDineroMovilData result = opDelegate.getServerResponse();
		
		params.put("tipoComprobante", "dineroMovil");
		params.put("folioOperacion", result.getFolio());
		//SPEI
		params.put("beneficiario", Tools.removeSpecialCharacters(operation.getBeneficiario()));
		params.put(Server.CONCEPTO_PARAM, Tools.removeSpecialCharacters(operation.getConcepto()));

	}
	
	/**
	 * Carga los datos especificos de una Transferencia De Alta Retiro Sin Tarjeta
	 * @param params La tabla de parametros a llenar.
	 */
	private void CargarDatosDeAltaRetiroSinTarjeta(Hashtable<String, String> params) {
		AltaRetiroSinTarjetaDelegate opDelegate = (AltaRetiroSinTarjetaDelegate)resultadosDelegate.getOperationDelegate();
		RetiroSinTarjeta operation = opDelegate.getModeloRetiroSinTarjeta();
		RetiroSinTarjetaResult result = opDelegate.getRetiroSinTarjetaResult();
		
		params.put("tipoComprobante", "retiroSinTarjeta");
		params.put("folioOperacion", result.getFolio());
		//SPEI
		params.put("beneficiario", Tools.removeSpecialCharacters(operation.getBeneficiario()));
		params.put("concepto", Tools.removeSpecialCharacters(operation.getConcepto()));

	}
	
	/**
	 * Carga los datos especificos de una Compra De Tiempo Aire
	 * @param params La tabla de parametros a llenar.
	 */
	private void CargarDatosDeCompraDeTA(Hashtable<String, String> params) {
		TiempoAireDelegate opDelegate = (TiempoAireDelegate)resultadosDelegate.getOperationDelegate();
		CompraTiempoAire operation = opDelegate.getCompraTiempoAire();
		CompraTiempoAireResult result = opDelegate.getResult();
		
		params.put("tipoComprobante", "compraTA");
		params.put("folioOperacion", result.getFolio());
	}
	
	/**
	 * Carga los datos especificos de un Pago De Servicios
	 * @param params La tabla de parametros a llenar.
	 */
	private void CargarDatosDePagoDeServicios(Hashtable<String, String> params) {
		PagoServiciosDelegate opDelegate = (PagoServiciosDelegate)resultadosDelegate.getOperationDelegate();
		PagoServicio operation = opDelegate.getPagoServicio();
		PagoServicioResult result = opDelegate.getResult();
		
		params.put("tipoComprobante", "pagoServicio");
		params.put("folioOperacion", result.getFolio());
		//SPEI
		params.put("concepto", Tools.removeSpecialCharacters(operation.getConceptoServicio()));
		params.put(ServerConstants.CODIGO_TRANSACCION, result.getCodTrans());

	}
	
	/**
	 * Carga los datos especificos de una Transferencia A Terceros
	 * @param params La tabla de parametros a llenar.
	 */
	private void CargarDatosDeTerceros(Hashtable<String, String> params) {
		OtrosBBVADelegate opDelegate = (OtrosBBVADelegate)resultadosDelegate.getOperationDelegate();
		TransferenciaOtrosBBVA operation = opDelegate.getTransferenciaOtrosBBVA();
		TransferResult result = opDelegate.getResult();
		
		params.put("tipoComprobante", "otrosBancomer");
		params.put("folioOperacion", result.getFolio());
		params.put("beneficiario", Tools.removeSpecialCharacters(operation.getBeneficiario()));
		params.put(ServerConstants.CODIGO_TRANSACCION, result.getCodigoTrans());

	}
	
	/**
	 * Carga los datos especificos de una Transferencia A Terceros
	 * @param params La tabla de parametros a llenar.
	 */
	private void CargarDatosPagoTDC(Hashtable<String, String> params) {
		PagoTdcDelegate opDelegate = (PagoTdcDelegate)resultadosDelegate.getOperationDelegate();
		
		PagoDeTarjeta dataTDC = opDelegate.getdataTDC();
		
		TransferirCuentasPropiasData resultado = opDelegate.getResultado();
		
		params.put("tipoComprobante", "otrosBancomer");
		params.put("folioOperacion", resultado.getReference());
		params.put("beneficiario", Tools.removeSpecialCharacters(" "));
		params.put(ServerConstants.CODIGO_TRANSACCION, resultado.getCodTrans());
	}
	
	/**
	 * Carga los datos especificos de una Transferencia A Cuenta Express
	 * @param params La tabla de parametros a llenar.
	 */
	private void CargarDatosDeCuentaExpress(Hashtable<String, String> params) {
		OtrosBBVADelegate opDelegate = (OtrosBBVADelegate)resultadosDelegate.getOperationDelegate();
		TransferenciaOtrosBBVA operation = opDelegate.getTransferenciaOtrosBBVA();
		TransferResult result = opDelegate.getResult();
		
		params.put("tipoComprobante", "cuentaExpress");
		params.put("folioOperacion", result.getFolio());
		params.put("beneficiario", Tools.removeSpecialCharacters(operation.getBeneficiario()));
		params.put(ServerConstants.CODIGO_TRANSACCION, result.getCodigoTrans());

	}
	
	/**
	 * Carga los datos especificos de una Transferencia A Terceros
	 * @param params La tabla de parametros a llenar.
	 */
	private void CargarDatosDeEntreMisCuentas(Hashtable<String, String> params) {
		TransferirMisCuentasDelegate opDelegate = (TransferirMisCuentasDelegate)resultadosDelegate.getOperationDelegate();
		TransferenciaInterna operation = opDelegate.getInnerTransaction();
		TransferirCuentasPropiasData result = opDelegate.getResultado();
		
		params.put("tipoComprobante", "otrosBancomer");
		params.put("folioOperacion", result.getReference());
		params.put("beneficiario", SuiteApp.appContext.getString(R.string.bmovil_common_titular_de_la_cuenta));
		params.put(ServerConstants.CODIGO_TRANSACCION, result.getCodTrans());
		
	}
	
	private void CargarDatosDeMisTarjetas(Hashtable<String, String> params) {
		PagoTdcDelegate opDelegate = (PagoTdcDelegate)resultadosDelegate.getOperationDelegate();
		//TransferenciaInterna operation = opDelegate.getInnerTransaction();
		TransferirCuentasPropiasData result = opDelegate.getResultado();
		
		params.put("tipoComprobante", "otrosBancomer");
		//params.put(Server.ORIGIN_PARAM, operation.getCuentaOrigen().getNumber());
		params.put(Server.ORIGIN_PARAM,opDelegate.getCuentaRetiro());//cuenta origen
		//params.put(Server.AMOUNT_PARAM, Tools.formatAmount(operation.getImporte().replaceAll("[.]", ""),false));
		params.put(Server.AMOUNT_PARAM, Tools.formatAmount(opDelegate.getImporte(),false));//importe

		params.put(Server.OPERATION_DATE_PARAM, formatDate(result.getServerDate()));
		params.put(Server.OPERATION_HOUR_PARAM, formatTime(result.getServerTime()));
		params.put("folioOperacion", result.getReference());
	//	params.put(Server.ACCOUNT_PARAM, operation.getCuentaDestino().getNumber());
		//params.put(Server.ACCOUNT_PARAM, opDelegate.getCuentaTDC().getNumber());//cuenta destino TDC
		params.put(Server.ACCOUNT_PARAM, opDelegate.getCuentaTDC().getNumber());//cuenta destino TDC
		params.put("beneficiario", SuiteApp.appContext.getString(R.string.bmovil_common_titular_de_la_cuenta));
		params.put(ServerConstants.CODIGO_TRANSACCION, result.getCodTrans());
	}
}
