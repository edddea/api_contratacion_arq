package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

@SuppressWarnings("serial")
public class CompraTiempoAireResult implements ParsingHandler {

	String cuentaCargo;
	String folio;
	String saldoActual;
	String fecha;
	String hora;
	
	public String getCuentaCargo() {
		return cuentaCargo;
	}

	public String getFolio() {
		return folio;
	}

	public String getSaldoActual() {
		return saldoActual;
	}

	public String getFecha() {
		return fecha;
	}

	public String getHora() {
		return hora;
	}
	
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		this.cuentaCargo = parser.parseNextValue("AS");
		this.saldoActual = parser.parseNextValue("IM");
		if(this.saldoActual != null)
			this.saldoActual = this.saldoActual.replace(",", "").replace(".", "");
		this.folio = parser.parseNextValue("FO");
		this.fecha = parser.parseNextValue("FE");
		this.hora = parser.parseNextValue("HR");
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {

	}

}
