package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class EnvioEmailData implements ParsingHandler {
	/**
	 * Folio de la operación.
	 */
	private String folio;
	
	/**
	 * @return Folio de la operación.
	 */
	public String getFolio() {
		return folio;
	}
	
	public EnvioEmailData() {
		folio = null;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		folio = null;
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		folio = parser.parseNextValue("folio");
	}
}
