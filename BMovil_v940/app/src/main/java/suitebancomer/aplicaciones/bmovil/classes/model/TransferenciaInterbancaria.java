package suitebancomer.aplicaciones.bmovil.classes.model;


public class TransferenciaInterbancaria extends FrecuenteMulticanalData{

	private Account cuentaOrigen;
	private String institucionBancaria;
	private String tipoCuentaDestino;
	private String monedaCuentaDestino;
	private String numeroCuentaDestino;
	private String beneficiario;
	private String referencia;
	private String concepto;
	private String importe;
	private String aliasFrecuente;
	private String tipoOperacion;
	private String nombreBanco;
	private String frecuenteMulticanal;
	
	public TransferenciaInterbancaria(){
		
	}
	
	public TransferenciaInterbancaria(Account cuentaOrigen,
			String institucionBancaria,  String numeroCuentaDestino,
			String beneficiario, String referencia, String concepto,
			String importe, String aliasFrecuente, String tipoOperacion,
			String nombreBanco,String tipoCuentaDestino) {
		super();
		this.cuentaOrigen = cuentaOrigen;
		this.institucionBancaria = institucionBancaria;
		this.numeroCuentaDestino = numeroCuentaDestino;
		this.beneficiario = beneficiario;
		this.referencia = referencia;
		this.concepto = concepto;
		this.importe = importe;
		this.aliasFrecuente = aliasFrecuente;
		this.tipoOperacion = tipoOperacion;
		this.nombreBanco = nombreBanco;
		this.tipoCuentaDestino = tipoCuentaDestino;
	}


	/**
	 * @return the cuentaOrigen
	 */
	public Account getCuentaOrigen() {
		return cuentaOrigen;
	}


	/**
	 * @param cuentaOrigen the cuentaOrigen to set
	 */
	public void setCuentaOrigen(Account cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}


	/**
	 * @return the institucionBancaria
	 */
	public String getInstitucionBancaria() {
		return institucionBancaria;
	}


	/**
	 * @param institucionBancaria the institucionBancaria to set
	 */
	public void setInstitucionBancaria(String institucionBancaria) {
		this.institucionBancaria = institucionBancaria;
	}


	/**
	 * @return the tipoCuentaDestino
	 */
	public String getTipoCuentaDestino() {
		return tipoCuentaDestino;
	}


	/**
	 * @param tipoCuentaDestino the tipoCuentaDestino to set
	 */
	public void setTipoCuentaDestino(String tipoCuentaDestino) {
		this.tipoCuentaDestino = tipoCuentaDestino;
	}


	/**
	 * @return the monedaCuentaDestino
	 */
	public String getMonedaCuentaDestino() {
		return monedaCuentaDestino;
	}


	/**
	 * @param monedaCuentaDestino the monedaCuentaDestino to set
	 */
	public void setMonedaCuentaDestino(String monedaCuentaDestino) {
		this.monedaCuentaDestino = monedaCuentaDestino;
	}


	/**
	 * @return the numeroCuentaDestino
	 */
	public String getNumeroCuentaDestino() {
		return numeroCuentaDestino;
	}


	/**
	 * @param numeroCuentaDestino the numeroCuentaDestino to set
	 */
	public void setNumeroCuentaDestino(String numeroCuentaDestino) {
		this.numeroCuentaDestino = numeroCuentaDestino;
	}


	/**
	 * @return the beneficiario
	 */
	public String getBeneficiario() {
		return beneficiario;
	}


	/**
	 * @param beneficiario the beneficiario to set
	 */
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}


	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}


	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}


	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}


	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}


	/**
	 * @return the importe
	 */
	public String getImporte() {
		return importe;
	}


	/**
	 * @param importe the importe to set
	 */
	public void setImporte(String importe) {
		this.importe = importe;
	}


	/**
	 * @return the aliasFrecuente
	 */
	public String getAliasFrecuente() {
		return aliasFrecuente;
	}


	/**
	 * @param aliasFrecuente the aliasFrecuente to set
	 */
	public void setAliasFrecuente(String aliasFrecuente) {
		this.aliasFrecuente = aliasFrecuente;
	}


	/**
	 * @return the tipoOperacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}


	/**
	 * @param tipoOperacion the tipoOperacion to set
	 */
	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}


	/**
	 * @return the nombreBanco
	 */
	public String getNombreBanco() {
		return nombreBanco;
	}


	/**
	 * @param nombreBanco the nombreBanco to set
	 */
	public void setNombreBanco(String nombreBanco) {
		this.nombreBanco = nombreBanco;
	}

	public String getFrecuenteMulticanal() {
		return frecuenteMulticanal;
	}

	public void setFrecuenteMulticanal(String frecuenteMulticanal) {
		this.frecuenteMulticanal = frecuenteMulticanal;
	}
	
	
	
	

}
