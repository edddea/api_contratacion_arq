package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ResultadosAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class ResultadosAutenticacionDelegate extends DelegateBaseAutenticacion {
public final static long RESULTADOS_AUTENTICACION_DELEGATE_ID = 0x1ef4f4c61ca112abL;
	
	//private ArrayList<Object> datosLista;
	private DelegateBaseOperacion operationDelegate;
	private int listaOpcionesMenu;
	private boolean vienedetransferir=false;
	/**
	 * PendingIntent to tell the SMS app to notify us.
	 */
	private PendingIntent mSentPendingIntent;
	
	/** 
	 * The BroadcastReceiver that we use to listen for the notification back.
	 */
	private BroadcastReceiver mBroadcastReceiver;
	
	private ResultadosAutenticacionViewController resultadosAutenticacionViewController;
	
	public ResultadosAutenticacionDelegate(DelegateBaseOperacion operationDelegate) {
		this.operationDelegate = operationDelegate;
		listaOpcionesMenu = operationDelegate.getOpcionesMenuResultados();
	}
	
	/**
	 * @return Delegado especifico de la operación realizada.
	 */
	public DelegateBaseOperacion getOperationDelegate() {
		return operationDelegate;
	}
	
	public void setResultadosViewController(ResultadosAutenticacionViewController viewController) {
		this.resultadosAutenticacionViewController = viewController;
	}
	
	public BroadcastReceiver getmBroadcastReceiver() {
		return mBroadcastReceiver;
	}

	public void setmBroadcastReceiver(BroadcastReceiver mBroadcastReceiver) {
		this.mBroadcastReceiver = mBroadcastReceiver;
	}

	public void consultaDatosLista() {
		resultadosAutenticacionViewController.setListaDatos(operationDelegate.getDatosTablaResultados());
	}
	
	public DelegateBaseOperacion consultaOperationDelegate() {
		return operationDelegate;
	}
	
	public void enviaPeticionOperacion() {
		
	}
	
	public void consultaOpcionesMenu() {
		
	}
	
	public void consultaTextoSMS() {
		
	}

	public void enviaSMS() {
		String smsText = Tools.removeSpecialCharacters(operationDelegate.getTextoSMS());	
		mSentPendingIntent = PendingIntent.getBroadcast(resultadosAutenticacionViewController, 0,  new Intent(Constants.SENT), 0);
        SmsManager smsMgr = SmsManager.getDefault();
        resultadosAutenticacionViewController.muestraIndicadorActividad(resultadosAutenticacionViewController.getString(R.string.label_information),
        		resultadosAutenticacionViewController.getString(R.string.sms_sending));
        String mPhone=Session.getInstance(SuiteApp.appContext).getUsername();
        
        if(mBroadcastReceiver == null){
    		mBroadcastReceiver = resultadosAutenticacionViewController.createBroadcastReceiver();
        }
        
        ArrayList<String> messages = smsMgr.divideMessage(smsText);
        for (int i = 0; i < messages.size(); i++) {
		     String text = messages.get(i).trim();
		     if(text.length()>0) {
		    	 if(Server.ALLOW_LOG) Log.d("sms mensaje", text);
	  		     // send the message, passing in the pending intent, sentPI
		    	 smsMgr.sendTextMessage(mPhone, null, text, mSentPendingIntent, null);
		    	 resultadosAutenticacionViewController.registerReceiver(mBroadcastReceiver, new IntentFilter(Constants.SENT));
		     }
	     }
		
    }

	public void guardaPDF() {
		
	}
	
	public void enviaEmail() {
		// SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showEnviarCorreo(this);
	}
	
	public void guardaFrecuente() {
		((BmovilViewsController)resultadosAutenticacionViewController.getParentViewsController()).showAltaFrecuente(operationDelegate);
	}
	
	public void guardaRapido() {
		
	}
	
	public void borraOperacion() {
		
	}
	
	public void setOperacionTransferirEnCurso(boolean transferir){
		this.vienedetransferir=transferir;
	}
	public boolean isOperacionTransferirEnCurso(){
		return vienedetransferir;
	}
	
	@Override
	public int getNombreImagenEncabezado() {
		return operationDelegate.getNombreImagenEncabezado();
	}
	
	@Override
	public int getTextoEncabezado() {
		return operationDelegate.getTextoEncabezado();
	}
	
	@Override
	public String getTextoTituloResultado() {
		return operationDelegate.getTextoTituloResultado();
	}
	
	@Override
	public int getColorTituloResultado() {
		return operationDelegate.getColorTituloResultado();
	}
	
	@Override
	public String getTextoPantallaResultados() {
		return operationDelegate.getTextoPantallaResultados();
	}
	
	@Override
	public String getTextoEspecialResultados() {
		return operationDelegate.getTextoEspecialResultados();
	}
	
	@Override
	public int getOpcionesMenuResultados() {
		return listaOpcionesMenu;
	}
	
	@Override
	public String getTextoAyudaResultados() {
		return operationDelegate.getTextoAyudaResultados();
	}
	
	public void accionBotonResultados(){
		operationDelegate.accionBotonResultados();
	}
	
	public int getImagenBotonResultados(){
		return operationDelegate.getImagenBotonResultados();
	}
	
}
