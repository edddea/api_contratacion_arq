package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.util.Log;
import android.view.View;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultarObtenerComprobantesViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaObtenerComprobante;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaPagoServiciosData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTransferenciasCuentasBBVAData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTransferenciasMisCuentasData;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaTransferenciasOtrosBancosData;
import suitebancomer.classes.gui.controllers.BaseViewController;
import tracking.TrackingHelper;

public class ObtenerComprobantesDelegate extends DelegateBaseAutenticacion {
	/**
	 * Identificador del delegado.
	 */
	public static final long OBTENER_COMPROBANTE_DELEGATE_ID=-1L ;
	
	/**
	 * El controlador actual de la pantalla.
	 */
	private BaseViewController viewController;
	
	/**
	 * @return El controlador de la pantalla.
	 */
	public BaseViewController getViewController() {
		return viewController;
	}

	/**
	 * Instancia de tipo parsingHndler para enviar de manera dinamica a la peticion al server
	 */
	public static ParsingHandler parsingHandler;

	/**
	 * @param viewController El controlador de la pantalla a establecer.
	 */
	public void setViewController(BaseViewController viewController) {
		this.viewController = viewController;
	}
	

	private ArrayList<ConsultaObtenerComprobante> movimientosOC;

	
	private boolean isTransferencia=true;
	
	private int tipoOperacionActual;

	private int periodoActual=0;
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#realizaOperacion(suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionViewController, java.lang.String, java.lang.String, java.lang.String)
	 */
	
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if( viewController != null)
			((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
	}

	public boolean isTransferencia() {
		return isTransferencia;
	}

	public void setTransferencia(boolean isTransferencia) {
		this.isTransferencia = isTransferencia;
	}

	public ArrayList<ConsultaObtenerComprobante> getMovimientosOC() {
		return movimientosOC;
	}

	public void setMovimientosOC(ArrayList<ConsultaObtenerComprobante> movimientosOC) {
		this.movimientosOC = movimientosOC;
	}

	public int getMovimientoSeleccionadoOC() {
		return movimientoSeleccionadoOC;
	}

	public void setMovimientoSeleccionadoOC(int movimientoSeleccionadoOC) {
		this.movimientoSeleccionadoOC = movimientoSeleccionadoOC;
	}

	public int getTipoOperacion() {
		return tipoOperacionActual;
	}

	public int getPeriodoActual() {
		return periodoActual;
	}

	public void setPeriodoActual(int periodoActual) {
		this.periodoActual = periodoActual;
	}

	public void setTipoOperacion(int tipoOperacion) {
		this.tipoOperacionActual = tipoOperacion;
	}

	public void verMasMovimientos() {
		periodoActual++;
		consultarMovimientosOC();
	}
	
	
	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#analyzeResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			ArrayList movimientosRecibidos = null;
			
			if (operationId == Server.CONSULTA_TRANSFERENCIAS_MIS_CUENTAS) {
				isTransferencia=true;	
				movimientosRecibidos = ((ConsultaTransferenciasMisCuentasData)response.getResponse()).getMovimientos();
				periodoActual = Integer.parseInt(((ConsultaTransferenciasMisCuentasData)response.getResponse()).getPeriodo());
				
			} else if (operationId == Server.CONSULTA_TRANSFERENCIAS_CUENTA_BBVA) {
				isTransferencia=true;
				movimientosRecibidos = ((ConsultaTransferenciasCuentasBBVAData)response.getResponse()).getMovimientos();
				periodoActual = Integer.parseInt(((ConsultaTransferenciasCuentasBBVAData)response.getResponse()).getPeriodo());
				
			} else if (operationId == Server.CONSULTA_TRANSFERENCIAS_A_OTROS_BANCOS) {
				isTransferencia=true;
				movimientosRecibidos = ((ConsultaTransferenciasOtrosBancosData)response.getResponse()).getMovimientos();
				periodoActual = Integer.parseInt(((ConsultaTransferenciasOtrosBancosData)response.getResponse()).getPeriodo());
				
			} else if (operationId == Server.CONSULTA_PAGO_SERVICIOS) {
				isTransferencia=false;
				movimientosRecibidos = ((ConsultaPagoServiciosData)response.getResponse()).getMovimientos();
				periodoActual = Integer.parseInt(((ConsultaPagoServiciosData)response.getResponse()).getPeriodo());
			}
			
			if ((this.movimientosOC == null) || (this.movimientosOC.size() == 0)) {
				this.movimientosOC = movimientosRecibidos;
			} else {
				this.movimientosOC.addAll(movimientosRecibidos);
			}
			
			actualizarMovimientosOC();
			
		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
			if (viewController != null) {
				((BmovilViewsController)viewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlertEspecial(viewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
			} else {
				BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
				BmovilViewsController bmvc = bmApp.getBmovilViewsController(); 
				BaseViewController current = bmvc.getCurrentViewControllerApp();
				current.showInformationAlertEspecial(viewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
			}
			
			//this.movimientosOC = null;
			actualizarMovimientosOC();
			
		} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
			BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
			BmovilViewsController bmvc = bmApp.getBmovilViewsController();
			BaseViewController current = bmvc.getCurrentViewControllerApp();
			current.showInformationAlertEspecial(viewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
			//this.movimientosOC = null;
			actualizarMovimientosOC();
		}
		
		//if ((periodoActual == 2) || (this.movimientosOC == null) || (this.movimientosOC.isEmpty())) {
		if (periodoActual >= 2 || response.getStatus() == ServerResponse.OPERATION_ERROR ||
				(response.getStatus() == ServerResponse.OPERATION_WARNING && response.getMessageCode().equals(Constants.CODE_CNE1446))) {
			((ConsultarObtenerComprobantesViewController) viewController).muestraVerMasMovimientos(false);
		} else {
			((ConsultarObtenerComprobantesViewController) viewController).muestraVerMasMovimientos(true);
		}
		
	}
	
	



	
	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#getDelegateIdentifier()
	 */
	@SuppressWarnings("static-access")
	@Override
	public long getDelegateIdentifier() {
		return this.OBTENER_COMPROBANTE_DELEGATE_ID;
	}
	
	/* ####################################################################################################################### */
	


	
	public ArrayList<Object> getHeaderListaMovimientos() {
		ArrayList<Object> result = new ArrayList<Object>();
		
		result.add(null);
		result.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_lista_fecha));
		result.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_lista_importe));
		
		return result;
	}
	
	public ArrayList<Object> getDatosTablaMovimientosOC() {
		ArrayList<Object> datos = new ArrayList<Object>();
		ArrayList<String> registro;
		
		if(null == movimientosOC || movimientosOC.isEmpty()) {
			registro = new ArrayList<String>();
			registro.add(null);
			registro.add("");
			registro.add("");
			registro.add("");
			registro.add("");
		} else {
			for(ConsultaObtenerComprobante consulta : movimientosOC) {
				registro = new ArrayList<String>();
				registro.add(null);
				registro.add(Tools.formatShortDate(consulta.getFecha()));
				registro.add(Tools.formatAmount(consulta.getImporte(), false));
				datos.add(registro);
			}
		}
		
		return datos;
	}
	
	public void consultarMovimientosOC() {
		
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		Session session = Session.getInstance(SuiteApp.appContext);
		
		paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
		paramTable.put(ServerConstants.IUM,session.getIum());
		paramTable.put(ServerConstants.PERIODO,String.valueOf(periodoActual));
		//JAIG REVISAR
		//Hay que revisarlo BIEN PASA VARIOS VALORES
		((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(tipoOperacionActual, paramTable, true, parsingHandler, isJsonValueCode.DEPOSITOS, viewController, true);
	}
	
	public void actualizarMovimientosOC() {
		
		ConsultarObtenerComprobantesViewController controller = (ConsultarObtenerComprobantesViewController)viewController;
		
		ArrayList<Object> movimientos = getDatosTablaMovimientosOC();
		
		controller.getListaEnvios().setVisibility(View.VISIBLE);
		controller.getListaEnvios().setNumeroFilas(movimientos.size());
		controller.getListaEnvios().setLista(movimientos);
		
		if(movimientos.isEmpty())
			controller.getListaEnvios().setTextoAMostrar("");
		else{
			controller.getListaEnvios().setTextoAMostrar(null);
			controller.getListaEnvios().setNumeroColumnas(2);
		}
		
		controller.getListaEnvios().cargarTabla();
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#performAction(java.lang.Object)
	 */
	@Override
	public void performAction(Object obj) {
		ConsultarObtenerComprobantesViewController controller = (ConsultarObtenerComprobantesViewController)viewController;
		movimientoSeleccionadoOC = controller.getListaEnvios().getOpcionSeleccionada();
			
		//AMZ inicio
		Map<String,Object> ConsultaRealizadaMap = new HashMap<String, Object>();
			
		ConsultaRealizadaMap.put("evento_realizada","event52");
		ConsultaRealizadaMap.put("&&products","consulta;obtenercomprobante");
		ConsultaRealizadaMap.put("eVar12","operacion realizada");
		TrackingHelper.trackConsultaRealizada(ConsultaRealizadaMap);
		
		//AMZ fin
		showDetallesMovimientoOC();
	}
	
	private int movimientoSeleccionadoOC;
	
	private void showDetallesMovimientoOC() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showDetalleObtenerComprobante();
	}
	
	public ArrayList<Object> getDatosTablaDetalleMovimientoOC() {
		ArrayList<Object> datos = new ArrayList<Object>();
		
		ConsultaObtenerComprobante movimiento = movimientosOC.get(movimientoSeleccionadoOC);
		ArrayList<String> fila;
		
		
		if (isTransferencia) {
			fila = new ArrayList<String>();
			//fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_oprealizada));
			fila.add("Cuenta de depósito");
			/***RHO***/
			//fila.add(movimiento.getCuentaAbonoOGuia());
			if(Server.ALLOW_LOG) Log.d("TRANS",movimiento.getCuentaAbonoOGuia());
			fila.add(Tools.hideAccountNumber(movimiento.getCuentaAbonoOGuia()));
			datos.add(fila);
			
		} else {
			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_convenio));
			/***RHO***/
			//fila.add(movimiento.getCuentaAbonoOGuia());
			if(Server.ALLOW_LOG) Log.d("SERV",movimiento.getCuentaCargoOConvenio());
			fila.add(Tools.enmascaraCuentaDestino(movimiento.getCuentaCargoOConvenio()));
			datos.add(fila);
		}
		
		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_importe));
		fila.add(Tools.formatAmount(movimiento.getImporte(), false));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_tipoop));
		
		
		if(tipoOperacionActual == Server.CONSULTA_TRANSFERENCIAS_MIS_CUENTAS){
			fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_tipooperacion_transmiscuentas));	
		} 
		else if(tipoOperacionActual == Server.CONSULTA_TRANSFERENCIAS_CUENTA_BBVA){
			fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_tipooperacion_transcuentasbbva));
		}
		else if(tipoOperacionActual == Server.CONSULTA_TRANSFERENCIAS_A_OTROS_BANCOS){
			fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_tipooperacion_transotrosbancos));
		}
		else if(tipoOperacionActual == Server.CONSULTA_PAGO_SERVICIOS){
			fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_movimientos_tipooperacion_pagoservicios));
		}

		datos.add(fila);

		fila = new ArrayList<String>();
		if (isTransferencia) {
			fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_titulo_datodeenvio));
		} else {
			fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_titulo_datodelenvio));
		}
		fila.add(" ");
		datos.add(fila);
		
		if (isTransferencia) {
			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_cuentaretiro));
			//fila.add(movimiento.getCuentaCargoOConvenio());
			if(Server.ALLOW_LOG) Log.d("TRANS2",movimiento.getCuentaCargoOConvenio());
			fila.add(movimiento.getCuentaCargoOConvenio());
			datos.add(fila);
			
		} else {
			fila = new ArrayList<String>();
			fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_guia));
			if(Server.ALLOW_LOG) Log.d("SERV2",movimiento.getCuentaAbonoOGuia());
			fila.add(movimiento.getCuentaAbonoOGuia());
			datos.add(fila);
		}
		
		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_folio));
		fila.add(movimiento.getFolio());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_fecha));
		fila.add(Tools.formatDate(movimiento.getFecha()));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(viewController.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_hora));
		fila.add(Tools.formatTime(movimiento.getHora()));
		datos.add(fila);

		
		
		return datos;
	}
	

	public void enviaEmail() {
		
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showEnviarCorreo(this);
	
	}




}
