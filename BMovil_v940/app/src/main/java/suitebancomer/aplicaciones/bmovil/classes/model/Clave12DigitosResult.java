package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import android.util.Log;

public class Clave12DigitosResult implements ParsingHandler {

	/**
	 * Default serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	private String estado;
	private String mensajeInformativo;
	private String claveRetiro;
	private String folioCanal;


	/**
	 * Constructor vacio
	 */
	public Clave12DigitosResult () {
		this.estado = "";
		this.mensajeInformativo = "";
		this.claveRetiro = "";
		this.folioCanal = "";
	}


	/**
	 * @param estado
	 * @param mensajeInformativo
	 * @param claveRetiro
	 * @param folioCanal
	 */
	public Clave12DigitosResult(String estado, String mensajeInformativo,
			String claveRetiro, String folioCanal) {
		super();
		this.estado = estado;
		this.mensajeInformativo = mensajeInformativo;
		this.claveRetiro = claveRetiro;
		this.folioCanal = folioCanal;
	}


	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}


	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}


	/**
	 * @return the mensajeInformativo
	 */
	public String getMensajeInformativo() {
		return mensajeInformativo;
	}


	/**
	 * @param mensajeInformativo the mensajeInformativo to set
	 */
	public void setMensajeInformativo(String mensajeInformativo) {
		this.mensajeInformativo = mensajeInformativo;
	}


	/**
	 * @return the claveRetiro
	 */
	public String getClaveRetiro() {
		return claveRetiro;
	}


	/**
	 * @param claveRetiro the claveRetiro to set
	 */
	public void setClaveRetiro(String claveRetiro) {
		this.claveRetiro = claveRetiro;
	}


	/**
	 * @return the folioCanal
	 */
	public String getFolioCanal() {
		return folioCanal;
	}


	/**
	 * @param folioCanal the folioCanal to set
	 */
	public void setFolioCanal(String folioCanal) {
		this.folioCanal = folioCanal;
	}


	@Override
	public void process(Parser parser) throws IOException, ParsingException {

	}


	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {

		try {

			this.estado = parser.parseNextValue("estado");
			this.mensajeInformativo = parser.parseNextValue("mensajeInformativo");
			this.claveRetiro = parser.parseNextValue("claveRetiro");
			this.folioCanal = parser.parseNextValue("folioCanal");

		} catch (final Exception e) {

			Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);

		}

	}


}
