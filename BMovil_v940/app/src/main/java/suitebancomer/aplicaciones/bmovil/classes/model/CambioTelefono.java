package suitebancomer.aplicaciones.bmovil.classes.model;

public class CambioTelefono {
	
	private String nuevoTelefono;
	private String nombreCompania;
	
	public String getNuevoTelefono() {
		return nuevoTelefono;
	}
	
	public void setNuevoTelefono(String nuevoTelefono) {
		this.nuevoTelefono = nuevoTelefono;
	}
	
	public String getNombreCompania() {
		return nombreCompania;
	}
	
	public void setNombreCompania(String nombreCompania) {
		this.nombreCompania = nombreCompania;
	}

}
