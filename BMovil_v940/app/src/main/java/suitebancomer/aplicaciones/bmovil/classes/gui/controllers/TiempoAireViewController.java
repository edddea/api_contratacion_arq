package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;


import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import bancomer.api.common.commons.Constants.Perfil;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TiempoAireDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Payment;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import java.util.HashMap;
import java.util.Map;
import tracking.TrackingHelper;

public class TiempoAireViewController extends BaseViewController implements OnClickListener {

	LinearLayout vista;
	public CuentaOrigenViewController componenteCtaOrigen;
	public ListaSeleccionViewController listaSeleccion;
	ImageButton registrarNuevoCelular;
	TiempoAireDelegate delegate;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_compra_tiempo_aire_seleccion_view);
		setTitle(R.string.tiempo_aire_title,R.drawable.bmovil_tiempo_aire_icono);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("tiempo aire", parentManager.estados);
		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID));
		delegate = (TiempoAireDelegate)getDelegate();
		delegate.setcontroladorTiempoAire(this);
		vista = (LinearLayout)findViewById(R.id.tiempo_aire_seleccion_view_controller_layout);
		registrarNuevoCelular = (ImageButton) findViewById(R.id.tiempo_aire_seleccion_view_controller_btn_nuevo_celular);
		registrarNuevoCelular.setOnClickListener(this);
		init();
	}
	
	public void init(){
		scaleForCurrentScreen();
		cargaCuentaOrigenComponent();
		componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.compraTiempoAire);
		cargaListaSeleccionComponent();
		delegate.consultarFrecuentes(Constants.tipoCFTiempoAire);
//		cargaListaSeleccionComponent();
	}
	
	@SuppressWarnings("deprecation")
	public void cargaCuentaOrigenComponent(){
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();

		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.bottomMargin = guiTools.getEquivalenceInPixels(20.0);
//		params.leftMargin = guiTools.getEquivalenceInPixels(20.0);

		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.setDelegate(delegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
		componenteCtaOrigen.init();
		vista.addView(componenteCtaOrigen);
		
	}

	@SuppressWarnings("deprecation")
	public void cargaListaSeleccionComponent(){
		delegate.getCompraTiempoAire().setCuentaOrigen(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		ArrayList<Object> listaCuetasAMostrar = delegate.getDatosTablaFrecuentes();
		
		ArrayList<Object> lista = new ArrayList<Object>();
		ArrayList<Object> encabezado = new ArrayList<Object>();
		encabezado.add(null);
		encabezado.add(getString(R.string.altafrecuente_nombrecorto));
		encabezado.add(getString(R.string.tiempo_aire_encabezado_lista));

		lista = listaCuetasAMostrar;
		GuiTools guiTools = GuiTools.getCurrent();
		
		LinearLayout.LayoutParams params;
		params = new LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//		params.topMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.rightMargin =  getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
		
		if (listaSeleccion == null) {
			listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
			listaSeleccion.setDelegate(delegate);
			listaSeleccion.setNumeroColumnas(2);
			listaSeleccion.setEncabezado(encabezado);
			listaSeleccion.setTitle(getString(R.string.mainMenu_favoritePayment));
			listaSeleccion.setLista(lista);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(lista.size());
			listaSeleccion.setExisteFiltro(true);
			listaSeleccion.setSingleLine(true);
			listaSeleccion.cargarTabla();
//			if (listaCuetasAMostrar.size() == 0){
//				listaSeleccion.setEnabled(false);
//			}
			vista.addView(listaSeleccion);

		}else {
			listaSeleccion.setLista(lista);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(lista.size());
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.cargarTabla();
		}
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
	}

	@Override
	public void onClick(View v) {
		if (v == registrarNuevoCelular) {
			
			Session session = Session.getInstance(SuiteApp.appContext);
			Perfil perfil = session.getClientProfile();
			
			if (!Constants.Perfil.recortado.equals(perfil)) {
				delegate.setFrecuente(false);
				//frecuente correo electronico
				delegate.getCompraTiempoAire().setAliasFrecuente(null);
				delegate.getCompraTiempoAire().setCorreoFrecuente(null);

				delegate.setTipoOperacion(Constants.Operacion.compraTiempoAire);

				//ARR
				Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
				
				//ARR
				paso1OperacionMap.put("evento_paso1", "event46");
				paso1OperacionMap.put("&&products", "operaciones;comprar+tiempo aire");
				paso1OperacionMap.put("eVar12", "paso1:tiempo aire");

				TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
				((BmovilViewsController)parentViewsController).showTiempoAireDetalleViewController();
			} else {
				delegate.comprobarRecortado();
			}
		}
	}
	
	public void muestraDetalleTiempoAire(Payment selectedPayment){
		delegate.setFrecuente(true);
		delegate.setTipoOperacion(Constants.Operacion.compraTiempoAireF);
		delegate.getCompraTiempoAire().setAliasFrecuente(selectedPayment.getNickname());
		delegate.getCompraTiempoAire().setNombreCompania(selectedPayment.getBeneficiary());
		delegate.getCompraTiempoAire().setCelularDestino(selectedPayment.getBeneficiaryAccount());
		delegate.getCompraTiempoAire().setFrecuenteMulticanal(selectedPayment.getIdToken());
		delegate.getCompraTiempoAire().setIdCanal(selectedPayment.getIdCanal());
		delegate.getCompraTiempoAire().setUsuario(selectedPayment.getUsuario());
		//frecuente correo electronico
		delegate.getCompraTiempoAire().setCorreoFrecuente(selectedPayment.getCorreoFrecuente());

		//ARR
				Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
				
				//ARR
				paso1OperacionMap.put("evento_paso1", "event46");
				paso1OperacionMap.put("&&products", "operaciones;comprar+tiempo aire");
				paso1OperacionMap.put("eVar12", "paso1:tiempo aire");

				TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
		((BmovilViewsController)parentViewsController).showTiempoAireDetalleViewController();
	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		if (delegate != null) {
			delegate.setcontroladorTiempoAire(this);
			if (delegate.getCompraTiempoAire().getCuentaOrigen() != null) {
				componenteCtaOrigen.setIndiceCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getListaCuentaOrigen().indexOf(delegate.getCompraTiempoAire().getCuentaOrigen()));
				//componenteCtaOrigen.actualizaComponente();
				componenteCtaOrigen.actualizaComponente(false);
			}
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
		super.goBack();
	}
	
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(vista);
		guiTools.scale(registrarNuevoCelular);

	}
}
