package suitebancomer.aplicaciones.bmovil.classes.model;

public class SpeiAccount {
	//#region Class fields.
	/**
	 * The account.
	 */
	private Account numeroCuenta;
	
	/**
	 * The client number.
	 */
	private String numeroCliente;
	
	/**
	 * The associated phone.
	 */
	private String telefonoAsociado;
	
	/**
	 * The code of the company which the phone belongs.
	 */
	private String codigoCompania;
	
	/**
	 * The name of the company.
	 */
	private String nombreCompania;
	
	/**
	 * The date of the last modification.
	 */
	private String fechaUltimaModificacion;
	
	/**
	 * The SPEI status.
	 */
	private String indicadorSpeiActivado;
	//#endregion

	//#region Getters and Setters.
	/**
	 * @return The account.
	 */
	public Account getNumeroCuenta() {
		return numeroCuenta;
	}

	/**
	 * @param numeroCuenta The account.
	 */
	public void setNumeroCuenta(Account numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	/**
	 * @return The client number.
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}

	/**
	 * @param numeroCliente The client number.
	 */
	public void setNumeroCliente(String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	/**
	 * @return The associated phone.
	 */
	public String getTelefonoAsociado() {
		return telefonoAsociado;
	}

	/**
	 * @param telefonoAsociado The associated phone.
	 */
	public void setTelefonoAsociado(String telefonoAsociado) {
		this.telefonoAsociado = telefonoAsociado;
	}

	/**
	 * @return The code of the company which the phone belongs.
	 */
	public String getCodigoCompania() {
		return codigoCompania;
	}

	/**
	 * @param codigoCompania The code of the company which the phone belongs.
	 */
	public void setCodigoCompania(String codigoCompania) {
		this.codigoCompania = codigoCompania;
	}

	/**
	 * @return The name of the company.
	 */
	public String getNombreCompania() {
		return nombreCompania;
	}

	/**
	 * @param nombreCompania The name of the company.
	 */
	public void setNombreCompania(String nombreCompania) {
		this.nombreCompania = nombreCompania;
	}

	/**
	 * @return the The date of the last modification.
	 */
	public String getFechaUltimaModificacion() {
		return fechaUltimaModificacion;
	}

	/**
	 * @param fechaUltimaModificacion The date of the last modification.
	 */
	public void setFechaUltimaModificacion(String fechaUltimaModificacion) {
		this.fechaUltimaModificacion = fechaUltimaModificacion;
	}

	/**
	 * @return The SPEI status.
	 */
	public String getIndicadorSpeiActivado() {
		return indicadorSpeiActivado;
	}

	/**
	 * @param indicadorSpeiActivado The SPEI status.
	 */
	public void setIndicadorSpeiActivado(String indicadorSpeiActivado) {
		this.indicadorSpeiActivado = indicadorSpeiActivado;
	}
	//#endregion

	/**
	 * @param numeroCuenta The account.
	 * @param numeroCliente The client number.
	 * @param telefonoAsociado The associated phone.
	 * @param codigoCompania The code of the company which the phone belongs.
	 * @param nombreCompania The name of the company.
	 * @param fechaUltimaModificacion The date of the last modification.
	 * @param indicadorSpeiActivado The SPEI status.
	 */
	public SpeiAccount(Account numeroCuenta, 
					  String numeroCliente,
					  String telefonoAsociado, 
					  String codigoCompania,
					  String nombreCompania, 
					  String fechaUltimaModificacion,
					  String indicadorSpeiActivado) {
		super();
		this.numeroCuenta = numeroCuenta;
		this.numeroCliente = numeroCliente;
		this.telefonoAsociado = telefonoAsociado;
		this.codigoCompania = codigoCompania;
		this.nombreCompania = nombreCompania;
		this.fechaUltimaModificacion = fechaUltimaModificacion;
		this.indicadorSpeiActivado = indicadorSpeiActivado;
	}
	
	public SpeiAccount() {
		super();
		this.numeroCuenta = null;
		this.numeroCliente = null;
		this.telefonoAsociado = null;
		this.codigoCompania = null;
		this.nombreCompania = null;
		this.fechaUltimaModificacion = null;
		this.indicadorSpeiActivado = null;
	}
}
