package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import suitebancomer.classes.gui.controllers.BaseViewController;
import android.os.Bundle;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class AyudaAgregarRapido extends BaseViewController {
	public AyudaAgregarRapido() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_ayuda_agregar_rapido);
		setTitle(R.string.bmovil_ayuda_rapidos_titulo, R.drawable.bmovil_dinero_movil_icono);	
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
}
