package suitebancomer.aplicaciones.bmovil.classes.common;

/**
 * Created by nicolas.arriaza on 10/07/2015.
 *
 * Constants creado para las constantes propias de la aplicacion principal no compartidas con las APIs
 */
public class BmovilConstants {

    /**
     * The application version.
     */
    public static final String APPLICATION_VERSION = "1021";

    /*
	 * Opciones Menu Administrar *
	 */
    public static final String MADMINISTRAR_ACERCADE = "00";
    public static final String MADMINISTRAR_CAMBIAR_CONTRASENA = "01";
    public static final String MADMINISTRAR_CAMBIO_TELEFONO = "02";
    public static final String MADMINISTRAR_CAMBIO_CUENTA = "03";
    public static final String MADMINISTRAR_ACTUALIZAR_CUENTAS = "04";
    public static final String MADMINISTRAR_SUSPENDER_CANCELAR = "05";
    public static final String MADMINISTRAR_CONFIGURAR_MONTOS = "06";
    public static final String MADMINISTRAR_CONFIGURAR_ALERTAS = "07";
    public static final String MADMINISTRAR_CONFIGURAR_CORREO = "08";
    public static final String MADMINISTRAR_OPERAR_SIN_TOKEN = "09";
    public static final String MADMINISTRAR_OPERAR_CON_TOKEN = "10";
    public static final String MADMINISTRAR_CONSULTAR_CONTRATO = "11";
    public static final String MADMINISTRAR_OPERAR_RECORTADO = "12";
    //SPEI
    public static final String MADMINISTRAR_ASOCIAR_CELULAR	= "13";
    public static final String MADMINISTRAR_NOVEDADES = "14";
    //Paperless
    public static final String MADMINISTRAR_CONSULTA_ESTATUS_ENVIO_ESTADO_DE_CUENTA	= "15";
    //Paperless
    public static final int PERIODOS_TOTALES= 12;

    /**
     * Opciones menu consultar
     */
    public static final String MenuConsultar_movimientos_opcion = "0";
    public static final String MenuConsultar_enviosdm_opcion = "1";
    public static final String MenuConsultar_opfrecuentes_opcion = "2";
    public static final String MenuConsultar_oprapidas_opcion = "3";
    public static final String MenuConsultar_interbancarios = "4";
    public static final String MenuConsultar_retirosintarjeta_opcion ="5";
    public static final String MenuConsultar_depositosrecibidos_opcion = "6";//VALIDAR SI SE INCREMENTA A 4
    public static final String MenuConsultar_obtenercomprobante_opcion = "7";//VALIDAR SI SE INCREMENTA A 5
    public static final String MenuConsultar_otroscreditos_opcion = "8";//VALIDAR SI SE INCREMENTA A 6
    //Paperless
//    public static final String MenuConsultar_estado_de_cuenta = "4";

    //SPEI

    /**
     * Code to determine if a SPEI account has a phone associated.
     */
    public static final String SPEI_PHONE_ASSOCIATED_CODE = "S";

    /**
     * Code to determine if a SPEI account has not a phone associated.
     */
    public static final String SPEI_PHONE_UNASSOCIATED_CODE = "N";

    /**
     * Operation code for the association of a phone to an account.
     */
    public static final String SPEI_OPERATION_TYPE_ASSOCIATE = "Alta";

    /**
     * Operation code for the diassociation of a phone to an account.
     */
    public static final String SPEI_OPERATION_TYPE_DIASSOCIATE = "Baja";

    /**
     * Operation code for the modification of a phone associated to an account.
     */
    public static final String SPEI_OPERATION_TYPE_MODIFY = "Modificacion";

    /**
     * Operation code for the association of a phone to an account.
     */
    public static final String SPEI_OPERATION_TYPE_FOR_SERVER_ASSOCIATE = "ALTA";

    /**
     * Operation code for the diassociation of a phone to an account.
     */
    public static final String SPEI_OPERATION_TYPE_FOR_SERVER_DIASSOCIATE = "BAJA";

    /**
     * Operation code for the modification of a phone associated to an account.
     */
    public static final String SPEI_OPERATION_TYPE_FOR_SERVER_MODIFY = "MODIFICA";

    /**
     * Nombre para identificar los shared preferences
     */
    public static final String SHARED_NAME = "preferenciasBBBVA";

    /**
     * Nombre para identificar la version de catalogo de spei
     * */
    public static final String VERSION_SPEI = "version_spei";

    /**
     * Nombre del catalogo spei
     * */
    public static final String CATALOGO_SPEI = "spei.tmp";

    //Termina SPEI

    /**consumo clausulas**/
    public static final String PREFORMALIZADO = "F";
    public static final String PREABPROBADO = "P";
    public static final String IDBOVEDA1 = "PBCCMPPI02";
    public static final String IDBOVEDA2 = "PBCCMPPI03";

    //PAPERLESS
    /**
     * DATOS DE URL INVOCACION TEXTO PAPERLESS
     */
    public static final String OPERACION ="consultaTextoPaperless";

    public static final String ID_PRODUCTO_TP ="PBPAPERL01";

    public static final String INDICADOR_PAGINA ="N";

    public static final String INDICADOR_SUBSECCION ="0";

    public static final String VERSION_PAPERLESS ="";

    public static final String ULTIMA_SECCION_CONSULTADA ="";

    public static final String NUMERO_CELULAR_TP = "5523545676";

    public static final String IUM_TP = "3CC9DBF86E863F7CE6D7F3279DE44E8A";


    /**
     * DATOS DE URL INVOCACION INHIBIR ENVIO PAPERLESS
     */

    public static final String ID_PRODUCTO = "PBPAPERL01";

    public static final String CLAVE_CONTRATACION ="BMV";

    public static final String CLAVE_RESPUESTA = "0001";

    public static final String CLAVE_RESPUESTA_RECHAZO = "0002";

    public static final String ID_CAMPAÑA = "0429";

    public static final String CAUSA_INHIBICION = "PP";

    public static final String CAMPAÑA_PAPERLESS_NO_ACEPTADA = "1";

    public static final String CAMPAÑA_PAPERLESS_VISIBLE = "0";
}
