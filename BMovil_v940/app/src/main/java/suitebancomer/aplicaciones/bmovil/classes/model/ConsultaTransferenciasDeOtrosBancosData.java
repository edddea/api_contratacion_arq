package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class ConsultaTransferenciasDeOtrosBancosData  implements ParsingHandler {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String estado;
	private ArrayList<ConsultaDepositosRecbidosTransDeOtrosBancos> movimientos;
	private String codigoMensaje;
	private String descripcionMensaje;
	private String periodo;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodigoMensaje() {
		return codigoMensaje;
	}

	public void setCodigoMensaje(String codigoMensaje) {
		this.codigoMensaje = codigoMensaje;
	}

	public String getDescripcionMensaje() {
		return descripcionMensaje;
	}

	public void setDescripcionMensaje(String descripcionMensaje) {
		this.descripcionMensaje = descripcionMensaje;
	}

	public String getPeriodo() {
		return periodo;
	}

	public ArrayList<ConsultaDepositosRecbidosTransDeOtrosBancos> getMovimientos() {
		return movimientos;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	/**
	 * Constructor por defecto.
	 */
	public ConsultaTransferenciasDeOtrosBancosData() {

		estado = null;
		codigoMensaje = null;
		descripcionMensaje = null;
		periodo = null;
		movimientos = new ArrayList<ConsultaDepositosRecbidosTransDeOtrosBancos>();
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {

		estado = parser.parseNextValue("estado");
		codigoMensaje = parser.parseNextValue("codigoMensaje");
		descripcionMensaje = parser.parseNextValue("descripcionMensaje");
		if (estado.equals("OK")) {
			periodo = parser.parseNextValue("periodo");
			JSONObject aux = parser.parserNextObject("arrMovtos");
			JSONArray aux1;
			try {

				aux1 = aux.getJSONArray("ocMovtos");
				for (int i = 0; i < aux1.length(); i++) {
					ConsultaDepositosRecbidosTransDeOtrosBancos cob = new ConsultaDepositosRecbidosTransDeOtrosBancos();
					cob.setBancoOrdenante(aux1.getJSONObject(i).getString(
							"bancoOrdenante"));
					cob.setClaveRastreo(aux1.getJSONObject(i).getString(
							"claveRastreo"));
					cob.setCuentaOrdenante(aux1.getJSONObject(i).getString(
							"cuentaOrdenante"));
					cob.setReferenciaNumerica(aux1.getJSONObject(i).getString(
							"referenciaNumerica"));
					cob.setMotivoPago(aux1.getJSONObject(i).getString(
							"motivoPago"));
					cob.setFecha(aux1.getJSONObject(i).getString("fecha"));
					cob.setHora(aux1.getJSONObject(i).getString("hora"));
					cob.setImporte(aux1.getJSONObject(i).getString("importe"));
					movimientos.add(cob);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				if(Server.ALLOW_LOG) e.printStackTrace();
			}
		}

	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub

	}
}
