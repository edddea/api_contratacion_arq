package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.RegistrarOperacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class RegistrarOperacionDelegate extends DelegateBaseAutenticacion {

	public static final long REGISTRAR_OPERACION_DELEGATE_ID = 1946309723105600055L;
	private DelegateBaseAutenticacion delegateOp;
	private RegistrarOperacionViewController viewController;
	private TipoInstrumento tipoInstrumento;

	public RegistrarOperacionDelegate(DelegateBaseAutenticacion delegateOp) {
		this.delegateOp = delegateOp;
	}

	/**
	 * 
	 * @return el delegate de la operacion que el usuarioo quiere realizar.
	 */
	public DelegateBaseAutenticacion getDelegateOp() {
		return delegateOp;
	}

	public void setViewController(RegistrarOperacionViewController viewController) {
		this.viewController = viewController;
	}

	/**
	 * Retorna el tipo de instrumento a usar. DP270. OCRA. SoftToken.
	 * sinInstrumento
	 * 
	 * @return TipoInstrumento
	 */
	public TipoInstrumento consultaTipoInstrumento() {
		if (tipoInstrumento == null) {
			Session session = Session.getInstance(SuiteApp.appContext);
			String instrumento = session.getSecurityInstrument();
			if (instrumento.equals(Constants.IS_TYPE_DP270)) {
				tipoInstrumento = TipoInstrumento.DP270;
			} else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
				tipoInstrumento = TipoInstrumento.OCRA;
			} else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)) {
				tipoInstrumento = TipoInstrumento.SoftToken;
			} else {
				tipoInstrumento = TipoInstrumento.sinInstrumento;
			}
		}
		return tipoInstrumento;
	}

	@Override
	public String getEtiquetaCampoNip() {
		return viewController.getString(R.string.confirmation_nip);
	}

	@Override
	public String getTextoAyudaNIP() {
		return viewController
				.getString(R.string.confirmation_autenticacion_ayudaNip);
	}

	@Override
	public String getEtiquetaCampoContrasenia() {
		return viewController.getString(R.string.confirmation_aut_contrasena);
	}

	@Override
	public String getEtiquetaCampoOCRA() {
		return viewController.getString(R.string.confirmation_ocra);
	}

	@Override
	public String getEtiquetaCampoDP270() {
		return viewController.getString(R.string.confirmation_dp270);
	}

	@Override
	public String getEtiquetaCampoSoftokenActivado() {
		return viewController.getString(R.string.confirmation_softtokenActivado);
	}

	@Override
	public String getEtiquetaCampoSoftokenDesactivado() {
		return viewController.getString(R.string.confirmation_softtokenDesactivado);
	}

	@Override
	public String getEtiquetaCampoCVV() {
		return viewController.getString(R.string.confirmation_CVV);
	}

	@Override
	public String getTextoAyudaInstrumentoSeguridad(TipoInstrumento tipoInstrumento) {
	switch (tipoInstrumento) {
		case SoftToken:
			if (SuiteApp.getSofttokenStatus()) {
				return viewController.getString(R.string.confirmation_ayudaRegistroSofttokenActivado);
			} else {
				return viewController.getString(R.string.confirmation_ayudaRegistroSofttokenDesactivado);
			}
		case OCRA:
			return viewController.getString(R.string.confirmation_ayudaRegistroOCRA);
		case DP270:
			return viewController.getString(R.string.confirmation_ayudaRegistroDP270);
		case sinInstrumento:
		default:
			return "";
		}
	}
	
	@Override
	public int getTextoEncabezado() {
		return delegateOp.getTextoEncabezado();
	}
	
	@Override
	public int getNombreImagenEncabezado() {
		return delegateOp.getNombreImagenEncabezado();
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
			viewController.limpiarCampos();
			((BmovilViewsController) viewController.getParentViewsController())
					.getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
		}
		delegateOp.analyzeResponse(operationId, response);
	}

	public DelegateBaseAutenticacion getOperationDelegate() {
		return delegateOp;
	}

	/**
	 * Valida que el campo requerido haya sido ingresado y que tenga la longitud solicitada.
	 * @return true si son validos los datos ingresados; false en caso contrario.
	 * 		
	 */
	public boolean validaDatos() {
		boolean esOK = true;
		TipoOtpAutenticacion tokenAMostrar = tokenAMostrar();
		String asm = null;
		String mensaje = null;
		if (tokenAMostrar != Constants.TipoOtpAutenticacion.ninguno) {
			asm = viewController.pideASM();
			if (asm.equals("")) {
				mensaje = viewController
						.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				esOK = false;
			} else if (asm.length() != Constants.ASM_LENGTH) {
				mensaje = viewController
						.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.ASM_LENGTH;
				mensaje += " ";
				mensaje += viewController
						.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				esOK = false;
			}

			switch (tipoInstrumento) {
			case OCRA:
				mensaje += getEtiquetaCampoOCRA();
				break;
			case DP270:
				mensaje += getEtiquetaCampoDP270();
				break;
			case SoftToken:
				if (SuiteApp.getSofttokenStatus()) {
					mensaje += getEtiquetaCampoSoftokenActivado();
				} else {
					mensaje += getEtiquetaCampoSoftokenDesactivado();
				}
				break;
			default:
				break;
			}
			mensaje += ".";
		}

		if (!esOK) {
			viewController.showInformationAlert(mensaje);
		}
		return esOK;

	}

	/**
	 * Realiza la invocacion para hacer el registro de operacion
	 */
	public void registrarOperacion() {
		String asm = viewController.pideASM(), newToken = null;
		
		if(tipoInstrumento == TipoInstrumento.SoftToken && SuiteApp.getSofttokenStatus())
			newToken = loadOtpFromSofttoken(TipoOtpAutenticacion.registro);
		
		if(null != newToken)
			asm = newToken;
		
		delegateOp.realizaOperacion(viewController, asm);
	}

	public ArrayList<Object> getDatosRegistroOp() {
		return delegateOp.getDatosRegistroOp();
	}
	
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		return TipoOtpAutenticacion.registro;
	}

	
//	public String getToken() {
//		String token = null;
//		token = GeneraOTPSTDelegate.generarOtpChallenge(delegateOp.getNumeroCuentaParaRegistroOperacion());
//		
//		return token;
//	}
	
	@Override
	public String loadOtpFromSofttoken(TipoOtpAutenticacion tipoOTP) {
		return loadOtpFromSofttoken(tipoOTP, delegateOp);
	}
}
