package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;
import java.util.Hashtable;

import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuConsultarViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.TipoConsultaViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.classes.gui.controllers.BaseViewController;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;

public class TipoConsultaDelegate extends DelegateBaseOperacion {

	public static final long TIPO_CONSULTA_DELEGATE_ID = 1166819871521586228L;
	private TipoConsultaViewController tipoConsultaViewController;

	/**
	 * @return the tipoConsultaViewController
	 */
	public TipoConsultaViewController getTipoConsultaViewController() {
		return tipoConsultaViewController;
	}

	/**
	 * @param tipoConsultaViewController the tipoConsultaViewController to set
	 */
	public void setTipoConsultaViewController(TipoConsultaViewController tipoConsultaViewController) {
		this.tipoConsultaViewController = tipoConsultaViewController;
	}
	

	@Override
	public void performAction(Object obj) {
	
		if(obj instanceof String){
			String selected = (String) obj;
				tipoConsultaViewController.opcionSeleccionada(selected);
		}
	}
	
	
	public ArrayList<Object> getDatosTablaMenuTipoFrecuentes(){
		
		ArrayList<Object> lista = new ArrayList<Object>();

		ArrayList<Object> registros;
		
		Session session = Session.getInstance(SuiteApp.getInstance());
		if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.transferirBancomerF,	session.getClientProfile())) {
		registros = new ArrayList<Object>(2);
		registros.add(Constants.tipoCFOtrosBBVA);
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_tipo_consulta_frecuentes_otrosBBVA_opcion));
		lista.add(registros);
		}
		if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.transferirBancomerF,	session.getClientProfile())) {
		registros = new ArrayList<Object>(2);
		registros.add(Constants.tipoCFCExpress);
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_tipo_consulta_frecuentes_express_opcion));
		lista.add(registros);
		}
		if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.transferirInterbancariaF,	session.getClientProfile())) {
		registros = new ArrayList<Object>(2);
		registros.add(Constants.tipoCFOtrosBancos);
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_tipo_consulta_frecuentes_otrosBancos_opcion));
		lista.add(registros);
		}
		if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.dineroMovilF,	session.getClientProfile())) {
		registros = new ArrayList<Object>(2);
		registros.add(Constants.tipoCFDineroMovil);
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_tipo_consulta_frecuentes_dineromovil_opcion));
		lista.add(registros);
		}
		if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.compraTiempoAireF,	session.getClientProfile())) {
		registros = new ArrayList<Object>(2);
		registros.add(Constants.tipoCFTiempoAire);
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_tipo_consulta_frecuentes_compraTA_opcion));
		lista.add(registros);
		}
		if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.pagoServiciosF,session.getClientProfile())) {
		registros = new ArrayList<Object>(2);
		registros.add(Constants.tipoCFPagosCIE);
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_tipo_consulta_frecuentes_pagoServ_opcion));
		lista.add(registros);
		}
		return lista;
	}
	
	public boolean validaHorarioOperacion(String operacion){
		boolean valido = true;
		if (operacion.equals(Constants.tipoCFOtrosBancos)) {
			valido = Session.getInstance(SuiteApp.getInstance())
					.isOpenHoursForExternalTransfers();

		} else if (operacion.equals(Constants.tipoCFPagosCIE)) {
			valido = Session.getInstance(SuiteApp.getInstance())
					.isOpenHoursForServicePayments();
		}		
		return valido;
	}
	
	public boolean validarUsuarioTDC() {
		suitebancomercoms.aplicaciones.bmovil.classes.common.Session session = suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(SuiteAppAdmonApi.appContext);
		Constants.Perfil perfil = session.getClientProfile();
		Account[] cuentasUsuario = session.getAccounts();
		boolean perfilNoAvanzado = !perfil.equals(Constants.Perfil.avanzado);
		boolean soloTieneUnaCuenta = (cuentasUsuario != null && cuentasUsuario.length == 1);

		if (perfilNoAvanzado || soloTieneUnaCuenta) {
			Account cuentaEje = Tools.obtenerCuentaEje();
			if (cuentaEje != null && cuentaEje.getType().equals("TC")) {
				return false;
			}
		}
		return true;
	}

	public String getTextoMensajeAlertaHorario(String operacion){
		String msj = "";
		if (operacion.equals(Constants.tipoCFOtrosBancos)) {
			msj = Session.getInstance(SuiteApp.getInstance())
					.getOpenHoursForExternalTransfersMessage();
		} else if (operacion.equals(Constants.tipoCFPagosCIE)) {
			msj = Session.getInstance(SuiteApp.getInstance())
					.getOpenHoursForServicePaymentsMessage();
		}
		return msj;
	}
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params, boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {
		 if (tipoConsultaViewController instanceof TipoConsultaViewController) {
			((BmovilViewsController)tipoConsultaViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller);
		 }
		
	}
	
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			
			if (operationId == Server.OP_SOLICITAR_ALERTAS) {
				
				SolicitarAlertasData validacionalertas = (SolicitarAlertasData) response.getResponse();
				if (Constants.ALERT02.equals(validacionalertas.getValidacionAlertas())){
					// 02 no tiene alertas
					
					SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
					SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_dineromovil_recortado_sin_alertas));
				
					
				} else{
				
					setSa(validacionalertas);
					analyzeAlertasRecortadoSinError();
				}
				return;
				
			} 
			

		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
			

		}else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
					
		}		
	}
	
	
	public void comprobarRecortado(){
		Session session = Session.getInstance(SuiteApp.appContext);
		Constants.Perfil perfil = session.getClientProfile();
		
//		if(Constants.Perfil.recortado.equals(perfil)){
//				
//					if(Constants.siEstatusAlertas.equals(session.getEstatusAlertas())){
//						// tiene ALERTAS
						
						solicitarAlertas(getTipoConsultaViewController());
			
//					} else {
//						// NO tiene ALERTAS
//						
//						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
//						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_dineromovil_recortado_sin_alertas));
//						
//					}
//				}
		
	}
}
