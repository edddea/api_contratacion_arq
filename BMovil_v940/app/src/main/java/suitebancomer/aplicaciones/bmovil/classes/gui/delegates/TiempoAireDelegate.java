package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.util.Log;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.model.CatalogoVersionado;
import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerMapperUtil;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionRegistroViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultarFrecuentesViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.RegistrarOperacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.TiempoAireDetalleViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.TiempoAireViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.CompraTiempoAire;
import suitebancomer.aplicaciones.bmovil.classes.model.CompraTiempoAireResult;
import suitebancomer.aplicaciones.bmovil.classes.model.FrecuenteMulticanalResult;
import suitebancomer.aplicaciones.bmovil.classes.model.Payment;
import suitebancomer.aplicaciones.bmovil.classes.model.PaymentExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.Rapido;
import suitebancomer.aplicaciones.bmovil.classes.model.RegistrarOperacionResult;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.classes.gui.controllers.BaseViewController;

public class TiempoAireDelegate extends DelegateBaseAutenticacion {

	public final static long TIEMPO_AIRE_DELEGATE_ID = 0xf6e588275b9a27b3L;
	private final String ctaOrigen_tag = "CM";
	private BaseViewController controladorTiempoAire;
	//private
	CompraTiempoAire compraTiempoAire;
	private boolean frecuente;
	private Constants.Operacion tipoOperacion;
	private Payment[] frecuentes;
	private CompraTiempoAireResult result;
	Payment selectedPayment;
	private boolean fromCunsultar;
	private boolean bajaFrecuente;
	protected SolicitarAlertasData sa;
	//ARR
		public boolean res = false;

	public TiempoAireDelegate(boolean fromConsulta) {
		compraTiempoAire = new CompraTiempoAire();
		this.fromCunsultar = fromConsulta;
		this.bajaFrecuente = false;
	}

	public BaseViewController getcontroladorTiempoAire() {
		return controladorTiempoAire;
	}

	public void setcontroladorTiempoAire(
			BaseViewController controladorTiempoAire) {
		this.controladorTiempoAire = controladorTiempoAire;
	}

	public CompraTiempoAireResult getResult() {
		return result;
	}

	public CompraTiempoAire getCompraTiempoAire() {
		return compraTiempoAire;
	}

	public void setCompraTiempoAire(CompraTiempoAire compraTiempoAire) {
		this.compraTiempoAire = compraTiempoAire;
		//frecuente correo electronico
		aliasFrecuente = null;
		correoFrecuente = null;
	}

	public boolean isFrecuente() {
		return frecuente;
	}

	public void setFrecuente(boolean frecuente) {
		this.frecuente = frecuente;
	}

	public Constants.Operacion getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(Constants.Operacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public Payment[] getFrecuentes() {
		return frecuentes;
	}

	public void setFrecuentes(Payment[] frecuentes) {
		this.frecuentes = frecuentes;
	}

	public boolean isFromCunsultar() {
		return fromCunsultar;
	}

	public void setFromCunsultar(boolean fromCunsultar) {
		this.fromCunsultar = fromCunsultar;
	}

	public boolean isBajaFrecuente() {
		return bajaFrecuente;
	}

	public void setBajaFrecuente(boolean bajaFrecuente) {
		this.bajaFrecuente = bajaFrecuente;
	}

	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la
	 * lista es ordenada seg�n sea reguerido.
	 * 
	 * @return
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext)
				.getClientProfile();

		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext)
				.getAccounts();

		if (profile == Constants.Perfil.avanzado) {
			for (Account acc : accounts) {
				if (acc.isVisible() && (!acc.getType().equals(Constants.CREDIT_TYPE))) {
					accountsArray.add(acc);
					break;
				}
			}

			for (Account acc : accounts) {
				if (!acc.isVisible() && (!acc.getType().equals(Constants.CREDIT_TYPE)))
					accountsArray.add(acc);
			}
		} else {
			for (Account acc : accounts) {
				if (acc.isVisible()&& (!acc.getType().equals(Constants.CREDIT_TYPE)))
					accountsArray.add(acc);
			}
		}

		return accountsArray;
	}

	/**
	 * Validates if the amount is correct.
	 * 
	 * @param amount
	 *            The amount for the transaction.
	 * @return True if the amount is correct.
	 */
	public boolean validaMonto(String amount) {
		int value = -1;

		try {
			value = Integer.parseInt(amount);
		} catch (NumberFormatException ex) {
			if(Server.ALLOW_LOG) Log.e("TransferirMisCuentasDelegate",
					"Error while parsing the amount as an int.", ex);
			return false;
		}

		return (value > 0);
	}

	@Override
	public int getOpcionesMenuResultados() {
		if (frecuente)
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF
					| SHOW_MENU_RAPIDA;
		else if (bajaFrecuente) {
			return super.getOpcionesMenuResultados();
		} else
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF
					| SHOW_MENU_FRECUENTE | SHOW_MENU_RAPIDA;
	}

	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.bmovil_tiempo_aire_icono;
	}

	@Override
	public int getTextoEncabezado() {
		return R.string.tiempo_aire_title;
	}

	@Override
	public int getColorTituloResultado() {
		if (bajaFrecuente) {
			return R.color.magenta;
		} else {
			return R.color.verde_limon;
		}
	}

	@Override
	public String getTextoTituloResultado() {
		if (bajaFrecuente) {
			return controladorTiempoAire
					.getString(R.string.baja_frecuentes_titulo_tabla_resultados);
		} else {
			return controladorTiempoAire
					.getString(R.string.transferir_detalle_operacion_exitosaTitle);
		}
	}

	@Override
	public String getTextoTituloConfirmacion() {
		return controladorTiempoAire.getString(R.string.confirmation_subtitulo);
	}

	@Override
	public String getTextoAyudaResultados() {
		if (bajaFrecuente) {
			return super.getTextoAyudaResultados();
		} else {
			return controladorTiempoAire
					.getString(R.string.tiempo_aire_texto_ayuda_resultados);
		}
	}

	@Override
	public String getTextoSMS() {
		String folio = result.getFolio();
		String importe = Tools.formatAmount(compraTiempoAire.getImporte(),
				false);
		String fecha = Tools.formatDate(result.getFecha());
		String hora = Tools.formatTime(result.getHora());

		StringBuilder msg = new StringBuilder();
		msg.append(SuiteApp.appContext.getString(R.string.smsText_firstFrom)
				+ " "
				+ compraTiempoAire.getCuentaOrigen().getNombreTipoCuenta(
						SuiteApp.getInstance().getResources()));
		msg.append(" "
				+ Tools.hideAccountNumber(compraTiempoAire.getCuentaOrigen()
						.getNumber()) + " "
				+ SuiteApp.appContext.getString(R.string.smsText_al) + " "
				+ compraTiempoAire.getNombreCompania());
		msg.append(" "
				+ Tools.hideAccountNumber(compraTiempoAire.getCelularDestino())
				+ " "
				+ SuiteApp.appContext.getString(R.string.smsText_byQuantity));
		msg.append(" " + importe + " "
				+ SuiteApp.appContext.getString(R.string.smsText_folio) + " "
				+ folio + " " + fecha + " " + hora);
		String msgText = msg.toString().replaceAll("\n", "");
		//System.out.println(msgText);
		return msgText;
	}

	// @Override
	// public String getTextoEmail() {
	// //El correo se compondrá del contenido del componente
	// �ListaDatosViewController�
	// //mostrado en la pantalla de resultados.
	// return super.getTextoEmail();
	// }
	//
	// @Override
	// public String getTextoPDF() {
	// //El PDF se compondrá del contenido del componente
	// �ListaDatosViewController�
	// //mostrado en la pantalla de resultados
	// return super.getTextoPDF();
	// }

	@Override
	public boolean mostrarContrasenia() {
		boolean value;
		if (bajaFrecuente) {
			value = Autenticacion.getInstance()
					.mostrarContrasena(
							this.tipoOperacion,
							Session.getInstance(SuiteApp.appContext)
									.getClientProfile());
		} else {
			value = Autenticacion
					.getInstance()
					.mostrarContrasena(
							this.tipoOperacion,
							Session.getInstance(SuiteApp.appContext)
									.getClientProfile(),
							Tools.getDoubleAmountFromServerString(compraTiempoAire
									.getImporte()));
		}
		return value;
	}

	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			if (bajaFrecuente) {
				tipoOTP = Autenticacion.getInstance().tokenAMostrar(
						this.tipoOperacion,
						Session.getInstance(SuiteApp.appContext)
								.getClientProfile());
			} else {
				tipoOTP = Autenticacion.getInstance().tokenAMostrar(
						this.tipoOperacion,
						Session.getInstance(SuiteApp.appContext)
								.getClientProfile(),
						Tools.getDoubleAmountFromServerString(compraTiempoAire
								.getImporte()));
			}
		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
					"Error on Autenticacion.mostrarNIP execution.", ex);
			tipoOTP = null;
		}

		return tipoOTP;
	}

	@Override
	public boolean mostrarNIP() {
		boolean value;
		if (bajaFrecuente) {
			value = Autenticacion.getInstance()
					.mostrarNIP(
							this.tipoOperacion,
							Session.getInstance(SuiteApp.appContext)
									.getClientProfile());
		} else {
			value = Autenticacion.getInstance()
					.mostrarNIP(
							this.tipoOperacion,
							Session.getInstance(SuiteApp.appContext)
									.getClientProfile());
		}
		return value;
	}

	@Override
	public void performAction(Object obj) {
		if (controladorTiempoAire instanceof TiempoAireViewController
				&& obj instanceof Account) {
			((TiempoAireViewController) controladorTiempoAire)
					.cargaListaSeleccionComponent();
		} else if (controladorTiempoAire instanceof TiempoAireDetalleViewController) {
			if (obj instanceof Account) {
				((TiempoAireDetalleViewController) controladorTiempoAire)
						.reactivarCtaOrigen();
			} else if (obj instanceof String) {
				((TiempoAireDetalleViewController) controladorTiempoAire)
						.actualizarImporte((String) obj);
			} else if (obj instanceof Compania) {
				setConvenioSeleccionado((Compania) obj);
				((TiempoAireDetalleViewController) controladorTiempoAire).importe.setText(R.string.transferir_interbancario_hint_bancodestino);
			}
		} else if (obj instanceof Payment) {
			if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Eligio un frecuente");
			selectedPayment = (Payment) obj;

			boolean companyExists = checkIfCompanyExists(selectedPayment
					.getBeneficiary());
			if (companyExists) {
				esteCelular = selectedPayment
						.getNickname()
						.equals(SuiteApp.appContext
								.getString(R.string.bmovil_common_este_celular));
				Session session = Session.getInstance(SuiteApp.appContext);
				Perfil perfil = session.getClientProfile();
				if (!Constants.Perfil.recortado.equals(perfil)) {
					((TiempoAireViewController) controladorTiempoAire)
							.muestraDetalleTiempoAire(selectedPayment);
				} else {
					// recortado
					if (esteCelular) {
						((TiempoAireViewController) controladorTiempoAire)
								.muestraDetalleTiempoAire(selectedPayment);
					} else {
						comprobarRecortado();
						// controladorTiempoAire.showInformationAlert(" Para comprar tiempo aire a otro celular, por seguridad, es necesario activar tus alertas en cualquier cajero autom�tico Bancomer ");
					}

				}
			} else {
				controladorTiempoAire.showInformationAlert(SuiteApp.appContext
						.getString(R.string.payment_favorite_not_available)
						+ " " + selectedPayment.getBeneficiary());
			}
		}
	}

	private boolean checkIfCompanyExists(String company) {
		Session session = Session.getInstance(SuiteApp.appContext);
		CatalogoVersionado catalog = session.getCatalogoTiempoAire();
		Vector<Object> objetos = catalog.getObjetos();

		for (Object obj : objetos) {
			Compania compania = (Compania) obj;
			if (company.equalsIgnoreCase(compania.getNombre()))
				return true;
		}

		return false;
	}

	public void consultarFrecuentes(String tipoFrecuente) {
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> paramTable = null;
		paramTable = new Hashtable<String, String>();
		// paramTable.put(Server.USERNAME_PARAM, session.getUsername());// NT
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());// IU
		// paramTable.put(Server.NUMERO_CLIENTE_PARAM, session.getClientNumber());// TE
		//paramTable.put(Server.TIPO_CONSULTA_PARAM, tipoFrecuente);// TP
		// doNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable,
		// controladorTiempoAire);

		paramTable.put("TE", session.getClientNumber());//TE
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT,	session.getUsername());//NT
		paramTable.put("TP",	tipoFrecuente);//TP

		//CHECK INVOCACION
				String tipoConsulta = tipoFrecuente;
				if(tipoConsulta.equals(Constants.tipoCFOtrosBBVA))
				{
					//paramTable.put("numeroTelefono", 		session.getUsername());//NT
					//paramTable.put(ServerConstants.JSON_IUM_ETIQUETA,			session.getIum());//IU
					//paramTable.put("numeroCliente",	session.getClientNumber());//TE
					//JAIG SI
					paramTable.put("order","TP*TE*IU*NT");
					((BmovilViewsController)controladorTiempoAire.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION_BBVA, paramTable,true, new PaymentExtract(), isJsonValueCode.NONE,controladorTiempoAire,true);
				}
				else
				{

					//paramTable.put(ServerConstants.PARAMS_TEXTO_IU,			session.getIum());//IU
					//paramTable.put("TE",	session.getClientNumber());//TE
					//paramTable.put("TP",	tipoFrecuente);//TP
					paramTable.put("AP", "");
					//Correo Freceuntes
					paramTable.put("VM", Constants.APPLICATION_VERSION);

					//JAIG SI
					paramTable.put("order","TP*TE*IU*VM*AP*NT");
					((BmovilViewsController)controladorTiempoAire.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable,false, new PaymentExtract(),isJsonValueCode.NONE, controladorTiempoAire,true);

				}
		/*((BmovilViewsController) controladorTiempoAire
				.getParentViewsController()).getBmovilApp()
				.invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION,
						paramTable, controladorTiempoAire, false);
						*/
	}

	@Override
	public void doNetworkOperation(int operationId,
			Hashtable<String, ?> params,boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {
		((BmovilViewsController) controladorTiempoAire
				.getParentViewsController()).getBmovilApp()
				.invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller, true);
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			if (operationId == Server.OP_SOLICITAR_ALERTAS) {
				SolicitarAlertasData validacionalertas = (SolicitarAlertasData) response.getResponse();
				
				if(getTransferErrorResponse() == null){
					if (Constants.ALERT02.equals(validacionalertas.getValidacionAlertas())){
						// 02 no tiene alertas
						
						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.tiempo_aire_mensaje_recortado_sinalertas_comprar_otro_celular));
						
					} else{
					
						setSa(validacionalertas);
						analyzeAlertasRecortadoSinError();
					}
					return;
					
				}else{
					setSa(validacionalertas);
					analyzeAlertasRecortado();
				}
				return;
			} 
			
			if (operationId == Server.OP_VALIDAR_CREDENCIALES) {
				showConfirmacionRegistro();
				return;
			}
			
			if (response.getResponse() instanceof CompraTiempoAireResult) {
				result = (CompraTiempoAireResult) response.getResponse();
				if (frecuente
						&& (tokenAMostrar() == Constants.TipoOtpAutenticacion.codigo || tokenAMostrar() == Constants.TipoOtpAutenticacion.registro)
						&& Tools.isEmptyOrNull(compraTiempoAire
								.getFrecuenteMulticanal()) && !esteCelular) {
					actualizaFrecuenteAMulticanal();
				} else {
					transferenciaExitosa();
				}
			} else if (response.getResponse() instanceof PaymentExtract) {
				PaymentExtract extract = (PaymentExtract) response
						.getResponse();
				setFrecuentes(extract.getPayments());
				mostrarListaFrecuentes();
			} else if (response.getResponse() instanceof FrecuenteMulticanalResult) {
				transferenciaExitosa();
			} else {
				((BmovilViewsController) controladorTiempoAire
						.getParentViewsController())
						.showResultadosViewController(this, -1, -1);
			}

		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
			((BmovilViewsController) controladorTiempoAire
					.getParentViewsController()).getCurrentViewControllerApp()
					.showInformationAlert(response.getMessageText());

		} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
			
			if ((Constants.CODE_CNE0234.equals(response.getMessageCode()))
					|| (Constants.CODE_CNE0235.equals(response.getMessageCode()))
					|| (Constants.CODE_CNE0236.equals(response.getMessageCode()))) {
				
				Session session = Session.getInstance(SuiteApp.appContext);
				Constants.Perfil perfil = session.getClientProfile();

				if (Constants.Perfil.recortado.equals(perfil)) {
					setTransferErrorResponse(response);
					solicitarAlertas(controladorTiempoAire);
				}
			}
		}
	}

	public void comprobarRecortado() {
	
		solicitarAlertas(controladorTiempoAire);
	}

	private void mostrarListaFrecuentes() {
		if (fromCunsultar) {
			((ConsultarFrecuentesViewController) controladorTiempoAire)
					.muestraFrecuentes();
		} else {
			((TiempoAireViewController) controladorTiempoAire)
					.cargaListaSeleccionComponent();
		}

	}

	public ArrayList<Object> getDatosTablaFrecuentes() {
		ArrayList<Object> listaDatos = new ArrayList<Object>();
		Payment esteCel = new Payment();
		esteCel.setNK(SuiteApp.appContext
				.getString(R.string.bmovil_common_este_celular));
		esteCel.setBeneficiary(Session.getInstance(SuiteApp.appContext)
				.getCompaniaUsuario());
		esteCel.setBeneficiaryAccount(Session.getInstance(SuiteApp.appContext)
				.getUsername());
		ArrayList<Object> registro;
		if (frecuentes != null) {
			if (!fromCunsultar) {
				registro = new ArrayList<Object>();
				registro.add(esteCel);
				registro.add(esteCel.getNickname());
				registro.add(esteCel.getBeneficiaryAccount());
				listaDatos.add(registro);
			}
			for (Payment payment : frecuentes) {
				registro = new ArrayList<Object>();
				registro.add(payment);
				registro.add(payment.getNickname());
				registro.add(payment.getBeneficiaryAccount());
				listaDatos.add(registro);
			}
		} else {
			if (!fromCunsultar) {
				registro = new ArrayList<Object>();
				registro.add(esteCel);
				registro.add(esteCel.getNickname());
				registro.add(esteCel.getBeneficiaryAccount());
				listaDatos.add(registro);
			}
		}
		return listaDatos;
	}

	/**
	 * Set the source service for the transaction.
	 * 
	 * @param sourceService
	 *            The source service
	 * @throws InvalidParameterException
	 *             If the source account is null.
	 */
	public void setConvenioSeleccionado(Compania compania)
			throws InvalidParameterException {
		if (null == compania) {
			InvalidParameterException ex = new InvalidParameterException(
					"The source service can not be null.");
			if(Server.ALLOW_LOG) Log.e("TransferirMisCuentasDelegate",
					"The source service can not be null.", ex);
			throw ex;
		} else {
			if (selectedPayment == null) {
				compraTiempoAire.setClaveCompania(compania.getClave());
				compraTiempoAire.setNombreCompania(compania.getNombre());
			}
		}
	}

	public ArrayList<Object> getListaServicios() {

		CatalogoVersionado catalogoServicios = Session.getInstance(
				SuiteApp.appContext).getCatalogoTiempoAire();
		Vector<Object> vectorServicios = catalogoServicios.getObjetos();
		int serviciosSize = vectorServicios.size();
		ArrayList<Object> listaServicios = new ArrayList<Object>(serviciosSize);
		Compania currentConvenio = null;
		for (int i = 0; i < serviciosSize; i++) {
			currentConvenio = (Compania) vectorServicios.get(i);
			listaServicios.add(currentConvenio);
			currentConvenio = null;
		}
		vectorServicios = null;
		return listaServicios;

	}

	public ArrayList<Object> getImportes() {
		ArrayList<Object> listaImportes = new ArrayList<Object>();
		ArrayList<Object> listaServicios = getListaServicios();
		for (int i = 0; i < listaServicios.size(); i++) {
			Compania compania = (Compania) listaServicios.get(i);
			if (compraTiempoAire.getClaveCompania().endsWith(
					compania.getClave())) {
				for (int importe : compania.getMontosValidos()) {
					ArrayList<Object> registro = new ArrayList<Object>();
					registro.add(String.valueOf(importe));
					registro.add(String.valueOf(importe));
					listaImportes.add(registro);
				}
			}
		}

		return listaImportes;
	}

	public void validarDatos() {
		if (((TiempoAireDetalleViewController) controladorTiempoAire).numCelular
				.getText().toString().length() < 10) {
			((TiempoAireDetalleViewController) controladorTiempoAire)
					.showInformationAlert(R.string.nipperAirtimePurchase_invalidPhonenumber);
		} else if (((TiempoAireDetalleViewController) controladorTiempoAire).numCelularConfirmacion
				.getText().toString().length() < 10) {
			((TiempoAireDetalleViewController) controladorTiempoAire)
					.showInformationAlert(R.string.nipperAirtimePurchase_missingPhonenumberConfirmation);
		} else if (!((TiempoAireDetalleViewController) controladorTiempoAire).numCelularConfirmacion
				.getText()
				.toString()
				.equals(((TiempoAireDetalleViewController) controladorTiempoAire).numCelular
						.getText().toString())) {
			((TiempoAireDetalleViewController) controladorTiempoAire)
					.showInformationAlert(R.string.nipperAirtimePurchase_invalidPhonenumberConfirmation);
		} else if (((TiempoAireDetalleViewController) controladorTiempoAire).importe
				.getText().toString().equals("")) {
			((TiempoAireDetalleViewController) controladorTiempoAire)
					.showInformationAlert(R.string.nipperAirtimePurchase_invalidAmount);
		} else {
			if (!controladorTiempoAire.isHabilitado())
				return;
			controladorTiempoAire.setHabilitado(false);
			compraTiempoAire
					.setCelularDestino(((TiempoAireDetalleViewController) controladorTiempoAire).numCelular
							.getText().toString());
			showConfirmacion();
			//ARR
			res = true;
		}
	}

	public void showConfirmacion() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		if (registrarOperacion())
			bMovilVC.showRegistrarOperacion(this);
		else
			bMovilVC.showConfirmacion(this);
	}

	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {

		ArrayList<Object> datosConfirmacion = new ArrayList<Object>();
		ArrayList<String> registro;

		if (bajaFrecuente) {

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.altafrecuente_nombrecorto));
			registro.add(compraTiempoAire.getAliasFrecuente());
			datosConfirmacion.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_confirmacion_numero_celular));// transferir.interbancario.banco.destino
			registro.add(compraTiempoAire.getCelularDestino());
			datosConfirmacion.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_confirmacion_compania_celular));// transferir.interbancario.numero.tarjeta.cuenta
			registro.add(compraTiempoAire.getNombreCompania());
			datosConfirmacion.add(registro);

			ArrayList<Object> entryList = datosConfirmacion;
			for (int i = entryList.size() - 1; i >= 0; i--) {
				try {
					@SuppressWarnings("unchecked")
					ArrayList<String> entry = (ArrayList<String>) entryList
							.get(i);
					if (entry.get(1).trim().equals(Constants.EMPTY_STRING))
						entryList.remove(i);
				} catch (Exception ex) {
					if(Server.ALLOW_LOG) Log.i(getClass().getSimpleName(),
						"Error al quitar el elemento.", ex);
				}
			}

		} else {

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
			registro.add(Tools.hideAccountNumber(compraTiempoAire
					.getCuentaOrigen().getNumber()));
			datosConfirmacion.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_confirmacion_numero_celular));// transferir.interbancario.banco.destino
			registro.add(compraTiempoAire.getCelularDestino());
			datosConfirmacion.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_confirmacion_compania_celular));// transferir.interbancario.numero.tarjeta.cuenta
			registro.add(compraTiempoAire.getNombreCompania());
			datosConfirmacion.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_confirmacion_importe));
			if(Server.ALLOW_LOG) Log.d("Saldo importe recarga", compraTiempoAire.getImporte());
			registro.add(Tools.formatAmount(compraTiempoAire.getImporte(),
					false));
			datosConfirmacion.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_confirmacion_fecha_operacion));
			java.util.Date currentDate = Session.getInstance(
					SuiteApp.appContext).getServerDate();
			registro.add(Tools.dateToString(currentDate));
			datosConfirmacion.add(registro);

		}

		return datosConfirmacion;

	}

	@Override
	public ArrayList<Object> getDatosTablaResultados() {

		ArrayList<Object> datosResultados = new ArrayList<Object>();
		ArrayList<String> registro;

		if (bajaFrecuente) {

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.altafrecuente_nombrecorto));
			registro.add(compraTiempoAire.getAliasFrecuente());
			datosResultados.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_confirmacion_numero_celular));// transferir.interbancario.banco.destino
			registro.add(compraTiempoAire.getCelularDestino());
			datosResultados.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_confirmacion_compania_celular));// transferir.interbancario.numero.tarjeta.cuenta
			registro.add(compraTiempoAire.getNombreCompania());
			datosResultados.add(registro);

		} else {

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
			registro.add(Tools.hideAccountNumber(compraTiempoAire
					.getCuentaOrigen().getNumber()));
			datosResultados.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_confirmacion_numero_celular));// transferir.interbancario.banco.destino
			registro.add(compraTiempoAire.getCelularDestino());
			datosResultados.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_confirmacion_compania_celular));// transferir.interbancario.numero.tarjeta.cuenta
			registro.add(compraTiempoAire.getNombreCompania());
			datosResultados.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_confirmacion_importe));
			if(Server.ALLOW_LOG) Log.d("Saldo importe recarga", compraTiempoAire.getImporte());
			registro.add(Tools.formatAmount(compraTiempoAire.getImporte(),
					false));
			datosResultados.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_resultados_fechaoperacion));
			registro.add(Tools.formatDate(result.getFecha()));
			datosResultados.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_resultados_horaoperacion));
			registro.add(Tools.formatTime(result.getHora()));
			datosResultados.add(registro);

			registro = new ArrayList<String>();
			registro.add(controladorTiempoAire
					.getString(R.string.tiempo_aire_tabla_resultados_folio));
			registro.add(result.getFolio());// agregar format day
			datosResultados.add(registro);
			//frecuente correo electronico
			//aliasFrecuente = compraTiempoAire.getAliasFrecuente() != null && !compraTiempoAire.getAliasFrecuente().isEmpty() ? compraTiempoAire.getAliasFrecuente() : aliasFrecuente;
			if(aliasFrecuente != null && !aliasFrecuente.isEmpty()){
				registro = new ArrayList<String>();
				registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_alias_frecuente));
				registro.add(aliasFrecuente);
				datosResultados.add(registro);
			}

			//correo frecuente
			//correoFrecuente = compraTiempoAire.getCorreoFrecuente() != null && !compraTiempoAire.getCorreoFrecuente().isEmpty() ? compraTiempoAire.getCorreoFrecuente() : correoFrecuente;
			if(correoFrecuente !=null && !correoFrecuente.isEmpty()){
				registro = new ArrayList<String>();
				registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_correo_electronico_frecuente));
				registro.add(correoFrecuente);
				datosResultados.add(registro);
			}
			correoFrecuente = compraTiempoAire.getCorreoFrecuente() != null && !compraTiempoAire.getCorreoFrecuente().isEmpty() ? compraTiempoAire.getCorreoFrecuente() : correoFrecuente;


		}

		return datosResultados;

	}

	@Override
	public void realizaOperacion(
			ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController,
			String contrasenia, String nip, String token, String cvv,
			String campoTarjeta) {
		// prepare data
		Hashtable<String, String> paramTable = null;
		int operacion = 0;
		Session session = Session.getInstance(SuiteApp.appContext);

		operacion = Server.BAJA_FRECUENTE;
		// completar hashtable
		paramTable = new Hashtable<String, String>();

		paramTable.put(Server.USERNAME_PARAM, session.getUsername());// "NT"
		paramTable.put(Server.PASSWORD_PARAM,
				Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia);// NP
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());// IU
		paramTable.put(Server.NUMERO_CLIENTE_PARAM, session.getClientNumber());// TE
		paramTable.put(Server.DESCRIPCION_PARAM,
				compraTiempoAire.getAliasFrecuente());// DE
		paramTable.put(Server.PAYMENT_ACCOUNT_PARAM,
				compraTiempoAire.getCelularDestino());// CA
		paramTable.put(Server.BENEFICIARIO_PARAM,
				compraTiempoAire.getNombreCompania());// BF
		paramTable.put(Server.REFERENCIA_NUMERICA_PARAM,
				compraTiempoAire.getCelularDestino());// BF
		// String idToken = (tokenAMostrar() !=
		// Constants.TipoOtpAutenticacion.ninguno) ? "T" : "";
		paramTable.put(Server.ID_TOKEN, selectedPayment.getIdToken());
		paramTable.put(Server.TIPO_CONSULTA_PARAM, Constants.TPTiempoAire);// TP
		String va = Autenticacion.getInstance().getCadenaAutenticacion(
				this.tipoOperacion,
				Session.getInstance(SuiteApp.appContext).getClientProfile());
		paramTable.put(Server.VA_PARAM, Tools.isEmptyOrNull(va) ? "" : va);// validar
		paramTable.put(Server.ID_CANAL, compraTiempoAire.getIdCanal());
		paramTable.put(Server.USUARIO, compraTiempoAire.getUsuario());
		Hashtable<String, String> paramTable2= ServerMapperUtil.mapperBaja(paramTable);
		doNetworkOperation(operacion, paramTable2,false,null,isJsonValueCode.NONE,
				confirmacionAutenticacionViewController);

	}

	@Override
	public void realizaOperacion(
			ConfirmacionViewController confirmacionViewController,
			String contrasenia, String nip, String token, String campoTarjeta) {
		// prepare data
		Hashtable<String, String> paramTable = null;
		int operacion = 0;
		Session session = Session.getInstance(SuiteApp.appContext);

		if (bajaFrecuente) {

			operacion = Server.BAJA_FRECUENTE;
			// completar hashtable
			paramTable = new Hashtable<String, String>();

			paramTable.put(Server.USERNAME_PARAM, session.getUsername());// "NT"
			paramTable.put(Server.PASSWORD_PARAM,
					Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia);// NP
			paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());// IU
			paramTable.put(Server.NUMERO_CLIENTE_PARAM,
					session.getClientNumber());// TE
			paramTable.put(Server.DESCRIPCION_PARAM,
					compraTiempoAire.getAliasFrecuente());// DE
			paramTable.put(Server.PAYMENT_ACCOUNT_PARAM,
					compraTiempoAire.getCelularDestino());// CA
			paramTable.put(Server.BENEFICIARIO_PARAM,
					compraTiempoAire.getNombreCompania());// BF
			paramTable.put(Server.REFERENCIA_NUMERICA_PARAM,
					compraTiempoAire.getCelularDestino());// BF
			paramTable.put(Server.TIPO_CONSULTA_PARAM, Constants.TPTiempoAire);// TP
			String va = Autenticacion.getInstance()
					.getCadenaAutenticacion(
							this.tipoOperacion,
							Session.getInstance(SuiteApp.appContext)
									.getClientProfile());
			paramTable.put(Server.VA_PARAM, Tools.isEmptyOrNull(va) ? "" : va);// validar
			Hashtable<String, String> paramsTable2= ServerMapperUtil.mapperBaja(paramTable);
			//JAIG
			doNetworkOperation(operacion, paramsTable2, false, null, isJsonValueCode.NONE, confirmacionViewController);

		} else {

			operacion = Server.COMPRA_TIEMPO_AIRE;
			// completar hashtable
			String type = compraTiempoAire.getCuentaOrigen().getType();
			boolean esExpress = Constants.EXPRESS_TYPE.equals(type);
			String ctaOrigen = esExpress ? ctaOrigen_tag
					+ compraTiempoAire.getCuentaOrigen().getNumber()
					: compraTiempoAire.getCuentaOrigen().getFullNumber();

			Hashtable<String, String> params = new Hashtable<String, String>();// generaParametrosParaCuentas(transferenciaInterbancaria.getCuentaOrigen().getType(),
			// transferenciaInterbancaria.getTipoCuentaDestino());

			params.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());// "NT"
			params.put(ServerConstants.PARAMS_TEXTO_NP, Tools.isEmptyOrNull(contrasenia) ? ""
					: contrasenia);// NP
			params.put(ServerConstants.IUM_ETIQUETA, session.getIum());// IU
			params.put("NU",
					compraTiempoAire.getCelularDestino());// NU
			params.put("CC", ctaOrigen);// CC
			params.put("OA", compraTiempoAire.getClaveCompania());// OA
			params.put("IM",
					compraTiempoAire.getImporte() == null ? "" : compraTiempoAire
							.getImporte());// IM
			params.put("NI", Tools.isEmptyOrNull(nip) ? "" : nip);
			params.put("CV", "");
			params.put("OT", Tools.isEmptyOrNull(token) ? ""
					: token);
			String va = Autenticacion
					.getInstance()
					.getCadenaAutenticacion(
							this.tipoOperacion,
							Session.getInstance(SuiteApp.appContext)
									.getClientProfile(),
							Tools.getDoubleAmountFromServerString(compraTiempoAire
									.getImporte()));
			params.put("VA", Tools.isEmptyOrNull(va) ? "" : va);
			//JAIG
			doNetworkOperation(operacion, params, false, new CompraTiempoAireResult(), isJsonValueCode.NONE, confirmacionViewController);
		}

		//doNetworkOperation(operacion, paramTable, confirmacionViewController);
	}

	@Override
	public ArrayList<Object> getDatosHeaderTablaFrecuentes() {
		ArrayList<Object> registros = new ArrayList<Object>();
		registros.add("");
		registros.add(controladorTiempoAire
				.getString(R.string.altafrecuente_nombrecorto));
		registros.add(controladorTiempoAire
				.getString(R.string.tiempo_aire_encabezado_lista));
		return registros;
	}

	@SuppressWarnings("unchecked")
	public void operarFrecuente(int indexSelected) {
		ArrayList<Object> arlPayments = null;
		
		Session session = Session.getInstance(SuiteApp.appContext);
		Perfil perfil = session.getClientProfile();
		if (!Constants.Perfil.recortado.equals(perfil) || (Constants.Perfil.recortado.equals(perfil)&& esteCelular)) {
		
			if (controladorTiempoAire instanceof ConsultarFrecuentesViewController) {
				arlPayments = ((ConsultarFrecuentesViewController) controladorTiempoAire)
						.getListaFrecuentes().getLista();
			}
	
			Payment selectedPayment = frecuentes[indexSelected];
			if (arlPayments != null && arlPayments.size() > 0) {
				selectedPayment = (Payment) ((ArrayList<Object>) arlPayments
						.get(indexSelected)).get(0);
			}
	
			ArrayList<Account> cuentas = cargaCuentasOrigen();
			if (cuentas.size() > 0)
				getCompraTiempoAire().setCuentaOrigen(cuentas.get(0));
	
			if (checkIfCompanyExists(selectedPayment.getBeneficiary())) {
				if (controladorTiempoAire != null) {
					frecuente = true;
					this.selectedPayment = selectedPayment;
					setTipoOperacion(Constants.Operacion.compraTiempoAireF);
					compraTiempoAire.setNombreCompania(selectedPayment
							.getBeneficiary());
					compraTiempoAire.setCelularDestino(selectedPayment
							.getBeneficiaryAccount());
					compraTiempoAire.setIdCanal(selectedPayment.getIdCanal());
					compraTiempoAire.setUsuario(selectedPayment.getUsuario());
					//frecuente correo electronico
					compraTiempoAire.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());
					compraTiempoAire.setAliasFrecuente(selectedPayment.getNickname());
					compraTiempoAire.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());

					((BmovilViewsController) controladorTiempoAire
							.getParentViewsController())
							.showTiempoAireDetalleViewController();
				}
			} else {
				controladorTiempoAire.showInformationAlert(SuiteApp.appContext
						.getString(R.string.payment_favorite_not_available)
						+ " "
						+ selectedPayment.getBeneficiary());
			}
		} else if (Constants.Perfil.recortado.equals(perfil)){
			// No ha seleccionado este celular
			comprobarRecortado();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void eliminarFrecuente(int indexSelected) {
		setBajaFrecuente(true);
		ArrayList<Object> arlPayments = null;
		if (controladorTiempoAire instanceof ConsultarFrecuentesViewController) {
			arlPayments = ((ConsultarFrecuentesViewController) controladorTiempoAire)
					.getListaFrecuentes().getLista();
		}

		Payment selectedPayment = frecuentes[indexSelected];
		if (arlPayments != null && arlPayments.size() > 0) {
			selectedPayment = (Payment) ((ArrayList<Object>) arlPayments
					.get(indexSelected)).get(0);
		}
		this.selectedPayment = selectedPayment;
		setTipoOperacion(Constants.Operacion.bajaFrecuente);
		compraTiempoAire.setNombreCompania(selectedPayment.getBeneficiary());
		ArrayList<Object> listaServicios = getListaServicios();
		for (int i = 0; i < listaServicios.size(); i++) {
			Compania compania = (Compania) listaServicios.get(i);
			if (compraTiempoAire.getNombreCompania().endsWith(
					compania.getNombre())) {
				compraTiempoAire.setClaveCompania(compania.getClave());
			}
		}
		compraTiempoAire.setCelularDestino(selectedPayment
				.getBeneficiaryAccount());
		compraTiempoAire.setAliasFrecuente(selectedPayment.getNickname());
		compraTiempoAire.setIdCanal(selectedPayment.getIdCanal());
		compraTiempoAire.setUsuario(selectedPayment.getUsuario());
		showConfirmacionAutenticacion();
	}

	@Override
	public Hashtable<String, String> getParametrosAltaFrecuentes() {
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> paramTable = null;
		paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
		paramTable.put(ServerConstants.PARAMS_TEXTO_IU, session.getIum());
		paramTable.put("TE", session.getClientNumber());
		paramTable.put("CA", compraTiempoAire.getCelularDestino());
		paramTable.put("BF",compraTiempoAire.getNombreCompania());
		paramTable.put("CP", "");
		paramTable.put("TP", Constants.TPTiempoAire);
		paramTable.put("OA", compraTiempoAire.getNombreCompania());
		paramTable.put("CB", "");
		paramTable.put("IM", "");
		paramTable.put("RF", "");
		paramTable.put("AP", "");
		paramTable.put("CV", "");
		paramTable.put("CC", "");
		return paramTable;
	}

	@Override
	public ArrayList<Object> getDatosTablaAltaFrecuentes() {
		ArrayList<Object> datosResultados = new ArrayList<Object>();
		ArrayList<String> registro;
		registro = new ArrayList<String>();
		registro.add(controladorTiempoAire
				.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
		registro.add(Tools.hideAccountNumber(compraTiempoAire.getCuentaOrigen()
				.getNumber()));
		datosResultados.add(registro);

		registro = new ArrayList<String>();
		registro.add(controladorTiempoAire
				.getString(R.string.tiempo_aire_tabla_confirmacion_numero_celular));// transferir.interbancario.banco.destino
		registro.add(compraTiempoAire.getCelularDestino());
		datosResultados.add(registro);

		registro = new ArrayList<String>();
		registro.add(controladorTiempoAire
				.getString(R.string.tiempo_aire_tabla_confirmacion_compania_celular));// transferir.interbancario.numero.tarjeta.cuenta
		registro.add(compraTiempoAire.getNombreCompania());
		datosResultados.add(registro);

		registro = new ArrayList<String>();
		registro.add(controladorTiempoAire
				.getString(R.string.tiempo_aire_tabla_resultados_fechaoperacion));
		registro.add(Tools.formatDate(result.getFecha()));
		datosResultados.add(registro);
		return datosResultados;
	}

	/**
	 * envia la actualizacion del frecuente
	 */
	private void actualizaFrecuenteAMulticanal() {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), "actualizaFrecuenteAMulticanal");
		Session session = Session.getInstance(SuiteApp.appContext);

		if (compraTiempoAire.getCelularDestino().equals(session.getUsername())) {
			compraTiempoAire.setIdCanal(Constants.EMPTY_STRING);
			compraTiempoAire.setUsuario(Constants.EMPTY_STRING);
		}

		Hashtable<String, String> paramTable = null;
		paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());
		paramTable.put("TE", session.getClientNumber());
		paramTable.put("TP", Constants.TPTiempoAire);
		paramTable.put("CA",
				compraTiempoAire.getCelularDestino());
		paramTable.put("DE", compraTiempoAire.getAliasFrecuente());
		paramTable.put("RF", "");
		paramTable.put("BF",
				compraTiempoAire.getNombreCompania());
		paramTable.put("CB", "");
		paramTable.put("CN", compraTiempoAire.getIdCanal());
		paramTable.put("US", compraTiempoAire.getUsuario());
		paramTable.put("OA", "");
		//freceunte correo electronico
		paramTable.put("CE", compraTiempoAire.getCorreoFrecuente());

		doNetworkOperation(Server.ACTUALIZAR_FRECUENTE, paramTable,false, new FrecuenteMulticanalResult(),isJsonValueCode.NONE,
				controladorTiempoAire);
	}

	/**
	 * continuacion del flujo despues de actulizar el frecuente continuacion del
	 * flujo normal para mostrar resultados
	 */
	private void transferenciaExitosa() {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), "transferenciaExitosa");
		Session session = Session.getInstance(SuiteApp.appContext);
		int longitud = compraTiempoAire.getImporte().length();
		BigDecimal fullImporte = new BigDecimal(compraTiempoAire
				.getCuentaOrigen().getBalance());
		fullImporte = fullImporte.setScale(2, RoundingMode.HALF_UP);
		fullImporte = fullImporte.subtract(new BigDecimal(compraTiempoAire
				.getImporte().substring(0, longitud - 2)));
		session.actualizaMonto(compraTiempoAire.getCuentaOrigen(), Tools
				.convertDoubleToBigDecimalAndReturnString(fullImporte
						.doubleValue()));
		((BmovilViewsController) controladorTiempoAire
				.getParentViewsController()).showResultadosViewController(this,
				-1, -1);
	}

	public void showConfirmacionAutenticacion() {
		BmovilViewsController bmovilParentController = ((BmovilViewsController) controladorTiempoAire
				.getParentViewsController());
		bmovilParentController.showConfirmacionAutenticacionViewController(
				this, getNombreImagenEncabezado(), getTextoEncabezado(),
				R.string.confirmation_subtitulo);
	}

	// #region Rapido
	private boolean operacionRapida;

	public boolean isOperacionRapida() {
		return operacionRapida;
	}

	public void cargarRapido(Rapido rapido) {
		operacionRapida = (null != rapido);

		if (null == rapido)
			return;

		if (null == compraTiempoAire)
			compraTiempoAire = new CompraTiempoAire();

		compraTiempoAire.setCelularDestino(rapido.getTelefonoDestino());
		compraTiempoAire.setImporte(Tools.formatAmountForServer(String
				.valueOf(rapido.getImporte())));

		// Cargar compa�ia de celular
		Compania compania = null;
		for (Object comp : Session.getInstance(SuiteApp.appContext)
				.getCatalogoDineroMovil().getObjetos()) {
			compania = (Compania) comp;
			if (compania.getNombre().equalsIgnoreCase(
					rapido.getCompaniaCelular()))
				break;
			compania = null;
		}
		// Agregar la compa�ia si se encontro en los catálogos.
		if (null != compania) {
			compraTiempoAire.setClaveCompania(compania.getClave());
			compraTiempoAire.setNombreCompania(compania.getNombre());
		}

		Account cuenta = Tools.obtenerCuenta(rapido.getCuentaOrigen());
		if (!Constants.INVALID_ACCOUNT_NUMBER.equals(cuenta.getNumber()))
			compraTiempoAire.setCuentaOrigen(cuenta);
		else
			compraTiempoAire.setCuentaOrigen(Tools.obtenerCuentaEje());
	}

	// #endregion

	/**
	 * @category RegistrarOperacion
	 * @return
	 */
	public boolean registrarOperacion() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext)
				.getClientProfile();
		Operacion operacion;
		if (frecuente)
			operacion = Operacion.compraTiempoAireF;
		else
			operacion = Operacion.compraTiempoAire;
		Autenticacion aut = Autenticacion.getInstance();
		boolean value = aut.validaRegistro(operacion, perfil);
		if(Server.ALLOW_LOG) Log.d("RegistroOP", value + " compraTiempoAireF? " + frecuente);
		return value;
	}

	/**
	 * @category RegistrarOperacion
	 * @return
	 */
	protected ArrayList<Object> getDatosRegistroOp() {
		ArrayList<Object> datosConfirmacion = new ArrayList<Object>();
		ArrayList<String> registro;

		registro = new ArrayList<String>();
		registro.add(controladorTiempoAire
				.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
		registro.add(Tools.hideAccountNumber(compraTiempoAire.getCuentaOrigen()
				.getNumber()));
		datosConfirmacion.add(registro);

		registro = new ArrayList<String>();
		registro.add(controladorTiempoAire
				.getString(R.string.tiempo_aire_tabla_confirmacion_numero_celular));// transferir.interbancario.banco.destino
		registro.add(compraTiempoAire.getCelularDestino());
		datosConfirmacion.add(registro);

		registro = new ArrayList<String>();
		registro.add(controladorTiempoAire
				.getString(R.string.tiempo_aire_tabla_confirmacion_compania_celular));// transferir.interbancario.numero.tarjeta.cuenta
		registro.add(compraTiempoAire.getNombreCompania());
		datosConfirmacion.add(registro);

		// registro = new ArrayList<String>();
		// registro.add(controladorTiempoAire.getString(R.string.tiempo_aire_tabla_confirmacion_importe));
		// if(Server.ALLOW_LOG) Log.d("Saldo importe recarga", compraTiempoAire.getImporte());
		// registro.add(Tools.formatAmount(compraTiempoAire.getImporte(),false));
		// datosConfirmacion.add(registro);

		registro = new ArrayList<String>();
		registro.add(controladorTiempoAire
				.getString(R.string.tiempo_aire_tabla_confirmacion_fecha_operacion));
		java.util.Date currentDate = Session.getInstance(SuiteApp.appContext)
				.getServerDate();
		registro.add(Tools.dateToString(currentDate));
		datosConfirmacion.add(registro);

		return datosConfirmacion;
	}

	/**
	 * @category RegistrarOperacion
	 */
	@Override
	public ArrayList<Object> getDatosRegistroExitoso() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> registro;

		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext
				.getString(R.string.bmovil_registrar_tabla_op));
		registro.add(SuiteApp.appContext
				.getString(R.string.bmovil_registrar_tabla_exito));
		tabla.add(registro);

		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext
				.getString(R.string.bmovil_registrar_tabla_cuenta));
		registro.add(compraTiempoAire.getCelularDestino());
		tabla.add(registro);

		return tabla;
	}

	/**
	 * @category RegistrarOperacion realiza el registro de operacion
	 * @param rovc
	 * @param token
	 */
	@Override
	protected void realizaOperacion(RegistrarOperacionViewController rovc,
			String token) {
		int operationId = Server.OP_VALIDAR_CREDENCIALES;
		Hashtable<String, String> params = new Hashtable<String, String>();

		Session session = Session.getInstance(SuiteApp.appContext);
		String cuentaDestino = compraTiempoAire.getCelularDestino();

		params.put(ServerConstants.ID_OPERACION, ServerConstants.COMPRA_TIEMPO_AIRE);
		params.put(ServerConstants.CELULAR_BENEFICIARIO, compraTiempoAire.getCelularDestino());
		params.put(ServerConstants.COMPANIA_BENEFICIARIO, compraTiempoAire.getNombreCompania());

		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		params.put(ServerConstants.CVE_ACCESO, "");
		params.put(Server.J_NIP, "");
		params.put(Server.J_CVV2, "");
		params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
		params.put(Server.J_AUT, "00010");
		params.put("tarjeta5Dig", "");
		params.put(ServerConstants.IUM, session.getIum());
		params.put(ServerConstants.VERSION, Constants.APPLICATION_VERSION);
		doNetworkOperation(operationId, params,true,new RegistrarOperacionResult(),isJsonValueCode.NONE, rovc);
	}

	/**
	 * @category RegistrarOperacion Realiza la operacion de tranferencia desde
	 *           la pantalla de confirmacionRegistro
	 */
	@Override
	public void realizaOperacion(
			ConfirmacionRegistroViewController viewController,
			String contrasenia, String nip, String token) {

		Session session = Session.getInstance(SuiteApp.appContext);
		String va = Autenticacion.getInstance().getCadenaAutenticacion(
				this.tipoOperacion,
				session.getClientProfile(),
				Tools.getDoubleAmountFromServerString(compraTiempoAire
						.getImporte()));

		int operacion = Server.COMPRA_TIEMPO_AIRE;

		String type = compraTiempoAire.getCuentaOrigen().getType();
		boolean esExpress = Constants.EXPRESS_TYPE.equals(type);
		String ctaOrigen = esExpress ? ctaOrigen_tag
				+ compraTiempoAire.getCuentaOrigen().getNumber()
				: compraTiempoAire.getCuentaOrigen().getFullNumber();

		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.ID_OPERACION, ServerConstants.COMPRA_TIEMPO_AIRE);
		params.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());// "NT"
		params.put(ServerConstants.PARAMS_TEXTO_NP, Tools.isEmptyOrNull(contrasenia) ? ""
				: contrasenia);// NP
		params.put(ServerConstants.IUM_ETIQUETA, session.getIum());// IU
		params.put("NU",
				compraTiempoAire.getCelularDestino());// NU
		params.put("CC", ctaOrigen);// CC
		params.put("OA", compraTiempoAire.getClaveCompania());// OA
		params.put("IM",
				compraTiempoAire.getImporte() == null ? "" : compraTiempoAire
						.getImporte());// IM
		params.put("NI", Tools.isEmptyOrNull(nip) ? "" : nip);
		params.put("CV", "");
		params.put("OT", Tools.isEmptyOrNull(token) ? ""
				: token);
		params.put("VA", Tools.isEmptyOrNull(va) ? "" : va);
		//JAIG
		doNetworkOperation(operacion, params,false,new CompraTiempoAireResult(),isJsonValueCode.NONE, viewController);
	}

	/**
	 * @category RegistrarOperacion Muestra la pantalla de confirmacion registro
	 */
	public void showConfirmacionRegistro() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		bMovilVC.showConfirmacionRegistro(this);
	}

	@Override
	public String getNumeroCuentaParaRegistroOperacion() {
		String cuentaDestino = compraTiempoAire.getCelularDestino();
		return cuentaDestino.substring(cuentaDestino.length() - 5);
	}

	/**
	 * Bandera para indentificar si se trata del frecuente "Este Celular"
	 */
	private boolean esteCelular = false;

	/**
	 * Estblece el valor para la bandera "esteCelular".
	 * 
	 * @param esteCelular
	 *            El valor a establecer.
	 */
	public void setEsteCelular(boolean esteCelular) {
		this.esteCelular = esteCelular;
	}
}
