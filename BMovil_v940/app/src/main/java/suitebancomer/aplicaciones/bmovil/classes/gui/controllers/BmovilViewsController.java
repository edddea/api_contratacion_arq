package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Looper;
import android.util.Log;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;
import com.bancomer.mbanking.softtoken.SuiteAppApi;
import com.google.zxing.client.android.CaptureActivity;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.timer.TimerController;
import mtto.suitebancomer.aplicaciones.bmovil.classes.entrada.InitMantenimiento;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.OlvidastePasswordViewController;
import suitebancomer.activacion.aplicaciones.bmovil.classes.entrada.InitActivacion;
import suitebancomer.activacion.aplicaciones.bmovil.classes.gui.controllers.ActivacionViewController;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.activities.BaseActivity;
import suitebancomer.aplicaciones.bbvacredit.gui.activities.MenuPrincipalActivity;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.entrada.InitContratacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.MenuHamburguesaViewsControllers;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.entradas.InitReactivacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.AltaFrecuenteDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.AltaRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.BorrarDatosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.CampaniaPaperlessDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfigurarAlertasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionAsignacionSpeiDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionRegistroDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaInterbancariosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaMovimientosInterbancariosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaRapidosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultarEstatusEnvioEstadodeCuentaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ContratoOfertaConsumoDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DepositosRecibidosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetalleOfertaConsumoDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetalleOfertaEFIDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetalleOfertaILCDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DineroMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.EnviarCorreoDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.EstadodeCuentaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ExitoEFIDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ExitoILCDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ExitoOfertaConsumoDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.InterbancariosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MantenimientoSpeiMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuAdministrarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuComprarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuConsultarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuPrincipalDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuTransferenciasInterbancariasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MisCuentasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ModificarImporteDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MovimientosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ObtenerComprobantesDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.OtrosBBVADelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagoServiciosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagoTdcDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PopUpPaperlessDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.RegistrarOperacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ResultadosAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ResultadosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TiempoAireDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TipoConsultaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirMisCuentasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.PagoServicio;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.aplicaciones.bmovil.classes.model.Rapido;
import suitebancomer.aplicaciones.bmovil.classes.model.TextoPaperless;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaDineroMovil;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaInterbancaria;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaOtrosBBVA;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.IngresoDatosSTViewController;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.controllers.BaseViewsController;
import suitebancomer.classes.gui.controllers.MenuSuiteViewController;
import suitebancomer.classes.gui.delegates.MenuSuiteDelegate;
import suitebancomer.classes.gui.views.CuentaOrigenListViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.model.BanderasServer;
import suitebancomer.classes.gui.delegates.BaseDelegate;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;
import tracking.TrackingHelper;

//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ContratacionAutenticacionDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ContratacionDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.QuitarSuspencionDelegate;
//SPEI
//Retiro sin tarjeta


public class BmovilViewsController extends BaseViewsController {

	private BmovilApp bmovilApp;
	//AMZ
	public ArrayList<String> estados = new ArrayList<String>();
	private MenuPrincipalViewController controladorMenuPrincipal = new MenuPrincipalViewController();
	//AMZ
	
	/*
	 * @Override public void setCurrentActivity(BaseViewController
	 * currentViewController) { // TODO Auto-generated method stub
	 * super.setCurrentActivity(currentViewController); Log.i("Si", "No"); }
	 */
	public BmovilViewsController(BmovilApp bmovilApp) {
		super();
		this.bmovilApp = bmovilApp;
	}

	public void cierraViewsController() {
		bmovilApp = null;
		clearDelegateHashMap();
		super.cierraViewsController();
	}

	public void cerrarSesionBackground() 	{
		SuiteApp.getInstance().getBmovilApplication()
				.setApplicationInBackground(true);
		bmovilApp.logoutApp(true);
	}

	@Override
	public void showMenuInicial() {
		showMenuPrincipal();
	}

	public void showMenuPrincipal() {
		showMenuPrincipal(false);
	}

	public void showMenuPrincipal(boolean inverted) {

		//One CLick
		MenuPrincipalDelegate delegate = (MenuPrincipalDelegate) getBaseDelegateForKey(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuPrincipalDelegate(new Promociones());
			addDelegateToHashMap(
					MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID, delegate);
		}		
		//Termina One click

		showViewController(MenuPrincipalViewController.class,
				Intent.FLAG_ACTIVITY_CLEAR_TOP, inverted);
		SuiteApp.getInstance().getBmovilApplication()
				.setApplicationLogged(true);

	}

	public void showPantallaActivacion() {
		showViewController(ActivacionViewController.class,Intent.FLAG_ACTIVITY_NO_HISTORY);
	}

	public void showBorrarDatos() {
		BorrarDatosDelegate borrarDatosDelegate = new BorrarDatosDelegate();
		addDelegateToHashMap(BorrarDatosDelegate.BORRAR_DATOS_DELEGATE_ID,
				borrarDatosDelegate);
		showViewController(BorrarDatosViewController.class);
	}

	public BmovilApp getBmovilApp() {
		return bmovilApp;
	}

	public void setBmovilApp(BmovilApp bmovilApp) {
		this.bmovilApp = bmovilApp;
	}

	public void showMisCuentasViewController() {
		MisCuentasDelegate delegate = (MisCuentasDelegate) getBaseDelegateForKey(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MisCuentasDelegate();
			addDelegateToHashMap(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID,
					delegate);
		}
		showViewController(MisCuentasViewController.class);
	}

	public void showConsultarMovimientos() {
		MovimientosDelegate delegate = (MovimientosDelegate) getBaseDelegateForKey(MovimientosDelegate.MOVIMIENTOS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MovimientosDelegate();
			addDelegateToHashMap(MovimientosDelegate.MOVIMIENTOS_DELEGATE_ID,
					delegate);
		}
		showViewController(ConsultaMovimientosViewController.class);
	}
	
	public void showConsultarDepositosRecibidosCuenta(Account cuenta) {
		DepositosRecibidosDelegate delegate = (DepositosRecibidosDelegate) getBaseDelegateForKey(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new DepositosRecibidosDelegate();
			addDelegateToHashMap(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID,
					delegate);
		}
		delegate.setCuentaActual(cuenta);
		showViewController(ConsultarDepositosRecibidosViewController.class);
	}
	
	public void showConsultarDepositosRecibidos() {
		DepositosRecibidosDelegate delegate = (DepositosRecibidosDelegate) getBaseDelegateForKey(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new DepositosRecibidosDelegate();
			addDelegateToHashMap(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID,
					delegate);
		}
		showViewController(ConsultarDepositosRecibidosViewController.class);
	}
	
	public void showConsultarObtenerComprobante() {
		ObtenerComprobantesDelegate delegate = (ObtenerComprobantesDelegate) getBaseDelegateForKey(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID);
		if (delegate == null) {
			delegate = new ObtenerComprobantesDelegate();
			addDelegateToHashMap(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID,
					delegate);
		}
		showViewController(ConsultarObtenerComprobantesViewController.class);
	}


	public void showConsultarEstadosDeCuenta() {
		EstadodeCuentaDelegate delegate = (EstadodeCuentaDelegate) getBaseDelegateForKey(EstadodeCuentaDelegate.ESTADODECUENTA_DELEGATE_ID);
		if (delegate == null) {
			delegate = new EstadodeCuentaDelegate();
			addDelegateToHashMap(EstadodeCuentaDelegate.ESTADODECUENTA_DELEGATE_ID,
					delegate);
		}
		showViewController(EstadodeCuentaViewController.class);
	}

	/**
	 * Muestra la ventana de consultar envios de dinero movil.
	 */
	public void showConsultarDineroMovil() {
		DineroMovilDelegate delegate = (DineroMovilDelegate) getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
		if (delegate == null) {
			delegate = new DineroMovilDelegate();
			addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
					delegate);
		}
		showViewController(ConsultarDineroMovilViewController.class);
	}
	
	/**
	 * Muestra la pantalla para consultar Retiro sin tarjeta.
	 */
	public void showConsultarRetiroSinTarjeta() {
		ConsultaRetiroSinTarjetaDelegate delegate = (ConsultaRetiroSinTarjetaDelegate) getBaseDelegateForKey(ConsultaRetiroSinTarjetaDelegate.RETIRO_SIN_TARJETA_DELEGATE_ID);

		if (delegate == null) {
			delegate = new ConsultaRetiroSinTarjetaDelegate();
			addDelegateToHashMap(
					ConsultaRetiroSinTarjetaDelegate.RETIRO_SIN_TARJETA_DELEGATE_ID,
					delegate);
		}
		//delegate.resetPeriodo();
		showViewController(ConsultaRetiroSinTarjetaViewController.class);
	}

	public void showCuentaOrigenListViewController() {
		showViewController(CuentaOrigenListViewController.class);
	}

	public void showDetalleMovimientosViewController() {
		showViewController(DetalleMovimientosViewController.class);
	}

	@Override
	public void onUserInteraction() {
		
		/*if (bmovilApp != null) {
			if(SuiteApp.getInstance().getBmovilApplication().getSessionTimer()==null){
				bmovilApp.initTimer();
			}
			bmovilApp.resetLogoutTimer();
		}*/

		if(SuiteApp.getInstance().getBmovilApplication().isApplicationLogged()){
			TimerController timerContr = TimerController.getInstance();
			timerContr.resetTimer();
		}
		super.onUserInteraction();
	}

	@Override
	public boolean consumeAccionesDeReinicio() {
		if (!SuiteApp.getInstance().getBmovilApplication()
				.isApplicationLogged()) {
			((MenuSuiteViewController) SuiteApp.getInstance()
					.getSuiteViewsController().getCurrentViewControllerApp())
					.setShouldHideLogin(true);
			SuiteApp.getInstance().getSuiteViewsController()
					.showMenuSuite(true);
			return true;
		}
		return false;
	}

	@Override
	public boolean consumeAccionesDePausa() {
		if (SuiteApp.getInstance().getBmovilApplication().isApplicationLogged()
				&& !isActivityChanging()) {
			cerrarSesionBackground();
			return true;
		}
		return false;
	}

	@Override
	public boolean consumeAccionesDeAlto() {
		if (!SuiteApp.getInstance().getBmovilApplication().isChangingActivity()
				&& currentViewControllerApp != null) {
			currentViewControllerApp.hideSoftKeyboard();
		}
		SuiteApp.getInstance().getBmovilApplication()
				.setChangingActivity(false);
		return true;
	}

	/*
	 * Muestra el menu de administrar
	 */
	public void showMenuAdministrar() {
		MenuAdministrarDelegate delegate = (MenuAdministrarDelegate) getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuAdministrarDelegate();
			addDelegateToHashMap(
					MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID,
					delegate);
		}
		showViewController(MenuAdministrarViewController.class);
	}

	/*
	 * Muestra la pantalla de acerca de...
	 */
	public void showAcercaDe() {
		MenuAdministrarDelegate delegate = (MenuAdministrarDelegate) getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuAdministrarDelegate();
			addDelegateToHashMap(
					MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID,
					delegate);
		}
		showViewController(AcercaDeViewController.class);
	}

	/*
	 * Muestra la pantalla de Novedades
	 */
	public void showNovedades() {
		MenuAdministrarDelegate delegate = (MenuAdministrarDelegate) getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuAdministrarDelegate();
			addDelegateToHashMap(
					MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID,
					delegate);
		}
		showViewController(NovedadesViewController.class);
	}
	
	public void showNovedadesLogin() {
		MenuSuiteDelegate delegate = (MenuSuiteDelegate) getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuSuiteDelegate();
			addDelegateToHashMap(
					MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID,
					delegate);
		}
		showViewController(NovedadesViewController.class);
	}
	
	public void showActivacionExitosa() {
		MenuAdministrarDelegate delegate = (MenuAdministrarDelegate) getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuAdministrarDelegate();
			addDelegateToHashMap(
					MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID,
					delegate);
		}
		showViewController(ActivacionExitosaSTViewController.class);
	}

	/**
	 * Muestra la pantalla de confirmacion autenticacion
	 */
	public void showConfirmacionAutenticacionViewController(
			DelegateBaseAutenticacion autenticacionDelegate, int resIcon,
			int resTitle, int resSubtitle) {
		showConfirmacionAutenticacionViewController(autenticacionDelegate,
				resIcon, resTitle, resSubtitle, R.color.primer_azul);
	}

	/**
	 * Muestra la pantalla de confirmacion autenticacion
	 */
	public void showConfirmacionAutenticacionViewController(
			DelegateBaseAutenticacion autenticacionDelegate, int resIcon,
			int resTitle, int resSubtitle, int resTitleColor) {
		ConfirmacionAutenticacionDelegate confirmacionDelegate = (ConfirmacionAutenticacionDelegate) getBaseDelegateForKey(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		if (confirmacionDelegate != null) {
			removeDelegateFromHashMap(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		}
		confirmacionDelegate = new ConfirmacionAutenticacionDelegate(
				autenticacionDelegate);
		addDelegateToHashMap(
				ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID,
				confirmacionDelegate);
		//AMZ
				if(estados.size() == 0)
				{
					//AMZ
					TrackingHelper.trackState("reactivacion", estados);
					//AMZ
				}
		showViewController(ConfirmacionAutenticacionViewController.class);
	}

	/**
	 * Muestra la pantalla de resultados
	 */
	public void showResultadosViewController(
			DelegateBaseOperacion delegateBaseOperacion, int resIcon,
			int resTitle) {
		ResultadosDelegate resultadosDelegate = (ResultadosDelegate) getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		if (resultadosDelegate != null) {
			removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		}
		resultadosDelegate = new ResultadosDelegate(delegateBaseOperacion);
		addDelegateToHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID,
				resultadosDelegate);
		showViewController(ResultadosViewController.class);
	}
	
	public void showResultadosViewControllerWithoutFreq(
			DelegateBaseOperacion delegateBaseOperacion, int resIcon,
			int resTitle) {
		ResultadosDelegate resultadosDelegate = (ResultadosDelegate) getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		if (resultadosDelegate != null) {
			removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		}
		resultadosDelegate = new ResultadosDelegate(delegateBaseOperacion);
		resultadosDelegate.setFrecOpOK(true);
		addDelegateToHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID,
				resultadosDelegate);
		showViewController(ResultadosViewController.class);
	}

	public void showOpcionesTransferir(String tipoOperacion, int resIcon,
			int resTitle, int resSubtitle) {
		String[] llaves = new String[4];
		Object[] valores = new Object[4];

		llaves[0] = Constants.PANTALLA_BASE_ICONO;
		valores[0] = Integer.valueOf(resIcon);
		llaves[1] = Constants.PANTALLA_BASE_TITULO;
		valores[1] = Integer.valueOf(resTitle);
		llaves[2] = Constants.PANTALLA_BASE_SUBTITULO;
		valores[2] = Integer.valueOf(resSubtitle);
		llaves[3] = Constants.PANTALLA_BASE_TIPO_OPERACION;
		valores[3] = String.valueOf(tipoOperacion);

		TransferirDelegate delegate = (TransferirDelegate) getBaseDelegateForKey(TransferirDelegate.TRANSFERIR_DELEGATE_ID);
		if (delegate == null) {
			delegate = new TransferirDelegate();
			addDelegateToHashMap(TransferirDelegate.TRANSFERIR_DELEGATE_ID,
					delegate);
		}

		showViewController(OpcionesTransferViewController.class, 0, false,
				llaves, valores);
	}

	/**
	 * Muestra la pantalla de transferencia mis cuentas
	 */
	public void showTransferirMisCuentas() {
		TransferirMisCuentasDelegate delegate = (TransferirMisCuentasDelegate) getBaseDelegateForKey(TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new TransferirMisCuentasDelegate();
			addDelegateToHashMap(
					TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID,
					delegate);
		}
		showViewController(TransferirMisCuentasSeleccionViewController.class);
	}

	/**
	 * Muestra la pantalla de transferencia entre mis cuentas detalle
	 */
	public void showTransferirMisCuentasDetalle() {
		TransferirMisCuentasDelegate delegate = (TransferirMisCuentasDelegate) getBaseDelegateForKey(TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new TransferirMisCuentasDelegate();
			addDelegateToHashMap(
					TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID,
					delegate);
		}
		showViewController(TransferMisCuentasDetalleViewController.class);
	}

	/**
	 * Muestra la pantalla de confirmacion para transferencias.
	 * 
	 * @param delegateBaseOperacion
	 *            EL DelegateBaseOperacion que manda a llamar la pantalla de
	 *            confirmaci�n.
	 */
	public void showConfirmacion(DelegateBaseOperacion delegateBaseOperacion) {
		// El delegado de confirmaci�n se crea siempre, esto ya que el delegado
		// que contiene el internamente cambia segun quien invoque este método.
		ConfirmacionDelegate delegate = (ConfirmacionDelegate) getBaseDelegateForKey(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
		if (delegate != null) {
			removeDelegateFromHashMap(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
		}
		delegate = new ConfirmacionDelegate(delegateBaseOperacion);
		addDelegateToHashMap(
				ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID,
				delegate);
		showViewController(ConfirmacionViewController.class);
	}

	/*
	 * Muestra la pantalla para alta de frecuente
	 */
	public void showAltaFrecuente(DelegateBaseOperacion delegateBaseOperacion) {
		AltaFrecuenteDelegate delegate = (AltaFrecuenteDelegate) getBaseDelegateForKey(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID);
		if (delegate != null) {
			removeDelegateFromHashMap(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID);
		}
		delegate = new AltaFrecuenteDelegate(delegateBaseOperacion);
		addDelegateToHashMap(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID,
				delegate);
		showViewController(AltaFrecuenteViewController.class);
	}

	/*
	 * Muestra la pantalla de transferViewController gen�rica para transferir
	 * comprar pagar consultar
	 */
	public void showTransferViewController(String tipoOperacion,
			boolean isExpress, boolean isTDC) {
		TransferirDelegate delegate = (TransferirDelegate) getBaseDelegateForKey(TransferirDelegate.TRANSFERIR_DELEGATE_ID);
		if (delegate == null) {
			delegate = new TransferirDelegate();
			addDelegateToHashMap(TransferirDelegate.TRANSFERIR_DELEGATE_ID,
					delegate);
		}
		delegate.setTipoOperacion(tipoOperacion);
		delegate.setExpress(isExpress);
		delegate.setTDC(isTDC);
		delegate.crearModeloDeOperacion();
		showViewController(TransferViewController.class);
	}

	/*
	 * Muestra la pantalla de transferViewController gen�rica para transferir
	 * comprar pagar consultar
	 */
	public void showSelectTDCViewController(String tipoOperacion,
			boolean isExpress, boolean isTDC) {
		PagoTdcDelegate delegate = (PagoTdcDelegate) getBaseDelegateForKey(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID);

		if (delegate == null) {
			delegate = new PagoTdcDelegate();
			addDelegateToHashMap(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID,
					delegate);
		}

//		delegate.setTipoOperacion(tipoOperacion);
//		delegate.setExpress(isExpress);
//		delegate.setTDC(isTDC);
//		delegate.crearModeloDeOperacion();
		showViewController(SeleccionaTdcViewController.class);
	}
	
	/*
	 * Muestra la pantalla de InterbancariosViewController
	 */
	public void showInterbancariosViewController(Object obj) {
		InterbancariosDelegate delegate = (InterbancariosDelegate) getBaseDelegateForKey(InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID);
		if (delegate == null) {
			if (obj instanceof TransferenciaInterbancaria) {
				delegate = new InterbancariosDelegate(true);
			} else
				delegate = new InterbancariosDelegate();
			addDelegateToHashMap(
					InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID, delegate);
		}
		delegate.setBajaFrecuente(false);
		if (obj instanceof Constants.Operacion) {
			delegate.setTipoOperacion((Constants.Operacion) obj);
			delegate.setEsFrecuente(false);
		}
		if (obj instanceof TransferenciaInterbancaria) {
			delegate.setEsFrecuente(true);
			delegate.setTransferenciaInterbancaria((TransferenciaInterbancaria) obj);
		}
		showViewController(InterbancariosViewController.class);
	}

	public void showOpcionesPagar() {
		String[] llaves = new String[4];
		Object[] valores = new Object[4];

		llaves[0] = Constants.PANTALLA_BASE_ICONO;
		valores[0] = Integer.valueOf(R.drawable.icono_pagar_servicios);
		llaves[1] = Constants.PANTALLA_BASE_TITULO;
		valores[1] = Integer.valueOf(R.string.pagar_title);
		llaves[2] = Constants.PANTALLA_BASE_SUBTITULO;
		valores[2] = Integer.valueOf(R.string.pagar_subtitle);

		PagarDelegate delegate = (PagarDelegate) getBaseDelegateForKey(PagarDelegate.PAGAR_DELEGATE_ID);
		if (delegate == null) {
			delegate = new PagarDelegate();
			addDelegateToHashMap(PagarDelegate.PAGAR_DELEGATE_ID, delegate);
		}

		showViewController(OpcionesPagosViewController.class, 0, false, llaves,
				valores);
	}

	public void showTipoConsultaFrecuentes() {
		TipoConsultaDelegate delegate = (TipoConsultaDelegate) getBaseDelegateForKey(TipoConsultaDelegate.TIPO_CONSULTA_DELEGATE_ID);
		if (delegate == null) {
			delegate = new TipoConsultaDelegate();
			addDelegateToHashMap(
					TipoConsultaDelegate.TIPO_CONSULTA_DELEGATE_ID, delegate);
		}
		showViewController(TipoConsultaViewController.class);
	}

	/*
	 * Muestra la pantalla de PagoServiciosViewController
	 */
	public void showPagosViewController(Constants.Operacion tipoOperacion) {
		PagoServiciosDelegate delegate = (PagoServiciosDelegate) getBaseDelegateForKey(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID);
		/*if (delegate == null) {
			delegate = new PagoServiciosDelegate();
			addDelegateToHashMap(
					PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID, delegate);
		}*/
		removeDelegateFromHashMap(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID);
		delegate = new PagoServiciosDelegate();
		addDelegateToHashMap(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID, delegate);
		delegate.setTipoOperacion(tipoOperacion);
		showViewController(PagoServiciosFrecuentesViewController.class);
	}

	/*
	 * 
	 */
	public void showConsultaFrecuentes(String tipoFrecuente) {
		String[] llaves = new String[2];
		Object[] valores = new Object[2];
		DelegateBaseOperacion delegate = null;
		llaves[0] = Constants.FREQUENT_REQUEST_SCREEN_DELEGATE_KEY;
		llaves[1] = Constants.FREQUENT_REQUEST_SCREEN_TITLE_KEY;
		Session session = Session.getInstance(SuiteApp.getInstance());
		if (tipoFrecuente.equals(Constants.tipoCFOtrosBancos)) {
			
			
			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirInterbancariaF,	session.getClientProfile())) {
			valores[0] = InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID;
			valores[1] = SuiteApp.getInstance().getString(
					R.string.externalTransfer_frequentTitle);
			removeDelegateFromHashMap(InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID);
			delegate = new InterbancariosDelegate(true);
			addDelegateToHashMap(
					InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID, delegate);
			}else{
				//NO ESTA PERMITIDO
				
			}

		} else if (tipoFrecuente.equals(Constants.tipoCFPagosCIE)) {
			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.pagoServiciosF,	session.getClientProfile())) {
			valores[0] = PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID;
			valores[1] = SuiteApp.getInstance().getString(
					R.string.servicesPayment_frequentTitle);
			removeDelegateFromHashMap(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID);
			delegate = new PagoServiciosDelegate();
			addDelegateToHashMap(
					PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID, delegate);
		}else{
			//NO ESTA PERMITIDO
			
		}

		} else if (tipoFrecuente.equals(Constants.tipoCFTiempoAire)) {
			
				if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.compraTiempoAireF,	session.getClientProfile())) {
				valores[0] = TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID;
				valores[1] = SuiteApp.getInstance().getString(
						R.string.tiempo_aire_title);
				removeDelegateFromHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
				delegate = new TiempoAireDelegate(true);
				addDelegateToHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID,
						delegate);
			
		}else{
			//NO ESTA PERMITIDO
			
		}

		} else if (tipoFrecuente.equals(Constants.tipoCFOtrosBBVA)) {
			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirBancomerF,	session.getClientProfile())) {
			valores[0] = OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID;
			valores[1] = SuiteApp.getInstance().getString(
					R.string.transferir_otrosBBVA_title);
			removeDelegateFromHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
			delegate = new OtrosBBVADelegate(false, false, true);
			addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID,
					delegate);
		}else{
			//NO ESTA PERMITIDO
			
		}

		} else if (tipoFrecuente.equals(Constants.tipoCFCExpress)) {
			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirBancomerF,	session.getClientProfile())) {
			valores[0] = OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID;
			valores[1] = SuiteApp.getInstance().getString(
					R.string.transferir_otrosBBVA_express_title);
			removeDelegateFromHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
			delegate = new OtrosBBVADelegate(true, false, true);
			addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID,
					delegate);
		}else{
			//NO ESTA PERMITIDO
			
		}

		} else if (tipoFrecuente.equals(Constants.tipoCFDineroMovil)) {
					
			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.dineroMovilF,	session.getClientProfile())) {
			valores[0] = DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID;
			valores[1] = SuiteApp.getInstance().getString(
					R.string.transferir_dineromovil_titulo);
			removeDelegateFromHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
			delegate = new DineroMovilDelegate();
			addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
					delegate);
		}else{
			//NO ESTA PERMITIDO
			TipoConsultaDelegate tipoConsultaDelegate = (TipoConsultaDelegate) getBaseDelegateForKey(TipoConsultaDelegate.TIPO_CONSULTA_DELEGATE_ID);
			tipoConsultaDelegate.comprobarRecortado();
			
		}
		}
		if (delegate != null)
			showViewController(ConsultarFrecuentesViewController.class, 0,
					false, llaves, valores);
	}

	/*
	 * Muestra la pantalla del menu consultar
	 */
	public void showMenuConsultar() {
		MenuConsultarDelegate delegate = (MenuConsultarDelegate) getBaseDelegateForKey(MenuConsultarDelegate.MENU_CONSULTAR_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuConsultarDelegate();
			addDelegateToHashMap(
					MenuConsultarDelegate.MENU_CONSULTAR_DELEGATE_ID, delegate);
		}
		showViewController(MenuConsultarViewController.class);

	}

	/*
	 * Muestra la pantalla de PagoServiciosViewController
	 */
	public void showPagoServiciosViewController(PagoServicio pagoServicio) {

		PagoServiciosDelegate delegate = (PagoServiciosDelegate) getBaseDelegateForKey(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new PagoServiciosDelegate();
			addDelegateToHashMap(
					PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID, delegate);
		}
		delegate.setBajaFrecuente(false);
		if (pagoServicio != null) {
			delegate.setPagoServicio(pagoServicio);
			delegate.setFrecuente(true);
			delegate.setTipoOperacion(Constants.Operacion.pagoServiciosF);
	//Preregistro pago de servicios elinimar 30 min otp registro
	//		delegate.setPreregister(false);
		} else {
			delegate.getPagoServicio().setConvenioServicio("");
			delegate.setFrecuente(false);
			//Preregistro pago de servicios elinimar 30 min otp registro
//			if (delegate.isPreregister()) {
//				delegate.setTipoOperacion(Constants.Operacion.pagoServiciosP);
//			} else {
				delegate.setTipoOperacion(Constants.Operacion.pagoServicios);
//			}
		}
		showViewController(PagoServiciosViewController.class);
	}

	public void showPagoServiciosAyudaViewController(String[] llaves,
			Object[] valores) {
		showViewController(PagoServiciosAyudaViewController.class, 0, false,
				llaves, valores);
	}

	public void showLecturaCodigoViewController(int codigoActividad) {
		showViewControllerForResult(CaptureActivity.class, codigoActividad);
	}

	/**
	 * Muestra la pantalla de tranferencia de dinero m�vil.
	 */
	public void showTransferirDineroMovil(TransferenciaDineroMovil frecuente,
			boolean bajaDM) {
		DineroMovilDelegate delegate = (DineroMovilDelegate) getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
		if (null == delegate) {
			delegate = new DineroMovilDelegate();
			addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
					delegate);
		}
		if (null != frecuente) {
			delegate.setTransferencia(frecuente);
			delegate.esFrecuente(true);
		}
		//Valida que al entrar a la pantalla de dinero movil
		//no precargue los datos de un frecuente después de operarñpo
		// en una operación nueva
		if(null == frecuente)
		{
			delegate.esFrecuente(false);
		}
		delegate.setEsBajaDM(bajaDM);
		delegate.setBajaFrecuente(false);
		showViewController(DineroMovilViewController.class);
	}

	/**
	 * 
	 * @param rapido
	 */
	public void showTransferirDineroMovil(Rapido rapido) {
		DineroMovilDelegate delegate = (DineroMovilDelegate) getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
		if (null == delegate) {
			delegate = new DineroMovilDelegate();
			addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
					delegate);
		}
		delegate.cargarRapido(rapido);
		delegate.esFrecuente(false);
		delegate.setEsBajaDM(false);
		delegate.setBajaFrecuente(false);
		showViewController(DineroMovilViewController.class);
	}

	/*
	 * Muestra la pantalla del Menu Comprar
	 */
	public void showComprarViewController() {
		MenuComprarDelegate delegate = (MenuComprarDelegate) getBaseDelegateForKey(MenuComprarDelegate.MENU_COMPRAR_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuComprarDelegate();
			addDelegateToHashMap(MenuComprarDelegate.MENU_COMPRAR_DELEGATE_ID,
					delegate);
		}
		showViewController(MenuComprarViewController.class);
	}

	public void showTiempoAireViewController(Rapido rapido) {
		TiempoAireDelegate delegate = (TiempoAireDelegate) getBaseDelegateForKey(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
		if (delegate != null) {
			removeDelegateFromHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
		}
		delegate = new TiempoAireDelegate(false);
		addDelegateToHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID,
				delegate);
		delegate.setTipoOperacion(Constants.Operacion.compraTiempoAireR);
		delegate.setFrecuente(false);
		delegate.cargarRapido(rapido);

		showViewController(TiempoAireDetalleViewController.class);
	}

	/*
	 * Muestra la pantalla de Tiempo Aire
	 */
	public void showTiempoAireViewController(Constants.Operacion tipoOpracion) {
		TiempoAireDelegate delegate = (TiempoAireDelegate) getBaseDelegateForKey(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
		if (delegate != null) {
			removeDelegateFromHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
		}
		delegate = new TiempoAireDelegate(false);
		addDelegateToHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID,
				delegate);
		delegate.setTipoOperacion(tipoOpracion);
		showViewController(TiempoAireViewController.class);
	}

	/*
	 * Muestra la segunda pantalla de Tiempo Aire
	 */
	public void showTiempoAireDetalleViewController() {
		TiempoAireDelegate delegate = (TiempoAireDelegate) getBaseDelegateForKey(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
		if (delegate == null) {
			delegate = new TiempoAireDelegate(false);
			addDelegateToHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID,
					delegate);
		}
		showViewController(TiempoAireDetalleViewController.class);
	}

	/*
	 * Muestra la pantalla de Transferencias terceros
	 */
	public void showOtrosBBVAViewController(Object obj, boolean esExpress,
			boolean esTDC) {
		OtrosBBVADelegate delegate = (OtrosBBVADelegate) getBaseDelegateForKey(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
		if (delegate == null) {
			if (obj instanceof TransferenciaOtrosBBVA) {
				delegate = new OtrosBBVADelegate(esExpress, esTDC, true);
			} else
				delegate = new OtrosBBVADelegate(esExpress, esTDC);
			addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID,
					delegate);
		}
		delegate.setEsExpress(esExpress);
		delegate.setEsTDC(esTDC);
		if (obj instanceof Constants.Operacion) {
			delegate.setTipoOperacion((Constants.Operacion) obj);
			delegate.setEsFrecuente(false);
		}
		if (obj instanceof TransferenciaOtrosBBVA) {
			delegate.setEsFrecuente(true);
			delegate.setTransferenciaOtrosBBVA((TransferenciaOtrosBBVA) obj);
		}
		showViewController(OtrosBBVAViewController.class);
	}

	public void showOtrosBBVAViewController(Rapido rapido) {
		OtrosBBVADelegate delegate = (OtrosBBVADelegate) getBaseDelegateForKey(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
		if (delegate == null) {
			delegate = new OtrosBBVADelegate(false, false);
			addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID,
					delegate);
		}

		delegate.cargarRapido(rapido);

		// if(obj instanceof Constants.Operacion){
		// delegate.setTipoOperacion((Constants.Operacion)obj);
		// delegate.setEsFrecuente(false);
		// }
		// if(obj instanceof TransferenciaOtrosBBVA){
		// delegate.setEsFrecuente(true);
		// delegate.setTransferenciaOtrosBBVA((TransferenciaOtrosBBVA) obj);
		// }
		showViewController(OtrosBBVAViewController.class);
	}
	
	/**
	 * Muestra la pantalla de detalles de un movimiento de Obtener Comprobante.
	 */
	public void showDetalleObtenerComprobante() {
		ObtenerComprobantesDelegate delegate = (ObtenerComprobantesDelegate) getBaseDelegateForKey(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ObtenerComprobantesDelegate();
			addDelegateToHashMap(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID,
					delegate);
		}
		showViewController(DetalleObtenerComprobanteViewController.class);
	}
	
	/**
	 * Muestra la pantalla de detalles de un movimiento de Depositos Recibidos.
	 */
	public void showDetalleDepositosRecibidos() {
		DepositosRecibidosDelegate delegate = (DepositosRecibidosDelegate) getBaseDelegateForKey(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID);
		if (null == delegate) {
			delegate = new DepositosRecibidosDelegate();
			addDelegateToHashMap(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID,
					delegate);
		}
		showViewController(DetalleDepositosRecibidosViewController.class);
	}

	/**
	 * Muestra la pantalla de detalles de un movimiento de dinero movil.
	 */
	public void showDetalleRetiroSinTarjeta() {
		ConsultaRetiroSinTarjetaDelegate delegate = (ConsultaRetiroSinTarjetaDelegate) getBaseDelegateForKey(ConsultaRetiroSinTarjetaDelegate.RETIRO_SIN_TARJETA_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ConsultaRetiroSinTarjetaDelegate();
			addDelegateToHashMap(ConsultaRetiroSinTarjetaDelegate.RETIRO_SIN_TARJETA_DELEGATE_ID,
					delegate);
		}
		showViewController(DetalleConsultaRetiroSinTarjetaViewController.class);
	}
	
	/**
	 * Muestra la pantalla de detalles de un movimiento de dinero movil.
	 */
	public void showDetalleDineroMovil() {
		DineroMovilDelegate delegate = (DineroMovilDelegate) getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
		if (null == delegate) {
			delegate = new DineroMovilDelegate();
			addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
					delegate);
		}
		showViewController(DetalleDineroMovilViewController.class);
	}

	@Override
	public void removeDelegateFromHashMap(long key) {
		// TODO Auto-generated method stub
		if (key == BorrarDatosDelegate.BORRAR_DATOS_DELEGATE_ID) {
			if (Server.ALLOW_LOG) Log.d("MES", "MES");
		}
		super.removeDelegateFromHashMap(key);
	}

	public void showListaBancos(int codigoActividad) {
		showViewControllerForResult(ListaBancosViewController.class,
				codigoActividad);
	}

	/**
	 * Muestra la pantalla para administrar envio de estado de cuenta
	 */
	public void showConsultarEstatusEnvioEstadodeCuenta() {
		ConsultarEstatusEnvioEstadodeCuentaDelegate delegate = (ConsultarEstatusEnvioEstadodeCuentaDelegate) getBaseDelegateForKey(ConsultarEstatusEnvioEstadodeCuentaDelegate.CONSULTARESTATUSENVIOESTADODECUENTA_DELEGATE_ID);
		if (delegate == null) {
			delegate = new ConsultarEstatusEnvioEstadodeCuentaDelegate();
			addDelegateToHashMap(
					ConsultarEstatusEnvioEstadodeCuentaDelegate.CONSULTARESTATUSENVIOESTADODECUENTA_DELEGATE_ID, delegate);
		}
		showViewController(ConsultarEstatusEnvioEstadodeCuentaViewController.class);
	}


	/**
	 * Muestra la pantalla del confirmar paperless
	 */

	public void showConfirmarPaperless() {
		CampaniaPaperlessDelegate delegate = (CampaniaPaperlessDelegate) getBaseDelegateForKey(CampaniaPaperlessDelegate.CAMPANIA_PAPERLESS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new CampaniaPaperlessDelegate();
			addDelegateToHashMap(
					CampaniaPaperlessDelegate.CAMPANIA_PAPERLESS_DELEGATE_ID,
					delegate);
		}
		showViewController(CampaniaPaperlessViewController.class);
	}

	/**
	 * Muestra la pantalla de ingresar datos de contratación.
	 */
	public void showContratacion(ConsultaEstatus consultaEstatus, String estatusServicio, boolean deleteData) {
		/*
		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionDelegate();
			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,	delegate);
		}
		delegate.setConsultaEstatus(consultaEstatus);
		delegate.setDeleteData(deleteData);

		if (estatusServicio.equalsIgnoreCase(Constants.STATUS_ENGAGEMENT_UNCOMPLETE)) {
			showContratacionAutenticacion();
		} else {			
			//AMZ
			estados.clear();
			//AMZ
			TrackingHelper.trackState("contratacion datos", estados);
			//AMZ
			showViewController(IngresarDatosViewController.class);			
		}*/

		//__________- invocacion al api de softoken ___________//
		SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/
		ServerCommons.ALLOW_LOG=Server.ALLOW_LOG;
		ServerCommons.DEVELOPMENT=Server.DEVELOPMENT;
		ServerCommons.SIMULATION=Server.SIMULATION;

		sftoken.onCreate(this.getCurrentViewController());
		sftoken.setIntentToReturn(new MenuSuiteViewController());
		sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());


		//__________- invocacion al api de contratacion ___________//
		suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus ce=new suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus();
		ce.setCompaniaCelular(consultaEstatus.getCompaniaCelular());
		ce.setEmailCliente(consultaEstatus.getEmailCliente());
		ce.setEstatus(consultaEstatus.getEstatus());
		ce.setEstatusAlertas(consultaEstatus.getEstatusAlertas());
		ce.setEstatusInstrumento(consultaEstatus.getEstatusInstrumento());
		ce.setInstrumento(consultaEstatus.getInstrumento());
		ce.setNombreCliente(consultaEstatus.getNombreCliente());
		ce.setNumCelular(consultaEstatus.getNumCelular());
		ce.setNumCliente(consultaEstatus.getNumCliente());
		ce.setNumTarjeta(consultaEstatus.getNumTarjeta());
		ce.setPerfilAST(consultaEstatus.getPerfilAST());
		ce.setPerfil(consultaEstatus.getPerfil());

		suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
				Server.SIMULATION,Server.ALLOW_LOG, Server.EMULATOR,Server.DEVELOPMENT
		);
		//inicializa la clase
		InitContratacion initi=new InitContratacion(this.getCurrentViewController(),new MenuSuiteViewController(),new MenuSuiteViewController(),bs);
		//raliza el start del api
		initi.startContratacion(ce,estatusServicio, deleteData);
	}

	/**
	 * Ejecuta flujo
	 * 
	 */
	public void showContratacionEP11(BaseViewController ownerController, ConsultaEstatus consultaEstatus,
			String estatusServicio, boolean deleteData) {

		/*ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionDelegate();
			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,
					delegate);
		}

		delegate.setEscenarioAlternativoEA11(false);
		delegate.setConsultaEstatus(consultaEstatus);
		delegate.setDeleteData(deleteData);
		IngresarDatosViewController ingresarDatosViewController = new IngresarDatosViewController(
				true);
		ingresarDatosViewController.setDelegate(delegate);
		ingresarDatosViewController.setContratacionDelegate(delegate);
		ingresarDatosViewController.setParentViewsController(SuiteApp
				.getInstance().getBmovilApplication()
				.getBmovilViewsController());
		
		SuiteApp.getInstance().getBmovilApplication()
				.getBmovilViewsController()
				.setCurrentActivityApp(ownerController);

		delegate.setOwnerController(ownerController);

		delegate.validaCamposST(consultaEstatus.getCompaniaCelular(),
				consultaEstatus.getNumTarjeta(), true);*/
	}


	/**
	 * Muestra la pantalla de ingresar datos de contratación.
	 */
	public void showContratacion() {
		/*ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionDelegate();
			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,
					delegate);
		}
		showViewController(IngresarDatosViewController.class);
		*/



		//__________- invocacion al api de softoken ___________//
		SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/
		ServerCommons.ALLOW_LOG=Server.ALLOW_LOG;
		ServerCommons.DEVELOPMENT=Server.DEVELOPMENT;
		ServerCommons.SIMULATION=Server.SIMULATION;

		sftoken.onCreate(this.getCurrentViewController());
		sftoken.setIntentToReturn(new MenuSuiteViewController());
		sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());


		//__________- invocacion al api de contratacion ___________//
		suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
				Server.SIMULATION,Server.ALLOW_LOG, Server.EMULATOR,Server.DEVELOPMENT
		);
		//inicializa la clase
		InitContratacion initi=new InitContratacion(this.getCurrentViewController(),new MenuSuiteViewController(),new MenuSuiteViewController(),bs);
		//raliza el start del api
		initi.startContratacion(null, null, false);
	}

	/**
	 * Muestra la pantalla de reactivación.
	 */
	public void showReactivacion(ConsultaEstatus consultaEstatus) {
		//ReactivacionDelegate delegate = (ReactivacionDelegate) getBaseDelegateForKey(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
		//if (null != delegate)
		//	removeDelegateFromHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
		//delegate = new ReactivacionDelegate(consultaEstatus);

		//addDelegateToHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID,
		//		delegate);
		//showConfirmacionAutenticacionViewController(delegate, 0, 0, 0);
		suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus consultaEstatusReac = new suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus();
		consultaEstatusReac.setPerfil(consultaEstatus.getPerfil());
		consultaEstatusReac.setPerfilAST(consultaEstatus.getPerfilAST());
		consultaEstatusReac.setNumTarjeta(consultaEstatus.getNumTarjeta());
		consultaEstatusReac.setNumCliente(consultaEstatus.getNumCliente());
		consultaEstatusReac.setNumCelular(consultaEstatus.getNumCelular());
		consultaEstatusReac.setNombreCliente(consultaEstatus.getNombreCliente());
		consultaEstatusReac.setCompaniaCelular(consultaEstatus.getCompaniaCelular());
		consultaEstatusReac.setEmailCliente(consultaEstatus.getEmailCliente());
		consultaEstatusReac.setEstatus(consultaEstatus.getEstatus());
		consultaEstatusReac.setEstatusAlertas(consultaEstatus.getEstatusAlertas());
		consultaEstatusReac.setEstatusInstrumento(consultaEstatus.getEstatusInstrumento());
		consultaEstatusReac.setInstrumento(consultaEstatus.getInstrumento());
		BanderasServer bandera = new BanderasServer(Server.SIMULATION,
				Server.ALLOW_LOG, Server.EMULATOR, Server.DEVELOPMENT);
		InitReactivacion initReactivacion = new InitReactivacion(bandera, SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp(),
				(MenuSuiteViewController)SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp(),
				new MenuSuiteViewController());
		String instrumento = Session.getInstance(SuiteApp.appContext).getSecurityInstrument();
		suitebancomercoms.aplicaciones.bmovil.classes.common.Session sessionApis = suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(SuiteApp.appContext);
		sessionApis.setSecurityInstrument(instrumento);
		initReactivacion.showReactivacion(consultaEstatusReac,
				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().estados);
	}

	/**
	 * Muestra la pantalla de reactivación.
	 */


	public void showReactivacion(ConsultaEstatus consultaEstatus,
			String password) {
		/*ReactivacionDelegate delegate = (ReactivacionDelegate) getBaseDelegateForKey(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
		if (null != delegate)
			removeDelegateFromHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
		delegate = new ReactivacionDelegate(consultaEstatus);
		addDelegateToHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID,
				delegate);

		delegate.setPassword(password);
		delegate.setOwnerController(SuiteApp.getInstance()
				.getSuiteViewsController().getCurrentViewControllerApp());

		Session session = Session.getInstance(SuiteApp.appContext);
		//KeyStoreManager keyStore = session.getKeyStoreManager();
		
		try {
			KeyStoreManager keyStore = session.getKeyStoreManager();
			keyStore.setEntry(Constants.USERNAME, " ");
			keyStore.setEntry(Constants.SEED, " ");
			keyStore.setEntry(Constants.CENTRO, " ");
		} catch (KeyManagerStoreException e) {
			if(Server.ALLOW_LOG) Log.e("Error KeyStore", "BmovilViewsController: Error al borrar datos en KeyStore");
		}
		delegate.borrarDatosDeSession();*/

		suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus consultaEstatusReac = new suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus();
		consultaEstatusReac.setPerfil(consultaEstatus.getPerfil());
		consultaEstatusReac.setPerfilAST(consultaEstatus.getPerfilAST());
		consultaEstatusReac.setNumTarjeta(consultaEstatus.getNumTarjeta());
		consultaEstatusReac.setNumCliente(consultaEstatus.getNumCliente());
		consultaEstatusReac.setNumCelular(consultaEstatus.getNumCelular());
		consultaEstatusReac.setNombreCliente(consultaEstatus.getNombreCliente());
		consultaEstatusReac.setCompaniaCelular(consultaEstatus.getCompaniaCelular());
		consultaEstatusReac.setEmailCliente(consultaEstatus.getEmailCliente());
		consultaEstatusReac.setEstatus(consultaEstatus.getEstatus());
		consultaEstatusReac.setEstatusAlertas(consultaEstatus.getEstatusAlertas());
		consultaEstatusReac.setEstatusInstrumento(consultaEstatus.getEstatusInstrumento());
		consultaEstatusReac.setInstrumento(consultaEstatus.getInstrumento());
		BanderasServer bandera = new BanderasServer(Server.SIMULATION,
				Server.ALLOW_LOG, Server.EMULATOR, Server.DEVELOPMENT);
		InitReactivacion initReactivacion = new InitReactivacion(bandera, SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp(),
				(MenuSuiteViewController)SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp(),
				new MenuSuiteViewController());
		String instrumento = Session.getInstance(SuiteApp.appContext).getSecurityInstrument();
		suitebancomercoms.aplicaciones.bmovil.classes.common.Session sessionApis = suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(SuiteApp.appContext);
		sessionApis.setSecurityInstrument(instrumento);
		initReactivacion.showReactivacion(consultaEstatusReac, password,
				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().estados);

	}

	/**
	 * Muestra la pantalla de definir contraseña la contratación.
	 */
	public void showDefinirPassword() {		

		//__________- invocacion al api de softoken ___________//
		SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/
		ServerCommons.ALLOW_LOG=Server.ALLOW_LOG;
		ServerCommons.DEVELOPMENT=Server.DEVELOPMENT;
		ServerCommons.SIMULATION=Server.SIMULATION;

		sftoken.onCreate(this.getCurrentViewController());
		sftoken.setIntentToReturn(new MenuSuiteViewController());
		sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());


		//__________- invocacion al api de contratacion ___________//
		suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
				Server.SIMULATION,Server.ALLOW_LOG, Server.EMULATOR,Server.DEVELOPMENT
		);
		//inicializa la clase
		InitContratacion initi=new InitContratacion(this.getCurrentViewController(),new MenuSuiteViewController(),new MenuSuiteViewController(),bs);
		//raliza el start del api
		initi.startContratacionDefinicionPassword();
	}

	/**
	 * Muestra la pantalla de autenticacion softoken.
	 * @param currentPassword
	 * @param consultaEstatus
	 */
	public void showAutenticacionSoftoken(ConsultaEstatus consultaEstatus, String currentPassword) {


		//showViewController(IngresoDatosSTViewController.class);

		String[] extrasKeys= {"login"};
		Object[] extras= {true};

		//showViewController(IngresoDatosSTViewController.class, 0, false, extrasKeys, extras);


	//showViewController(IngresoDatosSTViewController.class, 0, false, extrasKeys, extras);

		//__________- invocacion al api de contratacion ___________//
		suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
				Server.SIMULATION,Server.ALLOW_LOG, Server.EMULATOR,Server.DEVELOPMENT
		);
		//inicializa la clase
		InitContratacion initi=new InitContratacion(this.getCurrentViewController(),new MenuSuiteViewController(),new MenuSuiteViewController(),bs);


		//__________- invocacion al api de softoken ___________//
		SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/
		ServerCommons.ALLOW_LOG=Server.ALLOW_LOG;
		ServerCommons.DEVELOPMENT=Server.DEVELOPMENT;
		ServerCommons.SIMULATION=Server.SIMULATION;

		sftoken.onCreate(this.getCurrentViewController());
		sftoken.setIntentToReturn(new MenuSuiteViewController());
		sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());
		sftoken.showViewController(IngresoDatosSTViewController.class, 0, false, extrasKeys, extras, this.getCurrentViewController());

	}

	/**
	 * Muestra la pantalla de ayuda para agregar una operación rápida.
	 */
	public void showAyudaAgregarRapido() {
		showViewController(AyudaAgregarRapidoViewController.class);
	}

	/**
	 * Muestra la pantalla de resultados
	 */
	public void showResultadosAutenticacionViewController(
			DelegateBaseOperacion delegateBaseOperacion, int resIcon,
			int resTitle) {
		ResultadosAutenticacionDelegate delegate = (ResultadosAutenticacionDelegate) getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		if (delegate != null) {
			removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		}
		delegate = new ResultadosAutenticacionDelegate(delegateBaseOperacion);
		addDelegateToHashMap(
				ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID,
				delegate);
		showViewController(ResultadosAutenticacionViewController.class);
	}

	/**
	 * Muestra la pantalla para desbloqueo
	 * 
	 * @param ConsultaEstatus
	 */
	public void showDesbloqueo(ConsultaEstatus ce) {
//		NuevaContraseniaDelegate delegate = (NuevaContraseniaDelegate) getBaseDelegateForKey(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID);
//		if (delegate != null) {
//			removeDelegateFromHashMap(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID);
//		}
//		delegate = new NuevaContraseniaDelegate(ce);
//		addDelegateToHashMap(
//				NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID,
//				delegate);
		//showViewController(NuevaContraseniaViewController.class);

		mtto.suitebancomer.aplicaciones.bmovil.classes.model.BanderasServer banderas=
				new mtto.suitebancomer.aplicaciones.bmovil.classes.model.BanderasServer(Server.SIMULATION, Server.ALLOW_LOG, Server.EMULATOR, Server.DEVELOPMENT);
		InitMantenimiento initMantenimiento=new InitMantenimiento(this.getCurrentViewController(), new MenuSuiteViewController(),  new MenuSuiteViewController(), banderas);

		suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus consutaEstats=new suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus();
		consutaEstats.setPerfil(ce.getPerfil());
		consutaEstats.setPerfilAST(ce.getPerfilAST());
		consutaEstats.setNumTarjeta(ce.getNumTarjeta());
		consutaEstats.setNumCliente(ce.getNumCliente());
		consutaEstats.setCompaniaCelular(ce.getCompaniaCelular());
		consutaEstats.setNumCelular(ce.getNumCelular());
		consutaEstats.setEmailCliente(ce.getEmailCliente());
		consutaEstats.setEstatus(ce.getEstatus());
		consutaEstats.setEstatusAlertas(ce.getEstatusAlertas());
		consutaEstats.setEstatusInstrumento(ce.getEstatusInstrumento());
		consutaEstats.setInstrumento(ce.getInstrumento());
		consutaEstats.setNombreCliente(ce.getNombreCliente());
		initMantenimiento.startDesbloqueo(consutaEstats);
	}

	/**
	 * Muestra la confirmacion para quitar la suspension
	 * 
	 * @param ConsultaEstatus
	 */
	public void showQuitarSuspension(ConsultaEstatus ce) {
	/*	QuitarSuspencionDelegate delegate = (QuitarSuspencionDelegate) getBaseDelegateForKey(QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID);
		if (delegate != null) {
			removeDelegateFromHashMap(QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID);
		}
		delegate = new QuitarSuspencionDelegate(ce);
		addDelegateToHashMap(
				QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID,
				delegate);
		*/
		//showConfirmacionAutenticacionViewController(delegate, 0, 0, 0);

		mtto.suitebancomer.aplicaciones.bmovil.classes.model.BanderasServer banderas=
				new mtto.suitebancomer.aplicaciones.bmovil.classes.model.BanderasServer(Server.SIMULATION, Server.ALLOW_LOG, Server.EMULATOR, Server.DEVELOPMENT);
		InitMantenimiento initMantenimiento=new InitMantenimiento(this.getCurrentViewController(), new MenuSuiteViewController(),  new MenuSuiteViewController(), banderas);

		suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus consutaEstats=new suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus();
		consutaEstats.setPerfil(ce.getPerfil());
		consutaEstats.setPerfilAST(ce.getPerfilAST());
		consutaEstats.setNumTarjeta(ce.getNumTarjeta());
		consutaEstats.setNumCliente(ce.getNumCliente());
		consutaEstats.setCompaniaCelular(ce.getCompaniaCelular());
		consutaEstats.setNumCelular(ce.getNumCelular());
		consutaEstats.setEmailCliente(ce.getEmailCliente());
		consutaEstats.setEstatus(ce.getEstatus());
		consutaEstats.setEstatusAlertas(ce.getEstatusAlertas());
		consutaEstats.setEstatusInstrumento(ce.getEstatusInstrumento());
		consutaEstats.setInstrumento(ce.getInstrumento());
		consutaEstats.setNombreCliente(ce.getNombreCliente());

		initMantenimiento.startQuitarSuspencion(consutaEstats);

	}

	/**
	 * Muestra la pantalla de configurar alertas.
	 */
	public void showConfigurarAlertas() {
		ConfigurarAlertasDelegate delegate = (ConfigurarAlertasDelegate) getBaseDelegateForKey(ConfigurarAlertasDelegate.CONFIGURAR_ALERTAS_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ConfigurarAlertasDelegate();
			addDelegateToHashMap(
					ConfigurarAlertasDelegate.CONFIGURAR_ALERTAS_DELEGATE_ID,
					delegate);
		}

		showViewController(ConfigurarAlertasViewController.class);
	}

	/**
	 * Muestra la pantalla de ayuda para la contratacion.
	 */
	public void showAyudaContratacion() {
		showViewController(AyudaContratacionViewController.class);
	}

	/**
	 * Muestra la pantalla de informaci�n contratacion con y sin Token.
	 */
	public void showAyudaContratacionTokens() {
		//showViewController(AyudaContratacionTokensViewController.class);
	}

	/**
	 * Muestra la pantalla de confirmaci�n autenticación para el estatus PS.
	 */
	/*public void showContratacionAutenticacion() {
		ContratacionDelegate contrataciondelegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
		ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);

		if (delegate != null){
			removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		}
		
		delegate = new ContratacionAutenticacionDelegate(contrataciondelegate);
		addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID, delegate);
		showViewController(ContratacionAutenticacionViewController.class);
	}*/

	/**
	 * Muestra la pantalla de confirmacion autenticacion
	 */
	public void showContratacionAutenticacion(DelegateBaseAutenticacion autenticacionDelegate) {

		//__________- invocacion al api de softoken ___________//
		SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/
		ServerCommons.ALLOW_LOG=Server.ALLOW_LOG;
		ServerCommons.DEVELOPMENT=Server.DEVELOPMENT;
		ServerCommons.SIMULATION=Server.SIMULATION;

		sftoken.onCreate(this.getCurrentViewController());
		sftoken.setIntentToReturn(new MenuSuiteViewController());
		sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());


		//__________- invocacion al api de contratacion ___________//
		suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
				Server.SIMULATION,Server.ALLOW_LOG, Server.EMULATOR,Server.DEVELOPMENT
		);
		//inicializa la clase
		InitContratacion initi=new InitContratacion(this.getCurrentViewController(),new MenuSuiteViewController(),new MenuSuiteViewController(),bs);
		//raliza el start del api
		/*
		TODO se crea un objeto nuevo de DelegateBaseAutenticacion para realizar pruebas de cuales elementos utiliza
		 */
		initi.startContratacionViewController(new suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.DelegateBaseAutenticacion());
	}

	/**
	 * Muestra los terminos y condiciones de uso.
	 * 
	 * @param terminosDeUso
	 *            Terminos de uso.
	 */
	public void showTerminosDeUso(String terminosDeUso) {
		/*showViewController(TerminosCondicionesViewController.class, 0, false,
				new String[] { Constants.TERMINOS_DE_USO_EXTRA },
				new Object[] { terminosDeUso });*/
	}

	public void showActivacion(final ConsultaEstatus ce, final String password,
			final boolean appActivada) {
		if (appActivada) {
			SuiteApp.getInstance()
					.getSuiteViewsController()
					.getCurrentViewControllerApp()
					.showInformationAlert(
							R.string.bmovil_activacion_alerta_reactivar,
							new OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									/*ActivacionDelegate aDelegate = (ActivacionDelegate) getBaseDelegateForKey(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
									if (aDelegate != null)
										removeDelegateFromHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
									aDelegate = new ActivacionDelegate(ce,
											password);
									aDelegate.borrarDatos();
									addDelegateToHashMap(
											ActivacionDelegate.ACTIVACION_DELEGATE_ID,
											aDelegate);*/
									dialog.dismiss();
									SuiteApp.getInstance()
											.getSuiteViewsController()
											.getCurrentViewControllerApp().runOnUiThread(new Runnable() {
										@Override
										public void run() {
											String title = "";
											String msg = SuiteApp.appContext.getString(R.string.alert_StoreSession);
											SuiteApp.getInstance()
													.getSuiteViewsController()
													.getCurrentViewControllerApp().muestraIndicadorActividad(title, msg);
											new Thread(new Runnable() {
												public void run() {
													Looper.prepare();
													InitActivacion initActivacion = new InitActivacion(new BanderasServer(Server.SIMULATION, Server.ALLOW_LOG, Server.EMULATOR,
															Server.DEVELOPMENT), currentViewControllerApp,
															(MenuSuiteViewController) SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp(),
															new MenuSuiteViewController());
													suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus consultaEstatus = new suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus();
													consultaEstatus.setCompaniaCelular(ce.getCompaniaCelular());
													consultaEstatus.setEmailCliente(ce.getEmailCliente());
													consultaEstatus.setEstatus(ce.getEstatus());
													consultaEstatus.setEstatusAlertas(ce.getEstatusAlertas());
													consultaEstatus.setEstatusInstrumento(ce.getEstatusInstrumento());
													consultaEstatus.setInstrumento(ce.getInstrumento());
													consultaEstatus.setNombreCliente(ce.getNombreCliente());
													consultaEstatus.setNumCelular(ce.getNumCelular());
													consultaEstatus.setNumCliente(ce.getNumCliente());
													consultaEstatus.setNumTarjeta(ce.getNumTarjeta());
													consultaEstatus.setPerfilAST(ce.getPerfilAST());
													consultaEstatus.setPerfil(ce.getPerfil());
													initActivacion.borraDatos(consultaEstatus, password, estados);
													SuiteApp.getInstance()
															.getSuiteViewsController()
															.getCurrentViewControllerApp().ocultaIndicadorActividad();
													Looper.loop();
												}
											}).start();
										}
									});
								}
							});
		}
	}

	public void showActivacion(ConsultaEstatus ce, String password) {
		/*ActivacionDelegate aDelegate = (ActivacionDelegate) getBaseDelegateForKey(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
		if (aDelegate != null)
			removeDelegateFromHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
		aDelegate = new ActivacionDelegate(ce, password);

		addDelegateToHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID,
				aDelegate);
		//AMZ
		TrackingHelper.trackState("activacion", estados);
		//AMZ
		showViewController(ActivacionViewController.class);*/
		InitActivacion initActivacion = new InitActivacion(new BanderasServer(Server.SIMULATION,
				Server.ALLOW_LOG, Server.EMULATOR, Server.DEVELOPMENT), currentViewControllerApp,
				(MenuSuiteViewController)SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp(),
				new MenuSuiteViewController());
		suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus consultaEstatus = new suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus();
		consultaEstatus.setCompaniaCelular(ce.getCompaniaCelular());
		consultaEstatus.setEmailCliente(ce.getEmailCliente());
		consultaEstatus.setEstatus(ce.getEstatus());
		consultaEstatus.setEstatusAlertas(ce.getEstatusAlertas());
		consultaEstatus.setEstatusInstrumento(ce.getEstatusInstrumento());
		consultaEstatus.setInstrumento(ce.getInstrumento());
		consultaEstatus.setNombreCliente(ce.getNombreCliente());
		consultaEstatus.setNumCelular(ce.getNumCelular());
		consultaEstatus.setNumCliente(ce.getNumCliente());
		consultaEstatus.setNumTarjeta(ce.getNumTarjeta());
		consultaEstatus.setPerfilAST(ce.getPerfilAST());
		consultaEstatus.setPerfil(ce.getPerfil());
		initActivacion.showActivacion(consultaEstatus, password, estados);
	}


	/**
	 * Muestra la pantalla para consultar rápidos.
	 */
	public void showConsultarRapidas() {
		ConsultaRapidosDelegate delegate = (ConsultaRapidosDelegate) getBaseDelegateForKey(ConsultaRapidosDelegate.CONSULTA_RAPIDOS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new ConsultaRapidosDelegate();
			addDelegateToHashMap(
					ConsultaRapidosDelegate.CONSULTA_RAPIDOS_DELEGATE_ID,
					delegate);
		}
		showViewController(ConsultaRapidosViewController.class);
	}

	//SPEI

		/**
		 * Shows the account association screen.
		 */
		public void showAsociacionCuentaTelefono() {
			MantenimientoSpeiMovilDelegate delegate = (MantenimientoSpeiMovilDelegate)getBaseDelegateForKey(MantenimientoSpeiMovilDelegate.DELEGATE_ID);
			if(null == delegate) {
				delegate = new MantenimientoSpeiMovilDelegate();
				addDelegateToHashMap(MantenimientoSpeiMovilDelegate.DELEGATE_ID, delegate);
			}
			showViewController(ConsultaCuentaSpeiMovilViewController.class);
		}
		
		/**
		 * Shows the screen to associate an account with a phone.
		 */
		public void showDetailAsociacionCuentaTelefono() {
			MantenimientoSpeiMovilDelegate delegate = (MantenimientoSpeiMovilDelegate)getBaseDelegateForKey(MantenimientoSpeiMovilDelegate.DELEGATE_ID);
			if(null == delegate) {
				delegate = new MantenimientoSpeiMovilDelegate();
				addDelegateToHashMap(MantenimientoSpeiMovilDelegate.DELEGATE_ID, delegate);
			}
			showViewController(DetalleCuentaSpeiMovilViewController.class);
		}

	
	//Termina SPEI
	
	
	

	/**
	 * Muestra la pantalla de enviar correo
	 */
	public void showEnviarCorreo(ResultadosDelegate resultadosDelegate) {
		EnviarCorreoDelegate delegate = (EnviarCorreoDelegate) getBaseDelegateForKey(EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID);
		if (null == delegate) {
			delegate = new EnviarCorreoDelegate();
			addDelegateToHashMap(
					EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID, delegate);
		}
		delegate.setResultadosDelegate(resultadosDelegate);
		//delegate.setOperacionDelegate(resultadosDelegate.getOperationDelegate());
		showViewController(EnviarCorreoViewController.class);
	}
	
	/**
	 * Muestra la pantalla de enviar correo
	 */
	public void showEnviarCorreo(DepositosRecibidosDelegate depositosDelegate) {
		EnviarCorreoDelegate delegate = (EnviarCorreoDelegate) getBaseDelegateForKey(EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID);
		if (null == delegate) {
			delegate = new EnviarCorreoDelegate();
			addDelegateToHashMap(
					EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID, delegate);
		}
		//delegate.setOperacionDelegate(depositosDelegate);
		showViewController(EnviarCorreoViewController.class);
	}
	
	/**
	 * Muestra la pantalla de enviar correo
	 */
	public void showEnviarCorreo(ObtenerComprobantesDelegate obtenerComprobantesDelegate) {
		EnviarCorreoDelegate delegate = (EnviarCorreoDelegate) getBaseDelegateForKey(EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID);
		if (null == delegate) {
			delegate = new EnviarCorreoDelegate();
			addDelegateToHashMap(
					EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID, delegate);
		}
		//delegate.setOperacionDelegate(obtenerComprobantesDelegate);
		showViewController(EnviarCorreoViewController.class);
	}

	/**
	 * Muestra la pantalla de confirmacion para transferencias.
	 * 
	 * @param delegateBaseOperacion
	 *            EL DelegateBaseOperacion que manda a llamar la pantalla de
	 *            confirmaci�n.
	 */
	public void showConfirmacionRegistro(
			DelegateBaseOperacion delegateBaseOperacion) {
		// El delegado de confirmaci�n se crea siempre, esto ya que el delegado
		// que contiene el internamente cambia segun quien invoque este método.
		ConfirmacionRegistroDelegate delegate = (ConfirmacionRegistroDelegate) getBaseDelegateForKey(ConfirmacionRegistroDelegate.CONFIRMACION_REGISTRO_DELEGATE_DELEGATE_ID);
		if (delegate != null) {
			removeDelegateFromHashMap(ConfirmacionRegistroDelegate.CONFIRMACION_REGISTRO_DELEGATE_DELEGATE_ID);
		}
		delegate = new ConfirmacionRegistroDelegate(delegateBaseOperacion);
		addDelegateToHashMap(
				ConfirmacionRegistroDelegate.CONFIRMACION_REGISTRO_DELEGATE_DELEGATE_ID,
				delegate);
		showViewController(ConfirmacionRegistroViewController.class);
	}

	/**
	 * 
	 * @param delegateOp
	 */
	public void showRegistrarOperacion(DelegateBaseAutenticacion delegateOp) {
		//SPEI
		showRegistrarOperacion(delegateOp, false);
	}
	public void showRegistrarOperacion(DelegateBaseAutenticacion delegateOp, boolean showHelpImage){
		//Termina SPEI
		
		RegistrarOperacionDelegate delegate = (RegistrarOperacionDelegate) getBaseDelegateForKey(RegistrarOperacionDelegate.REGISTRAR_OPERACION_DELEGATE_ID);
		if (delegate != null)
			removeDelegateFromHashMap(RegistrarOperacionDelegate.REGISTRAR_OPERACION_DELEGATE_ID);
		delegate = new RegistrarOperacionDelegate(delegateOp);
		addDelegateToHashMap(
				RegistrarOperacionDelegate.REGISTRAR_OPERACION_DELEGATE_ID,
				delegate);
		//SPEI
		
		String[] extrasKeys = { "showHelpImage" };
		Object[] extrasValues = { Boolean.valueOf(showHelpImage) };
		showViewController(RegistrarOperacionViewController.class, 0, false, extrasKeys, extrasValues);
	}
	
	/**
	 * Shows the confirmation screen for the SPEI maintenance operations. 
	 * @param operationDelegate The delegate of the current operation. 
	 */
	public void showSpeiConfirmation(DelegateBaseAutenticacion operationDelegate, boolean showHelpImage) {
		ConfirmacionAsignacionSpeiDelegate confirmDelegate;
		confirmDelegate = (ConfirmacionAsignacionSpeiDelegate)getBaseDelegateForKey(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID);
		if(confirmDelegate != null)
			removeDelegateFromHashMap(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID);
		confirmDelegate = new ConfirmacionAsignacionSpeiDelegate(operationDelegate);
		
		addDelegateToHashMap(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID, confirmDelegate);
		
		String[] keys = new String[] { "showHelpImage" };
		Object[] values = new Object[] { Boolean.valueOf(showHelpImage) };
		
		showViewController(ConfirmacionAsignacionSpeiViewController.class, 0, false, keys, values);
	}
	
	
	
	/**
	 * Shows the terms and conditions screen.
	 * @param terminosDeUso Terms and conditions HTML.
	 * @param title The title resource identifier.
	 * @param icon The icon recource identifier.
	 */
	public void showTermsAndConditions(String termsHtml, int title, int icon) {
		/*String[] keys = {Constants.TERMINOS_DE_USO_EXTRA, Constants.TITLE_EXTRA, Constants.ICON_EXTRA};
		Object[] values = {termsHtml, Integer.valueOf(title), Integer.valueOf(icon)};
		
		showViewController(TerminosCondicionesViewController.class, 0, false, keys, values);*/
	}

	public void showOtrosBBVASpeiViewController(Object obj, boolean esExpress, boolean esTDC) {
		OtrosBBVADelegate delegate = (OtrosBBVADelegate) getBaseDelegateForKey(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
		
		if (delegate == null) {
			delegate = new OtrosBBVADelegate(esExpress, esTDC);
			addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID, delegate);
		}
		
		delegate.setEsExpress(esExpress);
		delegate.setEsTDC(esTDC);
		delegate.setSpei(true);
		delegate.setTipoOperacion((Constants.Operacion) obj);
		delegate.setEsFrecuente(false);
		//Termina SPEI
		showViewController(RegistrarOperacionViewController.class);
	}
	public void touchMenu(){
		String eliminado="";
		if (estados.size() == 2){
			eliminado = estados.remove(1);
		}else{
			int tam = estados.size();
		for (int i=1;i<tam;i++){
			eliminado = estados.remove(1);
		}
		}
	}
	
	public void touchAtras()
	{

		int ultimo = estados.size()-1;
		
		String eliminado;
		if(ultimo >= 0)
		{
			String ult = estados.get(ultimo);
			if(ult == "reactivacion" || ult == "activacion" || ult == "menu token" || ult == "contratacion datos" || ult == "activacion datos")
			{
				estados.clear();
			}else
			{
				 eliminado = estados.remove(ultimo);
			}
		} 
	}

	//One click
	
	/*
	 * Muestra la pantalla de detalle oferta ILC
	 */
	public void showDetalleILC(OfertaILC ofertaILC, Promociones promocion ) {
		// TODO Auto-generated method stub
		DetalleOfertaILCDelegate delegate = (DetalleOfertaILCDelegate) getBaseDelegateForKey(DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE);
		if (null != delegate) 
			removeDelegateFromHashMap(DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE);
		delegate = new DetalleOfertaILCDelegate(ofertaILC, promocion);
		addDelegateToHashMap(
				DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE, delegate);
		
		showViewController(DetalleILCViewController.class);
	}
	
	/*
	 * Muestra la pantalla de exito oferta ILC
	 */
	public void showExitoILC(AceptaOfertaILC aceptaOfertaILC, OfertaILC ofertaILC ) {
		// TODO Auto-generated method stub
		ExitoILCDelegate delegate = (ExitoILCDelegate) getBaseDelegateForKey(ExitoILCDelegate.EXITO_OFERTA_DELEGATE);
		if (null != delegate) 
				removeDelegateFromHashMap(ExitoILCDelegate.EXITO_OFERTA_DELEGATE);
			delegate = new ExitoILCDelegate(aceptaOfertaILC,ofertaILC );
			addDelegateToHashMap(
					ExitoILCDelegate.EXITO_OFERTA_DELEGATE, delegate);
		
		showViewController(ExitoOfertaILCViewController.class);
	}
	
	/*
	 * Muestra la pantalla de detalle oferta ILC
	 */
	public void showDetalleEFI(OfertaEFI ofertaEFI, Promociones promocion ) {
		// TODO Auto-generated method stub
		DetalleOfertaEFIDelegate delegate = (DetalleOfertaEFIDelegate) getBaseDelegateForKey(DetalleOfertaEFIDelegate.DETALLE_OFERTA_EFI_DELEGATE);
		if (null == delegate) {
			delegate = new DetalleOfertaEFIDelegate(ofertaEFI, promocion);
			addDelegateToHashMap(
					DetalleOfertaEFIDelegate.DETALLE_OFERTA_EFI_DELEGATE, delegate);
		}else{
			delegate.setOfertaEFI(ofertaEFI);
		}
		showViewController(DetalleEFIViewController.class);
	}
	
	/*
	 * Muestra la pantalla de exito oferta ILC
	 */
	public void showExitoEFI(AceptaOfertaEFI aceptaOfertaEFI, OfertaEFI ofertaEFI) {
		// TODO Auto-generated method stub
		ExitoEFIDelegate delegate = (ExitoEFIDelegate) getBaseDelegateForKey(ExitoEFIDelegate.EXITO_OFERTA_EFI_DELEGATE);
		if (null != delegate) 
				removeDelegateFromHashMap(ExitoEFIDelegate.EXITO_OFERTA_EFI_DELEGATE);			
		delegate = new ExitoEFIDelegate(aceptaOfertaEFI,ofertaEFI );
			addDelegateToHashMap(
					ExitoEFIDelegate.EXITO_OFERTA_EFI_DELEGATE, delegate);
		
		showViewController(ExitoOfertaEFIViewController.class);
	}
	
	/*
	 * Muestra la pantalla de importe a modificar EFI
	 */
	public void showModificaImporteEFI(OfertaEFI ofertaEFI) {
		// TODO Auto-generated method stub
		ModificarImporteDelegate delegate = (ModificarImporteDelegate) getBaseDelegateForKey(ModificarImporteDelegate.MODIFICAR_IMPORTE_DELEGATE_ID);
		if (null != delegate) 
				removeDelegateFromHashMap(ModificarImporteDelegate.MODIFICAR_IMPORTE_DELEGATE_ID);
			delegate = new ModificarImporteDelegate(ofertaEFI);
			addDelegateToHashMap(
					ModificarImporteDelegate.MODIFICAR_IMPORTE_DELEGATE_ID, delegate);
		
		showViewController(ModificarImporteEFIViewController.class);
	}
	
	/*
	 * Muestra la pantalla de detalle oferta consumo 
	 */
	public void showDetalleConsumo(OfertaConsumo ofertaConsumo, Promociones promocion ) {
		// TODO Auto-generated method stub
		DetalleOfertaConsumoDelegate delegate = (DetalleOfertaConsumoDelegate) getBaseDelegateForKey(DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE);
		if (null == delegate) {
			delegate = new DetalleOfertaConsumoDelegate(ofertaConsumo, promocion);
			addDelegateToHashMap(
					DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE, delegate);
		}else{
			delegate.setOfertaConsumo(ofertaConsumo);
		}
		showViewController(DetalleConsumoViewController.class);
	}
	
	/*
	 * Muestra la pantalla de contrato oferta consumo 
	 */
	public void showContratoConsumo(OfertaConsumo ofertaConsumo, Promociones promocion) {
		// TODO Auto-generated method stub
		ContratoOfertaConsumoDelegate delegate = (ContratoOfertaConsumoDelegate) getBaseDelegateForKey(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE);			
		if (null != delegate) 
				removeDelegateFromHashMap(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE);
			delegate = new ContratoOfertaConsumoDelegate(ofertaConsumo,promocion);
			delegate.setTipoOperacion(Operacion.oneClickBmovilConsumo);
			addDelegateToHashMap(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE, delegate);				
			showViewController(ContratoConsumoViewController.class);
	}
	
	/*
	 * Muestra la pantalla de PopUpPaperless
	 */
	public void showTextoPaperless(TextoPaperless textoPaperless, String claveCampanaPaperless) {
		PopUpPaperlessDelegate delegate = (PopUpPaperlessDelegate) getBaseDelegateForKey(PopUpPaperlessDelegate.TEXTO_PAPERLESS_ID);
		if (null != delegate)
			removeDelegateFromHashMap(PopUpPaperlessDelegate.TEXTO_PAPERLESS_ID);
		delegate = new PopUpPaperlessDelegate(textoPaperless, claveCampanaPaperless);
		addDelegateToHashMap(
				PopUpPaperlessDelegate.TEXTO_PAPERLESS_ID, delegate);

		showViewController(PopUpPaperlessController.class);
	}

	/**
	 * Muestra la poliza consumo.
	 * 
	 * @param terminosDeUso
	 *            Terminos de uso.
	 */
	public void showPoliza(String poliza) {
		showViewController(CaracteristicasPolizaViewController.class, 0, false,
				new String[] { Constants.TERMINOS_DE_USO_EXTRA },
				new Object[] { poliza });
	}
	
	/**
	 * Muestra formato domiciliacion
	 * 
	 * @param terminosDeUso
	 *            Terminos de uso.
	 */
	
	public void showformatodomiciliacion(String terminos) {
		showViewController(CaracteristicasPolizaViewController.class, 0, false,
				new String[] { Constants.DOMICILIACION_CONSUMO },
				new Object[] { terminos });
	}
	
	/**
	 * Muestra contrato consumo
	 * 
	 * @param terminosDeUso
	 *            Terminos de uso.
	 */
	
	public void showContrato(String contrato) {
		showViewController(CaracteristicasPolizaViewController.class, 0, false,
				new String[] { Constants.CONTRATO_CONSUMO },
				new Object[] { contrato });
	}		
	
	/**
	 * Muestra terminos y condiciones consumo.
	 * 
	 * @param terminosDeUso
	 *            Terminos de uso.
	 */
	
	public void showTerminosConsumo(String terminos) {
		showViewController(CaracteristicasPolizaViewController.class, 0, false,
				new String[] { Constants.TERMINOS_DE_USO_CONSUMO },
				new Object[] { terminos });
	}
	
	/*
	 * Muestra la pantalla de exito  oferta consumo 
	 */
	public void showExitoConsumo(AceptaOfertaConsumo aceptaOfertaConsumo, OfertaConsumo ofertaConsumo) {
		// TODO Auto-generated method stub
		ExitoOfertaConsumoDelegate delegate = (ExitoOfertaConsumoDelegate) getBaseDelegateForKey(ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE);
		if (null == delegate) {
			delegate = new ExitoOfertaConsumoDelegate(aceptaOfertaConsumo,ofertaConsumo);
			addDelegateToHashMap(
					ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE, delegate);
		}else{
			//delegate.setOfertaConsumo(ofertaConsumo);
		}
		showViewController(ExitoOfertaConsumoViewController.class);
	}
	/**
	 * Muestra la pantalla para consultar Interbancarios.
	 */
	public void showConsultarInterbancarios() {
		ConsultaInterbancariosDelegate delegate = (ConsultaInterbancariosDelegate) getBaseDelegateForKey(ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID);
		
		if (delegate == null) {
			delegate = new ConsultaInterbancariosDelegate();
			addDelegateToHashMap(
					ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID,
					delegate);
		}
		//delegate.resetPeriodo();
		delegate.resetPeriodoConsultas();
		showViewController(ConsultaInterbancariosViewController.class);
	}

	/**
	 * Muestra la pantalla detalle interbancaria
	 */
	public void showDetalleInterbancaria(ConsultaInterbancariosDelegate delegate) {
		// TODO Auto-generated method stub
		if (delegate != null)
			removeDelegateFromHashMap(ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID);
		addDelegateToHashMap(
				ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID,
				delegate);
		
		showViewController(DetalleMoviemientoSpeiViewController.class);
	}

	public void showDetalleConsultaMovsInterbancaria(ConsultaMovimientosInterbancariosDelegate delegate) {
		if (delegate != null){
			removeDelegateFromHashMap(ConsultaMovimientosInterbancariosDelegate.CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID);
			addDelegateToHashMap(ConsultaMovimientosInterbancariosDelegate.CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID, delegate);
		}
		showViewController(DetalleConsultaMovimientoSpeiViewController.class);
	}

	public void showPagoTDCCuenta(Account cuenta) {
		PagoTdcDelegate delegate = (PagoTdcDelegate) getBaseDelegateForKey(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID);
		if (delegate == null) {
			delegate = new PagoTdcDelegate();
			addDelegateToHashMap(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID,
					delegate);
		}
		delegate.setCuentaActual(cuenta);
		delegate.setCuentaOrigenSeleccionada(null);
		showViewController(PagoTdcViewController.class);
	}

	public void showPagoTDCCuenta(Account cuenta, Account cuentaOrigen) {
		PagoTdcDelegate delegate = (PagoTdcDelegate) getBaseDelegateForKey(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID);
		if (delegate == null) {
			delegate = new PagoTdcDelegate();
			addDelegateToHashMap(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID,
					delegate);
		}
		delegate.setCuentaActual(cuenta);
		delegate.setCuentaOrigenSeleccionada(cuentaOrigen);
		showViewController(PagoTdcViewController.class);
	}
	
	public void showRetiroSinTarjetaViewController() {
		AltaRetiroSinTarjetaDelegate delegate = (AltaRetiroSinTarjetaDelegate) getBaseDelegateForKey(AltaRetiroSinTarjetaDelegate.ALTA_RETIRO_SINTAR_DELEGATE_ID);
		if (null == delegate) {
			delegate = new AltaRetiroSinTarjetaDelegate();
			addDelegateToHashMap(AltaRetiroSinTarjetaDelegate.ALTA_RETIRO_SINTAR_DELEGATE_ID,delegate);
		}
		showViewController(AltaRetiroSinTarjetaViewController.class);
	}

	public void showMenuTransferenciasInterbancariasViewController() {
		MenuTransferenciasInterbancariasDelegate delegate = (MenuTransferenciasInterbancariasDelegate) getBaseDelegateForKey(MenuTransferenciasInterbancariasDelegate.MENU_TRANSFERENCIAS_INTERBANCARIAS_DELEGATE_ID);
		if (null == delegate) {
			delegate = new MenuTransferenciasInterbancariasDelegate();
			addDelegateToHashMap(MenuTransferenciasInterbancariasDelegate.MENU_TRANSFERENCIAS_INTERBANCARIAS_DELEGATE_ID,
					delegate);
		}

		showViewController(MenuTransferenciasInterbancariasViewController.class);

	}

	public void showConsultaMovimientosInterbancariosViewController() {
		ConsultaMovimientosInterbancariosDelegate delegate = (ConsultaMovimientosInterbancariosDelegate) getBaseDelegateForKey(ConsultaMovimientosInterbancariosDelegate.CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ConsultaMovimientosInterbancariosDelegate();
			addDelegateToHashMap(ConsultaMovimientosInterbancariosDelegate.CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID,
					delegate);
		}

		showViewController(ConsultaMovimientosInterbancariosViewController.class);

	}

	public void showMenuBBVACredit() {
		showViewController(MenuPrincipalActivity.class);
	}


	public void showBackViewCredit()
	{
		showViewController(MainController.getInstance().getActivityController().getCurrentActivity().getClass());
	}



	public void showBackViewCredit(BaseActivity activity)
	{
		showViewController(activity.getClass());
	}

	public void showMenuHamburguesaViewController(String[] arr1, String[] arr2)
	{
		showViewController(MenuHamburguesaViewsControllers.class,0,false,arr1,arr2);
	}

	public void showAltaRetiroSinTarjetaViewController() {
		showViewController(AltaRetiroSinTarjetaViewController.class,Intent.FLAG_ACTIVITY_CLEAR_TOP,false);
	}

}