package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.consultainversiones.gui.controllers.DetalleInversionViewController;
import bancomer.api.consultainversiones.gui.controllers.DetalleMenuInversionesViewController;
import bancomer.api.consultainversiones.implementations.InitConsultaInversiones;
import bancomer.api.consultainversiones.implementations.InitMenuInversiones;
import bancomer.api.consultainversiones.models.ConsultaInversionesPlazoData;
import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;
import bancomer.api.consultaotroscreditos.models.ConsultaOtrosCreditosData;
import bancomer.api.consultaotroscreditos.models.Creditos;
import bancomer.api.pagarcreditos.gui.controllers.PagarCreditoViewController;
import bancomer.api.pagarcreditos.implementations.InitPagoCredito;
import bancomer.api.pagarcreditos.io.BanderasServer;
import bancomer.api.pagarcreditos.models.Credito;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuPrincipalViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MisCuentasViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import bancomer.api.pagarcreditos.models.ImportesPagoCreditoData;

public class MisCuentasDelegate extends DelegateBaseAutenticacion {

	public final static long MIS_CUENTAS_DELEGATE_ID = 0xfa68bdd87706a316L;
	
	private MisCuentasViewController misCuentasViewController;
	
	private Account cuentaSeleccionada;

	private Creditos creditoSeleccionado;

	private Class<?> selectedClass;
	
	private Boolean opcionRetiroSinTarjeta= false;

	private ConsultaOtrosCreditosData consultaOtrosCreditosData;

	private ConsultaInversionesPlazoData consultaInversionesPlazoData;

	public Boolean getOpcionRetiroSinTarjeta() {
		return opcionRetiroSinTarjeta;
	}

	public void setOpcionRetiroSinTarjeta(Boolean opcionRetiroSinTarjeta) {
		this.opcionRetiroSinTarjeta = opcionRetiroSinTarjeta;
	}

	public ConsultaOtrosCreditosData getConsultaOtrosCreditosData() {
		return consultaOtrosCreditosData;
	}

	public void setConsultaOtrosCreditosData(
			ConsultaOtrosCreditosData consultaOtrosCreditosData) {
		this.consultaOtrosCreditosData = consultaOtrosCreditosData;
	}

	public Creditos getCreditoSeleccionado() {
		return creditoSeleccionado;
	}

	public void setCreditoSeleccionado(Creditos creditoSeleccionado) {
		this.creditoSeleccionado = creditoSeleccionado;
	}

	public ConsultaInversionesPlazoData getConsultaInversionesPlazoData() {
		return consultaInversionesPlazoData;
	}

	public void setConsultaInversionesPlazoData(ConsultaInversionesPlazoData consultaInversionesPlazoData) {
		this.consultaInversionesPlazoData = consultaInversionesPlazoData;
	}


	public void setCuentaSeleccionada(Account cuentaSeleccionada) {
		this.cuentaSeleccionada = cuentaSeleccionada;
	}



	public void inicializar()
	{

	}
	
	public MisCuentasViewController getMisCuentasViewController() {
		return misCuentasViewController;
	}

	public void setMisCuentasViewController(MisCuentasViewController misCuentasViewController) {
		this.misCuentasViewController = misCuentasViewController;
	}
	
	public ArrayList<Object> getOpcionesVisibles() {
		ArrayList<Object> registros;
		ArrayList<Object>  lista = new ArrayList<Object>();
		
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();

		if(!selectedClass.equals(Creditos.class)) {

			boolean validateFullMenu = (profile == Constants.Perfil.avanzado) || cuentaSeleccionada.isVisible();

			registros = new ArrayList<Object>(2);
			registros.add(Constants.OPERACION_VER_MOVIMIENTOS_TYPE);
			registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_movimientos));
			lista.add(registros);

			if (validateFullMenu) {
				//if(Autenticacion.getInstance().isVisible(Constants.Operacion.dineroMovil, profile)){
				registros = new ArrayList<Object>(2);
				//registros.add(Constants.OPERACION_EFECTIVO_MOVIL_TYPE);
				//registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_dineroMovil));
				registros.add(Constants.OPERACION_TRASPASO_MIS_CUENTAS);
				registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_traspasoMisCuentas));
				lista.add(registros);
				//}

				if (Autenticacion.getInstance().isVisible(Constants.Operacion.dineroMovil, profile) && !cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.OPERACION_EFECTIVO_MOVIL_TYPE);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_dineroMovil));
					lista.add(registros);
				}

				if (Autenticacion.getInstance().isVisible(Constants.Operacion.transferirBancomer, profile) && !cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.INTERNAL_TRANSFER_DEBIT_TYPE);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_transferenciaBancomer));
					lista.add(registros);
				}

				/*if(Autenticacion.getInstance().isVisible(Constants.Operacion.transferirBancomer, profile)){
					registros = new ArrayList<Object>(2);
					registros.add(Constants.INTERNAL_TRANSFER_EXPRESS_TYPE);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_transferenciaExpress));
					lista.add(registros);
				}
				*/
				if (Autenticacion.getInstance().isVisible(Constants.Operacion.transferirInterbancaria, profile)) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.EXTERNAL_TRANSFER_TYPE);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_transferenciaOtros));
					lista.add(registros);
				}

				//			if(Autenticacion.getInstance().isVisible(Constants.Operacion.importesTDC, profile)){
//				if (cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.OPERACION_TDC);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_pagarTDC));
					lista.add(registros);
//				}

				if (Autenticacion.getInstance().isVisible(Constants.Operacion.pagoServicios, profile)) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.SERVICE_PAYMENT_TYPE);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_pagarServicio));
					lista.add(registros);
				}

				if (Autenticacion.getInstance().isVisible(Constants.Operacion.compraTiempoAire, profile) && !cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.AIRTIME_PURCHASE_TYPE);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_compraTiempoAire));
					lista.add(registros);
				}

				/*if(Autenticacion.getInstance().isVisible(Constants.Operacion.compraTiempoAire, profile)){
					registros = new ArrayList<Object>(2);
					registros.add(Constants.AIRTIME_PURCHASE_TYPE);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_compraTiempoAire));
					lista.add(registros);
				}*/

				//O3
				if (!cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)) {
					registros = new ArrayList<Object>(2);
					registros.add(Constants.OPERACION_DEPOSITOS_RECIBIDOS);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_depositosrecibidos));
					lista.add(registros);
				}

				//Retiro sin tarjeta
				if (Autenticacion.getInstance().isVisible(Constants.Operacion.retiroSinTarjeta, profile) && !cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)) {
					//&& (!cuentaSeleccionada.getType().equalsIgnoreCase("TC"))){
					registros = new ArrayList<Object>(2);
					registros.add(Constants.OPERACION_RETIRO_SINTARJETA);
					registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_retiroSinTarjeta));
					lista.add(registros);
				}
			}
		}else{
			// Se habrá seleccionado un credito
			registros = new ArrayList<Object>(2);
			registros.add(Constants.OPERACION_OTROS_CREDITOS);
			registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_informacionGeneral));
			lista.add(registros);

			registros = new ArrayList<Object>(2);
			registros.add(Constants.OPERACION_PAGAR_OTROS_CREDITOS);
			registros.add(SuiteApp.appContext.getString(R.string.misCuentasQuiero_pagarCredito));
			lista.add(registros);
		}
		
		return lista;
	}
	
	public ArrayList<Object> agruparCuentas(){
		ArrayList<Object> arregloCuentasTarjetas = new ArrayList<Object>();
		
        ArrayList<Account> cuentas = new ArrayList<Account>(5);
        ArrayList<Account> tarjetas = new ArrayList<Account>(5);
        
        for(Account a : Session.getInstance(SuiteApp.appContext).getAccounts()){
        //for(Account a : Session.getAccountsMC()){
      	  if (!a.getType().equals(Constants.CREDIT_TYPE)&& !a.getType().equals(Constants.INVERSION_TYPE) ){
           	  cuentas.add(a);
      	  } else if (a.getType().equals(Constants.CREDIT_TYPE)) {
      		  tarjetas.add(a);
      	  }
        }
        
        if (cuentas.size() > 0) {
			arregloCuentasTarjetas.add(cuentas);
		}
        if (tarjetas.size() > 0) {
			arregloCuentasTarjetas.add(tarjetas);
		}
        
		return arregloCuentasTarjetas;
	}
	
	public ArrayList<Account> getInversiones() {
		ArrayList<Account> inversiones = new ArrayList<Account>();

		for(Account a : Session.getInstance(SuiteApp.appContext).getAccounts()){
			if (a.getType().equals(Constants.INVERSION_TYPE)) {
				inversiones.add(a);
			}
		}

		return inversiones;
	}

	@Override
	public void performAction(Object obj) {
		if (misCuentasViewController.getMisCuentasOpciones() != null && misCuentasViewController.getMisCuentasOpciones().isShowing()) {
			if(selectedClass.equals(Creditos.class)){
				if (bancomer.api.common.commons.Tools.isNetworkConnected(misCuentasViewController)) {
					misCuentasViewController.getMisCuentasOpciones().opcionSeleccionada(creditoSeleccionado, (String)obj);
				} else {
					misCuentasViewController.getMisCuentasOpciones().dismiss();
					misCuentasViewController.showErrorMessage(misCuentasViewController.getString(R.string.error_communications));
				}
			}else{
				misCuentasViewController.getMisCuentasOpciones().opcionSeleccionada(cuentaSeleccionada, (String)obj);
			}
		} else if(obj instanceof Creditos){
			creditoSeleccionado = (Creditos)obj;
			selectedClass = Creditos.class;

			misCuentasViewController.seleccionaCredito(creditoSeleccionado);
		} else if(obj instanceof Account && ((Account) obj).getType().equals("IN")) {
			cuentaSeleccionada = (Account)obj;
			consultarInversion(((Account) obj).getFullNumber());
		} else {
			cuentaSeleccionada = (Account)obj;
			selectedClass = Account.class;
			misCuentasViewController.seleccionaCuenta(cuentaSeleccionada);
		}
	}

	public void consultarImportesPagoCredito(String numeroCredito) {
		//misCuentasViewController.muestraIndicadorActividad(misCuentasViewController.getString(R.string.alert_operation), misCuentasViewController.getString(R.string.alert_connecting));
		//misCuentasViewController.getParentViewsController().setActivityChanging(true);

		InitPagoCredito init = InitPagoCredito.getInstance(misCuentasViewController, new BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		init.setCallBackSession(misCuentasViewController);
		init.setCallBackAPI(misCuentasViewController);
		init.configurar();

		init.consultarImportes(numeroCredito);
	}

	public void mostrarPagoCredito(ImportesPagoCreditoData importesPagoCreditoData) {
		InitPagoCredito init = InitPagoCredito.getInstance(misCuentasViewController, new BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		misCuentasViewController.getParentViewsController().setActivityChanging(true);
		init.setCallBackSession(misCuentasViewController);
		init.setCallBackAPI(misCuentasViewController);

		Credito creditos = new Credito();
		creditos.setAdeudo(creditoSeleccionado.getAdeudo());
		creditos.setEstatusCredito(creditoSeleccionado.getEstatusCredito());
		creditos.setNumeroCredito(creditoSeleccionado.getNumeroCredito());
		creditos.setTipoCredito(creditoSeleccionado.getTipoCredito());

		init.configurar();
		init.setImportesAndCredito(importesPagoCreditoData, creditos);

		final Intent intent = new Intent(misCuentasViewController,
				PagarCreditoViewController.class);
		misCuentasViewController.startActivity(intent);
	}

	public void procesarClickCreditos(Creditos credito, String opcionSeleccionada){
		/**
		 * Inicializamos los estados posibles para los escenarios
		 */
		String statusVigente = "0";
		ArrayList<String> listaEstados = new ArrayList<String>();
		listaEstados.add("2");
		listaEstados.add("3");
		listaEstados.add("C");
		listaEstados.add("V");
		/**
		 *
		 */


		if(credito.getEstatusCredito().equalsIgnoreCase(statusVigente)){
			//EA#4
			// Llamada al modulo de Consulta Creditos
			Session session = Session.getInstance(SuiteApp.getInstance());
			// Inicializamos la clase de configuracion

			if (opcionSeleccionada.equals(Constants.OPERACION_OTROS_CREDITOS)) {

				misCuentasViewController.muestraIndicadorActividad(misCuentasViewController.getString(R.string.alert_operation), misCuentasViewController.getString(R.string.alert_connecting));
				((BmovilViewsController)misCuentasViewController.getParentViewsController()).setActivityChanging(true);

				InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance(misCuentasViewController);//(misCuentasViewController,((BmovilViewsController)misCuentasViewController.getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient());
				// Establecemos los parametros necesarios
				init.getConsultaSend().setIUM(session.getIum());
				init.getConsultaSend().setUsername(session.getUsername());
				init.getConsultaSend().setCallBackModule(misCuentasViewController);
				init.getConsultaSend().setCallBackSession(misCuentasViewController);
				init.getConsultaSend().setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR);

				// Realizamos la llamada a consulta otros creditos
				init.preShowDetalleConsultarOtrosCreditos(credito);
			} else if (opcionSeleccionada.equals(Constants.OPERACION_PAGAR_OTROS_CREDITOS)) {
				consultarImportesPagoCredito(credito.getNumeroCredito());
			}
		}
		else if(listaEstados.contains(credito.getEstatusCredito())){
			//EA#5
			DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					misCuentasViewController.getMisCuentasOpciones().dismiss();
				}
			};

			if(credito.getEstatusCredito().equalsIgnoreCase("V")){
				if(credito.getTipoCredito().equalsIgnoreCase("C") || credito.getTipoCredito().equalsIgnoreCase("P") || credito.getTipoCredito().equalsIgnoreCase("N")){
					misCuentasViewController.showInformationAlert(misCuentasViewController.getString(R.string.label_error), misCuentasViewController.getString(R.string.bmovil_menu_miscuentas_otroscreditos_error_estatusVC), onClickListener);
				}else if (credito.getTipoCredito().equalsIgnoreCase("H")){
					misCuentasViewController.showInformationAlert(misCuentasViewController.getString(R.string.label_error), misCuentasViewController.getString(R.string.bmovil_menu_miscuentas_otroscreditos_error_estatusVH), onClickListener);
				}

			}else{
				if(credito.getTipoCredito().equalsIgnoreCase("H")){
					misCuentasViewController.showInformationAlert(misCuentasViewController.getString(R.string.label_error), misCuentasViewController.getString(R.string.bmovil_menu_miscuentas_otroscreditos_error_estatusBH), onClickListener);
				}else if(credito.getTipoCredito().equalsIgnoreCase("C") || credito.getTipoCredito().equalsIgnoreCase("P") || credito.getTipoCredito().equalsIgnoreCase("N")){
					misCuentasViewController.showInformationAlert(misCuentasViewController.getString(R.string.label_error), misCuentasViewController.getString(R.string.bmovil_menu_miscuentas_otroscreditos_error_estatusBC), onClickListener);
				}

			}
		}
	}
	
	public void comprobarRecortado(){
	//	Session session = Session.getInstance(SuiteApp.appContext);
	//	Constants.Perfil perfil = session.getClientProfile();
		
	//	if(Constants.Perfil.recortado.equals(perfil)){
				
				//	if(Constants.siEstatusAlertas.equals(session.getEstatusAlertas())){
						// tiene ALERTAS
						
						solicitarAlertas(misCuentasViewController);
			
//					} else {
//						// NO tiene ALERTAS
//						
//						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
//						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_dineromovil_recortado_sin_alertas));
//						
//					}
			//	}
		
	}


	
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {
		 if (misCuentasViewController instanceof MisCuentasViewController) {
			
			 ((BmovilViewsController)misCuentasViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller, true);
		}
		
	}
	
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			if (operationId == Server.OP_SOLICITAR_ALERTAS) {
				
				SolicitarAlertasData validacionalertas = (SolicitarAlertasData) response.getResponse();
				if (Constants.ALERT02.equals(validacionalertas.getValidacionAlertas())){
					// 02 no tiene alertas
					SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
					if (opcionRetiroSinTarjeta){
						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_retiroSinTarjeta_recortado_sin_alertas));
						opcionRetiroSinTarjeta=false;
					}else {
						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_dineromovil_recortado_sin_alertas));
					}
					
				} else{
				
					setSa(validacionalertas);
					analyzeAlertasRecortadoSinError();
				}
				return;
				
			}  else if(operationId == Server.OP_CONSULTA_OTROS_CREDITOS){

				ConsultaOtrosCreditosData consultaOtrosCreditosData = (ConsultaOtrosCreditosData) response.getResponse();

				setConsultaOtrosCreditosData(consultaOtrosCreditosData);

				if (! (SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController() instanceof MisCuentasViewController) ){
					((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showMisCuentasViewController();
				}else{
					procesaOtrosCreditos();
				}
			}
			
		}else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {} 
		
	else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {}
		misCuentasViewController.showInformationAlertEspecial(misCuentasViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
	}


	public void procesaOtrosCreditos(){
		ConsultaOtrosCreditosData ccData = getConsultaOtrosCreditosData();

		if((ccData != null) && (ccData.getListaCreditos() != null) && (!ccData.getListaCreditos().isEmpty())){
			ArrayList<Object> encabezado = new ArrayList<Object>();
			encabezado.add(null);
			encabezado.add(misCuentasViewController.getString(R.string.bmovil_menu_miscuentas_otroscreditos_tabla_titulo));

			List<Creditos> listaCreditos = ccData.getListaCreditos();
			ArrayList<Object> listaTmp = new ArrayList<Object>();

			Creditos credito = null;
			ArrayList<Object> registros;
			Double saldoActual;
			Double saldoTotal = 0.0;
			for (int i = 0; i < listaCreditos.size(); i++)
			{
				registros = new ArrayList<Object>();

				credito = listaCreditos.get(i);
				registros.add(credito);

				String creditName = credito.getNumeroCredito();
				if(credito.getTipoCredito().equalsIgnoreCase("H")){
					creditName = "Hipotecario";
				}else if(credito.getTipoCredito().equalsIgnoreCase("C")){
					creditName = "Consumo";
				}else if(credito.getTipoCredito().equalsIgnoreCase("P")){
					creditName = "Préstamo Personal";
				}else if(credito.getTipoCredito().equalsIgnoreCase("N")){
					creditName = "Nómina";
				}
				registros.add(creditName+"\n"+ Tools.hideAccountNumber(credito.getNumeroCredito()));

				saldoActual = Tools.getDoubleAmountFromServerString(credito.getAdeudo());
				Boolean neg = false;
				if(saldoActual < 0) neg = true;
				registros.add(Tools.formatAmount(credito.getAdeudo(), neg));

				listaTmp.add(registros);

				saldoTotal = saldoTotal + saldoActual;
			}
			String saldo = Tools.convertDoubleToBigDecimalAndReturnString(saldoTotal);
			encabezado.add(Tools.formatAmount(saldo,saldoTotal<0));

			misCuentasViewController.llenarEncabezados(encabezado, 2, listaTmp);
		}else{
			misCuentasViewController.ocultaTablaOtrosCreditos();
		}
	}


	public void mostrarInversion(ConsultaInversionesPlazoData inversionesPlazo) {
		InitConsultaInversiones init = InitConsultaInversiones.getInstance(misCuentasViewController, new bancomer.api.consultainversiones.io.BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		misCuentasViewController.getParentViewsController().setActivityChanging(true);
		init.setCallBackSession(misCuentasViewController);
		init.setCallBackAPI(misCuentasViewController);

		init.configurar(inversionesPlazo.getListaInversiones().get(0));
		init.setInversionesData(inversionesPlazo);

		final Intent intent = new Intent(misCuentasViewController,
				DetalleInversionViewController.class);
		misCuentasViewController.startActivity(intent);
	}

	public void mostrarMenuInversiones(ConsultaInversionesPlazoData inversionesPlazo){

		InitMenuInversiones init = InitMenuInversiones.getInstance(misCuentasViewController, new bancomer.api.consultainversiones.io.BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		misCuentasViewController.getParentViewsController().setActivityChanging(true);
		init.setCallBackSession(misCuentasViewController);
		init.setCallBackAPI(misCuentasViewController);
		init.setNumeroCuenta(inversionesPlazo.getNumeroCuenta());

		init.configurar(inversionesPlazo.getListaInversiones());

		final Intent intent = new Intent(misCuentasViewController,
				DetalleMenuInversionesViewController.class);
		misCuentasViewController.startActivity(intent);
	}


	public void consultarInversion(String fullNumber) {
		misCuentasViewController.muestraIndicadorActividad(misCuentasViewController.getString(R.string.alert_operation), misCuentasViewController.getString(R.string.alert_connecting));
		misCuentasViewController.getParentViewsController().setActivityChanging(true);

		InitConsultaInversiones init = InitConsultaInversiones.getInstance(misCuentasViewController, new bancomer.api.consultainversiones.io.BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		init.setCallBackSession(misCuentasViewController);
		init.setCallBackAPI(misCuentasViewController);
		init.configurar(null);

		init.consultarInversiones(fullNumber);
	}

	public Account getCuentaSeleccionada() {
		return cuentaSeleccionada;
	}

	@Override
	public boolean mostrarContrasenia() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.consultarCreditos,
				perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarCVV(Constants.Operacion.consultarCreditos, perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.consultarCreditos,
				perfil);
		return value;
	}

	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public Constants.TipoOtpAutenticacion tokenAMostrar() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.consultarCreditos,
					perfil);
		} catch (Exception ex) {
			Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
			tipoOTP = null;
		}
		return tipoOTP;
	}

	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {

		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;

		fila = new ArrayList<String>();
		fila.add("Consulta");
		fila.add("Creditos Otorgados");
		tabla.add(fila);
		fila = new ArrayList<String>();
		fila.add("Fecha de operación");

		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		fila.add(formatter.format(date));

		tabla.add(fila);

		return tabla;
	}

	@Override
	public int getTextoEncabezado() {
		int resTitle;
		resTitle = R.string.mis_cuentas_title;
		return resTitle;
	}

	@Override
	public int getNombreImagenEncabezado() {
		int resIcon = R.drawable.bmovil_mis_cuentas_icono;
		return resIcon;
	}


	@Override
	public String getTextoTituloResultado() {
		int txtTitulo = R.string.transferir_detalle_operacion_exitosaTitle;
		return misCuentasViewController.getString(txtTitulo);
	}


	/**
	 * Invoca la operacion para hacer el cambio de cuenta asociada
	 */
	@Override
	public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController, String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
		confirmacionAutenticacionViewController.muestraIndicadorActividad(confirmacionAutenticacionViewController.getString(R.string.alert_operation), confirmacionAutenticacionViewController.getString(R.string.alert_connecting));

		// Llamada al modulo de Consulta Creditos
		Session session = Session.getInstance(SuiteApp.getInstance());
		// Inicializamos la clase de configuracion
		InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance(confirmacionAutenticacionViewController);//(confirmacionAutenticacionViewController,((BmovilViewsController)confirmacionAutenticacionViewController.getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient());
		// Establecemos los parametros necesarios
		init.getConsultaSend().setIUM(session.getIum());
		init.getConsultaSend().setUsername(session.getUsername());
		init.getConsultaSend().setCveAcceso(contrasenia==null?"":contrasenia);
		init.getConsultaSend().setTarjeta5Dig(campoTarjeta==null?"":campoTarjeta);
		init.getConsultaSend().setCodigoNip(nip==null?"":nip);
		init.getConsultaSend().setCodigoCvv2(cvv==null?"":cvv);
		init.getConsultaSend().setCodigoOtp(token==null?"":token);
		init.getConsultaSend().setCallBackModule(confirmacionAutenticacionViewController);
		init.getConsultaSend().setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR);
		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultarCreditos, session.getClientProfile());
		init.getConsultaSend().setCadenaAutenticacion(cadenaAutenticacion);
		// Realizamos la peticion
		init.lookForCreditosData(confirmacionAutenticacionViewController);


	}


	public void consultaOtrosCreditos(MenuPrincipalViewController viewController){
		viewController.muestraIndicadorActividad(viewController.getString(R.string.alert_operation), viewController.getString(R.string.alert_connecting));

		// Llamada al modulo de Consulta Creditos
		Session session = Session.getInstance(SuiteApp.getInstance());
		// Inicializamos la clase de configuracion
		if (Server.ALLOW_LOG) Log.d("[CGI-Configuracion-Obligatorio] >> PRE -", ((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient().toString());
		InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance(viewController);//(viewController, ((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient());
		// Establecemos los parametros necesarios
		init.getConsultaSend().setIUM(session.getIum());
		init.getConsultaSend().setUsername(session.getUsername());
		init.getConsultaSend().setCallBackModule(misCuentasViewController);
		init.getConsultaSend().setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR);
		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultarCreditos, session.getClientProfile());
		init.getConsultaSend().setCadenaAutenticacion(cadenaAutenticacion);
		// Realizamos la peticion
		init.lookForCreditosData(viewController);


	}
}
