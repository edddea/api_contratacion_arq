package suitebancomer.aplicaciones.bmovil.classes.model;

public class ConsultaDepositosRecbidosTransDeOtrosBancos {
	private String fecha;
	private String hora;
	private String importe;
	private String bancoOrdenante;
	private String cuentaOrdenante;
	private String motivoPago;
	private String claveRastreo;
	private String referenciaNumerica;
	
	
	public String getFecha() {
		return fecha;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getHora() {
		return hora;
	}


	public void setHora(String hora) {
		this.hora = hora;
	}


	public String getImporte() {
		return importe;
	}


	public void setImporte(String importe) {
		this.importe = importe;
	}


	public String getBancoOrdenante() {
		return bancoOrdenante;
	}


	public void setBancoOrdenante(String bancoOrdenante) {
		this.bancoOrdenante = bancoOrdenante;
	}


	public String getCuentaOrdenante() {
		return cuentaOrdenante;
	}


	public void setCuentaOrdenante(String cuentaOrdenante) {
		this.cuentaOrdenante = cuentaOrdenante;
	}


	public String getMotivoPago() {
		return motivoPago;
	}


	public void setMotivoPago(String motivoPago) {
		this.motivoPago = motivoPago;
	}


	public String getClaveRastreo() {
		return claveRastreo;
	}


	public void setClaveRastreo(String claveRastreo) {
		this.claveRastreo = claveRastreo;
	}


	public String getReferenciaNumerica() {
		return referenciaNumerica;
	}


	public void setReferenciaNumerica(String referenciaNumerica) {
		this.referenciaNumerica = referenciaNumerica;
	}


	ConsultaDepositosRecbidosTransDeOtrosBancos(){
		fecha="";
		hora="";
		importe="";
		bancoOrdenante="";
		cuentaOrdenante="";
		motivoPago="";
		claveRastreo="";
		referenciaNumerica="";
	}
}
