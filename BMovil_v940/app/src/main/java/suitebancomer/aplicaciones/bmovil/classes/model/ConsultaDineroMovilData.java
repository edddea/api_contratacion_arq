package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ConsultaDineroMovilData implements ParsingHandler {
	private ArrayList<ConsultaDineroMovil> movimientos;
	private String indicadorPagina;
	private String areaPagina;
	private String numeroMovimientos;
	
	/**
	 * @return the movimientos
	 */
	public ArrayList<ConsultaDineroMovil> getMovimientos() {
		return movimientos;
	}

	/**
	 * @param movimientos the movimientos to set
	 */
	public void setMovimientos(ArrayList<ConsultaDineroMovil> movimientos) {
		this.movimientos = movimientos;
	}

	/**
	 * @return the indicadorPagina
	 */
	public String getIndicadorPagina() {
		return indicadorPagina;
	}

	/**
	 * @param indicadorPagina the indicadorPagina to set
	 */
	public void setIndicadorPagina(String indicadorPagina) {
		this.indicadorPagina = indicadorPagina;
	}

	/**
	 * @return the areaPagina
	 */
	public String getAreaPagina() {
		return areaPagina;
	}

	/**
	 * @param areaPagina the areaPagina to set
	 */
	public void setAreaPagina(String areaPagina) {
		this.areaPagina = areaPagina;
	}

	/**
	 * @return the numeroMovimientos
	 */
	public String getNumeroMovimientos() {
		return numeroMovimientos;
	}

	/**
	 * @param numeroMovimientos the numeroMovimientos to set
	 */
	public void setNumeroMovimientos(String numeroMovimientos) {
		this.numeroMovimientos = numeroMovimientos;
	}
	
	public ConsultaDineroMovilData()  {
		movimientos = new ArrayList<ConsultaDineroMovil>();
		indicadorPagina = "";
		areaPagina = "";
		numeroMovimientos = "";
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		indicadorPagina = parser.parseNextValue("IP");
		areaPagina = parser.parseNextValue("AA");
		numeroMovimientos = parser.parseNextValue("OC");
		
		movimientos = new ArrayList<ConsultaDineroMovil>();
		ConsultaDineroMovil consulta;
		int numMov = Integer.parseInt(numeroMovimientos);
		for(int i = 0; i < numMov; i++) {
			consulta = new ConsultaDineroMovil();
			consulta.setFolioOperacion(parser.parseNextValue("FO"));
			consulta.setImporte(parser.parseNextValue("IM"));
			
			// Si no viene el tipo de cuenta en CC.
			Account origen = new Account();
			origen.setNumber(parser.parseNextValue("CC"));
			consulta.setCuentaOrigen(origen);
			
			consulta.setCodigoCanal(parser.parseNextValue("CG"));
			consulta.setEstatus(parser.parseNextValue("EO"));
			consulta.setCelularBeneficiario(parser.parseNextValue("NU"));
			consulta.setCompaniaBeneficiario(parser.parseNextValue("OA"));
			consulta.setBeneficiario(parser.parseNextValue("BF"));
			consulta.setConcepto(parser.parseNextValue("CP"));
			consulta.setFechaAlta(parser.parseNextValue("FE"));
			consulta.setFechaVigencia(parser.parseNextValue("FX"));
			consulta.setHoraExpiracion(parser.parseNextValue("HO"));
			consulta.setCodigoSeguridad(parser.parseNextValue("CS"));

			movimientos.add(consulta);
		}
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}
}
