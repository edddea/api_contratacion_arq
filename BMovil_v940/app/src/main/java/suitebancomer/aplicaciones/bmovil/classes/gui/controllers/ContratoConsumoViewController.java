package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;




import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.gui.delegates.SecurityComponentDelegate;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ContratoOfertaConsumoDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetalleOfertaConsumoDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetalleOfertaEFIDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuPrincipalDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import com.bancomer.oneclickseguros.delegates.DetalleDelegate;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import tracking.TrackingHelper;
import android.widget.ScrollView;


public class ContratoConsumoViewController extends BaseViewController {

	private TextView lblTextoOferta;
	private TextView contrato1;
	private TextView contrato2;//formato
	private TextView contrato3;
	private TextView contrato4;
	private TextView contrato5;
	private TextView txtaviso_seguros_link;
	private TextView txtaviso_seguros;
	private TextView contrato6;
	private TextView txtseguros1;
	private TextView txtseguros2;
	private TextView txtaviso3;//aviso checkbox 3-1
	private TextView lblmensajecheck4;
	private CheckBox cbConsumo1;
	private CheckBox cbConsumo2;
	private CheckBox cbConsumo3;
	private CheckBox cbConsumo4;
	private CheckBox cbConsumo5;
	private RadioButton radioButton1;
	private RadioButton radioButton2;
	private ImageButton btnconfirmar;
	private ImageButton imgaviso_seguro;
	private ImageButton imgaviso;
	BaseViewController me;
	private ContratoOfertaConsumoDelegate contratoOfertaConsumoDelegate;


	LinearLayout vista;
	LinearLayout layout_contrato_seguros;
	LinearLayout layout_aviso_seguros;// layout aviso checkbox 2
	LinearLayout layout_aviso_dom;// layout aviso3-2 checkbox 3
	LinearLayout layout_avisotoken;// layout aviso token 
	private ImageButton imgaviso_token;
	private TextView txtaviso_token;
	
	LinearLayout linearchecKbox1;
	LinearLayout layout_contrato2;
	/**todo lo de token **/
	private LinearLayout contenedorContrasena;
	private LinearLayout contenedorNIP;
	private LinearLayout contenedorASM;
	private LinearLayout contenedorCVV;
	private TextView campoContrasena;
	private TextView campoNIP;
	private TextView campoASM;
	private TextView campoCVV;
	private EditText contrasena;
	private EditText nip;	
	private EditText asm;
	private EditText cvv;
	private TextView instruccionesContrasena;
	private TextView instruccionesNIP;
	private TextView instruccionesASM;
	private TextView instruccionesCVV;
	private boolean isAseguradora=true;
	private boolean isOpcionesSelec;
	private boolean isBuro=true;
	private boolean isSeguro=true;
	//Nuevo Campo
		private TextView campoTarjeta;
		private LinearLayout contenedorCampoTarjeta;
		private EditText tarjeta;
		private TextView instruccionesTarjeta;
	
	//AMZ
	public BmovilViewsController parentManager;	
	//ARR
			Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
			
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_contrato_oferta_consumo);
		me=this;
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
					
				TrackingHelper.trackState("clausulas consumo", parentManager.estados);
				//AMZ	
		
		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE));

		contratoOfertaConsumoDelegate = (ContratoOfertaConsumoDelegate)getDelegate();
		contratoOfertaConsumoDelegate.setControladorDetalleILCView(this);

		setTitle(R.string.bmovil_promocion_title_consumo, R.drawable.icono_consumo);
		vista = (LinearLayout)findViewById(R.id.contratoofertaConsumo_view_controller_layout);
		layout_contrato_seguros = (LinearLayout)findViewById(R.id.layout_contrato_seguros); 
		layout_aviso_seguros = (LinearLayout)findViewById(R.id.layout_aviso_seguros); 
		layout_aviso_dom = (LinearLayout)findViewById(R.id.layout_aviso_dom); 
		layout_avisotoken = (LinearLayout)findViewById(R.id.layout_avisotoken); 
		linearchecKbox1=(LinearLayout)findViewById(R.id.linearchecKbox1); 
		layout_contrato2=(LinearLayout)findViewById(R.id.layout_contrato2); 
		lblTextoOferta = (TextView) findViewById(R.id.lblTextoOferta);
		contrato1 = (TextView) findViewById(R.id.contrato1);
		contrato2 = (TextView) findViewById(R.id.contrato2);
		contrato3 = (TextView) findViewById(R.id.contrato3);
		contrato4 = (TextView) findViewById(R.id.contrato4);
		contrato5 = (TextView) findViewById(R.id.contrato5);
		contrato6 = (TextView) findViewById(R.id.contrato6);
		txtseguros1 = (TextView) findViewById(R.id.txtseguros1);
		txtseguros2 = (TextView) findViewById(R.id.txtseguros2);
		txtaviso_token = (TextView) findViewById(R.id.txtaviso_token);
		imgaviso_token = (ImageButton) findViewById(R.id.imgaviso_token);
		imgaviso = (ImageButton) findViewById(R.id.imgaviso);
		txtaviso3 = (TextView) findViewById(R.id.txtaviso3);
		lblmensajecheck4 = (TextView) findViewById(R.id.lblmensajecheck4);
		txtaviso_seguros_link = (TextView) findViewById(R.id.txtaviso_seguros_link);
		cbConsumo1=(CheckBox) findViewById(R.id.cbConsumo1);
		cbConsumo2=(CheckBox) findViewById(R.id.cbConsumo2);
		cbConsumo3=(CheckBox) findViewById(R.id.cbConsumo3);
		cbConsumo4=(CheckBox) findViewById(R.id.cbConsumo4);
		cbConsumo5=(CheckBox) findViewById(R.id.cbConsumo5);
		radioButton1=(RadioButton) findViewById(R.id.radioButton1);
		radioButton2=(RadioButton) findViewById(R.id.radioButton2);
		txtaviso_seguros=(TextView)findViewById(R.id.txtaviso_seguros);
		imgaviso_seguro=(ImageButton)findViewById(R.id.imgaviso_seguro);
		findViews();
		configurarPantalla();		
		/*cbConsumo2.setClickable(false);
		cbConsumo3.setClickable(false);
		cbConsumo4.setClickable(false);
		cbConsumo5.setClickable(false);*/
		txtseguros1.setText("Seguros Bancomer, con costo de\n"+Tools.formatAmount(contratoOfertaConsumoDelegate.getOfertaConsumo().getImpSegSal(),false)+" por única vez.");
		
		//validacion buro credito
		if(contratoOfertaConsumoDelegate.getOfertaConsumo().getEstatusOferta().equals(Constants.PREFORMALIZADO)){
			cbConsumo1.setChecked(true);
			linearchecKbox1.setVisibility(View.GONE);
			//cbConsumo2.setClickable(true);
			cbConsumo1.setClickable(false);
			isBuro=false;
		}
		//validacion aseguradora 
		if(contratoOfertaConsumoDelegate.getOfertaConsumo().getProducto().equals(Constants.IDBOVEDA1)||contratoOfertaConsumoDelegate.getOfertaConsumo().getProducto().equals(Constants.IDBOVEDA2)){
			cbConsumo2.setChecked(true);
			layout_contrato2.setVisibility(View.GONE);
			isAseguradora=false;
			//cbConsumo3.setClickable(true);
			cbConsumo2.setClickable(false);
			isSeguro=false;
		}	
		
		layout_contrato_seguros.setVisibility(View.GONE);
		layout_aviso_dom.setVisibility(View.GONE);
		txtaviso3.setVisibility(View.GONE);
		lblmensajecheck4.setVisibility(View.GONE);
		//poliza de el radioButton 2
		//layout_aviso_seguros.setVisibility(View.GONE);
		
		txtaviso3.setText("El pago se realizará de forma automática (Domiciliación) durante "+contratoOfertaConsumoDelegate.getOfertaConsumo().getTotalPagos()+" pagos los días "+contratoOfertaConsumoDelegate.getOfertaConsumo().getDescDiasPago()+" de cada mes, por un importe de "+Tools.formatAmount(contratoOfertaConsumoDelegate.getOfertaConsumo().getPagoMenFijo(),false)+" con cargo a la cuenta BBVA Bancomer terminación"+Tools.hideAccountNumber(contratoOfertaConsumoDelegate.getOfertaConsumo().getCuentaVinc())+". *El IVA puede variar en cada pago.");
		String importe =Tools.formatAmount(contratoOfertaConsumoDelegate.getOfertaConsumo().getImporte(),false);
		String contrat4="Acepto contratar el crédito por\n"+importe;
		int cont4= contrat4.indexOf(importe);
		SpannableString conte4 = new SpannableString(contrat4);
		conte4.setSpan(new ForegroundColorSpan(Color.rgb(0,158,229)),cont4,contrat4.length(),0);
		contrato4.setText(conte4);
		
		String contrat5 =contrato5.getText().toString();
		SpannableString content5 = new SpannableString(contrat5);
		content5.setSpan(new UnderlineSpan(), 0, contrat5.length(), 0);
		contrato5.setText(content5);
		
		contrato5.setOnClickListener(clickListener);
		
		String aviso =txtaviso_seguros_link.getText().toString();
		SpannableString contentAviso = new SpannableString(aviso);
		contentAviso.setSpan(new UnderlineSpan(), 0, aviso.length(), 0);
		txtaviso_seguros_link.setText(contentAviso);
		
		txtaviso_seguros_link.setOnClickListener(clickListener);
		
		contrasena.addTextChangedListener(new BmovilTextWatcher(this));
		nip.addTextChangedListener(new BmovilTextWatcher(this));
		asm.addTextChangedListener(new BmovilTextWatcher(this));
		cvv.addTextChangedListener(new BmovilTextWatcher(this));
		tarjeta.addTextChangedListener(new BmovilTextWatcher(this));
		/****/
	    final ScrollView sv = (ScrollView) findViewById(R.id.body_layout);
	    sv.postDelayed(new Runnable() {
	        @Override
	        public void run() {
	            //sv.fullScroll(ScrollView.FOCUS_UP);
	            sv.smoothScrollTo(0, 0);
	        }
	    }, 300);
	    /****/
		
		}
	
	
	public void onCheckboxClick(View sender){
		if(sender==cbConsumo1){
			if(cbConsumo1.isChecked()){
				cbConsumo1.setClickable(false);
				//cbConsumo2.setClickable(true);
				//cbConsumo1.setBackgroundResource(R.drawable.an_ic_check_on);
			}
		}else if(sender==cbConsumo2){
			if(!cbConsumo1.isClickable()){				
					layout_contrato_seguros.setVisibility(View.VISIBLE);
					cbConsumo2.setClickable(false);
					//cbConsumo3.setClickable(true);
					cbConsumo2.setBackgroundResource(R.drawable.an_ic_check_on);
			}else{
				if(isBuro)
					showInformationAlert("Debes aceptar consulta de mi historial crediticio antes.");
				
				cbConsumo2.setBackgroundResource(R.drawable.an_ic_check_off);
				}		
		}else if(sender==cbConsumo3){
			if(!cbConsumo2.isClickable()&&!cbConsumo1.isClickable()){
				// color en el texto del check anterior.
				String txtCon2= "Acepto contratar el seguro de mi\ncrédito con: Seguros Bancomer.";
				String colortxtS= "Seguros Bancomer.";
				int con2= txtCon2.indexOf(colortxtS);
				SpannableString cont2 = new SpannableString(txtCon2);
				cont2.setSpan(new ForegroundColorSpan(Color.rgb(0,158,229)),con2,txtCon2.length(),0);		
				contrato2.setText(cont2);
				//finaliza color
				layout_contrato_seguros.setVisibility(View.GONE);
				layout_aviso_dom.setVisibility(View.VISIBLE);
				txtaviso3.setVisibility(View.VISIBLE);
				cbConsumo3.setClickable(false);
				//cbConsumo4.setClickable(true);
				cbConsumo3.setBackgroundResource(R.drawable.an_ic_check_on);
				}else{
					if(isBuro&& isSeguro)
						showInformationAlert("Debes aceptar contratar el seguro de mi crédito antes.");
					else if(isBuro && !isSeguro)
						showInformationAlert("Debes aceptar consulta de mi historial crediticio antes.");
					else if(isSeguro&& !isBuro)
						showInformationAlert("Debes aceptar contratar el seguro de mi crédito antes.");
					cbConsumo3.setBackgroundResource(R.drawable.an_ic_check_off);
				}
		}else if(sender==cbConsumo4){
			if(!cbConsumo3.isClickable()&&!cbConsumo2.isClickable()&&!cbConsumo1.isClickable()){
					layout_aviso_dom.setVisibility(View.GONE);
					txtaviso3.setVisibility(View.GONE);
					lblmensajecheck4.setVisibility(View.VISIBLE);
					cbConsumo4.setClickable(false);	
					//cbConsumo5.setClickable(true);
					cbConsumo4.setBackgroundResource(R.drawable.an_ic_check_on);
				}else{
					showInformationAlert("Debes aceptar contratar el pago automático de mi crédito antes.");
					cbConsumo4.setBackgroundResource(R.drawable.an_ic_check_off);
				}		
							
		}else if(sender==cbConsumo5){
			if(!cbConsumo4.isClickable() && !cbConsumo3.isClickable()&&!cbConsumo2.isClickable()&&!cbConsumo1.isClickable()){
				cbConsumo5.setClickable(false);
				isOpcionesSelec=true;
				cbConsumo5.setBackgroundResource(R.drawable.an_ic_check_on);
			}else{
				showInformationAlert("Debes aceptar contratar el crédito antes.");
				cbConsumo5.setBackgroundResource(R.drawable.an_ic_check_off);
				}			
		}else if(sender==radioButton1){
				if(radioButton1.isChecked()){
					//layout_aviso_seguros.setVisibility(View.GONE);
					radioButton2.setChecked(false);
				}			
		}else if(sender==radioButton2){
			if(radioButton2.isChecked()){
				//layout_aviso_seguros.setVisibility(View.VISIBLE);
				radioButton1.setChecked(false);
			}
		}
	}
	
	
	OnClickListener clickListener=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(v==btnconfirmar){

				if(Session.getInstance(SuiteApp.appContext).getOfertaDelSimulador()){
					//ARR
					paso1OperacionMap.put("evento_paso2", "event52");
					paso1OperacionMap.put("&&products", "simulador;simulador:contratacion consumo;");
					paso1OperacionMap.put("eVar12", "paso2:acepta contratacion");


				}else{
					//ARR
					paso1OperacionMap.put("evento_paso2", "event47");
					paso1OperacionMap.put("&&products", "promociones;detalle consumo+clausulas consumo");
					paso1OperacionMap.put("eVar12", "paso2:acepta oferta consumo");

				}



				TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
				
				if(isOpcionesSelec){
					if(contratoOfertaConsumoDelegate.enviaPeticionOperacion()){
				if(isAseguradora){		
					if(radioButton1.isChecked()){
						contratoOfertaConsumoDelegate.realizaOperacion(Server.EXITO_OFERTA_CONSUMO, me,1);
					}else{
						contratoOfertaConsumoDelegate.realizaOperacion(Server.EXITO_OFERTA_CONSUMO, me,2);	
						}
					}else{
						contratoOfertaConsumoDelegate.realizaOperacion(Server.EXITO_OFERTA_CONSUMO, me,3);	
						}
					}
				}else{
					showInformationAlert("Es necesario autorizar todas las condiciones para continuar.");
				}
			}else if(v==txtaviso_seguros_link){
				//ARR
				paso1OperacionMap.put("evento_paso2", "event47");
				paso1OperacionMap.put("&&products", "promociones;detalle consumo+clausulas consumo");
				paso1OperacionMap.put("eVar12", "paso2:caracteristicas poliza");
				
				TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
				
				contratoOfertaConsumoDelegate.realizaOperacion(Server.POLIZA_OFERTA_CONSUMO, me,0);
				contratoOfertaConsumoDelegate.showPoliza();
			}else if(v==contrato5){
				//ARR
				paso1OperacionMap.put("evento_paso2", "event47");
				paso1OperacionMap.put("&&products", "promociones;detalle consumo+clausulas consumo");
				paso1OperacionMap.put("eVar12", "paso2:terminos oferta consumo");
				
				TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
				contratoOfertaConsumoDelegate.realizaOperacion(Server.TERMINOS_OFERTA_CONSUMO, me,0);
			}
		}
	};
	
	private void configurarPantalla() {
		
		mostrarContrasena(contratoOfertaConsumoDelegate.mostrarContrasenia());
		mostrarNIP(contratoOfertaConsumoDelegate.mostrarNIP());
		mostrarASM(contratoOfertaConsumoDelegate.tokenAMostrar());
		mostrarCVV(contratoOfertaConsumoDelegate.mostrarCVV());
		mostrarCampoTarjeta(contratoOfertaConsumoDelegate.mostrarCampoTarjeta());
		
		LinearLayout contenedorPadre = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);
		
		if (contenedorContrasena.getVisibility() == View.GONE &&
			contenedorNIP.getVisibility() == View.GONE &&
			contenedorASM.getVisibility() == View.GONE &&
			contenedorCVV.getVisibility() == View.GONE &&
			contenedorCampoTarjeta.getVisibility() == View.GONE) {
			//contenedorPadre.setVisibility(View.GONE);
			
			contenedorPadre.setBackgroundColor(0);
			
			contenedorPadre.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
			/*float camposHeight = contenedorPadre.getMeasuredHeight();
			
			ViewGroup contenido = (ViewGroup)this.findViewById(android.R.id.content).getRootView();//findViewById(android.R.id.content);
			contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
			float contentHeight = contenido.getMeasuredHeight();*/	
		}
		
		btnconfirmar.setOnClickListener(clickListener);
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(lblTextoOferta, true);
		gTools.scale(contrato1, true);
		gTools.scale(contrato2, true);
		gTools.scale(contrato3, true);
		gTools.scale(contrato4, true);
		gTools.scale(contrato5, true);
		gTools.scale(contrato6, true);
		gTools.scale(txtseguros1, true);
		gTools.scale(txtseguros2, true);
		gTools.scale(txtaviso_seguros, true);
		gTools.scale(imgaviso_seguro);
		gTools.scale(lblmensajecheck4, true);
		gTools.scale(cbConsumo1);
		gTools.scale(txtaviso_seguros_link);
		gTools.scale(cbConsumo2);
		gTools.scale(cbConsumo3);
		gTools.scale(cbConsumo4);
		gTools.scale(cbConsumo5);
		gTools.scale(radioButton1);
		gTools.scale(radioButton2);
		gTools.scale(btnconfirmar);
		gTools.scale(layout_aviso_dom);
		gTools.scale(layout_aviso_seguros);
		gTools.scale(layout_avisotoken);
		gTools.scale(layout_contrato_seguros);
		gTools.scale(linearchecKbox1);
		gTools.scale(txtaviso_token,true);
		gTools.scale(imgaviso_token);
		gTools.scale(imgaviso);
		gTools.scale(txtaviso3,true);
		gTools.scale(findViewById(R.id.confirmacion_campos_layout));
		gTools.scale(findViewById(R.id.txtaviso_aviso_dom));
		
		gTools.scale(contenedorContrasena);
		gTools.scale(contenedorNIP);
		gTools.scale(contenedorASM);
		gTools.scale(contenedorCVV);
		
		gTools.scale(contrasena, true);
		gTools.scale(nip, true);
		gTools.scale(asm, true);
		gTools.scale(cvv, true);
		
		gTools.scale(campoContrasena, true);
		gTools.scale(campoNIP, true);
		gTools.scale(campoASM, true);
		gTools.scale(campoCVV, true);
		
		gTools.scale(instruccionesContrasena, true);
		gTools.scale(instruccionesNIP, true);
		gTools.scale(instruccionesASM, true);
		gTools.scale(instruccionesCVV, true);

		gTools.scale(contenedorCampoTarjeta);
		gTools.scale(tarjeta, true);
		gTools.scale(campoTarjeta, true);
		gTools.scale(instruccionesTarjeta, true);
		
	}
	
	private void findViews() {
		contenedorContrasena	= (LinearLayout)findViewById(R.id.campo_confirmacion_contrasena_layout);
		contenedorNIP 			= (LinearLayout)findViewById(R.id.campo_confirmacion_nip_layout);
		contenedorASM 			= (LinearLayout)findViewById(R.id.campo_confirmacion_asm_layout);
		contenedorCVV 			= (LinearLayout)findViewById(R.id.campo_confirmacion_cvv_layout);
		
		contrasena 				= (EditText)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
		nip 					= (EditText)contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
		asm						= (EditText)contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
		cvv						= (EditText)contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);
		
		campoContrasena			= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_label);
		campoNIP				= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_label);
		campoASM				= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_label);
		campoCVV				= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_label);
	
		instruccionesContrasena	= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
		instruccionesNIP		= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
		instruccionesASM		= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
		instruccionesCVV		= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);
		
		contenedorCampoTarjeta  = (LinearLayout)findViewById(R.id.campo_confirmacion_campotarjeta_layout);
		tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
		campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
		instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);
		
		btnconfirmar 		= (ImageButton)findViewById(R.id.confirmacion_confirmar_button);
		
		
	}
	
	public void pideContrasenia() {
		contenedorContrasena.setVisibility(View.GONE);
		//findViewById(R.id.confirmacion_contrasena_layout).setVisibility(View.GONE);
	}
	
	public void pideClaveSeguridad() {
		contenedorASM.setVisibility(View.GONE);
		//findViewById(R.id.confirmacion_asm_layout).setVisibility(View.GONE);
	}
	
	/*
	* 
	*/
	public void mostrarContrasena(boolean visibility){
		contenedorContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		contrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoContrasena.setText(contratoOfertaConsumoDelegate.getEtiquetaCampoContrasenia());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
			contrasena.setFilters(userFilterArray);
			contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		instruccionesContrasena.setVisibility(View.GONE);
	}
	
	/*
	* 
	*/
	public void mostrarNIP(boolean visibility){
		contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		nip.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoNIP.setText(contratoOfertaConsumoDelegate.getEtiquetaCampoNip());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
			nip.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
			String instrucciones = contratoOfertaConsumoDelegate.getTextoAyudaNIP();
			layout_avisotoken.setVisibility(View.GONE);
			if (instrucciones.equals("")) {
				instruccionesNIP.setVisibility(View.GONE);
			} else {
				instruccionesNIP.setVisibility(View.VISIBLE);
				instruccionesNIP.setText(instrucciones);
			}
		} else {
			nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}
	
	/*
	* 
	*/
	public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){
		switch(tipoOTP) {
			case ninguno:
				contenedorASM.setVisibility(View.GONE);
				campoASM.setVisibility(View.GONE);
				asm.setVisibility(View.GONE);
				asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
				break;
			case codigo:
			case registro:
				contenedorASM.setVisibility(View.VISIBLE);
				campoASM.setVisibility(View.VISIBLE);
				asm.setVisibility(View.VISIBLE);
				InputFilter[] userFilterArray = new InputFilter[1];
				userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);	
				asm.setFilters(userFilterArray);
				cambiarAccionTexto(contrasena);
				cambiarAccionTexto(tarjeta);
				cambiarAccionTexto(nip);
				asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
		}
		
		Constants.TipoInstrumento tipoInstrumento = contratoOfertaConsumoDelegate.consultaTipoInstrumentoSeguridad();


		switch (tipoInstrumento) {
			case OCRA:
				campoASM.setText(contratoOfertaConsumoDelegate.getEtiquetaCampoOCRA());
				//asm.setTransformationMethod(null);
				break;
			case DP270:
				campoASM.setText(contratoOfertaConsumoDelegate.getEtiquetaCampoDP270());
				//asm.setTransformationMethod(null);
				break;
			case SoftToken:
				if(SuiteApp.getSofttokenStatus()) {
					asm.setText(Constants.DUMMY_OTP);
					asm.setEnabled(false);
					campoASM.setText(contratoOfertaConsumoDelegate.getEtiquetaCampoSoftokenActivado());
				} else {
					asm.setText("");
					asm.setEnabled(true);
					campoASM.setText(contratoOfertaConsumoDelegate.getEtiquetaCampoSoftokenDesactivado());
					//asm.setTransformationMethod(null);
				}

				break;
			default:
				break;
		}
		String instrucciones = contratoOfertaConsumoDelegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);

		if (instrucciones.equals("")) {
			instruccionesASM.setVisibility(View.GONE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}	
	}
		
	private void mostrarCampoTarjeta(boolean visibility) {
		contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		tarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoTarjeta.setText(contratoOfertaConsumoDelegate.getEtiquetaCampoTarjeta());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(5);
			tarjeta.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
			String instrucciones = contratoOfertaConsumoDelegate.getTextoAyudaTarjeta();
			if (instrucciones.equals("")) {
				instruccionesTarjeta.setVisibility(View.GONE);
			} else {
				instruccionesTarjeta.setVisibility(View.VISIBLE);
				instruccionesTarjeta.setText(instrucciones);
			}
		} else {
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		
	}

	
	private void cambiarAccionTexto(EditText campo) {
		if (campo.getVisibility() == View.VISIBLE) {
			campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
	}
	public String pideContrasena() {
		if (contrasena.getVisibility() == View.GONE) {
			return "";
		} else {
			return contrasena.getText().toString();
		}
	}
	
	public String pideNIP() {
		if (nip.getVisibility() == View.GONE) {
			return "";
		} else {
			return nip.getText().toString();
		}
	}
	
	public String pideASM() {
		if (asm.getVisibility() == View.GONE) {
			return "";
		} else {
			return asm.getText().toString();
		}
	}
	
	public String pideCVV() {
		if (cvv.getVisibility() == View.GONE) {
			return "";
		} else {
			return cvv.getText().toString();
		}
	}
	
	/*
	* 
	*/
	public void mostrarCVV(boolean visibility){
		contenedorCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		cvv.setVisibility(visibility ? View.VISIBLE:View.GONE);
		instruccionesCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		
		if (visibility) {
			campoCVV.setText(contratoOfertaConsumoDelegate.getEtiquetaCampoCVV());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
			cvv.setFilters(userFilterArray);
			String instrucciones = contratoOfertaConsumoDelegate.getTextoAyudaCVV();
			instruccionesCVV.setText(instrucciones);
			instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(nip);
			cambiarAccionTexto(asm);
			cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}
	
	public void limpiarCampos(){
		contrasena.setText("");
		nip.setText("");
		asm.setText("");
		cvv.setText("");
		tarjeta.setText("");
	}


	public String pideTarjeta(){
		if (tarjeta.getVisibility() == View.GONE) {
			return "";
		}else 
			return tarjeta.getText().toString();		
	}
	
	
	public void goBack() {
		//ARR
				paso1OperacionMap.put("evento_paso2", "event47");
				paso1OperacionMap.put("&&products", "promociones;detalle consumo+clausulas consumo");
				paso1OperacionMap.put("eVar12", "paso2:rechazo oferta consumo");

				TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
				
		// TODO Auto-generated method stub
		//super.goBack();
		contratoOfertaConsumoDelegate.confirmarRechazoatras();

		Session session = Session.getInstance(SuiteApp.appContext);
		session.setImporteMinimo("");
		session.setImporteParcial("");
		session.setImporteInicial("");
		session.setChanged(false);

	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);

	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();

	}
	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		contratoOfertaConsumoDelegate.analyzeResponse(operationId, response);
	}
	
}
