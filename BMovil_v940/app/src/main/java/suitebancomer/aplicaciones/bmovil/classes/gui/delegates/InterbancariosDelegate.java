package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.math.BigDecimal;




import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Hashtable;

import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerMapperUtil;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
//import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.CambioTelefonoViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionRegistroViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultarFrecuentesViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.InterbancariosViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.RegistrarOperacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.CambioTelefono;
import suitebancomer.aplicaciones.bmovil.classes.model.Catalog;
import suitebancomer.aplicaciones.bmovil.classes.model.Comision;
import suitebancomer.aplicaciones.bmovil.classes.model.FrecuenteMulticanalResult;
import suitebancomer.aplicaciones.bmovil.classes.model.Payment;
import suitebancomer.aplicaciones.bmovil.classes.model.PaymentExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.RegistrarOperacionResult;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaInterbancaria;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferResult;
import suitebancomer.classes.gui.controllers.BaseViewController;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.text.InputFilter;
import android.util.Log;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.TransferViewController;
//SPEI
import android.widget.TextView;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelper;

public class InterbancariosDelegate extends DelegateBaseAutenticacion {

	private Constants.Operacion tipoOperacion;
	public final static long INTERBANCARIOS_DELEGATE_ID = 0xcea434c61ca109b2L;
	private InterbancariosViewController interbancariosViewController;
	private ConsultarFrecuentesViewController FrecuentesViewController;
	private TransferenciaInterbancaria transferenciaInterbancaria;
	private TransferResult result;//interbancariosResult
	private boolean esFrecuente;
	private BaseViewController frecuenteViewController;
	private Payment[] frecuentes;
	private boolean bajaFrecuente;
	private Payment selectedPayment;
	protected SolicitarAlertasData sa;
	//protected String tempPassword;
	
	public boolean getEsFrecuente() {
		return esFrecuente;
	}
	
	public void setEsFrecuente(boolean esFrecuente){
		this.esFrecuente = esFrecuente;
	}

	int longitudNumeroCuenta;
	Comision comision;
	
	public InterbancariosDelegate() {
		longitudNumeroCuenta = 0;
	}
	
	public InterbancariosDelegate(boolean esFrecuente) {
		this.esFrecuente = esFrecuente;
		this.bajaFrecuente = false;
		if(esFrecuente)
			tipoOperacion = Constants.Operacion.transferirInterbancariaF;
	}

	public TransferenciaInterbancaria getTransferenciaInterbancaria() {
		return transferenciaInterbancaria;
	}

	public void setTransferenciaInterbancaria(TransferenciaInterbancaria transferenciaInterbancaria) {
		this.transferenciaInterbancaria = transferenciaInterbancaria;
		//frecuentes correo electronico
		this.aliasFrecuente = null;
		this.correoFrecuente = null;
	}


	public boolean isBajaFrecuente() {
		return bajaFrecuente;
	}

	public void setBajaFrecuente(boolean bajaFrecuente) {
		this.bajaFrecuente = bajaFrecuente;
	}

	@Override
	public int getOpcionesMenuResultados() {
		/*if(esFrecuente)
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF | SHOW_MENU_RAPIDA  ;
		else if (bajaFrecuente) {
			return super.getOpcionesMenuResultados();
		}
		else 
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF | SHOW_MENU_FRECUENTE | SHOW_MENU_RAPIDA ;*/
		if(esFrecuente){
			if(transferenciaInterbancaria.getCuentaOrigen().getType().equalsIgnoreCase(Constants.EXPRESS_TYPE)){
				return SHOW_MENU_SMS | SHOW_MENU_PDF | SHOW_MENU_RAPIDA  ;
			}else{
				return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF | SHOW_MENU_RAPIDA  ;
			}
		}else if (bajaFrecuente) {
			return super.getOpcionesMenuResultados();
		}
		else {
			if(transferenciaInterbancaria.getCuentaOrigen().getType().equalsIgnoreCase(Constants.EXPRESS_TYPE)){
				return SHOW_MENU_SMS | SHOW_MENU_PDF | SHOW_MENU_RAPIDA| SHOW_MENU_FRECUENTE  ;
			}else{
				return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF | SHOW_MENU_RAPIDA| SHOW_MENU_FRECUENTE   ;
			}
		}
	}

	@Override
	public String getTextoSMS() {
	
		String folio = result.getFolio();
		String importe =Tools.formatAmount( transferenciaInterbancaria.getImporte(),false);
		String fecha = Tools.formatDate(result.getFecha());
		String hora = Tools.formatTime(result.getHora());

		StringBuilder msg = new StringBuilder();
		msg.append(SuiteApp.appContext.getString(R.string.transferir_interbancario_texto_titulo_sms));
		msg.append(SuiteApp.appContext.getString(R.string.smsText_firstFrom) + " " + Tools.hideAccountNumber(transferenciaInterbancaria.getCuentaOrigen().getNumber()));
		msg.append(" "+SuiteApp.appContext.getString(R.string.smsText_a) + " "+Tools.hideAccountNumber( transferenciaInterbancaria.getNumeroCuentaDestino())+" ");
		msg.append(SuiteApp.appContext.getString(R.string.smsText_secondFrom) +" " +transferenciaInterbancaria.getBeneficiario().substring(0, transferenciaInterbancaria.getBeneficiario().length()>10?10:transferenciaInterbancaria.getBeneficiario().length()));
		msg.append(" "+SuiteApp.appContext.getString(R.string.smsText_byQuantity) +" " +importe+" "+SuiteApp.appContext.getString(R.string.smsText_folio) +" " +folio +" "+fecha +" "+hora);
		String msgText = msg.toString().replaceAll("\n", "");
//		System.out.println(msgText);
		return msgText;
	}

	@Override
	public String getTextoEmail() {
		// TODO Auto-generated method stub
		//El correo se compondrá del contenido del componente �ListaDatosViewController�
		//mostrado en la pantalla de resultados.
		return super.getTextoEmail();
	}

	@Override
	public String getTextoPDF() {
		// TODO Auto-generated method stub
		//El PDF se compondrá del contenido del componente �ListaDatosViewController� 
		//mostrado en la pantalla de resultados
		return super.getTextoPDF();
	}

	@Override
	public String getTextoPantallaResultados() {
		// TODO Auto-generated method stub
		return "";
		//return interbancariosViewController.getString(R.string.transferir_interbancario_texto_titulo_resultados);
	}

	@Override
	public String getTextoTituloResultado() {
		if (bajaFrecuente) {
			return frecuenteViewController.getString(R.string.baja_frecuentes_titulo_tabla_resultados);
		} else {
			return interbancariosViewController.getString(R.string.transferir_interbancario_texto_titulo_resultados);
		}
	}

	@Override
	public String getTextoTituloConfirmacion() {
		
		return interbancariosViewController.getString(R.string.confirmation_subtitulo);
	}

	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
//		Resources res = SuiteApp.appContext.getResources();
		ArrayList<Object> datosConfirmacion = new ArrayList<Object>();
		ArrayList<String> registro;
		
		if (bajaFrecuente) {
			registro = new ArrayList<String>();
			registro.add(frecuenteViewController.getString(R.string.altafrecuente_nombrecorto));
			registro.add(transferenciaInterbancaria.getAliasFrecuente());
			datosConfirmacion.add(registro);
		
			registro = new ArrayList<String>();
			registro.add(frecuenteViewController.getString(R.string.transferir_interbancario_tabla_resultados_banco));//transferir.interbancario.banco.destino
			registro.add(transferenciaInterbancaria.getNombreBanco());
			datosConfirmacion.add(registro);
			
			//SPEI
			String accountLabel = Constants.EMPTY_STRING;
			if(transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.PHONE_TYPE_ACCOUNT))
				accountLabel = SuiteApp.appContext.getString(R.string.bmovil_detalle_asociar_cuenta_telefono_label_phone);
			else
				accountLabel = SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta);
			//Termina SPEI
		
			registro = new ArrayList<String>();
			//registro.add(frecuenteViewController.getString(R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta));//transferir.interbancario.numero.tarjeta.cuenta
			//registro.add(transferenciaInterbancaria.getNumeroCuentaDestino());
			//datosConfirmacion.add(registro);
			registro.add(accountLabel);//transferir.interbancario.numero.tarjeta.cuenta
			registro.add(transferenciaInterbancaria.getNumeroCuentaDestino());
			datosConfirmacion.add(registro);
			//Termina SPEI
		
			registro = new ArrayList<String>();
			registro.add(frecuenteViewController.getString(R.string.common_beneficiaryLabel));//transferir.interbancario.numero.tarjeta.cuenta
			registro.add(transferenciaInterbancaria.getBeneficiario());
			datosConfirmacion.add(registro);
		
			registro = new ArrayList<String>();
			registro.add(frecuenteViewController.getString(R.string.transferir_interbancario_referencia));
			registro.add(transferenciaInterbancaria.getReferencia());
			datosConfirmacion.add(registro);
		
			registro = new ArrayList<String>();
			registro.add(frecuenteViewController.getString(R.string.transferir_interbancario_concepto));
			registro.add(transferenciaInterbancaria.getConcepto());
			datosConfirmacion.add(registro);
			
			ArrayList<Object> entryList = datosConfirmacion;
			for(int i = entryList.size() - 1; i >= 0; i--) {
				try {
					@SuppressWarnings("unchecked")
					ArrayList<String> entry = (ArrayList<String>)entryList.get(i);
					if(entry.get(1).trim().equals(Constants.EMPTY_STRING)) 
						entryList.remove(i);
				} catch(Exception ex) {
					if(Server.ALLOW_LOG) Log.i(getClass().getSimpleName(), "Error al quitar el elemento.", ex);
				}
			}
		} else {
			//SPEI
			String accountTitle = "";
			if(transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.PHONE_TYPE_ACCOUNT))
				accountTitle = SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_numero_celular);
			else
				accountTitle = SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta);

			registro = new ArrayList<String>();
			
			/*registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
			registro.add(Tools.hideAccountNumber(transferenciaInterbancaria.getCuentaOrigen().getNumber()));
			datosConfirmacion.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_banco_destino));//transferir.interbancario.banco.destino
			registro.add(transferenciaInterbancaria.getNombreBanco());
			datosConfirmacion.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta));//transferir.interbancario.numero.tarjeta.cuenta
			registro.add(transferenciaInterbancaria.getNumeroCuentaDestino());
			datosConfirmacion.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_beneficiario));
			registro.add(transferenciaInterbancaria.getBeneficiario());
			datosConfirmacion.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_importe));
			registro.add(Tools.formatAmount(transferenciaInterbancaria.getImporte(),false));
			datosConfirmacion.add(registro);*/
			
			//SPEI
			if(!transferenciaInterbancaria.getCuentaOrigen().getCelularAsociado().equals(""))
			{
				registro = new ArrayList<String>();
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_numero_celular));
				registro.add(transferenciaInterbancaria.getCuentaOrigen().getCelularAsociado());
				datosConfirmacion.add(registro);
			}
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
			registro.add(Tools.hideAccountNumber(transferenciaInterbancaria.getCuentaOrigen().getNumber()));
			datosConfirmacion.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_banco_destino));//transferir.interbancario.banco.destino
			registro.add(transferenciaInterbancaria.getNombreBanco());
			datosConfirmacion.add(registro);
			registro = new ArrayList<String>();
			registro.add(accountTitle);//transferir.interbancario.numero.tarjeta.cuenta
			registro.add(transferenciaInterbancaria.getNumeroCuentaDestino());
			datosConfirmacion.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_beneficiario));
			registro.add(transferenciaInterbancaria.getBeneficiario());
			datosConfirmacion.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_importe));
			registro.add(Tools.formatAmount(transferenciaInterbancaria.getImporte(),false));
			datosConfirmacion.add(registro);
			//Termina SPEI
			if(transferenciaInterbancaria.getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE) && transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.CREDIT_TYPE)){
	
				if(!Tools.isEmptyOrNull(comision.getComision()))
				{
					registro = new ArrayList<String>();
					registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_comision));
					registro.add(Tools.formatAmount(comision.getComision(),false));
					datosConfirmacion.add(registro);
				}
			}
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_referencia));
			registro.add(transferenciaInterbancaria.getReferencia());
			datosConfirmacion.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_concepto));
			registro.add(transferenciaInterbancaria.getConcepto());
			datosConfirmacion.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_fechaoperacion));
			java.util.Date currentDate = Session.getInstance(SuiteApp.appContext).getServerDate();
			registro.add(Tools.dateToString(currentDate));
			datosConfirmacion.add(registro);
			
		}
		
		return datosConfirmacion;
	}

	@Override
	public ArrayList<Object> getDatosTablaResultados() {
//		Resources res = SuiteApp.appContext.getResources();
		ArrayList<Object> datosResultados = new ArrayList<Object>();
		ArrayList<String> registro;
		
		if (bajaFrecuente) {
			
			registro = new ArrayList<String>();
			registro.add(frecuenteViewController.getString(R.string.altafrecuente_nombrecorto));
			registro.add(transferenciaInterbancaria.getAliasFrecuente());
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(frecuenteViewController.getString(R.string.transferir_interbancario_tabla_resultados_banco));//transferir.interbancario.banco.destino
			registro.add(transferenciaInterbancaria.getNombreBanco());
			datosResultados.add(registro);
			
			//SPEI
			String accountLabel = Constants.EMPTY_STRING;
			if(transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.PHONE_TYPE_ACCOUNT))
				accountLabel = SuiteApp.appContext.getString(R.string.bmovil_detalle_asociar_cuenta_telefono_label_phone);
			else
				accountLabel = SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta);
			//TEermina SPEI
			
			registro = new ArrayList<String>();
			/*registro.add(frecuenteViewController.getString(R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta));//transferir.interbancario.numero.tarjeta.cuenta
			registro.add(transferenciaInterbancaria.getNumeroCuentaDestino());
			datosResultados.add(registro);*/
			//SPEI
			registro.add(accountLabel);//transferir.interbancario.numero.tarjeta.cuenta
			registro.add(transferenciaInterbancaria.getNumeroCuentaDestino());
			datosResultados.add(registro);
			//Termina SPEI

			registro = new ArrayList<String>();
			registro.add(frecuenteViewController.getString(R.string.common_beneficiaryLabel));//transferir.interbancario.numero.tarjeta.cuenta
			registro.add(transferenciaInterbancaria.getBeneficiario());
			datosResultados.add(registro);

			registro = new ArrayList<String>();
			registro.add(frecuenteViewController.getString(R.string.transferir_interbancario_referencia));
			registro.add(transferenciaInterbancaria.getReferencia());
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(frecuenteViewController.getString(R.string.transferir_interbancario_concepto));
			registro.add(transferenciaInterbancaria.getConcepto());
			datosResultados.add(registro);

		} else {
			//SPEI
			String accountTitle = "";
			if(transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.PHONE_TYPE_ACCOUNT))
				accountTitle = SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_numero_celular);
			else
				accountTitle = SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta);

			//Termina SPEI
				/*registro = new ArrayList<String>();
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
				registro.add(Tools.hideAccountNumber(transferenciaInterbancaria.getCuentaOrigen().getNumber()));
				datosResultados.add(registro);
				registro = new ArrayList<String>();
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_banco));
				registro.add(transferenciaInterbancaria.getNombreBanco());
				datosResultados.add(registro);
				registro = new ArrayList<String>();
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_cuentadeposito));
				registro.add(transferenciaInterbancaria.getNumeroCuentaDestino());
				datosResultados.add(registro);
				registro = new ArrayList<String>();
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_beneficiario));
				registro.add(transferenciaInterbancaria.getBeneficiario());
				datosResultados.add(registro);
				registro = new ArrayList<String>();
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_importe));
				registro.add(Tools.formatAmount(transferenciaInterbancaria.getImporte(),false));
				datosResultados.add(registro);
				if(transferenciaInterbancaria.getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE) && transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.CREDIT_TYPE))
				{
					if(!Tools.isEmptyOrNull(result.getComision()))
					{ 
						registro = new ArrayList<String>();
						registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_comision));
						registro.add(Tools.formatAmount(result.getComision(),false));
						datosResultados.add(registro);
					}
				}
				registro = new ArrayList<String>();
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_referencia));
				registro.add(transferenciaInterbancaria.getReferencia());
				datosResultados.add(registro);
				registro = new ArrayList<String>();
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_concepto));
				registro.add(transferenciaInterbancaria.getConcepto());
				datosResultados.add(registro);
				registro = new ArrayList<String>();
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_fechaoperacion));
				registro.add(Tools.formatDate(result.getFecha()));
				datosResultados.add(registro);
				registro = new ArrayList<String>();
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_horaoperacion));
				registro.add(Tools.formatTime(result.getHora()));
				datosResultados.add(registro);
				registro = new ArrayList<String>();
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_folio));
				registro.add(result.getFolio());//agregar format day
				datosResultados.add(registro);
				*/
			//SPEI
			/*if(!transferenciaInterbancaria.getCuentaOrigen().getCelularAsociado().equals(""))
			{
				registro = new ArrayList<String>();
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_numero_celular));
				registro.add(transferenciaInterbancaria.getCuentaOrigen().getCelularAsociado());
				datosResultados.add(registro);
			}
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
			registro.add(Tools.hideAccountNumber(transferenciaInterbancaria.getCuentaOrigen().getNumber()));
			datosResultados.add(registro);
			if(transferenciaInterbancaria.getCuentaOrigen().getCelularAsociado().equals(""))*/
			registro = new ArrayList<String>();
			if(transferenciaInterbancaria.getCuentaOrigen().getCelularAsociado().equals(""))
			{
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
				registro.add(Tools.hideAccountNumber(transferenciaInterbancaria.getCuentaOrigen().getNumber()));
			}
			else
			{
				registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_numero_celular));
				registro.add(transferenciaInterbancaria.getCuentaOrigen().getCelularAsociado());
			}
			datosResultados.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_banco));
			registro.add(transferenciaInterbancaria.getNombreBanco());
			datosResultados.add(registro);
			registro = new ArrayList<String>();
			registro.add(accountTitle);
			registro.add(transferenciaInterbancaria.getNumeroCuentaDestino());
			datosResultados.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_beneficiario));
			registro.add(transferenciaInterbancaria.getBeneficiario());
			datosResultados.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_importe));
			registro.add(Tools.formatAmount(transferenciaInterbancaria.getImporte(),false));
			datosResultados.add(registro);
			if(transferenciaInterbancaria.getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE) && transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.CREDIT_TYPE))
			{
				if(!Tools.isEmptyOrNull(result.getComision()))
				{ 
					registro = new ArrayList<String>();
					registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_comision));
					registro.add(Tools.formatAmount(result.getComision(),false));
					datosResultados.add(registro);
				}
			}
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_referencia));
			registro.add(transferenciaInterbancaria.getReferencia());
			datosResultados.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_concepto));
			registro.add(transferenciaInterbancaria.getConcepto());
			datosResultados.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_fechaoperacion));
			registro.add(Tools.formatDate(result.getFecha()));
			datosResultados.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_horaoperacion));
			registro.add(Tools.formatTime(result.getHora()));
			datosResultados.add(registro);
			registro = new ArrayList<String>();
			registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_folio));
			registro.add(result.getFolio());//agregar format day
			datosResultados.add(registro);
			//Termina SPEI
			//aliasFrecuente = transferenciaInterbancaria.getAliasFrecuente() != null && !transferenciaInterbancaria.getAliasFrecuente().isEmpty() ? transferenciaInterbancaria.getAliasFrecuente() : aliasFrecuente;
			if(aliasFrecuente != null && !aliasFrecuente.isEmpty()){
				registro = new ArrayList<String>();
				registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_alias_frecuente));
				registro.add(aliasFrecuente);
				datosResultados.add(registro);
			}

			//correo frecuente
			//correoFrecuente = transferenciaInterbancaria.getCorreoFrecuente() != null && !transferenciaInterbancaria.getCorreoFrecuente().isEmpty() ? transferenciaInterbancaria.getCorreoFrecuente() : correoFrecuente;
			if(correoFrecuente !=null && !correoFrecuente.isEmpty()){
				registro = new ArrayList<String>();
				registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_correo_electronico_frecuente));
				registro.add(correoFrecuente);
				datosResultados.add(registro);
			}
			correoFrecuente = transferenciaInterbancaria.getCorreoFrecuente() != null && !transferenciaInterbancaria.getCorreoFrecuente().isEmpty() ? transferenciaInterbancaria.getCorreoFrecuente() : correoFrecuente;

		}
		
		return datosResultados;
	}
	
	public ArrayList<Object> getDatosTablaClave() {
//		Resources res = SuiteApp.appContext.getResources();
		if(!bajaFrecuente){
			ArrayList<Object> datosResultados = new ArrayList<Object>();
			ArrayList<String> registro;
			
			registro = new ArrayList<String>();
			registro.add("");
			registro.add(interbancariosViewController.getString(R.string.bmovil_consultar_interbancario_clave_rastreo));
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add("");
			registro.add(Tools.formatCR(result.getClave()));
			datosResultados.add(registro);
			
			
			return datosResultados;
		}
		return null;
	}

	@Override
	public boolean mostrarContrasenia() {
		
		boolean value =  bajaFrecuente ? Autenticacion.getInstance().mostrarContrasena(this.tipoOperacion, 
				Session.getInstance(SuiteApp.appContext).getClientProfile()) : Autenticacion.getInstance().mostrarContrasena(this.tipoOperacion, 
				Session.getInstance(SuiteApp.appContext).getClientProfile(),
				Tools.getDoubleAmountFromServerString(transferenciaInterbancaria.getImporte()));
		return value;
	}

	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = bajaFrecuente ? Autenticacion.getInstance().tokenAMostrar(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile()) : Autenticacion.getInstance().tokenAMostrar(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile(),
					Tools.getDoubleAmountFromServerString(transferenciaInterbancaria.getImporte()));
		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.mostrarNIP execution.", ex);
			tipoOTP = null;
		}
		
		return tipoOTP;
	}

	@Override
	public boolean mostrarNIP() {
		boolean value =  bajaFrecuente ? Autenticacion.getInstance().mostrarNIP(this.tipoOperacion, 
				Session.getInstance(SuiteApp.appContext).getClientProfile()) : Autenticacion.getInstance().mostrarNIP(this.tipoOperacion, 
				Session.getInstance(SuiteApp.appContext).getClientProfile(),
				Tools.getDoubleAmountFromServerString(transferenciaInterbancaria.getImporte()));
	
		return value;
	}
	
	@Override
	public boolean mostrarCVV() {	
		double importe = Tools.getDoubleAmountFromServerString(transferenciaInterbancaria.getImporte());
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile(); 
		boolean value =  bajaFrecuente ? Autenticacion.getInstance().mostrarCVV(tipoOperacion, perfil) : Autenticacion.getInstance().mostrarCVV(tipoOperacion, perfil, importe);
		return value;
	}

	@Override
	public ArrayList<Object> getDatosTablaAltaFrecuentes() {
		ArrayList<Object> datosAltaFrecuente = new ArrayList<Object>();
		ArrayList<String> registro;
		//SPEI
		String accountTitle = "";
		String accountValue = "";
		if(transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.PHONE_TYPE_ACCOUNT)) {
				accountTitle = SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_numero_celular);
				accountValue = transferenciaInterbancaria.getNumeroCuentaDestino();
			} else {
				accountTitle = SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_cuentadeposito);
				accountValue = Tools.hideAccountNumber(transferenciaInterbancaria.getNumeroCuentaDestino());
			}
		//Termina SPEI
		//cuenta de retiro
		registro = new ArrayList<String>();
		registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
		registro.add(Tools.hideAccountNumber(transferenciaInterbancaria.getCuentaOrigen().getNumber()));
		datosAltaFrecuente.add(registro);
		//institucion bancaria
		registro = new ArrayList<String>();
		registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_banco));
		registro.add(transferenciaInterbancaria.getNombreBanco());
		datosAltaFrecuente.add(registro);
		//cuenta deposito
		registro = new ArrayList<String>();
		//registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_cuentadeposito));
		//registro.add(Tools.hideAccountNumber(transferenciaInterbancaria.getNumeroCuentaDestino()));
		registro.add(accountTitle);//SPEI
		registro.add(accountValue);//SPEI
		datosAltaFrecuente.add(registro);
		//beneficiario
		registro = new ArrayList<String>();
		registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_beneficiario));
		registro.add(transferenciaInterbancaria.getBeneficiario());
		datosAltaFrecuente.add(registro);		
		//referencia
		registro = new ArrayList<String>();
		registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_referencia));
		registro.add(transferenciaInterbancaria.getReferencia());
		datosAltaFrecuente.add(registro);
		//concepto
		registro = new ArrayList<String>();
		registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_concepto));
		registro.add(transferenciaInterbancaria.getConcepto());
		datosAltaFrecuente.add(registro);
		registro = new ArrayList<String>();
		registro.add(interbancariosViewController.getString(R.string.transferir_interbancario_tabla_resultados_fechaoperacion));
		registro.add(Tools.formatDate(result.getFecha()));
		datosAltaFrecuente.add(registro);
		return datosAltaFrecuente;
	}

	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.bmovil_transferir_icono;
	}

	@Override
	public int getTextoEncabezado() {
		return R.string.opcionesTransfer_menu_otrosbancos;
	}

	@Override
	public String getTextoBotonOperacionNueva() {
		return interbancariosViewController.getString(R.string.transferir_interbancario_texto_boton_operacion_nueva);
	}

	@Override
	public void realizaOperacion(ConfirmacionViewController confirmacionViewController,	String contrasenia, String nip, String token, String campoTarjeta) {
		//prepare data
        Hashtable<String,String> paramTable = null;

        int operacion = 0;
     	Session session = Session.getInstance(SuiteApp.appContext);
     	
     	if (bajaFrecuente) {
			
    		operacion = Server.BAJA_FRECUENTE;
			//completar hashtable
			paramTable.put(Server.USERNAME_PARAM, session.getUsername());//"NT"
			paramTable.put(Server.PASSWORD_PARAM, Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia );//NP
			paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());//IU
			paramTable.put(Server.NUMERO_CLIENTE_PARAM, session.getClientNumber());//TE
			paramTable.put(Server.DESCRIPCION_PARAM, transferenciaInterbancaria.getAliasFrecuente());//DE
			paramTable.put(Server.PAYMENT_ACCOUNT_PARAM, transferenciaInterbancaria.getTipoCuentaDestino() + transferenciaInterbancaria.getNumeroCuentaDestino());//CA
//			paramTable.put(Server.BENEFICIARIO_PARAM, compraTiempoAire.getNombreCompania());//BF
//			paramTable.put(Server.REFERENCIA_NUMERICA_PARAM, compraTiempoAire.getCelularDestino());//RF
			paramTable.put(Server.TIPO_CONSULTA_PARAM, Constants.TPTransferInterbancarias);//TP
			String va =  Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile());
			paramTable.put(Server.VA_PARAM,Tools.isEmptyOrNull(va)?"":va);//validar

			Hashtable<String,String> paramTable2=new Hashtable<String, String>();
			paramTable2=ServerMapperUtil.mapperBaja(paramTable);
			//JAIG CHECK
			doNetworkOperation(operacion, paramTable2, false, null, isJsonValueCode.NONE, confirmacionViewController);

		} else {
			
			operacion = Server.EXTERNAL_TRANSFER_OPERATION;
			String tipoCuentaDestino = transferenciaInterbancaria.getTipoCuentaDestino();
			//completar hashtable
			paramTable = new Hashtable<String, String>();//generaParametrosParaCuentas(transferenciaInterbancaria.getCuentaOrigen().getType(),
													 //transferenciaInterbancaria.getTipoCuentaDestino());
			

			//paramTable.put(Server.CARD_TYPE_PARAM, Constants.CREDIT_TYPE);
			paramTable.put(ServerConstants.ID_OPERACION, ServerConstants.TRANSFERENCIA_INTERBANCARIA);
			paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());//"NT"
			paramTable.put(ServerConstants.PARAMS_TEXTO_NP,  Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia );//NP
			paramTable.put(ServerConstants.PARAMS_TEXTO_TE, session.getClientNumber());//TE
			paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());//IU
			if(tipoCuentaDestino.equals(Constants.CREDIT_TYPE)&&transferenciaInterbancaria.getCuentaOrigen().getCelularAsociado().length()>0)
				paramTable.put("CC", transferenciaInterbancaria.getCuentaOrigen().getFullNumber());//CC
			else if(transferenciaInterbancaria.getCuentaOrigen().getCelularAsociado().length()>0)
				paramTable.put("CC", "SM"+transferenciaInterbancaria.getCuentaOrigen().getCelularAsociado());//CC
			else
				paramTable.put("CC", transferenciaInterbancaria.getCuentaOrigen().getFullNumber());//CC
			paramTable.put("CA", transferenciaInterbancaria.getNumeroCuentaDestino());//CA
			paramTable.put("IM", transferenciaInterbancaria.getImporte()==null?"": transferenciaInterbancaria.getImporte());//IM
			paramTable.put("CB", transferenciaInterbancaria.getInstitucionBancaria());//CB
			paramTable.put("MP", Tools.removeSpecialCharacters(transferenciaInterbancaria.getConcepto()==null?"": transferenciaInterbancaria.getConcepto()));//MP
			paramTable.put("BF", Tools.removeSpecialCharacters(transferenciaInterbancaria.getBeneficiario()==null?"": transferenciaInterbancaria.getBeneficiario()));//BF
			paramTable.put("RF",transferenciaInterbancaria.getReferencia());//RF
			paramTable.put("NI", "");
			paramTable.put("CV", "");
			paramTable.put("OT", Tools.isEmptyOrNull(token)?"":token);
			String va =  Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile(),
					Tools.getDoubleAmountFromServerString(transferenciaInterbancaria.getImporte()));
			paramTable.put("VA",Tools.isEmptyOrNull(va)?"":va);//validar

			
				if(tipoCuentaDestino.equals(Constants.CREDIT_TYPE)){
					paramTable.put("TP", Constants.TP_TC_VALUE);//TP
				}else if(tipoCuentaDestino.equals(Constants.CLABE_TYPE_ACCOUNT)){
					paramTable.put("TP", Constants.CLABE_TYPE_ACCOUNT);//TP
				}//SPEI
				else if(tipoCuentaDestino.equals(Constants.PHONE_TYPE_ACCOUNT)){
					//paramTable.put("TP", Constants.PHONE_TYPE_ACCOUNT);//PH
					paramTable.put("TP", "TL");//PH
				}//TERMINA SPEI
				else{
					paramTable.put("TP", Constants.TP_TD_VALUE);//TP
				}
				//JAIG
				doNetworkOperation(operacion, paramTable, false, new TransferResult(), isJsonValueCode.NONE, confirmacionViewController);
		}
     	

	}

	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if(interbancariosViewController != null)
			((BmovilViewsController)interbancariosViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
		else if(frecuenteViewController != null)
		  ((BmovilViewsController)frecuenteViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
			if(operationId == Server.OP_VALIDAR_CREDENCIALES){
				showConfirmacionRegistro();
				return;
			}
			
			if(operationId == Server.OP_SOLICITAR_ALERTAS){
				
				setSa((SolicitarAlertasData) response.getResponse());
				analyzeAlertasRecortado();
				return;
			}
			
			if (response.getResponse() instanceof TransferResult) {
				result = (TransferResult) response.getResponse();
				if ( esFrecuente && (tokenAMostrar() == Constants.TipoOtpAutenticacion.codigo 
						|| tokenAMostrar() == Constants.TipoOtpAutenticacion.registro)
						&& Tools.isEmptyOrNull(transferenciaInterbancaria.getFrecuenteMulticanal())) {
					actualizaFrecuenteAMulticanal();
				} else {
					transferenciaExitosa();
				}
			}else if (response.getResponse() instanceof Comision){
				comision = (Comision) response.getResponse();
				showConfirmacion();
			}else if(response.getResponse() instanceof PaymentExtract){
				PaymentExtract extract = (PaymentExtract) response.getResponse();
				frecuentes = extract.getPayments();
				mostrarListaFrecuentes();
			}else if(response.getResponse() instanceof FrecuenteMulticanalResult){
				transferenciaExitosa();
			}else {
				((BmovilViewsController)frecuenteViewController.getParentViewsController()).showResultadosViewController(this, -1, -1);
			}
			
		}else if(response.getStatus() == ServerResponse.OPERATION_WARNING){
			// warning o error??
			
			if(interbancariosViewController != null)
			((BmovilViewsController)interbancariosViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
			else if(frecuenteViewController != null)
				((BmovilViewsController)frecuenteViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
			else{
				BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
				BmovilViewsController bmvc = bmApp.getBmovilViewsController(); 
				BaseViewController current = bmvc.getCurrentViewControllerApp();
				current.showInformationAlert(response.getMessageText());
			}
		} else if (response.getStatus() == ServerResponse.OPERATION_ERROR){
			Session session = Session.getInstance(SuiteApp.appContext);
			Constants.Perfil perfil = session.getClientProfile();
			if(Constants.Perfil.recortado.equals(perfil)){
			
					setTransferErrorResponse(response);
					solicitarAlertas(interbancariosViewController);
			
			}
		}
			
			
			
		
	}

	public Constants.Operacion getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(Constants.Operacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public void setInterbancariosViewController(InterbancariosViewController interbancariosViewController) {
		this.interbancariosViewController = interbancariosViewController;
	}
	
	
	public BaseViewController getFrecuenteViewController() {
		return frecuenteViewController;
	}

	public void setFrecuenteViewController(
			BaseViewController frecuenteViewController) {
		this.frecuenteViewController = frecuenteViewController;
	}

	public TransferResult getResult(){		
		return result;
	}
	
	public String textoMensajeAlertaTC() {
	       return Session.getInstance(SuiteApp.getInstance()).getOpenHoursForExternalTransfersMessageUsingTC();
	}
	
	public void validaDatos(){
		String beneficiario = Tools.removeSpecialCharacters(interbancariosViewController.txtBeneficiario.getText().toString()).trim();
		String concepto = Tools.removeSpecialCharacters(interbancariosViewController.txtConcepto.getText().toString()).trim();
		transferenciaInterbancaria.setNumeroCuentaDestino(interbancariosViewController.txtNumeroCuenta.getText().toString());
		transferenciaInterbancaria.setBeneficiario(beneficiario);
		transferenciaInterbancaria.setImporte(Tools.formatAmountForServer(interbancariosViewController.textImporte.getAmount()));
		transferenciaInterbancaria.setReferencia(interbancariosViewController.txtReferencia.getText().toString());
		transferenciaInterbancaria.setConcepto(concepto);
		String typeCard  = transferenciaInterbancaria.getTipoCuentaDestino();
//		String nombreBanco = Session.getInstance(SuiteApp.appContext).obtenerNombreBanco(transferenciaInterbancaria.getInstitucionBancaria(), typeCard);
//	    String codigoBanco = Session.getInstance(SuiteApp.appContext).obtenerCodigoBanco(transferenciaInterbancaria.getInstitucionBancaria(), typeCard);
//		transferenciaInterbancaria.setNombreBanco(nombreBanco);
//		transferenciaInterbancaria.setInstitucionBancaria(codigoBanco);
		
		if (Tools.isEmptyOrNull(typeCard)){
			interbancariosViewController.showInformationAlert(R.string.transferir_interbancario_alerta_tipo_tarjeta);
		} 
		else if(Tools.isEmptyOrNull(transferenciaInterbancaria.getNombreBanco()) || Tools.isEmptyOrNull(transferenciaInterbancaria.getInstitucionBancaria())) {
			interbancariosViewController.showInformationAlert(R.string.transferir_interbancario_alerta_banco);
		}//SPEI
		else if(transferenciaInterbancaria.getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE) && typeCard.equals(Constants.PHONE_TYPE_ACCOUNT)) {
			interbancariosViewController.showInformationAlert(R.string.transferir_interbancario_error_tdc_to_phone);
		}
		else if (interbancariosViewController.txtNumeroCuenta.length() == 0) {
			if(typeCard.equals(Constants.PHONE_TYPE_ACCOUNT))
				interbancariosViewController.showInformationAlert(R.string.transferir_interbancario_alerta_celular);
			else
				interbancariosViewController.showInformationAlert(R.string.transferir_interbancario_alerta_numero_cuenta);
		}//TERMINA SPEI
		else if (interbancariosViewController.txtNumeroCuenta.length() != longitudNumeroCuenta){
			String mensaje = interbancariosViewController.getString(R.string.transferir_interbancario_alerta_numero_cuenta_menor1);
			mensaje += " ";
			mensaje += String.valueOf(longitudNumeroCuenta);
			mensaje += " ";
			//if (transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.DEBIT_TYPE) || transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.CREDIT_TYPE)) {
				//mensaje += interbancariosViewController.getString(R.string.transferir_interbancario_alerta_numero_cuenta_menor2);
			//SPEI
			if(typeCard.equals(Constants.PHONE_TYPE_ACCOUNT)){
				mensaje += interbancariosViewController.getString(R.string.transferir_interbancario_alerta_numero_cuenta_menor2_celular);
			} else if (transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.DEBIT_TYPE) || transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.CREDIT_TYPE)) {
				mensaje += interbancariosViewController.getString(R.string.transferir_interbancario_alerta_numero_cuenta_menor2);
			} 
			else { //TERMINA SPEI	
				mensaje += interbancariosViewController.getString(R.string.transferir_interbancario_alerta_numero_cuenta_menor2_clabe);
			}

			interbancariosViewController.showInformationAlert(mensaje);
		} 
		else if (beneficiario.length() == 0) {
			interbancariosViewController.txtBeneficiario.setText(beneficiario);
			interbancariosViewController.showInformationAlert(R.string.transferir_interbancario_alerta_beneficiario);
		}
		else if (!validaMonto(Tools.formatAmountForServer(interbancariosViewController.textImporte.getAmount()))) {
			interbancariosViewController.showInformationAlert(R.string.transferir_otrosBBVA_importe_validacion_alerta);
		}
		else if (interbancariosViewController.txtReferencia.length() == 0) {
			interbancariosViewController.showInformationAlert(R.string.transferir_interbancario_alerta_referencia);
		}
		else if (interbancariosViewController.txtReferencia.length() < 6) {
			interbancariosViewController.showInformationAlert(R.string.transferir_interbancario_alerta_referencia_menor_seis_digitos);
		}
		else if (concepto.length() == 0) {
			interbancariosViewController.txtConcepto.setText(concepto);
			interbancariosViewController.showInformationAlert(R.string.transferir_interbancario_alerta_concepto);
		}
		else  {
			if(transferenciaInterbancaria.getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE) && transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.CREDIT_TYPE))
				consultaComision();
			else{

				//ARR
				Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
				
				//ARR
				paso2OperacionMap.put("evento_paso2", "event47");
				paso2OperacionMap.put("&&products", "operaciones;transferencias+otros bancos");
				paso2OperacionMap.put("eVar12", "paso2:cuenta e importe");

				TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
				showConfirmacion();
			}
		}
	}
	
	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada seg�n sea reguerido.
	 * @return
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		//Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccountsIB();
		
		if(profile == Constants.Perfil.avanzado) {
		//if(profile == Constants.Perfil.avanzado || profile == Perfil.basico) {
//			if(interbancariosViewController instanceof InterbancariosViewController) {
				for(Account acc : accounts) {
					if(acc.isVisible()) {
						accountsArray.add(acc);
						break;
					}
				}
				
				for(Account acc : accounts) {
					if(!acc.isVisible())
						accountsArray.add(acc);
				}
		//	}
			
		} else {
			for(Account acc : accounts) {
				if(acc.isVisible())
					accountsArray.add(acc);
			}
		}
		
		return accountsArray;
	}

	/**
	 * Verifica si es necesario registrar la operacion o solo confirmar.
	 */
	public void showConfirmacion() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		if(registrarOperacion()){
			bMovilVC.showRegistrarOperacion(this);
		}else{
			bMovilVC.showConfirmacion(this);
		}
	}
	
	/**
	 * Muestra la pantalla de confirmacion registro
	 */
	public void showConfirmacionRegistro() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		bMovilVC.showConfirmacionRegistro(this);		
	}	

	@Override
	public int getColorTituloResultado() {
		if (bajaFrecuente) {
			return R.color.magenta;
		} else {
			return R.color.verde_limon;
		}
	}
    public void numeroTarjeta() {
//    	interbancariosViewController.txtNumeroCuenta.setText("");
    	if (transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.DEBIT_TYPE)) {

        	// fija el texto y el tama�o del campo adecuados para tarjeta de debito
        	InputFilter[] FilterArray = new InputFilter[1];
        	FilterArray[0] = new InputFilter.LengthFilter(Constants.CARD_NUMBER_LENGTH);
        	longitudNumeroCuenta = Constants.CARD_NUMBER_LENGTH;
        	interbancariosViewController.txtNumeroCuenta.setFilters(FilterArray);

        } else if (transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.CREDIT_TYPE)) {
        	if (transferenciaInterbancaria.getInstitucionBancaria() == null || 
				transferenciaInterbancaria.getInstitucionBancaria().equals("")){
            	InputFilter[] FilterArray = new InputFilter[1];
            	FilterArray[0] = new InputFilter.LengthFilter(Constants.CARD_NUMBER_LENGTH);
            	longitudNumeroCuenta = Constants.CARD_NUMBER_LENGTH;
            	interbancariosViewController.txtNumeroCuenta.setFilters(FilterArray);
			} else if (transferenciaInterbancaria.getInstitucionBancaria().equals(Constants.AMEX_ID)){
				InputFilter[] FilterArray = new InputFilter[1];
	        	FilterArray[0] = new InputFilter.LengthFilter(Constants.AMEX_CARD_NUMBER_LENGTH);
	        	longitudNumeroCuenta = Constants.AMEX_CARD_NUMBER_LENGTH;
	        	interbancariosViewController.txtNumeroCuenta.setFilters(FilterArray);
			} else {
				InputFilter[] FilterArray = new InputFilter[1];
            	FilterArray[0] = new InputFilter.LengthFilter(Constants.CARD_NUMBER_LENGTH);
            	longitudNumeroCuenta = Constants.CARD_NUMBER_LENGTH;
            	interbancariosViewController.txtNumeroCuenta.setFilters(FilterArray);
			}
        }else if (transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.CLABE_TYPE_ACCOUNT)) {
        	
        	// fija el texto y el tama�o del campo adecuados para cuenta clabe
        	InputFilter[] FilterArray = new InputFilter[1];
        	FilterArray[0] = new InputFilter.LengthFilter(Constants.CUENTA_CLABE_LENGTH);
        	longitudNumeroCuenta = Constants.CUENTA_CLABE_LENGTH;
        	interbancariosViewController.txtNumeroCuenta.setFilters(FilterArray);
        }
    	//SPEI
        else if(transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.PHONE_TYPE_ACCOUNT)) {
        	// fija el texto y el tamaÔøΩo del campo adecuados para cuenta clabe
        	InputFilter[] FilterArray = new InputFilter[1];
        	FilterArray[0] = new InputFilter.LengthFilter(Constants.TELEPHONE_NUMBER_LENGTH);
        	longitudNumeroCuenta = Constants.TELEPHONE_NUMBER_LENGTH;
        	interbancariosViewController.txtNumeroCuenta.setFilters(FilterArray);
        }
    	//Termina SPEI
    }
	
    public ArrayList<Object> getListaTipoCuentaDestino(){
		ArrayList<Object> listaOpcionesMenu = new ArrayList<Object>(); 
		ArrayList<Object> registros;

//		if (Constants.CREDIT_TYPE.equals(transferenciaInterbancaria.getCuentaOrigen().getType())) {
//
//			registros = new ArrayList<Object>(2);
//			registros.add(Constants.CREDIT_TYPE);
//			registros.add(SuiteApp.appContext.getString(R.string.cardType_credit));
//			listaOpcionesMenu.add(registros);
//
//        } else {
            // fill the combo
			registros = new ArrayList<Object>(2);
			registros.add(Constants.DEBIT_TYPE);
			registros.add(SuiteApp.appContext.getString(R.string.cardType_debit));
			listaOpcionesMenu.add(registros);
			
			registros = new ArrayList<Object>(2);
			registros.add(Constants.CREDIT_TYPE);
			registros.add(SuiteApp.appContext.getString(R.string.cardType_credit));
			listaOpcionesMenu.add(registros);
			
			registros = new ArrayList<Object>(2);
			registros.add(Constants.CLABE_TYPE_ACCOUNT);
			registros.add(SuiteApp.appContext.getString(R.string.cardType_clabe));
			listaOpcionesMenu.add(registros);
			
			//SPEI

			registros = new ArrayList<Object>(2);
			registros.add(Constants.PHONE_TYPE_ACCOUNT);
			registros.add(SuiteApp.appContext.getString(R.string.cardType_phone));
			listaOpcionesMenu.add(registros);
			//Termina SPEI
//        }
		
		return listaOpcionesMenu;
    }
    
    public ArrayList<Object> getListadoBancos(){
        boolean isCreditCard = (transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.CREDIT_TYPE));
        Catalog banks = Session.getInstance(SuiteApp.appContext).getBanks(isCreditCard);
		ArrayList<Object> listaOpcionesMenu = new ArrayList<Object>(); 
		ArrayList<Object> registros;
        
        int size = (banks != null) ? banks.size() : 0;

        for (int i = 0; i < size; i++) {
			registros = new ArrayList<Object>(2);
			registros.add(banks.getItem(i));
			registros.add(banks.getItem(i).getValue());
			listaOpcionesMenu.add(registros);
        }
        
        return listaOpcionesMenu;
    }
    
	/**
	 * Validates if the amount is correct.
	 * @param amount The amount for the transaction.
	 * @return True if the amount is correct.
	 */
	public boolean validaMonto(String amount) {
		long value = -1;
		
		try {
			value = Long.valueOf(amount);
		} catch (NumberFormatException ex) {
			if(Server.ALLOW_LOG) Log.e("TransferirMisCuentasDelegate", "Error while parsing the amount as an int.", ex);
			return false;
		}
		
		return (value > 0);	
	}

    
    @Override
    public void performAction(Object obj) {
    	if(interbancariosViewController != null)
    	interbancariosViewController.actualizarSeleccion(obj);
    	
    }

//    String obtenerNombreBanco(String codigo){
//    	NameValuePair banco = null;
//    	String nombreBanco = "";
//        Catalog banksTC = Session.getInstance(SuiteApp.appContext).getBanks(false);
//       
//        if((banco = banksTC.getNameValuePair(codigo))!= null){
//        	nombreBanco = banco.getValue();        	
//        }else{
//        	 Catalog banksTDD = Session.getInstance(SuiteApp.appContext).getBanks(true);
//        	 if((banco = banksTDD.getNameValuePair(codigo))!= null){
//        		 nombreBanco = banco.getValue();            	
//             }        	
//        }
//        return nombreBanco;   	
//    }
    
   @Override
   	public String getTextoAyudaResultados() {
   		if (bajaFrecuente) {
   			return super.getTextoAyudaResultados();
   		} else {
   		//	return interbancariosViewController.getString(R.string.transferir_interbancario_texto_ayuda_resultados);
   			return interbancariosViewController.getString(R.string.bmovil_consultar_interbancario_text_especial_resultados);

   		}
   }

   void consultaComision(){
	   Session session = Session.getInstance(SuiteApp.appContext);
	   Hashtable<String,String> paramTable = null;
	   paramTable = new Hashtable<String, String>();
	   paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());//NT
	   paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());//IU
	   paramTable.put("CC", transferenciaInterbancaria.getCuentaOrigen().getFullNumber());//CC
	   paramTable.put("CA", transferenciaInterbancaria.getTipoCuentaDestino()+transferenciaInterbancaria.getNumeroCuentaDestino());//CA
	   paramTable.put("IM", transferenciaInterbancaria.getImporte()==null?"": transferenciaInterbancaria.getImporte());//IM
	   paramTable.put("TO", Constants.COMISION_I);//TO
	   //JAIG
	   doNetworkOperation(Server.CONSULTAR_COMISION_I, paramTable,false,new Comision(),isJsonValueCode.NONE, interbancariosViewController);
   }
   
   //Consulta de frecuentes 
//   BaseViewController 
   /**
    * @return the viewController
    */
   public BaseViewController getViewController() {
	   return frecuenteViewController;
   }

   /**
    * @param viewController the viewController to set
    */
   public void setViewController(BaseViewController viewController) {
	   this.frecuenteViewController = viewController;
   }
   
   @Override
   public void consultarFrecuentes(String tipoFrecuente){
	   esFrecuente = true;
	   Session session = Session.getInstance(SuiteApp.appContext);
	   Hashtable<String,String> paramTable = null;
	   paramTable = new Hashtable<String, String>();
	 //  paramTable.put(Server.USERNAME_PARAM, 	session.getUsername());//NT
	   paramTable.put(ServerConstants.IUM_ETIQUETA,			session.getIum());//IU
	 //  paramTable.put(Server.NUMERO_CLIENTE_PARAM,session.getClientNumber());//TE
	 //  paramTable.put(Server.TIPO_CONSULTA_PARAM,tipoFrecuente);//TP
//	   doNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable, frecuenteViewController);
	   paramTable.put("NT", session.getUsername());
	   paramTable.put("TE", session.getClientNumber());
	   paramTable.put("TP",tipoFrecuente);//TP
	   String tipoConsulta = tipoFrecuente!=null?tipoFrecuente:"";
	   if(tipoConsulta.equals(Constants.tipoCFOtrosBBVA))
		{
			//paramTable.put("numeroTelefono", 	session.getUsername());
		  // paramTable.put("IUM",			session.getIum());
		  // paramTable.put("numeroCliente",session.getClientNumber());
		   //JAIG
			paramTable.put("order","TP*TE*IU*NT");
		   ((BmovilViewsController)frecuenteViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION_BBVA, paramTable,true, new PaymentExtract(),isJsonValueCode.NONE, frecuenteViewController,true);
		}
		else
		{
			//paramTable.put("NT", 	session.getUsername());
			//paramTable.put(ServerConstants.IUM_ETIQUETA,			session.getIum());
			//paramTable.put("TE",session.getClientNumber());
			//paramTable.put("TP",tipoFrecuente);//TP
			paramTable.put("AP", "");
			//correo freceuntes
			paramTable.put("VM", Constants.APPLICATION_VERSION);

			//JAIG
			paramTable.put("order","TP*TE*IU*VM*AP*NT");
			 ((BmovilViewsController)frecuenteViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable,false, new PaymentExtract(),isJsonValueCode.NONE, frecuenteViewController,true);
		}
   }
   
   private void mostrarListaFrecuentes(){
	  ((ConsultarFrecuentesViewController)frecuenteViewController).muestraFrecuentes();
   }
   
   @SuppressWarnings("unchecked")
   public void operarFrecuente(int indexSelected){
	   bajaFrecuente = false;
	   ArrayList<Object> arlPayments = null;
	   if (frecuenteViewController instanceof ConsultarFrecuentesViewController) {
		   arlPayments = ((ConsultarFrecuentesViewController)frecuenteViewController).getListaFrecuentes().getLista();
	   }
	   
	   //Payment selectedPayment = frecuentes[indexSelected];
	   if (arlPayments != null && arlPayments.size() > 0) {
		   selectedPayment = (Payment)((ArrayList<Object>)arlPayments.get(indexSelected)).get(0);
	   }
	   ArrayList<Account> cuentas = cargaCuentasOrigen();
	   Account cuentaOrigen = null ;
	   if(cuentas.size() > 0)
		   cuentaOrigen = cuentas.get(0);
	   String typeCard  = selectedPayment.getBeneficiaryTypeAccount();
	   String nombreBanco = Session.getInstance(SuiteApp.appContext).obtenerNombreBanco(selectedPayment.getBankCode(), typeCard);
	   String codigoBanco = Session.getInstance(SuiteApp.appContext).obtenerCodigoBanco(selectedPayment.getBankCode(), typeCard);
	   this.transferenciaInterbancaria = new TransferenciaInterbancaria(cuentaOrigen, 
			   															codigoBanco,
			   															selectedPayment.getBeneficiaryAccount(),
			   															selectedPayment.getBeneficiary(),
			   															selectedPayment.getReference(),
			   															selectedPayment.getConcept(),
			   															selectedPayment.getAmount(),
			   															selectedPayment.getNickname(),
			   															Constants.TPTransferInterbancarias,
			   															nombreBanco,
			   															typeCard);
	   transferenciaInterbancaria.setFrecuenteMulticanal(selectedPayment.getIdToken());
	   transferenciaInterbancaria.setIdCanal(selectedPayment.getIdCanal());
	   transferenciaInterbancaria.setUsuario(selectedPayment.getUsuario());
	   //frecuente correo electronico
	   transferenciaInterbancaria.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());

	   esFrecuente = true;
	   if(typeCard.equals(Constants.TP_TC_VALUE))
	   {
		   if(isOpenHoursTC()){
				  if(frecuenteViewController != null)
							((BmovilViewsController)frecuenteViewController.getParentViewsController()).showInterbancariosViewController(transferenciaInterbancaria);
			   }else{
				   if(interbancariosViewController != null)
						((BmovilViewsController)interbancariosViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(textoMensajeAlertaTC());
						else 
						if(frecuenteViewController != null)
							((BmovilViewsController)frecuenteViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(textoMensajeAlertaTC());

			   }
	   }
	   else
	   {
		   if(isOpenHours()){
				  if(frecuenteViewController != null)
							((BmovilViewsController)frecuenteViewController.getParentViewsController()).showInterbancariosViewController(transferenciaInterbancaria);
			   }else{
				   if(interbancariosViewController != null)
						((BmovilViewsController)interbancariosViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(textoMensajeAlerta());
						else 
						if(frecuenteViewController != null)
							((BmovilViewsController)frecuenteViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(textoMensajeAlerta());

			   }
	   }
	   
	   
	  

   }
   
   @SuppressWarnings("unchecked")
   public void frecuenteTC(int indexSelected){
	   ArrayList<Object> arlPayments = null;
	   if (frecuenteViewController instanceof ConsultarFrecuentesViewController) {
		   arlPayments = ((ConsultarFrecuentesViewController)frecuenteViewController).getListaFrecuentes().getLista();
	   }
	   
	   //Payment selectedPayment = frecuentes[indexSelected];
	   if (arlPayments != null && arlPayments.size() > 0) {
		   selectedPayment = (Payment)((ArrayList<Object>)arlPayments.get(indexSelected)).get(0);
	   }
	   ArrayList<Account> cuentas = cargaCuentasOrigen();
	   Account cuentaOrigen = null ;
	   if(cuentas.size() > 0)
		   cuentaOrigen = cuentas.get(0);
	   String typeCard  = selectedPayment.getBeneficiaryTypeAccount();
	   String nombreBanco = Session.getInstance(SuiteApp.appContext).obtenerNombreBanco(selectedPayment.getBankCode(), typeCard);
	   String codigoBanco = Session.getInstance(SuiteApp.appContext).obtenerCodigoBanco(selectedPayment.getBankCode(), typeCard);
	   this.transferenciaInterbancaria = new TransferenciaInterbancaria(cuentaOrigen, 
			   															codigoBanco,
			   															selectedPayment.getBeneficiaryAccount(),
			   															selectedPayment.getBeneficiary(),
			   															selectedPayment.getReference(),
			   															selectedPayment.getConcept(),
			   															selectedPayment.getAmount(),
			   															selectedPayment.getNickname(),
			   															Constants.TPTransferInterbancarias,
			   															nombreBanco,
			   															typeCard);
	   transferenciaInterbancaria.setFrecuenteMulticanal(selectedPayment.getIdToken());
	   transferenciaInterbancaria.setIdCanal(selectedPayment.getIdCanal());
	   transferenciaInterbancaria.setUsuario(selectedPayment.getUsuario());

   }
   
   @SuppressWarnings("unchecked")
   @Override
   public void eliminarFrecuente(int indexSelected) {
	   setBajaFrecuente(true);
	   esFrecuente = false;
	   ArrayList<Object> arlPayments = null;
	   if (frecuenteViewController instanceof ConsultarFrecuentesViewController) {
		   arlPayments = ((ConsultarFrecuentesViewController)frecuenteViewController).getListaFrecuentes().getLista();
	   }
	   
	   Payment selectedPayment = frecuentes[indexSelected];
	   if (arlPayments != null && arlPayments.size() > 0) {
		   selectedPayment = (Payment)((ArrayList<Object>)arlPayments.get(indexSelected)).get(0);
	   }
	   this.selectedPayment = selectedPayment;
	   ArrayList<Account> cuentas = cargaCuentasOrigen();
	   Account cuentaOrigen = null ;
	   if(cuentas.size() > 0)
		   cuentaOrigen = cuentas.get(0);
	   String typeCard  = selectedPayment.getBeneficiaryTypeAccount();
	   String nombreBanco = Session.getInstance(SuiteApp.appContext).obtenerNombreBanco(selectedPayment.getBankCode(), typeCard);
	   this.transferenciaInterbancaria = new TransferenciaInterbancaria(cuentaOrigen, 
			   															selectedPayment.getBankCode(),
			   															selectedPayment.getBeneficiaryAccount(),
			   															selectedPayment.getBeneficiary(),
			   															selectedPayment.getReference(),
			   															selectedPayment.getConcept(),
			   															selectedPayment.getAmount(),
			   															selectedPayment.getNickname(),
			   															Constants.TPTransferInterbancarias,
			   															nombreBanco,
			   															typeCard);
	   transferenciaInterbancaria.setFrecuenteMulticanal(selectedPayment.getIdToken());
	   transferenciaInterbancaria.setIdCanal(selectedPayment.getIdCanal());
	   transferenciaInterbancaria.setUsuario(selectedPayment.getUsuario());
	   setTipoOperacion(Constants.Operacion.bajaFrecuente);
	   showConfirmacionAutenticacion();

   }
   
   @Override
	public ArrayList<Object> getDatosTablaFrecuentes() {
		ArrayList<Object> listaDatos = new ArrayList<Object>();
		if (frecuentes != null) {
			for (Payment payment : frecuentes) {
				String typeCard  = payment.getBeneficiaryTypeAccount();
				String nombreBanco = Session.getInstance(SuiteApp.appContext).obtenerNombreBanco(payment.getBankCode(), typeCard);
				if(Tools.isEmptyOrNull(nombreBanco))
					continue;
				ArrayList<Object> registro = new ArrayList<Object>();
				registro.add(payment);
				//registro.add(typeCard);
				registro.add(payment.getNickname());
				registro.add(nombreBanco);
				registro.add(Tools.hideAccountNumber(payment.getBeneficiaryAccount()));
				listaDatos.add(registro);
			}
		} 
		return listaDatos;
	}
   
	@Override
	public ArrayList<Object> getDatosHeaderTablaFrecuentes() {
		ArrayList<Object> registros = new ArrayList<Object>();
		registros.add("");
		registros.add(SuiteApp.appContext.getString(R.string.altafrecuente_nombrecorto));
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_frecuentes_header_banco));
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_frecuentes_header_cuenta));
		return registros;
	}
   

	/**
	 * Check if the current server date is between the available operation time,
	 * which is "hard coded" to Monday to Friday from 6:00 to 17:20
	 * 
	 * @return true if the current server date is between the available
	 *         operation hours, false if not
	 */
	public boolean isOpenHours() {
		return Session.getInstance(SuiteApp.getInstance())
				.isOpenHoursForExternalTransfers();
	}
	public boolean isOpenHoursTC() {
		return Session.getInstance(SuiteApp.getInstance()).isOpenHoursForExternalTransfersUsingTC();
	}	

	
	/**
	 * Displays a text that indicates that external transfers cannot be
	 * performed during current time.
	 */
	public String textoMensajeAlerta() {
		return Session.getInstance(SuiteApp.getInstance())
				.getOpenHoursForExternalTransfersMessage();
	}
   
	
	@Override
	public Hashtable<String, String> getParametrosAltaFrecuentes() {
		 Session session = Session.getInstance(SuiteApp.appContext);
		 Hashtable<String,String> paramTable = null;		
		 paramTable = new Hashtable<String, String>();
		 paramTable.put("NT", session.getUsername());
		 paramTable.put("IU", session.getIum());
		 paramTable.put("TE", session.getClientNumber());
		 paramTable.put("CA", transferenciaInterbancaria.getTipoCuentaDestino() + transferenciaInterbancaria.getNumeroCuentaDestino());
		 paramTable.put("BF", transferenciaInterbancaria.getBeneficiario());
		 paramTable.put("CP", transferenciaInterbancaria.getConcepto());
		 paramTable.put("TP", Constants.TPTransferInterbancarias);
		 paramTable.put(Server.PAYMENT_ACCOUNT_PARAM, transferenciaInterbancaria.getTipoCuentaDestino()+transferenciaInterbancaria.getNumeroCuentaDestino());
		 paramTable.put("CB", transferenciaInterbancaria.getInstitucionBancaria());
		 paramTable.put("OA", "");
		 paramTable.put("CC", "");
		 paramTable.put("IM", transferenciaInterbancaria.getImporte());
         paramTable.put("AP", "");
		 paramTable.put("CV", "");
		 paramTable.put("RF", transferenciaInterbancaria.getReferencia());
		 return paramTable;
	}
	
	private void actualizaFrecuenteAMulticanal(){
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), "actualizaFrecuenteAMulticanal");
		 Session session = Session.getInstance(SuiteApp.appContext);
		   Hashtable<String,String> paramTable = null;
		   paramTable = new Hashtable<String, String>();
		   paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
		   paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());
		   paramTable.put("TE", session.getClientNumber());
		   paramTable.put("TP", Constants.TPTransferInterbancarias);
		   paramTable.put("CA", transferenciaInterbancaria.getNumeroCuentaDestino());
		   paramTable.put("DE", transferenciaInterbancaria.getAliasFrecuente());
		   paramTable.put("RF", transferenciaInterbancaria.getReferencia());
		   paramTable.put("BF", transferenciaInterbancaria.getBeneficiario());
		   paramTable.put("CB", transferenciaInterbancaria.getInstitucionBancaria());
		   paramTable.put("CN", transferenciaInterbancaria.getIdCanal());
		   paramTable.put("US", transferenciaInterbancaria.getUsuario());
		   paramTable.put("OA", "");
		//freceunte correo electronico
		paramTable.put("CE", transferenciaInterbancaria.getCorreoFrecuente());

		//JAIG
		   doNetworkOperation(Server.ACTUALIZAR_FRECUENTE, paramTable,false,new FrecuenteMulticanalResult(),isJsonValueCode.NONE, interbancariosViewController);
	}

	private void transferenciaExitosa(){
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), "transferenciaExitosa");
		Session session = Session.getInstance(SuiteApp.appContext);
		String saldoActual = obtenerSaldoActual();
		session.actualizaMonto(transferenciaInterbancaria.getCuentaOrigen(), saldoActual);		
		((BmovilViewsController)interbancariosViewController.getParentViewsController()).showResultadosViewController(this, -1, -1);		
	}

	public void showConfirmacionAutenticacion(){
		BmovilViewsController bmovilParentController = ((BmovilViewsController)frecuenteViewController.getParentViewsController());
		bmovilParentController.showConfirmacionAutenticacionViewController(this, getNombreImagenEncabezado(), 
																				getTextoEncabezado(), 
																				R.string.confirmation_subtitulo);
	}
	
	@Override
	public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController,String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
		//prepare data
		Hashtable<String,String> paramTable = null;
		//tempPassword = Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia;
		int operacion = 0;
		Session session = Session.getInstance(SuiteApp.appContext);

		operacion = Server.BAJA_FRECUENTE;
		//completar hashtable
		paramTable = new Hashtable<String, String>();

		paramTable.put(Server.USERNAME_PARAM, session.getUsername());//"NT"
		paramTable.put(Server.PASSWORD_PARAM, Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia );//NP
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());//IU
		paramTable.put(Server.NUMERO_CLIENTE_PARAM, session.getClientNumber());//TE
		paramTable.put(Server.DESCRIPCION_PARAM, transferenciaInterbancaria.getAliasFrecuente());//DE
		paramTable.put(Server.PAYMENT_ACCOUNT_PARAM, transferenciaInterbancaria.getTipoCuentaDestino() + transferenciaInterbancaria.getNumeroCuentaDestino());//CA
//		paramTable.put(Server.BENEFICIARIO_PARAM, compraTiempoAire.getNombreCompania());//BF
//		paramTable.put(Server.REFERENCIA_NUMERICA_PARAM, compraTiempoAire.getCelularDestino());//RF
//		String idToken = (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) ? "T" : "";
		if (!transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.CLABE_TYPE_ACCOUNT)) {
			paramTable.put(Server.BANK_CODE_PARAM, transferenciaInterbancaria.getInstitucionBancaria());
		}
		paramTable.put(Server.ID_TOKEN, selectedPayment.getIdToken());
		paramTable.put(Server.TIPO_CONSULTA_PARAM, Constants.TPTransferInterbancarias);//TP
		String va =  Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,
				Session.getInstance(SuiteApp.appContext).getClientProfile());
		paramTable.put(Server.VA_PARAM,Tools.isEmptyOrNull(va)?"":va);//validar
		paramTable.put(Server.ID_CANAL, transferenciaInterbancaria.getIdCanal());
		paramTable.put(Server.USUARIO, transferenciaInterbancaria.getUsuario());

		Hashtable<String,String> paramTable2=new Hashtable<String, String>();
		paramTable2=ServerMapperUtil.mapperBaja(paramTable);
		//JAIG CHECK
		doNetworkOperation(operacion, paramTable2, false, null, isJsonValueCode.NONE, confirmacionAutenticacionViewController);

	}

	protected String obtenerSaldoActual(){
		boolean esExpress = transferenciaInterbancaria.getCuentaOrigen().getType().equals(Constants.EXPRESS_TYPE);
		String saldoActual = "";
		if(esExpress){
			int longitud = transferenciaInterbancaria.getImporte().length();
	    	BigDecimal fullImporte = new BigDecimal(transferenciaInterbancaria.getCuentaOrigen().getBalance());
	    	BigDecimal subtractImporte = new BigDecimal(transferenciaInterbancaria.getImporte().substring(0, longitud-2));
	    	fullImporte = fullImporte.setScale(2, RoundingMode.HALF_UP);
	    	fullImporte = fullImporte.subtract(subtractImporte);
	    	saldoActual = fullImporte.toString();
	    	saldoActual = Tools.formatAmountForServer(saldoActual);
		}else{
			saldoActual = result.getSaldoActual();
		}
		return saldoActual;
	}

	/**
	 * Valida si es necesario hacer el registro de operacion para una 
	 * nueva transferencia interbancaria o para una transferencia interbancaria
	 * desde un frecuente.
	 *  
	 * @return true si la Operacion actual requiere registro. 
	 */
	public boolean registrarOperacion(){
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		Operacion operacion;
		if(esFrecuente)
			operacion = Operacion.transferirInterbancariaF;
		else
			operacion = Operacion.transferirInterbancaria;
		Autenticacion aut = Autenticacion.getInstance();
		boolean value = aut.validaRegistro(operacion, perfil);
		if(Server.ALLOW_LOG) Log.d("RegistroOP",value+" InterbancarioF? "+esFrecuente);
		return value;
	}
	
	@Override
	protected ArrayList<Object> getDatosRegistroOp() {
		ArrayList<Object> datosConfirmacion = new ArrayList<Object>();
		ArrayList<String> registro;
		final BaseViewController vc = interbancariosViewController;
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.transferir_interbancario_tabla_resultados_cuentaretiro));
		registro.add(Tools.hideAccountNumber(transferenciaInterbancaria.getCuentaOrigen().getNumber()));
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.transferir_interbancario_banco_destino));// transferir.interbancario.banco.destino
		registro.add(transferenciaInterbancaria.getNombreBanco());
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.transferir_interbancario_tabla_resultados_numero_tarjeta_cuenta));// transferir.interbancario.numero.tarjeta.cuenta
		registro.add(transferenciaInterbancaria.getNumeroCuentaDestino());
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.transferir_interbancario_tabla_resultados_beneficiario));
		registro.add(transferenciaInterbancaria.getBeneficiario());
		datosConfirmacion.add(registro);
//		registro = new ArrayList<String>();
//		registro.add(vc.getString(R.string.transferir_interbancario_tabla_resultados_importe));
//		registro.add(Tools.formatAmount(transferenciaInterbancaria.getImporte(), false));
//		datosConfirmacion.add(registro);
		if (transferenciaInterbancaria.getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE)
				&& transferenciaInterbancaria.getTipoCuentaDestino().equals(Constants.CREDIT_TYPE)) {

			if (!Tools.isEmptyOrNull(comision.getComision())) {
				registro = new ArrayList<String>();
				registro.add(vc.getString(R.string.transferir_interbancario_tabla_resultados_comision));
				registro.add(Tools.formatAmount(comision.getComision(), false));
				datosConfirmacion.add(registro);
			}
		}
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.transferir_interbancario_tabla_resultados_referencia));
		registro.add(transferenciaInterbancaria.getReferencia());
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.transferir_interbancario_tabla_resultados_concepto));
		registro.add(transferenciaInterbancaria.getConcepto());
		datosConfirmacion.add(registro);
		registro = new ArrayList<String>();
		registro.add(vc.getString(R.string.transferir_interbancario_tabla_resultados_fechaoperacion));
		java.util.Date currentDate = Session.getInstance(SuiteApp.appContext).getServerDate();
		registro.add(Tools.dateToString(currentDate));
		datosConfirmacion.add(registro);

		return datosConfirmacion;
	}
	
	@Override
	public ArrayList<Object> getDatosRegistroExitoso() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> registro;
		
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_op));
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_exito));
		tabla.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_cuenta));
		registro.add(transferenciaInterbancaria.getNumeroCuentaDestino());
		tabla.add(registro);
		
		return tabla;
	}
	
	@Override
	protected void realizaOperacion(RegistrarOperacionViewController rovc,	String token) {
       
		int operationId = Server.OP_VALIDAR_CREDENCIALES;
		
		Hashtable<String, String> params = new Hashtable<String, String>();		
     	Session session = Session.getInstance(SuiteApp.appContext);
     	String cuentaDestino = transferenciaInterbancaria.getNumeroCuentaDestino();
     	
		/*params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		params.put(ServerConstants.CVE_ACCESO, "" );
		params.put(Server.J_NIP, "");
		params.put(Server.J_CVV2, "");
		params.put(ServerConstants.CODIGO_OTP, token == null ? "":token);
		params.put(Server.J_AUT, "00010");
		params.put("cuentaDestino", cuentaDestino);*/

		params.put(ServerConstants.ID_OPERACION, ServerConstants.TRANSFERENCIA_INTERBANCARIA);
		params.put(ServerConstants.BANCO_BENEFICIARIO, transferenciaInterbancaria.getNombreBanco());
		params.put(ServerConstants.CUENTA_OTRO_BANCO, transferenciaInterbancaria.getNumeroCuentaDestino());
     	//SPEI
     	params.put(Server.J_NUM_TELEFONO, session.getUsername());
		params.put(Server.J_NUM_CLIENTE, session.getClientNumber());
		params.put(Server.J_CVE_ACCESO, "" );
		params.put(Server.J_NIP, "");
		params.put(Server.J_CVV2, "");
		params.put(Server.J_OTP, token == null ? "":token);
		params.put(Server.J_AUT, "00010");
		//Termina SPEI
		params.put("tarjeta5Dig", "");
		params.put(ServerConstants.IUM, session.getIum());
		params.put(ServerConstants.VERSION, Constants.APPLICATION_VERSION);
		//JAIG
		doNetworkOperation(operationId, params,true,new RegistrarOperacionResult(),isJsonValueCode.NONE, rovc);
	}
	
	 /**
	  * Realiza la operacion de tranferencia interbancaria desde la pantalla de confirmacionRegistro
	  */
	@Override
	public void realizaOperacion(ConfirmacionRegistroViewController viewController,	String contrasenia, String nip, String token) {
        Hashtable<String,String> paramTable = null;

        int operacion = Server.EXTERNAL_TRANSFER_OPERATION;;
     	Session session = Session.getInstance(SuiteApp.appContext);
		String tipoCuentaDestino = transferenciaInterbancaria.getTipoCuentaDestino();
		//completar hashtable
		paramTable = new Hashtable<String, String>();//generaParametrosParaCuentas(transferenciaInterbancaria.getCuentaOrigen().getType(),
												 //transferenciaInterbancaria.getTipoCuentaDestino());


		//paramTable.put(Server.CARD_TYPE_PARAM, Constants.CREDIT_TYPE);
		paramTable.put(ServerConstants.ID_OPERACION, ServerConstants.TRANSFERENCIA_INTERBANCARIA);
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());//"NT"
		paramTable.put(ServerConstants.PARAMS_TEXTO_NP,  Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia );//NP
		paramTable.put(ServerConstants.PARAMS_TEXTO_TE, session.getClientNumber());//TE
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());//IU
		if(tipoCuentaDestino.equals(Constants.CREDIT_TYPE)&&transferenciaInterbancaria.getCuentaOrigen().getCelularAsociado().length()>0)
			paramTable.put("CC", transferenciaInterbancaria.getCuentaOrigen().getFullNumber());//CC
		else if(transferenciaInterbancaria.getCuentaOrigen().getCelularAsociado().length()>0)
			paramTable.put("CC", "SM"+transferenciaInterbancaria.getCuentaOrigen().getCelularAsociado());//CC
		else
			paramTable.put("CC", transferenciaInterbancaria.getCuentaOrigen().getFullNumber());//CC
		paramTable.put("CA", transferenciaInterbancaria.getNumeroCuentaDestino());//CA
		paramTable.put("IM", transferenciaInterbancaria.getImporte()==null?"": transferenciaInterbancaria.getImporte());//IM
		paramTable.put("CB", transferenciaInterbancaria.getInstitucionBancaria());//CB
		paramTable.put("MP", Tools.removeSpecialCharacters(transferenciaInterbancaria.getConcepto()==null?"": transferenciaInterbancaria.getConcepto()));//MP
		paramTable.put("BF", Tools.removeSpecialCharacters(transferenciaInterbancaria.getBeneficiario()==null?"": transferenciaInterbancaria.getBeneficiario()));//BF
		paramTable.put("RF",transferenciaInterbancaria.getReferencia());//RF
		paramTable.put("NI", "");
		paramTable.put("CV", "");
		paramTable.put("OT", Tools.isEmptyOrNull(token)?"":token);
		String va =  Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,
				Session.getInstance(SuiteApp.appContext).getClientProfile(),
				Tools.getDoubleAmountFromServerString(transferenciaInterbancaria.getImporte()));
		paramTable.put("VA",Tools.isEmptyOrNull(va)?"":va);//validar


			if(tipoCuentaDestino.equals(Constants.CREDIT_TYPE)){
				paramTable.put("TP", Constants.TP_TC_VALUE);//TP
			}else if(tipoCuentaDestino.equals(Constants.CLABE_TYPE_ACCOUNT)){
				paramTable.put("TP", Constants.CLABE_TYPE_ACCOUNT);//TP
			}//SPEI
			else if(tipoCuentaDestino.equals(Constants.PHONE_TYPE_ACCOUNT)){
				//paramTable.put("TP", Constants.PHONE_TYPE_ACCOUNT);//PH
				paramTable.put("TP", "TL");//PH
			}//TERMINA SPEI
			else{
				paramTable.put("TP", Constants.TP_TD_VALUE);//TP
			}
			//JAIG
			doNetworkOperation(operacion, paramTable,false,new TransferResult(),isJsonValueCode.NONE, viewController);



	}

	@Override
	public String getNumeroCuentaParaRegistroOperacion() {
		String cuentaDestino = transferenciaInterbancaria.getNumeroCuentaDestino();
		return cuentaDestino.substring(cuentaDestino.length() - 5);
	}
	
	public boolean validaTC() {
		// TODO Auto-generated method stub
		if(transferenciaInterbancaria.getTipoCuentaDestino().equalsIgnoreCase(Constants.CREDIT_TYPE))
			return false;
		else
			return true;
	}
}

