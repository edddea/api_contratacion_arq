package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.DetalleMoviemientoSpeiViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaInterbancariaResult;
import suitebancomer.aplicaciones.bmovil.classes.model.DetalleInterbancaria;
import suitebancomer.classes.gui.controllers.BaseViewController;

public class ConsultaInterbancariosDelegate extends DelegateBaseAutenticacion{

	/**
	 * 
	 */
	public static final long CONSULTA_INTERBANCARIOS_DELEGATE_ID = -2283769288772638685L;

	private static int PERIODO_CONSULTA=0;

	/**
	 * Controlador actual que consume las acciones de este delegado.
	 */
	private BaseViewController ownerController;
	
	ArrayList<DetalleInterbancaria> arrDetalleInterbancaria;

	private DetalleInterbancaria detalleInterbancaria;

	private PendingIntent mSentPendingIntent;

	private BroadcastReceiver mBroadcastReceiver;
	
	
	// #endregion

	
	public ConsultaInterbancariosDelegate() {
		// TODO Auto-generated constructor stub
		PERIODO_CONSULTA=0;
		arrDetalleInterbancaria=null;

	}
	
	// #region Getters y Setters
	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.delegates.BaseDelegate#getDelegateIdentifier()
	 */
	
	
	
	@Override
	public long getDelegateIdentifier() {
		return CONSULTA_INTERBANCARIOS_DELEGATE_ID;
	}

	/**
	 * @return Controlador actual que consume las acciones de este delegado.
	 */
	public BaseViewController getOwnerController() {
		return ownerController;
	}

	/**
	 * @param ownerController Controlador actual que consume las acciones de este delegado.
	 */
	public void setOwnerController(BaseViewController ownerController) {
		this.ownerController = ownerController;
	}

	public ArrayList<Object> getDatosHeaderTablaInterbancarios() {
		// TODO Auto-generated method stub
		ArrayList<Object> registros = new ArrayList<Object>();
		registros.add("");
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_interbancario_fecha));
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_interbancario_banco));
		//registros.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_interbancario_cuenta));
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_interbancario_importe));
		registros.add(SuiteApp.appContext.getString(R.string.bmovil_consultar_interbancario_estado));
		return registros;
	}

	
	public ArrayList<Object> getInterbancarios(){
		ArrayList<Object> listaDatos = new ArrayList<Object>();
		if (arrDetalleInterbancaria != null) {
			for (DetalleInterbancaria spei : arrDetalleInterbancaria) {
				ArrayList<Object> registro = new ArrayList<Object>();
				registro.add(spei);
				registro.add(spei.getFechaAplicacion());
				registro.add(spei.getNombreBanco());
				//registro.add(Tools.hideAccountNumber(spei.getCuentaOrigen()));
				registro.add(Tools.formatAmount(spei.getImporte(),false));
				registro.add(spei.getEstatusOperacion());
				
				listaDatos.add(registro);
			}
		} 
		return listaDatos;
	}
	
	/*parameters[i++] = new NameValuePair(ServerConstants.NUMERO_TELEFONO,
			(String) params.get(ServerConstants.NUMERO_TELEFONO));
	parameters[i++] = new NameValuePair(ServerConstants.CVE_ACCESO,
			(String) params.get(ServerConstants.CVE_ACCESO));
	parameters[i++] = new NameValuePair(ServerConstants.CODIGO_NIP,
			(String) params.get(ServerConstants.CODIGO_NIP));
	parameters[i++] = new NameValuePair(ServerConstants.CODIGO_OTP,
			(String) params.get(ServerConstants.CODIGO_OTP));
	parameters[i++] = new NameValuePair(ServerConstants.CODIGO_CVV2,
			(String) params.get(ServerConstants.CODIGO_CVV2));
	parameters[i++] = new NameValuePair(ServerConstants.CADENA_AUTENTICACION,
			(String) params.get(ServerConstants.CADENA_AUTENTICACION));
	parameters[i++] = new NameValuePair(ServerConstants.TARJETA_5DIG,
			(String) params.get(ServerConstants.TARJETA_5DIG));

	parameters[i++] = new NameValuePair(ServerConstants.PERIODO,
			(String) params.get(ServerConstants.PERIODO));
	parameters[i++] = new NameValuePair(ServerConstants.IUM_ETIQUETA,
			(String) params.get(ServerConstants.IUM_ETIQUETA));*/
	
	
	public void consultarInterbancarios(int periodo){
		Session session = Session.getInstance(SuiteApp.appContext);
		   Hashtable<String,String> paramTable = null;
		   paramTable = new Hashtable<String, String>();
		   paramTable.put("numeroCelular", 	session.getUsername());//NT
		   paramTable.put(ServerConstants.JSON_IUM_ETIQUETA,			session.getIum());//IU
		   
		   paramTable.put(ServerConstants.CVE_ACCESO,"");//IU
		   paramTable.put(ServerConstants.CODIGO_NIP,"");//IU
		   paramTable.put(ServerConstants.CODIGO_OTP,"");//IU
		   paramTable.put(ServerConstants.CODIGO_CVV2,"");//IU
		   paramTable.put(ServerConstants.CADENA_AUTENTICACION,"00000");//Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultaTransferenciasSPEI, session.getClientProfile()));//IU
		   paramTable.put(ServerConstants.TARJETA_5DIG,"");//IU
		   paramTable.put(ServerConstants.PERIODO,""+periodo);//IU
		  
		  // doNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable, frecuenteViewController);
		   //JAIG NO
		  ((BmovilViewsController)ownerController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.OP_CONSULTA_INTERBANCARIOS, paramTable,true, new ConsultaInterbancariaResult(),isJsonValueCode.NONE, ownerController,false);
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		ConsultaInterbancariaResult result=(ConsultaInterbancariaResult)response.getResponse();
		if(arrDetalleInterbancaria==null){
			arrDetalleInterbancaria=result.getArrTransferenciaSPEI();
		}else{
			arrDetalleInterbancaria.addAll(result.getArrTransferenciaSPEI());
		}
	//	arrDetalleInterbancaria=result.getArrTransferenciaSPEI();
		Collections.sort(arrDetalleInterbancaria);

	}
	
	public ArrayList<DetalleInterbancaria> getArrDetalleInterbancaria() {
		return arrDetalleInterbancaria;
	}

	public void setArrDetalleInterbancaria(
			ArrayList<DetalleInterbancaria> arrDetalleInterbancaria) {
		this.arrDetalleInterbancaria = arrDetalleInterbancaria;
	}

	public ArrayList<Object> getDetalleSpei() {
		// TODO Auto-generated method stub
		ArrayList<Object> datosDetalle = new ArrayList<Object>();
		ArrayList<String> registro;
		
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_estatus));
		registro.add(detalleInterbancaria.getTextoEstatus());
		registro.add(detalleInterbancaria.getTextoEstatus());
		datosDetalle.add(registro);
		if(detalleInterbancaria.getEstatusOperacion().equalsIgnoreCase("207")){
			registro = new ArrayList<String>();
			registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_hora_operacion));
			registro.add(detalleInterbancaria.getHoraLiquidacion());
			datosDetalle.add(registro);
		}
		if(detalleInterbancaria.getEstatusOperacion().equalsIgnoreCase("205")||detalleInterbancaria.getEstatusOperacion().equalsIgnoreCase("201")){
			registro = new ArrayList<String>();
			registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_hora_validacion));
			registro.add(detalleInterbancaria.getHoraAprobacion());
			datosDetalle.add(registro);
		}
		if(detalleInterbancaria.getEstatusOperacion().equalsIgnoreCase("206")){
		
		registro = new ArrayList<String>();
		
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_hora_operacionp));
		registro.add(detalleInterbancaria.getHoraAcuse());
		datosDetalle.add(registro);
		}
		
		if(detalleInterbancaria.getEstatusOperacion().equalsIgnoreCase("301")){
			registro = new ArrayList<String>();
			registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_fecha_devolucion));
			registro.add(detalleInterbancaria.getFechaDevolucion());
			datosDetalle.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_hora_devolucion));
			registro.add(detalleInterbancaria.getHoraDevolucion());
			datosDetalle.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_detalle_devolucion));
			registro.add(detalleInterbancaria.getDetalleDevolicion());
			datosDetalle.add(registro);
		}
		
		
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_cuenta_retiro));
		registro.add(Tools.hideAccountNumber(detalleInterbancaria.getCuentaOrigen()));
		datosDetalle.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_banco));
		registro.add(detalleInterbancaria.getNombreBanco());
		datosDetalle.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_numero_tarjeta));
		//registro.add(Tools.hideAccountNumber(detalleInterbancaria.getCuentaDestino()));
		registro.add(detalleInterbancaria.getCuentaDestino());
		datosDetalle.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_beneficiario));
		registro.add(detalleInterbancaria.getNombreBeneficiario());
		datosDetalle.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_importe));
		registro.add(Tools.formatAmount(detalleInterbancaria.getImporte(),false));
		datosDetalle.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_referencia));
		registro.add(detalleInterbancaria.getReferencia());
		datosDetalle.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_concepto));
		registro.add(detalleInterbancaria.getDescripcion());
		datosDetalle.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_fecha_alta));
		registro.add(detalleInterbancaria.getFechaAplicacion());
		datosDetalle.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_hora_alta));
		registro.add(detalleInterbancaria.getHoraAlta());
		datosDetalle.add(registro);
		
		/*if(detalleInterbancaria.getEstatusOperacion().equalsIgnoreCase("207")){
			registro = new ArrayList<String>();
			registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_hora_operacion));
			registro.add(detalleInterbancaria.getHoraLiquidacion());
			datosDetalle.add(registro);
		}
		if(detalleInterbancaria.getEstatusOperacion().equalsIgnoreCase("205")||detalleInterbancaria.getEstatusOperacion().equalsIgnoreCase("201")){
			registro = new ArrayList<String>();
			registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_hora_validacion));
			registro.add(detalleInterbancaria.getHoraAprobacion());
			datosDetalle.add(registro);
		}
		if(detalleInterbancaria.getEstatusOperacion().equalsIgnoreCase("206")){
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_hora_operacionp));
		registro.add(detalleInterbancaria.getHoraAcuse());
		datosDetalle.add(registro);
		}
		if(detalleInterbancaria.getEstatusOperacion().equalsIgnoreCase("301")){
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_fecha_devolucion));
		registro.add(detalleInterbancaria.getFechaDevolucion());
		datosDetalle.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_hora_devolucion));
		registro.add(detalleInterbancaria.getHoraDevolucion());
		datosDetalle.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_detalle_devolucion));
		registro.add(detalleInterbancaria.getDetalleDevolicion());
		datosDetalle.add(registro);
		}*/
		registro = new ArrayList<String>();
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_detalle_folio_operacion));
		registro.add(detalleInterbancaria.getFolio());
		datosDetalle.add(registro);
		
		return datosDetalle;
	}
	
	@Override
	public void performAction(Object obj) {
		// TODO Auto-generated method stub
		detalleInterbancaria=(DetalleInterbancaria)obj;
		((BmovilViewsController)ownerController.getParentViewsController()).showDetalleInterbancaria(this);
	}
	
	
	public int getOpcionesMenuResultados() {
		
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL ;
	}
	
	public void enviaSMS() {
    	String smsText = Tools.removeSpecialCharacters(getTextoSMS());

		mSentPendingIntent = PendingIntent.getBroadcast(ownerController, 0,  new Intent(Constants.SENT), 0);
        SmsManager smsMgr = SmsManager.getDefault();
        ownerController.muestraIndicadorActividad(ownerController.getString(R.string.label_information),
        		ownerController.getString(R.string.sms_sending));
        String mPhone=Session.getInstance(SuiteApp.appContext).getUsername();
        
        if(mBroadcastReceiver == null){
    		mBroadcastReceiver = ((DetalleMoviemientoSpeiViewController)ownerController).createBroadcastReceiver();
        }
        
        ArrayList<String> messages = smsMgr.divideMessage(smsText);
        for (int i = 0; i < messages.size(); i++) {
		     String text = messages.get(i).trim();
		     if(text.length()>0) {
		    	 if(Server.ALLOW_LOG) Log.d("sms mensaje", text);
	  		     // send the message, passing in the pending intent, sentPI
		    	 smsMgr.sendTextMessage(mPhone, null, text, mSentPendingIntent, null);
		    	 ownerController.registerReceiver(mBroadcastReceiver, new IntentFilter(Constants.SENT));
		     }
	     }
		
    }

	public void guardaPDF() {
		
	}
	
	public void enviaEmail() {
		
		ResultadosDelegate resultadosDelegate=new ResultadosDelegate(this);
		
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showEnviarCorreo(resultadosDelegate);
	}
	
	
	@Override
	public String getTextoSMS() {
		// TODO Auto-generated method stub
		String folio = detalleInterbancaria.getFolio();
		String importe =Tools.formatAmount( detalleInterbancaria.getImporte(),false);
		String fecha = Tools.formatDate(detalleInterbancaria.getFechaAplicacion());
		String hora = Tools.formatTime(detalleInterbancaria.getHoraAprobacion());

		StringBuilder msg = new StringBuilder();
		msg.append(SuiteApp.appContext.getString(R.string.transferir_interbancario_texto_titulo_sms));
		msg.append(SuiteApp.appContext.getString(R.string.smsText_firstFrom) + " " + Tools.hideAccountNumber(detalleInterbancaria.getCuentaOrigen()));
		msg.append(" "+SuiteApp.appContext.getString(R.string.smsText_a) + " "+Tools.hideAccountNumber( detalleInterbancaria.getCuentaDestino())+" ");
		msg.append(SuiteApp.appContext.getString(R.string.smsText_secondFrom) +" " +detalleInterbancaria.getNombreBeneficiario().substring(0, detalleInterbancaria.getNombreBeneficiario().length()>10?10:detalleInterbancaria.getNombreBeneficiario().length()));
		msg.append(" "+SuiteApp.appContext.getString(R.string.smsText_byQuantity) +" " +importe+" "+SuiteApp.appContext.getString(R.string.smsText_folio) +" " +folio +" "+fecha +" "+hora);
		String msgText = msg.toString().replaceAll("\n", "");
//		System.out.println(msgText);
		return msgText;
		//return "";
	}
	
	@Override
	public String getTextoAyudaResultados() {
		return "Para guardar esta información oprime el botón de opciones";
	}
	
	@Override
	public String getTextoEmail() {
		// TODO Auto-generated method stub
		return "";
	}

	public ArrayList<Object> getDatosClave() {
		// TODO Auto-generated method stub
		ArrayList<Object> datosDetalle = new ArrayList<Object>();
		ArrayList<String> registro;
		
		registro = new ArrayList<String>();
		registro.add("");
		registro.add(ownerController.getString(R.string.bmovil_consultar_interbancario_clave_rastreo));
		datosDetalle.add(registro);
		
		registro = new ArrayList<String>();
		registro.add("");
		registro.add(Tools.formatCR(detalleInterbancaria.getClaveRastreo()));
		datosDetalle.add(registro);
		return datosDetalle;
	}

	public static int getPeriodoConsulta() {
		// TODO Auto-generated method stub
		int periodo=PERIODO_CONSULTA;
		PERIODO_CONSULTA++;
		
		return periodo;
	}

	public void setPeriodoConsulta(int i) {
		// TODO Auto-generated method stub
		PERIODO_CONSULTA=0;
	}

	public DetalleInterbancaria getDetalleInterbancaria() {
		return detalleInterbancaria;
	}

	public void setDetalleInterbancaria(DetalleInterbancaria detalleInterbancaria) {
		this.detalleInterbancaria = detalleInterbancaria;
	}

	//public void resetPeriodo() {
		// TODO Auto-generated method stub
	//	PERIODO_CONSULTA=0;
	//}
	public void resetPeriodoConsultas() {
		// TODO Auto-generated method stub
		PERIODO_CONSULTA=0;
		arrDetalleInterbancaria=null;
	}	
}
