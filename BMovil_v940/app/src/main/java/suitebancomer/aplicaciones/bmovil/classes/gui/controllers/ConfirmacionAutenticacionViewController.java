package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.consultaotroscreditos.models.ConsultaOtrosCreditosData;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionAutenticacionDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MantenimientoAlertasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MisCuentasDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.NuevaContraseniaDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.QuitarSuspencionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaDatosViewController;
import tracking.TrackingHelper;

public class ConfirmacionAutenticacionViewController extends BaseViewController
		implements View.OnClickListener, CallBackModule {

	private ImageButton confirmarButton;

	private LinearLayout contenedorPrincipal;
	private LinearLayout contenedorContrasena;
	private LinearLayout contenedorNIP;
	private LinearLayout contenedorASM;
	private LinearLayout contenedorCVV;

	private TextView campoContrasena;
	private TextView campoNIP;
	private TextView campoASM;
	private TextView campoCVV;
	private EditText contrasena;
	private EditText nip;
	private EditText asm;
	private EditText cvv;
	private TextView instruccionesContrasena;
	private TextView instruccionesNIP;
	private TextView instruccionesASM;
	private TextView instruccionesCVV;

	// Nuevo Campo
	private TextView campoTarjeta;
	private LinearLayout contenedorCampoTarjeta;
	private EditText tarjeta;
	private TextView instruccionesTarjeta;

	private ConfirmacionAutenticacionDelegate confirmacionAutenticacionDelegate;
	//AMZ
		public BmovilViewsController parentManager;
		public String titulo;
		//AMZ

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE,
				R.layout.layout_bmovil_confirmacion);
		
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication()
				.getBmovilViewsController());
		setDelegate(getParentViewsController()
				.getBaseDelegateForKey(
						ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID));
		setTitulo();

		confirmacionAutenticacionDelegate = (ConfirmacionAutenticacionDelegate) getDelegate();
		confirmacionAutenticacionDelegate
				.setConfirmacionAutenticacionViewController(this);

		findViews();
		scaleToScreenSize();

		
		confirmacionAutenticacionDelegate.consultaDatosLista();

		
		configuraPantalla();
		
		moverScroll();
		
//		this.statusdesactivado = !((confirmacionAutenticacionDelegate
//				.getOperationDelegate() instanceof QuitarSuspencionDelegate)
//				|| (confirmacionAutenticacionDelegate.getOperationDelegate() instanceof NuevaContraseniaDelegate));

		this.statusdesactivado = false;

		contrasena.addTextChangedListener(new BmovilTextWatcher(this));
		nip.addTextChangedListener(new BmovilTextWatcher(this));
		cvv.addTextChangedListener(new BmovilTextWatcher(this));
		asm.addTextChangedListener(new BmovilTextWatcher(this));
		tarjeta.addTextChangedListener(new BmovilTextWatcher(this));
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
			
				//TrackingHelper.trackState("reactivacion", parentManager.estados);
				//}else{
				TrackingHelper.trackState("confautent", parentManager.estados);

	}
	@Override
	protected void onDestroy() {
		parentViewsController.removeDelegateFromHashMap( confirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
		habilitarBtnContinuar();
		//if (statusdesactivado)
			/*if (!(confirmacionAutenticacionDelegate
					.getOperationDelegate() instanceof MantenimientoAlertasDelegate)) {
				if (parentViewsController.consumeAccionesDeReinicio()) {
					return;
				}
			}*/
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
		statusdesactivado = true;
	}

	private void configuraPantalla() {
		mostrarContrasena(confirmacionAutenticacionDelegate
				.consultaDebePedirContrasena());
		mostrarCampoTarjeta(confirmacionAutenticacionDelegate
				.mostrarCampoTarjeta());
		mostrarNIP(confirmacionAutenticacionDelegate.consultaDebePedirNIP());
		mostrarASM(confirmacionAutenticacionDelegate
				.consultaInstrumentoSeguridad());
		mostrarCVV(confirmacionAutenticacionDelegate.consultaDebePedirCVV());

		LinearLayout contenedorPadre = (LinearLayout) findViewById(R.id.confirmacion_campos_layout);

		if (contenedorContrasena.getVisibility() == View.GONE
				&& contenedorNIP.getVisibility() == View.GONE
				&& contenedorASM.getVisibility() == View.GONE
				&& contenedorCVV.getVisibility() == View.GONE
				&& contenedorCampoTarjeta.getVisibility() == View.GONE) {
			// contenedorPadre.setVisibility(View.GONE);
			contenedorPadre.setBackgroundColor(0);
		}

		contenedorPadre.measure(MeasureSpec.UNSPECIFIED,
				MeasureSpec.UNSPECIFIED);
		float camposHeight = contenedorPadre.getMeasuredHeight();
		// contenedorPadre.measure(MeasureSpec.UNSPECIFIED,
		// MeasureSpec.UNSPECIFIED);
		// float camposHeight = contenedorPadre.getMeasuredHeight();
		//
		// LinearLayout layoutListaDatos =
		// (LinearLayout)findViewById(R.id.confirmacion_lista_datos);
		// layoutListaDatos.measure(MeasureSpec.UNSPECIFIED,
		// MeasureSpec.UNSPECIFIED);
		// float listaHeight = layoutListaDatos.getMeasuredHeight();
		//
		// ViewGroup contenido =
		// (ViewGroup)this.findViewById(android.R.id.content).getRootView();//findViewById(android.R.id.content);
		// contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		// float contentHeight = contenido.getMeasuredHeight();
		//
		// System.out.println("Los valores " + camposHeight + " y " +
		// contentHeight + " y " + listaHeight);
		//
		// float margin =
		// getResources().getDimension(R.dimen.confirmacion_fields_initial_margin);

		LinearLayout layoutListaDatos = (LinearLayout) findViewById(R.id.confirmacion_lista_datos);
		layoutListaDatos.measure(MeasureSpec.UNSPECIFIED,
				MeasureSpec.UNSPECIFIED);
		float listaHeight = layoutListaDatos.getMeasuredHeight();
		// float maximumSize = (contentHeight * 4) / 5;
		// System.out.println("Altura maxima " +maximumSize);
		// float elementsSize = listaHeight + camposHeight;
		// System.out.println("Altura mixta " +elementsSize);
		// float heightParaValidar = (contentHeight*3)/4;
		// System.out.println("heightParaValidar " +contentHeight);
		//
		// if (elementsSize >= contentHeight) {
		// RelativeLayout.LayoutParams camposLayout =
		// (RelativeLayout.LayoutParams)contenedorPadre.getLayoutParams();
		// camposLayout.addRule(RelativeLayout.BELOW,
		// R.id.confirmacion_lista_datos);
		// }

		ViewGroup contenido = (ViewGroup) this.findViewById(
				android.R.id.content).getRootView();// findViewById(android.R.id.content);
		contenido.measure(MeasureSpec.EXACTLY, MeasureSpec.EXACTLY);
		float contentHeight = contenido.getMeasuredHeight();

		//System.out.println("Los valores " + camposHeight + " y "
		//	+ contentHeight + " y " + listaHeight);

		float margin = getResources().getDimension(
				R.dimen.confirmacion_fields_initial_margin);

		float maximumSize = (contentHeight * 4) / 5;
		//System.out.println("Altura maxima " + maximumSize);
		float elementsSize = listaHeight + camposHeight;
		//System.out.println("Altura mixta " + elementsSize);
		float heightParaValidar = (contentHeight * 3) / 4;
		//System.out.println("heightParaValidar " + contentHeight);

		if (elementsSize >= contentHeight) {
			// RelativeLayout.LayoutParams camposLayout =
			// (RelativeLayout.LayoutParams)contenedorPadre.getLayoutParams();
			// camposLayout.addRule(RelativeLayout.BELOW,
			// R.id.confirmacion_lista_datos);
		}

		confirmarButton.setOnClickListener(this);
	}

	public void setTitulo() {
	
		ConfirmacionAutenticacionDelegate confirmacionAutenticacionDelegate = (ConfirmacionAutenticacionDelegate) getDelegate();

		setTitle(confirmacionAutenticacionDelegate.consultaOperationsDelegate()
				.getTextoEncabezado(), confirmacionAutenticacionDelegate
				.consultaOperationsDelegate().getNombreImagenEncabezado());
	}

	@SuppressWarnings("deprecation")
	public void setListaDatos(ArrayList<Object> datos) {
		LinearLayout.LayoutParams params = new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		// params.topMargin =
		// getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
		// params.leftMargin =
		// getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
		// params.rightMargin =
		// getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);

		ListaDatosViewController listaDatos = new ListaDatosViewController(
				this, params, parentViewsController);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.setTitulo(R.string.confirmation_subtitulo);
		listaDatos.showLista();
		LinearLayout layoutListaDatos = (LinearLayout) findViewById(R.id.confirmacion_lista_datos);
		layoutListaDatos.addView(listaDatos);
	}

	public void pideContrasenia() {
		findViewById(R.id.campo_confirmacion_contrasena_layout).setVisibility(
				View.GONE);
	}

	public void pideClaveSeguridad() {
		findViewById(R.id.campo_confirmacion_asm_layout).setVisibility(
				View.GONE);
	}

	/*
	* 
	*/
	public void mostrarContrasena(boolean visibility) {
		contenedorContrasena.setVisibility(visibility ? View.VISIBLE
				: View.GONE);
		campoContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
		contrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
		if (visibility) {
			campoContrasena.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoContrasenia());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(
					Constants.PASSWORD_LENGTH);
			contrasena.setFilters(userFilterArray);
			contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		instruccionesContrasena.setVisibility(View.GONE);
	}

	/*
	* 
	*/
	public void mostrarNIP(boolean visibility) {
		contenedorNIP.setVisibility(visibility ? View.VISIBLE : View.GONE);
		campoNIP.setVisibility(visibility ? View.VISIBLE : View.GONE);
		nip.setVisibility(visibility ? View.VISIBLE : View.GONE);
		if (visibility) {
			campoNIP.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoNip());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(
					Constants.NIP_LENGTH);
			nip.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
			String instrucciones = confirmacionAutenticacionDelegate
					.getTextoAyudaNIP();
			if (instrucciones.equals("")) {
				instruccionesNIP.setVisibility(View.GONE);
			} else {
				instruccionesNIP.setVisibility(View.VISIBLE);
				instruccionesNIP.setText(instrucciones);
			}
		} else {
			nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}

	/*
	* 
	*/
	public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP) {
		switch (tipoOTP) {
		case ninguno:
			contenedorASM.setVisibility(View.GONE);
			campoASM.setVisibility(View.GONE);
			asm.setVisibility(View.GONE);
			asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
			break;
		case codigo:
		case registro:
			contenedorASM.setVisibility(View.VISIBLE);
			campoASM.setVisibility(View.VISIBLE);
			asm.setVisibility(View.VISIBLE);
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(
					Constants.ASM_LENGTH);
			asm.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			cambiarAccionTexto(nip);
			asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
			break;
		}

		Constants.TipoInstrumento tipoInstrumento = confirmacionAutenticacionDelegate
				.consultaTipoInstrumentoSeguridad();

		switch (tipoInstrumento) {
		case OCRA:
			campoASM.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoOCRA());
			//asm.setTransformationMethod(null);
			break;
		case DP270:
			campoASM.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoDP270());
			//asm.setTransformationMethod(null);
			break;
		case SoftToken:
			if (SuiteApp.getSofttokenStatus()) {
				asm.setText(Constants.DUMMY_OTP);
				asm.setEnabled(false);
				campoASM.setText(confirmacionAutenticacionDelegate
						.getEtiquetaCampoSoftokenActivado());
			} else {
				asm.setText("");
				asm.setEnabled(true);
				campoASM.setText(confirmacionAutenticacionDelegate
						.getEtiquetaCampoSoftokenDesactivado());
				//asm.setTransformationMethod(null);
			}
			// String otp = GeneraOTPSTDelegate.generarOtpTiempo();
			// if(null != otp) {
			// asm.setText(otp);
			// asm.setEnabled(false);
			// }
			// campoASM.setText(confirmacionAutenticacionDelegate.getOperationDelegate().getEtiquetaCampoSoftokenActivado());
			// break;
		default:
			break;
		}
		String instrucciones = confirmacionAutenticacionDelegate
				.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
		if (instrucciones.equals("")) {
			instruccionesASM.setVisibility(View.GONE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}
	}

	private void cambiarAccionTexto(EditText campo) {
		if (campo.getVisibility() == View.VISIBLE) {
			campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
	}

	public String pideContrasena() {
		if (contrasena.getVisibility() == View.GONE) {
			return "";
		} else {
			return contrasena.getText().toString();
		}
	}

	public String pideNIP() {
		if (nip.getVisibility() == View.GONE) {
			return "";
		} else {
			return nip.getText().toString();
		}
	}

	public String pideASM() {
		if (asm.getVisibility() == View.GONE) {
			return "";
		} else {
			return asm.getText().toString();
		}
	}

	public String pideCVV() {
		if (cvv.getVisibility() == View.GONE) {
			return "";
		} else {
			return cvv.getText().toString();
		}
	}

	/*
	* 
	*/
	public void mostrarCVV(boolean visibility) {
		contenedorCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
		campoCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
		cvv.setVisibility(visibility ? View.VISIBLE : View.GONE);
		instruccionesCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);

		if (visibility) {
			campoCVV.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoCVV());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(
					Constants.CVV_LENGTH);
			cvv.setFilters(userFilterArray);
			String instrucciones = confirmacionAutenticacionDelegate
					.getTextoAyudaCVV();
			instruccionesCVV.setText(instrucciones);
			instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE
					: View.VISIBLE);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			cambiarAccionTexto(nip);
			cambiarAccionTexto(asm);
			cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}

	@Override
	public void onClick(View v) {
		confirmarButton.setEnabled(false);
		if (v == confirmarButton && !parentViewsController.isActivityChanging()) {
			botonConfirmarClick();
		}
	}

	public void botonConfirmarClick() {
		confirmacionAutenticacionDelegate.enviaPeticionOperacion();
		if(confirmacionAutenticacionDelegate.res == true){
			//AMZ inicio
			titulo = getString(confirmacionAutenticacionDelegate.getOperationDelegate().getTextoEncabezado());
			
			Map<String,Object> OperacionRealizadaMap = new HashMap<String, Object>();
				if(titulo == getString(R.string.bmovil_consultar_dineromovil_titulo)){
					//AMZ
					OperacionRealizadaMap.put("evento_realizada","event52");
					OperacionRealizadaMap.put("&&products","operaciones;cancelar dinero movil");
					OperacionRealizadaMap.put("eVar12","operacion_cancelada");
					TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);

				}else if(titulo == getString(R.string.bmovil_cambio_cuenta_title)){
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","operaciones;admin+cambio cuenta");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == getString(R.string.bmovil_configurar_correo_titulo)){
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","operaciones;admin+configura correo");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == getString(R.string.bmovil_cambio_telefono_title)){
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","operaciones;admin+cambio celular");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == getString(R.string.bmovil_suspender_title)){
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","operaciones;admin+suspender o cancelar");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == getString(R.string.bmovil_cancelar_title)){
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","operaciones;admin+suspender o cancelar");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}else if(titulo == getString(R.string.bmovil_configurar_montos_title)){
					//AMZ
					OperacionRealizadaMap.put("evento_realizada","event52");
					OperacionRealizadaMap.put("&&products","operaciones;admin+configura montos");
					OperacionRealizadaMap.put("eVar12","operacion realizada");
					TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
				}
			}
	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		confirmacionAutenticacionDelegate
				.analyzeResponse(operationId, response);
	}

	private void findViews() {
		
		
		contenedorPrincipal = (LinearLayout) findViewById(R.id.confirmacion_lista_datos);
		contenedorContrasena = (LinearLayout) findViewById(R.id.campo_confirmacion_contrasena_layout);
		contenedorNIP = (LinearLayout) findViewById(R.id.campo_confirmacion_nip_layout);
		contenedorASM = (LinearLayout) findViewById(R.id.campo_confirmacion_asm_layout);
		contenedorCVV = (LinearLayout) findViewById(R.id.campo_confirmacion_cvv_layout);

		contrasena = (EditText) contenedorContrasena
				.findViewById(R.id.confirmacion_contrasena_edittext);
		nip = (EditText) contenedorNIP
				.findViewById(R.id.confirmacion_nip_edittext);
		asm = (EditText) contenedorASM
				.findViewById(R.id.confirmacion_asm_edittext);
		cvv = (EditText) contenedorCVV
				.findViewById(R.id.confirmacion_cvv_edittext);

		campoContrasena = (TextView) contenedorContrasena
				.findViewById(R.id.confirmacion_contrasena_label);
		campoNIP = (TextView) contenedorNIP
				.findViewById(R.id.confirmacion_nip_label);
		campoASM = (TextView) contenedorASM
				.findViewById(R.id.confirmacion_asm_label);
		campoCVV = (TextView) contenedorCVV
				.findViewById(R.id.confirmacion_cvv_label);

		instruccionesContrasena = (TextView) contenedorContrasena
				.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
		instruccionesNIP = (TextView) contenedorNIP
				.findViewById(R.id.confirmacion_nip_instrucciones_label);
		instruccionesASM = (TextView) contenedorASM
				.findViewById(R.id.confirmacion_asm_instrucciones_label);
		instruccionesCVV = (TextView) contenedorCVV
				.findViewById(R.id.confirmacion_cvv_instrucciones_label);

		contenedorCampoTarjeta = (LinearLayout) findViewById(R.id.campo_confirmacion_campotarjeta_layout);
		tarjeta = (EditText) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
		campoTarjeta = (TextView) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
		instruccionesTarjeta = (TextView) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);



		confirmarButton = (ImageButton) findViewById(R.id.confirmacion_confirmar_button);
	}

	private void scaleToScreenSize() {
		
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		// guiTools.scaleAll(contenedorPrincipal);

		// guiTools.scaleAll((LinearLayout)findViewById(R.id.confirmacion_parent_parent_view));

		guiTools.scale(contenedorPrincipal);
		guiTools.scale(findViewById(R.id.confirmacion_campos_layout));

		guiTools.scale(contenedorContrasena);
		guiTools.scale(contenedorNIP);
		guiTools.scale(contenedorASM);
		guiTools.scale(contenedorCVV);

		guiTools.scale(contrasena, true);
		guiTools.scale(nip, true);
		guiTools.scale(asm, true);
		guiTools.scale(cvv, true);

		guiTools.scale(campoContrasena, true);
		guiTools.scale(campoNIP, true);
		guiTools.scale(campoASM, true);
		guiTools.scale(campoCVV, true);

		guiTools.scale(instruccionesContrasena, true);
		guiTools.scale(instruccionesNIP, true);
		guiTools.scale(instruccionesASM, true);
		guiTools.scale(instruccionesCVV, true);

		// nuevo campo
		guiTools.scale(contenedorCampoTarjeta);
		guiTools.scale(tarjeta, true);
		guiTools.scale(campoTarjeta, true);
		guiTools.scale(instruccionesTarjeta, true);

		guiTools.scale(confirmarButton);

	}

	public void limpiarCampos() {
		contrasena.setText("");
		nip.setText("");
		asm.setText("");
		cvv.setText("");
		tarjeta.setText("");
	}

	public void habilitarBtnContinuar() {

		confirmarButton.setEnabled(true);
	}

	public String pideTarjeta() {
		if (tarjeta.getVisibility() == View.GONE) {
			return "";
		} else {
			return tarjeta.getText().toString();
		}
	}

	private void mostrarCampoTarjeta(boolean visibility) {
		contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE
				: View.GONE);
		campoTarjeta.setVisibility(visibility ? View.VISIBLE : View.GONE);
		tarjeta.setVisibility(visibility ? View.VISIBLE : View.GONE);
		if (visibility) {
			campoTarjeta.setText(confirmacionAutenticacionDelegate
					.getEtiquetaCampoTarjeta());
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(5);
			tarjeta.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
			String instrucciones = confirmacionAutenticacionDelegate
					.getTextoAyudaTarjeta();
			if (instrucciones.equals("")) {
				instruccionesTarjeta.setVisibility(View.GONE);
			} else {
				instruccionesTarjeta.setVisibility(View.VISIBLE);
				instruccionesTarjeta.setText(instrucciones);
			}
		} else {
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}

	}

	@Override
	public void returnToPrincipal() {
		// TODO Auto-generated method stub

	}

	@Override
	public void returnDataFromModule(String operation,
									 bancomer.api.common.io.ServerResponse response) {
		this.ocultaIndicadorActividad();
		// Consulta Otros Creditos
		MisCuentasDelegate delegateC = (MisCuentasDelegate) ((BmovilViewsController)getParentViewsController()).getBaseDelegateForKey(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID);
		if (delegateC == null) {
			delegateC = new MisCuentasDelegate();
			((BmovilViewsController)getParentViewsController()).addDelegateToHashMap(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID,
					delegateC);
		}

		ocultaIndicadorActividad();

		ConsultaOtrosCreditosData consultaOtrosCreditosData = (ConsultaOtrosCreditosData) response.getResponse();

		delegateC.setConsultaOtrosCreditosData(consultaOtrosCreditosData);

		((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showMisCuentasViewController();

	}

}
