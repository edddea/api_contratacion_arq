package suitebancomer.aplicaciones.bmovil.classes.common;

import suitebancomer.classes.gui.controllers.BaseViewController;
import android.text.Editable;
import android.text.TextWatcher;

/**
 * Custom TextWatcher to ensure the call of the onUserInteraction method when the text is changed.
 * @see BaseViewController#onUserInteraction()
 */
public class BmovilTextWatcher implements TextWatcher{
	/**
	 * The controller that holds the editable text field.
	 */
	private BaseViewController controller;
	
	/**
	 * Constructor.
	 * @param controller The controller that holds the editable text field. 
	 */
	public BmovilTextWatcher(BaseViewController controller) {
		this.controller = controller;
	}

	@Override
	public void afterTextChanged(Editable s) {
		if(null != controller)
			controller.onUserInteraction();
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {}
}
