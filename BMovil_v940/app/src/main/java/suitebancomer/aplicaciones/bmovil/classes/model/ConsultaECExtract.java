/*
 * Copyright (c) 2009 BBVA. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
//import suitebancomer.aplicaciones.bmovil.classes.common.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

/**
 * ConsultaECExtract wraps an account periods, the list of the last 12 periods
 * 
 * @author Carlos Santoyo
 */
public class ConsultaECExtract  implements ParsingHandler {

    /**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 3272545845918202838L;

    /**
     * The periods
     */
    private ArrayList<Periodo> periodos = null;

    /**
     * Default constructor
     */
    public ConsultaECExtract() {
    	setPeriodos(new ArrayList<Periodo>());
    }

    /**
     * Constructor with parameters
 
     * @param periodos the periodos Array
     */
    public ConsultaECExtract(ArrayList<Periodo> periodos) {

        this.setPeriodos(periodos);
        
    }


    /**
     * Parse periodos from parser data
     * @param parser the parser
     * @return the movements
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     * @throws NumberFormatException on format errors 
     */
    private ArrayList<Periodo> parsePeriodos(ParserJSON parser) throws IOException, ParsingException {
        ArrayList<Periodo>result = new ArrayList<Periodo>();
        String ocPeriodos = parser.parseNextValue("arrPeriodosEC");
        JSONArray arrayPeriodos = new JSONArray();
        try {
			JSONObject jsonObject = new JSONObject(ocPeriodos);
			arrayPeriodos = jsonObject.getJSONArray("ocPeriodos");
		} catch (JSONException e) {
			if(Server.ALLOW_LOG) e.printStackTrace();
		}
        int numPeriodos = arrayPeriodos.length();
       for(int i = 0; i < numPeriodos; i++) {
    	    
			try {
				
				JSONObject periodoObj = arrayPeriodos.getJSONObject(i);
				//String periodo = Tools.parsePeriodoFecha(periodoObj.getString("periodoEC"));
				String periodo = periodoObj.getString("periodoEC");
				String fechaCorte = periodoObj.getString("fechaCorte");
				String referencia = periodoObj.getString("referencia");
				Periodo objPeriodo = new Periodo(periodo,fechaCorte,referencia);
				
				result.add(objPeriodo);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				if(Server.ALLOW_LOG) e.printStackTrace();
			}
     
        }
		if(Server.ALLOW_LOG) Log.d("TEMA",result.size()+"");
        return result;
    }

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
        this.setPeriodos(parsePeriodos(parser));
	}

	public ArrayList<Periodo> getPeriodos() {
	    return periodos;
	}

	public void setPeriodos(ArrayList<Periodo> periodos) {
	    this.periodos = periodos;
	}

    
}
