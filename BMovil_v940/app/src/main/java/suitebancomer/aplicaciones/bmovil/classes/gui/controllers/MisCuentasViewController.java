package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TextView;

import com.bancomer.base.callback.CallBackAPI;
import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.base.callback.CallBackSession;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.consultainversiones.models.ConsultaInversionesPlazoData;
import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;
import bancomer.api.consultaotroscreditos.models.Creditos;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.SincronizarSesion;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.common.ToolsCommons;
import suitebancomer.aplicaciones.bmovil.classes.entrada.InitContratacion;
import suitebancomer.aplicaciones.bmovil.classes.entrada.InitializeApiAdmon;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.MenuHamburguesaViewsControllers;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MisCuentasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioPerfilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.controllers.MenuSuiteViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import tracking.TrackingHelper;

import android.widget.ImageView;
import bancomer.api.pagarcreditos.models.ImportesPagoCreditoData;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;

/**
 * 
 * @author Luis Alberto de la Cruz Torres
 *
 */
public class MisCuentasViewController extends BaseViewController  implements CallBackModule, CallBackSession, CallBackAPI {

	LinearLayout vista;
	ListaSeleccionViewController listaSeleccion;
	ArrayList<Object> lista;
	ArrayList<Object> lista2;
	ArrayList<Object> lista3;
	ArrayList<Object> lista4;
	MisCuentasDelegate delegate;
	MisCuentasOpcionesViewController misCuentasOpciones;
	ListView listview;
	ListView listview2;
	ListView listview3;
	ListView listview4;
	int opcionSeleccionada;
	LlenarTablaCuentasEnPesos adapter;
	LlenarTablaTarjetas adapter2;
	LlenarTablaOtrosCreditos adapter3;
	LlenarTablaInversionesAPlazo adapter4;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ

	/**
	 * Default constructor for this activity
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_mis_cuentas);
		marqueeEnabled = true;
		setTitle(R.string.mis_cuentas_title, R.drawable.bmovil_mis_cuentas_icono);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("mis cuentas", parentManager.estados);

		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		delegate = (MisCuentasDelegate) parentViewsController.getBaseDelegateForKey(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID);
		if(delegate==null){
			delegate = new MisCuentasDelegate();
			parentViewsController.addDelegateToHashMap(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID,delegate);
		}
		delegate.setMisCuentasViewController(this);

		vista = (LinearLayout) findViewById(R.id.misCuentas_layout);
		listview = (ListView) findViewById(R.id.mis_cuentas_tabla1_View);
		listview2 = (ListView) findViewById(R.id.mis_cuentas_tabla2_View);
		listview3 = (ListView) findViewById(R.id.mis_cuentas_tabla3_View);
		listview4 = (ListView) findViewById(R.id.mis_cuentas_tabla4_View);

		Session session = Session.getInstance(SuiteApp.getInstance());

		if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.consultarCreditos, session.getClientProfile()) &&
				delegate.getConsultaOtrosCreditosData() != null &&
				delegate.getConsultaOtrosCreditosData().getListaCreditos() != null &&
				!delegate.getConsultaOtrosCreditosData().getListaCreditos().isEmpty()) {
			delegate.procesaOtrosCreditos();
		} else {
			ocultaTablaOtrosCreditos();
		}

		llenarTablas();
		llenarTablaInversiones();
	}

	public void ocultaTablaOtrosCreditos() {
		listview3.setVisibility(View.GONE);
		((TableLayout) findViewById(R.id.mis_cuentas_cabecera_tabla3)).setVisibility(View.GONE);
	}

	public void ocultaTablaInversionesPlazo() {
		listview4.setVisibility(View.GONE);
		((TableLayout) findViewById(R.id.mis_cuentas_cabecera_tabla4)).setVisibility(View.GONE);
		lista4 = new ArrayList<Object>();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}

		getParentViewsController().setCurrentActivityApp(this);

		if (misCuentasOpciones != null && misCuentasOpciones.isShowing()) {
			misCuentasOpciones.dismiss();
			misCuentasOpciones = null;
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (parentViewsController.consumeAccionesDePausa()) {
			return;
		}

		if (misCuentasOpciones != null && misCuentasOpciones.isShowing()) {
			misCuentasOpciones.dismiss();
			misCuentasOpciones = null;
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (misCuentasOpciones != null) {
			if (misCuentasOpciones.isShowing()) {
				misCuentasOpciones.dismiss();
				misCuentasOpciones = null;
			}
		}
	}

	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID);
		super.goBack();
	}

	@SuppressWarnings("unchecked")
	public void llenarTablas() {
		// perfil avanzado
		if (Session.getInstance(SuiteApp.appContext).getClientProfile() == Constants.Perfil.avanzado) {
			ArrayList<Account> cuentas;
			ArrayList<Object> registros;
			ArrayList<Object> encabezado;

			ArrayList<Object> gruposDeCuentas = new ArrayList<Object>();
			gruposDeCuentas = delegate.agruparCuentas();
			int numeroTablas = gruposDeCuentas.size();
			if (numeroTablas == 1) {
				((TableLayout) findViewById(R.id.mis_cuentas_cabecera_tabla2)).setVisibility(View.GONE);
				listview2.setVisibility(View.GONE);
			}
			ArrayList<String> titulos = new ArrayList<String>();
			for (int i = 0; i < numeroTablas; i++) {
				if (i == 0) {
					titulos.add(getString(R.string.misCuentas_tablaCuentas));
				}
				if (i == 1) {
					titulos.add(getString(R.string.misCuentas_tablaCredito));
				}
			}
			for (int a = 0; a < numeroTablas; a++) {
				double saldoCuentas = 0;

				encabezado = new ArrayList<Object>();
				encabezado.add(null);
				encabezado.add(titulos.get(a));
				cuentas = (ArrayList<Account>) gruposDeCuentas.get(a);

				ArrayList<Object> listaTmp = new ArrayList<Object>();
				Account currentAccount = null;
				for (int i = 0; i < cuentas.size(); i++) {
					registros = new ArrayList<Object>();
					currentAccount = cuentas.get(i);
					registros.add(currentAccount);
					registros.add(currentAccount.getPublicName(getResources(), false));
					String saldonuevo = Tools.convertDoubleToBigDecimalAndReturnString(cuentas.get(i).getBalance());
					registros.add(Tools.formatAmount(saldonuevo, cuentas.get(i).getBalance() < 0));
					listaTmp.add(registros);

					saldoCuentas = saldoCuentas + cuentas.get(i).getBalance();
				}
				String saldo = Tools.convertDoubleToBigDecimalAndReturnString(saldoCuentas);
				encabezado.add(Tools.formatAmount(saldo, saldoCuentas < 0));
				llenarEncabezados(encabezado, a, listaTmp);

			}
		} else if ((Session.getInstance(SuiteApp.appContext).getClientProfile() == Constants.Perfil.basico)
				|| (Session.getInstance(SuiteApp.appContext).getClientProfile() == Constants.Perfil.recortado)) {

			ArrayList<Account> cuentas;
			ArrayList<Object> registros;
			ArrayList<Object> encabezado;

			ArrayList<Object> gruposDeCuentas = new ArrayList<Object>();
			ArrayList<Account> cuentaEje = new ArrayList<Account>();
			Account cEje = Tools.obtenerCuentaEje();
			cuentaEje.add(cEje);
			gruposDeCuentas.add(cuentaEje);
			boolean esCredito = (cEje.getType().equals(Constants.CREDIT_TYPE));

			int numeroTablas = 1;

			if (numeroTablas == 1) {
				((TableLayout) findViewById(R.id.mis_cuentas_cabecera_tabla2)).setVisibility(View.GONE);
				listview2.setVisibility(View.GONE);
			}
			ArrayList<String> titulos = new ArrayList<String>();
			if (esCredito) {
				titulos.add(getString(R.string.misCuentas_tablaCredito));
			} else {
				titulos.add(getString(R.string.misCuentas_tablaCuentas));
			}

			for (int a = 0; a < numeroTablas; a++) {
				double saldoCuentas = 0;

				encabezado = new ArrayList<Object>();
				encabezado.add(null);
				encabezado.add(titulos.get(a));
				cuentas = (ArrayList<Account>) gruposDeCuentas.get(a);

				ArrayList<Object> listaTmp = new ArrayList<Object>();
				Account currentAccount = null;

				for (int i = 0; i < cuentas.size(); i++) {
					registros = new ArrayList<Object>();
					currentAccount = cuentas.get(i);
					registros.add(currentAccount);
					registros.add(currentAccount.getPublicName(getResources(), false));
					String saldonuevo = Tools.convertDoubleToBigDecimalAndReturnString(cuentas.get(i).getBalance());
					registros.add(Tools.formatAmount(saldonuevo, cuentas.get(i).getBalance() < 0));
					listaTmp.add(registros);
					saldoCuentas = saldoCuentas + cuentas.get(i).getBalance();
				}

				if ((Session.getInstance(SuiteApp.appContext).getClientProfile() == Constants.Perfil.basico)
						&& Session.getInstance(SuiteApp.appContext).getAccounts().length > 1
						&& (cEje != null && !cEje.getType().equals(Constants.CREDIT_TYPE))
						&& !cEje.getType().equals(Constants.EXPRESS_TYPE)) {
					TextView verMasCuentas = (TextView) findViewById(R.id.linkVerMasCuentas);
					verMasCuentas.setVisibility(View.VISIBLE);
					GuiTools guiTools = GuiTools.getCurrent();
					guiTools.init(getWindowManager());
					guiTools.scale(verMasCuentas, true);
				}

				String saldo = Tools.convertDoubleToBigDecimalAndReturnString(saldoCuentas);
				encabezado.add(Tools.formatAmount(saldo, saldoCuentas < 0));
				llenarEncabezados(encabezado, a, listaTmp);
			}
		}

	}

	public void llenarTablaInversiones() {

		ArrayList<Account> inversiones = delegate.getInversiones();

		if ((inversiones != null) && (!inversiones.isEmpty())
				&& Autenticacion.getInstance().isVisible(Constants.Operacion.consultarInversiones, Session.getInstance(SuiteApp.appContext).getClientProfile())) {
			ArrayList<Object> encabezado = new ArrayList<Object>();
			encabezado.add(null);
			encabezado.add(getString(R.string.bmovil_menu_miscuentas_consultaInversionesPlazo_tabla_titulo));

			ArrayList<Object> listaTmp = new ArrayList<Object>();

			Account inversion = null;
			ArrayList<Object> registros;
			Double saldoActual;
			Double saldoTotal = 0.0;
			for (int i = 0; i < inversiones.size(); i++) {
				registros = new ArrayList<Object>();

				inversion = inversiones.get(i);
				registros.add(inversion);
				registros.add(ToolsCommons.hideAccountNumber(inversion.getNumber()));

				saldoActual = inversion.getBalance();
				Boolean neg = false;
				if (saldoActual > 0) {
					if (saldoActual < 0) neg = true;
					registros.add(Tools.formatAmount(Tools.convertDoubleToBigDecimalAndReturnString(inversion.getBalance()), neg));

					listaTmp.add(registros);
				}
				saldoTotal = saldoTotal + saldoActual;
			}
			String saldo = Tools.convertDoubleToBigDecimalAndReturnString(saldoTotal);
			encabezado.add(Tools.formatAmount(saldo, saldoTotal < 0));
			if (!listaTmp.isEmpty()) {
				llenarEncabezados(encabezado, 3, listaTmp);
			} else {
				ocultaTablaInversionesPlazo();
			}
		} else {
			ocultaTablaInversionesPlazo();
		}
	}

	public void seleccionaCuenta(Account cuenta) {
		//muestra mis cuentas quiero
		misCuentasOpciones = new MisCuentasOpcionesViewController(this, (BmovilViewsController) parentViewsController, cuenta, delegate);
	}

	public void seleccionaCredito(Creditos credito) {
		//muestra mis cuentas quiero
		misCuentasOpciones = new MisCuentasOpcionesViewController(this, (BmovilViewsController) parentViewsController, credito, delegate);
	}

	public MisCuentasOpcionesViewController getMisCuentasOpciones() {
		return misCuentasOpciones;
	}

	/**
	 * Métodos para llenar los encabezados de las tablas
	 */
	public void llenarEncabezados(ArrayList<Object> encabezado, int numeroTabla, ArrayList<Object> lista) {
		if (numeroTabla == 0) {
			this.lista = lista;
			TextView celda1 = (TextView) findViewById(R.id.mis_cuentas_cabecera_celda_1);
			celda1.setText((String) encabezado.get(1));

			TextView celda2 = (TextView) findViewById(R.id.mis_cuentas_cabecera_celda_2);
			celda2.setText((String) encabezado.get(2));
			celda2.setGravity(Gravity.RIGHT);
			celda2.setSelected(true);
			celda2.setSingleLine(true);

			adapter = new LlenarTablaCuentasEnPesos();
			listview.setAdapter(adapter);

			ListAdapter listAdapter = listview.getAdapter();
			int totalHeight = 0;
			for (int i = 0; i < listAdapter.getCount(); i++) {
				View listItem = listAdapter.getView(i, null, listview);
				listItem.measure(0, 0);
				totalHeight += listItem.getMeasuredHeight();
			}

			ViewGroup.LayoutParams params = listview.getLayoutParams();
			params.height = totalHeight
					+ (listview.getDividerHeight() * (listview.getCount() - 1));
			listview.setLayoutParams(params);
			listview.requestLayout();
		} else if (numeroTabla == 1) {
			this.lista2 = lista;
			TextView celda1 = (TextView) findViewById(R.id.mis_cuentas_cabecera_celda_3);
			celda1.setText((String) encabezado.get(1));

			TextView celda2 = (TextView) findViewById(R.id.mis_cuentas_cabecera_celda_4);
			celda2.setText((String) encabezado.get(2));
			celda2.setGravity(Gravity.RIGHT);
			celda2.setSelected(true);
			celda2.setSingleLine(true);
			adapter2 = new LlenarTablaTarjetas();
			listview2.setAdapter(adapter2);

			ListAdapter listAdapter = listview2.getAdapter();
			int totalHeight = 0;
			for (int i = 0; i < listAdapter.getCount(); i++) {
				View listItem = listAdapter.getView(i, null, listview2);
				listItem.measure(0, 0);
				totalHeight += listItem.getMeasuredHeight();
			}

			ViewGroup.LayoutParams params = listview2.getLayoutParams();
			params.height = totalHeight
					+ (listview2.getDividerHeight() * (listview2.getCount() - 1));
			listview2.setLayoutParams(params);
			listview2.requestLayout();
		} else if (numeroTabla == 2) {
			this.lista3 = lista;
			TextView celda1 = (TextView) findViewById(R.id.mis_cuentas_cabecera_celda_5);
			celda1.setText((String) encabezado.get(1));

			TextView celda2 = (TextView) findViewById(R.id.mis_cuentas_cabecera_celda_6);
			celda2.setText((String) encabezado.get(2));
			celda2.setGravity(Gravity.RIGHT);
			celda2.setSelected(true);
			celda2.setSingleLine(true);
			adapter3 = new LlenarTablaOtrosCreditos();
			listview3.setAdapter(adapter3);

			ListAdapter listAdapter = listview3.getAdapter();
			int totalHeight = 0;
			for (int i = 0; i < listAdapter.getCount(); i++) {
				View listItem = listAdapter.getView(i, null, listview3);
				listItem.measure(0, 0);
				totalHeight += listItem.getMeasuredHeight();
			}

			ViewGroup.LayoutParams params = listview3.getLayoutParams();
			params.height = totalHeight
					+ (listview3.getDividerHeight() * (listview3.getCount() - 1));
			listview3.setLayoutParams(params);
			listview3.requestLayout();
		} else if (numeroTabla == 3) {
			this.lista4 = lista;


			TextView celda1 = (TextView) findViewById(R.id.mis_cuentas_cabecera_celda_7);
			celda1.setText((String) encabezado.get(1));

			TextView celda2 = (TextView) findViewById(R.id.mis_cuentas_cabecera_celda_8);
			celda2.setText((String) encabezado.get(2));
			celda2.setGravity(Gravity.RIGHT);
			celda2.setSelected(true);
			celda2.setSingleLine(true);
			adapter4 = new LlenarTablaInversionesAPlazo();
			listview4.setAdapter(adapter4);

			ListAdapter listAdapter = listview4.getAdapter();
			int totalHeight = 0;
			for (int i = 0; i < listAdapter.getCount(); i++) {
				View listItem = listAdapter.getView(i, null, listview4);
				listItem.measure(0, 0);
				totalHeight += listItem.getMeasuredHeight();
			}

			ViewGroup.LayoutParams params = listview4.getLayoutParams();
			params.height = totalHeight
					+ (listview4.getDividerHeight() * (listview4.getCount() - 1));
			listview4.setLayoutParams(params);
			listview4.requestLayout();
		}
	}
	@Override
	public void userInteraction() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();
	}

	@Override
	public void cierraSesionBackground(Boolean isChanging) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		parentViewsController.setActivityChanging(isChanging);
		if (parentManager.consumeAccionesDePausa()) {
			finish();
			if (MenuHamburguesaViewsControllers.getIsOterApp()) {
				MenuHamburguesaViewsControllers.setIsOterApp(Boolean.FALSE);
			}
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	public void cierraSesion() {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication()
				.setApplicationInBackground(Boolean.FALSE);
		SuiteApp.getInstance().getBmovilApplication().logoutApp(Boolean.FALSE);
	}

	@Override
	public void returnMenuPrincipal() {
		Session.getInstance(SuiteApp.appContext).reloadFiles();
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(Boolean.TRUE);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(true);
	}

	@Override
	public void closeSession(String userName, String ium, String client) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getBmovilApp().closeSession(userName, ium, client);
	}

	private class LlenarTablaCuentasEnPesos extends BaseAdapter {

		@Override
		public int getCount() {
			return lista.size();
		}

		@Override
		public Object getItem(int position) {
			return lista.get(position);
		}

		@SuppressWarnings({"unchecked", "deprecation"})
		@Override
		public View getView(final int position, View convertView,
							ViewGroup parent) {

			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.list_item_productos, null);
			}

			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					opcionSeleccionada = position;
					delegate.performAction(((ArrayList<Object>) lista.get(opcionSeleccionada)).get(0));
				}
			});
			TextView celda1 = (TextView) convertView.findViewById(R.id.lista_celda_1);
			TextView celda2 = (TextView) convertView.findViewById(R.id.lista_celda_2);
			TextView celda3 = (TextView) convertView.findViewById(R.id.lista_celda_3);
			celda3.setVisibility(View.GONE);
			TextView celda4 = (TextView) convertView.findViewById(R.id.lista_celda_4);
			celda4.setVisibility(View.GONE);
			ImageButton celda5 = (ImageButton) convertView.findViewById(R.id.lista_celda_check);
			celda5.setVisibility(View.GONE);
			ArrayList<Object> registro = (ArrayList<Object>) lista.get(position);

			LinearLayout.LayoutParams params = new LayoutParams(0, LayoutParams.FILL_PARENT, 0.6f);
			celda1.setText((String) registro.get(1));
			celda1.setLayoutParams(params);
			celda1.setGravity(Gravity.LEFT);
			celda1.setTextAppearance(getApplicationContext(), R.style.movimientos_celda_style);
			//celda1.setTextColor(getResources().getColor(R.color.cuarto_azul));
			//celda1.setPaintFlags(celda1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

			LinearLayout.LayoutParams params2 = new LayoutParams(0, LayoutParams.FILL_PARENT, 0.4f);
			celda2.setText((String) registro.get(2));
			celda2.setLayoutParams(params2);
			celda2.setGravity(Gravity.RIGHT);
			celda2.setTextAppearance(getApplicationContext(), R.style.movimientos_celda_style);
			celda2.setSelected(marqueeEnabled);
			celda2.setSingleLine(true);
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
	}

	private class LlenarTablaTarjetas extends BaseAdapter {

		@Override
		public int getCount() {
			return lista2.size();
		}

		@Override
		public Object getItem(int position) {
			return lista2.get(position);
		}

		@SuppressWarnings({"unchecked", "deprecation"})
		@Override
		public View getView(final int position, View convertView,
							ViewGroup parent) {

			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.list_item_productos, null);
			}

			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					opcionSeleccionada = position;
					delegate.performAction(((ArrayList<Object>) lista2.get(opcionSeleccionada)).get(0));
				}
			});
			TextView celda1 = (TextView) convertView.findViewById(R.id.lista_celda_1);
			TextView celda2 = (TextView) convertView.findViewById(R.id.lista_celda_2);
			TextView celda3 = (TextView) convertView.findViewById(R.id.lista_celda_3);
			celda3.setVisibility(View.GONE);
			TextView celda4 = (TextView) convertView.findViewById(R.id.lista_celda_4);
			celda4.setVisibility(View.GONE);
			ImageButton celda5 = (ImageButton) convertView.findViewById(R.id.lista_celda_check);
			celda5.setVisibility(View.GONE);
			ArrayList<Object> registro = (ArrayList<Object>) lista2.get(position);

			LinearLayout.LayoutParams params = new LayoutParams(0, LayoutParams.FILL_PARENT, 0.6f);
			celda1.setText((String) registro.get(1));
			celda1.setLayoutParams(params);
			celda1.setGravity(Gravity.LEFT);
			celda1.setTextAppearance(getApplicationContext(), R.style.movimientos_celda_style);
			//celda1.setTextColor(getResources().getColor(R.color.cuarto_azul));
			//celda1.setPaintFlags(celda1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

			LinearLayout.LayoutParams params2 = new LayoutParams(0, LayoutParams.FILL_PARENT, 0.4f);
			celda2.setText((String) registro.get(2));
			celda2.setLayoutParams(params2);
			celda2.setGravity(Gravity.RIGHT);
			celda2.setTextAppearance(getApplicationContext(), R.style.movimientos_celda_style);
			celda2.setSelected(true);
//			celda2.setTextColor(getResources().getColor(R.color.cuarto_azul));
//			celda2.setPaintFlags(celda1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
			celda2.setSingleLine(true);
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
	}

	private class LlenarTablaOtrosCreditos extends BaseAdapter {

		@Override
		public int getCount() {
			return lista3.size();
		}

		@Override
		public Object getItem(int position) {
			return lista3.get(position);
		}

		@SuppressWarnings({"unchecked", "deprecation"})
		@Override
		public View getView(final int position, View convertView,
							ViewGroup parent) {

			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.list_item_productos, null);
			}

			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					opcionSeleccionada = position;
					delegate.performAction(((ArrayList<Object>) lista3.get(opcionSeleccionada)).get(0));
				}
			});
			TextView celda1 = (TextView) convertView.findViewById(R.id.lista_celda_1);
			TextView celda2 = (TextView) convertView.findViewById(R.id.lista_celda_2);
			TextView celda3 = (TextView) convertView.findViewById(R.id.lista_celda_3);
			celda3.setVisibility(View.GONE);
			TextView celda4 = (TextView) convertView.findViewById(R.id.lista_celda_4);
			celda4.setVisibility(View.GONE);
			ImageButton celda5 = (ImageButton) convertView.findViewById(R.id.lista_celda_check);
			celda5.setVisibility(View.GONE);
			ArrayList<Object> registro = (ArrayList<Object>) lista3.get(position);

			LinearLayout.LayoutParams params = new LayoutParams(0, LayoutParams.FILL_PARENT, 0.6f);
			celda1.setText((String) registro.get(1));
			celda1.setLayoutParams(params);
			celda1.setGravity(Gravity.LEFT);
			celda1.setTextAppearance(getApplicationContext(), R.style.movimientos_celda_style);
			//celda1.setTextColor(getResources().getColor(R.color.cuarto_azul));
			//celda1.setPaintFlags(celda1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

			LinearLayout.LayoutParams params2 = new LayoutParams(0, LayoutParams.FILL_PARENT, 0.4f);
			celda2.setText((String) registro.get(2));
			celda2.setLayoutParams(params2);
			celda2.setGravity(Gravity.RIGHT);
			celda2.setTextAppearance(getApplicationContext(), R.style.movimientos_celda_style);
			celda2.setSelected(true);
//			celda2.setTextColor(getResources().getColor(R.color.cuarto_azul));
//			celda2.setPaintFlags(celda1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
			celda2.setSingleLine(true);
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
	}

	private class LlenarTablaInversionesAPlazo extends BaseAdapter {

		@Override
		public int getCount() {
			return lista4.size();
		}

		@Override
		public Object getItem(int position) {
			return lista4.get(position);
		}

		@SuppressWarnings({"unchecked", "deprecation"})
		@Override
		public View getView(final int position, View convertView,
							ViewGroup parent) {

			if (convertView == null) {
				convertView = getLayoutInflater().inflate(R.layout.list_item_productos, null);
			}

			convertView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					opcionSeleccionada = position;
					delegate.performAction(((ArrayList<Object>) lista4.get(opcionSeleccionada)).get(0));
				}
			});
			TextView celda1 = (TextView) convertView.findViewById(R.id.lista_celda_1);
			TextView celda2 = (TextView) convertView.findViewById(R.id.lista_celda_2);
			TextView celda3 = (TextView) convertView.findViewById(R.id.lista_celda_3);
			celda3.setVisibility(View.GONE);
			TextView celda4 = (TextView) convertView.findViewById(R.id.lista_celda_4);
			celda4.setVisibility(View.GONE);
			ImageButton celda5 = (ImageButton) convertView.findViewById(R.id.lista_celda_check);
			celda5.setVisibility(View.GONE);
			ArrayList<Object> registro = (ArrayList<Object>) lista4.get(position);

			LinearLayout.LayoutParams params = new LayoutParams(0, LayoutParams.FILL_PARENT, 0.6f);
			celda1.setText((String) registro.get(1));
			celda1.setLayoutParams(params);
			celda1.setGravity(Gravity.LEFT);
			celda1.setTextAppearance(getApplicationContext(), R.style.movimientos_celda_style);
			//celda1.setTextColor(getResources().getColor(R.color.cuarto_azul));
			//celda1.setPaintFlags(celda1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

			LinearLayout.LayoutParams params2 = new LayoutParams(0, LayoutParams.FILL_PARENT, 0.4f);
			celda2.setText((String) registro.get(2));
			celda2.setLayoutParams(params2);
			celda2.setGravity(Gravity.RIGHT);
			celda2.setTextAppearance(getApplicationContext(), R.style.movimientos_celda_style);
			celda2.setSelected(true);
//			celda2.setTextColor(getResources().getColor(R.color.cuarto_azul));
//			celda2.setPaintFlags(celda1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
			celda2.setSingleLine(true);
			return convertView;
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return 0;
		}
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if (MotionEvent.ACTION_DOWN == ev.getAction()) {
			if (Server.ALLOW_LOG)
				Log.d(this.getClass().getName(), "Touch Event Action: ACTION_DOWN");
			setMarqueeEnabled(false);
		} else if (MotionEvent.ACTION_UP == ev.getAction()) {
			if (Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Touch Event Action: ACTION_UP");
			setMarqueeEnabled(true);
		}

		return super.dispatchTouchEvent(ev);
	}

	public void setMarqueeEnabled(boolean enabled) {
		TextView celda1 = (TextView) findViewById(R.id.mis_cuentas_cabecera_celda_1);
		TextView celda2 = (TextView) findViewById(R.id.mis_cuentas_cabecera_celda_2);
		TextView celda3 = (TextView) findViewById(R.id.mis_cuentas_cabecera_celda_3);
		TextView celda4 = (TextView) findViewById(R.id.mis_cuentas_cabecera_celda_4);

		celda1.setSelected(enabled);
		celda2.setSelected(enabled);
		celda3.setSelected(enabled);
		celda4.setSelected(enabled);

		marqueeEnabled = enabled;
	}

	private boolean marqueeEnabled;

	public void onLinkCambioPerfil(View sender) {
		// TODO AMB Hacer referencia al cambio de perfil de administrar

		SuiteAppAdmonApi suiteAppAdmonApi = new SuiteAppAdmonApi();
		suiteAppAdmonApi.onCreate(this);
		SuiteAppAdmonApi.getInstance().getBmovilApplication().setApplicationLogged(SuiteApp.getInstance().getBmovilApplication().isApplicationLogged());
		SuiteAppAdmonApi.setCallBackSession(null);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(Boolean.TRUE);
		final CambioPerfilDelegate cp = new CambioPerfilDelegate();
		SincronizarSesion.getInstance().SessionToSessionApi(SuiteApp.appContext);

		if (cp.verificarCambioPerfil(Session.getInstance(
				SuiteApp.appContext).getClientProfile())) {
			cp.mostrarCambioPerfil(this);

		} else {
			//cp.cambioPerfilSinToken();
			this.showYesNoAlert(R.string.cambioPerfil_alert_basicoAavanzado_noTokenActivo, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (DialogInterface.BUTTON_POSITIVE == which) {
						cp.actalizaBanderaCambioPerfil();
						suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs = new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
								Server.SIMULATION, Server.ALLOW_LOG, Server.EMULATOR, Server.DEVELOPMENT
						);
						//inicializa la clase
						InitContratacion initi = new InitContratacion(SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController(), new MenuSuiteViewController(), new MenuSuiteViewController(), bs);

						SuiteAppApi sftoken = new SuiteAppApi();

						//Setea los  parametros para simulacion, produccion y test
						ServerCommons.ALLOW_LOG = Server.ALLOW_LOG;
						ServerCommons.DEVELOPMENT = Server.DEVELOPMENT;
						ServerCommons.SIMULATION = Server.SIMULATION;
						sftoken.onCreate(SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController());
						sftoken.setIntentToReturn(new MenuSuiteViewController());


						sftoken.initWithSession(SuiteApp.getInstance().getBmovilApplication().isApplicationLogged(), SuiteApp.getInstance().getMenuPrincipalViewController(), Session.getInstance(SuiteApp.appContext).getClientNumber(), Session.getInstance(SuiteApp.appContext).getUsername(), Session.getInstance(SuiteApp.appContext).getIum());
						sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());

						SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
						sftoken.showViewController(suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.IngresoDatosSTViewController.class, 0, false, null, null, SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController());
					}
				}
			});

		}
	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}

	@Override
	public void returnToPrincipal() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(true);
	}

	@Override
	public void returnDataFromModule(String operation, suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse response) {
		ocultaIndicadorActividad();
		getParentViewsController().setActivityChanging(false);
		if (Integer.parseInt(operation) == ApiConstants.OP_CONSULTA_INVERSIONES_PLAZO) {
			if (response == null) {

            } else if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                ConsultaInversionesPlazoData data = (ConsultaInversionesPlazoData) response.getResponse();
                data.setNumeroCuenta(Tools.hideAccountNumber(delegate.getCuentaSeleccionada().getFullNumber()));
                if (! (SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController() instanceof MisCuentasViewController) ){
                    ((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showMisCuentasViewController();
                }else{
                    if(data.getListaInversiones().size() > 1){
                        delegate.mostrarMenuInversiones(data);
                    }else{
                        delegate.mostrarInversion(data);
                    }
                }
            } else {
                if (response.getStatus() == ServerResponse.OPERATION_WARNING) {

                } else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
                    showInformationAlertEspecial(getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
                }
            }
		} else if (Integer.parseInt(operation) == ApiConstants.OP_CONSULTA_IMPORTES_PAGO_CREDITOS) {
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				ImportesPagoCreditoData importesPagoCredito = (ImportesPagoCreditoData) response.getResponse();
				delegate.mostrarPagoCredito(importesPagoCredito);
			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {

			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				showInformationAlertEspecial(getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
			}
		}
	}


	@Override
	public void returnDataFromModule(String operation, bancomer.api.common.io.ServerResponse response) {
		ocultaIndicadorActividad();
//		if(operation.equalsIgnoreCase(bancomer.api.consultaotroscreditos.io.Server.OPERATION_CODES[bancomer.api.consultaotroscreditos.io.Server.OP_CONSULTA_OTROS_CREDITOS])){
//
//			MisCuentasDelegate delegateC = delegate;
//
//			if(delegateC == null) delegateC = (MisCuentasDelegate) (SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController()).getBaseDelegateForKey(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID);
//
//			if(delegateC.getConfirmacionAutenticacionViewController() != null) delegateC.getConfirmacionAutenticacionViewController().ocultaIndicadorActividad();
//
//			ConsultaOtrosCreditosData consultaOtrosCreditosData = (ConsultaOtrosCreditosData) response.getResponse();
//
//			delegateC.setConsultaOtrosCreditosData(consultaOtrosCreditosData);
//
//			((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showMisCuentasViewController();
//
//		}else{

		if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

			//JMT
			Map<String,Object> inicioOperacionMap = new HashMap<String, Object>();

			//JMT
			inicioOperacionMap.put("evento_inicio", "event45");
			inicioOperacionMap.put("&&products","consulta;mis creditos");
			inicioOperacionMap.put("eVar12", "inicio");

			TrackingHelper.trackInicioOperacion(inicioOperacionMap);

			InitConsultaOtrosCreditos.getInstance().showDetallesOtrosCreditos(delegate.getCreditoSeleccionado());

		}else{


			android.content.DialogInterface.OnClickListener onClickListener = new android.content.DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					getMisCuentasOpciones().dismiss();
				}
			};

			if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				showInformationAlertEspecial(getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), onClickListener);

			}else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
				showInformationAlertEspecial(getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), onClickListener);
			}

		}
	}

/*
	@Override
	public void returnDataFromModule(String operation, bancomer.api.common.io.ServerResponse response) {

		ocultaIndicadorActividad();
//		if(operation.equalsIgnoreCase(bancomer.api.consultaotroscreditos.io.Server.OPERATION_CODES[bancomer.api.consultaotroscreditos.io.Server.OP_CONSULTA_OTROS_CREDITOS])){
//
//			MisCuentasDelegate delegateC = delegate;
//
//			if(delegateC == null) delegateC = (MisCuentasDelegate) (SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController()).getBaseDelegateForKey(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID);
//
//			if(delegateC.getConfirmacionAutenticacionViewController() != null) delegateC.getConfirmacionAutenticacionViewController().ocultaIndicadorActividad();
//
//			ConsultaOtrosCreditosData consultaOtrosCreditosData = (ConsultaOtrosCreditosData) response.getResponse();
//
//			delegateC.setConsultaOtrosCreditosData(consultaOtrosCreditosData);
//
//			((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showMisCuentasViewController();
//
//		}else{

		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {

			//JMT
			Map<String, Object> inicioOperacionMap = new HashMap<String, Object>();

			//JMT
			inicioOperacionMap.put("evento_inicio", "event45");
			inicioOperacionMap.put("&&products", "consulta;mis creditos");
			inicioOperacionMap.put("eVar12", "inicio");

			TrackingHelper.trackInicioOperacion(inicioOperacionMap);

			InitConsultaOtrosCreditos.getInstance().showDetallesOtrosCreditos(delegate.getCreditoSeleccionado());

		} else {


			android.content.DialogInterface.OnClickListener onClickListener = new android.content.DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					getMisCuentasOpciones().dismiss();
				}
			};

			if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				showInformationAlertEspecial(getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), onClickListener);

			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				showInformationAlertEspecial(getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), onClickListener);
			}
		}*/
	}




