package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MovimientosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Movement;
import suitebancomer.aplicaciones.bmovil.classes.model.MovementExtract;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaDatosViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.annotation.SuppressLint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import tracking.TrackingHelper;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

/**
 * @author Luis Alberto de la Cruz Torres
 */

public class ConsultaMovimientosViewController extends BaseViewController {

	public TextView getVerMasMovimientos() {
		return verMasMovimientos;
	}

	public void setVerMasMovimientos(TextView verMasMovimientos) {
		this.verMasMovimientos = verMasMovimientos;
	}
	
	LinearLayout vistaErrorGenericoTDC;
	
	LinearLayout vista;
	MovimientosDelegate movimientosDelegate;
	CuentaOrigenViewController componenteCtaOrigen;
	ListaDatosViewController listaDatos;
	ListaDatosViewController listaDatosTDC;
	LinearLayout listaTDC;
	ListaSeleccionViewController listaSeleccion;
	ArrayList<Movement> movimientos;
	TextView verMasMovimientos;
	TextView movimientosTransito;

	TextView labelErrorTdc=null;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ

	String TDCsaldoAlCorte;
	String TDCpagoMinimo;
	String TDCfechaSaldo;
	String TDCfechaLimite;
	String TDCPagoNoIntereses;
	String TDCPuntosBancomer;
	String TDCMovimientoTransito;

	/**
	 * Default constructor for this activity
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_consulta_movimientos);
		setTitle(R.string.consultar_movimientos_title, R.drawable.bmovil_consultar_icono);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("movimientos", parentManager.estados);
		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(MovimientosDelegate.MOVIMIENTOS_DELEGATE_ID));
		movimientosDelegate = (MovimientosDelegate)getDelegate(); 
		movimientosDelegate.setConsultaMovimientosViewController(this);
		
		vista = (LinearLayout)findViewById(R.id.consulta_movimientos_layout);

		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scale(findViewById(R.id.layoutRoot));

		showCuentaOrigen();

		if(movimientosDelegate.getCuentaSeleccionada().getType().equals(Constants.CREDIT_TYPE)){
			movimientosDelegate.consultaImportesTDC();
		}else{
			movimientosDelegate.consultaMovimientos(Constants.MOVIMIENTOS_MES_ACTUAL);
		}
	}


	@SuppressWarnings("deprecation")
	public void showCuentaOrigen(){
		ArrayList<Account> listaCuetasAMostrar = movimientosDelegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.balanceDetail_componentTitle));
		componenteCtaOrigen.setDelegate(movimientosDelegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
//		if(movimientosDelegate.getCuentaSeleccionada() != null){
//
//			componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(movimientosDelegate.getCuentaSeleccionada()));
//		}else{

		if((listaCuetasAMostrar != null)&&(!listaCuetasAMostrar.isEmpty())) componenteCtaOrigen.setIndiceCuentaSeleccionada(0);
//		}
		componenteCtaOrigen.init();
		vista.addView(componenteCtaOrigen);
	}
	
	@SuppressWarnings("deprecation")
	public void llenaListaDatos(boolean listavacia){
		
		MovementExtract datosMovimientos = movimientosDelegate.getTotalMovementsExtract();
		ArrayList<Object> registros;
		ArrayList<Object> lista = new ArrayList<Object>();
		ArrayList<String> opciones = new ArrayList<String>();
		ArrayList<String> datos = new ArrayList<String>();
		
		if ((movimientosDelegate.getTotalMovementsExtract() != null) && (!listavacia)) {
			this.anadirDatosMovimientos(movimientosDelegate.getTotalMovementsExtract());
		}
		
		if (componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getType().equals(Constants.CREDIT_TYPE)) {
			opciones.add(getString(R.string.balanceDetail_previousBalance));
			opciones.add(getString(R.string.balanceDetail_dateInfo));
			//opciones.add(getString(R.string.balanceDetail_currentBalance));
			
			if (movimientos != null) {
				/*String previousBalance = TDCsaldoAlCorte;
				boolean isNegative = previousBalance.startsWith("-");
				if (isNegative) {
	                previousBalance = previousBalance.substring(1);
	            }
				datos.add(Tools.formatAmount(previousBalance, isNegative));

				String minimumPayment = TDCpagoMinimo;

				if (isNegative) {
					minimumPayment = minimumPayment.substring(1);
	            }
				datos.add(Tools.formatDate(TDCfechaSaldo));*/
				datos.add(Tools.formatDate(datosMovimientos.getDate()));//TODO
				
			} else if (movimientos == null) {
				datos.add("");
				datos.add("");
			}
			
			/*for (int i = 0; i < opciones.size(); i++) {
				registros = new ArrayList<Object>();
				registros.add(opciones.get(i));
				registros.add(datos.get(i));
				lista.add(registros);
			}*/
			 
		} else {
			opciones.add(getString(R.string.balanceDetail_currentBalance));
			registros = new ArrayList<Object>();
			registros.add(opciones.get(0));
			if (datosMovimientos != null) {
				registros.add(Tools.formatDate(datosMovimientos.getDate()));
			} else if (datosMovimientos == null) {
				registros.add("");
			}
			lista.add(registros);
		}

		LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		if (listaDatos == null) {
			listaDatos = new ListaDatosViewController(this, params, parentViewsController);
			listaDatos.setNumeroCeldas(2);
			listaDatos.setLista(lista);
			listaDatos.setNumeroFilas(opciones.size());
			listaDatos.showLista();
			vista.addView(listaDatos);
			if (componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getType().equals(Constants.CREDIT_TYPE)) {
				listaDatos.setVisibility(View.GONE);

			}else {
				listaDatos.setVisibility(View.VISIBLE);

			}
		} else {
			listaDatos.setLista(lista);
			listaDatos.setNumeroFilas(opciones.size());
			listaDatos.showLista();
			if (componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getType().equals(Constants.CREDIT_TYPE)) {
				listaDatos.setVisibility(View.GONE);

			}else {
				listaDatos.setVisibility(View.VISIBLE);

			}
		}
		if(listaTDC == null){
			listaTDC = new LinearLayout(this);
			listaTDC.setOrientation(LinearLayout.VERTICAL);
			listaTDC.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
			vista.addView(listaTDC);
			listaTDC.setVisibility(View.GONE);
		} 
		
		
		
		if(vistaErrorGenericoTDC == null){
			vistaErrorGenericoTDC = new LinearLayout(this);
			vistaErrorGenericoTDC.setOrientation(LinearLayout.VERTICAL);
			vistaErrorGenericoTDC.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
			vista.addView(vistaErrorGenericoTDC);
			vistaErrorGenericoTDC.setVisibility(View.GONE);
		} else {
			
			vistaErrorGenericoTDC.setVisibility(View.GONE);

		}
		
		
		
		
		
		
		if(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getType().equals(Constants.CREDIT_TYPE)){
			
			//Toast.makeText(SuiteApp.getInstance(),"tdc movimientos", Toast.LENGTH_LONG).show();
			
			if(listaTDC.getChildCount() == 0){
				muestraTDCDataBlock();
			}else{
				rellenaTDCDataBlock();
			}
		}
		//si se hizo una consulta correcta y despues da error
		//de contrato inexistente para que se limpien los datos antiguos
		if (listavacia) {
			borrarTDCDataBlock();
			//muestraErrorGenericoTDC();
		}
		
		llenaListaSeleccion();
		ocultaIndicadorActividad();
	}
	
	private void anadirDatosMovimientos(MovementExtract me) {
		if (movimientos == null) {
			movimientos = me.getMovements();
			
			if (componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getType().equals(Constants.CREDIT_TYPE)) {
				//TDCsaldoAlCorte = me.getPreviousBalance();
				//TDCpagoMinimo = me.getMinPayment();
				//TDCfechaSaldo = me.getDate();
				if(movimientosDelegate.getResImpTDC() != null){					
					TDCsaldoAlCorte = movimientosDelegate.getResImpTDC().getSaldoAlCorte();
					TDCpagoMinimo = movimientosDelegate.getResImpTDC().getPagoMinimo();
					TDCfechaSaldo = movimientosDelegate.getResImpTDC().getSaldoAlCorte();
					TDCfechaLimite = movimientosDelegate.getResImpTDC().getFechaPago();
					TDCPagoNoIntereses = movimientosDelegate.getResImpTDC().getPagoNogeneraInt();
					TDCPuntosBancomer = movimientosDelegate.getResImpTDC().getPuntosBancomer();
					TDCMovimientoTransito = movimientosDelegate.getResImpTDC().getSaldoMovsTransito();
				}
			}
		} else {
			movimientos.addAll(me.getMovements());
		}
	}
	
	public void borrarTDCDataBlock(){
		if(listaTDC != null){
			listaTDC.setVisibility(View.GONE);
		}
	}
	
	
	public ArrayList<Object> getListaTDCData(){
		
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;

		String getFechaDeCorte = Tools.formatDateTDC(movimientosDelegate.getResImpTDC().getFechaCorte());//movimientosDelegate.getMovimiento().getAmount();
		//boolean isNegative = getPagoSaldoActual.startsWith("-");
		fila = new ArrayList<String>();
		fila.add(getString(R.string.bmovil_cuenta_deposito_fechacorte));
		fila.add(getFechaDeCorte);
		tabla.add(fila);
		
		String getFechalimiteDePago = Tools.formatDateTDC(movimientosDelegate.getResImpTDC().getFechaPago());//movimientosDelegate.getMovimiento().getAmount();
		//boolean isNegative = getPagoSaldoActual.startsWith("-");
		fila = new ArrayList<String>();
		fila.add(getString(R.string.bmovil_cuenta_deposito_fechalimite));
		fila.add(getFechalimiteDePago);
		tabla.add(fila);

		String getPagoMinimo = movimientosDelegate.getResImpTDC().getPagoMinimo();
		boolean isNegative = getPagoMinimo.startsWith("-");
//		muestraTDCData(getString(R.string.bmovil_cuenta_deposito_pagomin),Tools.formatAmount(getPagoMinimo, isNegative),true,true);
		fila = new ArrayList<String>();
		fila.add(getString(R.string.bmovil_cuenta_deposito_pagomin));
		fila.add(Tools.formatAmount(getPagoMinimo, isNegative));
		tabla.add(fila);
		
		
		//String getPagoSaldoActual = Tools.convertDoubleToBigDecimalAndReturnString(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getBalance());//movimientosDelegate.getMovimiento().getAmount();
//		String getPagoSaldoActual = movimientosDelegate.getResImpTDC().getSaldoActual();
//		isNegative = getPagoSaldoActual.startsWith("-");
//		fila = new ArrayList<String>();
//		fila.add(getString(R.string.bmovil_cuenta_deposito_pagosaldoactual));
//		fila.add(Tools.formatAmount(getPagoSaldoActual, isNegative));
//		tabla.add(fila);

		//Movimiento en Transito
		/* Se comenta temporalmente, porsteriormente se activara
		String getMovimientosTransito = movimientosDelegate.getResImpTDC().getSaldoMovsTransito();
		isNegative = getMovimientosTransito.startsWith("-");
		fila = new ArrayList();
		fila.add(getString(R.string.bmovil_cuenta_deposito_movimientos_transito));
		fila.add(Tools.formatAmount(getMovimientosTransito, isNegative));
		tabla.add(fila);*/

		String getPagoNogeneraInt = movimientosDelegate.getResImpTDC().getPagoNogeneraInt();
		isNegative = getPagoNogeneraInt.startsWith("-");
//		muestraTDCData(getString(R.string.bmovil_cuenta_deposito_pagonointeres),Tools.formatAmount(getPagoNogeneraInt, isNegative),true,false);
		fila = new ArrayList<String>();
		fila.add(getString(R.string.bmovil_cuenta_deposito_pagonointeres));
		fila.add(Tools.formatAmount(getPagoNogeneraInt, isNegative));
		tabla.add(fila);

		//Saldo disponible
		/* Se comenta temporalmente, porsteriormente se activara
		String getSaldoDisponible = movimientosDelegate.getResImpTDC().getSaldoDisponible();
		isNegative = getSaldoDisponible.startsWith("-");
		fila = new ArrayList<String>();
		fila.add(getString(R.string.bmovil_cuenta_deposito_saldo_disponible));
		fila.add(Tools.formatAmount(getSaldoDisponible, isNegative));
		tabla.add(fila); */

		String getSaldoAlCorte = movimientosDelegate.getResImpTDC().getSaldoAlCorte();
		isNegative = getSaldoAlCorte.startsWith("-");
		fila = new ArrayList<String>();
		fila.add(getString(R.string.bmovil_cuenta_deposito_saldoalcorte));
		fila.add(Tools.formatAmount(getSaldoAlCorte, isNegative));
		tabla.add(fila);


		String getPuntosBancomer = movimientosDelegate.getResImpTDC().getPuntosBancomer();
		fila = new ArrayList<String>();
		fila.add(getString(R.string.bmovil_cuenta_deposito_puntos_bancomer));
		fila.add(Tools.formatPuntosBancomer(getPuntosBancomer));
		tabla.add(fila);

		return tabla;
	}
	
	public void rellenaTDCDataBlock(){
		
		//Toast.makeText(SuiteApp.getInstance(),"rellenaTDCDataBlock movimientos", Toast.LENGTH_SHORT).show();

		if (componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getType().equals(Constants.CREDIT_TYPE)) {
			if(movimientosDelegate.getResImpTDC() != null){
				listaTDC.setVisibility(View.VISIBLE);
				
			/*	ArrayList<Object> lista = listaDatos.getLista();
				ArrayList<Object> registro = new ArrayList<Object>();
				registro.add(getString(R.string.bmovil_cuenta_deposito_fechalimite));
				registro.add(Tools.formatDate(movimientosDelegate.getResImpTDC().getFechaPago()));
				lista.add(registro);
				
				listaDatos.setLista(lista);
				listaDatos.setNumeroFilas(lista.size());
				listaDatos.showLista();
*/

				listaDatosTDC.setLista(getListaTDCData());
				listaDatosTDC.setNumeroFilas(getListaTDCData().size());
				listaDatosTDC.showLista();

			}
		}
	}
	
	public void muestraTDCDataBlock(){
		
		//Toast.makeText(SuiteApp.getInstance(),"muestraTDCDataBlock movimientos", Toast.LENGTH_SHORT).show();

		if (componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getType().equals(Constants.CREDIT_TYPE)) {
			if(movimientosDelegate.getResImpTDC() != null){
				listaTDC.setVisibility(View.VISIBLE);

			/*	ArrayList<Object> lista = listaDatos.getLista();
				ArrayList<Object> registro = new ArrayList<Object>();
				registro.add(getString(R.string.bmovil_cuenta_deposito_fechalimite));
				registro.add(Tools.formatDate(movimientosDelegate.getResImpTDC().getFechaPago()));
				lista.add(registro);
				
				listaDatos.setLista(lista);
				listaDatos.setNumeroFilas(lista.size());
				listaDatos.showLista();
				*/
				muestraTDCData("", "", true, true, false);
				//muestraTDCData(getString(R.string.bmovil_cuenta_deposito_importe),"",true,true,true);
				muestraTDCData(getListaTDCData());
			}
		}
	}
	
	@SuppressLint("InflateParams") 
	public void muestraTDCData(String description,String value, Boolean paddingUp, Boolean paddingDown, Boolean title){

		LinearLayout tdcData = (LinearLayout) getLayoutInflater().inflate(R.layout.lista_datos_template, null);
		TextView desc = ((TextView)tdcData.findViewById(R.id.LoginSubTitleTxt));
		TextView val = ((TextView)tdcData.findViewById(R.id.textView2));
		
		desc.setText(description);
		val.setText(value);
		
		if(title){
			desc.setTextColor(getResources().getColor(R.color.primer_azul));
			desc.setTextSize(TypedValue.COMPLEX_UNIT_PX, 16);
		}
		
		listaTDC.addView(tdcData);
		
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scale(tdcData);
		guiTools.scale(desc, true);
		guiTools.scale(val, true);
		if(paddingUp && paddingDown){
			tdcData.setPadding(0, 10, 0, 10);
			
		}else if(paddingUp){
			tdcData.setPadding(0, 10, 0, 0);
			
		}else if(paddingDown){
			tdcData.setPadding(0, 0, 0, 10);
			
		}
	}
	
	@SuppressLint("InflateParams") 
	public void muestraTDCData(ArrayList<Object> tabla){
		
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(listaTDC);

		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		listaDatosTDC = new ListaDatosViewController(this, params, parentViewsController);
//		listaDatosTDC.setTitulo(R.string.bmovil_cuenta_deposito_importe);
		listaDatosTDC.setNumeroCeldas(2);
		listaDatosTDC.setLista(tabla);
		listaDatosTDC.setNumeroFilas(tabla.size());
		listaDatosTDC.showLista();
		listaTDC.addView(listaDatosTDC);
	}
	
	@SuppressWarnings("deprecation")
	public void llenaListaSeleccion(){
		
		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		ArrayList<Object> registros;
		ArrayList<Object> lista = new ArrayList<Object>();
		ArrayList<Object> encabezado = new ArrayList<Object>();
		encabezado.add(null);
		encabezado.add(getString(R.string.balanceDetail_headerDate));
		encabezado.add(getString(R.string.balanceDetail_headerDescription));
		encabezado.add(getString(R.string.balanceDetail_headerAmount));


		Collections.sort(movimientos, new Comparator<Movement>() {

			DateFormat f = new SimpleDateFormat("dd/MM/yyyy");

			@Override
			public int compare(Movement lhs, Movement rhs) {
				try {
					return f.parse(rhs.getAuthorizationDate()).compareTo(f.parse(lhs.getAuthorizationDate()));
				} catch (ParseException e) {
					throw new IllegalArgumentException(e);
				}
			}
		});


		if (movimientos != null) {
			for (int i = 0; i < movimientos.size(); i++) {
				registros = new ArrayList<Object>();
				registros.add(movimientos.get(i));
				registros.add(movimientos.get(i).getDate());
				registros.add(movimientos.get(i).getShortDescription());
				registros.add(movimientos.get(i).getAmount());
				lista.add(registros);
			}
		} else if (movimientos == null) {
			registros = new ArrayList<Object>();
			registros.add("");
			registros.add("");
			registros.add("");
			registros.add("");
			lista.add(registros);
		}
		
		params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		params.topMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
//		params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
		params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_bottom_margin);;
		
		if (listaSeleccion == null) {
			listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
			listaSeleccion.setDelegate(movimientosDelegate);
			listaSeleccion.setNumeroColumnas(3);
			listaSeleccion.setEncabezado(encabezado);
			listaSeleccion.setTitle(getString(R.string.balanceDetail_lastMovements));
			listaSeleccion.setLista(lista);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(lista.size());
			listaSeleccion.setExisteFiltro(false);
			listaSeleccion.cargarTabla();
			vista.addView(listaSeleccion);

		}else {
			listaSeleccion.setLista(lista);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(lista.size());
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.cargarTabla();
		}
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
	}
	
	public CuentaOrigenViewController getComponenteCtaOrigen() {
		return componenteCtaOrigen;
	}
	
	@SuppressLint("InflateParams") 
	public void muestraVerMasMovimientos(boolean visible){
		if(visible && null == this.verMasMovimientos){
			this.verMasMovimientos = (TextView) getLayoutInflater().inflate(R.layout.layout_bmovil_ver_mas_movimientos, null);
			vista.addView(verMasMovimientos);
			GuiTools guiTools = GuiTools.getCurrent();
			guiTools.init(getWindowManager());
			guiTools.scale(verMasMovimientos, true);
			verMasMovimientos.setPadding(0, 0, 0, 10);
			verMasMovimientos.setVisibility(View.VISIBLE);
		}
		
		if(null != verMasMovimientos){
			verMasMovimientos.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
	}

	/*
	 * TODO AMB
	 * Se comenta por el momento para posteriormente mostrar los movimientos en transito
	 */
	/*
	@SuppressLint("InflateParams")
	public void muestraMovimientosTransito(boolean visible){
		if(visible && null == this.verMasMovimientos){
			this.movimientosTransito = (TextView) getLayoutInflater().inflate(R.layout.layout_bmovil_ver_movimiento_transito, null);
			vista.addView(movimientosTransito);
			GuiTools guiTools = GuiTools.getCurrent();
			guiTools.init(getWindowManager());
			guiTools.scale(movimientosTransito, true);
			movimientosTransito.setPadding(0, 0, 0, 20);
			movimientosTransito.setVisibility(View.VISIBLE);
		}

		if(null != movimientosTransito){
			movimientosTransito.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
	}*/

	public void onLinkVerMasMovimientos(View sender) {
		this.movimientosDelegate.verMasMovimientos();
	}

	public void onLinkVerMovimientosTransito(View view){
		this.movimientosDelegate.verMovimientosTransito();
	}

	public void processNetworkResponse(int operationId, ServerResponse response) {
		movimientosDelegate.analyzeResponse(operationId, response);
	}
		
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(MovimientosDelegate.MOVIMIENTOS_DELEGATE_ID);
		super.goBack();
	}
	public void borrarListaMovimientos() {
		movimientos = null;
	}
	
public void muestraErrorGenericoTDC() {
		
		if (listaTDC!=null){
		
		listaTDC.setVisibility(View.GONE);
		
		if (labelErrorTdc==null){
		
		labelErrorTdc = new TextView(this);
		labelErrorTdc.setLines(3);
		labelErrorTdc.setSingleLine(false);
		labelErrorTdc.setGravity(Gravity.CENTER);
		labelErrorTdc.setTypeface(null, Typeface.BOLD);

		//labelErrorTdc.sette
		labelErrorTdc.setText(R.string.bmovil_cuenta_pagotdc_error);
		vistaErrorGenericoTDC.addView(labelErrorTdc);

		}

		vistaErrorGenericoTDC.setVisibility(View.VISIBLE);
		
		} else {
			
			
		/*	listaTDC = new LinearLayout(this);
			listaTDC.setOrientation(LinearLayout.VERTICAL);
			listaTDC.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, 80));
			vista.addView(listaTDC);*/
			
			if (labelErrorTdc==null){
				
				
				labelErrorTdc = new TextView(this);
				labelErrorTdc.setLines(3);
				labelErrorTdc.setSingleLine(false);
				labelErrorTdc.setGravity(Gravity.CENTER);
				labelErrorTdc.setTypeface(null, Typeface.BOLD);

				//labelErrorTdc.sette
				labelErrorTdc.setText(R.string.bmovil_cuenta_pagotdc_error);
				vistaErrorGenericoTDC.addView(labelErrorTdc);

			}

			vistaErrorGenericoTDC.setVisibility(View.VISIBLE);

		}
		
		
		if (verMasMovimientos!=null){
		
		verMasMovimientos.setVisibility(View.GONE);

		}
//		if (movimientosTransito!=null){
//			movimientosTransito.setVisibility(View.GONE);
//		}
		
		
		
	}

}

