package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;
import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionRegistroViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.gui.controllers.BaseViewController;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class ConfirmacionRegistroDelegate extends DelegateBaseAutenticacion {
	private ConfirmacionRegistroViewController viewController;
	public final static long CONFIRMACION_REGISTRO_DELEGATE_DELEGATE_ID = 0x9ef4f4c61ca109afL;
	private DelegateBaseOperacion opDelegate;
	private boolean debePedirContrasena;
	private boolean debePedirNip;
	private TipoOtpAutenticacion tokenAMostrar;
	private boolean debePedirCVV;
	
	private TipoInstrumento tipoInstrumentoSeguridad;

	public void setViewController(ConfirmacionRegistroViewController viewController) {
		this.viewController = viewController;
	}

	public ConfirmacionRegistroDelegate(DelegateBaseOperacion delegateOp) {
		this.opDelegate = delegateOp;
		debePedirContrasena = opDelegate.mostrarContrasenia();
		debePedirNip = opDelegate.mostrarNIP();
		debePedirCVV = opDelegate.mostrarCVV();
		tokenAMostrar = opDelegate.tokenAMostrar();
		String instrumento = Session.getInstance(SuiteApp.appContext).getSecurityInstrument();
		if (instrumento.equals(Constants.IS_TYPE_DP270)) {
			tipoInstrumentoSeguridad = TipoInstrumento.DP270;
		} else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
			tipoInstrumentoSeguridad = TipoInstrumento.OCRA;
		} else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)) {
			tipoInstrumentoSeguridad = TipoInstrumento.SoftToken;
		} else {
			tipoInstrumentoSeguridad = TipoInstrumento.sinInstrumento;
		}
	}

	public void consultaDatosLista() {
		viewController.setListaDatos(opDelegate.getDatosTablaConfirmacion());
	}
	
	public DelegateBaseOperacion consultaOperationsDelegate() {
		return opDelegate;
	}
	
	public boolean consultaDebePedirContrasena() {
		return debePedirContrasena;
	}
	
	public boolean consultaDebePedirNIP() {
		return debePedirNip;
	}
	
	public boolean consultaDebePedirCVV() {
		return debePedirCVV;
	}
	
	public Constants.TipoInstrumento consultaTipoInstrumentoSeguridad() {
		return tipoInstrumentoSeguridad;
	}
	
	public Constants.TipoOtpAutenticacion consultaInstrumentoSeguridad() {
		return tokenAMostrar;
	}
	
	public boolean validaDatos(){
		boolean esOk = true;
		StringBuilder msg = new StringBuilder("");
		if(debePedirContrasena){
			String contrasena = viewController.pideContrasena();
			if(contrasena.length() == 0){
				esOk = false;
				msg.append(viewController.getString(R.string.confirmation_valorVacio));
				msg.append(" ");
				msg.append(viewController.getString(R.string.confirmation_componenteContrasena));
				msg.append(".");
			}else if(contrasena.length() != Constants.PASSWORD_LENGTH){
				esOk = false;
				msg.append(viewController.getString(R.string.confirmation_valorIncompleto1));
				msg.append(" ");
				msg.append(Constants.PASSWORD_LENGTH);
				msg.append(" ");
				msg.append(viewController.getString(R.string.confirmation_valorIncompleto2));
				msg.append(" ");
				msg.append(viewController.getString(R.string.confirmation_componenteContrasena));
				msg.append(".");
			}
		}else if(debePedirNip){
			String nip = viewController.pideNIP();
			if(nip.length() == 0){
				esOk = false;
				msg.append(viewController.getString(R.string.confirmation_valorVacio));
				msg.append(" ");
				msg.append(viewController.getString(R.string.confirmation_componenteNip));
				msg.append(".");
			}else if(nip.length() != Constants.NIP_LENGTH){
				esOk = false;
				msg.append(viewController.getString(R.string.confirmation_valorIncompleto1));
				msg.append(" ");
				msg.append(Constants.NIP_LENGTH);
				msg.append(" ");
				msg.append(viewController.getString(R.string.confirmation_valorIncompleto2));
				msg.append(" ");
				msg.append(viewController.getString(R.string.confirmation_componenteNip));
				msg.append(".");
			}			
			
		}else if(tokenAMostrar != Constants.TipoOtpAutenticacion.ninguno){
			String asm = viewController.pideASM();
			if (asm.equals("")) {
				esOk = false;
				String mensaje = viewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
				mensaje += ".";
				msg = new StringBuilder(mensaje);

			} else if (asm.length() != Constants.ASM_LENGTH) {
				esOk = false;
				String mensaje = viewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.ASM_LENGTH;
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}	
				mensaje += ".";
				msg = new StringBuilder(mensaje);
			}
			
		}else if(debePedirCVV){
			String cvv = viewController.pideCVV();
			if (cvv.equals("")) {
				esOk = false;
				String mensaje = viewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_componenteCvv);
				mensaje += ".";
				msg = new StringBuilder(mensaje);
			} else if (cvv.length() != Constants.CVV_LENGTH) {
				esOk = false;
				String mensaje = viewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.CVV_LENGTH;
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_componenteCvv);
				mensaje += ".";
				msg = new StringBuilder(mensaje);
			}			
		}		
		
		if(!esOk)
			viewController.showInformationAlert(msg.toString());
			
		return esOk;
	}
	
	/**
	 * notifica al delegate de la op actual la confirmacion para realizar la operacion.
	 * 
	 * @param contrasena
	 * @param nip
	 * @param asm
	 * @param cvv
	 */
	public void enviaPeticionOperacion(String contrasena, String nip, String asm, String cvv) {
		String newToken = null;
		if(tokenAMostrar != TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == TipoInstrumento.SoftToken && SuiteApp.getSofttokenStatus())
			newToken = loadOtpFromSofttoken(tokenAMostrar);
		if(null != newToken)
			asm = newToken;
		
		
				if (opDelegate instanceof AltaRetiroSinTarjetaDelegate) {
					
					opDelegate.realizaOperacion(viewController, contrasena, nip, asm, cvv);


				}else {
					
					opDelegate.realizaOperacion(viewController, contrasena, nip, asm);

				}
	}

	@Override
	public String getEtiquetaCampoNip() {
		return viewController.getString(R.string.confirmation_nip);
	}
	
	@Override
	public String getTextoAyudaNIP() {
		return viewController.getString(R.string.confirmation_ayudaNip);
	}
	
	@Override
	public String getEtiquetaCampoContrasenia() {
		return viewController.getString(R.string.confirmation_contrasena);
	}
	
	@Override
	public String getEtiquetaCampoOCRA() {
		return viewController.getString(R.string.confirmation_ocra);
	}
	
	@Override
	public String getEtiquetaCampoDP270() {
		return viewController.getString(R.string.confirmation_dp270);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenActivado() {
		return viewController.getString(R.string.confirmation_softtokenActivado);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenDesactivado() {
		return viewController.getString(R.string.confirmation_softtokenDesactivado);
	}
	
	@Override
	public String getEtiquetaCampoCVV() {
		return viewController.getString(R.string.confirmation_CVV);
	}
	
	@Override
	public int getTextoEncabezado() {
		return opDelegate.getTextoEncabezado();
	}
	
	@Override
	public int getNombreImagenEncabezado() {
		return opDelegate.getNombreImagenEncabezado();
	}
	
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if(response.getStatus() == ServerResponse.OPERATION_ERROR){
			viewController.limpiarCampos();
			BmovilApp bmApp = SuiteApp.getInstance().getBmovilApplication();
			BmovilViewsController bmvc = bmApp.getBmovilViewsController(); 
			BaseViewController current = bmvc.getCurrentViewControllerApp();
			current.showInformationAlert(response.getMessageText());
		}
		opDelegate.analyzeResponse(operationId, response);
	}

	@Override
	public ArrayList<Object> getDatosRegistroOp() {
		return opDelegate.getDatosTablaConfirmacion();
	}

	@Override
	public ArrayList<Object> getDatosRegistroExitoso() {
		return opDelegate.getDatosRegistroExitoso();
	}
	
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		return tokenAMostrar;
	}

	@Override
	public String loadOtpFromSofttoken(TipoOtpAutenticacion tipoOTP) {
		return loadOtpFromSofttoken(tipoOTP, opDelegate);
	}
}
