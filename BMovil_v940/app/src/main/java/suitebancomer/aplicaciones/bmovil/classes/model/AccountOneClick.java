/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.bmovil.classes.model;

import android.content.res.Resources;

import com.bancomer.mbanking.R;

import java.io.Serializable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.model.HostAccounts;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;

/**
*
* @author Stefanini IT Solutions.
*
* Account wraps the data needed to define an account for Bancomer.
* in MBanking application
*
*/
public class AccountOneClick implements Serializable, HostAccounts {

	    /**
	     * Account number.
	     */
	    private String number = null;

	    /**
	     * Current balance.
	     */
	    private double balance;

	    /**
	     * Date of current balance.
	     */
	    private String date = null;

	    /**
	     * Flag to determine if the account is visible.
	     */
	    private boolean visible = true;

	    /**
	     * Currency of the account.
	     */
	    private String currency = null;

	    /**
	     * Type of account.
	     */
	    private String type = null;

	    /**
	     * Concept of account.
	     */
	    private String concept = null;
	    
	    /**
	     * Alias de la cuenta
	     */
	    private String alias;

	    /**
	     * Default constructor.
	     */
	    
	    //New Attributes SPEI OPERATIVA DIARIA
	    
	    /**
	     * Telephone number associated
	     */
	    private String celularAsociado = null;
	    
	    /**
	     * Company's code 
	     */
	    
	    private String codigoCompania = null;
	    
	    /**
	     * Company's description
	     */
	    
	    private String descripcionCompania = null;
	    
	    /**
	     * Last modify date
	     */
	    
	    private String fechaUltimaModificacion = null;
	    
	    /**
	     * SPEI indicator
	     */
	    
	    private String indicadorSPEI = null;


	    /**
	     * Default constructor.
	     */
	    
	    public AccountOneClick() {
	    }
	    
	    /**
	     * Constructor with parameters.
	     * @param num account number
	     * @param bal current balance
	     * @param dat balance date
	     * @param vis true if the account is visible, false if not
	     * @param curren account currency
	     * @param types accunt type
	     * @param concepts account concept
	     */
	    public AccountOneClick(String num, double bal, String dat,
							   boolean vis, String curren, String types, String concepts, String alias) {
	        this.balance = bal;
	        this.number = num;
	        this.date = dat;
	        this.visible = vis;
	        this.currency = curren;
	        this.type = types;
	        this.concept = concepts;
	        this.alias = (alias != null? alias : "");
	    }
	    
	  //Se agrego contructor para opcion actualizar cuentas del menu administrar
	    public AccountOneClick(String num, double bal, String dat,
							   boolean vis, String curren, String types, String concepts, String alias, String celularAsociado,
							   String codigoCompania, String descripcionCompania, String fechaUltimaModificacion, String indicadorSPEI) {
	        this.balance = bal;
	        this.number = num;
	        this.date = dat;
	        this.visible = vis;
	        this.currency = curren;
	        this.type = types;
	        this.concept = concepts;
	        this.alias = (alias != null? alias : "");
	        this.celularAsociado = celularAsociado;
	        this.codigoCompania = codigoCompania;
	        this.descripcionCompania = descripcionCompania;
	        this.fechaUltimaModificacion = fechaUltimaModificacion;
	        this.indicadorSPEI = indicadorSPEI;
	        
	    }

	    /**
	     * Get the account balance.
	     * @return the account balance
	     */
	    public double getBalance() {
	        return balance;
	    }

	    /**
	     * Set the account balance.
	     * @param bal the account balance
	     */
	    public void setBalance(double bal) {
	        this.balance = bal;
	    }

	    /**
	     * get the account number.
	     * @return the account number
	     */
	    public String getNumber() {
	        return number;
	    }

	    /**
	     * Set the account number.
	     * @param num the account number
	     */
	    public void setNumber(String num) {
	        this.number = num;
	    }

	    /**
	     * Get the balance date.
	     * @return the balance date
	     */
	    public String getDate() {
	        return date;
	    }

	    /**
	     * Set the balance date.
	     * @param dat the balance date
	     */
	    public void setDate(String dat) {
	        this.date = dat;
	    }

	    /**
	     * Get the concept.
	     * @return the concept
	     */
	    public String getConcept() {
	        return concept;
	    }

	    /**
	     * Set the concept.
	     * @param conc the concept
	     */
	    public void setConcept(String conc) {
	        this.concept = conc;
	    }

	    /**
	     * Get the currency.
	     * @return the currency
	     */
	    public String getCurrency() {
	        return currency;
	    }

	    /**
	     * Set the currency.
	     * @param curr the currency
	     */
	    public void setCurrency(String curr) {
	        this.currency = curr;
	    }

	    /**
	     * Get the type.
	     * @return the type
	     */
	    public String getType() {
	        return type;
	    }

	    /**
	     * Set the type.
	     * @param t the type
	     */
	    public void setType(String t) {
	        this.type = t;
	    }

	    /**
	     * Get if the account is visible.
	     * @return true if it is visible, false if not
	     */
	    public boolean isVisible() {
	        return visible;
	    }

	    /**
	     * Set true if it is visible.
	     * @param vis true if it is visible, false if not
	     */
	    public void setVisible(boolean vis) {
	        this.visible = vis;
	    }
	    
	    /**
	     * Get a public name of the account, which is composed of the account type,.
	     * the currency, and the last digits of the account number
	     * @return the public name
	     */
	    public String getPublicName(Resources res, boolean isCuentaOrigen) {
	        String typeName = "";
	        boolean flag=true;
	        //Here is where they specify the text to be shown.
			if((celularAsociado!=null) && (!celularAsociado.equals(""))) {
					typeName = res.getString(R.string.spei_phone_string) + " " + celularAsociado ;
					flag=false;
			}else if (!alias.equals("")) {
	        	typeName = alias;
	        }
	        else {
	        	typeName = getNombreTipoCuenta(res);
	        } 
	        
	        StringBuffer sb = new StringBuffer(typeName);
	        
	        if (!isCuentaOrigen & flag) {
		        if (alias.equals("")) {
			        sb.append(" ").append(currency);
				}
		        
		        sb.append("\n").append(Tools.hideAccountNumber(number));
			}
	        if (isCuentaOrigen & flag) {
		        sb.append(" ").append(Tools.hideAccountNumber(number));
			}
	        
	        if(flag)
	        	typeName = sb.toString();

	        return typeName;
	    }
	    
	    
	    public String getNombreTipoCuenta(Resources res) {
	    	String typeName = "";
	    	if (Constants.CHECK_TYPE.equals(type)) {
	            typeName = res.getString(R.string.accounttype_check);
	        } else if (Constants.CREDIT_TYPE.equals(type)) {
	            typeName = res.getString(R.string.accounttype_credit);
	        } else if (Constants.LIBRETON_TYPE.equals(type)) {
	            typeName = res.getString(R.string.accounttype_libreton);
	        } else if (Constants.SAVINGS_TYPE.equals(type)) {
	            typeName = res.getString(R.string.accounttype_savings);
	        } else if (Constants.EXPRESS_TYPE.equals(type)) {
	            typeName = res.getString(R.string.accounttype_express);
	        } else if (Constants.CLABE_TYPE_ACCOUNT.equals(type)) {
		       	typeName = res.getString(R.string.accounttype_clabe);
	        } else if (Constants.PREPAID_TYPE.equals(type)) {
	        	typeName = res.getString(R.string.accounttype_prepaid);
	        }
	    	
	    	return typeName;
	    }
	    
	    /**
	     * Get a the number of the account with the prefix indicating the account type.
	     * @return the account number with the type of account as prefix
	     */
	    public String getFullNumber() {
	        StringBuffer sb = new StringBuffer(type);
	        sb.append(number);
	        return sb.toString();
	    }

		public String getAlias() {
			return alias;
		}
		//One click

		public void setAlias(String alias) {
			this.alias=alias;
		}



		//SPEI NEW ATTRIBUTES SETTERS AND GETTERS REGION

		public String getCelularAsociado() {
			return celularAsociado;
		}

		public void setCelularAsociado(String celularAsociado) {
			this.celularAsociado = celularAsociado;
		}

		public String getCodigoCompania() {
			return codigoCompania;
		}

		public void setCodigoCompania(String codigoCompania) {
			this.codigoCompania = codigoCompania;
		}

		public String getDescripcionCompania() {
			return descripcionCompania;
		}

		public void setDescripcionCompania(String descripcionCompania) {
			this.descripcionCompania = descripcionCompania;
		}

		public String getFechaUltimaModificacion() {
			return fechaUltimaModificacion;
		}

		public void setFechaUltimaModificacion(String fechaUltimaModificacion) {
			this.fechaUltimaModificacion = fechaUltimaModificacion;
		}

		public String getIndicadorSPEI() {
			return indicadorSPEI;
		}

		public void setIndicadorSPEI(String indicadorSPEI) {
			this.indicadorSPEI = indicadorSPEI;
		}

		//END REGION



}
