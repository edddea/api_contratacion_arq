package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaRapidosDelegate;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class ConsultaRapidosViewController extends BaseViewController {
	private ConsultaRapidosDelegate opDelegate;
	private ListaSeleccionViewController listaSeleccion;
	private LinearLayout layoutListaSeleccion;
	
	public ConsultaRapidosViewController() {
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_consultar_rapidos);
		setTitle(R.string.bmovil_consultar_rapidos_titulo, R.drawable.bmovil_consultar_icono);
		
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		opDelegate = (ConsultaRapidosDelegate)parentViewsController.getBaseDelegateForKey(ConsultaRapidosDelegate.CONSULTA_RAPIDOS_DELEGATE_ID);
		opDelegate.setOwnerController(this);
		setDelegate(opDelegate);

		init();
	}

	private void findViews() {
		layoutListaSeleccion = (LinearLayout)findViewById(R.id.listaSeleccionLayout);
	}

	private void scaleToCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.baseContainer));
		guiTools.scale(findViewById(R.id.btnOperar));
		guiTools.scale(findViewById(R.id.btnEliminar));
	}

	private void init() {
		findViews();
		scaleToCurrentScreen();
		cargarListaSeleccion();
	}
	
	public void cargarListaSeleccion() {
		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT,	LayoutParams.WRAP_CONTENT);
		
		listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
		listaSeleccion.setDelegate(opDelegate);
		
		ArrayList<Object> encabezados = opDelegate.cargarEncabezadosListaSeleccion();
		ArrayList<Object> datos = opDelegate.cargarDatosListaSeleccion();
		
		
		listaSeleccion.setEncabezado(encabezados);
		listaSeleccion.setLista(datos);
		listaSeleccion.setTextoAMostrar((0 == datos.size()) ? getString(R.string.bmovil_consultar_rapidos_sin_rapidos) : null);
		listaSeleccion.setOpcionSeleccionada(-1);
		listaSeleccion.setAlturaFija(false);
		listaSeleccion.setNumeroFilas(datos.size());
		listaSeleccion.setExisteFiltro(false);
		listaSeleccion.setTitle(getString(R.string.bmovil_consultar_rapidos_titulo_lista_seleccion_titulo));
		listaSeleccion.setSingleLine(true);
		
		listaSeleccion.setSeleccionable((0 < datos.size()));
		listaSeleccion.setOverridedColumnWidths((0 == datos.size()) ? (new float[]{1.0f, 0.0f, 0.0f}) : (new float[] {0.25f, 0.33f, 0.29f}));
		
		listaSeleccion.cargarTabla();
		layoutListaSeleccion.addView(listaSeleccion);
	}
	
	public void onBtnOperarClick(View caller) {
		int index = listaSeleccion.getOpcionSeleccionada();
		if(-1 == index)
			return;
		
		opDelegate.operarRapido(index);
	}
	
	public void onBtnEliminarClick(View caller) {
		
	}

	/*
	 * Estos de cajon! :|
	 */
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(ConsultaRapidosDelegate.CONSULTA_RAPIDOS_DELEGATE_ID);
		super.goBack();
	}
}
