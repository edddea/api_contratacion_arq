package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import android.text.Html;
import android.util.Log;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class TextoPaperless implements ParsingHandler{
	public String estado;
	private String tcTitulo;
	private String tcSubtitulo;
	private String tcDescripcion;
	private String tcPieDeMensaje;
	private String tcFraseDelDia;	
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}	
	public String getTcSubtitulo() {
		return tcSubtitulo;
	}
	public void setTcSubtitulo(String tcSubtitulo) {
		this.tcSubtitulo = tcSubtitulo;
	}
	public String getTcDescripcion() {
		return tcDescripcion;
	}
	public void setTcDescripcion(String tcDescripcion) {
		this.tcDescripcion = tcDescripcion;
	}
	public String getTcPieDeMensaje() {
		return tcPieDeMensaje;
	}
	public void setTcPieDeMensaje(String tcPieDeMensaje) {
		this.tcPieDeMensaje = tcPieDeMensaje;
	}
	public String getTcFraseDelDia() {
		return tcFraseDelDia;
	}
	public String getTcTitulo() {
		return tcTitulo;
	}
	public void setTcTitulo(String tcTitulo) {
		this.tcTitulo = tcTitulo;
	}
	public void setTcFraseDelDia(String tcFraseDelDia) {
		this.tcFraseDelDia = tcFraseDelDia;
	}
	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		estado=decode(parser.parseNextValue("estado"));	
		tcTitulo=decode(parser.parseNextValue("tcTitulo"));
		tcSubtitulo=decode(parser.parseNextValue("tcSubtitulo"));
		tcDescripcion=decode(parser.parseNextValue("tcDescripcion"));
		tcPieDeMensaje=decode(parser.parseNextValue("tcPieDeMensaje"));
		tcFraseDelDia=decode(parser.parseNextValue("tcFraseDelDia"));		
	}
	
	private String decode(String source){
		return Html.fromHtml(source).toString();
	}



	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		if (Server.ALLOW_LOG) Log.w("Process-Paperles", "Entro Sin nada");
		
	} 
}
