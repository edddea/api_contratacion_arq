package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class TransferenciaOtrosBBVA extends FrecuenteMulticanalData implements ParsingHandler {


	/**
	 * 
	 */
	private static final long serialVersionUID = 2399946033287517553L;
	private Account cuentaOrigen;	
	private String tipoCuenta;
	private String cuentaDestino;
	private String beneficiario;
	private String importe;
	private String aliasFrecuente;
	private String tipoOperacion;
	private String frecuenteMulticanal;
	private String concepto;
	//SPEI
	private String numeroCelular;
	

	/**
	 * @return the concepto
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto the concepto to set
	 */
	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public void setFrecuenteMulticanal(String frecuenteMulticanal) {
		this.frecuenteMulticanal = frecuenteMulticanal;
	}


	/**
	 * @return the cuentaOrigen
	 */
	public Account getCuentaOrigen() {
		return cuentaOrigen;
	}


	/**
	 * @return the tipoCuenta
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}


	/**
	 * @return the beneficiario
	 */
	public String getBeneficiario() {
		return beneficiario;
	}


	/**
	 * @return the importe
	 */
	public String getImporte() {
		return importe;
	}


	/**
	 * @return the aliasFrecuente
	 */
	public String getAliasFrecuente() {
		return aliasFrecuente;
	}


	/**
	 * @return the tipoOperacion
	 */
	public String getTipoOperacion() {
		return tipoOperacion;
	}


	/**
	 * @return the frecuenteMulticanal
	 */
	public String getFrecuenteMulticanal() {
		return frecuenteMulticanal;
	}

	public void setCuentaOrigen(Account cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}


	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}


	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}


	public void setImporte(String importe) {
		this.importe = importe;
	}


	public void setAliasFrecuente(String aliasFrecuente) {
		this.aliasFrecuente = aliasFrecuente;
	}


	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}


	public String getCuentaDestino() {
		return cuentaDestino;
	}


	public void setCuentaDestino(String cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}

	//SPEI Revisar
	public String getNumeroCelular() {
		return numeroCelular;
	}

	
	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}
//Termina SPEI


	@Override
	public void process(Parser parser) throws IOException, ParsingException {

	//parser de tags 
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

}
