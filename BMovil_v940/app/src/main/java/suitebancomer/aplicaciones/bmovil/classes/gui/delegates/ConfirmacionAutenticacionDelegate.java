package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;

public class ConfirmacionAutenticacionDelegate extends DelegateBaseAutenticacion {
	public final static long CONFIRMACION_AUTENTICACION_DELEGATE_ID = 0x9d1a3aed49317e48L;
	
//	private ArrayList<String> datosLista;
	private DelegateBaseAutenticacion operationDelegate;
	private boolean debePedirContrasena;
	private boolean debePedirNip;
	private Constants.TipoOtpAutenticacion tokenAMostrar;
	private boolean debePedirCVV;
	private Constants.TipoInstrumento tipoInstrumentoSeguridad;
	//private String textoInstrumentoSeguridad;
	
	private ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController;

	private boolean debePedirTarjeta;
	//AMZ
		public boolean res = false;
	
	public ConfirmacionAutenticacionDelegate(DelegateBaseAutenticacion delegateBaseAutenticacion) {
		this.operationDelegate = delegateBaseAutenticacion;
		debePedirContrasena = operationDelegate.mostrarContrasenia();
		debePedirNip = operationDelegate.mostrarNIP();
		debePedirCVV = operationDelegate.mostrarCVV();
		tokenAMostrar = operationDelegate.tokenAMostrar();
		debePedirTarjeta = mostrarCampoTarjeta();
		String instrumento = Session.getInstance(SuiteApp.appContext).getSecurityInstrument();
		if (instrumento.equals(Constants.IS_TYPE_DP270)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
		} else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
		} else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
		} else {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
		}
		
		//textoInstrumentoSeguridad = operationDelegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumentoSeguridad);
	}
	
	public void setConfirmacionAutenticacionViewController(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController) {
		this.confirmacionAutenticacionViewController = confirmacionAutenticacionViewController;
	}

	public void consultaDatosLista() {
		confirmacionAutenticacionViewController.setListaDatos(operationDelegate.getDatosTablaConfirmacion());
	}
	
	public DelegateBaseAutenticacion consultaOperationsDelegate() {
		return operationDelegate;
	}
	
	public boolean consultaDebePedirContrasena() {
		return debePedirContrasena;
	}
	
	public boolean consultaDebePedirNIP() {
		return debePedirNip;
	}
	
	public boolean consultaDebePedirCVV() {
		return debePedirCVV;
	}
	
	public Constants.TipoInstrumento consultaTipoInstrumentoSeguridad() {
		return tipoInstrumentoSeguridad;
	}
	
	public Constants.TipoOtpAutenticacion consultaInstrumentoSeguridad() {
		return tokenAMostrar;
	}
	
	public void enviaPeticionOperacion() {
		String contrasena = null;
		String nip = null;
		String asm = null;
		String cvv = null;
		res = false;

		OnClickListener listener = new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				dialog.dismiss();
				confirmacionAutenticacionViewController.habilitarBtnContinuar();
			}
		};
		if (debePedirContrasena) {
			contrasena = confirmacionAutenticacionViewController.pideContrasena();
			if (contrasena.equals("")) {
				String mensaje = confirmacionAutenticacionViewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += confirmacionAutenticacionViewController.getString(R.string.confirmation_componenteContrasena);
				mensaje += ".";
				confirmacionAutenticacionViewController.showInformationAlert(mensaje,listener);
				return;
			} else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
				String mensaje = confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.PASSWORD_LENGTH;
				mensaje += " ";
				mensaje += confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += confirmacionAutenticacionViewController.getString(R.string.confirmation_componenteContrasena);
				mensaje += ".";
				confirmacionAutenticacionViewController.showInformationAlert(mensaje,listener);
				return;
			}
		}
				
		String tarjeta = null;
		if(debePedirTarjeta){
			tarjeta = confirmacionAutenticacionViewController.pideTarjeta();
			String mensaje = "";
			if(tarjeta.equals("")){
				mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
				confirmacionAutenticacionViewController.showInformationAlert(mensaje, listener);
				return;
			}else if(tarjeta.length() != 5){
				mensaje =  "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
				confirmacionAutenticacionViewController.showInformationAlert(mensaje, listener);
				return;
			}			
		}
		
		if (debePedirNip) {
			nip = confirmacionAutenticacionViewController.pideNIP();
			if (nip.equals("")) {
				String mensaje = confirmacionAutenticacionViewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += confirmacionAutenticacionViewController.getString(R.string.confirmation_componenteNip);
				mensaje += ".";
				confirmacionAutenticacionViewController.showInformationAlert(mensaje,listener);
				return;
			} else if (nip.length() != Constants.NIP_LENGTH) {
				String mensaje = confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.NIP_LENGTH;
				mensaje += " ";
				mensaje += confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += confirmacionAutenticacionViewController.getString(R.string.confirmation_componenteNip);
				mensaje += ".";
				confirmacionAutenticacionViewController.showInformationAlert(mensaje,listener);
				return;
			}
		}
		if (tokenAMostrar != Constants.TipoOtpAutenticacion.ninguno) {
			asm = confirmacionAutenticacionViewController.pideASM();
			if (asm.equals("")) {
				String mensaje = confirmacionAutenticacionViewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
				mensaje += ".";
				confirmacionAutenticacionViewController.showInformationAlert(mensaje,listener);
				return;
			} else if (asm.length() != Constants.ASM_LENGTH) {
				String mensaje = confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.ASM_LENGTH;
				mensaje += " ";
				mensaje += confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}	
				mensaje += ".";
				confirmacionAutenticacionViewController.showInformationAlert(mensaje,listener);
				return;
			}
		}
		if (debePedirCVV) {
			cvv = confirmacionAutenticacionViewController.pideCVV();
			if (cvv.equals("")) {
				String mensaje = confirmacionAutenticacionViewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += confirmacionAutenticacionViewController.getString(R.string.confirmation_componenteCvv);
				mensaje += ".";
				confirmacionAutenticacionViewController.showInformationAlert(mensaje,listener);
				return;
			} else if (cvv.length() != Constants.CVV_LENGTH) {
				String mensaje = confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.CVV_LENGTH;
				mensaje += " ";
				mensaje += confirmacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += confirmacionAutenticacionViewController.getString(R.string.confirmation_componenteCvv);
				mensaje += ".";
				confirmacionAutenticacionViewController.showInformationAlert(mensaje,listener);
				return;
			}
		}
		
		String newToken = null;
		if(tokenAMostrar != TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == TipoInstrumento.SoftToken && SuiteApp.getSofttokenStatus())
			newToken = loadOtpFromSofttoken(tokenAMostrar);
		if(null != newToken)
			asm = newToken;
		
		operationDelegate.realizaOperacion(confirmacionAutenticacionViewController, contrasena, nip, asm, cvv, tarjeta);
		confirmacionAutenticacionViewController.habilitarBtnContinuar();
		res = true;

	}
	
	@Override
	public String getEtiquetaCampoContrasenia() {
		return confirmacionAutenticacionViewController.getString(R.string.confirmation_aut_contrasena);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenActivado() {
		return confirmacionAutenticacionViewController.getString(R.string.confirmation_softtokenActivado);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenDesactivado() {
		return confirmacionAutenticacionViewController.getString(R.string.confirmation_softtokenDesactivado);
	}
	
	@Override
	public String getEtiquetaCampoCVV() {
		return confirmacionAutenticacionViewController.getString(R.string.confirmation_CVV);
	}
	
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if(response.getStatus() == ServerResponse.OPERATION_ERROR){
			confirmacionAutenticacionViewController.limpiarCampos();
			((BmovilViewsController)confirmacionAutenticacionViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
		}
		operationDelegate.analyzeResponse(operationId, response);
	}
	
	public DelegateBaseAutenticacion getOperationDelegate() {
		return operationDelegate;
	}
	
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		return tokenAMostrar;
	}
	
	@Override
	public boolean mostrarCampoTarjeta() {
		return operationDelegate.mostrarCampoTarjeta();
	}
	
	@Override
	public String loadOtpFromSofttoken(TipoOtpAutenticacion tipoOTP) {
		return loadOtpFromSofttoken(tipoOTP, operationDelegate);
	}
}
