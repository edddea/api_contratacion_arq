package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;



import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirMisCuentasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelper;

public class TransferirMisCuentasSeleccionViewController extends BaseViewController {

	public LinearLayout vista;
	public TransferirMisCuentasDelegate delegate;
	public CuentaOrigenViewController componenteCtaOrigen;
	public ListaSeleccionViewController listaSeleccion;
	public BmovilViewsController parentManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_transfer_mis_cuentas_seleccion_view);
		setTitle(R.string.opcionesTransfer_menu_miscuentas,R.drawable.bmovil_transferir_icono);
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
			
				TrackingHelper.trackState("miscuentas", parentManager.estados);
		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID));
		delegate = (TransferirMisCuentasDelegate)getDelegate();
		delegate.setCallerController(this);
		vista = (LinearLayout)findViewById(R.id.transfer_mis_cuentas_layout);
		muestraComponenteCuentaOrigen();
	}
	
	@SuppressWarnings("deprecation")
	public void muestraComponenteCuentaOrigen(){
		GuiTools.getCurrent().init(getWindowManager());
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		params.topMargin = 8;
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
//		params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);

		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
		componenteCtaOrigen.setDelegate(delegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.init();
		componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.transferir);
		vista.addView(componenteCtaOrigen);
		muestraListaCuentas();
	}
	
	@SuppressWarnings("deprecation")
	public void muestraListaCuentas(){
		delegate.setCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuntasListaSeleccion();
		
		ArrayList<Object> registros;
		ArrayList<Object> lista = new ArrayList<Object>();
		ArrayList<Object> encabezado = new ArrayList<Object>();
		encabezado.add(null);
		encabezado.add(getString(R.string.transferir_lista_seleccion_tipo_cuenta));
		encabezado.add(getString(R.string.transferir_lista_seleccion_cuenta));

		if (listaCuetasAMostrar != null) {
			for (int i = 0; i < listaCuetasAMostrar.size(); i++) {
				if(!listaCuetasAMostrar.get(i).getType().equals("IN")) {
					registros = new ArrayList<Object>();
					registros.add(listaCuetasAMostrar.get(i));

					if(!listaCuetasAMostrar.get(i).getAlias().equals("")){
						registros.add(listaCuetasAMostrar.get(i).getAlias());
					}else {
					registros.add(getTipoCuenta(getResources(), listaCuetasAMostrar.get(i).getType()));
					}

					registros.add(Tools.hideAccountNumber(listaCuetasAMostrar.get(i).getNumber()));
					lista.add(registros);
				}
			}
		}else if (listaCuetasAMostrar == null){
			registros = new ArrayList<Object>();
			registros.add("");
			registros.add("");
			lista.add(registros);
		}
		
		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.topMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_border_margin);
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.mis_cuentas_table_side_margin);
//		params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.mis_cuentas_table_side_margin);
		params.bottomMargin = GuiTools.getCurrent().getEquivalenceFromScaledPixels( getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_border_margin));		

		if (listaSeleccion == null) {
			listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
			listaSeleccion.setDelegate(delegate);
			listaSeleccion.setNumeroColumnas(2);
			listaSeleccion.setEncabezado(encabezado);
			listaSeleccion.setTitle(getString(R.string.transferir_lista_seleccion_titulo));
			listaSeleccion.setLista(lista);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(7);
			listaSeleccion.setExisteFiltro(false);
			listaSeleccion.cargarTabla();
			vista.addView(listaSeleccion);

		}else {
			listaSeleccion.setLista(lista);
			listaSeleccion.setOpcionSeleccionada(lista.indexOf(delegate.getInnerTransaction().getCuentaDestino()));
			listaSeleccion.cargarTabla();
		}
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled(true);
	}
	
	public void opcionSeleccionada(Account cuenta){
		//ARR
				Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
				
		if (delegate.getInnerTransaction().getCuentaOrigen().getType().equals(Constants.CREDIT_TYPE)) {
			if (delegate.getInnerTransaction().getCuentaOrigen().getType().equals(cuenta.getType())) {
				this.showInformationAlert(R.string.transferir_validacion_tdc_a_tdc);
			}else {
				delegate.guardaTransferenciaInterna(cuenta);
				listaSeleccion.cargarTabla();
				delegate.showTransferMisCuentasDetalle();
			}
		} else {

			//ARR
			paso1OperacionMap.put("evento_paso1", "event46");
			paso1OperacionMap.put("&&products", "operaciones;transferencias+mis cuentas");
			paso1OperacionMap.put("eVar12", "paso1:eleccion cuenta");

			TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
			delegate.guardaTransferenciaInterna(cuenta);
			listaSeleccion.cargarTabla();
			delegate.showTransferMisCuentasDetalle();
		}
	}
	
	public String getTipoCuenta(Resources res, String type)
	{
		String tipoCuenta = "";
        if (Constants.CHECK_TYPE.equals(type)) {
        	tipoCuenta = res.getString(R.string.accounttype_check);
        } else if (Constants.CREDIT_TYPE.equals(type)) {
        	tipoCuenta = res.getString(R.string.accounttype_credit);
        } else if (Constants.LIBRETON_TYPE.equals(type)) {
        	tipoCuenta = res.getString(R.string.accounttype_libreton);
        } else if (Constants.SAVINGS_TYPE.equals(type)) {
        	tipoCuenta = res.getString(R.string.accounttype_savings);
        } else if (Constants.EXPRESS_TYPE.equals(type)) {
        	tipoCuenta = res.getString(R.string.accounttype_express);
        } else if (Constants.CLABE_TYPE_ACCOUNT.equals(type)) {
        	tipoCuenta = res.getString(R.string.accounttype_clabe);
        }else if (Constants.PREPAID_TYPE.equals(type)) {
			tipoCuenta = res.getString(R.string.accounttype_prepaid);
		}
        
        return tipoCuenta;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		if (delegate != null) {
			delegate.setCallerController(this);
			componenteCtaOrigen.setIndiceCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getListaCuentaOrigen().indexOf(delegate.getInnerTransaction().getCuentaOrigen()));
			//componenteCtaOrigen.actualizaComponente();
			componenteCtaOrigen.actualizaComponente(false);
			muestraListaCuentas();
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		super.goBack();
	}
	
	public void actualizaComponenteCtaOrigen(){
		componenteCtaOrigen.getCuentaOrigenDelegate().setCuentaSeleccionada(delegate.getInnerTransaction().getCuentaOrigen());
		//componenteCtaOrigen.actualizaComponente();
		componenteCtaOrigen.actualizaComponente(false);

	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if(MotionEvent.ACTION_DOWN == ev.getAction()) {
			if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Touch Event Action: ACTION_DOWN");
			listaSeleccion.setMarqueeEnabled(false);
		}else if(MotionEvent.ACTION_UP == ev.getAction()) {
			if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Touch Event Action: ACTION_UP");
			listaSeleccion.setMarqueeEnabled(true);
		}
		
		return super.dispatchTouchEvent(ev);
	}
}
