package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ContratacionBmovilData implements ParsingHandler {
	/**
	 *  Folio de la arquitectura que devuelve la trasacci�n.
	 */
	private String folioArq;
	
	/**
	 * @return Folio de la arquitectura que devuelve la trasacci�n.
	 */
	public String getFolioArq() {
		return folioArq;
	}

	/**
	 * @param folioArq Folio de la arquitectura que devuelve la trasacci�n.
	 */
	public void setFolioArq(String folioArq) {
		this.folioArq = folioArq;
	}

	public ContratacionBmovilData() {
		folioArq = null;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		folioArq = null;
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		folioArq = parser.parseNextValue("folioArq");
	}
}
