package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;



import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DineroMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import tracking.TrackingHelper;



import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class ConsultarDineroMovilViewController extends BaseViewController {
	//AMZ
			public BmovilViewsController parentManager;
			//AMZ
	/**
	 * Layout base, contenedor principal dela vista.
	 */
	private LinearLayout rootLayout;
	
	/**
	 * Etiqueta de t�tulo para el filtro se movimientos.
	 */
	private TextView lblSelecciona;
	
	/**
	 * Combobox de selecci�n de movimientos.
	 */
	private TextView comboFiltro;
	
	/**
	 * Layout para el componente que mostrara la lista de movimientos.
	 */
	private LinearLayout listaEnviosLayout;
	
	/**
	 * Componente de lista de movimientos de dinero m�vil.
	 */
	private ListaSeleccionViewController listaEnvios;
	
	/**
	 * Delegado de la vista.
	 */
	private DineroMovilDelegate delegate;
	
	/**
	 * Lista de envios de dinero movil.
	 */
	private ArrayList<Object> envios;
	
	/**
	 * @return La lista de envios de dinero movil.
	 */
	public ArrayList<Object> getEnvios() {
		return envios;
	}

	/**
	 * @param envios La lista de envios de dinero movil.
	 */
	public void setEnvios(ArrayList<Object> envios) {
		this.envios = envios;
	}
	
	/*
	 * No javadoc.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_consulta_dinero_movil);
		setTitle(R.string.bmovil_consultar_dineromovil_titulo, R.drawable.bmovil_consultar_icono);		
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("movil", parentManager.estados);
		
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID));
		delegate = (DineroMovilDelegate)getDelegate();
		delegate.setViewController(this);
		
		init();
	}
	
	/**
	 * Inicializa el controlador de la ventana.
	 */
	private void init() {
		envios = new ArrayList<Object>();
		envios.add(new ArrayList<String>());
		
		findViews();
		cargaListaDatos();
		scaleForCurrentScreen();
	}
	
	/**
	 * Busca las vistas usadas y guarda sus referencias.
	 */
	private void findViews() {
		rootLayout = (LinearLayout)findViewById(R.id.rootLayout);
		lblSelecciona = (TextView)findViewById(R.id.lblSelecciona);
		comboFiltro = (TextView)findViewById(R.id.comboboxFiltroEstado);
		listaEnviosLayout = (LinearLayout)findViewById(R.id.layoutListaEnvios);
	}
	
	/**
	 * Redimensiona las vistas para que mantegan la relacion de aspecto en cualquier pantalla.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scalePaddings(rootLayout);
		guiTools.scale(lblSelecciona, true);
		guiTools.scale(comboFiltro, true);
		guiTools.scale(listaEnviosLayout);
	}
	
	/**
	 * Carga la lista movimientos de dinero movil.
	 */
	private void cargaListaDatos() {
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		//envios = delegate.cargarMovimientosDineroMovil(Constants.MOVIMIENTOS_DINERO_MOVIL_VIGENTES);
		ArrayList<Object> encabezados = delegate.getHeaderListaMovimientos();
		ArrayList<Object> datos = new ArrayList<Object>();
		
		listaEnvios = new ListaSeleccionViewController(this, params, parentViewsController);
		listaEnvios.setDelegate(delegate);
		listaEnvios.setEncabezado(encabezados);
		
		listaEnvios.setNumeroColumnas(4);
		listaEnvios.setTitle(getString(R.string.bmovil_consultar_dineromovil_movimientos_lista_titulo));
		listaEnvios.setOpcionSeleccionada(-1);
		listaEnvios.setSeleccionable(false);
		listaEnvios.setAlturaFija(true);
		listaEnvios.setNumeroFilas(datos.size());
		listaEnvios.setSingleLine(true);
		
		listaEnvios.setLista(datos);
		listaEnvios.setTextoAMostrar(getString(R.string.bmovil_consultar_dineromovil_sin_envios));
		listaEnvios.cargarTabla();
		delegate.consultarMovimientosDM(Constants.TipoMovimientoDM.VIGENTES);
		
		listaEnviosLayout.addView(listaEnvios);
	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		this.delegate.analyzeResponse(operationId, response);
	}

	/**
	 * Manejador para el evento de click de un movimiento.
	 * @param view
	 */
	public void onMovimientoSeleccionado(View view) {
		
	}
	
	public void onTipoMovimientoSeleccionado(Constants.TipoMovimientoDM tipo) {
		String tipoMovimiento = "";
		switch (tipo) {
		case VIGENTES:
			tipoMovimiento = getString(R.string.bmovil_consultar_dineromovil_movimientos_tipo_vigente);
			break;
		case CANCELADOS:
			tipoMovimiento = getString(R.string.bmovil_consultar_dineromovil_movimientos_tipo_cancelado);
			break;
		case VENCIDOS:
			tipoMovimiento = getString(R.string.bmovil_consultar_dineromovil_movimientos_tipo_vencido);
			break;
		default:
			tipoMovimiento = "";
			break;
		}
		comboFiltro.setText(tipoMovimiento);
		delegate.consultarMovimientosDM(tipo);
	}
	
	public void onTipoMovimientoClick(View view) {
		delegate.setViewController(this);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		final String[] importes = new String[] {getString(R.string.bmovil_consultar_dineromovil_movimientos_tipo_vigente), 
												getString(R.string.bmovil_consultar_dineromovil_movimientos_tipo_cancelado), 
												getString(R.string.bmovil_consultar_dineromovil_movimientos_tipo_vencido)};
		
		builder.setTitle(R.string.bmovil_consultar_dineromovil_movimientos_tipo_titulo).setItems(importes, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				if(Constants.TipoMovimientoDM.VIGENTES.value == which) {
					onTipoMovimientoSeleccionado(Constants.TipoMovimientoDM.VIGENTES);
				} else if(Constants.TipoMovimientoDM.CANCELADOS.value == which) {
					onTipoMovimientoSeleccionado(Constants.TipoMovimientoDM.CANCELADOS);
				} else if(Constants.TipoMovimientoDM.VENCIDOS.value == which) {
					onTipoMovimientoSeleccionado(Constants.TipoMovimientoDM.VENCIDOS);
				}
				
			}
		}).show();
	}

	/**
	 * @return the listaEnvios
	 */
	public ListaSeleccionViewController getListaEnvios() {
		return listaEnvios;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		if(null != listaEnvios) {
			setDelegate(parentViewsController.getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID));
			delegate = (DineroMovilDelegate)getDelegate();
			delegate.setViewController(this);
			listaEnvios.setDelegate(this.delegate);
		}
		
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if(MotionEvent.ACTION_DOWN == ev.getAction()) {
			if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Touch Event Action: ACTION_DOWN");
			listaEnvios.setMarqueeEnabled(false);
		}else if(MotionEvent.ACTION_UP == ev.getAction()) {
			if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Touch Event Action: ACTION_UP");
			listaEnvios.setMarqueeEnabled(true);
		}
		
		return super.dispatchTouchEvent(ev);
	}
}
