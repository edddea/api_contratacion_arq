package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.R.drawable;
import com.bancomer.mbanking.R.id;
import com.bancomer.mbanking.R.layout;
import com.bancomer.mbanking.R.string;

import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaInterbancariosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MisCuentasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MovimientosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagoTdcDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirMisCuentasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.AmountField;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import tracking.TrackingHelper;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

public class PagoTdcViewController extends BaseViewController implements OnClickListener{

	private PagoTdcDelegate delegate;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	private ScrollView vista;
	private LinearLayout cuentaRetiroLO;
	private LinearLayout fechaLayout;
	private LinearLayout  fechaCorteLayout;
	private LinearLayout listaTDCLayout;
	
	private LinearLayout errorGenericoLayout;

	
	private CuentaOrigenViewController componenteCtaOrigen;
	private EditText textCuentaDestino;
	private ImageButton continuarButton;
	private AmountField textImporte;
	private EditText txtMotivo;
	
	private TextView fecha;
	private TextView fechaCorte;
	private TextView pagoMinimo;
	private TextView pagoNoIntereses;
	private TextView saldoCorte;
	private TextView saldoActual;
	private TextView puntosBancomer;
	
	private RadioButton radioPagoMinimo;
	private RadioButton radioPagoNoIntereses;
	private RadioButton radioSaldoCorte;
	private RadioButton radioOtraCantidad;
	private RadioButton radioSaldoActual;
	
	private LinearLayout radioPagoMinimolinear;
	private LinearLayout radioPagoNoIntereseslinear;
	private LinearLayout radioSaldoCortelinear;
	private LinearLayout radioOtraCantidadlinear;
	private LinearLayout radioSaldoActuallinear;
	
	public CuentaOrigenViewController getComponenteCtaOrigen() {
		return componenteCtaOrigen;
	}


	public void setComponenteCtaOrigen(
			CuentaOrigenViewController componenteCtaOrigen) {
		this.componenteCtaOrigen = componenteCtaOrigen;
	}


	/**
	 * Default constructor for this activity
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.activity_pago_tdc_view_controller);
		//marqueeEnabled = true;
		setTitle(R.string.transferir_otrosBBVA_TDC_title, R.drawable.bmovil_mis_cuentas_icono);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("pagar tdc", parentManager.estados);
		
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		delegate = (PagoTdcDelegate)parentViewsController.getBaseDelegateForKey(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID);
		delegate.setPagoTdcViewController(this);
		
		//Toast.makeText(SuiteApp.getInstance(),"onCreate PAgo Tdc", Toast.LENGTH_LONG).show();

		

		
		init();
	}
	

	public void init(){
		findViews();
		scaleForCurrentScreen();
		muestraComponenteCuentaOrigen();
		delegate.consultaImportesTDC();
		// Set TDC number
		textCuentaDestino.setText(Tools.enmascaraCuentaDestinoPagoTDC(delegate.getCuentaTDC().getNumber()));
		
		InputFilter[] filterImporte = {new InputFilter.LengthFilter(Constants.AMOUNT_LENGTH_PAGO_TDC)};
		textImporte.setFilters(filterImporte);
//		textCuentaDestino.addTextChangedListener(new BmovilTextWatcher(this));
//		textImporte.addTextChangedListener(new BmovilTextWatcher(this));
//		txtMotivo.addTextChangedListener(new BmovilTextWatcher(this));

	}
	
	@SuppressWarnings("deprecation")
	public void muestraComponenteCuentaOrigen(){
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
		componenteCtaOrigen.setDelegate(delegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		if(delegate.getCuentaOrigenSeleccionada() != null){
			componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(delegate.getCuentaOrigenSeleccionada()));
		}else{
			componenteCtaOrigen.setIndiceCuentaSeleccionada(0);
		}
		componenteCtaOrigen.init();
		componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.transferir);
		cuentaRetiroLO.addView(componenteCtaOrigen);
//		delegate.setCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		cargaCuentaSeleccionada();
	}
	
	public void cargaCuentaSeleccionada(){
//		textCuentaDestino.setText(Tools.enmascaraCuentaDestino(delegate.getInnerTransaction().getCuentaDestino().getNumber()));
	}
	

	/********************************************************* CheckNegativosTDC *******************************************************/
	
	/**
	 * Recupera los importes del radio pasado como parametro a excepcion de "Otra cantidad"
	 * @param radio
	 * @return
	 */
	private String getImporteByRadioNoOther(RadioButton radio){
		String ret = "";
		if(delegate.getResImpTDC() != null){
			if(radio.equals(radioPagoMinimo)){
				ret = delegate.getResImpTDC().getPagoMinimo();
			}else if(radio.equals(radioPagoNoIntereses)){
				ret = delegate.getResImpTDC().getPagoNogeneraInt();
			}else if(radio.equals(radioSaldoCorte)){
				ret = delegate.getResImpTDC().getSaldoAlCorte();
			}else if(radio.equals(radioSaldoActual)){
				ret = Tools.convertDoubleToBigDecimalAndReturnString(delegate.getCuentaTDC().getBalance());
			}
		}
		
		return ret;
	}
	
	/**
	 * Recupera la lista de todos los radios presentes en la ventana a excepcion de "Otra cantidad"
	 * @return
	 */
	private ArrayList<RadioButton> getRadioListNoOther(){
		ArrayList<RadioButton> listaData = new ArrayList<RadioButton>();
		listaData.add(radioPagoMinimo);
		listaData.add(radioPagoNoIntereses);
		listaData.add(radioSaldoCorte);
		listaData.add(radioSaldoActual);
		
		return listaData;
	}
	
	/**
	 * Recupera el Linear superior al radio pasado como parametro el cual tiene el listener del click a excepcion de "Otra cantidad"
	 * @param radio
	 * @return
	 */
	private LinearLayout getRadioSuperViewNoOther(RadioButton radio){
		
		LinearLayout ret = null;
		
		if(radio.equals(radioPagoMinimo)){
			ret = radioPagoMinimolinear;
			
		}else if(radio.equals(radioPagoNoIntereses)){
			ret = radioPagoNoIntereseslinear;
			
		}else if(radio.equals(radioSaldoCorte)){
			ret = radioSaldoCortelinear;
			
		}else if(radio.equals(radioSaldoActual)){
			ret = radioSaldoActuallinear;
			
		}
		
		return ret;
			
	}
	
	/**
	 * Comprueba si el importe del radio dado como parametro es negativo o no
	 * @param radio
	 * @return
	 */
	private Boolean isNegativeRadioNoOther(RadioButton radio){
		return getImporteByRadioNoOther(radio).startsWith("-");
	}
	
	/**
	 * Deshabilita el click en la lista de radios en caso de tener un importe negativo a excepcion de "Otra cantidad"
	 * @param lista
	 * @param index
	 */
	private void isNegativeTdcData(ArrayList<RadioButton> lista, Integer index){
		
		if(lista.size() == index){
			// Finish
		}else{
			// Si se procesa el elemento
			
			RadioButton radio = lista.get(index);
			// Comprobamos negatividad
			if(isNegativeRadioNoOther(radio)){
				deshabilitaRadioOnNegative(radio);
			}
			
			isNegativeTdcData(lista, ++index );
		}
		
	}
	
	/**
	 * Deshabilita el click en la lista de radios en caso de tener un importe negativo a excepcion de "Otra cantidad"
	 */
	public void checkNegativeInTdcData(){
		
		if(delegate.getResImpTDC() != null){
			ArrayList<RadioButton> listaData = getRadioListNoOther();
			
			isNegativeTdcData(listaData, 0);
		}		
	}
	
	/**
	 * Deshabilita un radio y su super Layout
	 * @param radio
	 */
	private void deshabilitaRadioOnNegative(RadioButton radio){
		radio.setEnabled(false);
		
		// Si tiene superLayout anulamos el click
		LinearLayout superView = getRadioSuperViewNoOther(radio);
		if(superView != null) superView.setOnClickListener(null);
	}

	/********************************************************* End CheckNegativosTDC *******************************************************/
	
	/**
	 * Establece el comportamiento de los radios al entrar a la ventana
	 * @param esError
	 */
	public void setUpRadios(Boolean esError){
		
		if(esError){
			// Tratamiento de los combos en caso de que haya error en la peticion de importes tdc
			changeRadio(radioOtraCantidad);
		}else{
			// Tratamiento normal de los combos
			
			// Comprobamos negatividad
			checkNegativeInTdcData();
			
			if(isNegativeRadioNoOther(radioPagoMinimo)){	
				// Habiltamos solo otra cantidad si pagoMinimo es negativo
				changeRadio(radioOtraCantidad);
			}else{
				changeRadio(radioPagoMinimo);
			}
		}
	}
	
	private void changeRadio(RadioButton button){
		radioPagoMinimo.setChecked(false);
		radioPagoNoIntereses.setChecked(false);
		radioSaldoCorte.setChecked(false);
		radioOtraCantidad.setChecked(false);
		radioSaldoActual.setChecked(false);
		
		if(button.isChecked()){
			button.setChecked(false);
			
		}else{
			button.setChecked(true);
			
		}
		
		if(!button.equals(radioOtraCantidad)){
			textImporte.setVisibility(View.INVISIBLE);
		}else{
			textImporte.reset();
			textImporte.setVisibility(View.VISIBLE);
		}
	}
	
	public String getImporteRaw(){
		String importe = "0";
		
		if(radioPagoMinimo.isChecked()){
			importe = delegate.getResImpTDC().getPagoMinimo();
		}else if(radioPagoNoIntereses.isChecked()){
			importe = delegate.getResImpTDC().getPagoNogeneraInt();
		}else if(radioSaldoCorte.isChecked()){
			importe = delegate.getResImpTDC().getSaldoAlCorte();
		}else if(radioSaldoActual.isChecked()){
			importe = Tools.convertDoubleToBigDecimalAndReturnString(delegate.getCuentaTDC().getBalance());
		}else if(radioOtraCantidad.isChecked()){
			importe = textImporte.getAmount().replaceAll("\\.", "");
		}
		if(Server.ALLOW_LOG) System.out.println("Importe Raw: "+ importe);
		return importe;
	}
	
	public String getImporte(){
		String importe = Tools.formatAmount("0",false);
		if(radioPagoMinimo.isChecked()){
			importe = pagoMinimo.getText().toString();
		}else if(radioPagoNoIntereses.isChecked()){
			importe = radioPagoNoIntereses.getText().toString();
		}else if(radioSaldoCorte.isChecked()){
			importe = saldoCorte.getText().toString();
		}else if(radioSaldoActual.isChecked()){
			importe = saldoActual.getText().toString();
		}else if(radioOtraCantidad.isChecked()){
			importe = "$"+Tools.formatUserAmount(textImporte.getAmount());
		}
		if(Server.ALLOW_LOG) System.out.println("Importe: "+ importe);
		return importe;
	}
	
	private Boolean validaImporte(){
		Boolean ret = true;
		if(radioOtraCantidad.isChecked()){
			try{
				if(Double.valueOf(textImporte.getAmount()) <= 0 ){
					ret = false;
					showInformationAlert(R.string.error_no_import_to_pay);
				}
			}catch(Exception e){
				ret = false;
				showInformationAlert(R.string.error_no_import_to_pay);
			}
		}
		
		return ret;
	}
	
	@Override
	public void onClick(View v) {
		
		if(v.equals((LinearLayout)findViewById(R.id.linear1))){
			changeRadio(radioPagoMinimo);
			
		}else if(v.equals((LinearLayout)findViewById(R.id.linear2))){
			changeRadio(radioPagoNoIntereses);
			
		}else if(v.equals((LinearLayout)findViewById(R.id.linear3))){
			changeRadio(radioSaldoCorte);
			
		}else if(v.equals((LinearLayout)findViewById(R.id.linear4))){
			changeRadio(radioOtraCantidad);
			
		}else if(v.equals((LinearLayout)findViewById(R.id.linear5))){
			changeRadio(radioSaldoActual);
			
		}else if(v.equals(radioPagoMinimo)){
			changeRadio(radioPagoMinimo);
		}else if(v.equals(radioPagoNoIntereses)){
			changeRadio(radioPagoNoIntereses);
		}else if(v.equals(radioSaldoCorte)){
			changeRadio(radioSaldoCorte);
		}else if(v.equals(radioOtraCantidad)){
			changeRadio(radioOtraCantidad);
		}else if(v.equals(radioSaldoActual)){
			changeRadio(radioSaldoActual);
		}else if(v.equals(continuarButton)){
			if(validaImporte()){
				delegate.setDataTdcAcc();
				this.delegate.showConfirmacion();
			}
		}
	}
	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
//		delegate.setCallerController(this);
		textImporte.reset();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		super.goBack();
	}
	
	public void actualizaCuentaOrigen(Account cuenta){
		if(Server.ALLOW_LOG) Log.d("TransferirMisCuentasDetalleViewController", "Actualizar la cuenta origen actual por  "+cuenta.getNumber());
		delegate.setCuentaOrigenSeleccionada(cuenta);
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
	}

	private void findViews() {
		vista = (ScrollView)findViewById(R.id.pagoTDCLL);
		cuentaRetiroLO = (LinearLayout)findViewById(R.id.transfer_mis_cuentas_layout);
		textCuentaDestino = (EditText)findViewById(R.id.detalle_transfer_cuenta_destino_edit);
		textCuentaDestino.setEnabled(false);
		textImporte = (AmountField)findViewById(R.id.detalle_transfer_importe_edit);
		txtMotivo = (EditText)findViewById(R.id.txtMotivo);
		
		fecha = (TextView)findViewById(R.id.fecha);
		fechaCorte = (TextView)findViewById(R.id.fechaCorte);

		pagoMinimo = (TextView)findViewById(R.id.pagominImp);
		pagoNoIntereses = (TextView)findViewById(R.id.pagonointeresImp);
		saldoCorte = (TextView)findViewById(R.id.saldocorteImp);
		saldoActual = (TextView)findViewById(R.id.saldoActualImp);
		puntosBancomer = (TextView)findViewById(id.puntos_bancomer_valor);
		
		continuarButton = (ImageButton)findViewById(R.id.PTDC_boton_continuar);
		continuarButton.setOnClickListener(this);
		
		radioPagoMinimo = (RadioButton)findViewById(R.id.radioButton1);
		radioPagoMinimo.setOnClickListener(this);
		radioPagoNoIntereses = (RadioButton)findViewById(R.id.radioButton2);
		radioPagoNoIntereses.setOnClickListener(this);
		radioSaldoCorte = (RadioButton)findViewById(R.id.radioButton3);
		radioSaldoCorte.setOnClickListener(this);
		radioOtraCantidad = (RadioButton)findViewById(R.id.radioButton4);
		radioOtraCantidad.setOnClickListener(this);
		radioSaldoActual = (RadioButton)findViewById(R.id.radioButton5);
		radioSaldoActual.setOnClickListener(this);
		
		/* Texto a ocultar */
		fechaLayout = (LinearLayout)findViewById(R.id.linear0);
		fechaCorteLayout = (LinearLayout)findViewById(R.id.linear01);
		listaTDCLayout = (LinearLayout)findViewById(R.id.lista);
		
		//inicialmente esta oculto
		errorGenericoLayout = (LinearLayout)findViewById(R.id.labelErrorTdc);
		errorGenericoLayout.setVisibility(View.GONE);
		
		// Inicializar boton linears
		radioPagoMinimolinear = (LinearLayout)findViewById(R.id.linear1);
		radioPagoMinimolinear.setOnClickListener(this);
		radioPagoNoIntereseslinear = (LinearLayout)findViewById(R.id.linear2);
		radioPagoNoIntereseslinear.setOnClickListener(this);
		radioSaldoCortelinear = (LinearLayout)findViewById(R.id.linear3);
		radioSaldoCortelinear.setOnClickListener(this);
		radioOtraCantidadlinear = (LinearLayout)findViewById(R.id.linear4);
		radioOtraCantidadlinear.setOnClickListener(this);
		radioSaldoActuallinear = (LinearLayout)findViewById(R.id.linear5);
		radioSaldoActuallinear.setOnClickListener(this);

	}
	
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.rootLayout));
		
		guiTools.scale(findViewById(R.id.transfer_mis_cuentas_layout));
		guiTools.scale(findViewById(R.id.detalle_transfer_cuenta_destino_text), true);
		guiTools.scale(findViewById(R.id.detalle_transfer_cuenta_destino_edit));
		

		guiTools.scale(findViewById(R.id.accdataLayout));
		guiTools.scale(findViewById(R.id.linear0));
		guiTools.scale(findViewById(R.id.linear1));
		guiTools.scale(findViewById(R.id.linear2));
		guiTools.scale(findViewById(R.id.sublinear2));
		guiTools.scale(findViewById(R.id.linear3));
		guiTools.scale(findViewById(R.id.linear4));
		guiTools.scale(findViewById(R.id.labelErrorTdc));


		guiTools.scale(findViewById(R.id.errorTdc), true);
		guiTools.scale(findViewById(R.id.fechCorte), true);
		guiTools.scale(findViewById(R.id.fechaCorte), true);
		guiTools.scale(findViewById(R.id.fechLimite), true);
		guiTools.scale(findViewById(R.id.importe), true);
		guiTools.scale(findViewById(R.id.pagomin), true);
		guiTools.scale(findViewById(R.id.pagonointereses), true);
		guiTools.scale(findViewById(R.id.pagonointereses2), true);
		guiTools.scale(findViewById(R.id.saldoalcorte), true);
		guiTools.scale(findViewById(R.id.otrascantidad), true);
		guiTools.scale(findViewById(R.id.saldoActual), true);
		guiTools.scale(findViewById(R.id.fecha), true);
		guiTools.scale(findViewById(R.id.pagominImp), true);
		guiTools.scale(findViewById(R.id.pagonointeresImp), true);
		guiTools.scale(findViewById(R.id.saldocorteImp), true);
		guiTools.scale(findViewById(R.id.saldoActualImp), true);
		guiTools.scale(findViewById(R.id.radioButton1));
		guiTools.scale(findViewById(R.id.radioButton2));
		guiTools.scale(findViewById(R.id.radioButton3));
		guiTools.scale(findViewById(R.id.radioButton4));
		guiTools.scale(findViewById(R.id.radioButton5));


		guiTools.scale(findViewById(id.puntos_bancomer_valor));
		guiTools.scale(findViewById(id.puntos_bancomer));
		guiTools.scale(findViewById(id.linear02));

		guiTools.scale(findViewById(R.id.detalle_transfer_importe_edit), true);
		
		guiTools.scale(findViewById(R.id.continuarlayout));		
		guiTools.scale(continuarButton);
	}
	
	public void setDataFromTDC(){
		fecha.setText(Tools.formatDateTDC(delegate.getResImpTDC().getFechaPago()));
		
		fechaCorte.setText(Tools.formatDateTDC(delegate.getResImpTDC().getFechaCorte()));
		
		String getPagoMinimo = delegate.getResImpTDC().getPagoMinimo();
		boolean isNegative = getPagoMinimo.startsWith("-");
		pagoMinimo.setText(Tools.formatAmount(getPagoMinimo,isNegative));
		
		String getPagoNogeneraInt = delegate.getResImpTDC().getPagoNogeneraInt();
		isNegative = getPagoNogeneraInt.startsWith("-");
		pagoNoIntereses.setText(Tools.formatAmount(getPagoNogeneraInt,isNegative));
		
		String getSaldoAlCorte = delegate.getResImpTDC().getSaldoAlCorte();
		isNegative = getSaldoAlCorte.startsWith("-");
		saldoCorte.setText(Tools.formatAmount(getSaldoAlCorte,isNegative));
		
		String getSaldoActual = Tools.convertDoubleToBigDecimalAndReturnString(delegate.getCuentaTDC().getBalance());
		isNegative = getSaldoActual.startsWith("-");
		saldoActual.setText(Tools.formatAmount(getSaldoActual,isNegative));

		String getPuntosBancomer = delegate.getResImpTDC().getPuntosBancomer();
		puntosBancomer.setText(Tools.formatPuntosBancomer(getPuntosBancomer));
	}
	
	@SuppressWarnings("deprecation")
	public void muestraListaCuentas(){
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
	}
	
	public void ocultaDatosTDC(){
		listaTDCLayout.setVisibility(View.GONE);
		fechaLayout.setVisibility(View.GONE);
		fechaCorteLayout.setVisibility(View.GONE);

	}
	
	
	public void muestraErrorGenericoTDC(){

		errorGenericoLayout.setVisibility(View.VISIBLE);

	}
	
	
	
}
