package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ConsultaTerminosDeUsoData implements ParsingHandler {
	/**
	 * Terminos de uso en formato HTML.
	 */
	private String terminosHtml;
	
	/**
	 * @return Terminos de uso en formato HTML.
	 */
	public String getTerminosHtml() {
		return terminosHtml;
	}

	/**
	 * @param terminosHtml Terminos de uso en formato HTML.
	 */
	public void setTerminosHtml(String terminosHtml) {
		this.terminosHtml = terminosHtml;
	}

	public ConsultaTerminosDeUsoData() {
		terminosHtml = null;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		terminosHtml = null;
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		terminosHtml = parser.parseNextValue("textoTerminosCondiciones").replace("\u0093", "\"").replace("\u0094", "\"").replace("<&#33;---&#8226;--->","");
	}
}
