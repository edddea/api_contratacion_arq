package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;


import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

import com.bancomer.mbanking.SuiteApp;


public class PaymentExtract implements ParsingHandler {

	
	  /**
     * Number of occurrences in server response
     */
    private String strOcurrences = null;

    /**
     * The frequent payments
     */
    private Payment[] payments = null;


    /**
     * Default constructor
     */
    public PaymentExtract() {
    }

    /**
     * Constructor with parameters
     * @param accountNumber the account number
     * @param balance the current balance
     * @param previousBalance the previous balance
     * @param deadline the payment deadline
     * @param minPayment the minimum payment
     * @param movements the movements
     * @param date the balance date
     */
    public PaymentExtract(String strOcurrences, Payment[] payments) {
        this.strOcurrences = strOcurrences;
        this.payments = payments;
    }

    /**
     * Get the frequent payments
     * @return the payments
     */
    public Payment[] getPayments() {
        return payments;
    }

    /**
     * Get the number of occurrences
     * @return the number of occurrences
     */
    public String getOcurrences() {
        return strOcurrences;
    }

    /**
     * Get the size of Payments array
     * @return the number of payments
     */
    public int getSize() {
        return payments.length;
    }

    /**
     * Set the number of occurrences
     * @param the number of occurrences
     */
    public void setOcurrences(String strOcurrences) {
        this.strOcurrences = strOcurrences;
    }

    /**
     * Returns all the payments in a string
     * For debugging purposes only.
     * This function CAN be deleted
     */
    public String toString() {

        String strResult = "";
        for (int i = 0; i< Integer.parseInt(this.strOcurrences); i++) {
            strResult+= "NK: " + this.payments[i].getNickname();
            strResult+= "\nCharge account: " + this.payments[i].getChargeAccount();
            strResult+= "\nBenef account: " + this.payments[i].getBeneficiaryAccount();
            strResult+= "\nAmount: " + this.payments[i].getAmount();
            strResult+= "\nBenef: " + this.payments[i].getBeneficiary();
            strResult+= "\nRefer: " + this.payments[i].getReference();
            strResult+= "\nConcept: " + this.payments[i].getConcept();
            strResult+= "\nDesc: " + this.payments[i].getDescription();
            strResult+= "\nBank Code: " + this.payments[i].getBankCode();
            strResult+= "\nOp Code: " + this.payments[i].getOperationCode();
            strResult+= "\nAp idNumber: " + this.payments[i].getIdNumber();
			//frecuente correo electronico
			strResult+= "\nEmail: " + this.payments[i].getCorreoFrecuente();

			strResult+= "\n";
        }
        return strResult;
    }

    /**
     * Process the login response and store the attributes
     * @param parser reference to the parser
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public void process(Parser parser) throws IOException, ParsingException {

        this.payments = parsePayments(parser);
    }

    /**
     * Parse movements from parser data
     * @param parser the parser
     * @return the payments
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     * @throws NumberFormatException on format errors
     */
    private Payment[] parsePayments(Parser parser) throws IOException, NumberFormatException, ParsingException {

        int movementCount = Integer.parseInt(parser.parseNextValue("OC"));
        this.strOcurrences = String.valueOf(movementCount);
        Payment[] result = new Payment[movementCount];
        Payment[] resultFrequent = new Payment[movementCount];
        Payment[] resultPreregistered = new Payment[movementCount];
        Payment payment = null;
        // Parses individually each payment
        
        for(int i = 0; i < movementCount; i++) {
        	String strNK = parser.parseNextValue("NK");
            String strChargeAccount = parser.parseNextValue("CC");
            String strBeneficiaryAccount = parser.parseNextValue("CA");
            String strAmount = Tools.formatAmountFromServer(parser.parseNextValue("IM"));
            String strBeneficiary = parser.parseNextValue("BF");
			// Siempre se convierte a mayusculas la descripcion de la compania
			strBeneficiary = strBeneficiary.toUpperCase();
			String strReference = parser.parseNextValue("RF");
            String strConcept = parser.parseNextValue("CP");
            String strDescription = parser.parseNextValue("DE");
            String strBankCode = parser.parseNextValue("CB");
            
            // Si el código de banco tiene menos de 5 dígitos se busca el código completo.
            if(strBankCode.length() <= Constants.UNCOMPLETE_BANK_CODE_LENGTH)
            	strBankCode = completeBankCode(strBankCode);
            
            String strOperationCode = parser.parseNextValue("TO");
            String idNumber = parser.parseNextValue("AP",false);
            String strOperadora = parser.parseNextValue("OA",false);
            String indicadorFrecuente = parser.parseNextValue("IF");
            String idToken = parser.parseNextValue("IT");
            String canal = parser.parseNextValue("CN");
            String usuario = parser.parseNextValue("US");
			//frecuente correo electronico
			String correo = parser.parseNextValue("CE");

			payment = new Payment(strNK, strChargeAccount,
								  strBeneficiaryAccount, 
								  strAmount, strBeneficiary,
								  strReference, strConcept,
								  strDescription, strBankCode,
								  strOperationCode, idNumber);
			if(null != strOperadora)
				payment.setOperadora(strOperadora.toUpperCase());
			else
				payment.setOperadora(strOperadora);
           	payment.setIndicadorFrecuente(indicadorFrecuente);
           	payment.setIdToken(idToken);
           	payment.setIdCanal(canal);
           	payment.setUsuario(usuario);
			//frecuente correo electronico
			payment.setCorreoFrecuente(correo);


			if (indicadorFrecuente.equals("F")) {
           		resultFrequent[i] = payment;
           	} else if (indicadorFrecuente.equals("P")) {
           		resultPreregistered[i] = payment;
           	} 
        }
        
        int resultIndex = 0;
        for (int i=0; i<movementCount;i++) {
        	payment = resultPreregistered[i];
        	if (payment == null) {
        		continue;
        	} else {
        		result[resultIndex] = payment;
        		resultIndex++;
        	}
        }
        
        for (int i=0; i<movementCount;i++) {
        	payment = resultFrequent[i];
        	if (payment == null) {
        		continue;
        	} else {
        		result[resultIndex] = payment;
        		resultIndex++;
        	}
        }        
       	
        return result;
    }

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		this.payments= parsePayments(parser);

		
	}
	
	/**
	 * Completa el código del banco en caso de ser necesario.
	 * @param uncompleteCode El código de banco incompleto (ultimos 4 dígitos).
	 * @return El código del banco completo.
	 */
	private String completeBankCode(String uncompleteCode) {
		// Si el código de banco no es de 4 dígitos se asume que el servidor lo env�o completo (5 dígitos).
		if(uncompleteCode.length() != Constants.UNCOMPLETE_BANK_CODE_LENGTH)
			return uncompleteCode;
		
		String completeCode = "";
		Session session = Session.getInstance(SuiteApp.appContext);
		// Se obtiene el c�talogo de bancos para TDC.
		Catalog banks = session.getBanks(true);
		String[] banksKeys = banks.getCodes();
		int selectedIndex = -1;
		
		for(int i = 0; (i < banksKeys.length) && (selectedIndex < 0); i++) {
			if(banksKeys[i].endsWith(uncompleteCode))
				selectedIndex = i;
		}
		
		// Si no se encontro el código de banco en el c�talogo de TDC
		if(selectedIndex < 0) {
			// Se cargan los códigos de bancos para TDD
			banks = session.getBanks(false);
			banksKeys = banks.getCodes();
			
			for(int i = 0; (i < banksKeys.length) && (selectedIndex < 0); i++) {
				if(banksKeys[i].endsWith(uncompleteCode))
					selectedIndex = i;
			}
		}
		
		completeCode = (selectedIndex < 0) ? null : banksKeys[selectedIndex];
		return completeCode;
	}
    
//    /**
//     * 
//     */
//    private String numCliente;
//    
//    /**
//     * 
//     */
//    private String idCanal;
//    
//    /**
//     * 
//     */
//    private String usuario;
//    
//    /**
//     * 
//     */
//    private String funcion;
//    
//    /**
//     * 
//     */
//    private String subcanal;
//    
//    /**
//     * 
//     */
//    private String entidad;
//    
//    /**
//     * 
//     */
//    private String nombrePago;
//    
//    /**
//	 * @return the numCliente
//	 */
//	public String getNumCliente() {
//		return numCliente;
//	}
//
//	/**
//	 * @param numCliente the numCliente to set
//	 */
//	public void setNumCliente(String numCliente) {
//		this.numCliente = numCliente;
//	}
//
//	public String getIdCanal() {
//		return idCanal;
//	}
//
//	public void setIdCanal(String idCanal) {
//		this.idCanal = idCanal;
//	}
//
//	public String getUsuario() {
//		return usuario;
//	}
//
//	public void setUsuario(String usuario) {
//		this.usuario = usuario;
//	}
//
//	public String getFuncion() {
//		return funcion;
//	}
//
//	public void setFuncion(String funcion) {
//		this.funcion = funcion;
//	}
//
//	public String getSubcanal() {
//		return subcanal;
//	}
//
//	public void setSubcanal(String subcanal) {
//		this.subcanal = subcanal;
//	}
//
//	public String getEntidad() {
//		return entidad;
//	}
//
//	public void setEntidad(String entidad) {
//		this.entidad = entidad;
//	}
//
//	public String getNombrePago() {
//		return nombrePago;
//	}
//
//	public void setNombrePago(String nombrePago) {
//		this.nombrePago = nombrePago;
//	}
private Payment[] parsePayments(ParserJSON parser) throws IOException, NumberFormatException, ParsingException {    	
    	
    	parser.parseNextValue("estado");
    	JSONArray arrayFrecuentes = parser.parseNextValueWithArray("frecuentes", false);
    	
    	int numFrecuentes = arrayFrecuentes.length();    	   	   	
    	this.strOcurrences = String.valueOf(numFrecuentes);
    	
        Payment[] result = new Payment[numFrecuentes];
        Payment[] resultFrequent = new Payment[numFrecuentes];
        Payment[] resultPreregistered = new Payment[numFrecuentes];
        
        Payment payment = null;
        
    	for(int i = 0; i< numFrecuentes; i++ ){
    		try {
    			JSONObject asuntoObj = arrayFrecuentes.getJSONObject(i);
    			
    			String strNK = asuntoObj.getString("nombreCorto");    			    			
    			if(Server.ALLOW_LOG) Log.e("strNK",strNK+"");
    			
    			String strChargeAccount = asuntoObj.getString("cuentaCargo");
    			if(Server.ALLOW_LOG) Log.e("strChargeAccount",strChargeAccount+"");
    			
    			//String aux = asuntoObj.getString("cuentaAbono");
    			//String strBeneficiaryAccount = aux.substring(2, aux.length());
    			String strBeneficiaryAccount = asuntoObj.getString("cuentaAbono");
    			if(Server.ALLOW_LOG) Log.e("strBeneficiaryAccount",strBeneficiaryAccount+"");
    			
    			String strAmount = asuntoObj.getString("importe");
    			if(Server.ALLOW_LOG) Log.e("strAmount",strAmount+"");
    			
    			String strBeneficiary = asuntoObj.getString("beneficiario");
    			strBeneficiary = strBeneficiary.replace("%20"," ");
    			if(Server.ALLOW_LOG) Log.e("strBeneficiary",strBeneficiary+"");
    			
    			String strReference = asuntoObj.getString("referencia");
    			if(Server.ALLOW_LOG) Log.e("strReference",strReference+"");
    			
    			String strConcept = asuntoObj.getString("concepto");
    			if(Server.ALLOW_LOG) Log.e("strConcept",strConcept+"");
    			
    			
    			String strDescription = asuntoObj.getString("descripcion");
    			if(Server.ALLOW_LOG) Log.e("strDescription",strDescription+"");
    			
    			String strBankCode = asuntoObj.getString("codigoBanco");
    			
    			if(strBankCode.length() <= Constants.UNCOMPLETE_BANK_CODE_LENGTH)
    	            	strBankCode = completeBankCode(strBankCode);   			
    			if(Server.ALLOW_LOG) Log.e("strBankCode",strBankCode+"");
    			
    			String strOperationCode = asuntoObj.getString("codigoOperacion");
    			if(Server.ALLOW_LOG) Log.e("strOperationCode",strOperationCode+"");
    			String indicadorFrecuente = asuntoObj.getString("indicadorFrecuentes");
    			if(Server.ALLOW_LOG) Log.e("indicadorFrecuente",indicadorFrecuente+"");
    			String idToken = asuntoObj.getString("indicadorToken"); 
    			if(Server.ALLOW_LOG) Log.e("idToken",idToken+"");
    			String canal = asuntoObj.getString("canal"); 
    			if(Server.ALLOW_LOG) Log.e("canal",canal+"");
    			
    			String usuario = asuntoObj.getString("usuario");  
    			if(Server.ALLOW_LOG) Log.e("usuario",usuario+"");
				//frecuente correo electronico
				String correo = asuntoObj.getString("correoElectronico");
				if(Server.ALLOW_LOG) Log.e("correoElectronico",correo+"");




				payment = new Payment(strNK, strChargeAccount,
						  strBeneficiaryAccount, 
						  strAmount, strBeneficiary,
						  strReference, strConcept,
						  strDescription, strBankCode,
						  strOperationCode);
    			    			
    			payment.setIndicadorFrecuente(indicadorFrecuente);
               	payment.setIdToken(idToken);
               	payment.setIdCanal(canal);
               	payment.setUsuario(usuario);
				//frecuente correo electronico
				payment.setCorreoFrecuente(correo);



				if (indicadorFrecuente.equals("F")) {
               		resultFrequent[i] = payment;
               		
               	} else if (indicadorFrecuente.equals("P")) {
               		resultPreregistered[i] = payment;
               	} 
    			
    		}catch (JSONException e) {
    			if(Server.ALLOW_LOG) throw new ParsingException("Error formato");
    		}
    	}
    		
    		int resultIndex = 0;
            for (int j=0; j<numFrecuentes;j++) {
            	payment = resultPreregistered[j];
            	if (payment == null) {
            		continue;
            	} else {
            		result[resultIndex] = payment;
            		resultIndex++;
            	}
            }
            
            for (int k=0; k<numFrecuentes;k++) {
            	payment = resultFrequent[k];
            	if (payment == null) {
            		continue;
            	} else {
            		result[resultIndex] = payment;
            		resultIndex++;
            	}
            }        
    	 return result;    	
    }
	
	
}
