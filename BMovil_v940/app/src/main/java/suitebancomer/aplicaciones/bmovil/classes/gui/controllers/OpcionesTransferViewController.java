package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;



import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DineroMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelper;

public class OpcionesTransferViewController extends BaseViewController {

	LinearLayout vista;
	ListaSeleccionViewController listaSeleccion;
	TextView titulo;
	ArrayList<Object> lista;
	TransferirDelegate delegate;
	Bundle extras;
	BmovilViewsController parentManager;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE,
				R.layout.layout_bmovil_opciones_transfer);
		extras = getIntent().getExtras();
		setTitle(extras.getInt(Constants.PANTALLA_BASE_TITULO),
				extras.getInt(Constants.PANTALLA_BASE_ICONO));

		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(suiteApp.getBmovilApplication()
				.getBmovilViewsController());
		setDelegate(parentViewsController
				.getBaseDelegateForKey(TransferirDelegate.TRANSFERIR_DELEGATE_ID));
		delegate = (TransferirDelegate) getDelegate();
		if(delegate==null){
			delegate = new TransferirDelegate();
			parentViewsController.addDelegateToHashMap(TransferirDelegate.TRANSFERIR_DELEGATE_ID,delegate);
			setDelegate(delegate);
		}
		delegate.setControladorTransferencias(this);
		delegate.setTipoOperacion(extras
				.getString(Constants.PANTALLA_BASE_TIPO_OPERACION));
		vista = (LinearLayout) findViewById(R.id.opciones_transfer_layout);
		init();
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("transferir", parentManager.estados);
	}

	public void init() {
		cargaListaSeleccionComponent();
	}

	@SuppressWarnings("deprecation")
	public void cargaListaSeleccionComponent() {

		ArrayList<String> titulos = new ArrayList<String>();
		titulos.add(getString(extras.getInt(Constants.PANTALLA_BASE_SUBTITULO)));

		lista = delegate.cargarListasMenu();

		/*
		 * LinearLayout.LayoutParams params = new
		 * LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		 * params.topMargin = getResources().getDimensionPixelOffset(R.dimen.
		 * cuenta_origen_list_border_margin); params.leftMargin =
		 * getResources().
		 * getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
		 * params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.
		 * cuenta_origen_list_side_margin);
		 */
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(vista);

		LinearLayout.LayoutParams params = new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		listaSeleccion = new ListaSeleccionViewController(this, params,
				parentViewsController);
		listaSeleccion.setDelegate(delegate);
		listaSeleccion.setLista(lista);
		listaSeleccion.setNumeroColumnas(1);
		listaSeleccion.setTitle(getString(extras
				.getInt(Constants.PANTALLA_BASE_SUBTITULO)));
		listaSeleccion.setOpcionSeleccionada(-1);
		listaSeleccion.setSeleccionable(false);
		listaSeleccion.setAlturaFija(false);

		listaSeleccion.cargarTabla();
		vista.addView(listaSeleccion);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		if (delegate != null) {
			delegate.setControladorTransferencias(this);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public void goBack() {
		parentViewsController
				.removeDelegateFromHashMap(TransferirDelegate.TRANSFERIR_DELEGATE_ID);
		super.goBack();
	}

	public void opcionSeleccionada(String opcionSeleccionada) {
		//ARR
				Map<String,Object> inicioOperacionMap = new HashMap<String, Object>();

		Session session = Session.getInstance(SuiteApp.getInstance());
		if (opcionSeleccionada
				.equals(Constants.OPERACION_TRANSFERENCIA_MIS_CUENTAS_TYPE)) {
			if(Server.ALLOW_LOG) Log.d("OpcionesTransferViewController",
					"Entraste a transferencias entre mis cuentas");

			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirPropias,	session.getClientProfile())) {
				//ARR
				inicioOperacionMap.put("evento_inicio", "event45");
				inicioOperacionMap.put("&&products", "operaciones;transferencias+mis cuentas");
				inicioOperacionMap.put("eVar12", "inicio");

				TrackingHelper.trackInicioOperacion(inicioOperacionMap);
				if (Session.getInstance(SuiteApp.appContext).getAccounts().length > Constants.CUENTAS_PROPIAS_LENGTH) {
					((BmovilViewsController) parentViewsController).showTransferirMisCuentas();
				} else {
					this.showInformationAlert(R.string.opcionesTransfer_alerta_texto);
				}
			} else {
				//  NO esta permitido
			}
		} else if (opcionSeleccionada.equals(Constants.INTERNAL_TRANSFER_DEBIT_TYPE)) {
			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirBancomer,	session.getClientProfile())) {
				if (Session.getInstance(SuiteApp.appContext).puedeOperarConTDCEje()) {
					if(Server.ALLOW_LOG) Log.d("OpcionesTransferViewController",	"Entraste a transferencias otras cuentas BBVA");
					//ARR
				inicioOperacionMap.put("evento_inicio", "event45");
				inicioOperacionMap.put("&&products", "operaciones;transferencias+otra cuenta bbva bancomer");
				inicioOperacionMap.put("eVar12", "inicio");

				TrackingHelper.trackInicioOperacion(inicioOperacionMap);;
					((BmovilViewsController) parentViewsController).showTransferViewController(Constants.Operacion.transferirBancomer.value,false, false);
				} else {
					showInformationAlert(R.string.opcionesTransfer_alerta_texto_tdc);
				}
			} else {
				// NO esta permitido
			}
		}/*SPEI else if (opcionSeleccionada
				.equals(Constants.INTERNAL_TRANSFER_EXPRESS_TYPE)) {
			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirBancomer,	session.getClientProfile())) {
				if (Session.getInstance(SuiteApp.appContext).puedeOperarConTDCEje()) {
					if(Server.ALLOW_LOG) Log.d("OpcionesTransferViewController","Entraste a transferencias cuenta express");
					((BmovilViewsController) parentViewsController).showTransferViewController(Constants.Operacion.transferirBancomer.value,true, false);
				} else {
					showInformationAlert(R.string.opcionesTransfer_alerta_texto_tdc);
				}
			} else {
				//  NO esta permitido
			}
		}Termina SPEI*/ 
		else if (opcionSeleccionada.equals(Constants.EXTERNAL_TRANSFER_TYPE)) {
			if(Server.ALLOW_LOG) Log.d("OpcionesTransferViewController","Entraste a transferencias otros bancos Indicator");
			//ARR
			inicioOperacionMap.put("evento_inicio", "event45");
			inicioOperacionMap.put("&&products", "operaciones;transferencias+otros bancos");
			inicioOperacionMap.put("eVar12", "inicio");

			TrackingHelper.trackInicioOperacion(inicioOperacionMap);
			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirInterbancaria,	session.getClientProfile())) {
				if (delegate.isOpenHours()) {
					if(Server.ALLOW_LOG) Log.e("Entra", "Entra");
					((BmovilViewsController) parentViewsController).showTransferViewController(Constants.Operacion.transferirInterbancaria.value,false, false);
				} else {
					if(Session.getListA().size()<=0 && Session.getInstance(SuiteApp.getInstance()).isWeekend())
						this.showInformationAlert(Session.getInstance(SuiteApp.getInstance()).notAssociatedMessage());
					else
						this.showInformationAlert(delegate.textoMensajeAlerta());
				}
			} else {
				// NO esta permitido
			}
		} else if (opcionSeleccionada
				.equals(Constants.OPERACION_EFECTIVO_MOVIL_TYPE)) {

			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.dineroMovil,session.getClientProfile())) {
				if(Server.ALLOW_LOG) Log.d("OpcionesTransferViewController",	"Entraste a dinero móvil");
				//ARR
				inicioOperacionMap.put("evento_inicio", "event45");
				inicioOperacionMap.put("&&products", "operaciones;transferencias+dinero movil");
				inicioOperacionMap.put("eVar12", "inicio");

				TrackingHelper.trackInicioOperacion(inicioOperacionMap);
				if (Session.getInstance(SuiteApp.appContext).puedeOperarConTDCEje()) {
					((BmovilViewsController) parentViewsController).showTransferViewController(Constants.Operacion.dineroMovil.value,false, false);
				} else {
					showInformationAlert(R.string.opcionesTransfer_alerta_texto_tdc);
				}
			}else {
				// EA#9 Caso de Uso Transferir Dinero Movil
//				DineroMovilDelegate dineroMovilDelegate = new DineroMovilDelegate();
//				dineroMovilDelegate.comprobarRecortado();
				delegate.opcionRetiroSinTarjeta=false;
				delegate.comprobarRecortado();
			}
		}
		/*else if (opcionSeleccionada.equals(Constants.OPERACION_SPEI_MOVIL)) {
			
			if (Session.getInstance(SuiteApp.appContext).puedeOperarConTDCEje()) {
				if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Entraste a SPEI mÛvil");
				((BmovilViewsController)parentViewsController).showOtrosBBVASpeiViewController(Constants.Operacion.transferirBancomer, false, false);
			}
			
		}Termina SPEI*/
		else if (opcionSeleccionada
                .equals(Constants.OPERACION_RETIRO_SINTARJETA)) {

			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.retiroSinTarjeta,session.getClientProfile())) {
				if(Server.ALLOW_LOG) Log.d("OpcionesTransferViewController","Entraste a retiro sin tarjeta");
                //ARR
                inicioOperacionMap.put("evento_inicio", "event45");
                inicioOperacionMap.put("&&products", "operaciones;transferencias+dinero movil");
                inicioOperacionMap.put("eVar12", "inicio");

                TrackingHelper.trackInicioOperacion(inicioOperacionMap);
                if (existenCuentasParaRetiro()) {
                        //((BmovilViewsController) parentViewsController).showTransferViewController(Constants.Operacion.retiroSinTarjeta.value,false, false);
                        ((BmovilViewsController)parentViewsController).showRetiroSinTarjetaViewController();
                        
                } else {
                        showInformationAlert(R.string.opcionesTransfer_alerta_texto_tdc);
                }
        	}else {
                // EA#9 Caso de Uso Transferir Dinero Movil
//                 DineroMovilDelegate dineroMovilDelegate = new DineroMovilDelegate();
//                 dineroMovilDelegate.comprobarRecortado();
                delegate.opcionRetiroSinTarjeta=true;
                delegate.comprobarRecortado();
        	}
		}


		
		else {
			if(Server.ALLOW_LOG) Log.d("OpcionesTransferViewController",
					"Opciones a validar para los menus de comprar y pagar");
		}
	}
	
	private boolean existenCuentasParaRetiro() {
        Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
        
        Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
        Boolean existenCuentas=true;
        
         Account cuentaEje = Tools.obtenerCuentaEje();
        
        
         //si el usuario es basico y su cuenta eje es TDC o no es basico pero solo tiene una cuenta y es TDC
        if ((((profile == Constants.Perfil.basico))|| (accounts.length == 1) ) && (cuentaEje.getType().equals(Constants.CREDIT_TYPE))){ //cuentaEjeTDC) ){

                existenCuentas=false;
        }
        

        return existenCuentas;
	}
	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}

}
