 package suitebancomer.aplicaciones.bmovil.classes.common;

 import android.content.Context;
 import android.util.Log;

 import com.bancomer.mbanking.SuiteApp;

 import java.io.File;
 import java.io.FileNotFoundException;
 import java.io.IOException;
 import java.io.InputStream;
 import java.io.OutputStream;
 import java.util.Properties;

 import bancomer.api.common.commons.Constants;
 import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class DatosBmovilFileManager {

	/**
	 * Nombre del archivo de propiedades.
	 */
	private static final String CONFIGURATION_FILE_NAME = "datosBmovil.prop";

	private static final String ACTIVATED_RMS = "activadoBmovil"; // 1

	private static final String PENDING_STATUS_RMS = "pendientedeDescarga"; // 8

	private static final String LOGIN_RMS = "userName";// 2

	private static final String SEED_RMS = "semilla";// 5

	private static final String SOFTTOKEN_RMS = "activadoSoftoken";// 11
	private static final String VIA_RMS = "via";// 6

	private static final String TOKEN_RMS = "token";// 7

	private static final String CATALOG1_VERSION_RMS_S = "catalogo1S";// 300

	private static final String CATALOG4_VERSION_RMS_S = "catalogo4S";// 301

	private static final String CATALOG5_VERSION_RMS_S = "catalogo5S";// 302

	private static final String CATALOG8_VERSION_RMS_S = "catalogo8S";// 303

	private static final String TA_CATALOG_VERSION_RMS_S = "catalogoTAS";// 304

	private static final String DM_CATALOG_VERSION_RMS_S = "catalogoDMS";// 305

	private static final String SERVICES_CATALOG_VERSION_RMS_S = "catalogoServS";// 306
	
	private static final String MANTENIMIENTO_SPEI_VERSION_RMS_S = "mantenimientoSPEI"; // 307

	/**
	 * Propiedades de la aplicacion.
	 */
	private Properties properties;

	/**
	 * La instancia de la clase.
	 */
	private static DatosBmovilFileManager manager = null;

	/**
	 * @return La instancia de la clase.
	 */
	public static DatosBmovilFileManager getCurrent() {
		if (null == manager)
			manager = new DatosBmovilFileManager();
		return manager;
	}

	/**
	 * Inicializa el administrador de propiedades.
	 */
	private DatosBmovilFileManager() {
		properties = null;
		File file = new File(SuiteApp.appContext.getFilesDir(),
				CONFIGURATION_FILE_NAME);

		if (!file.exists())
			initPropertiesFile(file);
		else
			loadPropertiesFile();
	}

	/**
	 * Inicializa el archivo de propiedades con los valores iniciales para cada
	 * propiedad.
	 * 
	 * @param file
	 */
	private void initPropertiesFile(File file) {
		try {
			file.createNewFile();
		} catch (IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
				"Error al crear el archivo de DatosBmovil.", ioEx);
			return;
		}

		loadPropertiesFile();

		if (null != properties) {
			setPropertynValue(ACTIVATED_RMS, "");
			setPropertynValue(PENDING_STATUS_RMS, "0");
			setPropertynValue(LOGIN_RMS, "");
			setPropertynValue(SEED_RMS, "0");
			setPropertynValue(SOFTTOKEN_RMS, "");
			setPropertynValue(VIA_RMS, "");
			setPropertynValue(TOKEN_RMS, "");
			setPropertynValue(CATALOG1_VERSION_RMS_S, "0");
			setPropertynValue(CATALOG4_VERSION_RMS_S, "0");
			setPropertynValue(CATALOG5_VERSION_RMS_S, "0");
			setPropertynValue(CATALOG8_VERSION_RMS_S, "0");
			setPropertynValue(TA_CATALOG_VERSION_RMS_S, "0");
			setPropertynValue(DM_CATALOG_VERSION_RMS_S, "0");
			setPropertynValue(MANTENIMIENTO_SPEI_VERSION_RMS_S, "0");// se agrego spei prueba
			setPropertynValue(SERVICES_CATALOG_VERSION_RMS_S, "0");
		}
	}
	
	public void borrarDatos(){
		//Para este tipo de archivo no seria necesario borrar debido a que
		//En la bd tenia un id de una secuencia y aqui tenemos claves fijas por lo cual
		//podemos acceder al atributo directamente y cambiarle el valor
		//este cambio lo hemos realizado porque al cerrar session se ejecuta esta funcion
		//y al dar doble click sobre el boton atras crea un conflicto sobre el fichero
		//que hace que no se almacene datosBmovil al cerrar la session mientras se cierra la app
	}

	/**
	 * Carga el archivo de propiedades.
	 */
	private void loadPropertiesFile() {
		InputStream input;
		try {
			input = SuiteApp.appContext.openFileInput(CONFIGURATION_FILE_NAME);
		} catch (FileNotFoundException fnfEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
					"Error al cargar el archivo de DatosBmovil para lectura.",
					fnfEx);
			return;
		}

		properties = new Properties();
		try {
			properties.load(input);
		} catch (IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
				"Error al cargas las propiedades.", ioEx);
			properties = null;
			return;
		}
	}

	private void setPropertynValue(String propertyName, String value) {
		if (null == properties || Tools.isEmptyOrNull(propertyName))
			return;
		if (null == value)
			value = "";

		properties.setProperty(propertyName, value);
		storeFileForProperty(propertyName, value);
	}

	private void setPropertynValue(String propertyName, boolean value) {
		// if(null == properties || Tools.isEmptyOrNull(propertyName))
		// return;
		//
		// String propertyValue = String.valueOf(value);
		//
		// properties.setProperty(propertyName, propertyValue);
		// storeFileForProperty(propertyName, propertyValue);
		String propertyValue = String.valueOf(value);
		setPropertynValue(propertyName, propertyValue);
	}

	private String getPropertynValue(String propertyName) {
		if (null == properties)
			return null;

		String value = properties.getProperty(propertyName);
		return value;
	}

	public void setCatalogoServS(String value) {
		setPropertynValue(SERVICES_CATALOG_VERSION_RMS_S, value);
	}

	public String getCatalogoServS() {
		return getPropertynValue(SERVICES_CATALOG_VERSION_RMS_S);
	}
	
	public void setCatalogoMantenimientoSPEIS(String value) {
		setPropertynValue(MANTENIMIENTO_SPEI_VERSION_RMS_S, value);
	}
	
	public String getCatalogoMantenimientoSPEIS() {
		return getPropertynValue(MANTENIMIENTO_SPEI_VERSION_RMS_S);
	}

	public void setCatalogoDMS(String value) {
		setPropertynValue(DM_CATALOG_VERSION_RMS_S, value);
	}

	public String getCatalogoDMS() {
		return getPropertynValue(DM_CATALOG_VERSION_RMS_S);
	}

	public void setCatalogoTAS(String value) {
		setPropertynValue(TA_CATALOG_VERSION_RMS_S, value);
	}

	public String getCatalogoTAS() {

		return getPropertynValue(TA_CATALOG_VERSION_RMS_S);

	}

	public String getCatalogo8S() {

		return getPropertynValue(CATALOG8_VERSION_RMS_S);
		

	}

	public void setCatalogo8S(String value) {
		setPropertynValue(CATALOG8_VERSION_RMS_S, value);
	}

	public String getCatalogo5S() {

		return getPropertynValue(CATALOG5_VERSION_RMS_S);
	}

	public void setCatalogo5S(String value) {
		setPropertynValue(CATALOG5_VERSION_RMS_S, value);
	}

	public String getCatalogo4S() {

		return getPropertynValue(CATALOG4_VERSION_RMS_S);
	}

	public void setCatalogo4S(String value) {
		setPropertynValue(CATALOG4_VERSION_RMS_S, value);
	}

	public String getCatalogo1S() {

		return getPropertynValue(CATALOG1_VERSION_RMS_S);
	}

	public void setCatalogo1S(String value) {
		setPropertynValue(CATALOG1_VERSION_RMS_S, value);
	}

	public String getToken() {

		return getPropertynValue(TOKEN_RMS);
	}

	public void setToken(String value) {
		setPropertynValue(TOKEN_RMS, value);
	}

	public String getVia() {

		return getPropertynValue(VIA_RMS);
	}

	public void setVia(String value) {
		setPropertynValue(VIA_RMS, value);
	}

	public boolean getSoftoken() {
		boolean result;

		String value = getPropertynValue(SOFTTOKEN_RMS);
		result = Boolean.parseBoolean(value);

		return result;
	}

	public void setSoftoken(boolean value) {
		setPropertynValue(SOFTTOKEN_RMS, value);
	}

	public long getSeed() {
		long result;

		String value = getPropertynValue(SEED_RMS);
		if(value.equals(Constants.EMPTY_STRING)){
			result=0;
		}else{
			result = Long.parseLong(value);
		}
		return result;
	}

	public String getSeedStr() {
//		String value = getPropertynValue(SEED_RMS);
		return getPropertynValue(SEED_RMS);
	}
	
	public void setSeed(String value) {
		setPropertynValue(SEED_RMS, value);
	}

	public boolean getActivado() {
		boolean result;

		String value = getPropertynValue(ACTIVATED_RMS);
		result = Boolean.parseBoolean(value);

		return result;
	}

	public void setActivado(boolean value) {
		setPropertynValue(ACTIVATED_RMS, value);
	}

	public void setPendienteDeDescarga(String value) {
		setPropertynValue(PENDING_STATUS_RMS, value);
	}

	public int getPendienteDeDescarga() {
		int result;

		String value = getPropertynValue(PENDING_STATUS_RMS);
		
			result = Integer.parseInt(value);
		
		return result;
	}

	public void setLogin(String value) {
		setPropertynValue(LOGIN_RMS, value);
	}

	public String getLogin() {
		return getPropertynValue(LOGIN_RMS);
	}

	private boolean storeFileForProperty(String propertyName, String propertyValue) {
		if (Tools.isEmptyOrNull(propertyName))
			propertyName = "No name indicated.";
		if (Tools.isEmptyOrNull(propertyValue))
			propertyName = "No value indicated.";

		OutputStream output;
		try {
			output = SuiteApp.appContext.openFileOutput(CONFIGURATION_FILE_NAME, Context.MODE_PRIVATE);
		} catch (FileNotFoundException fnfEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),"Error al abrir el archivo para guardar la propiedad "
							+ propertyName, fnfEx);
			return false;
		}

		try {
			properties.store(output, null);
		} catch (IOException ioEx) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al guardar en el archivo de propiedades los valores: "
						+ propertyName + " - " + propertyValue, ioEx);
			return false;
		}

		if(Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Archivo actualizado con los siguientes cambios: "
						+ propertyName + " - " + propertyValue);
		return true;
	}

	public static void reloadFile(){
		manager=null;
	}
}
