package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuAdministrarDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagoTdcDelegate;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.controllers.BaseViewsController;
import suitebancomer.classes.gui.delegates.MenuSuiteDelegate;
import suitebancomer.classes.gui.views.AmountField;
import tracking.TrackingHelper;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.R.id;
import com.bancomer.mbanking.R.layout;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

public class NovedadesViewController extends BaseViewController {
	
	private ScrollView vista;
	private LinearLayout novedades1;
	private LinearLayout novedades2;
	private LinearLayout novedades3;
	private LinearLayout novedades4;
	private LinearLayout novedades5;
	private LinearLayout novedades6;
	private LinearLayout novedades7;
	private LinearLayout novedades8;
	private LinearLayout novedades9;
	private LinearLayout novedades10;
	private LinearLayout novedades11;
	private LinearLayout novedades12;
	
	private TextView parrafo1;
	private TextView parrafo2;
	private TextView parrafo3;
	private TextView parrafo4;
	private TextView parrafo5;
	private TextView parrafo6;
	private TextView parrafo7;
	private TextView parrafo8;
	private TextView parrafo9;
	private TextView parrafo10;
	private TextView parrafo11;
	private TextView parrafo12;
	
	/**
	 * 
	 */
	private BmovilViewsController parentManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE,R.layout.activity_novedades_view_controller);
		setTitle(R.string.novedades_novedades_title, R.drawable.an_ic_aviso);

		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		parentManager.setCurrentActivityApp(this);
		TrackingHelper.trackState("novedades", parentManager.estados);
		
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());

		if(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getLastDelegateKey() != MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID){
			MenuSuiteDelegate delegate = (MenuSuiteDelegate)SuiteApp.getInstance().getSuiteViewsController().getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
			if (delegate == null) {
				delegate = new MenuSuiteDelegate();
			}
			setDelegate(delegate);
		}else{
			setDelegate(parentViewsController.getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID));
			
		}
		
		init();
	}
	
	
	public void init(){
		findViews();
		scaleForCurrentScreen();	
	}
	
	
	private void findViews() {
		vista = (ScrollView)findViewById(R.id.novelties_scroll);
		novedades1 = (LinearLayout)findViewById(R.id.novelties_1_layout);
		novedades2 = (LinearLayout)findViewById(R.id.novelties_2_layout);
		novedades3 = (LinearLayout)findViewById(R.id.novelties_3_layout);
//		novedades4 = (LinearLayout)findViewById(R.id.novelties_4_layout);
//		novedades5 = (LinearLayout)findViewById(R.id.novelties_5_layout);
//		novedades6 = (LinearLayout)findViewById(R.id.novelties_6_layout);
//		novedades7 = (LinearLayout)findViewById(R.id.novelties_7_layout);
//		novedades8 = (LinearLayout)findViewById(R.id.novelties_8_layout);
//		novedades9 = (LinearLayout)findViewById(R.id.novelties_9_layout);
//		novedades10 = (LinearLayout)findViewById(R.id.novelties_10_layout);
//		novedades11 = (LinearLayout)findViewById(R.id.novelties_11_layout);
//		novedades12 = (LinearLayout)findViewById(R.id.novelties_12_layout);
		
		parrafo1 = (TextView)findViewById(R.id.paragraph_1_label);
		parrafo2 = (TextView)findViewById(R.id.paragraph_2_label);
		parrafo3 = (TextView)findViewById(R.id.paragraph_3_label);
//		parrafo4 = (TextView)findViewById(R.id.paragraph_4_label);
//		parrafo5 = (TextView)findViewById(R.id.paragraph_5_label);
//		parrafo6 = (TextView)findViewById(R.id.paragraph_6_label);
//		parrafo7 = (TextView)findViewById(R.id.paragraph_7_label);
//		parrafo8 = (TextView)findViewById(R.id.paragraph_8_label);
//		parrafo9 = (TextView)findViewById(R.id.paragraph_9_label);
//		parrafo10 = (TextView)findViewById(R.id.paragraph_10_label);
//		parrafo11 = (TextView)findViewById(R.id.paragraph_11_label);
//		parrafo12 = (TextView)findViewById(R.id.paragraph_12_label);
	}
	
	
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.novelties_layout));
		
		guiTools.scale(findViewById(R.id.novelties_1_layout));
		guiTools.scale(findViewById(R.id.paragraph_1_label), true);
		
		guiTools.scale(findViewById(R.id.novelties_2_layout));
		guiTools.scale(findViewById(R.id.paragraph_2_label), true);

		guiTools.scale(findViewById(R.id.novelties_3_layout));
		guiTools.scale(findViewById(R.id.paragraph_3_label), true);
		
//		guiTools.scale(findViewById(R.id.novelties_4_layout));
//		guiTools.scale(findViewById(R.id.paragraph_4_label), true);

//		guiTools.scale(findViewById(R.id.novelties_5_layout));
//		guiTools.scale(findViewById(R.id.paragraph_5_label), true);
//
//		guiTools.scale(findViewById(R.id.novelties_6_layout));
//		guiTools.scale(findViewById(R.id.paragraph_6_label), true);
//
//		guiTools.scale(findViewById(R.id.novelties_7_layout));
//		guiTools.scale(findViewById(R.id.paragraph_7_label), true);
//
//		guiTools.scale(findViewById(R.id.novelties_8_layout));
//		guiTools.scale(findViewById(R.id.paragraph_8_label), true);
//
//		guiTools.scale(findViewById(R.id.novelties_9_layout));
//		guiTools.scale(findViewById(R.id.paragraph_9_label), true);
//
//		guiTools.scale(findViewById(R.id.novelties_10_layout));
//		guiTools.scale(findViewById(R.id.paragraph_10_label), true);
//
//		guiTools.scale(findViewById(R.id.novelties_11_layout));
//		guiTools.scale(findViewById(R.id.paragraph_11_label), true);
//
//		guiTools.scale(findViewById(R.id.novelties_12_layout));
//		guiTools.scale(findViewById(R.id.paragraph_12_label), true);
	}

	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getLastDelegateKey() != MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID){

		    if (keyCode == KeyEvent.KEYCODE_BACK ) {
		    	
		    	MenuSuiteDelegate delegate = (MenuSuiteDelegate)SuiteApp.getInstance().getSuiteViewsController().getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
				if (delegate == null) {
					delegate = new MenuSuiteDelegate();
					parentViewsController.addDelegateToHashMap(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID, delegate);
				}
				delegate.getMenuSuiteViewController().setIsFlipPerforming(false);
				delegate.getMenuSuiteViewController().setComesFromNov(true);
				
		    }
		    
		}
	    return super.onKeyDown(keyCode, event);
	}*/
	
	@Override
	public void goBack() {
		
		if(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getLastDelegateKey() != MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID){
			MenuSuiteDelegate delegate = (MenuSuiteDelegate)SuiteApp.getInstance().getSuiteViewsController().getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
			if (delegate == null) {
				delegate = new MenuSuiteDelegate();
				parentViewsController.addDelegateToHashMap(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID, delegate);
			}
		}else{
			parentViewsController.removeDelegateFromHashMap(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
			
		}
		super.goBack();
	}
	
	@Override
	protected void onResume() {
		super.onResume();		
		getParentViewsController().setCurrentActivityApp(this);

	}

}
