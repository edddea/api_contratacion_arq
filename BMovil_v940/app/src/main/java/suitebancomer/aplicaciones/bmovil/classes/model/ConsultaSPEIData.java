package suitebancomer.aplicaciones.bmovil.classes.model;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.consultaotroscreditos.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by Nicolas Arriaza on 31/07/2015.
 */
public class ConsultaSPEIData implements ParsingHandler {

    private String estado;

    private String periodoConsulta;

    private ArrayList<TransferenciaSPEI> transferencias;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPeriodoConsulta() {
        return periodoConsulta;
    }

    public void setPeriodoConsulta(String periodoConsulta) {
        this.periodoConsulta = periodoConsulta;
    }

    public ArrayList<TransferenciaSPEI> getTransferencias() {
        return transferencias;
    }

    public void setTransferencias(ArrayList<TransferenciaSPEI> transferencias) {
        this.transferencias = transferencias;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    private void parseTransferencias(ParserJSON parser) throws IOException, ParsingException {

        transferencias = new ArrayList<TransferenciaSPEI>();

        try{

            JSONArray jsonArray = parser.parseNextValueWithArray("transferencias",true);

            for(int i = 0; i<jsonArray.length(); ++i){
                JSONObject transferObj = jsonArray.getJSONObject(i);
                TransferenciaSPEI transfer = new TransferenciaSPEI();

                transfer.setBancoOrdenante(transferObj.getString("bancoOrdenante"));
                transfer.setCuentaOrdenante(transferObj.getString("cuentaOrdenante"));
                transfer.setNombreOrdenante(transferObj.getString("nombreOrdenante"));
                transfer.setFechaCalendario(transferObj.getString("fechaCalendario"));
                transfer.setMontodelPago(transferObj.getString("montoDelPago"));
                transfer.setBancoBeneficiario(transferObj.getString("bancoBeneficiario"));
                transfer.setCuentaBeneficiaria(transferObj.getString("cuentaBeneficiaria"));
                transfer.setNombreDelBeneficiario(transferObj.getString("nombreDelBeneficiario"));
                transfer.setConceptoDelPago(transferObj.getString("conceptoDelPago"));
                transfer.setNumeroDeReferencia(transferObj.getString("numeroDeReferencia"));
                transfer.setClaveDeRastreo(transferObj.getString("claveDeRastreo"));

                transferencias.add(transfer);
            }

        }catch(Exception e){
            if( Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
        }

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        periodoConsulta = parser.parseNextValue("periodoConsulta",true);
        parseTransferencias(parser);
    }
}
