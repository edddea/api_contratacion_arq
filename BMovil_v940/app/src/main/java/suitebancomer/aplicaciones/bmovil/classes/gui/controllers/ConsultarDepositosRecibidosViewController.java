package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DepositosRecibidosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MovimientosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import tracking.TrackingHelper;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class ConsultarDepositosRecibidosViewController extends BaseViewController {
	// AMZ
	public BmovilViewsController parentManager;
	// AMZ
	/**
	 * Layout base, contenedor principal dela vista.
	 */
	private LinearLayout rootLayout;
	
	private LinearLayout cuentaOrigenLayout;

	CuentaOrigenViewController componenteCtaOrigen;
	
	/**
	 * Etiqueta de t�tulo para el filtro se movimientos.
	 */
	private TextView lblSelecciona;

	/**
	 * Combobox de selecci�n de movimientos.
	 */
	private TextView comboFiltro;

	/**
	 * Layout para el componente que mostrara la lista de movimientos.
	 */
	private LinearLayout listaEnviosLayout;

	/**
	 * Componente de lista de movimientos de depositos recibidos.
	 */
	private ListaSeleccionViewController listaEnvios;

	/**
	 * Delegado de la vista.
	 */
	private DepositosRecibidosDelegate delegate;

	
	
	TextView verMasMovimientos;
	
	private boolean optionSelected = false;
	
	
	public CuentaOrigenViewController getComponenteCtaOrigen() {
		return componenteCtaOrigen;
	}
	/*
	 * No javadoc.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_consulta_depositos_recibidos);
		setTitle(R.string.bmovil_consultar_depositosrecibidos_titulo, R.drawable.bmovil_consultar_icono);
		// AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();

		TrackingHelper.trackState("depositos", parentManager.estados);

		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID));
		delegate = (DepositosRecibidosDelegate) getDelegate();
		delegate.setViewController(this);

		init();
		
	}

	/**
	 * Inicializa el controlador de la ventana.
	 */
	private void init() {

		findViews();
		showCuentaOrigen();
		cargaListaDatos();
		
		scaleForCurrentScreen();
	}
	
	public void showCuentaOrigen(){
		ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.bmovil_consultar_depositosrecibidos_subtitulo));
		componenteCtaOrigen.setDelegate(delegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(delegate.getCuentaActual()));
		componenteCtaOrigen.init();
		cuentaOrigenLayout.addView(componenteCtaOrigen);
	}

	/**
	 * Busca las vistas usadas y guarda sus referencias.
	 */
	private void findViews() {
		rootLayout = (LinearLayout) findViewById(R.id.rootLayout);
		lblSelecciona = (TextView) findViewById(R.id.lblSelecciona);
		comboFiltro = (TextView) findViewById(R.id.comboboxFiltroEstado);
		listaEnviosLayout = (LinearLayout) findViewById(R.id.layoutListaEnvios);
		cuentaOrigenLayout = (LinearLayout) findViewById(R.id.layoutCuentaOrigen );
	}

	/**
	 * Redimensiona las vistas para que mantegan la relacion de aspecto en
	 * cualquier pantalla.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		guiTools.scalePaddings(rootLayout);
		guiTools.scale(cuentaOrigenLayout);
		guiTools.scale(lblSelecciona, true);
		guiTools.scale(comboFiltro, true);
		guiTools.scale(listaEnviosLayout);
	}

	/**
	 * Carga la lista movimientos de Depositos Recibidos.
	 */
	private void cargaListaDatos() {
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		
		ArrayList<Object> encabezados = delegate.getHeaderListaMovimientos();
		ArrayList<Object> datos = new ArrayList<Object>();

		listaEnvios = new ListaSeleccionViewController(this, params,
				parentViewsController);
		listaEnvios.setDelegate(delegate);
		listaEnvios.setEncabezado(encabezados);

		listaEnvios.setNumeroColumnas(2);
		listaEnvios.setTitle(getString(R.string.bmovil_consultar_dineromovil_movimientos_lista_titulo));
		listaEnvios.setOpcionSeleccionada(-1);
		listaEnvios.setSeleccionable(false);
		listaEnvios.setAlturaFija(true);
		listaEnvios.setNumeroFilas(datos.size());
		listaEnvios.setSingleLine(true);

		listaEnvios.setLista(datos);
		listaEnvios.setTextoAMostrar(" ");
		listaEnvios.cargarTabla();
		//delegate.setTipoOperacion(Server.CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO);
		
		//EA#8 Si viniera de Mis cuentas quiero deberia ser distinto de nulo
		Account cuenta = delegate.getCuentaActual();
		
		if (cuenta == null) {
			cuenta = componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada();
//		} else {
//			componenteCtaOrigen.getCuentaOrigenDelegate().performAction(cuenta);
		}
		delegate.setCuentaActual(cuenta);
//		delegate.consultarMovimientosDR();
		
		listaEnvios.setVisibility(View.GONE);
		listaEnviosLayout.addView(listaEnvios);
	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		this.delegate.analyzeResponse(operationId, response);
	}


	public void onTipoOperacionSeleccionado(Constants.TipoOperacionDR tipo) {
		String tipoOperacion = "";
		int operacion = -1;//0;
		switch (tipo) {
		
		case DEPOBBVABANCOMERYEFECTIVO:
			tipoOperacion = getString(R.string.bmovil_consultar_depositosrecibidos_movimientos_tipooperacion_depobbvaefectivo);
			operacion= Server.CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO;
			break;
		
		case DEPOCONCHEQUES:
			tipoOperacion = getString(R.string.bmovil_consultar_depositosrecibidos_movimientos_tipooperacion_depocheques);
			operacion= Server.CONSULTA_DEPOSITOS_CHEQUES;
			break;
			
		case TRANSDEOTROSBANCOS:
			tipoOperacion = getString(R.string.bmovil_consultar_depositosrecibidos_movimientos_tipooperacion_transdeotrosbancos);
			operacion= Server.CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS;
			break;

		default:
			tipoOperacion = "";
			break;
		}
		/*comboFiltro.setText(tipoOperacion);
		delegate.setPeriodoActual(0);
		delegate.setTipoOperacion(operacion);
		delegate.setMovimientosDR(null);
		delegate.consultarMovimientosDR();*/
		if(operacion != -1 || tipoOperacion.equals("")){
			comboFiltro.setText(tipoOperacion);
			delegate.setPeriodoActual(0);
			delegate.setTipoOperacion(operacion);
			delegate.setMovimientosDR(null);
			muestraVerMasMovimientos(false);  /*******/
			delegate.actualizarMovimientosDR(); /***/
			listaEnvios.setOpcionSeleccionada(-1); /***RHO***/
			delegate.consultarMovimientosDR();
		}
	}

	public void onTipoOperacionClick(View view) {
		delegate.setViewController(this);

		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		final String[] importes = new String[] {
				getString(R.string.bmovil_consultar_depositosrecibidos_movimientos_tipooperacion_depobbvaefectivo),
				getString(R.string.bmovil_consultar_depositosrecibidos_movimientos_tipooperacion_depocheques),
				getString(R.string.bmovil_consultar_depositosrecibidos_movimientos_tipooperacion_transdeotrosbancos)};

		builder.setTitle(
				R.string.bmovil_consultar_dineromovil_movimientos_tipo_titulo)
				.setItems(importes, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						if (Constants.TipoOperacionDR.DEPOBBVABANCOMERYEFECTIVO.value == which) {
							onTipoOperacionSeleccionado(Constants.TipoOperacionDR.DEPOBBVABANCOMERYEFECTIVO);
							optionSelected = true;
						} else if (Constants.TipoOperacionDR.TRANSDEOTROSBANCOS.value == which) {
							onTipoOperacionSeleccionado(Constants.TipoOperacionDR.TRANSDEOTROSBANCOS);
							optionSelected = true;
						} else if (Constants.TipoOperacionDR.DEPOCONCHEQUES.value == which) {
							onTipoOperacionSeleccionado(Constants.TipoOperacionDR.DEPOCONCHEQUES);
							optionSelected = true;
						}

					}
				}).show();
	}

	/**
	 * @return the listaEnvios
	 */
	public ListaSeleccionViewController getListaEnvios() {
		return listaEnvios;
	}

	@Override
	protected void onResume() {
		super.onResume();

		
		if (null != listaEnvios) {
			setDelegate(parentViewsController
					.getBaseDelegateForKey(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID));
			delegate = (DepositosRecibidosDelegate) getDelegate();
			delegate.setViewController(this);
			listaEnvios.setDelegate(this.delegate);
		}

		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if (MotionEvent.ACTION_DOWN == ev.getAction()) {
			// if(Server.ALLOW_LOG) Log.d(this.getClass().getName(),
			// "Touch Event Action: ACTION_DOWN");
			listaEnvios.setMarqueeEnabled(false);
		} else if (MotionEvent.ACTION_UP == ev.getAction()) {
			// if(Server.ALLOW_LOG) Log.d(this.getClass().getName(),
			// "Touch Event Action: ACTION_UP");
			listaEnvios.setMarqueeEnabled(true);
		}

		return super.dispatchTouchEvent(ev);
	}
	public void muestraVerMasMovimientos(boolean visible){
		if(visible && null == this.verMasMovimientos){
			this.verMasMovimientos = (TextView) getLayoutInflater().inflate(R.layout.layout_bmovil_ver_mas_movimientos, null);
			
			listaEnviosLayout.addView(verMasMovimientos);
			GuiTools guiTools = GuiTools.getCurrent();
			guiTools.init(getWindowManager());
			guiTools.scale(verMasMovimientos, true);
			verMasMovimientos.setPadding(0, 0, 0, 10);
		}
		
		if(null != verMasMovimientos){
			verMasMovimientos.setVisibility(visible ? View.VISIBLE : View.GONE);
		}
	}
	public void onLinkVerMasMovimientos(View sender) {
		delegate.verMasMovimientos();
	}
	
	//Modificación 20150324
	public TextView getComboFiltro() {
		return comboFiltro;
	}
	
	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID);
		super.goBack();
	}

}
