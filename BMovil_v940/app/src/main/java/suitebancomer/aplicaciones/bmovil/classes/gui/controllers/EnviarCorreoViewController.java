package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.EnviarCorreoDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ResultadosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import tracking.TrackingHelper;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EnviarCorreoViewController extends BaseViewController {
	//AMZ
		private BmovilViewsController parentManager;
		
	// #region Variables.
	/**
	 * Delegado asociado con el controlador.
	 */
	private EnviarCorreoDelegate ecDelegate;
	
	/**
	 * CheckBox para seleccionar si se envía un correo al destinatario.
	 */
	private CheckBox cbCorreoBeneficiario;
	
	/**
	 * CheckBox para seleccionar si se copia al usuario en el correo.
	 */
	private CheckBox cbConCopia;
	
	/**
	 * Campo de texto para indicar el correo del destinatario.
	 */
	private EditText tbCorreoBeneficiario;
	
	/**
	 * Campo de texto para indicar el mensaje del correo.
	 */
	private EditText tbMensaje;
	// #endregion
	
	public EnviarCorreoViewController() {
		ecDelegate = null;
	}
	
	// #region Ciclo de vida.
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_enviar_correo);
		setTitle(R.string.bmovil_enviar_correo_titulo, R.drawable.icono_enviar_correo);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		
		TrackingHelper.trackState("correo", parentManager.estados);
		
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID));
		ecDelegate = (EnviarCorreoDelegate)getDelegate(); 
		ecDelegate.setOwnerController(this);
		
		init();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		getParentViewsController().setCurrentActivityApp(this);
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID);
		super.goBack();
	}
	// #endregion
	
	// #region Configurar pantalla.
	/**
	 * Inicializa la pantalla.
	 */
	private void init() {
//frecuente correo electronico
		String email = null;

		findViews();
		scaleForCurrentScreen();
		
		tbMensaje.setText(ecDelegate.getResultadosDelegate().getOperationDelegate().getTextoEmail());
//		tbCorreoBeneficiario.addTextChangedListener(new TextWatcher() {
//			@Override
//			public void onTextChanged(CharSequence s, int start, int before, int count) {}
//
//			@Override
//			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
//
//			@Over ride
//			public void afterTextChanged(Editable s) {
//				onUserInteraction();
//			}
//		});
		//Frecuente correo electronico
		email = ecDelegate.getResultadosDelegate().getOperationDelegate().getCorreoFrecuente();

		if (email != null && !email.isEmpty()) {
			Pattern pat = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
			if (pat.matcher(email).matches()) {
				tbCorreoBeneficiario.setText(email.toLowerCase());
				//tbCorreoBeneficiario.setEnabled(false);
			}
		}
		tbCorreoBeneficiario.addTextChangedListener(new BmovilTextWatcher(this));
		tbMensaje.addTextChangedListener(new BmovilTextWatcher(this));
	}

	/**
	 * Busca la referencia a todas las subvistas necesarias.
	 */
	private void findViews() {
		cbCorreoBeneficiario = (CheckBox)findViewById(R.id.cbBeneficiario);
		cbConCopia = (CheckBox)findViewById(R.id.cbCopia);
		tbCorreoBeneficiario = (EditText)findViewById(R.id.tbCorreoBeneficiario);
		tbMensaje = (EditText)findViewById(R.id.tbMensaje);
	}
	
	/**
	 * Escala las subvistas para la pantalla actual.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.rootLayout));
		guiTools.scale(findViewById(R.id.lblEnviarAlBeneficiario), true);
		guiTools.scale(cbCorreoBeneficiario);
		guiTools.scale(findViewById(R.id.lblCorreoBeneficiario), true);
		guiTools.scale(tbCorreoBeneficiario, true);
		guiTools.scale(findViewById(R.id.lblConCopia), true);
		guiTools.scale(cbConCopia);
		guiTools.scale(findViewById(R.id.lblMensaje), true);
		guiTools.scale(tbMensaje, true);
		guiTools.scale(findViewById(R.id.layoutSend));
		guiTools.scale(findViewById(R.id.btnEnviar));
	}
	// #endregion
	
	// #region Enevtos
	/**
	 * Evento al presionar el boton Enviar.
	 * @param sender Vista que invoca el método.
	 */
	public void onBotonEnviarClick(View sender) {
		ecDelegate.validaDatos(cbCorreoBeneficiario.isChecked(), 
							   cbConCopia.isChecked(), 
							   tbCorreoBeneficiario.getText().toString(), 
							   tbMensaje.getText().toString());
		//AMZ
				if(isHabilitado() == true){
					TrackingHelper.trackState("resul", parentManager.estados);
				}
	}
	
	/**
	 * Habilita o deshabilta el campo de correo del beneficiario segun el estado del CheckBox del beneficiario.
	 * @param sender La vista que invoca al método.
	 */
	public void onCheckboxBeneficiarioClick(View sender) {
		if(sender != cbCorreoBeneficiario)
			return;
		tbCorreoBeneficiario.setEnabled(cbCorreoBeneficiario.isChecked());
	}
	// #endregion

	/* (non-Javadoc)
	 * @see suitebancomer.classes.gui.controllers.BaseViewController#processNetworkResponse(int, suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		ecDelegate.analyzeResponse(operationId, response);
	}
}
