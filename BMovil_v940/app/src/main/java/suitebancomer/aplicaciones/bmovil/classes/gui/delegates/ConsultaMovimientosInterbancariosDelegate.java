package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ErrorData;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultaMovimientosInterbancariosViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaSPEIData;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaSPEI;
import suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Created by Nicolas Arriaza on 04/08/2015.
 */
public class ConsultaMovimientosInterbancariosDelegate extends DelegateBaseAutenticacion {

    /****************************
     * Constantes
     ****************************/
    public static final long CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID = 2280609066072638685L;

    public static final String PERIODO_INICIAL = "0";

    public static final String PERIODO_FINAL = "2";

    /****************************
     * Constructors
     ****************************/
    public ConsultaMovimientosInterbancariosDelegate() {
        periodo = 0;
        listaMovimientos = new HashMap<Account, HashMap<String, ConsultaSPEIData>>();
    }

    /****************************
     * Variables
     ****************************/
    private BaseViewController caller;

    private ConsultaMovimientosInterbancariosViewController viewController;

    private Account cuentaSeleccionada;

    private TransferenciaSPEI transferenciaSeleccionada;

    private ArrayList<Account> listaCuentaOrigen;

    private ConsultaSPEIData consultaSPEIData;

    private Integer periodo;

    private HashMap<Account, HashMap<String, ConsultaSPEIData>> listaMovimientos;

    private String contrasenia = "";

    private String nip = "";

    private String token = "";

    private String campoTarjeta = "";

    private String cvv2 = "";

    private Boolean vengoDeConfirmacion = false;

    private ErrorData errorData;

    /****************************
     * Getters & Setters
     ****************************/

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCampoTarjeta() {
        return campoTarjeta;
    }

    public void setCampoTarjeta(String campoTarjeta) {
        this.campoTarjeta = campoTarjeta;
    }

    public String getCvv2() {
        return cvv2;
    }

    public void setCvv2(String cvv2) {
        this.cvv2 = cvv2;
    }

    public Integer getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Integer periodo) {
        this.periodo = periodo;
    }

    public HashMap<Account, HashMap<String, ConsultaSPEIData>> getListaMovimientos() {
        return listaMovimientos;
    }

    public void setListaMovimientos(HashMap<Account, HashMap<String, ConsultaSPEIData>> listaMovimientos) {
        this.listaMovimientos = listaMovimientos;
    }

    public ConsultaMovimientosInterbancariosViewController getViewController() {
        return viewController;
    }

    public void setViewController(ConsultaMovimientosInterbancariosViewController viewController) {
        this.viewController = viewController;
    }

    public Account getCuentaSeleccionada() {
        return cuentaSeleccionada;
    }

    public void setCuentaSeleccionada(Account cuentaSeleccionada) {
        this.cuentaSeleccionada = cuentaSeleccionada;
    }

    public ArrayList<Account> getListaCuentaOrigen() {
        return listaCuentaOrigen;
    }

    public void setListaCuentaOrigen(ArrayList<Account> listaCuentaOrigen) {
        this.listaCuentaOrigen = listaCuentaOrigen;
    }

    public BaseViewController getCaller() {
        return caller;
    }

    public void setCaller(BaseViewController caller) {
        this.caller = caller;
    }

    public ConsultaSPEIData getConsultaSPEIData() {
        return consultaSPEIData;
    }

    public void setConsultaSPEIData(ConsultaSPEIData consultaSPEIData) {
        this.consultaSPEIData = consultaSPEIData;
    }

    public TransferenciaSPEI getTransferenciaSeleccionada() {
        return transferenciaSeleccionada;
    }

    /**************************** Metodos ****************************/

    /**
     * Get cuentas origen
     *
     * @return
     */
    public void cargaCuentasOrigen() {
        Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
        ArrayList<Account> accountsArray = new ArrayList<Account>();
        Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();

        if (profile == Constants.Perfil.avanzado) {
            for (Account acc : accounts) {
                if (acc.isVisible() && (acc.getType().equalsIgnoreCase("CH") || acc.getType().equalsIgnoreCase("AH") || acc.getType().equalsIgnoreCase("LI"))) {
                        accountsArray.add(acc);
                }
            }
            for(Account acc : accounts) {
                if (!acc.isVisible() && (acc.getType().equalsIgnoreCase("CH") || acc.getType().equalsIgnoreCase("AH") || acc.getType().equalsIgnoreCase("LI"))) {
                    accountsArray.add(acc);
                }
            }
        } else {
            for (Account acc : accounts) {
                if (acc.isVisible())
                    accountsArray.add(acc);
            }
        }

        this.setListaCuentaOrigen(accountsArray);
        if (!accountsArray.isEmpty()) this.setCuentaSeleccionada(accountsArray.get(0));

    }

    public Boolean preCheckCuentasOrigen() {
        Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
        ArrayList<Account> accountsArray = new ArrayList<Account>();
        Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
        Boolean showAlert = false;

        if (profile == Constants.Perfil.avanzado) {
            for (Account acc : accounts) {
                if (acc.isVisible()) {
                    if ((acc.getType().equalsIgnoreCase("CH") || acc.getType().equalsIgnoreCase("AH") || acc.getType().equalsIgnoreCase("LI"))) {
                        accountsArray.add(acc);
                        break;
                    }
                } else if (!acc.isVisible() && (acc.getType().equalsIgnoreCase("CH") || acc.getType().equalsIgnoreCase("AH") || acc.getType().equalsIgnoreCase("LI"))){
                    accountsArray.add(acc);
                    break;
                }
            }

            if(accountsArray.isEmpty()) showAlert = true;
        } else if (profile == Constants.Perfil.basico) {
            for (Account acc : accounts) {
                if (acc.isVisible()){
                    if(!acc.getType().equalsIgnoreCase("CH") && !acc.getType().equalsIgnoreCase("AH") && !acc.getType().equalsIgnoreCase("LI")) {
                        showAlert = true;
                        break;
                    }
                } else if (acc.isVisible())
                    accountsArray.add(acc);
            }
        } else {
            for (Account acc : accounts) {
                if (acc.isVisible())
                    accountsArray.add(acc);
            }
        }
        return showAlert;
    }

    public ArrayList<Object> getDatosHeaderTablaInterbancarios() {
        ArrayList<Object> registros = new ArrayList<Object>();
        registros.add("");
        registros.add(SuiteApp.appContext.getString(R.string.bmovil_consulta_movimientos_interbancarios_movimientos_fecha));
        registros.add(SuiteApp.appContext.getString(R.string.bmovil_consulta_movimientos_interbancarios_movimientos_descripcion));
        registros.add(SuiteApp.appContext.getString(R.string.bmovil_consulta_movimientos_interbancarios_movimientos_importe));
        return registros;
    }

    public ArrayList<String> getLayoutFecha(String fecha) {
        ArrayList<String> lista = new ArrayList<String>();
        String dia = "";
        String mes = "";

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date dateFecha = sdf.parse(fecha);
            dia = (new SimpleDateFormat("dd")).format(dateFecha);
            mes = (new SimpleDateFormat("MMM")).format(dateFecha);

            // formatear cada campo segun se requiere
            // Quitamos el punto final
            mes = mes.substring(0, (mes.length() - 1));
            // Primera letra caps
            mes = mes.substring(0, 1).toUpperCase() + mes.substring(1);
            mes = " " + mes;

        } catch (ParseException e) {
            e.printStackTrace();
        }

        lista.add(dia);
        lista.add(mes);
        return lista;
    }

    public ArrayList<Object> getInterbancarios() {
        ArrayList<Object> listaDatos = new ArrayList<Object>();
        if (listaMovimientos != null) {
            for (TransferenciaSPEI spei : getListaMovimientosData(cuentaSeleccionada)) {
                ArrayList<Object> registro = new ArrayList<Object>();
                registro.add(spei);
                registro.add(getLayoutFecha(Tools.formatDate(spei.getFechaCalendario())));
                registro.add(spei.getBancoBeneficiario());
                Boolean negativo = spei.getMontodelPago().startsWith("-");
                registro.add(Tools.formatAmount(spei.getMontodelPago(), negativo));

                listaDatos.add(registro);
            }
        }
        return listaDatos;
    }

    public Boolean isPeriodoExceded(String periodo) {
        Boolean ret = false;
        if (periodo.equals(PERIODO_FINAL)) {
            viewController.ocultaMasMovimientos();
            ret = true;
        } else {
            viewController.muestraMasMovimientos();
        }

        return ret;
    }

    public void addListaMovimientos(Account acc, String periodo, ConsultaSPEIData data) {
        HashMap<String, ConsultaSPEIData> hash = new HashMap<String, ConsultaSPEIData>();

        if (!listaMovimientos.containsKey(acc)) {
            hash.put(periodo, data);
            listaMovimientos.put(acc, hash);
        } else {
            hash = listaMovimientos.get(acc);

            if (!hash.containsKey(periodo)) {
                hash.put(periodo, data);
            }
        }
    }

    public ArrayList<TransferenciaSPEI> getListaMovimientosData(Account acc) {
        ArrayList<TransferenciaSPEI> ret = new ArrayList<TransferenciaSPEI>();

        if (listaMovimientos.containsKey(acc)) {
            HashMap<String, ConsultaSPEIData> hash = listaMovimientos.get(acc);
            Iterator<String> it = hash.keySet().iterator();
            while (it.hasNext()) {
                ret.addAll(hash.get(it.next()).getTransferencias());
            }
        }

       ordenarTransferencias(ret);

        return ret;
    }

    public void masMovimientosAction() {
        periodo++;
        consultaMovimientosInterbancarios(periodo.toString(), viewController);
    }

    public String getPeriodo(Account acc) {
        String periodo = PERIODO_INICIAL;

        if (listaMovimientos.containsKey(acc)) {
            HashMap<String, ConsultaSPEIData> hash = listaMovimientos.get(acc);
            Iterator<String> it = hash.keySet().iterator();
            while (it.hasNext()) {
                String next = it.next();

                if (next.compareTo(periodo) > 0) {
                    periodo = next;
                }
            }
        }
        return periodo;
    }

    @Override
    public void performAction(Object obj) {
        if (obj instanceof Account) {
            viewController.getComponenteCtaOrigen().isSeleccionado();
            viewController.muestraListaCuentas();
            viewController.getComponenteCtaOrigen().setSeleccionado(false);

            cuentaSeleccionada = (Account) obj;

            periodo = Integer.valueOf(getPeriodo(cuentaSeleccionada));

            consultaMovimientosInterbancarios(periodo.toString(), viewController);
        } else if (obj instanceof TransferenciaSPEI) {
            transferenciaSeleccionada = (TransferenciaSPEI) obj;
            ((BmovilViewsController) viewController.getParentViewsController()).showDetalleConsultaMovsInterbancaria(this);
        }
    }

    @Override
    public String getTextoAyudaResultados() {
        return "Para guardar esta información oprime el botón de opciones";
    }

    /****************************
     * Server
     ****************************/
    public void consultaMovimientosInterbancarios(String periodo, BaseViewController caller) {
        setCaller(caller);

        caller.muestraIndicadorActividad(caller.getString(R.string.alert_operation), caller.getString(R.string.alert_connecting));

        Session session = Session.getInstance(SuiteApp.appContext);

        String cadAut = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultaMovInterbancarios, session.getClientProfile());

        //prepare data
        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        paramTable.put(ServerConstants.PERIODO, periodo);
        paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
        paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        paramTable.put(ServerConstants.CUENTA, cuentaSeleccionada.getFullNumber());
        paramTable.put(ServerConstants.CODIGO_NIP, nip);
        paramTable.put(ServerConstants.CODIGO_CVV2, cvv2);
        paramTable.put(ServerConstants.CVE_ACCESO, contrasenia);
        paramTable.put(ServerConstants.CADENA_AUTENTICACION, cadAut);
        paramTable.put(ServerConstants.TARJETA_5DIG, campoTarjeta);
        paramTable.put(ServerConstants.CODIGO_OTP, token);

        doNetworkOperation(Server.OP_CONSULTA_SPEI, paramTable, true, new ConsultaSPEIData(), Server.isJsonValueCode.NONE, caller);
    }

    @Override
    public void doNetworkOperation(int operationId,	Hashtable<String, ?> params, boolean isJson, ParsingHandler handler, Server.isJsonValueCode isJsonValueCode, BaseViewController caller) {
        ((BmovilViewsController) caller.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode, caller, true);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
            // Se necesita comprobar que no viene de confirmacion ya que este delegate es la base usada para la autenticacion y en caso de que venga de confirmacion
            // debe hacer el show de la ventana en cuestión además de la configuración base de la misma
            ConsultaSPEIData data = (ConsultaSPEIData) response.getResponse();

            if (caller instanceof ConfirmacionAutenticacionViewController) {
                if (Server.ALLOW_LOG)
                    Log.d("ConsultaMofimientosInterbancariosDelegate >>", "analyzeResponse -- Operation Successful -- Caller: ConfirmationViewController");
                ConsultaMovimientosInterbancariosDelegate delegate = (ConsultaMovimientosInterbancariosDelegate) SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getBaseDelegateForKey(CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID);
                if (null == delegate) {
                    delegate = new ConsultaMovimientosInterbancariosDelegate();
                    SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().addDelegateToHashMap(CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID,
                            delegate);
                }

                delegate.setConsultaSPEIData(data);

                ordenarTransferencias(getConsultaSPEIData().getTransferencias());

                delegate.addListaMovimientos(delegate.getCuentaSeleccionada(), data.getPeriodoConsulta(), data);
                delegate.setPeriodo(Integer.valueOf(data.getPeriodoConsulta()));

                SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConsultaMovimientosInterbancariosViewController();

            } else {
                if (Server.ALLOW_LOG)
                    Log.d("ConsultaMofimientosInterbancariosDelegate >>", "analyzeResponse -- Operation Successful -- Caller: ConsultaMovimientosInterbancariosDelegate");
                this.setConsultaSPEIData(data);

                ordenarTransferencias(getConsultaSPEIData().getTransferencias());

                addListaMovimientos(cuentaSeleccionada, data.getPeriodoConsulta(), data);
                setPeriodo(Integer.valueOf(data.getPeriodoConsulta()));

                isPeriodoExceded(data.getPeriodoConsulta());

                if ((data == null) ||
                        (data != null && data.getTransferencias() == null) ||
                        (data != null && data.getTransferencias() != null && data.getTransferencias().isEmpty())) {

                    viewController.ocultaMasMovimientos();
                }

                viewController.cargarLista();
            }
        } else {
            viewController.cargarLista();
            caller.showInformationAlertEspecial(caller.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
        }

    }

    public void ordenarTransferencias(ArrayList<TransferenciaSPEI> transferencias) {
        ArrayList<TransferenciaSPEI> transferenciasDestinoBancomer = new ArrayList<TransferenciaSPEI>();
        ArrayList<TransferenciaSPEI> transferenciasDestinoOtro = new ArrayList<TransferenciaSPEI>();

        for (TransferenciaSPEI transferencia :transferencias) {
            if (transferencia.getBancoBeneficiario().equalsIgnoreCase("BBVA BANCOMER")) {
                transferenciasDestinoBancomer.add(transferencia);
            } else {
                transferenciasDestinoOtro.add(transferencia);
            }
        }

        TransferenciaSPEI.FechaComparator fechaComparator = new TransferenciaSPEI.FechaComparator();
        Collections.sort(transferenciasDestinoBancomer, fechaComparator);
        Collections.sort(transferenciasDestinoOtro, new TransferenciaSPEI.ClaveRastreoComparator());

        for (TransferenciaSPEI transferenciaBancomer : transferenciasDestinoBancomer) {
            boolean insertado = false;
            for (int i = 0; i < transferenciasDestinoOtro.size(); i++) {
                TransferenciaSPEI transferenciaOtro = transferenciasDestinoOtro.get(i);
                if (fechaComparator.compare(transferenciaBancomer, transferenciaOtro) < 0) {
                    transferenciasDestinoOtro.add(i, transferenciaBancomer);
                    insertado = true;
                    break;
                }
            }
            if (!insertado) {
                transferenciasDestinoOtro.add(transferenciaBancomer);
            }
        }

        transferencias.clear();
        transferencias.addAll(transferenciasDestinoOtro);
    }

    /****************************
     * Detalle
     ****************************/
    public ArrayList<Object> getDatosClave() {
        ArrayList<Object> datosDetalle = new ArrayList<Object>();
        ArrayList<String> registro;

        registro = new ArrayList<String>();
        registro.add("");
        registro.add(viewController.getString(R.string.bmovil_consultar_interbancario_clave_rastreo));
        datosDetalle.add(registro);

        registro = new ArrayList<String>();
        registro.add("");
        registro.add(Tools.formatCR(transferenciaSeleccionada.getClaveDeRastreo()));
        datosDetalle.add(registro);
        return datosDetalle;
    }

    public ArrayList<Object> getDetalleSpei() {
        ArrayList<Object> datosDetalle = new ArrayList<Object>();
        ArrayList<String> registro;

        registro = new ArrayList<String>();
        registro.add(viewController.getString(R.string.bmovil_consulta_movimientos_interbancarios_detalle_bancoOrdenante));
        registro.add(transferenciaSeleccionada.getBancoOrdenante());
        datosDetalle.add(registro);

        registro = new ArrayList<String>();
        registro.add(viewController.getString(R.string.bmovil_consulta_movimientos_interbancarios_detalle_cuentaRetiro));
        registro.add(transferenciaSeleccionada.getCuentaOrdenante());
        datosDetalle.add(registro);

        registro = new ArrayList<String>();
        registro.add(viewController.getString(R.string.bmovil_consulta_movimientos_interbancarios_detalle_nombreOrdenante));
        registro.add(transferenciaSeleccionada.getNombreOrdenante());
        datosDetalle.add(registro);

        registro = new ArrayList<String>();
        registro.add(viewController.getString(R.string.bmovil_consulta_movimientos_interbancarios_detalle_fechaOperacion));
        registro.add(Tools.formatDate(transferenciaSeleccionada.getFechaCalendario()));
        datosDetalle.add(registro);

        registro = new ArrayList<String>();
        registro.add(viewController.getString(R.string.bmovil_consulta_movimientos_interbancarios_detalle_importe));
        Boolean negativo = transferenciaSeleccionada.getMontodelPago().startsWith("-");
        registro.add(Tools.formatAmount(transferenciaSeleccionada.getMontodelPago(), negativo));
        datosDetalle.add(registro);

        registro = new ArrayList<String>();
        registro.add(viewController.getString(R.string.bmovil_consulta_movimientos_interbancarios_detalle_bancoDestino));
        registro.add(transferenciaSeleccionada.getBancoBeneficiario());
        datosDetalle.add(registro);

        registro = new ArrayList<String>();
        registro.add(viewController.getString(R.string.bmovil_consulta_movimientos_interbancarios_detalle_cuentaAsociada));
        registro.add(transferenciaSeleccionada.getCuentaBeneficiaria());
        datosDetalle.add(registro);

        registro = new ArrayList<String>();
        registro.add(viewController.getString(R.string.bmovil_consulta_movimientos_interbancarios_detalle_nombreBeneficiario));
        registro.add(transferenciaSeleccionada.getNombreDelBeneficiario());
        datosDetalle.add(registro);

        registro = new ArrayList<String>();
        registro.add(viewController.getString(R.string.bmovil_consulta_movimientos_interbancarios_detalle_motivoPago));
        registro.add(transferenciaSeleccionada.getConceptoDelPago());
        datosDetalle.add(registro);

        registro = new ArrayList<String>();
        registro.add(viewController.getString(R.string.bmovil_consulta_movimientos_interbancarios_detalle_referenciaNumerica));
        registro.add(transferenciaSeleccionada.getNumeroDeReferencia());
        datosDetalle.add(registro);

        return datosDetalle;
    }

    public int getOpcionesMenuResultados() {
        return SHOW_MENU_EMAIL;
    }

    public void enviaEmail() {

        ResultadosDelegate resultadosDelegate = new ResultadosDelegate(this);

        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showEnviarCorreo(resultadosDelegate);
    }

    /****************************
     * Autenticacion
     ****************************/
    @Override
    public boolean mostrarCVV() {
        Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

        return Autenticacion.getInstance().mostrarCVV(Constants.Operacion.consultaMovInterbancarios, perfil);
    }

    @Override
    public boolean mostrarNIP() {
        Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

        return Autenticacion.getInstance().mostrarNIP(Constants.Operacion.consultaMovInterbancarios, perfil);
    }

    @Override
    public boolean mostrarContrasenia() {
        Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

        return Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.consultaMovInterbancarios, perfil);
    }

    @Override
    public Constants.TipoOtpAutenticacion tokenAMostrar() {
        Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

        return Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.consultaMovInterbancarios, perfil);
    }

    @Override
    public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController, String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
        ConsultaMovimientosInterbancariosDelegate delegate = (ConsultaMovimientosInterbancariosDelegate) SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getBaseDelegateForKey(CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID);
        if (null == delegate) {
            delegate = new ConsultaMovimientosInterbancariosDelegate();
            SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().addDelegateToHashMap(CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID,
                    delegate);
        }

        delegate.setContrasenia((null == contrasenia) ? "" : contrasenia);
        delegate.setNip((null == nip) ? "" : nip);
        delegate.setToken((null == token) ? "" : token);
        delegate.setCampoTarjeta((null == campoTarjeta) ? "" : campoTarjeta);
        delegate.setCvv2((null == cvv) ? "" : cvv);

        String periodo = "0";
        delegate.cargaCuentasOrigen();
        delegate.consultaMovimientosInterbancarios(periodo, confirmacionAutenticacionViewController);

    }

    @Override
    public ArrayList<Object> getDatosTablaConfirmacion() {

        ArrayList<Object> tabla = new ArrayList<Object>();
        ArrayList<String> fila;

        fila = new ArrayList<String>();
        fila.add(SuiteApp.getInstance().getString(R.string.bmovil_spei_autenticacion_table_operacion));
        fila.add(SuiteApp.getInstance().getString(R.string.bmovil_spei_autenticacion_table_nombreOperacion));
        tabla.add(fila);
        fila = new ArrayList<String>();
        fila.add(SuiteApp.getInstance().getString(R.string.bmovil_spei_autenticacion_table_fechaOperacion));

        Date date = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        fila.add(formatter.format(date));

        tabla.add(fila);

        return tabla;
    }

    @Override
    public int getTextoEncabezado() {
        int resTitle;
        resTitle = R.string.bmovil_spei_title;
        return resTitle;
    }

    @Override
    public int getNombreImagenEncabezado() {
        int resIcon = R.drawable.bmovil_consultar_icono;
        return resIcon;
    }


    @Override
    public String getTextoTituloResultado() {
        int txtTitulo = R.string.confirmation_subtitulo_op;
        return SuiteApp.getInstance().getString(txtTitulo);
    }

    public ErrorData getErrorData() {
        return errorData;
    }

    public void setErrorData(ErrorData errorData) {
        this.errorData = errorData;
    }
}
