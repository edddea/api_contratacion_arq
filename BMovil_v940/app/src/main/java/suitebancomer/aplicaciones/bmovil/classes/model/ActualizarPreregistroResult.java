package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ActualizarPreregistroResult implements ParsingHandler {

	private String folio;
	
	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
	
		this.folio = parser.parseNextValue("FO");

	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

}
