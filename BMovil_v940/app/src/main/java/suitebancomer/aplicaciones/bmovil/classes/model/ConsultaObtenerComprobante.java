package suitebancomer.aplicaciones.bmovil.classes.model;

public class ConsultaObtenerComprobante {
	private String cuentaAbono;
	private String fecha;
	private String hora;
	private String importe;
	private String folio;
	private String cuentaCargo;
	public String getCuentaAbonoOGuia() {
		return cuentaAbono;
	}
	public void setCuentaAbonoOGuia(String cuentaAbono) {
		this.cuentaAbono = cuentaAbono;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getCuentaCargoOConvenio() {
		return cuentaCargo;
	}
	public void setCuentaCargoOConvenio(String cuentaCargo) {
		this.cuentaCargo = cuentaCargo;
	}
	ConsultaObtenerComprobante(){
		cuentaAbono="";
		fecha="";
		hora="";
		importe="";
		folio="";
		cuentaCargo="";
	}
}
