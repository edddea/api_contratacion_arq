package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

//import suitebancomer.aplicaciones.bmovil.classes.common.Operacion;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

import com.bancomer.base.callback.CallBackSession;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.apipush.ApiPushData;
import com.bancomer.mbanking.apipush.RegistrationIntentService;
import com.bancomer.mbanking.apipush.UtilsGCM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Tools;
import bancomer.api.consultaotroscreditos.models.ConsultaOtrosCreditosData;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.MenuPrincipalCreditDelegate;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.CambioPerfilFileManager;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.SincronizarSesion;
import suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa.ConstanstMenuOpcionesHamburguesa;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.InitHamburguesa;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.MenuHamburguesaViewsControllers;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.PerfilUserViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuPrincipalDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MisCuentasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.aplicaciones.bmovil.classes.model.Rapido;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.controllers.MenuSuiteViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;
import suitebancomer.classes.gui.views.CustomButton;
import suitebancomer.classes.gui.views.CustomButtonTDC;
//import tracking.TrackingHelper;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

//One Click
//Termina One Click


/**
 * Represents the bmovil main menu activity
 * @author michaelandrade
 *
 */
public class MenuPrincipalViewController extends BaseViewController implements OnClickListener, CallBackModule, CallBackSession {
	private ImageButton misCuentasButton;
	private ImageButton administrarButton;
	private ImageButton transferirButton;
	private ImageButton consultarButton;
	private ImageButton pagarButton;
	private ImageButton comprarButton;
	private ImageButton btnHamburguesa;
	//private ImageButton alertasButton;
	//BBVA CREDIT INTEGRATION.
	private ImageButton bbvaCredit;
	//paperless
	private Dialog popUp = null;

	//One click
	private LinearLayout layoutpromociones;
	private LinearLayout layout_menuprincipal;
	private LinearLayout bmovil_main_menu_promociones;
	private Button promocionButton;
	private LinearLayout layoutMisCuentasSuperior;
	private ImageButton btnMisCuentasSuperior;
	private ImageButton btnRetiroSinTarjeta;

	public MenuPrincipalDelegate delegate;
	boolean isCampana=false;
	boolean isAplicaCredit = false;
	private TextView lblImporte;
	//public String cveCamp;
	//Termina One CLick
		
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	
	private boolean alreadyClosing;
	private ViewGroup mBodyLayout;
	private boolean validaPaperless;
    private Session oSession;

	DrawerLayout drawerLayout;
	ListView listaHamburguesa;
	View footer;

	/**
	 * Default constructor for this activity
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		//One Click
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_menu_principal);
		//SuiteApp.appOrigen = "";//especifica que bmovil fue el que inicio
		SuiteApp.PASOMENUPRINCIPAL= new Boolean(true);

	//	super.onCreate(savedInstanceState, SHOW_HEADER, R.layout.layout_bmovil_menu_principal);

//		MenuSuiteViewController suiteViewController = (MenuSuiteViewController)SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController();
//		suiteViewController.setShouldHideLogin(true);
//		suiteViewController.restableceMenu();
        oSession = Session.getInstance(SuiteApp.appContext);
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		//One CLick
				/*setDelegate(parentViewsController.getBaseDelegateForKey(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID));
				delegate = (MenuPrincipalDelegate)getDelegate();
				delegate.setViewController(this); */

				BaseDelegate baseDelegate = parentViewsController.getBaseDelegateForKey(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID);
				if(baseDelegate == null || !(baseDelegate instanceof MenuPrincipalDelegate)){
					MenuPrincipalDelegate delegateAux = new MenuPrincipalDelegate(new Promociones());
					parentViewsController.removeDelegateFromHashMap(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID);
					parentViewsController.addDelegateToHashMap(
							MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID, delegateAux);
				}
				setDelegate(parentViewsController.getBaseDelegateForKey(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID));

				delegate = (MenuPrincipalDelegate)getDelegate();
				delegate.setViewController(this);

				//Termina One CLick
				validaPaperless= checkForPaperless();

				init();
				//One CLick
				//originalif(isCampana){
		        if(isCampana){
					setTitle(R.string.bmovil_promocion_title, R.drawable.an_icono_oneclick);
				}
				//Termina One Click
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("menu ppal", parentManager.estados);
		TrackingHelper.trackState("menu ppal");
		if(validaPaperless){
			mBodyLayout = (ViewGroup) findViewById(R.id.body_layout);
			mBodyLayout.setVisibility(View.GONE);
		}

		//Si el login fue exitoso
		//Revisando la implementacion
		if(UtilsGCM.checkPlayServices(this)){
			Session s= Session.getInstance(this.getBaseContext());
			ApiPushData a= ApiPushData.getInstance(s.getUsername(),s.getClientNumber());
			Intent intent = new Intent(this, RegistrationIntentService.class);
			startService(intent);
		}
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		listaHamburguesa = (ListView) findViewById(R.id.list_opciones_menu);
		//listaHamburguesa.addHeaderView(MenuSuiteViewController.getHeader());
		//footer = getLayoutInflater().inflate(R.layout.footer_burger, null);
		//listaHamburguesa.addFooterView(footer);
	}

	public void restoreMenu(){
		if(mBodyLayout != null)
			mBodyLayout.setVisibility(View.VISIBLE);
	}
	private boolean checkForPaperless() {
		boolean result = false;
		String cve;
		if(Session.getInstance(this).getPromociones()!=null && Session.getInstance(this).getPromociones().length > 0){
			Promociones[] promociones=Session.getInstance(this).getPromociones();
			String cveCamp = "";
			for(int i=0; i< promociones.length;i++){
				if(Server.ALLOW_LOG) {
					Log.d("PROOMO", "cion");
				}
				cve=promociones[i].getCveCamp();
				cveCamp = cve.substring(0, 4);
				if(cveCamp.equals("0429") && campanaPaperlessVisible() /*&& !campanaPaperlessNoAceptada()*/) {
						result = true;
					}
				}
			}
		return result;
	}
	private boolean campanaPaperlessNoAceptada() {
		CambioPerfilFileManager manager = CambioPerfilFileManager.getCurrent();
		if(manager.getNoAceptacionCampanaPaperless() == null)
			manager.setNoAceptacionCampanaPaperless(Constants.CAMPAÑA_PAPERLESS_VISIBLE);
		if (Server.ALLOW_LOG) Log.d("PROP",manager.getNoAceptacionCampanaPaperless());
		//return manager.getNoAceptacionCampanaPaperless().equals(Constants.CAMPAÑA_PAPERLESS_NO_ACEPTADA);
		return false;
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.getInstance().setMenuPrincipalViewController(this);
		suitebancomercoms.aplicaciones.bmovil.classes.common.Session session = suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(SuiteApp.appContext);
		if(session.isCmbPerfilHamburguesa()){
			SincronizarSesion.getInstance().SessionToSessionApi(SuiteApp.appContext);
			drawerLayout.openDrawer(listaHamburguesa);
			session.setCmbPerfilHamburguesa(Boolean.FALSE);
		}
		//muestraOpcionAlertas();

		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		setHabilitado(true);
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		setHabilitado(true);
		showHamburguesa();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if(drawerLayout.isDrawerOpen(listaHamburguesa)){
			if (!MenuHamburguesaViewsControllers.getIsOterApp()){
				drawerLayout.closeDrawer(listaHamburguesa);
			}
		}
		parentViewsController.consumeAccionesDePausa();
	}

	/*@Override
	public void onStop() {
		super.onStop();
		if(MenuHamburguesaViewsControllers.isBanderaHamburguesa()){
			btnHamburguesa.setVisibility(View.VISIBLE);
			if(!MenuHamburguesaViewsControllers.getIsOterApp()){
				parentViewsController.setActivityChanging(false);
				if (parentManager.consumeAccionesDePausa()) {
					return;
				}
				getParentViewsController().setCurrentActivityApp(this);
				setHabilitado(true);
			}
		}
		else
		{
			btnHamburguesa.setVisibility(View.INVISIBLE);
		}
	}*/
	
	/**
	 * Listener for views clicked
	 */
	@Override
	public void onClick(View v) {
		//ARR
				Map<String, Object> eventoMenu = new HashMap<String, Object>();
		if(!isHabilitado())
			return;
		setHabilitado(false);
		if(parentViewsController.isActivityChanging())
			return;

		/*if (v == alertasButton) {
			//ARR
			eventoMenu.put("evento_menu", "event27");
			eventoMenu.put("eVar27", "alertas");

			TrackingHelper.trackMenuPrincipal(eventoMenu);
			alertasSelected();
		}else */
		if (v == misCuentasButton) {
			//ARR
			eventoMenu.put("evento_menu", "event27");
			eventoMenu.put("eVar27", "mis cuentas");
			TrackingHelper.trackMenuPrincipal(eventoMenu);
			misCuentasSelected();
		}else if(v == administrarButton){
			//ARR
			eventoMenu.put("evento_menu", "event27");
			eventoMenu.put("eVar27", "administrar");
			TrackingHelper.trackMenuPrincipal(eventoMenu);
			administrarSelected();
		}else if(v==btnRetiroSinTarjeta){
			((BmovilViewsController)parentViewsController).showRetiroSinTarjetaViewController();
		}else if(v==btnMisCuentasSuperior){
			misCuentasSelected();
		}else if(v == transferirButton){
			//ARR
			eventoMenu.put("evento_menu", "event27");
			eventoMenu.put("eVar27", "transferir");
			TrackingHelper.trackMenuPrincipal(eventoMenu);
			transferirSelected();
		} else if (v == pagarButton) {
			//ARR
			eventoMenu.put("evento_menu", "event27");
			eventoMenu.put("eVar27", "pagar");
			TrackingHelper.trackMenuPrincipal(eventoMenu);
			pagarSelected();
		} else if(v == consultarButton){
			//ARR
			eventoMenu.put("evento_menu", "event27");
			eventoMenu.put("eVar27", "consultar");
			TrackingHelper.trackMenuPrincipal(eventoMenu);
			consultarSelected();		
		}else if(v == comprarButton) {
			//ARR
			eventoMenu.put("evento_menu", "event27");
			eventoMenu.put("eVar27", "comprar");
			TrackingHelper.trackMenuPrincipal(eventoMenu);
			comprarSelected();
		} else if (v == botonRapido1) {
			onRapidoClick(1);
			setHabilitado(true);
		} else if (v == botonRapido2) {
			onRapidoClick(2);
			setHabilitado(true);
		} else if (v == botonRapido3) {
			onRapidoClick(3);
			setHabilitado(true);
		} else if (v == botonRapido4) {
			onRapidoClick(4);
			setHabilitado(true);
		} else if (v == botonRapido5) {
			onRapidoClick(5);
			setHabilitado(true);
		}else if (v == bbvaCredit){
			Session.getInstance(SuiteApp.appContext).setOfertaDelSimulador(true);
			BBVACreditTask();
			Map<String,Object> click_menu_principal = new HashMap<String, Object>();
			click_menu_principal.put("evento_menu","event27");
			click_menu_principal.put("eVar27","simulador");
			TrackingHelper.trackMenuPrincipal(click_menu_principal);
		}//BBVA CREDIT INTEGRATION
		else if(v == btnHamburguesa){
			setHabilitado(true);
			drawerLayout.openDrawer(listaHamburguesa);
		}
	}
	
	/**
	 * Acci�n cuando la opci�n mis cuentas es seleccionada
	 */
	private void misCuentasSelected() {
//		((BmovilViewsController)getParentViewsController()).showMisCuentasViewController();
		Session session = Session.getInstance(SuiteApp.appContext);
		Autenticacion aut = Autenticacion.getInstance();
		Constants.Perfil perfil = session.getClientProfile();
		String cadAutenticacion =  aut.getCadenaAutenticacion(Constants.Operacion.consultarCreditos, perfil);
		if(Server.ALLOW_LOG) {
			Log.d(">> CGI", "Cadena de autenticacion >> " + cadAutenticacion);
			Log.d(">> CGI", "Perfil autenticacion >> " + perfil.name());
		}
		if(("00000").equals(cadAutenticacion)){
			if (Tools.isNetworkConnected(this)) {
				delegate.consultaOtrosCreditos();
			} else {
				((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showMisCuentasViewController();
			}
		}else{
			if(Server.ALLOW_LOG) {
				Log.d(">> CGI", "Go to confirmacion!");
			}
			MisCuentasDelegate delegate = (MisCuentasDelegate) ((BmovilViewsController)getParentViewsController()).getBaseDelegateForKey(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID);
			if (delegate == null) {
				delegate = new MisCuentasDelegate();
				((BmovilViewsController)getParentViewsController()).addDelegateToHashMap(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID,
						delegate);
			}
			showConfirmacion(delegate);
		}
	}

	public void showHamburguesa()
	{
		if(MenuHamburguesaViewsControllers.getDatosBmovil()!=null){
			Log.e("quitar","controlerInstanciado");
		}else{
			Log.e("quitar","ControllerNOInstanciado");
		}
		if(MenuHamburguesaViewsControllers.getDatosBmovil()!=null && MenuHamburguesaViewsControllers.getDatosBmovil().getCatalogoHamburguesa() != null)
		{
			listaHamburguesa.addHeaderView(MenuSuiteViewController.getHeader());
			//footer = getLayoutInflater().inflate(R.layout.footer_burger, null);
			//listaHamburguesa.addFooterView(footer);

			InitHamburguesa initHamburguesa = new InitHamburguesa(new suitebancomer.aplicaciones.bmovil.classes.model.hamburguesa.BanderasServer(Server.SIMULATION,Server.ALLOW_LOG, Server.EMULATOR, Server.DEVELOPMENT));
			initHamburguesa.getInstance().setCallBackSession(this);
			initHamburguesa.getInstance().setCallBackModule(this);
			MenuHamburguesaViewsControllers.iniciaHamburguesa(this, ConstanstMenuOpcionesHamburguesa.CON_SESSION, ConstanstMenuOpcionesHamburguesa.VALUE_NOMBRE_APP);
			btnHamburguesa.setVisibility(View.VISIBLE);
			MenuHamburguesaViewsControllers.bloquearDrawer(drawerLayout, false);
			listaHamburguesa = MenuHamburguesaViewsControllers.getListaOpciones();
			MenuHamburguesaViewsControllers.getImagenPerfil().setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					MenuHamburguesaViewsControllers.setIsOterApp(Boolean.TRUE);
					parentManager.setActivityChanging(true);
					Intent perfilHamburguesa = new Intent(MenuPrincipalViewController.this, PerfilUserViewController.class);
					MenuPrincipalViewController.this.startActivity(perfilHamburguesa);

				}
			});
			listaHamburguesa.setOnItemClickListener(new AdapterView.OnItemClickListener(){
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id)
				{
					if(position != 0 && position <= MenuHamburguesaViewsControllers.getElementosMenu().size())
					{
						position -= listaHamburguesa.getHeaderViewsCount();
						final String optionSelected = MenuHamburguesaViewsControllers.getElementosMenu().get(position).getNombreOpcion();
						parentManager.setActivityChanging(true);
						MenuHamburguesaViewsControllers.clickOpcionesHamburguesa(optionSelected, MenuPrincipalViewController.this, position);
					}
					/*else if (position > 8){
						cierraSesion();
					}*/
				}
			});
		}else
		{
			MenuHamburguesaViewsControllers.bloquearDrawer(drawerLayout, true);
			btnHamburguesa.setVisibility(View.INVISIBLE);
		}
	}

	public void showConfirmacion(MisCuentasDelegate delegate){
		int resSubtitle = 0;
		int resTitleColor = 0;
		int resIcon = R.drawable.bmovil_mis_cuentas_icono;
		int resTitle = R.string.mis_cuentas_title;
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(delegate, resIcon, resTitle, resSubtitle, resTitleColor);
	}
	
	@Override
	public void goBack() {
		//MenuSuiteViewController menuViewController = (MenuSuiteViewController)SuiteApp.getInstance().getSuiteViewsController().getCurrentViewControllerApp();
		//menuViewController.setShouldHideLogin(true);
		//ARR
		if(drawerLayout.isDrawerOpen(listaHamburguesa)){
			drawerLayout.closeDrawer(listaHamburguesa);
		}
		else {
			Map<String, Object> eventoSalir = new HashMap<String, Object>();
			eventoSalir.put("evento_salir", "event23");

			TrackingHelper.trackDesconexiones(eventoSalir);

		
		if (!alreadyClosing) {
			alreadyClosing = true;
			SuiteApp app = SuiteApp.getInstance();
			app.closeBmovilAppSession();
//			BmovilViewsController bmovilViewsController = (BmovilViewsController)getParentViewsController();
//			bmovilViewsController.getBmovilApp().logoutApp();
//			bmovilViewsController.setActivityChanging(true);
			}
			//finish();
			super.goBack();
		}
	}
	
	/**
	 * Accion cuando la opcion de Administrar es seleccionada 
	 */
	private void administrarSelected(){
		((BmovilViewsController)getParentViewsController()).showMenuAdministrar();
		
		// TODO Remover, solo para probar Contratacion.
		//((BmovilViewsController)getParentViewsController()).showContratacion();
	}

	/**
	 * Accion cuando la opcion de Transferir es seleccionada 
	 */
	private void transferirSelected(){
		((BmovilViewsController)getParentViewsController()).showOpcionesTransferir(Constants.Operacion.transferir.value, R.drawable.bmovil_transferir_icono, R.string.opciones_transferir_title, R.string.opcionesTransfer_menu_titulo);
	}
	 /**
	  * Accion cuando la opcion de Pagar es seleccionada 
	  */
	private void pagarSelected(){
		((BmovilViewsController)getParentViewsController()).showOpcionesPagar();
	}
	
	/**
	 * Acción cuando la opcion de Consultar es seleccionada
	 */
	private void consultarSelected(){
		((BmovilViewsController)getParentViewsController()).showMenuConsultar();
	}
	
	//One CLcik
		/**
		 * Acci€n cuando la opcion es una promocion
		 */
		private void promocionSelected(View v){
			if(Server.ALLOW_LOG) Log.d("", "estoy entrando aqui");
			delegate.promocionesselected(v);
			//delegate.realizaOperacion(Server.CONSULTA_DETALLE_OFERTA, this);
		}
	//Termina One click


	/**
	 * Inicializa la vista y las variables.
	 */
	private void init() {
		// Resize to screen.
		findViews();
		scaleForCurrentScreen();
		
		misCuentasButton.setOnClickListener(this);
		administrarButton.setOnClickListener(this);
		transferirButton.setOnClickListener(this);
		consultarButton.setOnClickListener(this);
		pagarButton.setOnClickListener(this);
		comprarButton.setOnClickListener(this);
		btnHamburguesa.setOnClickListener(this);
		btnMisCuentasSuperior.setOnClickListener(this);
		btnRetiroSinTarjeta.setOnClickListener(this);
		//alertasButton.setOnClickListener(this);

		//BBVA CREDIT INTEGRATION
		bbvaCredit.setOnClickListener(this);
		botonRapido1.setOnClickListener(this);
		botonRapido2.setOnClickListener(this);
		botonRapido3.setOnClickListener(this);
		botonRapido4.setOnClickListener(this);
		botonRapido5.setOnClickListener(this);
		inicializaRapidos();
	}
	
	/**
	 * Busca y establece la referencia a todas las vistas necesarias.
	 */
	private void findViews() {
		// Find menu items.
		misCuentasButton = (ImageButton) findViewById(R.id.bmovil_menu_option_miscuentas);
		administrarButton = (ImageButton) findViewById(R.id.bmovil_menu_option_administrar);
		transferirButton = (ImageButton) findViewById(R.id.bmovil_menu_option_transferir);
		consultarButton = (ImageButton) findViewById(R.id.bmovil_menu_option_consultar);
		pagarButton = (ImageButton) findViewById(R.id.bmovil_menu_option_pagar);
		comprarButton = (ImageButton) findViewById(R.id.bmovil_menu_option_comprar);
		btnHamburguesa = (ImageButton) findViewById(R.id.bmovil_menu_hamburguesa);
		//alertasButton = (ImageButton)findViewById(R.id.bmovil_menu_option_alertas);
		/*====Retiro sin tarjeta====*/
		layoutMisCuentasSuperior =(LinearLayout)findViewById(R.id.layout_menu_principalapp_mis_cuentas);
		btnMisCuentasSuperior =(ImageButton)findViewById(R.id.btn_miscuentas_superior);
		btnRetiroSinTarjeta =(ImageButton)findViewById(R.id.bmovil_menu_option_retiro_sin_tarjeta);
		/*====Retiro sin tarjeta====*/
		//BBVA CREDIT INTEGRATION
		bbvaCredit = (ImageButton) findViewById(R.id.bmovil_menu_option_bbva_credit);

		//One CLick
		layout_menuprincipal = (LinearLayout) findViewById(R.id.layout_menu_principalapp);
		layoutpromociones = (LinearLayout) findViewById(R.id.layoutbmovil_promociones);
		bmovil_main_menu_promociones = (LinearLayout) findViewById(R.id.bmovil_main_menu_promociones);
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		float density = metrics.density;
		//if(Session.getInstance(this).getPromociones()!=null){
		//if(Session.getInstance(this).getPromociones().length>0){
		if (Session.getInstance(this).getPromociones() != null && Session.getInstance(this).getPromociones().length > 0) {
			Promociones[] promociones = Session.getInstance(this).getPromociones();
			for (int i = 0; i < promociones.length; i++) {
				String cveCamp = promociones[i].getCveCamp().substring(0, 4);

				//carrusel
				String carrusel = promociones[i].getCarrusel();
				if(carrusel!=null){
					if (carrusel.equals("SI")) {
						validateCampain(density, cveCamp, promociones, i);
						isCampana = true;

						//JQH Validación para mostrar botón BBVA Credit cuando hay productos con LDA 0060, 0130,0296
						if (cveCamp.equals("0060") || cveCamp.equals("0130") || cveCamp.equals("0296")) {
								isAplicaCredit =true;
						}

					} else if (carrusel.equals("NO")){
						validateCampain(density, cveCamp, promociones, i);
						isCampana = false;//checar
					}
				}
			}

		} else {
			isCampana = false;
		}

		if (isCampana) {
			layoutpromociones = (LinearLayout) findViewById(R.id.layoutbmovil_promociones);
			layoutpromociones.setVisibility(LinearLayout.VISIBLE);
			bmovil_main_menu_promociones.setVisibility(LinearLayout.VISIBLE);
			//JQH bbvaCredit.setVisibility(View.VISIBLE);
			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

			//JQH Validación para mostrar botón BBVA Credit cuando hay productos con LDA 0060, 0130,0296
			if (isAplicaCredit) {
				bbvaCredit.setVisibility(View.VISIBLE);
				params.setMargins(0, 0, 0, 0);
			} else {
				bbvaCredit.setVisibility(View.GONE);
				params.setMargins(0, 20, 0, 0);
			}

			if (bbvaCredit.getVisibility() == View.VISIBLE){
				muestraCreditos(true);

			}else{
				muestraCreditos(false);
			}


			layout_menuprincipal.setLayoutParams(params);

		} else {
			layoutpromociones = (LinearLayout) findViewById(R.id.layoutbmovil_promociones);
			layoutpromociones.setVisibility(LinearLayout.GONE);
			bmovil_main_menu_promociones.setVisibility(LinearLayout.GONE);
			bbvaCredit.setVisibility(View.GONE);
			muestraCreditos(false);
			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
			params.setMargins(0, 20, 0, 0);
			layout_menuprincipal.setLayoutParams(params);
		}
		// Find fast payment items.
		botonRapido1 = (Button) findViewById(R.id.botonRapido1);
		botonRapido2 = (Button) findViewById(R.id.botonRapido2);
		botonRapido3 = (Button) findViewById(R.id.botonRapido3);
		botonRapido4 = (Button) findViewById(R.id.botonRapido4);
		botonRapido5 = (Button) findViewById(R.id.botonRapido5);
	}

	/**
	 * oculta o muestra el boton de miscuentas superior e inferior segun el comportaminto del boton
	 * creditos
	 * @param mostrar
	 */
	private void muestraCreditos(Boolean mostrar){
		if(mostrar){
			layoutMisCuentasSuperior.setVisibility(View.GONE);
			misCuentasButton.setVisibility(View.VISIBLE);
		}else{
			layoutMisCuentasSuperior.setVisibility(View.VISIBLE);
			misCuentasButton.setVisibility(View.GONE);
			/*LayoutParams param = new LayoutParams(misCuentasButton.getLayoutParams().width, misCuentasButton.getLayoutParams().height);
			Resources r = this.getResources();
			int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, r.getDisplayMetrics());
			param.setMargins(0, 0, px, 0);
			transferirButton.setLayoutParams(param);*/
		}
	}
				//carrusel

	//carrusel valida campania
	private void validateCampain(float density, String cveCamp,Promociones[] promociones, int i){

		int right =(int) (10* density);
				if(cveCamp.equals("0130")){	
				promocionButton= new Button(this);
				promocionButton.setId(i);				
				promocionButton.setText(Tools.formatAmount(promociones[i].getMonto(),false));
				/*String prueba=Tools.formatAmount("99999900",false);
				promocionButton.setText(prueba);*/
				promocionButton.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
				promocionButton.setTextColor(Color.rgb(246,137,30));
				promocionButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10);
				int t=(int)(10*density);
				int r=(int)(50*density);
				promocionButton.setPadding(0,t,r,0);
				promocionButton.setBackgroundResource(R.drawable.an_btn_ilcmenu);
				int w = (int) (243 * density);
				int h = (int) (55* density);
				LayoutParams lyParam = new LayoutParams(w,h);
				lyParam.setMargins(0, 0, right, 0);
				promocionButton.setLayoutParams(lyParam);
				promocionButton.setOnClickListener(clickListener);
				GuiTools guiTools = GuiTools.getCurrent();
				guiTools.init(getWindowManager());
				guiTools.scale(promocionButton, true);
				bmovil_main_menu_promociones.addView(promocionButton);
				//isCampana=true;
					isCampana = isCampana || true;
				}else if(cveCamp.equals("0377")){
					promocionButton= new Button(this);
					promocionButton.setId(i);
					promocionButton.setText(Tools.formatAmount(promociones[i].getMonto(),false));
					/*String prueba=Tools.formatAmount("99999900",false);
					promocionButton.setText(prueba);*/
					promocionButton.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
					promocionButton.setTextColor(Color.rgb(62,182,187));
					promocionButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10);
					int t=(int)(10*density);
					int r=(int)(70*density);
					promocionButton.setPadding(0,t,r,0);
					promocionButton.setBackgroundResource(R.drawable.anbtn_efi_promocion_menu);
					int w = (int) (243 * density);
					int h = (int) (55* density);
					LayoutParams lyParam = new LayoutParams(w,h);
					lyParam.setMargins(0, 0, right, 0);
					promocionButton.setLayoutParams(lyParam);
					promocionButton.setOnClickListener(clickListener);
					GuiTools guiTools = GuiTools.getCurrent();
					guiTools.init(getWindowManager());
					guiTools.scale(promocionButton, true);
					bmovil_main_menu_promociones.addView(promocionButton);
					//isCampana=true;
					isCampana= isCampana || true;
					//consumo oneclick
				}else if(cveCamp.equals("0060")) {
					promocionButton = new CustomButton(this);
					promocionButton.setId(i);
					promocionButton.setText(Tools.formatAmount(promociones[i].getMonto(), false));
					//String prueba=Tools.formatAmount("99999900",false);
					//promocionButton.setText(prueba);
					promocionButton.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
					promocionButton.setTextColor(Color.rgb(102, 204, 0));
					promocionButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10);
					/*int t=(int)(6*density);
					int r=(int)(40*density);*/
					int t = (int) (10 * density);
					int r = (int) (0 * density);
					promocionButton.setPadding(0, t, r, 0);
					promocionButton.setBackgroundResource(R.drawable.btn_consumo);
					int w = (int) (243 * density);
					int h = (int) (55 * density);
					LayoutParams lyParam = new LayoutParams(w, h);
					lyParam.setMargins(0, 0, right, 0);
					promocionButton.setLayoutParams(lyParam);
					promocionButton.setOnClickListener(clickListener);
					GuiTools guiTools = GuiTools.getCurrent();
					guiTools.init(getWindowManager());
					guiTools.scale(promocionButton, true);
					bmovil_main_menu_promociones.addView(promocionButton);
					//isCampana = true;
					isCampana= isCampana || true;
					//Termina one clcik
				}/** Venta TDC Adicional  **/
				else if (cveCamp.equals("0485")) {
					promocionButton = new CustomButton(this);
					promocionButton.setId(i);
					promocionButton.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
					promocionButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10);


					int t = (int) (10 * density);
					int r = (int) (0 * density);
					promocionButton.setPadding(0, t, r, 0);
					promocionButton.setBackgroundResource(R.drawable.an_bt_tdc_adicional);

					int w = (int) (243 * density);
					int h = (int) (55 * density);
					LayoutParams lyParam = new LayoutParams(w, h);
					lyParam.setMargins(0, 0, right, 0);
					promocionButton.setLayoutParams(lyParam);
					promocionButton.setOnClickListener(clickListener);
					GuiTools guiTools = GuiTools.getCurrent();
					guiTools.init(getWindowManager());
					guiTools.scale(promocionButton, true);
					bmovil_main_menu_promociones.addView(promocionButton);
					isCampana=true;
				}/** Domiciliación TDC **/
				else if (cveCamp.equals("0489")) {
					promocionButton = new CustomButton(this);
					promocionButton.setId(i);
					promocionButton.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
					promocionButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10);


					int t = (int) (10 * density);
					int r = (int) (0 * density);
					promocionButton.setPadding(0, t, r, 0);
					promocionButton.setBackgroundResource(R.drawable.an_btn_dom);

					int w = (int) (243 * density);
					int h = (int) (55 * density);
					LayoutParams lyParam = new LayoutParams(w, h);
					lyParam.setMargins(0, 0, right, 0);
					promocionButton.setLayoutParams(lyParam);
					promocionButton.setOnClickListener(clickListener);
					GuiTools guiTools = GuiTools.getCurrent();
					guiTools.init(getWindowManager());
					guiTools.scale(promocionButton, true);
					bmovil_main_menu_promociones.addView(promocionButton);
					isCampana=true;
				}/** Venta TDC Titular  **/
				else if(cveCamp.equals("0296")){
					promocionButton= new CustomButtonTDC(this);
					promocionButton.setId(i);
					promocionButton.setText(suitebancomer.aplicaciones.bmovil.classes.common.Tools.formatAmount(promociones[i].getMonto(), false));
					promocionButton.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
					promocionButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10);
					int t=(int)(10*density);
					int r=(int)(0*density);
					promocionButton.setPadding(0,t,r,0);
					promocionButton.setBackgroundResource(R.drawable.btn_tdc);
					int w = (int) (243 * density);
					int h = (int) (55* density);
					LayoutParams lyParam = new LayoutParams(w,h);
					lyParam.setMargins(0,0, right,0);
					promocionButton.setLayoutParams(lyParam);
					promocionButton.setOnClickListener(clickListener);
					GuiTools guiTools = GuiTools.getCurrent();
					guiTools.init(getWindowManager());
					guiTools.scale(promocionButton, true);
					bmovil_main_menu_promociones.addView(promocionButton);
					isCampana=true;
				}else if(cveCamp.equals("0429")){
					isCampana= isCampana || false;
//				if(cveCamp.equals("0429")){
					Log.e("cveCamp.equals(0429)", "Entrooo");
					if(validaPaperless) {
						Log.e("validaPaperless", "Entrooo");
						delegate.prepararTextoPaperless(promociones[i].getCveCamp());
					}else
						Log.e("validaPaperless", "No Entrooo");
				}
				else if(cveCamp.equals("0133")){
					promocionButton= new Button(this);
					promocionButton.setId(i);
					promocionButton.setText(suitebancomer.aplicaciones.bmovil.classes.common.Tools.formatAmount(promociones[i].getMonto(), false));
					promocionButton.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
					promocionButton.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10);
					promocionButton.setTextColor(Color.rgb(89, 54, 23));
					int t=(int)(15*density);
					int r=(int)(102*density);
					promocionButton.setPadding(0,t,r,0);
					promocionButton.setBackgroundResource(R.drawable.an_sf);
					int w = (int) (243 * density);
					int h = (int) (55* density);
					LayoutParams lyParam = new LayoutParams(w,h);
					lyParam.setMargins(0,0, right,0);
					promocionButton.setLayoutParams(lyParam);
					promocionButton.setOnClickListener(clickListener);
					GuiTools guiTools = GuiTools.getCurrent();
					guiTools.init(getWindowManager());
					guiTools.scale(promocionButton, true);
					bmovil_main_menu_promociones.addView(promocionButton);
					isCampana=true;
				}
			}
		/*else{
			layoutpromociones=(LinearLayout)findViewById(R.id.layoutbmovil_promociones);
			layoutpromociones.setVisibility(LinearLayout.GONE);		
			bmovil_main_menu_promociones.setVisibility(LinearLayout.GONE);	
			//isCampana=false;
		}*/
		
		//Validar Campaña Paperless
				/*if(Session.getInstance(this).getPromociones()!=null && Session.getInstance(this).getPromociones().length>0){
					
					Promociones[] promociones=Session.getInstance(this).getPromociones(); 
					for(int i=0; i< promociones.length;i++){
						if(cveCamp.equals("0429")){
							layoutpromociones=(LinearLayout)findViewById(R.id.layoutbmovil_promociones);
							layoutpromociones.setVisibility(LinearLayout.GONE);		
							bmovil_main_menu_promociones.setVisibility(LinearLayout.GONE);	
							isCampana=false;
						}
						
					} 
					
				}*/
		
		//Termina one clcik
		

	//}
	
	/**
	 * La campaña paperless es visible
	 *
	 * @return false si el usuario cuenta con un perfil recortado
	 */

	boolean campanaPaperlessVisible() {
		boolean visible = false;
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext)
				.getClientProfile();
		visible = (profile != Constants.Perfil.recortado);
		return visible;
	}


	/**
	 * Scale the view for the current screen resolution.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		// Scale menu items.
		guiTools.scale(findViewById(R.id.bmovil_main_menu_layout_first));
		guiTools.scale(findViewById(R.id.bmovil_main_menu_layout_second));
		guiTools.scale(findViewById(R.id.bmovil_main_menu_layout_third));
		guiTools.scale(findViewById(R.id.bmovil_main_menu_layout_fourth));
		guiTools.scale(findViewById(R.id.bmovil_main_menu_layout_fifth));
		//One Clcik
		
		guiTools.scale(findViewById(R.id.layoutbmovil_promociones));
		guiTools.scale(findViewById(R.id.horizontalscroll_promociones));
		guiTools.scale(bmovil_main_menu_promociones);
		
		//Termina One clcik

		
		guiTools.scale(misCuentasButton);
		guiTools.scale(administrarButton);
		guiTools.scale(transferirButton);
		guiTools.scale(consultarButton);
		guiTools.scale(pagarButton);
		guiTools.scale(comprarButton);
		guiTools.scale(bbvaCredit);
		guiTools.scale(btnHamburguesa);

		//guiTools.scale(layoutMisCuentasSuperior);
		guiTools.scale(btnMisCuentasSuperior);
		guiTools.scale(btnRetiroSinTarjeta);
		//guiTools.scale(alertasButton);
		
		// Scale fast payment items.
		guiTools.scale(findViewById(R.id.contenedorRapidos));
		guiTools.scale(findViewById(R.id.contenedorRapido1));
		guiTools.scale(findViewById(R.id.contenedorRapido2));
		guiTools.scale(findViewById(R.id.contenedorRapido3));
		guiTools.scale(findViewById(R.id.contenedorRapido4));
		guiTools.scale(findViewById(R.id.contenedorRapido5));
		guiTools.scale(botonRapido1, true);
		guiTools.scale(botonRapido2, true);
		guiTools.scale(botonRapido3, true);
		guiTools.scale(botonRapido4, true);
		guiTools.scale(botonRapido5, true);
	}

	/**
	 * Funcion para mostrar la opcion alertas si es perfil avanzado
	 */
	/*public void muestraOpcionAlertas(){
		// Si el perfil es avanzado se muestra el boton
		if(delegate.muestraOpcionAlertas()){
			alertasButton.setVisibility(View.VISIBLE);
		}else{
			alertasButton.setVisibility(View.GONE);
		}
	}*/

	/**
	 * Accion cuando la opcion de Comprar es seleccionada 
	 */
	private void comprarSelected(){
		((BmovilViewsController)getParentViewsController()).showComprarViewController();
	}
	//One Click
		OnClickListener clickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(Server.ALLOW_LOG) Log.d("", "si estpy entrando....!");
				promocionSelected(v);
                oSession.setOfertaDelSimulador(false); //MOD 53808
			}
		};
		
		//Termina One click
		


	// #region Rapidos.
	/**
	 * Primer botón de rápidos.
	 */
	private Button botonRapido1;

	/**
	 * Segundo botón de rápidos.
	 */
	private Button botonRapido2;

	/**
	 * Tercer botón de rápidos.
	 */
	private Button botonRapido3;

	/**
	 * Cuarto botón de rápidos.
	 */
	private Button botonRapido4;

	/**
	 * Quinto botón de rápidos.
	 */
	private Button botonRapido5;
	
	/**
	 * Lista de todos los rápidos.
	 */
	private ArrayList<Rapido> listaRapidos;
	
	/**
	 * �ndice de los rápidos que se muestran actulmente. 
	 */
	private int paginaListaRapidos;
	
	/**
	 * Número de paginas totales a mostrar.
	 */
	private int paginasTotalesRapidos;
	
	/**
	 * Bandera para indicar si esta presente el botón de agregar mas rápidos.
	 */
	private boolean agregarMas;
	
	/**
	 * Bandera para indicar si esta presente el boton de cargar mas rápidos.
	 */
	private boolean cargarMas;
	
	/**
	 * Carga los datos necesarios para el control de rápidos.
	 */
	private void inicializaRapidos() {
		agregarMas = false;
		cargarMas = false;
		
		cargarListaDeRapidos();
		
		paginaListaRapidos = -1;
		
		cargarMasRapidos();
		
		if(cargarMas) {
			botonRapido5.setBackgroundResource(R.drawable.bmovil_btn_rapida_mas);
		} else if(agregarMas) {
			//botonRapido5.setBackgroundResource(R.drawable.bmovil_btn_rapida_agregar);
			botonRapido5.setVisibility(View.GONE);
		}
	}
	
	/**
	 * Carga la lista de rápidos.
	 */
	private void cargarListaDeRapidos() {
		Rapido[] _rapidos = Session.getInstance(SuiteApp.appContext).getRapidos();
		listaRapidos = (null == _rapidos) ? null : new ArrayList<Rapido>(Arrays.asList(_rapidos));
		//listaRapidos = new ArrayList<Rapido>(Arrays.asList(Session.getInstance(SuiteApp.appContext).getRapidos()));
		
		if(null == listaRapidos)
			listaRapidos = new ArrayList<Rapido>();
		
		// ----------------------------------------------------------------------------------------
		// TODO: Remover. Ejemplo para prueba de rápidos.
//		int FAST_PAYMENT_COUNT = 2;
//		Rapido r;
//		Random rn = new Random();
//		for(int i = 0; i < FAST_PAYMENT_COUNT; i++) {
//			r = new Rapido();
//			r.setNombreCorto(java.util.UUID.randomUUID().toString().substring(0, 6));
//			int rand = rn.nextInt() % 2;
//			r.setCodigoOperacion((0 == rand) ? 
//								 Constants.RAPIDOS_CODIGO_OPERACION_PAGO_SERVICIOS : 
//								 Constants.RAPIDOS_CODIGO_OPERACION_RECARGAS);
//			
//			listaRapidos.add(r);
//		}
		// ----------------------------------------------------------------------------------------
		
		if(listaRapidos.size() < Constants.MAX_RAPIDOS_POR_PAGINA) {
			paginasTotalesRapidos = 1;
			agregarMas = true;
			cargarMas = false;
		} else if(listaRapidos.size() <= Constants.MAX_RAPIDOS_POR_PAGINA) {
			paginasTotalesRapidos = 1;
			agregarMas = false;
			cargarMas = false;
		} else if(listaRapidos.size() > Constants.MAX_RAPIDOS_POR_PAGINA) {
			paginasTotalesRapidos = listaRapidos.size() / (Constants.MAX_RAPIDOS_POR_PAGINA - 1);
			agregarMas = false;
			cargarMas = true;
			
			// Si el número de rápidos no es multiplo de la cantidad de rapidos que se pueden mostrar en pantalla,
			// entonces se agrega una pagina mas que esta estara parcialmente llena.
			if(0 != (listaRapidos.size() % (Constants.MAX_RAPIDOS_POR_PAGINA - 1)))
				paginasTotalesRapidos++;
		}
	}
	
	/**
	 * Carga los siguientes 4 botones de rápidos.
	 */
	private void cargarMasRapidos() {
		//Toast.makeText(this, "Cargar mas", Toast.LENGTH_SHORT).show();
		
		paginaListaRapidos = (++paginaListaRapidos) % paginasTotalesRapidos;
		int offset = (Constants.MAX_RAPIDOS_POR_PAGINA - 1) * paginaListaRapidos;
		int index;
		
		for(index = 0; index < (Constants.MAX_RAPIDOS_POR_PAGINA - 1) && (offset + index) < listaRapidos.size(); index++)
			actualizarBotonRapido(index, listaRapidos.get(offset + index));
		
		for(; index < (Constants.MAX_RAPIDOS_POR_PAGINA - 1); index++)
			actualizarBotonRapido(index, null);
		
		// Si el número de rápidos y el máximo por pagina coinciden se carga en la última posici�n un r�pido.
		if(Constants.MAX_RAPIDOS_POR_PAGINA == listaRapidos.size())
			actualizarBotonRapido(4, listaRapidos.get(offset + 4));
	}
	
	/**
	 *  
	 */
	private void agregarRapido() {
		((BmovilViewsController)getParentViewsController()).showAyudaAgregarRapido();
	}
	
	/**
	 * Actualiza la informacion y que muestra el boton de opraci�n rápida indicada.
	 * @param index El �ndice del botón por actulizar.
	 * @param data Los datos del r�pido.
	 */
	private void actualizarBotonRapido(int index, Rapido data) {
		Button botonParaActualizar = null;
		
		if(0 == index)
			botonParaActualizar = botonRapido1;
		else if(1 == index)
			botonParaActualizar = botonRapido2;
		else if(2 == index)
			botonParaActualizar = botonRapido3;
		else if(3 == index)
			botonParaActualizar = botonRapido4;
		else if(4 == index)
			botonParaActualizar = botonRapido5;
		
		if(null == data) {
			botonParaActualizar.setVisibility(View.GONE);
		} else {
			if(Constants.RAPIDOS_CODIGO_OPERACION_TIEMPO_AIRE.equals(data.getTipoRapido()))
				botonParaActualizar.setBackgroundResource(R.drawable.bmovil_btn_rapida_tiempo_aire);
			else if(Constants.RAPIDOS_CODIGO_OPERACION_DINERO_MOVIL.equals(data.getTipoRapido()))
				botonParaActualizar.setBackgroundResource(R.drawable.bmovil_btn_rapida_dinero_movil);
			else if(Constants.RAPIDOS_CODIGO_OPERACION_OTROS_BBVA.equals(data.getTipoRapido()))
				botonParaActualizar.setBackgroundResource(R.drawable.bmovil_btn_rapida_transferir);
			
			botonParaActualizar.setText(data.getNombreCorto());
			
			if(View.VISIBLE != botonParaActualizar.getVisibility())
				botonParaActualizar.setVisibility(View.VISIBLE);
		}
	}
	
	/**
	 * Comportamiento al presionar un boton de rápidos.
	 * @param index
	 */
	private void onRapidoClick(int index) {
		if(index < (Constants.MAX_RAPIDOS_POR_PAGINA)) {
			operarRapido(index);
		} else {
			if(cargarMas) {
				cargarMasRapidos();
			} else if(agregarMas) {
				agregarRapido();
			} else {
				operarRapido(index);
			}
		}
	}
	
	private void operarRapido(int index) {
		if(listaRapidos.size() < index)
			return;
		
		BmovilViewsController bmovilViewsController = (BmovilViewsController)getParentViewsController();
		Rapido selected = listaRapidos.get(index - 1);
		
		if(Constants.RAPIDOS_CODIGO_OPERACION_OTROS_BBVA.equals(selected.getTipoRapido())) {
			bmovilViewsController.showOtrosBBVAViewController(selected);
		} else if(Constants.RAPIDOS_CODIGO_OPERACION_TIEMPO_AIRE.equals(selected.getTipoRapido())) {
			bmovilViewsController.showTiempoAireViewController(selected);
		} else if(Constants.RAPIDOS_CODIGO_OPERACION_DINERO_MOVIL.equals(selected.getTipoRapido())) {
			bmovilViewsController.showTransferirDineroMovil(selected);
		}
	}

	//One Click
		public void processNetworkResponse(int operationId, ServerResponse response) {
			delegate.analyzeResponse(operationId, response);
		}
		//Termina One CLick
		

	// #endregion

	/**
	 * Acci�n cuando la opci�n alertas es seleccionada
	 */
	/*private void alertasSelected() {

		Session session = Session.getInstance(SuiteApp.appContext);
		InitAlertasDigitales init = InitAlertasDigitales.getInstance(this, ((BmovilViewsController) getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient());

		init.getConsultaSend().setCveAcceso(session.getPassword());
		init.getConsultaSend().setUsername(session.getUsername());

		GeneraOTPS generaOtps = new GeneraOTPSTDelegate();
		init.getConsultaSend().setGeneraOtps(generaOtps);
		init.getConsultaSend().setCallBackModule(this);
		init.getConsultaSend().setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR);
		init.getConsultaSend().setCatalogoTelefonicas(Session.getInstance(SuiteApp.appContext).getCatalogoDineroMovil());
		init.getConsultaSend().setPerfil(Session.getInstance(SuiteApp.getInstance()).getClientProfile().toString());
		init.getConsultaSend().setSoftTokenStatus(SuiteApp.getSofttokenStatus());
		init.getConsultaSend().setSecurityInstrument(Session.getInstance(SuiteApp.getInstance()).getSecurityInstrument());
		init.getConsultaSend().getAutenticacion().setAvanzado(Autenticacion.getInstance().getAvanzado());
		init.getConsultaSend().getAutenticacion().setBasico(Autenticacion.getInstance().getBasico());
		init.getConsultaSend().getAutenticacion().setRecortado(Autenticacion.getInstance().getRecortado());
		init.getConsultaSend().getAutenticacion().setLimiteOperacion(Autenticacion.getInstance().getLimiteOperacion());

		parentViewsController.setActivityChanging(true);
		Log.i("[CGI-Configuracion-Obligatorio]>> ", "Cliente http en app principal"+ ((BmovilViewsController)getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient());
		init.showAlertasDigitales();

	}*/

	@Override
	public void returnToPrincipal() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(true);
	}


	@Override
	public void returnDataFromModule(String operation, bancomer.api.common.io.ServerResponse response) {

		// Consulta Otros Creditos

		//if(operation.equalsIgnoreCase(bancomer.api.consultaotroscreditos.io.Server.OPERATION_CODES[bancomer.api.consultaotroscreditos.io.Server.OP_CONSULTA_OTROS_CREDITOS])){
		if(operation.equalsIgnoreCase(String.valueOf(ApiConstants.OP_CONSULTA_OTROS_CREDITOS))){
			MisCuentasDelegate delegateC = (MisCuentasDelegate) ((BmovilViewsController)getParentViewsController()).getBaseDelegateForKey(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID);
			if (delegateC == null) {
				delegateC = new MisCuentasDelegate();
				((BmovilViewsController)getParentViewsController()).addDelegateToHashMap(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID, delegateC);
			}

			ocultaIndicadorActividad();

			ConsultaOtrosCreditosData consultaOtrosCreditosData = (ConsultaOtrosCreditosData) response.getResponse();

			delegateC.setConsultaOtrosCreditosData(consultaOtrosCreditosData);

			((BmovilViewsController) SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().getParentViewsController()).showMisCuentasViewController();
		}
	}

	//BBVA CREDIT INTEGRATION

	private void BBVACreditTask()
	{
		Session session = Session.getInstance(this);
		// Alberto
		MainController.getInstance().setCurrentActivity(this);
		MainController.getInstance().setContext(this);
		MainController.getInstance().setMenuSuiteController(this);
		MainController.getInstance().muestraIndicadorActividad(getResources().getString(R.string.indicador_operacion), getResources().getString(R.string.indicador_conectando));

		MenuPrincipalCreditDelegate delegateMP = new MenuPrincipalCreditDelegate();
		delegateMP.setSessionByBmovil(session.getClientNumber(), session.getPassword(), session.getIum(), session.getUsername(), session.getEmail());
		delegateMP.doCalculo();


	}

	public void showMenuCredit()
	{
		MainController.getInstance().ocultaIndicadorActividad();
		((BmovilViewsController)getParentViewsController()).showMenuBBVACredit();
	}

	public void ofertaILC(OfertaILC oferta,Promociones promocion)
	{
		((BmovilViewsController)getParentViewsController()).showDetalleILC(oferta, promocion);
	}


	public void ofertaConsumo(OfertaConsumo oferta, Promociones promocion)
	{
		((BmovilViewsController)getParentViewsController()).showDetalleConsumo(oferta, promocion);
	}


	public void habilitarVista()
	{
		getParentViewsController().setCurrentActivityApp(this);
		setHabilitado(true);
	}

	//END REGION BBVA CREDIT INTEGRATION

	@Override
	public void userInteraction() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();
	}

	@Override
	public void cierraSesionBackground(Boolean isChanging) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		parentViewsController.setActivityChanging(isChanging);
		if (parentManager.consumeAccionesDePausa()) {
			finish();
			if(MenuHamburguesaViewsControllers.getIsOterApp()){
				MenuHamburguesaViewsControllers.setIsOterApp(Boolean.FALSE);
			}
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	public void cierraSesion() {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication()
				.setApplicationInBackground(Boolean.FALSE);
		SuiteApp.getInstance().getBmovilApplication().logoutApp(Boolean.FALSE);
	}

	@Override
	public void returnMenuPrincipal() {
		Session.getInstance(SuiteApp.appContext).reloadFiles();
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(Boolean.TRUE);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(true);
	}

	@Override
	public void closeSession(String userName, String ium, String client) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getBmovilApp().closeSession(userName, ium, client);
	}

}
