package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ImportesTDCData implements ParsingHandler {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String mensajeInformativo;
	
	private String fechaPago;
	
	private String fechaCorte;
	
	private String pagoMinimo;
	
	private String pagoNogeneraInt;
	
	private String saldoAlCorte;

	private String puntosBancomer;

	private String saldoMovsTransito;

	private String saldoActual;

	private String saldoDisponible;

	public ImportesTDCData(String mensajeInformativo, String fechaPago, String fechaCorte,
			String pagoMinimo, String saldoActual, String saldoMovsTransito, String saldoDisponible, String pagoNogeneraInt, String saldoAlCorte, String puntosBancomer) {
		super();
		this.mensajeInformativo = mensajeInformativo;
		this.fechaCorte = fechaCorte;
		this.fechaPago = fechaPago;
		this.pagoMinimo = pagoMinimo;
		this.saldoActual = saldoActual;
		this.saldoMovsTransito = saldoMovsTransito;
		this.saldoDisponible = saldoDisponible;
		this.pagoNogeneraInt = pagoNogeneraInt;
		this.saldoAlCorte = saldoAlCorte;
		this.puntosBancomer = puntosBancomer;
	}

	public ImportesTDCData() {
		// TODO Auto-generated constructor stubthis.mensajeInformativo = mensajeInformativo;
		this.fechaPago = "";
		this.fechaCorte = "";
		this.pagoMinimo = "";
		this.pagoNogeneraInt = "";
		this.saldoAlCorte = "";
		this.puntosBancomer = "";
		this.saldoMovsTransito = "";
		this.saldoActual = "";
		this.saldoDisponible = "";
	}

	public String getMensajeInformativo() {
		return mensajeInformativo;
	}

	public void setMensajeInformativo(String mensajeInformativo) {
		this.mensajeInformativo = mensajeInformativo;
	}

	public String getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}
	
	public String getFechaCorte() {
		return fechaCorte;
	}

	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}

	public String getPagoMinimo() {
		return pagoMinimo;
	}

	public void setPagoMinimo(String pagoMinimo) {
		this.pagoMinimo = pagoMinimo;
	}

	public String getPagoNogeneraInt() {
		return pagoNogeneraInt;
	}

	public void setPagoNogeneraInt(String pagoNogeneraInt) {
		this.pagoNogeneraInt = pagoNogeneraInt;
	}

	public String getSaldoAlCorte() {
		return saldoAlCorte;
	}

	public void setSaldoAlCorte(String saldoAlCorte) {
		this.saldoAlCorte = saldoAlCorte;
	}

	public String getPuntosBancomer() {
		return puntosBancomer;
	}

	public void setPuntosBancomer(String puntosBancomer) {
		this.puntosBancomer = puntosBancomer;
	}

	public String getSaldoMovsTransito() {
		return saldoMovsTransito;
	}

	public void setSaldoMovsTransito(String saldoMovsTransito) {
		this.saldoMovsTransito = saldoMovsTransito;
	}

	public String getSaldoActual() {
		return saldoActual;
	}

	public void setSaldoActual(String saldoActual) {
		this.saldoActual = saldoActual;
	}

	public String getSaldoDisponible() {
		return saldoDisponible;
	}

	public void setSaldoDisponible(String saldoDisponible) {
		this.saldoDisponible = saldoDisponible;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		try{
			this.mensajeInformativo = parser.parseNextValue("mensajeInformativo");
			JSONArray datos = parser.parseNextValueWithArray("datos");
			
			for (int i = 0; i < datos.length(); i++) {
				JSONObject jsonDatos= datos.getJSONObject(i);
				this.fechaPago = jsonDatos.getString("fechaPago");
				this.fechaCorte = jsonDatos.getString("fechaCorte");
				this.pagoMinimo = jsonDatos.getString("pagoMinimo");
				this.pagoNogeneraInt = jsonDatos.getString("pagoNoGeneraInt");
				this.saldoAlCorte = jsonDatos.getString("saldoAlCorte");
				this.puntosBancomer = jsonDatos.getString("puntosBancomer");
				this.saldoMovsTransito = jsonDatos.getString("saldoMovsTransito");
				this.saldoActual = jsonDatos.getString("saldoActual");
				this.saldoDisponible = jsonDatos.getString("saldoDisponible");
			}
		}catch(Exception e){
			Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
		}
		
	}

}
