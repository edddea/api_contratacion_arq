package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.util.Log;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.gui.delegates.BaseDelegate;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;

public class CuentaOrigenDelegate extends BaseDelegate{
	
	public final static long CUENTA_ORIGEN_DELEGATE_ID = 0x4fd454c56ca109f1L;
	
	CuentaOrigenViewController cuentaOrigenViewController;
	Account cuentaSeleccionada;
	public int indiceCuenta;
	ArrayList<Account> listaCuentaOrigen;
	Constants.Operacion tipoOperacion;

	public CuentaOrigenDelegate(Account cuentaSeleccionada) {
		this.cuentaSeleccionada = cuentaSeleccionada;
	}
	
	public CuentaOrigenViewController getCuentaOrigenViewController() {
		return cuentaOrigenViewController;
	}

	public void setCuentaOrigenViewController(
			CuentaOrigenViewController cuentaOrigenViewController) {
		this.cuentaOrigenViewController = cuentaOrigenViewController;
	}

	public Constants.Operacion getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(Constants.Operacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public Account getCuentaSeleccionada()
	{
		return cuentaSeleccionada;
	}
	
	public void setCuentaSeleccionada(Account cuenta)
	{
		cuentaSeleccionada = cuenta;
		indiceCuenta = listaCuentaOrigen.indexOf(cuenta);
		if(Server.ALLOW_LOG) Log.v(this.getClass().getSimpleName(), "CuentaOrigen envÌa: " + cuenta.getNumber());
		cuentaOrigenViewController.getDelegate().performAction(cuenta);
	}
	
	public void setCuentaSiguiente()
	{
		if (listaCuentaOrigen.size() > 1) {
			if (indiceCuenta == listaCuentaOrigen.size() -1) {
				indiceCuenta = 0;
			}
			else 
			{
				indiceCuenta += 1;
			}
		}
		setCuentaSeleccionada(listaCuentaOrigen.get(indiceCuenta));
	}
	
	public void setCuentaPrevia()
	{
		if (listaCuentaOrigen.size() > 1) {
			if (indiceCuenta == 0) {
				indiceCuenta = listaCuentaOrigen.size() -1;
			} 
			else 
			{
				indiceCuenta -= 1;
			}
		}
		setCuentaSeleccionada(listaCuentaOrigen.get(indiceCuenta));
	}
	
	public void setListaCuentasOrigen(ArrayList<Account>cuentasNuevas)
	{
		listaCuentaOrigen = cuentasNuevas;
		indiceCuenta = 0;
	}
	
	
	public ArrayList<Account> getListaCuentaOrigen() {
		return listaCuentaOrigen;
	}

	@Override
	public void performAction(Object obj) {
		if(Server.ALLOW_LOG) Log.d("CuentaOrigenDelegate", "Presionaste una cuenta de la lista" + ((Account)obj).getNumber());
		setCuentaSeleccionada((Account)obj);
		cuentaOrigenViewController.setIndiceCuentaSeleccionada(indiceCuenta);
		cuentaOrigenViewController.dejarDeMostrarLista();
	}
	
	public void actualizarCuentaSeleccionada(){
		cuentaSeleccionada = getListaCuentaOrigen().get(indiceCuenta);
	}
}
