package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.webkit.WebView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class AyudaContratacionViewController extends BaseViewController {
/*
	public AyudaContratacionViewController() {
		// TODO Auto-generated constructor stub
	}
*/
	private WebView contentView;
	
	/**
	 * Default constructor for this activity
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER, R.layout.layout_bmovil_ayuda_contratacion);
		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		
		findViews();
		scaleToScreenSize();
		
		InputStream htmlStream = getResources().openRawResource(R.raw.ayuda_contratacion);
		StringBuilder total = new StringBuilder();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(htmlStream, "UTF8"));
			
			String line;
			while ((line = reader.readLine()) != null) {
			    total.append(line);
			}
		} catch (UnsupportedEncodingException e) {
			total = null;
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "No se pudo adquirir el contenido.", e);
		} catch (IOException e) {
			total = null;
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Ocurrio un error mientras se leia el contenido.", e);
		}
		
		if(null != total) {
			contentView.loadDataWithBaseURL("file:///android_res/raw/", total.toString(), "text/html", "UTF-8", null);
			contentView.setVerticalScrollBarEnabled(true);
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	private void findViews() {
		contentView = (WebView)findViewById(R.id.contentView);
	}

	private void scaleToScreenSize() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(contentView, false);
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		float touchX = ev.getX();
		float touchY = ev.getY();
		int[] listaSeleccionPos = new int[2];
		
		contentView.getLocationOnScreen(listaSeleccionPos);
		
		float listaSeleccionX2 = listaSeleccionPos[0] + contentView.getMeasuredWidth();
		float listaSeleccionY2 = listaSeleccionPos[1] + contentView.getMeasuredHeight();
		
		if ((touchX >= listaSeleccionPos[0] && touchX <= listaSeleccionX2) &&
			(touchY >= listaSeleccionPos[1] && touchY <= listaSeleccionY2)) {
			contentView.getParent().requestDisallowInterceptTouchEvent(true);
		}
		
		return super.dispatchTouchEvent(ev);
	}
}
