package suitebancomer.aplicaciones.bmovil.classes.model;

import java.util.Vector;

public class CatalogoVersionado {

	private String version;

	private Vector<Object> objetos;
	
	public CatalogoVersionado(String version) {
		this.version = version;
		objetos = new Vector<Object>();
	}
	
	public String getVersion() {
		return version;
	}
	
	public void setVersion(String version) {
		this.version = version;
	}

	public void agregarObjeto(Object obj) {
		objetos.add(obj);
	}
	
	public Vector<Object> getObjetos() {
		return objetos;
	}
	
	public void setObjetos(Vector<Object> objetos) {
		this.objetos = objetos;
	}
	
	public int getNumeroObjetos() {
		return objetos.size();
	}
}
