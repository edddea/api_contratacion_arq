package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.model.CatalogoVersionado;
import bancomer.api.common.model.Convenio;
import mtto.suitebancomer.aplicaciones.bmovil.classes.entrada.InitMantenimiento;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerMapperUtil;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionRegistroViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultarFrecuentesViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.PagoServiciosAyudaViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.PagoServiciosFrecuentesViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.PagoServiciosViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.RegistrarOperacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.NombreCIEData;
import suitebancomer.aplicaciones.bmovil.classes.model.PagoServicio;
import suitebancomer.aplicaciones.bmovil.classes.model.PagoServicioResult;
import suitebancomer.aplicaciones.bmovil.classes.model.Payment;
import suitebancomer.aplicaciones.bmovil.classes.model.PaymentExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.RegistrarOperacionResult;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.classes.gui.controllers.BaseViewController;

public class PagoServiciosDelegate extends DelegateBaseAutenticacion {
	public final static long PAGO_SERVICIOS_DELEGATE_ID = 0xc4810fdda06505e2L;

	BaseViewController controladorPagos;
	Constants.Operacion tipoOperacion;
	PagoServicio pagoServicio;
	PagoServicioResult result;
	Payment selectedPayment;
	protected SolicitarAlertasData sa;

	private Payment[] frecuentes;

	//	private boolean preregister;
	private boolean isFrecuente;
	private boolean bajaFrecuente;
	//ARR
	public boolean res = false;

//	public boolean isPreregister() {
//		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
//		if(profile == Constants.Perfil.avanzado){
//			preregister = true;
//			tipoOperacion = Constants.Operacion.pagoServicios;
//		} else {
//			preregister = false;
//		}
//		return preregister;
//	}
//	public void setPreregister(boolean preregister) {
//		this.preregister = preregister;
//	}

	public PagoServiciosDelegate() {
		pagoServicio = new PagoServicio();
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
//		if(profile == Constants.Perfil.avanzado){
//			preregister = true;
		tipoOperacion = Constants.Operacion.pagoServicios;
//		} else {
//			preregister = false;
//		}
		isFrecuente = false;
		this.bajaFrecuente = false;
	}

	public PagoServiciosDelegate(PagoServicio servicio, boolean isFrecuente, boolean isRapido) {
		pagoServicio = servicio;
//		preregister = false;
		this.isFrecuente = isFrecuente;
	}

	public PagoServicioResult getResult() {
		return result;
	}

	public PagoServicio getPagoServicio() {
		return pagoServicio;
	}

	public void setPagoServicio(PagoServicio pagoServicio) {
		this.pagoServicio = pagoServicio;
	}

	public BaseViewController getControladorPagos() {
		return controladorPagos;
	}

	public void setControladorPagos(BaseViewController controladorPagos) {
		this.controladorPagos = controladorPagos;
	}

	public void setTipoOperacion(Constants.Operacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public Constants.Operacion getTipoOperacion() {
		return tipoOperacion;
	}

	public boolean isBajaFrecuente() {
		return bajaFrecuente;
	}

	public void setBajaFrecuente(boolean bajaFrecuente) {
		this.bajaFrecuente = bajaFrecuente;
	}

	public void reset() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
//		if(profile == Constants.Perfil.avanzado){
//			preregister = true;
//		} else {
//			preregister = false;
//		}
		isFrecuente = false;
		if (pagoServicio != null) {
			pagoServicio.reset();
		}
		bajaFrecuente = false;
	}

	public String getTextoTitulo() {
		return "";
	}

	public Object getListaFrecuentes() {
		return null;
	}

	public Operacion getOperationContext() {
		return Operacion.pagoServicios;
	}

	public Object invocaDelegateOperacion() {
		return null;
	}

	public void setFrecuente(boolean frecuente) {
		this.isFrecuente = frecuente;
	}

	public boolean isFrecuente() {
		return isFrecuente;
	}

	public void configuraPantalla() {
//		if (preregister) {
//			((PagoServiciosViewController)controladorPagos).hideCamposImporte();
//		} else {
		((PagoServiciosViewController) controladorPagos).showCamposImporte();
//		}

		if (isFrecuente) {
			CatalogoVersionado catalogo = Session.getInstance(SuiteApp.appContext).getCatalogoServicios();
			int sizeServicios = catalogo.getNumeroObjetos();
			Vector<Object> serviciosVector = catalogo.getObjetos();
			Convenio currentConvenio = null;
			Convenio matchingConvenio = null;
			((PagoServiciosViewController)controladorPagos).showCamposConvenio();
			for (int i=0; i<sizeServicios; i++) {
				currentConvenio = (Convenio) serviciosVector.get(i);
				if (currentConvenio.getNumeroConvenio().equals(pagoServicio.getConvenioServicio())) {
					matchingConvenio = currentConvenio;
					currentConvenio = null;
					((PagoServiciosViewController)controladorPagos).hideCamposConvenio();
					break;
				}
			}

			((PagoServiciosViewController)controladorPagos).prellenaCampos(matchingConvenio,
					pagoServicio.getConvenioServicio(),
					pagoServicio.getReferenciaServicio(),
					pagoServicio.getConceptoServicio(),
					pagoServicio.getImporte());
		}
	}

	@Override
	public ArrayList<Object> getDatosHeaderTablaFrecuentes() {
		ArrayList<Object> registros = new ArrayList<Object>();
		registros.add("");
		registros.add(SuiteApp.appContext.getString(R.string.servicesPayment_frequentList_headerList));
		return registros;
	}

	@Override
	public void consultarFrecuentes(String tipoOperacion) {
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> paramTable = null;
		paramTable = new Hashtable<String, String>();

		String tipoConsulta = tipoOperacion!=null?tipoOperacion:"";
		if(tipoConsulta.equals(Constants.tipoCFOtrosBBVA))
		{
			paramTable.put("numeroTelefono", 	session.getUsername());
			paramTable.put("IUM",			session.getIum());
			paramTable.put("numeroCliente",session.getClientNumber());

			//JAIG
			((BmovilViewsController)controladorPagos.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION_BBVA, paramTable,true, new PaymentExtract(),isJsonValueCode.NONE, controladorPagos,false);
		}
		else
		{
			paramTable.put("NT", 	session.getUsername());
			paramTable.put(ServerConstants.IUM_ETIQUETA,			session.getIum());
			paramTable.put("TE",session.getClientNumber());
			paramTable.put("TP",tipoConsulta);//TP
			paramTable.put("AP", "");
			//correo frecuentes
			paramTable.put("VM", Constants.APPLICATION_VERSION);

			//JAIG
			((BmovilViewsController)controladorPagos.getParentViewsController()).getBmovilApp().invokeNetworkOperation(Server.FAVORITE_PAYMENT_OPERATION, paramTable,false, new PaymentExtract(),isJsonValueCode.NONE, controladorPagos,false);
		}
	}

	public void performAction(Object obj) {
		if (controladorPagos instanceof PagoServiciosFrecuentesViewController) {
			if (obj instanceof Payment) {
				if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "Eligio un frecuente");
				Payment selectedPayment = (Payment) obj;
				ArrayList<Account> cuentas = cargaCuentasOrigen();
				Account cuentaOrigen = null;
				if(cuentas.size() > 0){
					cuentaOrigen = cuentas.get(0);
				}
				PagoServicio pagoServicio = new PagoServicio(cuentaOrigen,
						selectedPayment.getBeneficiaryAccount(),
						selectedPayment.getBeneficiary(),
						selectedPayment.getReference(),
						selectedPayment.getConcept(),
						selectedPayment.getAmount(),
						selectedPayment.getNickname(),
						"",
						selectedPayment.getUsuario());
				pagoServicio.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());
				pagoServicio.setPreregistered(selectedPayment.getIndicadorFrecuente().equals("P"));
				//SPEI
				if(pagoServicio.isPreregistered()){
					pagoServicio.setUsuario(selectedPayment.getUsuario());
				}//Termina SPEI
				((PagoServiciosFrecuentesViewController)controladorPagos).frecuenteSeleccionado(pagoServicio);
			} else {
				((PagoServiciosFrecuentesViewController) controladorPagos).cargaListaSeleccionComponent();
			}
		} else if (controladorPagos instanceof PagoServiciosViewController) {
			if (obj instanceof Account) {
				((PagoServiciosViewController) controladorPagos).actualizaOpcionSeleccionada((Account) obj);
			} else if (obj instanceof Convenio) {
				((PagoServiciosViewController) controladorPagos).actualizaCampos((Convenio)obj);
			}

		}
	}

	@Override
	public int getOpcionesMenuResultados() {
		/*if (preregister) {
			return 0;
		} else */if (isFrecuente) {
			if (pagoServicio.isPreregistered()) {
				if (pagoServicio.getCuentaOrigen().getType().equalsIgnoreCase(Constants.EXPRESS_TYPE)) {
					return SHOW_MENU_SMS | SHOW_MENU_PDF | SHOW_MENU_FRECUENTE | SHOW_MENU_RAPIDA;
				} else {
					return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF | SHOW_MENU_FRECUENTE | SHOW_MENU_RAPIDA;
				}
			} else {
				if (pagoServicio.getCuentaOrigen().getType().equalsIgnoreCase(Constants.EXPRESS_TYPE)) {
					return SHOW_MENU_SMS | SHOW_MENU_PDF | SHOW_MENU_RAPIDA;
				} else {
					return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF | SHOW_MENU_RAPIDA;
				}
			}
		} /* else if (isFast) { //TODO implement for fast payment
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF | SHOW_MENU_FRECUENTE;
		}*/ else if (bajaFrecuente) {
			return super.getOpcionesMenuResultados();
		} else {
			if (pagoServicio.getCuentaOrigen().getType().equalsIgnoreCase(Constants.EXPRESS_TYPE)) {
				return SHOW_MENU_SMS | SHOW_MENU_PDF | SHOW_MENU_FRECUENTE | SHOW_MENU_RAPIDA;
			} else {
				return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF | SHOW_MENU_FRECUENTE | SHOW_MENU_RAPIDA;
			}
		}
	}

	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la
	 * lista es ordenada seg�n sea reguerido.
	 *
	 * @return
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();

		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext)
				.getAccounts();

		if (profile == Constants.Perfil.avanzado) {
			for (Account acc : accounts) {
				if (acc.isVisible()) {
					accountsArray.add(acc);
					break;
				}
			}

			for (Account acc : accounts) {
				if (!acc.isVisible())
					accountsArray.add(acc);
			}

		} else {
			for (Account acc : accounts) {
				if (acc.isVisible())
					accountsArray.add(acc);
			}
		}

		return accountsArray;
	}

	/**
	 * Set the source account for the transaction.
	 *
	 * @param sourceAccount
	 *            The source account
	 * @throws InvalidParameterException
	 *             If the source account is null.
	 */
	public void setCuentaSeleccionada(Account sourceAccount) throws InvalidParameterException {
		if (null == sourceAccount) {
			InvalidParameterException ex = new InvalidParameterException("The source account can not be null.");
			if(Server.ALLOW_LOG) Log.e("TransferirMisCuentasDelegate","The source account can not be null.", ex);
			throw ex;
		} else {
			pagoServicio.setCuentaOrigen(sourceAccount);
		}
	}

	/**
	 * Set the source service for the transaction.
	 *
	 * @param sourceService
	 *            The source service
	 * @throws InvalidParameterException
	 *             If the source account is null.
	 */
	public void setConvenioSeleccionado(Convenio servicio) throws InvalidParameterException {
		if (null == servicio) {
			InvalidParameterException ex = new InvalidParameterException("The source service can not be null.");
			if(Server.ALLOW_LOG) Log.e("TransferirMisCuentasDelegate","The source service can not be null.", ex);
			throw ex;
		} else {
			pagoServicio.setConvenioServicio(servicio.getNumeroConvenio());
			pagoServicio.setServicio(servicio.getNombre());

			if (controladorPagos instanceof PagoServiciosViewController) {
				if (pagoServicio.getServicio().equals(controladorPagos.getString(R.string.servicesPayment_otherServices))) {
					((PagoServiciosViewController)controladorPagos).showCamposConvenio();
					((PagoServiciosViewController)controladorPagos).hideLeerCodigoButton();
				} else {
					((PagoServiciosViewController)controladorPagos).hideCamposConvenio();
					((PagoServiciosViewController)controladorPagos).showLeerCodigoButton();
				}
			}
		}
	}

	/*
	 * �todo para cargar la lista de frecuentes
	 */
	/*
	 * public ArrayList<Frecuente> cargaFrecuentes(){
	 *
	 * }
	 */

	/**
	 * Check if the current server date is between the available operation time,
	 * which is "hard coded" to Monday to Friday from 6:00 to 17:20
	 *
	 * @return true if the current server date is between the available
	 *         operation hours, false if not
	 */
	public boolean isOpenHours() {
		return Session.getInstance(SuiteApp.getInstance()).isOpenHoursForServicePayments();
	}

	/**
	 * Displays a text that indicates that external transfers cannot be
	 * performed during current time.
	 */
	public String textoMensajeAlerta() {
		return Session.getInstance(SuiteApp.getInstance()).getOpenHoursForServicePaymentsMessage();
	}

	/**
	 * Shows the help screen.
	 *
	 * @param type the Context or Reference help kind
	 */
	public void requestReferenceHelp() {

		if(pagoServicio.getServicio() == null || pagoServicio.getServicio().equals("")){
			return;
		}

		requestHelpForType(PagoServiciosAyudaViewController.REFERENCE_HELP_ID);
	}

	/**
	 * Shows the help screen.
	 *
	 * @param type the Context or Reference help kind
	 */
	public void requestReasonHelp() {

		if(pagoServicio.getServicio() == null || pagoServicio.getServicio().equals("")){
			return;
		}

		requestHelpForType(PagoServiciosAyudaViewController.REASON_HELP_ID);
	}

	private void requestHelpForType(String helpType) {
		// Set a predefined screen width: 128, 176, 240, 320
		Display display = controladorPagos.getWindowManager().getDefaultDisplay();
		int width = display.getWidth();
		int width_tx = width;
		if (width <= 128) {
			width = 128;
		} else if (width <= 176) {
			width = 176;
		} else if (width <= 240) {
			width = 240;
		} else {
			width = 320;
		}

		// Gets the service payment provider
		String provider = "";
		provider = pagoServicio.getConvenioServicio();

		String[] llaves = new String[4];
		Object[] valores = new Object[4];

		llaves[0] = Server.HELP_IMAGE_TYPE_PARAM;
		valores[0] = helpType;
		llaves[1] = Server.SCREEN_SIZE_PARAM;
		valores[1] = String.valueOf(width);
		llaves[2] = Server.SERVICE_PROVIDER_PARAM;
		valores[2] = provider;
		llaves[3] = "TEXT_WIDTH";
		valores[3] = Integer.valueOf(width_tx);

		((BmovilViewsController)controladorPagos.getParentViewsController()).showPagoServiciosAyudaViewController(llaves, valores);
	}

	public void consultaDatosCodigo(String valorCodigo, String numConvenio) {
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put("CM", numConvenio);
		paramTable.put("QR", valorCodigo);
		//JAIG
		SuiteApp.getInstance().getBmovilApplication().invokeNetworkOperation(Server.CONSULTAR_CODIGO_PAGO_SERVICIOS,
				paramTable, false, new PagoServicio(), isJsonValueCode.NONE, controladorPagos, true);

	}

	public void validaDatos(String referencia, String concepto, String importe, String convenio) {
		boolean requiresCIEName = false;
		if ((pagoServicio.getServicio() == null || pagoServicio.getServicio().equals(""))) {
			controladorPagos.showInformationAlert(R.string.servicesPayment_selectService);
			return;
		}
		if (pagoServicio.getServicio().equals(controladorPagos.getString(R.string.servicesPayment_otherServices))) {
			if (convenio == null || convenio.equals("")) {
				controladorPagos.showInformationAlert(R.string.servicesPayment_invalidCIE);
				return;
			} else {
				pagoServicio.setConvenioServicio(convenio);
				if (!isFrecuente) {
					requiresCIEName = true;
				}
			}
		}
		if (referencia == null || referencia.equals("")) {
			controladorPagos.showInformationAlert(R.string.servicesPayment_referenceTooShort);
			return;
		} else {
			pagoServicio.setReferenciaServicio(referencia);
		}

//		if (!preregister) {
		if (!validaMonto(Tools.formatAmountForServer(importe))) {
			controladorPagos.showInformationAlert(R.string.servicesPayment_invalid_amount);
			return;
		} else {
			pagoServicio.setImporte(Tools.formatAmountForServer(importe));
		}
//		}

		pagoServicio.setConceptoServicio(concepto == null ? "" : concepto);

		if (requiresCIEName) {
			requestCIEName();
		} else {
			//ARR
			res = true;
			boolean validaRegistro = Autenticacion.getInstance().validaRegistro(Operacion.pagoServicios, Session.getInstance(SuiteApp.appContext).getClientProfile());
			//con frecuentes no pida la OTP de registro
			if (isFrecuente) {
				validaRegistro = false;
			}
			if (validaRegistro) {
				showRegistroOperacion();
			} else {
				showConfirmacion();
			}
		}
	}

	private void requestCIEName() {
		Hashtable<String,String> paramTable = new Hashtable<String, String>();
		Session session = Session.getInstance(SuiteApp.appContext);
		int operacion = Server.CONSULTA_CIE;
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());
		paramTable.put("NU", pagoServicio.getConvenioServicio());
		//JAIG
		doNetworkOperation(operacion, paramTable, false, new NombreCIEData(), isJsonValueCode.NONE, controladorPagos);
	}

	/**
	 * Validates if the amount is correct.
	 * @param amount The amount for the transaction.
	 * @return True if the amount is correct.
	 */
	public boolean validaMonto(String amount) {
		int value = -1;

		try {
			value = Integer.parseInt(amount);
		} catch (NumberFormatException ex) {
			if(Server.ALLOW_LOG) Log.e("TransferirMisCuentasDelegate", "Error while parsing the amount as an int.", ex);
			return false;
		}

		return (value > 0);
	}

	private void showConfirmacion() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacion(this);
	}

	private void showRegistroOperacion() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showRegistrarOperacion(this);
	}

//	public void showConfirmacionRegistro() {
//		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionRegistro(this);
//	}hcf

	public Payment[] getFrecuentes() {
		return frecuentes;
	}

	public void setFrecuentes(Payment[] frecuentes) {
		this.frecuentes = frecuentes;
	}

	private void mostrarListaFrecuentes() {
		if (controladorPagos instanceof PagoServiciosFrecuentesViewController) {
			((PagoServiciosFrecuentesViewController)controladorPagos).muestraFrecuentes();
		} else if (controladorPagos instanceof ConsultarFrecuentesViewController) {
			((ConsultarFrecuentesViewController)controladorPagos).muestraFrecuentes();
		}
	}

	public void operarFrecuente(int indexSelected){
		reset();
		ArrayList<Object> arlPayments = null;
		if (controladorPagos instanceof ConsultarFrecuentesViewController) {
			arlPayments = ((ConsultarFrecuentesViewController)controladorPagos).getListaFrecuentes().getLista();
		}

		Payment selectedPayment = frecuentes[indexSelected];
		if (arlPayments != null && arlPayments.size() > 0) {
			selectedPayment = (Payment)((ArrayList<Object>)arlPayments.get(indexSelected)).get(0);
		}
		ArrayList<Account> cuentas = cargaCuentasOrigen();
		Account cuentaOrigen = null ;
		if(cuentas.size() > 0)
			cuentaOrigen = cuentas.get(0);

		this.pagoServicio = new PagoServicio(cuentaOrigen,
				selectedPayment.getBeneficiaryAccount(),
				selectedPayment.getBeneficiary(),
				selectedPayment.getReference(),
				selectedPayment.getConcept(),
				selectedPayment.getAmount(),
				selectedPayment.getNickname(),
				"",
				selectedPayment.getUsuario());
		pagoServicio.setCorreoFrecuente(selectedPayment.getCorreoFrecuente());
		isFrecuente = true;
//		   preregister = false;
		if(isOpenHours()){
			if(controladorPagos != null) {
				((BmovilViewsController)controladorPagos.getParentViewsController()).showPagoServiciosViewController(pagoServicio);
			}

		}else{
			((BmovilViewsController)controladorPagos.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(textoMensajeAlerta());
		}
	}

	@Override
	public ArrayList<Object> getDatosTablaFrecuentes() {
		ArrayList<Object> listaDatos = new ArrayList<Object>();
		if (frecuentes != null) {
			for (Payment payment : frecuentes) {
				ArrayList<Object> registro = new ArrayList<Object>();
				registro.add(payment);
				if (payment.getIndicadorFrecuente().equals("P")) {
					String nickname = SuiteApp.appContext.getString(R.string.servicesPayment_registro_preregisterPrefix);
					nickname += " " + payment.getBeneficiaryAccount();
					registro.add(nickname);
				} else {
					registro.add(payment.getNickname());
				}

				listaDatos.add(registro);
			}
		}/* else {
			ArrayList<Object> registro = new ArrayList<Object>();
			registro.add("");
			registro.add("Sin frecuentes");
			listaDatos.add(registro);
		}*/
		return listaDatos;
	}

	@Override
	public void realizaOperacion(ConfirmacionViewController confirmacionViewController, String contrasenia, String nip, String token, String campoTarjeta) {
		int operacion = 0;
		Hashtable<String,String> paramTable = new Hashtable<String, String>();
		Session session = Session.getInstance(SuiteApp.appContext);
		/*if (preregister) {
			operacion = Server.PREREGISTRAR_PAGO_SERVICIOS;
			paramTable.put("NT", session.getUsername());
			paramTable.put("NP", Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia );
			paramTable.put("IU", session.getIum());
			paramTable.put("CC", "");
			paramTable.put("FN",Server.PAYMENT_SERVICES_PREREGISTER_FUNCTION);
			paramTable.put("TP",Server.PAYMENT_SERVICES_PREREGISTER_REASON_TYPE);
			paramTable.put("CI", pagoServicio.getConvenioServicio());
			if (tokenAMostrar() == Constants.TipoOtpAutenticacion.ninguno) {
				paramTable.put("IT", "");
			} else {
				paramTable.put("IT", Server.PAYMENT_SERVICES_TOKEN_TYPE);
			}
			paramTable.put("BF", pagoServicio.getServicio());
			paramTable.put("RF", pagoServicio.getReferenciaServicio());
			paramTable.put("CP", Tools.removeSpecialCharacters(pagoServicio.getConceptoServicio()));
			paramTable.put("NI", Tools.isEmptyOrNull(nip)? "" : nip);
			paramTable.put("CV", Server.CVV2_PARAM);
			paramTable.put("OT", Tools.isEmptyOrNull(token)? "" : token);
			String va =  Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,session.getClientProfile());
			paramTable.put("VA", va);
			paramTable.put("order", "NT*NP*IU*CC*FN*TP*CI*IT*BF*RF*CP*NI*CV*OT*VA");
			doNetworkOperation(operacion, paramTable, false, new PagoServicioResult(PagoServicioResult.RESULT_PREREGISTER), isJsonValueCode.NONE, confirmacionViewController);
		} else */if (bajaFrecuente) {

			operacion = Server.BAJA_FRECUENTE;
			//completar hashtable
			paramTable = new Hashtable<String, String>();

			paramTable.put(Server.USERNAME_PARAM, session.getUsername());//"NT"
			paramTable.put(Server.PASSWORD_PARAM, Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia );//NP
			paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());//IU
			paramTable.put(Server.NUMERO_CLIENTE_PARAM, session.getClientNumber());//TE
			paramTable.put(Server.DESCRIPCION_PARAM, pagoServicio.getAliasFrecuente());//DE
			paramTable.put(Server.PAYMENT_ACCOUNT_PARAM, pagoServicio.getConvenioServicio());//CA
//			paramTable.put(Server.BENEFICIARIO_PARAM, compraTiempoAire.getNombreCompania());//BF
			paramTable.put(Server.REFERENCIA_NUMERICA_PARAM, pagoServicio.getReferenciaServicio());//RF
			paramTable.put(Server.TIPO_CONSULTA_PARAM, Constants.TPPagoServicios);//TP
			String va =  Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile());
			paramTable.put(Server.VA_PARAM,Tools.isEmptyOrNull(va)?"":va);//validar

			Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
			paramTable2= ServerMapperUtil.mapperBaja(paramTable);
			doNetworkOperation(operacion, paramTable2, false, null, isJsonValueCode.NONE, confirmacionViewController);

		} else {
			operacion = Server.SERVICE_PAYMENT_OPERATION;
			paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
			paramTable.put(ServerConstants.PARAMS_TEXTO_NP, Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia );
			paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());
			paramTable.put("CC", pagoServicio.getCuentaOrigen().getFullNumber());
			paramTable.put("CA", pagoServicio.getConvenioServicio());
			paramTable.put("RF", pagoServicio.getReferenciaServicio());
			paramTable.put("CP", Tools.removeSpecialCharacters(pagoServicio.getConceptoServicio()));
			paramTable.put("IM", pagoServicio.getImporte());
			paramTable.put("NI", Tools.isEmptyOrNull(nip)? "" : nip);
			paramTable.put("CV", "");
			//paramTable.put(Server.CVV2_PARAM, Tools.isEmptyOrNull(cvv)? "" : nip);
			paramTable.put("OT", Tools.isEmptyOrNull(token)? "" : token);
			String va =  Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,
					session.getClientProfile(),
					Tools.getDoubleAmountFromServerString(pagoServicio.getImporte()));
			paramTable.put(ServerConstants.PARAMS_TEXTO_VA, va);
			//JAIG
			doNetworkOperation(operacion, paramTable, false, new PagoServicioResult(PagoServicioResult.RESULT_PAYMENT), isJsonValueCode.NONE, confirmacionViewController);
		}


	}

	@Override
	protected void realizaOperacion(RegistrarOperacionViewController rovc, String token){
		int operationId = Server.OP_VALIDAR_CREDENCIALES;
		Hashtable<String, String> params = new Hashtable<String, String>();
		Session session = Session.getInstance(SuiteApp.appContext);

		params.put(ServerConstants.ID_OPERACION,ServerConstants.PAGO_SERVICIOS);
		params.put(ServerConstants.SERVICIO, pagoServicio.getServicio());
		params.put(ServerConstants.CONVENIO, pagoServicio.getConvenioServicio());
		params.put(ServerConstants.REFERENCIA, pagoServicio.getReferenciaServicio());

		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put("numeroCliente", session.getClientNumber());
		params.put("cveAcceso", "" );
		params.put(Server.J_NIP, "");
		params.put(Server.J_CVV2, "");
		params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
		params.put(Server.J_AUT, "00010");
		params.put(ServerConstants.IUM, session.getIum());
		params.put("tarjeta5Dig", "");
		params.put(ServerConstants.VERSION, Constants.APPLICATION_VERSION);
		//JAIG SI
		doNetworkOperation(operationId, params,true, new RegistrarOperacionResult(),isJsonValueCode.NONE, rovc);
	}

	public void realizaOperacion(ConfirmacionRegistroViewController viewController,	String contrasena, String nip, String asm) {
		int operacion = 0;
		Hashtable<String,String> paramTable = new Hashtable<String, String>();
		Session session = Session.getInstance(SuiteApp.appContext);

		operacion = Server.SERVICE_PAYMENT_OPERATION;
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
		paramTable.put(ServerConstants.PARAMS_TEXTO_NP, Tools.isEmptyOrNull(contrasena) ? "" : contrasena );
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());
		paramTable.put("CC", pagoServicio.getCuentaOrigen().getFullNumber());
		paramTable.put("CA", pagoServicio.getConvenioServicio());
		paramTable.put("RF", pagoServicio.getReferenciaServicio());
		paramTable.put("CP", Tools.removeSpecialCharacters(pagoServicio.getConceptoServicio()));
		paramTable.put("IM", pagoServicio.getImporte());
		paramTable.put("NI", Tools.isEmptyOrNull(nip)? "" : nip);
		paramTable.put("CV", "");
		//paramTable.put(Server.CVV2_PARAM, Tools.isEmptyOrNull(cvv)? "" : nip);
		paramTable.put("OT", Tools.isEmptyOrNull(asm)? "" : asm);
		String va =  Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,
				session.getClientProfile(),
				Tools.getDoubleAmountFromServerString(pagoServicio.getImporte()));
		paramTable.put(ServerConstants.PARAMS_TEXTO_VA, va);
		//JAIG
		doNetworkOperation(operacion, paramTable, false, new PagoServicioResult(PagoServicioResult.RESULT_PAYMENT), isJsonValueCode.NONE, viewController);
	}

	//hcf
	/**
	 * @category RegistrarOperacion
	 * Muestra la pantalla de confirmacion registro
	 */
	public void showConfirmacionRegistro() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		bMovilVC.showConfirmacionRegistro(this);
	}

	@Override
	public String getNumeroCuentaParaRegistroOperacion() {
		String cuentaDestino = pagoServicio.getReferenciaServicio();
		return cuentaDestino.substring(cuentaDestino.length() - 5);
	}
	//hcf

	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {
		SuiteApp.getInstance().getBmovilApplication().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
	}

	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
		ArrayList<Object> datosConfirmacion = new ArrayList<Object>();
		ArrayList<String> registro;

		if (bajaFrecuente) {
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.altafrecuente_nombrecorto));
			registro.add(pagoServicio.getAliasFrecuente());
			datosConfirmacion.add(registro);

			//Nombre de servicio
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_service));
			registro.add(pagoServicio.getServicio());
			datosConfirmacion.add(registro);

			//Convenio
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_cieAgreement));
			registro.add(pagoServicio.getConvenioServicio());
			datosConfirmacion.add(registro);

			//Referencia
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_reference));
			registro.add(pagoServicio.getReferenciaServicio());
			datosConfirmacion.add(registro);

			//Concepto
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_reason));
			registro.add(pagoServicio.getConceptoServicio());
			datosConfirmacion.add(registro);

			ArrayList<Object> entryList = datosConfirmacion;
			for(int i = entryList.size() - 1; i >= 0; i--) {
				try {
					@SuppressWarnings("unchecked")
					ArrayList<String> entry = (ArrayList<String>)entryList.get(i);
					if(entry.get(1).trim().equals(Constants.EMPTY_STRING))
						entryList.remove(i);
				} catch(Exception ex) {
					if(Server.ALLOW_LOG) Log.i(getClass().getSimpleName(), "Error al quitar el elemento.", ex);
				}
			}
		} else {

			//Cuenta de retiro
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_account));
			registro.add(Tools.hideAccountNumber(pagoServicio.getCuentaOrigen().getNumber()));
			datosConfirmacion.add(registro);
			//Nombre de servicio
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_service));
			registro.add(pagoServicio.getServicio());
			datosConfirmacion.add(registro);
			//Convenio
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_cieAgreement));
			registro.add(pagoServicio.getConvenioServicio());
			datosConfirmacion.add(registro);
			//Referencia
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_reference));
			registro.add(pagoServicio.getReferenciaServicio());
			datosConfirmacion.add(registro);
			//Concepto
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_reason));
			registro.add(pagoServicio.getConceptoServicio());
			datosConfirmacion.add(registro);
			//Importe
//			if (!preregister) {
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_amount));
			registro.add(Tools.formatAmount(pagoServicio.getImporte(), false));
			datosConfirmacion.add(registro);
//			}

		}

		return datosConfirmacion;
	}

	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		ArrayList<Object> datosResultados = new ArrayList<Object>();
		ArrayList<String> registro;

		if (bajaFrecuente) {

			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.altafrecuente_nombrecorto));
			registro.add(pagoServicio.getAliasFrecuente());
			datosResultados.add(registro);

			//Nombre de servicio
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_service));
			registro.add(pagoServicio.getServicio());
			datosResultados.add(registro);
			//Convenio
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_cieAgreement));
			registro.add(pagoServicio.getConvenioServicio());
			datosResultados.add(registro);
			//Referencia
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_reference));
			registro.add(pagoServicio.getReferenciaServicio());
			datosResultados.add(registro);
			//Concepto
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_reason));
			registro.add(pagoServicio.getConceptoServicio());
			datosResultados.add(registro);

		} else {

			// Cuenta de retiro
			registro = new ArrayList<String>();
			registro.add(controladorPagos
					.getString(R.string.servicesPayment_account));
			registro.add(Tools.hideAccountNumber(pagoServicio.getCuentaOrigen()
					.getNumber()));
			datosResultados.add(registro);
			// Nombre de servicio
			registro = new ArrayList<String>();
			registro.add(controladorPagos
					.getString(R.string.servicesPayment_service));
			registro.add(pagoServicio.getServicio());
			datosResultados.add(registro);
			// Convenio
			registro = new ArrayList<String>();
			registro.add(controladorPagos
					.getString(R.string.servicesPayment_cieAgreement));
			registro.add(pagoServicio.getConvenioServicio());
			datosResultados.add(registro);
			// Referencia
			registro = new ArrayList<String>();
			registro.add(controladorPagos
					.getString(R.string.servicesPayment_reference));
			registro.add(pagoServicio.getReferenciaServicio());
			datosResultados.add(registro);
			// Concepto
			registro = new ArrayList<String>();
			registro.add(controladorPagos
					.getString(R.string.servicesPayment_reason));
			registro.add(pagoServicio.getConceptoServicio());
			datosResultados.add(registro);

//			if (!preregister) {
			//Importe
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.servicesPayment_amount));
			registro.add(Tools.formatAmount(pagoServicio.getImporte(),false));
			datosResultados.add(registro);
			//fecha
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.transferir_interbancario_tabla_resultados_fechaoperacion));
			registro.add(Tools.formatDate(result.getFecha()));
			datosResultados.add(registro);
			//hora
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.transferir_interbancario_tabla_resultados_horaoperacion));
			registro.add(Tools.formatTime(result.getHora()));
			datosResultados.add(registro);
			//folio
			registro = new ArrayList<String>();
			registro.add(controladorPagos.getString(R.string.transferir_interbancario_tabla_resultados_folio));
			registro.add(result.getFolio());//agregar format day
			datosResultados.add(registro);
//			}
			//alias frecuente
			//aliasFrecuente = transferenciaOtrosBBVA.getAliasFrecuente() != null && !transferenciaOtrosBBVA.getAliasFrecuente().isEmpty() ? transferenciaOtrosBBVA.getAliasFrecuente() : aliasFrecuente;
			if(aliasFrecuente != null && !aliasFrecuente.isEmpty()){
				registro = new ArrayList<String>();
				registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_alias_frecuente));
				registro.add(aliasFrecuente);
				datosResultados.add(registro);
			}

			//correo frecuente
			//correoFrecuente = transferenciaOtrosBBVA.getCorreoFrecuente() != null && !transferenciaOtrosBBVA.getCorreoFrecuente().isEmpty() ? transferenciaOtrosBBVA.getCorreoFrecuente() : correoFrecuente;
			if(correoFrecuente !=null && !correoFrecuente.isEmpty()){
				registro = new ArrayList<String>();
				registro.add(SuiteApp.appContext.getString(R.string.transferir_interbancario_tabla_resultados_correo_electronico_frecuente));
				registro.add(correoFrecuente);
				datosResultados.add(registro);
			}
			correoFrecuente = pagoServicio.getCorreoFrecuente() != null && !pagoServicio.getCorreoFrecuente().isEmpty() ? pagoServicio.getCorreoFrecuente() : correoFrecuente;

			Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
			if ((profile != Constants.Perfil.recortado)) {
				Session session = Session.getInstance(SuiteApp.appContext);
				Hashtable<String, String> paramTable = new Hashtable<String, String>();
				paramTable.put("numeroCelular", session.getUsername());
				paramTable.put("convenio", pagoServicio.getConvenioServicio());
				paramTable.put("referencia", pagoServicio.getReferenciaServicio());
				//doNetworkOperation(ApiConstants.OP_CONSULTA_PAYMENT_SERVICES, paramTable, true, null, isJsonValueCode.NONE, controladorPagos);
			}

		}

		return datosResultados;
	}

	@Override
	public ArrayList<Object> getDatosRegistroOp() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> registro;

		registro = new ArrayList<String>();
		registro.add(controladorPagos.getString(R.string.servicesPayment_account));
		registro.add(Tools.hideAccountNumber(pagoServicio.getCuentaOrigen().getNumber()));
		tabla.add(registro);
		//Nombre de servicio
		registro = new ArrayList<String>();
		registro.add(controladorPagos.getString(R.string.servicesPayment_service));
		registro.add(pagoServicio.getServicio());
		tabla.add(registro);
		//Convenio
		registro = new ArrayList<String>();
		registro.add(controladorPagos.getString(R.string.servicesPayment_cieAgreement));
		registro.add(pagoServicio.getConvenioServicio());
		tabla.add(registro);
		//Referencia
		registro = new ArrayList<String>();
		registro.add(controladorPagos.getString(R.string.servicesPayment_reference));
		registro.add(pagoServicio.getReferenciaServicio());
		tabla.add(registro);
		//Concepto
		registro = new ArrayList<String>();
		registro.add(controladorPagos.getString(R.string.servicesPayment_reason));
		registro.add(pagoServicio.getConceptoServicio());
		tabla.add(registro);

		return tabla;
	}

	@Override
	public ArrayList<Object> getDatosRegistroExitoso() {ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> registro;

		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_op));
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_exito));
		tabla.add(registro);

		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.servicesPayment_service));
		registro.add(pagoServicio.getServicio());
		tabla.add(registro);

		return tabla;
	}

	@Override
	public String getTextoTituloConfirmacion() {
		return controladorPagos.getString(R.string.confirmation_subtitulo);
	}

	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.icono_pagar_servicios;
	}

	@Override
	public int getTextoEncabezado() {
		return R.string.servicesPayment_title;
	}

	@Override
	public String getTextoTituloResultado() {
		/*if (preregister) {
			return SuiteApp.getInstance().getString(R.string.servicesPayment_registro_exitosaTitle);
		} else */if (bajaFrecuente) {
			return SuiteApp.getInstance().getString(R.string.baja_frecuentes_titulo_tabla_resultados);
		} else {
			return SuiteApp.getInstance().getString(R.string.servicesPayment_operacion_exitosaTitle);
		}
	}

	@Override
	public int getColorTituloResultado() {
		if (bajaFrecuente) {
			return R.color.magenta;
		} else {
			return R.color.verde_limon;
		}
	}

	@Override
	public String getTextoEspecialResultados() {
//		if (preregister) {
//			return SuiteApp.appContext.getString(R.string.servicesPayment_preregisterResultText);
//		} else {
		return super.getTextoEspecialResultados();
//		}
	}

	@Override
	public String getTextoAyudaResultados() {
		if (bajaFrecuente) {
			return super.getTextoAyudaResultados();
		} else {
//   			if (preregister)
//   				return "";
			return SuiteApp.appContext.getString(R.string.transferir_interbancario_texto_ayuda_resultados);
		}
	}

	@Override
	public String getTextoSMS() {
		String from = SuiteApp.appContext.getString(R.string.smsText_firstFrom);
		String to = SuiteApp.appContext.getString(R.string.smsText_servicePayment_To);
		String by = SuiteApp.appContext.getString(R.string.smsText_byQuantity);
		String folio = SuiteApp.appContext.getString(R.string.smsText_folio);
		String accountNumber = pagoServicio.getCuentaOrigen().getNumber();
		accountNumber = accountNumber.substring(accountNumber.length()-4, accountNumber.length());

		StringBuilder smsText = new StringBuilder(from);
		smsText.append(" ");
		smsText.append(pagoServicio.getCuentaOrigen().getNombreTipoCuenta(SuiteApp.appContext.getResources()));
		smsText.append(" ");
		smsText.append(accountNumber);
		smsText.append(" ");
		smsText.append(to);
		smsText.append(" ");
		smsText.append(pagoServicio.getConvenioServicio());
		smsText.append(" ");
		smsText.append(by);
		smsText.append(" ");
		smsText.append(Tools.formatAmount(pagoServicio.getImporte(), false));
		smsText.append(" ");
		smsText.append(folio);
		smsText.append(" ");
		smsText.append(result.getFolio());
		smsText.append(" ");
		smsText.append(Tools.formatDate(result.getFecha()).toString() + Tools.formatTime(result.getHora()));
		smsText.append("");

		return smsText.toString();
	}

	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
//			if (preregister) {
//				tipoOTP = Autenticacion.getInstance().tokenAMostrar(this.tipoOperacion,
//						Session.getInstance(SuiteApp.appContext).getClientProfile());
//			} else {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile(),
					Tools.getDoubleAmountFromServerString(pagoServicio.getImporte()));
//			}

		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.mostrarNIP execution.", ex);
			tipoOTP = null;
		}

		return tipoOTP;
	}

	@Override
	public boolean mostrarNIP() {
		boolean value;
//		if (preregister) {
//			value =  Autenticacion.getInstance().mostrarNIP(this.tipoOperacion,
//					Session.getInstance(SuiteApp.appContext).getClientProfile());
//		} else {
		value =  Autenticacion.getInstance().mostrarNIP(this.tipoOperacion,
				Session.getInstance(SuiteApp.appContext).getClientProfile(),
				Tools.getDoubleAmountFromServerString(pagoServicio.getImporte()));
//		}

		return value;
	}

	@Override
	public boolean mostrarContrasenia() {
		boolean value;
		if (/*preregister || */bajaFrecuente) {
			value =  Autenticacion.getInstance().mostrarContrasena(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile());
		} else {
			value =  Autenticacion.getInstance().mostrarContrasena(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile(),
					Tools.getDoubleAmountFromServerString(pagoServicio.getImporte()));
		}
		return value;
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (ServerResponse.OPERATION_SUCCESSFUL == response.getStatus()) {
			if (operationId == Server.OP_SOLICITAR_ALERTAS) {

				setSa((SolicitarAlertasData) response.getResponse());
				analyzeAlertasRecortado();
				return;

			} else if (operationId == Server.CONSULTA_CIE) {
				NombreCIEData nombre = (NombreCIEData) response.getResponse();
				pagoServicio.setServicio(nombre.getNombreCIE());
				showConfirmacion();
			} else if (operationId == Server.HELP_IMAGE_OPERATION) {
				Bitmap image = null;
				Object r = response.getResponse();
				if (controladorPagos instanceof PagoServiciosAyudaViewController) {
					if (r == null) {
						((PagoServiciosAyudaViewController) controladorPagos).showUnfoundImage();
					} else if (r instanceof Bitmap) {
						image = (Bitmap) r;
						((PagoServiciosAyudaViewController) controladorPagos).showData(image);
					}
				}
			} else if (operationId == Server.CONSULTAR_CODIGO_PAGO_SERVICIOS) {
				PagoServicio ps = (PagoServicio) response.getResponse();
				pagoServicio.setReferenciaServicio(ps.getReferenciaServicio());
				pagoServicio.setConceptoServicio(ps.getConceptoServicio());
				pagoServicio.setImporte(ps.getImporte());

				((PagoServiciosViewController) controladorPagos).llenaCamposConCodigo(pagoServicio.getReferenciaServicio(),
						pagoServicio.getConceptoServicio(),
						pagoServicio.getImporte());
			} else if (operationId == Server.SERVICE_PAYMENT_OPERATION) {
				result = (PagoServicioResult) response.getResponse();
				if (result.getResultType() == PagoServicioResult.RESULT_PAYMENT) {
					Session.getInstance(SuiteApp.appContext).actualizaMonto(pagoServicio.getCuentaOrigen(),
							result.getNuevoSaldo());
				}
				((BmovilViewsController) controladorPagos.getParentViewsController()).showResultadosViewController(this, 0, 0);
				controladorPagos.getParentViewsController().removeDelegateFromHashMap(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID);
			} else if (operationId == Server.PREREGISTRAR_PAGO_SERVICIOS) {
				result = (PagoServicioResult) response.getResponse();
				((BmovilViewsController) controladorPagos.getParentViewsController()).showResultadosViewController(this, 0, 0);
				controladorPagos.getParentViewsController().removeDelegateFromHashMap(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID);
			} else if (operationId == Server.FAVORITE_PAYMENT_OPERATION) {
				if (response.getResponse() instanceof PaymentExtract) {
					PaymentExtract extract = (PaymentExtract) response.getResponse();
					setFrecuentes(extract.getPayments());
					mostrarListaFrecuentes();
				}
			} else if(operationId == Server.OP_VALIDAR_CREDENCIALES){
				showConfirmacionRegistro();
				return;
			} else if (operationId == Server.BAJA_FRECUENTE) {
				((BmovilViewsController) controladorPagos.getParentViewsController()).showResultadosViewController(this, -1, -1);
			} /*else if (operationId == ApiConstants.OP_CONSULTA_PAYMENT_SERVICES) {
				mostrarPopup("altaPaymentServices");
			} else if (operationId == ApiConstants.OP_ALTA_PAYMENT_SERVICES) {
				mostrarPopup("confirmacionPaymentServices");
			} else if (ServerResponse.OPERATION_ERROR == response.getStatus()) {

				if ((Constants.CODE_CNE0234.equals(response.getMessageCode()))
						|| (Constants.CODE_CNE0235.equals(response.getMessageCode()))
						|| (Constants.CODE_CNE0236.equals(response.getMessageCode()))) {

					Session session = Session.getInstance(SuiteApp.appContext);
					Constants.Perfil perfil = session.getClientProfile();

					if (Constants.Perfil.recortado.equals(perfil)) {
						setTransferErrorResponse(response);
						solicitarAlertas(controladorPagos);
					}

				} else if (operationId != Server.FAVORITE_PAYMENT_OPERATION && operationId != ApiConstants.OP_CONSULTA_PAYMENT_SERVICES && operationId != ApiConstants.OP_ALTA_PAYMENT_SERVICES) {
					if (null != controladorPagos) {
						controladorPagos.showInformationAlert(response.getMessageText());
					}
				}
			}*/
		}
	}
	/*
	 * Alta frecuentes de pago de servicios
	 */
	@Override
	public Hashtable<String, String> getParametrosAltaFrecuentes() {
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String,String> paramTable = null;
		paramTable = new Hashtable<String, String>();
		paramTable.put("NT", session.getUsername());
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());
		paramTable.put("TE", session.getClientNumber());
		paramTable.put("CA", pagoServicio.getConvenioServicio());
		paramTable.put("BF", pagoServicio.getServicio());
		paramTable.put("IM", pagoServicio.getImporte());
		if(pagoServicio.isPreregistered()){
			//CÛdigo Karen
			//String usuario = pagoServicio.getUsuario();
			//paramTable.put(Server.USUARIO, pagoServicio.getUsuario());
			paramTable.put("RF", pagoServicio.getReferenciaServicio());
			paramTable.put("C1", pagoServicio.getUsuario());
		}else{
			paramTable.put("RF", pagoServicio.getReferenciaServicio());
			paramTable.put("CP", pagoServicio.getConceptoServicio());
			paramTable.put("TP", Constants.TPPagoServicios);
			paramTable.put("OA", "");
		}
		return paramTable;
	}

	@Override
	public ArrayList<Object> getDatosTablaAltaFrecuentes() {
		ArrayList<Object> datosResultados = new ArrayList<Object>();
		ArrayList<String> registro;
		// Cuenta de retiro
		registro = new ArrayList<String>();
		registro.add(controladorPagos
				.getString(R.string.servicesPayment_account));
		registro.add(Tools.hideAccountNumber(pagoServicio.getCuentaOrigen()
				.getNumber()));
		datosResultados.add(registro);
		// Nombre de servicio
		registro = new ArrayList<String>();
		registro.add(controladorPagos
				.getString(R.string.servicesPayment_service));
		registro.add(pagoServicio.getServicio());
		datosResultados.add(registro);
		// Convenio
		registro = new ArrayList<String>();
		registro.add(controladorPagos
				.getString(R.string.servicesPayment_cieAgreement));
		registro.add(pagoServicio.getConvenioServicio());
		datosResultados.add(registro);
		// Referencia
		registro = new ArrayList<String>();
		registro.add(controladorPagos
				.getString(R.string.servicesPayment_reference));
		registro.add(pagoServicio.getReferenciaServicio());
		datosResultados.add(registro);
		// Concepto
		registro = new ArrayList<String>();
		registro.add(controladorPagos
				.getString(R.string.servicesPayment_reason));
		registro.add(pagoServicio.getConceptoServicio());
		datosResultados.add(registro);

		return datosResultados;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void eliminarFrecuente(int indexSelected) {
		ArrayList<Object> arlPayments = null;
		if (controladorPagos instanceof ConsultarFrecuentesViewController) {
			arlPayments = ((ConsultarFrecuentesViewController)controladorPagos).getListaFrecuentes().getLista();
		}

		Payment selectedPayment = frecuentes[indexSelected];
		if (arlPayments != null && arlPayments.size() > 0) {
			selectedPayment = (Payment)((ArrayList<Object>)arlPayments.get(indexSelected)).get(0);
		}
		this.selectedPayment = selectedPayment;
		ArrayList<Account> cuentas = cargaCuentasOrigen();
		Account cuentaOrigen = null ;
		if(cuentas.size() > 0)
			cuentaOrigen = cuentas.get(0);

		this.pagoServicio = new PagoServicio(cuentaOrigen,
				selectedPayment.getBeneficiaryAccount(),
				selectedPayment.getBeneficiary(),
				selectedPayment.getReference(),
				selectedPayment.getConcept(),
				selectedPayment.getAmount(),
				selectedPayment.getNickname(),
				"",
				selectedPayment.getUsuario());
		pagoServicio.setIdCanal(selectedPayment.getIdCanal());
		pagoServicio.setUsuario(selectedPayment.getUsuario());
		isFrecuente = false;
//		   preregister = false;
		setBajaFrecuente(true);
		setTipoOperacion(Constants.Operacion.bajaFrecuente);
		showConfirmacionAutenticacion();
	}

	public void showConfirmacionAutenticacion(){
		BmovilViewsController bmovilParentController = ((BmovilViewsController)controladorPagos.getParentViewsController());
		bmovilParentController.showConfirmacionAutenticacionViewController(this, getNombreImagenEncabezado(),
				getTextoEncabezado(),
				R.string.confirmation_subtitulo);
	}

	@Override
	public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController,String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
		int operacion = 0;
		Hashtable<String,String> paramTable = new Hashtable<String, String>();
		Session session = Session.getInstance(SuiteApp.appContext);

		operacion = Server.BAJA_FRECUENTE;
		paramTable = new Hashtable<String, String>();

		paramTable.put(Server.USERNAME_PARAM, session.getUsername());//"NT"
		paramTable.put(Server.PASSWORD_PARAM, Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia );//NP
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());//IU
		paramTable.put(Server.NUMERO_CLIENTE_PARAM, session.getClientNumber());//TE
		paramTable.put(Server.DESCRIPCION_PARAM, pagoServicio.getAliasFrecuente());//DE
		paramTable.put(Server.PAYMENT_ACCOUNT_PARAM, pagoServicio.getConvenioServicio());//CA
//		paramTable.put(Server.BENEFICIARIO_PARAM, compraTiempoAire.getNombreCompania());//BF
		paramTable.put(Server.REFERENCIA_NUMERICA_PARAM, pagoServicio.getReferenciaServicio());//RF
//		String idToken = (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) ? "T" : "";
		paramTable.put(Server.ID_TOKEN, selectedPayment.getIdToken());
		paramTable.put(Server.TIPO_CONSULTA_PARAM, Constants.TPPagoServicios);//TP
		String va =  Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,
				Session.getInstance(SuiteApp.appContext).getClientProfile());
		paramTable.put(Server.VA_PARAM,Tools.isEmptyOrNull(va)?"":va);//validar
		paramTable.put(Server.ID_CANAL, pagoServicio.getIdCanal());
		paramTable.put(Server.USUARIO, pagoServicio.getUsuario());
		Hashtable<String, String> paramTable2 = new Hashtable<String, String>();
		paramTable2= ServerMapperUtil.mapperBaja(paramTable);
		doNetworkOperation(operacion, paramTable2,false,null,isJsonValueCode.NONE, confirmacionAutenticacionViewController);

	}

	public int getImagenBotonPago() {
		int resourceImage = 0;
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
		if(profile == Constants.Perfil.avanzado){
			resourceImage = R.drawable.btn_registro_nuevo_servicio;
		}else{
			resourceImage = R.drawable.btn_pagar_nuevo_servicio;
		}
		return resourceImage;
	}

/*	public void mostrarPopup(String popUp) {

		final ResultadosDelegate aDelegate = (ResultadosDelegate) controladorPagos.getParentViewsController().getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		final AlertDialog mAlertDialog;
		AlertDialog.Builder builder = new AlertDialog.Builder(aDelegate.getResultadosViewController());
		LayoutInflater inflater = controladorPagos.getLayoutInflater();
		builder.setCancelable(false);
		mAlertDialog = builder.create();

		if(popUp.equals("altaPaymentServices"))
		{
			View vista = inflater.inflate(R.layout.activity_alta_payment_services, null);
			ImageButton btnCancel = (ImageButton) vista.findViewById(R.id.imgCancelar);
			Button btnAceptar = (Button) vista.findViewById(R.id.btnAceptar);

			mAlertDialog.setView(vista, 0, 0, 0, 0);

			btnCancel.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mAlertDialog.dismiss();
				}
			});

			btnAceptar.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mAlertDialog.dismiss();

					Session session = Session.getInstance(SuiteApp.appContext);
					Hashtable<String, String> paramTable = new Hashtable<String, String>();

					paramTable.put("numeroCelular", session.getUsername());
					paramTable.put("convenio", pagoServicio.getConvenioServicio());
					paramTable.put("referencia", pagoServicio.getReferenciaServicio());
					paramTable.put("cuentaEje", Tools.obtenerCuentaEje().getNumber());
					doNetworkOperation(ApiConstants.OP_ALTA_PAYMENT_SERVICES, paramTable, true, null, isJsonValueCode.NONE, controladorPagos);
				}

			});

			mAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					aDelegate.getResultadosViewController().setHabilitado(true);
				}
			});

		} else if(popUp.equals("confirmacionPaymentServices")){

			View vista = inflater.inflate(R.layout.activity_confirmacion_payment_services, null);
			Button btnAceptar = (Button) vista.findViewById(R.id.btnAceptar);

			mAlertDialog.setView(vista, 0, 0, 0, 0);

			btnAceptar.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					mAlertDialog.dismiss();
				}
			});

			mAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					aDelegate.getResultadosViewController().setHabilitado(true);
				}
			});
		}
		mAlertDialog.show();
	}*/
}
