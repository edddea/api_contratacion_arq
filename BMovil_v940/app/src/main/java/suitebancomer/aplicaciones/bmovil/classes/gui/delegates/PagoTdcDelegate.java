package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.sql.DataTruncation;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Hashtable;

import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MisCuentasViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.PagoTdcViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.SeleccionaTdcViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.TransferMisCuentasDetalleViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.TransferirMisCuentasSeleccionViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Comision;
import suitebancomer.aplicaciones.bmovil.classes.model.ImportesTDCData;
import suitebancomer.aplicaciones.bmovil.classes.model.MovementExtract;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaInterna;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferirCuentasPropiasData;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;

public class PagoTdcDelegate extends DelegateBaseOperacion {
	
	public class PagoDeTarjeta{
		private String numeroCelular;
		private String IUM;
		private Account cuentaRetiro;
		private Account cuentaDeposito;
		private String importe;
		private String fechaPago;
		
		private String fechaCorte;
		
		
		public PagoDeTarjeta() {

			this.numeroCelular = Session.getInstance(SuiteApp.appContext).getUsername();
			this.IUM = Session.getInstance(SuiteApp.appContext).getIum();
			this.fechaPago = Tools.formatDate(Session.getInstance(SuiteApp.appContext).getServerDate().getTime());
		}
		public String getNumeroCelular() {
			return numeroCelular;
		}
		public void setNumeroCelular(String numeroCelular) {
			this.numeroCelular = numeroCelular;
		}
		public String getIUM() {
			return IUM;
		}
		public void setIUM(String iUM) {
			IUM = iUM;
		}
		public Account getCuentaRetiro() {
			return cuentaRetiro;
		}
		public void setCuentaRetiro(Account cuentaRetiro) {
			this.cuentaRetiro = cuentaRetiro;
		}
		public Account getCuentaDeposito() {
			return cuentaDeposito;
		}
		public void setCuentaDeposito(Account cuentaDeposito) {
			this.cuentaDeposito = cuentaDeposito;
		}
		public String getImporte() {
			return importe;
		}
		public void setImporte(String importe) {
			this.importe = importe;
		}
		public String getFechaPago() {
			return fechaPago;
		}
		public void setFechaPago(String fechaPago) {
			this.fechaPago = fechaPago;
		}
		
		public String getFechaCorte() {
			return fechaCorte;
		}
		public void setFechaCorte(String fechaCorte) {
			this.fechaCorte = fechaCorte;
		}
		
		
		
		
	}
	
	/********************************************************* Variables *******************************************************/
	public final static long PAGO_TDC_DELEGATE_ID = 4L;
	private BaseViewController pagoTdcViewController;
	
	private Account cuentaTDC;
	private ImportesTDCData resImpTDC;
	
	private PagoDeTarjeta dataTDC = new PagoDeTarjeta();
	private Constants.Operacion operacion = Constants.Operacion.transferirPropias;
	private Constants.Operacion operacionAutenticacion = Constants.Operacion.pagarTarjetaCredito;

	
	private TransferirCuentasPropiasData resultado;
	
	private Account cuentaOrigenSeleccionada = null;
	
	/********************************************************* End Variables *******************************************************/
	
	/********************************************************* Getters&Setters *******************************************************/
	
	public Account getCuentaOrigenSeleccionada() {
		return cuentaOrigenSeleccionada;
	}

	public void setCuentaOrigenSeleccionada(Account cuentaOrigenSeleccionada) {
		this.cuentaOrigenSeleccionada = cuentaOrigenSeleccionada;
	}

	//Metodos para envio de correo
	
	public String getImporte (){//creacion del objeto para obtener los datos de importe
			
			return dataTDC.getImporte();
		}
		
	public String getCuentaRetiro (){//creacion del objeto para obtener los datos de la cuenta de retiro
		
			
			return dataTDC.getCuentaRetiro().getNumber();
		}
	public TransferirCuentasPropiasData getResultado() {
		return resultado;
	}
	

	public PagoDeTarjeta getdataTDC() {
		return dataTDC;
	}

	public void setdataTDC(PagoDeTarjeta dataTDC) {
		this.dataTDC = dataTDC;
	}
	
	public ImportesTDCData getResImpTDC() {
		return resImpTDC;
	}

	public void setResImpTDC(ImportesTDCData resImpTDC) {
		this.resImpTDC = resImpTDC;
	}

	public Account getCuentaTDC() {
		return cuentaTDC;
	}

	public void setCuentaTDC(Account cuentaTDC) {
		this.cuentaTDC = cuentaTDC;
	}

	public void setCuentaActual(Account cuenta){
		cuentaTDC = cuenta;
	}
	
	public BaseViewController getPagoTdcViewController() {
		return pagoTdcViewController;
	}

	public void setPagoTdcViewController(BaseViewController pagoTdcViewController) {
		this.pagoTdcViewController = pagoTdcViewController;
	}
	
	/********************************************************* End Getters&Setters *******************************************************/
	
	public void setDataTdcAcc(){
		

		
		dataTDC.setCuentaDeposito(this.getCuentaTDC());
		dataTDC.setCuentaRetiro(((PagoTdcViewController)pagoTdcViewController).getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada());
		dataTDC.setImporte(((PagoTdcViewController)pagoTdcViewController).getImporteRaw());
	}
	
	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada seg�n sea reguerido.
	 * @return
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		Boolean cuentaEjeTDC = true;
		

				
		/*if(profile == Constants.Perfil.basico) {
			
			for(Account acc : accounts) {
				if(acc.isVisible() && (!acc.getType().equals(Constants.CREDIT_TYPE))){
					accountsArray.add(acc);
					cuentaEjeTDC = false;
				}
			}
			
		}else{
			
			for(Account acc : accounts) {
				if(acc.isVisible() && (!acc.getType().equals(Constants.CREDIT_TYPE))) {
					accountsArray.add(acc);
					cuentaEjeTDC = false;
					break;// wow
				}
			}
			
			for(Account acc : accounts) {
				if(!acc.isVisible()  && (!acc.getType().equals(Constants.CREDIT_TYPE)))
					accountsArray.add(acc);
			}
			
		}  */

		if(profile == Constants.Perfil.avanzado) {
//			if(otrosBBVAViewController instanceof InterbancariosViewController) {
			for(Account acc : accounts) {
				if(acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE)) {
					accountsArray.add(acc);
					break;
				}
			}

			for(Account acc : accounts) {
				if(!acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE))
					accountsArray.add(acc);
			}
			//	}

		} else {
			for(Account acc : accounts) {
				if(acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE))
					accountsArray.add(acc);
			}
		}
		// TODO EVH REVISAR
		//frecuente con correo
		//if(  (Tools.obtenerCuentaEje().getType().equals(Constants.CREDIT_TYPE))){ //cuentaEjeTDC) ){
		if( ( (profile == Constants.Perfil.basico) || (accounts.length == 1) ) && (Tools.obtenerCuentaEje().getType().equals(Constants.CREDIT_TYPE))){ //cuentaEjeTDC) ){

			pagoTdcViewController.showInformationAlert(R.string.error_cuenta_eje_credito);
		}
		
		return accountsArray;
	}
	
	public void consultaImportesTDC() {
		
		pagoTdcViewController.muestraIndicadorActividad(
				pagoTdcViewController.getString(R.string.alert_operation),
				pagoTdcViewController.getString(R.string.alert_connecting));
		Session session = Session.getInstance(SuiteApp.appContext);
		
		//prepare data
        Hashtable<String,String> paramTable = new Hashtable<String,String>();       
        
        //paramTable.put(ServerConstants.OPERACION, Server.OPERATION_CODES[Server.OP_CONSULTA_TDC]);
        paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());//cuentaActual.getCelularAsociado());
        paramTable.put(ServerConstants.TIPO_CUENTA,"TC");
        paramTable.put(ServerConstants.NUMERO_TARJETA,cuentaTDC.getNumber());
        paramTable.put(ServerConstants.PERIODO, "1");
        
		doNetworkOperation(Server.OP_CONSULTA_TDC, paramTable,true,new ImportesTDCData(),isJsonValueCode.NONE, pagoTdcViewController);
	}
	
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if(operationId == Server.OP_CONSULTA_TDC){
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
//				Object parsedres = response.getResponse().toString().replaceAll("/^<.>$/", "");
				resImpTDC = (ImportesTDCData)response.getResponse();
				
				((PagoTdcViewController)pagoTdcViewController).setDataFromTDC();
				
				// Tratamos los combos para que se inicialicen según nuestra petición
				// @esError indica si se inicializan los combos si es una respuesta de error
				((PagoTdcViewController)pagoTdcViewController).setUpRadios(false);
			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING){
				
			}else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
//				pagoTdcViewController.showInformationAlertEspecial(pagoTdcViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
				((PagoTdcViewController)pagoTdcViewController).ocultaDatosTDC();
				
				if (((response.getMessageCode()!=null) && (!response.getMessageCode().equals("MPE1245")))
				
					||(response.getMessageCode()==null)){
						
				  // pagoTdcViewController.showInformationAlert(pagoTdcViewController.getString(R.string.label_error), "La información no se encuentra disponible por el momento, consulte mas tarde", null);

					((PagoTdcViewController)pagoTdcViewController).muestraErrorGenericoTDC();
				   
				}else {
	        		pagoTdcViewController.showInformationAlertEspecial(pagoTdcViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
				}
				
				// Tratamos los combos para que se inicialicen según nuestra petición
				// @esError indica si se inicializan los combos si es una respuesta de error
				((PagoTdcViewController)pagoTdcViewController).setUpRadios(true);
				
			}
			pagoTdcViewController.ocultaIndicadorActividad();
		}else if(operationId == Server.SELF_TRANSFER_OPERATION){
			Log.e("niko","entra en res");
			if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
				resultado = (TransferirCuentasPropiasData)response.getResponse();
				showResultados();
			}else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
				Session session = Session.getInstance(SuiteApp.appContext);
				Constants.Perfil perfil = session.getClientProfile();
				
				if(Constants.Perfil.recortado.equals(perfil)){
						
						setTransferErrorResponse(response);
						solicitarAlertas(pagoTdcViewController);

				}
			}
		}else if(operationId == Server.OP_SOLICITAR_ALERTAS){
			
			
				setSa((SolicitarAlertasData) response.getResponse());
				analyzeAlertasRecortado();
				return;
			
//				resultado = (TransferirCuentasPropiasData)response.getResponse();
//				showResultados();
		}
	}
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {
		((BmovilViewsController)caller.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
	}
	

	
	/*****/
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		TipoOtpAutenticacion value = null;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		value = Autenticacion.getInstance().tokenAMostrar(this.operacion, perfil,Tools.getDoubleAmountFromServerString(((PagoTdcViewController)pagoTdcViewController).getImporteRaw()));
		
		return value;
	}

	@Override
	public boolean mostrarNIP() {
		

		
		boolean mostrar = false;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		mostrar = Autenticacion.getInstance().mostrarNIP(this.operacion, perfil,Tools.getDoubleAmountFromServerString(((PagoTdcViewController)pagoTdcViewController).getImporteRaw()));
		
		return mostrar;
	}

	@Override
	public boolean mostrarContrasenia() {
		boolean mostrar = false;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		mostrar = Autenticacion.getInstance().mostrarContrasena(this.operacion, perfil,Tools.getDoubleAmountFromServerString(((PagoTdcViewController)pagoTdcViewController).getImporteRaw()));
		
		return mostrar;
	}
	
	
	
	
	@Override
	public boolean mostrarCVV() {
		boolean mostrar = false;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		mostrar = Autenticacion.getInstance().mostrarCVV(this.operacion, perfil,Tools.getDoubleAmountFromServerString(((PagoTdcViewController)pagoTdcViewController).getImporteRaw()));
		
		return mostrar;
	}
	
	
	

	
	
	
	public void showConfirmacion() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacion(this);
	}
	
	public void showResultados() {
		this.actualizarMontoCuentas();
		
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showResultadosViewController(this, getNombreImagenEncabezado(), getTextoEncabezado());
	}
	
	@Override
	public long getDelegateIdentifier() {
		// TODO Auto-generated method stub
		return super.getDelegateIdentifier();
	}
	

	/**
	 * Actualiza los montos de las cuentas de origen y destino.
	 */
	@SuppressWarnings("unchecked")
	public void actualizarMontoCuentas() {
		ArrayList<String> datosCuentaOrigen;
		ArrayList<String> datosCuentaDestino;
		
		try {
			datosCuentaOrigen = (ArrayList<String>)resultado.getAccountList().get(Constants.SELF_TRANSFER_ORIGIN_ACCOUNT_INDEX);
			datosCuentaDestino = (ArrayList<String>)resultado.getAccountList().get(Constants.SELF_TRANSFER_DESTINATION_ACCOUNT_INDEX);
			
			Session.getInstance(SuiteApp.appContext).actualizaMonto(getCuentaOrigen(), 
																	datosCuentaOrigen.get(Constants.SELF_TRANSFER_ACCOUNT_BALANCE_INDEX));
			Session.getInstance(SuiteApp.appContext).actualizaMonto(dataTDC.cuentaDeposito, 
																	datosCuentaDestino.get(Constants.SELF_TRANSFER_ACCOUNT_BALANCE_INDEX));
		} catch(Exception ex) {
			Log.e("actualizarMontoCuentas", "Error al interpretar los datos de cuentas del servidor.", ex);
		}
	}
	
	private Account getCuentaOrigen(){
		return ((PagoTdcViewController)pagoTdcViewController).getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada();
	}
	
	@Override
	public void realizaOperacion(
			ConfirmacionViewController confirmacionViewController,
			String contrasenia, String nip, String token, String campoTarjeta) {
//		this.ownerController = confirmacionViewController;

		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		Session session = Session.getInstance(SuiteApp.appContext);

		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
		
		paramTable.put(ServerConstants.PARAMS_TEXTO_NP, (null == contrasenia) ? ""
				: contrasenia);
		
		paramTable.put(ServerConstants.IUM_ETIQUETA, session.getIum());
		
		paramTable.put("CC", getCuentaOrigen().getType()
				+ getCuentaOrigen().getNumber());
		paramTable.put("CA", Constants.CREDIT_TYPE + cuentaTDC.getNumber());
		
		DecimalFormat dc = new DecimalFormat("#.00");
		dc.setMaximumFractionDigits(2);
		//paramTable.put(Server.AMOUNT_PARAM,
				//Tools.formatAmountForServer( dc.format(Tools.getDoubleAmountFromServerString( dataTDC.getImporte() )) ));
		paramTable.put("IM",dataTDC.getImporte());
		if(Server.ALLOW_LOG) System.out.println("ImporteDelegate: "+dc.format(Tools.getDoubleAmountFromServerString( dataTDC.getImporte() )) );
		if(Server.ALLOW_LOG) System.out.println("ImporteDelegate sin formato: "+dataTDC.getImporte() );
		paramTable.put("TE", session.getClientNumber());
		
		paramTable.put("NI", (null == nip) ? "" : nip);
		
		paramTable.put("CV", "");
		paramTable.put("OT", (null == token) ? "" : token);
		
		paramTable.put("VA",
				Autenticacion.getInstance().getCadenaAutenticacion(
						this.operacion,
						Session.getInstance(SuiteApp.appContext)
								.getClientProfile(),
						Tools.getDoubleAmountFromServerString(dataTDC.getImporte())));
		//JAIG
		doNetworkOperation(Server.SELF_TRANSFER_OPERATION, paramTable, false,new TransferirCuentasPropiasData(),isJsonValueCode.NONE,
				confirmacionViewController);
		
	}
	
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
		

		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(pagoTdcViewController.getString(R.string.confirmation_lista_cuenta_origen));
		fila.add(Tools.hideAccountNumber(dataTDC.getCuentaRetiro().getNumber()));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(pagoTdcViewController.getString(R.string.confirmation_lista_cuenta_destino));
		fila.add(Tools.hideAccountNumber(dataTDC.getCuentaDeposito().getNumber()));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(pagoTdcViewController.getString(R.string.confirmation_lista_importe));
		fila.add(Tools.formatAmount(dataTDC.getImporte(),false));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(pagoTdcViewController.getString(R.string.confirmation_lista_fecha));
		fila.add(dataTDC.getFechaPago());
		tabla.add(fila);
		
		return tabla;
	}
	
	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		
	
		
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(pagoTdcViewController.getString(R.string.result_lista_cuenta_origen));
		fila.add(Tools.hideAccountNumber(dataTDC.getCuentaRetiro().getNumber()));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(pagoTdcViewController.getString(R.string.result_lista_cuenta_destino_abr));
		fila.add(Tools.hideAccountNumber(dataTDC.getCuentaDeposito().getNumber()));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(pagoTdcViewController.getString(R.string.result_lista_importe));
		fila.add(Tools.formatAmount(dataTDC.getImporte(),false));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(pagoTdcViewController.getString(R.string.result_lista_fecha));
		fila.add(Tools.formatDate(Tools.parseDateTime(resultado.getServerDate(), resultado.getServerTime())));
		tabla.add(fila);

		fila = new ArrayList<String>();
		fila.add(pagoTdcViewController.getString(R.string.result_lista_hora));
		fila.add(Tools.formatTime(resultado.getServerTime()));
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(pagoTdcViewController.getString(R.string.result_lista_folio));
		fila.add(resultado.getReference());
		tabla.add(fila);
		
		return tabla;
	}
	
	

	@Override
	public int getOpcionesMenuResultados() {
		return SHOW_MENU_SMS | SHOW_MENU_EMAIL;
	}
	
	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.icono_pagar_servicios;
	}

	@Override
	public int getTextoEncabezado() {
		return R.string.pago_tdc_title;
	}
	
	
	@Override
	public String getTextoTituloResultado() {
		return pagoTdcViewController.getString(R.string.transferir_detalle_operacion_exitosaTitle);

	}


	@Override
	public int getColorTituloResultado() {
		return R.color.verde_limon;
	}


	@Override
	public String getTextoAyudaResultados() {
		return "Para guardar esta información oprime el botón de opciones";
	}
	
	@Override
	public String getTextoEmail() {
		// TODO Auto-generated method stub
		//El correo se compondr� del contenido del componente �ListaDatosViewController�
		//mostrado en la pantalla de resultados.
		return super.getTextoEmail();
	}
	

	@Override
	public String getTextoSMS() {
		StringBuilder builder = new StringBuilder("Pago a ");
		builder.append(dataTDC.getCuentaDeposito().getType());
		builder.append(" ");
		builder.append(Tools.hideAccountNumber(dataTDC.getCuentaDeposito().getNumber()));		
		builder.append(" Fecha y hora devueltas en la operación ");
		builder.append(Tools.formatDate(resultado.getServerDate()));
		builder.append(" ");
		builder.append(Tools.formatTime(resultado.getServerTime()));
						
		return builder.toString();
	}



	/****/
	
	@Override
	public void performAction(Object obj) {
		if(pagoTdcViewController instanceof PagoTdcViewController){

			if(Server.ALLOW_LOG) Log.d("TransferirMisCuentasDelegate","Regrese de lista Seleccion");
			if(Server.ALLOW_LOG) 	Log.d("PagoTDCDelegate","Seleccion false");
			((PagoTdcViewController)pagoTdcViewController).getComponenteCtaOrigen().isSeleccionado();
			((PagoTdcViewController)pagoTdcViewController).muestraListaCuentas();
			((PagoTdcViewController)pagoTdcViewController).getComponenteCtaOrigen().setSeleccionado(false);
		}else if(pagoTdcViewController instanceof SeleccionaTdcViewController){
			if(Server.ALLOW_LOG) Log.d("PagoTDCDelegate","Seleccion true");
			if(((SeleccionaTdcViewController)pagoTdcViewController).componenteCtaOrigen.isSeleccionado()){
				((SeleccionaTdcViewController)pagoTdcViewController).muestraListaCuentas();
				((SeleccionaTdcViewController)pagoTdcViewController).componenteCtaOrigen.setSeleccionado(false);
			}else{
				((SeleccionaTdcViewController)pagoTdcViewController).opcionSeleccionada((Account)obj);
			}
		}
	}
	

}
