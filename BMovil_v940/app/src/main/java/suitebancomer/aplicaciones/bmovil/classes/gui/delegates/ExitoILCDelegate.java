package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;
import java.util.Hashtable;

import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaILC;
import suitebancomer.classes.gui.controllers.BaseViewController;

public class ExitoILCDelegate extends DelegateBaseOperacion {
	private BaseViewController viewController;
	AceptaOfertaILC aceptaiOfertaILC;
	OfertaILC ofertaILC;
	String fechaCat="";
	public static final long EXITO_OFERTA_DELEGATE = 631453334566765999L;


	public ExitoILCDelegate(AceptaOfertaILC aceptaOfertaILC, OfertaILC ofertaILC){
		this.aceptaiOfertaILC= aceptaOfertaILC;
		this.ofertaILC= ofertaILC;

	}
	
	public OfertaILC getOfertaILC() {
		return ofertaILC;
	}

	public void setOfertaILC(OfertaILC ofertaILC) {
		this.ofertaILC = ofertaILC;
	}

	public AceptaOfertaILC getAceptaiOfertaILC() {
		return aceptaiOfertaILC;
	}

	public void setAceptaiOfertaILC(AceptaOfertaILC aceptaiOfertaILC) {
		this.aceptaiOfertaILC = aceptaiOfertaILC;
	}

	@Override
	public int getOpcionesMenuResultados() {
			return SHOW_MENU_SMS | SHOW_MENU_EMAIL ;
	}
	
	public void realizaOperacion(int idOperacion,
			BaseViewController baseViewController, boolean isEmail, boolean isSMS){
		if(idOperacion == Server.EXITO_OFERTA){
			Session session = Session.getInstance(SuiteApp.appContext);
			String user = session.getUsername();
			String ium= session.getIum();
			Hashtable<String, String> params = new Hashtable<String, String>();
			//params.put("operacion", "exitoILC");
			params.put("numeroTarjeta",aceptaiOfertaILC.getNumeroTarjeta());
			String lineaActual= Tools.formatAmount(aceptaiOfertaILC.getLineaActual(),false);
			String lineaA=lineaActual.replace("$", "");
			params.put("lineaActual",lineaA);
			
			String lineaFinal= Tools.formatAmount(aceptaiOfertaILC.getLineaFinal(),false);
			String lineaF=lineaFinal.replace("$", "");
			params.put("lineaFinal",lineaF);
			params.put("numeroCelular",user);
			params.put("email",aceptaiOfertaILC.getEmail());
			params.put("folioInternet",aceptaiOfertaILC.getFolioInternet());
			params.put("Cat",ofertaILC.getCat());
			params.put("fechaCat",ofertaILC.getFechaCat());
			String mensajeA="";
			if(isEmail && isSMS){
				String importe= Tools.formatAmount(aceptaiOfertaILC.getImporte(),false);
				String imp=importe.replace("$", "");
				params.put("importe", imp);
				mensajeA="011";
			}
			else if(isEmail){
				String importe= Tools.formatAmount(aceptaiOfertaILC.getImporte(),false);
				String imp=importe.replace("$", "");
				params.put("importe", imp);
			
				mensajeA="010";
			}
			else if(isSMS){
				String importe= Tools.formatAmount(aceptaiOfertaILC.getImporte(),false);
				String imp=importe.replace("$", "");
				params.put("importe", imp);
				mensajeA="001";
			}
			
			
			params.put("mensajeA",mensajeA);
			params.put("IUM",ium);
			//JAIG
			doNetworkOperation(Server.EXITO_OFERTA, params,true,null,isJsonValueCode.ONECLICK,
					baseViewController);
		}else if(idOperacion == Server.CONSULTA_DETALLE_OFERTA_EFI){
			Session session = Session.getInstance(SuiteApp.appContext);
			String user = session.getUsername();
			String ium= session.getIum();		
			Hashtable<String, String> params = new Hashtable<String, String>();
			//params.put("operacion", "cDetalleOfertaBMovil");
			params.put("numeroCelular",user );
			params.put("cveCamp",aceptaiOfertaILC.getPromocion().getCveCamp());
			params.put("IUM", ium);
			//JAIG
			doNetworkOperation(Server.CONSULTA_DETALLE_OFERTA_EFI, params,true,new OfertaEFI(),isJsonValueCode.ONECLICK,
					baseViewController);
		}else if(idOperacion == Server.CONSULTA_DETALLE_OFERTA){
			Session session = Session.getInstance(SuiteApp.appContext);
			String user = session.getUsername();
			String ium= session.getIum();		
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("operacion", "cDetalleOfertaBMovil");
			params.put("numeroCelular",user );
			params.put("cveCamp",aceptaiOfertaILC.getPromocion().getCveCamp());
			params.put("IUM", ium);
			//JAIG isjsonvalueCode=isJsonValueCode.ONECLICK;
			doNetworkOperation(Server.CONSULTA_DETALLE_OFERTA, params,true, new OfertaILC(),isJsonValueCode.ONECLICK,
					baseViewController);
		}
		
	}
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson, ParsingHandler handler,isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if( viewController != null)
			((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,false);
	}
		
		public void analyzeResponse(int operationId, ServerResponse response) {
			if(operationId==Server.CONSULTA_DETALLE_OFERTA){
				OfertaILC ofertaILC= (OfertaILC)response.getResponse();
				((BmovilViewsController)viewController.getParentViewsController()).showDetalleILC(ofertaILC, aceptaiOfertaILC.getPromocion());
				if(Server.ALLOW_LOG) Log.d("", "estoy entrando analyze");
				}else if(operationId==Server.CONSULTA_DETALLE_OFERTA_EFI){
					OfertaEFI ofertaEFI= (OfertaEFI)response.getResponse();
					((BmovilViewsController)viewController.getParentViewsController()).showDetalleEFI(ofertaEFI, aceptaiOfertaILC.getPromocion());
					
				}
		}

	
	@Override
	public ArrayList<Object> getDatosTablaResultados() {

		ArrayList<Object> datosResultados = new ArrayList<Object>();
		ArrayList<String> registro;				
		formatoFechaCatMostrar();
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_credito));
			registro.add(Tools.hideAccountNumber(aceptaiOfertaILC.getNumeroTarjeta()));
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_beneficiario));
			registro.add(aceptaiOfertaILC.getBeneficiario());
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_linea1));
			registro.add(Tools.formatAmount(aceptaiOfertaILC.getLineaActual().replace(",",""),false));
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_linea));
			registro.add(Tools.formatAmount(aceptaiOfertaILC.getImporte().replace(",",""),false));
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_linea2));
			registro.add(Tools.formatAmount(aceptaiOfertaILC.getLineaFinal().replace(",",""),false));
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_fecha));
			registro.add(aceptaiOfertaILC.getFechaOperacion());
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_hora));
			registro.add(aceptaiOfertaILC.getHora());
			datosResultados.add(registro);
			
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.bmovil_result_folio));
			registro.add(aceptaiOfertaILC.getFolioInternet());
			datosResultados.add(registro);
		
		return datosResultados;
		
	}
	
	@Override
	public int getTextoEncabezado() {
		return R.string.bmovil_pantallailc_title;
	}
	
	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.icono_pagar_servicios;
	}
	
	public void showMenu() {
		// TODO Auto-generated method stub
		((BmovilViewsController)viewController.getParentViewsController()).showMenuPrincipal();
		
	}
	
	public void showResultados(){
		((BmovilViewsController)viewController.getParentViewsController()).showResultadosViewController(this, -1, -1);
	}
	
	public void showDetallePromocion(){
		String cveCamp=aceptaiOfertaILC.getPromocion().getCveCamp().substring(0,4);
		if(cveCamp.equals("0130"))
			realizaOperacion(Server.CONSULTA_DETALLE_OFERTA,viewController,false,false );
		else if(cveCamp.equals("0377"))		
			realizaOperacion(Server.CONSULTA_DETALLE_OFERTA_EFI,viewController,false,false);		
			
	}
	
	public BaseViewController getcontroladorExitoILC() {
		return viewController;
	}

	public void setcontroladorExitoILCView(BaseViewController viewController) {
		this.viewController = viewController;
	}
	
	@Override
   	public String getTextoAyudaResultados() {
    	return "CAT "+ofertaILC.getCat()+"%"+" sin I.V.A Informativo.\nFecha de Cálculo al "+fechaCat;
		
	}
	
	@Override
	public String getTituloTextoEspecialResultados() {
		return "";
	}
	
	@Override
	public String getTextoEspecialResultados() {
		return viewController.getString(R.string.bmovil_result_textoAyuda);
	}
	
	public void formatoFechaCatMostrar(){
		try{	
			String[] meses = { "enero", "febrero", "marzo", "abril", "mayo", 
			      "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };		
			String fechaCatM= ofertaILC.getFechaCat();
			int mesS=Integer.parseInt(fechaCatM.substring(5,7));
			String dia= fechaCatM.substring(fechaCatM.length()-2);
			String anio= fechaCatM.substring(0,4);
			
			fechaCat= dia+" de "+meses[mesS-1].toString()+ " de "+anio;
		    
				}catch(Exception ex){
					if(Server.ALLOW_LOG) Log.e("","error formato"+ex);
			}
		}

}
