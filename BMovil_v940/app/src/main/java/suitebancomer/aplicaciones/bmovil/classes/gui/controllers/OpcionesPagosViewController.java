package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.base.callback.CallBackSession;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.SincronizarSesion;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.MenuHamburguesaViewsControllers;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import tracking.TrackingHelper;

public class OpcionesPagosViewController extends BaseViewController   implements CallBackModule, CallBackSession {

	LinearLayout vista;
	ListaSeleccionViewController listaSeleccion;
	TextView titulo;
	ArrayList<Object> lista;
	PagarDelegate pagarDelegate;
	String screenSubtitle;
	//AMZ
			public BmovilViewsController parentManager;
			//AMZ
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_opciones_transfer);
		Bundle extras = getIntent().getExtras();
		setTitle(extras.getInt(Constants.PANTALLA_BASE_TITULO),extras.getInt(Constants.PANTALLA_BASE_ICONO));
		screenSubtitle = getString(extras.getInt(Constants.PANTALLA_BASE_SUBTITULO));
		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(PagarDelegate.PAGAR_DELEGATE_ID));
		pagarDelegate = (PagarDelegate)getDelegate();
		if (pagarDelegate == null) {
			pagarDelegate = new PagarDelegate();
			parentViewsController.addDelegateToHashMap(PagarDelegate.PAGAR_DELEGATE_ID, pagarDelegate);
			setDelegate(pagarDelegate);
		}
		pagarDelegate.setControladorPagos(this);
		vista = (LinearLayout)findViewById(R.id.opciones_transfer_layout);
		init();
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("pagar", parentManager.estados);
	}
	
	public void init(){
		cargaListaSeleccionComponent();
	}
	
	@SuppressWarnings("deprecation")
	public void cargaListaSeleccionComponent(){

		ArrayList<String> titulos = new ArrayList<String>();
		titulos.add(screenSubtitle);
		
		lista = pagarDelegate.cargarListasMenu();
			
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(vista);
		
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
		listaSeleccion.setDelegate(pagarDelegate);
		listaSeleccion.setLista(lista);
		listaSeleccion.setNumeroColumnas(1);
		listaSeleccion.setTitle(screenSubtitle);
		listaSeleccion.setOpcionSeleccionada(-1);
		listaSeleccion.setSeleccionable(false);
		listaSeleccion.setAlturaFija(false);

		listaSeleccion.cargarTabla();
		vista.addView(listaSeleccion);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		if (pagarDelegate != null) {
			pagarDelegate.setControladorPagos(this);
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(PagarDelegate.PAGAR_DELEGATE_ID);
		super.goBack();
	}
	
	public void opcionSeleccionada(String opcionSeleccionada) {
		//ARR
				Map<String,Object> inicioOperacionMap = new HashMap<String, Object>();
		Session session = Session.getInstance(SuiteApp.getInstance());
		
		if (opcionSeleccionada.equals(Constants.SERVICE_PAYMENT_TYPE)) {
			//ARR
			inicioOperacionMap.put("evento_inicio", "event45");
			inicioOperacionMap.put("&&products", "operaciones;pagar+servicio");
			inicioOperacionMap.put("eVar12", "inicio");

			TrackingHelper.trackInicioOperacion(inicioOperacionMap);
			if(Autenticacion.getInstance().operarOperacion(Constants.Operacion.pagoServicios,	session.getClientProfile())){
				if (pagarDelegate.isOpenHours()) {
					((BmovilViewsController)parentViewsController).showPagosViewController(Constants.Operacion.pagoServicios);
				} else {
					showInformationAlert(pagarDelegate.textoMensajeAlerta());
				}
			} else {
				// No esta permitido
			}
		} else if (opcionSeleccionada.equals(Constants.OPERACION_PAGAR_OTRAS_TDC)) {
			//ARR
			inicioOperacionMap.put("evento_inicio", "event45");
			inicioOperacionMap.put("&&products", "operaciones;pagar+tarjeta credito");
			inicioOperacionMap.put("eVar12", "inicio");

			TrackingHelper.trackInicioOperacion(inicioOperacionMap);
			if(Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirBancomer,	session.getClientProfile())){
				if (Session.getInstance(SuiteApp.appContext).puedeOperarConTDCEje()) {
					((BmovilViewsController) parentViewsController)
					.showTransferViewController(
							Constants.Operacion.transferirBancomer.value,
							false, true);
				} else {
					showInformationAlert(R.string.opcionesTransfer_alerta_texto_tdc);
				}
			} else {
				// No esta permitido
			}
		} else if (opcionSeleccionada.equals(Constants.INTERNAL_TRANSFER_CREDIT_TYPE)) {
			
			//	Toast.makeText(SuiteApp.getInstance(),"INTERNAL_TRANSFER_CREDIT_TYPE seleccionada", Toast.LENGTH_LONG).show();

				//ARR
				inicioOperacionMap.put("evento_inicio", "event45");
				inicioOperacionMap.put("&&products", "operaciones;pagar+tarjeta credito");
				inicioOperacionMap.put("eVar12", "inicio");

				TrackingHelper.trackInicioOperacion(inicioOperacionMap);
				if(Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirBancomer,	session.getClientProfile())){
					
					
					
					if (existeCuentaTDC()){
					
							if (existeMasDeUnaCuenta()) {
							
								if (Session.getInstance(SuiteApp.appContext).puedeOperarConTDCEje()) {
									((BmovilViewsController) parentViewsController)
									.showSelectTDCViewController(
										Constants.Operacion.transferirBancomer.value,
										false, true);
								} else {
									showInformationAlert(R.string.opcionesTransfer_alerta_texto_tdc);
								}
							}else {
								showInformationAlert(R.string.error_cuenta_eje_credito);
			
								
							}
						
				
					}else {
						
						showInformationAlert(R.string.error_sin_tdc);

						
					}	
							
							
							
							
							
				
				} else {
					// No esta permitido
				}
		} else if (opcionSeleccionada.equals(Constants.OPERACION_PAGAR_OTROS_CREDITOS)) {
			//ARR
			inicioOperacionMap.put("evento_inicio", "event45");
			inicioOperacionMap.put("&&products", "operaciones;pagar+mis creditos");
			inicioOperacionMap.put("eVar12", "inicio");

			TrackingHelper.trackInicioOperacion(inicioOperacionMap);

			Log.d("CGI-Nice-Ppl", "Niko is back bitches");

			if (!bancomer.api.common.commons.Tools.isNetworkConnected(this)) {
				showErrorMessage(getString(R.string.error_communications));
				return;
			}

			Autenticacion aut = Autenticacion.getInstance();
			Constants.Perfil perfil = session.getClientProfile();

			if(Autenticacion.getInstance().operarOperacion(Constants.Operacion.consultarCreditos,	session.getClientProfile())){

				String cadAutenticacion =  aut.getCadenaAutenticacion(Constants.Operacion.consultarCreditos, perfil);
				if(Server.ALLOW_LOG) {
					Log.d(">> CGI", "Cadena de autenticacion >> " + cadAutenticacion);
					Log.d(">> CGI", "Perfil autenticacion >> " + perfil.name());
				}
				//cadAutenticacion = "11001";

				if(("00000").equals(cadAutenticacion)){

					muestraIndicadorActividad(getString(R.string.alert_operation),getString(R.string.alert_connecting));
					// Call to modulo
					InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance(this);//, ((BmovilViewsController)getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient());
					init.getConsultaSend().setIUM(session.getIum());
					init.getConsultaSend().setCallBackSession(this);
					init.getConsultaSend().setCallBackModule(this);
					init.getConsultaSend().setUsername(session.getUsername());
					init.getConsultaSend().setCadenaAutenticacion(cadAutenticacion);
					init.getConsultaSend().setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR);
					parentViewsController.setActivityChanging(true);
					init.preShowConsultarOtrosCreditos();


				}else{

					PagarDelegate delegate = (PagarDelegate) ((BmovilViewsController)getParentViewsController()).getBaseDelegateForKey(PagarDelegate.PAGAR_DELEGATE_ID);
					if (delegate == null) {
						delegate = new PagarDelegate();
						((BmovilViewsController)getParentViewsController()).addDelegateToHashMap(PagarDelegate.PAGAR_DELEGATE_ID,
								delegate);
					}
					showConfirmacion(delegate);
				}
			}else{
				showInformationAlert(R.string.error_operacion_no_permitida);
			}

			}
	}

	public void showConfirmacion(PagarDelegate delegate){
		int resSubtitle = 0;
		int resTitleColor = 0;
		int resIcon = R.drawable.bmovil_configurarmontos_icono;
		int resTitle = R.string.bmovil_menu_miscuentas_otroscreditos_otrosCredito;
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(pagarDelegate, resIcon, resTitle, resSubtitle, resTitleColor);
	}

	private boolean existeCuentaTDC() {
		boolean existeTDC=false;


		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		
		
		for(Account acc : accounts) {
			
			if (acc.getType().equals(Constants.CREDIT_TYPE)) {
				existeTDC=true;
			}
		}
		
		
		
		return existeTDC;
	}

	private boolean existeMasDeUnaCuenta() {
		
		boolean existenMasdeUna=true;

		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		Boolean cuentaEjeTDC = true;
		

				
		if(profile == Constants.Perfil.basico) {
			
			for(Account acc : accounts) {
				if(acc.isVisible() && (!acc.getType().equals(Constants.CREDIT_TYPE))){
					accountsArray.add(acc);
					cuentaEjeTDC = false;
				}
			}
			
		}else{
			
			for(Account acc : accounts) {
				if(acc.isVisible() && (!acc.getType().equals(Constants.CREDIT_TYPE))) {
					accountsArray.add(acc);
					cuentaEjeTDC = false;
					break;// wow
				}
			}
			
			for(Account acc : accounts) {
				if(!acc.isVisible()  && (!acc.getType().equals(Constants.CREDIT_TYPE)))
					accountsArray.add(acc);
			}
			
		}  
		
		
		
		if( ( (profile == Constants.Perfil.basico) || (accounts.length == 1) ) && (Tools.obtenerCuentaEje().getType().equals(Constants.CREDIT_TYPE))){ //cuentaEjeTDC) ){
			//showInformationAlert(R.string.error_cuenta_eje_credito);
			existenMasdeUna=false;

		}
		
	/*	if (accountsArray.size()>1){
			existenMasdeUna=true;
		} 
		*/
		return existenMasdeUna;
		
		
		
	}

	@Override
	public void returnToPrincipal() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(true);
	}

	@Override
	public void returnDataFromModule(String operation, bancomer.api.common.io.ServerResponse response) {

		ocultaIndicadorActividad();
		if(pagarDelegate.getConfirmacionAutenticacionViewController() != null) pagarDelegate.getConfirmacionAutenticacionViewController().ocultaIndicadorActividad();
		if(response.getStatus() == suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse.OPERATION_SUCCESSFUL){

			InitConsultaOtrosCreditos.getInstance().showConsultarOtrosCreditos();

		}else{
			parentViewsController.setActivityChanging(false);
			if (response.getStatus() == suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse.OPERATION_WARNING) {
				showInformationAlertEspecial(getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);

			}else if(response.getStatus() == suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse.OPERATION_ERROR){
				showInformationAlertEspecial(getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
			}

		}
	}

	@Override
	public void userInteraction() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();
	}

	@Override
	public void cierraSesionBackground(Boolean isChanging) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		parentViewsController.setActivityChanging(isChanging);
		if (parentManager.consumeAccionesDePausa()) {
			finish();
			if(MenuHamburguesaViewsControllers.getIsOterApp()){
				MenuHamburguesaViewsControllers.setIsOterApp(Boolean.FALSE);
			}
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	public void cierraSesion() {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication()
				.setApplicationInBackground(Boolean.FALSE);
		SuiteApp.getInstance().getBmovilApplication().logoutApp(Boolean.FALSE);
	}

	@Override
	public void returnMenuPrincipal() {
		Session.getInstance(SuiteApp.appContext).reloadFiles();
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(Boolean.TRUE);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(true);
	}

	@Override
	public void closeSession(String userName, String ium, String client) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getBmovilApp().closeSession(userName, ium, client);
	}
}
