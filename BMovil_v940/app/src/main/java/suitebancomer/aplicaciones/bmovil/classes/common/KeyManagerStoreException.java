package suitebancomer.aplicaciones.bmovil.classes.common;

public class KeyManagerStoreException extends Exception {

	private static final long serialVersionUID = 1934098484417065232L;

	public KeyManagerStoreException() {
		// TODO Auto-generated constructor stub
	}

	public KeyManagerStoreException(String detailMessage) {
		super(detailMessage);
		// TODO Auto-generated constructor stub
	}

	public KeyManagerStoreException(Throwable throwable) {
		super(throwable);
		// TODO Auto-generated constructor stub
	}

	public KeyManagerStoreException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
		// TODO Auto-generated constructor stub
	}

}
