package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuConsultarViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.classes.gui.controllers.BaseViewController;

public class MenuConsultarDelegate extends DelegateBaseAutenticacion{
	
	/*
	 * 
	 */
	public static final long MENU_CONSULTAR_DELEGATE_ID = -8610021420369274295L;

	/*
	 * 
	 */
	private MenuConsultarViewController menuConsultarViewController;

	private Boolean opcionRetiroSinTarjeta= false;

	private ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController;

	public ConfirmacionAutenticacionViewController getConfirmacionAutenticacionViewController(){
		return confirmacionAutenticacionViewController;
	}

	public void setConfirmacionAutenticacionViewController(ConfirmacionAutenticacionViewController c){
		this.confirmacionAutenticacionViewController = c;
	}

	public Boolean getOpcionRetiroSinTarjeta() {
		return opcionRetiroSinTarjeta;
	}

	public void setOpcionRetiroSinTarjeta(Boolean opcionRetiroSinTarjeta) {
		this.opcionRetiroSinTarjeta = opcionRetiroSinTarjeta;
	}
	
	/*
	 * devuelve las opciones|identificador para el menu de consultar 
	 */
	public ArrayList<?> getDatosTablaMenu() {

		ArrayList<ArrayList<String>> lista = new ArrayList<ArrayList<String>>();
		
		ArrayList<String> registro = null;
		Session session = Session.getInstance(SuiteApp.getInstance());
		
		
		registro = new ArrayList<String>();
		registro.add(BmovilConstants.MenuConsultar_movimientos_opcion);
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_menu_consultar_movimientos_opcion));
		lista.add(registro);
		
		if (Autenticacion.getInstance().mostrarOperacion(Constants.Operacion.dineroMovil,	session.getClientProfile())) {
		registro = new ArrayList<String>();
		registro.add(BmovilConstants.MenuConsultar_enviosdm_opcion);
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_menu_consultar_enviosdm_opcion));
		lista.add(registro);
		}
		
		registro = new ArrayList<String>();
		registro.add(BmovilConstants.MenuConsultar_opfrecuentes_opcion);
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_menu_consultar_opfrecuentes_opcion));
		lista.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(BmovilConstants.MenuConsultar_interbancarios);
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_menu_consultar_interbancarias_opcion));
		lista.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(BmovilConstants.MenuConsultar_depositosrecibidos_opcion);
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_menu_consultar_depositosrecibidos_opcion));
		lista.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(BmovilConstants.MenuConsultar_obtenercomprobante_opcion);
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_menu_consultar_obtenercomprobante_opcion));
		lista.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(BmovilConstants.MenuConsultar_retirosintarjeta_opcion);
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_menu_consultar_retirosintarjeta_opcion));
		lista.add(registro);

		registro = new ArrayList<String>();
		registro.add(BmovilConstants.MenuConsultar_otroscreditos_opcion);
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_menu_consultar_otroscreditos_opcion));
		lista.add(registro);
		
		/*registro = new ArrayList<String>();
		registro.add(Constants.MenuConsultar_estadosdecuenta_opcion);
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_menu_consultar_estadosdecuenta_opcion));
		lista.add(registro);*/
		
//		registro = new ArrayList<String>();
//		registro.add(Constants.MenuConsultar_oprapidas_opcion);
//		registro.add(SuiteApp.appContext.getString(R.string.bmovil_menu_consultar_oprapidas_opcion));		
//		lista.add(registro);

		return lista;
	}
	
	@Override
	public void performAction(Object obj) {	
		if(obj instanceof String){
			String selected = (String) obj;
				menuConsultarViewController.opcionSeleccionada(selected);
		}
	}
	
	/**
	 * @return the menuConsultarViewController
	 */
	public MenuConsultarViewController getMenuConsultarViewController() {
		return menuConsultarViewController;
	}


	/**
	 * @param menuConsultarViewController the menuConsultarViewController to set
	 */
	public void setMenuConsultarViewController(
			MenuConsultarViewController menuConsultarViewController) {
		this.menuConsultarViewController = menuConsultarViewController;
	}
	
	
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {
		 if (menuConsultarViewController instanceof MenuConsultarViewController) {
			((BmovilViewsController)menuConsultarViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller);
		 }
		
	}
	
	
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			
			if (operationId == Server.OP_SOLICITAR_ALERTAS) {
				
				SolicitarAlertasData validacionalertas = (SolicitarAlertasData) response.getResponse();
				if (Constants.ALERT02.equals(validacionalertas.getValidacionAlertas())){
					// 02 no tiene alertas
					
					SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
					//SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_dineromovil_recortado_sin_alertas));
					if (opcionRetiroSinTarjeta){
						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_retiroSinTarjeta_recortado_sin_alertas));
						opcionRetiroSinTarjeta=false;
					}else {
						SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert(SuiteApp.getInstance().getString(R.string.opcionesTransfer_alerta_texto_dineromovil_recortado_sin_alertas));
					}
				
					
				} else{
				
					setSa(validacionalertas);
					analyzeAlertasRecortadoSinError();
				}
				return;

			} 
			
			
			

		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
			

		}else if(response.getStatus() == ServerResponse.OPERATION_ERROR){
					
		}		
	}
	
	/**
	 * Muestra una alerta por pantalla
	 */
	private void mostrarAlert(String mensaje) {
		((BmovilViewsController) menuConsultarViewController.getParentViewsController())
		.getCurrentViewControllerApp().showInformationAlert(
				mensaje);
	}


	
	
	public void comprobarRecortado(){

		solicitarAlertas(getMenuConsultarViewController());
			
	}


	@Override
	public boolean mostrarContrasenia() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.consultarCreditos,
				perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarCVV(Constants.Operacion.consultarCreditos, perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.consultarCreditos,
				perfil);
		return value;
	}

	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public Constants.TipoOtpAutenticacion tokenAMostrar() {
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.consultarCreditos,
					perfil);
		} catch (Exception ex) {
			Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
			tipoOTP = null;
		}
		return tipoOTP;
	}

	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {

		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;

		fila = new ArrayList<String>();
		fila.add("Consulta");
		fila.add("Creditos Otorgados");
		tabla.add(fila);
		fila = new ArrayList<String>();
		fila.add("Fecha de operación");

		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		fila.add(formatter.format(date));

		tabla.add(fila);

		return tabla;
	}

	@Override
	public int getTextoEncabezado() {
		int resTitle;
		resTitle = R.string.bmovil_menu_miscuentas_otroscreditos_otrosCredito;
		return resTitle;
	}

	@Override
	public int getNombreImagenEncabezado() {
		int resIcon = R.drawable.bmovil_configurarmontos_icono;
		return resIcon;
	}


	@Override
	public String getTextoTituloResultado() {
		int txtTitulo = R.string.transferir_detalle_operacion_exitosaTitle;
		return menuConsultarViewController.getString(txtTitulo);
	}


	/**
	 * Invoca la operacion para hacer el cambio de cuenta asociada
	 */
	@Override
	public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController, String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
		confirmacionAutenticacionViewController.muestraIndicadorActividad(confirmacionAutenticacionViewController.getString(R.string.alert_operation), confirmacionAutenticacionViewController.getString(R.string.alert_connecting));
		setConfirmacionAutenticacionViewController(confirmacionAutenticacionViewController);
		// Llamada al modulo de Consulta Creditos
		Session session = Session.getInstance(SuiteApp.getInstance());
		// Inicializamos la clase de configuracion
		InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance(menuConsultarViewController);//(menuConsultarViewController, ((BmovilViewsController)menuConsultarViewController.getParentViewsController()).getBmovilApp().getServer().getClienteHttp().getClient());
		// Establecemos los parametros necesarios
		init.getConsultaSend().setIUM(session.getIum());
		init.getConsultaSend().setUsername(session.getUsername());
		init.getConsultaSend().setCallBackModule(menuConsultarViewController);
		init.getConsultaSend().setCveAcceso(contrasenia == null ? "" : contrasenia);
		init.getConsultaSend().setTarjeta5Dig(campoTarjeta == null ? "" : campoTarjeta);
		init.getConsultaSend().setCodigoNip(nip == null ? "" : nip);
		init.getConsultaSend().setCodigoCvv2(cvv == null ? "" : cvv);
		init.getConsultaSend().setCodigoOtp(token == null ? "" : token);
		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultarCreditos, session.getClientProfile());
		init.getConsultaSend().setCadenaAutenticacion(cadenaAutenticacion);
		init.getConsultaSend().setServerParams(Server.DEVELOPMENT, Server.SIMULATION, Server.EMULATOR);
		init.setActivity(menuConsultarViewController);
		// Realizamos la llamada a Consulta Otros Creditos
		((BmovilViewsController)menuConsultarViewController.getParentViewsController()).setCurrentActivityApp(confirmacionAutenticacionViewController);
		((BmovilViewsController)menuConsultarViewController.getParentViewsController()).setActivityChanging(true);
		init.preShowConsultarOtrosCreditos();

	}

}
