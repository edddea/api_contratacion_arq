package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.RegistrarOperacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaDatosViewController;
import tracking.TrackingHelper;

public class RegistrarOperacionViewController extends BaseViewController {

	private RegistrarOperacionDelegate delegate;
	private LinearLayout contenedorASM, contenedorLista;
	private EditText asm;
	private TextView campoASM;
	private TextView instruccionesASM;
	private ListaDatosViewController listaDatos;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	//SPEI
	private View helpImage;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_registrar_operacion);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("registro", parentManager.estados);
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		delegate = (RegistrarOperacionDelegate) getParentViewsController().getBaseDelegateForKey(RegistrarOperacionDelegate.REGISTRAR_OPERACION_DELEGATE_ID);
		setDelegate(delegate);
		setTitulo();
		delegate.setViewController(this);		
		findViews();
		scaleToScreenSize();
		inicializarPantalla();
		moverScroll();
		asm.addTextChangedListener(new BmovilTextWatcher(this));
		
		//SPEI
//		boolean showHelpImage = (Boolean)getIntent().getExtras().get("showHelpImage");
//		helpImage.setVisibility(showHelpImage ? View.VISIBLE : View.GONE);
		//Termina SPEI	
	}
		
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	private void setTitulo(){
		setTitle(delegate.getTextoEncabezado(),
				delegate.getNombreImagenEncabezado());
	}

	private void findViews() {
		contenedorASM = (LinearLayout) findViewById(R.id.registrar_op_campo_asm_layout);
		asm = (EditText) findViewById(R.id.registrar_op_asm_edittext);
		campoASM = (TextView) findViewById(R.id.registrar_op_asm_label);
		instruccionesASM = (TextView) findViewById(R.id.registrar_op_asm_instrucciones_label);
		contenedorLista = (LinearLayout) findViewById(R.id.registrar_op_lista_datos);
		//SPEI
		helpImage = findViewById(R.id.helpImage);
	}
	
	/**
	 * metodo que se encarga de escalar las vistas en base a la pantalla
	 */
	private void scaleToScreenSize() {
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(findViewById(R.id.registrar_op_contenedor_principal), false);
		gTools.scale(contenedorLista, false);
		gTools.scale(contenedorASM, false);
		gTools.scale(asm, true);
		gTools.scale(campoASM, true);
		gTools.scale(instruccionesASM, true);
		gTools.scale(findViewById(R.id.registrar_op_confirmar_button));
		//SPEI
		gTools.scale(helpImage);
	}
	
	/**
	 * metodo que inicializa la pantalla
	 */
	@SuppressWarnings("deprecation")
	private void inicializarPantalla(){
		ArrayList<Object> datos = delegate.getDatosRegistroOp();
		LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		listaDatos = new ListaDatosViewController(this, params, getParentViewsController());
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.setTitulo(R.string.bmovil_registrar_op_subtitle);
		listaDatos.showLista();
		contenedorLista.addView(listaDatos);
		
		mostrarCampoRegistro();
	}
	
	/**
	 * Configura y muestra el campo de registro cuando es necesario con
	 * los mensajes correspondientes
	 * 
	 */
	private void mostrarCampoRegistro(){
		TipoOtpAutenticacion token = delegate.tokenAMostrar();
		switch (token) {
		case registro:
			contenedorASM.setVisibility(View.VISIBLE);
			campoASM.setVisibility(View.VISIBLE);
			asm.setVisibility(View.VISIBLE);
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
			asm.setFilters(userFilterArray);
			asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
			break;

		default:
			break;
		}

		TipoInstrumento tipoInstrumento = delegate.consultaTipoInstrumento();

		switch (tipoInstrumento) {
		case OCRA:
			campoASM.setText(delegate.getEtiquetaCampoOCRA());
			//asm.setTransformationMethod(null);
			break;
		case DP270:
			campoASM.setText(delegate.getEtiquetaCampoDP270());
			//asm.setTransformationMethod(null);
			break;
		case SoftToken:
			if(SuiteApp.getSofttokenStatus()) {
				asm.setText(Constants.DUMMY_OTP);
				asm.setEnabled(false);
				campoASM.setText(delegate.getEtiquetaCampoSoftokenActivado());
			} else {
				asm.setText("");
				asm.setEnabled(true);
				campoASM.setText(delegate.getEtiquetaCampoSoftokenDesactivado());
				//asm.setTransformationMethod(null);
			}
			
//			if(SuiteApp.getSofttokenStatus()) {
//				String otp = delegate.getToken();
//				if(null != otp) {
//					asm.setText(otp);
//					asm.setEnabled(false);
//					asm.setTransformationMethod(null);
//				}
//				campoASM.setText(delegate.getEtiquetaCampoSoftokenActivado());
//			} else {
//				asm.setText("");
//				asm.setEnabled(true);
//				campoASM.setText(delegate.getEtiquetaCampoSoftokenDesactivado());
//			}			
			break;
		default:
			break;
		}
		String instrucciones = delegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
		if (instrucciones.equals("")) {
			instruccionesASM.setVisibility(View.GONE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}

	}


	public void botonContinuarClick(View view) {
		if(parentViewsController.isActivityChanging())
			return;
		if (validaDatos())
		{
			//ARR
			Map<String,Object> paso3OperacionMap = new HashMap<String, Object>();
			
			
			//Comprobar titulo
			if(getString(delegate.getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrascuentasbbva))
			{
				//ARR
				paso3OperacionMap.put("evento_paso3", "event48");
				paso3OperacionMap.put("&&products", "operaciones;transferencias+otra cuenta bbva bancomer");
				paso3OperacionMap.put("eVar12", "paso3:registra");

				TrackingHelper.trackPaso3Operacion(paso3OperacionMap);
			}
			else if(getString(delegate.getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_cuentaexpress))
			{
				//ARR
				paso3OperacionMap.put("evento_paso3", "event48");
				paso3OperacionMap.put("&&products", "operaciones;transferencias+cuenta express");
				paso3OperacionMap.put("eVar12", "paso3:registra");

				TrackingHelper.trackPaso3Operacion(paso3OperacionMap);
			}
			else if(getString(delegate.getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrosbancos))
			{
				//ARR
				paso3OperacionMap.put("evento_paso3", "event48");
				paso3OperacionMap.put("&&products", "operaciones;transferencias+otros bancos");
				paso3OperacionMap.put("eVar12", "paso3:registra");

				TrackingHelper.trackPaso3Operacion(paso3OperacionMap);
			}
			else if(getString(delegate.getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_dineromovil))
			{
				//ARR
				paso3OperacionMap.put("evento_paso3", "event48");
				paso3OperacionMap.put("&&products", "operaciones;transferencias+dinero movil");
				paso3OperacionMap.put("eVar12", "paso3:registra");

				TrackingHelper.trackPaso3Operacion(paso3OperacionMap);
			}
			else if(getString(delegate.getTextoEncabezado()) == getString(R.string.transferir_otrosBBVA_TDC_title))
			{
				//ARR
				paso3OperacionMap.put("evento_paso3", "event48");
				paso3OperacionMap.put("&&products", "operaciones;pagar+tarjeta credito");
				paso3OperacionMap.put("eVar12", "paso3:registra");

				TrackingHelper.trackPaso3Operacion(paso3OperacionMap);
			}
			else if(getString(delegate.getTextoEncabezado()) == getString(R.string.tiempo_aire_title))
			{
				//ARR
				paso3OperacionMap.put("evento_paso3", "event48");
				paso3OperacionMap.put("&&products", "operaciones;comprar+tiempo aire");
				paso3OperacionMap.put("eVar12", "paso3:registra");

				TrackingHelper.trackPaso3Operacion(paso3OperacionMap);
			}
		
			delegate.registrarOperacion();
		}
	}

	boolean validaDatos() {
		return delegate.validaDatos();
	}

	public void limpiarCampos() {
		if(asm.getVisibility() == View.VISIBLE){
			asm.setText("");
		}
	}

	public String pideASM() {
		if (asm.getVisibility() == View.GONE) {
			return "";
		} else {
			return asm.getText().toString();
		}
	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {	
		delegate.analyzeResponse(operationId, response);
	}
}
