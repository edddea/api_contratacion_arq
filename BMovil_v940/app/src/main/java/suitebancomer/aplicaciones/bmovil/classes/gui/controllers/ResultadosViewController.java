package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.timer.TimerController;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.AltaRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaRetiroSinTarjetaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultarEstatusEnvioEstadodeCuentaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DineroMovilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ResultadosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaDatosViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import tracking.TrackingHelper;////
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;


import com.bancomer.mbanking.R;
import com.bancomer.mbanking.StartBmovilInBack;
import com.bancomer.mbanking.SuiteApp;

import java.util.HashMap;
import java.util.Map;
import tracking.TrackingHelper;
//One click
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.TypedValue;

public class ResultadosViewController extends BaseViewController implements View.OnClickListener {
	
	private ResultadosDelegate resultadosDelegate;
	private ConsultarEstatusEnvioEstadodeCuentaDelegate consultarEstatusEnvioEstadodeCuentaDelegate;
	private TextView tituloResultados;
	private TextView textoResultados;
	private LinearLayout aviso;
	private TextView instruccionesResultados;
	private TextView titulotextoEspecialResultados;
	private TextView lblAviso;
	private TextView textoEspecialResultados;
	private ImageButton menuButton;
	//Mejoras Bmovil
	private ImageButton compartirButton;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_resultados);
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(getParentViewsController().getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID));
		
		resultadosDelegate = (ResultadosDelegate)getDelegate();
		resultadosDelegate.setResultadosViewController(this);
		
		findViews();
		scaleToScreenSize();
		
		menuButton.setOnClickListener(this);
		//Mejoras Bmovil
		compartirButton.setOnClickListener(this);
		setTitulo();
		resultadosDelegate.consultaDatosLista();
		configuraPantalla();
		moverScroll();
		if (resultadosDelegate.getOperationDelegate() instanceof AltaRetiroSinTarjetaDelegate){
			showInformationAlert(R.string.retiro_sintarjetaresultado_alert);
		}
		//viene de dinero movil
		if (resultadosDelegate.getOperationDelegate() instanceof DineroMovilDelegate ){
			//si es envio
			if(((DineroMovilDelegate) resultadosDelegate.getOperationDelegate()).getTipoOperacion() == Constants.Operacion.dineroMovilF ) {
				showInformationAlert(R.string.label_dinero_movil_alert);
			}
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		parentViewsController.setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
    	if(resultadosDelegate.getmBroadcastReceiver() != null ){
    		unregisterReceiver(resultadosDelegate.getmBroadcastReceiver());
    		resultadosDelegate.setmBroadcastReceiver(null);
    	}
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		//resultados envioec paperless
		if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.bmovil_Consultar_Estatus_EnvioEC_title)
				||getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.confirmacion_paperless_title))
		{
			return false;
		}
		else {
			//resultados envioec paperless
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.menu_bmovil_resultados, menu);
			//AMZ
			int opc = parentManager.estados.size() - 1;
			int opc2 = parentManager.estados.size() - 2;
			if (parentManager.estados.get(opc) == "opciones") {
				String rem = parentManager.estados.remove(opc);
			} else if (parentManager.estados.get(opc2) == "sms" || parentManager.estados.get(opc2) == "correo"
					|| parentManager.estados.get(opc2) == "alta frecuentes") {
				String rem = parentManager.estados.remove(opc2);
				rem = parentManager.estados.remove(parentManager.estados.size() - 1);
			} else {
				TrackingHelper.trackState("opciones", parentManager.estados);
			}
			return true;
		}
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		int opcionesMenu = resultadosDelegate.getOpcionesMenuResultados();
		boolean showMenu = false;
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_SMS) != DelegateBaseOperacion.SHOW_MENU_SMS) {
			menu.removeItem(R.id.save_menu_sms_button);
		} else {
			showMenu = true;
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_EMAIL) != DelegateBaseOperacion.SHOW_MENU_EMAIL) {
			menu.removeItem(R.id.save_menu_email_button);
		} else {
			showMenu = true;
			//menu.removeItem(R.id.save_menu_email_button);//TODO remover para mostrar menu completo
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_PDF) != DelegateBaseOperacion.SHOW_MENU_PDF) {
			menu.removeItem(R.id.save_menu_pdf_button);
		} else {
			//showMenu = true;
			menu.removeItem(R.id.save_menu_pdf_button);//TODO remover para mostrar menu completo
		}
//		resultados envioec paperless
		//if (resultadosDelegate.getFrecOpOK() || ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_FRECUENTE) != DelegateBaseOperacion.SHOW_MENU_FRECUENTE)) {
		if (resultadosDelegate.getFrecOpOK() || ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_FRECUENTE) != DelegateBaseOperacion.SHOW_MENU_FRECUENTE)) {
			menu.removeItem(R.id.save_menu_frecuente_button);
		} else {
			showMenu = true;
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_RAPIDA) != DelegateBaseOperacion.SHOW_MENU_RAPIDA) {
			menu.removeItem(R.id.save_menu_rapida_button);
		} else {
			//showMenu = true;
			menu.removeItem(R.id.save_menu_rapida_button);//TODO remover para mostrar menu completo
		}
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_BORRAR) != DelegateBaseOperacion.SHOW_MENU_BORRAR) {
			menu.removeItem(R.id.save_menu_borrar_button);
		} else {
			showMenu = true;
		}
		
		return showMenu;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		//ARR
				Map<String,Object> envioConfirmacionMap = new HashMap<String, Object>();
		switch(item.getItemId()) {
			case R.id.save_menu_sms_button:
				resultadosDelegate.enviaSMS();
				//AMZ
				int sms = parentManager.estados.size()-1;
				if(parentManager.estados.get(sms)=="resul"){
					String rem = parentManager.estados.remove(sms);
					rem = parentManager.estados.remove(parentManager.estados.size()-1);
				}
					TrackingHelper.trackState("sms", parentManager.estados);
					TrackingHelper.trackState("resul", parentManager.estados);
				
				//ARR Comprobacion de titulos
				
				if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_miscuentas))
				{
					//ARR
					envioConfirmacionMap.put("events", "event17");
					envioConfirmacionMap.put("eVar17", "sms:transferencias+mis cuentas");

					TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
				}
				else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrascuentasbbva))
				{
					//ARR
					envioConfirmacionMap.put("events", "event17");
					envioConfirmacionMap.put("eVar17", "sms:transferencias+otra cuenta bbva bancomer");

					TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
				}
				else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_cuentaexpress))
				{
					//ARR
					envioConfirmacionMap.put("events", "event17");
					envioConfirmacionMap.put("eVar17", "sms:transferencias+cuenta express");

					TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
				}
				else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrosbancos))
				{
					//ARR
					envioConfirmacionMap.put("events", "event17");
					envioConfirmacionMap.put("eVar17", "sms:transferencias+otros bancos");

					TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
				}
				else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_dineromovil))
				{
					//ARR
					envioConfirmacionMap.put("events", "event17");
					envioConfirmacionMap.put("eVar17", "sms:transferencias+dinero movil");

					TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
				}
				return true;
			case R.id.save_menu_email_button:
				resultadosDelegate.enviaEmail();
				//AMZ
				int correo = parentManager.estados.size()-1;
				if(parentManager.estados.get(correo)=="resul"){
					String rem = parentManager.estados.remove(correo);
					rem = parentManager.estados.remove(parentManager.estados.size()-1);
				}
				
				//ARR Comprobacion de titulos
				
				if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_miscuentas))
				{
					//ARR
					envioConfirmacionMap.put("events", "event17");
					envioConfirmacionMap.put("eVar17", "correo:transferencias+mis cuentas");

					TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
				}
				else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrascuentasbbva))
				{
					//ARR
					envioConfirmacionMap.put("events", "event17");
					envioConfirmacionMap.put("eVar17", "correo:transferencias+otra cuenta bbva bancomer");

					TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
				}
				else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_cuentaexpress))
				{
					//ARR
					envioConfirmacionMap.put("events", "event17");
					envioConfirmacionMap.put("eVar17", "correo:transferencias+cuenta express");

					TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
				}
				else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrosbancos))
				{
					//ARR
					envioConfirmacionMap.put("events", "event17");
					envioConfirmacionMap.put("eVar17", "correo:transferencias+otros bancos");

					TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
				}
				else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_dineromovil))
				{
					//ARR
					envioConfirmacionMap.put("events", "event17");
					envioConfirmacionMap.put("eVar17", "correo:transferencias+dinero movil");

					TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
				}
				return true;
			case R.id.save_menu_pdf_button:
				resultadosDelegate.guardaPDF();
				return true;
			case R.id.save_menu_frecuente_button:
				resultadosDelegate.guardaFrecuente();
				//AMZ
				int frec = parentManager.estados.size()-1;
				if(parentManager.estados.get(frec)=="resul"){	
					String rem = parentManager.estados.remove(frec);
					rem = parentManager.estados.remove(parentManager.estados.size()-1);
				}
				return true;
			case R.id.save_menu_rapida_button:
				resultadosDelegate.guardaRapido();
				return true;
			case R.id.save_menu_borrar_button:
				resultadosDelegate.borraRapido();
				return true;
			default:
				return false;
		}
	}
	
	@Override
	public void goBack() {
		//Se supone que el boton de atras no haga nada
	}
	
	public void setTitulo() {
		super.setTitle(resultadosDelegate.getTextoEncabezado(), 
						resultadosDelegate.getNombreImagenEncabezado());
	}

	/**
	 * muestra las cuentas a modificar tratandose de consultaEstatusEnvioEstadodeCuenta.
	 * pantallaresultado envioec
	 */
	@SuppressWarnings("deprecation")
	private void mostrarCuentasResult(){
	    consultarEstatusEnvioEstadodeCuentaDelegate = new ConsultarEstatusEnvioEstadodeCuentaDelegate();

		//Log.w("Alberto", datos.toString());
		//Log.w("Alberto", "" + datos.getClass());
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(findViewById(R.id.resultados_lista_cuentas));

		LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		ListaSeleccionViewController listaCuentas = new ListaSeleccionViewController(this, params,parentViewsController);
		//ListaDatosViewController listaCuentas = new ListaDatosViewController(this, params, parentViewsController);
		ArrayList<Object> datos = consultarEstatusEnvioEstadodeCuentaDelegate.getDatosResult();
		ArrayList<Object> encabezado = new ArrayList<Object>();
		encabezado.add(null);
	    encabezado.add(getString(R.string.bmovil_Consultar_Estatus_EnvioEC_Cuenta));
	    encabezado.add(getString(R.string.bmovil_Consultar_Estatus_EnvioEC_Envio));
	    encabezado.add(null);
	        /*
	        listaCuentas.setDelegate(consultarEstatusEnvioEstadodeCuentaDelegate);
	        */
	        listaCuentas.setEncabezado(encabezado);
            listaCuentas.setNumeroColumnas(2);
            listaCuentas.setEcFlag(true);
	        listaCuentas.setLista(datos);
	        listaCuentas.setOpcionSeleccionada(-1);
	        listaCuentas.setSeleccionable(false);
	        listaCuentas.setAlturaFija(true);
	        listaCuentas.setNumeroFilas(datos.size());
	        listaCuentas.setExisteFiltro(false);
	        listaCuentas.setClickable(false);
	        listaCuentas.setMarqueeEnabled(false);
			listaCuentas.cargarTabla();

	      //  listaCuentas.setNumeroCeldas(2);

	    //	listaDatos.setTitulo(R.string.confirmation_subtitulo);
	    //  listaCuentas.showLista();

		LinearLayout layoutCuentas = (LinearLayout)findViewById(R.id.resultados_lista_cuentas);
		layoutCuentas.addView(listaCuentas);
		layoutCuentas.setVisibility(View.VISIBLE);

	}

	public void setListaDatos(ArrayList<Object> datos) {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(findViewById(R.id.resultados_lista_datos));

		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		ListaDatosViewController listaDatos = new ListaDatosViewController(this, params, parentViewsController);
		if(resultadosDelegate.getOperationDelegate() instanceof AltaRetiroSinTarjetaDelegate){
			ArrayList<String> lista = listaDatos.getTablaDatosAColorear();

			if(lista.contains(getString(R.string.result_retironsintarjeta_importe))){
				lista.remove(getString(R.string.result_retironsintarjeta_importe));
			}


			if(!lista.contains(getString(R.string.bmovil_consultar_retirosintarjeta_detalles_clave_retiro))) lista.add(getString(R.string.bmovil_consultar_retirosintarjeta_detalles_clave_retiro));

			listaDatos.setTablaDatosAColorear(lista);

		}
		cambiarColores(listaDatos);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.showLista();
		LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.resultados_lista_datos);
		layoutListaDatos.addView(listaDatos);
	}

	private void cambiarColores(ListaDatosViewController listaDatos) {
		if(resultadosDelegate.getOperationDelegate() instanceof AltaRetiroSinTarjetaDelegate) {
			listaDatos.setTitulosPrimerAzulRetiroSinTarjeta(false);
		}else if(resultadosDelegate.getOperationDelegate() instanceof DineroMovilDelegate){
			listaDatos.setTitulosPrimerAzulRetiroDineromovil(false);;
		}
		if(resultadosDelegate.getOperationDelegate() instanceof ConsultaRetiroSinTarjetaDelegate){
			ArrayList<String> lista = listaDatos.getTablaDatosAColorear();

			if(lista.contains(getString(R.string.result_retironsintarjeta_importe))){
				lista.remove(getString(R.string.result_retironsintarjeta_importe));
			}

			if(!lista.contains(getString(R.string.bmovil_consultar_dineromovil_detalles_datos_otp))) lista.add(getString(R.string.bmovil_consultar_dineromovil_detalles_datos_otp));
			if(!lista.contains(getString(R.string.bmovil_consultar_retirosintarjeta_detalles_clave_retiro))) lista.add(getString(R.string.bmovil_consultar_retirosintarjeta_detalles_clave_retiro));

			listaDatos.setTablaDatosAColorear(lista);
		}
	}

	public void configuraPantalla() {
		String titulo = resultadosDelegate.getTextoTituloResultado();
		String texto = resultadosDelegate.getTextoPantallaResultados();
		String instrucciones = resultadosDelegate.getTextoAyudaResultados();
		String tituloTextoEspecial = resultadosDelegate.getTituloTextoEspecialResultados();
		String textoEspecial = resultadosDelegate.getTextoEspecialResultados();
		
		if (titulo.equals("")) {
			tituloResultados.setVisibility(View.GONE);
		} else {
			tituloResultados.setText(titulo);
			tituloResultados.setTextColor(getResources().getColor(resultadosDelegate.getColorTituloResultado()));
		}
		
		if (texto.equals("")) {
			textoResultados.setVisibility(View.GONE);
		} else {
			textoResultados.setText(texto);
		}
		
		if (instrucciones.equals("")) {
			instruccionesResultados.setVisibility(View.GONE);
		} else {
			instruccionesResultados.setText(instrucciones);
		}
		
		if (tituloTextoEspecial.equals("")) {
			titulotextoEspecialResultados.setVisibility(View.GONE);
		}else {
			titulotextoEspecialResultados.setText(tituloTextoEspecial);
		}
		
		if (textoEspecial.equals("")) {
			textoEspecialResultados.setVisibility(View.GONE);
		} else {
			textoEspecialResultados.setText(textoEspecial);
		}
		
		//Mejoras Bmovil
		if(instruccionesResultados.getText() == ""){
			compartirButton.setVisibility(View.INVISIBLE);
		}
		
		//AMZ 
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	if(titulo == this.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_operacion_cancelada)){
		TrackingHelper.trackState("cancelada", parentManager.estados);
	}else if(titulo == this.getString(R.string.transferir_detalle_operacion_exitosaTitle)){
		TrackingHelper.trackState("resul", parentManager.estados);
	}else{
		TrackingHelper.trackState("resul", parentManager.estados);
		
		LinearLayout layoutConfirmacionAviso = (LinearLayout) findViewById(R.id.seeAviso);
		//pantalla resultados envioec paperless
		/*layoutConfirmacionAviso.measure(MeasureSpec.UNSPECIFIED,
				MeasureSpec.UNSPECIFIED);
		float avisoHeight = layoutConfirmacionAviso.getMeasuredHeight();*/

	}
		//resultados envioec paperless
	if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.consultar_estados_de_cuenta_title))
	{
	   aviso.setVisibility(View.VISIBLE);
	}

	if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.bmovil_Consultar_Estatus_EnvioEC_title))
	{
	   textoEspecialResultados.setVisibility(View.GONE);
	   tituloResultados.setTextColor(getResources().getColor(R.color.paperles_verde));
	   aviso.setVisibility(View.VISIBLE);
	   lblAviso.setText(R.string.bmovil_Consultar_Estatus_EnvioEC_lblAviso);
	   mostrarCuentasResult();
	   //btnCompartir.setVisibility(View.VISIBLE);
	}
	if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.confirmacion_paperless_title))
	{
		textoEspecialResultados.setVisibility(View.GONE);
		lblAviso.setText(R.string.resultado_texto_especial);
	    super.setTitle(resultadosDelegate.getTextoEncabezado(), resultadosDelegate.getNombreImagenEncabezado(),
			resultadosDelegate.getColorTituloResultado());

	}//resultados envioec paperless







	//One click
	if(!textoEspecial.equals("")&& tituloTextoEspecial.equals("")&& !instrucciones.equals("")){
		String text= textoEspecial;
		SpannableString textS= new SpannableString(text);
		/*AbsoluteSizeSpan headerSpan = new AbsoluteSizeSpan(18);
		textS.setSpan(headerSpan, 0, text.length(), 0);*/
		textS.setSpan(new ForegroundColorSpan(Color.rgb(77,77,77)),0,text.length(),0);
		textoEspecialResultados.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
		textoEspecialResultados.setText(textS);
		String text2= instrucciones;
		SpannableString textS2= new SpannableString(text2);
		/*AbsoluteSizeSpan headerSpan2 = new AbsoluteSizeSpan(14);
		textS2.setSpan(headerSpan2, 0, text2.length(), 0);*/
		textS2.setSpan(new ForegroundColorSpan(Color.rgb(176,176,176)),0,text2.length(),0);
		instruccionesResultados.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
		instruccionesResultados.setText(textS2);
		titulotextoEspecialResultados.setVisibility(View.GONE);
	}
	
	//Termina one CLlick
		/*
		ViewGroup contenido = (ViewGroup)this.findViewById(android.R.id.content).getRootView();//findViewById(android.R.id.content);
		contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float contentHeight = contenido.getMeasuredHeight();
		LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.resultados_lista_datos);
		layoutListaDatos.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float listaHeight = layoutListaDatos.getMeasuredHeight();
		textoResultados.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float textoHeight = textoResultados.getMeasuredHeight();
		instruccionesResultados.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float instruccionesHeight = instruccionesResultados.getMeasuredHeight();
		textoEspecialResultados.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float textoEspecialHeight = textoEspecialResultados.getMeasuredHeight();
		menuButton.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		float buttonHeight = menuButton.getMeasuredHeight();
		
		float margin = getResources().getDimension(R.dimen.confirmacion_fields_initial_margin);
		
		float maximumSize = (contentHeight * 4) / 5;
		System.out.println("Altura maxima " +maximumSize);
		float elementsSize = listaHeight + textoHeight + instruccionesHeight + textoEspecialHeight + buttonHeight;
		System.out.println("Altura mixta " +elementsSize);
		float heightParaValidar = (contentHeight*3)/4;
		System.out.println("heightParaValidar " +contentHeight);
		
		RelativeLayout.LayoutParams botonLayout = (RelativeLayout.LayoutParams)menuButton.getLayoutParams();
		if (elementsSize <= contentHeight) {
			botonLayout.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		} else {
			botonLayout.addRule(RelativeLayout.BELOW, R.id.resultado_texto_especial);
		}
		*/
	}
	
	@Override
	public void onClick(View v) {
		if (v == menuButton && !parentViewsController.isActivityChanging()) {
			botonMenuClick();
		}else if (v==compartirButton){
			openOptionsMenu();
		}
	}
	
	public void botonMenuClick() {
		//AMZ
				((BmovilViewsController)parentViewsController).touchMenu();

		parentViewsController.removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		parentViewsController.removeDelegateFromHashMap(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
		if(bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)  || SuiteApp.PASOMENUPRINCIPAL){//flujo bmovil
			((BmovilViewsController)parentViewsController).showMenuPrincipal(true);
		}else{
			SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
			ActivityCompat.finishAffinity(this);

			Intent i=new Intent(this, StartBmovilInBack.class);
			i.putExtra(bancomer.api.common.commons.Constants.RESULT_STRING,bancomer.api.common.commons.Constants.TRUE_STRING);
			i.putExtra( bancomer.api.common.commons.Constants.TIMEOUT_STRING, bancomer.api.common.commons.Constants.FALSE_STRING);
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			finish();
		}
	}


	public BroadcastReceiver createBroadcastReceiver() {
    	
	     return new BroadcastReceiver() {
			@Override
		     public void onReceive(Context ctx, Intent intent) {
		    	 
		    	 String toastMessage;
		    	 
			     switch (getResultCode()) {
				     case Activity.RESULT_OK:
					     toastMessage = getString(R.string.sms_success);    
					     break;
				     case SmsManager.RESULT_ERROR_NO_SERVICE: //"SMS: No service"
				    	 toastMessage = getString(R.string.sms_error_noService);
				    	 break;
				     case SmsManager.RESULT_ERROR_NULL_PDU: //"SMS: Null PDU"
				    	 toastMessage = getString(R.string.sms_error_nullPdu);
				    	 break;
				     case SmsManager.RESULT_ERROR_RADIO_OFF: //"SMS: Radio off"
				    	 toastMessage = getString(R.string.sms_error_radioOff);
				    	 break;
				     case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
				     default:
				    	 toastMessage = getString(R.string.sms_error);
				    	 break;
			     }
			     
			     ocultaIndicadorActividad();
			     Toast.makeText(getBaseContext(), toastMessage,Toast.LENGTH_SHORT).show();
		     }
	     };
	}
	
	private void findViews() {
		tituloResultados = (TextView)findViewById(R.id.resultado_titulo);
		textoResultados = (TextView)findViewById(R.id.resultado_texto);
		lblAviso = (TextView)findViewById(R.id.lblAviso);//paperless
		aviso = (LinearLayout)findViewById(R.id.seeAviso);//paperless
		instruccionesResultados = (TextView)findViewById(R.id.resultado_instrucciones);
		textoEspecialResultados = (TextView) findViewById(R.id.resultado_texto_especial);
		titulotextoEspecialResultados = (TextView) findViewById(R.id.resultado_titulo_texto_especial);
		menuButton = (ImageButton)findViewById(R.id.resultados_menu_button);
		//Mejoras Bmovil
		compartirButton = (ImageButton)findViewById(R.id.resultados_compartir);
		//Aviso resultado campa�a paperless
		if(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado() == R.string.confirmacion_paperless_title){
			aviso.setVisibility(View.VISIBLE);
		}
		else
			aviso.setVisibility(View.GONE);

		//btnCompartir = (ImageView) findViewById(R.id.imgBtnCompartir);
	}
	
	private void scaleToScreenSize() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scale(lblAviso, true);
		guiTools.scale(tituloResultados, true);
		guiTools.scale(textoResultados, true);
		guiTools.scale(instruccionesResultados, true);
		guiTools.scale(textoEspecialResultados, true);
		guiTools.scale(titulotextoEspecialResultados, true);
		guiTools.scale(menuButton);
	}
	
	public void setListaClave(ArrayList<Object> datos) {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(findViewById(R.id.resultados_lista_clave));
		
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		
		ListaSeleccionViewController listaSeleccion = new ListaSeleccionViewController(this, params,
					parentViewsController);
			listaSeleccion.setDelegate(resultadosDelegate);
			listaSeleccion.setFijarLista(true);
			listaSeleccion.setNumeroColumnas(1);
			listaSeleccion.setEncabezado(null);
			listaSeleccion.setLista(datos);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(datos.size());
			listaSeleccion.setExisteFiltro(false);
			listaSeleccion.cargarTabla();
			LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.resultados_lista_clave);
			layoutListaDatos.addView(listaSeleccion);
			
		
			findViewById(R.id.resultados_lista_clave).setVisibility(View.VISIBLE);
	}
	//One click

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		resultadosDelegate.analyzeResponse(operationId, response);
	}
	//Termina One click
	
}
