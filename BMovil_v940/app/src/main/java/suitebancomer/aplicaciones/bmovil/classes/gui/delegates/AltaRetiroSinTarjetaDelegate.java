package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.bancomer.mbanking.BmovilApp;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.StartBmovilInBack;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.model.CatalogoVersionado;
import bancomer.api.common.model.Compania;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.AltaRetiroSinTarjetaViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionRegistroViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.PagoTdcViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.RegistrarOperacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultarLimitesResult;
import suitebancomer.aplicaciones.bmovil.classes.model.RegistrarOperacionResult;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjeta;
import suitebancomer.aplicaciones.bmovil.classes.model.RetiroSinTarjetaResult;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import tracking.TrackingHelper;

public class AltaRetiroSinTarjetaDelegate extends DelegateBaseAutenticacion {
	
	
	
	
	
	public final static long ALTA_RETIRO_SINTAR_DELEGATE_ID = 5L;
	private BaseViewController altaRetiroSinTarjetaViewController;
	private RetiroSinTarjetaResult retiroSinTarjetaResult;
	
	
	private RetiroSinTarjeta modeloRetiroSinTarjeta;
	

	private Constants.Operacion tipoOperacion;
	
	
	private Session session;
	
	
	private ConsultarLimitesResult limitesMontos;
	

	
	
	
	private Account cuentaOrigenSeleccionada = null;
	
	public Account getCuentaOrigenSeleccionada() {
		return cuentaOrigenSeleccionada;
	}


	public void setCuentaOrigenSeleccionada(Account cuentaOrigenSeleccionada) {
		this.cuentaOrigenSeleccionada = cuentaOrigenSeleccionada;
	}
	
	
	

	
	public ConsultarLimitesResult getLimitesMontos() {
		return limitesMontos;
	}


	public void setLimitesMontos(ConsultarLimitesResult limitesMontos) {
		this.limitesMontos = limitesMontos;
	}


	public RetiroSinTarjeta getModeloRetiroSinTarjeta() {
		return modeloRetiroSinTarjeta;
	}


	public void setModeloRetiroSinTarjeta(RetiroSinTarjeta modeloRetiroSinTarjeta) {
		this.modeloRetiroSinTarjeta = modeloRetiroSinTarjeta;
	}


	public BaseViewController getAltaRetiroSinTarjetaViewController() {
		return altaRetiroSinTarjetaViewController;
	}



	public void setAltaRetiroSinTarjetaViewController(
			BaseViewController altaRetiroSinTarjetaViewController) {
		this.altaRetiroSinTarjetaViewController = altaRetiroSinTarjetaViewController;
	}




	public RetiroSinTarjetaResult getRetiroSinTarjetaResult() {
		return retiroSinTarjetaResult;
	}


	public void setRetiroSinTarjetaResult(
			RetiroSinTarjetaResult retiroSinTarjetaResult) {
		this.retiroSinTarjetaResult = retiroSinTarjetaResult;
	}

	
	

	/**
	 * @return El tipo de operación.
	 */
	public Constants.Operacion getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	 * @param tipoOperacion El tipo de operación a estblecer.
	 */
	public void setTipoOperacion(Constants.Operacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	
	
	/**
	 * Constructor por defecto
	 */
	public AltaRetiroSinTarjetaDelegate() {

		this.tipoOperacion = Constants.Operacion.retiroSinTarjeta;
	
	}
	
	
	
	

	
/* peticion al servidor de alta de retiro sin tarjeta desde confirmacion*/
	public void operacionAltaRetiroSinTarjeta(ConfirmacionViewController confirmacionViewController,String contrasenia, String nip, String token, String campoTarjeta, String cvv) {
		
	/*	altaRetiroSinTarjetaViewController.muestraIndicadorActividad(
				altaRetiroSinTarjetaViewController.getString(R.string.alert_operation),
				altaRetiroSinTarjetaViewController.getString(R.string.alert_connecting));
		*/
		session = Session.getInstance(SuiteApp.appContext);
		
		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(tipoOperacion, session.getClientProfile(), Tools.getDoubleAmountFromServerString(modeloRetiroSinTarjeta.getImporte()));

		String tipoCuentaSeleccionada= modeloRetiroSinTarjeta.getCuentaOrigen().getType();
		String numeroCuentaSeleccionada= modeloRetiroSinTarjeta.getCuentaOrigen().getNumber();
		
		String importeRetiro= modeloRetiroSinTarjeta.getImporte();
		String conceptoRetiro= modeloRetiroSinTarjeta.getConcepto();


		
		//prepare data
        Hashtable<String,String> paramTable = new Hashtable<String,String>();       
        
        //paramTable.put(ServerConstants.OPERACION, Server.OPERATION_CODES[Server.OP_RETIRO_SIN_TAR]);

		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        paramTable.put("numeroCelular", session.getUsername());
        paramTable.put(ServerConstants.CVE_ACCESO, Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia);
        paramTable.put(ServerConstants.CODIGO_NIP, Tools.isEmptyOrNull(nip) ? "" : nip);
        paramTable.put(ServerConstants.CODIGO_CVV2, Tools.isEmptyOrNull(cvv) ? "" : cvv);
        paramTable.put(ServerConstants.CADENA_AUTENTICACION, cadenaAutenticacion);
        paramTable.put(ServerConstants.CODIGO_OTP,  Tools.isEmptyOrNull(token) ? "" : token);

        paramTable.put(ServerConstants.TIPO_CUENTA,tipoCuentaSeleccionada);
        paramTable.put("numeroCuenta", numeroCuentaSeleccionada);
        paramTable.put("cuentaDestino", "");
        
        
        
        paramTable.put(ServerConstants.PERFIL_CLIENTE,session.getClientProfile().name());
        paramTable.put(ServerConstants.TARJETA_5DIG,Tools.isEmptyOrNull(campoTarjeta) ? "" : campoTarjeta);
        
        
        paramTable.put("importe",importeRetiro);
        paramTable.put("concepto", conceptoRetiro);
        

        //JAIG NO
		doNetworkOperation(Server.OP_RETIRO_SIN_TAR, paramTable,true, new RetiroSinTarjetaResult(),isJsonValueCode.NONE, confirmacionViewController);
	}


	/**
	 * regresa a el activity StartBmovilInBack indicando que ocurrio un error
	 */
	private void returnToStartBmovilInBack(){
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
		ActivityCompat.finishAffinity(altaRetiroSinTarjetaViewController);
		altaRetiroSinTarjetaViewController.finish();
		Intent i=new Intent(SuiteApp.getInstance(), StartBmovilInBack.class);
		i.putExtra( Constants.RESULT_STRING,Constants.FALSE_STRING);
		i.putExtra( Constants.TIMEOUT_STRING,Constants.FALSE_STRING);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		altaRetiroSinTarjetaViewController.startActivity(i);
	}

	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		
		
		if(operationId == Server.OP_RETIRO_SIN_TAR){
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				
				retiroSinTarjetaResult = (RetiroSinTarjetaResult)response.getResponse();
				
				operacionExistosa();
				
			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING){
				if(bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)){//flujo bmovil
					altaRetiroSinTarjetaViewController.showInformationAlert(response.getMessageText());
				}else{
					altaRetiroSinTarjetaViewController.showInformationAlert(response.getMessageText(), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							dialogInterface.dismiss();
							returnToStartBmovilInBack();
						}
					});
				}

				
			}else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				
				altaRetiroSinTarjetaViewController.ocultaIndicadorActividad();
				if(bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)){//flujo bmovil
					altaRetiroSinTarjetaViewController.showInformationAlert(response.getMessageText());
				}else{
					altaRetiroSinTarjetaViewController.showInformationAlert(response.getMessageText(), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {
							dialogInterface.dismiss();
							returnToStartBmovilInBack();
						}
					});
				}
				//se trata en el BaseSubapplication.java
				//si entra por error pero no trae codigo ni mensaje, se debe tratar?

			}
			
			altaRetiroSinTarjetaViewController.ocultaIndicadorActividad();
			
		}
		
		/*else if(operationId == Server.OP_SOLICITAR_ALERTAS){
			
			
				setSa((SolicitarAlertasData) response.getResponse());
				analyzeAlertasRecortado();
				return;

		}*/
		
		
		else if(operationId == Server.OP_SOLICITAR_ALERTAS){
			
			analyzeAlertasRecortadoSinError();
			return;
		} 
		
		else if(operationId == Server.OP_VALIDAR_CREDENCIALES){
			showConfirmacionRegistro();
			return;
		}
		
		
		/*else if (operationId == Server.CONSULTAR_LIMITES){
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			
				limitesMontos = (ConsultarLimitesResult) response
						.getResponse();
			}
		}*/
		
		
		
	}
	
	private void operacionExistosa() {
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), "operacionExistosa");
		showResultados();
		
	}


	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson, ParsingHandler handler,isJsonValueCode isJsonValueCode, BaseViewController caller) {
		((BmovilViewsController)caller.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson, handler,isJsonValueCode, caller,true);
	}
	
	

	
	
	/**
	 * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada segun sea requerido.
	 * @return
	 */
	public ArrayList<Account> cargaCuentasOrigen() {
		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		Boolean cuentaEjeTDC = true;
		

				
		if(profile == Constants.Perfil.basico) {
			
			for(Account acc : accounts) {
				if(acc.isVisible() && (!acc.getType().equals(Constants.CREDIT_TYPE))){
					accountsArray.add(acc);
					cuentaEjeTDC = false;
				}
			}
			
		}else{
			
			for(Account acc : accounts) {
				if(acc.isVisible() && (!acc.getType().equals(Constants.CREDIT_TYPE))) {
					accountsArray.add(acc);
					cuentaEjeTDC = false;
					break;// wow
				}
			}
			
			for(Account acc : accounts) {
				if(!acc.isVisible()  && (!acc.getType().equals(Constants.CREDIT_TYPE)))
					accountsArray.add(acc);
			}
			
		}  
		
		if( ( (profile == Constants.Perfil.basico) || (accounts.length == 1) ) && (Tools.obtenerCuentaEje().getType().equals(Constants.CREDIT_TYPE))){ //cuentaEjeTDC) ){

			altaRetiroSinTarjetaViewController.showInformationAlert(R.string.error_cuenta_eje_credito);
		}
		
		return accountsArray;
	}
	
	
	private Account getCuentaOrigen(){
		return ((PagoTdcViewController)altaRetiroSinTarjetaViewController).getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada();
	}
	
	
	
	/**
	 * Carga la lista de companias para las transferencias de dinero movil.
	 * @return La lista de Companias en una mapa con clave 'nombre' y valor la propia compania.
	 */
	public HashMap<String,Compania> cargarListaCompanias() {
		HashMap<String,Compania> listaCompanias = new HashMap<String,Compania>();
		CatalogoVersionado catalogoDineroMovil = Session.getInstance(SuiteApp.appContext).getCatalogoDineroMovil();
		
		Compania compIterada;
		
		for (Object comp : catalogoDineroMovil.getObjetos()) {
			compIterada=(Compania)comp;
			listaCompanias.put(compIterada.getNombre(),compIterada);
		}
		return listaCompanias;
	}
	
	
	
	/**
	 * Obtiene el catálogo de importes para la compania especificada.
	 * @param identificadorCompania Identificador de la compania a buscar.
	 * @return La lista de importes.
	 */
	public String[] obtenerCatalogoDeImportes(String identificadorCompania) {
		ArrayList<String> importes = new ArrayList<String>();
		
		CatalogoVersionado catalogoDineroMovil = Session.getInstance(SuiteApp.appContext).getCatalogoDineroMovil();
		Vector<Object> companias = catalogoDineroMovil.getObjetos();
		for (Object comp : companias) {
			Compania compania = (Compania)comp;
			if(identificadorCompania == compania.getNombre()) {
				for (int monto : compania.getMontosValidos()) {
					importes.add(String.valueOf(monto)); 
				}
				break;
			}
		}
		importes.add(SuiteApp.appContext.getResources().getString(R.string.transferir_dineromovil_datos_importe_otro));
		
		return importes.toArray(new String[importes.size()]);
	}
	
	
	
	/****/
	
	@Override
	public void performAction(Object obj) {
		if(altaRetiroSinTarjetaViewController instanceof AltaRetiroSinTarjetaViewController){

			if(Server.ALLOW_LOG) Log.d("altaRSTDelegate","Regrese de lista Seleccion");
			
			((AltaRetiroSinTarjetaViewController)altaRetiroSinTarjetaViewController).getComponenteCtaOrigen().isSeleccionado();
			((AltaRetiroSinTarjetaViewController)altaRetiroSinTarjetaViewController).muestraListaCuentas();
			((AltaRetiroSinTarjetaViewController)altaRetiroSinTarjetaViewController).getComponenteCtaOrigen().setSeleccionado(false);
		}
		
		/*else if(pagoTdcViewController instanceof SeleccionaTdcViewController){
			Log.d("PagoTDCDelegate","Seleccion true");
			if(((SeleccionaTdcViewController)pagoTdcViewController).componenteCtaOrigen.isSeleccionado()){
				((SeleccionaTdcViewController)pagoTdcViewController).muestraListaCuentas();
				((SeleccionaTdcViewController)pagoTdcViewController).componenteCtaOrigen.setSeleccionado(false);
			}else{
				((SeleccionaTdcViewController)pagoTdcViewController).opcionSeleccionada((Account)obj);
			}
		}*/
	}
	
	
	
	
	
	/**
	 * Valida los datos para la tranferencia.
	 * @param numeroTelefono
	 * @param importe
	 * @param otroImporte
	 * @param beneficiario
	 */
	public void validaDatos(String telefono, String importe, String otroImporte, String beneficiario) {
		double monto; 
		double otroMonto;
		AltaRetiroSinTarjetaViewController controller = null;
		
		try {
			controller = (AltaRetiroSinTarjetaViewController)altaRetiroSinTarjetaViewController;
		} catch (ClassCastException ex) {
			if (Server.ALLOW_LOG) Log.e("AltaRSDelegate", "Error al convertir el view controller a un AltaRetiroSinTarjetaViewController", ex);
			return;
		}
		
		try {
			monto = Double.parseDouble(importe); 
		} catch (Exception ex) {
			monto = 0.0;
		}
		try {
			otroMonto = Double.parseDouble(otroImporte); 
		} catch (Exception ex) {
			otroMonto = 0.0;
		}

					
		altaRetiroSinTarjetaViewController.setHabilitado(true);
		if(Constants.TELEPHONE_NUMBER_LENGTH != telefono.length()) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_telefono_corto);
		} else if (0 == beneficiario.length()) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_beneficiario_nulo);
		} else if (null != importe && Constants.BALANCE_EN_CEROS == monto) {
			controller.showInformationAlert(R.string.transferir_retirosintarjeta_error_importe_vacio);
		} else if (null == importe && Constants.BALANCE_EN_CEROS == otroMonto) {
			controller.showInformationAlert(R.string.transferir_retirosintarjeta_error_otro_vacio);
		} else if (null == importe && 0 != (otroMonto % 100.0)) {
			controller.showInformationAlert(R.string.transferir_dineromovil_error_otro_invalido);
		}  else{
			
			modeloRetiroSinTarjeta.setCelularBeneficiario(telefono);
			modeloRetiroSinTarjeta.setImporte(Tools.formatAmountForServer(String.valueOf((null == importe) ? otroMonto : monto)));
			modeloRetiroSinTarjeta.setBeneficiario(beneficiario);
	
			tipoOperacion = Operacion.retiroSinTarjeta;
			

			//ARR
			Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
			
			//ARR
			paso2OperacionMap.put("evento_paso2", "event47");
			paso2OperacionMap.put("&&products", "operaciones;transferencias+retiro sin tarjeta");
			paso2OperacionMap.put("eVar12", "paso2:cuenta e importe");

			TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
				
			showConfirmacion();			
		}
	}
	
	
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {

		
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_cuenta_retiro));
		fila.add(Tools.hideAccountNumber(modeloRetiroSinTarjeta.getCuentaOrigen().getNumber()));
		tabla.add(fila);
		
		
		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_numero_celular));
		fila.add(modeloRetiroSinTarjeta.getCelularBeneficiario());
		tabla.add(fila);
		
		
		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_compania_celular));
		fila.add(modeloRetiroSinTarjeta.getCompania().getNombre());
		tabla.add(fila);
		
		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_beneficiario));
		fila.add(modeloRetiroSinTarjeta.getBeneficiario());
		tabla.add(fila);

		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_importe));
		fila.add(Tools.formatAmount(modeloRetiroSinTarjeta.getImporte(),false));
		tabla.add(fila);
		
		
		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_concepto));
		fila.add(modeloRetiroSinTarjeta.getConcepto());
		tabla.add(fila);
		
	
		return tabla;
	}
	
	
	
	@Override
	public int getNombreImagenEncabezado() {

		return R.drawable.bmovil_dinero_movil_icono;
	}
	
	
	



	@Override
	public int getTextoEncabezado() {
		return (R.string.transferir_alta_retiro_sintar_title);

	}


	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		TipoOtpAutenticacion value = null;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		value = Autenticacion.getInstance().tokenAMostrar(this.tipoOperacion, perfil,Tools.getDoubleAmountFromServerString(((AltaRetiroSinTarjetaViewController)altaRetiroSinTarjetaViewController).getDatosBeneficiario().getImporte()));
		
		return value;
	}

	@Override
	public boolean mostrarNIP() {
		

		
		boolean mostrar = false;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		mostrar = Autenticacion.getInstance().mostrarNIP(this.tipoOperacion, perfil,Tools.getDoubleAmountFromServerString(((AltaRetiroSinTarjetaViewController)altaRetiroSinTarjetaViewController).getDatosBeneficiario().getImporte()));
		
		return mostrar;
	}

	@Override
	public boolean mostrarContrasenia() {
		boolean mostrar = false;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		mostrar = Autenticacion.getInstance().mostrarContrasena(this.tipoOperacion, perfil,Tools.getDoubleAmountFromServerString(((AltaRetiroSinTarjetaViewController)altaRetiroSinTarjetaViewController).getDatosBeneficiario().getImporte()));
		
		return mostrar;
	}
	
	



	@Override
	public boolean mostrarCVV() {
		boolean mostrar = false;
		Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		mostrar = Autenticacion.getInstance().mostrarCVV(this.tipoOperacion, perfil,Tools.getDoubleAmountFromServerString(((AltaRetiroSinTarjetaViewController)altaRetiroSinTarjetaViewController).getDatosBeneficiario().getImporte()));
		
		return mostrar;
	}
	
	
	
	
	
	/**
	 * Muestra la pantalla de confirmacion.
	 */
	public void showConfirmacion() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		
		if (registrarOperacion())
			bMovilVC.showRegistrarOperacion(this);
		else
			bMovilVC.showConfirmacion(this);
		
	}
	
	
	
	/**
	 * Muestra la pantalla de resultados.
	 */
	public void showResultados() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showResultadosViewController(this, getNombreImagenEncabezado(), getTextoEncabezado());
	}



	@Override
	public void realizaOperacion(ConfirmacionViewController confirmacionViewController,String contrasenia, String nip, String token, String campoTarjeta, String cvv) {
		
		//Toast.makeText(SuiteApp.getInstance(),"realizaOperacion en AltaRetiroSinTarjetaDelegate", Toast.LENGTH_SHORT).show();
		//se llama al metodo que realiza la peticion de alta al servidor
		operacionAltaRetiroSinTarjeta(confirmacionViewController,contrasenia, nip, token,campoTarjeta ,cvv);

	}
	


	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		ArrayList<Object> datosTabla =  new ArrayList<Object>();
		ArrayList<String> registro = null;

			registro = new ArrayList<String>();
			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_clave_retiro));
			registro.add(Tools.formatClaveRetiroGuiones(retiroSinTarjetaResult.getClaveRetiro()));
			datosTabla.add(registro);

			registro = new ArrayList<String>();
			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_codigo_seguridad));
			registro.add(retiroSinTarjetaResult.getClaveRst4());
			datosTabla.add(registro);

			registro = new ArrayList<String>();
			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_cuenta_retiro));
			registro.add(Tools.hideAccountNumber(modeloRetiroSinTarjeta.getCuentaOrigen().getNumber()));
			datosTabla.add(registro);
			
//			registro = new ArrayList<String>();
//			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_numero_celular));
//			registro.add(modeloRetiroSinTarjeta.getCelularBeneficiario());
//			datosTabla.add(registro);
			
//			registro = new ArrayList<String>();
//			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_compania_celular));
//			registro.add(modeloRetiroSinTarjeta.getCompania().getNombre());
//			datosTabla.add(registro);
//
//			registro = new ArrayList<String>();
//			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_beneficiario));
//			registro.add(modeloRetiroSinTarjeta.getBeneficiario());
//			datosTabla.add(registro);
//
			registro = new ArrayList<String>();
			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_importe));
			registro.add(Tools.formatAmount(retiroSinTarjetaResult.getImporte(),false));
			datosTabla.add(registro);
			
			
			registro = new ArrayList<String>();
			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_concepto));
			registro.add(modeloRetiroSinTarjeta.getConcepto());
			datosTabla.add(registro);
			
			
			registro = new ArrayList<String>();
			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_fecha_alta));
			registro.add(Tools.formatDate(retiroSinTarjetaResult.getFechaAlta()));
			datosTabla.add(registro);
			
			
			registro = new ArrayList<String>();
			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_vigencia));
			registro.add(Tools.formatDate(retiroSinTarjetaResult.getVigencia())+" "+retiroSinTarjetaResult.getHoraOperacion()+" hrs");
			datosTabla.add(registro);
			
			
			registro = new ArrayList<String>();
			registro.add(altaRetiroSinTarjetaViewController.getString(R.string.result_retironsintarjeta_folio));
			registro.add(retiroSinTarjetaResult.getFolio());
			datosTabla.add(registro);
			

		
		return datosTabla;
	}
	
	
	
	@Override
	public String getTextoTituloResultado() {

		return SuiteApp.appContext.getString(R.string.transferir_retirosintarjeta_resultados_titulo);
	}
	
	
	@Override
	public int getColorTituloResultado() {

		return R.color.verde_limon;
		
	}
	
	
	
	
	@Override
	public int getOpcionesMenuResultados() {

		return  SHOW_MENU_EMAIL | SHOW_MENU_SMS ;
		
	}
	
	
	@Override
	public String getTextoAyudaResultados() {
		return "Para guardar esta información oprime el botón de opciones";
	}
	
	
	
	
	
	
	
	/* peticion al servidor de alta de retiro sin tarjeta desde registro confirmacion*/
	public void operacionAltaRetiroSinTarjetaRegistro(ConfirmacionRegistroViewController confirmacionRegistroViewController,String contrasenia, String nip, String token, String campoTarjeta, String cvv) {
		
	/*	altaRetiroSinTarjetaViewController.muestraIndicadorActividad(
				altaRetiroSinTarjetaViewController.getString(R.string.alert_operation),
				altaRetiroSinTarjetaViewController.getString(R.string.alert_connecting));
		*/
		session = Session.getInstance(SuiteApp.appContext);
		
		String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(tipoOperacion, session.getClientProfile(), Tools.getDoubleAmountFromServerString(modeloRetiroSinTarjeta.getImporte()));

		String tipoCuentaSeleccionada= modeloRetiroSinTarjeta.getCuentaOrigen().getType();
		String numeroCuentaSeleccionada= modeloRetiroSinTarjeta.getCuentaOrigen().getNumber();
		
		String importeRetiro= modeloRetiroSinTarjeta.getImporte();
		String conceptoRetiro= modeloRetiroSinTarjeta.getConcepto();


		
		//prepare data
        Hashtable<String,String> paramTable = new Hashtable<String,String>();

		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        paramTable.put("numeroCelular", session.getUsername());
        paramTable.put(ServerConstants.CVE_ACCESO, Tools.isEmptyOrNull(contrasenia) ? "" : contrasenia);
        paramTable.put(ServerConstants.CODIGO_NIP, Tools.isEmptyOrNull(nip) ? "" : nip);
        paramTable.put(ServerConstants.CODIGO_CVV2, Tools.isEmptyOrNull(cvv) ? "" : cvv);
        paramTable.put(ServerConstants.CADENA_AUTENTICACION, cadenaAutenticacion);
        paramTable.put(ServerConstants.CODIGO_OTP,  Tools.isEmptyOrNull(token) ? "" : token);

        paramTable.put(ServerConstants.TIPO_CUENTA,tipoCuentaSeleccionada);
        paramTable.put("numeroCuenta", numeroCuentaSeleccionada);
        paramTable.put("cuentaDestino", "");
        
        
        
        paramTable.put(ServerConstants.PERFIL_CLIENTE,session.getClientProfile().name());
        paramTable.put(ServerConstants.TARJETA_5DIG,Tools.isEmptyOrNull(campoTarjeta) ? "" : campoTarjeta);
        
        
        paramTable.put("importe",importeRetiro);
        paramTable.put("concepto", conceptoRetiro);
        
        //JAIG NO
		doNetworkOperation(Server.OP_RETIRO_SIN_TAR, paramTable, true, new RetiroSinTarjetaResult(), isJsonValueCode.NONE,confirmacionRegistroViewController);
	}
	
	

	

	/**
	 * @category RegistrarOperacion
	 * @return
	 */
	public boolean registrarOperacion(){
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();
		Operacion operacion;

		operacion = Operacion.retiroSinTarjeta;
		Autenticacion aut = Autenticacion.getInstance();
		boolean value = aut.validaRegistro(operacion, perfil);

		if(Server.ALLOW_LOG) Log.d("RegistroOP",value+" retiroSinTarjeta ");
		
		return value;
	}
	
	
	
	/**
	 *  @category RegistrarOperacion
	 * @return
	 */
	protected ArrayList<Object> getDatosRegistroOp() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
		
		
		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_cuenta_retiro));
		fila.add(Tools.hideAccountNumber(modeloRetiroSinTarjeta.getCuentaOrigen().getNumber()));
		tabla.add(fila);
		
		
//		fila = new ArrayList<String>();
//		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_numero_celular));
//		fila.add(modeloRetiroSinTarjeta.getCelularBeneficiario());
//		tabla.add(fila);
//
//
//		fila = new ArrayList<String>();
//		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_compania_celular));
//		fila.add(modeloRetiroSinTarjeta.getCompania().getNombre());
//		tabla.add(fila);
//
//		fila = new ArrayList<String>();
//		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_beneficiario));
//		fila.add(modeloRetiroSinTarjeta.getBeneficiario());
//		tabla.add(fila);

		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_importe));
		fila.add(Tools.formatAmount(modeloRetiroSinTarjeta.getImporte(),false));
		tabla.add(fila);
		
		
		fila = new ArrayList<String>();
		fila.add(altaRetiroSinTarjetaViewController.getString(R.string.confirmation_retironsintarjeta_concepto));
		fila.add(modeloRetiroSinTarjeta.getConcepto());
		tabla.add(fila);
		
		return tabla;
	}
	
	/**
	 * @category RegistrarOperacion
	 */
	@Override
	public ArrayList<Object> getDatosRegistroExitoso() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> registro;
		
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_op));
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_exito));
		tabla.add(registro);
		
		registro = new ArrayList<String>();
		registro.add(SuiteApp.appContext.getString(R.string.bmovil_registrar_tabla_cuenta));
		registro.add(modeloRetiroSinTarjeta.getCelularBeneficiario());
		tabla.add(registro);
				
		return tabla;
	}
	
	/**
	 * @category RegistrarOperacion
	 * realiza el registro de operacion
	 * @param rovc
	 * @param token
	 */
	@Override
	protected void realizaOperacion(RegistrarOperacionViewController rovc,	String token) {

		int operationId = Server.OP_VALIDAR_CREDENCIALES;
		Hashtable<String, String> params = new Hashtable<String, String>();		
		session = Session.getInstance(SuiteApp.appContext);
		
		
     	//String cuentaDestino = modeloRetiroSinTarjeta.getCelularBeneficiario();
     	String cuentaDestino = "";

     	
		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		params.put(ServerConstants.CVE_ACCESO, "" );
		params.put(Server.J_NIP, "");
		params.put(Server.J_CVV2, "");
		params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
		params.put(Server.J_AUT, "00010");
		params.put("cuentaDestino", cuentaDestino);
		params.put("tarjeta5Dig", "");
		//JAIG NO
		doNetworkOperation(operationId, params,true, new RegistrarOperacionResult(),isJsonValueCode.NONE, rovc);
	}
	
	 /**
	  * @category RegistrarOperacion
	  * Realiza la operacion de tranferencia desde la pantalla de confirmacionRegistro
	  */
	@Override
	public void realizaOperacion(ConfirmacionRegistroViewController viewController,	String contrasena, String nip, String asm, String cvv) {

		operacionAltaRetiroSinTarjetaRegistro(viewController,contrasena, nip, asm,null,cvv);

	}
		
	/**
	 * @category RegistrarOperacion
	 * Muestra la pantalla de confirmacion registro
	 */
	public void showConfirmacionRegistro() {
		BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
		BmovilViewsController bMovilVC = app.getBmovilViewsController();
		bMovilVC.showConfirmacionRegistro(this);		
	}
	
	
	/*
	@Override
	public String getNumeroCuentaParaRegistroOperacion() {
		String cuentaDestino = modeloRetiroSinTarjeta.getCelularBeneficiario();
		return cuentaDestino.substring(cuentaDestino.length() - 5);
	}
	*/
	

	
	
	public void consultarLimites() {

		int operationId = Server.CONSULTAR_LIMITES;
		session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
		params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		//JAIG SI
		doNetworkOperation(operationId, params,true, new ConsultarLimitesResult(),isJsonValueCode.NONE, altaRetiroSinTarjetaViewController);
		
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion#getTextoSMS()
	 */

	@Override
	public String getTextoSMS() {
		StringBuilder builder = new StringBuilder();
		builder.append("Retiro s/tarjeta por ");
		builder.append(Tools.formatAmount(modeloRetiroSinTarjeta.getImporte(), false));
		builder.append(" al ");
		builder.append(modeloRetiroSinTarjeta.getCompania().getNombre());
		builder.append(" ");
		builder.append(Tools.hideAccountNumber(modeloRetiroSinTarjeta.getCelularBeneficiario()));
		builder.append(" de ");
		builder.append(modeloRetiroSinTarjeta.getBeneficiario());
		builder.append(" Folio: ");
		builder.append(retiroSinTarjetaResult.getFolio());
		builder.append(" Codigo seg: ");
		builder.append(retiroSinTarjetaResult.getClaveRst4());
		builder.append(" Vence: ");
		builder.append(Tools.formatDate(retiroSinTarjetaResult.getFechaAlta()));
		builder.append(" Concepto: ");
		builder.append(modeloRetiroSinTarjeta.getConcepto());		
		return builder.toString();
	}

	
//	Retiro sin tarjeta por <$###,###.##> al <nombre de la compañía> 
//	<*los últimos cinco dígitos del número celular> de <beneficiario>  
//	Folio: <folio>  
//	Codigo seg: <código de seguridad>  
//	Informe a Beneficiario Vence: <dd/mm/yyyy  hh:mm> 
//	Concepto: <concepto>.
	

	

	
	
}
