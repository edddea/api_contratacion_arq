package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;



import java.util.HashMap;
import java.util.Map;


import bancomer.api.common.callback.CallBackModule;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetalleOfertaConsumoDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetalleOfertaEFIDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuPrincipalDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import tracking.TrackingHelper;


public class DetalleConsumoViewController extends BaseViewController {

	private TextView lblTexto2;
	private TextView lblTexto3;
	private TextView lblTexto5;
	private TextView lblTexto6;
	public TextView lblTexto7;
	private TextView lblTexto8;
	private TextView lblTexto9;
	private EditText edtTextoMonto;

	private double montoInicial;
	private double montoMinimo;
	private double montoModificado;

	private String customAmount;


	// Atributos Montos Parciales OOROZCO 20160112

	private String amountString=null;
	private StringBuffer typedString= new StringBuffer();
	private boolean isSettingText = false;
	private boolean isResetting = false;
	private boolean mAcceptCents=false;
	private boolean flag=true;



	private ImageButton btnmeInteresa;
	BaseViewController me;
	private DetalleOfertaConsumoDelegate detalleOfertaConsumoDelegate;
	LinearLayout vista;

	//AMZ
	public BmovilViewsController parentManager;
	//ARR
	Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();

	public ImageButton getBtnmeInteresa() {
		return btnmeInteresa;
	}


	public void setFlag(boolean flag) {
		this.flag = flag;
	}


	public boolean isFlag() {
		return flag;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_detalleconsumo);
		me=this;
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();

		TrackingHelper.trackState("detalle consumo", parentManager.estados);
		//AMZ

		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE));
		detalleOfertaConsumoDelegate = (DetalleOfertaConsumoDelegate)getDelegate();
		detalleOfertaConsumoDelegate.setControladorDetalleILCView(this);
		setTitle(R.string.bmovil_promocion_title_consumo, R.drawable.icono_consumo);
		vista = (LinearLayout)findViewById(R.id.detalleofertaConsumo_view_controller_layout);

		lblTexto2 = (TextView) findViewById(R.id.lblTexto2);
		lblTexto3 = (TextView) findViewById(R.id.lblTexto3);
		lblTexto5 = (TextView) findViewById(R.id.lblTexto5);
		lblTexto6 = (TextView) findViewById(R.id.lblTexto6);
		lblTexto7 = (TextView) findViewById(R.id.lblTexto7);
		lblTexto8 = (TextView) findViewById(R.id.lblTexto8);
		lblTexto9 = (TextView) findViewById(R.id.lblTexto9);
		edtTextoMonto = (EditText) findViewById(R.id.edtTextoMonto);

		setValues();


		edtTextoMonto.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				reset();
				mAcceptCents = true;
				isSettingText = true;
				edtTextoMonto.setText("");
				return false;
			}
		});


		/*edtTextoMonto.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				//You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
				if (keyCode == KeyEvent.KEYCODE_DEL) {
					if (edtTextoMonto.length() > 3 && flag) {
						edtTextoMonto.setSelection(edtTextoMonto.length() - 3);
						flag = false;
					}

				}
				return false;
			}
		});*/


		edtTextoMonto.addTextChangedListener(new TextWatcher() {

			/**
			 * Save the value of string before changing the text
			 */
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				String amountField = edtTextoMonto.getText().toString();
				if (!isSettingText) {
					amountString = amountField;
				}
			}

			/**
			 * When the value string has changed
			 */
			public void onTextChanged(CharSequence s, int start, int before, int count) {

				String amountField = edtTextoMonto.getText().toString();

				if (!isSettingText && !isResetting) {


					if (!flag) {
						String aux;

						try {
							aux = edtTextoMonto.getText().toString().substring(0, edtTextoMonto.length() - 3);
							typedString = new StringBuffer();

							if (aux.charAt(0) == '0')
								aux = aux.substring(1, aux.length());
						} catch (Exception ex) {
							aux = "";

						}

						for (int i = 0; i < aux.length(); i++) {
							if (aux.charAt(i) != ',')
								typedString.append(aux.charAt(i));
						}
						flag = true;

						setFormattedText();
						amountField = edtTextoMonto.getText().toString();
						amountString = amountField;
					} else {


						if (edtTextoMonto.length() < amountString.length()) {
							reset();
						} else if (edtTextoMonto.length() > amountString.length()) {

							int newCharIndex = edtTextoMonto.getSelectionEnd() - 1;
							//there was no selection in the field
							if (newCharIndex == -2) {
								newCharIndex = edtTextoMonto.length() - 1;
							}

							char num = amountField.charAt(newCharIndex);
							if (!(num == '0' && typedString.length() == 0)) {
								typedString.append(num);
							}
							setFormattedText();


							if (edtTextoMonto.getText().toString().equals("0.00")) {
								btnmeInteresa.setBackgroundResource(R.drawable.bmovil_btn_meinteresa);
								String credito = detalleOfertaConsumoDelegate.getOfertaConsumo().getImporte();
								edtTextoMonto.setHint(Tools.formatCredit(credito));
							} else
								btnmeInteresa.setBackgroundResource(R.drawable.btn_modificar_monto_calcular);
						}

					}

				}
			}

			public void afterTextChanged(Editable s) {
				isSettingText = false;
				edtTextoMonto.setSelection(edtTextoMonto.length());
				if(edtTextoMonto.getText().toString().equals("0.00") || edtTextoMonto.length()==0){
					btnmeInteresa.setBackgroundResource(R.drawable.bmovil_btn_meinteresa);
				}
			}
		});



        edtTextoMonto.setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View view, int keyCode, KeyEvent event) {

				if (keyCode == KeyEvent.KEYCODE_ENTER) {
					InputMethodManager manager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
					if (manager != null)
						manager.hideSoftInputFromWindow(view.getWindowToken(), 0);

					if (edtTextoMonto.getText().toString().equals("0.00") || edtTextoMonto.length() == 0)
						btnmeInteresa.setBackgroundResource(R.drawable.bmovil_btn_meinteresa);
				}


				return false;
			}
		});







		//To add  btnmeInteresa.setBackgroundResource(R.drawable.btn_modificar_monto_calcular);
		//To add  btnmeInteresa.setBackgroundResource(R.drawable.bmovil_btn_meinteresa);

		btnmeInteresa = (ImageButton)findViewById(R.id.bmovil_btn_meinteresa);
		btnmeInteresa.setOnClickListener(clickListener);
		configurarPantalla();
		validateOfertaDelSimulador();
	}

	private void validateOfertaDelSimulador(){//MOD 53808
		if(Session.getInstance(SuiteApp.appContext).getOfertaDelSimulador()){  //si retorna true, entró por credit, si es false, entró por oneclick
			lblTexto9.setVisibility(View.GONE);
			edtTextoMonto.setVisibility(View.GONE);
		} else{
			lblTexto9.setVisibility(View.VISIBLE);
			edtTextoMonto.setVisibility(View.VISIBLE);
		}
	}

	OnClickListener clickListener=new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			Session session = Session.getInstance(SuiteApp.appContext);

			if(session.getImporteParcial().equals("")){
				session.setImporteInicial(detalleOfertaConsumoDelegate.getOfertaConsumo().getImporte());
				session.setImporteMinimo(detalleOfertaConsumoDelegate.getOfertaConsumo().getMontoMinimo());
			}

			// String lblInicial = Tools.formatAmount(detalleOfertaConsumoDelegate.getOfertaConsumo().getImporte(), false);
			//String lblMin = Tools.formatAmount(detalleOfertaConsumoDelegate.getOfertaConsumo().getMontoMinimo(), false);



			String lblInicial = Tools.formatAmount(session.getImporteInicial(),false);
			String lblMin = Tools.formatAmount(session.getImporteMinimo(),false);

			if (v == btnmeInteresa) {

				if(Session.getInstance(SuiteApp.appContext).getOfertaDelSimulador()){
					paso1OperacionMap.put("evento_paso1", "event46");
					paso1OperacionMap.put("&&products", "simulador:contratacion consumo");
					paso1OperacionMap.put("eVar12", "paso1:consulta detalle");

				} else{
					paso1OperacionMap.put("evento_paso1", "event46");
					paso1OperacionMap.put("&&products", "promociones;detalle consumo");
					paso1OperacionMap.put("eVar12", "paso1:clausulas consumo");
				}

				try {
					customAmount = edtTextoMonto.getText().toString();

					if(customAmount.length() > 0) {
						String value = Tools.formatSplitAmount(customAmount);

						Log.w("Valor obtenido",value);

						montoModificado = Double.parseDouble(value);
						session.setImporteParcial(Tools.formatSplitReverse(value));
					} else {
						montoModificado = 0;
					}
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch(RuntimeException e){
					e.printStackTrace();
				} catch(Exception e){
					e.printStackTrace();
				}
				if(montoModificado == 0 ) {
					((BmovilViewsController) parentViewsController).showContratoConsumo(detalleOfertaConsumoDelegate.getOfertaConsumo(), detalleOfertaConsumoDelegate.getPromocion());
					//ARR
					paso1OperacionMap.put("evento_paso1", "event46");
					paso1OperacionMap.put("&&products", "promociones;detalle consumo");
					paso1OperacionMap.put("eVar12", "paso1:clausulas consumo");

					TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
					detalleOfertaConsumoDelegate.realizaOperacion(Server.CONTRATO_OFERTA_CONSUMO, me);

				} else if (montoModificado < montoMinimo) {
					showErrorMessage("El importe esta por debajo del rango, el mínimo a ingresar es de "+lblMin,new DialogInterface.OnClickListener() {
						@Override
						public void onClick( DialogInterface dialog,int which) {
							resetValues();
						}
					});

				} else if (montoModificado > montoInicial) {
					showErrorMessage("El importe supera el límite, el máximo a ingresar es de "+lblInicial,new DialogInterface.OnClickListener() {
						@Override
						public void onClick( DialogInterface dialog,int which) {
							resetValues();
						}
					});
					resetValues();
				} else if (montoModificado <= montoInicial) {
					muestraIndicadorActividad("", "Calculando");

					String value = Tools.formatSplitAmount(customAmount);
					String value2 = Tools.formatSplitReverse(value);
					session.setImporteParcial(value2);
					session.setChanged(true);
					detalleOfertaConsumoDelegate.realizaOperacionCONSUMO(Server.CONSULTA_DETALLE_OFERTA_CONSUMO, me);
					edtTextoMonto.setText("");
					edtTextoMonto.setHint(Tools.formatCredit(session.getImporteInicial()));
					btnmeInteresa.setBackgroundResource(R.drawable.bmovil_btn_meinteresa);
				}
			}
		}
	};

	private void configurarPantalla() {
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(lblTexto2, true);
		gTools.scale(lblTexto3, true);
		gTools.scale(lblTexto5, true);
		gTools.scale(lblTexto6, true);
		gTools.scale(lblTexto7, true);
		gTools.scale(lblTexto8, true);
		gTools.scale(btnmeInteresa);

	}


	public void goBack() {
		Session session = Session.getInstance(SuiteApp.appContext);
		//ARR
		paso1OperacionMap.put("evento_paso1", "event46");
		paso1OperacionMap.put("&&products", "promociones;detalle consumo");
		paso1OperacionMap.put("eVar12", "paso1:rechazo oferta consumo");

		TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
		// TODO Auto-generated method stub
		//detalleOfertaConsumoDelegate.confirmarRechazoatras();
		detalleOfertaConsumoDelegate.realizaOperacion(Server.RECHAZO_OFERTA_CONSUMO, me);
		session.setImporteMinimo("");
		session.setImporteParcial("");
		session.setImporteInicial("");
		session.setChanged(false);
//        resetValues();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);

	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		detalleOfertaConsumoDelegate.analyzeResponse(operationId, response);
	}

	public void setValues(){
		Session session = Session.getInstance(SuiteApp.appContext);

		if(!session.isChanged()) {
			session.setImporteMinimo(detalleOfertaConsumoDelegate.getOfertaConsumo().getMontoMinimo());
			session.setImporteInicial(detalleOfertaConsumoDelegate.getOfertaConsumo().getImporte());
		}

		if(!session.getOfertaDelSimulador()) {
			montoMinimo = Double.parseDouble(Tools.formatAmount(session.getImporteMinimo()));
			montoInicial = Double.parseDouble(Tools.formatAmount(session.getImporteInicial()));
		}

		String lblText7=Tools.formatAmount(detalleOfertaConsumoDelegate.getOfertaConsumo().getImporte(), false);
		SpannableString texto7 = new SpannableString(lblText7);
		texto7.setSpan(new UnderlineSpan(), 0, lblText7.length(), 0);

		lblTexto7.setText(texto7);

		edtTextoMonto.setHint(Tools.formatCredit(session.getImporteInicial()));

		String lblText5="Con un pago máximo de \n"+ Tools.formatAmount(detalleOfertaConsumoDelegate.getOfertaConsumo().getPagoMenFijo(), false)+" durante "+detalleOfertaConsumoDelegate.getOfertaConsumo().getPlazoDes()+".\n Tasa mensual del "+detalleOfertaConsumoDelegate.getOfertaConsumo().getTasaMensual()+"% sin IVA.";

		String pagoMen=Tools.formatAmount(detalleOfertaConsumoDelegate.getOfertaConsumo().getPagoMenFijo(),false);
		String quincenas=detalleOfertaConsumoDelegate.getOfertaConsumo().getPlazoDes();
		String tasaMen=detalleOfertaConsumoDelegate.getOfertaConsumo().getTasaMensual()+"%";
		//String comision="Sin comisiones.";
		detalleOfertaConsumoDelegate.formatoFechaCatMostrar();
		String cat=detalleOfertaConsumoDelegate.getOfertaConsumo().getCat();
		String catFormat= cat.substring(0,4);
		lblTexto3.setText("Tasa Fija anual "+detalleOfertaConsumoDelegate.getOfertaConsumo().getTasaAnual()+"% sin IVA con un CAT "+catFormat+"%\n");
		lblTexto2.setText("Fecha de Cálculo al "+detalleOfertaConsumoDelegate.getfechaCatVisual()+"\n *El IVA puede variar en cada pago.");
		int IpagoMen=lblText5.indexOf(pagoMen);
		int Iquincenas=lblText5.indexOf(quincenas);
		int ItasaMen=lblText5.indexOf(tasaMen);
		//int Icomision=lblText5.indexOf(comision);

		SpannableString content = new SpannableString(lblText5);
		content.setSpan(new ForegroundColorSpan(Color.rgb(134,200,45)),IpagoMen,IpagoMen+pagoMen.length(),0);
		content.setSpan(new ForegroundColorSpan(Color.rgb(0,0,0)),Iquincenas,Iquincenas+quincenas.length(),0);
		content.setSpan(new ForegroundColorSpan(Color.rgb(0,0,0)),ItasaMen,ItasaMen+tasaMen.length(),0);
		//content.setSpan(new ForegroundColorSpan(Color.rgb(9,79,164)),Icomision,Icomision+comision.length(),0);

		lblTexto5.setText(content);

	}

	public void resetValues(){
		btnmeInteresa.setBackgroundResource(R.drawable.bmovil_btn_meinteresa);
		edtTextoMonto.setText("");
		edtTextoMonto.setHint(Tools.formatCredit(Session.getInstance(DetalleConsumoViewController.this).getImporteInicial()));

	}





	//Methods For Montos Parciales OOROZCO 20160112


	private void setFormattedText(){
		isSettingText = true;
		String text = typedString.toString();

		if(mAcceptCents){
			text += "00";
		}

		edtTextoMonto.setText(Tools.formatAmountFromServer(text));
	}

	public void reset() {
		this.isResetting = true;
		this.typedString=new StringBuffer();
		this.isResetting = false;
	}


	@Override
	public void onUserInteraction() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();
	}
}
