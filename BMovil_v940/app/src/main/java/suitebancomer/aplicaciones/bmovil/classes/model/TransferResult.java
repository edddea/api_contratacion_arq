package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;


import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

@SuppressWarnings("serial")
public class TransferResult implements ParsingHandler {

	private String folio;
	private String comision;
	private String saldoActual;
	private String fecha;
	private String hora;
	private String cuentaRetiro;
	private String clave;
	private String codigoTrans;

	public String getCodigoTrans() {
		return codigoTrans;
	}

	public void setCodigoTrans(String codigoTrans) {
		this.codigoTrans = codigoTrans;
	}


	public String getFolio() {
		return folio;
	}

	public String getComision() {
		return comision;
	}
	
	/**
	 * @return the saldoActual
	 */
	public String getSaldoActual() {
		return saldoActual;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @return the hora
	 */
	public String getHora() {
		return hora;
	}
	
	public String getCuentaRetiro() {
		return cuentaRetiro;
	}

	public void setCuentaRetiro(String cuentaRetiro) {
		this.cuentaRetiro = cuentaRetiro;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		this.folio = parser.parseNextValue("FO");
		this.cuentaRetiro = parser.parseNextValue("AS");
		String tempComision = parser.parseNextValue("CD", false);
		if (!Tools.isEmptyOrNull(tempComision))
			this.comision = tempComision;
		this.saldoActual = parser.parseNextValue("IM");
		this.fecha = parser.parseNextValue("FE");
		this.hora = parser.parseNextValue("HR");
		
		try{
			this.clave = parser.parseNextValue("CR",false);
		}catch(Exception e){
		//this.codigoTrans=parser.parseNextValue("TR");	
			
		}
		this.codigoTrans=parser.parseNextValue("TR");
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}


	

}
