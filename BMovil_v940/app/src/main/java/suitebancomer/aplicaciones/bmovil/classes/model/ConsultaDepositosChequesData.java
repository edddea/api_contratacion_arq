package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class ConsultaDepositosChequesData implements ParsingHandler {

	private static final long serialVersionUID = 1L;
	private ArrayList<ConsultaDepositosRecibidosChequesEfectivo> movimientos;
	private String estado;
	private String codigoMensaje;
	private String descripcionMensaje;
	private String periodo;
	
	/**
	 * Constructor por defecto.
	 */
	public ConsultaDepositosChequesData() {

		estado = null;
		codigoMensaje = null;
		descripcionMensaje = null;
		periodo = null;
		movimientos = new ArrayList<ConsultaDepositosRecibidosChequesEfectivo>();
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCodigoMensaje() {
		return codigoMensaje;
	}

	public void setCodigoMensaje(String codigoMensaje) {
		this.codigoMensaje = codigoMensaje;
	}

	public String getDescripcionMensaje() {
		return descripcionMensaje;
	}

	public void setDescripcionMensaje(String descripcionMensaje) {
		this.descripcionMensaje = descripcionMensaje;
	}

	public String getPeriodo() {
		return periodo;
	}

	public ArrayList<ConsultaDepositosRecibidosChequesEfectivo> getMovimientos() {
		return movimientos;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {

		estado = parser.parseNextValue("estado");
		codigoMensaje = parser.parseNextValue("codigoMensaje");
		descripcionMensaje = parser.parseNextValue("descripcionMensaje");
		if (estado.equals("OK")) {
			periodo = parser.parseNextValue("periodo");
			JSONObject aux = parser.parserNextObject("arrMovtos");
			JSONArray aux1;
			try {

				aux1 = aux.getJSONArray("ocMovtos");
				for (int i = 0; i < aux1.length(); i++) {
					JSONObject movimientoJSON = aux1.getJSONObject(i);
					
					ConsultaDepositosRecibidosChequesEfectivo cob = new ConsultaDepositosRecibidosChequesEfectivo();
					cob.setNumeroSucursal(movimientoJSON.has("numeroSucursal") ? movimientoJSON.getString("numeroSucursal") : "");
					cob.setNombreSucursal(movimientoJSON.has("nombreSucursal") ? movimientoJSON.getString("nombreSucursal") : "");
					cob.setEstadoSucursal(movimientoJSON.has("estadoSucursal") ? movimientoJSON.getString("estadoSucursal") : "");
					cob.setPoblacionSucursal(movimientoJSON.has("poblacionSucursal") ? movimientoJSON.getString("poblacionSucursal") : "");
					cob.setFecha(movimientoJSON.has("fecha") ? movimientoJSON.getString("fecha") : "");
					cob.setHora(movimientoJSON.has("hora") ? movimientoJSON.getString("hora") : "");
					cob.setImporte(movimientoJSON.has("importe") ? movimientoJSON.getString("importe") : "");
					cob.setBanco(movimientoJSON.has("banco") ? movimientoJSON.getString("banco") : "");
					/***RHO***/
					cob.setNumMovto(movimientoJSON.has("numMovto") ? movimientoJSON.getString("numMovto") : "");
					
					movimientos.add(cob);
				}

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				if(Server.ALLOW_LOG) e.printStackTrace();
			}
		}

	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub

	}

}
