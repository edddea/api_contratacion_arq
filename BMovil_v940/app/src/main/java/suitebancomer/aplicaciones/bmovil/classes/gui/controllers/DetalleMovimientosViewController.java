package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import bancomer.api.common.commons.Constants;

import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MisCuentasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MovimientosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.Movement;
import suitebancomer.aplicaciones.bmovil.classes.model.MovementExtract;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import tracking.TrackingHelper;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

/**
 * 
 * @author Luis Alberto de la Cruz Torres
 *
 */

public class DetalleMovimientosViewController extends BaseViewController implements View.OnClickListener{

	/**
	 * Cuenta a consultar
	 */
	private TextView cuenta;
	
	/**
	 * Descripci�n del movimientos
	 */
	private TextView descripcion;
	
	/**
	 * Importe del movimiento
	 */
	private TextView importe;
	/**
	 * Fecha de aplicación
	 */
	private TextView fechaAplicacion;
	
	/**
	 * Fecha de autorizaci�n
	 */
	private TextView fechaAutorizacion;
	
	MovementExtract resumenMovimientos;
	Movement movimientoSeleccionado;
	ImageButton btnMenu;
	//AMZ
			public BmovilViewsController parentManager;
			//AMZ
	
	/**
	 * Default constructor for this activity
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_detalle_movimientos);
		setTitle(R.string.consultar_movimientos_title, R.drawable.bmovil_consultar_icono);
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
			
				TrackingHelper.trackState("detalle mov", parentManager.estados);

		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());

		cuenta = (TextView) findViewById(R.id.detalle_movimientos_cuenta);
		descripcion = (TextView) findViewById(R.id.detalle_movimientos_descripcion);
		importe = (TextView) findViewById(R.id.detalle_movimientos_importe);
		fechaAplicacion = (TextView) findViewById(R.id.detalle_movimientos_fecha_aplicacion);
		fechaAutorizacion = (TextView) findViewById(R.id.detalle_movimientos_fecha_autorizacion);
		btnMenu = (ImageButton) findViewById(R.id.detalle_movimientos_boton_menu);
		btnMenu.setOnClickListener(this);
		
		resumenMovimientos = ((MovimientosDelegate)parentViewsController.getBaseDelegateForKey(MovimientosDelegate.MOVIMIENTOS_DELEGATE_ID)).getTotalMovementsExtract();
		movimientoSeleccionado = ((MovimientosDelegate)parentViewsController.getBaseDelegateForKey(MovimientosDelegate.MOVIMIENTOS_DELEGATE_ID)).getMovimiento();
		scaleForCurrentScreen();
		llenaDetalleDescripcion();
	}
	
	public void llenaDetalleDescripcion(){
		cuenta.setText(getTipoCuenta(getResources()));
		descripcion.setText(movimientoSeleccionado.getDescription());
		importe.setText(movimientoSeleccionado.getAmount());
		fechaAplicacion.setText(getString(R.string.movementDetail_applicationDate) + " "+ Tools.formatDate(movimientoSeleccionado.getApplicationDate()));
		fechaAutorizacion.setText(getString(R.string.movementDetail_authorizationDate) + " " + Tools.formatDate(movimientoSeleccionado.getAuthorizationDate()));
	}
	
	@Override
	public void onClick(View v) {
		if (v == btnMenu) {
			((BmovilViewsController)parentViewsController).touchMenu();

			parentViewsController.removeDelegateFromHashMap(MovimientosDelegate.MOVIMIENTOS_DELEGATE_ID);
			((BmovilViewsController)parentViewsController).showMenuPrincipal(true);
			parentViewsController.removeDelegateFromHashMap(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID);
		}
	}
	
	public String getTipoCuenta(Resources res)
	{
		String tipoCuenta = "";
		String tipo = resumenMovimientos.getAccountType();
		String numero = resumenMovimientos.getAccountNumber();
        if (Constants.CHECK_TYPE.equals(tipo)) {
        	tipoCuenta = res.getString(R.string.accounttype_check);
        } else if (Constants.CREDIT_TYPE.equals(tipo)) {
        	tipoCuenta = res.getString(R.string.accounttype_credit);
        } else if (Constants.LIBRETON_TYPE.equals(tipo)) {
        	tipoCuenta = res.getString(R.string.accounttype_libreton);
        } else if (Constants.SAVINGS_TYPE.equals(tipo)) {
        	tipoCuenta = res.getString(R.string.accounttype_savings);
        } else if (Constants.EXPRESS_TYPE.equals(tipo)) {
        	tipoCuenta = res.getString(R.string.accounttype_express);
        } else if (Constants.CLABE_TYPE_ACCOUNT.equals(tipo)) {
        	tipoCuenta = res.getString(R.string.accounttype_clabe);
        } else if (Constants.PREPAID_TYPE.equals(tipo)) {
        	tipoCuenta = res.getString(R.string.accounttype_prepaid);
        }
        
        StringBuffer sb = new StringBuffer(tipoCuenta);
        sb.append(" ").append(Tools.hideAccountNumber(numero));
        tipoCuenta = sb.toString();

        return tipoCuenta;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}

	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.layoutRoot));
		guiTools.scale(findViewById(R.id.detalle_movimientos_title), true);
		guiTools.scale(findViewById(R.id.detalle_movimientos_cuenta), true);
		guiTools.scale(findViewById(R.id.detalle_movimientos_descripcion), true);
		guiTools.scale(findViewById(R.id.detalle_movimientos_importe), true);
		guiTools.scale(findViewById(R.id.detalle_movimientos_fecha_aplicacion), true);
		guiTools.scale(findViewById(R.id.detalle_movimientos_fecha_autorizacion), true);
		guiTools.scale(btnMenu);

	}

}
