package suitebancomer.aplicaciones.bmovil.classes.common;

public class ConstantsPropertyList {
	public static final String USERNAME = "username";
	public static final String OK = "Ok";
	public static final String OK_2 = "Ok_2";
	public static final String PROFILE_BASIC = "PROFILE_BASIC";
	public static final String PROFILE_ADVANCED_OCRA = "PROFILE_ADVANCED_OCRA";
	public static final String PROFILE_ADVANCED_DP270 = "PROFILE_ADVANCED_DP270 ";
	public static final String PROFILE_ADVANCED_SOFTTOKEN = "PROFILE_ADVANCED_SOFTTOKEN";
	public static final String PROFILE_CHANGE_BASIC_TO_ADVANCED = "PROFILE_CHANGE_BASIC_TO_ADVANCED";
	public static final String PROFILE_CHANGE_ADVANCED_TO_BASIC = "PROFILE_CHANGE_ADVANCED_TO_BASIC";
	public static final String ACCOUNTS_ONE_EXPRESS = "ACCOUNTS_ONE_EXPRESS";
	public static final String ACCOUNTS_ONE_ACCOUNT = "ACCOUNTS_ONE_ACCOUNT";
	public static final String ACCOUNTS_TDC_IS_MAIN = "ACCOUNTS_TDC_IS_MAIN";
	public static final String ACCOUNTS_MULTIPLE_ACCOUNTS_ZERO_BALANCE = "ACCOUNTS_MULTIPLE_ACCOUNTS_ZERO_BALANCE";
	public static final String ACCOUNTS_ALL_ACCOUNTS = "ACCOUNTS_ALL_ACCOUNTS";
	public static final String STATUS_NOT_A1 = "STATUS_NOT_A1";
	public static final String OPTIONAL_UPDATE = "OPTIONAL_UPDATE";
	public static final String MANDATORY_UPDATE = "MANDATORY_UPDATE";
	public static final String NO_FAST_PAYMENT = "NO_FAST_PAYMENT";
	public static final String FAST_PAYMENT_3 = "FAST_PAYMENT_3";
	public static final String FAST_PAYMENT_5 = "FAST_PAYMENT_5";
	public static final String FAST_PAYMENT_6 = "FAST_PAYMENT_6";
	public static final String OPERACIONES_CAT_AU = "OPERACIONES_CAT_AU";
	public static final String LISTA_TELEFONICAS = "LISTA_TELEFONICAS";
	public static final String CORREO_EJEMPLO = "CORREO_EJEMPLO";
	
	//---DATOS register()
	public static final String REGISTER_RROR = "registerERROR";
	
	//---DATOS activate()
	public static final String ACTIVATE_ERROR = "activateERROR";
	
	//---DATOS login() 
	public static final String LOGIN_ERROR = "loginError";
	public static final String LOGIN_OK = "loginOk";
	public static final String VERSION_C1 = "versionC1";
	public static final String VERSION_C4 = "versionC4";
	public static final String VERSION_C5 = "versionC5";
	public static final String VERSION_C8 = "versionC8";
	public static final String VERSION_TA1 = "versionTA_1";
	public static final String VERSION_TA2 = "versionTA_2";
	public static final String VERSION_TA3 = "versionTA_3";
	public static final String VERSION_TA4 = "versionTA_4";
	public static final String VERSION_TA5 = "versionTA_5";
	public static final String VERSION_DM1 = "versionDM_1";
	public static final String VERSION_DM2 = "versionDM_2";
	public static final String VERSION_DM3 = "versionDM_3";
	public static final String VERSION_DM4 = "versionDM_4";
	public static final String VERSION_DM5 = "versionDM_5";
	public static final String VERSION_SV1 = "versionSV_1";
	public static final String VERSION_SV2 = "versionSV_2";
	public static final String VERSION_SV3 = "versionSV_3";
	public static final String VERSION_SV4 = "versionSV_4";
	public static final String VERSION_SV5 = "versionSV_5";
	public static final String VERSION_SPEI1 = "versionSPEI_1";
	public static final String VERSION_SPEI2 = "versionSPEI_2";
	public static final String VERSION_SPEI3 = "versionSPEI_3";
	public static final String VERSION_SPEI4 = "versionSPEI_4";
	public static final String VERSION_SPEI5 = "versionSPEI_5";
	public static final String VERSION_OTRA1 = "versionOTRA_1";
	public static final String VERSION_OTRA2 = "versionOTRA_2";
	public static final String VERSION_OTRA3 = "versionOTRA_3";
	public static final String VERSION_OTRA4 = "versionOTRA_4";
	public static final String COMPLETA_VERSION1 = "CompletaVersion_1";
	public static final String COMPLETA_VERSION2 = "CompletaVersion_2";
	public static final String COMPLETA_VERSION3 = "CompletaVersion_3";
	public static final String COMPLETA_VERSION4 = "CompletaVersion_4";
	public static final String NO_VERSION1 = "noVersion_1";
	public static final String NO_VERSION2 = "noVersion_2";
	public static final String NO_VERSION3 = "noVersion_3";
	public static final String NO_VERSION4 = "noVersion_4";
	public static final String NO_VERSION5 = "noVersion_5";
	public static final String NO_VERSION6 = "noVersion_6";
	public static final String DATE_LOGIN1 = "dateLogin_1";
	public static final String DATE_LOGIN2 = "dateLogin_2";
	public static final String DATE_LOGIN3 = "dateLogin_3";
	public static final String DATE_LOGIN4 = "dateLogin_4";
	public static final String DATE_LOGIN5 = "dateLogin_5";
	 
	 //---DATOS getMovements()
	public static final String MOVEMENTSLI_OK1 = "MovementsLI_OK_1";
	public static final String MOVEMENTSLI_OK2 = "MovementsLI_OK_2";
	public static final String MOVEMENTSLI_ERROR = "MovementsLI_ERROR";
	public static final String MOVEMENTSLI_AVISO = "MovementsLI_AVISO";
	public static final String MOVEMENTSTC_OK = "MovementsTC_OK";
	public static final String NO_MOVIMIENTOS1 = "noMovimientos_1";
	public static final String NO_MOVIMIENTOS2 = "noMovimientos_2";
	public static final String NO_MOVIMIENTOS_ERROR = "noMovimientosERROR";
	 
	 //---DATOS consultaEstatusDelServicio()
	public static final String CONESTATUS_SERVICIO_OK = "conEstausServicioOK";
	 
	 //---DATOS cambiaPerfil()
	public static final String CAMBIO_PERFIL = "cambioPerfil";
	 
	 //---DATOS preregistrarServicio()
	public static final String PREG_SERVICIO_OK = "pRegServicioOK";
	public static final String PREG_SERVIIO_ERROR = "pRegServicioERROR";
	 
	 //---DATOS consultaBeneficiario()
	public static final String CONBENEFICIARIO_OK = "conBeneficiarioOK";
	 
	 //---DATOS selfTransfer()
	public static final String SELFTRANFER_OK = "selfTransferOK";
	public static final String SELFTRANFER_ERROR1 = "selfTransferERROR_1";
	public static final String SELFTRANFER_ERROR2 = "selfTransferERROR_2";
	public static final String SELFTRANFER_ERROR3 = "selfTransferERROR_3";
	public static final String SELFTRANFER_ERROR4 = "selfTransferERROR_4";
	 
	 //---DATOS performAirtimePurchase()
	public static final String PERFPURCHASE_OK = "perfPurchaseOK";
	public static final String PERFPURCHASE_ERROR1 = "perfPurchaseERROR_1";
	public static final String PERFPURCHASE_ERROR2 = "perfPurchaseERROR_2";
	public static final String PERFPURCHASE_ERROR3 = "perfPurchaseERROR_3";
	public static final String PERFPURCHASE_ERROR4 = "perfPurchaseERROR_4";
	 
	 //---DATOS servicePayment()
	public static final String SERVICEPAYMENT_OK = "servicePaymentOK";
	public static final String SERVICEPAYMENT_ERROR1 = "servicePaymentERROR_1";
	public static final String SERVICEPAYMENT_ERROR2 = "servicePaymentERROR_2";
	public static final String SERVICEPAYMENT_ERROR3 = "servicePaymentERROR_3";
	public static final String SERVICEPAYMENT_ERROR4 = "servicePaymentERROR_4";
	 
	 //---DATOS bancomerTransfer()
	public static final String BANCTRANSFER_OK = "bancTransferOK";
	public static final String BANCTRANSFER_AVISO = "bancTransferAVISO";
	public static final String BANCTRANSFER_ERROR1 = "bancTransferERROR_1";
	public static final String BANCTRANSFER_ERROR2 = "bancTransferERROR_2";
	public static final String BANCTRANSFER_ERROR3 = "bancTransferERROR_3";
	 
	 //---DATOS altaOperacionSinTarjeta()
	public static final String ALTAOPSIN_TRAJETAOK = "altaOpSinTarjetaOK";
	public static final String ALTAOPSIN_TRAJETAERROR1 = "altaOpSinTarjetaERROR_1";
	public static final String ALTAOPSIN_TRAJETAERROR2 = "altaOpSinTarjetaERROR_2";
	public static final String ALTAOPSIN_TRAJETAERROR3 = "altaOpSinTarjetaERROR_3";
	public static final String ALTAOPSIN_TRAJETAERROR4 = "altaOpSinTarjetaERROR_4";
	 
	 //---DATOS externalTransfer()
	public static final String EXTERNALTRANSFER_OK1 = "externalTransferOK_1";
	public static final String EXTERNALTRANSFER_OK2 = "externalTransferOK_2";
	public static final String EXTERNALTRANSFER_OK3 = "externalTransferOK_3";
	public static final String EXTERNALTRANSFER_AVISO = "externalTransferAVISO";
	public static final String EXTERNALTRANSFER_ERROR1 = "externalTransferERROR_1";
	public static final String EXTERNALTRANSFER_ERROR2 = "externalTransferERROR_2";
	public static final String EXTERNALTRANSFER_ERROR3 = "externalTransferERROR_3";
	public static final String EXTERNALTRANSFER_ERROR4 = "externalTransferERROR_4";
	 
	 //---DATOS changePassword()
	public static final String CHANGEPASSWORD_OK = "changePasswordOK";
	 
	 //---DATOS favoritePaymentOperation()
	public static final String FAVORITEPAYMENTOP_ERROR = "favoritePaymentOpERROR";
	public static final String TIPOCFPAGOS_CIE1 = "tipoCFPagosCIE_1";
	public static final String TIPOCFPAGOS_CIE2 = "tipoCFPagosCIE_2";
	public static final String TIPOCFPAGOS_CIE3 = "tipoCFPagosCIE_3";
	public static final String TIPOCFPAGOS_CIE4 = "tipoCFPagosCIE_4";
	public static final String TIPOCFPAGOS_CIE5 = "tipoCFPagosCIE_5";
	public static final String TIPOCFPAGOS_CIE6 = "tipoCFPagosCIE_6";
	public static final String TIPOCF_TIEMPOAIRE1 = "tipoCFTiempoAire_1";
	public static final String TIPOCF_TIEMPOAIRE2 = "tipoCFTiempoAire_2";
	public static final String TIPOCF_TIEMPOAIRE3 = "tipoCFTiempoAire_3";
	public static final String TIPOCF_TIEMPOAIRE4 = "tipoCFTiempoAire_4";
	public static final String TIPOCF_TIEMPOAIRE5 = "tipoCFTiempoAire_5";
	public static final String TIPOCF_TIEMPOAIRE6 = "tipoCFTiempoAire_6";
	public static final String TIPOCF_TIEMPOAIRE7 = "tipoCFTiempoAire_7";
	public static final String TIPOCFOTROS_BBVA1 = "tipoCFOtrosBBVA_1";
	public static final String TIPOCFOTROS_BBVA2 = "tipoCFOtrosBBVA_2";
	public static final String TIPOCFOTROS_BBVA3 = "tipoCFOtrosBBVA_3";
	public static final String TIPOCFOTROS_BBVA4 = "tipoCFOtrosBBVA_4";
	public static final String TIPOCFOTROS_BBVA5 = "tipoCFOtrosBBVA_5";
	public static final String TIPOCFOTROS_BANCOS1 = "tipoCFOtrosBancos_1";
	public static final String TIPOCFOTROS_BANCOS2 = "tipoCFOtrosBancos_2";
	public static final String TIPOCFOTROS_BANCOS3 = "tipoCFOtrosBancos_3";
	public static final String TIPOCFOTROS_BANCOS4 = "tipoCFOtrosBancos_4";
	public static final String TIPOCFOTROS_BANCOS5 = "tipoCFOtrosBancos_5";
	public static final String TIPOCFOTROS_BANCOS6 = "tipoCFOtrosBancos_6";
	public static final String TIPOCFOTROS_BANCOS7 = "tipoCFOtrosBancos_7";
	public static final String TIPOCFOTROS_BANCOS8 = "tipoCFOtrosBancos_8";
	public static final String TIPOCFOTROS_BANCOS9 = "tipoCFOtrosBancos_9";
	public static final String TIPOCFOTROS_BANCOS10 = "tipoCFOtrosBancos_10";
	public static final String TIPOCFOTROS_BANCOS11 = "tipoCFOtrosBancos_11";
    public static final String TIPOCFOTROS_BANCOS12 = "tipoCFOtrosBancos_12";
    public static final String TIPOCFOTROS_BANCOS13 = "tipoCFOtrosBancos_13";
    public static final String TIPOCF_DINEROMOVIL1 = "tipoCFDineroMovil_1";
    public static final String TIPOCF_DINEROMOVIL2 = "tipoCFDineroMovil_2";
    public static final String TIPOCF_DINEROMOVIL3 = "tipoCFDineroMovil_3";
    public static final String TIPOCF_DINEROMOVIL4 = "tipoCFDineroMovil_4";
    public static final String TIPOCF_DINEROMOVIL5 = "tipoCFDineroMovil_5";
    public static final String TIPOCF_DINEROMOVIL6 = "tipoCFDineroMovil_6";
    public static final String TIPOCF_CEXPRESS1 = "tipoCFCExpress_1";
    public static final String TIPOCF_CEXPRESS2 = "tipoCFCExpress_2";
    public static final String TIPOCF_CEXPRESS3 = "tipoCFCExpress_3";
    public static final String TIPOCF_CEXPRESS4 = "tipoCFCExpress_4";
    public static final String TIPOCF_TRAJETASCREDITO1 = "tipoCFTarjetasCredito_1";
    public static final String TIPOCF_TRAJETASCREDITO2 = "tipoCFTarjetasCredito_2";
    public static final String TIPOCF_TRAJETASCREDITO3 = "tipoCFTarjetasCredito_3";
	 
	 //---DATOS consultaOperacionesSinTarjeta()
    public static final String VIGENTES_ERROR = "vigentesERROR";
    public static final String VIGENTES_OK1 = "vigentesOK_1";
    public static final String VIGENTES_OK2 = "vigentesOK_2";
    public static final String VIGENTES_OK3 = "vigentesOK_3";
    public static final String VIGENTES_OK4 = "vigentesOK_4";
    public static final String VIGENTES_OK5 = "vigentesOK_5";
    public static final String CANCELADOS_ERROR = "canceladosERROR";
    public static final String CANCELADOS_OK1 = "canceladosOK_1";
    public static final String CANCELADOS_OK2 = "canceladosOK_2";
    public static final String VENCIDOS_ERROR = "vencidosERROR";
    public static final String VENCIDOS_OK1 = "vencidosOK_1";
    public static final String VENCIDOS_OK2 = "vencidosOK_2";
    public static final String VENCIDOS_OK3 = "vencidosOK_3";
	 
	 //---DATOS bajaOperacionSinTarjeta()
    public static final String BAJAOPSINTARJETAOK = "bajaOpSinTarjetaOK";
	 
	 //---DATOS getPaymentCodeData()
    public static final String GETPAYMENTCODEDATAOK = "getPaymentCodeDataOK";
    public static final String GETPAYMENTCODEDATAERROR = "getPaymentCodeDataERROR";
	 
	 //---DATOS consultaTransferenciasDeOtrosBancos()
    public static final String TIPOCUENTA = "tipoCuenta";
    public static final String PERIODO_1 = "periodo_1";
    public static final String PERIODO_2 = "periodo_2";
    public static final String PERIODO_3 = "periodo_3";
	 
	 //---DATOS consultaTransferenciasClientesBancomer()
    public static final String CONTRANSCLIENTBANCOMER = "conTransClientBancomer";
	 
	 //---DATOS consultaTransferenciasOtrosBancos()
    public static final String CONOTROSBANCOSPERIDO_1 = "conOtrosBancosPerido_1";
    public static final String CONOTROSBANCOSPERIDO_2 = "conOtrosBancosPerido_2";
    public static final String CONOTROSBANCOSPERIDO_3 = "conOtrosBancosPerido_3";
    public static final String CONOTROSBANCOSERROR = "conOtrosBancosERROR";
	 
	 //---DATOS consultaTransferenciasMisCuentas()
    public static final String CONTRANSMISCUENTASPERIODO_1 = "conTransMisCuentasPeriodo_1";
    public static final String CONTRANSMISCUENTASPERIODO_2 = "conTransMisCuentasPeriodo_2";
    public static final String CONTRANSMISCUENTASPERIODO_3 = "conTransMisCuentasPeriodo_3";
    public static final String CONTRANSMISCUENTASERROR = "conTransMisCuentasERROR";
	 
	 //---DATOS consultaTransferenciasCuentaBBVA()
    public static final String CONTRANSCUENTABBVA_PERIODO1 = "conTransCuentaBBVA_Periodo1";
    public static final String CONTRANSCUENTABBVA_PERIODO2 = "conTransCuentaBBVA_Periodo2";
    public static final String CONTRANSCUENTABBVA_PERIODO3 = "conTransCuentaBBVA_Periodo3";
    public static final String CONTRANSCUENTABBVA_ERROR = "conTransCuentaBBVA_ERROR";
    public static final String CONTRANSCUENTABBVA_AVISO = "conTransCuentaBBVA_AVISO";
	 
	 //---DATOS consultaPagoServicios()
    public static final String CONPAGOSERVPERIODO_1 = "conPagoServPeriodo_1";
    public static final String CONPAGOSERVPERIODO_2 = "conPagoServPeriodo_2";
    public static final String CONPAGOSERVPERIODO_3 = "conPagoServPeriodo_3";
    public static final String CONPAGOSERVERROR = "conPagoServERROR";
	 
	 //---DATOS consultaDepositoEfectivo()
    public static final String CONDEPEFECTIVOOK = "conDepEfectivoOK";
	 
	 //---DATOS consultaDepositoCheques()
    public static final String CONDEPCHEQUESERROR = "conDepChequesERROR";
    public static final String CONDEPCHEQUESTIPOCUENTAERROR = "conDepChequesTipoCuentaERROR";
    public static final String CONDEPCHEQUESTIPOCUENTAAVISO = "conDepChequesTipoCuentaAVISO";
    public static final String CONDEPCHEQUESPERIODO_1 = "conDepChequesPeriodo_1";
    public static final String CONDEPCHEQUESPERIODO_2 = "conDepChequesPeriodo_2";
    public static final String CONDEPCHEQUESPERIODO_3 = "conDepChequesPeriodo_3";
	 
	 //---DATOS consultaDepositoChequesDetalle()
    public static final String CONDEPCHEQUEDETALLEOK = "conDepChequeDetalleOK";
	 
	 //---DATOS consultaDepositosBBVABancomerYEfectivo()
    public static final String CONDEPBBVAEFECTIVOAVISO = "conDepBBVAEfectivoAVISO";
    public static final String CONDEPBBVAEFECTIVOTIPOCUENTAERROR = "conDepBBVAEfectivoTipoCuentaERROR";
    public static final String CONDEPBBVAEFECTIVOPERIODO_ERROR_1 = "conDepBBVAEfectivoPeriodo_ERROR_1";
    public static final String CONDEPBBVAEFECTIVOPERIODO_ERROR_2 = "conDepBBVAEfectivoPeriodo_ERROR_2";
    public static final String CONDEPBBVAEFECTIVOPERIODO_ERROR_3 = "conDepBBVAEfectivoPeriodo_ERROR_3";
    public static final String CONDEPBBVAEFECTIVOPERIODO_1 = "conDepBBVAEfectivoPeriodo_1";
    public static final String CONDEPBBVAEFECTIVOPERIODO_2 = "conDepBBVAEfectivoPeriodo_2";
    public static final String CONDEPBBVAEFECTIVOPERIODO_3 = "conDepBBVAEfectivoPeriodo_3";
	 
	 //---DATOS consultaDepositosBBVABancomerYEfectivoDetalle()
    public static final String CONDEPBBVAEFECTIVODETALLEOK = "conDepBBVAEfectivoDetalleOK";
	 
	 //---DATOS solictarAlertas()
    public static final String SOLICITARALERTASOK_1 = "solicitarAlertasOK_1";
    public static final String SOLICITARALERTASOK_2 = "solicitarAlertasOK_2";
	 
	 //---DATOS consultaComision()
    public static final String CONSULTACOMISIONOK = "consultaComisionOK";
	 
	 //---DATOS consultaCIE()
    public static final String CONSULTACIE_OK = "consultaCIE_OK";
	 
	 //---DATOS actualizarPregistradoAFrecuente()
    public static final String ACTPREGISTRADOFRECUENTEOK = "actPregistradoFrecuenteOK";
	 
	 //---DATOS consultaTarjetaParaContratacion()
    public static final String CONTARJETACONTRATACIONOK = "conTarjetaContratacionOK";
	 
	 //---DATOS cambioTelefonoAsociado()
    public static final String CAMBIOTELASOCIADOOK = "cambioTelAsociadoOK";
	 
	 //---DATOS cambioCuentaAsociada()
    public static final String CAMBIOCUENTAASOCIADAOK_1 = "cambioCuentaAsociadaOk_1";
    public static final String CAMBIOCUENTAASOCIADAOK_2 = "cambioCuentaAsociadaOk_2";
	 
	 //---DATOS suspenderCancelarServicio()
    public static final String SUSPENDERCANCELARSERVOK = "suspenderCancelarServOK";
	 
	 //---DATOS actualizarCuentas()
    public static final String ACTUALIZACUENTAAVISO = "actualizaCuentaAVISO";
    public static final String ACTUALIZACUENTAOK = "actualizaCuentaOK";
	 
	 //---DATOS consultarLimites()
    public static final String CONSULTARLIMITESOK = "consultarLimitesOk";
	 
	 //---DATOS cambiarLimites()
    public static final String CAMBIARLIMITESOK = "cambiarLimitesOK";
	 
	 //--DATOS consultaEstatusMantenimiento()
	 public static final String CONESTATUSMANTENIMIENTOOK = "conEstatusMantenimientoOK";
    public static final String CONESTATUSMANTENIMIENTOOK_0 = "conEstatusMantenimientoOK_0";
    public static final String CONESTATUSMANTENIMIENTOOK_1 = "conEstatusMantenimientoOK_1";
    public static final String CONESTATUSMANTENIMIENTOOK_2 = "conEstatusMantenimientoOK_2";
    public static final String CONESTATUSMANTENIMIENTOOK_3 = "conEstatusMantenimientoOK_3";
    public static final String CONESTATUSMANTENIMIENTOOK_4 = "conEstatusMantenimientoOK_4";
    public static final String CONESTATUSMANTENIMIENTOOK_5 = "conEstatusMantenimientoOK_5";
    public static final String CONESTATUSMANTENIMIENTOOK_6 = "conEstatusMantenimientoOK_6";
    public static final String CONESTATUSMANTENIMIENTOOK_7 = "conEstatusMantenimientoOK_7";
    public static final String MANTENIMIENTOTIPOINSTRUMENTO = "mantenimientoTipoInstrumento";
    public static final String MANTENIMIENTOPERFIL = "mantenimientoPerfil";
	 
	 //---DATOS desbloqueoContrasena()
    public static final String DESBLOQUEOCONTRASENAOK = "desbloqueoContrasenaOK";
	 
	 //---DATOS realizarActivacion()
    public static final String REALIZARACTIVACIONOK = "realizarActivacionOK";
	 
	 //---DATOS contratacionBmovilAlertas()
    public static final String CONTRATACIONBMOVILALERTAOK_1 = "contratacionBmovilAlertaOK_1";
    public static final String CONTRATACIONBMOVILALERTAOK_2 = "contratacionBmovilAlertaOK_2";
    public static final String CONTRATACIONBMOVILALERTAERROR = "contratacionBmovilAlertaERROR";
	 
	 //---DATOS consultarTerminosSesion()
    public static final String CONSTERMINOSESIONOK = "consTerminoSesionOK";
	 
	 //---DATOS consultarTerminos()
    public static final String CONSULTARTERMINOSOK = "consultarTerminosOK";
	 
	 //---DATOS configurarCorreo()
    public static final String CONFIGURARCORREOERROR = "configurarCorreoERROR";
    public static final String CONFIGURARCORREOOK = "configurarCorreoOK";
	 
	 //---DATOS enviarCorreo()
    public static final String ENVIARCORREOERROR = "enviarCorreoERROR";
	 
	 //---DATOS finalizarContratacionAlertas()
    public static final String FINCONTRALERTAERROR = "finContrAlertaERROR";
    public static final String FINCONTRALERTAOK_1 = "finContrAlertaOK_1";
    public static final String FINCONTRALERTAOK_2 = "finContrAlertaOK_2";
	 
	 //---DATOS requestSpeiTermsAndConditions()
    public static final String REQUESTSPEITERMSCONDITIONSOK_1 = "requestSpeiTermsConditionsOK_1";
    public static final String REQUESTSPEITERMSCONDITIONSOK_2 = "requestSpeiTermsConditionsOK_2";
	 
	 //---DATOS consultThirdsAccountBBVA()
    public static final String CONTHIRDSACCOUNTBBVA_OK = "conThirdsAccountBBVA_OK";
	 
	 //---DATOS consultaTarjetaST()
    public static final String CONSULTATARJETAST_OK1 = "consultaTarjetaST_OK1";
    public static final String CONSULTATARJETAST_OK2 = "consultaTarjetaST_OK2";
    public static final String CONSULTATARJETAST_OK3 = "consultaTarjetaST_OK3";
    public static final String CONSULTATARJETAST_OK4 = "consultaTarjetaST_OK4";
    public static final String CONSULTATARJETAST_OK5 = "consultaTarjetaST_OK5";
    public static final String CONSULTATARJETAST_OK6 = "consultaTarjetaST_OK6";
    public static final String CONSULTATARJETAST_ERROR = "consultaTarjetaST_ERROR";
    public static final String CONSULTATARJETASTVALIDACIONALERTAS = "consultaTarjetaSTvalidacionAlertas";
    public static final String CONSULTATARJETASTINDICADORCONTRATACION = "consultaTarjetaSTindicadorContratacion";
    public static final String CONSULTATARJETASTSWITCHENROLAMIENTO = "consultaTarjetaSTswitchEnrolamiento";
    public static final String CONSULTATARJETASTDISPOSITIVOFISICO = "consultaTarjetaSTdispositivoFisico";
    public static final String CONSULTATARJETASTESTATUSDISPOSITIVO = "consultaTarjetaSTestatusDispositivo";
	 
	 //---DATOS autenticacionST()
    public static final String AUTENTICACIONST_OK = "autenticacionST_OK";
	 
	 //---DATOS contratacionEnrolamiento()
    public static final String CONTENROLAMIENTOOK = "contEnrolamientoOK";
    public static final String CONTENROLAMIENTOERROR_1 = "contEnrolamientoERROR_1";
    public static final String CONTENROLAMIENTOERROR_2 = "contEnrolamientoERROR_2";
    public static final String CONTENROLAMIENTOERROR_3 = "contEnrolamientoERROR_3";
	 
	 //---DATOS finalizarContratacionST()
    public static final String FINCONTRATACIONST_ERROR1 = "finContratacionST_ERROR1";
    public static final String FINCONTRATACIONST_ERROR2 = "finContratacionST_ERROR2";
	 
	 //---DATOS solicitudST()
    public static final String SOLICITUDST_ERROR = "solicitudST_ERROR";
	 
	 //---DATOS mantenimientoAlertas()
    public static final String MANTENIMIENTOALERTASOK = "mantenimientoAlertasOK";
	 
	 //---DATOS consultaDetalleOferta()
    public static final String CONDETALLEOFERTAOK = "conDetalleOfertaOK";
	 
	 //---DATOS aceptaOfertaILC()
    public static final String ACEPTAOFERTAILC_OK1 = "aceptaOfertaILC_OK1";
    public static final String ACEPTAOFERTAILC_OK2 = "aceptaOfertaILC_OK2";
    public static final String ACEPTAOFERTAILC_ERROR = "aceptaOfertaILC_ERROR";
	 
	 //---DATOS exitoOfertaILC()
    public static final String EXITOOFERTAILC_ERROR = "exitoOfertaILC_ERROR";
	 
	 //---DATOS rechazoOfertaILC()
    public static final String RECHAZOOFERTAILC_OK = "rechazoOfertaILC_OK";
	 
	 //---DATOS consultaDetalleOfertaEFI()
    public static final String CONDETALLEOFERTAEFI_OK = "conDetalleOfertaEFI_OK";
	 
	 //---DATOS consultaAceptaOfertaEFI()
    public static final String CONACEPTAOFERTAEFI_OK1 = "conAceptaOfertaEFI_OK1";
    public static final String CONACEPTAOFERTAEFI_OK2 = "conAceptaOfertaEFI_OK2";
	public static final String CONACEPTAOFERTAEFI_ERROR = "conAceptaOfertaEFI_ERROR";
	 
	 //---DATOS exitoOfertaEFI()
	public static final String EXITOOFERTAEFI_ERROR = "exitoOfertaEFI_ERROR";
	 
	 //---DATOS simuladorEFI()
	public static final String SIMULADOREFI_OK = "simuladorEFI_OK";
	 
	 //---DATOS consultaDetalleConsumo()
	public static final String CONDETALLECONSUMOOK_1 = "conDetalleConsumoOK_1";
	public static final String CONDETALLECONSUMOOK_2 = "conDetalleConsumoOK_2";
	 
	 //---DATOS polizaConsumo()
	public static final String POLIZACONSUMOOK = "polizaConsumoOK";
	 
	 //---DATOS terminosConsumo()
	public static final String TERMINOSCONSUMOOK = "terminosConsumoOK";
	 
	 //---DATOS exitoConsumo()
	public static final String EXITOCONSUMOOK_1 = "exitoConsumoOK_1";
	public static final String EXITOCONSUMOOK_2 = "exitoConsumoOK_2";
	public static final String EXITOCONSUMOOK_3 = "exitoConsumoOK_3";
	public static final String EXITOCONSUMOOK_4 = "exitoConsumoOK_4";
	public static final String EXITOCONSUMOERROR = "exitoConsumoERROR";
	 
	 //---DATOS domiciliacionConsumo()
	public static final String DOMICILIACIONCONSUMOOK = "domiciliacionConsumoOK";
	 
	 //---DATOS contratoConsumo()
	public static final String CONTRATOCONSUMOOK = "contratoConsumoOK";
	 
	 //---DATOS rechazoOfertaConsumo()
	public static final String RECHAZOOFERTACONSUMOOK = "rechazoOfertaConsumoOK";
	 
	 //---DATOS envioSMSOfertaConsumo()
	public static final String ENVIOSMSOFERTACONSUMOERROR = "envioSMSOfertaConsumoERROR";
	 
	 //---DATOS consultaInterbancarios()
	public static final String CONINTERBANCARIOSOK_1 = "conInterbancariosOK_1";
	public static final String CONINTERBANCARIOSOK_2 = "conInterbancariosOK_2";
	 
	 //---DATOS importesTDC()
	public static final String IMPORTESTDC_ERRORCOMUNICACION1 = "importesTDC_ErrorComunicacion1";
	public static final String IMPORTESTDC_ERRORCOMUNICACION2 = "importesTDC_ErrorComunicacion2";
	public static final String IMPORTESTDC_ERRORCONTRATO = "importesTDC_ErrorContrato";
	public static final String IMPORTESTDC_OK = "importesTDC_OK";
	public static final String IMPORTESTDC_OK2 = "importesTDC_OK2";

	//---DATOS ConsultaSPEI()
	public static final String CONSULTASPEI_OK_P0 = "consultaSPEI_OK_P0";
	public static final String CONSULTASPEI_OK_P1 = "consultaSPEI_OK_P1";
	public static final String CONSULTASPEI_OK_P2 = "consultaSPEI_OK_P2";
	public static final String CONSULTASPEI_ERROR = "consultaSPEI_ERROR";
	public static final String ENVIARCORREOSPEI_OK = "enviarCorreoSPEI_OK";
	public static final String ENVIARCORREOSPEI_ERROR = "enviarCorreoSPEI_ERROR";
}
