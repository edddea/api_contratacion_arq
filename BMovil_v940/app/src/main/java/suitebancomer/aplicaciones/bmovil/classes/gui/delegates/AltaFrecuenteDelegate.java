package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import static suitebancomer.aplicaciones.bmovil.classes.common.Tools.removeSpecialCharacters;

import java.util.ArrayList;
import java.util.Hashtable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.AltaFrecuenteViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.ActualizarPreregistroResult;
import suitebancomer.classes.gui.controllers.BaseViewController;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class AltaFrecuenteDelegate extends DelegateBaseOperacion{
	/**
	 * 
	 */
	public static final long ALTA_FRECUENTE_DELEGATE_ID = -6927594692349945408L;
	private AltaFrecuenteViewController viewController;
	private DelegateBaseOperacion delegateOperacion;
	boolean esOk;
	private String folioActualizacionPreregistro;
	
	AltaFrecuenteDelegate altaFreqDel;
	
	public AltaFrecuenteDelegate() {
		
	}
	
	public AltaFrecuenteDelegate(DelegateBaseOperacion delegateOperacion) {
		this.setDelegateOperacion(delegateOperacion);
	}
	
	public void realizaAltaDeFrecuente(){
		//
		boolean valido = true;
		String contrasena = "";
		String nip = "";
		String asm = "";
		//String tarjeta = ""; 
		//String cvv = null;
		//frecuente correo electronico
		if(viewController.getNombreCorto().length() == 0 || ( !viewController.getNombreCorto().isEmpty()
				&& viewController.getNombreCorto().charAt(0)==' ')){
		//if(viewController.getNombreCorto().length() == 0){
			String mensaje = viewController.getString(R.string.altafrecuente_nombrecorto_short_error);
			viewController.showInformationAlert(mensaje);
			valido = false;
			//freceunte correo electronico
		}else if(!viewController.getCorreoElectronico().trim().isEmpty() && !validaCorreoElectronico(viewController.getCorreoElectronico().toString())){
				String mensaje = viewController.getString(R.string.altafrecuente_correoelectronico_short_error);
				viewController.showInformationAlert(mensaje);
				valido = false;
			//freceunte correo electronico termina
		}else if (mostrarContrasenia()) {
			contrasena = viewController.pideContrasena();
			if (contrasena.equals("")) {
				String mensaje = viewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_componenteContrasena);
				mensaje += ".";
				viewController.showInformationAlert(mensaje);
				valido = false;
			} else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
				String mensaje = viewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.PASSWORD_LENGTH;
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_componenteContrasena);
				mensaje += ".";
				viewController.showInformationAlert(mensaje);
				valido = false;
			}
		}else if (mostrarNIP()) {
			nip = viewController.pideNIP();
			if (nip.equals("")) {
				String mensaje = viewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_componenteNip);
				mensaje += ".";
				viewController.showInformationAlert(mensaje);
				valido = false;
			} else if (nip.length() != Constants.NIP_LENGTH) {
				String mensaje = viewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.NIP_LENGTH;
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_componenteNip);
				mensaje += ".";
				viewController.showInformationAlert(mensaje);
				valido = false;
			}
		}else
		if (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) {
			asm = viewController.pideASM();
			if (asm.equals("")) {
				String mensaje = viewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				switch (consultaTipoInstrumentoSeguridad()) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
				mensaje += ".";
				viewController.showInformationAlert(mensaje);
				valido = false;
			} else if (asm.length() != Constants.ASM_LENGTH) {
				String mensaje = viewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.ASM_LENGTH;
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				switch (consultaTipoInstrumentoSeguridad()) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}	
				mensaje += ".";
				viewController.showInformationAlert(mensaje);
				valido = false;
			}
		}
		
		if(!valido)
			return;
		
		String newToken = null;
		if(tokenAMostrar() != TipoOtpAutenticacion.ninguno && 
				consultaTipoInstrumentoSeguridad() == TipoInstrumento.SoftToken && 
				SuiteApp.getSofttokenStatus())
			newToken = loadOtpFromSofttoken(tokenAMostrar());
		if(null != newToken)
			asm = newToken;
		
		//String idToken = (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) ? "T" : "";
		String idToken = (Session.getInstance(SuiteApp.appContext).getClientProfile() == Perfil.avanzado) ? "T" : "";
		
		String va = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.altaFrecuente, 
				Session.getInstance(SuiteApp.appContext).getClientProfile());
		//Alta de frecuente
		 Hashtable<String,String> paramTable = getParametrosAltaFrecuentes();

		paramTable.put("NI", nip);
		paramTable.put("OT", asm);
		paramTable.put("NP", Tools.isEmptyOrNull(contrasena) ? "" : contrasena );
		paramTable.put("VA",Tools.isEmptyOrNull(va) ? "" : va );
		//paramTable.put("TP","");
		//paramTable.put("OA","");


		String nombreCorto = viewController.getNombreCorto();
		//freceunte correo electronico
		String correoElectronico = viewController.getCorreoElectronico();
		if(esPreregistrado()){
			paramTable.put("order","NT*NP*IU*TE*C1*FN*NK*CA*BF*RF*NI*CV*OT*VA");
			paramTable.put("FN","I380");
			 paramTable.put("NK", removeSpecialCharacters(nombreCorto));
			 //JAIG SI CHECAR PARAMS
			 doNetworkOperation(Server.ACTUALIZAR_PREREGISTRO_FRECUENTE, paramTable,false,new ActualizarPreregistroResult(), Server.isJsonValueCode.NONE.NONE, viewController);
		}else{
			 paramTable.put("order","NT*NP*IU*TE*DE*CC*CA*BF*CB*IM*RF*CP*TP*OA*AP*IT*NI*CV*OT*VA*CE");//freceunte correo electronico agrega CE");
			 paramTable.put(ServerConstants.PARAMS_TEXTO_IT, idToken);
			 paramTable.put("DE", removeSpecialCharacters(nombreCorto));
			//freceunte correo electronico
			 paramTable.put("CE", correoElectronico);

			//JAIG SI CHECAR PARAMS
			 doNetworkOperation(Server.ALTA_FRECUENTE, paramTable,false,null, Server.isJsonValueCode.NONE, viewController);
		}
	}
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson, ParsingHandler handler,Server.isJsonValueCode isJsonValueCode, BaseViewController caller) {
		((BmovilViewsController)getViewController().getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller);
	}
	
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){
			esOk = true;
			if(response.getResponse() instanceof ActualizarPreregistroResult){
				folioActualizacionPreregistro = ((ActualizarPreregistroResult)response.getResponse()).getFolio();
			}
			//((BmovilViewsController)viewController.getParentViewsController()).showResultadosViewController(this, -1, -1);
			//Mejoras Bmovil
			altaFreqDel = this;
			viewController.showInformationAlert( R.string.alert_alta_frecuente_exitosa, new OnClickListener() {							
				@Override
				public void onClick(DialogInterface dialog, int which) {
					//((BmovilViewsController)viewController.getParentViewsController()).showResultadosViewController(delegateOperacion, -1, -1);
					//recuente correo electronico
					delegateOperacion.setAliasFrecuente(viewController.getNombreCorto());
					delegateOperacion.setCorreoFrecuente(viewController.getCorreoElectronico());

					((BmovilViewsController)viewController.getParentViewsController()).showResultadosViewControllerWithoutFreq(delegateOperacion, -1, -1);
				}
			});
		}else if(response.getStatus() == ServerResponse.OPERATION_WARNING){
			esOk = false;
			((BmovilViewsController)viewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
		}
	}
	
	@Override
	public ArrayList<Object> getDatosTablaResultados() {
		ArrayList<Object> datosResultados = getDatosTablaAltaFrecuentes();
		ArrayList<String> registro;
		registro = new ArrayList<String>();
		registro.add(viewController.getString(R.string.altafrecuente_nombrecorto));
		registro.add(viewController.getNombreCorto());
		datosResultados.add(registro);
		if(!Tools.isEmptyOrNull(folioActualizacionPreregistro) && esPreregistrado()){
			registro = new ArrayList<String>();
			registro.add(viewController.getString(R.string.result_lista_folio));
			registro.add(folioActualizacionPreregistro);
			datosResultados.add(registro);			
		}
		return datosResultados;
	}
	
	@Override
	public int getOpcionesMenuResultados() {
		return 0;
	}
	
	@Override
	public String getTextoTituloResultado() {	
		return viewController.getString(R.string.altafrecuente_titulo_resultados);
	}
	@Override
	public int getColorTituloResultado() {
		return delegateOperacion.getColorTituloResultado();
	}
	
	@Override
	public int getTextoEncabezado() {
		return R.string.altafrecuente_titulo;
	}
	
	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.bmovil_alta_frecuente_icono;
	}

	@Override
	public ArrayList<Object> getDatosTablaAltaFrecuentes() {
		return delegateOperacion.getDatosTablaAltaFrecuentes();
	}

	public AltaFrecuenteViewController getViewController() {
		return viewController;
	}

	public void setViewController(AltaFrecuenteViewController viewController) {
		this.viewController = viewController;
	}

	public DelegateBaseOperacion getDelegateOperacion() {
		return delegateOperacion;
	}

	public void setDelegateOperacion(DelegateBaseOperacion delegateOperacion) {
		this.delegateOperacion = delegateOperacion;
	}
		
	@Override
	public Hashtable<String, String> getParametrosAltaFrecuentes() {
		return delegateOperacion.getParametrosAltaFrecuentes();
	}

	@Override
	public boolean mostrarContrasenia() {
		boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.altaFrecuente, 
								Session.getInstance(SuiteApp.appContext).getClientProfile());
		return value;
	}
	
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.altaFrecuente,
									Session.getInstance(SuiteApp.appContext).getClientProfile());
		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.mostrarNIP execution.", ex);
			tipoOTP = null;
		}		
		return tipoOTP;
	}

	@Override
	public boolean mostrarNIP() {
		boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.altaFrecuente, 
									Session.getInstance(SuiteApp.appContext).getClientProfile());
		return value;
	}
	
	@Override
	public boolean mostrarCVV() {
		Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile(); 
		boolean value =  Autenticacion.getInstance().mostrarCVV(Constants.Operacion.altaFrecuente, perfil);
		return value;
	}
	
	@Override
	public String getEtiquetaCampoNip() {
		return viewController.getString(R.string.confirmation_nip);
	}
	
	@Override
	public String getTextoAyudaNIP() {
		return viewController.getString(R.string.confirmation_ayudaNip);
	}
	
	@Override
	public String getEtiquetaCampoContrasenia() {
		return viewController.getString(R.string.confirmation_contrasena);
	}
	
	@Override
	public String getEtiquetaCampoOCRA() {
		return viewController.getString(R.string.confirmation_ocra);
	}
	
	@Override
	public String getEtiquetaCampoDP270() {
		return viewController.getString(R.string.confirmation_dp270);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenActivado() {
		return viewController.getString(R.string.confirmation_softtokenActivado);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenDesactivado() {
		return viewController.getString(R.string.confirmation_softtokenDesactivado);
	}

	public String getTextoAyudaInstrumentoSeguridad(Constants.TipoInstrumento tipoInstrumento) {
		Constants.TipoOtpAutenticacion tokenAMostrar = tokenAMostrar();
		if(getContext()==null && viewController!=null){
			setContext(viewController);
		}
		return getTextoAyudaInstrumentoSeguridad(tipoInstrumento,SuiteApp.getSofttokenStatus(),tokenAMostrar);
	}

	/*
	@Override
	public String getTextoAyudaInstrumentoSeguridad(Constants.TipoInstrumento tipoInstrumento) {
		Constants.TipoOtpAutenticacion tokenAMostrar = tokenAMostrar();
		if (tokenAMostrar == Constants.TipoOtpAutenticacion.ninguno) {
			return "";
		} else if (tokenAMostrar == Constants.TipoOtpAutenticacion.registro) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (SuiteApp.getSofttokenStatus()) {
						return viewController.getString(R.string.confirmation_ayudaRegistroSofttokenActivado);
					} else {
						return viewController.getString(R.string.confirmation_ayudaRegistroSofttokenDesactivado);
					}
				case OCRA:
					return viewController.getString(R.string.confirmation_ayudaRegistroOCRA);
				case DP270:
					return viewController.getString(R.string.confirmation_ayudaRegistroDP270);
				case sinInstrumento:
				default:
					return "";
			}
		} else if (tokenAMostrar == Constants.TipoOtpAutenticacion.codigo) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (SuiteApp.getSofttokenStatus()) {
						return viewController.getString(R.string.confirmation_ayudaCodigoSofttokenActivado);
					} else {
						return viewController.getString(R.string.confirmation_ayudaCodigoSofttokenDesactivado);
					}					
				case OCRA:
					return viewController.getString(R.string.confirmation_ayudaCodigoOCRA);
				case DP270:
					return viewController.getString(R.string.confirmation_ayudaCodigoDP270);
				case sinInstrumento:
				default:
					return "";
			}
		}
		return "";
	}

	*/


	public TipoInstrumento consultaTipoInstrumentoSeguridad() {
		Constants.TipoInstrumento tipoInstrumentoSeguridad = null;
		String instrumento = Session.getInstance(SuiteApp.appContext)
				.getSecurityInstrument();
		
		if (instrumento.equals(Constants.IS_TYPE_DP270)) {
			
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
			
		} else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
			
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
			
		} else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)) {
			
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
			
		} else {
			
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
			
		}
		
		return tipoInstrumentoSeguridad;
	}
	
	public boolean validaDatos(){
		boolean valido = true;
		String contrasena = null;
		String nip = null;
		String asm = null;
		
		//String cvv = null;
		if(viewController.getNombreCorto().length() == 0){
			String mensaje = viewController.getString(R.string.altafrecuente_nombrecorto_short_error);
			viewController.showInformationAlert(mensaje);
			valido = false;
		}else
		if (mostrarContrasenia()) {
			contrasena = viewController.pideContrasena();
			if (contrasena.equals("")) {
				String mensaje = viewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_componenteContrasena);
				mensaje += ".";
				viewController.showInformationAlert(mensaje);
				valido = false;
			} else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
				String mensaje = viewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.PASSWORD_LENGTH;
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_componenteContrasena);
				mensaje += ".";
				viewController.showInformationAlert(mensaje);
				valido = false;
			}
		}else
		if (mostrarNIP()) {
			nip = viewController.pideNIP();
			if (nip.equals("")) {
				String mensaje = viewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_componenteNip);
				mensaje += ".";
				viewController.showInformationAlert(mensaje);
				valido = false;
			} else if (nip.length() != Constants.NIP_LENGTH) {
				String mensaje = viewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.NIP_LENGTH;
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_componenteNip);
				mensaje += ".";
				viewController.showInformationAlert(mensaje);
				valido = false;
			}
		}else
		if (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) {
			asm = viewController.pideASM();
			if (asm.equals("")) {
				String mensaje = viewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				switch (consultaTipoInstrumentoSeguridad()) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
				mensaje += ".";
				viewController.showInformationAlert(mensaje);
				valido = false;
			} else if (asm.length() != Constants.ASM_LENGTH) {
				String mensaje = viewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.ASM_LENGTH;
				mensaje += " ";
				mensaje += viewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				switch (consultaTipoInstrumentoSeguridad()) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}	
				mensaje += ".";
				viewController.showInformationAlert(mensaje);
				valido = false;
			}
		}
		return valido;		
	}
	
	protected boolean esPreregistrado(){
		boolean esPagoDeServicio = (delegateOperacion instanceof PagoServiciosDelegate);
		boolean esPreregistrado = false; 
		if(esPagoDeServicio){
			esPreregistrado = ((PagoServiciosDelegate)delegateOperacion).pagoServicio.isPreregistered();
		}		
		return esPreregistrado;
	}

	@Override
	public String loadOtpFromSofttoken(TipoOtpAutenticacion tipoOTP) {
		return loadOtpFromSofttoken(tipoOTP, delegateOperacion);
	}
	//freceunte correo electronico
	private boolean validaCorreoElectronico(String email){
		String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

		Pattern pattern = Pattern.compile(PATTERN_EMAIL);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

}
