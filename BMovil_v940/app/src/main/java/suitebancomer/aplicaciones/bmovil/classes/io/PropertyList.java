package suitebancomer.aplicaciones.bmovil.classes.io;

import java.util.ResourceBundle;

import android.util.Log;


public class PropertyList {
	public String read(String propertyToRecover)
    {
		ResourceBundle myResources = ResourceBundle.getBundle("suitebancomer.aplicaciones.bmovil.classes.io.propertiesFile");
		String prop = myResources.getString(propertyToRecover);
		if(Server.ALLOW_LOG) Log.w("DATOS PROPERTY", prop);
		return prop;
    }
}
