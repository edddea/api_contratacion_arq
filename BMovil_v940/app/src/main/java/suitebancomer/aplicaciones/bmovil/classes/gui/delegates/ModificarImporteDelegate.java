package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

import android.util.Log;
import android.view.View;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConsultarFrecuentesViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.DetalleEFIViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ModificarImporteEFIViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.ImporteEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaEFI;
import suitebancomer.aplicaciones.bmovil.classes.model.Payment;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.AmountField;

public class ModificarImporteDelegate extends DelegateBaseOperacion{
	public final static long MODIFICAR_IMPORTE_DELEGATE_ID = 0xc4810fdda06505e4L;
	BaseViewController viewController;
	OfertaEFI ofertaEFI;
	public ArrayList<Object> listaDatos;
	Account accountSeleccionada; 
	public String montoSolicitado="";
	
	public AmountField txtImporte; //declaracion nueva
	ModificarImporteDelegate modificarImporteDelegate; //nueva igual y no se requiere
	
	ModificarImporteEFIViewController modificarImporteEFIViewController;
	
	AmountField amountField;
	
	DetalleEFIViewController detalleEFIViewController;
	DetalleOfertaEFIDelegate detalleOfertaEFIDelegate;
	
	public int index;
   public int i;

	public ModificarImporteDelegate(OfertaEFI ofertaEFI){
		this.ofertaEFI= ofertaEFI;
	}
	
	public void realizaOperacion(int idOperacion,
			BaseViewController baseViewController) {
		if (idOperacion == Server.SIMULADOR_EFI){
			Session session = Session.getInstance(SuiteApp.appContext);
			String user = session.getUsername();
			String ium= session.getIum();
			Hashtable<String, String> params = new Hashtable<String, String>();
			//params.put("operacion", "SimulacionEFI");
			params.put("numeroCelular", user);
			params.put("nuevoImporte",ofertaEFI.getMontoDisponible());// es lo q escribio en el texview
			params.put("plazoMeses",ofertaEFI.getPlazoMeses());
			String tasaAnual= ofertaEFI.getTasaAnual().replace(".", ",");
			params.put("tasaAnual",tasaAnual);
			String comisionEFIPorcentaje= ofertaEFI.getComisionEFIPorcentaje().replace(".", ",");
			params.put("comisionEFIPorcentaje",comisionEFIPorcentaje);
			params.put("IUM",ium);
			//JAIG
			doNetworkOperation(Server.SIMULADOR_EFI, params,true, new ImporteEFI(),isJsonValueCode.NONE,
					baseViewController);
		}
	
	}
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson,ParsingHandler handler, isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if( viewController != null)
			((BmovilViewsController)viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,false);
	}
	
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (operationId == Server.SIMULADOR_EFI) {
			ImporteEFI importeEFI= (ImporteEFI)response.getResponse();
			ofertaEFI.setPagoMensualSimulador(importeEFI.getPagoMensual());
			ofertaEFI.setComisionEFIMonto(importeEFI.getComisionEFIMonto());
			((BmovilViewsController)viewController.getParentViewsController()).showDetalleEFI(ofertaEFI,null);
		}
	}
	
	@Override
	public ArrayList<Object> getDatosHeaderTablaFrecuentes() {
		ArrayList<Object> registros = new ArrayList<Object>();
		registros.add("");
		registros.add("Tipo de cuenta");
		registros.add("Cuenta");
		return registros;
	}
	
	@Override
	public ArrayList<Object> getDatosTablaFrecuentes() {
		Session session = Session.getInstance(SuiteApp.appContext);
		listaDatos = new ArrayList<Object>();
		ArrayList<Object> registro;
		Account[] accounts = session.getAccounts();
		i=0;
		
		 for (Account account : accounts) {
			 	registro = new ArrayList<Object>();
			 if(account.getType().compareTo(Constants.CREDIT_TYPE)!=0){	
			 	registro.add(account);
			 	registro.add(account.getNombreTipoCuenta(SuiteApp.appContext.getResources()));
			 	registro.add(Tools.hideAccountNumber(account.getNumber()));
			 	listaDatos.add(registro);
			 	
			 	} 
		 }
		
		
		 
		return listaDatos;
		
		
		
		}
	
	@SuppressWarnings("unchecked")
	@Override
	public void operarFrecuente(int indexSelected){
		ArrayList<Object> arrAccount = null;
		arrAccount = ((ModificarImporteEFIViewController)viewController).listaSeleccion.getLista();		
		if (arrAccount != null && arrAccount.size() > 0) {
		accountSeleccionada = (Account)((ArrayList<Object>)arrAccount.get(indexSelected)).get(0);
		ofertaEFI.setAccountDestino(accountSeleccionada);
		ofertaEFI.setTipoCuenta(accountSeleccionada); //**Tipo de cuenta
		
		
		}
	}
	
	
	
	
	public boolean guardarDatos(String importeSolicitado){
		boolean esOK = true;
		String msgError ="";
		Double montoTDC= Double.parseDouble(ofertaEFI.getMontoDisponibleFijo()+"00");
		String importeSol= importeSolicitado.replace(".", "");
		Double montoSoicitado=Double.parseDouble(importeSol);
		if(montoSoicitado>montoTDC){
			msgError="Lo sentimos, su monto máximo es de " + Tools.formatAmount((ofertaEFI.getMontoDisponibleFijo()+"00"), false); 
			esOK= false;
			//amountField.reset();
		
		}
		if(montoSoicitado<1){
			msgError="Lo sentimos, el monto mínimo es de " + Tools.formatAmount("100", false);
			esOK= false;
		}		
		if(!esOK){
			viewController.showInformationAlert(msgError);
		}
		if(esOK){
			String importe = Tools.formatAmountForServer(((ModificarImporteEFIViewController)viewController).txtImporte.getAmount());
			String imp=importe.substring(0, importe.length()-2);
			ofertaEFI.setMontoDisponible(imp);
	
			
		}
		return esOK;
		
	}
	
	/*public void montoMostrar(){
		Double montoTDC= Double.parseDouble(ofertaEFI.getMontoDispTDC());
		Double montodisp= Double.parseDouble(ofertaEFI.getMontoDisponible());
		if(montodisp>montoTDC){
			montoMostrar=ofertaEFI.getMontoDispTDC();
		}else{
			montoMostrar= ofertaEFI.getMontoDisponible();
		}
	}*/
		
	
	public OfertaEFI getOfertaEFI() {
		return ofertaEFI;
	}

	public void setOfertaEFI(OfertaEFI ofertaEFI) {
		this.ofertaEFI = ofertaEFI;
	}


	public BaseViewController getViewController() {
		return viewController;
	}
	public void setViewController(BaseViewController viewController) {
		this.viewController = viewController;
	}
	
           
	
	public int getIndexcuenta() {
		Session session = Session.getInstance(SuiteApp.appContext);
		Account[] accounts = session.getAccounts();
		for(int i=0; i<accounts.length; i++){
			if (accounts[i].getNumber().equals(ofertaEFI.getAccountDestino().getNumber())){
				index=i;
				break;
			}
		}		
		// TODO Auto-generated method stub
		return index;
	}
		 
	
	public void setIndexcuenta(int ind) {
				this.index=ind;
		
	}
	
	public DetalleOfertaEFIDelegate detalleEfiDelegate() {
		return detalleOfertaEFIDelegate;
	}

	 
}
