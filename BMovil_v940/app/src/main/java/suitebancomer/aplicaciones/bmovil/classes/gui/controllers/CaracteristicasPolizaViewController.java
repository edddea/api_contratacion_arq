package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import bancomer.api.common.commons.Constants;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import tracking.TrackingHelper;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.GestureDetector;
import android.view.View;
import android.view.View.OnTouchListener;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class CaracteristicasPolizaViewController extends BaseViewController {
	/**
	 * Campo de texto para terminos de uso.
	 */
	private WebView wvpoliza;
	private GestureDetector gestureDetector;
	private AtomicBoolean mPreventAction = new AtomicBoolean(false);

	
	//AMZ
	private BmovilViewsController parentManager;
	
	//ARR
		Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
		
	TextView lblTitulo;
	String poliza;
	String termino;
	String contrato;
	String domiciliacion;
	public CaracteristicasPolizaViewController() {
		wvpoliza = null;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_consumo_poliza);
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		setTitle(R.string.bmovil_promocion_title_consumo, R.drawable.icono_consumo);
		
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
					
				TrackingHelper.trackState("poliza", parentManager.estados);
				//AMZ	
		
		poliza = (String)this.getIntent().getExtras().get(Constants.TERMINOS_DE_USO_EXTRA);
		termino= (String)this.getIntent().getExtras().get(Constants.TERMINOS_DE_USO_CONSUMO);
		contrato= (String)this.getIntent().getExtras().get(Constants.CONTRATO_CONSUMO);
		domiciliacion= (String)this.getIntent().getExtras().get(Constants.DOMICILIACION_CONSUMO);
		
		/******/
		ViewGroup scroll = (ViewGroup) findViewById(R.id.body_layout);
		ViewGroup parent = (ViewGroup) scroll.getParent(); 
		parent.removeView(scroll);
		
		LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(R.layout.layout_bmovil_consumo_poliza, parent, true);
        
		findViews();
		scaleToScreenSize();
		/******/
		
		WebSettings settings = wvpoliza.getSettings();
		String result;
		if(null != poliza){
		    wvpoliza.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			wvpoliza.loadUrl(poliza);
			wvpoliza.getSettings().setBuiltInZoomControls(true);
			double factor = GuiTools.getScaleFactor();
			wvpoliza.setInitialScale((int) (35 * factor));
		}else if(null != termino){
		 	//wvpoliza.loadData(termino, "text/html", "utf-8"); //ORIGINAL
		   	result=parse(termino);			
			settings.setUseWideViewPort(true);
			double factor = GuiTools.getScaleFactor();
		    wvpoliza.setInitialScale((int) (35 * factor));//122
			wvpoliza.getSettings().setBuiltInZoomControls(true);
			wvpoliza.loadData(result, "text/html", "utf-8");				
		}else if(null != contrato){
			//wvpoliza.loadData(contrato, "text/html", "utf-8"); //original
			result=parse(contrato);
			settings.setUseWideViewPort(true);
			double factor = GuiTools.getScaleFactor();
		    wvpoliza.setInitialScale((int) (122*factor));
			wvpoliza.loadData(result, "text/html", "utf-8");
		}else if(null != domiciliacion){
			/*wvpoliza.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			wvpoliza.loadData(domiciliacion, "text/html", "utf-8");*/
			result=parse(domiciliacion);
			settings.setUseWideViewPort(true);
			double factor = GuiTools.getScaleFactor();
			    wvpoliza.setInitialScale((int) (122*factor));
			wvpoliza.loadData(result, "text/html", "utf-8");
		}
		/****/
        gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
        	 @Override
             public boolean onDoubleTap(MotionEvent e) {
        		 //Toast.makeText(MainActivity.this, "double tap", Toast.LENGTH_SHORT).show();
                 mPreventAction.set(true);
                 return true;
            }
        	@Override
        	public boolean onDoubleTapEvent(MotionEvent e) {
                mPreventAction.set(true);
        		return true;
        	}
        	
        	@Override
            public boolean onSingleTapUp(MotionEvent e) {
                mPreventAction.set(true); // this is the key! this would block double tap to zoom to fire
                return false;
            }
        	 
         });
        gestureDetector.setIsLongpressEnabled(true);
        wvpoliza.setOnTouchListener(new OnTouchListener(){
            @TargetApi(Build.VERSION_CODES.FROYO)
			@Override
            public boolean onTouch(View v, MotionEvent event) {
            	int pointId = 0;            	
            	if(Build.VERSION.SDK_INT > 7){
                   	int index = (event.getAction() & MotionEvent.ACTION_POINTER_INDEX_MASK)>>MotionEvent.ACTION_POINTER_INDEX_SHIFT;
            		pointId = event.getPointerId(index);
            	}
                if (pointId == 0){
                	gestureDetector.onTouchEvent(event);
                	if (mPreventAction.get()){
                		mPreventAction.set(false);
                		return true;
                	}
                	return wvpoliza.onTouchEvent(event);
                }
                else return true;
             }
        });
        /****/

	}
	
	private String parse(String source){
		StringBuilder data = new StringBuilder();
		data.append(source);
		data.insert(data.indexOf("head")+5, "<meta name=\"viewport\" content=\"height=device-height,width=1280\"/>");		
		return data.toString().replace("divstyle", "div style").replace("tdcolspan","td colspan")
				.replace("tdwidth", "td width")
				.replace("solid1px", "solid 1px")
				.replace("width:801px", " width:800px")
				.replace("</body></html><html><head><title>Contrato - Contrato Credit&oacute;n N&oacute;mina sin Seguro</title></head><body>","")
				;		
	}

	/**
	 * Busca las referencias a las vistas.
	 */
	private void findViews() {
		wvpoliza = (WebView)findViewById(R.id.poliza);
		lblTitulo=(TextView)findViewById(R.id.lblTitulo);
		if(null != poliza)
			lblTitulo.setText(R.string.bmovil_contratoconsumo_titlepoliza);
		else if(null != termino)
			lblTitulo.setText(R.string.bmovil_contratoconsumo_terminosconsumo_web);
		else if(null != contrato)
			lblTitulo.setText(R.string.bmovil_contratoconsumo_link1);	
		else if(null != domiciliacion)
			lblTitulo.setText(R.string.bmovil_contratoconsumo_link4);	
	}
	
	/**
	 * Escala las vistas para acomodarse a la pantalla actual.
	 */
	private void scaleToScreenSize() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.layoutBaseContainer));
		guiTools.scale(findViewById(R.id.lblTitulo), true);
		guiTools.scale(wvpoliza);
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	/*@Override
	protected void onResume() {
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(this);
		super.onResume();
	}*/
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);

	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();

	}
	
	

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		float touchX = ev.getX();
		float touchY = ev.getY();
		int[] webViewPos = new int[2];
		
		wvpoliza.getLocationOnScreen(webViewPos);
		
		float listaSeleccionX2 = webViewPos[0] + wvpoliza.getMeasuredWidth();
		float listaSeleccionY2 = webViewPos[1] + wvpoliza.getMeasuredHeight();
		
		if ((touchX >= webViewPos[0] && touchX <= listaSeleccionX2) &&
			(touchY >= webViewPos[1] && touchY <= listaSeleccionY2)) {
			wvpoliza.getParent().requestDisallowInterceptTouchEvent(true);
		}
		
		return super.dispatchTouchEvent(ev);
	}

}
