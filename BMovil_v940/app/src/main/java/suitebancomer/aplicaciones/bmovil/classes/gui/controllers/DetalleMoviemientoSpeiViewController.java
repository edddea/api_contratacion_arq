package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;






import android.widget.TextView;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaInterbancariosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaDatosViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;
import tracking.TrackingHelper;
	

public class DetalleMoviemientoSpeiViewController extends BaseViewController implements View.OnClickListener{

	private ConsultaInterbancariosDelegate delegate;

	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	//Mejoras Bmovil
	private ImageButton compartirButton;
	private TextView instruccionesResultados;
	private TextView textoEspecial;
			
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_detalle_spei);
		setTitle(R.string.bmovil_consultar_interbancario_detalle_title, R.drawable.bmovil_consultar_icono);
		
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		TrackingHelper.trackState("detalle interbancaria", parentManager.estados);
		
		
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		delegate = (ConsultaInterbancariosDelegate)parentViewsController.getBaseDelegateForKey(ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID);
		delegate.setOwnerController(this);
		setDelegate(delegate);
		findViews();
		setListaDatos(delegate.getDetalleSpei());
		setListaClave(delegate.getDatosClave());
		configurarGUI();
		configurarCompartir();
	}

	public void configurarCompartir(){
		String instrucciones = delegate.getTextoAyudaResultados();
		if (instrucciones.equals("")) {
			instruccionesResultados.setVisibility(View.GONE);
		} else {
			instruccionesResultados.setText(instrucciones);
		}
		//Mejoras Bmovil
		if(instruccionesResultados.getText() == ""){
			compartirButton.setVisibility(View.INVISIBLE);
		}

		textoEspecial.setText(this.getString(R.string.bmovil_consultar_interbancario_text_opciones_simple));
		textoEspecial.setVisibility(View.VISIBLE);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		parentViewsController.setActivityChanging(false);
		delegate.setOwnerController(this);
	}
	
	
	public void configurarGUI(){
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scale(instruccionesResultados, true);
		guiTools.scale(textoEspecial, true);
		guiTools.scale(compartirButton);
	}
	
	public void setListaDatos(ArrayList<Object> datos) {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(findViewById(R.id.lstDatos));
		
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		ListaDatosViewController listaDatos = new ListaDatosViewController(this, params, parentViewsController);
		listaDatos.setTitulo(R.string.bmovil_consultar_interbancarios_titulo_detalle);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.showLista();
		LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.lstDatos);
		layoutListaDatos.addView(listaDatos);
	}
	
	public void setListaClave(ArrayList<Object> datos) {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(findViewById(R.id.lstClave));
		
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		
		ListaSeleccionViewController listaSeleccion = new ListaSeleccionViewController(this, params,
					parentViewsController);
			//listaSeleccion.setDelegate(delegate);
		    listaSeleccion.setDelegate(new BaseDelegate());
		    listaSeleccion.setFijarLista(true);
			listaSeleccion.setNumeroColumnas(1);
			listaSeleccion.setEncabezado(null);
			listaSeleccion.setLista(datos);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(datos.size());
			listaSeleccion.setExisteFiltro(false);
			listaSeleccion.cargarTabla();
			LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.lstClave);
			layoutListaDatos.addView(listaSeleccion);
		
	}

	public void findViews(){
		textoEspecial = (TextView)findViewById(R.id.lblTexto);
		instruccionesResultados = (TextView)findViewById(R.id.resultado_instrucciones);
		compartirButton = (ImageButton)findViewById(R.id.resultados_compartir);
		compartirButton.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_bmovil_resultados, menu);
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		int opcionesMenu = delegate.getOpcionesMenuResultados();
		boolean showMenu = false;
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_SMS) != DelegateBaseOperacion.SHOW_MENU_SMS) {
			menu.removeItem(R.id.save_menu_sms_button);
		} else {
			showMenu = true;
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_EMAIL) != DelegateBaseOperacion.SHOW_MENU_EMAIL) {
			menu.removeItem(R.id.save_menu_email_button);
		} else {
			//showMenu = true;
			menu.removeItem(R.id.save_menu_email_button);//TODO remover para mostrar menu completo
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_PDF) != DelegateBaseOperacion.SHOW_MENU_PDF) {
			menu.removeItem(R.id.save_menu_pdf_button);
		} else {
			//showMenu = true;
			menu.removeItem(R.id.save_menu_pdf_button);//TODO remover para mostrar menu completo
		}
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_FRECUENTE) != DelegateBaseOperacion.SHOW_MENU_FRECUENTE) {
			menu.removeItem(R.id.save_menu_frecuente_button);
		} else {
			showMenu = true;
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_RAPIDA) != DelegateBaseOperacion.SHOW_MENU_RAPIDA) {
			menu.removeItem(R.id.save_menu_rapida_button);
		} else {
			//showMenu = true;
			menu.removeItem(R.id.save_menu_rapida_button);//TODO remover para mostrar menu completo
		}
		if ((opcionesMenu & DelegateBaseOperacion.SHOW_MENU_BORRAR) != DelegateBaseOperacion.SHOW_MENU_BORRAR) {
			menu.removeItem(R.id.save_menu_borrar_button);
		} else {
			showMenu = true;
		}
		
		return showMenu;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.save_menu_sms_button:
				delegate.enviaSMS();
				return true;
			case R.id.save_menu_email_button:
				delegate.enviaEmail();
				return true;
			default:
				return false;
		}
	}
	
	public BroadcastReceiver createBroadcastReceiver() {
    	
	     return new BroadcastReceiver() {
			@Override
		     public void onReceive(Context ctx, Intent intent) {
		    	 
		    	 String toastMessage;
		    	 
			     switch (getResultCode()) {
				     case Activity.RESULT_OK:
					     toastMessage = getString(R.string.sms_success);    
					     break;
				     case SmsManager.RESULT_ERROR_NO_SERVICE: //"SMS: No service"
				    	 toastMessage = getString(R.string.sms_error_noService);
				    	 break;
				     case SmsManager.RESULT_ERROR_NULL_PDU: //"SMS: Null PDU"
				    	 toastMessage = getString(R.string.sms_error_nullPdu);
				    	 break;
				     case SmsManager.RESULT_ERROR_RADIO_OFF: //"SMS: Radio off"
				    	 toastMessage = getString(R.string.sms_error_radioOff);
				    	 break;
				     case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
				     default:
				    	 toastMessage = getString(R.string.sms_error);
				    	 break;
			     }
			     
			     ocultaIndicadorActividad();
			     Toast.makeText(getBaseContext(), toastMessage,Toast.LENGTH_SHORT).show();
		     }
	     };
	}
	
	public void onMenuButtonClicked(View sender) {
		((BmovilViewsController)getParentViewsController()).showMenuPrincipal(true);
	}

	@Override
	public void onClick(View v) {
		if (v==compartirButton){
			openOptionsMenu();
		}
	}
}
