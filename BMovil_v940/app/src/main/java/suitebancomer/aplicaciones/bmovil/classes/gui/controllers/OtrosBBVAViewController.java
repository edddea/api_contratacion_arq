package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.AbstractContactMannager;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.InterbancariosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.OtrosBBVADelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaOtrosBBVA;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.AmountField;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import tracking.TrackingHelper;

//SPEI

public class OtrosBBVAViewController extends BaseViewController implements View.OnClickListener {

	private RelativeLayout vista;
	private LinearLayout vistaCtaOrigen;
	private CuentaOrigenViewController componenteCtaOrigen;
	private TextView txtNumeroCuentaLabel;
	private EditText txtNumeroCuenta;
	private TextView txtImporteLabel;
	private AmountField textImporte;
	private TextView fechaLabel;
	private TextView fecha;
	private ImageButton btnContinuar;
	//SPEI
	private TextView accountTypeSelectorTitle;
	private TextView accountTypeSelector;
	
	// boton para seleccionar contactos de la agenda del dispositivo
	private ImageButton btnAgendaBBVA;

	// Codigo de respuesta de contactos
	public static final int ContactsRequestCode = 1;

	// Bandera para indicar si se estan pidiendo contactos.
	private boolean pidiendoContactos;

	// END SPEI
	
	//private EditText tbConcepto;
	//private TextView lblTituloConcepto;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	
	
	private OtrosBBVADelegate otrosBBVADelegate;
	private EditText txtConcepto;
	private TextView lblTituloConcepto;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_transferir_otros_bbva);
		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID));
		otrosBBVADelegate = (OtrosBBVADelegate)getDelegate();
		otrosBBVADelegate.setOtrosBBVAViewController(this);
		
		int title = 0;
		int icono = R.drawable.bmovil_transferir_icono;
		//SPEI
		/*if (otrosBBVADelegate.isEsExpress()) {
			title = R.string.opcionesTransfer_menu_cuentaexpress;
		}else*/
		if (otrosBBVADelegate.isEsTDC()) {
			title = R.string.transferir_otrosBBVA_TDC_title;
			icono = R.drawable.icono_pagar_servicios;
		} else {
			title = R.string.opcionesTransfer_menu_otrascuentasbbva;
		}
		
		setTitle(title, icono);
		
		findViews();
		scaleToScreenSize();
		
		fecha.setText(Tools.getCurrentDate());
		btnContinuar.setOnClickListener(this);
		
		init();
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
				TrackingHelper.trackState("nuevo", parentManager.estados);
		
		if (otrosBBVADelegate.isEsExpress()) {
			InputFilter[] filter = {new InputFilter.LengthFilter(Constants.CHECK_ACCOUNT_NUMBER_LENGTH)};
			txtNumeroCuenta.setFilters(filter);
			txtNumeroCuenta.setHint(R.string.bmovil_common_hint_telefono);
		} else {
			InputFilter[] filter = {new InputFilter.LengthFilter(Constants.CARD_NUMBER_LENGTH)};
			txtNumeroCuenta.setFilters(filter);
		}
		
		InputFilter[] filterImporte = {new InputFilter.LengthFilter(Constants.AMOUNT_LENGTH)};
		textImporte.setFilters(filterImporte);
		
		configuraPantalla();
		
		txtNumeroCuenta.addTextChangedListener(new BmovilTextWatcher(this));
		txtConcepto.addTextChangedListener(new BmovilTextWatcher(this));

		//tbConcepto.addTextChangedListener(new BmovilTextWatcher(this));
		//SPEI
		
		//bandera contactos
		pidiendoContactos = false;
		
		if(otrosBBVADelegate.getEsFrecuente()) {
			accountTypeSelectorTitle.setVisibility(View.GONE);
			accountTypeSelector.setVisibility(View.GONE);
			
			boolean isExpress = otrosBBVADelegate.getTransferenciaOtrosBBVA().getTipoCuenta().equals(Constants.EXPRESS_TYPE);
			boolean isSpei = otrosBBVADelegate.getTransferenciaOtrosBBVA().getTipoCuenta().equals(Constants.SPEI_TYPE);
			
			otrosBBVADelegate.setEsExpress(isExpress);
			otrosBBVADelegate.setSpei(isSpei);
					
			if(!isExpress && !isSpei) {
				accountTypeSelector.setText(getString(R.string.transferir_otrosBBVA_accotun_type_card));
				otrosBBVADelegate.setPhoneNumber(false);
			} else {
				accountTypeSelector.setText(getString(R.string.transferir_otrosBBVA_accotun_type_phone));
				otrosBBVADelegate.setPhoneNumber(true);
	
			}
			txtNumeroCuentaLabel.setText(accountTypeSelector.getText().toString());
		}
		//Termina SPEI
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (!pidiendoContactos) {
			if (parentViewsController.consumeAccionesDeReinicio()) {
				return;
			}
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if(!pidiendoContactos) {
		    parentViewsController.consumeAccionesDePausa();
		}
	}
	
	@Override
	public void goBack() {
		if(otrosBBVADelegate.isOperacionRapida()) {
			otrosBBVADelegate.setOperacionRapida(false);
		} else if(!otrosBBVADelegate.getEsFrecuente() || !pidiendoContactos) {
			parentViewsController.removeDelegateFromHashMap(InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID);
		}
		
		super.goBack();
	}
	
	public void init(){
		if(otrosBBVADelegate.isOperacionRapida()) {
			cargarCuentas();
			return;
		}
		
		if(!otrosBBVADelegate.getEsFrecuente())
			//frecuente correo electronico
			//otrosBBVADelegate.setTransferenciaOtrosBBVA(((TransferirDelegate)parentViewsController.getBaseDelegateForKey(TransferirDelegate.TRANSFERIR_DELEGATE_ID)).getTransferenciaOtrosBBVA());
		{
			TransferenciaOtrosBBVA transferenciaOtrosBBVA = new TransferenciaOtrosBBVA();
			transferenciaOtrosBBVA.setCuentaOrigen(((TransferirDelegate) parentViewsController.getBaseDelegateForKey(TransferirDelegate.TRANSFERIR_DELEGATE_ID)).getTransferenciaOtrosBBVA().getCuentaOrigen());
			otrosBBVADelegate.setTransferenciaOtrosBBVA(transferenciaOtrosBBVA);
		}
			cargarCuentas();
    }
	
	public void configuraPantalla() {
		if (otrosBBVADelegate.isEsExpress()) {
			txtNumeroCuentaLabel.setText(getString(R.string.transferir_otrosBBVA_destiny_express));
		} 
		
		if (otrosBBVADelegate.getEsFrecuente()) {
			txtNumeroCuenta.setText(otrosBBVADelegate.getTransferenciaOtrosBBVA().getCuentaDestino());
			txtNumeroCuenta.setFocusable(false);
			txtNumeroCuenta.setEnabled(false);
		} else if(otrosBBVADelegate.isOperacionRapida()) {
			configurarParaRapido();
		}
	}

	@SuppressWarnings("deprecation")
	public void cargarCuentas(){
		ArrayList<Account> listaCuetasAMostrar = otrosBBVADelegate.cargaCuentasOrigen();
		GuiTools guiTools = GuiTools.getCurrent();
		
		LinearLayout.LayoutParams params;
		params = new LayoutParams(guiTools.getEquivalenceInPixels(280.0), LinearLayout.LayoutParams.WRAP_CONTENT);
		
		//LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		params.topMargin = 8;
		//params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
		//params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
		
		//SPEI
		if(null == otrosBBVADelegate.getTransferenciaOtrosBBVA()) {
			TransferenciaOtrosBBVA model = new TransferenciaOtrosBBVA();
			for(Account acc : Session.getInstance(SuiteApp.appContext).getAccounts()) {
				if(acc.isVisible()) {
					model.setCuentaOrigen(acc);
					break;
				}
			}
			otrosBBVADelegate.setTransferenciaOtrosBBVA(model);
		}
		//Termina SPEI

		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
		componenteCtaOrigen.setDelegate(otrosBBVADelegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(otrosBBVADelegate.getTransferenciaOtrosBBVA().getCuentaOrigen()));
		componenteCtaOrigen.init();
		vistaCtaOrigen.addView(componenteCtaOrigen);
	}
	
	public RelativeLayout getVista() {
		return vista;
	}

	public LinearLayout getVistaCtaOrigen() {
		return vistaCtaOrigen;
	}

	public CuentaOrigenViewController getComponenteCtaOrigen() {
		return componenteCtaOrigen;
	}

	public EditText getTxtNumeroCuenta() {
		return txtNumeroCuenta;
	}

	public AmountField getTextImporte() {
		return textImporte;
	}

	public TextView getFecha() {
		return fecha;
	}

	public OtrosBBVADelegate getOtrosBBVADelegate() {
		return otrosBBVADelegate;
	}
	
	public void actualizarSeleccion(Object select){
		if (select instanceof Account) {
			otrosBBVADelegate.getTransferenciaOtrosBBVA().setCuentaOrigen((Account)select);
			componenteCtaOrigen.getImgDerecha().setEnabled(true);
			componenteCtaOrigen.getImgIzquierda().setEnabled(true);
			componenteCtaOrigen.getVistaCtaOrigen().setEnabled(true);
		}
	}
	
	@Override
	public void onClick(View v) {
		if (v == btnContinuar && !parentViewsController.isActivityChanging()) {
			otrosBBVADelegate.getTransferenciaOtrosBBVA().setConcepto(txtConcepto.getText().toString());
		//SPEI
			if(accountTypeSelector.getText().toString().equals(getString(R.string.transferir_interbancario_hint_tipocuenta))) {
				showInformationAlert(R.string.transferir_otrosBBVA_error_account_type_needed);
			} else {
			otrosBBVADelegate.validaDatos(txtNumeroCuenta.getText().toString(), 
										Tools.formatAmountForServer(textImporte.getText().toString()));
			}
		}
		else if(v == btnAgendaBBVA){
			if(pidiendoContactos)
				return;
			pidiendoContactos = true;
			AbstractContactMannager.getContactManager().requestContactData(this);
		}
	}
	
	@Override  
	public void onActivityResult(int reqCode, int resultCode, Intent data) {  
	    super.onActivityResult(reqCode, resultCode, data);
	    pidiendoContactos = false;
	    if (resultCode == Activity.RESULT_OK) {
	    	switch (reqCode) {
			case ContactsRequestCode:
				obtenerContacto(data);
				break;
			}
	    }
	}
	
	/**
	 * Obtiene los datos del contacto seleccionado.
	 * @param data Los datos del Intent.
	 */
	public void obtenerContacto(Intent data) {
		AbstractContactMannager manager = AbstractContactMannager.getContactManager();
		manager.getContactData(this, data);
		
		if(manager.getNumeroDeTelefono().equalsIgnoreCase(""))
			return;

	    txtNumeroCuenta.setText(manager.getNumeroDeTelefono());
	}
	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		otrosBBVADelegate.analyzeResponse(operationId, response);
	}
	
	private void findViews() {
		vista = (RelativeLayout)findViewById(R.id.transferir_otrosBBVA_view_controller_layout);
		vistaCtaOrigen = (LinearLayout)findViewById(R.id.transferir_otrosBBVA_cuenta_origen_view);
		txtNumeroCuentaLabel = (TextView)findViewById(R.id.transferir_otrosBBVA_destino_label);
		txtNumeroCuenta = (EditText)findViewById(R.id.transferir_otrosBBVA_destino);
		txtImporteLabel = (TextView)findViewById(R.id.transferir_otrosBBVA_importe_label);
		textImporte = (AmountField)findViewById(R.id.transferir_otrosBBVA_importe);
		fechaLabel = (TextView)findViewById(R.id.transferir_otrosBBVA_fecha_operacion_label);
		fecha = (TextView)findViewById(R.id.transferir_otrosBBVA_fecha_operacion);
		btnContinuar = (ImageButton)findViewById(R.id.transferir_otrosBBVA_continuar);
		//SPEI
		accountTypeSelectorTitle = (TextView)findViewById(R.id.accountTypeSelectorTitle);
		accountTypeSelector = (TextView)findViewById(R.id.accountTypeSelector);
		
		btnAgendaBBVA = (ImageButton)findViewById(R.id.btnAgendaBBVA);
		btnAgendaBBVA.setOnClickListener(this);
		
//		lblTituloConcepto = (TextView)findViewById(R.id.transferir_otrosBBVA_concepto_label);
//		tbConcepto = (EditText)findViewById(R.id.transferir_otrosBBVA_concepto);
		lblTituloConcepto = (TextView)findViewById(R.id.transferir_otrosBBVA_concepto_label);
		txtConcepto = (EditText)findViewById(R.id.transferir_otrosBBVA_concepto);
	}
	
	private void scaleToScreenSize() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(txtNumeroCuentaLabel, true);
		guiTools.scale(txtNumeroCuenta, true);
		guiTools.scale(txtImporteLabel, true);
		guiTools.scale(textImporte, true);
		guiTools.scale(lblTituloConcepto, true);
		guiTools.scale(txtConcepto, true);
		guiTools.scale(fechaLabel, true);
		guiTools.scale(fecha, true);
		guiTools.scale(btnContinuar);
		guiTools.scale(accountTypeSelectorTitle, true);
		guiTools.scale(accountTypeSelector, true);
		
	//	guiTools.scale(lblTituloConcepto, true);
	//	guiTools.scale(txtConcepto, true);
	
		guiTools.scale(btnAgendaBBVA);
		
		guiTools.scale(vistaCtaOrigen);
	}
	
	private void configurarParaRapido() {
		TransferenciaOtrosBBVA trans = otrosBBVADelegate.getTransferenciaOtrosBBVA();
		this.txtNumeroCuenta.setText(trans.getCuentaDestino());
		this.textImporte.setAmount(trans.getImporte());
		this.txtNumeroCuenta.setEnabled(false);
	}
	
	//SPEI
	public void onAccountTypeSelectorClick(View sender) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		final String[] options = new String[] {
			getString(R.string.transferir_otrosBBVA_accotun_type_card),
			getString(R.string.transferir_otrosBBVA_accotun_type_phone)	
		};
		
		builder.setTitle(R.string.transferir_otrosBBVA_accotun_type_title).setItems(options, new DialogInterface.OnClickListener() {
//		builder.setItems(options, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				String selectedOption = options[which];
				accountTypeSelector.setText(selectedOption);
				txtNumeroCuentaLabel.setText(selectedOption);
				
				txtNumeroCuenta.setText(Constants.EMPTY_STRING);
				InputFilter[] filterArray;
				if(0 == which){	// Tarjeta/Cuenta
					filterArray = new InputFilter[] {new InputFilter.LengthFilter(Constants.CARD_NUMBER_LENGTH)};
				    btnAgendaBBVA.setVisibility(View.GONE);
				}
				else{			// Numero celular
					filterArray = new InputFilter[] {new InputFilter.LengthFilter(Constants.TELEPHONE_NUMBER_LENGTH)};
					btnAgendaBBVA.setVisibility(View.VISIBLE);
				}
				txtNumeroCuenta.setFilters(filterArray);
				
				otrosBBVADelegate.setPhoneNumber(0 != which);
			}
		}).show();
	}
	
	public String getSelectedAccountType() {
		return accountTypeSelector.getText().toString();
	}
	//SPEI
	
}
