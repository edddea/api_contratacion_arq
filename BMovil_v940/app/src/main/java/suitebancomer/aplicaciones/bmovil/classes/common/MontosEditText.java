package suitebancomer.aplicaciones.bmovil.classes.common;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.bancomer.mbanking.R;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.DetalleConsumoViewController;

/**
 * Created by OOROZCO on 1/13/16.
 */
public class MontosEditText extends EditText {

    Context contextClass;


    public MontosEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        contextClass = context;
    }

    @Override
    public void setOnKeyListener(OnKeyListener l) {
        super.setOnKeyListener( new View.OnKeyListener() {
                                    @Override
                                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                                        //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                                        if(keyCode == KeyEvent.KEYCODE_DEL){
                                            if(MontosEditText.this.length()>3 && ((DetalleConsumoViewController)contextClass).isFlag())
                                            {
                                                MontosEditText.this.setSelection(MontosEditText.this.length()-3);
                                                ((DetalleConsumoViewController)contextClass).setFlag(false);
                                            }

                                        }
                                        return false;
                                    }
                                }
        );

    }
}