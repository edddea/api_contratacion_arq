package suitebancomer.aplicaciones.bmovil.classes.common;

import android.content.Context;

import com.bancomer.base.SuiteApp;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;

/**
 * Created by evaltierrah on 19/08/2015.
 */
public class SincronizarSesion {

    /**
     * Instancia de Sincronizacion
     */
    private static SincronizarSesion ourInstance = new SincronizarSesion();

    /**
     * Constructor privado
     */
    private SincronizarSesion() {
    }

    /**
     * Obtiene la instancia de sincronizacion
     *
     * @return Regresa la instancia de Sincronizacion
     */
    public static SincronizarSesion getInstance() {
        return ourInstance;
    }

    /**
     * Sincroniza la Sesion de BMovil con la Sesion de las APIs de BConnect
     *
     * @param context Contexto de la aplicacion
     */
    public void SessionToSessionApi(final Context context) {
        SuiteApp suiteApp = new SuiteApp();
        suiteApp.onCreate(context);
        SuiteApp.contextoActual = null;
        final Session sesionBase = Session.getInstance(context);
        final suitebancomercoms.aplicaciones.bmovil.classes.common.Session sesionApi = suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(context);
        sesionApi.setActivationCode(sesionBase.getActivationCode());
        //sesionApi.setAceptaCambioPerfil(sesionBase.getAceptaCambioPerfil());
        sesionApi.setApplicationActivated(sesionBase.isApplicationActivated());
        sesionApi.setCatalogoDineroMovil(sesionBase.getCatalogoDineroMovil());
        sesionApi.setClientNumber(sesionBase.getClientNumber());
        sesionApi.setClientProfile(sesionBase.getClientProfile());
        sesionApi.setCompaniaUsuario(sesionBase.getCompaniaUsuario());
        sesionApi.setEmail(sesionBase.getEmail());
        sesionApi.setEstatusIS(sesionBase.getEstatusIS());
        sesionApi.setIum(sesionBase.getIum());
        sesionApi.setListA(sesionBase.getListA());
        sesionApi.setListB(sesionBase.getListB());
        sesionApi.setNombreCliente(sesionBase.getNombreCliente());
        sesionApi.setPassword(sesionBase.getPassword());
        sesionApi.setPendingStatus(sesionBase.getPendingStatus());
        sesionApi.setUsername(sesionBase.getUsername());
        sesionApi.setSecurityInstrument(sesionBase.getSecurityInstrument());
        sesionApi.setSeed(sesionBase.getSeed());
        sesionApi.setValidity(sesionBase.getValidity());
        sesionApi.setCatalogoMantenimientoSPEI(sesionBase.getCatalogoMantenimientoSPEI());
        sesionApi.setSwitchSPEI(sesionBase.getSwitchSPEI());
        sesionApi.setCatalogoServicios(sesionBase.getCatalogoServicios());
        sesionApi.setCatalogoDineroMovil(sesionBase.getCatalogoDineroMovil());
        sesionApi.setCatalogoHoras(sesionBase.getCatalogoHoras());
        sesionApi.setCatalogoTelefonicas(sesionBase.getCatalogoTelefonicas());
        sesionApi.setCatalogoTiempoAire(sesionBase.getCatalogoTiempoAire());
        sesionApi.setCatalogVersions(sesionBase.getCatalogVersions());
        //sesionApi.setAceptaCambioPerfil(sesionBase.getAceptaCambioPerfil());
        sesionApi.setAccounts(sesionBase.getAccounts());
        suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion.getInstance().setLimiteOperacion(Autenticacion.getInstance().getLimiteOperacion());

    }

    /**
     * Sincroniza la Sesion de APIs de BConnect con la Sesion de las BMovil
     *
     * @param context Contexto de la aplicacion
     */
    public void SessionApiToSession(final Context context) {
        SuiteApp suiteApp = new SuiteApp();
        suiteApp.onCreate(context);
        final Session sesionBase = Session.getInstance(context);
        final suitebancomercoms.aplicaciones.bmovil.classes.common.Session sesionApi = suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(context);
        sesionBase.setActivationCode(sesionApi.getActivationCode());
        sesionBase.setAceptaCambioPerfil(sesionApi.getAceptaCambioPerfil());
        sesionBase.setApplicationActivated(sesionApi.isApplicationActivated());
        sesionBase.setCatalogoDineroMovil(sesionApi.getCatalogoDineroMovil());
        sesionBase.setClientNumber(sesionApi.getClientNumber());
        sesionBase.setClientProfile(sesionApi.getClientProfile());
        sesionBase.setCompaniaUsuario(sesionApi.getCompaniaUsuario());
        sesionBase.setEmail(sesionApi.getEmail());
        sesionBase.setEstatusIS(sesionApi.getEstatusIS());
        sesionBase.setIum(sesionApi.getIum());
        sesionBase.setListA(sesionApi.getListA());
        sesionBase.setListB(sesionApi.getListB());
        sesionBase.setNombreCliente(sesionApi.getNombreCliente());
        sesionBase.setPassword(sesionApi.getPassword());
        sesionBase.setPendingStatus(sesionApi.getPendingStatus());
        sesionBase.setUsername(sesionApi.getUsername());
        sesionBase.setSecurityInstrument(sesionApi.getSecurityInstrument());
        sesionBase.setSeed(sesionApi.getSeed());
        sesionBase.setValidity(sesionApi.getValidity());
        sesionBase.setCatalogoServicios(sesionApi.getCatalogoServicios());
        sesionBase.setCatalogoTiempoAire(sesionApi.getCatalogoTiempoAire());
        sesionBase.setCatalogoTelefonicas(sesionApi.getCatalogoTelefonicas());
        sesionBase.setCatalogVersions(sesionApi.getCatalogVersions());
        sesionBase.setCatalogoHoras(sesionApi.getCatalogoHoras());
        sesionBase.setCatalogoDineroMovil(sesionApi.getCatalogoDineroMovil());
        sesionBase.setSwitchSPEI(sesionApi.getSwitchSPEI());
        sesionBase.setAceptaCambioPerfil(sesionApi.getAceptaCambioPerfil());
        sesionBase.setAccounts(sesionApi.getAccounts());
        Autenticacion.getInstance().setLimiteOperacion(suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion.getInstance().getLimiteOperacion());
        if(!Constants.EMPTY_STRING.equals(sesionBase.getUsername())) {
            DatosBmovilFileManager.getCurrent().setLogin(sesionBase.getUsername());
        }
        if(!Constants.EMPTY_STRING.equals(sesionApi.isApplicationActivated())) {
            DatosBmovilFileManager.getCurrent().setActivado(sesionApi.isApplicationActivated());
        }
    }

}
