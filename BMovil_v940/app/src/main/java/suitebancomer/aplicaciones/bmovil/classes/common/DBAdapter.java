/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomer.aplicaciones.bmovil.classes.common;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.classes.common.PropertiesManager;

/**
 * 
 * Allows operations with sqlite database
 * 
 * @author Stefanini IT Solutions.
 */

public class DBAdapter {

	private static final int DATABASE_VERSION = 6;

	// The index (key) column name for use in where clauses.
	public static final String KEY_ID = "_id";

	/**
	 * Activated record type for RMS persistence
	 */
	private static final int ACTIVATED_RMS_ID = 1;

	/**
	 * Login record type for RMS persistence
	 */
	private static final int LOGIN_RMS_ID = 2;

	/**
	 * Autofill record type for RMS persistence
	 */
	private static final int AUTOFILL_RMS_ID = 3;

	/**
	 * Seed record type for RMS persistence
	 */
	private static final int SEED_RMS_ID = 5;

	/**
	 * via 1 o 2
	 */
	private static final int VIA_RMS_ID = 6;

	/**
	 * Bancomer Token record type for RMS persistence
	 */
	private static final int TOKEN_RMS_ID = 7;

	/**
	 * Bancomer Token record type for RMS persistence
	 */
	private static final int PENDING_STATUS_RMS_ID = 8;

	private static final int PROFILE_UPDATE_FLAG_RMS_ID = 9;
	/**
	 * Softoken record type for RMS persistence
	 */
	private static final int SOFTTOKEN_RMS_ID = 11;

	/**
	 * First catalog version record type for RMS persistence (It's mandatory
	 * that the value of this constant and the values of the rest of the
	 * catalogX_version_rms_id constants are consecutive)
	 */
	private static final int CATALOG1_VERSION_RMS_ID = 300;

	/**
	 * Fourth catalog version record type for RMS persistence
	 */
	private static final int CATALOG4_VERSION_RMS_ID = 301;

	/**
	 * Fifth catalog record type for RMS persistence
	 */
	private static final int CATALOG5_VERSION_RMS_ID = 302;

	/**
	 * Eighth catalog record type for RMS persistence
	 */
	private static final int CATALOG8_VERSION_RMS_ID = 303;

	/**
	 * TA catalog record type for RMS persistence
	 */
	private static final int TA_CATALOG_VERSION_RMS_ID = 304;

	/**
	 * DM catalog record type for RMS persistence
	 */
	private static final int DM_CATALOG_VERSION_RMS_ID = 305;

	/**
	 * Services catalog record type for RMS persistence
	 */
	private static final int SERVICES_CATALOG_VERSION_RMS_ID = 306;

	// The name and column index of each column in your database.
	public static final String KEY_IDENTIFICADOR = Constants.IDENTIFICADOR;
	public static final String KEY_VALUE = Constants.VALUE;
	public static final String KEY_SCATALOGV = Constants.SCATALOGV;
	public static final String KEY_ICATALOGV = Constants.ICATALOGV;

	public static final String KEY_CAT_OA = Constants.CAT_OA;
	public static final String KEY_CAT_MG = Constants.CAT_MG;
	public static final String KEY_CAT_CV = Constants.CAT_CV;
	public static final String KEY_CAT_IM = Constants.CAT_IM;
	public static final String KEY_CAT_OR = Constants.CAT_OR;

	public static final String KEY_CAT_AU_PE = Constants.CAT_AU_PE;
	public static final String KEY_CAT_AU_PW = Constants.CAT_AU_PW;
	public static final String KEY_CAT_AU_TK = Constants.CAT_AU_TK;
	public static final String KEY_CAT_AU_C2 = Constants.CAT_AU_C2;
	public static final String KEY_CAT_AU_NP = Constants.CAT_AU_NP;
	public static final String KEY_CAT_AU_RE = Constants.CAT_AU_RE;

	public static final String KEY_CAT_TL_IM = Constants.CAT_TL_IM;

	public static final String KEY_CAT_BM_DT = Constants.CAT_BM_DT;
	public static final String KEY_CAT_BM_HR = Constants.CAT_BM_HR;

	// TO-DO: Create public field for each column in your table.

	// SQL Statement to create a new table.
	private static final String CREATE_TABLE1 = "create table if not exists "
			+ Constants.TABLE_MBANKING + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_IDENTIFICADOR
			+ " text not null, " + KEY_VALUE + " text, " + KEY_SCATALOGV
			+ " text, " + KEY_ICATALOGV + " integer );";

	private static final String CREATE_TABLE2 = "create table if not exists "
			+ Constants.TABLE_CAT + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_IDENTIFICADOR
			+ " text not null, " + KEY_VALUE + " text, " + KEY_SCATALOGV
			+ " text, " + KEY_ICATALOGV + " integer );";

	private static final String CREATE_TABLE3 = "create table if not exists "
			+ Constants.TABLE_APP_STATUS + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_IDENTIFICADOR
			+ " text not null, " + KEY_VALUE + " text, " + KEY_SCATALOGV
			+ " text, " + KEY_ICATALOGV + " integer );";

	private static final String CREATE_TABLE4 = "create table if not exists "
			+ Constants.TABLE_PROFILE_STATUS + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_IDENTIFICADOR
			+ " text not null, " + KEY_VALUE + " text, " + KEY_SCATALOGV
			+ " text, " + KEY_ICATALOGV + " integer );";

	private static final String CREATE_TABLE5 = "create table if not exists "
			+ Constants.TABLE_NUEVO_CATALOGO + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_IDENTIFICADOR
			+ " text not null, " + KEY_CAT_OA + " text not null, " + KEY_CAT_MG
			+ " text not null, " + KEY_CAT_CV + " text not null, " + KEY_CAT_IM
			+ " text, " + KEY_CAT_OR + " integer );";

	private static final String CREATE_TABLE6 = "create table if not exists "
			+ Constants.TABLE_AUTHENTICATION_CATALOG + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_IDENTIFICADOR
			+ " text not null, " + KEY_CAT_AU_PE + " text not null, "
			+ KEY_CAT_AU_PW + " text not null, " + KEY_CAT_AU_TK
			+ " text not null, " + KEY_CAT_AU_C2 + " text not null, "
			+ KEY_CAT_AU_NP + " text not null, " + KEY_CAT_AU_RE
			+ " text not null);";

	private static final String CREATE_TABLE7 = "create table if not exists "
			+ Constants.TABLE_TELEFONICAS_CATALOG + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_IDENTIFICADOR
			+ " text not null, " + KEY_CAT_TL_IM + " text not null);";

	private static final String CREATE_TABLE8 = "create table if not exists "
			+ Constants.TABLE_BMOVIL_ACTIVATION + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_IDENTIFICADOR
			+ " text not null, " + KEY_VALUE + " text not null);";

	private static final String CREATE_TABLE9 = "create table if not exists "
			+ Constants.TABLE_BANDERAS_BMOVIL + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_IDENTIFICADOR
			+ " text not null, " + KEY_VALUE + " integer not null);";

	private static final String CREATE_TABLE10 = "create table if not exists "
			+ Constants.TABLE_TEMPORAL_ST + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_IDENTIFICADOR
			+ " text not null, " + KEY_VALUE + " text not null);";

	private static final String CREATE_TABLE11 = "create table if not exists "
			+ Constants.TABLE_TEMPORAL_CAMBIO_TELEFONO + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_IDENTIFICADOR
			+ " text not null, " + KEY_VALUE + " text not null);";

	// SQL Statement to delete a content of the table
	private static final String DELETE_TABLE = "delete from ";

	// Variable to hold the database instance
	private static SQLiteDatabase db;

	// Context of the application using the database.
	private final Context context;

	/**
	 * Database open/upgrade helper.
	 */
	private myDbHelper dbHelper;

	/**
	 * Opens and connects to the specified socket TCP/IP connection.
	 * 
	 * @param _context
	 *            Context of the application using the database.
	 */
	public DBAdapter(Context _context) {
		context = _context;
		dbHelper = new myDbHelper(context, Constants.DATABASE_NAME, null,
				DATABASE_VERSION);
	}

	/**
	 * Open and return a readable/writable instance of the database.
	 * 
	 */
	public DBAdapter open() {
		try {
			db = dbHelper.getWritableDatabase();
		} catch (SQLiteException ex) {
			db = dbHelper.getReadableDatabase();
		}

		return this;
	}

	/**
	 * Open and return a readable/writable instance of the database.
	 * 
	 */
	public void close() {
		try {
			db.close();
			dbHelper.close();
		} catch (SQLiteException e) {
			if(Server.ALLOW_LOG) Log.i("DBAdapter.java", "Error close()");
			if(Server.ALLOW_LOG) e.printStackTrace();
		} finally {
			db = null;
			dbHelper = null;
		}

	}

	/**
	 * Insert a data
	 * 
	 * @param identificador
	 * @param value
	 * @param catalogvs
	 * @param catalogvi
	 * @param table
	 *            return the row ID of the newly inserted row, or -1 if an error
	 *            occurred
	 */
	public long insertEntry(int identificador, String value, String catalogvs,
			int catalogvi, String table) {
		ContentValues contentValues = new ContentValues();
		// fill in ContentValues to represent the new row
		contentValues.put(KEY_IDENTIFICADOR, identificador);
		contentValues.put(KEY_VALUE, value);
		contentValues.put(KEY_SCATALOGV, catalogvs);
		contentValues.put(KEY_ICATALOGV, catalogvi);
		return db.insert(table, null, contentValues);
	}

	public long insertNewCatalogEntry(int identificador, String nombre,
			String imagen, String clave, String importes, int orden) {
		ContentValues contentValues = new ContentValues();
		// fill in ContentValues to represent the new row
		contentValues.put(KEY_IDENTIFICADOR, identificador);
		contentValues.put(KEY_CAT_OA, nombre);
		contentValues.put(KEY_CAT_MG, imagen);
		contentValues.put(KEY_CAT_CV, clave);
		if (importes == null) {
			contentValues.putNull(KEY_CAT_IM);
		} else {
			contentValues.put(KEY_CAT_IM, importes);
		}
		contentValues.put(KEY_CAT_OR, orden);
		return db.insert(Constants.TABLE_NUEVO_CATALOGO, null, contentValues);
	}

	/**
	 * Delete a table row.
	 * 
	 * @param _rowIndex
	 * @param table
	 */
	public boolean removeEntry(long _rowIndex, String table) {
		return db.delete(table, KEY_ID + "=" + _rowIndex, null) > 0;
	}

	/**
	 * Get all the records in the table
	 * 
	 * @param table
	 */
	public Cursor getAllEntries(String table) {
		return db.query(table, new String[] { KEY_ID, KEY_IDENTIFICADOR,
				KEY_VALUE, KEY_SCATALOGV, KEY_ICATALOGV }, null, null, null,
				null, null);
	}

	public Cursor getAllNewCatalogEntries() {
		return db.query(Constants.TABLE_NUEVO_CATALOGO, new String[] { KEY_ID,
				KEY_IDENTIFICADOR, KEY_CAT_OA, KEY_CAT_MG, KEY_CAT_CV,
				KEY_CAT_IM, KEY_CAT_OR }, null, null, null, null, null);
	}

	/**
	 * Get the number of rows in the table
	 * 
	 * @param table
	 */
	public int getNumRecords(String table) {
		Cursor c = null;
		int total = 0;
		try {
			c = db.query(table, new String[] { KEY_ID, KEY_IDENTIFICADOR },
					null, null, null, null, null);
			total = c.getCount();
		} catch (SQLiteException e) {
			if(Server.ALLOW_LOG) Log.i("DBAdapter,java", "Error getNumRecords()");
		} finally {
			if (c != null)
				c.close();
		}
		return total;
	}

	/**
	 * Delete the contents of the table
	 * 
	 * @param table
	 */
	public void deleteContent(String table) {
		db.execSQL(DELETE_TABLE + table);
	}

	// #region Authentication Table.
	/**
	 * Select all the entries in the Authentication Catalog table.
	 * 
	 * @return A Cursor object, which is positioned before the first entry.
	 */
	public Cursor getAllAuthenticationCatalogEntries() {
		return db.query(Constants.TABLE_AUTHENTICATION_CATALOG, null, null,
				null, null, null, null);
	}

	/**
	 * Insert a new row in the table, Softtoken entry is set to "false". <br/>
	 * This method is for compatibility, since this method was added before
	 * Softtoken was available.
	 * 
	 * @param operation
	 *            The operation name.
	 * @param profile
	 *            The profile (basic or advanced).
	 * @param password
	 *            The password requirement.
	 * @param token
	 *            The token requirement.
	 * @param cvv2
	 *            The cvv2 requirement.
	 * @param nip
	 *            The nip requirement.
	 * @return The row ID of the newly inserted row, or -1 if an error occurred.
	 */
	public long insertAuthenticationCatalogEntry(String operation,
			String profile, String password, String token, String cvv2,
			String nip) {
		return insertAuthenticationCatalogEntry(operation, profile, password,
				token, cvv2, nip, "false");
	}

	/**
	 * Insert a new row in the table.
	 * 
	 * @param operation
	 *            The operation name.
	 * @param profile
	 *            The profile (basic or advanced).
	 * @param password
	 *            The password requirement.
	 * @param token
	 *            The token requirement.
	 * @param cvv2
	 *            The cvv2 requirement.
	 * @param nip
	 *            The nip requirement.
	 * @param registro
	 *            The softtoken requirement.
	 * @return The row ID of the newly inserted row, or -1 if an error occurred.
	 */
	public long insertAuthenticationCatalogEntry(String operation,
			String profile, String password, String token, String cvv2,
			String nip, String registro) {
		ContentValues contentValues = new ContentValues();

		contentValues.put(KEY_IDENTIFICADOR, operation);
		contentValues.put(KEY_CAT_AU_PE, profile);
		contentValues.put(KEY_CAT_AU_PW, password);
		contentValues.put(KEY_CAT_AU_TK, token);
		contentValues.put(KEY_CAT_AU_C2, cvv2);
		contentValues.put(KEY_CAT_AU_NP, nip);
		contentValues.put(KEY_CAT_AU_RE, registro);

		return db.insert(Constants.TABLE_AUTHENTICATION_CATALOG, null,
				contentValues);
	}

	/**
	 * Selects an existing entry from the Authentication Catalog table.
	 * 
	 * @param operation
	 *            The operation name.
	 * @return A Cursor object, which is positioned before the first entry
	 */
	public Cursor selectAuthenticationCatalogEntry(String operation) {
		String selection = KEY_IDENTIFICADOR + " = " + operation;
		return db.query(Constants.TABLE_AUTHENTICATION_CATALOG, null,
				selection, null, null, null, null);
	}

	/**
	 * Selects an existing entry from the Authentication Catalog table.
	 * 
	 * @param operation
	 *            The operation name.
	 * @param profile
	 *            The profile (basic or advanced).
	 * @return A Cursor object, which is positioned before the first entry
	 */
	public Cursor selectAuthenticationCatalogEntry(String operation,
			String profile) {
		String selection = KEY_IDENTIFICADOR + " = " + operation + " AND "
				+ KEY_CAT_AU_PE + " = " + profile;
		return db.query(Constants.TABLE_AUTHENTICATION_CATALOG, null,
				selection, null, null, null, null);
	}

	/**
	 * Updates a entry in the Authentication Catalog table, Softtoken entry is
	 * set to "false". <br/>
	 * This method is for compatibility, since this method was added before
	 * Softtoken was available.
	 * 
	 * @param operation
	 *            The operation name.
	 * @param profile
	 *            The profile (basic or advanced).
	 * @param password
	 *            The password requirement.
	 * @param token
	 *            The token requirement.
	 * @param cvv2
	 *            The cvv2 requirement.
	 * @param nip
	 *            The nip requirement.
	 * @return The number of rows affected.
	 */
	public int updateAuthenticationCatalogEntry(String operation,
			String profile, String password, String token, String cvv2,
			String nip) {
		return updateAuthenticationCatalogEntry(operation, profile, password,
				token, cvv2, nip, "false");
	}

	/**
	 * Updates a entry in the Authentication Catalog table.
	 * 
	 * @param operation
	 *            The operation name.
	 * @param profile
	 *            The profile (basic or advanced).
	 * @param password
	 *            The password requirement.
	 * @param token
	 *            The token requirement.
	 * @param cvv2
	 *            The cvv2 requirement.
	 * @param nip
	 *            The nip requirement.
	 * @return The number of rows affected.
	 */
	public int updateAuthenticationCatalogEntry(String operation,
			String profile, String password, String token, String cvv2,
			String nip, String registro) {
		String selection = KEY_IDENTIFICADOR + " = " + operation + " AND "
				+ KEY_CAT_AU_PE + " = " + profile;

		ContentValues contentValues = new ContentValues();

		contentValues.put(KEY_IDENTIFICADOR, operation);
		contentValues.put(KEY_CAT_AU_PE, profile);
		contentValues.put(KEY_CAT_AU_PW, password);
		contentValues.put(KEY_CAT_AU_TK, token);
		contentValues.put(KEY_CAT_AU_C2, cvv2);
		contentValues.put(KEY_CAT_AU_NP, nip);
		contentValues.put(KEY_CAT_AU_RE, registro);

		return db.update(Constants.TABLE_AUTHENTICATION_CATALOG, contentValues,
				selection, null);
	}

	// #endregion

	// #region Phone Companies Table.
	/**
	 * Insert a phone company entry in the table.
	 */
	public long insertPhoneCompaniesEntry(String name, String image) {
		if (null == name || null == image)
			return -1L;

		ContentValues contentValues = new ContentValues();

		contentValues.put(KEY_IDENTIFICADOR, name);
		contentValues.put(KEY_CAT_TL_IM, image);

		return db.insert(Constants.TABLE_TELEFONICAS_CATALOG, null,
				contentValues);
	}

	/**
	 * Update a phone company entry.
	 */
	public int updatePhoneCompaniesEntry(String companyName, String image) {
		if (null == companyName || null == image)
			return -1;

		String selection = KEY_IDENTIFICADOR + " = " + companyName;

		ContentValues contentValues = new ContentValues();
		contentValues.put(KEY_IDENTIFICADOR, companyName);
		contentValues.put(KEY_CAT_TL_IM, image);

		return db.update(Constants.TABLE_TELEFONICAS_CATALOG, contentValues,
				selection, null);
	}

	/**
	 * Select a phone company entry.
	 */
	public Cursor selectPhoneCompaniesEntry(String companyName) {
		String selection = KEY_IDENTIFICADOR + " = " + companyName;
		return db.query(Constants.TABLE_TELEFONICAS_CATALOG, null, selection,
				null, null, null, null);
	}

	/**
	 * Select all phone companies entries.
	 */
	public Cursor selectAllPhoneCompaniesEntries() {
		return db.query(Constants.TABLE_TELEFONICAS_CATALOG, null, null, null,
				null, null, null);
	}

	/**
	 * Delete a phone company entry.
	 */
	public int deletePhoneCompaniesEntry(String companyName) {
		if (null == companyName)
			return -1;

		String selection = KEY_IDENTIFICADOR + " = " + companyName;
		return db.delete(Constants.TABLE_TELEFONICAS_CATALOG, selection, null);
	}

	// #endregion

	// #region Bmovil Activation Table.

	/**
	 * Insert a Bmovil activation entry in the table.
	 */
	public long insertBmovilActivationEntry(String name, String value) {
		if (null == name || null == value)
			return -1L;

		ContentValues contentValues = new ContentValues();

		contentValues.put(KEY_IDENTIFICADOR, name);
		contentValues.put(KEY_VALUE, value);

		return db
				.insert(Constants.TABLE_BMOVIL_ACTIVATION, null, contentValues);
	}

	/**
	 * Update a Bmovil activation entry.
	 */
	public int updateBmovilActivationEntry(String name, String value) {
		if (null == name || null == value)
			return -1;

		String selection = KEY_IDENTIFICADOR + " = " + name;

		ContentValues contentValues = new ContentValues();

		contentValues.put(KEY_IDENTIFICADOR, name);
		contentValues.put(KEY_VALUE, value);

		return db.update(Constants.TABLE_BMOVIL_ACTIVATION, contentValues,
				selection, null);
	}

	/**
	 * Select a Bmovil activation entry.
	 */
	public Cursor selectBmovilActivationEntry(String name) {
		String selection = KEY_IDENTIFICADOR + " = '" + name + "'";
		return db.query(Constants.TABLE_BMOVIL_ACTIVATION, null, selection,
				null, null, null, null);
	}

	/**
	 * Select all Bmovil activation entries.
	 */
	public Cursor selectAllBmovilActivationEntries() {
		return db.query(Constants.TABLE_BMOVIL_ACTIVATION, null, null, null,
				null, null, null);
	}

	/**
	 * Delete all the first activation data from the DB.
	 * 
	 * @return The number of deleted rows.
	 */
	public int deleteAllBmovilActivationEntries() {
		return db.delete(Constants.TABLE_BMOVIL_ACTIVATION, null, null);
	}

	// #endregion

	/**
	 * Select all table entries.
	 */
	public Cursor selectAllGenericEntries(String table) {
		return db.query(table, null, null, null, null, null, null);
	}

	/**
	 * Inserta todas las entradas de una tabla.
	 * 
	 * @param table
	 *            el nombre de la tabla
	 * @param mapNameValue
	 *            un mapa con los nombres y los valores de los atributos
	 * @return si actualizo filas o no
	 */
	public long insertAllGenericEntries(String table,
			HashMap<String, Object> mapNameValue) {
		long numUpdatedRows = 0L;
		Iterator<Entry<String, Object>> it = mapNameValue.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>) it
					.next();
			if (insertGenericEntry(table, (String) pairs.getKey(),
					pairs.getValue()) == -1L) {
				return -1L;
			}
			it.remove();
		}

		return numUpdatedRows;
	}

	/**
	 * Actualiza todas las entradas de una tabla.
	 * 
	 * @param table
	 *            el nombre de la tabla
	 * @param mapNameValue
	 *            un mapa con los nombres y los valores de los atributos
	 * @return el numero de filas actualizadas
	 */
	public int updateAllGenericEntries(String table,
			HashMap<String, Object> mapNameValue) {
		int numUpdatedRows = 0;
		Iterator<Entry<String, Object>> it = mapNameValue.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>) it
					.next();
			numUpdatedRows += updateGenericEntry(table,
					(String) pairs.getKey(), pairs.getValue());
			it.remove();
		}

		return numUpdatedRows;
	}

	/**
	 * Borra todas las entradas de una tabla.
	 * 
	 * @return el numero de filas borradas
	 */
	public int deleteAllGenericEntries(String table) {
		return db.delete(table, null, null);
	}

	/**
	 * Selecciona un atributo de una tabla concreta.
	 * 
	 * @param name
	 *            el nombre del atributo
	 * @return el cursor con el contenido de la fila buscada
	 */
	public Cursor selectGenericEntry(String table, String name) {
		String selection = KEY_IDENTIFICADOR + " = '" + name + "'";
		return db.query(table, null, selection, null, null, null, null);
	}

	/**
	 * Inserta una tributo en la tabla concreta.
	 * 
	 * @param name
	 *            el nombre del atributo
	 * @param value
	 *            el valor del atributo
	 * @return el numero de filas insertadas
	 */
	public long insertGenericEntry(String table, String name, Object value) {
		if (null == name || null == value) {
			return -1L;
		}

		ContentValues contentValues = new ContentValues();
		contentValues.put(KEY_IDENTIFICADOR, name);

		if (value instanceof String) {
			contentValues.put(KEY_VALUE, (String) value);
		} else if (value instanceof Integer) {
			contentValues.put(KEY_VALUE, (Integer) value);
		}

		return db.insert(table, null, contentValues);
	}

	/**
	 * Actualiza un atributo en la tabla concreta.
	 * 
	 * @param name
	 *            el nombre del atributo
	 * @param value
	 *            el valor del atributo
	 * @return el numero de filas actualizadas
	 */
	public int updateGenericEntry(String table, String name, Object value) {
		if (null == name || null == value) {
			return -1;
		}

		String selection = KEY_IDENTIFICADOR + " = '" + name + "'";

		ContentValues contentValues = new ContentValues();
		contentValues.put(KEY_IDENTIFICADOR, name);

		if (value instanceof String) {
			contentValues.put(KEY_VALUE, (String) value);
		} else if (value instanceof Integer) {
			contentValues.put(KEY_VALUE, (Integer) value);
		}

		return db.update(table, contentValues, selection, null);
	}

	/**
	 * 
	 * 
	 * FIN: METODOS GENERICOS PARA ESCRIBIR EN TABLAS banderasBmovil Y
	 * temporalST
	 * 
	 * 
	 */

	/**
	 * Hide the logic used to decide if a database needs to be created or
	 * upgraded before its opened.
	 */
	private static class myDbHelper extends SQLiteOpenHelper {
		/**
		 * Hide the logic used to decide if a database needs to be created or
		 * upgraded before its opened.
		 * 
		 * @param context
		 *            of the application using the database.
		 * @param name
		 *            of the database.
		 * @param null CursorFactory
		 * @param version
		 *            database
		 */
		public myDbHelper(Context context, String name, CursorFactory factory,	int version) {
			super(context, name, factory, version);
		}

		/**
		 * Called when no database exists in disk and the helper class needs to
		 * create a new one.
		 * 
		 * @param _db database.
		 */
		@Override
		public void onCreate(SQLiteDatabase _db) {
			// _db.execSQL(CREATE_TABLE1);
			//Es posible que se borre estatusAplicacion.Prop
			PropertiesManager.getCurrent();
			DatosBmovilFileManager.getCurrent();
//			_db.execSQL(CREATE_TABLE2);
//			CatFileManager.getCurrent();
//			_db.execSQL(CREATE_TABLE3);
			// _db.execSQL(CREATE_TABLE4);
			CompaniasTelefonicasCatalogFileManager.getCurrent();
//			_db.execSQL(CREATE_TABLE5);
			NuevoCatalogoFileManager.getCurrent();
//			_db.execSQL(CREATE_TABLE6);
//			CatalogoAutenticacionFileManager.getCurrent();
			// _db.execSQL(CREATE_TABLE7);
			CambioPerfilFileManager.getCurrent();
//			_db.execSQL(CREATE_TABLE8);
			ActivacionBmovilFileManager.getCurrent();
			// _db.execSQL(CREATE_TABLE9);
			BanderasBMovilFileManager.getCurrent();
			// _db.execSQL(CREATE_TABLE10);
			TemporalSTFileManager.getCurrent();
			// _db.execSQL(CREATE_TABLE11);
			TemporalCambioTelefonoFileManager.getCurrent();

		}

		/**
		 * Called when there is a database version mismatch meaning that the
		 * version of the database on disk needs to be upgraded to the current
		 * version.
		 * 
		 * @param _db database.
		 * @param _oldVersion of the database.
		 * @param _newVersion of the database.
		 */
		@Override
		public void onUpgrade(SQLiteDatabase _db, int _oldVersion, int _newVersion) {
			// Log the version upgrade.
			if(Server.ALLOW_LOG) Log.w("TaskDBAdapter", "Upgrading from version " + _oldVersion
					+ " to " + _newVersion);

			switch (_oldVersion) {
				case 1:
					updateFromVersion1ToVersion2(_db);
				case 2:
					updateFromVersion2ToVersion3(_db);
				case 3:
					updateFromVersion3ToVersion4(_db);
				case 4:
					updateFromVersion4ToVersion5(_db);
				case 5:
					updateFromVersion5ToVersion6(_db);
					break;
				default: // If the old version is not one of the expected,
							// delete
							// all the data.
					if(Server.ALLOW_LOG) Log.w(this.getClass().getSimpleName(),
							"No se esperaba esta version de la bd, se eliminar� todo el contenido.");
					_db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_MBANKING);
					_db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_CAT);
					_db.execSQL("DROP TABLE IF EXISTS "	+ Constants.TABLE_APP_STATUS);
					_db.execSQL("DROP TABLE IF EXISTS "	+ Constants.TABLE_PROFILE_STATUS);
					_db.execSQL("DROP TABLE IF EXISTS "	+ Constants.TABLE_NUEVO_CATALOGO);
					_db.execSQL("DROP TABLE IF EXISTS "	+ Constants.TABLE_AUTHENTICATION_CATALOG);
					_db.execSQL("DROP TABLE IF EXISTS "	+ Constants.TABLE_TELEFONICAS_CATALOG);
					_db.execSQL("DROP TABLE IF EXISTS "	+ Constants.TABLE_BMOVIL_ACTIVATION);
					_db.execSQL("DROP TABLE IF EXISTS "	+ Constants.TABLE_BANDERAS_BMOVIL);
					_db.execSQL("DROP TABLE IF EXISTS "	+ Constants.TABLE_TEMPORAL_ST);
					_db.execSQL("DROP TABLE IF EXISTS "	+ Constants.TABLE_TEMPORAL_CAMBIO_TELEFONO);
					onCreate(_db);
					break;
			}

		}

		/**
		 * Operations to update the database from version 1 to version 2.
		 * 
		 * @param _db
		 *            The database.
		 */
		private void updateFromVersion1ToVersion2(SQLiteDatabase _db) {
			if (null == _db)
				return;

			if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Agregando las tablas: "
					+ Constants.TABLE_APP_STATUS
					+ Constants.TABLE_PROFILE_STATUS
					+ Constants.TABLE_NUEVO_CATALOGO
					+ Constants.TABLE_AUTHENTICATION_CATALOG);

			// Create the new tables.
			_db.execSQL(CREATE_TABLE3);
			_db.execSQL(CREATE_TABLE4);
			_db.execSQL(CREATE_TABLE5);
			_db.execSQL(CREATE_TABLE6);
			_db.execSQL(CREATE_TABLE7);

			// Make the where clause to delete all the updated catalog entries.
			// 1.4.5.8
			int affectedRows = 0;
			String[] deletedCatalogs = { "101", "102", "105", "106" };
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < deletedCatalogs.length; i++) {
				builder.append(KEY_IDENTIFICADOR);
				builder.append(" = ?");

				if (i < (deletedCatalogs.length - 1))
					builder.append(" OR ");
			}

			// Delete the data from deprecated catalogs.
			affectedRows = _db.delete(Constants.TABLE_CAT, builder.toString(),
					deletedCatalogs);
			if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "DELETED " + affectedRows
					+ " from deprecated catalogs.");

			// Update the key identifier from the remain catalogs.
			// The only catalog remaining untouched is the associated with the
			// key CATALOG1_RMS_ID.
			ContentValues cv;

			affectedRows = 0;
			cv = new ContentValues();
			cv.put(KEY_IDENTIFICADOR, "101");
			affectedRows = _db.update(Constants.TABLE_CAT, cv,
					KEY_IDENTIFICADOR + " = '103';", null);
			if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "UPDATED " + affectedRows
			+ " from catalog 103.");

			affectedRows = 0;
			cv = new ContentValues();
			cv.put(KEY_IDENTIFICADOR, "102");
			affectedRows = _db.update(Constants.TABLE_CAT, cv,
					KEY_IDENTIFICADOR + " = '104';", null);
			if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "UPDATED " + affectedRows
				+ " from catalog 104.");

			affectedRows = 0;
			cv = new ContentValues();
			cv.put(KEY_IDENTIFICADOR, "103");
			affectedRows = _db.update(Constants.TABLE_CAT, cv,
					KEY_IDENTIFICADOR + " = '107';", null);
			if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "UPDATED " + affectedRows
					+ " from catalog 107.");
		}

		/**
		 * Operations to update the database from version 2 to version 3.
		 * 
		 * @param _db
		 *            The database.
		 */
		private void updateFromVersion2ToVersion3(SQLiteDatabase _db) {
			if (null == _db)
				return;

			if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Agregando las tablas: "
				+ Constants.TABLE_TELEFONICAS_CATALOG + ", "
					+ Constants.TABLE_BMOVIL_ACTIVATION);

			// Create the new tables.
			_db.execSQL(CREATE_TABLE7);
			_db.execSQL(CREATE_TABLE8);

			ContentValues contentValues = new ContentValues();
			contentValues.put(KEY_IDENTIFICADOR, KEY_CAT_BM_DT);
			contentValues.put(KEY_VALUE,
					Constants.NON_REGISTERED_ACTIVATION_DATE);
			_db.insert(Constants.TABLE_BMOVIL_ACTIVATION, null, contentValues);

			contentValues = new ContentValues();
			contentValues.put(KEY_IDENTIFICADOR, KEY_CAT_BM_HR);
			contentValues.put(KEY_VALUE,
					Constants.NON_REGISTERED_ACTIVATION_DATE);
			_db.insert(Constants.TABLE_BMOVIL_ACTIVATION, null, contentValues);
		}

		/**
		 * Operations to update the database from version 2 to version 3.
		 * 
		 * @param _db
		 *            The database.
		 */
		private void updateFromVersion3ToVersion4(SQLiteDatabase _db) {
			if (null == _db)
				return;

			if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Agregando las tablas: "
					+ Constants.TABLE_BANDERAS_BMOVIL + ", "
					+ Constants.TABLE_TEMPORAL_ST + ", "
					+ Constants.TABLE_TEMPORAL_CAMBIO_TELEFONO);

			// Create the new tables.

			_db.execSQL(CREATE_TABLE9);
			_db.execSQL(CREATE_TABLE10);
			_db.execSQL(CREATE_TABLE11);

			// ContentValues contentValues = new ContentValues();
			// contentValues.put(KEY_IDENTIFICADOR, KEY_CAT_BM_DT);
			// contentValues.put(KEY_VALUE,
			// Constants.NON_REGISTERED_ACTIVATION_DATE);
			// _db.insert(Constants.TABLE_BANDERAS_BMOVIL, null, contentValues);
			//
			// contentValues = new ContentValues();
			// contentValues.put(KEY_IDENTIFICADOR, KEY_CAT_BM_HR);
			// contentValues.put(KEY_VALUE,
			// Constants.NON_REGISTERED_ACTIVATION_DATE);
			// _db.insert(Constants.TABLE_TEMPORAL_ST, null, contentValues);
			//
			// contentValues = new ContentValues();
			// contentValues.put(KEY_IDENTIFICADOR, KEY_CAT_BM_HR);
			// contentValues.put(KEY_VALUE,
			// Constants.NON_REGISTERED_ACTIVATION_DATE);
			// _db.insert(Constants.TABLE_TEMPORAL_CAMBIO_TELEFONO, null,
			// contentValues);
		}

		private void updateFromVersion4ToVersion5(SQLiteDatabase _db) {
			if (null == _db)
				return;

			if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(),
				"Pasando informacion y borrando las tablas: "
						+ Constants.TABLE_MBANKING + ", "
						+ Constants.TABLE_BANDERAS_BMOVIL + ", "
						+ Constants.TABLE_TEMPORAL_ST + ", "
						+ Constants.TABLE_TEMPORAL_CAMBIO_TELEFONO + " "
						+ Constants.TABLE_TELEFONICAS_CATALOG +" "
						+ Constants.TABLE_APP_STATUS + " "
						+ Constants.TABLE_BMOVIL_ACTIVATION+ " "
						+ Constants.TABLE_PROFILE_STATUS+" "
						+ Constants.TABLE_CAT
						+ Constants.TABLE_NUEVO_CATALOGO
						+ Constants.TABLE_AUTHENTICATION_CATALOG);

			// Pasar informacion de tabla a archivo
			updateMBanking(_db);
			updateTemporalCambioTelefono(_db);
			updateBanderasBMovil(_db);
			updateTemporalST(_db);
			updateCambioPerfil(_db);
			updateCompaniasTelefonicasCatalog(_db);
			updateActivacionBmovil(_db);
			updateAppStatus(_db);
			updateCat(_db);
			updateNuevoCatalago(_db);
			updateCatalogoAutenticacion(_db);
		}
		
		private void updateFromVersion5ToVersion6(SQLiteDatabase _db) {
			DatosBmovilFileManager.getCurrent().setCatalogo1S("0");
			DatosBmovilFileManager.getCurrent().setCatalogo4S("0");
			DatosBmovilFileManager.getCurrent().setCatalogo5S("0");
			DatosBmovilFileManager.getCurrent().setCatalogo8S("0");
			DatosBmovilFileManager.getCurrent().setCatalogoTAS("0");
			DatosBmovilFileManager.getCurrent().setCatalogoDMS("0");
			DatosBmovilFileManager.getCurrent().setCatalogoServS("0");
		}

	}
	

	
	private static void updateCatalogoAutenticacion(SQLiteDatabase _db){
		_db.execSQL("DROP TABLE IF EXISTS "
				+ Constants.TABLE_AUTHENTICATION_CATALOG);	
	}
	
	private static void updateNuevoCatalago(SQLiteDatabase _db){
		_db.execSQL("DROP TABLE IF EXISTS "
				+ Constants.TABLE_NUEVO_CATALOGO);	
	}
	
	private static void updateCat(SQLiteDatabase _db){
		_db.execSQL("DROP TABLE IF EXISTS "
				+ Constants.TABLE_CAT);	
	}
	
	private static void updateAppStatus(SQLiteDatabase _db){
		_db.execSQL("DROP TABLE IF EXISTS "
				+ Constants.TABLE_APP_STATUS);	
	}

	private static void updateActivacionBmovil(SQLiteDatabase _db) {
		Cursor re = null;
		re = _db.query(Constants.TABLE_BMOVIL_ACTIVATION, new String[] {
				KEY_ID, KEY_IDENTIFICADOR, KEY_VALUE }, null, null, null, null,
				null);

		if (re.moveToFirst()) {
			do {
				ActivacionBmovilFileManager.getCurrent()
						.addActivacion(
								re.getString(re
										.getColumnIndex(KEY_IDENTIFICADOR)),
								re.getString(re.getColumnIndex(KEY_VALUE)));
			} while (re.moveToNext());
		}
		_db.execSQL("DROP TABLE IF EXISTS "
				+ Constants.TABLE_BMOVIL_ACTIVATION);
	}

	private static void updateCompaniasTelefonicasCatalog(SQLiteDatabase _db) {
		Cursor re = null;
		re = _db.query(Constants.TABLE_TELEFONICAS_CATALOG, new String[] {
				KEY_ID, KEY_IDENTIFICADOR, KEY_CAT_TL_IM }, null, null, null, null,
				null);

		if (re.moveToFirst()) {
			do {
				CompaniasTelefonicasCatalogFileManager.getCurrent()
						.addNewValueCatalog(
								re.getString(re
										.getColumnIndex(KEY_IDENTIFICADOR)),
								re.getString(re.getColumnIndex(KEY_CAT_TL_IM)));
			} while (re.moveToNext());
		}
		_db.execSQL("DROP TABLE IF EXISTS "
				+ Constants.TABLE_TELEFONICAS_CATALOG);
	}

	private static void updateCambioPerfil(SQLiteDatabase _db) {
		Cursor re = null;
		re = _db.query(Constants.TABLE_PROFILE_STATUS, new String[] { KEY_ID,
				KEY_IDENTIFICADOR, KEY_VALUE }, null, null, null, null, null);

		if (re.moveToFirst()) {
			do {
				String id = re.getString(re
						.getColumnIndex(Constants.IDENTIFICADOR));

				if (CambioPerfilFileManager.PROP_CAMBIO_PERFIL_ID.equals(id)) {
					CambioPerfilFileManager.getCurrent().setCambioPerfil(
							re.getString(re.getColumnIndex(KEY_VALUE)));
				}

			} while (re.moveToNext());
		}
		_db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_PROFILE_STATUS);
	}

	private static void updateTemporalST(SQLiteDatabase _db) {
		Cursor re = null;
		re = _db.query(Constants.TABLE_TEMPORAL_ST, new String[] { KEY_ID,
				KEY_IDENTIFICADOR, KEY_VALUE }, null, null, null, null, null);

		if (re.moveToFirst()) {
			do {
				String id = re.getString(re
						.getColumnIndex(Constants.IDENTIFICADOR));

				if (TemporalSTFileManager.PROP_TEMPORALST_CELULAR.equals(id)) {
					TemporalSTFileManager.getCurrent().setTemporalSTCelular(
							re.getString(re.getColumnIndex(KEY_VALUE)));
				} else if (TemporalSTFileManager.PROP_TEMPORALST_COMPANIA
						.equals(id)) {
					TemporalSTFileManager.getCurrent().setTemporalSTCompania(
							re.getString(re.getColumnIndex(KEY_VALUE)));
				} else if (TemporalSTFileManager.PROP_TEMPORALST_CONTRASENA
						.equals(id)) {
					TemporalSTFileManager.getCurrent().setTemporalSTContrasena(
							re.getString(re.getColumnIndex(KEY_VALUE)));
				} else if (TemporalSTFileManager.PROP_TEMPORALST_CORREO
						.equals(id)) {
					TemporalSTFileManager.getCurrent().setTemporalSTCorreo(
							re.getString(re.getColumnIndex(KEY_VALUE)));
				} else if (TemporalSTFileManager.PROP_TEMPORALST_PERFIL
						.equals(id)) {
					TemporalSTFileManager.getCurrent().setTemporalSTPerfil(
							re.getString(re.getColumnIndex(KEY_VALUE)));
				} else if (TemporalSTFileManager.PROP_TEMPORALST_TARJETA
						.equals(id)) {
					TemporalSTFileManager.getCurrent().setTemporalSTTarjeta(
							re.getString(re.getColumnIndex(KEY_VALUE)));
				}

			} while (re.moveToNext());
		}
		_db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_TEMPORAL_ST);
	}

	private static void updateBanderasBMovil(SQLiteDatabase _db) {
		Cursor re = null;
		re = _db.query(Constants.TABLE_BANDERAS_BMOVIL, new String[] { KEY_ID,
				KEY_IDENTIFICADOR, KEY_VALUE }, null, null, null, null, null);

		if (re.moveToFirst()) {
			do {
				String id = re.getString(re
						.getColumnIndex(Constants.IDENTIFICADOR));

				if (BanderasBMovilFileManager.PROP_BANDERAS_CAMBIO_CELULAR
						.equals(id)) {
					BanderasBMovilFileManager.getCurrent()
							.setBanderasBMovilCambioCelular(
									re.getString(re.getColumnIndex(KEY_VALUE)));
				} else if (BanderasBMovilFileManager.PROP_BANDERAS_CAMBIO_PERFIL
						.equals(id)) {
					BanderasBMovilFileManager.getCurrent()
							.setBanderasBMovilCambioPerfil(
									re.getString(re.getColumnIndex(KEY_VALUE)));
				} else if (BanderasBMovilFileManager.PROP_BANDERAS_CONTRATAR
						.equals(id)) {
					BanderasBMovilFileManager.getCurrent()
							.setBanderasBMovilContratar(
									re.getString(re.getColumnIndex(KEY_VALUE)));
				} else if (BanderasBMovilFileManager.PROP_BANDERAS_CONTRATAR2x1
						.equals(id)) {
					BanderasBMovilFileManager.getCurrent()
							.setBanderasBMovilContratar2x1(
									re.getString(re.getColumnIndex(KEY_VALUE)));
				}
			} while (re.moveToNext());
		}
		_db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_BANDERAS_BMOVIL);
	}

	private static void updateTemporalCambioTelefono(SQLiteDatabase _db) {
		Cursor re = null;
		re = _db.query(Constants.TABLE_TEMPORAL_CAMBIO_TELEFONO, new String[] {
				KEY_ID, KEY_IDENTIFICADOR, KEY_VALUE }, null, null, null, null,
				null);

		if (re.moveToFirst()) {
			do {
				String id = re.getString(re
						.getColumnIndex(Constants.IDENTIFICADOR));

				if (TemporalCambioTelefonoFileManager.PROP_TEMPORAL_CAMBIO_TELEFONO_TARJETA
						.equals(id)) {
					TemporalCambioTelefonoFileManager.getCurrent()
							.setTemporalCambioTelefonoTarjeta(
									re.getString(re.getColumnIndex(KEY_VALUE)));
				}
			} while (re.moveToNext());
		}
		_db.execSQL("DROP TABLE IF EXISTS "
				+ Constants.TABLE_TEMPORAL_CAMBIO_TELEFONO);
	}

	private static void updateMBanking(SQLiteDatabase _db) {
		Cursor re = null;
		re = _db.query(Constants.TABLE_MBANKING, new String[] { KEY_ID,
				KEY_IDENTIFICADOR, KEY_VALUE, KEY_SCATALOGV, KEY_ICATALOGV },
				null, null, null, null, null);

		if (re.moveToFirst()) {
			do {
				int id = re.getInt(re.getColumnIndex(Constants.IDENTIFICADOR));

				switch (id) {
				case ACTIVATED_RMS_ID:
					DatosBmovilFileManager.getCurrent().setActivado(
							Boolean.parseBoolean(re.getString(re
									.getColumnIndex(KEY_VALUE))));
					PropertiesManager.getCurrent().setBmovilActivated(Boolean.parseBoolean(re.getString(re
							.getColumnIndex(KEY_VALUE))));
					break;
				case PENDING_STATUS_RMS_ID:
					DatosBmovilFileManager.getCurrent().setPendienteDeDescarga(re.getString(re
									.getColumnIndex(KEY_VALUE)));
					break;
				case LOGIN_RMS_ID:
					DatosBmovilFileManager.getCurrent().setLogin(
							re.getString(re
									.getColumnIndex(KEY_VALUE)));
					break;
				case SEED_RMS_ID:
					DatosBmovilFileManager.getCurrent().setSeed(
							String.valueOf(re.getLong(re
									.getColumnIndex(KEY_VALUE))));
					break;
				case VIA_RMS_ID:
					DatosBmovilFileManager.getCurrent().setVia(
							re.getString(re.getColumnIndex(KEY_VALUE)));
					break;
				case TOKEN_RMS_ID:
					DatosBmovilFileManager.getCurrent().setToken(
							re.getString(re.getColumnIndex(KEY_VALUE)));
					break;
				case SOFTTOKEN_RMS_ID:
					DatosBmovilFileManager.getCurrent().setSoftoken(
							Boolean.parseBoolean(re.getString(re
									.getColumnIndex(KEY_VALUE))));
					PropertiesManager.getCurrent().setSofttokenActivated(Boolean.parseBoolean(re.getString(re
									.getColumnIndex(KEY_VALUE))));
					break;
				case CATALOG1_VERSION_RMS_ID:
					DatosBmovilFileManager.getCurrent().setCatalogo1S("0");
					break;
				case CATALOG4_VERSION_RMS_ID:
					DatosBmovilFileManager.getCurrent().setCatalogo4S("0");
					break;
				case CATALOG5_VERSION_RMS_ID:
					DatosBmovilFileManager.getCurrent().setCatalogo5S("0");
					break;
				case CATALOG8_VERSION_RMS_ID:
					DatosBmovilFileManager.getCurrent().setCatalogo8S("0");
					break;
				case TA_CATALOG_VERSION_RMS_ID:
					DatosBmovilFileManager.getCurrent().setCatalogoTAS("0");
					break;
				case DM_CATALOG_VERSION_RMS_ID:
					DatosBmovilFileManager.getCurrent().setCatalogoDMS("0");
					break;
				case SERVICES_CATALOG_VERSION_RMS_ID:
					DatosBmovilFileManager.getCurrent().setCatalogoServS("0");
					break;
				}
			} while (re.moveToNext());
		}
		_db.execSQL("DROP TABLE IF EXISTS " + Constants.TABLE_MBANKING);
	}
}
