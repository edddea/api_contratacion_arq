package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.Collections;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ErrorData;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.MenuTransferenciasInterbancariasViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaSPEIData;
import suitebancomer.aplicaciones.bmovil.classes.model.TransferenciaSPEI;
import suitebancomer.classes.gui.delegates.BaseDelegate;

import static suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaMovimientosInterbancariosDelegate.CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID;

/**
 * Created by Nicolas Arriaza on 04/08/2015.
 */
public class MenuTransferenciasInterbancariasDelegate extends BaseDelegate {

    /**************************** Constantes ****************************/
    public static final long MENU_TRANSFERENCIAS_INTERBANCARIAS_DELEGATE_ID = 8610021420323232323L;

    /**************************** Variables ****************************/
    private MenuTransferenciasInterbancariasViewController viewController;

    /**************************** Getters & Setters ****************************/
    public MenuTransferenciasInterbancariasViewController getViewController() {
        return viewController;
    }

    public void setViewController(MenuTransferenciasInterbancariasViewController viewController) {
        this.viewController = viewController;
    }


    /**************************** Metodos ****************************/
    public ArrayList<?> getDatosTablaMenu() {

        ArrayList<ArrayList<String>> lista = new ArrayList<ArrayList<String>>();

        ArrayList<String> registro = null;

        registro = new ArrayList<String>();
        registro.add(SuiteApp.appContext.getString(R.string.bmovil_spei_menuTransferencias_table_opcionEstatus));
        registro.add(SuiteApp.appContext.getString(R.string.bmovil_spei_menuTransferencias_table_opcionEstatus));
        lista.add(registro);

        registro = new ArrayList<String>();
        registro.add(SuiteApp.appContext.getString(R.string.bmovil_spei_menuTransferencias_table_opcionMovimientos));
        registro.add(SuiteApp.appContext.getString(R.string.bmovil_spei_menuTransferencias_table_opcionMovimientos));
        lista.add(registro);

        return lista;
    }

    @Override
    public void performAction(Object obj) {
        if(obj instanceof String){
            String selected = (String) obj;
            opcionSeleccionada(selected);
        }
    }

    private void opcionSeleccionada(String selectedOption){
        Session session = Session.getInstance(SuiteApp.getInstance());

        if(selectedOption.equals(SuiteApp.appContext.getString(R.string.bmovil_spei_menuTransferencias_table_opcionEstatus))){
            if(Server.ALLOW_LOG) Log.d("CGI", "MenuTransferenciasInterbancariasDelegate >> Estatus");

            if(Autenticacion.getInstance().operarOperacion(Constants.Operacion.consultaTransferenciaSPEI,	session.getClientProfile())){
                SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConsultarInterbancarios();
            }

        }else if(selectedOption.equals(SuiteApp.appContext.getString(R.string.bmovil_spei_menuTransferencias_table_opcionMovimientos))){
            if(Server.ALLOW_LOG) Log.d("CGI", "MenuTransferenciasInterbancariasDelegate >> Movimientos");
            if(Autenticacion.getInstance().operarOperacion(Constants.Operacion.consultaMovInterbancarios,	session.getClientProfile())){
                showConsultaMovimientosInter();
            }
        }
    }

    private void showConsultaMovimientosInter(){

        ConsultaMovimientosInterbancariosDelegate delegate = (ConsultaMovimientosInterbancariosDelegate) SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getBaseDelegateForKey(CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID);
        if (null == delegate) {
            delegate = new ConsultaMovimientosInterbancariosDelegate();
            SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().addDelegateToHashMap(CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID,
                    delegate);
        }

        String cadenaAutenticacion = Autenticacion.getInstance().getCadenaAutenticacion(Constants.Operacion.consultaMovInterbancarios, Session.getInstance(SuiteApp.getInstance()).getClientProfile());

        if(cadenaAutenticacion.equals("00000")){
            // Go normal
            if(delegate.preCheckCuentasOrigen()){
                SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController().showInformationAlert("Para ver tus movimientos deberás cambiar tu cuenta asociada a tu servicio por una tarjeta de débito desde el menú administrar.");
            }else{
                String periodo = "0";
                delegate.cargaCuentasOrigen();
                delegate.consultaMovimientosInterbancarios(periodo, viewController);
            }
        }else{
            // Go confirmacion
            SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(delegate, 0, 0, 0, 0);
        }

    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        viewController.ocultaIndicadorActividad();

        if(operationId == Server.OP_CONSULTA_SPEI) {

            ConsultaMovimientosInterbancariosDelegate delegate = (ConsultaMovimientosInterbancariosDelegate) SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getBaseDelegateForKey(CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID);
            if (null == delegate) {
                delegate = new ConsultaMovimientosInterbancariosDelegate();
                SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().addDelegateToHashMap(CONSULTA_MOVIMIENTOS_INTERBANCARIOS_DELEGATE_ID,
                        delegate);
            }

            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL){

                ConsultaSPEIData data = (ConsultaSPEIData) response.getResponse();
                delegate.setConsultaSPEIData(data);

                // Ordenamos el array segun clave de rastreo
                Collections.sort(delegate.getConsultaSPEIData().getTransferencias(), new TransferenciaSPEI.ClaveRastreoComparator());

                delegate.addListaMovimientos(delegate.getCuentaSeleccionada(), data.getPeriodoConsulta(), data);
                delegate.setPeriodo(Integer.valueOf(data.getPeriodoConsulta()));

                SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConsultaMovimientosInterbancariosViewController();
            } else {
                delegate.setErrorData(new ErrorData(viewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText()));
                SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showConsultaMovimientosInterbancariosViewController();
            }

        }

    }
}
