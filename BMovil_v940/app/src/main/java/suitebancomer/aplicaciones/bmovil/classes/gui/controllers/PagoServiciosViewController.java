package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.model.CatalogoVersionado;
import bancomer.api.common.model.Convenio;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagoServiciosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.AmountField;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.SeleccionHorizontalViewController;
import tracking.TrackingHelper;

public class PagoServiciosViewController extends BaseViewController implements View.OnClickListener{

	private LinearLayout layoutCuentaOrigen;
	private TextView tituloSeleccionHorizontal;
	private LinearLayout layoutSeleccionHorizontal;
	private SeleccionHorizontalViewController seleccionHorizontal;
	public CuentaOrigenViewController componenteCtaOrigen;
	private PagoServiciosDelegate pagoServiciosDelegate;
	private TextView convenioLabel;
	private EditText convenio;
	private TextView referenciaLabel;
	private EditText referencia;
	private TextView conceptoLabel;
	private EditText concepto;
	private TextView importeLabel;
	private AmountField importe;
	private ImageButton leerCodigoButton;
	private	ImageButton continuarButton;
	private	ImageButton ayudaReferenciaButton;
	private	ImageButton ayudaConceptoButton;
	private boolean skipParentValidation;
	//AMZ
		public BmovilViewsController parentManager;
		//AMZ
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_pagar_servicios);
		setTitle(R.string.servicesPayment_title, R.drawable.icono_pagar_servicios);
		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID));
		pagoServiciosDelegate = (PagoServiciosDelegate)getDelegate();
		if (pagoServiciosDelegate == null) {
			pagoServiciosDelegate = new PagoServiciosDelegate();
			parentViewsController.addDelegateToHashMap(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID, pagoServiciosDelegate);
			setDelegate(pagoServiciosDelegate);
		}
		pagoServiciosDelegate.setControladorPagos(this);
		
		findViews();
		scaleToScreenSize();
	
		leerCodigoButton.setOnClickListener(this);
		ayudaReferenciaButton.setOnClickListener(this);
		ayudaConceptoButton.setOnClickListener(this);

		InputFilter[] filters = new InputFilter[1];
		filters[0] = new InputFilter.LengthFilter(Constants.SERVICES_PAYMENT_CIE_AGREEMENT_LENGTH);
		convenio.setFilters(filters);
		filters = new InputFilter[1];
		filters[0] = new InputFilter.LengthFilter(Constants.SERVICES_PAYMENT_REFERENCE_LENGTH);
		referencia.setFilters(filters);
		filters = new InputFilter[1];
		filters[0] = new InputFilter.LengthFilter(Constants.SERVICES_PAYMENT_REASON_LENGTH);
		concepto.setFilters(filters);
		filters = new InputFilter[1];
		filters[0] = new InputFilter.LengthFilter(Constants.AMOUNT_LENGTH);
		importe.setFilters(filters);
		continuarButton.setOnClickListener(this);
		
		cargaCuentaOrigenComponent();
		cargaSeleccionHorizontal();
		pagoServiciosDelegate.configuraPantalla();
		
		convenio.addTextChangedListener(new BmovilTextWatcher(this));
		referencia.addTextChangedListener(new BmovilTextWatcher(this));
		concepto.addTextChangedListener(new BmovilTextWatcher(this));
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
			
				TrackingHelper.trackState("nuevo", parentManager.estados);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (!skipParentValidation && parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		
		parentViewsController.setCurrentActivityApp(this);
		pagoServiciosDelegate.setControladorPagos(this);
		skipParentValidation = false;
		componenteCtaOrigen.setIndiceCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getListaCuentaOrigen().indexOf(pagoServiciosDelegate.getPagoServicio().getCuentaOrigen()));
		//componenteCtaOrigen.actualizaComponente();
		componenteCtaOrigen.actualizaComponente(false);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		if (!skipParentValidation) {
			parentViewsController.consumeAccionesDePausa();
		}
	}
	
	public void cargaCuentaOrigenComponent(){
		ArrayList<Account> listaCuetasAMostrar = pagoServiciosDelegate.cargaCuentasOrigen();
		GuiTools guiTools = GuiTools.getCurrent();
		
		LinearLayout.LayoutParams params;
		params = new LayoutParams(guiTools.getEquivalenceInPixels(280.0), LinearLayout.LayoutParams.WRAP_CONTENT);
		
		//LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		params.topMargin = 8;
		//params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
		//params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);

		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.setDelegate(pagoServiciosDelegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
		componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(pagoServiciosDelegate.getPagoServicio().getCuentaOrigen()));
		componenteCtaOrigen.init();
		layoutCuentaOrigen.addView(componenteCtaOrigen);
	}
	
	public void actualizaOpcionSeleccionada(Account account) {
		pagoServiciosDelegate.setCuentaSeleccionada(account);
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled(true);
	}
	
	public void actualizaCampos(Convenio servicio) {
		pagoServiciosDelegate.setConvenioSeleccionado(servicio);
		limpiarCampos();
	}
	
	public void prellenaCampos(Convenio servicio, String strConvenio, String strReferencia, String strConcepto,
							   String strImporte) {
		if (servicio == null) {
			seleccionHorizontal.setOtroAsSelected();
		} else {
			seleccionHorizontal.setSelectedItem(servicio);
		}
		seleccionHorizontal.bloquearComponente();
		convenio.setText(strConvenio);
		convenio.setFocusable(false);
		convenio.setEnabled(false);
		convenio.setClickable(false);
		referencia.setText(strReferencia);
//		referencia.setFocusable(false);
//		referencia.setEnabled(false);
//		referencia.setClickable(false);
		concepto.setText(strConcepto);
//		concepto.setFocusable(false);
//		concepto.setEnabled(false);
//		concepto.setClickable(false);
		
		//TODO referencia y concepto deben bloquearse cuando es un rapido
		
		if (strImporte != null && !strImporte.equals("")) {
			importe.setAmount(Tools.formatAmountForServer(strImporte));
		}
	}
	
	public void llenaCamposConCodigo(String strReferencia, String strConcepto, String strImporte) {
		referencia.setText(strReferencia);
		//referencia.setFocusable(false);
		//referencia.setEnabled(false);
		//referencia.setClickable(false);
		concepto.setText(strConcepto);
		//concepto.setFocusable(false);
		//concepto.setEnabled(false);
		//concepto.setClickable(false);
		if (strImporte != null && !strImporte.equals("")) {
			importe.setAmount(strImporte);
		} else {
			importe.setAmount(getString(R.string.servicesPayment_amountDefault));
		}
	}
	
	public void showCamposConvenio() {
		convenioLabel.setVisibility(View.VISIBLE);
		convenio.setVisibility(View.VISIBLE);
		
		convenio.requestFocus();
	}
	
	public void hideCamposConvenio() {
		convenio.setText("");
		convenioLabel.setVisibility(View.GONE);
		convenio.setVisibility(View.GONE);
		
		referencia.requestFocus();
	}
	
	public void showLeerCodigoButton() {
		leerCodigoButton.setVisibility(View.VISIBLE);
	}
	
	public void hideLeerCodigoButton() {
		leerCodigoButton.setVisibility(View.GONE);
	}
	
	public void showCamposImporte() {
		importeLabel.setVisibility(View.VISIBLE);
		importe.setVisibility(View.VISIBLE);
		
		importe.setImeOptions(EditorInfo.IME_ACTION_DONE);
		concepto.setImeOptions(EditorInfo.IME_ACTION_NEXT);
	}
	
	public void hideCamposImporte() {
		importeLabel.setVisibility(View.GONE);
		importe.setVisibility(View.GONE);
		
		importe.setImeOptions(EditorInfo.IME_ACTION_NONE);
		concepto.setImeOptions(EditorInfo.IME_ACTION_DONE);
	}
	
	public void cargaSeleccionHorizontal() {
		tituloSeleccionHorizontal.setText(getString(R.string.servicesPayment_servicesComponentTitle));
		LinearLayout.LayoutParams params1 = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		//params1.topMargin = 8;
		//params1.leftMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
		//params1.rightMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
		
		CatalogoVersionado catalogoServicios = Session.getInstance(SuiteApp.appContext).getCatalogoServicios();
		Vector<Object> vectorServicios = catalogoServicios.getObjetos();
		int serviciosSize = vectorServicios.size();
		ArrayList<Object> listaServicios = new ArrayList<Object>(serviciosSize);
		
		Convenio currentConvenio = null;
		Convenio matchingConvenio = null;
		
		for (int i=0; i<serviciosSize; i++) {
			currentConvenio = (Convenio)vectorServicios.get(i);
			listaServicios.add(currentConvenio);
			// TODO: modificacion
			if (currentConvenio.getNumeroConvenio().equals(pagoServiciosDelegate.getPagoServicio().getConvenioServicio())) {
				matchingConvenio = currentConvenio;
				hideCamposConvenio();
			}
						
			currentConvenio = null;
		}
		vectorServicios = null;
		seleccionHorizontal = new SeleccionHorizontalViewController(this, params1, listaServicios, getDelegate(), true);
		layoutSeleccionHorizontal.addView(seleccionHorizontal);
		if (pagoServiciosDelegate.getPagoServicio().getConvenioServicio().equals("")) {
			pagoServiciosDelegate.setConvenioSeleccionado((Convenio)listaServicios.get(0));
		}else{
			if(matchingConvenio != null) pagoServiciosDelegate.setConvenioSeleccionado(matchingConvenio);
		}
	}

	private void limpiarCampos() {
		convenio.setText("");
		referencia.setText("");
		concepto.setText("");
		importe.reset();
	}
	
	@Override
	public void onClick(View v) {
		if (v == leerCodigoButton) {
			((BmovilViewsController)parentViewsController).showLecturaCodigoViewController(123456);
		} else if (v == ayudaReferenciaButton) {
			pagoServiciosDelegate.requestReferenceHelp();
		} else if (v == ayudaConceptoButton) {
			pagoServiciosDelegate.requestReasonHelp();
		} else if (v == continuarButton) {
			pagoServiciosDelegate.validaDatos(referencia.getText().toString(), Tools.removeSpecialCharacters(concepto.getText().toString()),
					importe.getAmount().toString(), convenio.getText().toString());
			//ARR
			if(pagoServiciosDelegate.res)
			{
				//ARR
				Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
				
				//ARR
				paso2OperacionMap.put("evento_paso2", "event47");
				paso2OperacionMap.put("&&products", "operaciones;pagar+servicio");
				paso2OperacionMap.put("eVar12", "paso2:referencia servicio");

				TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
			}
		
		}
		
	}	
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 123456) {
			if (resultCode == RESULT_OK) {
				String result = data.getExtras().getString("DATA");
				if(Server.ALLOW_LOG) Log.d("BancomerSuite :: barcodeScanner", "Barcode decode: " + result);
				
				Convenio cie = (Convenio)seleccionHorizontal.getSelectedItem();
				pagoServiciosDelegate.consultaDatosCodigo(result, cie.getNumeroConvenio());
			}
		} 
	}
	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		pagoServiciosDelegate.analyzeResponse(operationId, response);
	}
	
	private void findViews() {
		layoutCuentaOrigen = (LinearLayout)findViewById(R.id.pagarServicios_cuentaOrigen);
		tituloSeleccionHorizontal = (TextView)findViewById(R.id.pagarServicios_titulo_seleccionHorizontal);
		layoutSeleccionHorizontal = (LinearLayout)findViewById(R.id.pagarServicios_seleccionHorizontal);
		leerCodigoButton = (ImageButton)findViewById(R.id.pagarServicios_leerCodigo);
		convenioLabel = (TextView)findViewById(R.id.pagarServicios_convenio_label);
		convenio = (EditText)findViewById(R.id.pagarServicios_convenio);
		referenciaLabel = (TextView)findViewById(R.id.pagarServicios_referencia_label);
		referencia = (EditText)findViewById(R.id.pagarServicios_referencia);
		conceptoLabel = (TextView)findViewById(R.id.pagarServicios_concepto_label);
		concepto = (EditText)findViewById(R.id.pagarServicios_concepto);
		importeLabel = (TextView)findViewById(R.id.pagarServicios_monto_label);
		importe = (AmountField)findViewById(R.id.pagarServicios_monto);
		
		
		ayudaReferenciaButton = (ImageButton)findViewById(R.id.pagarServicios_ayuda_referencia);
		ayudaConceptoButton = (ImageButton)findViewById(R.id.pagarServicios_ayuda_concepto);
		continuarButton = (ImageButton)findViewById(R.id.pagarServicios_continuar); 
	}
	
	private void scaleToScreenSize() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(tituloSeleccionHorizontal, true);
		guiTools.scale(leerCodigoButton);
		guiTools.scale(convenioLabel, true);
		guiTools.scale(convenio, true);
		guiTools.scale(referenciaLabel, true);
		guiTools.scale(ayudaReferenciaButton);
		guiTools.scale(referencia, true);
		guiTools.scale(conceptoLabel, true);
		guiTools.scale(ayudaConceptoButton);
		guiTools.scale(concepto, true);
		guiTools.scale(importeLabel, true);
		guiTools.scale(importe, true);
		
		guiTools.scale(continuarButton);
		
		
		
		
		guiTools.scale(findViewById(R.id.pagarServicios_cuentaOrigen));
	}
}
