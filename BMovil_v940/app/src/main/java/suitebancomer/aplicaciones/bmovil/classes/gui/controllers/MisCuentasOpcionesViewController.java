package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import bancomer.api.consultaotroscreditos.models.Creditos;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MisCuentasDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagoTdcDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.TransferirDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import tracking.TrackingHelper;

public class MisCuentasOpcionesViewController extends Dialog { 
	
	BmovilViewsController parentManager;
	LinearLayout vista;
	ListaSeleccionViewController listaSeleccion;
	Dialog dialogoOpcionesQuiero;
	Account cuentaSeleccionada;
	Creditos creditoSeleccionado;
	MisCuentasDelegate delegate;

	public MisCuentasOpcionesViewController(Context context, BmovilViewsController parentManager, Account cuenta, MisCuentasDelegate delegate) {
		super(context);

		this.setContentView(R.layout.layout_mis_cuentas_opciones_view);
		
		this.parentManager = parentManager;
		this.cuentaSeleccionada = cuenta;
		this.delegate = delegate;
		vista = (LinearLayout) findViewById(R.id.mis_Cuentas_opciones_layout);
		llenarListaOpciones();
		this.setTitle(SuiteApp.appContext.getString(R.string.mis_cuentas_quiero_title));
		this.show();
	}

	public MisCuentasOpcionesViewController(Context context, BmovilViewsController parentManager, Creditos credito, MisCuentasDelegate delegate) {
		super(context);

		this.setContentView(R.layout.layout_mis_cuentas_opciones_view);

		this.parentManager = parentManager;
		this.creditoSeleccionado = credito;
		this.delegate = delegate;
		vista = (LinearLayout) findViewById(R.id.mis_Cuentas_opciones_layout);
		llenarListaOpciones();
		this.setTitle(SuiteApp.appContext.getString(R.string.mis_cuentas_quiero_title));
		this.show();
	}

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	}

	@SuppressWarnings("deprecation")
	public void llenarListaOpciones() {
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		params.leftMargin = 0;
		params.rightMargin = 0;
		params.bottomMargin = 0;

		listaSeleccion = new ListaSeleccionViewController(getContext(), params, parentManager);
		listaSeleccion.setDelegate(delegate);
		listaSeleccion.setLista(delegate.getOpcionesVisibles());
		listaSeleccion.setNumeroColumnas(1);
		listaSeleccion.setOpcionSeleccionada(-1);
		listaSeleccion.setSeleccionable(false);
		listaSeleccion.setAlturaFija(false);

		listaSeleccion.cargarTabla();
		
		vista.addView(listaSeleccion);
	}

	public void opcionSeleccionada(Creditos credito, String opcionSeleccionada) {

		delegate.procesarClickCreditos(credito, opcionSeleccionada);
		//this.dismiss();
	}

	public void opcionSeleccionada(Account cuenta, String opcionSeleccionada) {	
		//ARR
				Map<String,Object> inicioOperacionMap = new HashMap<String, Object>();

		
		if (opcionSeleccionada.equals(Constants.OPERACION_VER_MOVIMIENTOS_TYPE)) {
			if(Server.ALLOW_LOG) Log.d("MisCuentasOpcionesViewController", "Entraste a ver movimientos");
			//ARR
			inicioOperacionMap.put("evento_inicio", "event45");
			inicioOperacionMap.put("&&products","consulta;movimientos");
			inicioOperacionMap.put("eVar12", "inicio");

			TrackingHelper.trackInicioOperacion(inicioOperacionMap);
			parentManager.showConsultarMovimientos();
			dismiss();
		}else if (opcionSeleccionada.equals(Constants.EXTERNAL_TRANSFER_TYPE)) {
			if(Autenticacion.getInstance().isOperable(Constants.Operacion.transferirInterbancaria, Session.getInstance(SuiteApp.appContext).getClientProfile())){
				if(Server.ALLOW_LOG) Log.d("MisCuentasOpcionesViewController", "Entraste a transferencias otros bancos");
				TransferirDelegate delegate = new TransferirDelegate();				
				if (delegate.isOpenHours()) {
					dismiss();
					//ARR
					inicioOperacionMap.put("evento_inicio", "event45");
					inicioOperacionMap.put("&&products", "operaciones;transferencias+otros bancos");
					inicioOperacionMap.put("eVar12", "inicio");

					TrackingHelper.trackInicioOperacion(inicioOperacionMap);
					parentManager.showTransferViewController(Constants.Operacion.transferirInterbancaria.value, false, false);
				}
				else {
					dismiss();
					if(Session.getListA().size()<=0 && Session.getInstance(SuiteApp.getInstance()).isWeekend())
						((BmovilViewsController)parentManager).getCurrentViewControllerApp().showInformationAlert(Session.getInstance(SuiteApp.getInstance()).notAssociatedMessage());
					else
						((BmovilViewsController)parentManager).getCurrentViewControllerApp().showInformationAlert(delegate.textoMensajeAlerta());
				}
			}else{
				//TODO undefined in P026 Mis cuentas Quiero - RN16
			}
		} else if (opcionSeleccionada.equals(Constants.SERVICE_PAYMENT_TYPE)) {
			if(Autenticacion.getInstance().isOperable(Constants.Operacion.pagoServicios, Session.getInstance(SuiteApp.appContext).getClientProfile())){
				PagarDelegate pagarDelegate = new PagarDelegate();
				if(pagarDelegate.isOpenHours()){
					
				dismiss();

				//ARR
				inicioOperacionMap.put("evento_inicio", "event45");
				inicioOperacionMap.put("&&products", "operaciones;pagar+servicio");
				inicioOperacionMap.put("eVar12", "inicio");

				TrackingHelper.trackInicioOperacion(inicioOperacionMap);
				parentManager.showPagosViewController(Constants.Operacion.pagoServicios);
				}
				else{
					((BmovilViewsController)parentManager).getCurrentViewControllerApp().showInformationAlert(pagarDelegate.textoMensajeAlerta());
				}
			}else{
				//TODO undefined in P026 Mis cuentas Quiero - RN16
			}
		} else if (opcionSeleccionada.equals(Constants.AIRTIME_PURCHASE_TYPE)) {
			if(Autenticacion.getInstance().isOperable(Constants.Operacion.compraTiempoAire, Session.getInstance(SuiteApp.appContext).getClientProfile())){
				dismiss();
				//ARR
				inicioOperacionMap.put("evento_inicio", "event45");
				inicioOperacionMap.put("&&products", "operaciones;comprar+tiempo aire");
				inicioOperacionMap.put("eVar12", "inicio");
				parentManager.showTiempoAireViewController(Constants.Operacion.compraTiempoAire);
			}else{
				//TODO undefined in P026 Mis cuentas Quiero - RN16
			}
			// TODO:_niko
			}else if (opcionSeleccionada.equals(Constants.OPERACION_TRASPASO_MIS_CUENTAS)) {
		
				inicioOperacionMap.put("evento_inicio", "event45");
				inicioOperacionMap.put("&&products", "transferencias;traspaso+mis cuentas");
				inicioOperacionMap.put("eVar12", "inicio");

				TrackingHelper.trackInicioOperacion(inicioOperacionMap);
//				if(Autenticacion.getInstance().isOperable(Constants.Operacion.transferirBancomer, Session.getInstance(SuiteApp.appContext).getClientProfile())){
				if (Session.getInstance(SuiteApp.appContext).getAccounts().length > Constants.CUENTAS_PROPIAS_LENGTH) {			
				dismiss();
				parentManager.showTransferirMisCuentas();
				} else {
					((BmovilViewsController)parentManager).getCurrentViewControllerApp().showInformationAlert(R.string.opcionesTransfer_alerta_texto);
				}
//			}else{
//				//TODO undefined in P026 Mis cuentas Quiero - RN16
//			}
		} else if (opcionSeleccionada.equals(Constants.INTERNAL_TRANSFER_DEBIT_TYPE)) {
			//ARR
			inicioOperacionMap.put("evento_inicio", "event45");
			inicioOperacionMap.put("&&products", "operaciones;transferencias+otra cuenta bbva bancomer");
			inicioOperacionMap.put("eVar12", "inicio");

			TrackingHelper.trackInicioOperacion(inicioOperacionMap);
			if(Autenticacion.getInstance().isOperable(Constants.Operacion.transferirBancomer, Session.getInstance(SuiteApp.appContext).getClientProfile())){
				dismiss();
				boolean esTDC = cuenta.getType().equalsIgnoreCase("TC");
				
				if (!esTDC) {
					parentManager.showTransferViewController(Constants.Operacion.transferirBancomer.value, false, false);		
				} else {
					((BmovilViewsController)parentManager).getCurrentViewControllerApp().showInformationAlert(R.string.opcionesTransfer_alerta_texto_tdc);
				}
			}else{
				//TODO undefined in P026 Mis cuentas Quiero - RN16
			}
		}//SPEI
		/*else if (opcionSeleccionada.equals(Constants.INTERNAL_TRANSFER_EXPRESS_TYPE)) {
			if(Autenticacion.getInstance().isOperable(Constants.Operacion.transferirBancomer, Session.getInstance(SuiteApp.appContext).getClientProfile())){
				dismiss();
				boolean esTDC = cuenta.getType().equalsIgnoreCase("TC");
				
				if (!esTDC) {
					parentManager.showTransferViewController(Constants.Operacion.transferirBancomer.value, true, false);		
				} else {
					((BmovilViewsController)parentManager).getCurrentViewControllerApp().showInformationAlert(R.string.opcionesTransfer_alerta_texto_tdc);
				}
			}else{
				//TODO undefined in P026 Mis cuentas Quiero - RN16
			}
		}Termina SPEI */else if (opcionSeleccionada.equals(Constants.OPERACION_EFECTIVO_MOVIL_TYPE)) {
			//ARR
			inicioOperacionMap.put("evento_inicio", "event45");
			inicioOperacionMap.put("&&products", "operaciones;transferencias+dinero movil");
			inicioOperacionMap.put("eVar12", "inicio");

			TrackingHelper.trackInicioOperacion(inicioOperacionMap);
			if(Autenticacion.getInstance().isOperable(Constants.Operacion.dineroMovil, Session.getInstance(SuiteApp.appContext).getClientProfile())){
				dismiss();
				boolean esTDC = cuenta.getType().equalsIgnoreCase("TC");
				
				//if ((!esTDC) || (esTDC && Session.getInstance(SuiteApp.appContext).puedeOperarConTDCEje())) {
				if (!esTDC) {
					((BmovilViewsController)parentManager).showTransferViewController(Constants.Operacion.dineroMovil.value, false, false);
				} else {
					((BmovilViewsController)parentManager).getCurrentViewControllerApp().showInformationAlert(R.string.opcionesTransfer_alerta_texto_tdc);
				}
			}else{
				//Enviar Dinero m�vil	Recortado	�Transferir Dinero M�vil NFR�	EA#9, paso 1
//				DineroMovilDelegate dineroMovilDelegate = new DineroMovilDelegate();
//				dineroMovilDelegate.comprobarRecortado();
				delegate.comprobarRecortado();
				delegate.setOpcionRetiroSinTarjeta(false);
				//TODO undefined in P026 Mis cuentas Quiero - RN16
			}
		}else if (opcionSeleccionada.equals(Constants.OPERACION_DEPOSITOS_RECIBIDOS)) {
			((BmovilViewsController)parentManager).showConsultarDepositosRecibidosCuenta(cuenta); 
			
		}else if (opcionSeleccionada.equals(Constants.OPERACION_RETIRO_SINTARJETA)) {
				//ARR
				inicioOperacionMap.put("evento_inicio", "event45");
				inicioOperacionMap.put("&&products", "operaciones;transferencias+retiro sin tarjeta");
				inicioOperacionMap.put("eVar12", "inicio");

				TrackingHelper.trackInicioOperacion(inicioOperacionMap);
				if(Autenticacion.getInstance().isOperable(Constants.Operacion.retiroSinTarjeta, Session.getInstance(SuiteApp.appContext).getClientProfile())){
					dismiss();
					boolean esTDC = cuenta.getType().equalsIgnoreCase("TC");
					
					//if ((!esTDC) || (esTDC && Session.getInstance(SuiteApp.appContext).puedeOperarConTDCEje())) {
					if (!esTDC) {
						((BmovilViewsController)parentManager).showRetiroSinTarjetaViewController();
					} else {
						((BmovilViewsController)parentManager).getCurrentViewControllerApp().showInformationAlert(R.string.opcionesTransfer_alerta_texto_tdc);
					}
				}else{
					//Enviar Dinero m�vil	Recortado	�Transferir Dinero M�vil NFR�	EA#9, paso 1
//					DineroMovilDelegate dineroMovilDelegate = new DineroMovilDelegate();
//					dineroMovilDelegate.comprobarRecortado();
					delegate.setOpcionRetiroSinTarjeta(true);
					delegate.comprobarRecortado();
					//TODO undefined in P026 Mis cuentas Quiero - RN16
				}
			
			
		}else if(opcionSeleccionada.equals(Constants.OPERACION_TDC)){
			PagoTdcDelegate delegate = (PagoTdcDelegate) parentManager.getBaseDelegateForKey(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID);
			if (delegate == null) {
				delegate = new PagoTdcDelegate();
				parentManager.addDelegateToHashMap(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID, delegate);
			}

			if (cuentaSeleccionada.getType().equals(Constants.CREDIT_TYPE)) {
				//revisar Alex nov2015
				if (existeMasDeUnaCuenta()) {
					//operation tdc
					delegate.setCuentaOrigenSeleccionada(getCuentaSeleccionada());
					parentManager.showPagoTDCCuenta(cuenta);
				} else {
					parentManager.getCurrentViewControllerApp().showInformationAlert(R.string.error_cuenta_eje_credito);
				}
			} else {
				if (existeCuentaTDC()) {
					delegate.setCuentaOrigenSeleccionada(getCuentaSeleccionada());
					parentManager.showSelectTDCViewController(Constants.Operacion.transferirBancomer.value,Boolean.FALSE,Boolean.TRUE);
				} else {
					parentManager.getCurrentViewControllerApp().showInformationAlert(R.string.error_sin_tdc);
				}
			}
		}else {
			if(Server.ALLOW_LOG) Log.d("MisCuentasOpcionesViewController", "Opciones pendientes para ODTs posteriores");
		}
	}

	private boolean existeCuentaTDC() {
		boolean existeTDC=false;


		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();


		for(Account acc : accounts) {

			if (acc.getType().equals(Constants.CREDIT_TYPE)) {
				existeTDC=true;
			}
		}



		return existeTDC;
	}
	
	public Account getCuentaSeleccionada() {
		return cuentaSeleccionada;
	}
	
private boolean existeMasDeUnaCuenta() {
		
		boolean existenAlMenosUna=true;

		Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();
		
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
		Boolean cuentaEjeTDC = true;
		

				
		if(profile == Constants.Perfil.basico) {
			
			for(Account acc : accounts) {
				if(acc.isVisible() && (!acc.getType().equals(Constants.CREDIT_TYPE))){
					accountsArray.add(acc);
					cuentaEjeTDC = false;
				}
			}
			
		}else{
			
			for(Account acc : accounts) {
				if(acc.isVisible() && (!acc.getType().equals(Constants.CREDIT_TYPE))) {
					accountsArray.add(acc);
					cuentaEjeTDC = false;
					break;// wow
				}
			}
			
			for(Account acc : accounts) {
				if(!acc.isVisible()  && (!acc.getType().equals(Constants.CREDIT_TYPE)))
					accountsArray.add(acc);
			}
			
		}  
		
		if( ( (profile == Constants.Perfil.basico) || (accounts.length == 1) ) && (Tools.obtenerCuentaEje().getType().equals(Constants.CREDIT_TYPE))){ //cuentaEjeTDC) ){
			
			existenAlMenosUna=false;
			
		}
		

		
		return existenAlMenosUna;
		
		
		
	}
}
