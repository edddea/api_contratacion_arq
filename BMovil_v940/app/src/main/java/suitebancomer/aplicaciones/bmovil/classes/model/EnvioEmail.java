package suitebancomer.aplicaciones.bmovil.classes.model;

public class EnvioEmail {
	// #region Variables.
	/**
	 * Correo del beneficiario.
	 */
	private String correoBeneficiario;
	
	/**
	 * Correo del usuario.
	 */
	private String correoUsuario;
	
	/**
	 * Mensaje del correo.
	 */
	private String mensaje;
	
	/**
	 * ???
	 */
	private String datosOperacion;
	
	/**
	 * Tipo de email.
	 */
	private final String tipoEmail = "comprobante";
	// #endregion

	// #region Setters y Getters.
	/**
	 * @return Correo del beneficiario.
	 */
	public String getCorreoBeneficiario() {
		return correoBeneficiario;
	}

	/**
	 * @param correoBeneficiario Correo del beneficiario.
	 */
	public void setCorreoBeneficiario(String correoBeneficiario) {
		this.correoBeneficiario = correoBeneficiario;
	}

	/**
	 * @return Correo del usuario.
	 */
	public String getCorreoUsuario() {
		return correoUsuario;
	}

	/**
	 * @param correoUsuario Correo del usuario.
	 */
	public void setCorreoUsuario(String correoUsuario) {
		this.correoUsuario = correoUsuario;
	}

	/**
	 * @return Mensaje del correo.
	 */
	public String getMensaje() {
		return mensaje;
	}

	/**
	 * @param mensaje Mensaje del correo.
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	/**
	 * @return ???
	 */
	public String getDatosOperacion() {
		return datosOperacion;
	}

	/**
	 * @param datosOperacion ???
	 */
	public void setDatosOperacion(String datosOperacion) {
		this.datosOperacion = datosOperacion;
	}

	/**
	 * @return Tipo de email.
	 */
	public String getTipoEmail() {
		return tipoEmail;
	}
	// #endregion
	
	public EnvioEmail() {
		correoBeneficiario = null;
		correoUsuario = null;
		mensaje = null;
		datosOperacion = null;
	}
}
