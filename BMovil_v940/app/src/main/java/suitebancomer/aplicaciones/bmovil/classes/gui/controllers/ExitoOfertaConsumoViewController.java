package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.HashMap;

import java.util.Map;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AnalogClock;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.StartBmovilInBack;
import com.bancomer.mbanking.SuiteApp;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ExitoEFIDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ExitoOfertaConsumoDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuAdministrarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import tracking.TrackingHelper;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.ProgressBar;

public class ExitoOfertaConsumoViewController extends BaseViewController {
	private ExitoOfertaConsumoDelegate exitoConsumoDelegate;
	LinearLayout vista;
	BaseViewController me;
	private TextView lblTextoOferta;
	private TextView lblTextoAyudaOfertaPreformalizada;
	private TextView lblTextoAyudaOferta;
	private TextView lblCorreoOferta;
	private EditText editTextemail;
	private TextView lblsmsOferta;
	private EditText editTextNumero;
	private ImageButton btnmMenu;
	private TextView link1;
	private TextView link2;
	private TextView lblinf1;
	private TextView lblinf2;
	private TextView lblTextoAyuda2;
	public boolean isSMSEMail;
	//AMZ
	private BmovilViewsController parentManager;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.acepta_oferta_consumo);
		me=this;
		
		//AMZ
				parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();						
				TrackingHelper.trackState("exito consumo", parentManager.estados);
				

		SuiteApp suiteApp = (SuiteApp) getApplication();
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		exitoConsumoDelegate = (ExitoOfertaConsumoDelegate) parentViewsController.getBaseDelegateForKey(ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE);	
		exitoConsumoDelegate.setControladorDetalleILCView(this);
		setTitle(R.string.bmovil_promocion_title_consumo, R.drawable.icono_consumo);
		vista = (LinearLayout)findViewById(R.id.aceptaofertaConsumo_view_controller_layout);

		lblTextoOferta = (TextView) findViewById(R.id.lblTextoOferta);
		lblTextoAyudaOfertaPreformalizada= (TextView) findViewById(R.id.lblTextoAyudaOfertaPreformalizada);
		lblTextoAyudaOferta = (TextView) findViewById(R.id.lblTextoAyudaOferta);
		lblCorreoOferta = (TextView) findViewById(R.id.lblCorreoOferta);
		lblsmsOferta = (TextView) findViewById(R.id.lblsmsOferta);
		link1 = (TextView) findViewById(R.id.link1);
		link2 = (TextView) findViewById(R.id.link2);
		lblinf1 = (TextView) findViewById(R.id.lblinf1);
		lblinf2 = (TextView) findViewById(R.id.lblinf2);
		lblTextoAyuda2 = (TextView) findViewById(R.id.lblTextoAyuda2);

		editTextNumero=(EditText)findViewById(R.id.editTextNumero);
		editTextemail=(EditText) findViewById(R.id.editTextemail);
		btnmMenu = (ImageButton)findViewById(R.id.btnMenu);
		btnmMenu.setOnClickListener(clickListener);
		
		
		configurarPantalla();
		if(exitoConsumoDelegate.getOfertaConsumo().getEstatusOferta().equals(Constants.PREFORMALIZADO)){
			String text="¡Felicidades! \nYa tienes "+Tools.formatAmount(exitoConsumoDelegate.getOfertaConsumo().getImporte(), false)+"\nen tu cuenta.";
			String importe= Tools.formatAmount(exitoConsumoDelegate.getOfertaConsumo().getImporte(), false);
			int indexIm= text.indexOf(importe);
			SpannableString textImporte= new SpannableString(text);
			//valiadacion letra tamaño
			AbsoluteSizeSpan headerSpan= new AbsoluteSizeSpan(40);
			textImporte.setSpan(headerSpan, indexIm, indexIm+importe.length(), 0);
			textImporte.setSpan(new ForegroundColorSpan(Color.rgb(134,200,45)),indexIm,indexIm+ importe.length(),0);
			lblTextoOferta.setText(textImporte);
			lblTextoAyudaOfertaPreformalizada.setVisibility(View.GONE);
			isSMSEMail=true;
		}else if(exitoConsumoDelegate.getOfertaConsumo().getEstatusOferta().equals(Constants.PREABPROBADO)){
			if(exitoConsumoDelegate.getAceptaOfertaConsumo().getEstatusSal().equals("4")&& exitoConsumoDelegate.getAceptaOfertaConsumo().getScoreSal().equals("AP")){
				String text="¡Felicidades! \nya tienes "+Tools.formatAmount(exitoConsumoDelegate.getOfertaConsumo().getImporte(), false)+"\nen tu cuenta.";
				String importe= Tools.formatAmount(exitoConsumoDelegate.getOfertaConsumo().getImporte(), false);
				int indexIm= text.indexOf(importe);
				SpannableString textImporte= new SpannableString(text);
				AbsoluteSizeSpan headerSpan = new AbsoluteSizeSpan(40);
				textImporte.setSpan(headerSpan, indexIm, indexIm+importe.length(), 0);
				textImporte.setSpan(new ForegroundColorSpan(Color.rgb(134,200,45)),indexIm,indexIm+ importe.length(),0);
				lblTextoOferta.setText(textImporte);
				lblTextoAyudaOfertaPreformalizada.setVisibility(View.GONE);
				isSMSEMail=true;
			}
		}
		
		String txtlink1 =link1.getText().toString();
		SpannableString txtlink1s = new SpannableString(txtlink1);
		txtlink1s.setSpan(new UnderlineSpan(), 0, txtlink1.length(), 0);
		link1.setText(txtlink1s);
		link1.setOnClickListener(clickListener);
		
		String txtlink2 =link2.getText().toString();
		SpannableString txtlink2s = new SpannableString(txtlink2);
		txtlink2s.setSpan(new UnderlineSpan(), 0, txtlink2.length(), 0);
		link2.setText(txtlink2s);
		link2.setOnClickListener(clickListener);

		InputFilter[] FilterArray = new InputFilter[1];
    	FilterArray[0] = new InputFilter.LengthFilter(Constants.NUMERO_TELEFONO_LENGTH);
    	editTextNumero.setFilters(FilterArray);
    	Session session = Session.getInstance(SuiteApp.appContext);
    	if(isSMSEMail){
    	if(session.getEmail().compareTo("")==0){    		
    		lblTextoAyudaOferta.setText(R.string.bmovil_texto_extoILC3);
    		lblCorreoOferta.setVisibility(View.GONE);   		
    		editTextemail.setVisibility(View.GONE);
    		lblinf1.setVisibility(View.GONE);
    		
    	}else{
    		
    		editTextemail.setText(session.getEmail());
    		editTextemail.setEnabled(false);
    		editTextemail.setFocusable(false);
    		}		
    	}else{
    		lblCorreoOferta.setVisibility(View.GONE);   		
    		editTextemail.setVisibility(View.GONE);  
    		lblinf1.setVisibility(View.GONE); 
    	}
    	String user = session.getUsername();
		StringBuffer sb = new StringBuffer("*****");
		sb.append(user.substring(user.length() - Constants.VISIBLE_NUMBER_ACCOUNT));
		editTextNumero.setText(sb);
		editTextNumero.setEnabled(false);
		
		editTextNumero.setFocusable(false);
		
		exitoConsumoDelegate.realizaOperacion(Server.SMS_OFERTA_CONSUMO, me,isSMSEMail);
		ocultaIndicadorActividad();//Metodo que oculta el ProgressDialog

	
	}
	
	OnClickListener clickListener=new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			//AMZ
			Map<String,Object> OperacionMap = new HashMap<String, Object>();
			if(v==btnmMenu){
				//AMZ
				OperacionMap.put("evento_realizada","event52");
				OperacionMap.put("&&products","promociones;detalle consumo+clausulas consumo+exito consumo");
				OperacionMap.put("eVar12","operacion realizada oferta consumo");
				TrackingHelper.trackOperacionRealizada(OperacionMap);
				if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session
					SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
					ActivityCompat.finishAffinity(me);

					Intent i=new Intent(me, StartBmovilInBack.class);
					i.putExtra(bancomer.api.common.commons.Constants.RESULT_STRING, Constants.TRUE_STRING);
					i.putExtra( bancomer.api.common.commons.Constants.TIMEOUT_STRING, Constants.FALSE_STRING);
					i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(i);
					finish();
				}else{
					exitoConsumoDelegate.showMenu();
				}
			}else if(v==link1){
				//AMZ
				OperacionMap.put("evento_realizada","event52");
				OperacionMap.put("&&products","promociones;detalle consumo+clausulas consumo+exito consumo");
				OperacionMap.put("eVar12","operacion realizada oferta consumo");
				TrackingHelper.trackOperacionRealizada(OperacionMap);
				exitoConsumoDelegate.realizaOperacion(Server.CONTRATO_OFERTA_CONSUMO, me,false);
			}else if(v==link2){
				//AMZ
				OperacionMap.put("evento_realizada","event52");
				OperacionMap.put("&&products","promociones;detalle consumo+clausulas consumo+exito consumo");
				OperacionMap.put("eVar12","operacion realizada oferta consumo");
				TrackingHelper.trackOperacionRealizada(OperacionMap);
				exitoConsumoDelegate.realizaOperacion(Server.DOMICILIACION_OFERTA_CONSUMO, me,false);
			}
		}
	};
	
	private void configurarPantalla() {			
		GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(lblTextoOferta, true);
		gTools.scale(lblTextoAyudaOferta, true);
		gTools.scale(lblCorreoOferta, true);
		gTools.scale(lblsmsOferta, true);
		gTools.scale(editTextemail,true);
		gTools.scale(editTextNumero,true);
		gTools.scale(link1,true);
		gTools.scale(link2,true);
		gTools.scale(lblinf1,true);
		gTools.scale(lblinf2,true);
		gTools.scale(lblTextoAyuda2,true);
		gTools.scale(btnmMenu);
		gTools.scale(lblTextoAyudaOfertaPreformalizada,true);

		
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);

	}
	
	@Override
	protected void onPause() {
		super.onPause();
			parentViewsController.consumeAccionesDePausa();
		
	}
	
	public void goBack() {
		// TODO Auto-generated method stub
	}
		
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		
		exitoConsumoDelegate.analyzeResponse(operationId, response);
	}
	
	
	
}
