package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import static suitebancomer.aplicaciones.bmovil.classes.common.Tools.buildIUM;

import java.util.ArrayList;
import java.util.Hashtable;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.StartBmovilInBack;
import com.bancomer.mbanking.SuiteApp;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaILC;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;

public class DetalleOfertaILCDelegate extends BaseDelegate{
	AceptaOfertaILC aceptaOfertaILC;
	OfertaILC ofertaILC;
	Promociones promocion;
	public String fechaCat="";
	
	public static final long DETALLE_OFERTA_DELEGATE = 631453334566765659L;
	//BPromoViewsController bpromoViewsController;
	private BaseViewController controladorDetalleILCView;
//	SpannableStringController spannableStringController= new SpannableStringController();


	public DetalleOfertaILCDelegate(OfertaILC ofertaILC, Promociones promocion){
		this.ofertaILC= ofertaILC;
		this.promocion= promocion;

	}

	public OfertaILC getOfertaILC() {
		return ofertaILC;
	}
	

	public void setOfertaILC(OfertaILC ofertaILC) {
		this.ofertaILC = ofertaILC;
	}
	
	public Promociones getPromocion() {
		return promocion;
	}

	public void setPromocion(Promociones promocion) {
		this.promocion = promocion;
	}

	public void realizaOperacion(int idOperacion,
			BaseViewController baseViewController) {
		if (idOperacion == Server.ACEPTACION_OFERTA){
			Session session = Session.getInstance(SuiteApp.appContext);
			String user = session.getUsername();
			String ium= session.getIum();
			//formatoFechaCat();
			Hashtable<String, String> params = new Hashtable<String, String>();
			//params.put("operacion", "aceptacionILCBmovil");
			params.put("numeroTarjeta",getOfertaILC().getAccount().getNumber());
			params.put("lineaActual",getOfertaILC().getLineaActual());
			params.put("importe",getOfertaILC().getImporte());
			params.put("lineaFinal",getOfertaILC().getLineaFinal());
			params.put("cveCamp",promocion.getCveCamp());
			params.put("numeroCelular", user);
			params.put("IUM", ium);
			String cat= getOfertaILC().getCat();
			String catM=cat.replace(".", ",");
			params.put("Cat", catM);
			params.put("fechaCat", getOfertaILC().getFechaCat());// con o sin formato?
			//JAIG SI
			doNetworkOperation(Server.ACEPTACION_OFERTA, params,true, new AceptaOfertaILC(),isJsonValueCode.ONECLICK,
					baseViewController);
		}else if(idOperacion == Server.RECHAZO_OFERTA){
			Session session = Session.getInstance(SuiteApp.appContext);
			String user = session.getUsername();
			String ium= session.getIum();
			Hashtable<String, String> params = new Hashtable<String, String>();
			//params.put("operacion", "noAceptacionILCBMovil");
			params.put("numeroCelular", user);
			params.put("cveCamp",promocion.getCveCamp());
			params.put("descripcionMensaje","0002");
			params.put("contrato",getOfertaILC().getContrato());
			params.put("IUM", ium);
			//JAIG SI
			doNetworkOperation(Server.RECHAZO_OFERTA, params,true, null,isJsonValueCode.ONECLICK,
					baseViewController);
		}
	
	}
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params, boolean isJson, ParsingHandler handler,isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if( controladorDetalleILCView != null)
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson,handler,isJsonValueCode, caller,true);
	}
	
	public void analyzeResponse(int operationId, ServerResponse response) {
		if (operationId == Server.ACEPTACION_OFERTA) {
			if(response.getStatus() == ServerResponse.OPERATION_ERROR){
				aceptaOfertaILC = (AceptaOfertaILC) response.getResponse();
				Session session = Session.getInstance(SuiteApp.appContext);
				session.setPromocion(aceptaOfertaILC.getPromociones());
				controladorDetalleILCView.showInformationAlert("Aviso",response.getMessageCode()+"\n"+response.getMessageText(),
						new OnClickListener() {
							@Override
							public void onClick(
									DialogInterface dialog,
									int which) {
								((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showMenuPrincipal();
							}
						});
			}else{			
				aceptaOfertaILC = (AceptaOfertaILC) response.getResponse();
				Session session = Session.getInstance(SuiteApp.appContext);
				session.setPromocion(aceptaOfertaILC.getPromociones());
				((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showExitoILC(aceptaOfertaILC, ofertaILC);
			}
		}else if(operationId== Server.RECHAZO_OFERTA){
			if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session

				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
				ActivityCompat.finishAffinity(controladorDetalleILCView);

				Intent i=new Intent(controladorDetalleILCView, StartBmovilInBack.class);
				i.putExtra(bancomer.api.common.commons.Constants.RESULT_STRING, Constants.TRUE_STRING);
				i.putExtra( bancomer.api.common.commons.Constants.TIMEOUT_STRING, Constants.FALSE_STRING);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				controladorDetalleILCView.startActivity(i);
				controladorDetalleILCView.finish();
			}else {
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showMenuPrincipal();
			}
		}		
	}
	
	public AceptaOfertaILC getAceptaOfertaILC() {
		return aceptaOfertaILC;
	}
	public void setAceptaOfertaILC(AceptaOfertaILC aceptaOfertaILC) {
		this.aceptaOfertaILC = aceptaOfertaILC;
	}
			
	public BaseViewController getControladorDetalleILCView() {
		return controladorDetalleILCView;
	}
	public void setControladorDetalleILCView(
			BaseViewController controladorDetalleILCView) {
		this.controladorDetalleILCView = controladorDetalleILCView;
	}
	
	public ArrayList<Account> cargaCuentasOrigen() {
		cuentaOrigen();
		ArrayList<Account> accountsArray = new ArrayList<Account>();
		accountsArray= ofertaILC.getAccountILC();		
		return accountsArray;
	}
	
	public void cuentaOrigen(){
		Session session = Session.getInstance(SuiteApp.appContext);
		Account[] accounts=session.getAccounts();
		ArrayList<Account> accountILC;
		accountILC=new ArrayList<Account>();
		for(int i=0; i<accounts.length;i++){
			if(accounts[i].getNumber().compareTo(ofertaILC.getAccount().getNumber())==0){
				if(!accounts[i].getAlias().equals(""))
					ofertaILC.getAccount().setAlias(accounts[i].getAlias());				
					ofertaILC.getAccount().setBalance(accounts[i].getBalance());
					ofertaILC.getAccount().setType(Constants.CREDIT_TYPE);
					ofertaILC.getAccount().setCelularAsociado(accounts[i].getCelularAsociado());
					accountILC.add(ofertaILC.getAccount());
					ofertaILC.setAccountILC(accountILC);
					break;
			}
		}
		if(ofertaILC.getAccountILC()==null){
			ofertaILC.getAccount().setType(Constants.CREDIT_TYPE);
			accountILC.add(ofertaILC.getAccount());
			ofertaILC.setAccountILC(accountILC);
		}
	}
	
	public void formatoFechaCat(){
	try{	
		String[] meses = { "enero", "febrero", "marzo", "abril", "mayo", 
		      "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };		
		String fechaCatM= ofertaILC.getFechaCat();
		String fechaC=fechaCatM.replaceAll("de+","");
		String fechaCa=fechaC.replaceAll("\\s","");
		String mesSelec=fechaCa.substring(2,fechaCa.length()-4);
		String dia= fechaCatM.substring(0,2);
		String anio= fechaCatM.substring(fechaCatM.length()-4);
		int mes = -1;
	      for (int i=0; i<meses.length; i++) {
	         if ( meses[i].equalsIgnoreCase(mesSelec ) ) {
	            // El mes coincide con el elemento actual del array
	            mes = i+1;
	            break;
	         }
	      }
	     if(mes==10||mes==11||mes==12){ 
	    	 fechaCat=anio+"-"+mes+"-"+dia;
	     }else{
	    	 fechaCat=anio+"-0"+mes+"-"+dia;
	     }
			}catch(Exception ex){
				if(Server.ALLOW_LOG) Log.e("","error formato"+ex);
		}
	}
	
	public void formatoFechaCatMostrar(){
		try{	
			String[] meses = { "enero", "febrero", "marzo", "abril", "mayo", 
			      "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" };		
			String fechaCatM= ofertaILC.getFechaCat();
			int mesS=Integer.parseInt(fechaCatM.substring(5,7));
			String dia= fechaCatM.substring(fechaCatM.length()-2);
			String anio= fechaCatM.substring(0,4);
			
			fechaCat= dia+" de "+meses[mesS-1].toString()+ " de "+anio;
		    
				}catch(Exception ex){
					if(Server.ALLOW_LOG) Log.e("","error formato"+ex);
			}
		}
}
