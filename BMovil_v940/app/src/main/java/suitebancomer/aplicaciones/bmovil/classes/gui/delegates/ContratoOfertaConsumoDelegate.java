package suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;
import java.util.Hashtable;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.StartBmovilInBack;
import com.bancomer.mbanking.SuiteApp;

import bancomer.api.common.gui.delegates.SecurityComponentDelegate;
import suitebancomer.aplicaciones.bbvacredit.controllers.MainController;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.BaseDelegateOperacion;
import suitebancomer.aplicaciones.bbvacredit.gui.delegates.DetalleDeAlternativaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ContratoConsumoViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.Server.isJsonValueCode;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.AceptaOfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaPoliza;
import suitebancomer.aplicaciones.bmovil.classes.model.OfertaConsumo;
import suitebancomer.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.delegates.BaseDelegate;

public class ContratoOfertaConsumoDelegate extends DelegateBaseOperacion{
//public class ContratoOfertaConsumoDelegate extends SecurityComponentDelegate{

	OfertaConsumo ofertaConsumo;
	Promociones promocion;
	private Constants.Operacion tipoOperacion;
	Constants.TipoOtpAutenticacion tipoOTP;
	private Constants.TipoInstrumento tipoInstrumentoSeguridad;
	String token;

	String coNip;
	String coCvv;
	String coTarjeta;
	String cveAcceso;

	AceptaOfertaConsumo aceptaOferta;

	public static final long CONTRATO_OFERTA_CONSUMO_DELEGATE = 851451334266765688L;
	private BaseViewController controladorDetalleILCView;


	public ContratoOfertaConsumoDelegate(OfertaConsumo ofertaConsumo,Promociones promocion){
		this.ofertaConsumo= ofertaConsumo;
		this.promocion= promocion;		
		String instrumento = Session.getInstance(SuiteApp.appContext).getSecurityInstrument();
		if (instrumento.equals(Constants.IS_TYPE_DP270)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
		} else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
		} else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
		} else {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
		}

	}
	
	
	
	public Promociones getPromocion() {
		return promocion;
	}



	public void setPromocion(Promociones promocion) {
		this.promocion = promocion;
	}



	public OfertaConsumo getOfertaConsumo() {
		return ofertaConsumo;
	}



	public void setOfertaConsumo(OfertaConsumo ofertaConsumo) {
		this.ofertaConsumo = ofertaConsumo;
	}


	public void realizaOperacion(int idOperacion,
			BaseViewController baseViewController, int tipoSegBBVA) {
			Session session = Session.getInstance(SuiteApp.appContext);
			String ium= session.getIum();
			String numCel= session.getUsername();
		/*if (idOperacion == Server.POLIZA_OFERTA_CONSUMO){
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("operacion", "polizaConsumoBMovil");
			params.put("IdProducto", ofertaConsumo.getProducto());
			doNetworkOperation(Server.POLIZA_OFERTA_CONSUMO, params,
					baseViewController);
		}else */if(idOperacion == Server.TERMINOS_OFERTA_CONSUMO){
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("operacion", "consultaContratoConsumoBMovil");
			params.put("opcion", "1");
			params.put("IdProducto", ofertaConsumo.getProducto());
			params.put("versionE", " ");
			params.put("numeroCelular", numCel);
			params.put("IUM",ium);	
			//JAIG NO
			doNetworkOperation(Server.TERMINOS_OFERTA_CONSUMO, params,true, new ConsultaPoliza(),isJsonValueCode.CONSUMO,
					baseViewController);
		}else if(idOperacion==Server.EXITO_OFERTA_CONSUMO){
			Hashtable<String, String> params = new Hashtable<String, String>();

			String ca= Autenticacion.getInstance().getCadenaAutenticacion(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext)
							.getClientProfile());
			//params.put("operacion", "oneClickBmovilConsumo");
			/*
			parameters[11] = new NameValuePair("importePar ", importePar);
			*/
			params.put("numeroCelular", numCel);
			params.put("claveCamp",promocion.getCveCamp());
			params.put("estatus",ofertaConsumo.getEstatusOferta());
			params.put("cadenaAutenticacion",ca);
			if(tipoSegBBVA==1)
				params.put("seguroObli", "BBVA SEG");
			else if(tipoSegBBVA==2)
				params.put("seguroObli", "OTRA ASEG");
			else
				params.put("seguroObli", ""); //tiporadioburron
			params.put("IUM",ium);
/**
 *
 */
			if(coNip!=null){
				params.put("codigoNIP", coNip);
			}else{
				params.put("codigoNIP", "");
			}
			if(coCvv!=null){
				params.put("codigoCVV2", coCvv);
			}else{
				params.put("codigoCVV2", "");
			}
			if(token!=null)
				params.put("codigoOTP",token);
			else
				params.put("codigoOTP","");

			if(cveAcceso!=null){
				params.put("cveAcceso", cveAcceso);
			}else{
				params.put("cveAcceso", "");
			}
			if(coTarjeta!=null){
				params.put("tarjeta5Dig", coTarjeta);
			}else{
				params.put("tarjeta5Dig", "");
			}

			if(!session.getImporteParcial().equals("")){
				params.put("importePar", session.getImporteParcial());
				Log.i("importePart", "session.getImporteParcial(): "+session.getImporteParcial());
			} else {
				params.put("importePar","000");
				Log.i("importePart", "false");
			}

			//JAIG SI
			if(!session.getOfertaDelSimulador()) {
				doNetworkOperation(Server.EXITO_OFERTA_CONSUMO, params, true, new AceptaOfertaConsumo(), isJsonValueCode.CONSUMO,
						baseViewController);
			}else {
				/*MainController.getInstance().invokeNetworkOperation(Server.CONTRATA_ALTERNATIVA_CONSUMO, params, new BaseDelegateOperacion() {
					@Override
					protected String getCodigoOperacion() {
						return null;
					}
				}, "operacion", "conectando");*/
				DetalleDeAlternativaDelegate detalleDeAlternativaDelegate = new DetalleDeAlternativaDelegate(session.getClientNumber(), session.getIum(), session.getUsername(), session.getCveCamp());
				detalleDeAlternativaDelegate.setControladorContratoView((ContratoConsumoViewController)controladorDetalleILCView);
				detalleDeAlternativaDelegate.setOfertaConsumo(ofertaConsumo);
				controladorDetalleILCView.muestraIndicadorActividad("operacion","conectando");
				detalleDeAlternativaDelegate.contrataAlternativa(params);
			}
		}else if(idOperacion==Server.RECHAZO_OFERTA_CONSUMO) {
			Hashtable<String, String> params = new Hashtable<String, String>();
			params.put("operacion", "noAceptacionBMovil");
			params.put("numeroCelular",numCel);
			params.put("cveCamp", promocion.getCveCamp());
			params.put("descripcionMensaje", "0002");
			params.put("folioUG", ofertaConsumo.getFolioUG()); // definir de donde se obtendra
			params.put("IUM", ium);
			doNetworkOperation(Server.RECHAZO_OFERTA_CONSUMO, params,true, null,isJsonValueCode.ONECLICK,
					baseViewController);
		}
	}
	
	@Override
	public void doNetworkOperation(int operationId,	Hashtable<String, ?> params,boolean isJson, ParsingHandler handler,isJsonValueCode isJsonValueCode, BaseViewController caller) {
		if( controladorDetalleILCView != null)
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler,isJsonValueCode, caller,true);
	}
	
	public void analyzeResponse(int operationId, ServerResponse response) {
		/*if (operationId == Server.POLIZA_OFERTA_CONSUMO){
			ConsultaPoliza polizaResponse = (ConsultaPoliza)response.getResponse();
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showPoliza(polizaResponse.getTerminosHtml());
		}else*/ if (operationId == Server.TERMINOS_OFERTA_CONSUMO){
			ConsultaPoliza polizaResponse = (ConsultaPoliza)response.getResponse();
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showTerminosConsumo(polizaResponse.getTerminosHtml());
		}else if(operationId == Server.EXITO_OFERTA_CONSUMO){
			if(response.getStatus() == ServerResponse.OPERATION_ERROR){
				aceptaOferta= (AceptaOfertaConsumo)response.getResponse();
				if(aceptaOferta.getPromociones()!=null){
				Session session = Session.getInstance(SuiteApp.appContext);
				session.setPromocion(aceptaOferta.getPromociones());
				controladorDetalleILCView.showInformationAlert("Aviso",response.getMessageCode()+"\n"+response.getMessageText(),
						new OnClickListener() {
							@Override
							public void onClick(
									DialogInterface dialog,
									int which) {
								if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session

									SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
									ActivityCompat.finishAffinity(controladorDetalleILCView);

									Intent i=new Intent(controladorDetalleILCView, StartBmovilInBack.class);
									i.putExtra(bancomer.api.common.commons.Constants.RESULT_STRING, Constants.FALSE_STRING);
									i.putExtra( bancomer.api.common.commons.Constants.TIMEOUT_STRING, Constants.FALSE_STRING);
									i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									controladorDetalleILCView.startActivity(i);
									controladorDetalleILCView.finish();
								}else{
									((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showMenuPrincipal();
								}
							}
						});
				}else{
					
					controladorDetalleILCView.showInformationAlert("Aviso",response.getMessageCode()+"\n"+response.getMessageText(),
							new OnClickListener() {
								@Override
								public void onClick(
										DialogInterface dialog,
										int which) {
								}
							});
					
				}
			}else{	
			aceptaOferta= (AceptaOfertaConsumo)response.getResponse();
			Session session = Session.getInstance(SuiteApp.appContext);
			session.setPromocion(aceptaOferta.getPromociones());
			if(ofertaConsumo.getEstatusOferta().equals(Constants.PREFORMALIZADO)){
				((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showExitoConsumo(aceptaOferta, ofertaConsumo);//pantalla exito
			}else if(ofertaConsumo.getEstatusOferta().equals(Constants.PREABPROBADO)){
			OnClickListener listener = new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int arg1) {
					dialog.dismiss();
					if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session

						SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
						ActivityCompat.finishAffinity(controladorDetalleILCView);

						Intent i=new Intent(controladorDetalleILCView, StartBmovilInBack.class);
						i.putExtra(bancomer.api.common.commons.Constants.RESULT_STRING, Constants.FALSE_STRING);
						i.putExtra( bancomer.api.common.commons.Constants.TIMEOUT_STRING, Constants.FALSE_STRING);
						i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						controladorDetalleILCView.startActivity(i);
						controladorDetalleILCView.finish();
					}else {
						((BmovilViewsController) controladorDetalleILCView.getParentViewsController()).showMenuPrincipal();
					}
				}
			};			
			if(aceptaOferta.getScoreSal().equals("RE")){
				controladorDetalleILCView.showInformationAlert("Su crédito fue rechazado por buró.",listener);
				
			}else if(aceptaOferta.getScoreSal().equals("DI")){
				controladorDetalleILCView.showInformationAlert("Su crédito no fue autorizado, favor de acudir a sucursal.",listener);
				
			}else if(aceptaOferta.getScoreSal().equals("AP")||aceptaOferta.getScoreSal().equals("NB")){
				((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showExitoConsumo(aceptaOferta,ofertaConsumo);//pantalla exito
				}
			}
		}
		}else if(operationId== Server.RECHAZO_OFERTA_CONSUMO){
			if(!bancomer.api.common.commons.Constants.EMPTY_STRING.equals(SuiteApp.appOrigen)) {//bmovin no inicio la session

				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
				ActivityCompat.finishAffinity(controladorDetalleILCView);

				Intent i=new Intent(controladorDetalleILCView, StartBmovilInBack.class);
				i.putExtra(bancomer.api.common.commons.Constants.RESULT_STRING, Constants.FALSE_STRING);
				i.putExtra( bancomer.api.common.commons.Constants.TIMEOUT_STRING, Constants.FALSE_STRING);
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				controladorDetalleILCView.startActivity(i);
				controladorDetalleILCView.finish();
			}else{
				((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showMenuPrincipal();
			}
		}
	}
	
	public void showPoliza(){
		if(ofertaConsumo.getProducto().equals("PBCCMNOM01"))
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showPoliza("https://www.bancomermovil.com/imgsp_mx_web/imgsp_mx_web/web/ConsumoOneClic/Polizas/PolizaDeDesempleo/SeguroDesempleo.html");
		else if(ofertaConsumo.getProducto().equals("PBCCMNOM02"))									
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showPoliza("https://www.bancomermovil.com/imgsp_mx_web/imgsp_mx_web/web/ConsumoOneClic/Polizas/PolizaSeguroDeVida.html");
		else if(ofertaConsumo.getProducto().equals("PBCCMPPI01"))
			((BmovilViewsController)controladorDetalleILCView.getParentViewsController()).showPoliza("https://www.bancomermovil.com/imgsp_mx_web/imgsp_mx_web/web/ConsumoOneClic/Polizas/PolizaSeguroDeVida.html");
	}

	public BaseViewController getControladorDetalleILCView() {
		return controladorDetalleILCView;
	}

	public void setControladorDetalleILCView(
			BaseViewController controladorDetalleILCView) {
		this.controladorDetalleILCView = controladorDetalleILCView;
	}
	
	public Constants.Operacion getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(Constants.Operacion tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	
	@Override
	public boolean mostrarContrasenia() {
		boolean value;
			value = Autenticacion
					.getInstance()
					.mostrarContrasena(
							this.tipoOperacion,
							Session.getInstance(SuiteApp.appContext)
									.getClientProfile(),
							Tools.getDoubleAmountFromServerString(ofertaConsumo
									.getImporte()));
		
		return value;  //cambiar
	}

	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
				tipoOTP = Autenticacion.getInstance().tokenAMostrar(
						this.tipoOperacion,
						Session.getInstance(SuiteApp.appContext)
								.getClientProfile(),
						Tools.getDoubleAmountFromServerString(ofertaConsumo
								.getImporte()));
			
		} catch (Exception ex) {
			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
					"Error on Autenticacion.mostrarNIP execution.", ex);
			tipoOTP = null;
		}

		return tipoOTP;
		//return TipoOtpAutenticacion.codigo;
	}

	@Override
	public boolean mostrarNIP() {
		boolean value;
			value = Autenticacion.getInstance()
					.mostrarNIP(
							this.tipoOperacion,
							Session.getInstance(SuiteApp.appContext)
									.getClientProfile());
		
		return value; //cambiar
	}
	
	@Override
	public boolean mostrarCVV() {
		boolean value;
			value =  Autenticacion.getInstance().mostrarCVV(this.tipoOperacion,
					Session.getInstance(SuiteApp.appContext).getClientProfile(),
					Tools.getDoubleAmountFromServerString(ofertaConsumo.getImporte()));
				
	
		return value;//cambiar 
	}
       //Mostrar Campo tarjeta JMT
	@Override
	public boolean mostrarCampoTarjeta() {
		return (mostrarCVV() || mostrarNIP());
	}

	@Override
	public String getEtiquetaCampoNip() {
		return controladorDetalleILCView.getString(R.string.confirmation_nip);
	}
	
	@Override
	public String getTextoAyudaNIP() {
		return controladorDetalleILCView.getString(R.string.confirmation_ayudaNip);
	}
	
	@Override
	public String getEtiquetaCampoContrasenia() {
		return controladorDetalleILCView.getString(R.string.confirmation_contrasena);
	}
	
	@Override
	public String getEtiquetaCampoOCRA() {
		return controladorDetalleILCView.getString(R.string.confirmation_ocra);
	}
	
	@Override
	public String getEtiquetaCampoDP270() {
		return controladorDetalleILCView.getString(R.string.confirmation_dp270);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenActivado() {
		return controladorDetalleILCView.getString(R.string.confirmation_softtokenActivado);
	}
	
	@Override
	public String getEtiquetaCampoSoftokenDesactivado() {
		return controladorDetalleILCView.getString(R.string.confirmation_softtokenDesactivado);
	}
	
	@Override
	public String getEtiquetaCampoCVV() {
		return controladorDetalleILCView.getString(R.string.confirmation_CVV);
	}


	public String getTextoAyudaInstrumentoSeguridad(Constants.TipoInstrumento tipoInstrumento) {
		Constants.TipoOtpAutenticacion tokenAMostrar = tokenAMostrar();
		if(getContext()==null && controladorDetalleILCView!=null){
			setContext(controladorDetalleILCView);
		}
		return getTextoAyudaInstrumentoSeguridad(tipoInstrumento, SuiteApp.getSofttokenStatus(), tokenAMostrar);
	}

	/*
	@Override
	public String getTextoAyudaInstrumentoSeguridad(Constants.TipoInstrumento tipoInstrumento) {
		switch (tipoInstrumento) {
		case SoftToken:
			if (SuiteApp.getSofttokenStatus()) {
				return controladorDetalleILCView.getString(R.string.confirmation_ayudaRegistroSofttokenActivado);
			} else {
				return controladorDetalleILCView.getString(R.string.confirmation_ayudaRegistroSofttokenDesactivado);
			}
		case OCRA:
			return controladorDetalleILCView.getString(R.string.confirmation_ayudaRegistroOCRA);
		case DP270:
			return controladorDetalleILCView.getString(R.string.confirmation_ayudaRegistroDP270);
			//return controladorDetalleILCView.getString(R.string.confirmation_ayudaCodigoDP270);
		case sinInstrumento:
		default:
			return "";
		}
	}
*/
	public boolean enviaPeticionOperacion() {

		String contrasena = null;
		//String nip = null;
		String asm = null;
		//String cvv = null;

		if(controladorDetalleILCView instanceof ContratoConsumoViewController){
		if (mostrarContrasenia()) {
			contrasena = ((ContratoConsumoViewController)controladorDetalleILCView).pideContrasena();
			cveAcceso = contrasena;
			if (contrasena.equals("")) {
				String mensaje = controladorDetalleILCView.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += controladorDetalleILCView.getString(R.string.confirmation_componenteContrasena);
				mensaje += ".";
				controladorDetalleILCView.showInformationAlert(mensaje);
				return false;
			} else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
				String mensaje = controladorDetalleILCView.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.PASSWORD_LENGTH;
				mensaje += " ";
				mensaje += controladorDetalleILCView.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += controladorDetalleILCView.getString(R.string.confirmation_componenteContrasena);
				mensaje += ".";
				controladorDetalleILCView.showInformationAlert(mensaje);
				return false;
			}
		}
		
		String tarjeta = null;
		if(mostrarCampoTarjeta()){
			tarjeta = ((ContratoConsumoViewController)controladorDetalleILCView).pideTarjeta();
			coTarjeta = tarjeta;
			String mensaje = "";
			if(tarjeta.equals("")){
				mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
				controladorDetalleILCView.showInformationAlert(mensaje);
				return false;
			}else if(tarjeta.length() != 5){
				mensaje =  "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
				controladorDetalleILCView.showInformationAlert(mensaje);
				return false;
			}			
		}
		
		if (mostrarNIP()) {
			coNip = ((ContratoConsumoViewController)controladorDetalleILCView).pideNIP();
			if (coNip.equals("")) {
				String mensaje = controladorDetalleILCView.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += controladorDetalleILCView.getString(R.string.confirmation_componenteNip);
				mensaje += ".";
				controladorDetalleILCView.showInformationAlert(mensaje);
				return false;
			} else if (coNip.length() != Constants.NIP_LENGTH) {
				String mensaje = controladorDetalleILCView.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.NIP_LENGTH;
				mensaje += " ";
				mensaje += controladorDetalleILCView.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += controladorDetalleILCView.getString(R.string.confirmation_componenteNip);
				mensaje += ".";
				controladorDetalleILCView.showInformationAlert(mensaje);
				return false;
			}
		}
		if (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) {
			asm = ((ContratoConsumoViewController)controladorDetalleILCView).pideASM();
			if (asm.equals("")) {
				String mensaje = controladorDetalleILCView.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
				mensaje += ".";
				controladorDetalleILCView.showInformationAlert(mensaje);
				return false;
			} else if (asm.length() != Constants.ASM_LENGTH) {
				String mensaje = controladorDetalleILCView.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.ASM_LENGTH;
				mensaje += " ";
				mensaje += controladorDetalleILCView.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}	
				mensaje += ".";
				controladorDetalleILCView.showInformationAlert(mensaje);
				return false;
			}
		}
		if (mostrarCVV()) {
			coCvv = ((ContratoConsumoViewController)controladorDetalleILCView).pideCVV();
			if (coCvv.equals("")) {
				String mensaje = controladorDetalleILCView.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += controladorDetalleILCView.getString(R.string.confirmation_componenteCvv);
				mensaje += ".";
				controladorDetalleILCView.showInformationAlert(mensaje);
				return false;
			} else if (coCvv.length() != Constants.CVV_LENGTH) {
				String mensaje = controladorDetalleILCView.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.CVV_LENGTH;
				mensaje += " ";
				mensaje += controladorDetalleILCView.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += controladorDetalleILCView.getString(R.string.confirmation_componenteCvv);
				mensaje += ".";
				controladorDetalleILCView.showInformationAlert(mensaje);
				return false;
			}			
		}
		
		String newToken = null;
		if(tokenAMostrar() != TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == TipoInstrumento.SoftToken && SuiteApp.getSofttokenStatus())
			newToken = loadOtpFromSofttoken(tokenAMostrar());
		if(null != newToken)
			asm = newToken;
		
		//guardar variable en token a enviar en peticion
		token=asm;
		return true;		
		//operationDelegate.realizaOperacion(confirmacionViewController, contrasena, nip, asm, tarjeta);
		}
		return false;
	}
	
	public Constants.TipoInstrumento consultaTipoInstrumentoSeguridad() {
		return tipoInstrumentoSeguridad;
	}
	
	@Override
	public String loadOtpFromSofttoken(TipoOtpAutenticacion tipoOTP) {
		return loadOtpFromSofttoken(tipoOTP,this);
	}
	
	public void confirmarRechazoatras(){

		controladorDetalleILCView.showYesNoAlert("¿Quieres abandonar el proceso de contratación de tu crédito?",
				new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {

						if (DialogInterface.BUTTON_POSITIVE == which) {
							realizaOperacion(Server.RECHAZO_OFERTA_CONSUMO,controladorDetalleILCView,0);
						}else{
							dialog.dismiss();
						}

					}
				});

			}
	

}
