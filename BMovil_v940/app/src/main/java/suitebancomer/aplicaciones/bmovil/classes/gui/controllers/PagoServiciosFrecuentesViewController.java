package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;


import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.PagoServiciosDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.PagoServicio;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.CuentaOrigenViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import java.util.HashMap;
import java.util.Map;
import tracking.TrackingHelper;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class PagoServiciosFrecuentesViewController extends BaseViewController implements OnClickListener {

	LinearLayout vista;
	public CuentaOrigenViewController componenteCtaOrigen;
	public ListaSeleccionViewController listaSeleccion;
	ImageButton registrarNuevoServicio;
	PagoServiciosDelegate pagoServiciosDelegate;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_pago_servicios_view);
		setTitle(R.string.servicesPayment_title,R.drawable.icono_pagar_servicios);
		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID));
		pagoServiciosDelegate = (PagoServiciosDelegate)getDelegate();
		if(pagoServiciosDelegate==null){
			pagoServiciosDelegate = new PagoServiciosDelegate();
			parentViewsController.addDelegateToHashMap(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID,pagoServiciosDelegate);
			setDelegate(pagoServiciosDelegate);
		}
		pagoServiciosDelegate.setControladorPagos(this);
		findViews();
		registrarNuevoServicio.setBackgroundResource(pagoServiciosDelegate.getImagenBotonPago());
		scaleForCurrentScreen();
	
		registrarNuevoServicio.setOnClickListener(this);
		init();
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("servicios", parentManager.estados);

	}
	
	public void init(){
		cargaCuentaOrigenComponent();
		componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.pagoServicios);
		cargaListaSeleccionComponent();
		pagoServiciosDelegate.consultarFrecuentes(Constants.tipoCFPagosCIE);
	}
	
	public void cargaCuentaOrigenComponent(){
		ArrayList<Account> listaCuetasAMostrar = pagoServiciosDelegate.cargaCuentasOrigen();
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		params.topMargin = 8;
		//params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
		//params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);

		componenteCtaOrigen = new CuentaOrigenViewController(this, params,parentViewsController,this);
		componenteCtaOrigen.setDelegate(pagoServiciosDelegate);
		componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
		//componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.balanceDetail_componentTitle));
		componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
		componenteCtaOrigen.init();
		vista.addView(componenteCtaOrigen);
		
	}
	
	public void cargaListaSeleccionComponent(){
		pagoServiciosDelegate.setCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
		ArrayList<Object> lista  = pagoServiciosDelegate.getDatosTablaFrecuentes();
		
		GuiTools guiTools = GuiTools.getCurrent();
		
		LinearLayout.LayoutParams params;
		params = new LayoutParams(guiTools.getEquivalenceInPixels(280.0), LinearLayout.LayoutParams.WRAP_CONTENT);
		
		ArrayList<Object> encabezado = new ArrayList<Object>();
		encabezado.add(null);
		encabezado.add(getString(R.string.servicesPayment_frequentList_headerList));
		
		/*LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		params.topMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
		params.rightMargin =  getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
		params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);*/
		
		if (listaSeleccion == null) {
			listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
			listaSeleccion.setDelegate(pagoServiciosDelegate);
			listaSeleccion.setNumeroColumnas(1);
			listaSeleccion.setEncabezado(encabezado);
			listaSeleccion.setTitle(getString(R.string.mainMenu_favoritePayment));
			listaSeleccion.setLista(lista);
			if (lista.size() == 0) {
				listaSeleccion.setTextoAMostrar(getString(R.string.bmovil_consultar_frecuentes_emptylist));
			}else {
				listaSeleccion.setTextoAMostrar(null);
				listaSeleccion.setNumeroFilas(lista.size());		
			}
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(3);
			listaSeleccion.setExisteFiltro(true);
			listaSeleccion.setSingleLine(true);
			listaSeleccion.cargarTabla();

			vista.addView(listaSeleccion);

		}else {
			listaSeleccion.setLista(lista);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.cargarTabla();
		}
		componenteCtaOrigen.getImgDerecha().setEnabled(true);
		componenteCtaOrigen.getImgIzquierda().setEnabled(true);
		componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
	}
	
	public void consultaCamposAMostrar(){
		
	}
	
	@Override
	public void onClick(View v) {
		if (v == registrarNuevoServicio) {
			//ARR
			Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
		
			//ARR
			paso1OperacionMap.put("evento_paso1", "event46");
			paso1OperacionMap.put("&&products", "operaciones;pagar+servicio");
			paso1OperacionMap.put("eVar12", "paso1:eleccion servicio");

			TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
			((BmovilViewsController)parentViewsController).showPagoServiciosViewController(null);
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		if (pagoServiciosDelegate != null) {
			pagoServiciosDelegate.setControladorPagos(this);
			componenteCtaOrigen.setIndiceCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getListaCuentaOrigen().indexOf(pagoServiciosDelegate.getPagoServicio().getCuentaOrigen()));
	//		componenteCtaOrigen.actualizaComponente();
			componenteCtaOrigen.actualizaComponente(false);
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	public void muestraFrecuentes() {
		ArrayList<Object> listaDatos = null;
		listaDatos =  pagoServiciosDelegate.getDatosTablaFrecuentes();
		
		if (listaDatos.size() > 0) {
			listaSeleccion.setTextoAMostrar(null);
			listaSeleccion.setLista(listaDatos);
			listaSeleccion.setNumeroColumnas(((ArrayList<Object>) listaDatos.get(0)).size() - 1) ;
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(listaDatos.size());
			listaSeleccion.getFiltroLista().setListaOriginal(listaDatos);
			listaSeleccion.setSingleLine(true);
			listaSeleccion.cargarTabla();
		}else {
			listaSeleccion.setLista(listaDatos);
			if (listaDatos.size() == 0) {
				listaSeleccion.setLista(listaDatos);
				listaSeleccion.setTextoAMostrar(getString(R.string.bmovil_consultar_frecuentes_emptylist));
			}
			listaSeleccion.cargarTabla();
		}
	}
	
	public void frecuenteSeleccionado(PagoServicio pagoServicio){

		if (pagoServiciosDelegate.isOpenHours()) {
			pagoServiciosDelegate.setCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
			pagoServicio.setCuentaOrigen(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
			//ARR
			Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
			
			//ARR
			paso1OperacionMap.put("evento_paso1", "event46");
			paso1OperacionMap.put("&&products", "operaciones;pagar+servicio");
			paso1OperacionMap.put("eVar12", "paso1:eleccion servicio");

			TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
			((BmovilViewsController)parentViewsController).showPagoServiciosViewController(pagoServicio);
		} else {
		    this.showInformationAlert(pagoServiciosDelegate.textoMensajeAlerta());
		}
	}
	
	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		pagoServiciosDelegate.analyzeResponse(operationId, response);
	}
	
	private void findViews() {
		vista = (LinearLayout)findViewById(R.id.pagoServicios_view_controller_layout);
		registrarNuevoServicio = (ImageButton) findViewById(R.id.pagoServicios_view_controller_btn_servicio_nuevo);
	}
	
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(findViewById(R.id.pagoServicios_rootLayout));
		guiTools.scale(registrarNuevoServicio);
	}

}
