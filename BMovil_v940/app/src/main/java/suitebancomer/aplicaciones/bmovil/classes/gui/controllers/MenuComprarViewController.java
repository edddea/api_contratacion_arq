package suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;



import suitebancomer.aplicaciones.bmovil.classes.common.Autenticacion;
import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.MenuComprarDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.views.ListaSeleccionViewController;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

import tracking.TrackingHelper;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

public class MenuComprarViewController extends BaseViewController {

	LinearLayout vista;
	ListaSeleccionViewController listaSeleccion;
	TextView titulo;
	ArrayList<Object> lista;
	MenuComprarDelegate delegate;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, R.layout.layout_bmovil_opciones_transfer);
		setTitle(R.string.menu_comprar_title,R.drawable.bmovil_comprar_icono);
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("comprar", parentManager.estados);
		
		SuiteApp suiteApp = (SuiteApp)getApplication();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(MenuComprarDelegate.MENU_COMPRAR_DELEGATE_ID));
		delegate = (MenuComprarDelegate)getDelegate();
		if (delegate == null) {
			delegate = new MenuComprarDelegate();
			parentViewsController.addDelegateToHashMap(MenuComprarDelegate.MENU_COMPRAR_DELEGATE_ID, delegate);
			setDelegate(delegate);
		}
		delegate.setControladorMenuComprar(this);
		vista = (LinearLayout)findViewById(R.id.opciones_transfer_layout);
		cargaListaSeleccionComponent();
	}
	
	@SuppressWarnings("deprecation")
	public void cargaListaSeleccionComponent(){

		lista = delegate.cargarListasMenu();
			
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.topMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_border_margin);
//		params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
//		params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);

		listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
		listaSeleccion.setDelegate(delegate);
		listaSeleccion.setLista(lista);
		listaSeleccion.setNumeroColumnas(1);
		listaSeleccion.setTitle(getString(R.string.opcionesTransfer_menu_titulo));
		listaSeleccion.setOpcionSeleccionada(-1);
		listaSeleccion.setSeleccionable(false);
		listaSeleccion.setAlturaFija(false);

		listaSeleccion.cargarTabla();
		vista.addView(listaSeleccion);
	}

	public void opcionSeleccionada(String opcion){
		//ARR
				Map<String,Object> inicioOperacionMap = new HashMap<String, Object>();
		
		Session session = Session.getInstance(SuiteApp.getInstance());
		
		if (opcion.equals(Constants.OPERACION_COMPRAR_TIEMPO_AIRE)) {

			//hcf
			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.compraTiempoAire,	session.getClientProfile())) {
				if (Session.getInstance(SuiteApp.appContext).puedeOperarConTDCEje()) {

					//ARR
					inicioOperacionMap.put("evento_inicio", "event45");
					inicioOperacionMap.put("&&products", "operaciones;comprar+tiempo aire");
					inicioOperacionMap.put("eVar12", "inicio");

					TrackingHelper.trackInicioOperacion(inicioOperacionMap);
					if (Server.ALLOW_LOG)
						Log.d("MenuComprarViewController", "Entraste a compra de tiempo aire cuenta express");
					if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.compraTiempoAire, session.getClientProfile())) {
						((BmovilViewsController) parentViewsController).showTiempoAireViewController(Constants.Operacion.compraTiempoAire);
					} else {
						// No esta permitido

					}
				} else {
					showInformationAlert(R.string.opcionesTransfer_alerta_texto_tdc);
				}
			} else {

			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(MenuComprarDelegate.MENU_COMPRAR_DELEGATE_ID);
		super.goBack();
	}
	
}
