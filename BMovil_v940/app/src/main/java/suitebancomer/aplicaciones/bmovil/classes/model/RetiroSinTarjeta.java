package suitebancomer.aplicaciones.bmovil.classes.model;

import bancomer.api.common.model.Compania;

public class RetiroSinTarjeta {
	
	
	Account cuentaOrigen;
	String celularBeneficiario;
	Compania compania;
	String beneficiario;
	String importe;
	String concepto;

	
	
	
	
	
	
	public RetiroSinTarjeta() {
		super();
		this.cuentaOrigen = null;
		this.celularBeneficiario = "";
		this.compania =  null;
		this.beneficiario = "";
		
		this.importe = "";
		this.concepto = "";
	}
	
	
	
	public RetiroSinTarjeta(Account cuentaOrigen, String celularBeneficiario,
			Compania compania, String beneficiario, String importe, String concepto) {
		super();
		this.cuentaOrigen = cuentaOrigen;
		this.celularBeneficiario = celularBeneficiario;
		this.compania = compania;
		this.beneficiario = beneficiario;
		
		this.importe = beneficiario;
		this.concepto = beneficiario;

	}
	
	
	
	
	public Account getCuentaOrigen() {
		return cuentaOrigen;
	}
	public void setCuentaOrigen(Account cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}
	public String getCelularBeneficiario() {
		return celularBeneficiario;
	}
	public void setCelularBeneficiario(String celularBeneficiario) {
		this.celularBeneficiario = celularBeneficiario;
	}
	public Compania getCompania() {
		return compania;
	}
	public void setCompania(Compania compania) {
		this.compania = compania;
	}
	public String getBeneficiario() {
		return beneficiario;
	}
	public void setBeneficiario(String beneficiario) {
		this.beneficiario = beneficiario;
	}



	public String getImporte() {
		return importe;
	}



	public void setImporte(String importe) {
		this.importe = importe;
	}



	public String getConcepto() {
		return concepto;
	}



	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}
	
	
	
	

}
