package suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;

@SuppressWarnings("serial")
public class PagoServicioResult implements ParsingHandler {

	public final static int RESULT_PREREGISTER = 4378;
	public final static int RESULT_PAYMENT = 4379;
	
	int resultType;
	String guiaCIE;
	String folio;
	String asunto;
	String nuevoSaldo;
	String fecha;
	String hora;
	String codTrans;

	public String getCodTrans() {
		return codTrans;
	}

	public void setCodTrans(String codTrans) {
		this.codTrans = codTrans;
	}

	public PagoServicioResult() {

	}

	public PagoServicioResult(int resultType) {
		switch (resultType) {
			case RESULT_PREREGISTER:
				this.resultType = RESULT_PREREGISTER;
				break;
			case RESULT_PAYMENT:
			default:
				this.resultType = RESULT_PAYMENT;
				break;
		}
	}
	
	public int getResultType() {
		return resultType;
	}
	
	public String getFolio() {
		return folio;
	}
	
	public String getAsunto() {
		return asunto;
	}
	
	public String getNuevoSaldo() {
		return nuevoSaldo;
	}
	
	/**
	 * @return la guia del pago
	 */
	public String getGuiaCIE() {
		return guiaCIE;
	}

	/**
	 * @return the fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * @return the hora
	 */
	public String getHora() {
		return hora;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		if (resultType == RESULT_PREREGISTER) {
			this.folio = parser.parseNextValue("FO");
		} else if (resultType == RESULT_PAYMENT) {
			this.guiaCIE = parser.parseNextValue("GC");
			this.folio = parser.parseNextValue("FO");
			this.asunto = parser.parseNextValue("AS");
			this.nuevoSaldo = parser.parseNextValue("IM");
			this.fecha = parser.parseNextValue("FE");
			this.hora = parser.parseNextValue("HR");
			this.codTrans=parser.parseNextValue("TR");
		}
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

}
