package suitebancomer.classes.gui.delegates;

import java.util.Hashtable;

import bancomer.api.common.gui.delegates.SecurityComponentDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.classes.gui.controllers.BaseViewController;

public class BaseDelegate extends SecurityComponentDelegate {

	
	/**
	 * Invoke a network operation, controlling that it is called after all previous
	 * repaint events have been completed
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 */
	public void doNetworkOperation(final int operationId, final Hashtable<String,?> params, final boolean isJson, final ParsingHandler hadler,
                                   final Server.isJsonValueCode isJsonValue, final BaseViewController caller) {}
	
	/**
     * 
     * This function has to be overridden by child members, it implements
     * the processing for server response.
     * 
     * @param operationId
     * @param response
     */
    public void analyzeResponse(int operationId, ServerResponse response) {}
	
    public void performAction(Object obj) {}
    
    public long getDelegateIdentifier() { return 0; }
}
