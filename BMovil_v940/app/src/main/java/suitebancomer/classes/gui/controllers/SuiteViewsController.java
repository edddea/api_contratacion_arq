package suitebancomer.classes.gui.controllers;

import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.NovedadesViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.classes.gui.delegates.MenuSuiteDelegate;
import tracking.TrackingHelper;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.PackageManager;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.HashMap;
import java.util.Map;

public class SuiteViewsController extends BaseViewsController {

	public void showMenuSuite(boolean inverted) {
		showMenuSuite(inverted, null);
	}
	
	public void showMenuSuite(boolean inverted, String[] extras) {
		MenuSuiteDelegate suiteDelegate = (MenuSuiteDelegate)getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
		if (suiteDelegate == null) {
			suiteDelegate = new MenuSuiteDelegate();
			addDelegateToHashMap(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID, suiteDelegate);
		}
		Session session = Session.getInstance(SuiteApp.appContext);
		boolean activado = session.isApplicationActivated();
		suiteDelegate.setbMovilSelected(extras != null && extras[0].equalsIgnoreCase("bmovilselected"));
		
		/*if (android.os.Build.VERSION.SDK_INT >= 11) {
			showViewController(MenuSuiteViewController.class, Intent.FLAG_ACTIVITY_CLEAR_TASK |
														Intent.FLAG_ACTIVITY_NEW_TASK, inverted);
		} else {
			showViewController(MenuSuiteViewController.class, IntentCompat.FLAG_ACTIVITY_CLEAR_TASK |
																Intent.FLAG_ACTIVITY_NEW_TASK, inverted);
		}*/
		showViewController(MenuSuiteViewController.class, Intent.FLAG_ACTIVITY_CLEAR_TOP, inverted);
	}
	

	
	public void showContratacionPendiente() {
		
		//TODO
	}
	
	public void showNovedadesContactanos() {
		MenuSuiteDelegate delegate = (MenuSuiteDelegate) getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuSuiteDelegate();
			addDelegateToHashMap(
					MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID,
					delegate);
		}
		showViewController(NovedadesViewController.class);
	}
	
}
