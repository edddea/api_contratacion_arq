package suitebancomer.classes.gui.delegates;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;
import com.bancomer.mbanking.desactivada.SuiteAppDesac;
import com.bancomer.mbanking.login.InitLogin;
import com.bancomer.mbanking.login.SuiteAppLogin;
import com.bancomer.mbanking.softtoken.SofttokenApp;
import com.bancomer.mbanking.softtoken.SuiteAppApi;
import com.google.zxing.client.android.CaptureActivity;

import org.json.JSONObject;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.timer.TimerController;
import suitebancomer.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomer.aplicaciones.bmovil.classes.common.KeyManagerStoreException;
import suitebancomer.aplicaciones.bmovil.classes.common.KeyStoreManager;
import suitebancomer.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.SessionStoredListener;
import suitebancomer.aplicaciones.bmovil.classes.common.SincronizarSesion;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.entrada.InitContratacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioPerfilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.desactivada.ConsultaEstatusAplicacionDesactivadaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Parser;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Catalog;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaEstatusMantenimientoData;
import suitebancomer.aplicaciones.bmovil.classes.model.EstatusDelServicioData;
import suitebancomer.aplicaciones.bmovil.classes.model.LoginData;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.ActivacionSTViewController;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import suitebancomer.classes.common.PropertiesManager;
import suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.classes.gui.controllers.MenuSuiteViewController;
import suitebancomer.classes.gui.controllers.SuiteViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import tracking.TrackingHelper;


public class MenuSuiteDelegate extends BaseDelegate implements SessionStoredListener {

	private final static String STORE_SESSION_LOGIN = "sessionLogin";
	private final static String STORE_SESSION_ACTIVATION = "sessionActivation";
	private final static String STORE_SESSION_PROFILE_CHANGE = "sessionProfileChange";

	private String storeSessionType;
	private String currentPassword;
	
	public final static long MENU_SUITE_DELEGATE_ID = 0x3329474451002cd6L; 
	
	private BaseViewController menuSuiteViewController;
	
	private boolean isCallActive;
	private boolean bMovilSelected;
	private ServerResponse loginResponse;
	private LoginData loginData;
	private ConsultaEstatus consultaEstatus;
	private ConsultaEstatusMantenimientoData serverResponse;
	
	//Variable para almacenar el nombre de usuario
	private static String nombreUsuario;
			
	//ARR
	//Variable para usar en la identificacion de usuario en el Login. Para saltarsela o no si la aplicacion se inicia desde cero o no
	public static boolean encriptacionEnLogin = false;
	
	public void setbMovilSelected(boolean bMovilSelected) {
		this.bMovilSelected = bMovilSelected;
	}
	
	public boolean isbMovilSelected() {
		return bMovilSelected;
	}
	
	public MenuSuiteDelegate() {
	}
	
	public boolean isCallActive() {
		return isCallActive;
	}
	
	public void setCallActive(boolean isCallActive) {
		this.isCallActive = isCallActive;
	}
	
	public boolean isDisconnected(){
		SuiteApp suiteApp = SuiteApp.getInstance();
		
		 ConnectivityManager connectivity = (ConnectivityManager) 
				 suiteApp.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		 
         if (connectivity != null){
             NetworkInfo[] info = connectivity.getAllNetworkInfo();
             if (info != null){
                 for (int i = 0; i < info.length; i++){
                     if (info[i].getState() == NetworkInfo.State.CONNECTED){
                         return false;
                     }
                 }
             }
         }
         return true;
	}
	
	public void startBmovilApp() {
		if (SuiteApp.getInstance().getBmovilApplication() == null) {
			SuiteApp.getInstance().startBmovilApp();
		}
	}
	
	public int getBmovilAppStatus(SuiteApp suiteApp) {
		return suiteApp.getBmovilApplication().getApplicationStatus();
	}
	
	public BaseViewController getMenuSuiteViewController() {
		return menuSuiteViewController;
	}

	public void setMenuSuiteViewController(BaseViewController menuSuiteViewController) {
		this.menuSuiteViewController = menuSuiteViewController;
	}

	public void bmovilSelected() {
		
		//EA#10, RN8 y RN9: comprobar la conectividad a internet antes de acceder a la aplicación
		if (this.isDisconnected()) {
			((MenuSuiteViewController)menuSuiteViewController).setButtonsDisabled(false);
			menuSuiteViewController.showInformationAlert(R.string.menuSuite_alert_disconnected);

		} else {
			// EA#2

			borrarDatosBMovilSiNoExisteNumCelular();
			// Contin�a el flujo normal del escenario principal
			if(buscarBanderasBmovil()){
				if(existePendienteDescarga()){
					showActivacionSTEA12();
				} else {
					showActivacionSTEP();
				}			
			} else {
				if(PropertiesManager.getCurrent().getBmovilActivated()) {
					((MenuSuiteViewController)getMenuSuiteViewController()).configurarVistaLogin();
					showLogin();
				} else {

					consultaEstatusBmovil();
				}
			}
			
		}
	}
	
	/**
	 * Pantalla inicial BMovil: EA#2.
	 * Si la aplicacion esta activada y no hay un numero de celular almacenado, borra los
	 * datos del archivo datosBMovil y desactiva el flag bMovilActivated.
	 */
	private void borrarDatosBMovilSiNoExisteNumCelular() {
		if(PropertiesManager.getCurrent().getBmovilActivated()) {
			if (Session.getInstance(menuSuiteViewController).getUsername() == null) {
				DatosBmovilFileManager.getCurrent().borrarDatos();
				PropertiesManager.getCurrent().setBmovilActivated(false);
				Session.getInstance(menuSuiteViewController).loadRecordStore();
			}
		}
	}
	
	/**
	 * Devuelve si existe el archivo PendienteDescarga
	 * @return
	 */
	//private boolean existePendienteDescarga(){ paperless
	public boolean existePendienteDescarga(){
		boolean respuesta = false;
		SofttokenActivationBackupManager manager = SofttokenActivationBackupManager
				.getCurrent();
		if (manager.existsABackup()) {
			respuesta = true;
		}
		return respuesta;
	}
	
	//private void showActivacionSTEA12() { paperless
	public void showActivacionSTEA12() {


	//cambiarDeBMovilTokenMovil().showActivacionSTEA12();
	/*Activacion de softoken cuando existe archivo de pendiente descarga*/

		//__________- invocacion al api de contratacion ___________//
		suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
				Server.SIMULATION,Server.ALLOW_LOG, Server.EMULATOR,Server.DEVELOPMENT
		);
		//inicializa la clase
		InitContratacion initi=new InitContratacion(SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController(),new MenuSuiteViewController(),new MenuSuiteViewController(),bs);

		//__________- invocacion al api de softoken ___________//
		SuiteAppApi sftoken=new SuiteAppApi();

		sftoken.onCreate(SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController());

		ContratacionSTDelegate delegate = (ContratacionSTDelegate) sftoken.getSuiteViewsControllerApi().getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
		if (null == delegate) {
			delegate = new ContratacionSTDelegate();
			sftoken.getSuiteViewsControllerApi().addDelegateToHashMap(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID, delegate);
			sftoken.getSuiteViewsControllerApi().addDelegateToHashMap(suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID, delegate.generaTokendelegate);
		}
		delegate.setOwnerController(new ActivacionSTViewController());
		delegate.setEA12(true);
		delegate.setCargarDatos(true);

		sftoken.setIntentToReturn(new MenuSuiteViewController());
		sftoken.initWithSession(SuiteApp.getInstance().getBmovilApplication().isApplicationLogged(), SuiteApp.getInstance().getMenuPrincipalViewController(), Session.getInstance(SuiteApp.appContext).getClientNumber(),Session.getInstance(SuiteApp.appContext).getUsername(),Session.getInstance(SuiteApp.appContext).getIum());
		sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
		sftoken.showViewController(ActivacionSTViewController.class, 0, false, null, null,SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController() );

	}

	/**
	 * Redirige al caso de uso ActivacionST escenario principal
	 */
	//private void showActivacionSTEP() { paperless
	public void showActivacionSTEP() {

		//cambiarDeBMovilTokenMovil().showPantallaIngresoDatos();
		//showViewController(IngresoDatosSTViewController.class);
//__________- invocacion al api de contratacion ___________//
		suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
				Server.SIMULATION,Server.ALLOW_LOG, Server.EMULATOR,Server.DEVELOPMENT
		);
		//inicializa la clase
		InitContratacion initi=new InitContratacion(SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController(),new MenuSuiteViewController(),new MenuSuiteViewController(),bs);


		SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/
		ServerCommons.ALLOW_LOG=Server.ALLOW_LOG;
		ServerCommons.DEVELOPMENT=Server.DEVELOPMENT;
		ServerCommons.SIMULATION=Server.SIMULATION;
		sftoken.onCreate(SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController());
		sftoken.setIntentToReturn(new MenuSuiteViewController());


		sftoken.initWithSession(SuiteApp.getInstance().getBmovilApplication().isApplicationLogged(), SuiteApp.getInstance().getMenuPrincipalViewController(), Session.getInstance(SuiteApp.appContext).getClientNumber(),Session.getInstance(SuiteApp.appContext).getUsername(),Session.getInstance(SuiteApp.appContext).getIum());
		sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());

		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(true);
		sftoken.showViewController(suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.IngresoDatosSTViewController.class, 0, false, null, null, SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController());

	}

	/*private void showActivacionST(){
		cambiarDeBMovilTokenMovil().showActivacionSTEP25();
	}*/

	/*public SofttokenViewsController cambiarDeBMovilTokenMovil() {
		SofttokenViewsController viewsController = SuiteApp.getInstance()
				.getSofttokenApplication().getSottokenViewsController();
		menuSuiteViewController.setParentViewsController(viewsController);
		viewsController.setCurrentActivityApp(menuSuiteViewController);

		return viewsController;
	}
	*/
	private boolean buscarBanderasBmovil(){
		Session session = Session.getInstance(SuiteApp.appContext);

		if(session.getUsername()==null || Constants.EMPTY_STRING.equals(session.getUsername())){
			PropertiesManager.getCurrent().setBmovilActivated(false);
		}
		boolean banderaBmovil = Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CONTRATAR2x1));
		return banderaBmovil;
	}

	private void consultaEstatusBmovil() {
//		BmovilApp bmovilApp = SuiteApp.getInstance().getBmovilApplication();
//		BmovilViewsController bmovilViewsController = bmovilApp.getBmovilViewsController();
//		bmovilViewsController.setCurrentActivityApp(menuSuiteViewController);
//		bmovilViewsController.showConsultaEstatusAplicacionDesactivada();

		// Consulta estatus aplicacion desactivada EA#1
		String numberPhone=	buscarNumero();
		if(numberPhone != null) {
			((MenuSuiteViewController)getMenuSuiteViewController()).configurarVistaAplicacionDesactivada(numberPhone);
		}
	}

	public void leerContratacion() {
		int pendingStatus = Session.getInstance(SuiteApp.appContext).getPendingStatus();

		if (pendingStatus > 0) {
		} else {
			showLogin();
		}
	}

	private void showLogin() {
		((MenuSuiteViewController)menuSuiteViewController).establecerUsuario();
	}

	public void llamarLineaBancomer() {

		try {
	        Intent callIntent = new Intent(Intent.ACTION_CALL);
	        callIntent.setData(Uri.parse(Constants.TEL_URI+menuSuiteViewController.getString(R.string.menuSuite_callLocalNumber)));
            menuSuiteViewController.startActivity(callIntent);


	    } catch (ActivityNotFoundException e) {
	    	//menuSuiteViewController.showErrorMessage(menuSuiteViewController.getString(R.string.menuSuite_callErrorMessage));
			return;
	    }
	}

	// #region Softtoken.
	public void softtokenSelected() {
		if(null == SuiteApp.getInstance().getSofttokenApplication())
			SuiteApp.getInstance().startSofttokenApp();

		SofttokenViewsController viewsController = SuiteApp.getInstance().getSofttokenApplication().getSottokenViewsController();
		if(SuiteApp.getSofttokenStatus()) {
			((MenuSuiteViewController)menuSuiteViewController).restableceMenu();
			//viewsController.setCurrentActivityApp(menuSuiteViewController);
			//viewsController.showPantallaGeneraOTPST(false);

			/* invocar el api de softoken  para generar otps*/
			SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/
			ServerCommons.ALLOW_LOG= Server.ALLOW_LOG;
			ServerCommons.DEVELOPMENT=Server.DEVELOPMENT;
			ServerCommons.SIMULATION=Server.SIMULATION;
			sftoken.onCreate(SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController());
			sftoken.setIntentToReturn(new MenuSuiteViewController());



			suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate delegate2 = (suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate) sftoken.getSuiteViewsControllerApi().getBaseDelegateForKey(suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
			if (null == delegate2) {
				delegate2 = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate();
				sftoken.getSuiteViewsControllerApi().addDelegateToHashMap(
						suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,
						delegate2);
				sftoken.getSuiteViewsControllerApi().addDelegateToHashMap(suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID,
						delegate2.generaTokendelegate);
			}
			SuiteAppApi.setActivityQrToken(new CaptureActivity());
			sftoken.showViewController(suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.GeneraOTPSTViewController.class, Intent.FLAG_ACTIVITY_CLEAR_TOP, false, null, null, SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController());


		} else {
			//viewsController.showContratacionSotfttoken(((MenuSuiteViewController)menuSuiteViewController));
		}
	}

	public void leerContratacionST() {

	}
	// #endregion

	public void onBackPressed() {
//		if(menuSuiteViewController.getLoginViewController() != null &&
//		   menuSuiteViewController.getLoginViewController().getVisibility() == View.VISIBLE) {
//			menuSuiteViewController.plegarOpcion();
//		} else if(menuSuiteViewController.getAplicacionDesactivadaViewController() != null &&
//				  menuSuiteViewController.getAplicacionDesactivadaViewController().getVisibility() == View.VISIBLE) {
//			menuSuiteViewController.plegarOpcionAplicacionDesactivada();
//		} else if(menuSuiteViewController.getContratacionSTViewController() != null &&
//				  menuSuiteViewController.getContratacionSTViewController().getVisibility() == View.VISIBLE) {
//			menuSuiteViewController.plegarOpcionST();
//		} else {
			SuiteApp.getInstance().cierraAplicacionSuite();
//		}
	}


	/**
	 * Se selecciona la opci�n de men� Token M�vil.
	 * @param sender la opci�n seleccionada
	 */
	public void onBtnContinuarclick(View sender) {
		SofttokenApp softtokenApp = SuiteApp.getInstance().getSofttokenApplication();

		SofttokenViewsController viewsController = softtokenApp.getSottokenViewsController();
		if (SuiteApp.getSofttokenStatus()) {
			((MenuSuiteViewController)menuSuiteViewController).restableceMenu();
			//viewsController.setCurrentActivityApp(menuSuiteViewController);


				/* invocar el api de softoken  para generar otps*/
			SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/
			ServerCommons.ALLOW_LOG= Server.ALLOW_LOG;
			ServerCommons.DEVELOPMENT=Server.DEVELOPMENT;
			ServerCommons.SIMULATION=Server.SIMULATION;
			sftoken.onCreate(SuiteApp.getInstance().getSuiteViewsController().getCurrentViewController());
			//sftoken.getSofttokenApplicationApi();
			sftoken.setIntentToReturn(new MenuSuiteViewController());


			suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate delegate2 = (suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate) sftoken.getSuiteViewsControllerApi().getBaseDelegateForKey(suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
			if (null == delegate2) {
				delegate2 = new suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate();
				sftoken.getSuiteViewsControllerApi().addDelegateToHashMap(
						suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID,
						delegate2);
				sftoken.getSuiteViewsControllerApi().addDelegateToHashMap(suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate.GENERAR_OTP_DELEGATE_ID,
						delegate2.generaTokendelegate);
			}
			SuiteAppApi.setActivityQrToken(new CaptureActivity());
			sftoken.showViewController(suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.GeneraOTPSTViewController.class, Intent.FLAG_ACTIVITY_CLEAR_TOP, false, null, null, menuSuiteViewController);


		} else {
//			viewsController.setCurrentActivityApp(menuSuiteViewController);
//
//			SofttokenActivationBackupManager manager = SofttokenActivationBackupManager
//					.getCurrent();
//
//			if (manager.existsABackup()) {
//				softtokenApp.getSottokenViewsController()
//						.showPantallaCActivacionST(true);
//			} else {
//				softtokenApp.getSottokenViewsController()
//						.showPantallaIngresoDatos(menuSuiteViewController);
//			}

			//EA#10, RN8 y RN9: comprobar la conectividad a internet antes de acceder a la aplicación
			if (this.isDisconnected()) {
				((MenuSuiteViewController)menuSuiteViewController).setButtonsDisabled(false);
				menuSuiteViewController.showInformationAlert(R.string.menuSuite_alert_disconnected);

			} else {
				// Contin�a el flujo normal del EA#2: paso 6
				if(existePendienteDescarga()) {
					showActivacionSTEA12();
				} else {

					showActivacionSTEP();
				}
			}
		}
	}
	
	/**
	 * Metodo para cargar datos implementacion P026 BConnect EA#9, EA#10, EA#11, EA#12,EA#13
	 */
	public void cargaTelSeedKeystore() {

		// Recoger sesion del contexto
		Session session = Session.getInstance(SuiteApp.appContext);
		// Recoger KeyStoreManager
		//KeyStoreManager keySMan = session.getKeyStoreManager();

		// Inicializa Telefono y seed
		String telefono = null;
		String seed = null;
		//KeyStoreManager keySMan = null;
		KeyStoreWrapper kswrapper;


		try {
			// Recoger KeyStoreManager
			//keySMan = session.getKeyStoreManager();
 			kswrapper = session.getKeyStoreWrapper();

			// Telefono e IUM de KeyStore
			//telefono = keySMan.getEntry(Constants.USERNAME);
			//seed = keySMan.getEntry(Constants.SEED);

			// Telefono e IUM de KeyStore
			telefono = kswrapper.getUserName();
			seed = kswrapper.getSeed();

			if(Server.ALLOW_LOG) Log.i("Key", "BConnect MenuSuiteDelegate Username: " + telefono);
			if(Server.ALLOW_LOG) Log.i("Key", "BConnect MenuSuiteDelegate Seed: " + seed);

		/*} catch (KeyManagerStoreException e1) {
			// TODO Auto-generated catch block
			if(Server.ALLOW_LOG) e1.printStackTrace();
		}*/
		
		// Comprobar Bmovil en estatusAplicaciones
		if (PropertiesManager.getCurrent().getBmovilActivated()) {

			// Si bmovil activado, validar telefono e IUM en KeyStore
			if (!Tools.validaSeed(seed) || !Tools.validaTelefono(telefono)) {

				
				// Si datos no validos, comprueba en archivo datosBmovil
				DatosBmovilFileManager datosBmovil = DatosBmovilFileManager
						.getCurrent();
				if(Server.ALLOW_LOG) Log.i("BConnect", "datosBmovil " + datosBmovil);
				seed = datosBmovil.getSeedStr();
				telefono = datosBmovil.getLogin();
				if(Server.ALLOW_LOG) Log.i("DatosBmovil", "seed " + seed);
				if(Server.ALLOW_LOG) Log.i("DatosBmovil", "telefono " + telefono);
				// Validar telefono y seed
				if (Tools.validaSeed(seed) && Tools.validaTelefono(telefono)) {
					// EA#10
					//try {
						// Copia telefono y seed a keyStore

						if(Server.ALLOW_LOG) Log.i("Key",
								"BConnect MenuSuiteDelegate SET Username: "
										+ telefono);
						if(Server.ALLOW_LOG) Log.i("Key", "BConnect MenuSuiteDelegate SET Seed: "
								+ seed);
						kswrapper.setUserName(telefono);
						kswrapper.setSeed(seed);
						kswrapper.storeValueForKey(Constants.CENTRO, Constants.BMOVIL);
						/*
							keySMan.setEntry(Constants.USERNAME, telefono);
							keySMan.setEntry(Constants.SEED, seed);
							keySMan.setEntry(Constants.CENTRO, Constants.BMOVIL);

					} catch (IOException e) {
						// TODO Auto-generated catch block
						if (Server.ALLOW_LOG) Log.e("Error KeyStore", "MenuSuiteDelgate: Error al almacenar datos en KeyStore");
					}*/

					// No borrar ambos datos de datosBmovil EA310 Paso 5
					//datosBmovil.setSeed("");
					//datosBmovil.setLogin("");

				} else {
					// Si no se valida, cambia a bmovil = false EA#11
					PropertiesManager.getCurrent().setBmovilActivated(false);
				}
			}
		} else {
			// Si Bmovil desactivado o no existe estatusAplicaciones
			// Validar telefono y seed EA#12
			if (Tools.validaSeed(seed) && Tools.validaTelefono(telefono)) {
				// Si los datos son validos borra datos en keychain
				//try {
					kswrapper.setUserName(" ");
					kswrapper.setSeed(" ");
					kswrapper.storeValueForKey(Constants.CENTRO, " ");
					/*keySMan.setEntry(Constants.USERNAME, " ");
					keySMan.setEntry(Constants.SEED, " ");
					keySMan.setEntry(Constants.CENTRO, " ");*/
					if (Server.ALLOW_LOG) {
						Log.i("Key", "seed " + kswrapper.getSeed() );
						Log.i("Key", "telefono " +kswrapper.getUserName()  );
						Log.i("Key", "Centro " +  kswrapper.fetchValueForKey(Constants.CENTRO) );
					}
				/*} catch (IOException e) {
					// TODO Auto-generated catch block
					if (Server.ALLOW_LOG) Log.e("Error KeyStore", "MenuSuiteDelgate: Error al borrar datos en KeyStore");
				}*/
			}
		}
	} catch (Exception e1) {
		// TODO Auto-generated catch block
		// Si datos no validos, comprueba en archivo datosBmovil
		DatosBmovilFileManager datosBmovil = DatosBmovilFileManager
				.getCurrent();
		if (Server.ALLOW_LOG) Log.i("BConnect", "datosBmovil " + datosBmovil);
		seed = datosBmovil.getSeedStr();
		telefono = datosBmovil.getLogin();
		if (Server.ALLOW_LOG)
			Log.i("DatosBmovil", "seed " + seed);
		if (Server.ALLOW_LOG)
			Log.i("DatosBmovil", "telefono " + telefono);
		// Validar telefono y seed
		if (!(Tools.validaSeed(seed) && Tools.validaTelefono(telefono))) {
			// Si no se valida, cambia a bmovil = false EA#11
			PropertiesManager.getCurrent().setBmovilActivated(false);
		}
	}
}
	
	
	//TODO
	/***************/
	
	public void establecerUsuario(EditText mUserEdit) {
		Session session = Session.getInstance(SuiteApp.appContext);
		String user = session.getUsername();
		mUserEdit.setText(user);
		mUserEdit.setEnabled(true);
		mUserEdit.setEnabled(true);
		mUserEdit.setFocusable(true);
		mUserEdit.setFocusableInTouchMode(true);
		mUserEdit.setClickable(true);
		if ((user != null && user.length() > 0)
				&& session.isApplicationActivated()) {
			mUserEdit.setText(Tools.hideUsername(user));
			mUserEdit.setEnabled(false);
			mUserEdit.setFocusable(false);
			mUserEdit.setClickable(false);
		} else {
			if (mUserEdit.getText().toString().indexOf("*") != -1) {
				mUserEdit.setText("");
				mUserEdit.setEnabled(true);
				mUserEdit.setFocusable(true);
				mUserEdit.setFocusableInTouchMode(true);
				mUserEdit.setClickable(true);
				mUserEdit.requestFocus();
			}
		}
	}

	public void establecerUsuario(TextView mUserEdit) {
		Session session = Session.getInstance(SuiteApp.appContext);
		String user = session.getUsername();
		mUserEdit.setText(user);
		mUserEdit.setEnabled(true);
		mUserEdit.setEnabled(true);
		mUserEdit.setFocusable(true);
		mUserEdit.setFocusableInTouchMode(true);
		mUserEdit.setClickable(true);
		if ((user != null && user.length() > 0)
				&& session.isApplicationActivated()) {
			mUserEdit.setText(Tools.hideUsername(user));
			mUserEdit.setEnabled(false);
			mUserEdit.setFocusable(false);
			mUserEdit.setClickable(false);
		} else {
			if (mUserEdit.getText().toString().indexOf("*") != -1) {
				mUserEdit.setText("");
				mUserEdit.setEnabled(true);
				mUserEdit.setFocusable(true);
				mUserEdit.setFocusableInTouchMode(true);
				mUserEdit.setClickable(true);
				mUserEdit.requestFocus();
			}
		}
	}
	
	public void botonEntrar(String user, String pass) {
		if (Session.getInstance(SuiteApp.appContext).isApplicationActivated()) {

			if (validarCampos(user, pass)) {
				if (Session.getInstance(SuiteApp.appContext)
						.isApplicationActivated()) {
					login(user, pass);
				} else {
					registrarAplicacion(user, pass);
				}
				//ARR
				encriptacionEnLogin = true;
				//ARR
				nombreUsuario = Session.getInstance(SuiteApp.appContext).getUsername();
			}
		} else {
			DatosBmovilFileManager.getCurrent().setLogin(user);
			ConsultaEstatusAplicacionDesactivadaDelegate desactivada=new ConsultaEstatusAplicacionDesactivadaDelegate();
			SuiteAppDesac suit = new SuiteAppDesac();
			suit.onCreate(this.getMenuSuiteViewController());
			suit.setFlagsEnvironmment(Server.SIMULATION, Server.ALLOW_LOG, Server.EMULATOR, Server.DEVELOPMENT);

			suit.setCallBackContratacion(new MenuSuiteViewController());
			suit.setActivityForContratacion(new MenuSuiteViewController());
		
			suit.setActivityForSofttoken(new MenuSuiteViewController());			
			suit.setCallBackForSofttoken(new MenuSuiteViewController());
			
			suit.setCallBackForMantenimiento(new MenuSuiteViewController());
			suit.setActivityForMantenimiento(new MenuSuiteViewController());

			suit.setCallBackReactivacion((CallBackBConnect) menuSuiteViewController);
			suit.setActivityReactivacion(menuSuiteViewController);

			suit.setCallBackActivacion((CallBackBConnect) menuSuiteViewController);
			suit.setActivityActivacion(menuSuiteViewController);

			suit.setCallBackForDesactivada((CallBackBConnect) menuSuiteViewController);
			
			suit.setEstados(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().estados);
			SincronizarSesion.getInstance().SessionToSessionApi(SuiteApp.appContext);
			if (desactivada.validarDatos(user,true)) {
				nombreUsuario = user;
			}
			((MenuSuiteViewController) getMenuSuiteViewController()).setButtonsDisabled(false);
		}
	}
	
	public boolean validarCampos(String userCellPhone, String userPass) {
		if(Server.ALLOW_LOG) Log.d("LoginDelegate", "Aquí se debe validar los datos de acceso");

		Session session = Session.getInstance(SuiteApp.appContext);
		/* Validate user field */
		if (userCellPhone.length() == 0) {
			menuSuiteViewController
					.showInformationAlert(R.string.login_usernameCannotBeEmpty, createEnableButtonsClickListener());
			return false;
		} else if (userCellPhone.length() < Constants.TELEPHONE_NUMBER_LENGTH) {
			menuSuiteViewController
					.showInformationAlert(R.string.login_usernameTooShort, createEnableButtonsClickListener());
			return false;
		}

		/* Validate password field */
		if (userPass.length() == 0) {
			menuSuiteViewController
					.showInformationAlert(session.isApplicationActivated() ? R.string.error_passwordCannotBeEmptyForLogin
							: R.string.error_passwordCannotBeEmptyForActivation, createEnableButtonsClickListener());
			return false;
		} else if (userPass.length() < Constants.PASSWORD_LENGTH) {
			menuSuiteViewController
					.showInformationAlert(R.string.error_passwordTooShort, createEnableButtonsClickListener());
			if (menuSuiteViewController != null)
				((MenuSuiteViewController)menuSuiteViewController).limpiarCampoContrasena();
			return false;
		}

		return true;
	}

	public boolean validaUserName(final String userCellPhone) {
		if (userCellPhone.length() == 0
				&& !Session.getInstance(SuiteApp.appContext).isApplicationActivated()) {
			return Boolean.TRUE;
		} if (userCellPhone.length() == 0) {
			menuSuiteViewController
					.showInformationAlert(R.string.login_usernameCannotBeEmpty, createEnableButtonsClickListener());
			return Boolean.FALSE;
		} else if (userCellPhone.length() < Constants.TELEPHONE_NUMBER_LENGTH) {
			menuSuiteViewController
					.showInformationAlert(R.string.login_usernameTooShort, createEnableButtonsClickListener());
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

	/*** analizar **/

	public void registrarAplicacion(String username, String password) {
		Session session = Session.getInstance(SuiteApp.appContext);
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, username);
		paramTable.put(ServerConstants.PARAMS_TEXTO_NP, password);
		long seed = session.getSeed();
		if (seed == 0) {
			seed = System.currentTimeMillis();
			session.setSeed(seed);
		}
		String ium = Tools.buildIUM(username, seed, SuiteApp.appContext);
		session.setIum(ium);
		session.setUsername(username);
		session.setPassword(password);
		paramTable.put(ServerConstants.PARAMS_TEXTO_IU, ium);

		String marcaModelo = Build.BRAND + Build.MODEL;
		paramTable.put("MR", marcaModelo);
		paramTable.put("CT", "ANDROID");
		//JAIG
		doNetworkOperation(Server.ACTIVATION_STEP1_OPERATION, paramTable, false, null, Server.isJsonValueCode.NONE, menuSuiteViewController, false);
	}

	public void activarAplicacion(String activationCode) {
		final Session session = Session.getInstance(SuiteApp.appContext);
		if (!session.isApplicationActivated()) {
			String password = session.getPassword();
			String errorMsg = null;

			/* Validate activation code field */
			if (activationCode.length() == 0) {
				errorMsg = SuiteApp.appContext
						.getString(R.string.activation_activationCodeCannotBeEmpty);
			} else if (activationCode.length() < Constants.ACTIVATION_CODE_LENGTH) {
				errorMsg = SuiteApp.appContext.getString(
						R.string.activation_activationCodeTooShort,
						Constants.ACTIVATION_CODE_LENGTH);
			}

			/* If validation failed then send an error message */
			if (errorMsg != null) {
				menuSuiteViewController.showInformationAlert(errorMsg);
				return;
			}

			String ium = session.getIum();
			session.setActivationCode(activationCode);

			// prepare parameters
			Hashtable<String, String> paramTable = new Hashtable<String, String>();
			paramTable.put(ServerConstants.PARAMS_TEXTO_NT, session.getUsername());
			paramTable.put(ServerConstants.PARAMS_TEXTO_NP, password);
			paramTable.put("AC", activationCode);
			paramTable.put(ServerConstants.PARAMS_TEXTO_IU, ium);

			// performs the network operation
            //JAIG
			doNetworkOperation(Server.ACTIVATION_STEP2_OPERATION, paramTable, false, null, Server.isJsonValueCode.NONE,
                    menuSuiteViewController, false);
		} else {
			storeSessionType = STORE_SESSION_LOGIN;
			menuSuiteViewController.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					login(session.getUsername(), session.getPassword());
				}
			});
		}
	}

	public void login(String userCellPhone, String password) {

		Session session = Session.getInstance(SuiteApp.appContext);
		String username = session.getUsername();
		session.setPassword(password);

		InitLogin initLogin=new InitLogin(this.getMenuSuiteViewController());
		SuiteAppLogin.getInstance().setCallBackLogin((CallBackBConnect) menuSuiteViewController);

		SuiteAppLogin.getInstance().setCallBackContratacion(new MenuSuiteViewController());
		SuiteAppLogin.getInstance().setActivityForContratacion(new MenuSuiteViewController());

		SuiteAppLogin.getInstance().setActivityForSofttoken(new MenuSuiteViewController());
		SuiteAppLogin.getInstance().setCallBackForSofttoken(new MenuSuiteViewController());

		SuiteAppLogin.getInstance().setCallBackForMantenimiento(new MenuSuiteViewController());
		SuiteAppLogin.getInstance().setActivityForMantenimiento(new MenuSuiteViewController());

		SuiteAppLogin.getInstance().setCallBackReactivacion((CallBackBConnect) menuSuiteViewController);
		SuiteAppLogin.getInstance().setActivityReactivacion(menuSuiteViewController);

		SuiteAppLogin.getInstance().setCallBackActivacion((CallBackBConnect) menuSuiteViewController);
		SuiteAppLogin.getInstance().setActivityActivacion(menuSuiteViewController);

		try {
			if(!initLogin.login(username, password, true)){
				if (menuSuiteViewController != null)
					((MenuSuiteViewController)menuSuiteViewController).limpiarCampoContrasena();
			}
		}catch (Exception e){
			((MenuSuiteViewController) getMenuSuiteViewController()).setButtonsDisabled(false);
		}
		((MenuSuiteViewController) getMenuSuiteViewController()).setButtonsDisabled(false);
	}

	public boolean isBmovilAppActivated() {
		boolean actFlag = Session.getInstance(SuiteApp.appContext)
				.isApplicationActivated();
		if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "isBmovilAppActivated = " + actFlag);

		return actFlag;
	}

	public void consultaEstatusCliente() {
		Session session = Session.getInstance(SuiteApp.appContext);
		String username = session.getUsername();

		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.PARAMS_TEXTO_NT, username);
        //JAIG
		doNetworkOperation(Server.CONSULTA_ESTATUSSERVICIO, paramTable, false, new EstatusDelServicioData(), Server.isJsonValueCode.NONE,
				menuSuiteViewController, false);
	}
	

	public void doNetworkOperation(final int operationId,
			final Hashtable<String, ?> params, boolean isJson, ParsingHandler handler, Server.isJsonValueCode isJsonValueCode, final BaseViewController caller,
			boolean callerHandlesError) {

		if(menuSuiteViewController instanceof MenuSuiteViewController) ((MenuSuiteViewController)menuSuiteViewController).setButtonsDisabled(false);
		SuiteApp.getInstance()
				.getBmovilApplication()
				.invokeNetworkOperation(operationId, params, isJson, handler, isJsonValueCode, caller,
						callerHandlesError);
	}

	public void analyzeResponse(int operationId, ServerResponse response) {
		if (operationId == Server.LOGIN_OPERATION) {
			// Stores the session and goes to the menu screen
			Session session = Session.getInstance(SuiteApp.appContext);//2x1CGI
			// Now validates status
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL
					|| response.getStatus() == ServerResponse.OPERATION_OPTIONAL_UPDATE) {
				LoginData data = (LoginData) response.getResponse();

				consultaEstatus = new ConsultaEstatus();
				llenarConsultaEstatus(data);

				Session.getInstance(SuiteApp.appContext).setSecurityInstrument(
						data.getInsSeguridadCliente());

				final BmovilViewsController bmovilViewsController = SuiteApp
						.getInstance().getBmovilApplication()
						.getBmovilViewsController();

				bmovilViewsController.setCurrentActivityApp(menuSuiteViewController);
				SincronizarSesion.getInstance().SessionToSessionApi(SuiteApp.appContext);
				if (data.getEstatusServicio().equals(
						Constants.STATUS_APP_ACTIVE)) {
					loginResponse = response;
					loginData = (LoginData) response.getResponse();

					storeSessionAfterLogin(response);
					SincronizarSesion.getInstance().SessionToSessionApi(SuiteApp.appContext);
				} else if (data.getEstatusServicio().equals(
						Constants.STATUS_PENDING_ACTIVATION)
						|| data.getEstatusServicio().equals(
								Constants.STATUS_PENDING_SEND)) {
					// Eliminacion 2x1
					if (session.getSecurityInstrument().equals("S1")){

						procesadoEscenarioAlterno25(session,data);

					}else{
						bmovilViewsController.showActivacion(consultaEstatus,
							currentPassword, true);
					}
				} else if (data.getEstatusServicio().equals(
						Constants.STATUS_PASS_BLOCKED)) {
					bmovilViewsController.showDesbloqueo(consultaEstatus);
				} else if (data.getEstatusServicio().equals(
						Constants.STATUS_APP_SUSPENDED)) {
					bmovilViewsController.showQuitarSuspension(consultaEstatus);
				} else if (data.getEstatusServicio().equals(
						Constants.STATUS_USER_CANCELED)
						|| data.getEstatusServicio().equals(
								Constants.STATUS_BANK_CANCELED)) {
					String fecha = Tools.invertDateOrder(data
							.getFechaContratacion());
					if (fecha.equals(data.getServerDate()))
						menuSuiteViewController
								.showInformationAlert(menuSuiteViewController
										.getString(R.string.bmovil_estatus_aplicacion_desactivada_error_fechas_iguales));
					else
						bmovilViewsController.showContratacion(consultaEstatus,
								data.getEstatusServicio(), true);
				} else if (data.getEstatusServicio().equals(
						Constants.STATUS_CLIENT_NOT_FOUND)
						|| data.getEstatusServicio().equals(
								Constants.STATUS_ENGAGEMENT_UNCOMPLETE)) {
					bmovilViewsController.showContratacion(consultaEstatus,
							data.getEstatusServicio(), true);
				} else if(data.getEstatusServicio().equals(
						Constants.STATUS_WRONG_IUM)) {
					//Eliinacion 2x1
					if("S1".equals(session.getSecurityInstrument())) {
						procesadoEscenarioAlterno25(session, data);
					} else {
						menuSuiteViewController
								.showInformationAlert(R.string.bmovil_activacion_alerta_reactivar,
										new OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog, int which) {
												bmovilViewsController.showReactivacion(consultaEstatus,
														currentPassword);
											}
										}
								);
					}
				} else if (data.getEstatusServicio().equals(
						Constants.STATUS_NIP_BLOCKED)){
					menuSuiteViewController
							.showErrorMessage(R.string.error_nip_bloqueado);
				}else {
					menuSuiteViewController
							.showErrorMessage(R.string.error_communications);
				}
				
			} else {
				if (response
						.getMessageCode()
						.equals(SuiteApp.appContext
								.getString(R.string.menuSuite_update_mandatoryCode))) {
					showUpdateConfirmationAlert(response.getMessageText(),
							true, response.getUpdateURL());
				} else {
					menuSuiteViewController.showInformationAlert(response
							.getMessageText());
					if (menuSuiteViewController != null) {
						
						((MenuSuiteViewController)menuSuiteViewController).limpiarCampoContrasena();
						
					}
				}

			}

		} else if (operationId == Server.ACTIVATION_STEP1_OPERATION) {
			SincronizarSesion.getInstance().SessionToSessionApi(SuiteApp.appContext);
			BmovilViewsController bmovilViewsController = SuiteApp
					.getInstance().getBmovilApplication()
					.getBmovilViewsController();
			bmovilViewsController.showPantallaActivacion();
		} else if (operationId == Server.ACTIVATION_STEP2_OPERATION) {
			SuiteViewsController suiteViewsController = SuiteApp.getInstance()
					.getSuiteViewsController();
			if (suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController) {
				MenuSuiteViewController suiteViewController = (MenuSuiteViewController) suiteViewsController
						.getCurrentViewControllerApp();
				suiteViewController.restableceMenu();
			}
			storeSessionAfterActivation();
			SincronizarSesion.getInstance().SessionToSessionApi(SuiteApp.appContext);

			
		}
	}

	private void procesadoEscenarioAlterno25(final Session session,LoginData data) {


	/*	PropertiesManager.getCurrent().setSofttokenActivated(true);
		PropertiesManager.getCurrent().setBmovilActivated(true);

		DatosBmovilFileManager.getCurrent().setActivado(true);
		DatosBmovilFileManager.getCurrent().setSeed("1428588487931");
		DatosBmovilFileManager.getCurrent().setSoftoken(true);
		*/


		String estadoIs = data.getEstatusIS();
		session.setEstatusIS(estadoIs);


		//String estadoIsSession = session.getEstatusIS();

		//para probar solo
		//estadoIs="T3";
		if (estadoIs.equals("A1")){

			if (session.isSofttokenActivado()){

				final BmovilViewsController bmovilViewsController = SuiteApp
						.getInstance().getBmovilApplication()
						.getBmovilViewsController();

				menuSuiteViewController.showInformationAlert(
						R.string.bmovil_activacion_alerta_reactivar,
						new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
												int which) {

								Log.d(">> CGI-Nice-Ppl", "[EA#25] Entra en OK del alert");

								Log.d(">> CGI-Nice-Ppl", "[EA#25] Genera clase GeneraOTPSTDelegate");
								GeneraOTPSTDelegate generaOTPSTDelegate = new GeneraOTPSTDelegate();
								Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra el token");
								if (generaOTPSTDelegate.borraToken()) {
									Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra el token >> true");
									PropertiesManager.getCurrent().setSofttokenActivated(false);
								}

								PropertiesManager.getCurrent().setBmovilActivated(false);
								Log.d(">> CGI-Nice-Ppl", "[EA#25] BmovilActivated >> false");

								Log.d(">> CGI-Nice-Ppl", "[EA#25] DatosBMovil Borrado Inicio - Softoken>" + DatosBmovilFileManager.getCurrent().getSoftoken() + " - Activado>" + DatosBmovilFileManager.getCurrent().getActivado() + " - Seed>" + DatosBmovilFileManager.getCurrent().getSeed());
								DatosBmovilFileManager.getCurrent().setSoftoken(false);
								DatosBmovilFileManager.getCurrent().setActivado(false);
								DatosBmovilFileManager.getCurrent().setSeed("");
								Log.d(">> CGI-Nice-Ppl", "[EA#25] DatosBMovil Borrado Fin - Softoken>" + DatosBmovilFileManager.getCurrent().getSoftoken() + " - Activado>" + DatosBmovilFileManager.getCurrent().getActivado() + " - Seed>" + DatosBmovilFileManager.getCurrent().getSeed());

								Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra Session Inicio");
								borrarKeychain(session);
								Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra Session Fin");

								//aqui solo debe borrarse el archivo bmovil y el de softoken
								bmovilViewsController.showAutenticacionSoftoken(consultaEstatus, currentPassword);

							}
						});



			}else {

				procesadoEscenarioAlterno27( session, data);
			}


		}else {

			procesadoEscenarioAlterno26( session, data);

		}

	}

	private void procesadoEscenarioAlterno26(Session session, LoginData data) {
		borrarKeychain(session);
		menuSuiteViewController.showInformationAlert(R.string.alert_estado_instrumento_distintoA1, process());

	}
public OnClickListener process(){
	OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.dismiss();
			final BmovilViewsController bmovilViewsController = SuiteApp
					.getInstance().getBmovilApplication()
					.getBmovilViewsController();
			bmovilViewsController.showReactivacion(
					consultaEstatus, currentPassword);
		}
	};

	return listener;

}
	private void procesadoEscenarioAlterno27(final Session session, LoginData data) {

		final BmovilViewsController bmovilViewsController = SuiteApp
				.getInstance().getBmovilApplication()
				.getBmovilViewsController();

		menuSuiteViewController.showInformationAlert(
				R.string.bmovil_activacion_alerta_reactivar,
				new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog,
										int which) {

						PropertiesManager.getCurrent().setBmovilActivated(false);
						DatosBmovilFileManager.getCurrent().setActivado(false);
						DatosBmovilFileManager.getCurrent().setSeed("0");
						borrarKeychain(session);
						//aqui solo debe borrarse el archivo bmovil
						bmovilViewsController.showAutenticacionSoftoken(
								consultaEstatus, currentPassword);
					}
				});

	}

	/**
	 * Llena el objeto consulta estatus con los datos obtenidos de la respuesta
	 * de NACAR.
	 * 
	 * @param data
	 *            La respuesta de NACAR.
	 */
	private void llenarConsultaEstatus(LoginData data) {
		consultaEstatus.setCompaniaCelular(data.getCompaniaTelCliente());
		consultaEstatus.setEmailCliente(data.getEmailCliente());
		consultaEstatus.setEstatus(data.getEstatusServicio());
		consultaEstatus.setEstatusInstrumento(data.getEstatusInstrumento());
		consultaEstatus.setInstrumento(data.getInsSeguridadCliente());
		consultaEstatus.setNombreCliente("");
		consultaEstatus.setNumCelular(Session.getInstance(SuiteApp.appContext)
				.getUsername());
		consultaEstatus.setNumCliente(data.getClientNumber());
		consultaEstatus.setPerfilAST(data.getPerfiCliente());
		consultaEstatus.setPerfil(Tools.determinaPerfil(
				consultaEstatus.getPerfilAST(),
				consultaEstatus.getInstrumento(),
				consultaEstatus.getEstatusInstrumento()));
		consultaEstatus.setEstatusAlertas(data.getEstatusAlertas());
		
		//Guardamos EA (EsatusAlertas) para EA11# p026 Transferir BancomerNFR 
		
		Session session = Session.getInstance(SuiteApp.appContext);
		session.setEstatusAlertas(data.getEstatusAlertas());

		if(Server.ALLOW_LOG) Log.d("ConsultaEstatus", consultaEstatus.toString());
	}

	public void reciveRespuestaLoginApi(suitebancomercoms.aplicaciones.bmovil.classes.model.LoginData loginData, suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus consultaEstatus,String responsePlain){
		ServerResponse response=null;
		LoginData loginData1=new LoginData();
		try {
			if(!ServerCommons.ARQSERVICE){
					StringReader reader = new StringReader(responsePlain);
					Parser parser = new Parser(reader);
					response= new ServerResponse(loginData1);
					response.process(parser);
			}else{
					loginData1.process(new JSONObject(responsePlain).getJSONObject("response"));
					response= new ServerResponse(loginData1);
			}
		}catch (Exception e){
			if(Server.ALLOW_LOG)
				Log.e("error parseo de login",e.getMessage());
		}

		storeSessionAfterLogin(response);
	}
	/**
	 * Store session after login
	 * 
	 * @param response
	 *            the server response
	 */
	private void storeSessionAfterLogin(ServerResponse response) {
		SuiteApp.getInstance().getBmovilApplication()
				.setApplicationLogged(true);
		SuiteAppAdmonApi.getInstance().getBmovilApplication().setApplicationLogged(true);
		loginResponse = response;
		loginData = (LoginData) response.getResponse();
		final Session session = Session.getInstance(SuiteApp.appContext);
		session.setClientNumber(loginData.getClientNumber());
		session.setAccounts(loginData.getAccounts(), loginData.getServerDate(),
				loginData.getServerTime());
		session.setEmail(loginData.getEmailCliente());

		session.setNombreCliente(Tools.isEmptyOrNull(loginData
				.getNombreCliente()) ? "" : loginData.getNombreCliente());
		if (loginData.getPerfiCliente().equals(Constants.PROFILE_ADVANCED_03)) {
			session.setClientProfile(Constants.Perfil.avanzado);
		} else if (loginData.getPerfiCliente().equals(Constants.PROFILE_RECORTADO_02)) {
			session.setClientProfile(Constants.Perfil.recortado);
		}else if (loginData.getPerfiCliente().equals(Constants.PROFILE_BASIC_01) ||loginData.getPerfiCliente().equals(Constants.PROFILE_BASIC_00) ){
			session.setClientProfile(Constants.Perfil.basico);
		}

		session.setCompaniaUsuario(loginData.getCompaniaTelCliente());
		session.setSecurityInstrument(loginData.getInsSeguridadCliente());
		session.setEstatusIS(loginData.getEstatusIS());

		session.setAuthenticationJson(loginData.getAuthenticationJson());
		if (loginData.getCatalogoTiempoAire() != null) {
			session.updateCatalogoTiempoAire(loginData.getCatalogoTiempoAire());
		}
		if (loginData.getCatalogoDineroMovil() != null) {
			session.updateCatalogoDineroMovil(loginData
					.getCatalogoDineroMovil());
		}
		if (loginData.getCatalogoServicios() != null) {
			session.updateCatalogoServicios(loginData.getCatalogoServicios());
		}
		if (loginData.getCatalogoMantenimientoSPEI() != null) {
			session.updateCatalogoMantenimientoSPEI(loginData.getCatalogoMantenimientoSPEI());
		}

		/*
		 * if the server has returned at least 1 catalog it means that the
		 * catalog version has changed
		 */
		if (loginData.getCatalogs() != null) {
			escribirCatalogos(loginData);
		}

		// Verifica el timeout de la pantalla del teléfono, si este timeout es
		// menor que el regresado por el servidor, se toma el del teléfono
		int defTimeOut = 0;
		final int DELAY = 3000;
		final int oneMinuteInMilis = 60000;

		if (SuiteApp.getInstance()
				.getBmovilApplication().getBmovilViewsController()
				.getCurrentViewControllerApp() == null) {
			SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(
					menuSuiteViewController);
		}

		defTimeOut = Settings.System.getInt(SuiteApp.getInstance()
				.getBmovilApplication().getBmovilViewsController()
				.getCurrentViewControllerApp().getContentResolver(),
				Settings.System.SCREEN_OFF_TIMEOUT, DELAY);

		if (defTimeOut < loginData.getTimeout() * oneMinuteInMilis
				&& defTimeOut > 15000)
			session.setTimeout(defTimeOut);
		else
			session.setTimeout(loginData.getTimeout() * oneMinuteInMilis);

		session.setValidity(Session.VALID_STATUS);
		menuSuiteViewController.muestraIndicadorActividad("",
				menuSuiteViewController.getString(R.string.alert_StoreSession));
		storeSessionType = STORE_SESSION_LOGIN;
		//SPEI
		//  if(loginData.getSpeiCatalog() != null ) {
			//  System.out.println("LoginData es nulo? "+ loginData.getSpeiCatalog() );
    	//session.setSpeiCatalog(loginData.getSpeiCatalog());
    	//session.saveSpeiCatalog();
    	//session.saveSpeiCatalogJson();
		// }
		//termina SPEI
		session.storeSession(this);
	}

	private void storeSessionAfterActivation() {
		storeSessionType = STORE_SESSION_ACTIVATION;
		menuSuiteViewController.muestraIndicadorActividad("",
				menuSuiteViewController.getString(R.string.alert_StoreSession));
		Session session = Session.getInstance(SuiteApp.appContext);
		session.setApplicationActivated(true);
		session.storeSession(this);
	}

	public String getUsuario() {
		return Session.getInstance(SuiteApp.appContext).getUsername();
	}

	public String getIum() {
		return Session.getInstance(SuiteApp.appContext).getIum();
	}

	public Catalog[] leerCatalogos() {
		return Session.getInstance(SuiteApp.appContext).getCatalogs();
	}

	public void escribirCatalogos(LoginData loginData) {
		Session session = Session.getInstance(SuiteApp.appContext);
		session.setCatalogVersions(loginData.getCatalogVersions());
		// the parameter(array) logindata.GetCatalogs may have null elements
		// indicating that the server has not sended a catalog in it's
		// reply(that
		// only happens when our local catalog have the same version that server
		// catalog). In this case the session.updateCatalogs method will ignore
		// the passed param
		session.updateCatalogs(loginData.getCatalogs());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.common.SessionStoredListener
	 * #sessionStored()
	 */
	@Override
	public void sessionStored() {
		menuSuiteViewController.ocultaIndicadorActividad();
		if (storeSessionType.equals(STORE_SESSION_LOGIN)) {
			SincronizarSesion.getInstance().SessionToSessionApi(SuiteApp.appContext);
			if (loginResponse.getStatus() == ServerResponse.OPERATION_OPTIONAL_UPDATE) {
				menuSuiteViewController.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						showUpdateConfirmationAlert(
								loginData.getUpdateMessage(), false,
								loginData.getUpdateURL());
					}
				});
			} else if (loginResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				SuiteAppAdmonApi suiteAppAdmonApi = new SuiteAppAdmonApi();
				suiteAppAdmonApi.onCreate(menuSuiteViewController);
				SuiteAppAdmonApi.getInstance().getBmovilApplication().setApplicationLogged(SuiteApp.getInstance().getBmovilApplication().isApplicationLogged());
				SuiteAppAdmonApi.setCallBackBConnect((CallBackBConnect) menuSuiteViewController);
				SuiteAppAdmonApi.setCallBackSession(null);
				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(Boolean.TRUE);
				final CambioPerfilDelegate cambioPerfilDelegate = new CambioPerfilDelegate();
				Session session = Session.getInstance(menuSuiteViewController);
				//
			/*
					// Se valida si existe un cambio de perfil de basico a avanzado
				if (cambioPerfilDelegate
						.validarCambioPerfilAvanzado(menuSuiteViewController)) {

					session.saveBanderasBMovil(
							Constants.BANDERAS_CAMBIO_PERFIL, false, false);
					if (isTokenActivo()) {
						cambioPerfilDelegate
								.mostrarCambioPerfil(menuSuiteViewController);
					}
					
				}



			// Se valida si existe un cambio de perfil de avanzado a basico
			if (cambioPerfilDelegate.validarCambioPerfilBasico()) {
				SuiteAppAdmonApi.getInstance()
						.getBmovilApplication()
						.getBmovilViewsController()
						.addDelegateToHashMap(
								CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID,
								cambioPerfilDelegate);
				cambioPerfilDelegate
						.setBaseViewController(menuSuiteViewController);
				final CambioPerfilDelegate delegate = cambioPerfilDelegate;
				menuSuiteViewController.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						delegate.realizaOperacion();
					}
				});
				return;
			}
*/
				boolean checkLoginWarning = true;

				if (checkLoginWarning) {
					final String loginWarningMessage = loginData.getMessage();
					final String titleMessage = "";// SuiteApp.appContext.getString(R.string.label_important);
					
					// de Bienvenida
					//if (loginWarningMessage != null) { //MP vacio no muestra alert
					if (!("".equals(loginWarningMessage))) {	
						menuSuiteViewController.runOnUiThread(new Runnable() {
							@Override
							public void run() {

								// Parte de incidencia 22249 para que no se
								// muestre el alert
//								if (
//										!(parentViewController instanceof ActivacionViewController)
//										&& 
							/*	if(!(cambioPerfilDelegate.isCambioPerfil())) {
									menuSuiteViewController.showInformationAlert(
											titleMessage, loginWarningMessage,
											new OnClickListener() {
												public void onClick(DialogInterface dialog,	int which) {
													dialog.dismiss();
*/
													SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal();
//													if (parentViewController instanceof ActivacionViewController) {
//														parentViewController.finish();
//														MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteApp.getInstance().getSuiteViewsController()
//																.getCurrentViewControllerApp();
//														menuViewController.setShouldHideLogin(true);
//													}
											/*	}
											});

								}*/
								// Alberto Init Timer
								//SuiteApp.getInstance().getBmovilApplication()
								//		.initTimer();
								//SuiteApp.getInstance().getBmovilApplication()
								//		.resetLogoutTimer();

								TimerController timerContr = TimerController.getInstance();
								timerContr.setSessionCloser(SuiteApp.getInstance().getBmovilApplication());
								timerContr.initTimer(Session.getInstance(SuiteApp.appContext).getTimeout());
								timerContr.resetTimer();
							}
						});
					} else {
						if(!(cambioPerfilDelegate.isCambioPerfil())) {
							SuiteApp.getInstance().getBmovilApplication()
									.getBmovilViewsController().showMenuPrincipal();
						}
//						if (parentViewController instanceof ActivacionViewController) {
//							parentViewController.finish();
//							MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteApp
//									.getInstance().getSuiteViewsController()
//									.getCurrentViewControllerApp();
//							// menuViewController.restableceMenu();
//							menuViewController.setShouldHideLogin(true);
//						}

						// Alberto Init Timer
						//SuiteApp.getInstance().getBmovilApplication()
						//		.initTimer();
						//SuiteApp.getInstance().getBmovilApplication()
						//		.resetLogoutTimer();
						Looper.prepare();
						TimerController timerContr = TimerController.getInstance();
						timerContr.setSessionCloser(SuiteApp.getInstance().getBmovilApplication());
						timerContr.initTimer(Session.getInstance(SuiteApp.appContext).getTimeout());
						timerContr.resetTimer();
						Looper.loop();
					}

				} else {
					SuiteApp.getInstance().getBmovilApplication()
							.getBmovilViewsController().showMenuPrincipal();
//					if (parentViewController instanceof ActivacionViewController) {
//						parentViewController.finish();
//						MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteApp
//								.getInstance().getSuiteViewsController()
//								.getCurrentViewControllerApp();
//						menuViewController.setShouldHideLogin(true);
//					}

					// Alberto Init Timer
					//SuiteApp.getInstance().getBmovilApplication().initTimer();
					//SuiteApp.getInstance().getBmovilApplication()
					//.resetLogoutTimer();

					TimerController timerContr = TimerController.getInstance();
					timerContr.setSessionCloser(SuiteApp.getInstance().getBmovilApplication());
					timerContr.initTimer(Session.getInstance(SuiteApp.appContext).getTimeout());
					timerContr.resetTimer();
				}
				loginData = null;
				loginResponse = null;
			}
		} else if (storeSessionType.equals(STORE_SESSION_ACTIVATION)) {
			storeSessionType = STORE_SESSION_LOGIN;
			menuSuiteViewController.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					login(Session.getInstance(SuiteApp.appContext)
							.getUsername(),
							Session.getInstance(SuiteApp.appContext)
									.getPassword());
				}
			});
		} else if (storeSessionType.equals(STORE_SESSION_PROFILE_CHANGE)) {
			storeSessionType = STORE_SESSION_LOGIN;
			menuSuiteViewController.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					SuiteApp.getInstance().getBmovilApplication()
							.getBmovilViewsController().showMenuPrincipal();
					MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteApp
							.getInstance().getSuiteViewsController()
							.getCurrentViewControllerApp();
					menuViewController.restableceMenu();

					// Alberto Init Timer
					//SuiteApp.getInstance().getBmovilApplication().initTimer();
					//SuiteApp.getInstance().getBmovilApplication()
					//		.resetLogoutTimer();

					TimerController timerContr = TimerController.getInstance();
					timerContr.setSessionCloser(SuiteApp.getInstance().getBmovilApplication());
					timerContr.initTimer(Session.getInstance(SuiteApp.appContext).getTimeout());
					timerContr.resetTimer();
				}
			});
		}
	}

	/***
	 * Show an alert to inform the user that there is a new version available,
	 * and asks what to do.
	 * 
	 * @param message
	 *            the information to show
	 * @param mandatory
	 *            true if the update is mandatory, false if not
	 * @param updateUrl
	 *            the url where the update can be found
	 */
	public void showUpdateConfirmationAlert(String message, boolean mandatory,
			final String updateUrl) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				menuSuiteViewController);
		alertDialog.setTitle(R.string.menuSuite_update_alert_title);
		alertDialog.setMessage(message);
		alertDialog.setCancelable(false);
		// Update button
		alertDialog.setPositiveButton(
				R.string.menuSuite_update_alert_updateOption,
				new OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						String url = updateUrl;
						if (url.length() > 0) {
							if (!url.startsWith("http://")
									&& !url.startsWith("https://")) {
								url = "http://" + updateUrl;
							}
							Intent browserIntent = new Intent(
									Intent.ACTION_VIEW, Uri.parse(url));
							dialog.dismiss();
							menuSuiteViewController.startActivity(browserIntent);
						}

					}
				});

		// Continue button
		if (!mandatory) {
			alertDialog.setNegativeButton(R.string.alert_continue,
					new OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							if (Session.getInstance(SuiteApp.appContext)
									.getValidity() == Session.VALID_STATUS) {
								SuiteApp.getInstance().getBmovilApplication()
										.getBmovilViewsController()
										.showMenuPrincipal();
								MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteApp
										.getInstance()
										.getSuiteViewsController()
										.getCurrentViewControllerApp();
								menuViewController.setShouldHideLogin(true);

								// Alberto Init Timer
								//SuiteApp.getInstance().getBmovilApplication()
								//		.initTimer();
								//SuiteApp.getInstance().getBmovilApplication()
								//		.resetLogoutTimer();

								TimerController timerContr = TimerController.getInstance();
								timerContr.setSessionCloser(SuiteApp.getInstance().getBmovilApplication());
								timerContr.initTimer(Session.getInstance(SuiteApp.appContext).getTimeout());
								timerContr.resetTimer();
							} else {
								MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteApp
										.getInstance()
										.getSuiteViewsController()
										.getCurrentViewControllerApp();
								menuViewController.restableceMenu();
							}

						}
					});
		} else {
			alertDialog.setNegativeButton(
					R.string.menuSuite_update_alert_exitOption,
					new OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteApp
									.getInstance().getSuiteViewsController()
									.getCurrentViewControllerApp();
							menuViewController.restableceMenu();
							menuViewController.setShouldHideLogin(true);
							SuiteApp.getInstance().getBmovilApplication()
									.setApplicationLogged(false);
						}
					});
		}

		menuSuiteViewController.hideSoftKeyboard();
		alertDialog.show();
	}
	
	private boolean isTokenActivo() {
		return Constants.TYPE_SOFTOKEN.S1.value.equals(consultaEstatus
				.getInstrumento())
				&& Constants.ESTATUS_IS_ACTIVO.equals(consultaEstatus
						.getEstatus());
	}
	
	/******************************************************************************************************************************************************************************************************
	 * Consulta estatus aplicacion desactivada
	 ******************************************************************************************************************************************************************************************************/
	

	/**
	 * Direcciona al flujo correspondiente seg�n el estatus recibido. <br/>
	 * A1 - Reactivación. <br/>
	 * PE - Activación. <br/>
	 * PA - Activación. <br/>
	 * PB - Manda mensaje de error �Para desbloquear tu NIP debes acudir al
	 * cajero�. <br/>
	 * BI - Desbloqueo de contraseña. <br/>
	 * C4 - Contratación. <br/>
	 * CN - Contratación. <br/>
	 * S4 - Quitar suspensi�n. <br/>
	 * PS - Contratación EA#2.
	 */


	private OnClickListener createEnableButtonsClickListener() {
		return new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				((MenuSuiteViewController) getMenuSuiteViewController()).setButtonsDisabled(false);
			}
		};
	}

	
	/**
	 * Busca el número de celular dentro de sesion, si hay uno registrado lo
	 * establece en el controlador.
	 */
	public String buscarNumero() {
//		 Session session = Session.getInstance(SuiteApp.appContext);
		 String celular = DatosBmovilFileManager.getCurrent().getLogin();//session.getUsername();
		
		return celular;
	}

	// #endregion

	
	public void checkWallet(){
		if(Tools.appInstalled(menuSuiteViewController.getString(R.string.uri_bbva_wallet), menuSuiteViewController)){
			// Si la app Wallet esta instalada abre esa app

			//hcf Boton Wallet
			Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
			//hcf
			paso2OperacionMap.put("evento_paso2", "event47");
			paso2OperacionMap.put("&&products", "menu suite+wallet+wallet instalado");
			paso2OperacionMap.put("eVar12", "paso2:abre wallet");

			TrackingHelper.trackPaso2Operacion(paso2OperacionMap);

			Intent i = new Intent(Intent.ACTION_MAIN);
			PackageManager manager = menuSuiteViewController.getPackageManager();
			i = manager.getLaunchIntentForPackage(menuSuiteViewController
					.getString(R.string.uri_bbva_wallet));
			if(Server.ALLOW_LOG) Log.i("Cambio imagen llamada", menuSuiteViewController.getString(R.string.uri_bbva_wallet));
			i.setAction(Intent.ACTION_SEND);
			i.addCategory(Intent.CATEGORY_LAUNCHER);
	//		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			menuSuiteViewController.startActivity(i);
			menuSuiteViewController.finish();
		}else{
			// Si la app Wallet NO esta instalada dirige a tienda			

			//hcf Boton Wallet
			Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
			//hcf
			paso2OperacionMap.put("evento_paso2", "event47");
			paso2OperacionMap.put("&&products", "menu suite+wallet+descargar");
			paso2OperacionMap.put("eVar12", "paso2:descargar wallet");

			TrackingHelper.trackPaso2Operacion(paso2OperacionMap);

			try{
				// Abre la aplicacion market del dispositivo
			    menuSuiteViewController.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(menuSuiteViewController.getString(R.string.uri_prefix_app_market) + menuSuiteViewController.getString(R.string.uri_bbva_wallet))));
			}catch(ActivityNotFoundException anfe){
				// Abre en el navegador el market para descargar esa aplicacion
				menuSuiteViewController.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(menuSuiteViewController.getString(R.string.uri_prefix_app_market_web) + menuSuiteViewController.getString(R.string.uri_bbva_wallet))));
			}
		
		}
	}

	public void checkVidaBancomer(){
		if(Tools.appInstalled(menuSuiteViewController.getString(R.string.uri_bbva_vidabacomer), menuSuiteViewController)){
			// Si la app vida bancomer esta instalada abre esa app

			//hcf Boton vida
			Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
			//hcf
			paso2OperacionMap.put("evento_paso2", "event47");
			paso2OperacionMap.put("&&products", "menu suite + vida bancomer + vida bancomer instalado");
			paso2OperacionMap.put("eVar12", "paso2:abre vida bancomer");

			TrackingHelper.trackPaso2Operacion(paso2OperacionMap);

			Intent i = new Intent(Intent.ACTION_MAIN);
			PackageManager manager = menuSuiteViewController.getPackageManager();
			i = manager.getLaunchIntentForPackage(menuSuiteViewController
					.getString(R.string.uri_bbva_vidabacomer));
			if(Server.ALLOW_LOG) Log.i("Cambio imagen llamada", menuSuiteViewController.getString(R.string.uri_bbva_vidabacomer));
			i.setAction(Intent.ACTION_SEND);
			i.addCategory(Intent.CATEGORY_LAUNCHER);
			//		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			menuSuiteViewController.startActivity(i);
			menuSuiteViewController.finish();
		}else{
			// Si la app vida bancomer NO esta instalada dirige a tienda

			//hcf Boton vida bancomer
			Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
			//hcf
			paso2OperacionMap.put("evento_paso2", "event47");
			paso2OperacionMap.put("&&products", "menu suite + vida bancomer + descargar");
			paso2OperacionMap.put("eVar12", "paso2:descargar vida bancomer");

			TrackingHelper.trackPaso2Operacion(paso2OperacionMap);

			try{
				// Abre la aplicacion market del dispositivo
				menuSuiteViewController.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(menuSuiteViewController.getString(R.string.uri_prefix_app_market) + menuSuiteViewController.getString(R.string.uri_bbva_vidabacomer))));
			}catch(ActivityNotFoundException anfe){
				// Abre en el navegador el market para descargar esa aplicacion
				menuSuiteViewController.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(menuSuiteViewController.getString(R.string.uri_prefix_app_market_web) + menuSuiteViewController.getString(R.string.uri_bbva_vidabacomer))));
			}

		}
	}


	private void borrarKeychain(Session session){
		KeyStoreWrapper kswrapper= KeyStoreWrapper.getInstance(SuiteApp.appContext);
		try {
			kswrapper.setUserName(" ");  	//setEntry(Constants.USERNAME, " ");
			kswrapper.setSeed(" ");  		//setEntry(Constants.SEED, " ");
			kswrapper.storeValueForKey(Constants.CENTRO, " ");  //setEntry(Constants.CENTRO, " ");
			if(Server.ALLOW_LOG) {
				Log.i(this.getClass().getName(), "Eliminando datos de KeyChain...");
				Log.i("Key-> ", "UserName: " + kswrapper.getUserName());
				Log.i("Key->", "Seed: " + kswrapper.getSeed());
			}
		} catch (Exception e) { //------
			if(Server.ALLOW_LOG) {
				Log.e(this.getClass().getName(), e.getMessage());
			}
		} finally {
			final String telefono = Session.getInstance(SuiteApp.appContext).getUsername();
			Session.getInstance(SuiteApp.appContext).clearSession();
			Session.getInstance(SuiteApp.appContext).setUsername(telefono);
			SincronizarSesion.getInstance().SessionToSessionApi(SuiteApp.appContext);
		}

	}

	public void showContactanos(Context ctx) {

		// Comprobacion si app Linea bancomer esta instalada
//		if(Tools.appInstalled(currentViewControllerApp.getString(R.string.uri_linea_bancomer), currentViewControllerApp)){
//
//			//hcf Linea Bancomer instalado
//			Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
//			//hcf
//			paso2OperacionMap.put("evento_paso2", "event47");
//			paso2OperacionMap.put("&&products", "menu suite+linea bancomer+linea bancomer instalado");
//			paso2OperacionMap.put("eVar12", "paso2:abre linea bancomer");
//
//			TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
//
//			// Si la app Linea Bancomer esta instalada abre la app
//			Intent i = new Intent(Intent.ACTION_MAIN);
//			PackageManager manager = currentViewControllerApp.getPackageManager();
//			i = manager.getLaunchIntentForPackage(currentViewControllerApp
//					.getString(R.string.uri_linea_bancomer));
//			if(Server.ALLOW_LOG) Log.i("Cambio imagen llamada", currentViewControllerApp.getString(R.string.uri_linea_bancomer));
//			i.setAction(Intent.ACTION_SEND);
//			i.addCategory(Intent.CATEGORY_LAUNCHER);
//			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			currentViewControllerApp.startActivity(i);
//			currentViewControllerApp.finish();
//
//		}else{
//
//			//hcf Descarga Linea Bancomer
//			Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
//			//hcf
//			paso2OperacionMap.put("evento_paso2", "event47");
//			paso2OperacionMap.put("&&products", "menu suite+linea bancomer+descargar");
//			paso2OperacionMap.put("eVar12", "paso2:descargar linea bancomer");
//
//			TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
//
//			// Muestra alert para descarga de app
//			currentViewControllerApp.showYesNoAlert(currentViewControllerApp.getString(R.string.menuSuite_descarga_linea_bancomer_tittle),
//					currentViewControllerApp.getString(R.string.menuSuite_descarga_linea_bancomer),
//					currentViewControllerApp.getString(R.string.common_alert_yesno_positive_button),
//					currentViewControllerApp.getString(R.string.common_alert_yesno_negative_button), new OnClickListener() {
//
//				// Boton aceptar, a descarga app
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					// TODO Auto-generated method stub
//					// A descargar aplicacion
//					try{
//						// Abre la aplicacion market del dispositivo
//					    currentViewControllerApp.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(currentViewControllerApp.getString(R.string.uri_prefix_app_market) + currentViewControllerApp.getString(R.string.uri_linea_bancomer))));
//					}catch(ActivityNotFoundException anfe){
//						// Abre en el navegador el market para descargar esa aplicacion
//						currentViewControllerApp.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(currentViewControllerApp.getString(R.string.uri_prefix_app_market_web) + currentViewControllerApp.getString(R.string.uri_linea_bancomer))));
//					}
//				}
//			}, new OnClickListener() {
//
//				// Boton cancelar, alert de llamadas
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					// TODO Auto-generated method stub
//					// Muestra opciones de llamada
        final Dialog contactDialog = new Dialog(ctx);
		LayoutInflater inflater = (LayoutInflater) ctx.
				getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.menu_contactanos, null);
        contactDialog.setContentView(layout);
        Button llamar = (Button) layout.findViewById(R.id.llamar);
        Button app = (Button) layout.findViewById(R.id.app);
        Button cancelar = (Button) layout.findViewById(R.id.cancelar);

		contactDialog.setTitle(ctx.getString(R.string.menuSuite_menuTitle));
		//contactDialog.setCancelable(false);
        app.setHeight(llamar.getHeight());
       // app.setTextSize(llamar.getTextSize());
       // app.setCompoundDrawables(null, null, ctx.getResources().getDrawable(R.drawable.icono_nuevo), null);
       // app.setTextAlignment(llamar.getTextAlignment());
        llamar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        llamarLineaBancomer();
                        contactDialog.dismiss();
                    }
                }
        );

        app.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        appLineaBancomer();
                        contactDialog.dismiss();
                    }
                }
        );

        cancelar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        contactDialog.dismiss();
                    }
                }
        );




		/*contactDialog.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        llamarLineaBancomer();
                        return;
                    case 1:
                        appLineaBancomer();
                        return;

                    case 2:
                        dialog.dismiss();
                        return;
                }
            }
        });*/

        contactDialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		contactDialog.show();
        //.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

		//contactDialog.setOnDismissListener().setOnItemSelectedListener((MenuSuiteViewController) this.getMenuSuiteViewController());

	}

	private void appLineaBancomer() {
		// Comprobacion si app Linea bancomer esta instalada
		if(Tools.appInstalled(menuSuiteViewController.getString(R.string.uri_linea_bancomer), menuSuiteViewController)){
//
//			//hcf Linea Bancomer instalado
//			Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
//			//hcf
//			paso2OperacionMap.put("evento_paso2", "event47");
//			paso2OperacionMap.put("&&products", "menu suite+linea bancomer+linea bancomer instalado");
//			paso2OperacionMap.put("eVar12", "paso2:abre linea bancomer");
//
//			TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
//
//			// Si la app Linea Bancomer esta instalada abre la app
			Intent i = new Intent(Intent.ACTION_MAIN);
			PackageManager manager = menuSuiteViewController.getPackageManager();
			i = manager.getLaunchIntentForPackage(menuSuiteViewController
					.getString(R.string.uri_linea_bancomer));
//			if(Server.ALLOW_LOG) Log.i("Cambio imagen llamada", currentViewControllerApp.getString(R.string.uri_linea_bancomer));
			i.setAction(Intent.ACTION_SEND);
			i.addCategory(Intent.CATEGORY_LAUNCHER);
		//	i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            menuSuiteViewController.startActivity(i);
            menuSuiteViewController.finish();
//
		}else{
//
//			//hcf Descarga Linea Bancomer
//			Map<String,Object> paso2OperacionMap = new HashMap<String, Object>();
//			//hcf
//			paso2OperacionMap.put("evento_paso2", "event47");
//			paso2OperacionMap.put("&&products", "menu suite+linea bancomer+descargar");
//			paso2OperacionMap.put("eVar12", "paso2:descargar linea bancomer");
//
//			TrackingHelper.trackPaso2Operacion(paso2OperacionMap);
//
//			// Muestra alert para descarga de app
//			currentViewControllerApp.showYesNoAlert(currentViewControllerApp.getString(R.string.menuSuite_descarga_linea_bancomer_tittle),
//					currentViewControllerApp.getString(R.string.menuSuite_descarga_linea_bancomer),
//					currentViewControllerApp.getString(R.string.common_alert_yesno_positive_button),
//					currentViewControllerApp.getString(R.string.common_alert_yesno_negative_button), new OnClickListener() {
//
//				// Boton aceptar, a descarga app
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					// TODO Auto-generated method stub
//					// A descargar aplicacion
					try{
//						// Abre la aplicacion market del dispositivo
                        menuSuiteViewController.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(menuSuiteViewController.getString(R.string.uri_prefix_app_market) + menuSuiteViewController.getString(R.string.uri_linea_bancomer))));
					}catch(ActivityNotFoundException anfe){
//						// Abre en el navegador el market para descargar esa aplicacion
                        menuSuiteViewController.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(menuSuiteViewController.getString(R.string.uri_prefix_app_market_web) + menuSuiteViewController.getString(R.string.uri_linea_bancomer))));
//					}
//				}
//			}, new OnClickListener() {
//
//				// Boton cancelar, alert de llamadas
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					// TODO Auto-generated method stub
					// Muestra opciones de llamada
	}

}}}
