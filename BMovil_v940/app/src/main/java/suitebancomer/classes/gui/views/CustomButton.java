package suitebancomer.classes.gui.views;

import suitebancomer.classes.common.GuiTools;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.widget.Button;

public class CustomButton extends Button {
	
	private Paint m_paint = new Paint();

	public CustomButton(Context context) {
		super(context);		
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		
		final float scale = getResources().getDisplayMetrics().density;
		double factor = GuiTools.getScaleFactor();		
		m_paint.setColor(Color.rgb(102,204,0));
		m_paint.setTextSize((float) (11d * factor + 0.5d));
		m_paint.setAntiAlias(true);
		String text=getText().toString();
	    canvas.drawText(text, (float)(155d * factor) + 0.5f, (float)(29.2d * factor) + 0.5f, m_paint);
	}

}
