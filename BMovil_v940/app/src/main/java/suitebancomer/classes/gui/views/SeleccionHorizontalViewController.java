package suitebancomer.classes.gui.views;

import android.content.Context;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;

import bancomer.api.common.model.Compania;
import bancomer.api.common.model.Convenio;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.delegates.BaseDelegate;

public class SeleccionHorizontalViewController  extends LinearLayout implements View.OnClickListener, View.OnTouchListener {
	public final static String SELECT_SUFFIX = "_select";
	public final static String IMAGE_FORMAT = ".png";
	
	public final static String OTHER_ELEMENT_IMAGE_NAME = "otro.png";
	
	private static HashMap<String, Integer> imagenes;
	private static HashMap<String, Integer> imagenesSelec;
	
	private HorizontalScrollView componenteHorizontal;
	private LinearLayout contenidoComponenteHorizontal;
	private Object itemSeleccionado;
	private LayoutInflater inflater;
	private ArrayList<Object> componentes;
	private ArrayList<ImageButton> arregloBotones;
	private BaseDelegate delegate;
	
	private boolean blocked;

	public SeleccionHorizontalViewController(Context context,LinearLayout.LayoutParams layoutParams, 
											ArrayList<Object> listaComponentes, BaseDelegate delegate,
											boolean addOtherElement) {
		super(context);
		inflater = LayoutInflater.from(context);
		LinearLayout viewLayout = (LinearLayout) inflater.inflate(R.layout.layout_seleccion_horizontal_view, this, true);
		viewLayout.setLayoutParams(layoutParams);

		componenteHorizontal = (HorizontalScrollView)findViewById(R.id.scroll_seleccion_horizontal);
		contenidoComponenteHorizontal = (LinearLayout) findViewById(R.id.contenido_seleccion_horizontal);
		
        GuiTools.getCurrent().scalePaddings(contenidoComponenteHorizontal);
		
		this.componentes = listaComponentes;
		if (addOtherElement) {
			Convenio otroConvenio = new Convenio(SuiteApp.appContext.getString(R.string.servicesPayment_otherServices), OTHER_ELEMENT_IMAGE_NAME, "-1", Integer.toString(componentes.size()+1));
			componentes.add(otroConvenio);
		}
		this.delegate = delegate;
		reloadItems();
	}
	
	static {
		imagenes = new HashMap<String, Integer>();
		imagenesSelec = new HashMap<String, Integer>();
		
		imagenes.put("telcel.png", R.drawable.telcel);
		imagenesSelec.put("telcel.png", R.drawable.telcel_select);
		imagenes.put("nextel.png", R.drawable.nextel);
		imagenesSelec.put("nextel.png", R.drawable.nextel_select);
		imagenes.put("unefon.png", R.drawable.unefon);
		imagenesSelec.put("unefon.png", R.drawable.unefon_select);
		imagenes.put("iusacell.png", R.drawable.iusacel);
		imagenesSelec.put("iusacell.png", R.drawable.iusacel_select);
		imagenes.put("movistar.png", R.drawable.movistar);
		imagenesSelec.put("movistar.png", R.drawable.movistar_select);
		imagenes.put("telmex.png", R.drawable.telmex);
		imagenesSelec.put("telmex.png", R.drawable.telmex_select);
		imagenes.put("cfe.png", R.drawable.cfe);
		imagenesSelec.put("cfe.png", R.drawable.cfe_select);
		imagenes.put("liverpool.png", R.drawable.liverpool);
		imagenesSelec.put("liverpool.png", R.drawable.liverpool_select);
		imagenes.put("sky.png", R.drawable.btn_sky);
		imagenesSelec.put("sky.png", R.drawable.btn_skyon);
		imagenes.put("telcelcie.png", R.drawable.telcelcie);
		imagenesSelec.put("telcelcie.png", R.drawable.telcelcie_select);
		imagenes.put("avon.png", R.drawable.avon);
		imagenesSelec.put("avon.png", R.drawable.avon_select);
		imagenes.put("jafra.png", R.drawable.jafra);
		imagenesSelec.put("jafra.png", R.drawable.jafra_select);		
		imagenes.put("otro.png", R.drawable.otro);
		imagenesSelec.put("otro.png", R.drawable.otro_select);
	}

	public void setSelectedItem(Object item) {
		this.itemSeleccionado = item;
		changeSelection();
	}
	
	public void setOtroAsSelected() {
		this.itemSeleccionado = componentes.get(componentes.size()-1);
		changeSelection();
	}
	
	public Object getSelectedItem()
	{
		return itemSeleccionado;
	}
	
	public void changeSelection()
	{
		contenidoComponenteHorizontal.removeAllViews();
		reloadItems();
	}
	
	public void reloadItems()
	{
		GuiTools guitools = GuiTools.getCurrent();
        int selectedLateralSize = guitools.getEquivalenceInPixels(68);
        int basePading = guitools.getEquivalenceInPixels(8);
		
        LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.FILL_PARENT);
        params.setMargins(SuiteApp.appContext.getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_top_margin_no_selected),
        		SuiteApp.appContext.getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_side_margin_no_selected), 
        		SuiteApp.appContext.getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_top_margin_no_selected),
        		SuiteApp.appContext.getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_side_margin_no_selected));
        
        LinearLayout.LayoutParams params2 = new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.FILL_PARENT);
        params2.setMargins(SuiteApp.appContext.getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_top_margin_selected),
        		SuiteApp.appContext.getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_side_margin_selected), 
        		SuiteApp.appContext.getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_top_margin_selected),
        		SuiteApp.appContext.getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_side_margin_selected));
        
        LinearLayout.LayoutParams selectedButtonParams = new LayoutParams(selectedLateralSize, selectedLateralSize);
        selectedButtonParams.setMargins(basePading, 0, basePading, 0);
        
        arregloBotones = new ArrayList<ImageButton>();
        ImageButton imagen = null;
        ImageButton selectedButton = null;
        boolean imageFound = false;
        Object obj = null;
        Integer currentImage = null;
        int componentsSize = componentes.size();
        
//        for (int i = 0; i < componentsSize; i++) {
//        	imagen = new ImageButton(getContext());
//    		if ((itemSeleccionado == componentes.get(i)) ||
//    			(!imageFound && i==componentsSize-1)) {
//    			if (itemSeleccionado instanceof Convenio) {
//    				currentImage = (Integer)imagenesSelec.get(((Convenio)itemSeleccionado).getNombreImagen());
//					if (currentImage != null) {
//						imagen.setBackgroundResource(currentImage.intValue());
//					} else {
//    					imagen.setBackgroundResource(imagenesSelec.get(OTHER_ELEMENT_IMAGE_NAME));
//    				}
//    			} else if (itemSeleccionado instanceof Compania) {
//    				currentImage = imagenesSelec.get(((Compania)itemSeleccionado).getNombreImagen());
//    				if (currentImage != null) {
//						imagen.setBackgroundResource(currentImage.intValue());
//					} else {
//    					imagen.setBackgroundResource(imagenesSelec.get(OTHER_ELEMENT_IMAGE_NAME));
//    				}
//    			}
//	    		imagen.setLayoutParams(params2);
//	    		selectedButton = imagen;
//	    		imageFound = true;
//			} else {
//				obj = componentes.get(i);
//				if (obj instanceof Convenio) {
//					currentImage = imagenes.get(((Convenio)obj).getNombreImagen());
//					if (currentImage != null) {
//						imagen.setBackgroundResource(currentImage.intValue());
//					} else {
//    					imagen.setBackgroundResource(imagenes.get(OTHER_ELEMENT_IMAGE_NAME));
//    				}
//    			} else if (obj instanceof Compania) {
//    				currentImage = imagenes.get(((Compania)obj).getNombreImagen());
//    				if (currentImage != null) {
//    					imagen.setBackgroundResource(currentImage.intValue());
//    				} else {
//    					imagen.setBackgroundResource(imagenes.get(OTHER_ELEMENT_IMAGE_NAME));
//    				}
//    				
//    			}
//	    		imagen.setLayoutParams(params2);
//			}
//    		
//    		imagen.setId(i);
//    		imagen.setOnClickListener(this);
//    		arregloBotones.add(imagen);
//    		contenidoComponenteHorizontal.addView(imagen);
//		}
        
        for (int i = 0; i < componentsSize; i++) {
        	imagen = new ImageButton(getContext());
    		if ((itemSeleccionado == componentes.get(i)) ||	(!imageFound && i==componentsSize-1)) {
    			if (itemSeleccionado instanceof Convenio) {
    				currentImage = (Integer)imagenesSelec.get(((Convenio)itemSeleccionado).getNombreImagen());
					if (currentImage != null) {
						imagen.setBackgroundResource(currentImage.intValue());
					} else {
    					imagen.setBackgroundResource(imagenesSelec.get(OTHER_ELEMENT_IMAGE_NAME));
    				}
    			} else if (itemSeleccionado instanceof Compania) {
    				currentImage = imagenesSelec.get(((Compania)itemSeleccionado).getNombreImagen());
    				if (currentImage != null) {
						imagen.setBackgroundResource(currentImage.intValue());
					} else {
    					imagen.setBackgroundResource(imagenesSelec.get(OTHER_ELEMENT_IMAGE_NAME));
    				}
    			}
	    		imagen.setLayoutParams(selectedButtonParams);
	    		selectedButton = imagen;
	    		imageFound = true;
			} else {
				obj = componentes.get(i);
				if (obj instanceof Convenio) {
					currentImage = imagenes.get(((Convenio)obj).getNombreImagen());
					if (currentImage != null) {
						imagen.setBackgroundResource(currentImage.intValue());
					} else {
    					imagen.setBackgroundResource(imagenes.get(OTHER_ELEMENT_IMAGE_NAME));
    				}
    			} else if (obj instanceof Compania) {
    				currentImage = imagenes.get(((Compania)obj).getNombreImagen());
    				if (currentImage != null) {
    					imagen.setBackgroundResource(currentImage.intValue());
    				} else {
    					imagen.setBackgroundResource(imagenes.get(OTHER_ELEMENT_IMAGE_NAME));
    				}
    				
    			}
	    		imagen.setLayoutParams(selectedButtonParams);
			}
    		
    		imagen.setId(i);
    		imagen.setOnClickListener(this);
    		arregloBotones.add(imagen);
    		contenidoComponenteHorizontal.addView(imagen);
		}
	}
	
	public void bloquearComponente() {
		blocked = true;
		componenteHorizontal.setOnTouchListener(this);
		componenteHorizontal.setFocusable(false);
		componenteHorizontal.setClickable(false);
		int arregloBotonesSize = arregloBotones.size();
		ImageButton currentImageButton = null;
		for (int i=0; i<arregloBotonesSize; i++) {
			currentImageButton = arregloBotones.get(i);
			currentImageButton.setOnTouchListener(this);
			currentImageButton.setFocusable(false);
			currentImageButton.setClickable(false);
		}
	}
	
	@Override
	public void onClick(View v) {
		ImageButton selectedButton = null;
		for (int i = 0; i < arregloBotones.size(); i++) {
			selectedButton = arregloBotones.get(i);
			if (v == selectedButton) {
				Object selectedComponent = componentes.get(selectedButton.getId());
				setSelectedItem(selectedComponent);
				if (delegate != null) {
					delegate.performAction(selectedComponent);
				}
			}
			
		}
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		super.onWindowFocusChanged(hasWindowFocus);
		if (hasWindowFocus) {
			if (itemSeleccionado == null) {
				setSelectedItem(componentes.get(0));
			}
			ImageButton selectedButton = arregloBotones.get(componentes.indexOf(itemSeleccionado));	
			Rect outRect = new Rect();
			float rightOffset = SuiteApp.appContext.getResources().getDimensionPixelOffset(R.dimen.seleccionHorizontal_side_margin_no_selected);
			contenidoComponenteHorizontal.getLocalVisibleRect(outRect);
			int scroller = selectedButton.getRight() - outRect.right + (int)rightOffset;		
	        componenteHorizontal.scrollBy(scroller, outRect.top);
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		return blocked;
	}
}
