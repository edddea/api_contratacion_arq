package suitebancomer.classes.gui.controllers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.mbanking.R;
import com.bancomer.mbanking.SuiteApp;
import com.bancomer.mbanking.administracion.SuiteAppAdmonApi;
import com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui.InitContactanos;
import com.bbva.apicuentadigital.common.APICuentaDigital;

import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import mtto.suitebancomer.aplicaciones.bmovil.classes.entrada.InitMantenimiento;
import suitebancomer.aplicaciones.bmovil.classes.common.BmovilConstants;
import suitebancomer.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomer.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.aplicaciones.bmovil.classes.common.SincronizarSesion;
import suitebancomer.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa.ConstanstMenuOpcionesHamburguesa;
import suitebancomer.aplicaciones.bmovil.classes.delegates.hamburguesa.SelectPhotoPerfilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.entrada.InitializeApiAdmon;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.IndicadorHamburguesa;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.InitHamburguesa;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.MenuHamburguesaViewsControllers;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.entradas.InitReactivacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.CambioPerfilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.io.Server;
import suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenSession;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.ContratacionSofttokenViewController;
import suitebancomer.classes.common.GuiTools;
import suitebancomer.classes.gui.delegates.MenuSuiteDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.model.BanderasServer;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.LoginData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Reactivacion;
//import tracking.TrackingHelper;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class MenuSuiteViewController extends BaseViewController implements View.OnClickListener, AnimationListener,CallBackBConnect {



	/**
	 * The layout where the options and the login view are supposed to appear
	 */
	private FrameLayout baseLayout;

	/**
	 * The layout containing the suite options
	 */
	//private RelativeLayout menuOptionsLayout;
	private LinearLayout menuOptionsLayout;

	/**
	 * Reference to the first button of the option layout
	 */
	private ImageButton firstMenuOption;

	/**
	 * Reference to the second button of the option layout
	 */
	private ImageButton loginToken;

	//private ImageButton loginInfo;

	private EditText loginput;

	private EditText passinput;

	private ImageButton loginenter;

	private ImageButton contactanosBtn;

	private ImageButton btnHamburguesa;

	private MenuSuiteDelegate delegate;

	private boolean isFlipPerforming;

	private boolean shouldHideLogin;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ

	public boolean comesFromNov = false;

	private ImageButton loginWallet;

	public void setComesFromNov(Boolean b){
		comesFromNov = b;
	}
	public void setIsFlipPerforming(Boolean b){
		isFlipPerforming = b;
	}

	/**
	 * Referencia para el link de Olvidaste constrasenia
	 */
	TextView txtOlvidastePass;
	TextView txtQuieroCuenta;
	/**
	 * Guarda el margen del label
	 */
	int marginBottomLbl = 0;
	/**
	 * Guarda el margen del texto
	 */
	int marginBottomTxt = 0;

	DrawerLayout drawerLayout;
	ListView listaHamburguesa;
	public static View header;

	TextView numeroTrajeta;

	TextView txtContrasena;

	TextView txtVersion;

	/**
	 * Default constructor for this activity
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, 0, R.layout.layout_suite_menu_principal);
		ServerCommons.loadEnviromentConfig(this);
		setServerVariables();
		//SuiteApp.appOrigenPrincipal=Constants.EMPTY_STRING;
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		parentManager.setCurrentActivityApp(this);

		TrackingHelper.trackState("login", parentManager.estados);
		parentViewsController = SuiteApp.getInstance().getSuiteViewsController();
		parentViewsController.setCurrentActivityApp(this);


		delegate = (MenuSuiteDelegate)parentManager.getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuSuiteDelegate();
			parentManager.addDelegateToHashMap(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID, delegate);
		}

//		if( ((MenuSuiteDelegate)parentViewsController.getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID)) == null){
//			parentViewsController.addDelegateToHashMap(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID, delegate);
//		}

		delegate.setMenuSuiteViewController(this);

//		firstMenuOption = (ImageButton)findViewById(R.id.suite_menu_option_first);
//		firstMenuOption.setOnClickListener(this);


		initViews();

		//Modificación 49651 si la aplicación está desactivada -> Botón de novedades no se muestra
		if(!delegate.isBmovilAppActivated()){
			//loginInfo.setVisibility(View.INVISIBLE);
			//findViewById(R.id.LoginPassTxt).setVisibility(View.GONE);
			passinput.setVisibility(View.GONE);
			txtOlvidastePass.setVisibility(View.INVISIBLE);
			txtQuieroCuenta.setVisibility(View.VISIBLE);
			txtContrasena.setVisibility(View.GONE);
			loginput.setVisibility(View.VISIBLE);
			numeroTrajeta.setVisibility(View.GONE);
		}else{
			//loginInfo.setVisibility(View.VISIBLE);
			//findViewById(R.id.LoginPassTxt).setVisibility(View.VISIBLE);
			passinput.setVisibility(View.VISIBLE);
			txtOlvidastePass.setVisibility(View.VISIBLE);
			txtQuieroCuenta.setVisibility(View.INVISIBLE);
			txtContrasena.setVisibility(View.VISIBLE);
			numeroTrajeta.setVisibility(View.VISIBLE);
			loginput.setVisibility(View.GONE);
			//passinput.setInputType(InputType.TYPE_CLASS_PHONE | InputType.TYPE_NUMBER_VARIATION_PASSWORD);
		}

		delegate.bmovilSelected();

		txtVersion.setText(Double.toString(suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.getDoubleAmountFromServerString(Constants.APPLICATION_VERSION)));
		//numeroTrajeta.setText(loginput.getText().toString());
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		listaHamburguesa = (ListView) findViewById(R.id.list_opciones_menu);
		MenuHamburguesaViewsControllers.initValuesRequest(this, ConstanstMenuOpcionesHamburguesa.VALUE_NOMBRE_APP);
		header = getLayoutInflater().inflate(R.layout.header_burger, null);
		listaHamburguesa.addHeaderView(header);

	}

	private void setServerVariables() {
		Server.ALLOW_LOG = ServerCommons.ALLOW_LOG;
		Server.DEVELOPMENT = ServerCommons.DEVELOPMENT;
		Server.SIMULATION = ServerCommons.SIMULATION;
	}

	private void initViews(){
		LinearLayout mainLayout = (LinearLayout)findViewById(R.id.rootLayout);
		//ScrollView mainLayout = (ScrollView)findViewById(R.id.rootLayout);
		LinearLayout suiteMenuLayout = (LinearLayout)findViewById(R.id.suite_menu_base_layout);
		LinearLayout loginTitleLayout = (LinearLayout)findViewById(R.id.loginTitleLayout);
		LinearLayout credentiasLayout = (LinearLayout)findViewById(R.id.LoginCredentialsLayout);
		//LinearLayout titlesLayout = (LinearLayout)findViewById(R.id.LoginCredentialsTitlesLayout);
		LinearLayout inputsLayout = (LinearLayout)findViewById(R.id.LoginCredentialsInputsLayout);
		LinearLayout buttonsLayout = (LinearLayout)findViewById(R.id.LoginButtonsLayout);

		//ImageView loginImage = (ImageView)findViewById(R.id.LoginBancomerImg);

		//loginInfo = (ImageButton)findViewById(R.id.LoginInfoBtn);
		//loginInfo.setOnClickListener(this);
		loginenter = (ImageButton)findViewById(R.id.LoginEnterBtn);
		loginenter.setOnClickListener(this);
		loginWallet = (ImageButton)findViewById(R.id.LoginWalletBtn);
		loginWallet.setOnClickListener(this);
		contactanosBtn = (ImageButton) findViewById(R.id.LineaBancomerBtn);
		contactanosBtn.setOnClickListener(this);
		loginToken = (ImageButton)findViewById(R.id.LoginTokenMovilBtn);
		loginToken.setOnClickListener(this);

		btnHamburguesa = (ImageButton) findViewById(R.id.buttonHamburguesablanco);
		btnHamburguesa.setOnClickListener(this);
		// Referencia para el link Olvidaste contrasenia
		txtOlvidastePass = (TextView) findViewById(R.id.txtOlvidastePass);
		txtOlvidastePass.setOnClickListener(this);

		// Referencia para el link Quiero una cuenta
		txtQuieroCuenta = (TextView) findViewById(R.id.txtQuieroCuenta);
		txtQuieroCuenta.setOnClickListener(this);

		numeroTrajeta = (TextView) findViewById(R.id.txtNum);

		txtContrasena = (TextView) findViewById(R.id.txtOlvidastePas);

		txtVersion = (TextView) findViewById(R.id.txtVersion);

		//TextView subtitle = (TextView)findViewById(R.id.LoginSubTitleTxt);

		//TextView log = (TextView)findViewById(R.id.LoginLogTxt);

		//TextView pass = (TextView)findViewById(R.id.LoginPassTxt);

		loginput = (EditText)findViewById(R.id.LoginLogInput);
		InputFilter[] userFilterArray = new InputFilter[1];
		userFilterArray[0] = new InputFilter.LengthFilter(Constants.TELEPHONE_NUMBER_LENGTH);
		//log.setFilters(userFilterArray);

		passinput = (EditText)findViewById(R.id.LoginPassInput);
		passinput.setTransformationMethod(PasswordTransformationMethod.getInstance());
		InputFilter[] passFilterArray = new InputFilter[1];
		passFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
		passinput.setFilters(passFilterArray);

		Typeface typeFaceBook =Typeface.createFromAsset(getAssets(),"fonts/stag_sans_book.ttf");
		Typeface typeFaceLight =Typeface.createFromAsset(getAssets(),"fonts/stag_sans_light.otf");

		//subtitle.setTypeface(typeFaceLight);
		//log.setTypeface(typeFaceLight);
		//pass.setTypeface(typeFaceLight);
		txtOlvidastePass.setTypeface(typeFaceLight);
		txtQuieroCuenta.setTypeface(typeFaceLight);
		//TextView txtSpace = (TextView) findViewById(R.id.txtSpace);
		//txtSpace.setTypeface(typeFaceLight);
		loginput.setTypeface(typeFaceBook);
		passinput.setTypeface(typeFaceBook);

		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		guiTools.scale(mainLayout);
		guiTools.scale(suiteMenuLayout);
		guiTools.scale(loginTitleLayout);
		guiTools.scale(credentiasLayout);
		//guiTools.scale(titlesLayout);
		//guiTools.scale(inputsLayout);
		//guiTools.scale(buttonsLayout);
		//guiTools.scale(loginImage);
		//guiTools.scale(loginInfo);
		//guiTools.scale(loginenter);
		//guiTools.scale(loginToken);
		//guiTools.scale(loginWallet);
		//guiTools.scale(subtitle,true);
		//guiTools.scale(txtContrasena,true);
		//guiTools.scale(numeroTrajeta,true);
		//guiTools.scale(loginput);
		//guiTools.scale(passinput);
		guiTools.scale(btnHamburguesa);
		//guiTools.scale(txtOlvidastePass);
		//guiTools.scale(txtSpace);


		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		wm.getDefaultDisplay().getMetrics(metrics);

		ScrollView sw = (ScrollView) findViewById(R.id.body_layout);
		LinearLayout ll=(LinearLayout)findViewById(R.id.suite_menu_base_layout);
		Double statusBarHeight = Math.ceil(25 * getResources().getDisplayMetrics().density);
		final int sd=getStatusBarHeight();
		int altura=metrics.heightPixels-statusBarHeight.intValue();
		if(sd!=0 && sd!=statusBarHeight.intValue()){
			altura=metrics.heightPixels-sd;
		}
		sw.getLayoutParams().height=altura;
		ll.getLayoutParams().height=altura;

	}

	public int getStatusBarHeight() {
		int result = 0;
		int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
		if (resourceId > 0) {
			result = getResources().getDimensionPixelSize(resourceId);
		}
		return result;
	}

	@Override
	protected void onResume() {
		super.onResume();
		ServerCommons.loadEnviromentConfig(this);
		setServerVariables();
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();

		parentViewsController = SuiteApp.getInstance().getSuiteViewsController();

		parentViewsController.setCurrentActivityApp(this);
		parentManager.setCurrentActivityApp(this);

		delegate = (MenuSuiteDelegate)parentManager.getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuSuiteDelegate();
			parentManager.addDelegateToHashMap(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID, delegate);
		}
//		if( ((MenuSuiteDelegate)parentViewsController.getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID)) == null){
//			parentViewsController.addDelegateToHashMap(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID, delegate);
//		}
		delegate.setMenuSuiteViewController(this);

//		if (loginViewController != null) {
		reestableceLogin();
//			loginViewController.getDelegate().setParentViewController(this);
////			delegate.startBmovilApp();
//			loginViewController.getParentManager().setCurrentActivityApp(this);
////			if (shouldHideLogin || !SuiteApp.getInstance().getBmovilApplication().isApplicationLogged()) {
////				restableceMenu();
////			}
//		}
//
//		if(null != loginViewController) {
//			loginViewController.setVisibility(View.GONE);
//		}
//		if(null != contratacionSTViewController)
//			contratacionSTViewController.setVisibility(View.GONE);
//		if(null != aplicacionDesactivadaViewController)
//			aplicacionDesactivadaViewController.setVisibility(View.GONE);
//		if(null != menuOptionsLayout)
//			this.menuOptionsLayout.setVisibility(View.VISIBLE);
//		else
//			if(Server.ALLOW_LOG) Log.w("WTF", "");
//
//
//		if(delegate.isbMovilSelected())
		delegate.bmovilSelected();
//
		areButtonsDisabled = false;

		// Acciones de actualizacion
		delegate.cargaTelSeedKeystore();

		//Modificación 49651 si la aplicación está desactivada -> Botón de novedades no se muestra
		if(!delegate.isBmovilAppActivated()){
			//loginInfo.setVisibility(View.INVISIBLE);
			//findViewById(R.id.LoginPassTxt).setVisibility(View.GONE);
			passinput.setVisibility(View.GONE);
			txtOlvidastePass.setVisibility(View.INVISIBLE);
			txtQuieroCuenta.setVisibility(View.VISIBLE);

			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)loginput.getLayoutParams();
			if(params.bottomMargin > 0) {
				marginBottomTxt = params.bottomMargin;
			}
			params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, 0);
			loginput.setLayoutParams(params); //Padding(loginput.getLeft(), loginput.getPaddingTop(), loginput.getPaddingRight(), 0);
			//TextView log = (TextView)findViewById(R.id.LoginLogTxt);

			//LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams)log.getLayoutParams();
			//if(params1.bottomMargin > 0) {
			//	marginBottomLbl = params1.bottomMargin;
			//}
			//params1.setMargins(params1.leftMargin, params1.topMargin, params1.rightMargin, 0);
			//log.setLayoutParams(params1);
		}else{
			//loginInfo.setVisibility(View.VISIBLE);
			//findViewById(R.id.LoginPassTxt).setVisibility(View.VISIBLE);
			passinput.setVisibility(View.VISIBLE);
			txtOlvidastePass.setVisibility(View.VISIBLE);
			txtQuieroCuenta.setVisibility(View.INVISIBLE);
			txtContrasena.setVisibility(View.VISIBLE);
			numeroTrajeta.setVisibility(View.VISIBLE);
			loginput.setVisibility(View.GONE);
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)loginput.getLayoutParams();
			if(params.bottomMargin > 0) {
				marginBottomTxt = params.bottomMargin;
			}
			params.setMargins(params.leftMargin, params.topMargin, params.rightMargin, marginBottomTxt);
			loginput.setLayoutParams(params); //Padding(loginput.getLeft(), loginput.getPaddingTop(), loginput.getPaddingRight(), 0);
			//TextView log = (TextView)findViewById(R.id.LoginLogTxt);
			//LinearLayout.LayoutParams params1 = (LinearLayout.LayoutParams)log.getLayoutParams();
			//if(params1.bottomMargin > 0) {
			//	marginBottomLbl = params1.bottomMargin;
			//}
			//params1.setMargins(params1.leftMargin, params1.topMargin, params1.rightMargin, marginBottomLbl);
			//log.setLayoutParams(params1);
			//passinput.setInputType(InputType.TYPE_CLASS_PHONE);
		}
		MenuHamburguesaViewsControllers.setIsOterApp(Boolean.FALSE);
		showHamburguesa();
	}

	@Override
	protected void onPause() {
		super.onPause();
		hideSoftKeyboard();

		if (SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().consumeAccionesDePausa()) {
			parentViewsController.currentViewControllerApp.hideCurrentDialog();
		}
		parentViewsController.currentViewControllerApp.hideCurrentDialog();
		if(drawerLayout.isDrawerOpen(listaHamburguesa)){
			if (!MenuHamburguesaViewsControllers.getIsOterApp()){
				drawerLayout.closeDrawer(listaHamburguesa);
			}
		}
	}

	/**
	 * Method that listens whenever a view is touched (clicked)
	 */
	@Override
	public void onClick(View v) {

		// Recoger nuevamente BmovilViewsController para cuando se pulsa boton atras, cierrra la sesion y borra los delegates
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
		parentManager.setCurrentActivityApp(this);

		if(areButtonsDisabled)
			return;
		areButtonsDisabled = true;
		if (v == firstMenuOption) {
			if(Server.ALLOW_LOG) Log.e("firstMenuOption","ok");
			delegate.bmovilSelected();
			//mod 50246
			suitebancomer.aplicaciones.bmovil.classes.common.Session.getInstance(this);
			suitebancomer.aplicaciones.bmovil.classes.common.Session.takeViewToken=delegate;
		} else if (v == loginToken) {
			delegate.onBtnContinuarclick(v);
			//} else if (v == loginInfo) {
			//if(Server.ALLOW_LOG) Log.d("[CGI-Debug]", "Entra por Novedades");

			// Recoger nuevamente BmovilViewsController para cuando se pulsa boton atras, cierrra la sesion y borra los delegates
//			parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
//			parentManager.setCurrentActivityApp(this); checar

			//delegate = (MenuSuiteDelegate)parentManager.getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
			//if (delegate == null) {
			//	delegate = new MenuSuiteDelegate();
			//	parentManager.addDelegateToHashMap(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID, delegate);
			//}
			// Dirige a pantalla novedades
			//parentManager.showNovedadesLogin();
//			((SuiteViewsController)parentViewsController).showNovedadesContactanos();
			//SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showNovedadesLogin();
		} else if (v == loginWallet){
			//hcf Boton Vida Bancomer
			Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
			//hcf
			paso1OperacionMap.put("evento_paso1", "event46");
			paso1OperacionMap.put("&&products", "menu suite + vida bancomer");
			paso1OperacionMap.put("eVar12", "paso1:vida bancomer");

			TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
			if(Server.ALLOW_LOG) Log.d("[CGI-Debug]", "Entra por vida");
			delegate.checkWallet();

		} else if(v == loginenter){
			botonEntrar();
		}
		else if(v == btnHamburguesa){
			areButtonsDisabled = false;
			showHamburguesa();
			drawerLayout.openDrawer(listaHamburguesa);
		} else if(v.equals(txtOlvidastePass)) {
			areButtonsDisabled = Boolean.FALSE;
			String user = loginput.getText().toString();
			if(TextUtils.isEmpty(user) || user.contains("*")) {
				user = DatosBmovilFileManager.getCurrent().getLogin();
			}
			if(delegate.validaUserName(user)) {
				DatosBmovilFileManager.getCurrent().setLogin(user);
				final String username = DatosBmovilFileManager.getCurrent().getLogin();
				showOlvidasteConstrasena(username);
			}
		}
		else if (v == contactanosBtn){
			areButtonsDisabled = false;
			final InitContactanos initContactanos= new InitContactanos(this);
			initContactanos.iniciarContactanos();
		}else if(v==txtQuieroCuenta){
			final String celular= "";
			final String companiaTel="";
			final APICuentaDigital getInstance = new APICuentaDigital();
			getInstance.ejecutaAPICuentaDigital(this,celular, companiaTel, suitebancomercoms.aplicaciones.bmovil.classes.io.Server.DEVELOPMENT, suitebancomercoms.aplicaciones.bmovil.classes.io.Server.SIMULATION, suitebancomercoms.aplicaciones.bmovil.classes.io.Server.EMULATOR, suitebancomercoms.aplicaciones.bmovil.classes.io.Server.ALLOW_LOG);

		}
	}

	public void setButtonsDisabled(boolean value) {
		this.areButtonsDisabled = value;
	}

	public void plegarOpcion() {
//		if (isFlipPerforming) {
//			return;
//		}
//		isFlipPerforming = true;
//		FlipAnimation flipAnimation = new FlipAnimation(menuOptionsLayout, loginViewController);
//		flipAnimation.setAnimationListener(this);
//	    if (menuOptionsLayout.getVisibility() == View.GONE) {
//	        flipAnimation.reverse();
//	    }
//	    baseLayout.startAnimation(flipAnimation);
	}

	private void llamarLineaBancomer() {
		delegate.llamarLineaBancomer();
	}

//	public FrameLayout getBaseLayout() {
//		return baseLayout;
//	}

	public boolean getShouldHideLogin() {
		return shouldHideLogin;
	}

	public void setShouldHideLogin(boolean shouldHideLogin) {
		this.shouldHideLogin = shouldHideLogin;
	}




	/**
	 * Stores the session information after a login operation. If registering
	 * is taking place, it show the activation screen instead.
	 *
	 * @param response The server response to the login operation
	 */
	public void processNetworkResponse(int operationId, ServerResponse response) {

		if(Server.CONSULTA_MANTENIMIENTO == operationId) {
			delegate.analyzeResponse(operationId, response);
		} else if(operationId == Server.LOGIN_OPERATION) {
//			LoginDelegate loginDelegate = (LoginDelegate)((SuiteApp)getApplication()).getBmovilApplication().getBmovilViewsController().getBaseDelegateForKey(LoginDelegate.LOGIN_DELEGATE_ID);
//			if (loginDelegate == null) {
//				loginDelegate = new LoginDelegate();
//
//				loginDelegate.setParentViewController(this);
//				loginViewController.setDelegate(loginDelegate);
//				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().addDelegateToHashMap(LoginDelegate.LOGIN_DELEGATE_ID, loginDelegate);
//			}
//			loginDelegate.setLoginViewController(loginViewController);
			delegate.analyzeResponse(operationId, response);
		} else if(operationId == Server.CAMBIA_PERFIL) {
			/* TODO AMB Llamr el cambio de perfil de Administracion
			BmovilViewsController viewsController = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();

			CambioPerfilDelegate cpDelegate = (CambioPerfilDelegate)viewsController.getBaseDelegateForKey(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);
			if(null == cpDelegate)
				cpDelegate = new CambioPerfilDelegate();
			else
				viewsController.removeDelegateFromHashMap(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);

			cpDelegate.setChangeAdvancedToBasic(true);
			cpDelegate.analyzeResponse(operationId, response);
			*/
			SuiteAppAdmonApi suiteAppAdmonApi = new SuiteAppAdmonApi();
			suiteAppAdmonApi.onCreate(SuiteApp.appContext);

			InitializeApiAdmon.getInstance(this, new suitebancomer.aplicaciones.bmovil.classes.model.administracion.BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT)).initializeDependenceApis();
			final suitebancomer.classes.gui.controllers.administracion.BaseViewsController baseViewsController = com.bancomer.mbanking.administracion.SuiteAppAdmonApi
					.getInstance().getBmovilApplication()
					.getBmovilViewsController();


			CambioPerfilDelegate cambioPerfilDelegate =(CambioPerfilDelegate) baseViewsController.getBaseDelegateForKey(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);
			if(null == cambioPerfilDelegate){
				cambioPerfilDelegate = new CambioPerfilDelegate();
			}else {
				baseViewsController.removeDelegateFromHashMap(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);
			}

			suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse server = new
					suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse(response.getStatus(), response.getMessageCode(),response.getMessageText(),response.getResponse());

			cambioPerfilDelegate.setChangeAdvancedToBasic(true);
			cambioPerfilDelegate.analyzeResponse(operationId, server);

		}else if (operationId == Server.ACTIVATION_STEP1_OPERATION) {
			BmovilViewsController bmovilViewsController = SuiteApp
					.getInstance().getBmovilApplication()
					.getBmovilViewsController();
			bmovilViewsController.showPantallaActivacion();
		}else if (operationId == Server.ACTIVATION_STEP2_OPERATION) {
			SuiteViewsController suiteViewsController = SuiteApp.getInstance()
					.getSuiteViewsController();
			if (suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController) {
				MenuSuiteViewController suiteViewController = (MenuSuiteViewController) suiteViewsController
						.getCurrentViewControllerApp();
				suiteViewController.restableceMenu();
			}
			//storeSessionAfterActivation();
		}
	}

	public void restableceMenu() {
//    	if (loginViewController != null) {
//			loginViewController.setVisibility(View.GONE);
//		}
//    	if(contratacionSTViewController != null) {
//    		contratacionSTViewController.setVisibility(View.GONE);
//    		baseLayout.removeView(contratacionSTViewController);
//    		contratacionSTViewController = null;
//    	}
//    	if(aplicacionDesactivadaViewController != null) {
//    		aplicacionDesactivadaViewController.setVisibility(View.GONE);
//    		baseLayout.removeView(aplicacionDesactivadaViewController);
//    		aplicacionDesactivadaViewController = null;
//    	}
//    	menuOptionsLayout.setVisibility(View.VISIBLE);

	}

	@Override
	public void goBack() {
		if(drawerLayout.isDrawerOpen(listaHamburguesa)){
			drawerLayout.closeDrawer(listaHamburguesa);
		}
		else {
			delegate.onBackPressed();
		}
	}


	@Override
	public void onAnimationEnd(Animation animation) {
//		FlipAnimation flipAnimation = (FlipAnimation)animation;
//		if (!flipAnimation.isForward()) {
//			if(null != loginViewController) {
//				baseLayout.removeView(loginViewController);
//				setLoginViewController(null);
//			}
//			if(null != contratacionSTViewController) {
//				baseLayout.removeView(contratacionSTViewController);
//				setLoginViewController(null);
//			}
//			if(null != aplicacionDesactivadaViewController) {
//				baseLayout.removeView(aplicacionDesactivadaViewController);
//				setLoginViewController(null);
//			}
//		}
//		isFlipPerforming = false;
//		areButtonsDisabled = false;
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		isFlipPerforming = true;
	}

	@Override
	public void onAnimationStart(Animation animation) {
		isFlipPerforming = true;
	}

	// #region Softtoken
/*	private void softtokenSelected() {
		delegate.softtokenSelected();
	}*/

	private ContratacionSofttokenViewController contratacionSTViewController;

	public void setContratacionSTViewController(ContratacionSofttokenViewController contratacionSTViewController) {
		this.contratacionSTViewController = contratacionSTViewController;
	}

	public ContratacionSofttokenViewController getContratacionSTViewController() {
		return contratacionSTViewController;
	}

	public void plegarOpcionST() {
//		if (isFlipPerforming) {
//			return;
//		}
//		isFlipPerforming = true;
//		FlipAnimation flipAnimation = new FlipAnimation(menuOptionsLayout, contratacionSTViewController);
//		flipAnimation.setAnimationListener(this);
//	    if (menuOptionsLayout.getVisibility() == View.GONE) {
//	        flipAnimation.reverse();
//	    }
//	    baseLayout.startAnimation(flipAnimation);
	}
	// #endregion

	public void plegarOpcionAplicacionDesactivada() {
//		if (isFlipPerforming) {
//			return;
//		}
//		isFlipPerforming = true;
//		FlipAnimation flipAnimation = new FlipAnimation(menuOptionsLayout, aplicacionDesactivadaViewController);
//		flipAnimation.setAnimationListener(this);
//	    if (menuOptionsLayout.getVisibility() == View.GONE) {
//	        flipAnimation.reverse();
//	    }
//	    baseLayout.startAnimation(flipAnimation);
	}
	// #endregion

	private boolean areButtonsDisabled = false;

	public void establecerUsuario(){
		delegate.establecerUsuario(loginput);
		delegate.establecerUsuario(numeroTrajeta);
	}

	public void limpiarCampoContrasena() {
		passinput.setText("");
		passinput.invalidate();
	}

	public void reestableceLogin() {
		if (delegate != null) {
			delegate.establecerUsuario(loginput);
			delegate.establecerUsuario(numeroTrajeta);
			limpiarCampoContrasena();
		}
	}

	private void botonEntrar() {
		String user = loginput.getText().toString();
		String pass = passinput.getText().toString();
		delegate.botonEntrar(user, pass);
	}

	public void configurarVistaLogin() {
		//findViewById(R.id.LoginPassTxt).setVisibility(View.VISIBLE);
		passinput.setVisibility(View.VISIBLE);
		//passinput.setInputType(InputType.TYPE_CLASS_PHONE);
		findViewById(R.id.LoginButtonsLayout).setVisibility(View.VISIBLE);
	}

	public void configurarVistaAplicacionDesactivada(String telefono) {
		loginput.setText(telefono);
		//numeroTrajeta.setText(telefono);
		findViewById(R.id.LoginButtonsLayout).setVisibility(View.VISIBLE);
//		if(telefono.equalsIgnoreCase("")){
		//	findViewById(R.id.LoginPassTxt).setVisibility(View.GONE);
		passinput.setVisibility(View.GONE);
		txtOlvidastePass.setVisibility(View.VISIBLE);
		txtContrasena.setVisibility(View.GONE);
		numeroTrajeta.setVisibility(View.GONE);
		loginput.setVisibility(View.VISIBLE);
//		}else{
//			findViewById(R.id.LoginPassTxt).setVisibility(View.GONE);
//			passinput.setVisibility(View.GONE);
//			findViewById(R.id.LoginButtonsLayout).setVisibility(View.GONE);
//		}
	}

	//_____________uso del api sofftoken______________//
	@Override
	public void returnToPrincipal() {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showViewController(MenuSuiteViewController.class);
	}

	@Override
	public void returnObjectFromApi(Object response) {
			/*
			El siguiente fragmento de codigo solo es un caso puntual para bmovil ya que al cargar el
			 archivo en session y no lo lee nuevamente al hacer la peticion de una propiedad
			 se tiene que setear la propiedad en la session ya que solo ponerlo en el archivo no funciona
			 */

		Session.getInstance(this).reloadFiles();

		if(response==null) {
			SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		}else{

			SofttokenSession session = SofttokenSession.getCurrent();
			Session sessionApp = Session.getInstance(this);
			session.setSofttokenActivated(false);
			//SuiteApp.getInstance().getSofttokenApplication().resetApplicationData();
		}

		/*este fracmento si es general*/
		returnToPrincipal();
	}

	@Override
	public void showReactivacion(Reactivacion reactivacion) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		InitReactivacion initReactivacion = new InitReactivacion(new BanderasServer(Server.SIMULATION,
				Server.ALLOW_LOG, Server.EMULATOR, Server.DEVELOPMENT),
				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp(),
				this,
				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp());
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp().ocultaIndicadorActividad();
		initReactivacion.showReactivacion(reactivacion,
				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().estados);

	}

	@Override
	public void showReactivacion(ConsultaEstatus consultaEstatus, String password) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		InitReactivacion initReactivacion = new InitReactivacion(new BanderasServer(Server.SIMULATION,
				Server.ALLOW_LOG, Server.EMULATOR, Server.DEVELOPMENT),
				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp(),
				this,
				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp());
		initReactivacion.showReactivacion(consultaEstatus, password,
				SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().estados);

	}

	public void showHamburguesa()
	{
		if(MenuHamburguesaViewsControllers.getDatosBmovil().getCatalogoHamburguesa() != null)
		{
			MenuHamburguesaViewsControllers.iniciaHamburguesa(this,ConstanstMenuOpcionesHamburguesa.SIN_SESSION,ConstanstMenuOpcionesHamburguesa.VALUE_NOMBRE_APP);
			btnHamburguesa.setVisibility(View.VISIBLE);
			MenuHamburguesaViewsControllers.bloquearDrawer(drawerLayout, false);
			listaHamburguesa = MenuHamburguesaViewsControllers.getListaOpciones();
			listaHamburguesa.setOnItemClickListener(new AdapterView.OnItemClickListener(){
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					if(position != 0)
					{
						position -= listaHamburguesa.getHeaderViewsCount();
						final String optionSelected = MenuHamburguesaViewsControllers.getElementosMenu().get(position).getNombreOpcion();
						MenuHamburguesaViewsControllers.clickOpcionesHamburguesa(optionSelected, MenuSuiteViewController.this, position);
					}
				}
			});
		}else
		{
			MenuHamburguesaViewsControllers.bloquearDrawer(drawerLayout, true);
			btnHamburguesa.setVisibility(View.INVISIBLE);
		}
	}

	public void showOlvidasteConstrasena(final String numeroTelefono) {
		SincronizarSesion.getInstance().SessionToSessionApi(SuiteApp.appContext);
		InitMantenimiento initMantenimiento = new InitMantenimiento(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp(),
				this, this,
				new mtto.suitebancomer.aplicaciones.bmovil.classes.model.BanderasServer(Server.SIMULATION, Server.ALLOW_LOG, Server.EMULATOR, Server.DEVELOPMENT));
		initMantenimiento.startOlvidastePassword(numeroTelefono, this);
	}

	@Override
	public void returnToActivity(Context context, Activity activity, boolean cleanSession) {
		/*especial para bmovil*/
		if(cleanSession){
			Session.getInstance(this).reloadFiles();
		}
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		/* general */
		Intent launchActivity=new Intent(context,activity.getClass());
		context.startActivity(launchActivity);
	}

	@Override
	public void returnLogin(String numeroCelular, String password) {
		Session.getInstance(SuiteApp.appContext).reloadFiles();
		suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(SuiteApp.appContext).setPassword(password);
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		//delegate = new MenuSuiteDelegate();
		//delegate.setMenuSuiteViewController(this);
		delegate.login(numeroCelular, password);
		//SuiteApp.getInstance().getSuiteViewsController().showMenuSuite(true);
	}

	@Override
	public void returnMenuPrincipal() {
		Session.getInstance(this).reloadFiles();
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(Boolean.TRUE);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal();
	}

	@Override
	public void returDesactivada() {
		Session.getInstance(this).reloadFiles();
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
	}

	@Override
	public void userInteraction() {
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().onUserInteraction();
	}

	@Override
	public void cierraSesionBackground(Boolean isChanging) {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setActivityChanging(Boolean.FALSE);
		if (parentManager.consumeAccionesDePausa()) {
			finish();
			if(MenuHamburguesaViewsControllers.getIsOterApp()){
				MenuHamburguesaViewsControllers.setIsOterApp(Boolean.FALSE);
			}
			return;
		}
		SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(this);
	}

	@Override
	public void cierraSesion() {
		SincronizarSesion.getInstance().SessionApiToSession(SuiteApp.appContext);
		SuiteApp.getInstance().getBmovilApplication()
				.setApplicationInBackground(Boolean.FALSE);
		SuiteApp.getInstance().getBmovilApplication().logoutApp(Boolean.FALSE);
	}

	@Override
	public void returnLoginApi(LoginData loginData, ConsultaEstatus consultaEstatus,String responsePlain) {
		if(loginData!=null)//login exitoso y puede continuar su flujo
			delegate.reciveRespuestaLoginApi( loginData,  consultaEstatus,responsePlain);

	}

	/*
			En este metodo se reciben el parametro de ConsultaEstatus
			resultado de el api aplicacionDesactivada en el caso que se envie la bandera de
			continuaFlujoBconnect como falso al api.

			la aplicacion base los debera procesar segun su logica de negocio de la app base.
		*/
	@Override
	public void returnDesactivadaApi(ConsultaEstatus consultaEstatus) {
		if(Server.ALLOW_LOG)
			Log.d("returnDesactivadaApi",consultaEstatus.toString());

	}

	/**
	 * el metod es invocado por el api de login cuendo el usurio introudce la
	 * contraseña incorrecta, la logica que aplique este metodo dependera
	 * de cada aplicacion segun sus RN.
	 * en este caso solo se limpia el campo contraseña
	 */
	@Override
	public void limpiarPasswordLogin() {
		limpiarCampoContrasena();
	}

	public static View getHeader() {
		return header;
	}

}