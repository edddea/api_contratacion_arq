/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.bancomer.mbanking.apipush;

public final class R {
	public static final class attr {
		public static final int circleCrop = 0x7f010070;
		public static final int imageAspectRatio = 0x7f01006f;
		public static final int imageAspectRatioAdjust = 0x7f01006e;
	}
	public static final class bool {
		public static final int isAtLeastKitKat = 0x7f08000b;
	}
	public static final class color {
		public static final int amarillo = 0x7f0d0003;
		public static final int anaranjado = 0x7f0d0004;
		public static final int azul_fondo = 0x7f0d0007;
		public static final int blanco = 0x7f0d000b;
		public static final int common_action_bar_splitter = 0x7f0d0013;
		public static final int common_signin_btn_dark_text_default = 0x7f0d0014;
		public static final int common_signin_btn_dark_text_disabled = 0x7f0d0015;
		public static final int common_signin_btn_dark_text_focused = 0x7f0d0016;
		public static final int common_signin_btn_dark_text_pressed = 0x7f0d0017;
		public static final int common_signin_btn_default_background = 0x7f0d0018;
		public static final int common_signin_btn_light_text_default = 0x7f0d0019;
		public static final int common_signin_btn_light_text_disabled = 0x7f0d001a;
		public static final int common_signin_btn_light_text_focused = 0x7f0d001b;
		public static final int common_signin_btn_light_text_pressed = 0x7f0d001c;
		public static final int common_signin_btn_text_dark = 0x7f0d0074;
		public static final int common_signin_btn_text_light = 0x7f0d0075;
		public static final int cuarto_azul = 0x7f0d001d;
		public static final int cuarto_rojo = 0x7f0d001e;
		public static final int fondo_autenticacion = 0x7f0d0028;
		public static final int fucsia = 0x7f0d002a;
		public static final int gris_fondo = 0x7f0d0032;
		public static final int gris_texto_1 = 0x7f0d0034;
		public static final int gris_texto_2 = 0x7f0d0035;
		public static final int gris_texto_3 = 0x7f0d0036;
		public static final int gris_texto_4 = 0x7f0d0037;
		public static final int magenta = 0x7f0d0038;
		public static final int naranja = 0x7f0d0039;
		public static final int naranja2 = 0x7f0d003a;
		public static final int naranja_tenue = 0x7f0d003b;
		public static final int negro_bmovil_detalle = 0x7f0d003c;
		public static final int paperles_azul = 0x7f0d003e;
		public static final int paperles_gris = 0x7f0d003f;
		public static final int paperles_transparente = 0x7f0d0040;
		public static final int paperles_verde = 0x7f0d0041;
		public static final int possible_result_points = 0x7f0d0043;
		public static final int primer_azul = 0x7f0d0044;
		public static final int primer_rojo = 0x7f0d0046;
		public static final int progreso_fondo = 0x7f0d0047;
		public static final int quinto_azul = 0x7f0d004b;
		public static final int quinto_rojo = 0x7f0d004c;
		public static final int result_image_border = 0x7f0d004d;
		public static final int result_minor_text = 0x7f0d004e;
		public static final int result_points = 0x7f0d004f;
		public static final int result_text = 0x7f0d0050;
		public static final int result_view = 0x7f0d0051;
		public static final int segundo_azul = 0x7f0d0053;
		public static final int segundo_rojo = 0x7f0d0055;
		public static final int sexto_azul = 0x7f0d0057;
		public static final int sexto_rojo = 0x7f0d0058;
		public static final int status_text = 0x7f0d0059;
		public static final int tercer_azul = 0x7f0d005a;
		public static final int tercer_rojo = 0x7f0d005b;
		public static final int trans = 0x7f0d005c;
		public static final int turquesa = 0x7f0d005d;
		public static final int turquesa2 = 0x7f0d005e;
		public static final int turquesa3 = 0x7f0d005f;
		public static final int verde = 0x7f0d0060;
		public static final int verde2 = 0x7f0d0061;
		public static final int verde_bmovil_detalle = 0x7f0d0062;
		public static final int verde_limon = 0x7f0d0063;
		public static final int verde_st_activacion = 0x7f0d0064;
		public static final int viewfinder_frame = 0x7f0d0065;
		public static final int viewfinder_laser = 0x7f0d0066;
		public static final int viewfinder_mask = 0x7f0d0067;
	}
	public static final class dimen {
		public static final int activity_horizontal_margin = 0x7f09000e;
		public static final int activity_vertical_margin = 0x7f090019;
	}
	public static final class drawable {
		public static final int abonos = 0x7f020057;
		public static final int api_alertas_digitales_header_image = 0x7f020106;
		public static final int api_alertas_digitales_list_border = 0x7f02010b;
		public static final int api_alertas_digitales_list_border_bottom_cell = 0x7f02010c;
		public static final int cargos = 0x7f02023f;
		public static final int common_full_open_on_phone = 0x7f020243;
		public static final int common_ic_googleplayservices = 0x7f020244;
		public static final int common_signin_btn_icon_dark = 0x7f020245;
		public static final int common_signin_btn_icon_disabled_dark = 0x7f020246;
		public static final int common_signin_btn_icon_disabled_focus_dark = 0x7f020247;
		public static final int common_signin_btn_icon_disabled_focus_light = 0x7f020248;
		public static final int common_signin_btn_icon_disabled_light = 0x7f020249;
		public static final int common_signin_btn_icon_focus_dark = 0x7f02024a;
		public static final int common_signin_btn_icon_focus_light = 0x7f02024b;
		public static final int common_signin_btn_icon_light = 0x7f02024c;
		public static final int common_signin_btn_icon_normal_dark = 0x7f02024d;
		public static final int common_signin_btn_icon_normal_light = 0x7f02024e;
		public static final int common_signin_btn_icon_pressed_dark = 0x7f02024f;
		public static final int common_signin_btn_icon_pressed_light = 0x7f020250;
		public static final int common_signin_btn_text_dark = 0x7f020251;
		public static final int common_signin_btn_text_disabled_dark = 0x7f020252;
		public static final int common_signin_btn_text_disabled_focus_dark = 0x7f020253;
		public static final int common_signin_btn_text_disabled_focus_light = 0x7f020254;
		public static final int common_signin_btn_text_disabled_light = 0x7f020255;
		public static final int common_signin_btn_text_focus_dark = 0x7f020256;
		public static final int common_signin_btn_text_focus_light = 0x7f020257;
		public static final int common_signin_btn_text_light = 0x7f020258;
		public static final int common_signin_btn_text_normal_dark = 0x7f020259;
		public static final int common_signin_btn_text_normal_light = 0x7f02025a;
		public static final int common_signin_btn_text_pressed_dark = 0x7f02025b;
		public static final int common_signin_btn_text_pressed_light = 0x7f02025c;
		public static final int ic_launcher = 0x7f0202b8;
		public static final int ic_notificaciones = 0x7f0202bf;
		public static final int ic_plusone_medium_off_client = 0x7f0202c8;
		public static final int ic_plusone_small_off_client = 0x7f0202c9;
		public static final int ic_plusone_standard_off_client = 0x7f0202ca;
		public static final int ic_plusone_tall_off_client = 0x7f0202cb;
		public static final int ic_stat_ic_notification = 0x7f0202d5;
		public static final int logo_bancomer = 0x7f020368;
		public static final int notificaciones = 0x7f02037b;
		public static final int todos = 0x7f0203ac;
	}
	public static final class id {
		public static final int abonosIcon = 0x7f0e0104;
		public static final int abonosPush = 0x7f0e0103;
		public static final int adjust_height = 0x7f0e001b;
		public static final int adjust_width = 0x7f0e001c;
		public static final int cargosIcon = 0x7f0e0102;
		public static final int cargosPush = 0x7f0e0101;
		public static final int empty = 0x7f0e0106;
		public static final int fechaPush = 0x7f0e078d;
		public static final int horaPush = 0x7f0e078e;
		public static final int imagenNotificacion = 0x7f0e078f;
		public static final int listaNotificaciones = 0x7f0e0105;
		public static final int menu_notifications_layout = 0x7f0e00fe;
		public static final int none = 0x7f0e001a;
		public static final int notificaion = 0x7f0e078c;
		public static final int textoNotificacion = 0x7f0e0790;
		public static final int todosIcon = 0x7f0e0100;
		public static final int todosPush = 0x7f0e00ff;
	}
	public static final class integer {
		public static final int google_play_services_version = 0x7f0b0008;
	}
	public static final class layout {
		public static final int activity_main = 0x7f040032;
		public static final int notifications_adapter = 0x7f040203;
		public static final int notifications_custom_adapter = 0x7f040204;
	}
	public static final class mipmap {
		public static final int an_ic_abonos = 0x7f030000;
		public static final int an_ic_abonosoff = 0x7f030001;
		public static final int an_ic_aviso = 0x7f030002;
		public static final int an_ic_avisooff = 0x7f030003;
		public static final int an_ic_cargos = 0x7f030004;
		public static final int an_ic_cargosoff = 0x7f030005;
		public static final int an_ic_noti = 0x7f030006;
		public static final int an_ic_todos = 0x7f030007;
		public static final int an_ic_todosoff = 0x7f030008;
		public static final int header_image = 0x7f030010;
		public static final int ic_stat_ic_notification = 0x7f030014;
		public static final int ic_stat_ic_notification_bak = 0x7f030015;
		public static final int title_divider = 0x7f030019;
	}
	public static final class raw {
		public static final int confenccom = 0x7f060002;
		public static final int tono_bancomer = 0x7f060004;
	}
	public static final class string {
		public static final int accounttype_check = 0x7f070053;
		public static final int accounttype_clabe = 0x7f070054;
		public static final int accounttype_credit = 0x7f070055;
		public static final int accounttype_express = 0x7f070056;
		public static final int accounttype_libreton = 0x7f070057;
		public static final int accounttype_patrimonial = 0x7f070058;
		public static final int accounttype_prepaid = 0x7f070059;
		public static final int accounttype_savings = 0x7f07005a;
		public static final int action_settings = 0x7f07005e;
		public static final int app_label = 0x7f070100;
		public static final int app_name = 0x7f070101;
		public static final int auth_google_play_services_client_facebook_display_name = 0x7f070106;
		public static final int auth_google_play_services_client_google_display_name = 0x7f070107;
		public static final int bmovil_rapidos_dinero_movil = 0x7f0702d5;
		public static final int bmovil_rapidos_tiempo_aire = 0x7f0702d6;
		public static final int bmovil_rapidos_traspaso = 0x7f0702d7;
		public static final int calendar_availableHours = 0x7f070382;
		public static final int calendar_availableHoursIB = 0x7f070383;
		public static final int calendar_availableHoursTC = 0x7f070384;
		public static final int calendar_noAvailable = 0x7f070387;
		public static final int common_android_wear_notification_needs_update_text = 0x7f07000d;
		public static final int common_android_wear_update_text = 0x7f07000e;
		public static final int common_android_wear_update_title = 0x7f07000f;
		public static final int common_google_play_services_api_unavailable_text = 0x7f070010;
		public static final int common_google_play_services_enable_button = 0x7f070011;
		public static final int common_google_play_services_enable_text = 0x7f070012;
		public static final int common_google_play_services_enable_title = 0x7f070013;
		public static final int common_google_play_services_error_notification_requested_by_msg = 0x7f070014;
		public static final int common_google_play_services_install_button = 0x7f070015;
		public static final int common_google_play_services_install_text_phone = 0x7f070016;
		public static final int common_google_play_services_install_text_tablet = 0x7f070017;
		public static final int common_google_play_services_install_title = 0x7f070018;
		public static final int common_google_play_services_invalid_account_text = 0x7f070019;
		public static final int common_google_play_services_invalid_account_title = 0x7f07001a;
		public static final int common_google_play_services_needs_enabling_title = 0x7f07001b;
		public static final int common_google_play_services_network_error_text = 0x7f07001c;
		public static final int common_google_play_services_network_error_title = 0x7f07001d;
		public static final int common_google_play_services_notification_needs_update_title = 0x7f07001e;
		public static final int common_google_play_services_notification_ticker = 0x7f07001f;
		public static final int common_google_play_services_sign_in_failed_text = 0x7f070020;
		public static final int common_google_play_services_sign_in_failed_title = 0x7f070021;
		public static final int common_google_play_services_unknown_issue = 0x7f070022;
		public static final int common_google_play_services_unsupported_text = 0x7f070023;
		public static final int common_google_play_services_unsupported_title = 0x7f070024;
		public static final int common_google_play_services_update_button = 0x7f070025;
		public static final int common_google_play_services_update_text = 0x7f070026;
		public static final int common_google_play_services_update_title = 0x7f070027;
		public static final int common_google_play_services_updating_text = 0x7f070028;
		public static final int common_google_play_services_updating_title = 0x7f070029;
		public static final int common_open_on_phone = 0x7f07002a;
		public static final int common_signin_button_text = 0x7f07002b;
		public static final int common_signin_button_text_long = 0x7f07002c;
		public static final int gcm_defaultSenderId = 0x7f070520;
		public static final int hello_world = 0x7f070522;
		public static final int info_software = 0x7f070536;
		public static final int info_software_detail = 0x7f070537;
		public static final int nonValidOneMessage = 0x7f070627;
		public static final int nonValidTwoMessage = 0x7f070628;
		public static final int sin_datos = 0x7f07077a;
		public static final int spei_phone_string = 0x7f0707ec;
		public static final int title_activity_main = 0x7f070824;
	}
	public static final class style {
		public static final int AppBaseTheme = 0x7f0a0041;
		public static final int AppTheme = 0x7f0a0042;
		public static final int mis_cuentas_celda_style = 0x7f0a0104;
	}
	public static final class styleable {
		public static final int[] LoadingImageView = { 0x7f01006e, 0x7f01006f, 0x7f010070 };
		public static final int LoadingImageView_circleCrop = 2;
		public static final int LoadingImageView_imageAspectRatio = 1;
		public static final int LoadingImageView_imageAspectRatioAdjust = 0;
	}
}
