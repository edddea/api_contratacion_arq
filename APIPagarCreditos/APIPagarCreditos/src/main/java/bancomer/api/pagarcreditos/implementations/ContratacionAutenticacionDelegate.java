package bancomer.api.pagarcreditos.implementations;

import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.models.ConsultaTerminosDeUsoData;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;


public class ContratacionAutenticacionDelegate extends DelegateBaseAutenticacion {

    public final static long CONTRATACION_AUTENTICACION_DELEGATE_ID = 0x9d2a3eaf49317e46L;
    //private String textoInstrumentoSeguridad;
    //AMZ
    public boolean res = false;
    //private ArrayList<String> datosLista;
    private DelegateBaseAutenticacion operationDelegate;
    private boolean debePedirContrasena;
    private boolean debePedirNip;
    private TipoOtpAutenticacion tokenAMostrar;
    private boolean debePedirCVV;
    private TipoInstrumento tipoInstrumentoSeguridad;
    private ContratacionAutenticacionViewController contratacionAutenticacionViewController;

    private boolean debePedirTarjeta;

    public ContratacionAutenticacionDelegate(DelegateBaseAutenticacion delegateBaseAutenticacion) {
        this.operationDelegate = delegateBaseAutenticacion;
        debePedirContrasena = operationDelegate.mostrarContrasenia();
        debePedirNip = operationDelegate.mostrarNIP();
        debePedirCVV = operationDelegate.mostrarCVV();
        tokenAMostrar = operationDelegate.tokenAMostrar();
        //debePedirTarjeta = mostrarCampoTarjeta();
        debePedirTarjeta = operationDelegate.mostrarCampoTarjeta();
        String instrumento = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getSecurityInstrument();
        if (instrumento.equals(Constants.IS_TYPE_DP270)) {
            tipoInstrumentoSeguridad = TipoInstrumento.DP270;
        } else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
            tipoInstrumentoSeguridad = TipoInstrumento.OCRA;
        } else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)) {
            tipoInstrumentoSeguridad = TipoInstrumento.SoftToken;
        } else {
            tipoInstrumentoSeguridad = TipoInstrumento.sinInstrumento;
        }

        //textoInstrumentoSeguridad = operationDelegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumentoSeguridad);
    }

    public void setcontratacionAutenticacionViewController(ContratacionAutenticacionViewController contratacionAutenticacionViewController) {
        this.contratacionAutenticacionViewController = contratacionAutenticacionViewController;
    }

    public void consultaDatosLista() {
        contratacionAutenticacionViewController.setListaDatos(operationDelegate.getDatosTablaConfirmacion());
    }

    public DelegateBaseAutenticacion consultaOperationsDelegate() {
        return operationDelegate;
    }

    public boolean consultaDebePedirContrasena() {
        return debePedirContrasena;
    }

    public boolean consultaDebePedirNIP() {
        return debePedirNip;
    }

    public boolean consultaDebePedirCVV() {
        return debePedirCVV;
    }

    public TipoInstrumento consultaTipoInstrumentoSeguridad() {
        return tipoInstrumentoSeguridad;
    }

    public TipoOtpAutenticacion consultaInstrumentoSeguridad() {
        return tokenAMostrar;
    }

    public void enviaPeticionOperacion() {
        String contrasena = null;
        String nip = null;
        String asm = null;
        String cvv = null;
        boolean terminos = this.contratacionAutenticacionViewController.getTerminosAceptados();

        if (debePedirContrasena) {
            contrasena = contratacionAutenticacionViewController.pideContrasena();
            if (contrasena.equals("")) {
                String mensaje = contratacionAutenticacionViewController.getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                mensaje += contratacionAutenticacionViewController.getString(R.string.confirmation_componenteContrasena);
                mensaje += ".";
                contratacionAutenticacionViewController.showInformationAlert(mensaje);
                return;
            } else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
                String mensaje = contratacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.PASSWORD_LENGTH;
                mensaje += " ";
                mensaje += contratacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                mensaje += contratacionAutenticacionViewController.getString(R.string.confirmation_componenteContrasena);
                mensaje += ".";
                contratacionAutenticacionViewController.showInformationAlert(mensaje);
                return;
            }
        }

        String tarjeta = null;
        if (debePedirTarjeta) {
            tarjeta = contratacionAutenticacionViewController.pideTarjeta();
            String mensaje = "";
            if (tarjeta.equals("")) {
                mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
                contratacionAutenticacionViewController.showInformationAlert(mensaje);
                return;
            } else if (tarjeta.length() != 5) {
                mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
                contratacionAutenticacionViewController.showInformationAlert(mensaje);
                return;
            }
        }

        if (debePedirNip) {
            nip = contratacionAutenticacionViewController.pideNIP();
            if (nip.equals("")) {
                String mensaje = contratacionAutenticacionViewController.getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                mensaje += contratacionAutenticacionViewController.getString(R.string.confirmation_componenteNip);
                mensaje += ".";
                contratacionAutenticacionViewController.showInformationAlert(mensaje);
                return;
            } else if (nip.length() != Constants.NIP_LENGTH) {
                String mensaje = contratacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.NIP_LENGTH;
                mensaje += " ";
                mensaje += contratacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                mensaje += contratacionAutenticacionViewController.getString(R.string.confirmation_componenteNip);
                mensaje += ".";
                contratacionAutenticacionViewController.showInformationAlert(mensaje);
                return;
            }
        }

        if (tokenAMostrar != TipoOtpAutenticacion.ninguno) {
            asm = contratacionAutenticacionViewController.pideASM();

            if (asm.equals("")) {
                String mensaje = contratacionAutenticacionViewController.getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                switch (tipoInstrumentoSeguridad) {
                    case OCRA:
                        mensaje += getEtiquetaCampoOCRA();
                        break;
                    case DP270:
                        mensaje += getEtiquetaCampoDP270();
                        break;
                    case SoftToken:
                        if (SuiteAppPagoCreditoApi.getSofttokenStatus()) {
                            mensaje += getEtiquetaCampoSoftokenActivado();
                        } else {
                            mensaje += getEtiquetaCampoSoftokenDesactivado();
                        }
                        break;
                    default:
                        break;
                }
                mensaje += ".";
                contratacionAutenticacionViewController.showInformationAlert(mensaje);
                return;
            } else if (asm.length() != Constants.ASM_LENGTH) {
                String mensaje = contratacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.ASM_LENGTH;
                mensaje += " ";
                mensaje += contratacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                switch (tipoInstrumentoSeguridad) {
                    case OCRA:
                        mensaje += getEtiquetaCampoOCRA();
                        break;
                    case DP270:
                        mensaje += getEtiquetaCampoDP270();
                        break;
                    case SoftToken:
                        if (SuiteAppPagoCreditoApi.getSofttokenStatus()) {
                            mensaje += getEtiquetaCampoSoftokenActivado();
                        } else {
                            mensaje += getEtiquetaCampoSoftokenDesactivado();
                        }
                        break;
                    default:
                        break;
                }
                mensaje += ".";
                contratacionAutenticacionViewController.showInformationAlert(mensaje);
                return;
            }

        }

        if (debePedirCVV) {
            cvv = contratacionAutenticacionViewController.pideCVV();
            if (cvv.equals("")) {
                String mensaje = contratacionAutenticacionViewController.getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                mensaje += contratacionAutenticacionViewController.getString(R.string.confirmation_componenteCvv);
                mensaje += ".";
                contratacionAutenticacionViewController.showInformationAlert(mensaje);
                return;
            } else if (cvv.length() != Constants.CVV_LENGTH) {
                String mensaje = contratacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.CVV_LENGTH;
                mensaje += " ";
                mensaje += contratacionAutenticacionViewController.getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                mensaje += contratacionAutenticacionViewController.getString(R.string.confirmation_componenteCvv);
                mensaje += ".";
                contratacionAutenticacionViewController.showInformationAlert(mensaje);
                return;
            }
        }

        if (!terminos) {
            String mensaje = contratacionAutenticacionViewController.getString(R.string.contratacion_autenticacion_aceptar_terminos_y_condiciones_falta);
            contratacionAutenticacionViewController.showInformationAlert(mensaje);
            return;
        }

        String newToken = null;
        if (tokenAMostrar != TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == TipoInstrumento.SoftToken && SuiteAppPagoCreditoApi.getSofttokenStatus())
            newToken = loadOtpFromSofttoken(tokenAMostrar);
        if (null != newToken)
            asm = newToken;
        //AMZ
        res = true;
        operationDelegate.realizaOperacion(contratacionAutenticacionViewController, nip, asm, cvv, contrasena, terminos, tarjeta);
    }

    @Override
    public String getEtiquetaCampoContrasenia() {
        return contratacionAutenticacionViewController.getString(R.string.confirmation_contrasena);
    }

    @Override
    public String getEtiquetaCampoSoftokenActivado() {
        return contratacionAutenticacionViewController.getString(R.string.confirmation_softtokenActivado);
    }

    @Override
    public String getEtiquetaCampoSoftokenDesactivado() {
        return contratacionAutenticacionViewController.getString(R.string.confirmation_softtokenDesactivado);
    }

    @Override
    public String getEtiquetaCampoCVV() {
        return contratacionAutenticacionViewController.getString(R.string.confirmation_CVV);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
            if (Server.OP_CONSULTAR_TERMINOS == operationId) {
                ConsultaTerminosDeUsoData terminosResponse = (ConsultaTerminosDeUsoData) response.getResponse();
                //((BmovilViewsController)contratacionAutenticacionViewController.getParentViewsController())
                // .showTerminosDeUso(terminosResponse.getTerminosHtml());
                getParentBmovilViewsController().showTerminosDeUso(terminosResponse.getTerminosHtml());
            } else if (Server.OP_CONSULTAR_TERMINOS_SESION == operationId) {
                ConsultaTerminosDeUsoData terminosResponse = (ConsultaTerminosDeUsoData) response.getResponse();
                //((BmovilViewsController)contratacionAutenticacionViewController.getParentViewsController())
                //.showTerminosDeUso(terminosResponse.getTerminosHtml());
                getParentBmovilViewsController().showTerminosDeUso(terminosResponse.getTerminosHtml());
            } else {
                operationDelegate.analyzeResponse(operationId, response);
            }
        } else {
            contratacionAutenticacionViewController.limpiarCampos();
            //((BmovilViewsController)contratacionAutenticacionViewController.getParentViewsController())
            // .getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
            getParentBmovilViewsController().getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
        }
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson, ParsingHandler handler, BaseViewController caller) {
        if (contratacionAutenticacionViewController != null) {
            //((BmovilViewsController)contratacionAutenticacionViewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params,isJson,handler, caller);
            //((BmovilViewsController) SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getViewsController())
            //	.getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, caller);

            getParentBmovilViewsController().getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, caller);
        }
    }

    /**
     * Consulta los terminos y condiciones para bmovil.
     */
    public void consultarTerminosDeUso() {
        if (operationDelegate instanceof ContratacionDelegate) {
            ContratacionDelegate cDelegate = (ContratacionDelegate) operationDelegate;

            Hashtable<String, String> paramTable = new Hashtable<String, String>();
            int operacion = Server.OP_CONSULTAR_TERMINOS;
            paramTable.put(ServerConstants.PERFIL_CLIENTE, cDelegate
                    .getConsultaEstatus().getPerfilAST());
            //JAIG
            doNetworkOperation(operacion, paramTable, true, new ConsultaTerminosDeUsoData(),
                    contratacionAutenticacionViewController);
            ;

            // Se consultan los terminos de uso desde el flujo de cambio de perfil
        } else if (operationDelegate instanceof CambioPerfilDelegate) {
            CambioPerfilDelegate cDelegate = (CambioPerfilDelegate) operationDelegate;

            Hashtable<String, String> paramTable = new Hashtable<String, String>();
            paramTable.put(ServerConstants.PERFIL_CLIENTE, cDelegate.getNuevoPerfil());

            doNetworkOperation(Server.OP_CONSULTAR_TERMINOS_SESION, paramTable, true, new ConsultaTerminosDeUsoData(),
                    contratacionAutenticacionViewController);
        } else {
            return;
        }
    }

    @Override
    public TipoOtpAutenticacion tokenAMostrar() {
        return tokenAMostrar;
    }

    @Override
    public boolean mostrarCampoTarjeta() {
        //return operationDelegate.mostrarCampoTarjeta();
        return debePedirTarjeta;

    }

    @Override
    public String loadOtpFromSofttoken(TipoOtpAutenticacion tipoOTP) {
        return loadOtpFromSofttoken(tipoOTP, operationDelegate);
    }


}
