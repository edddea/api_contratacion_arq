package bancomer.api.pagarcreditos.implementations;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.pagarcreditos.R;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;


public class ConfirmacionDelegate extends DelegateBaseOperacion {
    public final static long CONFIRMACION_DELEGATE_DELEGATE_ID = 0x1ef4f4c61ca109b6L;
    //AMZ
    public boolean res = false;
    private ArrayList<String> datosLista;
    private DelegateBaseOperacion operationDelegate;
    private boolean debePedirContrasena;
    private boolean debePedirNip;
    private TipoOtpAutenticacion tokenAMostrar;
    private boolean debePedirCVV;
    private TipoInstrumento tipoInstrumentoSeguridad;
    //private String textoInstrumentoSeguridad;
    private ConfirmacionViewController confirmacionViewController;
    private boolean debePedirTarjeta;

    public ConfirmacionDelegate(DelegateBaseOperacion delegateBaseOperacion) {
        this.operationDelegate = delegateBaseOperacion;
        debePedirContrasena = operationDelegate.mostrarContrasenia();
        debePedirNip = operationDelegate.mostrarNIP();
        debePedirCVV = operationDelegate.mostrarCVV();
        tokenAMostrar = operationDelegate.tokenAMostrar();
        debePedirTarjeta = mostrarCampoTarjeta();
        String instrumento = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getSecurityInstrument();
        if (instrumento.equals(Constants.IS_TYPE_DP270)) {
            tipoInstrumentoSeguridad = TipoInstrumento.DP270;
        } else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
            tipoInstrumentoSeguridad = TipoInstrumento.OCRA;
        } else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)) {
            tipoInstrumentoSeguridad = TipoInstrumento.SoftToken;
        } else {
            tipoInstrumentoSeguridad = TipoInstrumento.sinInstrumento;
        }
    }

    public void setConfirmacionViewController(ConfirmacionViewController confirmacionViewController) {
        this.confirmacionViewController = confirmacionViewController;
    }

    public void consultaDatosLista() {
        confirmacionViewController.setListaDatos(operationDelegate.getDatosTablaConfirmacion());
    }

    public DelegateBaseOperacion consultaOperationsDelegate() {
        return operationDelegate;
    }

    public boolean consultaDebePedirContrasena() {
        return debePedirContrasena;
    }

    public boolean consultaDebePedirNIP() {
        return debePedirNip;
    }

    public boolean consultaDebePedirCVV() {
        return debePedirCVV;
    }

    public TipoInstrumento consultaTipoInstrumentoSeguridad() {
        return tipoInstrumentoSeguridad;
    }

    public TipoOtpAutenticacion consultaInstrumentoSeguridad() {
        return tokenAMostrar;
    }

    public void enviaPeticionOperacion() {
        String contrasena = null;
        String nip = null;
        String asm = null;
        String cvv = null;
        res = false;
        if (debePedirContrasena) {
            contrasena = confirmacionViewController.pideContrasena();
            if (contrasena.equals("")) {
                String mensaje = confirmacionViewController.getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                mensaje += confirmacionViewController.getString(R.string.confirmation_componenteContrasena);
                mensaje += ".";
                confirmacionViewController.showInformationAlert(mensaje);
                return;
            } else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
                String mensaje = confirmacionViewController.getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.PASSWORD_LENGTH;
                mensaje += " ";
                mensaje += confirmacionViewController.getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                mensaje += confirmacionViewController.getString(R.string.confirmation_componenteContrasena);
                mensaje += ".";
                confirmacionViewController.showInformationAlert(mensaje);
                return;
            }
        }

        String tarjeta = null;
        if (debePedirTarjeta) {
            tarjeta = confirmacionViewController.pideTarjeta();
            String mensaje = "";
            if (tarjeta.equals("")) {
                mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
                confirmacionViewController.showInformationAlert(mensaje);
                return;
            } else if (tarjeta.length() != 5) {
                mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
                confirmacionViewController.showInformationAlert(mensaje);
                return;
            }
        }

        if (debePedirNip) {
            nip = confirmacionViewController.pideNIP();
            if (nip.equals("")) {
                String mensaje = confirmacionViewController.getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                mensaje += confirmacionViewController.getString(R.string.confirmation_componenteNip);
                mensaje += ".";
                confirmacionViewController.showInformationAlert(mensaje);
                return;
            } else if (nip.length() != Constants.NIP_LENGTH) {
                String mensaje = confirmacionViewController.getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.NIP_LENGTH;
                mensaje += " ";
                mensaje += confirmacionViewController.getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                mensaje += confirmacionViewController.getString(R.string.confirmation_componenteNip);
                mensaje += ".";
                confirmacionViewController.showInformationAlert(mensaje);
                return;
            }
        }
        if (tokenAMostrar != TipoOtpAutenticacion.ninguno) {
            asm = confirmacionViewController.pideASM();
            if (asm.equals("")) {
                String mensaje = confirmacionViewController.getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                switch (tipoInstrumentoSeguridad) {
                    case OCRA:
                        mensaje += getEtiquetaCampoOCRA();
                        break;
                    case DP270:
                        mensaje += getEtiquetaCampoDP270();
                        break;
                    case SoftToken:
                        if (SuiteAppPagoCreditoApi.getSofttokenStatus()) {
                            mensaje += getEtiquetaCampoSoftokenActivado();
                        } else {
                            mensaje += getEtiquetaCampoSoftokenDesactivado();
                        }
                        break;
                    default:
                        break;
                }
                mensaje += ".";
                confirmacionViewController.showInformationAlert(mensaje);
                return;
            } else if (asm.length() != Constants.ASM_LENGTH) {
                String mensaje = confirmacionViewController.getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.ASM_LENGTH;
                mensaje += " ";
                mensaje += confirmacionViewController.getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                switch (tipoInstrumentoSeguridad) {
                    case OCRA:
                        mensaje += getEtiquetaCampoOCRA();
                        break;
                    case DP270:
                        mensaje += getEtiquetaCampoDP270();
                        break;
                    case SoftToken:
                        if (SuiteAppPagoCreditoApi.getSofttokenStatus()) {
                            mensaje += getEtiquetaCampoSoftokenActivado();
                        } else {
                            mensaje += getEtiquetaCampoSoftokenDesactivado();
                        }
                        break;
                    default:
                        break;
                }
                mensaje += ".";
                confirmacionViewController.showInformationAlert(mensaje);
                return;
            }
        }
        if (debePedirCVV) {
            cvv = confirmacionViewController.pideCVV();
            if (cvv.equals("")) {
                String mensaje = confirmacionViewController.getString(R.string.confirmation_valorVacio);
                mensaje += " ";
                mensaje += confirmacionViewController.getString(R.string.confirmation_componenteCvv);
                mensaje += ".";
                confirmacionViewController.showInformationAlert(mensaje);
                return;
            } else if (cvv.length() != Constants.CVV_LENGTH) {
                String mensaje = confirmacionViewController.getString(R.string.confirmation_valorIncompleto1);
                mensaje += " ";
                mensaje += Constants.CVV_LENGTH;
                mensaje += " ";
                mensaje += confirmacionViewController.getString(R.string.confirmation_valorIncompleto2);
                mensaje += " ";
                mensaje += confirmacionViewController.getString(R.string.confirmation_componenteCvv);
                mensaje += ".";
                confirmacionViewController.showInformationAlert(mensaje);
                return;
            }
        }

        String newToken = null;
        if (tokenAMostrar != TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == TipoInstrumento.SoftToken && SuiteAppPagoCreditoApi.getSofttokenStatus())
            newToken = loadOtpFromSofttoken(tokenAMostrar);
        if (null != newToken)
            asm = newToken;

		/*AYMB
         * if ((operationDelegate instanceof AltaRetiroSinTarjetaDelegate) || (operationDelegate instanceof ConsultaRetiroSinTarjetaDelegate)) {
			operationDelegate.realizaOperacion(confirmacionViewController, contrasena, nip, asm, tarjeta, cvv);
		}else {
			operationDelegate.realizaOperacion(confirmacionViewController, contrasena, nip, asm, tarjeta);
		}*/
        res = true;

    }


    @Override
    public String getEtiquetaCampoNip() {
        return confirmacionViewController.getString(R.string.confirmation_nip);
    }

    @Override
    public String getTextoAyudaNIP() {
        return confirmacionViewController.getString(R.string.confirmation_ayudaNip);
    }

    @Override
    public String getEtiquetaCampoContrasenia() {
        return confirmacionViewController.getString(R.string.confirmation_contrasena);
    }

    @Override
    public String getEtiquetaCampoOCRA() {
        return confirmacionViewController.getString(R.string.confirmation_ocra);
    }

    @Override
    public String getEtiquetaCampoDP270() {
        return confirmacionViewController.getString(R.string.confirmation_dp270);
    }

    @Override
    public String getEtiquetaCampoSoftokenActivado() {
        return confirmacionViewController.getString(R.string.confirmation_softtokenActivado);
    }

    @Override
    public String getEtiquetaCampoSoftokenDesactivado() {
        return confirmacionViewController.getString(R.string.confirmation_softtokenDesactivado);
    }

    @Override
    public String getEtiquetaCampoCVV() {
        return confirmacionViewController.getString(R.string.confirmation_CVV);
    }

    @Override
    public String getTextoAyudaInstrumentoSeguridad(TipoInstrumento tipoInstrumento) {
        if (tokenAMostrar == TipoOtpAutenticacion.ninguno) {
            return "";
        } else if (tokenAMostrar == TipoOtpAutenticacion.registro) {
            switch (tipoInstrumento) {
                case SoftToken:
                    if (SuiteAppPagoCreditoApi.getSofttokenStatus()) {
                        return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroSofttokenActivado);
                    } else {
                        return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroSofttokenDesactivado);
                    }
                case OCRA:
                    return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroOCRA);
                case DP270:
                    return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroDP270);
                case sinInstrumento:
                default:
                    return "";
            }
        } else if (tokenAMostrar == TipoOtpAutenticacion.codigo) {
            switch (tipoInstrumento) {
                case SoftToken:
                    if (SuiteAppPagoCreditoApi.getSofttokenStatus()) {
                        return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoSofttokenActivado);
                    } else {
                        return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoSofttokenDesactivado);
                    }
                case OCRA:
                    return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoOCRA);
                case DP270:
                    return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoDP270);
                case sinInstrumento:
                default:
                    return "";
            }
        }
        return "";
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        Constants.Perfil perfil = session.getClientProfile();

        if (response.getStatus() == ServerResponse.OPERATION_ERROR && !Constants.Perfil.recortado.equals(perfil)) {
            confirmacionViewController.limpiarCampos();
            ((BmovilViewsController) confirmacionViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
        }
        operationDelegate.analyzeResponse(operationId, response);
    }

    @Override
    public TipoOtpAutenticacion tokenAMostrar() {
        return tokenAMostrar;
    }

    @Override
    public boolean mostrarCampoTarjeta() {
        return operationDelegate.mostrarCampoTarjeta();
    }

    @Override
    public String loadOtpFromSofttoken(TipoOtpAutenticacion tipoOTP) {
        return loadOtpFromSofttoken(tipoOTP, operationDelegate);
    }


}
