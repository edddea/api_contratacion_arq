package bancomer.api.pagarcreditos.implementations;

import android.util.Log;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.models.ConsultarEstatusEnvioEC;
import bancomer.api.pagarcreditos.models.ConsultarEstatusEnvioECResult;
import suitebancomer.aplicaciones.bmovil.classes.model.CuentaEC;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

import static suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.hideAccountNumber;

public class ConsultarEstatusEnvioEstadodeCuentaDelegate extends DelegateBaseAutenticacion {

    /**
     *
     */
    public static final long CONSULTARESTATUSENVIOESTADODECUENTA_DELEGATE_ID = 6392075930232354965L;
    public boolean res = false;
    private ConsultarEstatusEnvioEstadodeCuentaViewController viewController;
    /**
     * The owner view controller.
     */
    private BaseViewController ownerController;

    /**
     * @return The view controller associated to this delegate.
     */
    public BaseViewController getCallerController() {
        return ownerController;
    }

    /**
     * @param callerController The view controller to associate to this delegate.
     */
    public void setCallerController(BaseViewController callerController) {
        this.ownerController = callerController;
    }
    // @return true si se debe mostrar contrasena, false en caso contrario.

    @Override
    public boolean mostrarContrasenia() {
        Perfil perfil = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
        boolean value = Autenticacion.getInstance().mostrarContrasena(Operacion.actualizarEstatusEnvioEC,
                perfil);
        return value;
    }


    // @return true si se debe mostrar CVV, false en caso contrario.

    @Override
    public boolean mostrarCVV() {
        Perfil perfil = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
        boolean value = Autenticacion.getInstance().mostrarCVV(Operacion.actualizarEstatusEnvioEC, perfil);
        return value;
    }


    // @return true si se debe mostrar NIP, false en caso contrario.

    @Override
    public boolean mostrarNIP() {
        Perfil perfil = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
        boolean value = Autenticacion.getInstance().mostrarNIP(Operacion.actualizarEstatusEnvioEC,
                perfil);
        return value;
    }

    @Override
    public int getOpcionesMenuResultados() {
        // TODO Auto-generated method stub
        return SHOW_MENU_EMAIL;
    }

    /**
     * @return El tipo de token a mostrar
     */
    @Override
    public TipoOtpAutenticacion tokenAMostrar() {
        Perfil perfil = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
        TipoOtpAutenticacion tipoOTP;
        try {
            tipoOTP = Autenticacion.getInstance().tokenAMostrar(Operacion.actualizarEstatusEnvioEC,
                    perfil);
        } catch (Exception ex) {
            Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
            tipoOTP = null;
        }
        return tipoOTP;
    }

    public ConsultarEstatusEnvioEstadodeCuentaViewController getViewController() {
        return viewController;
    }

    public void setViewController(ConsultarEstatusEnvioEstadodeCuentaViewController viewController) {
        this.viewController = viewController;
    }

    @Override
    public ArrayList<Object> getDatosTablaConfirmacion() {

        ArrayList<Object> tabla = new ArrayList<Object>();
        ArrayList<String> fila;
        ArrayList<CuentaEC> data = Session.getListaEC();

        for (CuentaEC acc : data) {
            if (acc.isFlag()) {
                fila = new ArrayList<String>();
                fila.add(hideAccountNumber(acc.getNumeroCuenta()));
                fila.add(acc.getEstadoEnvio().equals("D") ? "Activado" : "Suspendido");
                tabla.add(fila);
            }
        }
        return tabla;
    }

    public ArrayList<CuentaEC> getDatos() {

        ArrayList<CuentaEC> cuentas = new ArrayList<CuentaEC>();
        //	ArrayList<String> fila;
        ArrayList<CuentaEC> data = Session.getListaEC();

        for (CuentaEC acc : data) {
            CuentaEC currentaccount = new CuentaEC();
            if (acc.isFlag()) {
                currentaccount.setNumeroCuenta(acc.getNumeroCuenta());
                currentaccount.setEstadoEnvio(acc.getEstadoEnvio());
                cuentas.add(currentaccount);
            }
        }

        return cuentas;
    }

    public ArrayList<Object> getDatosResult() {

        ArrayList<Object> tabla = new ArrayList<Object>();
        ArrayList<String> fila;
        ArrayList<CuentaEC> data = getDatos();
        //fila = new ArrayList<String>();
        //fila.add("Cuenta");
        //fila.add("Envío");
        //tabla.add(fila);
        for (CuentaEC acc : data) {

            fila = new ArrayList<String>();
            fila.add(hideAccountNumber(acc.getNumeroCuenta()));

            fila.add(acc.getEstadoEnvio().equals("D") ? "Activado" : "Suspendido");
            tabla.add(fila);
        }

        return tabla;
    }

    public JSONObject ArrCuentas() {

        JSONObject arrcuentas = new JSONObject();
        JSONObject objetoGeneral = new JSONObject();
        JSONArray occuentas = new JSONArray();
        ArrayList<CuentaEC> cuentasmodif = getDatos();
        CuentaEC account;
        for (int i = 0; i < cuentasmodif.size(); i++) {
            JSONObject cuenta = new JSONObject();
            account = (CuentaEC) cuentasmodif.get(i);
            try {
                cuenta.put("numeroCuenta", account.getNumeroCuenta());
                cuenta.put("estatusEnvio", account.getEstadoEnvio());
            } catch (JSONException e) {
                if (ServerCommons.ALLOW_LOG) e.printStackTrace();
            }
            occuentas.put(cuenta);
        }
        try {
            arrcuentas.put("ocCuentas", occuentas);
            objetoGeneral.put("arrCuentas", arrcuentas);
            if (ServerCommons.ALLOW_LOG) Log.w("XX", objetoGeneral.toString());
        } catch (JSONException e) {
            if (ServerCommons.ALLOW_LOG) e.printStackTrace();
        }
        return arrcuentas;
        //return arrcuentas.toString();
        //return occuentas;

    }

    @Override
    public ArrayList<Object> getDatosTablaResultados() {

        ArrayList<Object> tabla = new ArrayList<Object>();

		/*
        ArrayList<String> fila2;
		fila2 = new ArrayList<String>();
		fila2.add(ownerController.getString(R.string.bmovil_Estado_de_cuenta_lblPeriodo));
		fila2.add(consultaEC.getPeriodo());

		ArrayList<String> fila3;
		fila3 = new ArrayList<String>();
		fila3.add(ownerController.getString(R.string.bmovil_Estado_de_cuenta_lblCorreo));
		fila3.add(consultaEC.geteMail());
		*/
        ArrayList<String> fila4;
        fila4 = new ArrayList<String>();
        fila4.add(ownerController.getString(R.string.bmovil_result_fecha));
        String fecha = Tools.formatDate(ConsultarEstatusEnvioECResult.getFecha());
        fila4.add(fecha);

        ArrayList<String> fila5;
        fila5 = new ArrayList<String>();
        fila5.add(ownerController.getString(R.string.bmovil_result_hora));
        String hora = Tools.parsetime(ConsultarEstatusEnvioECResult.getHora());
        fila5.add(hora);

        ArrayList<String> fila6;
        fila6 = new ArrayList<String>();
        fila6.add(ownerController.getString(R.string.bmovil_result_folio));
        String folio = ConsultarEstatusEnvioECResult.getFolio();
        fila6.add(folio);
        Log.i("dora", "cuentas cambiadas");
	/*	tabla.add(fila1);
		tabla.add(fila2);
		tabla.add(fila3); */
        tabla.add(fila4);
        tabla.add(fila5);
        tabla.add(fila6);
        return tabla;
    }

    /**
     * Invoca la operacion para hacer el cambio de estatus de envio
     */
    @Override
    public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController, String contrasenia, String nip, String token, String cvv, String campoTarjeta) {

        int operationId = Server.ACTUALIZAR_ESTATUS_EC;//OP Actualizar estatus envio de estado de cuenta
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        Autenticacion aut = Autenticacion.getInstance();
        Perfil perfil = session.getClientProfile();
        String cadAutenticacion = aut.getCadenaAutenticacion(Operacion.actualizarEstatusEnvioEC, perfil);

        Hashtable<String, Object> params = new Hashtable<String, Object>();
        JSONObject cuentas = null;
        try {
            cuentas = ArrCuentas();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        params.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
        params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());

        /**VERIFICAR VALIDAR 10/06**/

        params.put(ServerConstants.CADENA_AUTENTICACION, "00100"); //validar
        params.put(ServerConstants.CVE_ACCESO, contrasenia == null ? "" : contrasenia);
        params.put(ServerConstants.CODIGO_NIP, nip == null ? "" : nip);
        params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
        params.put("codigoOTPReg", "");
        params.put(ServerConstants.CODIGO_CVV2, cvv == null ? "" : cvv);

        /**VERIFICAR VALIDAR 10/06**/

        params.put(ServerConstants.ARRCUENTAS, cuentas.toString());
        params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
        params.put(Server.J_AUT, cadAutenticacion);
        doNetworkOperation(operationId, params, true, new ConsultarEstatusEnvioECResult(), confirmacionAutenticacionViewController);
    }

    @Override
    public void doNetworkOperation(int operationId,
                                   Hashtable<String, ?> params, boolean isJson, ParsingHandler handler, BaseViewController caller) {
        ((BmovilViewsController) viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, caller);
    }


    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

        if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
            if (response.getResponse() instanceof ConsultarEstatusEnvioEC) {
                ConsultarEstatusEnvioEC result = (ConsultarEstatusEnvioEC) response.getResponse();
                Session.setListaEC(result.getCuentas());
                viewController.inicializarPantalla();
            } else if (response.getResponse() instanceof ConsultarEstatusEnvioECResult) {
                showResultados();
            }
        } else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
            BaseViewControllerCommons current = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp();
            current.showInformationAlert(response.getMessageText());
        }

    }


    /**
     * Actualiza las cuentas almacenadas en session
     * con las cuentas recibidas al cambiar la cuenta asociada.
     *
     * @param accounts
     */
	/*private void actualizarCuentas(Account[] accounts){
		Session session = Session.getInstance(SuiteApp.appContext);
		session.updateAccounts(accounts);
	}*/

    /**
     * Muestra la pantalla de resultados.
     */
    private void showResultados() {
        BmovilViewsController bmovilParentController = ((BmovilViewsController) viewController.getParentViewsController());
        bmovilParentController.showResultadosViewController(this, getNombreImagenEncabezado(), getTextoEncabezado());
    }

    public void validarDatos() {

        if (!viewController.estateChange()) {
            ((ConsultarEstatusEnvioEstadodeCuentaViewController) ownerController)
                    .showInformationAlert(R.string.bmovil_Estado_de_cuenta_estatusinvalido);
        } else {

            showConfirmacion();

            res = true;
        }
    }


    /**
     * Muestra la pantalla de confirmacion
     */
    public void showConfirmacion() {
        BmovilApp app = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication();
        BmovilViewsController bMovilVC = app.getBmovilViewsController();
        if (registrarOperacion())
            bMovilVC.showRegistrarOperacion(this);
        else {
            int resSubtitle = R.string.confirmation_subtitulo;
            int resTitleColor = R.color.primer_azul;
            int resIcon = R.drawable.bmovil_cambiocuenta_icono;
            int resTitle = R.string.bmovil_Consultar_Estatus_EnvioEC_title;

            SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(this, resIcon, resTitle, resSubtitle, resTitleColor);
        }
    }

    /**
     * @return
     * @category RegistrarOperacion
     */
    public boolean registrarOperacion() {
        Perfil perfil = Session.getInstance(SuiteAppPagoCreditoApi.appContext)
                .getClientProfile();
        Operacion operacion;
        operacion = Operacion.actualizarEstatusEnvioEC;

        Autenticacion aut = Autenticacion.getInstance();
        boolean value = aut.validaRegistro(operacion, perfil);
        if (ServerCommons.ALLOW_LOG) Log.d("RegistroOP", value + " consultarEstadoCuenta? ");
        return value;
    }

    /**
     * Obtiene la lista de cuentas a mostrar en la tabla .
     *
     * @return ArrayList<Account> con las cuentas del usuario
     */
    public ArrayList<Object> getCuentasUsuario() {
        ArrayList<CuentaEC> lista = Session.getListaEC();
        ArrayList<Object> accountsArray = new ArrayList<Object>(lista.size());
        ArrayList<Object> item;

        if (lista.size() != 0) {
            for (CuentaEC acc : lista) {
                item = new ArrayList<Object>();
                item.add(acc);
                item.add(Tools.hideAccountNumber(acc.getNumeroCuenta()));
                item.add(acc.getEstadoEnvio().equals("I") ? "Suspendido" : "Activado");
                accountsArray.add(item);
            }
        } else {
            item = new ArrayList<Object>();
            item.add("");
            item.add("Sin cuentas");
            item.add("");
            accountsArray.add(item);
        }
        return accountsArray;
    }

    @Override
    public int getTextoEncabezado() {
        int resTitle = R.string.bmovil_Consultar_Estatus_EnvioEC_title;
        return resTitle;
    }


    @Override
    public int getNombreImagenEncabezado() {
        int resIcon = R.drawable.bmovil_administrar_icono;
        return resIcon;
    }

    @Override
    public String getTextoTituloResultado() {
        return viewController.getString(R.string.transferir_detalle_operacion_exitosaTitle);
    }

    public String getTextoEspecialResultados() {

        String textoEspecialResultados = SuiteAppPagoCreditoApi.appContext.getString(R.string.bmovil_Estado_de_cuenta_lbltextoEspecial);
        return textoEspecialResultados;
    }

    @Override
    public int getColorTituloResultado() {
        return R.color.primer_azul;
    }


    @Override
    public boolean mostrarCampoTarjeta() {
        return (mostrarCVV() || mostrarNIP());
    }
}