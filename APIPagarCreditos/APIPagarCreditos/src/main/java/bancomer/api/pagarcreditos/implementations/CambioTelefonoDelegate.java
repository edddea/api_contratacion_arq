package bancomer.api.pagarcreditos.implementations;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.model.CatalogoVersionado;
import bancomer.api.common.model.Compania;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.models.CambioTelefonoResult;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.SessionStoredListener;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

public class CambioTelefonoDelegate extends DelegateBaseAutenticacion implements SessionStoredListener {

    /**
     *
     */
    public static final long CAMBIO_TELEFONO_DELEGATE_ID = 5435021160204095682L;
    private CambioTelefonoViewController viewController;
    private String username = null;
    private String ium = null;
    private String client = null;
    private String version = null;


    public CambioTelefonoDelegate() {

    }

    public CambioTelefonoViewController getViewController() {
        return viewController;
    }


    public void setViewController(CambioTelefonoViewController viewController) {
        this.viewController = viewController;
    }


    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson, ParsingHandler handler, BaseViewController caller) {
        ((BmovilViewsController) viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, caller);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

        if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
            if (response.getResponse() instanceof CambioTelefonoResult)
                if (Server.ALLOW_LOG)
                    Log.d(getClass().getName(), "folio" + ((CambioTelefonoResult) response.getResponse()).getFolio());
            saveTempData();
            showResultados();

        } else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
            //cambio de telefono no exitoso
            BaseViewControllerCommons current = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp();
            current.showInformationAlert(response.getMessageText());
        }

    }

    /**
     * @return true si se debe mostrar contrasena, false en caso contrario.
     */
    @Override
    public boolean mostrarContrasenia() {
        Perfil perfil = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
        boolean value = Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.cambioTelefono,
                perfil);
        return value;
    }

    /**
     * @return true si se debe mostrar CVV, false en caso contrario.
     */
    @Override
    public boolean mostrarCVV() {
        Perfil perfil = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
        boolean value = Autenticacion.getInstance().mostrarCVV(Constants.Operacion.cambioTelefono, perfil);
        return value;
    }

    /**
     * @return true si se debe mostrar NIP, false en caso contrario.
     */
    @Override
    public boolean mostrarNIP() {
        Perfil perfil = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
        boolean value = Autenticacion.getInstance().mostrarNIP(Constants.Operacion.cambioTelefono,
                perfil);
        return value;
    }

    /**
     * @return El tipo de token a mostrar
     */
    @Override
    public TipoOtpAutenticacion tokenAMostrar() {
        Perfil perfil = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
        TipoOtpAutenticacion tipoOTP;
        try {
            tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.cambioTelefono,
                    perfil);
        } catch (Exception ex) {
            if (Server.ALLOW_LOG)
                Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
            tipoOTP = null;
        }
        return tipoOTP;
    }

    /**
     * @return Lista de compañías del catalogo de dinero movil
     */
    public ArrayList<Object> getListaCompanias() {
        CatalogoVersionado catalogoDineroMovil = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getCatalogoDineroMovil();
        Vector<Object> vectorCompanias = catalogoDineroMovil.getObjetos();
        return new ArrayList<Object>(vectorCompanias);
    }

    /**
     * @return true si el numero de telefono es valido y la confirmacion es igual
     */
    public boolean validaTelefono(String numTelefono, String numNuevoTelefono, Compania compania) {
        boolean esOK = true;
        int msgError = 0;
        if (Tools.isEmptyOrNull(numTelefono)) {
            esOK = false;
            msgError = R.string.bmovil_cambio_telefono_telefono_vacio;
        } else if (Tools.isEmptyOrNull(numNuevoTelefono)) {
            esOK = false;
            msgError = R.string.bmovil_cambio_telefono_confirmacion_vacio;
        } else if (numTelefono.length() != Constants.TELEPHONE_NUMBER_LENGTH) {
            esOK = false;
            msgError = R.string.bmovil_cambio_telefono_telefono_corto;
        } else if (numNuevoTelefono.length() != Constants.TELEPHONE_NUMBER_LENGTH) {
            esOK = false;
            msgError = R.string.bmovil_cambio_telefono_confirmacion_corto;
        } else if (!numNuevoTelefono.equals(numTelefono)) {
            esOK = false;
            msgError = R.string.bmovil_cambio_telefono_error_diferentes;
        }

        if (!esOK) {
            viewController.showInformationAlert(msgError);
        }
        return esOK;
    }

    /**
     * Muestra la pantalla de confirmacion.
     */
    public void showConfirmacion() {
        int resSubtitle = 0;
        int resTitleColor = 0;
        int resIcon = R.drawable.bmovil_cambiotelefono_icono;
        int resTitle = 0;
        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(this, resIcon, resTitle, resSubtitle, resTitleColor);
    }


    @Override
    public ArrayList<Object> getDatosTablaConfirmacion() {

        ArrayList<Object> tabla = new ArrayList<Object>();
        ArrayList<String> fila;

        fila = new ArrayList<String>();
        fila.add("Nuevo número");
        fila.add("".equals(viewController.getCambioTelefono().getNuevoTelefono()) ? Constants.TEXTO_CAMBIO_TELEFONO_VACIO : viewController.getCambioTelefono().getNuevoTelefono());
        tabla.add(fila);

        fila = new ArrayList<String>();
        fila.add("Compañía celular");
        fila.add(viewController.getCambioTelefono().getNombreCompania());
        tabla.add(fila);

        return tabla;
    }


    /**
     * Realiza la invocacion al servidor para hacer el cambio de telefono asociado
     */
    @Override
    public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController, String contrasenia, String nip, String token, String cvv, String campoTarjeta) {
        int operationId = Server.CAMBIO_TELEFONO;//OP Cambio de telefono
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        Autenticacion aut = Autenticacion.getInstance();
        Perfil perfil = session.getClientProfile();

        String cadAutenticacion = aut.getCadenaAutenticacion(Constants.Operacion.cambioTelefono, perfil);
        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
        params.put(ServerConstants.NUMERO_CLIENTE, session.getClientNumber());
        params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        params.put(ServerConstants.CVE_ACCESO, contrasenia == null ? "" : contrasenia);
        //params.put("numeroCelNuevo", (Constants.Perfil.recortado.equals(perfil)) ? "" : viewController.getCambioTelefono().getNuevoTelefono());
        //params.put("companiaCelularNueva", (Constants.Perfil.recortado.equals(perfil)) ? "" : viewController.getCambioTelefono().getNombreCompania());
        //params.put(ServerConstants.VERSION, (Constants.Perfil.recortado.equals(perfil)) ? "recortado" : "");
        params.put(ServerConstants.NUMERO_CELULAR_NUEVO, ((Perfil.recortado.equals(perfil)) && (getVersion() != null) && (Constants.VALOR_VERSION_RECORTADO.equals(getVersion()))) ? "" : viewController.getCambioTelefono().getNuevoTelefono());
        params.put(ServerConstants.COMPANIA_CELULAR_NUEVO, ((Perfil.recortado.equals(perfil)) && (getVersion() != null) && (Constants.VALOR_VERSION_RECORTADO.equals(getVersion()))) ? "" : viewController.getCambioTelefono().getNombreCompania());
        params.put(ServerConstants.VERSION, ((getVersion() != null) && (Constants.VALOR_VERSION_RECORTADO.equals(getVersion()))) ? getVersion() : "");
        params.put(ServerConstants.CODIGO_NIP, nip == null ? "" : nip);
        params.put(ServerConstants.CODIGO_CVV2, cvv == null ? "" : cvv);
        params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
        params.put(ServerConstants.CADENA_AUTENTICACION, cadAutenticacion);
        params.put(ServerConstants.TARJETA_5DIG, campoTarjeta == null ? "" : campoTarjeta);

        params.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
        List<String> listaEncriptar = Arrays.asList(ServerConstants.CVE_ACCESO,
                ServerConstants.CODIGO_NIP, ServerConstants.CODIGO_CVV2);
        Encripcion.setContext(SuiteAppPagoCreditoApi.appContext);
        Encripcion.encriptaCadenaAutenticacion(params, listaEncriptar);

        //JAIG
        doNetworkOperation(operationId, params, true, new CambioTelefonoResult(), confirmacionAutenticacionViewController);
    }

    /**
     * Sin opciones del menu
     */
    @Override
    public int getOpcionesMenuResultados() {
        return 0;
    }

    /**
     * Se encarga de respaldar los datos necesarios para hacer el cierre de sesion
     * cuando se borran los datos, antes del cierra de session.
     */
    private void saveTempData() {
//		SuiteAppPagoCreditoApi suiteApp = SuiteAppPagoCreditoApi.getInstance();
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);//.getApplicationContext());
        username = session.getUsername();
        ium = session.getIum();
        client = session.getClientNumber();
    }


    /**
     * Elimina la info despues de realizar el cambio de telefono
     * La informaci�n que debe ser borrada del teléfono es la siguiente:
     * 1.	Deber� eliminarse toda la informaci�n almacenada en el archivo DatosBmovil del teléfono, que contiene
     * a.	Usuario
     * b.	Compañia
     * c.	fecha mensaje perfil
     * d.	iteraciones
     * e.	Todas las versiones de catálogos
     * f.	Todos los catálogos
     * 2.	Deber� setear a Nulo esta misma informaci�n en el objeto sesión.
     * 3.	Deber� cambiarse a false la propiedad bmovilActivado (bmovilActivado=false) del archivo EstatusAplicaciones
     */
    private void borrarDatos() {
        Context appContext = SuiteAppPagoCreditoApi.appContext;
        Session session = Session.getInstance(appContext);
        KeyStoreWrapper kswrapper = session.getKeyStoreWrapper();

        String title = "Actualizando ...";
        String msg = "";//viewController.getString(R.string.alert_StoreSession);
        //viewController.muestraIndicadorActividad(title, msg);

        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewController().muestraIndicadorActividad(title, msg);
        session.clearSession();
        session.setApplicationActivated(false);
        session.setUsername(viewController.getCambioTelefono().getNuevoTelefono());
        session.setCompaniaUsuario(viewController.getCambioTelefono().getNombreCompania());
        session.storeSession(this);

        //Se borran los datos numero telefono y seed de KeyChain
        kswrapper.setUserName(" ");
        kswrapper.setSeed(" ");

    }

    /**
     * Notifica que la sesion ha sido guardada para continuar con el flujo de la
     * operacion realizada.
     */
    public void sessionStored() {
        getViewController().ocultaIndicadorActividad();
        getViewController().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //cerrar session.
                //SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().closeSession(username,ium,client);
                if (SuiteAppPagoCreditoApi.getCallBackSession() != null) {
                    SuiteAppPagoCreditoApi.getCallBackSession().closeSession(username, ium, client);
                }
            }
        });
    }

    @Override
    public ArrayList<Object> getDatosTablaResultados() {
        return getDatosTablaConfirmacion();
    }

    @Override
    public int getTextoEncabezado() {
        int resTitle = R.string.bmovil_cambio_telefono_title;
        return resTitle;
    }


    @Override
    public int getNombreImagenEncabezado() {
        int resIcon = R.drawable.bmovil_cambiotelefono_icono;
        return resIcon;
    }

    @Override
    public String getTextoPantallaResultados() {
        return "La actualización de tu número celular asociado ha sido realizada con éxito";
    }

    @Override
    public String getTextoTituloResultado() {
//		return SuiteAppPagoCreditoApi.getInstance().getApplicationContext().getString(R.string.transferir_detalle_operacion_exitosaTitle);
        return SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("transferir.detalle.operacion.exitosaTitle", "string"));
    }

    @Override
    public int getColorTituloResultado() {
        return R.color.verde_limon;
    }


    void showResultados() {
        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController()
                .showResultadosAutenticacionViewController(this, -1, -1);
    }

    @Override
    protected void accionBotonResultados() {
        borrarDatos();
    }

    @Override
    protected int getImagenBotonResultados() {
        return R.drawable.bmovil_btn_salir;
    }


    @Override
    public boolean mostrarCampoTarjeta() {
        return (mostrarCVV() || mostrarNIP());
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
