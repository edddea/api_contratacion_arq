package bancomer.api.pagarcreditos.implementations;

import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.commons.GuiTools;
import bancomer.api.pagarcreditos.commons.TrackingHelper;
import bancomer.api.pagarcreditos.gui.commons.controllers.ListaDatosViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

public class ConfirmacionViewController extends BaseViewController implements OnClickListener {
    //AMZ
    public BmovilViewsController parentManager;
    private LinearLayout contenedorPrincipal;
    private LinearLayout contenedorContrasena;
    private LinearLayout contenedorNIP;
    private LinearLayout contenedorASM;
    private LinearLayout contenedorCVV;
    private TextView campoContrasena;
    private TextView campoNIP;
    private TextView campoASM;
    private TextView campoCVV;
    private EditText contrasena;
    private EditText nip;
    private EditText asm;
    private EditText cvv;
    private TextView instruccionesContrasena;
    private TextView instruccionesNIP;
    private TextView instruccionesASM;
    private TextView instruccionesCVV;
    private ImageButton confirmarButton;
    private ConfirmacionDelegate confirmacionDelegate;
    //Nuevo Campo
    private TextView campoTarjeta;
    private LinearLayout contenedorCampoTarjeta;
    private EditText tarjeta;
    private TextView instruccionesTarjeta;
    //AMZ

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_confirmacion_admon);
        SuiteApp.appContext = this;
        //AMZ
        parentManager = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController();

        TrackingHelper.trackState("confirma", parentManager.estados);

        setParentViewsController(SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController());
        setDelegate((ConfirmacionDelegate) getParentViewsController().getBaseDelegateForKey(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID));
        setTitulo();

        confirmacionDelegate = (ConfirmacionDelegate) getDelegate();
        confirmacionDelegate.setConfirmacionViewController(this);

        findViews();
        scaleToScreenSize();

        confirmacionDelegate.consultaDatosLista();

        configuraPantalla();
        moverScroll();

        contrasena.addTextChangedListener(new BmovilTextWatcher(this));
        nip.addTextChangedListener(new BmovilTextWatcher(this));
        asm.addTextChangedListener(new BmovilTextWatcher(this));
        cvv.addTextChangedListener(new BmovilTextWatcher(this));
        tarjeta.addTextChangedListener(new BmovilTextWatcher(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        SuiteApp.appContext = this;
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }


    private void configuraPantalla() {
        mostrarContrasena(confirmacionDelegate.consultaDebePedirContrasena());
        mostrarNIP(confirmacionDelegate.consultaDebePedirNIP());
        mostrarASM(confirmacionDelegate.consultaInstrumentoSeguridad());
        mostrarCVV(confirmacionDelegate.consultaDebePedirCVV());
        mostrarCampoTarjeta(confirmacionDelegate.mostrarCampoTarjeta());

        LinearLayout contenedorPadre = (LinearLayout) findViewById(R.id.confirmacion_campos_layout);

        if (contenedorContrasena.getVisibility() == View.GONE &&
                contenedorNIP.getVisibility() == View.GONE &&
                contenedorASM.getVisibility() == View.GONE &&
                contenedorCVV.getVisibility() == View.GONE &&
                contenedorCampoTarjeta.getVisibility() == View.GONE) {
            //contenedorPadre.setVisibility(View.GONE);

            contenedorPadre.setBackgroundColor(0);

        }

        contenedorPadre.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        float camposHeight = contenedorPadre.getMeasuredHeight();
//		contenedorPadre.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
//		float camposHeight = contenedorPadre.getMeasuredHeight();
//		
//		LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.confirmacion_lista_datos);
//		layoutListaDatos.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
//		float listaHeight = layoutListaDatos.getMeasuredHeight();
//		
//		ViewGroup contenido = (ViewGroup)this.findViewById(android.R.id.content).getRootView();//findViewById(android.R.id.content);
//		contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
//		float contentHeight = contenido.getMeasuredHeight();
//		
//		System.out.println("Los valores " + camposHeight + " y " + contentHeight + " y " + listaHeight);
//		
//		float margin = getResources().getDimension(R.dimen.confirmacion_fields_initial_margin);

        LinearLayout layoutListaDatos = (LinearLayout) findViewById(R.id.confirmacion_lista_datos);
        layoutListaDatos.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        float listaHeight = layoutListaDatos.getMeasuredHeight();
//		float maximumSize = (contentHeight * 4) / 5;
//		System.out.println("Altura maxima " +maximumSize);
//		float elementsSize = listaHeight + camposHeight;
//		System.out.println("Altura mixta " +elementsSize);
//		float heightParaValidar = (contentHeight*3)/4;
//		System.out.println("heightParaValidar " +contentHeight);
//		
//		if (elementsSize >= contentHeight) {
//			RelativeLayout.LayoutParams camposLayout = (RelativeLayout.LayoutParams)contenedorPadre.getLayoutParams();
//			camposLayout.addRule(RelativeLayout.BELOW, R.id.confirmacion_lista_datos);
//		}

        ViewGroup contenido = (ViewGroup) this.findViewById(android.R.id.content).getRootView();//findViewById(android.R.id.content);
        contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
        float contentHeight = contenido.getMeasuredHeight();

        //System.out.println("Los valores " + camposHeight + " y " + contentHeight + " y " + listaHeight);

        float margin = getResources().getDimension(R.dimen.confirmacion_fields_initial_margin);

        float maximumSize = (contentHeight * 4) / 5;
        //System.out.println("Altura maxima " +maximumSize);
        float elementsSize = listaHeight + camposHeight;
        //System.out.println("Altura mixta " +elementsSize);
        float heightParaValidar = (contentHeight * 3) / 4;
        //	System.out.println("heightParaValidar " +contentHeight);

        if (elementsSize >= contentHeight) {
            //RelativeLayout.LayoutParams camposLayout = (RelativeLayout.LayoutParams)contenedorPadre.getLayoutParams();
            //camposLayout.addRule(RelativeLayout.BELOW, R.id.confirmacion_lista_datos);
        }

        confirmarButton.setOnClickListener(this);
    }

    public void setTitulo() {
        ConfirmacionDelegate confirmacionDelegate = (ConfirmacionDelegate) getDelegate();

        setTitle(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado(),
                confirmacionDelegate.consultaOperationsDelegate().getNombreImagenEncabezado());
    }

    @SuppressWarnings("deprecation")
    public void setListaDatos(ArrayList<Object> datos) {
        LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        //params.topMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_top_margin);
        //params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);
        //params.rightMargin =  getResources().getDimensionPixelOffset(R.dimen.resultados_side_margin);

        ListaDatosViewController listaDatos = new ListaDatosViewController(this, params, parentViewsController);
        listaDatos.setNumeroCeldas(2);
        listaDatos.setLista(datos);
        listaDatos.setNumeroFilas(datos.size());
        listaDatos.setTitulo(R.string.confirmation_subtitulo);
        listaDatos.showLista();
        LinearLayout layoutListaDatos = (LinearLayout) findViewById(R.id.confirmacion_lista_datos);
        layoutListaDatos.addView(listaDatos);
    }

    public void pideContrasenia() {
        contenedorContrasena.setVisibility(View.GONE);
        //findViewById(R.id.confirmacion_contrasena_layout).setVisibility(View.GONE);
    }

    public void pideClaveSeguridad() {
        contenedorASM.setVisibility(View.GONE);
        //findViewById(R.id.confirmacion_asm_layout).setVisibility(View.GONE);
    }

    /*
    *
    */
    public void mostrarContrasena(boolean visibility) {
        contenedorContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        campoContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        contrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
            campoContrasena.setText(confirmacionDelegate.getEtiquetaCampoContrasenia());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
            contrasena.setFilters(userFilterArray);
            contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
        instruccionesContrasena.setVisibility(View.GONE);
    }

    /*
    *
    */
    public void mostrarNIP(boolean visibility) {
        contenedorNIP.setVisibility(visibility ? View.VISIBLE : View.GONE);
        campoNIP.setVisibility(visibility ? View.VISIBLE : View.GONE);
        nip.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
            campoNIP.setText(confirmacionDelegate.getEtiquetaCampoNip());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
            nip.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(tarjeta);
            nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = confirmacionDelegate.getTextoAyudaNIP();
            if (instrucciones.equals("")) {
                instruccionesNIP.setVisibility(View.GONE);
            } else {
                instruccionesNIP.setVisibility(View.VISIBLE);
                instruccionesNIP.setText(instrucciones);
            }
        } else {
            nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    /*
    *
    */
    public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP) {
        switch (tipoOTP) {
            case ninguno:
                contenedorASM.setVisibility(View.GONE);
                campoASM.setVisibility(View.GONE);
                asm.setVisibility(View.GONE);
                asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
                break;
            case codigo:
            case registro:
                contenedorASM.setVisibility(View.VISIBLE);
                campoASM.setVisibility(View.VISIBLE);
                asm.setVisibility(View.VISIBLE);
                InputFilter[] userFilterArray = new InputFilter[1];
                userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
                asm.setFilters(userFilterArray);
                cambiarAccionTexto(contrasena);
                cambiarAccionTexto(tarjeta);
                cambiarAccionTexto(nip);
                asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
                break;
        }

        Constants.TipoInstrumento tipoInstrumento = confirmacionDelegate.consultaTipoInstrumentoSeguridad();

        switch (tipoInstrumento) {
            case OCRA:
                campoASM.setText(confirmacionDelegate.getEtiquetaCampoOCRA());
                //asm.setTransformationMethod(null);
                break;
            case DP270:
                campoASM.setText(confirmacionDelegate.getEtiquetaCampoDP270());
                //asm.setTransformationMethod(null);
                break;
            case SoftToken:
                if (SuiteAppPagoCreditoApi.getSofttokenStatus()) {
                    asm.setText(Constants.DUMMY_OTP);
                    asm.setEnabled(false);
                    campoASM.setText(confirmacionDelegate.getEtiquetaCampoSoftokenActivado());
                } else {
                    asm.setText("");
                    asm.setEnabled(true);
                    campoASM.setText(confirmacionDelegate.getEtiquetaCampoSoftokenDesactivado());
                    //asm.setTransformationMethod(null);
                }
//				String otp = GeneraOTPSTDelegate.generarOtpTiempo();
//				if(null != otp) {
//					asm.setText(otp);
//					asm.setEnabled(false);
//				}
//				campoASM.setText(confirmacionDelegate.getEtiquetaCampoSoftokenActivado());
                break;
            default:
                break;
        }
        String instrucciones = confirmacionDelegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
        if (instrucciones.equals("")) {
            instruccionesASM.setVisibility(View.GONE);
        } else {
            instruccionesASM.setVisibility(View.VISIBLE);
            instruccionesASM.setText(instrucciones);
        }
    }

    private void mostrarCampoTarjeta(boolean visibility) {
        contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE : View.GONE);
        campoTarjeta.setVisibility(visibility ? View.VISIBLE : View.GONE);
        tarjeta.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
            campoTarjeta.setText(confirmacionDelegate.getEtiquetaCampoTarjeta());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(5);
            tarjeta.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = confirmacionDelegate.getTextoAyudaTarjeta();
            if (instrucciones.equals("")) {
                instruccionesTarjeta.setVisibility(View.GONE);
            } else {
                instruccionesTarjeta.setVisibility(View.VISIBLE);
                instruccionesTarjeta.setText(instrucciones);
            }
        } else {
            tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }

    }


    private void cambiarAccionTexto(EditText campo) {
        if (campo.getVisibility() == View.VISIBLE) {
            campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        }
    }

    public String pideContrasena() {
        if (contrasena.getVisibility() == View.GONE) {
            return "";
        } else {
            return contrasena.getText().toString();
        }
    }

    public String pideNIP() {
        if (nip.getVisibility() == View.GONE) {
            return "";
        } else {
            return nip.getText().toString();
        }
    }

    public String pideASM() {
        if (asm.getVisibility() == View.GONE) {
            return "";
        } else {
            return asm.getText().toString();
        }
    }

    public String pideCVV() {
        if (cvv.getVisibility() == View.GONE) {
            return "";
        } else {
            return cvv.getText().toString();
        }
    }

    /*
    *
    */
    public void mostrarCVV(boolean visibility) {
        contenedorCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
        campoCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
        cvv.setVisibility(visibility ? View.VISIBLE : View.GONE);
        instruccionesCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);

        if (visibility) {
            campoCVV.setText(confirmacionDelegate.getEtiquetaCampoCVV());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
            cvv.setFilters(userFilterArray);
            String instrucciones = confirmacionDelegate.getTextoAyudaCVV();
            instruccionesCVV.setText(instrucciones);
            instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(nip);
            cambiarAccionTexto(asm);
            cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == confirmarButton && !parentViewsController.isActivityChanging()) {
            botonConfirmarClick();
        }
    }

    public void botonConfirmarClick() {
        confirmacionDelegate.enviaPeticionOperacion();
        if (confirmacionDelegate.res == true) {
            //texto
            if (Server.ALLOW_LOG)
                Log.d("titulo pantalla", confirmacionDelegate.getTextoTituloResultado());
            //	System.out.println(confirmacionDelegate.getTextoTituloResultado());
            if (confirmacionDelegate.getTextoTituloResultado() == getString(R.string.about_copyright)) {
                if (Server.ALLOW_LOG)
                    Log.d("titulo pantalla", confirmacionDelegate.getTextoTituloResultado());
            }
        }

        //ARR
        if (confirmacionDelegate.res) {
            Map<String, Object> operacionRealizadaMap = new HashMap<String, Object>();

            //Comprobacion de titulos

            if (getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_miscuentas)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;transferencias+mis cuentas");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            } else if (getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrascuentasbbva)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;transferencias+otra cuenta bbva bancomer");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            } else if (getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_cuentaexpress)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;transferencias+cuenta express");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            } else if (getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrosbancos)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;transferencias+otros bancos");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            } else if (getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_dineromovil)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;transferencias+dinero movil");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            } else if (getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.servicesPayment_title)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;pagar+servicio");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            } else if (getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.transferir_otrosBBVA_TDC_title)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;pagar+tarjeta credito");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            } else if (getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()) == getString(R.string.tiempo_aire_title)) {
                //ARR
                operacionRealizadaMap.put("evento_realizada", "event52");
                operacionRealizadaMap.put("&&products", "operaciones;comprar+tiempo aire");
                operacionRealizadaMap.put("eVar12", "operacion realizada");

                TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
            }
        }
    }

    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        if (SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getViewsController().getBaseDelegateForKey(
                ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID) != null) {
            confirmacionDelegate =
                    (ConfirmacionDelegate) SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getViewsController()
                            .getBaseDelegateForKey(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
        }

        confirmacionDelegate.analyzeResponse(operationId, response);
    }

    private void findViews() {
        contenedorPrincipal = (LinearLayout) findViewById(R.id.confirmacion_lista_datos);
        contenedorContrasena = (LinearLayout) findViewById(R.id.campo_confirmacion_contrasena_layout);
        contenedorNIP = (LinearLayout) findViewById(R.id.campo_confirmacion_nip_layout);
        contenedorASM = (LinearLayout) findViewById(R.id.campo_confirmacion_asm_layout);
        contenedorCVV = (LinearLayout) findViewById(R.id.campo_confirmacion_cvv_layout);

        contrasena = (EditText) contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
        nip = (EditText) contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
        asm = (EditText) contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
        cvv = (EditText) contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);

        campoContrasena = (TextView) contenedorContrasena.findViewById(R.id.confirmacion_contrasena_label);
        campoNIP = (TextView) contenedorNIP.findViewById(R.id.confirmacion_nip_label);
        campoASM = (TextView) contenedorASM.findViewById(R.id.confirmacion_asm_label);
        campoCVV = (TextView) contenedorCVV.findViewById(R.id.confirmacion_cvv_label);

        instruccionesContrasena = (TextView) contenedorContrasena.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
        instruccionesNIP = (TextView) contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
        instruccionesASM = (TextView) contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
        instruccionesCVV = (TextView) contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);

        contenedorCampoTarjeta = (LinearLayout) findViewById(R.id.campo_confirmacion_campotarjeta_layout);
        tarjeta = (EditText) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
        campoTarjeta = (TextView) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
        instruccionesTarjeta = (TextView) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);

        confirmarButton = (ImageButton) findViewById(R.id.confirmacion_confirmar_button);


    }

    private void scaleToScreenSize() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(contenedorPrincipal);
        guiTools.scale(findViewById(R.id.confirmacion_campos_layout));

        guiTools.scale(contenedorContrasena);
        guiTools.scale(contenedorNIP);
        guiTools.scale(contenedorASM);
        guiTools.scale(contenedorCVV);

        guiTools.scale(contrasena, true);
        guiTools.scale(nip, true);
        guiTools.scale(asm, true);
        guiTools.scale(cvv, true);

        guiTools.scale(campoContrasena, true);
        guiTools.scale(campoNIP, true);
        guiTools.scale(campoASM, true);
        guiTools.scale(campoCVV, true);

        guiTools.scale(instruccionesContrasena, true);
        guiTools.scale(instruccionesNIP, true);
        guiTools.scale(instruccionesASM, true);
        guiTools.scale(instruccionesCVV, true);

        guiTools.scale(contenedorCampoTarjeta);
        guiTools.scale(tarjeta, true);
        guiTools.scale(campoTarjeta, true);
        guiTools.scale(instruccionesTarjeta, true);

        guiTools.scale(confirmarButton);

    }

    public void limpiarCampos() {
        contrasena.setText("");
        nip.setText("");
        asm.setText("");
        cvv.setText("");
        tarjeta.setText("");
    }


    public String pideTarjeta() {
        if (tarjeta.getVisibility() == View.GONE) {
            return "";
        } else
            return tarjeta.getText().toString();
    }

}
