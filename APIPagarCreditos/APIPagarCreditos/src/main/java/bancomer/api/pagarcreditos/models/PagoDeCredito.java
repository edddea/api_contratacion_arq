package bancomer.api.pagarcreditos.models;

/**
 * Created by andres.vicentelinare on 27/10/2015.
 */
public class PagoDeCredito {
    private String tipoCredito;
    private String numCredito;
    private String cuentaRetiro;
    private String nombreBeneficiario;
    private String importePago;
    private String importePagoMoneda;
    private String fechaPago;

    public PagoDeCredito() {
        tipoCredito = "";
        numCredito = "";
        cuentaRetiro = "";
        nombreBeneficiario = "";
        importePago = "";
        importePagoMoneda = "";
        fechaPago = "";
    }

    public String getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public String getNumCredito() {
        return numCredito;
    }

    public void setNumCredito(String numCredito) {
        this.numCredito = numCredito;
    }

    public String getCuentaRetiro() {
        return cuentaRetiro;
    }

    public void setCuentaRetiro(String cuentaRetiro) {
        this.cuentaRetiro = cuentaRetiro;
    }

    public String getNombreBeneficiario() {
        return nombreBeneficiario;
    }

    public void setNombreBeneficiario(String nombreBeneficiario) {
        this.nombreBeneficiario = nombreBeneficiario;
    }

    public String getImportePago() {
        return importePago;
    }

    public void setImportePago(String importePago) {
        this.importePago = importePago;
    }

    public String getImportePagoMoneda() {
        return importePagoMoneda;
    }

    public void setImportePagoMoneda(String importePagoMoneda) {
        this.importePagoMoneda = importePagoMoneda;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }
}
