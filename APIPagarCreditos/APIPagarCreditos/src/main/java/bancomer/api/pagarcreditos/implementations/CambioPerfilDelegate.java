package bancomer.api.pagarcreditos.implementations;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.bancomer.mbanking.softtoken.SuiteAppApi;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.models.CambioDePerfilData;
import bancomer.api.pagarcreditos.models.CambioTelefono;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.SessionStoredListener;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomercoms.classes.common.PropertiesManager;

/**
 * Delegate de la pantalla cambio de perfil del usuario.
 *
 * @author CGI
 */
public class CambioPerfilDelegate extends DelegateBaseAutenticacion implements
        SessionStoredListener {

    /**
     * El identificador del delegate.
     */
    public final static long CAMBIO_PERFIL_DELEGATE_ID = 0xa79b6adacaef0c14L;

    /**
     * Controlador actual.
     */
    //private BaseViewController ownerController;
    private BaseViewController ownerController;

    /**
     * Controlador Transferir.
     */
    private BaseViewController transferirController;

    /**
     * Controlador base.
     */
    //private BaseViewControllerCommons baseViewController;
    private Activity baseViewController;

    /**
     * Indica si es un cambio de perfil de avanzado a basico en el proceso de
     * login.
     */
    private boolean changeAdvancedToBasic = false;

    /**
     * El nuevo perfil del usuario.
     */
    private String nuevoPerfil = null;

    /**
     * El folio a mostrar en la pantalla de resultados.
     */
    private String folio;


    private boolean cambioPerfil = false;

    /**
     * Viene de transferir.
     */
    private boolean vienedeTransferir = false;
    private boolean cambioPerfilLogin = false;

    /**
     * Valida si existe un cambio de perfil del usuario de basico a avanzado.
     *
     * @param activity la vista actual
     * @return true si existe un cambio de perfil. Falso en caso contrario
     */
    public boolean validarCambioPerfilAvanzado(Activity activity) {
        boolean validaCambioPerfil = false;

        SuiteAppPagoCreditoApi.getInstance()
                .getBmovilApplication()
                .getBmovilViewsController()
                .addDelegateToHashMap(
                        CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID, this);

        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        /*if (Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CAMBIO_PERFIL))) {
			
			mostrarContratacion();
			setBaseViewController(baseViewController);
			validaCambioPerfil = true;
			setNuevoPerfil(Constants.PROFILE_ADVANCED_03);
		} else {
			if (verificarCambioPerfil(false)) {
				if (session.getAceptaCambioPerfil()) {
					mostrarCambioPerfil(baseViewController);
					validaCambioPerfil = true;
					setNuevoPerfil(Constants.PROFILE_ADVANCED_03);
				}
			}
		}*/


        if (Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CAMBIO_PERFIL))) {
            validaCambioPerfil = true;
            setNuevoPerfil(Constants.PROFILE_ADVANCED_03);

        } else {
            if ((Perfil.basico.equals(session.getClientProfile())) && (session.getAceptaCambioPerfil())) {
                if (verificarCambioPerfil(session.getClientProfile())) {
                    cambioPerfil = true;
                    mostrarCambioPerfil(activity);
                    validaCambioPerfil = false;
                    setNuevoPerfil(Constants.PROFILE_ADVANCED_03);
                }
            }
        }


        if (Server.ALLOW_LOG) Log.d("LoginDelegate :: validarCambioPerfilAvanzado",
                "Cambiar a perfil avanzado: " + validaCambioPerfil);

        return validaCambioPerfil;
    }

    /**
     * Valida si existe un cambio de perfil del usuario de avanzado a basico.
     *
     * @return true si existe un cambio de perfil. Falso en caso contrario
     */
    public boolean validarCambioPerfilBasico() {
        boolean validaCambioPerfil = false;
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);

        if (Perfil.avanzado.equals(session.getClientProfile())) {
            validaCambioPerfil = verificarCambioPerfil(session.getClientProfile());
            if (validaCambioPerfil) {
                setNuevoPerfil(Constants.PROFILE_BASIC_01);
            }
        }

        if (Server.ALLOW_LOG) Log.d("LoginDelegate :: validarCambioPerfilBasico",
                "Cambiar a perfil basico: " + validaCambioPerfil);

        return validaCambioPerfil;
    }

    /**
     * Comprueba que se produce un cambio de perfil en la operacion de login.
     *
     * @param perfil determina si el perfil del usuario es basico o avanzado.
     *               Verdadero avanzado, falso basico
     * @return la verificacion
     */
    public boolean verificarCambioPerfil(Perfil perfil) {
        boolean verificado = false;

        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        String secInstrument = session.getSecurityInstrument();
        String estatusIS = session.getEstatusIS();
        if (null != secInstrument
                && !Constants.EMPTY_STRING.equals(secInstrument)) {
            if (Perfil.basico.equals(perfil)) {
                if (validarRN8() && validarRN9()) {
                    verificado = true;
                }

            } else if (Perfil.avanzado.equals(perfil)) {
                if (!Constants.IS_TYPE_DP270.equals(secInstrument)
                        && !Constants.IS_TYPE_OCRA.equals(secInstrument)
                        && !Constants.TYPE_SOFTOKEN.S1.value.equals(secInstrument)) {
                    verificado = true;
                } else {
                    if (!Constants.ESTATUS_IS_ACTIVO.equals(estatusIS)) {
                        verificado = true;
                    }
                }

            }
        }

        if (Server.ALLOW_LOG) Log.d("LoginDelegate :: verificarCambioPerfil",
                "Existe cambio de perfil: " + verificado);

        return verificado;
    }

    // RN 8 de Caso de uso Login

    private boolean validarRN8() {
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        String secInstrument = session.getSecurityInstrument();
        String estatusIS = session.getEstatusIS();

        return Constants.ESTATUS_IS_ACTIVO.equals(estatusIS)
                && (Constants.IS_TYPE_DP270.equals(secInstrument)
                || Constants.IS_TYPE_OCRA.equals(secInstrument)
                || Constants.TYPE_SOFTOKEN.S1.value.equals(secInstrument));
    }

    // RN 9 de Caso de uso Login

    private boolean validarRN9() {
        String tipo = Tools.obtenerCuentaEje().getType();

        return !Constants.CREDIT_TYPE.equals(tipo) && !Constants.EXPRESS_TYPE.equals(tipo);

    }

    /**
     * Muestra la pantallad de perfil cuando es de basico a avanzado.
     *
     * @param activity la vista actual
     */
    public void mostrarCambioPerfil(final Activity activity) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //SuiteAppAdmonApi.getInstance().getBmovilApplication()
                //.getBmovilViewsController().setCurrentActivityApp(null);
                SuiteAppPagoCreditoApi.getInstance().getBmovilApplication()
                        .getBmovilViewsController().showCambioPerfil();
                setCambioPerfilLogin(true);
            }
        });
    }

    /**
     * El usuario acepta el cambio de perfil.
     */
    public void cambioPerfilAceptado() {
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        if (Perfil.recortado.equals(session.getClientProfile())) {
            //EA#8
            Account cEje = Tools.obtenerCuentaEje();

            Hashtable<String, String> paramTable = new Hashtable<String, String>();
            paramTable.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
            paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
            paramTable.put(ServerConstants.NUMERO_TARJETA, cEje.getNumber());
            paramTable.put(ServerConstants.COMPANIA_CELULAR, session.getCompaniaUsuario());

            //JAIG
            SuiteAppPagoCreditoApi.getInstance().getBmovilApplication()
                    .invokeNetworkOperation(Server.OP_SOLICITAR_ALERTAS, paramTable, true, new SolicitarAlertasData(), ownerController);
        } else {
            mostrarContratacion();

        }
    }


    /**
     * El usuario Cambia de Perfil de Basico a Avanzado sin Token Activo
     */

    public void cambioPerfilSinToken() {
        final Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);

        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewController().showYesNoAlert(
                R.string.cambioPerfil_alert_basicoAavanzado_noTokenActivo,
                new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if (DialogInterface.BUTTON_POSITIVE == which) {
                            // Si la aplicacion Token movil esta
                            // activa, borra el token de Gemalto
                            if (session.isSofttokenActivado()) {
                                GeneraOTPSTDelegate generaOTPSTDelegate = new GeneraOTPSTDelegate();
                                if (generaOTPSTDelegate.borraToken()) {
                                    PropertiesManager.getCurrent().setSofttokenActivated(false);
                                }
                            }

                            // Guarda que se solicito un cambio de
                            // perfil y muestra la pantalla de
                            // ingreso datos del flujo de activacion
                            // softToken
                            session.saveBanderasBMovil(
                                    Constants.BANDERAS_CAMBIO_PERFIL, true,
                                    true);
                            mostrarIngresoDatos();
                        }

                    }
                });
    }

    /**
     * El usuario cancela el cambio de perfil de basico a avanzado o viceversa.
     */
    public void cambioPerfilCancelado() {
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        if (Perfil.basico.equals(session.getClientProfile())) {
            session.setAceptaCambioPerfil(Boolean.valueOf(false));
        }

        ownerController.getParentViewsController().removeDelegateFromHashMap(
                CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #mostrarContrasenia()
     */
    @Override
    public boolean mostrarContrasenia() {
        Operacion perfil = Operacion.cambioPerfilR;
        Perfil perfilClt = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
        return Autenticacion.getInstance().mostrarContrasena(
                //Constants.Operacion.cambioPerfil, Session.getInstance(SuiteApp.appContext).getClientProfile());
                Operacion.cambioPerfilR, Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile());

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #mostrarNIP()
     */
    @Override
    public boolean mostrarNIP() {
        return Autenticacion.getInstance().mostrarNIP(
                //	Constants.Operacion.cambioPerfil, Session.getInstance(SuiteApp.appContext).getClientProfile());
                Operacion.cambioPerfilR, Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile());

    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #tokenAMostrar()
     */
    @Override
    public TipoOtpAutenticacion tokenAMostrar() {

        TipoOtpAutenticacion tipoOTP;
        try {
            tipoOTP = Autenticacion.getInstance().tokenAMostrar(
                    //Constants.Operacion.cambioPerfil, Session.getInstance(SuiteApp.appContext).getClientProfile());
                    Operacion.cambioPerfilR, Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile());

        } catch (Exception ex) {
            if (Server.ALLOW_LOG) Log.e("DelegateBaseOperacion",
                    "Error on Autenticacion.mostrarNIP execution.", ex);
            tipoOTP = null;
        }

        return tipoOTP;
    }

    /*
     * (non-Javadoc)
     *
     * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.
     * DelegateBaseAutenticacion#mostrarCVV()
     */
    @Override
    public boolean mostrarCVV() {
        return Autenticacion.getInstance().mostrarCVV(
                //Constants.Operacion.cambioPerfil, Session.getInstance(SuiteApp.appContext).getClientProfile());
                Operacion.cambioPerfilR, Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile());

    }

    @Override
    public boolean mostrarCampoTarjeta() {
        return (mostrarCVV() || mostrarNIP());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getDatosTablaConfirmacion()
     */
    @Override
    public ArrayList<Object> getDatosTablaConfirmacion() {
        ArrayList<Object> lista = new ArrayList<Object>();
        ArrayList<Object> registros = new ArrayList<Object>();

        //registros.add(baseViewController.getString(R.string.cambioPerfil_confirmacionEtiqueta));
        registros.add(SuiteAppPagoCreditoApi.appContext.getString(R.string.cambioPerfil_confirmacionEtiqueta));
        if (isAdvancedNuevoPerfil()) {
            //registros.add(baseViewController.getString(R.string.cambioPerfil_confirmacionPerfilNuevoAvanzado));
            registros.add(SuiteAppPagoCreditoApi.appContext.getString(R.string.cambioPerfil_confirmacionPerfilNuevoAvanzado));
        } else {
            //registros.add(baseViewController.getString(R.string.cambioPerfil_confirmacionPerfilNuevoBasico));
            registros.add(SuiteAppPagoCreditoApi.appContext.getString(R.string.cambioPerfil_confirmacionPerfilNuevoBasico));
        }

        lista.add(registros);

        return lista;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getDatosTablaResultados()
     */
    @Override
    public ArrayList<Object> getDatosTablaResultados() {
        ArrayList<Object> datosResultados = new ArrayList<Object>();
        ArrayList<String> registro = new ArrayList<String>();

        //registro.add(ownerController
        //		.getString(R.string.cambioPerfil_resultados_etiquetaCambio));
        registro.add(SuiteAppPagoCreditoApi.appContext.getString(R.string.cambioPerfil_resultados_etiquetaCambio));
        if (isAdvancedNuevoPerfil()) {
            //registro.add(ownerController
            //		.getString(R.string.cambioPerfil_resultados_cambioPerfilToken));
            registro.add(SuiteAppPagoCreditoApi.appContext.getString(R.string.cambioPerfil_resultados_cambioPerfilToken));
        } else {
            //registro.add(ownerController
            //		.getString(R.string.cambioPerfil_resultados_cambioPerfilNoToken));
            registro.add(SuiteAppPagoCreditoApi.appContext.getString(R.string.cambioPerfil_resultados_cambioPerfilNoToken));
        }

        datosResultados.add(registro);

        registro = new ArrayList<String>();
        //registro.add(ownerController
        //		.getString(R.string.cambioPerfil_resultados_folio));
        registro.add(SuiteAppPagoCreditoApi.appContext.getString(R.string.cambioPerfil_resultados_folio));
        registro.add(folio);
        datosResultados.add(registro);

        return datosResultados;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getTextoTituloResultado()
     */
    @Override
    public String getTextoTituloResultado() {
        //return baseViewController.getString(R.string.cambioPerfil_tituloExito);
        return SuiteAppPagoCreditoApi.appContext.getString(R.string.cambioPerfil_tituloExito);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getColorTituloResultado()
     */
    @Override
    public int getColorTituloResultado() {
        return R.color.verde_limon;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getTextoPantallaResultados()
     */
    @Override
    public String getTextoPantallaResultados() {
        return "";
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getTextoEspecialResultados()
     */
    @Override
    public String getTextoEspecialResultados() {
        return "";
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getNombreImagenEncabezado()
     */
    @Override
    public int getNombreImagenEncabezado() {
        return R.drawable.icono_contratacion;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getTextoEncabezado()
     */
    @Override
    public int getTextoEncabezado() {
        return R.string.cambioPerfil_confirmacionTitulo;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getOpcionesMenuResultados()
     */
    @Override
    public int getOpcionesMenuResultados() {
		/*return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF
				| SHOW_MENU_FRECUENTE | SHOW_MENU_RAPIDA | SHOW_MENU_BORRAR;*/
        return 0;
    }

    /**
     * Realiza la operacion de cambio de perfil de avanzado a basico despues de
     * la pantalla de login. Al ser una peticion transparente (no se muestra
     * pantalla alguna de cambio de perfil) ninguno de los parametros habituales
     * son necesarios.
     */
    public void realizaOperacion() {
        realizaOperacion(null, null, null, null, null, false, null);
    }

    /*
     * (non-Javadoc)
     *
     * @see suitebancomer.aplicaciones.bmovil.classes.gui.delegates.
     * DelegateBaseAutenticacion
     * #realizaOperacion(suitebancomer.aplicaciones.bmovil
     * .classes.gui.controllers.ContratacionAutenticacionViewController,
     * java.lang.String, java.lang.String, java.lang.String, java.lang.String,
     * boolean, java.lang.String)
     */
    @Override
    public void realizaOperacion(
            ContratacionAutenticacionViewController contratacionAutenticacionViewController,
            String nip, String token, String cvv, String pwd, boolean terminos,
            String campoTarjeta) {
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        String username = session.getUsername();
        String numeroCliente = session.getClientNumber();

        String va = Autenticacion.getInstance().getCadenaAutenticacion(
                //Constants.Operacion.cambioPerfil, session.getClientProfile());
                Operacion.cambioPerfilR, session.getClientProfile());


        // Perfil perfil = Constants.Perfil.basico;
        if (Perfil.basico.equals(session.getClientProfile())) {
            changeAdvancedToBasic = false;
            setBaseViewController(contratacionAutenticacionViewController);
            // perfil = Constants.Perfil.avanzado;
        }

        // String va = Autenticacion.getInstance().getCadenaAutenticacion(
        // Constants.Operacion.cambioPerfil, perfil);

        // Se realiza la peticion de cambio de perfil
        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        paramTable.put(ServerConstants.NUMERO_TELEFONO, username);
        paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        paramTable.put(ServerConstants.NUMERO_CLIENTE, numeroCliente);
        paramTable.put(ServerConstants.PERFIL_NUEVO_ETIQUETA, nuevoPerfil);
        paramTable.put(ServerConstants.CODIGO_NIP, Tools.isEmptyOrNull(nip) ? "" : nip);
        paramTable.put(ServerConstants.CVE_ACCESO, Tools.isEmptyOrNull(pwd) ? "" : pwd);
        paramTable.put(ServerConstants.CODIGO_CVV2, Tools.isEmptyOrNull(cvv) ? "" : cvv);
        paramTable.put(ServerConstants.CODIGO_OTP, Tools.isEmptyOrNull(token) ? "" : token);
        paramTable.put(ServerConstants.TARJETA_5DIG, campoTarjeta == null ? "" : campoTarjeta);
        paramTable.put(ServerConstants.CADENA_AUTENTICACION, Tools.isEmptyOrNull(va) ? "" : va);

        paramTable.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
        List<String> listaEncriptar = Arrays.asList(ServerConstants.CVE_ACCESO,
                ServerConstants.CODIGO_NIP, ServerConstants.CODIGO_CVV2);
        Encripcion.setContext(SuiteAppPagoCreditoApi.appContext);
        Encripcion.encriptaCadenaAutenticacion(paramTable, listaEncriptar);

        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication()
                .invokeNetworkOperation(
                        Server.CAMBIA_PERFIL,
                        paramTable, true,
                        new CambioDePerfilData(),
                        ownerController);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.classes.gui.delegates.BaseDelegate#analyzeResponse(int,
     * suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
     */
    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {

            if (operationId == Server.CAMBIA_PERFIL) {
                Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);

                // Cambio de perfil de avanzado a basico desde el flujo de Login
                if (changeAdvancedToBasic) {
                    session.setClientProfile(Perfil.basico);
                    session.storeSession(this);
                } else {
                    // Se almacena el nuevo perfil del usuario
                    if (isAdvancedNuevoPerfil()) {
                        session.setClientProfile(Perfil.avanzado);
                        session.setAceptaCambioPerfil(Boolean.valueOf(true));
                    } else {
                        session.setClientProfile(Perfil.basico);
                    }

                    // Cambia el valor de la variable cambio de perfil en caso de que exista y su valor sea verdadero
                    session.saveBanderasBMovil(Constants.BANDERAS_CAMBIO_PERFIL, false, false);

                    // Se recoge el folio recibido de la respuesta a la peticion de cambio de perfil
                    if (response.getResponse() instanceof CambioDePerfilData) {
                        folio = ((CambioDePerfilData) response.getResponse()).getFolioCanal();
                    }

                    // Se muestra la pantalla de resultados
                    mostrarResultados();
                }

            } else if (operationId == Server.OP_SOLICITAR_ALERTAS) {

                final SolicitarAlertasData sa = (SolicitarAlertasData) response
                        .getResponse();

                String validacionalertas = sa.getValidacionAlertas();
                if (Constants.ALERT01.equals(validacionalertas)) {
                    // tiene que ir al Caso de Uso de Contratacion autenticación
                    mostrarContratacion();
                } else if (Constants.ALERT02.equals(validacionalertas)) {
                    // EA#10
                    ownerController
                            .showInformationAlert(
                                    ownerController
                                            .getString(R.string.label_information),
                                    ownerController
                                            .getString(R.string.cambioPerfil_contratacionalertas),
                                    ownerController
                                            .getString(R.string.common_accept),
                                    null);

                } else if (Constants.ALERT03.equals(validacionalertas) || Constants.ALERT04.equals(validacionalertas)) {
                    // EA#11 Caso de uso Cambio de Perfil
                    ownerController.showYesNoAlert(
                            SuiteAppPagoCreditoApi.appContext.getString(R.string.cambioPerfil_alert_recortado_cambiotelefono),
                            new OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    if (DialogInterface.BUTTON_POSITIVE == which) {
                                        CambioTelefono cambioTel = new CambioTelefono();

                                        cambioTel.setNuevoTelefono(sa.getNumeroAlertas());
                                        cambioTel.setNombreCompania(sa.getCompaniaAlertas());

                                        CambioTelefonoDelegate ctd = new CambioTelefonoDelegate();
                                        ctd.setVersion((Perfil.recortado.equals(Session.getInstance(SuiteAppPagoCreditoApi.getInstance()).getClientProfile())) ? Constants.VALOR_VERSION_RECORTADO : "");
                                        ctd.setViewController(new CambioTelefonoViewController());

                                        ctd.getViewController().setParentViewsController(SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController());
                                        ctd.getViewController().setCambioTelefono(cambioTel);

                                        ctd.showConfirmacion();

                                    } else {
                                        dialog.dismiss();
                                    }
                                }
                            });

                }
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.common.SessionStoredListener
     * #sessionStored()
     */
    @Override
    public void sessionStored() {
        baseViewController.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                SuiteAppPagoCreditoApi.getInstance().getBmovilApplication()
                        .getBmovilViewsController().showMenuPrincipal();
            }
        });
    }

    /**
     * Muestra la pantalla de contratacion.
     */


    public void mostrarContratacion() {
        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().showContratacionAutenticacion(this);
    }

    // --

    /**
     * Muestra la pantalla de resultados autenticacion.
     */
    private void mostrarResultados() {
        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication()
                .getBmovilViewsController()
                .showResultadosAutenticacionViewController(this, -1, -1);
    }

    /**
     * Muestra la pantalla de ingreso datos.
     */
    private void mostrarIngresoDatos() {
        SofttokenViewsController viewsController = SuiteAppApi.getInstanceApi()
                .getSofttokenApplicationApi().getSottokenViewsController();
        ownerController.setParentViewsController(viewsController);
        viewsController.setCurrentActivityApp(ownerController);
        viewsController.showPantallaIngresoDatos();
    }


    public void setTransferirViewController(BaseViewController viewController) {
        this.transferirController = viewController;
    }


    public void setBaseViewController(Activity baseViewController) {
        this.baseViewController = baseViewController;
    }

    /**
     * @param ownerController El controlador actual a establecer.
     *                        <p/>
     *                        public void setOwnerController(BaseViewController ownerController) {
     *                        this.ownerController = ownerController;
     *                        }
     */
    public void setOwnerController(BaseViewController ownerController) {
        this.ownerController = ownerController;
    }

    /**
     * Establece si existe un cambio de perfil de avanzado a basico en el login.
     *
     * @param changeAdvancedToBasic un cambio de perfil de avanzado a basico en el login
     */
    public void setChangeAdvancedToBasic(boolean changeAdvancedToBasic) {
        this.changeAdvancedToBasic = changeAdvancedToBasic;
    }


    @Override
    protected void accionBotonResultados() {
        if (isOperacionTransferirEnCurso()) {
            //BmovilApp app = SuiteApp.getInstance().getBmovilApplication();
            //BmovilViewsController bMovilVC = app.getBmovilViewsController();
            //bMovilVC.showConfirmacion((DelegateBaseAutenticacion)transferirController.getDelegate());

            ((ResultadosAutenticacionDelegate) ((BaseViewController) SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewController()).getDelegate()).setOperacionTransferirEnCurso(true);
            SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewController().goBack();

            ownerController.goBack();

        } else {
            BaseViewsController parentViewsController = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController();
            parentViewsController.removeDelegateFromHashMap(ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID);
            parentViewsController.removeDelegateFromHashMap(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
            ((BmovilViewsController) parentViewsController).showMenuPrincipal(true);
        }

    }

    @Override
    protected int getImagenBotonResultados() {
        if (isOperacionTransferirEnCurso()) {
            return R.drawable.btn_continuar;
        } else {
            return R.drawable.btn_menu;
        }
    }

    public boolean isOperacionTransferirEnCurso() {
        return vienedeTransferir;
    }

    public void setOperacionTransferirEnCurso(boolean transferir) {
        vienedeTransferir = transferir;
    }

    /**
     * Obtiene el nuevo perfil del usuario.
     *
     * @return el nuevo perfil del usuario
     */
    public String getNuevoPerfil() {
        return nuevoPerfil;
    }


    /**
     * Establece el nuevo perfil del usuario.
     *
     * @param nuevoPerfil el nuevo perfil del usuario
     */
    public void setNuevoPerfil(String nuevoPerfil) {
        if (null != nuevoPerfil) {
            this.nuevoPerfil = nuevoPerfil;
        } else {
            if (Perfil.avanzado.equals(Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile()) || Perfil.recortado.equals(Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile())) {
                this.nuevoPerfil = Constants.PROFILE_BASIC_01;
            } else if (Perfil.basico.equals(Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile())) {
                this.nuevoPerfil = Constants.PROFILE_ADVANCED_03;
            }
        }
    }

    /**
     * Determina el nuevo perfil del usuario.
     *
     * @return verdadero si el nuevo perfil del usuario es avanzado. Falso si es
     * basico
     */
    private boolean isAdvancedNuevoPerfil() {
        if (Constants.PROFILE_ADVANCED_03.equals(nuevoPerfil)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Devuelve el tipo enumerado del perfil de usuario.
     *
     * @return el tipo enumerado del perfil de usuario
     */
	/*private Constants.Perfil isAdvancedPerfil() {
		return Session.getInstance(SuiteApp.appContext).isAdvancedProfile() ? Constants.Perfil.avanzado
				: Constants.Perfil.basico;
	}*/
    public boolean isCambioPerfil() {
        return cambioPerfil;
    }

    public void setCambioPerfil(boolean cambioPerfil) {
        this.cambioPerfil = cambioPerfil;
    }

    public boolean isCambioPerfilLogin() {
        return cambioPerfilLogin;
    }

    public void setCambioPerfilLogin(boolean cambioPerfilLogin) {
        this.cambioPerfilLogin = cambioPerfilLogin;
    }
}
