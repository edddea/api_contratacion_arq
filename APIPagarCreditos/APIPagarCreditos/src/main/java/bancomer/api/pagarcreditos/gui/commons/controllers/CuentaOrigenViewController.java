package bancomer.api.pagarcreditos.gui.commons.controllers;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

import java.util.ArrayList;
import java.util.Iterator;

import bancomer.api.common.commons.Constants;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.gui.commons.delegates.CuentaOrigenDelegate;
import bancomer.api.pagarcreditos.implementations.BaseDelegate;
//import bancomer.api.pagarcreditos.implementations.BaseViewsController;
import bancomer.api.pagarcreditos.commons.GuiTools;
//import bancomer.api.pagarcreditos.implementations.SuiteAppPagoCreditoApi;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;
import bancomer.api.pagarcreditos.implementations.BmovilViewsController;

//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.administracion.MisCuentasDelegate;

public class CuentaOrigenViewController extends LinearLayout implements View.OnClickListener{

	private BaseViewsControllerCommons parentManager;
	private CuentaOrigenDelegate cuentaOrigenDelegate;
	private BaseDelegate delegate;
	private ImageButton imgIzquierda;
	private ImageButton imgDerecha;
	private TextView vistaCtaOrigen;
	private TextView tituloComponenteCtaOrigen;
	private LinearLayout componenteCtaOrigen;
	private ArrayList<Account> listaCuetasAMostrar;
	private int indiceCuentaSeleccionada;
	private Activity actividad;
	private String saldo;
	private boolean seleccionado;

	public CuentaOrigenViewController(Context context, LayoutParams layoutParams, BaseViewsControllerCommons parentManager, Activity actividad) {
		super(context);
		LayoutInflater inflater = LayoutInflater.from(context);
		LinearLayout viewLayout = (LinearLayout) inflater.inflate(SuiteApp.getResourceId("layout_cuenta_origen_view_admon", "layout"), this, true);
		viewLayout.setLayoutParams(layoutParams);
		this.parentManager = parentManager;
		this.actividad = actividad;

		setTituloComponenteCtaOrigen((TextView) findViewById(SuiteApp.getResourceId("componente_cuenta_origen_title", "id")));
		setImgIzquierda((ImageButton) findViewById(SuiteApp.getResourceId("img_izquierda", "id")));
		setImgDerecha((ImageButton) findViewById(SuiteApp.getResourceId("img_derecha", "id")));
		setVistaCtaOrigen((TextView) findViewById(SuiteApp.getResourceId("vista_cta_origen", "id")));
		componenteCtaOrigen = (LinearLayout) findViewById(SuiteApp.getResourceId("componente_cuenta_origen", "id"));

		getImgIzquierda().setOnClickListener(this);
		getImgDerecha().setOnClickListener(this);
		getVistaCtaOrigen().setOnClickListener(this);
		setSeleccionado(false);

		// Resize to screen.
		scaleForCurrentScreen();
	}
	
	public void init(){
		/*if (parentManager.getBaseDelegateForKey(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID) != null) {
			if(((MisCuentasDelegate)parentManager.getBaseDelegateForKey(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID)).getMisCuentasViewController().getMisCuentasOpciones() != null){
				Account cuenta = ((MisCuentasDelegate)parentManager.getBaseDelegateForKey(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID)).getMisCuentasViewController().getMisCuentasOpciones().getCuentaSeleccionada();
				if (cuenta != null) {
					setIndiceCuentaSeleccionada(getListaCuetasAMostrar().indexOf(cuenta));
				}
			}			
		}*/
		Account cuenta = listaCuetasAMostrar.get(indiceCuentaSeleccionada);		
		cuentaOrigenDelegate = new CuentaOrigenDelegate(cuenta);
		cuentaOrigenDelegate.setCuentaOrigenViewController(this);
		
		if (getListaCuetasAMostrar().size() < 2) {
			desactivarCuentaSiguientePrevia();
		} else if (getListaCuetasAMostrar().size() > 1) {
			activarCuentaSiguientePrevia();
		}
		cuentaOrigenDelegate.setListaCuentasOrigen(getListaCuetasAMostrar());
		cuentaOrigenDelegate.indiceCuenta = getIndiceCuentaSeleccionada();
		//actualizaComponente();
		actualizaComponente(false);
	}
	
	@Override
	public void onClick(View v) {
		if (v == getImgIzquierda()) {
			muestraCuentaPrevia();
		}else if (v == getImgDerecha()) {
			muestraCuentaSiguiente();
		} else if (v == getVistaCtaOrigen() && !parentManager.isActivityChanging()) {
			muestraListaCuentasView();
		}
	}

	public void muestraListaCuentasView() {
		((BmovilViewsController) parentManager).showCuentaOrigenListViewController();
		((BmovilViewsController) parentManager).addDelegateToHashMap(CuentaOrigenDelegate.CUENTA_ORIGEN_DELEGATE_ID, cuentaOrigenDelegate);
		//parentManager.showViewController(CuentaOrigenListViewController.class);
		//parentManager.addDelegateToHashMap(CuentaOrigenDelegate.CUENTA_ORIGEN_DELEGATE_ID, cuentaOrigenDelegate);
		setSeleccionado(true);
	}


	public void muestraCuentaSiguiente() {
		if(Server.ALLOW_LOG) Log.d("CuentaOrigen", "Presionaste flecha Derecha");
		getImgDerecha().setEnabled(false);
		getImgIzquierda().setEnabled(false);
		getVistaCtaOrigen().setEnabled(false);
		setSeleccionado(true);
		cuentaOrigenDelegate.setCuentaSiguiente();
		if (getIndiceCuentaSeleccionada() == getListaCuetasAMostrar().size() - 1) {
			setIndiceCuentaSeleccionada(0);
		} else {
			setIndiceCuentaSeleccionada(getIndiceCuentaSeleccionada() + 1);
		}
		//actualizaComponente();
		actualizaComponente(false);
	}

	public void muestraCuentaPrevia() {
		if(Server.ALLOW_LOG) Log.d("CuentaOrigen", "Presionaste flecha Izquierda");
		getImgDerecha().setEnabled(false);
		getImgIzquierda().setEnabled(false);
		getVistaCtaOrigen().setEnabled(false);
		setSeleccionado(true);
		cuentaOrigenDelegate.setCuentaPrevia();
		if (getIndiceCuentaSeleccionada() == 0) {
			setIndiceCuentaSeleccionada(getListaCuetasAMostrar().size() - 1);
		} else {
			setIndiceCuentaSeleccionada(getIndiceCuentaSeleccionada() - 1);
		}
		//actualizaComponente();
		actualizaComponente(false);
	}

	@SuppressWarnings("deprecation")
	public void desactivarCuentaSiguientePrevia() {
		componenteCtaOrigen
				.setBackgroundResource(R.drawable.cuenta_origen_sin_flechas);
//		LinearLayout.LayoutParams params = new LayoutParams(0,
//				LayoutParams.FILL_PARENT, 1.0f);
//		vistaCtaOrigen.setLayoutParams(params);
//		imgDerecha.setVisibility(View.GONE);
//		imgIzquierda.setVisibility(View.GONE);
		getImgDerecha().setVisibility(View.INVISIBLE);
		getImgIzquierda().setVisibility(View.INVISIBLE);
		getVistaCtaOrigen().setEnabled(false);
	}

	@SuppressWarnings("deprecation")
	public void activarCuentaSiguientePrevia() {
		componenteCtaOrigen
				.setBackgroundResource(R.drawable.cuenta_origen_con_flechas);
//		LinearLayout.LayoutParams params1 = new LayoutParams(0,
//				LayoutParams.FILL_PARENT, 0.70f);
//		LinearLayout.LayoutParams params2 = new LayoutParams(0,
//				LayoutParams.FILL_PARENT, 0.15f);
//		vistaCtaOrigen.setLayoutParams(params1);
//		imgDerecha.setLayoutParams(params2);
//		imgIzquierda.setLayoutParams(params2);
		getImgDerecha().setVisibility(View.VISIBLE);
		getImgIzquierda().setVisibility(View.VISIBLE);
		getVistaCtaOrigen().setEnabled(true);
	}

	public void setListaCuetasAMostrar(ArrayList<Account> listaCuetasAMostrar) {
		Iterator<Account> iterator = listaCuetasAMostrar.iterator();
		while (iterator.hasNext()) {
			Account account = iterator.next();
			if (account.getType().equals(Constants.INVERSION_TYPE)) {
				iterator.remove();
			}
		}
		this.listaCuetasAMostrar = listaCuetasAMostrar;
	}

	public CuentaOrigenDelegate getCuentaOrigenDelegate() {
		return cuentaOrigenDelegate;
	}

	public void setCuentaOrigenDelegate(CuentaOrigenDelegate cuentaOrigenDelegate) {
		this.cuentaOrigenDelegate = cuentaOrigenDelegate;
	}
	
	public BaseDelegate getDelegate() {
		return delegate;
	}

	public void setDelegate(BaseDelegate delegate) {
		this.delegate = delegate;
	}
	
	public void dejarDeMostrarLista(){
		//actualizaComponente();
		actualizaComponente(false);
		parentManager.getCurrentViewControllerApp().goBack();
	}

	public BaseViewsControllerCommons getParentManager() {
		return parentManager;
	}

	//public void actualizaComponente(){
	public void actualizaComponente(boolean mostrarAlias){
		cuentaOrigenDelegate.indiceCuenta = getIndiceCuentaSeleccionada();
		cuentaOrigenDelegate.actualizarCuentaSeleccionada();
		saldo = Tools.convertDoubleToBigDecimalAndReturnString(cuentaOrigenDelegate.getCuentaSeleccionada().getBalance());
		getVistaCtaOrigen().setText(cuentaOrigenDelegate.getCuentaSeleccionada().getPublicName(SuiteApp.appContext.getResources(),true) +
							   "\n"	+ 
							   Tools.formatAmount(saldo,false));
		//One CLick
		if(mostrarAlias){
			if(cuentaOrigenDelegate.getCuentaSeleccionada().getAlias().compareTo("")!=0){
				getVistaCtaOrigen().setText(cuentaOrigenDelegate.getCuentaSeleccionada().getAlias() + 
						   "\n"	+ 
						   Tools.formatAmount(saldo,false));
			}else{
				getVistaCtaOrigen().setText(cuentaOrigenDelegate.getCuentaSeleccionada().getPublicName(SuiteApp.appContext.getResources(),true) +
						   "\n"	+ 
						   Tools.formatAmount(saldo,false));
			}
			
		}else{
		getVistaCtaOrigen().setText(cuentaOrigenDelegate.getCuentaSeleccionada().getPublicName(SuiteApp.appContext.getResources(),true) +
							   "\n"	+ 
							   Tools.formatAmount(saldo,false));
		}

	}
	
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		
		guiTools.scale(getTituloComponenteCtaOrigen(), true);
		guiTools.scale(componenteCtaOrigen);
		guiTools.scale(getImgIzquierda());
		guiTools.scale(getVistaCtaOrigen(), true);
		guiTools.scale(getImgDerecha());
		
//		if(!GuiTools.isInitialized())
//			return;
//		
//		GuiTools guiTools = GuiTools.getCurrent();
//		LinearLayout.LayoutParams linearLayoutParams = null;
//		
//		linearLayoutParams = (LinearLayout.LayoutParams)tituloComponenteCtaOrigen.getLayoutParams();
//		tituloComponenteCtaOrigen.setLayoutParams(guiTools.scaleLayoutParams(linearLayoutParams));
//		guiTools.scalePaddings(tituloComponenteCtaOrigen);
//		guiTools.tryScaleText(tituloComponenteCtaOrigen);
//		
//		linearLayoutParams = (LinearLayout.LayoutParams)componenteCtaOrigen.getLayoutParams();
//		componenteCtaOrigen.setLayoutParams(guiTools.scaleLayoutParams(linearLayoutParams));
//		guiTools.scalePaddings(componenteCtaOrigen);
//		
//		linearLayoutParams = (LinearLayout.LayoutParams)imgIzquierda.getLayoutParams();
//		imgIzquierda.setLayoutParams(guiTools.scaleLayoutParams(linearLayoutParams));
//		guiTools.scalePaddings(imgIzquierda);
//		
//		linearLayoutParams = (LinearLayout.LayoutParams)imgDerecha.getLayoutParams();
//		imgDerecha.setLayoutParams(guiTools.scaleLayoutParams(linearLayoutParams));
//		guiTools.scalePaddings(imgDerecha);
//		
//		linearLayoutParams = (LinearLayout.LayoutParams)vistaCtaOrigen.getLayoutParams();
//		vistaCtaOrigen.setLayoutParams(guiTools.scaleLayoutParams(linearLayoutParams));
//		guiTools.scalePaddings(vistaCtaOrigen);
//		guiTools.tryScaleText(vistaCtaOrigen);
	}

	public ImageButton getImgDerecha() {
		return imgDerecha;
	}

	public void setImgDerecha(ImageButton imgDerecha) {
		this.imgDerecha = imgDerecha;
	}

	public ImageButton getImgIzquierda() {
		return imgIzquierda;
	}

	public void setImgIzquierda(ImageButton imgIzquierda) {
		this.imgIzquierda = imgIzquierda;
	}

	public TextView getVistaCtaOrigen() {
		return vistaCtaOrigen;
	}

	public void setVistaCtaOrigen(TextView vistaCtaOrigen) {
		this.vistaCtaOrigen = vistaCtaOrigen;
	}

	public ArrayList<Account> getListaCuetasAMostrar() {
		return listaCuetasAMostrar;
	}

	public TextView getTituloComponenteCtaOrigen() {
		return tituloComponenteCtaOrigen;
	}

	public void setTituloComponenteCtaOrigen(TextView tituloComponenteCtaOrigen) {
		this.tituloComponenteCtaOrigen = tituloComponenteCtaOrigen;
	}

	public int getIndiceCuentaSeleccionada() {
		return indiceCuentaSeleccionada;
	}

	public void setIndiceCuentaSeleccionada(int indiceCuentaSeleccionada) {
		if(indiceCuentaSeleccionada < 0)
			this.indiceCuentaSeleccionada = 0;
		else
			this.indiceCuentaSeleccionada = indiceCuentaSeleccionada;
	}

	public boolean isSeleccionado() {
		return seleccionado;
	}

	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
}
