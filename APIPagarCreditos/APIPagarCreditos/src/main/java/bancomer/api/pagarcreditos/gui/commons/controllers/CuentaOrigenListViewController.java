package bancomer.api.pagarcreditos.gui.commons.controllers;

import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.gui.commons.delegates.CuentaOrigenDelegate;
import bancomer.api.pagarcreditos.implementations.BaseViewController;
import bancomer.api.pagarcreditos.implementations.InitPagoCredito;
import bancomer.api.pagarcreditos.implementations.SuiteAppPagoCreditoApi;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;

public class CuentaOrigenListViewController extends BaseViewController {

    private LinearLayout vista;
    private ListaSeleccionViewController listaSeleccion;
    private ArrayList<Object> lista;
    private CuentaOrigenDelegate delegate;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_cuenta_origen_list_view);

        SuiteAppPagoCreditoApi suiteApp = SuiteAppPagoCreditoApi.getInstance();
        setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
        delegate = (CuentaOrigenDelegate) InitPagoCredito.initPagoCredito.getSuiteAppPagoCreditoApi().getBmovilApplication().getBmovilViewsController().getBaseDelegateForKey(CuentaOrigenDelegate.CUENTA_ORIGEN_DELEGATE_ID);
        vista = (LinearLayout) findViewById(R.id.cuenta_origen_list_view_layout);
        if (delegate.getTipoOperacion() == Constants.Operacion.transferir) {
            setTitle(R.string.bmovil_common_lista_cuentas_origen, R.drawable.bmovil_mis_cuentas_icono);
        } else if (delegate.getTipoOperacion() == Constants.Operacion.transferirInterbancaria) {
            setTitle(R.string.bmovil_common_lista_cuentas_origen, R.drawable.bmovil_mis_cuentas_icono);
        } else {
            setTitle(R.string.bmovil_common_lista_cuentas_origen, R.drawable.bmovil_mis_cuentas_icono);
        }

        ArrayList<Account> cuentas = delegate.getListaCuentaOrigen();
        ArrayList<Object> registros;

        lista = new ArrayList<Object>();
        for (int i = 0; i < cuentas.size(); i++) {
            registros = new ArrayList<Object>();
            registros.add(cuentas.get(i));
            registros.add(cuentas.get(i).getPublicName(getResources(), false));
            String saldo = Tools.convertDoubleToBigDecimalAndReturnString(cuentas.get(i).getBalance());
            registros.add(Tools.formatAmount(saldo, false));
            lista.add(registros);
        }

        LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        params.topMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_border_margin);
        params.leftMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
        params.rightMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_side_margin);
        params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.cuenta_origen_list_border_margin);

        listaSeleccion = new ListaSeleccionViewController(this, params, parentViewsController);
        listaSeleccion.setDelegate(delegate);
        listaSeleccion.setLista(lista);
        listaSeleccion.setTitle(getString(R.string.movimientos_selecciona_cuenta_retito));
        listaSeleccion.setNumeroColumnas(2);
        listaSeleccion.setOpcionSeleccionada(delegate.indiceCuenta);
        listaSeleccion.setSeleccionable(false);
        listaSeleccion.setAlturaFija(false);
        listaSeleccion.setNumeroFilas(lista.size());
        listaSeleccion.setExisteFiltro(false);
        listaSeleccion.setSingleLine(false);
        listaSeleccion.cargarTabla();
        vista.addView(listaSeleccion);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);

    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }
}
