package bancomer.api.pagarcreditos.gui.controllers;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.commons.GuiTools;
import bancomer.api.pagarcreditos.commons.TrackingHelper;
import bancomer.api.pagarcreditos.gui.commons.controllers.CuentaOrigenViewController;
import bancomer.api.pagarcreditos.gui.delegates.PagarCreditoDelegate;
import bancomer.api.pagarcreditos.implementations.BaseViewController;
import bancomer.api.pagarcreditos.implementations.BmovilViewsController;
import bancomer.api.pagarcreditos.implementations.SuiteAppPagoCreditoApi;
import bancomer.api.pagarcreditos.models.ImportesPagoCreditoData;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

public class PagarCreditoViewController extends BaseViewController implements View.OnClickListener {

    private PagarCreditoDelegate delegate;
    //AMZ
    public BmovilViewsController parentManager;
    //AMZ
    private ScrollView vista;
    private LinearLayout cuentaRetiroLO;
    private LinearLayout fechaCorteLayout;
    private LinearLayout listaTDCLayout;

    private LinearLayout errorGenericoLayout;

    private CuentaOrigenViewController componenteCtaOrigen;
    private ImageButton continuarButton;

    private TextView fechaCorte;
    private TextView dolar;
    private TextView pagoMinimo;
    private TextView tipoMoneda;
    private TextView valorMoneda;
    private Boolean marqueeEnabled;

    private RadioButton radioPagoMinimo;

    private LinearLayout radioPagoMinimolinear;

    public CuentaOrigenViewController getComponenteCtaOrigen() {
        return componenteCtaOrigen;
    }

    public void setComponenteCtaOrigen(
            CuentaOrigenViewController componenteCtaOrigen) {
        this.componenteCtaOrigen = componenteCtaOrigen;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.activity_pagar_credito_view_controller);
        //marqueeEnabled = true;
        setTitle(R.string.pago_credito_titulo, R.drawable.api_pago_credito_icono_pagar);
        //AMZ
        parentManager = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController();
        TrackingHelper.trackState("pagar tdc", parentManager.estados);

        setParentViewsController(SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController());
        delegate = (PagarCreditoDelegate) parentViewsController.getBaseDelegateForKey(PagarCreditoDelegate.PAGO_CREDITO_DELEGATE_ID);
        delegate.setPagarCreditoViewController(this);

        findViews();

        setDataFromPagoCredito();
        setUpRadios(false);

        scaleForCurrentScreen();
        muestraComponenteCuentaOrigen();
    }

    @SuppressWarnings("deprecation")
    public void muestraComponenteCuentaOrigen() {
        ArrayList<Account> listaCuetasAMostrar = delegate.cargaCuentasOrigen();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        componenteCtaOrigen = new CuentaOrigenViewController(this, params, parentViewsController, this);
        componenteCtaOrigen.getTituloComponenteCtaOrigen().setText(getString(R.string.transferir_detalle_cuenta_origen));
        componenteCtaOrigen.setDelegate(delegate);
        componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
        if (delegate.getCuentaOrigenSeleccionada() != null) {
            componenteCtaOrigen.setIndiceCuentaSeleccionada(listaCuetasAMostrar.indexOf(delegate.getCuentaOrigenSeleccionada()));
        } else {
            componenteCtaOrigen.setIndiceCuentaSeleccionada(0);
        }
        componenteCtaOrigen.init();
        componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.transferir);
        cuentaRetiroLO.addView(componenteCtaOrigen);
//		delegate.setCuentaSeleccionada(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada());
        cargaCuentaSeleccionada();
    }

    public void cargaCuentaSeleccionada() {
//		textCuentaDestino.setText(Tools.enmascaraCuentaDestino(delegate.getInnerTransaction().getCuentaDestino().getNumber()));
    }


    /********************************************************* CheckNegativosTDC *******************************************************/

    /**
     * Recupera los importes del radio pasado como parametro a excepcion de "Otra cantidad"
     *
     * @param radio
     * @return
     */
    private String getImporteByRadioNoOther(RadioButton radio) {
        String ret = "";
        if (delegate.getImportesPagoCredito() != null) {
            if (radio.equals(radioPagoMinimo)) {
                ret = delegate.getImportesPagoCredito().getPagoRequerido();
            }
        }
        return ret;
    }

    /**
     * Recupera la lista de todos los radios presentes en la ventana a excepcion de "Otra cantidad"
     *
     * @return
     */
    private ArrayList<RadioButton> getRadioListNoOther() {
        ArrayList<RadioButton> listaData = new ArrayList<RadioButton>();
        listaData.add(radioPagoMinimo);

        return listaData;
    }

    /**
     * Recupera el Linear superior al radio pasado como parametro el cual tiene el listener del click a excepcion de "Otra cantidad"
     *
     * @param radio
     * @return
     */
    private LinearLayout getRadioSuperViewNoOther(RadioButton radio) {
        LinearLayout ret = null;
        if (radio.equals(radioPagoMinimo)) {
            ret = radioPagoMinimolinear;
        }
        return ret;
    }

    /**
     * Comprueba si el importe del radio dado como parametro es negativo o no
     *
     * @param radio
     * @return
     */
    private Boolean isNegativeRadioNoOther(RadioButton radio) {
        return getImporteByRadioNoOther(radio).startsWith("-");
    }

    /**
     * Deshabilita el click en la lista de radios en caso de tener un importe negativo a excepcion de "Otra cantidad"
     *
     * @param lista
     * @param index
     */
    private void isNegativePagoCreditoData(ArrayList<RadioButton> lista, Integer index) {
        if (lista.size() == index) {
            // Finish
        } else {
            // Si se procesa el elemento

            RadioButton radio = lista.get(index);
            // Comprobamos negatividad
            if (isNegativeRadioNoOther(radio)) {
                deshabilitaRadioOnNegative(radio);
            }

            isNegativePagoCreditoData(lista, ++index);
        }
    }

    /**
     * Deshabilita el click en la lista de radios en caso de tener un importe negativo a excepcion de "Otra cantidad"
     */
    public void checkNegativeInPagoCreditoData() {
        if (delegate.getImportesPagoCredito() != null) {
            ArrayList<RadioButton> listaData = getRadioListNoOther();
            isNegativePagoCreditoData(listaData, 0);
        }
    }

    /**
     * Deshabilita un radio y su super Layout
     *
     * @param radio
     */
    private void deshabilitaRadioOnNegative(RadioButton radio) {
        radio.setEnabled(false);

        // Si tiene superLayout anulamos el click
        LinearLayout superView = getRadioSuperViewNoOther(radio);
        if (superView != null) superView.setOnClickListener(null);
    }

    /********************************************************* End CheckNegativosTDC *******************************************************/

    /**
     * Establece el comportamiento de los radios al entrar a la ventana
     *
     * @param esError
     */
    public void setUpRadios(Boolean esError) {
        if (esError) {
            // Tratamiento de los combos en caso de que haya error en la peticion de importes tdc
            //changeRadio(radioOtraCantidad);
        } else {
            // Tratamiento normal de los combos

            // Comprobamos negatividad
            checkNegativeInPagoCreditoData();
        }
    }

    private void changeRadio(RadioButton button) {
        radioPagoMinimo.setChecked(false);
        if (button.isChecked()) {
            button.setChecked(false);
        } else {
            button.setChecked(true);
        }
    }

    public String getImporteRaw() {
        String importe = "0";

        if (radioPagoMinimo.isChecked()) {
            importe = delegate.getImportesPagoCredito().getPagoRequerido();
        }
        if (Server.ALLOW_LOG) System.out.println("Importe Raw: " + importe);
        return importe;
    }

    public String getImporte() {
        String importe = Tools.formatAmount("0", false);
        if (radioPagoMinimo.isChecked()) {
            importe = pagoMinimo.getText().toString();
        }
        if (Server.ALLOW_LOG) System.out.println("Importe: " + importe);
        return importe;
    }

    private Boolean validaImporte() {
        Boolean todoOk = false;
        try {
            String pagoRequerido = delegate.getImportesPagoCredito().getPagoRequerido();
            if (!pagoRequerido.matches("\\d+")) {
                showInformationAlert(getString(R.string.pago_credito_sin_recibos_pendientes_alert), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        goBack();
                    }
                });
            } else if (!pagoRequerido.equals("000")) {
                if (Double.parseDouble(pagoRequerido.substring(0, pagoRequerido.length() - 2)) > delegate.getCuentaOrigen().getBalance()) {
                    showInformationAlert(getString(R.string.pago_credito_saldo_insuficiente_alert));
                } else {
                    todoOk = true;
                }
            } else {
                showInformationAlert(getString(R.string.pago_credito_sin_recibos_pendientes_alert), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        goBack();
                    }
                });
            }
        } catch (Exception e) {
            Log.d("PagarCreditoViewController", "excepcion validaImporte:  " + e.getMessage());
        }
        return todoOk;
    }

    @Override
    public void onClick(View v) {
        if (v.equals((LinearLayout) findViewById(R.id.linear1))) {
            changeRadio(radioPagoMinimo);

        } else if (v.equals(radioPagoMinimo)) {
            changeRadio(radioPagoMinimo);
        } else if (v.equals(continuarButton)) {
            if (validaImporte()) {
                delegate.setDataPagoCredito();
                this.delegate.showConfirmacion();
            }
        }
    }

    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    public void goBack() {
        super.goBack();
    }

    public void actualizaCuentaOrigen(Account cuenta) {
        if (Server.ALLOW_LOG)
            Log.d("TransferirMisCuentasDetalleViewController", "Actualizar la cuenta origen actual por  " + cuenta.getNumber());
        delegate.setCuentaOrigenSeleccionada(cuenta);
        componenteCtaOrigen.getImgDerecha().setEnabled(true);
        componenteCtaOrigen.getImgIzquierda().setEnabled(true);
        componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
    }

    private void findViews() {
        vista = (ScrollView) findViewById(R.id.pagoTDCLL);
        cuentaRetiroLO = (LinearLayout) findViewById(R.id.transfer_mis_cuentas_layout);

        fechaCorte = (TextView) findViewById(R.id.fechaCorte);

        dolar = (TextView) findViewById(R.id.dolar);
        pagoMinimo = (TextView) findViewById(R.id.valor_dolar);

        pagoMinimo.setSelected(true);
        pagoMinimo.setSingleLine(true);


        tipoMoneda = (TextView) findViewById(R.id.udi);
        valorMoneda = (TextView) findViewById(R.id.valor_udi);

        continuarButton = (ImageButton) findViewById(R.id.PTDC_boton_continuar);
        continuarButton.setOnClickListener(this);

        radioPagoMinimo = (RadioButton) findViewById(R.id.radioButton1);
        radioPagoMinimo.setOnClickListener(this);

		/* Texto a ocultar */
        fechaCorteLayout = (LinearLayout) findViewById(R.id.linear01);
        listaTDCLayout = (LinearLayout) findViewById(R.id.lista);

        //inicialmente esta oculto
        errorGenericoLayout = (LinearLayout) findViewById(R.id.labelErrorTdc);
        errorGenericoLayout.setVisibility(View.GONE);

        // Inicializar boton linears
        radioPagoMinimolinear = (LinearLayout) findViewById(R.id.linear1);
        radioPagoMinimolinear.setOnClickListener(this);
    }

    private void scaleForCurrentScreen() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.rootLayout));

        guiTools.scale(findViewById(R.id.transfer_mis_cuentas_layout));
        guiTools.scale(findViewById(R.id.detalle_transfer_cuenta_destino_text), true);
        guiTools.scale(findViewById(R.id.detalle_transfer_cuenta_destino_edit));

        guiTools.scale(findViewById(R.id.accdataLayout));
        guiTools.scale(findViewById(R.id.listaFechas));
        guiTools.scale(findViewById(R.id.linear01));
        guiTools.scale(findViewById(R.id.fechPago), true);
        guiTools.scale(findViewById(R.id.fechaCorte), true);

        guiTools.scale(findViewById(R.id.labelErrorTdc));
        guiTools.scale(findViewById(R.id.errorTdc), true);

        guiTools.scale(findViewById(R.id.importe), true);

        guiTools.scale(findViewById(R.id.lista));
        guiTools.scale(findViewById(R.id.linear1));

        guiTools.scale(findViewById(R.id.pagoreq), true);

        guiTools.scale(findViewById(R.id.linear02));
        guiTools.scale(findViewById(R.id.linear03));
        guiTools.scale(findViewById(R.id.dolar), true);
        guiTools.scale(findViewById(R.id.udi), true);
        guiTools.scale(findViewById(R.id.linear04));
        guiTools.scale(findViewById(R.id.valor_udi), true);
        guiTools.scale(findViewById(R.id.valor_dolar), true);

        guiTools.scale(findViewById(R.id.radioButton1));

        guiTools.scale(findViewById(R.id.detalle_transfer_importe_edit), true);

        guiTools.scale(findViewById(R.id.continuarlayout));
        guiTools.scale(continuarButton);
    }

    public void setDataFromPagoCredito() {
        String fecha = delegate.getImportesPagoCredito().getFechaPago();
        if (fecha.matches("\\d{4}/\\d{2}/\\d{2}")) {
            fechaCorte.setText(Tools.formatDateTDC(fecha));
        } else {
            if (fecha.compareTo("En actualizaciÃÂ³n") == 0) {
                fecha = "En actualización";
            }
            fechaCorte.setText(fecha);
        }

        String pagoRequerido = delegate.getImportesPagoCredito().getPagoRequerido();
        ImportesPagoCreditoData importesPagoCredito = delegate.getImportesPagoCredito();
        dolar.setVisibility(View.INVISIBLE);
        if (pagoRequerido.matches("\\d+")) {
            boolean isNegative = pagoRequerido.startsWith("-");
            pagoRequerido = Tools.formatAmount(pagoRequerido, isNegative);
            if (importesPagoCredito.getMoneda().compareTo("UDI P") == 0 || importesPagoCredito.getMoneda().compareTo("VSM") == 0) {
                pagoRequerido = pagoRequerido.substring(1);
                dolar.setVisibility(View.VISIBLE);
            }
        } else {
            if (pagoRequerido.compareTo("En actualizaciÃÂ³n") == 0) {
                pagoRequerido = "En actualización";
            }
            pagoMinimo.getLayout().getOffsetToLeftOf(-50);
        }
        pagoMinimo.setText(pagoRequerido);
        dolar.setText("$");

        if (importesPagoCredito.getMoneda().compareTo("UDI P") == 0 || importesPagoCredito.getMoneda().compareTo("VSM") == 0) {
            String tipoMoneda;
            if (importesPagoCredito.getMoneda().compareTo("UDI P") == 0) {
                tipoMoneda = getString(R.string.pago_credito_result_lista_udi);
            } else {
                tipoMoneda = getString(R.string.pago_credito_result_lista_vsm);
            }
            this.tipoMoneda.setText(tipoMoneda);
            String pagoRequeridoMoneda = importesPagoCredito.getPagoRequeridoMoneda();
            if (pagoRequeridoMoneda.matches("\\d+")) {
                boolean isNegative2 = pagoRequeridoMoneda.startsWith("-");
                pagoRequeridoMoneda = Tools.formatAmount(pagoRequeridoMoneda, isNegative2).substring(1);
                this.tipoMoneda.setVisibility(View.VISIBLE);
                importesPagoCredito.setPagoRequeridoMoneda(pagoRequeridoMoneda);
            } else {
                this.tipoMoneda.setVisibility(View.INVISIBLE);
            }
            valorMoneda.setText(pagoRequeridoMoneda);
            valorMoneda.setVisibility(View.VISIBLE);
        } else {
            ((LinearLayout) findViewById(R.id.linear1)).getLayoutParams().height *= 0.66;
        }
    }

    @SuppressWarnings("deprecation")
    public void muestraListaCuentas() {
        componenteCtaOrigen.getImgDerecha().setEnabled(true);
        componenteCtaOrigen.getImgIzquierda().setEnabled(true);
        componenteCtaOrigen.getVistaCtaOrigen().setEnabled((componenteCtaOrigen.getListaCuetasAMostrar().size() > 1));
    }

    public void ocultaDatosPagoCredito() {
        listaTDCLayout.setVisibility(View.GONE);
        fechaCorteLayout.setVisibility(View.GONE);
    }

    public void muestraErrorGenericoPagoCredito() {
        errorGenericoLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public PagarCreditoDelegate getDelegate() {
        return delegate;
    }

    public void setDelegate(PagarCreditoDelegate delegate) {
        this.delegate = delegate;
    }
}