package bancomer.api.pagarcreditos.implementations;

import android.content.Context;
import android.view.View;

import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.base.callback.CallBackSession;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

public class SuiteAppPagoCreditoApi extends com.bancomer.base.SuiteApp {
    public static boolean isSubAppRunning;
    public static Context appContext;
    private static SuiteAppPagoCreditoApi me = new SuiteAppPagoCreditoApi();
    private static BaseViewControllerCommons intentMenuAdministrar;
    private static BaseViewControllerCommons intentMenuPrincipal;
    private static BaseViewControllerCommons intentConfirmacionAut;
    private static BaseViewControllerCommons intentMenuSuit;
    private static BaseViewControllerCommons cambioPerfil;
    private static CallBackSession callBackSession;
    private static CallBackBConnect callBackBConnect;
    private SuiteViewsController suiteViewsController;
    // #region BmovilApp
    private BmovilApp bmovilApplication;

    public static BaseViewControllerCommons getIntentMenuAdministrar() {
        return intentMenuAdministrar;
    }

    public static void setIntentMenuAdministrar(
            BaseViewControllerCommons intentMenuAdministrar) {
        SuiteAppPagoCreditoApi.intentMenuAdministrar = intentMenuAdministrar;
    }

    public static BaseViewControllerCommons getIntentMenuPrincipal() {
        return intentMenuPrincipal;
    }

    public static void setIntentMenuPrincipal(
            BaseViewControllerCommons intentMenuPrincipal) {
        SuiteAppPagoCreditoApi.intentMenuPrincipal = intentMenuPrincipal;
    }

    public static BaseViewControllerCommons getIntentConfirmacionAut() {
        return intentConfirmacionAut;
    }

    public static void setIntentConfirmacionAut(
            BaseViewControllerCommons intentConfirmacionAut) {
        SuiteAppPagoCreditoApi.intentConfirmacionAut = intentConfirmacionAut;
    }

    public static BaseViewControllerCommons getIntentMenuSuit() {
        return SuiteAppPagoCreditoApi.intentMenuSuit;
    }

    public static void setIntentMenuSuit(BaseViewControllerCommons intentMenuSuit) {
        SuiteAppPagoCreditoApi.intentMenuSuit = intentMenuSuit;
    }

    public static BaseViewControllerCommons getCambioPerfil() {
        return cambioPerfil;
    }

    public static void setCambioPerfil(BaseViewControllerCommons cambioPerfil) {
        SuiteAppPagoCreditoApi.cambioPerfil = cambioPerfil;
    }

    ;

    public static SuiteAppPagoCreditoApi getInstance() {
        return me;
    }

    public static boolean getBmovilStatus() {
        return PropertiesManager.getCurrent().getBmovilActivated();
    }

    public static boolean getSofttokenStatus() {
        return PropertiesManager.getCurrent().getSofttokenActivated();
    }

    public static int getResourceId(String nombre, String tipo) {
        return appContext.getResources().getIdentifier(nombre, tipo, appContext.getPackageName());
    }

    public static int getResourceId(String nombre, String tipo, View vista) {
        return vista.getResources().getIdentifier(nombre, tipo, appContext.getPackageName());
    }

    public static CallBackSession getCallBackSession() {
        return callBackSession;
    }

    public static void setCallBackSession(CallBackSession callBackSession) {
        SuiteAppPagoCreditoApi.callBackSession = callBackSession;
    }

    public static CallBackBConnect getCallBackBConnect() {
        return callBackBConnect;
    }

    public static void setCallBackBConnect(CallBackBConnect callBackBConnect) {
        SuiteAppPagoCreditoApi.callBackBConnect = callBackBConnect;
    }
    // #endregion

    // #region SofttokenApp
//	private SofttokenApp softtokenApp;

//	public SofttokenApp getSofttokenApplication() {
//		if(softtokenApp == null)
//			startSofttokenApp();
//		return softtokenApp;
//	}

    public void onCreate(Context context) {
        super.onCreate(context);
        suiteViewsController = new SuiteViewsController();
        isSubAppRunning = false;

        appContext = context;
        me = this;

        startBmovilApp();
    }

//	public void startSofttokenApp() {
//		softtokenApp = new SofttokenApp(this);
//		isSubAppRunning = true;
//	}

//	public void closeSofttokenApp() {
//		if(null == softtokenApp)
//			return;
//		softtokenApp.cierraAplicacion();
//		softtokenApp = null;
//		isSubAppRunning = false;
//		
//		if(suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController)
//			((MenuSuiteViewController)suiteViewsController.getCurrentViewControllerApp()).setShouldHideLogin(true);
//		
//		suiteViewsController.showMenuSuite(true);
//	}
    // #endregion

    public void cierraAplicacionSuite() {
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public SuiteViewsController getSuiteViewsController() {
        return suiteViewsController;
    }

    public BmovilApp getBmovilApplication() {
        if (bmovilApplication == null)
            bmovilApplication = new BmovilApp(this);
        return bmovilApplication;
    }

    public void startBmovilApp() {
        bmovilApplication = new BmovilApp(this);
        isSubAppRunning = true;
    }
    /*public void showViewController(Class<?> viewController, int flags, boolean inverted, String[] extrasKeys, Object[] extras,Activity currentActivity) {
		//getBmovilApplicationApi().getBmovilViewsController().showViewController( viewController,  flags,  inverted,  extrasKeys,  extras);
        suiteViewsController.setActivityContext(currentActivity);
		suiteViewsController.showViewController(viewController, flags, inverted, extrasKeys, extras);
	}*/

    public void cierraAplicacionBmovil() {
        bmovilApplication.cierraAplicacion();
        bmovilApplication = null;
        isSubAppRunning = false;
        //reiniciaAplicacionBmovil();
    }

    public void reiniciaAplicacionBmovil() {
        if (bmovilApplication == null)
            bmovilApplication = new BmovilApp(this);
        bmovilApplication.reiniciaAplicacion();
        isSubAppRunning = true;
    }

    public void closeBmovilAppSession() {

//		if(suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController)
//			((MenuSuiteViewController)suiteViewsController.getCurrentViewControllerApp()).setShouldHideLogin(true);
//
//		suiteViewsController.showMenuSuite(true);


        SuiteAppPagoCreditoApi suiteApp = SuiteAppPagoCreditoApi.getInstance();
        Session session = Session.getInstance(suiteApp.getApplicationContext());

        if (session.getValidity() == Session.VALID_STATUS) {
            session.setValidity(Session.INVALID_STATUS);
        }

        bmovilApplication.closeBmovilAppSession(suiteViewsController.getCurrentViewControllerApp());

    }

    public void addDelegateToHashMap(long id, BaseDelegateCommons delegateCommons) {
        suiteViewsController.addDelegateToHashMap(id, delegateCommons);
    }

    public void showViewController(Class<?> viewController, int flags, boolean inverted, String[] extrasKeys, Object[] extras) {
        //getBmovilApplicationApi().getBmovilViewsController().showViewController( viewController,  flags,  inverted,  extrasKeys,  extras);
        suiteViewsController.showViewController(viewController, flags, inverted, extrasKeys, extras);
    }

}
