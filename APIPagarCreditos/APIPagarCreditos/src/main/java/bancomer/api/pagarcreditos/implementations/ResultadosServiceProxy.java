/**
 *
 */
package bancomer.api.pagarcreditos.implementations;

import android.util.Log;

import com.bancomer.base.SuiteApp;

import java.util.HashMap;
import java.util.Map;

import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.commons.TrackingHelper;
import suitebancomer.aplicaciones.resultados.proxys.IResultadosServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ParamTo;
import suitebancomer.aplicaciones.resultados.to.ResultadosViewTo;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

/**
 * @author lbermejo
 */
public class ResultadosServiceProxy implements IResultadosServiceProxy {

    /**
     */
    private static final long serialVersionUID = 3925667201345924488L;
    private BaseDelegateCommons baseDelegateCommons;

    public ResultadosServiceProxy(BaseDelegateCommons bdc) {
        this.baseDelegateCommons = bdc;
    }

    public BaseDelegateCommons getDelegate() {
        return baseDelegateCommons;
    }

    @Override
    public ResultadosViewTo getListaDatos() {

        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getListaDatos >> delegate");
        ResultadosViewTo to = new ResultadosViewTo();

        //si el delegate es instanceof ConsultaInterbancariosDelegate enviar true
        final ResultadosDelegate delegate = (ResultadosDelegate) baseDelegateCommons;
        to.setListaDatos(delegate.getOperationDelegate().getDatosTablaResultados());
        //if (delegate.getOperationDelegate() instanceof InterbancariosDelegate) {
        //if (!((InterbancariosDelegate) delegate.getOperationDelegate()).isBajaFrecuente()
        //		&& ((InterbancariosDelegate) delegate.getOperationDelegate()).validaTC()) {

        //resultadosViewController.setListaClave(((InterbancariosDelegate)
        //delegate.getOperationDelegate()).getDatosTablaClave());
        //to.setListaClave(((InterbancariosDelegate)
        //		delegate.getOperationDelegate()).getDatosTablaClave());
        //}
        return to;
    }

    @Override
    public ResultadosViewTo showFields() {

        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy showFields >> delegate");

        ResultadosViewTo to = new ResultadosViewTo();
        final ResultadosDelegate delegate = (ResultadosDelegate) baseDelegateCommons;
        to.setTitulo(delegate.getTextoTituloResultado());
        to.setTexto(delegate.getTextoPantallaResultados());
        to.setInstrucciones(delegate.getTextoAyudaResultados());
        to.setTituloTextoEspecial(delegate.getTituloTextoEspecialResultados());
        to.setTextoEspecial(delegate.getTextoEspecialResultados());
        to.setColorTituloResultado(delegate.getColorTituloResultado());

        //to.setTextoAyudaInsSeg(
        //			delegate.getTextoAyudaInstrumentoSeguridad(
        //						to.getInstrumentoSeguridad()));

        return to;
    }

    @Override
    public void setParamStateParentManager() {

        if (Server.ALLOW_LOG)
            Log.d(getClass().getName(), ">>proxy setParamStateParentManager >> delegate");
        BmovilViewsController parentManager = SuiteAppPagoCreditoApi.getInstance()
                .getBmovilApplication().getBmovilViewsController();
        String rem = null;
        int opc = parentManager.estados.size() - 1;
        int opc2 = parentManager.estados.size() - 2;
        if (parentManager.estados.get(opc) == "opciones") {
            rem = parentManager.estados.remove(opc);
        } else if (parentManager.estados.get(opc2) == "sms"
                || parentManager.estados.get(opc2) == "correo"
                || parentManager.estados.get(opc2) == "alta frecuentes") {
            rem = parentManager.estados.remove(opc2);
            rem = parentManager.estados
                    .remove(parentManager.estados.size() - 1);
        } else {
            TrackingHelper.trackState("opciones", parentManager.estados);
        }

    }

    public ResultadosViewTo getOnPrepareOptionsMenu() {

        if (Server.ALLOW_LOG)
            Log.d(getClass().getName(), ">>proxy getOnPrepareOptionsMenu >> delegate");
        final ResultadosDelegate delegate = (ResultadosDelegate) baseDelegateCommons;
        ResultadosViewTo to = new ResultadosViewTo();
        //int opcionesMenu = resultadosDelegate.getOpcionesMenuResultados();
        to.setOpcionesMenu(delegate.getOpcionesMenuResultados());
        to.setFrecOpOk(delegate.getFrecOpOK());
        return to;
    }

    public Boolean onOptionsItemSelected(Integer itemId) {

        if (Server.ALLOW_LOG)
            Log.d(getClass().getName(), ">>proxy onOptionsItemSelected >> delegate");
        final ResultadosDelegate resultadosDelegate =
                (ResultadosDelegate) baseDelegateCommons;
        BmovilViewsController parentManager = SuiteAppPagoCreditoApi.getInstance()
                .getBmovilApplication().getBmovilViewsController();

        //SuiteApp.getInstance().getString
        String textoEncabezado = SuiteApp.getInstance().getString(
                resultadosDelegate.consultaOperationDelegate().getTextoEncabezado());

        Map<String, Object> envioConfirmacionMap =
                new HashMap<String, Object>();
        if (itemId == R.id.save_menu_sms_button) {
            resultadosDelegate.enviaSMS();
            //AMZ
            int sms = parentManager.estados.size() - 1;
            if (parentManager.estados.get(sms) == "resul") {
                String rem = parentManager.estados.remove(sms);
                rem = parentManager.estados.remove(parentManager.estados.size() - 1);
            }
            TrackingHelper.trackState("sms", parentManager.estados);
            TrackingHelper.trackState("resul", parentManager.estados);
            if (textoEncabezado == SuiteApp.getInstance().getString(
                    R.string.opcionesTransfer_menu_miscuentas)) {
                //ARR
                envioConfirmacionMap.put("events", "event17");
                envioConfirmacionMap.put("eVar17", "sms:transferencias+mis cuentas");

                TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
            } else if (textoEncabezado == SuiteApp.getInstance().getString(
                    R.string.opcionesTransfer_menu_otrascuentasbbva)) {
                //ARR
                envioConfirmacionMap.put("events", "event17");
                envioConfirmacionMap.put("eVar17", "sms:transferencias+otra cuenta bbva bancomer");

                TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
            } else if (textoEncabezado == SuiteApp.getInstance().getString(
                    R.string.opcionesTransfer_menu_cuentaexpress)) {
                //ARR
                envioConfirmacionMap.put("events", "event17");
                envioConfirmacionMap.put("eVar17", "sms:transferencias+cuenta express");

                TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
            } else if (textoEncabezado == SuiteApp.getInstance().getString(
                    R.string.opcionesTransfer_menu_otrosbancos)) {
                //ARR
                envioConfirmacionMap.put("events", "event17");
                envioConfirmacionMap.put("eVar17", "sms:transferencias+otros bancos");

                TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
            } else if (textoEncabezado == SuiteApp.getInstance().getString(
                    R.string.opcionesTransfer_menu_dineromovil)) {
                //ARR
                envioConfirmacionMap.put("events", "event17");
                envioConfirmacionMap.put("eVar17", "sms:transferencias+dinero movil");

                TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
            }
            return true;
        } else if (itemId == R.id.save_menu_email_button) {
            resultadosDelegate.enviaEmail();
            //AMZ
            int correo = parentManager.estados.size() - 1;
            if (parentManager.estados.get(correo) == "resul") {
                String rem = parentManager.estados.remove(correo);
                rem = parentManager.estados.remove(parentManager.estados.size() - 1);
            }
            if (textoEncabezado == SuiteApp.getInstance().getString(
                    R.string.opcionesTransfer_menu_miscuentas)) {
                //ARR
                envioConfirmacionMap.put("events", "event17");
                envioConfirmacionMap.put("eVar17", "correo:transferencias+mis cuentas");

                TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
            } else if (textoEncabezado == SuiteApp.getInstance().getString(
                    R.string.opcionesTransfer_menu_otrascuentasbbva)) {
                //ARR
                envioConfirmacionMap.put("events", "event17");
                envioConfirmacionMap.put("eVar17", "correo:transferencias+otra cuenta bbva bancomer");

                TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
            } else if (textoEncabezado == SuiteApp.getInstance().getString(
                    R.string.opcionesTransfer_menu_cuentaexpress)) {
                //ARR
                envioConfirmacionMap.put("events", "event17");
                envioConfirmacionMap.put("eVar17", "correo:transferencias+cuenta express");

                TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
            } else if (textoEncabezado == SuiteApp.getInstance().getString(
                    R.string.opcionesTransfer_menu_otrosbancos)) {
                //ARR
                envioConfirmacionMap.put("events", "event17");
                envioConfirmacionMap.put("eVar17", "correo:transferencias+otros bancos");

                TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
            } else if (textoEncabezado == SuiteApp.getInstance().getString(
                    R.string.opcionesTransfer_menu_dineromovil)) {
                //ARR
                envioConfirmacionMap.put("events", "event17");
                envioConfirmacionMap.put("eVar17", "correo:transferencias+dinero movil");

                TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
            }
            return true;
        } else if (itemId == R.id.save_menu_pdf_button) {
            resultadosDelegate.guardaPDF();
            return true;
        } else if (itemId == R.id.save_menu_frecuente_button) {
            resultadosDelegate.guardaFrecuente();
            //AMZ
            int frec = parentManager.estados.size() - 1;
            if (parentManager.estados.get(frec) == "resul") {
                String rem = parentManager.estados.remove(frec);
                rem = parentManager.estados.remove(parentManager.estados.size() - 1);
            }
            return true;
        } else if (itemId == R.id.save_menu_rapida_button) {
            resultadosDelegate.guardaRapido();
            return true;
        } else if (itemId == R.id.save_menu_borrar_button) {
            resultadosDelegate.borraRapido();
            return true;
        } else {
            return false;
        }
    }

    public void botonMenuClick() {

        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy botonMenuClick >> delegate");
        /*
		 * //AMZ    en el proxy
		((BmovilViewsController)parentViewsController).touchMenu();
		parentViewsController.removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		parentViewsController.removeDelegateFromHashMap(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
		((BmovilViewsController)parentViewsController).showMenuPrincipal(true);
		 *
		 */

        BmovilViewsController parentManager = SuiteAppPagoCreditoApi.getInstance()
                .getBmovilApplication().getBmovilViewsController();
        parentManager.touchMenu();
        parentManager.removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
        parentManager.removeDelegateFromHashMap(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
        parentManager.showMenuPrincipal();

    }

    @Override
    public Boolean doOperation(ParamTo to) {
        return Boolean.TRUE;
    }

}
