package bancomer.api.pagarcreditos.implementations;

import android.content.res.Resources;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.models.CambioCuentaResult;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

import static suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.hideAccountNumber;

public class CambioCuentaDelegate extends DelegateBaseAutenticacion {

    /**
     *
     */
    public static final long CAMBIO_CUENTA_DELEGATE_ID = 6392075930222254965L;
    private CambioCuentaViewController viewController;
    //private String folio;

	/*
	 * Componentes de autenticacion
	 */

    /**
     * @return true si se debe mostrar contrasena, false en caso contrario.
     */
    @Override
    public boolean mostrarContrasenia() {
        Perfil perfil = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
        boolean value = Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.cambioCuenta,
                perfil);
        return value;
    }

    /**
     * @return true si se debe mostrar CVV, false en caso contrario.
     */
    @Override
    public boolean mostrarCVV() {
        Perfil perfil = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
        boolean value = Autenticacion.getInstance().mostrarCVV(Constants.Operacion.cambioCuenta, perfil);
        return value;
    }

    /**
     * @return true si se debe mostrar NIP, false en caso contrario.
     */
    @Override
    public boolean mostrarNIP() {
        Perfil perfil = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
        boolean value = Autenticacion.getInstance().mostrarNIP(Constants.Operacion.cambioCuenta,
                perfil);
        return value;
    }

    /**
     * @return El tipo de token a mostrar
     */
    @Override
    public TipoOtpAutenticacion tokenAMostrar() {
        Perfil perfil = Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
        TipoOtpAutenticacion tipoOTP;
        try {
            tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.cambioCuenta,
                    perfil);
        } catch (Exception ex) {
            if (Server.ALLOW_LOG)
                Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
            tipoOTP = null;
        }
        return tipoOTP;
    }

    public CambioCuentaViewController getViewController() {
        return viewController;
    }

    public void setViewController(CambioCuentaViewController viewController) {
        this.viewController = viewController;
    }

    @Override
    public ArrayList<Object> getDatosTablaConfirmacion() {

        ArrayList<Object> tabla = new ArrayList<Object>();
        ArrayList<String> fila;
        Resources res = viewController.getResources();
        Account selectedAccount = viewController.getCambioCuenta().getAccount();
        fila = new ArrayList<String>();
        fila.add(selectedAccount.getNombreTipoCuenta(SuiteAppPagoCreditoApi.appContext.getResources()));
        fila.add(hideAccountNumber(selectedAccount.getNumber()));
        tabla.add(fila);
        return tabla;
    }

    @Override
    public ArrayList<Object> getDatosTablaResultados() {
        ArrayList<Object> tabla = getDatosTablaConfirmacion(); //new ArrayList<Object>();
//		ArrayList<String> fila;
//		
//		Resources res = viewController.getResources();
//		String cuentaNueva = viewController.getCambioCuenta().getAccount().getPublicName(res, true);	
//		fila = new ArrayList<String>();
//		fila.add(viewController.getString(R.string.accounttype_check));
//		fila.add(cuentaNueva);
//		tabla.add(fila);
//		fila = new ArrayList<String>();
//		fila.add(viewController.getString(R.string.result_page));
//		fila.add(folio);
//		tabla.add(fila);		
//		
        return tabla;
    }

    /**
     * Invoca la operacion para hacer el cambio de cuenta asociada
     */
    @Override
    public void realizaOperacion(ConfirmacionAutenticacionViewController confirmacionAutenticacionViewController, String contrasenia, String nip, String token, String cvv, String campoTarjeta) {

        int operationId = Server.CAMBIO_CUENTA;//OP Cambio de cuenta
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        Autenticacion aut = Autenticacion.getInstance();
        Perfil perfil = session.getClientProfile();

        String companiaCelular = session.getCompaniaUsuario();
        String cuentaAnterior = Tools.obtenerCuentaEje().getFullNumber();
        String cuentaNueva = viewController.getCambioCuenta().getAccount().getFullNumber();
        String cadAutenticacion = aut.getCadenaAutenticacion(Constants.Operacion.cambioCuenta, perfil);

        Hashtable<String, String> params = new Hashtable<String, String>();

        params.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
        params.put("numeroCliente", session.getClientNumber());
        params.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        params.put(ServerConstants.CVE_ACCESO, contrasenia == null ? "" : contrasenia);
        params.put(ServerConstants.COMPANIA_CELULAR, companiaCelular);
        params.put(Server.J_CUENTA_A, cuentaAnterior);
        params.put(Server.J_CUENTA_N, cuentaNueva);
        params.put(Server.J_NIP, nip == null ? "" : nip);
        params.put(Server.J_CVV2, cvv == null ? "" : cvv);
        params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
        params.put(Server.J_AUT, cadAutenticacion);
        params.put("tarjeta5Dig", campoTarjeta == null ? "" : campoTarjeta);

        params.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
        List<String> listaEncriptar = Arrays.asList(ServerConstants.CVE_ACCESO,
                Server.J_NIP, Server.J_CVV2);
        Encripcion.setContext(SuiteAppPagoCreditoApi.appContext);
        Encripcion.encriptaCadenaAutenticacion(params, listaEncriptar);

        //JAIG
        doNetworkOperation(operationId, params, true, new CambioCuentaResult(), confirmacionAutenticacionViewController);
        //		doNetworkOperation(operationId, params, confirmacionAutenticacionViewController);
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson, ParsingHandler handler, BaseViewController caller) {
        ((BmovilViewsController) viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, caller);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
            //cambio de cuenta exitoso
            if (response.getResponse() instanceof CambioCuentaResult) {
                CambioCuentaResult result = (CambioCuentaResult) response.getResponse();
//				folio = result.getFolio();
                actualizarCuentas(result.getAsuntos());
                showResultados();
            }
        } else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
            //cambio de cuenta no exitoso
            //viewController.showInformationAlert(response.getMessageText());
            BaseViewControllerCommons current = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp();
            current.showInformationAlert(response.getMessageText());
        }

    }

    /**
     * Actualiza las cuentas almacenadas en session
     * con las cuentas recibidas al cambiar la cuenta asociada.
     *
     * @param accounts
     */
    private void actualizarCuentas(Account[] accounts) {
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        session.updateAccounts(accounts);
    }

    /**
     * Muestra la pantalla de resultados.
     */
    private void showResultados() {
        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController()
                .showResultadosViewController(this, -1, -1);
    }

    /**
     * Muestra la pantalla de confirmacion
     */
    public void showConfirmacion() {
        int resSubtitle = R.string.confirmation_subtitulo;
        int resTitleColor = R.color.primer_azul;
        int resIcon = R.drawable.bmovil_cambiocuenta_icono;
        int resTitle = R.string.bmovil_cambio_cuenta_title;
        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(this, resIcon, resTitle, resSubtitle, resTitleColor);
    }

    /**
     * Sin opciones del menu
     */
    @Override
    public int getOpcionesMenuResultados() {
        return 0;
    }

    /**
     * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen.
     *
     * @return ArrayList<Account> con las cuentas del usuario
     */
    public ArrayList<Object> getCuentasUsuario() {
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        Account[] accounts = session.getAccounts();
        ArrayList<Object> accountsArray = new ArrayList<Object>(accounts.length);
        Resources res = viewController.getResources();
        ArrayList<Object> item;
        for (Account acc : accounts) {
            item = new ArrayList<Object>(2);
            item.add(acc);
//			if (Constants.EXPRESS_TYPE.equals(acc.getType()))
//				item.add("Cuenta express");
//			else
            item.add(acc.getNombreTipoCuenta(SuiteAppPagoCreditoApi.appContext.getResources()));
//			StringBuilder sb = new StringBuilder(acc.getCurrency());
//			sb.append(" ");
//			sb.append(Tools.hideAccountNumber(acc.getNumber()));
//			item.add(sb.toString());
            item.add(Tools.hideAccountNumber(acc.getNumber()));
            if (acc.isVisible())
                accountsArray.add(0, item);
            else
                accountsArray.add(item);
        }
        return accountsArray;
    }

    /**
     * Handler de lista seleccion para obtener
     * la cuenta seleccionada por el usuario
     */
    public void performAction(Object obj) {
        if (viewController != null)
            viewController.actualizarCuenta(obj);
    }


    @Override
    public int getTextoEncabezado() {
        int resTitle = R.string.bmovil_cambio_cuenta_title;
        return resTitle;
    }


    @Override
    public int getNombreImagenEncabezado() {
        int resIcon = R.drawable.bmovil_cambiocuenta_icono;
        return resIcon;
    }

    @Override
    public String getTextoPantallaResultados() {
        String text = viewController.getString(R.string.bmovil_cambio_cuenta_texto_resultados);
        return text;
    }

    @Override
    public String getTextoTituloResultado() {
        return viewController.getString(R.string.transferir_detalle_operacion_exitosaTitle);
    }

    @Override
    public int getColorTituloResultado() {
        return R.color.verde_limon;
    }


    @Override
    public boolean mostrarCampoTarjeta() {
        return (mostrarCVV() || mostrarNIP());
    }
}
