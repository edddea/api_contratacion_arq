package bancomer.api.pagarcreditos.models;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class CambioDePerfilData implements ParsingHandler {

    private String mensajeInformativo;
    private String folioCanal;

    public CambioDePerfilData() {
        // TODO Auto-generated constructor stub
    }

    public String getMensajeInformativo() {
        return mensajeInformativo;
    }

    public void setMensajeInformativo(String mensajeInformativo) {
        this.mensajeInformativo = mensajeInformativo;
    }

    public String getFolioCanal() {
        return folioCanal;
    }

    public void setFolioCanal(String folioCanal) {
        this.folioCanal = folioCanal;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        throw new UnsupportedOperationException(getClass().getName());
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        folioCanal = parser.parseNextValue("folio");

    }

}

