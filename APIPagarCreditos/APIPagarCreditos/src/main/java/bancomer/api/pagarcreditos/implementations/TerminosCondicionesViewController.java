package bancomer.api.pagarcreditos.implementations;

import android.os.Build;
import android.os.Bundle;
import android.view.MotionEvent;
import android.webkit.WebView;

import com.bancomer.base.SuiteApp;

import java.util.HashMap;
import java.util.Map;

import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.commons.GuiTools;
import bancomer.api.pagarcreditos.commons.TrackingHelper;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;


public class TerminosCondicionesViewController extends BaseViewController {
    //ARR
    Map<String, Object> paso1OperacionMap = new HashMap<String, Object>();
    /**
     * Campo de texto para terminos de uso.
     */
    private WebView wvTerminos;
    //AMZ
    private BmovilViewsController parentManager;


    public TerminosCondicionesViewController() {
        wvTerminos = null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppPagoCreditoApi.getResourceId("layout_bmovil_terminos_y_condiciones_admon", "layout"));
        SuiteApp.appContext = this;
        setParentViewsController(SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController());
        setTitle(R.string.bmovil_contratacion_titulo, R.drawable.icono_contratacion);

        findViews();
        scaleToScreenSize();
        String terminos = (String) this.getIntent().getExtras().get(Constants.TERMINOS_DE_USO_EXTRA);
        if (null != terminos) {
            if (Build.VERSION.SDK_INT < 15) {
                wvTerminos.loadDataWithBaseURL(null, terminos, "text/html", "utf-8", null);
            } else {
                wvTerminos.loadData(terminos, "text/html", "utf-8");
            }
        }
        //AMZ
        parentManager = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController();
        TrackingHelper.trackState("condiciones", parentManager.estados);
    }

    /**
     * Busca las referencias a las vistas.
     */
    private void findViews() {
        wvTerminos = (WebView) findViewById(SuiteAppPagoCreditoApi.getResourceId("webViewTerminos", "id"));
    }

    /**
     * Escala las vistas para acomodarse a la pantalla actual.
     */
    private void scaleToScreenSize() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(SuiteAppPagoCreditoApi.getResourceId("layoutBaseContainer", "id")));
        guiTools.scale(findViewById(SuiteAppPagoCreditoApi.getResourceId("lblTitulo", "id")), true);
        guiTools.scale(wvTerminos);
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onResume()
     */
    @Override
    protected void onResume() {
        SuiteApp.appContext = this;
        setParentViewsController(SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController());
        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(this);
        super.onResume();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        float touchX = ev.getX();
        float touchY = ev.getY();
        int[] webViewPos = new int[2];

        wvTerminos.getLocationOnScreen(webViewPos);

        float listaSeleccionX2 = webViewPos[0] + wvTerminos.getMeasuredWidth();
        float listaSeleccionY2 = webViewPos[1] + wvTerminos.getMeasuredHeight();

        if ((touchX >= webViewPos[0] && touchX <= listaSeleccionX2) &&
                (touchY >= webViewPos[1] && touchY <= listaSeleccionY2)) {
            wvTerminos.getParent().requestDisallowInterceptTouchEvent(true);
        }

        return super.dispatchTouchEvent(ev);
    }
}
