package bancomer.api.pagarcreditos.implementations;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.model.Compania;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.commons.GuiTools;
import bancomer.api.pagarcreditos.commons.TrackingHelper;
import bancomer.api.pagarcreditos.models.CambioTelefono;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

public class CambioTelefonoViewController extends BaseViewController {

    private CambioTelefonoDelegate delegate;
    private EditText numeroTelefono;
    private EditText numeroTelefonoNuevo;
    private TextView numeroTelefonoLabel;
    private TextView numeroTelefonoNuevoLabel;
    private TextView companiaLabel;
    private LinearLayout contenedorPrincipal;
    private ImageButton botonContinuar;
    private CambioTelefono cambioTelefono;
    /**
     * Contenedor para el componente SeleccionHorizontal.
     */
    private LinearLayout seleccionHorizontalLayout;

    /**
     * El componente SeleccionHorizontal.
     */
    private SeleccionHorizontalViewController seleccionHorizontal;
    //AMZ
    private BmovilViewsController parentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteAppPagoCreditoApi.getResourceId("layout_bmovil_cambio_telefono_asociado_admon", "layout"));
        SuiteApp.appContext = this;
        setTitle(R.string.bmovil_cambio_telefono_title, R.drawable.bmovil_cambiotelefono_icono);
        //AMZ
        parentManager = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController();
        TrackingHelper.trackState("camb numero", parentManager.estados);

        setParentViewsController(SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController());
        setDelegate((CambioTelefonoDelegate) parentViewsController.getBaseDelegateForKey(CambioTelefonoDelegate.CAMBIO_TELEFONO_DELEGATE_ID));
        delegate = (CambioTelefonoDelegate) getDelegate();
        delegate.setViewController(this);
        findViews();
        scaleForCurrentScreen();
        inicializarPantalla();

        numeroTelefono.addTextChangedListener(new BmovilTextWatcher(this));
        numeroTelefonoNuevo.addTextChangedListener(new BmovilTextWatcher(this));
    }

    private void findViews() {
        //corregir los ids
        numeroTelefono = (EditText) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambio_telefono_numero_text", "id"));
        numeroTelefonoNuevo = (EditText) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambio_telefono_numero_nuevo_text", "id"));
        numeroTelefonoLabel = (TextView) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambio_telefono_numero_label", "id"));
        numeroTelefonoNuevoLabel = (TextView) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambio_telefono_numero_nuevo_label", "id"));
        companiaLabel = (TextView) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambio_telefono_compania_label", "id"));
        contenedorPrincipal = (LinearLayout) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambio_telefono_contenedor_principal", "id"));
        seleccionHorizontalLayout = (LinearLayout) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambio_telefono_seleccionHorizontalLayout", "id"));
        botonContinuar = (ImageButton) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambio_telefono_btnContinuar", "id"));
    }

    private void scaleForCurrentScreen() {

        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(numeroTelefono, true);
        gTools.scale(numeroTelefonoNuevo, true);
        gTools.scale(numeroTelefonoLabel, true);
        gTools.scale(numeroTelefonoNuevoLabel, true);
        gTools.scale(companiaLabel, true);
        gTools.scale(seleccionHorizontalLayout);
        gTools.scale(contenedorPrincipal);
        gTools.scale(botonContinuar);

    }

    private void inicializarPantalla() {
        InputFilter[] filter = {new InputFilter.LengthFilter(Constants.TELEPHONE_NUMBER_LENGTH)};
        numeroTelefono.setFilters(filter);
        numeroTelefonoNuevo.setFilters(filter);
        mostrarListadoCompanias();
        Session session = Session.getInstance(SuiteAppPagoCreditoApi.appContext);
        numeroTelefono.setText(session.getUsername());
        numeroTelefonoNuevo.setText(session.getUsername());
        String nombreCompania = session.getCompaniaUsuario();
        ArrayList<Object> companias = delegate.getListaCompanias();
        for (Object compania : companias) {
            if (((Compania) compania).getNombre().equalsIgnoreCase(nombreCompania)) {
                seleccionHorizontal.setSelectedItem(compania);
                break;
            }
        }
        cambioTelefono = new CambioTelefono();
    }

    /**
     * Handler del boton continuar
     *
     * @param view
     */
    public void botonContinuarClick(View view) {
        if (Server.ALLOW_LOG) Log.d("CambioTelefono", "accionContinuar");
        String numTelefono = numeroTelefono.getText().toString();
        String numNuevoTelefono = numeroTelefonoNuevo.getText().toString();
        Compania compania = (Compania) seleccionHorizontal.getSelectedItem();
        if (delegate.validaTelefono(numTelefono, numNuevoTelefono, compania)) {
            cambioTelefono.setNombreCompania(compania.getNombre());
            cambioTelefono.setNuevoTelefono(numNuevoTelefono);
            showInformationAlert(R.string.bmovil_cambio_telefono_advertencia, new OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    //AMZ
                    Map<String, Object> Paso1OperacionMap = new HashMap<String, Object>();
                    //AMZ
                    Paso1OperacionMap.put("evento_paso1", "event46");
                    Paso1OperacionMap.put("&&products", "operaciones;admin+cambio celular");
                    Paso1OperacionMap.put("eVar12", "paso1:cambio celular");
                    TrackingHelper.trackPaso1Operacion(Paso1OperacionMap);
                    showConfirmacion();

                }
            });

        }


    }

    /**
     * muestra la lista de compañías en el catalogo de dinero movil
     * en el componente lista seleccion horizontal
     */
    private void mostrarListadoCompanias() {

        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
        ArrayList<Object> companias = delegate.getListaCompanias();
        seleccionHorizontal = new SeleccionHorizontalViewController(this, params, companias, getDelegate(), false);
        seleccionHorizontalLayout.addView(seleccionHorizontal);

    }

    private void showConfirmacion() {
        delegate.showConfirmacion();
    }

    public CambioTelefono getCambioTelefono() {
        return cambioTelefono;
    }

    public void setCambioTelefono(CambioTelefono cambioTelefono) {
        this.cambioTelefono = cambioTelefono;
    }

    @Override
    protected void onPause() {
        super.onPause();
        parentViewsController.consumeAccionesDePausa();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SuiteApp.appContext = this;
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }
        getParentViewsController().setCurrentActivityApp(this);
    }

    @Override
    public void onBackPressed() {
        parentViewsController.removeDelegateFromHashMap(CambioTelefonoDelegate.CAMBIO_TELEFONO_DELEGATE_ID);
        super.onBackPressed();
    }

    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }

}
