package bancomer.api.pagarcreditos.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ImportesPagoCreditoData implements ParsingHandler {

    private static final long serialVersionUID = 1L;

    private String numCredito;
    private String moneda;
    private String pagoRequerido;
    private String pagoRequeridoMoneda;
    private String fechaPago;


    public ImportesPagoCreditoData(String numCredito, String pagoRequerido, String moneda, String pagoRequeridoMoneda, String fechaPago) {
        this.numCredito = numCredito;
        this.pagoRequerido = pagoRequerido;
        this.moneda = moneda;
        this.pagoRequeridoMoneda = pagoRequeridoMoneda;
        this.fechaPago = fechaPago;
    }

    public ImportesPagoCreditoData() {

        this.numCredito = "";
        this.pagoRequerido = "";
        this.moneda = "";
        this.pagoRequeridoMoneda = "";
        this.fechaPago = "";
    }

    public String getNumCredito() {
        return numCredito;
    }

    public void setNumCredito(String numCredito) {
        this.numCredito = numCredito;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getPagoRequerido() {
        return pagoRequerido;
    }

    public void setPagoRequerido(String pagoRequerido) {
        this.pagoRequerido = pagoRequerido;
    }

    public String getPagoRequeridoMoneda() {
        return pagoRequeridoMoneda;
    }

    public void setPagoRequeridoMoneda(String pagoRequeridoMoneda) {
        this.pagoRequeridoMoneda = pagoRequeridoMoneda;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub
        try {
            this.numCredito = parser.parseNextValue("numeroCredito");
            this.pagoRequerido = parser.parseNextValue("pagoReq");
            this.moneda = parser.parseNextValue("moneda");
            this.pagoRequeridoMoneda = parser.parseNextValue("pagoReqMoneda");
            this.fechaPago = parser.parseNextValue("fechaPago");
        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
        }

    }

}