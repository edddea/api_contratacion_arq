package bancomer.api.pagarcreditos.implementations;

import android.util.Log;

import java.util.ArrayList;

import bancomer.api.pagarcreditos.commons.TrackingHelper;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.handlers.ConfirmacionAutenticacionHandler;
import suitebancomer.aplicaciones.resultados.handlers.ConfirmacionHandler;
import suitebancomer.aplicaciones.resultados.handlers.ContratacionAutenticacionHandler;
import suitebancomer.aplicaciones.resultados.handlers.ResultadosAutenticacionHandler;
import suitebancomer.aplicaciones.resultados.handlers.ResultadosHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import bancomer.api.pagarcreditos.gui.commons.controllers.CuentaOrigenListViewController;
public class BmovilViewsController extends BaseViewsController {

    //AMZ
    public ArrayList<String> estados = new ArrayList<String>();
    private BmovilApp bmovilApp;

    /*
     * @Override public void setCurrentActivity(BaseViewController
     * currentViewController) { // TODO Auto-generated method stub
     * super.setCurrentActivity(currentViewController); Log.i("Si", "No"); }
     */
    public BmovilViewsController(BmovilApp bmovilApp) {
        super();
        this.bmovilApp = bmovilApp;
    }

    public void cierraViewsController() {
        bmovilApp = null;
        clearDelegateHashMap();
        super.cierraViewsController();
    }

    public void cerrarSesionBackground() {
        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication()
                .setApplicationInBackground(true);
    }

    @Override
    public void showMenuInicial() {
        showMenuPrincipal();
    }

    public void showMenuPrincipal() {
        showMenuPrincipal(false);
    }

    public void showMenuPrincipal(boolean inverted) {
        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication()
                .setApplicationLogged(true);
        if (SuiteAppPagoCreditoApi.getCallBackSession() != null) {
            if (ServerCommons.ALLOW_LOG) {
                Log.d(getClass().getSimpleName(), "Se ejecuta returnMenuPrincipal desde CallBackSession");
            }
            SuiteAppPagoCreditoApi.getCallBackSession().returnMenuPrincipal();
        } else if (SuiteAppPagoCreditoApi.getCallBackBConnect() != null) {
            if (ServerCommons.ALLOW_LOG) {
                Log.d(getClass().getSimpleName(), "Se ejecuta returnMenuPrincipal desde CallBackBConnect");
            }
            SuiteAppPagoCreditoApi.getCallBackBConnect().returnMenuPrincipal();
        }
    }

    public void showCuentaOrigenListViewController() {
        showViewController(CuentaOrigenListViewController.class);
    }

	/*TODO AMB
    public void showLogin() {
//		MenuSuiteViewController menuSuiteViewController = (MenuSuiteViewController) getCurrentViewControllerApp();
//		LoginDelegate loginDelegate = (LoginDelegate) getBaseDelegateForKey(LoginDelegate.LOGIN_DELEGATE_ID);
//		if (loginDelegate == null) {
//			loginDelegate = new LoginDelegate();
//			addDelegateToHashMap(LoginDelegate.LOGIN_DELEGATE_ID, loginDelegate);
//		}
//		loginDelegate.setParentViewController(menuSuiteViewController);
//		if (menuSuiteViewController.getLoginViewController() == null) {
//			LoginViewController loginViewController = new LoginViewController(
//					menuSuiteViewController, this);
//			loginViewController.setDelegate(loginDelegate);
//			menuSuiteViewController.setLoginViewController(loginViewController);
//			menuSuiteViewController.getBaseLayout()
//					.addView(loginViewController);
//			loginViewController.setVisibility(View.GONE);
//		}

//		menuSuiteViewController.plegarOpcion();
		//AMZ
		estados.clear();
		TrackingHelper.trackState("login", estados);
		//AMZ
				
	}*/


    public void showCambioPerfil() {
        CambioPerfilDelegate delegate = (CambioPerfilDelegate) getBaseDelegateForKey(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);
        if (null == delegate) {
            delegate = new CambioPerfilDelegate();
            addDelegateToHashMap(
                    CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID, delegate);
        }
        pantallaActivada();
        showViewController(CambioPerfilViewController.class);

    }

    public BmovilApp getBmovilApp() {
        return bmovilApp;
    }

    public void setBmovilApp(BmovilApp bmovilApp) {
        this.bmovilApp = bmovilApp;
    }


    @Override
    public void onUserInteraction() {

        if (bmovilApp != null) {

        }
        super.onUserInteraction();
    }

    @Override
    public boolean consumeAccionesDeReinicio() {
		/*if (!SuiteAppPagoCreditoApi.getInstance().getBmovilApplication()
				.isApplicationLogged()) {
			if (SuiteAppPagoCreditoApi.getIntentMenuPrincipal()!=null){
				if(Server.ALLOW_LOG) Log.d("Admon-BmovilViewsCont", "getIntentMenuPrincipal() >>" +
						SuiteAppPagoCreditoApi.getIntentMenuPrincipal().getClass().getSimpleName() );

				SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().showViewController(
						SuiteAppPagoCreditoApi.getIntentMenuPrincipal().getClass());
			}else{
				if(Server.ALLOW_LOG) Log.d("Admon-BmovilViewsCont", "Null object in getIntentMenuPrincipal()");
				showMenuAdministrar();
			}

			return true;
		}*/
        return false;
    }

    @Override
    public boolean consumeAccionesDePausa() {
		/*if (SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().isApplicationLogged()
				&& !isActivityChanging()) {
			cerrarSesionBackground();
			return true;
		}*/
        return false;
    }

    @Override
    public boolean consumeAccionesDeAlto() {
        if (!SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().isChangingActivity()
                && currentViewControllerApp != null) {
            currentViewControllerApp.hideSoftKeyboard();
        }
        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication()
                .setChangingActivity(false);
        return true;
    }

//	/**
//	 * Muestra la pantalla de confirmacion autenticacion
//	 */
//	public void showConfirmacionAutenticacionViewController(
//			DelegateBaseAutenticacion autenticacionDelegate, int resIcon,
//			int resTitle, int resSubtitle) {
//		showConfirmacionAutenticacionViewController(autenticacionDelegate,
//				resIcon, resTitle, resSubtitle, R.color.primer_azul);
//	}

    /**
     * Muestra la pantalla de confirmacion autenticacion
     */
    public void showConfirmacionAutenticacionViewController(
            DelegateBaseAutenticacion autenticacionDelegate, int resIcon,
            int resTitle, int resSubtitle, int resTitleColor) {

        ConfirmacionAutenticacionDelegate confirmacionDelegate = (ConfirmacionAutenticacionDelegate) getBaseDelegateForKey(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
        if (confirmacionDelegate != null) {
            removeDelegateFromHashMap(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
        }
        confirmacionDelegate = new ConfirmacionAutenticacionDelegate(
                autenticacionDelegate);
        addDelegateToHashMap(
                ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID,
                confirmacionDelegate);
        //AMZ
        if (estados.size() == 0) {
            //AMZ
            TrackingHelper.trackState("reactivacion", estados);
            //AMZ
        }

        //showViewController(ConfirmacionAutenticacionViewController.class);
        //SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().showViewController(SuiteAppPagoCreditoApi.getIntentConfirmacionAut().getClass());

        // API confirmacion   ConfirmacionAutenticacion
        Integer idText = confirmacionDelegate.getOperationDelegate().getTextoEncabezado();
        Integer idNombreImagenEncabezado = confirmacionDelegate.getOperationDelegate().getNombreImagenEncabezado();
        Boolean statusDesactivado = false;
        SuiteAppCRApi cr = new SuiteAppCRApi();
        cr.onCreate(SuiteAppPagoCreditoApi.appContext);
        SuiteAppCRApi.setCallBackSession(SuiteAppPagoCreditoApi.getCallBackSession());
        SuiteAppCRApi.setCallBackBConnect(SuiteAppPagoCreditoApi.getCallBackBConnect());
        ConfirmacionAutenticacionServiceProxy proxy = new ConfirmacionAutenticacionServiceProxy(confirmacionDelegate);
        cr.setProxy(proxy);

        ConfirmacionAutenticacionHandler handler = new ConfirmacionAutenticacionHandler(proxy, this, new ConfirmacionAutenticacionViewController());
        handler.setActivityChanging(isActivityChanging());
        handler.setDelegateId(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
        handler.invokeActivity(this.estados, idText, idNombreImagenEncabezado, statusDesactivado);

    }

    /**
     * Muestra la pantalla de resultados
     */
    public void showResultadosViewController(
            DelegateBaseOperacion delegateBaseOperacion, int resIcon,
            int resTitle) {
        ResultadosDelegate resultadosDelegate = (ResultadosDelegate) getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
        if (resultadosDelegate != null) {
            removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
        }
        resultadosDelegate = new ResultadosDelegate(delegateBaseOperacion);
        addDelegateToHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID,
                resultadosDelegate);
        //showViewController(ResultadosViewController.class);

        // API confirmacion   ResultadosAutenticacion
        Integer idText = resultadosDelegate.getOperationDelegate().getTextoEncabezado();
        Integer idNombreImagenEncabezado = resultadosDelegate.getOperationDelegate().getNombreImagenEncabezado();
        Boolean statusDesactivado = false;
        SuiteAppCRApi cr = new SuiteAppCRApi();
        cr.onCreate(SuiteAppPagoCreditoApi.appContext);
        SuiteAppCRApi.setCallBackSession(SuiteAppPagoCreditoApi.getCallBackSession());
        SuiteAppCRApi.setCallBackBConnect(SuiteAppPagoCreditoApi.getCallBackBConnect());
        ResultadosServiceProxy proxy = new ResultadosServiceProxy(resultadosDelegate);
        cr.setProxy(proxy);

        ResultadosHandler handler = new ResultadosHandler(proxy, this, new ResultadosViewController());
        handler.setActivityChanging(isActivityChanging());
        handler.setDelegateId(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
        handler.invokeActivity(this.estados, idText, idNombreImagenEncabezado);

        TrackingHelper.trackState("resul", estados);
    }

	/*TODO AMB
	public void showResultadosViewControllerWithoutFreq(
			DelegateBaseOperacion delegateBaseOperacion, int resIcon,
			int resTitle) {
		ResultadosDelegate resultadosDelegate = (ResultadosDelegate) getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		if (resultadosDelegate != null) {
			removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		}
		resultadosDelegate = new ResultadosDelegate(delegateBaseOperacion);
		resultadosDelegate.setFrecOpOK(true);
		addDelegateToHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID,
				resultadosDelegate);
		//showViewController(ResultadosViewController.class);

		// API confirmacion   ResultadosAutenticacion
		Integer idText = resultadosDelegate.getOperationDelegate().getTextoEncabezado();
		Integer idNombreImagenEncabezado = resultadosDelegate.getOperationDelegate().getNombreImagenEncabezado();
		Boolean statusDesactivado =false;
		SuiteAppCRApi cr=new SuiteAppCRApi();
		cr.onCreate(SuiteAppPagoCreditoApi.appContext);
		SuiteAppCRApi.setCallBackSession(SuiteAppPagoCreditoApi.getCallBackSession());
		SuiteAppCRApi.setCallBackBConnect(SuiteAppPagoCreditoApi.getCallBackBConnect());
		ResultadosServiceProxy proxy = new ResultadosServiceProxy(resultadosDelegate);
		cr.setProxy(proxy);

		ResultadosHandler handler = new ResultadosHandler(proxy, this,new ResultadosViewController() );
		handler.setActivityChanging(isActivityChanging());
		handler.setDelegateId(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		handler.invokeActivity(this.estados,idText, idNombreImagenEncabezado);

	}*/


    /**
     * Muestra la pantalla de confirmacion para transferencias.
     *
     * @param delegateBaseOperacion EL DelegateBaseOperacion que manda a llamar la pantalla de
     *                              confirmaci�n.
     */
    public void showConfirmacion(DelegateBaseOperacion delegateBaseOperacion) {
        // El delegado de confirmaci�n se crea siempre, esto ya que el delegado
        // que contiene el internamente cambia segun quien invoque este método.
        ConfirmacionDelegate delegate = (ConfirmacionDelegate) getBaseDelegateForKey(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
        if (delegate != null) {
            removeDelegateFromHashMap(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
        }
        delegate = new ConfirmacionDelegate(delegateBaseOperacion);
        addDelegateToHashMap(
                ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID,
                delegate);
        //showViewController(ConfirmacionViewController.class);

        // API confirmacion   Confirmacion
        Integer idText = delegate.consultaOperationsDelegate().getTextoEncabezado();
        Integer idNombreImagenEncabezado = delegate.consultaOperationsDelegate().getNombreImagenEncabezado();
        Boolean statusDesactivado = false;
        SuiteAppCRApi cr = new SuiteAppCRApi();
        cr.onCreate(SuiteAppPagoCreditoApi.appContext);
        SuiteAppCRApi.setCallBackSession(SuiteAppPagoCreditoApi.getCallBackSession());
        SuiteAppCRApi.setCallBackBConnect(SuiteAppPagoCreditoApi.getCallBackBConnect());
        ConfirmacionServiceProxy proxy = new ConfirmacionServiceProxy(delegate);
        cr.setProxy(proxy);

        ConfirmacionHandler handler = new ConfirmacionHandler(proxy, this, new ConfirmacionViewController());
        handler.setActivityChanging(isActivityChanging());
        handler.setDelegateId(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
        handler.invokeActivity(this.estados, idText, idNombreImagenEncabezado);

        TrackingHelper.trackState("confirma", estados);
    }

    /*
     * Muestra la pantalla para alta de frecuente
     */
    public void showAltaFrecuente(DelegateBaseOperacion delegateBaseOperacion) {
//		AltaFrecuenteDelegate delegate = (AltaFrecuenteDelegate) getBaseDelegateForKey(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID);
//		if (delegate != null) {
//			removeDelegateFromHashMap(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID);
//		}
//		delegate = new AltaFrecuenteDelegate(delegateBaseOperacion);
//		addDelegateToHashMap(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID,
//				delegate);
//		showViewController(AltaFrecuenteViewController.class);
    }


    @Override
    public void removeDelegateFromHashMap(long key) {
        // TODO Auto-generated method stub
//		if (key == BorrarDatosDelegate.BORRAR_DATOS_DELEGATE_ID) {
//			if(Server.ALLOW_LOG) Log.d("MES", "MES");
//		}
        super.removeDelegateFromHashMap(key);
    }

//	/**
//	 * Muestra la pantalla de ingresar datos de contratación.
//	 *TODO AMB
//	public void showContratacion(ConsultaEstatus consultaEstatus, String estatusServicio, boolean deleteData) {
//		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new ContratacionDelegate();
//			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,	delegate);
//		}
//		delegate.setConsultaEstatus(consultaEstatus);
//		delegate.setDeleteData(deleteData);
//
//		if (estatusServicio.equalsIgnoreCase(Constants.STATUS_ENGAGEMENT_UNCOMPLETE)) {
//			showContratacionAutenticacion();
//		} else {
//			//AMZ
//			estados.clear();
//			//AMZ
//			TrackingHelper.trackState("contratacion datos", estados);
//			//AMZ
////			showViewController(IngresarDatosViewController.class);
//		}
//	}*/
//
//	/**
//	 * Ejecuta flujo
//	 *
//	 *TODO AMB
//	public void showContratacionEP11(BaseViewController ownerController, ConsultaEstatus consultaEstatus,
//			String estatusServicio, boolean deleteData) {
//
//		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new ContratacionDelegate();
//			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,
//					delegate);
//		}
//
//		delegate.setEscenarioAlternativoEA11(false);
//		delegate.setConsultaEstatus(consultaEstatus);
//		delegate.setDeleteData(deleteData);
////		IngresarDatosViewController ingresarDatosViewController = new IngresarDatosViewController(
////				true);
////		ingresarDatosViewController.setDelegate(delegate);
////		ingresarDatosViewController.setContratacionDelegate(delegate);
////		ingresarDatosViewController.setParentViewsController(SuiteApp
////				.getInstance().getBmovilApplication()
////				.getBmovilViewsController());
//
//		//showViewController(IngresarDatosViewController.class);
//
//
//
////		SuiteAppPagoCreditoApi.getInstance().getBmovilApplication()
////				.getBmovilViewsController()
////				.setCurrentActivityApp(ownerController);
//
//		delegate.setOwnerController(ownerController);
//
//		delegate.validaCamposST(consultaEstatus.getCompaniaCelular(),
//				consultaEstatus.getNumTarjeta(), true);
//	}*/
//
//
//	/**
//	 * Muestra la pantalla de ingresar datos de contratación.
//	 */
//	/*TODO AMB
//	public void showContratacion() {
//		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new ContratacionDelegate();
//			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,
//					delegate);
//		}
////		showViewController(IngresarDatosViewController.class);
//
//	}*/

    /**
     * Muestra la pantalla de definir contraseña la contratación.
     */
    public void showDefinirPassword() {
        ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
        if (null == delegate) {
            delegate = new ContratacionDelegate();
            addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID, delegate);
        }

//		showViewController(ContratacionDefinicionPasswordViewController.class);
    }
//	/**
//	 * Muestra la pantalla de autenticacion softoken.
//	 * @param currentPassword
//	 * @param consultaEstatus
//	 */
//	/*TODO AMB
//	public void showAutenticacionSoftoken(ConsultaEstatus consultaEstatus, String currentPassword) {
//		String[] extrasKeys= {"login"};
//		Object[] extras= {true};
//
//		showViewController(IngresoDatosSTViewController.class, 0, false, extrasKeys, extras);
//	}*/

    /**
     * Muestra la pantalla de resultados
     */
    public void showResultadosAutenticacionViewController(
            DelegateBaseOperacion delegateBaseOperacion, int resIcon,
            int resTitle) {
        //ResultadosDelegate.RESULTADOS_DELEGATE_ID
        ResultadosAutenticacionDelegate delegate = (ResultadosAutenticacionDelegate)
                getBaseDelegateForKey(ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID);
        if (delegate != null) {
            //removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
            removeDelegateFromHashMap(ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID);
        }
        delegate = new ResultadosAutenticacionDelegate(delegateBaseOperacion);
        addDelegateToHashMap(
                ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID,
                delegate);
        //showViewController(ResultadosAutenticacionViewController.class);

        // API confirmacion   ResultadosAutenticacion
        Integer idText = delegate.getOperationDelegate().getTextoEncabezado();
        Integer idNombreImagenEncabezado = delegate.getOperationDelegate().getNombreImagenEncabezado();
        Boolean statusDesactivado = false;
        SuiteAppCRApi cr = new SuiteAppCRApi();
        cr.onCreate(SuiteAppPagoCreditoApi.appContext);
        SuiteAppCRApi.setCallBackSession(SuiteAppPagoCreditoApi.getCallBackSession());
        SuiteAppCRApi.setCallBackBConnect(SuiteAppPagoCreditoApi.getCallBackBConnect());
        ResultadosAutenticacionServiceProxy proxy = new ResultadosAutenticacionServiceProxy(delegate);
        cr.setProxy(proxy);

        ResultadosAutenticacionHandler handler = new ResultadosAutenticacionHandler(proxy, this, new ResultadosAutenticacionViewController());
        handler.setActivityChanging(isActivityChanging());
        handler.setDelegateId(ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID);
        handler.invokeActivity(this.estados, idText, idNombreImagenEncabezado, statusDesactivado);

    }

//	/**
//	 * Muestra la pantalla de configurar alertas.
//	 */
//	/*TODO AMB
//	public void showConfigurarAlertas() {
//		ConfigurarAlertasDelegate delegate = (ConfigurarAlertasDelegate) getBaseDelegateForKey(ConfigurarAlertasDelegate.CONFIGURAR_ALERTAS_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new ConfigurarAlertasDelegate();
//			addDelegateToHashMap(
//					ConfigurarAlertasDelegate.CONFIGURAR_ALERTAS_DELEGATE_ID,
//					delegate);
//		}
//
//		showViewController(ConfigurarAlertasViewController.class);
//	}*/
//

    /**
     * Muestra la pantalla de confirmaci�n autenticación para el estatus PS.
     */
    public void showContratacionAutenticacion() {
        ContratacionDelegate contrataciondelegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
        ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);

        if (delegate != null) {
            removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
        }

        delegate = new ContratacionAutenticacionDelegate(contrataciondelegate);
        addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID, delegate);
        //showViewController(ContratacionAutenticacionViewController.class);

        // API confirmacion   ContratacionAutenticacion
        Integer idText = delegate.consultaOperationsDelegate().getTextoEncabezado();
        Integer idNombreImagenEncabezado = delegate.consultaOperationsDelegate().getNombreImagenEncabezado();
        Boolean statusDesactivado = false;
        SuiteAppCRApi cr = new SuiteAppCRApi();
        cr.onCreate(SuiteAppPagoCreditoApi.appContext);
        SuiteAppCRApi.setCallBackSession(SuiteAppPagoCreditoApi.getCallBackSession());
        SuiteAppCRApi.setCallBackBConnect(SuiteAppPagoCreditoApi.getCallBackBConnect());
        ContratacionAutenticacionServiceProxy proxy = new ContratacionAutenticacionServiceProxy(delegate);
        cr.setProxy(proxy);

        ContratacionAutenticacionHandler handler = new ContratacionAutenticacionHandler(proxy, this, new ContratacionAutenticacionViewController());
        handler.setActivityChanging(isActivityChanging());
        handler.setDelegateId(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
        handler.invokeActivity(this.estados, idText, idNombreImagenEncabezado, statusDesactivado);

    }

    /**
     * Muestra la pantalla de confirmacion autenticacion
     */
    public void showContratacionAutenticacion(DelegateBaseAutenticacion autenticacionDelegate) {
        ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);

        if (delegate != null) {
            removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
        }

        delegate = new ContratacionAutenticacionDelegate(autenticacionDelegate);
        addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID, delegate);
        //showViewController(ContratacionAutenticacionViewController.class);

        // API confirmacion   ContratacionAutenticacion
        Integer idText = delegate.consultaOperationsDelegate().getTextoEncabezado();
        Integer idNombreImagenEncabezado = delegate.consultaOperationsDelegate().getNombreImagenEncabezado();
        Boolean statusDesactivado = false;
        SuiteAppCRApi cr = new SuiteAppCRApi();
        cr.onCreate(SuiteAppPagoCreditoApi.appContext);
        SuiteAppCRApi.setCallBackSession(SuiteAppPagoCreditoApi.getCallBackSession());
        SuiteAppCRApi.setCallBackBConnect(SuiteAppPagoCreditoApi.getCallBackBConnect());
        ContratacionAutenticacionServiceProxy proxy = new ContratacionAutenticacionServiceProxy(delegate);
        cr.setProxy(proxy);

        ContratacionAutenticacionHandler handler = new ContratacionAutenticacionHandler(proxy, this, new ContratacionAutenticacionViewController());
        handler.setActivityChanging(isActivityChanging());
        handler.setDelegateId(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
        handler.invokeActivity(this.estados, idText, idNombreImagenEncabezado, statusDesactivado);

    }

    /**
     * Muestra los terminos y condiciones de uso.
     *
     * @param terminosDeUso Terminos de uso.
     */
    public void showTerminosDeUso(String terminosDeUso) {
        showViewController(TerminosCondicionesViewController.class, 0, false,
                new String[]{Constants.TERMINOS_DE_USO_EXTRA},
                new Object[]{terminosDeUso});
    }

    /**
     * @param delegateOp
     */
    public void showRegistrarOperacion(DelegateBaseAutenticacion delegateOp) {
        //SPEI
        showRegistrarOperacion(delegateOp, false);
    }

    public void showRegistrarOperacion(DelegateBaseAutenticacion delegateOp, boolean showHelpImage) {
        //Termina SPEI

		/*AYMB
		 * RegistrarOperacionDelegate delegate = (RegistrarOperacionDelegate) getBaseDelegateForKey(RegistrarOperacionDelegate.REGISTRAR_OPERACION_DELEGATE_ID);
		if (delegate != null)
			removeDelegateFromHashMap(RegistrarOperacionDelegate.REGISTRAR_OPERACION_DELEGATE_ID);
		delegate = new RegistrarOperacionDelegate(delegateOp);
		addDelegateToHashMap(
				RegistrarOperacionDelegate.REGISTRAR_OPERACION_DELEGATE_ID,
				delegate);
		//SPEI

		String[] extrasKeys = { "showHelpImage" };
		Object[] extrasValues = { Boolean.valueOf(showHelpImage) };
		showViewController(RegistrarOperacionViewController.class, 0, false, extrasKeys, extrasValues);*/
    }


//	/**
//	 * Shows the terms and conditions screen.
//	 * @param termsHtml Terms and conditions HTML.
//	 * @param title The title resource identifier.
//	 * @param icon The icon recource identifier.
//	 */
//	public void showTermsAndConditions(String termsHtml, int title, int icon) {
//		String[] keys = {Constants.TERMINOS_DE_USO_EXTRA, Constants.TITLE_EXTRA, Constants.ICON_EXTRA};
//		Object[] values = {termsHtml, Integer.valueOf(title), Integer.valueOf(icon)};
//
//		showViewController(TerminosCondicionesViewController.class, 0, false, keys, values);
//	}

    public void touchMenu() {
        String eliminado = "";
        if (estados.size() == 2) {
            eliminado = estados.remove(1);
        } else {
            int tam = estados.size();
            for (int i = 1; i < tam; i++) {
                eliminado = estados.remove(1);
            }
        }
    }

    public void touchAtras() {

        int ultimo = estados.size() - 1;

        String eliminado;
        if (ultimo >= 0) {
            String ult = estados.get(ultimo);
            if (ult == "reactivacion" || ult == "activacion" || ult == "menu token" || ult == "contratacion datos" || ult == "activacion datos") {
                estados.clear();
            } else {
                eliminado = estados.remove(ultimo);
            }
        }
    }

    public void pantallaActivada() {
        setActivityChanging(true);
    }
}