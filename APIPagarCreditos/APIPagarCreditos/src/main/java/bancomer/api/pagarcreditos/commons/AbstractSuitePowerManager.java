package bancomer.api.pagarcreditos.commons;

import bancomer.api.pagarcreditos.commons.DonutPowerManager;
import bancomer.api.pagarcreditos.commons.EclairPowerManager;

/**
 * Abstract class to define a power manager for every Android version since api4 (Android Power Manager is not defined before api7).
 */
public abstract class AbstractSuitePowerManager {
    /**
     * Gets an AbstractContactMannager instance in accordance to the current api level.
     *
     * @return The AbstractContactMannager instance.
     */
    public static AbstractSuitePowerManager getSuitePowerManager() {
        return (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.ECLAIR_MR1) ? new DonutPowerManager() : new EclairPowerManager();
    }

    /**
     * Gets a boolean than indicates the current state of the screen (On or Off).
     *
     * @return True if the screen is On, false otherwise.
     */
    public abstract boolean isScreenOn();
}
