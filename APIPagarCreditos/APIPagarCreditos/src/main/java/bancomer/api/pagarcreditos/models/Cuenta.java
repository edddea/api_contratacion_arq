package bancomer.api.pagarcreditos.models;

import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.NombreCuenta;

public class Cuenta {

    private String tipoCuenta;
    private String numCuenta;
    private String indAlertas;

    /**
     * @return the tipoCuenta
     */
    public String getTipoCuenta() {
        return tipoCuenta;
    }

    /**
     * @param tipoCuenta the tipoCuenta to set
     */
    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }


    /**
     * @return the numCuenta
     */
    public String getNumCuenta() {
        return numCuenta;
    }

    /**
     * @param numCuenta the numCuenta to set
     */
    public void setNumCuenta(String numCuenta) {
        this.numCuenta = numCuenta;
    }


    /**
     * @return the indAlertas
     */
    public String getIndAlertas() {
        return indAlertas;
    }

    /**
     * @param indAlertas the indAlertas to set
     */
    public void setIndAlertas(String indAlertas) {
        this.indAlertas = indAlertas;
    }


    public String getNombreTipoCta() {
        // Variable nombre cuenta
        NombreCuenta nombreCuenta = null;
        String sNombreCuenta = null;
        if (tipoCuenta.equals(Constants.LIBRETON_TYPE)) {
            nombreCuenta = NombreCuenta.li;
        } else if (tipoCuenta.equals(Constants.CHECK_TYPE)) {
            nombreCuenta = NombreCuenta.ch;
        } else if (tipoCuenta.equals(Constants.SAVINGS_TYPE)) {
            nombreCuenta = NombreCuenta.ah;
        } else if (tipoCuenta.equals(Constants.CREDIT_TYPE)) {
            nombreCuenta = NombreCuenta.tc;
        } else if (tipoCuenta.equals(Constants.DEBIT_TYPE)) {
            nombreCuenta = NombreCuenta.td;
        } else if (tipoCuenta.equals(Constants.PREPAID_TYPE)) {
            nombreCuenta = NombreCuenta.tp;
        } else if (tipoCuenta.equals(Constants.EXPRESS_TYPE)) {
            nombreCuenta = NombreCuenta.ce;
        } else if (tipoCuenta.equals(Constants.T_ADICIONAL)) {
            nombreCuenta = NombreCuenta.ad;
        } else if (tipoCuenta.equals(Constants.T_MINI)) {
            nombreCuenta = NombreCuenta.mi;
        } else if (tipoCuenta.equals(Constants.T_STICKER)) {
            nombreCuenta = NombreCuenta.stick;
        } else if (tipoCuenta.equals(Constants.T_DIGITAL)) {
            nombreCuenta = NombreCuenta.digital;
        }
        if (nombreCuenta == null) {
            sNombreCuenta = Constants.TIPO_CUENTA_NO_DEFINIDA;
        } else {
            sNombreCuenta = nombreCuenta.value;
        }
        return sNombreCuenta;
    }

}

