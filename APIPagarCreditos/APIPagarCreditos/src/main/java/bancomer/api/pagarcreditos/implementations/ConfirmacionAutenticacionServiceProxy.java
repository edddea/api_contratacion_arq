/**
 *
 */
package bancomer.api.pagarcreditos.implementations;

import android.util.Log;


import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.pagarcreditos.R;
import suitebancomer.aplicaciones.resultados.proxys.IConfirmacionAutenticacionServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.resultados.to.ParamTo;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

/**
 * @author lbermejo
 */
public class ConfirmacionAutenticacionServiceProxy implements IConfirmacionAutenticacionServiceProxy {

    /**
     */
    private static final long serialVersionUID = 3925667201345924488L;
    private BaseDelegateCommons baseDelegateCommons;

    public ConfirmacionAutenticacionServiceProxy(BaseDelegateCommons bdc) {
        this.baseDelegateCommons = bdc;
    }

    @Override
    public ArrayList<Object> getListaDatos() {
        if (Server.ALLOW_LOG)
            Log.d(getClass().getName(), ">>proxy consultaOperationsIdTextoEncabezado >> delegate");
        final ConfirmacionAutenticacionDelegate delegate = (ConfirmacionAutenticacionDelegate) baseDelegateCommons;
        ArrayList<Object> list = delegate.consultaOperationsDelegate().getDatosTablaConfirmacion();
        return list;
    }

    @Override
    public ConfirmacionViewTo showFields() {
        ConfirmacionViewTo to = new ConfirmacionViewTo();
        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy showFields >> delegate");

        final ConfirmacionAutenticacionDelegate delegate =
                (ConfirmacionAutenticacionDelegate) baseDelegateCommons;
        to.setShowContrasena(delegate.consultaDebePedirContrasena());
        to.setShowNip(delegate.consultaDebePedirNIP());
        to.setTokenAMostrar(delegate.consultaInstrumentoSeguridad());
        to.setShowCvv(delegate.consultaDebePedirCVV());
        to.setShowTarjeta(delegate.mostrarCampoTarjeta());
        to.setTokenAMostrar(delegate.consultaInstrumentoSeguridad());
        to.setInstrumentoSeguridad(delegate.consultaTipoInstrumentoSeguridad());
        to.setTextoAyudaInsSeg(
                delegate.getTextoAyudaInstrumentoSeguridad(
                        to.getInstrumentoSeguridad()));
//		if(delegate.getOperationDelegate() instanceof MenuSuspenderCancelarDelegate ){
//			to.setCloseSession( Boolean.TRUE);
//		}else{
        to.setCloseSession(Boolean.FALSE);
//		}

		/*
            //mostrarContrasena(confirmacionDelegate.consultaDebePedirContrasena());
			//mostrarNIP(confirmacionDelegate.consultaDebePedirNIP());
			//mostrarASM(confirmacionDelegate.consultaInstrumentoSeguridad());
			//mostrarCVV(confirmacionDelegate.consultaDebePedirCVV());
			//mostrarCampoTarjeta(confirmacionDelegate.mostrarCampoTarjeta());  */
        return to;
    }

    @Override
    public Integer getMessageAsmError(Constants.TipoInstrumento tipoInstrumento) {

        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getMessageAsmError >> delegate");
        int idMsg = 0;

        switch (tipoInstrumento) {
            case OCRA:
                idMsg = R.string.confirmation_ocra;
                break;
            case DP270:
                idMsg = R.string.confirmation_dp270;
                break;
            case SoftToken:
                if (SuiteAppPagoCreditoApi.getSofttokenStatus()) {
                    idMsg = R.string.confirmation_softtokenActivado;
                } else {
                    idMsg = R.string.confirmation_softtokenDesactivado;
                }
                break;
            default:
                break;
        }

        return idMsg;
		
		/*switch (tipoInstrumentoSeguridad) {
			case OCRA:
				mensaje += getEtiquetaCampoOCRA();
				break;
			case DP270:
				mensaje += getEtiquetaCampoDP270();
				break;
			case SoftToken:
				if (SuiteAppAdmonApi.getSofttokenStatus()) {
					mensaje += getEtiquetaCampoSoftokenActivado();
				} else {
					mensaje += getEtiquetaCampoSoftokenDesactivado();
				}
				break;
			default:
				break;
		}
		*/
    }

    @Override
    public String loadOtpFromSofttoken(Constants.TipoOtpAutenticacion tipoOtpAutenticacion) {

        if (Server.ALLOW_LOG)
            Log.d(getClass().getName(), ">>proxy loadOtpFromSofttoken >> delegate");

        final ConfirmacionAutenticacionDelegate delegate =
                (ConfirmacionAutenticacionDelegate) baseDelegateCommons;
        String res = delegate.loadOtpFromSofttoken(tipoOtpAutenticacion);
        return res;
    }

    @Override
    public Integer consultaOperationsIdTextoEncabezado() {

        if (Server.ALLOW_LOG)
            Log.d(getClass().getName(), ">>proxy consultaOperationsIdTextoEncabezado >> delegate");

        //getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()
        final ConfirmacionAutenticacionDelegate delegate =
                (ConfirmacionAutenticacionDelegate) baseDelegateCommons;
        int res = delegate.consultaOperationsDelegate().getTextoEncabezado();

        return res;
    }

    @Override
    public Boolean doOperation(ParamTo to) {

        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy doOperation >> delegate");
        final ConfirmacionAutenticacionDelegate delegate =
                (ConfirmacionAutenticacionDelegate) baseDelegateCommons;
        ConfirmacionViewTo params = (ConfirmacionViewTo) to;
        //SuiteAppAdmonApi.appContext.
        ConfirmacionAutenticacionViewController caller = new ConfirmacionAutenticacionViewController();
        delegate.setConfirmacionAutenticacionViewController(caller);
        caller.setDelegate(delegate);
        caller.setParentViewsController(((BmovilViewsController)
                SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getViewsController()));

        try {
            delegate.getOperationDelegate().realizaOperacion(caller, params.getContrasena(),
                    params.getNip(), params.getAsm(),
                    params.getCvv(), params.getTarjeta());

            return Boolean.TRUE;
        } catch (Exception e) {
            //TODO Log
            return Boolean.FALSE;
        }
    }

    @Override
    public BaseDelegateCommons getDelegate() {
        return baseDelegateCommons;
    }

}
