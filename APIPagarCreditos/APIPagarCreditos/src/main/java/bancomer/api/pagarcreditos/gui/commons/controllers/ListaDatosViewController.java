package bancomer.api.pagarcreditos.gui.commons.controllers;

import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.commons.GuiTools;
import bancomer.api.pagarcreditos.implementations.SuiteAppPagoCreditoApi;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;

public class ListaDatosViewController extends LinearLayout {

    private BaseViewsControllerCommons parentManager;
    private ListView listview;
    private ArrayList<Object> lista;
    private int numeroCeldas;
    private int numeroFilas;
    private LlenarListadoAdapter adapter;
    private LayoutParams params;
    private float anchoColumna;
    private LayoutInflater inflater;
    private String title;
    private TextView titulo;
    private ArrayList<String> tablaDatosAColorear;
    //O3
    private ArrayList<String> tablaTitulosAColorear;
    private ArrayList<String> tablaDatosACambiarTamano;
    private ArrayList<String> nuevaTablaDatosACambiarTamano;

    public ListaDatosViewController(Context context, LayoutParams layoutParams, BaseViewsControllerCommons parentManager) {
        super(context);
        inflater = LayoutInflater.from(context);
        LinearLayout viewLayout = (LinearLayout) inflater.inflate(SuiteAppPagoCreditoApi.getResourceId("layout_lista_datos_view_admon", "layout"), this, true);
        viewLayout.setLayoutParams(layoutParams);
        this.parentManager = parentManager;

        listview = (ListView) findViewById(SuiteAppPagoCreditoApi.getResourceId("layout_lista_Datos", "id"));
        tablaDatosAColorear = new ArrayList<String>();
        tablaDatosAColorear.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("transferir.interbancario.tabla.resultados.importe", "string")));
        tablaDatosAColorear.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("transferir.dineromovil.resultados.codigo", "string")));
        tablaDatosAColorear.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("administrar.cambiarpassword.folioOperacionLabel", "string")));

        tablaDatosAColorear.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar_interbancario_estatus_liquidada", "string")));
        tablaDatosAColorear.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar_interbancario_estatus_devuelta", "string")));
        tablaDatosAColorear.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar_interbancario_estatus_proceso", "string")));
        tablaDatosAColorear.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar_interbancario_estatus_validacion", "string")));
        tablaDatosAColorear.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar_interbancario_estatus_enviada", "string")));
        //O3
        tablaTitulosAColorear = new ArrayList<String>();
        tablaTitulosAColorear.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar.obtenercomprobante.detalles.datos.titulo.datodelenvio", "string")));
        tablaTitulosAColorear.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar.obtenercomprobante.detalles.datos.titulo.datodeenvio", "string")));
        tablaTitulosAColorear.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar.depositosrecibidos.detalles.datos.titulo.datosuc", "string")));

        tablaDatosACambiarTamano = new ArrayList<String>();
        //tablaDatosACambiarTamaño.add(SuiteApp.getInstance().getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_tipoop));
        /******/
        nuevaTablaDatosACambiarTamano = new ArrayList<String>();
        nuevaTablaDatosACambiarTamano.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar.depositosrecibidos.detalles.datos.deporecibido", "string")));
        nuevaTablaDatosACambiarTamano.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar.depositosrecibidos.detalles.datos.titulo.datosuc", "string")));
        nuevaTablaDatosACambiarTamano.add(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar.depositosrecibidos.detalles.datos.referencianum", "string")));
    }

    public void setNumeroFilas(int numeroFilas) {
        this.numeroFilas = numeroFilas;
    }

    public void setTitulo(int resTitle) {
        this.title = parentManager.getCurrentViewControllerApp().getString(resTitle);
    }

    public void showLista() {
        if (title != null) {
            titulo = (TextView) findViewById(SuiteAppPagoCreditoApi.getResourceId("lista_datos_titulo", "id"));
            titulo.setText(title);
            titulo.setVisibility(View.VISIBLE);

            GuiTools.getCurrent().scale(titulo, true);
        }
        adapter = new LlenarListadoAdapter();
        listview.setAdapter(adapter);
        listview.setEnabled(false);

        ListAdapter listAdapter = listview.getAdapter();
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listview);
            listItem.measure(0, 0);
            totalHeight = listItem.getMeasuredHeight() * numeroFilas;
        }

        ViewGroup.LayoutParams params = listview.getLayoutParams();
        params.height = totalHeight + (listview.getDividerHeight() * (listview.getCount() - 1));
        listview.setLayoutParams(params);
        listview.requestLayout();
    }

    public ArrayList<Object> getLista() {
        return lista;
    }

    public void setLista(ArrayList<Object> lista) {
        this.lista = lista;
    }

    public int getNumeroCeldas() {
        return numeroCeldas;
    }

    public void setNumeroCeldas(int numeroCeldas) {
        this.numeroCeldas = numeroCeldas;
    }

    private class LlenarListadoAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return lista.size();
        }

        @Override
        public Object getItem(int position) {
            return lista.get(position);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            GuiTools guiTools = GuiTools.getCurrent();

            if (convertView == null) {
                convertView = inflater.inflate(SuiteAppPagoCreditoApi.getResourceId("list_item_productos_admon", "layout"), null);
            }

            anchoColumna = 1.0f / getNumeroCeldas();

            if (getNumeroCeldas() == 2) {
                params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.47f);
                ArrayList<Object> registro = (ArrayList<Object>) lista.get(position);

                TextView celda1 = (TextView) convertView.findViewById(SuiteAppPagoCreditoApi.getResourceId("lista_celda_1", "id"));
                celda1.setText((String) registro.get(0));
                celda1.setLayoutParams(params);
                celda1.setGravity(Gravity.LEFT);
                celda1.setMaxLines(1);
                //celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
                celda1.setSingleLine(true);
                celda1.setSelected(true);
                /*****O3*/
                //celda1.setEllipsize(null);
                if (tablaTitulosAColorear.contains((String) registro.get(0))) {
                    celda1.setTextColor(getResources().getColor(SuiteAppPagoCreditoApi.getResourceId("primer_azul", "color")));
                }
                if (nuevaTablaDatosACambiarTamano.contains((String) registro.get(0))) {//celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10.0f);
                    celda1.setEllipsize(null);
                    celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 12.0f);
                } else
                    celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
                guiTools.tryScaleText(celda1);

                params = new LayoutParams(0, LayoutParams.WRAP_CONTENT, 0.53f);

                TextView celda2 = (TextView) convertView.findViewById(SuiteAppPagoCreditoApi.getResourceId("lista_celda_2", "id"));
                celda2.setText((String) registro.get(1));
                if (tablaDatosAColorear.contains((String) registro.get(0))) {
                    celda2.setTextColor(getResources().getColor(R.color.tercer_azul));

                } else if (tablaDatosAColorear.contains((String) registro.get(1))) {
                    if (registro.get(1).toString().equalsIgnoreCase(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar_interbancario_estatus_liquidada", "string")))) {
                        celda2.setTextColor(getResources().getColor(R.color.verde));
                    } else if (registro.get(1).toString().equalsIgnoreCase(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar_interbancario_estatus_devuelta", "string")))) {
                        celda2.setTextColor(getResources().getColor(R.color.magenta));
                    } else if (registro.get(1).toString().equalsIgnoreCase(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar_interbancario_estatus_enviada", "string")))) {
                        celda2.setTextColor(getResources().getColor(R.color.tercer_azul));
                    } else if (registro.get(1).toString().equalsIgnoreCase(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar_interbancario_estatus_proceso", "string")))) {
                        celda2.setTextColor(getResources().getColor(R.color.naranja));
                    } else if (registro.get(1).toString().equalsIgnoreCase(SuiteAppPagoCreditoApi.appContext.getString(SuiteAppPagoCreditoApi.getResourceId("bmovil.consultar_interbancario_estatus_validacion", "string")))) {
                        celda2.setTextColor(getResources().getColor(R.color.naranja));
                    }
                }

                celda2.setLayoutParams(params);
                celda2.setGravity(Gravity.RIGHT);
                celda2.setMaxLines(1);
                //celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
                if (tablaDatosACambiarTamano.contains((String) registro.get(0))) {
                    celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10.0f);
                } else {
                    celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
                }
                celda2.setSingleLine(true);
                celda2.setSelected(true);
                guiTools.tryScaleText(celda2);

                ((TextView) convertView.findViewById(SuiteAppPagoCreditoApi.getResourceId("lista_celda_3", "id"))).setVisibility(View.GONE);
                ((TextView) convertView.findViewById(SuiteAppPagoCreditoApi.getResourceId("lista_celda_4", "id"))).setVisibility(View.GONE);
                ((ImageButton) convertView.findViewById(SuiteAppPagoCreditoApi.getResourceId("lista_celda_check", "id"))).setVisibility(View.GONE);
            }
            if (Server.ALLOW_LOG) Log.d("Filas", "fila " + position);

            return convertView;
        }

        @Override
        public long getItemId(int position) {
            // TODO Auto-generated method stub
            return 0;
        }
    }
}
