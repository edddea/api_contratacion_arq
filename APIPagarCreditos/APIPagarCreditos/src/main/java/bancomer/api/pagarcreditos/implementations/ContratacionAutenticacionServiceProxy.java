/**
 *
 */
package bancomer.api.pagarcreditos.implementations;

import android.util.Log;

import com.bancomer.base.SuiteApp;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.pagarcreditos.R;
import suitebancomer.aplicaciones.resultados.proxys.IContratacionAutenticacionServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.resultados.to.ParamTo;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

/**
 * @author lbermejo
 */
public class ContratacionAutenticacionServiceProxy
        implements IContratacionAutenticacionServiceProxy {

    /**
     */
    private static final long serialVersionUID = 3925667201345924488L;
    private BaseDelegateCommons baseDelegateCommons;

    public ContratacionAutenticacionServiceProxy(BaseDelegateCommons bdc) {
        this.baseDelegateCommons = bdc;
    }

    @Override
    public ArrayList<Object> getListaDatos() {

        //ContratacionAutenticacionDelegate
        //tipo operacion ContratacionDelegate   o CambioPerfilDelegate
        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getListaDatos >> delegate");
        final ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) baseDelegateCommons;
        ArrayList<Object> list = delegate.consultaOperationsDelegate().getDatosTablaConfirmacion();
        return list;
    }

    @Override
    public ConfirmacionViewTo showFields() {

        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy showFields >> delegate");
        ConfirmacionViewTo to = new ConfirmacionViewTo();
        final ContratacionAutenticacionDelegate delegate =
                (ContratacionAutenticacionDelegate) baseDelegateCommons;

        to.setShowContrasena(delegate.consultaDebePedirContrasena());
        to.setShowNip(delegate.consultaDebePedirNIP());
        to.setTokenAMostrar(delegate.consultaInstrumentoSeguridad());
        to.setShowCvv(delegate.consultaDebePedirCVV());
        to.setShowTarjeta(delegate.mostrarCampoTarjeta());

        to.setTokenAMostrar(delegate.consultaInstrumentoSeguridad());
        to.setInstrumentoSeguridad(delegate.consultaTipoInstrumentoSeguridad());
        to.setTextoAyudaInsSeg(
                delegate.getTextoAyudaInstrumentoSeguridad(
                        to.getInstrumentoSeguridad()));

        if (delegate.consultaOperationsDelegate() instanceof ContratacionDelegate) { //Llegamos desde contratación
            ContratacionDelegate contratacionDelegate =
                    (ContratacionDelegate) delegate.consultaOperationsDelegate();

            if (contratacionDelegate.isDeleteData()) {
                contratacionDelegate.deleteData();
            }
        }
        /*
		mostrarContrasena(contratacionAutenticacionDelegate.consultaDebePedirContrasena());
		mostrarNIP(contratacionAutenticacionDelegate.consultaDebePedirNIP());
		mostrarASM(contratacionAutenticacionDelegate.consultaInstrumentoSeguridad());
		mostrarCVV(contratacionAutenticacionDelegate.consultaDebePedirCVV());
		mostrarCampoTarjeta(contratacionAutenticacionDelegate.mostrarCampoTarjeta());	
		 */
		
		/*
		 * if ((contratacionAutenticacionDelegate.consultaOperationsDelegate().mostrarCVV())
				&& (!contratacionAutenticacionDelegate.consultaOperationsDelegate().mostrarNIP())) {
		 */
        return to;
    }

    @Override
    public Integer getMessageAsmError(Constants.TipoInstrumento tipoInstrumento) {

        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy getMessageAsmError >> delegate");
        int idMsg = 0;

        switch (tipoInstrumento) {
            case OCRA:
                idMsg = R.string.confirmation_ocra;
                break;
            case DP270:
                idMsg = R.string.confirmation_dp270;
                break;
            case SoftToken:
                if (SuiteApp.getSofttokenStatus()) {
                    idMsg = R.string.confirmation_softtokenActivado;
                } else {
                    idMsg = R.string.confirmation_softtokenDesactivado;
                }
                break;
            default:
                break;
        }

        return idMsg;
		
		/*switch (tipoInstrumentoSeguridad) {
			case OCRA:
				mensaje += getEtiquetaCampoOCRA();
				break;
			case DP270:
				mensaje += getEtiquetaCampoDP270();
				break;
			case SoftToken:
				if (SuiteApp.getSofttokenStatus()) {
					mensaje += getEtiquetaCampoSoftokenActivado();
				} else {
					mensaje += getEtiquetaCampoSoftokenDesactivado();
				}
				break;
			default:
				break;
		}
		*/
    }

    @Override
    public String loadOtpFromSofttoken(Constants.TipoOtpAutenticacion tipoOtpAutenticacion) {

        if (Server.ALLOW_LOG)
            Log.d(getClass().getName(), ">>proxy loadOtpFromSofttoken >> delegate");
        final ContratacionAutenticacionDelegate delegate =
                (ContratacionAutenticacionDelegate) baseDelegateCommons;
        String res = delegate.loadOtpFromSofttoken(tipoOtpAutenticacion);
        return res;
    }

    @Override
    public Boolean doOperation(ParamTo to) {

        if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy doOperation >> delegate");
        final ContratacionAutenticacionDelegate delegate =
                (ContratacionAutenticacionDelegate) baseDelegateCommons;

        ConfirmacionViewTo params = (ConfirmacionViewTo) to;
        ContratacionAutenticacionViewController caller = new ContratacionAutenticacionViewController();
        caller.setDelegate(delegate);
        caller.setParentViewsController(
                ((BmovilViewsController) SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getViewsController()));
        delegate.setcontratacionAutenticacionViewController(caller);
        try {

            String newToken = null;
            if (params.getTokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno
                    && params.getInstrumentoSeguridad() == Constants.TipoInstrumento.SoftToken
                    && SuiteApp.getSofttokenStatus()) {
                newToken = loadOtpFromSofttoken(params.getTokenAMostrar());
            }
            if (null != newToken) {
                params.setAsm(newToken);
            }
            //AMZ
            delegate.consultaOperationsDelegate().realizaOperacion(
                    caller, params.getNip(), params.getAsm(), params.getCvv(),
                    params.getContrasena(), params.getOkTerminos(), params.getTarjeta());

            return Boolean.TRUE;
        } catch (Exception e) {
            if (Server.ALLOW_LOG) Log.d(getClass().getName(), ">>proxy doOperation >> delegate");
            Log.e(getClass().getName(), ">>proxy doOperation >> delegate", e);
            return Boolean.FALSE;
        }

    }

    //@Override
    public Integer consultaOperationsIdTextoEncabezado() {

        if (Server.ALLOW_LOG)
            Log.d(getClass().getName(), ">>proxy consultaOperationsIdTextoEncabezado >> delegate");
        //getString(confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado()
        final ContratacionAutenticacionDelegate delegate =
                (ContratacionAutenticacionDelegate) baseDelegateCommons;
        int res = delegate.consultaOperationsDelegate().getTextoEncabezado();

        return res;
    }

    public Constants.Perfil consultaClienteProfile() {

        if (Server.ALLOW_LOG)
            Log.d(getClass().getName(), ">>proxy consultaClienteProfile >> delegate");
        return Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile();
    }

    public void consultarTerminosDeUso() {

        if (Server.ALLOW_LOG)
            Log.d(getClass().getName(), ">>proxy consultarTerminosDeUso >> delegate");
        final ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) baseDelegateCommons;
        ContratacionAutenticacionViewController caller = new ContratacionAutenticacionViewController();
        caller.setDelegate(delegate);
        caller.setParentViewsController(
                ((BmovilViewsController) SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getViewsController()));
        delegate.setcontratacionAutenticacionViewController(caller);
        delegate.consultarTerminosDeUso();

    }

    @Override
    public BaseDelegateCommons getDelegate() {
        return baseDelegateCommons;
    }

}
