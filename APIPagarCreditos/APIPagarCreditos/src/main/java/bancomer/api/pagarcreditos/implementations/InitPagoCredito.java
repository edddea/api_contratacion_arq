package bancomer.api.pagarcreditos.implementations;

import android.content.Context;

import com.bancomer.base.callback.CallBackAPI;
import com.bancomer.base.callback.CallBackSession;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import bancomer.api.pagarcreditos.gui.controllers.PagarCreditoViewController;
import bancomer.api.pagarcreditos.gui.delegates.PagarCreditoDelegate;
import bancomer.api.pagarcreditos.io.BanderasServer;
import bancomer.api.pagarcreditos.models.Credito;
import bancomer.api.pagarcreditos.models.ImportesPagoCreditoData;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

/**
 * Created by jinclan on 14/08/15.
 */
public class InitPagoCredito {

    public static InitPagoCredito initPagoCredito;
    boolean isActivityChanging;
    private Context context;
    //Estas son las APIS que dependen
    private SuiteAppPagoCreditoApi suiteAppPagoCreditoApi;
    private SuiteAppApi suiteAppApiSoftoken;
    private CallBackSession callBackSession;
    private CallBackAPI callBackAPI;

    private InitPagoCredito(BanderasServer banderasServer) {
        ServerCommons.ALLOW_LOG = banderasServer.getLogActivo();
        ServerCommons.DEVELOPMENT = banderasServer.isDesarrollo();
        ServerCommons.EMULATOR = banderasServer.getEmulador();
        ServerCommons.SIMULATION = banderasServer.getSimulacion();
    }

    public static InitPagoCredito getInstance(Context context, BanderasServer bandera) {
        if (initPagoCredito == null) {
            initPagoCredito = new InitPagoCredito(bandera);
        }
        initPagoCredito.setContext(context);
        return initPagoCredito;
    }

    public void setParameters(boolean ALLOW_LOG, boolean DEVELOPMENT, boolean EMULATOR, boolean SIMULATION) {
        ServerCommons.ALLOW_LOG = ALLOW_LOG;
        ServerCommons.DEVELOPMENT = DEVELOPMENT;
        ServerCommons.EMULATOR = EMULATOR;
        ServerCommons.SIMULATION = SIMULATION;
        // initializeDependenceApis();
    }

    public void initializeDependenceApis() {
        //Levanta la Api de Administracion
        //if(suiteAppPagoCreditoApi ==null){
        suiteAppPagoCreditoApi = new SuiteAppPagoCreditoApi();
        suiteAppPagoCreditoApi.onCreate(context);
        //}

        //Levanta el Api de Softoken
        //if(suiteAppApiSoftoken == null){
        suiteAppApiSoftoken = new SuiteAppApi();
        suiteAppApiSoftoken.onCreate(context);
        //}
    }

    /*
    * Para poder utilizarlas y setearles valores si es necesario
    * */
    public SuiteAppPagoCreditoApi getSuiteAppPagoCreditoApi() {
        return suiteAppPagoCreditoApi;
    }

    public SuiteAppApi getSuiteAppApiSoftoken() {
        return suiteAppApiSoftoken;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public CallBackSession getCallBackSession() {
        return callBackSession;
    }

    public void setCallBackSession(CallBackSession callBackSession) {
        this.callBackSession = callBackSession;
    }

    public CallBackAPI getCallBackAPI() {
        return callBackAPI;
    }

    public void setCallBackAPI(CallBackAPI callBackAPI) {
        this.callBackAPI = callBackAPI;
    }

    public void consultarImportes(String numeroCredito) {
        PagarCreditoDelegate delegate = (PagarCreditoDelegate) SuiteAppPagoCreditoApi
                .getInstance().getBmovilApplication()
                .getBmovilViewsController().getBaseDelegateForKey(PagarCreditoDelegate.PAGO_CREDITO_DELEGATE_ID);
        if (delegate == null) {
            delegate = new PagarCreditoDelegate();
            SuiteAppPagoCreditoApi
                    .getInstance().getBmovilApplication()
                    .getBmovilViewsController().addDelegateToHashMap(PagarCreditoDelegate.PAGO_CREDITO_DELEGATE_ID,
                    delegate);
        }

        PagarCreditoViewController pagarCreditoViewController = new PagarCreditoViewController();
        pagarCreditoViewController.setDelegate(delegate);
        delegate.setPagarCreditoViewController(pagarCreditoViewController);

        delegate.consultaImportesPagoCredito(numeroCredito);
    }

    public void configurar() {
        initPagoCredito.initializeDependenceApis();
        final bancomer.api.pagarcreditos.implementations.BaseViewsController baseViewsController = SuiteAppPagoCreditoApi
                .getInstance().getBmovilApplication()
                .getBmovilViewsController();
        final PagarCreditoDelegate delegate = new PagarCreditoDelegate();
        delegate.setCuentaActual(suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.obtenerCuentaEje());

        baseViewsController.addDelegateToHashMap(
                PagarCreditoDelegate.PAGO_CREDITO_DELEGATE_ID, delegate);
        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication()
                .setApplicationLogged(true);
        SuiteAppPagoCreditoApi.setCallBackSession(callBackSession);
    }

    public void setImportesAndCredito(ImportesPagoCreditoData importesPagoCreditoData, Credito credito) {
        PagarCreditoDelegate delegate = (PagarCreditoDelegate) SuiteAppPagoCreditoApi
                .getInstance().getBmovilApplication()
                .getBmovilViewsController().getBaseDelegateForKey(PagarCreditoDelegate.PAGO_CREDITO_DELEGATE_ID);
        if (delegate == null) {
            delegate = new PagarCreditoDelegate();
            SuiteAppPagoCreditoApi
                    .getInstance().getBmovilApplication()
                    .getBmovilViewsController().addDelegateToHashMap(PagarCreditoDelegate.PAGO_CREDITO_DELEGATE_ID,
                    delegate);
        }
        delegate.setCreditoActual(credito);
        delegate.setImportesPagoCredito(importesPagoCreditoData);
    }

    public void returnDataToPrincipal(String operation, ServerResponse response) {
        callBackAPI.returnDataFromModule(operation, response);
    }
}
