package bancomer.api.pagarcreditos.implementations;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.commons.GuiTools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;

public class BaseViewsController extends BaseViewsControllerCommons {

    protected static BaseViewControllerCommons currentViewController;
    private static Method methodOverridePendingTransition;

    static {
        Method[] actMethods = Activity.class.getMethods();
        int methodSize = actMethods.length;
        for (int i = 0; i < methodSize; i++) {
            if (actMethods[i].getName().equals("overridePendingTransition")) {
                methodOverridePendingTransition = actMethods[i];
                break;
            }
        }
    }

    //protected Activity rootViewController;
    protected BaseViewControllerCommons rootViewController;
    protected BaseViewControllerCommons currentViewControllerApp;
    protected Activity activityContext;

//	public void setActivityContext(Activity activityContext) {
//		this.activityContext = activityContext;
/*	}
public Activity getRootViewController() {
	return rootViewController;
}*/
    //protected Activity activityContext;
    private boolean isActivityChanging;

    public BaseViewControllerCommons getRootViewController() {
        return rootViewController;
    }

    public BaseViewControllerCommons getCurrentViewControllerApp() {
        return currentViewControllerApp;
    }

    public BaseViewControllerCommons getCurrentViewController() {
        return currentViewController;
    }

    public void setCurrentActivityApp(BaseViewControllerCommons currentViewController) {
        setCurrentActivityApp(currentViewController, false);
    }

    public void setCurrentActivityApp(BaseViewControllerCommons currentViewController, boolean asRootViewController) {
        this.currentViewControllerApp = currentViewController;
        if (rootViewController == null) {
            this.rootViewController = currentViewController;
        }
        isActivityChanging = false;

        BaseViewsController.currentViewController = currentViewController;
    }

    public boolean isActivityChanging() {
        return isActivityChanging;
    }

    public void setActivityChanging(boolean flag) {
        isActivityChanging = flag;
    }

    protected void showViewController(Class<?> viewController) {
        showViewController(viewController, 0, false);
    }

    public void showViewController(Class<?> viewController, int flags) {
        showViewController(viewController, flags, false);
    }

    public void showViewController(Class<?> viewController, int flags, boolean inverted) {
        showViewController(viewController, flags, inverted, null, null);
    }

    /**
     * Muestra la actividad especificada con los parametros indicados
     *
     * @param viewController La actividad a mostrar
     * @param flags          Banderas que se colocaran en el intent de la actividad
     * @param inverted       Si la pantalla debe mostrarse con animaci�n hacia adelante o hacia atras
     * @param extras         Arreglo con los extras a pasar al intento, deben acomodarse de manera llave - valor y la llave debe ser
     *                       de tipo String, si esta regla no se cumple no pasara nig�n par�metro al intento
     */
    public void showViewController(Class<?> viewController, int flags, boolean inverted,
                                   String[] extrasKeys, Object[] extras) {
        isActivityChanging = true;
        Intent intent = new Intent(SuiteAppPagoCreditoApi.appContext, viewController);
        if (flags != 0) {
            intent.setFlags(flags);
        }

        if (extrasKeys != null && extras != null && extras.length == extrasKeys.length) {
            int extrasLength = extras.length;
            for (int i = 0; i < extrasLength; i++) {
                if (extras[i] instanceof String) {
                    intent.putExtra(extrasKeys[i], (String) extras[i]);
                } else if (extras[i] instanceof Integer) {
                    intent.putExtra(extrasKeys[i], ((Integer) extras[i]).intValue());
                } else if (extras[i] instanceof Boolean) {
                    intent.putExtra(extrasKeys[i], ((Boolean) extras[i]).booleanValue());
                } else if (extras[i] instanceof Long) {
                    intent.putExtra(extrasKeys[i], ((Long) extras[i]).longValue());
                }
            }
        }

        SuiteAppPagoCreditoApi.appContext.startActivity(intent);
        if (inverted) {
            overrideScreenBackTransition();
        } else {
            overrideScreenForwardTransition();
        }
    }

    public void showViewController(Intent intent, int flags, boolean inverted, String[] extrasKeys, Object[] extras) {
        isActivityChanging = true;

        if (flags != 0) {
            intent.setFlags(flags);
        }

        if (extrasKeys != null && extras != null && extras.length == extrasKeys.length) {
            int extrasLength = extras.length;
            for (int i = 0; i < extrasLength; i++) {
                if (extras[i] instanceof String) {
                    intent.putExtra(extrasKeys[i], (String) extras[i]);
                } else if (extras[i] instanceof Integer) {
                    intent.putExtra(extrasKeys[i], ((Integer) extras[i]).intValue());
                } else if (extras[i] instanceof Boolean) {
                    intent.putExtra(extrasKeys[i], ((Boolean) extras[i]).booleanValue());
                } else if (extras[i] instanceof Long) {
                    intent.putExtra(extrasKeys[i], ((Long) extras[i]).longValue());
                }
            }
        }

        currentViewControllerApp.startActivity(intent);
        if (inverted) {
            overrideScreenBackTransition();
        } else {
            overrideScreenForwardTransition();
        }
    }

    /**
     * Muestra la actividad especificada esperando un resultado por parte de la actividad que la invoca
     *
     * @param viewController La actividad a mostrar
     * @param activityCode   codigo que se le asiganra a esta actividad
     */
    protected void showViewControllerForResult(Class<?> viewController, int activityCode) {
        isActivityChanging = true;
        Intent intent = new Intent(currentViewControllerApp, viewController);

        intent.setAction("com.google.zxing.client.android.SCAN");
        //intent.putExtra("ACTION", "com.google.zxing.client.android.SCAN");

        intent.putExtra("SCAN_WIDTH", (int) (GuiTools.getCurrent().ScreenHeigth * 0.9));
        intent.putExtra("SCAN_HEIGHT", (int) (GuiTools.getCurrent().ScreenWidth * 0.2));

        overrideScreenForwardTransition();
        currentViewControllerApp.startActivityForResult(intent, activityCode);

    }

    protected void backToRootViewController() {
        Intent intent = new Intent(currentViewControllerApp, rootViewController.getClass());
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        currentViewControllerApp.startActivity(intent);
        overrideScreenBackTransition();
    }

    public void cierraViewsControle() {
        rootViewController = null;
//		if (currentViewControllerApp != null && !(currentViewControllerApp instanceof MenuSuiteViewController)) {
//			currentViewControllerApp.finish();
//			overrideScreenBackTransition();
//			currentViewControllerApp = null;
//		}

        if (delegatesHashMap != null) {
            delegatesHashMap.clear();
        }
    }

    public Long getLastDelegateKey() {
        return (Long) delegatesHashMap.keySet().toArray()[delegatesHashMap.size() - 1];
    }

    public void removeDelegateFromHashMap(long key) {
        delegatesHashMap.remove(Long.valueOf(key));
    }

    public void clearDelegateHashMap() {
        delegatesHashMap.clear();
    }

    public void overrideScreenForwardTransition() {
        if (methodOverridePendingTransition != null) {
            try {
                methodOverridePendingTransition.invoke(currentViewControllerApp, new Object[]{R.anim.screen_enter, R.anim.screen_leave});
            } catch (NullPointerException npex) {
                //receiver was null
                if (Server.ALLOW_LOG)
                    Log.d("BaseViewsController :: overrideScreenForwardTransition", "Receiver was null");
            } catch (IllegalAccessException iaex) {
                //method not accesible
                if (Server.ALLOW_LOG)
                    Log.d("BaseViewsController :: overrideScreenForwardTransition", "Method not accesible");
            } catch (IllegalArgumentException iarex) {
                //incorrect arguments
                if (Server.ALLOW_LOG)
                    Log.d("BaseViewsController :: overrideScreenForwardTransition", "Incorrect arguments");
            } catch (InvocationTargetException itex) {
                //invocation returned exception
                if (Server.ALLOW_LOG)
                    Log.d("BaseViewsController :: overrideScreenForwardTransition", "Invocation exception: " + itex.toString());
            }
        }
    }

//    public void onUserInteraction() {}

    public void overrideScreenBackTransition() {
        if (methodOverridePendingTransition != null) {
            try {
                methodOverridePendingTransition.invoke(currentViewControllerApp, new Object[]{R.anim.screen_enter_back, R.anim.screen_leave_back});
            } catch (NullPointerException npex) {
                //receiver was null
                if (Server.ALLOW_LOG)
                    Log.d("BaseViewsController :: overrideScreenBackTransition", "Receiver was null");
            } catch (IllegalAccessException iaex) {
                //method not accesible
                if (Server.ALLOW_LOG)
                    Log.d("BaseViewsController :: overrideScreenBackTransition", "Method not accesible");
            } catch (IllegalArgumentException iarex) {
                //incorrect arguments
                if (Server.ALLOW_LOG)
                    Log.d("BaseViewsController :: overrideScreenBackTransition", "Incorrect arguments");
            } catch (InvocationTargetException itex) {
                //invocation returned exception
                if (Server.ALLOW_LOG)
                    Log.d("BaseViewsController :: overrideScreenBackTransition", "Invocation exception: " + itex.toString());
            }
        }
    }

    public boolean consumeAccionesDePausa() {
        return false;
    }

    public boolean consumeAccionesDeReinicio() {
        return false;
    }

    public boolean consumeAccionesDeAlto() {
        return false;
    }

    /**
     * Muestra el menu inicial de la aplicación que implemente el método.
     */
    public void showMenuInicial() {
    }

    public Activity getActivityContext() {
        return activityContext;
    }

    public void setActivityContext(Activity activityContext) {
        this.activityContext = activityContext;
        //this.rootViewController = activityContext;
    }

    public void showConsultarEstatusEnvioEstadodeCuenta() {
//		ConsultarEstatusEnvioEstadodeCuentaDelegate delegate = (ConsultarEstatusEnvioEstadodeCuentaDelegate) getBaseDelegateForKey(ConsultarEstatusEnvioEstadodeCuentaDelegate.CONSULTARESTATUSENVIOESTADODECUENTA_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new ConsultarEstatusEnvioEstadodeCuentaDelegate();
//			addDelegateToHashMap(
//					ConsultarEstatusEnvioEstadodeCuentaDelegate.CONSULTARESTATUSENVIOESTADODECUENTA_DELEGATE_ID, delegate);
//		}
//		showViewController(ConsultarEstatusEnvioEstadodeCuentaViewController.class);
    }
}
