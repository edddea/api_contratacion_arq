package bancomer.api.pagarcreditos.implementations;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.commons.GuiTools;
import bancomer.api.pagarcreditos.commons.TrackingHelper;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.LoginDelegate;

/**
 * La pantalla del cambio de perfil del usuario.
 *
 * @author CGI
 */
public class CambioPerfilViewController extends BaseViewController implements
        View.OnClickListener {

    /**
     * Delegate para cambio de perfil.
     */
    private CambioPerfilDelegate cambioPerfilDelegate;

    /**
     * .
     */
    private TextView texto1;

    /**
     * Texto de ayuda.
     */
    private TextView textoAyuda;
    private TextView texto2;

    /**
     * Boton de cancelar.
     */
    private Button botonCancelar;

    /**
     * Boton de aceptar.
     */
    private Button botonAceptar;
    //AMZ
    private BmovilViewsController parentManager;

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE,
                SuiteAppPagoCreditoApi.getResourceId("layout_bmovil_cambio_perfil_admon", "layout"));
        SuiteApp.appContext = this;
        setTitle(R.string.cambioPerfil_titulo, R.drawable.icono_cambio_perfil,
                R.color.naranja);

        setParentViewsController(SuiteAppPagoCreditoApi.getInstance().getBmovilApplication()
                .getBmovilViewsController());
        setDelegate((CambioPerfilDelegate) getParentViewsController().getBaseDelegateForKey(
                CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID));

        cambioPerfilDelegate = (CambioPerfilDelegate) getDelegate();
        findViews();
        setMessages();
        scaleToScreenSize();

        botonCancelar.setOnClickListener(this);
        botonAceptar.setOnClickListener(this);
        textoAyuda.setOnClickListener(this);

        cambioPerfilDelegate.setNuevoPerfil(null);

        //AMZ
        //CambioPerfilDelegate cambioPerfilDelegate = new CambioPerfilDelegate();
        if (Constants.Perfil.avanzado.equals(Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile()))
        //(cambioPerfilDelegate.verificarCambioPerfil(Session.getInstance(SuiteApp.appContext).isAdvancedProfile()))
        {
            //AMZ
            parentManager = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController();
            TrackingHelper.trackState("operarcontoken", parentManager.estados);
        } else {
            //AMZ
            parentManager = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController();
            TrackingHelper.trackState("operarsintoken", parentManager.estados);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onResume()
     */
    @Override
    protected void onResume() {
        super.onResume();
        SuiteApp.appContext = this;
        if (parentViewsController.consumeAccionesDeReinicio()) {
            return;
        }

        getParentViewsController().setCurrentActivityApp(this);
        cambioPerfilDelegate.setBaseViewController(this);
        cambioPerfilDelegate.setOwnerController(this);
    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onPause()
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /*
     * (non-Javadoc)
     *
     * @see android.view.View.OnClickListener#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {

        if (v == botonAceptar) {
            cambioPerfilDelegate.cambioPerfilAceptado();
            //AMZ
            Map<String, Object> Paso1OperacionMap = new HashMap<String, Object>();
            //AMZ
            //CambioPerfilDelegate cambioPerfilDelegate = new CambioPerfilDelegate();
            if (Constants.Perfil.avanzado.equals(Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile()))
            //(cambioPerfilDelegate.verificarCambioPerfil(Session.getInstance(
            //	SuiteApp.appContext).isAdvancedProfile()))
            {
                if (parentManager.estados.size() > 2) {
                    //AMZ
                    Paso1OperacionMap.put("evento_paso1", "event46");
                    Paso1OperacionMap.put("&&products", "operaciones;admin+operar con token");
                    Paso1OperacionMap.put("eVar12", "paso1:aviso");
                    TrackingHelper.trackPaso1Operacion(Paso1OperacionMap);
                }

            } else {
                if (parentManager.estados.size() > 2) {
                    //AMZ
                    Paso1OperacionMap.put("evento_paso1", "event46");
                    Paso1OperacionMap.put("&&products", "operaciones;admin+operar sin token");
                    Paso1OperacionMap.put("eVar12", "paso1:aviso");
                    TrackingHelper.trackPaso1Operacion(Paso1OperacionMap);
                }


            }
        } else if (v == botonCancelar) {
            cambioPerfilDelegate.cambioPerfilCancelado();
            // Se cancela el cambio de perfil y se muestra la pantalla de menu
            // si el flujo es desde Login, o la pantalla anterior para el resto
            // de casos
            Session session = Session.getInstance(SuiteAppPagoCreditoApi.getInstance());
            if (SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().isApplicationLogged() && !session.isCmbPerfilHamburguesa()) {
                finish();
                if (SuiteAppPagoCreditoApi.getCallBackBConnect() != null) {
                    if (ServerCommons.ALLOW_LOG) {
                        Log.d(getClass().getSimpleName(), "Se ejecuta returnMenuPrincipal desde CallBackBConnect");
                    }
                    SuiteAppPagoCreditoApi.getCallBackBConnect().returnMenuPrincipal();
                } else if (SuiteAppPagoCreditoApi.getCallBackSession() != null) {
                    if (ServerCommons.ALLOW_LOG) {
                        Log.d(getClass().getSimpleName(), "Se ejecuta returnMenuPrincipal desde CallBackSession");
                    }
                    SuiteAppPagoCreditoApi.getCallBackSession().returnMenuPrincipal();
                } else {
                    SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(false);
                }
            } else {
                super.goBack();
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see suitebancomer.classes.gui.controllers.BaseViewController#goBack()
     */
    @Override
    public void goBack() {
        // Solo cuando el perfil del usuario es avanzado se le permite volver a
        // la pantalla anterior
        if (Constants.Perfil.avanzado.equals(Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile())) {
            cambioPerfilDelegate.cambioPerfilCancelado();
        }
        if (!cambioPerfilDelegate.isCambioPerfilLogin()) {
            super.goBack();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see suitebancomer.classes.gui.controllers.BaseViewController#
     * processNetworkResponse(int,
     * suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
     */
    @Override
    public void processNetworkResponse(int operationId, ServerResponse response) {
        cambioPerfilDelegate.analyzeResponse(operationId, response);
    }

    /**
     * Inicializa las vistas de la pantalla.
     */
    private void findViews() {
        texto1 = (TextView) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambio_perfil_texto1", "id"));
        texto2 = (TextView) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambio_perfil_texto2", "id"));
        textoAyuda = (TextView) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambio_perfil_ayuda", "id"));
        botonCancelar = (Button) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambiar_perfil_boton_cancelar", "id"));
        botonAceptar = (Button) findViewById(SuiteAppPagoCreditoApi.getResourceId("cambiar_perfil_boton_aceptar", "id"));
    }

    /**
     * Escala el layout al tamanno de la pantalla.
     */
    private void scaleToScreenSize() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(texto1, true);
        guiTools.scale(texto2, true);
        guiTools.scale(textoAyuda, true);

        guiTools.scale(botonCancelar);
        guiTools.scale(botonAceptar);
    }

    /**
     * Muestra los siguientes mensajes: beneficios del upgrade al cambiar de
     * perfil de basico a avanzado; y desventajas del downgrade al cambio de
     * perfil de avanzado a basico.
     */
    private void setMessages() {
        if (Constants.Perfil.avanzado.equals(Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile())) {
            texto1.setText(R.string.cambioPerfil_downgrade_mensaje1);
            texto2.setText(R.string.cambioPerfil_downgrade_mensaje2);
        } else if (Constants.Perfil.basico.equals(Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile())) {
            texto1.setText(R.string.cambioPerfil_upgrade_mensaje1);
            texto2.setText(R.string.cambioPerfil_upgrade_mensaje2);
        } else if (Constants.Perfil.recortado.equals(Session.getInstance(SuiteAppPagoCreditoApi.appContext).getClientProfile())) {
            texto1.setText(R.string.cambioPerfil_downgrade_recortado_mensaje1);
            texto2.setText(R.string.cambioPerfil_downgrade_recortado_mensaje2);
        }
    }

}