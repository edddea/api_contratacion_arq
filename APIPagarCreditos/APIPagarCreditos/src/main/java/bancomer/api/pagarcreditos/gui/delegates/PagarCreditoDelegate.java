package bancomer.api.pagarcreditos.gui.delegates;

import android.util.Log;

import com.bancomer.base.SuiteApp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants;
import bancomer.api.pagarcreditos.R;
import bancomer.api.pagarcreditos.gui.controllers.PagarCreditoViewController;
import bancomer.api.pagarcreditos.implementations.BaseViewController;
import bancomer.api.pagarcreditos.implementations.BmovilViewsController;
import bancomer.api.pagarcreditos.implementations.ConfirmacionViewController;
import bancomer.api.pagarcreditos.implementations.DelegateBaseAutenticacion;
import bancomer.api.pagarcreditos.implementations.InitPagoCredito;
import bancomer.api.pagarcreditos.implementations.SuiteAppPagoCreditoApi;
import bancomer.api.pagarcreditos.models.Credito;
import bancomer.api.pagarcreditos.models.ImportesPagoCreditoData;
import bancomer.api.pagarcreditos.models.PagoDeCredito;
import bancomer.api.pagarcreditos.models.ResultadoPagoCreditoData;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.SolicitarAlertasData;

/**
 * Created by andres.vicentelinare on 21/10/2015.
 */
public class PagarCreditoDelegate extends DelegateBaseAutenticacion {

    public final static long PAGO_CREDITO_DELEGATE_ID = 4L;
    private BaseViewController pagarCreditoViewController;

    private Account cuentaTDC;
    private ImportesPagoCreditoData importesPagoCredito;

    private PagoDeCredito pagoCredito = new PagoDeCredito();
    private Constants.Operacion operacion = Constants.Operacion.pagoCreditoCH;

    private ResultadoPagoCreditoData resultado;

    private Account cuentaOrigenSeleccionada = null;
    private Credito creditoActual;

    /********************************************************* End Variables *******************************************************/

    /*********************************************************
     * Getters&Setters
     *******************************************************/

    public Account getCuentaOrigenSeleccionada() {
        return cuentaOrigenSeleccionada;
    }

    public void setCuentaOrigenSeleccionada(Account cuentaOrigenSeleccionada) {
        this.cuentaOrigenSeleccionada = cuentaOrigenSeleccionada;
    }

    public Credito getCreditoActual() {
        return creditoActual;
    }

    public void setCreditoActual(Credito creditoActual) {
        this.creditoActual = creditoActual;
    }

    public ResultadoPagoCreditoData getResultado() {
        return resultado;
    }


    public PagoDeCredito getPagoCreditoData() {
        return pagoCredito;
    }

    public void setPagoCreditoData(PagoDeCredito dataTDC) {
        this.pagoCredito = dataTDC;
    }

    public ImportesPagoCreditoData getImportesPagoCredito() {
        return importesPagoCredito;
    }

    public void setImportesPagoCredito(ImportesPagoCreditoData importesPagoCredito) {
        this.importesPagoCredito = importesPagoCredito;
    }

    public Account getCuentaTDC() {
        return cuentaTDC;
    }

    public void setCuentaTDC(Account cuentaTDC) {
        this.cuentaTDC = cuentaTDC;
    }

    public void setCuentaActual(Account cuenta) {
        cuentaTDC = cuenta;
    }

    public BaseViewController getPagarCreditoViewController() {
        return pagarCreditoViewController;
    }

    public void setPagarCreditoViewController(BaseViewController pagarCreditoViewController) {
        this.pagarCreditoViewController = pagarCreditoViewController;
    }

    /*********************************************************
     * End Getters&Setters
     *******************************************************/

    public void setDataPagoCredito() {
        pagoCredito.setTipoCredito(creditoActual.getTipoCredito());
        pagoCredito.setNumCredito(creditoActual.getNumeroCredito());
        pagoCredito.setCuentaRetiro(((PagarCreditoViewController) pagarCreditoViewController).getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada().getNumber());
        pagoCredito.setNombreBeneficiario(Session.getInstance(SuiteAppPagoCreditoApi.appContext).getNombreCliente());
        pagoCredito.setImportePago(importesPagoCredito.getPagoRequerido());
        pagoCredito.setImportePagoMoneda(importesPagoCredito.getPagoRequeridoMoneda());
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c.getTime());
        pagoCredito.setFechaPago(formattedDate);
    }

    /**
     * Obtiene la lista de cuentas a mostrar en el componente CuentaOrigen, la lista es ordenada seg�n sea reguerido.
     *
     * @return
     */
    public ArrayList<Account> cargaCuentasOrigen() {
        Constants.Perfil profile = Session.getInstance(SuiteApp.appContext).getClientProfile();

        ArrayList<Account> accountsArray = new ArrayList<Account>();
        Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
        Boolean cuentaEjeTDC = true;

        if (profile == Constants.Perfil.avanzado) {
            for (Account acc : accounts) {
                if (acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE)) {
                    accountsArray.add(acc);
                    break;
                }
            }
            for (Account acc : accounts) {
                if (!acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE))
                    accountsArray.add(acc);
            }
        } else {
            for (Account acc : accounts) {
                if (acc.isVisible() && !acc.getType().equalsIgnoreCase(Constants.CREDIT_TYPE))
                    accountsArray.add(acc);
            }
        }

        if (((profile == Constants.Perfil.basico) || (accounts.length == 1)) && Tools.obtenerCuentaEje() != null && (Tools.obtenerCuentaEje().getType().equals(Constants.CREDIT_TYPE))) {
            pagarCreditoViewController.showInformationAlert(R.string.error_cuenta_eje_credito);
        }

        return accountsArray;
    }

    public void consultaImportesPagoCredito(String numeroCredito) {
        Session session = Session.getInstance(SuiteApp.appContext);

        //prepare data
        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
        paramTable.put(ServerConstants.NUMERO_CREDITO, numeroCredito);

        doNetworkOperation(ApiConstants.OP_CONSULTA_IMPORTES_PAGO_CREDITOS, paramTable, true, new ImportesPagoCreditoData(), pagarCreditoViewController);
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if (operationId == ApiConstants.OP_CONSULTA_IMPORTES_PAGO_CREDITOS) {
            InitPagoCredito.initPagoCredito.returnDataToPrincipal(String.valueOf(ApiConstants.OP_CONSULTA_IMPORTES_PAGO_CREDITOS), response);
        } else if (operationId == ApiConstants.OP_PAGO_CREDITO_CH) {
            Log.e("niko", "entra en res");
            if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                resultado = (ResultadoPagoCreditoData) response.getResponse();
                showResultados();
            } else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
                Session session = Session.getInstance(SuiteApp.appContext);
                Constants.Perfil perfil = session.getClientProfile();

                if (Constants.Perfil.recortado.equals(perfil)) {
                    setTransferErrorResponse(response);
                    solicitarAlertas(pagarCreditoViewController);
                }
            }
        } else if (operationId == Server.OP_SOLICITAR_ALERTAS) {
            setSa((SolicitarAlertasData) response.getResponse());
            analyzeAlertasRecortado();
            return;
        }
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson, ParsingHandler handler, BaseViewController caller) {
        BmovilViewsController parent = null;
        if (pagarCreditoViewController.getParentViewsController() instanceof BmovilViewsController) {
            parent = ((BmovilViewsController) pagarCreditoViewController.getParentViewsController());
        } else {
            parent = SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController();
        }
        parent.getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, caller);
    }

    /*****/
    @Override
    public Constants.TipoOtpAutenticacion tokenAMostrar() {
        Constants.TipoOtpAutenticacion value = null;
        Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

        value = Autenticacion.getInstance().tokenAMostrar(this.operacion, perfil, Tools.getDoubleAmountFromServerString(((PagarCreditoViewController) pagarCreditoViewController).getImporteRaw()));

        return value;
    }

    @Override
    public boolean mostrarNIP() {
        boolean mostrar = false;
        Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

        mostrar = Autenticacion.getInstance().mostrarNIP(this.operacion, perfil, Tools.getDoubleAmountFromServerString(((PagarCreditoViewController) pagarCreditoViewController).getImporteRaw()));

        return mostrar;
    }

    @Override
    public boolean mostrarContrasenia() {
        boolean mostrar = false;
        Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

        mostrar = Autenticacion.getInstance().mostrarContrasena(this.operacion, perfil, Tools.getDoubleAmountFromServerString(((PagarCreditoViewController) pagarCreditoViewController).getImporteRaw()));

        return mostrar;
    }


    @Override
    public boolean mostrarCVV() {
        boolean mostrar = false;
        Constants.Perfil perfil = Session.getInstance(SuiteApp.appContext).getClientProfile();

        mostrar = Autenticacion.getInstance().mostrarCVV(this.operacion, perfil, Tools.getDoubleAmountFromServerString(((PagarCreditoViewController) pagarCreditoViewController).getImporteRaw()));

        return mostrar;
    }


    public void showConfirmacion() {
        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacion(this);
    }

    public void showResultados() {
        this.actualizarMontoCuentas();

        SuiteAppPagoCreditoApi.getInstance().getBmovilApplication().getBmovilViewsController().showResultadosViewController(this, getNombreImagenEncabezado(), getTextoEncabezado());
    }

    @Override
    public long getDelegateIdentifier() {
        // TODO Auto-generated method stub
        return super.getDelegateIdentifier();
    }


    /**
     * Actualiza los montos de las cuentas de origen y destino.
     */
    @SuppressWarnings("unchecked")
    public void actualizarMontoCuentas() {
        try {
            Session.getInstance(SuiteApp.appContext).actualizaMonto(getCuentaOrigen(),
                    resultado.getSaldoCuenta());
        } catch (Exception ex) {
            Log.e("actualizarMontoCuentas", "Error al interpretar los datos de cuentas del servidor.", ex);
        }
    }

    public Account getCuentaOrigen() {
        return ((PagarCreditoViewController) pagarCreditoViewController).getComponenteCtaOrigen().getCuentaOrigenDelegate().getCuentaSeleccionada();
    }

    @Override
    public void realizaOperacion(ConfirmacionViewController confirmacionViewController, String contrasenia,
                                 String nip, String token, String campoTarjeta, String cvv) {
//		this.ownerController = confirmacionViewController;
        Session session = Session.getInstance(SuiteApp.appContext);

        //prepare data
        Hashtable<String, String> paramTable = new Hashtable<String, String>();

        paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
        paramTable.put(ServerConstants.NUMERO_CELULAR, session.getUsername());
        paramTable.put(ServerConstants.NUMERO_CREDITO, creditoActual.getNumeroCredito());

        paramTable.put(ServerConstants.CODIGO_NIP, nip == null ? "" : nip);
        paramTable.put(ServerConstants.CODIGO_CVV2, cvv == null ? "" : cvv);
        paramTable.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
        paramTable.put(ServerConstants.CADENA_AUTENTICACION, Autenticacion.getInstance().getCadenaAutenticacion(this.operacion, session.getClientProfile()));
        paramTable.put(ServerConstants.CVE_ACCESO, contrasenia == null ? "" : contrasenia);

        paramTable.put(ServerConstants.IMPORTE_PAGO, importesPagoCredito.getPagoRequerido());
        paramTable.put(ServerConstants.TIPO_CREDITO, creditoActual.getTipoCredito());
        paramTable.put(ServerConstants.CUENTA_CARGO, getCuentaOrigen().getFullNumber());
        paramTable.put(ServerConstants.PARAMS_TEXTO_EN, "");

        List<String> listaEncriptar = Arrays.asList(ServerConstants.JSON_IUM_ETIQUETA,
                ServerConstants.NUMERO_CELULAR, ServerConstants.NUMERO_CREDITO,
                ServerConstants.CODIGO_NIP, ServerConstants.CODIGO_CVV2,
                ServerConstants.CODIGO_OTP, ServerConstants.CVE_ACCESO);
        Encripcion.setContext(SuiteAppPagoCreditoApi.appContext);
        Encripcion.encriptaCadenaAutenticacion(paramTable, listaEncriptar);
        //JAIG
        doNetworkOperation(ApiConstants.OP_PAGO_CREDITO_CH, paramTable, true, new ResultadoPagoCreditoData(), confirmacionViewController);
    }

    @Override
    public ArrayList<Object> getDatosTablaConfirmacion() {
        ArrayList<Object> tabla = new ArrayList<Object>();
        ArrayList<String> fila;

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_cuenta_retiro));
        fila.add(Tools.hideAccountNumber(pagoCredito.getCuentaRetiro()));
        tabla.add(fila);

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_num_credito));
        fila.add(pagoCredito.getNumCredito());
        tabla.add(fila);

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_beneficiario));
        fila.add(pagoCredito.getNombreBeneficiario());
        tabla.add(fila);

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_tipo_credito));
        fila.add(getTipoCredito(pagoCredito.getTipoCredito()));
        tabla.add(fila);

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_pago_recibos));
        fila.add(Tools.formatAmount(pagoCredito.getImportePago(), false));
        tabla.add(fila);

        if (importesPagoCredito.getMoneda().compareTo("UDI P") == 0 || importesPagoCredito.getMoneda().compareTo("VSM") == 0) {
            fila = new ArrayList<String>();
            String titulo = "";
            if (importesPagoCredito.getMoneda().compareTo("UDI P") == 0) {
                titulo = pagarCreditoViewController.getString(R.string.pago_credito_result_lista_udi);
            } else {
                titulo = pagarCreditoViewController.getString(R.string.pago_credito_result_lista_vsm);
            }
            fila.add(titulo);
            fila.add(pagoCredito.getImportePagoMoneda());
            tabla.add(fila);
        }

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_fecha));
        fila.add(pagoCredito.getFechaPago());
        tabla.add(fila);

        return tabla;
    }

    @Override
    public ArrayList<Object> getDatosTablaResultados() {
        ArrayList<Object> tabla = new ArrayList<Object>();
        ArrayList<String> fila;

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_cuenta_retiro));
        fila.add(Tools.hideAccountNumber(pagoCredito.getCuentaRetiro()));
        tabla.add(fila);

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_num_credito));
        fila.add(pagoCredito.getNumCredito());
        tabla.add(fila);

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_beneficiario));
        fila.add(pagoCredito.getNombreBeneficiario());
        tabla.add(fila);

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_tipo_credito));
        fila.add(getTipoCredito(pagoCredito.getTipoCredito()));
        tabla.add(fila);

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_pago_recibos));
        fila.add(Tools.formatAmount(pagoCredito.getImportePago(), false));
        tabla.add(fila);

        if (importesPagoCredito.getMoneda().compareTo("UDI P") == 0 || importesPagoCredito.getMoneda().compareTo("VSM") == 0) {
            fila = new ArrayList<String>();
            String titulo = "";
            if (importesPagoCredito.getMoneda().compareTo("UDI P") == 0) {
                titulo = pagarCreditoViewController.getString(R.string.pago_credito_result_lista_udi);
            } else {
                titulo = pagarCreditoViewController.getString(R.string.pago_credito_result_lista_vsm);
            }
            fila.add(titulo);
            fila.add(pagoCredito.getImportePagoMoneda());
            tabla.add(fila);
        }

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_fecha));
        fila.add(Tools.compruebaFormatoFecha(resultado.getFechaPago()));
        tabla.add(fila);

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_hora));
        fila.add(resultado.getHoraPago());
        tabla.add(fila);

        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_folio));
        fila.add(resultado.getFolio());
        tabla.add(fila);

        //if (importesPagoCredito.getFechaPago().compareTo("Inmediato") != 0) {
        fila = new ArrayList<String>();
        fila.add(pagarCreditoViewController.getString(R.string.pago_credito_result_lista_recibo_numero));
        fila.add(resultado.getNumRec());
        tabla.add(fila);
        //}

        return tabla;
    }

    private String getTipoCredito(String tipo) {
        String ret = tipo;
        if (tipo.equalsIgnoreCase("C")) {
            ret = "Consumo";
        } else if (tipo.equalsIgnoreCase("H")) {
            ret = "Hipotecario";
        } else if (tipo.equalsIgnoreCase("P")) {
            ret = "Préstamo Personal";
        } else if (tipo.equalsIgnoreCase("N")) {
            ret = "Nómina";
        }

        return ret;
    }

    @Override
    public int getOpcionesMenuResultados() {
        return 0;
    }

    @Override
    public int getNombreImagenEncabezado() {
        return R.drawable.api_pago_credito_icono_pagar;
    }

    @Override
    public int getTextoEncabezado() {
        return R.string.pago_credito_titulo;
    }

    @Override
    public String getTextoTituloResultado() {
        return pagarCreditoViewController.getString(R.string.transferir_detalle_operacion_exitosaTitle);
    }

    @Override
    public int getColorTituloResultado() {
        return R.color.verde_limon;
    }

    @Override
    public String getTextoAyudaResultados() {
        return "";
    }

    @Override
    public void performAction(Object obj) {

        if (Server.ALLOW_LOG) Log.d("TransferirMisCuentasDelegate", "Regrese de lista Seleccion");
        if (Server.ALLOW_LOG) Log.d("PagoTDCDelegate", "Seleccion false");
        ((PagarCreditoViewController) pagarCreditoViewController).muestraListaCuentas();
        ((PagarCreditoViewController) pagarCreditoViewController).getComponenteCtaOrigen().setSeleccionado(false);
    }
}
