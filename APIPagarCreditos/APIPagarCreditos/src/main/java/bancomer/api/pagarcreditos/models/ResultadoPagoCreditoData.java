package bancomer.api.pagarcreditos.models;

import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

public class ResultadoPagoCreditoData implements ParsingHandler {

    private static final long serialVersionUID = 1L;

    private String folio;
    private String cuentaCargo;
    private String saldoCuenta;
    private String fechaPago;
    private String horaPago;
    private String numRec;

    public ResultadoPagoCreditoData() {
        this.folio = "";
        this.cuentaCargo = "";
        this.saldoCuenta = "";
        this.fechaPago = "";
        this.horaPago = "";
        this.numRec = "";
    }

    public ResultadoPagoCreditoData(String folio, String cuentaCargo, String saldoCuenta, String fechaPago, String horaPago, String numRec) {
        this.folio = folio;
        this.cuentaCargo = cuentaCargo;
        this.saldoCuenta = saldoCuenta;
        this.fechaPago = fechaPago;
        this.horaPago = horaPago;
        this.numRec = numRec;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getCuentaCargo() {
        return cuentaCargo;
    }

    public void setCuentaCargo(String cuentaCargo) {
        this.cuentaCargo = cuentaCargo;
    }

    public String getSaldoCuenta() {
        return saldoCuenta;
    }

    public void setSaldoCuenta(String saldoCuenta) {
        this.saldoCuenta = saldoCuenta;
    }

    public String getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(String fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getHoraPago() {
        return horaPago;
    }

    public void setHoraPago(String horaPago) {
        this.horaPago = horaPago;
    }

    public String getNumRec() {
        return numRec;
    }

    public void setNumRec(String numRec) {
        this.numRec = numRec;
    }

    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub
        try {
            this.folio = parser.parseNextValue("folio");
            this.cuentaCargo = parser.parseNextValue("cuentaCargo");
            this.saldoCuenta = parser.parseNextValue("saldoCuenta");
            this.fechaPago = parser.parseNextValue("fechaPago");
            this.horaPago = parser.parseNextValue("horaPago");
            this.numRec = parser.parseNextValue("numRec");
        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
        }
    }
}
