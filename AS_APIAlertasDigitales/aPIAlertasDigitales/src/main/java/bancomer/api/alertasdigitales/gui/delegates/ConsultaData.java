package bancomer.api.alertasdigitales.gui.delegates;

import java.util.Hashtable;

import android.app.Activity;
import bancomer.api.alertasdigitales.implementations.BaseDelegateImpl;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import bancomer.api.alertasdigitales.io.ParsingHandler;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.common.commons.ServerConstants;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.alertasdigitales.R;


public class ConsultaData  extends BaseDelegateImpl{

	public final static long CONSULTA_DATA_DELEGATE_ID = 0x4fd454ce67b39f7L;
	
	// Variable para conocer si la petición ha acabado. Variable de espera para el callback
	private Boolean finishedCall;
	
	// Respuesta de la peticion. Se lee al ejecutar el callback
	private ServerResponse response;
	
	public Boolean getFinishedCall() {
		return finishedCall;
	}

	public void setFinishedCall(Boolean finishedCall) {
		this.finishedCall = finishedCall;
	}

	public ServerResponse getResponse() {
		return response;
	}

	public void setResponse(ServerResponse response) {
		this.response = response;
	}

	public void consultaOtrosCreditos(Activity activity) {
		// Al iniciar toda peticion se debe poner a false
		finishedCall = false;
		
		InitAlertasDigitales.getInstance().getBaseViewController().muestraIndicadorActividad(activity,
				activity.getString(R.string.alert_operation),
				activity.getString(R.string.alert_connecting));
		
		InitAlertasDigitales.getInstance().getBaseViewController().setDelegate(this);
		InitAlertasDigitales.getInstance().getBaseViewController().setActivity(activity);
		
		//prepare data
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		int operacion = 0;
		operacion = Server.OP_CONSULTA_OTROS_CREDITOS;
		paramTable.put(ServerConstants.OPERACION, "consultarCreditos");
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, InitAlertasDigitales.getInstance().getConsultaSend().getIUM());
		paramTable.put(ServerConstants.NUMERO_CELULAR, InitAlertasDigitales.getInstance().getConsultaSend().getUsername());
		paramTable.put(ServerConstants.CODIGO_NIP, InitAlertasDigitales.getInstance().getConsultaSend().getCodigoNip());
		paramTable.put(ServerConstants.CODIGO_CVV2, InitAlertasDigitales.getInstance().getConsultaSend().getCodigoCvv2());
		paramTable.put(ServerConstants.CODIGO_OTP, InitAlertasDigitales.getInstance().getConsultaSend().getCodigoOtp());
		paramTable.put(ServerConstants.CADENA_AUTENTICACION, InitAlertasDigitales.getInstance().getConsultaSend().getCadenaAutenticacion());
		paramTable.put(ServerConstants.CVE_ACCESO, InitAlertasDigitales.getInstance().getConsultaSend().getCveAcceso());
		paramTable.put(ServerConstants.TARJETA_5DIG, InitAlertasDigitales.getInstance().getConsultaSend().getTarjeta5Dig());
        
        doNetworkOperation(operacion, paramTable, InitAlertasDigitales.getInstance().getBaseViewController());
	}
	
	public void doNetworkOperation(int operationId, Hashtable<String, ?> params,final boolean isJson,
								   final ParsingHandler handler, BaseViewController caller) {
		InitAlertasDigitales.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params,isJson,handler, caller,true);
	}
	
	public void analyzeResponse(int operationId, ServerResponse response) {
		// Devolvemos la respuesta dada por el server y actualizamos el flag, indicando que la peticion ha finalizado
		this.response = response;
		finishedCall = true;
	}

}
