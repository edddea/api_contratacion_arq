package bancomer.api.alertasdigitales.gui.delegates;

import android.util.Log;

import java.util.ArrayList;

import bancomer.api.alertasdigitales.R;
import bancomer.api.alertasdigitales.gui.controllers.ConfirmacionViewController;
import bancomer.api.alertasdigitales.implementations.BaseDelegateImpl;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.generaOTPS.GeneraOTPS;
import bancomer.api.common.io.ServerResponse;


public class ConfirmacionDelegate extends BaseDelegateImpl
{
	public final static long CONFIRMACION_DELEGATE_DELEGATE_ID = 0x1ef4f4c61ca109bfL;
	
	private ArrayList<String> datosLista;
	private boolean debePedirContrasena;
	private boolean debePedirNip;
	private Constants.TipoOtpAutenticacion tokenAMostrar;
	private boolean debePedirCVV;
	
	
	private Constants.TipoInstrumento tipoInstrumentoSeguridad;
	
	private ConfirmacionViewController confirmacionViewController;
	
	private BaseDelegateImpl delegateCaller;
	
	private boolean debePedirTarjeta;
	//AMZ
		public boolean res = false;

	public BaseDelegateImpl getDelegateCaller() {
			return delegateCaller;
		}

		public void setDelegateCaller(BaseDelegateImpl delegateCaller) {
			this.delegateCaller = delegateCaller;
		}

	public ConfirmacionDelegate(BaseDelegateImpl delegateBaseOperacion) {
		
		this.delegateCaller = delegateBaseOperacion;
		
		debePedirContrasena = delegateCaller.mostrarContrasenia();
		debePedirNip = delegateCaller.mostrarNIP();
		debePedirCVV = delegateCaller.mostrarCVV();
		tokenAMostrar = delegateCaller.tokenAMostrar();
		debePedirTarjeta = delegateCaller.mostrarCampoTarjeta();
		
		String instrumento = InitAlertasDigitales.getInstance().getConsultaSend().getSecurityInstrument();
		
		if (instrumento.equals(Constants.IS_TYPE_DP270)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
		} else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
		} else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)) {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
		} else {
			tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
		}
	}

	public void setConfirmacionViewController(ConfirmacionViewController confirmacionViewController) {
		this.confirmacionViewController = confirmacionViewController;
	}

	public void consultaDatosLista() {
		confirmacionViewController.setListaDatos(delegateCaller.getDatosTablaConfirmacion());
	}
//	
//	public DelegateBaseOperacion consultaOperationsDelegate() {
//		return operationDelegate;
//	}
	
	public boolean consultaDebePedirContrasena() {
		return debePedirContrasena;
	}
	
	public boolean consultaDebePedirNIP() {
		return debePedirNip;
	}
	
	public boolean consultaDebePedirCVV() {
		return debePedirCVV;
	}
	
	public Constants.TipoInstrumento consultaTipoInstrumentoSeguridad() {
		return tipoInstrumentoSeguridad;
	}
	
	public Constants.TipoOtpAutenticacion consultaInstrumentoSeguridad() {
		return tokenAMostrar;
	}
	
	public void enviaPeticionOperacion() {
		String contrasena = null;
		String nip = null;
		String asm = null;
		String cvv = null;
		res = false;
		if (debePedirContrasena) {
			contrasena = confirmacionViewController.pideContrasena();
			if (contrasena.equals("")) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_componenteContrasena);
				mensaje += ".";
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(confirmacionViewController,mensaje);
				return;
			} else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.PASSWORD_LENGTH;
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_componenteContrasena);
				mensaje += ".";
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(confirmacionViewController,mensaje);
				return;
			}
		}
		
		String tarjeta = null;
		if(debePedirTarjeta){
			tarjeta = confirmacionViewController.pideTarjeta();
			String mensaje = "";
			if(tarjeta.equals("")){
				mensaje = "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(confirmacionViewController,mensaje);
				return;
			}else if(tarjeta.length() != 5){
				mensaje =  "Es necesario ingresar los últimos 5 dígitos de tu tarjeta";
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(confirmacionViewController,mensaje);
				return;
			}			
		}
		
		if (debePedirNip) {
			nip = confirmacionViewController.pideNIP();
			if (nip.equals("")) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_componenteNip);
				mensaje += ".";
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(confirmacionViewController,mensaje);
				return;
			} else if (nip.length() != Constants.NIP_LENGTH) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.NIP_LENGTH;
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_componenteNip);
				mensaje += ".";
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(confirmacionViewController,mensaje);
				return;
			}
		}
		if (tokenAMostrar != Constants.TipoOtpAutenticacion.ninguno) {
			asm = confirmacionViewController.pideASM();
			if (asm.equals("")) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (InitAlertasDigitales.getInstance().getConsultaSend().getSoftTokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
				mensaje += ".";
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(confirmacionViewController,mensaje);
				return;
			} else if (asm.length() != Constants.ASM_LENGTH) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.ASM_LENGTH;
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (InitAlertasDigitales.getInstance().getConsultaSend().getSoftTokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}	
				mensaje += ".";
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(confirmacionViewController,mensaje);
				return;
			}
		}
		if (debePedirCVV) {
			cvv = confirmacionViewController.pideCVV();
			if (cvv.equals("")) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorVacio);
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_componenteCvv);
				mensaje += ".";
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(confirmacionViewController,mensaje);
				return;
			} else if (cvv.length() != Constants.CVV_LENGTH) {
				String mensaje = confirmacionViewController.getString(R.string.confirmation_valorIncompleto1);
				mensaje += " ";
				mensaje += Constants.CVV_LENGTH;
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_valorIncompleto2);
				mensaje += " ";
				mensaje += confirmacionViewController.getString(R.string.confirmation_componenteCvv);
				mensaje += ".";
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(confirmacionViewController,mensaje);
				return;
			}
		}
		
		String newToken = null;
		if(tokenAMostrar != TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == TipoInstrumento.SoftToken && InitAlertasDigitales.getInstance().getConsultaSend().getSoftTokenStatus())
			//TODO
			newToken = loadOtpFromSofttoken(tokenAMostrar);
			if(Server.ALLOW_LOG) Log.i("adad","fsfsfs");
		if(null != newToken)
			asm = newToken;
		
		delegateCaller.realizaOperacionConfirmacion(confirmacionViewController, contrasena, nip, asm, tarjeta);
		res = true;

	}
	

	public String getEtiquetaCampoNip() {
		return confirmacionViewController.getString(R.string.confirmation_nip);
	}
	
	public String getTextoAyudaNIP() {
		return confirmacionViewController.getString(R.string.confirmation_ayudaNip);
	}
	
	public String getEtiquetaCampoContrasenia() {
		return confirmacionViewController.getString(R.string.confirmation_contrasena);
	}
	
	public String getEtiquetaCampoOCRA() {
		return confirmacionViewController.getString(R.string.confirmation_ocra);
	}
	
	public String getEtiquetaCampoDP270() {
		return confirmacionViewController.getString(R.string.confirmation_dp270);
	}
	
	public String getEtiquetaCampoSoftokenActivado() {
		return confirmacionViewController.getString(R.string.confirmation_softtokenActivado);
	}
	
	public String getEtiquetaCampoSoftokenDesactivado() {
		return confirmacionViewController.getString(R.string.confirmation_softtokenDesactivado);
	}
	
	public String getEtiquetaCampoCVV() {
		return confirmacionViewController.getString(R.string.confirmation_CVV);
	}
	
	public String getTextoAyudaInstrumentoSeguridad(Constants.TipoInstrumento tipoInstrumento) {
		if (tokenAMostrar == Constants.TipoOtpAutenticacion.ninguno) {
			return "";
		} else if (tokenAMostrar == Constants.TipoOtpAutenticacion.registro) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (InitAlertasDigitales.getInstance().getConsultaSend().getSoftTokenStatus()) {
						return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroSofttokenActivado);
					} else {
						return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroSofttokenDesactivado);
					}
				case OCRA:
					return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroOCRA);
				case DP270:
					return confirmacionViewController.getString(R.string.confirmation_ayudaRegistroDP270);
				case sinInstrumento:
				default:
					return "";
			}
		} else if (tokenAMostrar == Constants.TipoOtpAutenticacion.codigo) {
			switch (tipoInstrumento) {
				case SoftToken:
					if (InitAlertasDigitales.getInstance().getConsultaSend().getSoftTokenStatus()) {
						return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoSofttokenActivado);
					} else {
						return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoSofttokenDesactivado);
					}					
				case OCRA:
					return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoOCRA);
				case DP270:
					return confirmacionViewController.getString(R.string.confirmation_ayudaCodigoDP270);
				case sinInstrumento:
				default:
					return "";
			}
		}
		return "";
	}
	
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {
		
		Constants.Perfil perfil = InitAlertasDigitales.getInstance().getConsultaSend().getPerfil();
	
		if(response.getStatus() == ServerResponse.OPERATION_ERROR && !Constants.Perfil.recortado.equals(perfil)){
			confirmacionViewController.limpiarCampos();
			InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(confirmacionViewController,response.getMessageText());
		}
		delegateCaller.analyzeResponse(operationId, response);
	}
	

	public String loadOtpFromSofttoken(TipoOtpAutenticacion tipoOTP) {
		String otp = null;
		InitAlertasDigitales initAlertas = InitAlertasDigitales.getInstance();
		GeneraOTPS generaOtps = initAlertas.getConsultaSend().getGeneraOtps();
		// Verifica y carga el token seg�n el tipo que corresponda.
		if(TipoOtpAutenticacion.codigo == tipoOTP)
			otp = generaOtps.generarOtpTiempoApi();
		else if(TipoOtpAutenticacion.registro == tipoOTP)
			otp = (null == delegateCaller) ? null : generaOtps.generarOtpChallengeApi(delegateCaller.getNumeroCuentaParaRegistroOperacion());
		return otp;
		
	}

	
}
