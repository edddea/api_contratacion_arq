package bancomer.api.alertasdigitales.models;

import android.util.Log;

import java.io.IOException;

import bancomer.api.alertasdigitales.io.ParsingHandler;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;

public class EstadoData implements ParsingHandler  {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1880051584968511250L;
	
	private String estado;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		try{
			this.estado = parser.parseNextValue("estado");
		}catch(Exception e){
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
		}
		
	}
}
