package bancomer.api.alertasdigitales.gui.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

import bancomer.api.alertasdigitales.R;
import bancomer.api.alertasdigitales.gui.delegates.MensajeAbonoCargoDelegate;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.alertasdigitales.io.ServerResponseImpl;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.ListaSeleccionViewController;
import bancomer.api.common.timer.TimerController;

public class MensajesCuentaViewController extends Activity implements View.OnClickListener {

    private MensajeAbonoCargoDelegate mensajeAbonoCargoDelegate;

    // Layout de la lista de creditos
    private LinearLayout vista;


    // Instancia del adapter para la lista seleccionable
    private ListaSeleccionViewController listaSeleccion;

    // Instancia del adapter para la lista seleccionable
    private ListaSeleccionViewController listaSeleccion_cuentasmensaje;

    //[CGI-Configuracion-Obligatorio] Instancia del baseviewController encargado de la funcionalidad comun de las vistas
    private BaseViewController baseViewController;

    //[CGI-Configuracion-Obligatorio] Instancia del parentManager encargado de la pila de delegates y las redirecciones
    private BaseViewsController parentManager;

    public BaseViewController getBaseViewController() {
        return baseViewController;
    }
    public void setBaseViewController(BaseViewController baseViewController) {
        this.baseViewController = baseViewController;
    }
    public BaseViewsController getParentManager() {
        return parentManager;
    }
    public void setParentManager(BaseViewsController parentManager) {
        this.parentManager = parentManager;
    }

    public ListaSeleccionViewController getListaSeleccion() {
        return listaSeleccion;
    }

    public ListaSeleccionViewController getListaSeleccion_cuentasmensaje() {
        return listaSeleccion_cuentasmensaje;
    }

    public void irAtras(){
        super.onBackPressed();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //[CGI-Configuracion-Obligatorio] Llamada a Activity
        super.onCreate(savedInstanceState);

        //[CGI-Configuracion-Obligatorio] Inicializacion del singleton de parametros globales
        InitAlertasDigitales init = InitAlertasDigitales.getInstance();

        //[CGI-Configuracion-Obligatorio] Tomamos la instancia del baseviewController encargado de la funcionalidad comun de las vistas
        baseViewController = init.getBaseViewController();
        //[CGI-Configuracion-Obligatorio] Tomamos la instancia del parentManager encargado de la pila de delegates y las redirecciones
        parentManager = init.getParentManager();


        //[CGI-Configuracion-Obligatorio] Llamadas para estandarizar las ventanas
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER|BaseViewController.SHOW_TITLE, R.layout.api_alertas_digitales_mensajes_cuenta);
        baseViewController.setTitle(this, R.string.bmovil_menu_alertasdigitales_titulo, R.drawable.api_alertas_digitales_an_ic_alertas);

        //[CGI-Configuracion-Obligatorio] Tomamos el delegate en caso de estar ya instanciado
        mensajeAbonoCargoDelegate = (MensajeAbonoCargoDelegate) parentManager.getBaseDelegateForKey(MensajeAbonoCargoDelegate.MENSAJES_ABONO_CARGO_DELEGATE_ID);
        if (mensajeAbonoCargoDelegate == null) {
            mensajeAbonoCargoDelegate = new MensajeAbonoCargoDelegate();
            parentManager.addDelegateToHashMap(MensajeAbonoCargoDelegate.MENSAJES_ABONO_CARGO_DELEGATE_ID,
                    mensajeAbonoCargoDelegate);
        }

        //[CGI-Configuracion-Obligatorio] Establecemos el viewController en el delegate
        mensajeAbonoCargoDelegate.setMensajesCuentaViewController(this);

        vista = (LinearLayout)findViewById(R.id.lista_consulta__otros_creditos_layout);
        scaleToGui();
        llenaListaSeleccion();
        llenaListaSeleccionMensajes();
    }
    @Override
    protected void onResume() {
        super.onResume();
        //[CGI-Configuracion-Obligatorio] Parametros necesarios para hacer animaciones de vuelta atras
        parentManager.setCurrentActivityApp(this);
        parentManager.setActivityChanging(false);

        //[CGI-Configuracion-Obligatorio] Establecemos el activity actual en el baseViewController para establecer sobre que activity ha de actuar ya que no hereda de la vista
        baseViewController.setActivity(this);

        //[CGI-Configuracion-Obligatorio] Establecemos el delegate actual en el baseViewController para conocer a que delegate debe volver
        baseViewController.setDelegate(mensajeAbonoCargoDelegate);
    }

    public void scaleToGui(){
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
    }

    @Override
    protected void onPause() {
		/*if(TimerManager.isMainAppInControl()){
			parentManager.setCurrentActivityApp(this);
			parentManager.setActivityChanging(false);
			baseViewController.goBack(this, parentManager);
			Log.d("Bebug>>", ">>>>>>>>>>>> onPause");
		}*/

        TimerController timerContr = TimerController.getInstance();
        if(timerContr.isCerrandoSesion()){
            parentManager.setCurrentActivityApp(this);
            parentManager.setActivityChanging(false);
            baseViewController.goBack(this, parentManager);
            if(Server.ALLOW_LOG) Log.d("Bebug>>", ">>>>>>>>>>>> onPause");
        }
        super.onPause();
        parentManager.consumeAccionesDePausa();
    }


    @Override
    public void onBackPressed(){
        //TimerManager.setMainAppInControl(true);
        baseViewController.goBack(this, parentManager);
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @SuppressWarnings("deprecation")
    public void llenaListaSeleccion(){

        LinearLayout.LayoutParams params;
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        ArrayList<Object> lista = new ArrayList<Object>();
        ArrayList<Object> encabezado = new ArrayList<Object>();

        /**
         * Recorrer datos y añadir a registros
         */
        ArrayList<String> registro;
        registro = new ArrayList<String>();
        registro.add(null);
        registro.add(mensajeAbonoCargoDelegate.getCuentaSeleccionada().getNombreTipoCta());
        registro.add(Tools.hideAccountNumber(mensajeAbonoCargoDelegate.getCuentaSeleccionada().getNumCuenta()));

        encabezado.add(registro);


        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//		params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_bottom_margin);;

        if (listaSeleccion == null) {
            listaSeleccion = new ListaSeleccionViewController(this, params, baseViewController.getParentViewsController());
            listaSeleccion.setDelegate(mensajeAbonoCargoDelegate);
            listaSeleccion.setNumeroColumnas(2);
           // listaSeleccion.setEncabezado(encabezado);
            listaSeleccion.setTitle("");
            listaSeleccion.setLista(encabezado);
            listaSeleccion.setOpcionSeleccionada(-1);
            listaSeleccion.setSeleccionable(false);
            listaSeleccion.setAlturaFija(true);
            listaSeleccion.setNumeroFilas(encabezado.size());
            listaSeleccion.setExisteFiltro(false);
            listaSeleccion.cargarTabla();
            vista.addView(listaSeleccion);

        }else {
            listaSeleccion.setLista(encabezado);
            listaSeleccion.setAlturaFija(true);
            listaSeleccion.setNumeroFilas(encabezado.size());
            listaSeleccion.setOpcionSeleccionada(-1);
            listaSeleccion.cargarTabla();
        }
    }

    @SuppressWarnings("deprecation")
    public void llenaListaSeleccionMensajes() {

        LinearLayout.LayoutParams params;
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        ArrayList<Object> lista = new ArrayList<Object>();
        ArrayList<Object> encabezado = new ArrayList<Object>();
        encabezado.add(null);
        encabezado.add(getString(R.string.bmovil_mensajesabono_alertasdigitales_cabecera1));
        encabezado.add(getString(R.string.bmovil_mensajesabono_alertasdigitales_cabecera2));
        encabezado.add(getString(R.string.bmovil_mensajesabono_alertasdigitales_cabecera3));

        /**
         * Recorrer datos y añadir a registros
         */
        lista = mensajeAbonoCargoDelegate.getDatosTabla();

        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//		params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_bottom_margin);;
        if (listaSeleccion_cuentasmensaje == null) {
            listaSeleccion_cuentasmensaje = new ListaSeleccionViewController(this, params, baseViewController.getParentViewsController());
            listaSeleccion_cuentasmensaje.setDelegate(mensajeAbonoCargoDelegate);
            listaSeleccion_cuentasmensaje.setNumeroColumnas(3);
            listaSeleccion_cuentasmensaje.setEncabezado(encabezado);
            listaSeleccion_cuentasmensaje.setTitle("");
            listaSeleccion_cuentasmensaje.setLista(lista);
            listaSeleccion_cuentasmensaje.setOpcionSeleccionada(-1);
            listaSeleccion_cuentasmensaje.setSeleccionable(false);
            listaSeleccion_cuentasmensaje.setAlturaFija(true);
            listaSeleccion_cuentasmensaje.setNumeroFilas(lista.size());
            listaSeleccion_cuentasmensaje.setExisteFiltro(false);
            listaSeleccion_cuentasmensaje.cargarTabla();
            listaSeleccion_cuentasmensaje.setSingleLine(true);
            vista.addView(listaSeleccion_cuentasmensaje);

        } else {
            listaSeleccion_cuentasmensaje.setLista(lista);
            listaSeleccion_cuentasmensaje.setAlturaFija(true);
            listaSeleccion_cuentasmensaje.setNumeroFilas(lista.size());
            listaSeleccion_cuentasmensaje.setOpcionSeleccionada(-1);
            listaSeleccion_cuentasmensaje.cargarTabla();
        }
    }


    public void processNetworkResponse(int operationId, ServerResponseImpl response) {
        mensajeAbonoCargoDelegate.analyzeResponse(operationId, response);
    }


    @Override
    public void onClick(View v) {
        if(v == listaSeleccion_cuentasmensaje) {
            mensajeAbonoCargoDelegate.detalleMenEnv();
        }
    }

    @Override
    public void onUserInteraction(){
        baseViewController.onUserInteraction();
    }
}
