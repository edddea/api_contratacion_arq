package bancomer.api.alertasdigitales.gui.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;

import bancomer.api.alertasdigitales.R;
import bancomer.api.alertasdigitales.gui.delegates.AlertasDigitalesDelegate;
import bancomer.api.alertasdigitales.gui.delegates.AlertasDigitalesSmsDelegate;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.alertasdigitales.io.ServerResponseImpl;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.ListaSeleccionViewController;
import bancomer.api.common.timer.TimerController;

/**
 * Created by leleyvah on 03/05/2016.
 */
public class AlertasDigitalesSmsViewController extends Activity {
    // Layout de la lista de creditos
    private LinearLayout vista;
    // Instancia del delegate
    private AlertasDigitalesSmsDelegate alertasDigitalesDelegate;
    // Instancia del adapter para la lista seleccionable
    private ListaSeleccionViewController listaSeleccion;
    //[CGI-Configuracion-Obligatorio] Instancia del baseviewController encargado de la funcionalidad comun de las vistas
    private BaseViewController baseViewController;
    //[CGI-Configuracion-Obligatorio] Instancia del parentManager encargado de la pila de delegates y las redirecciones
    private BaseViewsController parentManager;
    public BaseViewController getBaseViewController() {
        return baseViewController;
    }
    public void setBaseViewController(BaseViewController baseViewController) {
        this.baseViewController = baseViewController;
    }
    public BaseViewsController getParentManager() {
        return parentManager;
    }
    public void setParentManager(BaseViewsController parentManager) {
        this.parentManager = parentManager;
    }

    public ListaSeleccionViewController getListaSeleccion() {
        return listaSeleccion;
    }

    public void irAtras(){
        super.onBackPressed();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        //[CGI-Configuracion-Obligatorio] Llamada a Activity
        super.onCreate(savedInstanceState);
        //[CGI-Configuracion-Obligatorio] Inicializacion del singleton de parametros globales
        InitAlertasDigitales init = InitAlertasDigitales.getInstance();
        //[CGI-Configuracion-Obligatorio] Tomamos la instancia del baseviewController encargado de la funcionalidad comun de las vistas
        baseViewController = init.getBaseViewController();
        //[CGI-Configuracion-Obligatorio] Tomamos la instancia del parentManager encargado de la pila de delegates y las redirecciones
        parentManager = init.getParentManager();
        //[CGI-Configuracion-Obligatorio] Llamadas para estandarizar las ventanas
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER|BaseViewController.SHOW_TITLE, R.layout.api_alertas_digitales_sms_layout_bmovil_alertas_digitales);
        baseViewController.setTitle(this, R.string.bmovil_menu_alertasdigitales_titulo, R.drawable.api_alertas_digitales_an_ic_alertas);
        //[CGI-Configuracion-Obligatorio] Tomamos el delegate en caso de estar ya instanciado
        alertasDigitalesDelegate = (AlertasDigitalesSmsDelegate) parentManager.getBaseDelegateForKey(AlertasDigitalesDelegate.ALERTAS_DIGITALES_DELEGATE_ID);
        if (alertasDigitalesDelegate == null) {
            alertasDigitalesDelegate = new AlertasDigitalesSmsDelegate();
            parentManager.addDelegateToHashMap(AlertasDigitalesDelegate.ALERTAS_DIGITALES_DELEGATE_ID, alertasDigitalesDelegate);
        }
        //[CGI-Configuracion-Obligatorio] Establecemos el viewController en el delegate
        alertasDigitalesDelegate.setAlertasDigitalesSmsViewController(this);
        vista = (LinearLayout)findViewById(R.id.lista_consulta__otros_creditos_layout);
        // Peticion de consulta cuentas alertas
        alertasDigitalesDelegate.consultaCuentasAlertasDigitales();
        scaleToGui();
    }


    @Override
    protected void onResume() {
        super.onResume();
        //[CGI-Configuracion-Obligatorio] Parametros necesarios para hacer animaciones de vuelta atras
        parentManager.setCurrentActivityApp(this);
        parentManager.setActivityChanging(false);
        //[CGI-Configuracion-Obligatorio] Establecemos el activity actual en el baseViewController para establecer sobre que activity ha de actuar ya que no hereda de la vista
        baseViewController.setActivity(this);
        //[CGI-Configuracion-Obligatorio] Establecemos el delegate actual en el baseViewController para conocer a que delegate debe volver
        baseViewController.setDelegate(alertasDigitalesDelegate);
    }

    public void scaleToGui(){
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(findViewById(R.id.layoutRoot));
        guiTools.scale(findViewById(R.id.lista_consulta__otros_creditos_layout));
        guiTools.scale(findViewById(R.id.ayudaCuentaSinAlertas));
    }


    @SuppressWarnings("deprecation")
    public void llenaListaSeleccion(){
        LinearLayout.LayoutParams params;
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ArrayList<Object> lista = new ArrayList<Object>();
        ArrayList<Object> encabezado = new ArrayList<Object>();
        encabezado.add(null);
        encabezado.add(getString(R.string.bmovil_menu_alertasdigitales_cabecera1));
        encabezado.add(getString(R.string.bmovil_menu_alertasdigitales_cabecera2));
        lista = alertasDigitalesDelegate.getDatosTabla();
        params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        if (listaSeleccion == null) {
            listaSeleccion = new ListaSeleccionViewController(this, params, baseViewController.getParentViewsController());
            listaSeleccion.setDelegate(alertasDigitalesDelegate);
            listaSeleccion.setNumeroColumnas(2);
            listaSeleccion.setEncabezado(encabezado);
            listaSeleccion.setTitle("");
            listaSeleccion.setLista(lista);
            listaSeleccion.setOpcionSeleccionada(-1);
            listaSeleccion.setSeleccionable(false);
            listaSeleccion.setAlturaFija(true);
            listaSeleccion.setNumeroFilas(lista.size());
            listaSeleccion.setExisteFiltro(false);
            listaSeleccion.cargarTabla();
            vista.addView(listaSeleccion);

        }else {
            listaSeleccion.setLista(lista);
            listaSeleccion.setAlturaFija(true);
            listaSeleccion.setNumeroFilas(lista.size());
            listaSeleccion.setOpcionSeleccionada(-1);
            listaSeleccion.cargarTabla();
        }
    }

    public void mostrarAyudaCuentaSinAlertas() {
        View ayudaCuentaSinAlertas = findViewById(R.id.ayudaCuentaSinAlertas);
        ayudaCuentaSinAlertas.setVisibility(View.VISIBLE);
    }

    public void ocultarAyudaCuentaSinAlertas() {
        View ayudaCuentaSinAlertas = findViewById(R.id.ayudaCuentaSinAlertas);
        ayudaCuentaSinAlertas.setVisibility(View.GONE);
    }

    @Override
    public void onUserInteraction(){
        baseViewController.onUserInteraction();
    }


    public void processNetworkResponse(int operationId, ServerResponseImpl response) {
        alertasDigitalesDelegate.analyzeResponse(operationId, response);
    }


    @Override
    protected void onPause() {

        TimerController timerContr = TimerController.getInstance();
        if(timerContr.isCerrandoSesion()){
            parentManager.setCurrentActivityApp(this);
            parentManager.setActivityChanging(false);
            baseViewController.goBack(this, parentManager);
            if(Server.ALLOW_LOG) Log.d("Bebug>>", ">>>>>>>>>>>> onPause");
        }
        super.onPause();
        parentManager.consumeAccionesDePausa();
    }

    @Override
    public void onBackPressed(){
        baseViewController.goBack(this, parentManager);
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

