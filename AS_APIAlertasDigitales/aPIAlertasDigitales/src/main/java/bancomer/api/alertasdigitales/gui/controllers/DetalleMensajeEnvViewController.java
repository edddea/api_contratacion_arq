package bancomer.api.alertasdigitales.gui.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.StringTokenizer;

import bancomer.api.alertasdigitales.R;
import bancomer.api.alertasdigitales.gui.delegates.MensajeAbonoCargoDelegate;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.ServerConstants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.ListaDatosViewController;
import bancomer.api.common.timer.TimerController;

public class DetalleMensajeEnvViewController extends Activity {

    private MensajeAbonoCargoDelegate mensajeAbonoCargoDelegate;
    private TextView cuenta,fecha,hora,tipo,importe,alias,divisa,metodoEnvio,estado,mensaje,mensaje2, numeroCelular, company;
    private TextView cuentaLbl, fechaLbl, horaLbl, tipoLbl, importeLbl, aliasLbl, divisaLbl, metodoEnvioLbl, estadoLbl, mensajeLbl, numeroCelularLbl, companyLbl;
    private LinearLayout companyLinear, celularLinear;
    // Layout de la lista de creditos
    private LinearLayout vista;

    private LinearLayout listaDetallesLayout;
    // Instancia del adapter para la lista seleccionable
    private ListaDatosViewController listaDatos;

    //[CGI-Configuracion-Obligatorio] Instancia del baseviewController encargado de la funcionalidad comun de las vistas
    private BaseViewController baseViewController;

    //[CGI-Configuracion-Obligatorio] Instancia del parentManager encargado de la pila de delegates y las redirecciones
    private BaseViewsController parentManager;

    public BaseViewController getBaseViewController() {
        return baseViewController;
    }
    public void setBaseViewController(BaseViewController baseViewController) {
        this.baseViewController = baseViewController;
    }
    public BaseViewsController getParentManager() {
        return parentManager;
    }
    public void setParentManager(BaseViewsController parentManager) {
        this.parentManager = parentManager;
    }

    public ListaDatosViewController getListaDatos() {
        return listaDatos;
    }

    public void irAtras(){
        super.onBackPressed();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        //[CGI-Configuracion-Obligatorio] Llamada a Activity
        super.onCreate(savedInstanceState);

        //[CGI-Configuracion-Obligatorio] Inicializacion del singleton de parametros globales
        InitAlertasDigitales init = InitAlertasDigitales.getInstance();

        //[CGI-Configuracion-Obligatorio] Tomamos la instancia del baseviewController encargado de la funcionalidad comun de las vistas
        baseViewController = init.getBaseViewController();
        //[CGI-Configuracion-Obligatorio] Tomamos la instancia del parentManager encargado de la pila de delegates y las redirecciones
        parentManager = init.getParentManager();


        //[CGI-Configuracion-Obligatorio] Llamadas para estandarizar las ventanas
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER|BaseViewController.SHOW_TITLE, R.layout.api_alertas_digitales_detalle_mensajeenv);
        baseViewController.setTitle(this, R.string.bmovil_menu_alertasdigitales_titulo, R.drawable.api_alertas_digitales_an_ic_alertas);

        //[CGI-Configuracion-Obligatorio] Tomamos el delegate en caso de estar ya instanciado
        mensajeAbonoCargoDelegate = (MensajeAbonoCargoDelegate) parentManager.getBaseDelegateForKey(MensajeAbonoCargoDelegate.MENSAJES_ABONO_CARGO_DELEGATE_ID);
        if (mensajeAbonoCargoDelegate == null) {
            mensajeAbonoCargoDelegate = new MensajeAbonoCargoDelegate();
            parentManager.addDelegateToHashMap(MensajeAbonoCargoDelegate.MENSAJES_ABONO_CARGO_DELEGATE_ID,
                    mensajeAbonoCargoDelegate);
        }

        //[CGI-Configuracion-Obligatorio] Establecemos el viewController en el delegate
        mensajeAbonoCargoDelegate.setDetalleMensajeEnvViewController(this);

        vista = (LinearLayout)findViewById(R.id.lista_consulta__otros_creditos_layout);

        findViews();
        scaleToGui();
        cargarTabla();
    }

    private void findViews() {
        listaDetallesLayout = (LinearLayout)findViewById(R.id.listaDetallesLayout);
        cuenta =(TextView) findViewById(R.id.cuentaValor);
        fecha = (TextView) findViewById(R.id.fechaValor);
        hora = (TextView) findViewById(R.id.horaValor);
        tipo = (TextView) findViewById(R.id.tipoValor);
        importe = (TextView) findViewById(R.id.importeValor);
        alias = (TextView) findViewById(R.id.aliasValor);
        divisa = (TextView) findViewById(R.id.divisaValor);
        metodoEnvio = (TextView) findViewById(R.id.metodoEnvioValor);
        estado = (TextView) findViewById(R.id.estadoValor);
        mensaje = (TextView) findViewById(R.id.mensajeValor1);
        mensaje2 = (TextView) findViewById(R.id.mensajeValor2);
        numeroCelular = (TextView) findViewById(R.id.numeroCelularValor);
        company = (TextView) findViewById(R.id.compañiaValor);
        cuentaLbl =(TextView) findViewById(R.id.cuenta);
        fechaLbl = (TextView) findViewById(R.id.fecha);
        horaLbl = (TextView) findViewById(R.id.hora);
        tipoLbl = (TextView) findViewById(R.id.tipo);
        importeLbl = (TextView) findViewById(R.id.importe);
        aliasLbl = (TextView) findViewById(R.id.alias);
        divisaLbl = (TextView) findViewById(R.id.divisa);
        metodoEnvioLbl = (TextView) findViewById(R.id.metodoEnvio);
        estadoLbl = (TextView) findViewById(R.id.estado);
        mensajeLbl = (TextView) findViewById(R.id.mensaje);
        numeroCelularLbl = (TextView) findViewById(R.id.numeroCelular);
        companyLbl = (TextView) findViewById(R.id.compañia);
        celularLinear = (LinearLayout) findViewById(R.id.linearNumeroCelular);
        companyLinear = (LinearLayout) findViewById(R.id.linearCompañia);


    }

    @Override
    protected void onResume() {
        super.onResume();
        //[CGI-Configuracion-Obligatorio] Parametros necesarios para hacer animaciones de vuelta atras
        parentManager.setCurrentActivityApp(this);
        parentManager.setActivityChanging(false);

        //[CGI-Configuracion-Obligatorio] Establecemos el activity actual en el baseViewController para establecer sobre que activity ha de actuar ya que no hereda de la vista
        baseViewController.setActivity(this);

        //[CGI-Configuracion-Obligatorio] Establecemos el delegate actual en el baseViewController para conocer a que delegate debe volver
        baseViewController.setDelegate(mensajeAbonoCargoDelegate);
    }

    public void scaleToGui(){
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(cuenta, true);
        guiTools.scale(fecha, true);
        guiTools.scale(hora, true);
        guiTools.scale(tipo, true);
        guiTools.scale(importe, true);
        guiTools.scale(alias, true);
        guiTools.scale(divisa, true);
        guiTools.scale(metodoEnvio, true);
        guiTools.scale(estado, true);
        guiTools.scale(mensaje, true);
        guiTools.scale(mensaje2, true);
        guiTools.scale(numeroCelular, true);
        guiTools.scale(company, true);

        guiTools.scale(cuentaLbl, true);
        guiTools.scale(fechaLbl, true);
        guiTools.scale(horaLbl, true);
        guiTools.scale(tipoLbl, true);
        guiTools.scale(importeLbl, true);
        guiTools.scale(aliasLbl, true);
        guiTools.scale(divisaLbl, true);
        guiTools.scale(metodoEnvioLbl, true);
        guiTools.scale(estadoLbl, true);
        guiTools.scale(mensajeLbl, true);
        guiTools.scale(numeroCelularLbl, true);
        guiTools.scale(companyLbl, true);
    }

    @Override
    protected void onPause() {
		/*if(TimerManager.isMainAppInControl()){
			parentManager.setCurrentActivityApp(this);
			parentManager.setActivityChanging(false);
			baseViewController.goBack(this, parentManager);
			Log.d("Bebug>>", ">>>>>>>>>>>> onPause");
		}*/

        TimerController timerContr = TimerController.getInstance();
        if(timerContr.isCerrandoSesion()){
            parentManager.setCurrentActivityApp(this);
            parentManager.setActivityChanging(false);
            baseViewController.goBack(this, parentManager);
            if(Server.ALLOW_LOG) Log.d("Bebug>>", ">>>>>>>>>>>> onPause");
        }
        super.onPause();
        parentManager.consumeAccionesDePausa();
    }


    @Override
    public void onBackPressed(){
        //TimerManager.setMainAppInControl(true);
        baseViewController.goBack(this, parentManager);
    }
    @Override
    protected void onStop() {
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private String formatImporte(String importe,String divisa){
        String sign = "";
        if(importe.startsWith("-")){
            importe = importe.substring(1);
            sign = "-";
        }else if(importe.startsWith("+")){
            importe = importe.substring(1);
        }

        try {
            importe = (Float.valueOf(importe)).toString();
        }catch(NumberFormatException e){
            if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), e.getMessage());
        }

        return sign+divisa+Tools.formatUserAmount(importe);
    }

    public void cargaListaDatos() {
        if(Server.ALLOW_LOG) Log.d(">> CGI", "Pintamos Detalle en pantalla");
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        ArrayList<Object> detalles = new ArrayList<Object>();

        listaDatos = new ListaDatosViewController(this, params,
                baseViewController.getParentViewsController());
        detalles = cargarListaDetalle();

        listaDatos.setNumeroCeldas(2);
        listaDatos.setLista(detalles);
        listaDatos.setNumeroFilas(detalles.size());
        listaDatos.showLista();
        listaDetallesLayout.addView(listaDatos);
    }

    private void cargarTabla(){
        cuenta.setText(Tools.hideAccountNumber(mensajeAbonoCargoDelegate.getCuentaSeleccionada().getNumCuenta()));
        fecha.setText(getFromTimeStamp(ServerConstants.FECHA));
        hora.setText(getFromTimeStamp(ServerConstants.HORA));
        tipo.setText(mensajeAbonoCargoDelegate.getCuentaSel().getdesOp());
        importe.setText(formatImporte(mensajeAbonoCargoDelegate.getCuentaSel().getimOp(),"$"));
        alias.setText(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getAlias());
        divisa.setText(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getDivisa());
        String dispositivo ="";
        if(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getDispositivo().equalsIgnoreCase("101")){
            dispositivo = ServerConstants.SMS;
        }else{
            dispositivo = ServerConstants.PUSH;
        }
        metodoEnvio.setText(dispositivo);
        if(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getDispositivo().equalsIgnoreCase("101")) {
           company.setText(getCompany());
           numeroCelular.setText(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getCorreoPart1());
           celularLinear.setVisibility(View.VISIBLE);
           companyLinear.setVisibility(View.VISIBLE);
        }else{
            celularLinear.setVisibility(View.GONE);
            companyLinear.setVisibility(View.GONE);

        }
        estado.setText(getEstadoEnvio(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getEstadoEnvio()));
        mensaje.setText((mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getMensaje1()));
        mensaje.setSingleLine(false);
        mensaje2.setText((mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getMensaje2()));
        mensaje2.setSingleLine(false);


    }

    private ArrayList<Object> cargarListaDetalle() {
        ArrayList<Object> detalles = new ArrayList<Object>();
        ArrayList<String> fila1 = new ArrayList<String>();
        fila1.add("Cuenta");
        fila1.add(Tools.hideAccountNumber(mensajeAbonoCargoDelegate.getCuentaSeleccionada().getNumCuenta()));
        detalles.add(fila1);

        fila1 = new ArrayList<String>();
        fila1.add("Fecha");
        fila1.add(getFromTimeStamp(ServerConstants.FECHA));
        detalles.add(fila1);

        fila1 = new ArrayList<String>();
        fila1.add("Hora");
        fila1.add(getFromTimeStamp(ServerConstants.HORA));
        detalles.add(fila1);

        fila1 = new ArrayList<String>();
        fila1.add("Tipo");
        fila1.add(mensajeAbonoCargoDelegate.getCuentaSel().getdesOp());
        detalles.add(fila1);

        fila1 = new ArrayList<String>();
        fila1.add("Importe");
        fila1.add(formatImporte(mensajeAbonoCargoDelegate.getCuentaSel().getimOp(),"$"));
        detalles.add(fila1);

        fila1 = new ArrayList<String>();
        fila1.add("Alias");
        fila1.add(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getCliente());
        detalles.add(fila1);

        fila1 = new ArrayList<String>();
        fila1.add("Divisa");
        fila1.add(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getDivisa());
        detalles.add(fila1);

        fila1 = new ArrayList<String>();
        fila1.add("Método envío");
        String dispositivo="";
        if(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getDispositivo().equalsIgnoreCase("101")){
            dispositivo = ServerConstants.SMS;
        }else{
            dispositivo = ServerConstants.PUSH;
        }
        fila1.add(dispositivo);
        detalles.add(fila1);

        if(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getDispositivo().equalsIgnoreCase("101")) {
            fila1 = new ArrayList<String>();
            fila1.add("Compañía");

            fila1.add(getCompany());
            detalles.add(fila1);

            fila1 = new ArrayList<String>();
            fila1.add("Número celular");
            fila1.add(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getCorreoPart1());
            detalles.add(fila1);
        }

        fila1 = new ArrayList<String>();
        fila1.add("Estado");
        fila1.add(getEstadoEnvio(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getEstadoEnvio()));
        detalles.add(fila1);

        fila1 = new ArrayList<String>();
        fila1.add("Mensaje");
        fila1.add(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getMensaje1()+mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getMensaje2());
        detalles.add(fila1);

        return detalles;
    }

    /**
     *
     * @param estado
     * @return empty string if no match
     */
    private String getEstadoEnvio(String estado){
        String ret = "";
        if(estado.equalsIgnoreCase(getString(R.string.api_alertas_digitales_estado_error_shortcode))){
            ret = getString(R.string.api_alertas_digitales_estado_error);
        }else if(estado.equalsIgnoreCase(getString(R.string.api_alertas_digitales_estado_enviado_shortcode))){
            ret = getString(R.string.api_alertas_digitales_estado_enviado);
        }else if(estado.equalsIgnoreCase(getString(R.string.api_alertas_digitales_estado_cancelado_shortcode))){
            ret = getString(R.string.api_alertas_digitales_estado_cancelado);
        }

        return ret;
    }

    public String getCompany(){

        if(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getTelefonica().equalsIgnoreCase("01"))
            return "Telcel";
        if(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getTelefonica().equalsIgnoreCase("02"))
            return "Iusacell";
        if(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getTelefonica().equalsIgnoreCase("03"))
            return "Movistar";
        if(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getTelefonica().equalsIgnoreCase("04"))
            return "Nextel";
        if(mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getTelefonica().equalsIgnoreCase("05"))
            return "Unefon";
        else
            return "Compañia no registrada";

        }

    /**
     *
     * @param Este timeStamp viene como AAAA-MM-DD-HH.MM.SS.NNNNNN
     * @return
     */
    private String getFromTimeStamp(String opcion){
        String ret="";
        String timeStamp = mensajeAbonoCargoDelegate.getDetalleMensajeEnvData().getTimeEst();
        if(timeStamp!=null && !timeStamp.equalsIgnoreCase("")) {
            StringTokenizer str = new StringTokenizer(timeStamp, "-");
            ArrayList<String> tms = new ArrayList<String>();
            while (str.hasMoreTokens()) {
                tms.add(str.nextToken());
            }
            str = new StringTokenizer(tms.get(3), ".");
            while (str.hasMoreTokens()) {
                tms.add(str.nextToken());
            }
            //Estado actual de tms = AAAA|MM|DD|HH.MM.SS.NNNNNN|HH|MM|SS|NNNNNN
            if (opcion.equalsIgnoreCase(ServerConstants.FECHA)) {
                ret = tms.get(2) + "/" + tms.get(1) + "/" + tms.get(0);
            } else {
                ret = tms.get(4) + ":" + tms.get(5) + ":" + tms.get(6);
            }
        }
            return ret;
    }

    @Override
    public void onUserInteraction(){
        baseViewController.onUserInteraction();
    }
}
