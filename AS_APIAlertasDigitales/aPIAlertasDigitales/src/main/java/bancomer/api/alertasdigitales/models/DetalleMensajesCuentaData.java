package bancomer.api.alertasdigitales.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bancomer.api.alertasdigitales.io.ParsingHandler;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;

/**
 * Created by juan.belmonterodrigu on 04/08/2015.
 */
public class DetalleMensajesCuentaData implements ParsingHandler {

    /**
     *
     */
    private static final long serialVersionUID = 123L;

    private String estado;

    private List<CuentaMensaje> listaCuentas = new ArrayList<CuentaMensaje>();


    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub

        try {
            this.estado = parser.parseNextValue("estado");

            JSONArray datos = parser.parseNextValueWithArray("detCuentas", false);

            if(datos != null){
                JSONObject item;
                for (int i = 0; i < datos.length(); i++) {

                    item = datos.getJSONObject(i);

                    CuentaMensaje cuenta = new CuentaMensaje();

                    cuenta.setfechaOp(item.getString("fechaOp"));
                    cuenta.sethoraOp(item.getString("horaOp"));
                    cuenta.setdesOp(item.getString("desOp"));
                    cuenta.setimOp(item.getString("imOp"));
                    cuenta.setcanalEnv(item.getString("canalEnv"));
                    cuenta.setestEnv(item.getString("estEnv"));
                    cuenta.settimeEst(item.getString("timeEst"));

                    this.listaCuentas.add(cuenta);
                }

            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
        }

    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<CuentaMensaje> getListaCuentas() {
        return listaCuentas;
    }

    public void setListaCuentas(List<CuentaMensaje> listaCuentas) {
        this.listaCuentas = listaCuentas;
    }
    }