package bancomer.api.alertasdigitales.gui.controllers;

import java.util.ArrayList;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.support.v4.view.ViewPager;
import bancomer.api.alertasdigitales.gui.delegates.AlertasDigitalesDelegate;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.alertasdigitales.io.ServerResponseImpl;
import bancomer.api.alertasdigitales.models.Cuenta;
import bancomer.api.alertasdigitales.models.DetalleCuentaAlertasData;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.ListaSeleccionViewController;
import bancomer.api.common.timer.TimerController;
import bancomer.api.alertasdigitales.R;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;


public class AlertasDigitalesViewController extends FragmentActivity {
	MyFragmentPagerAdapter adapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
	//AMZ
	public ArrayList<String> estados = new ArrayList<String>();
	ViewPager pager = null;
	// Layout de la lista de creditos
	private LinearLayout vista;
	private int numerodeRegistros;
	// Instancia del delegate
	private AlertasDigitalesDelegate alertasDigitalesDelegate;
	// Instancia del adapter para la lista seleccionable
	private ListaSeleccionViewController listaSeleccion;
	//[CGI-Configuracion-Obligatorio] Instancia del baseviewController encargado de la funcionalidad comun de las vistas
	private BaseViewController baseViewController;
	//[CGI-Configuracion-Obligatorio] Instancia del parentManager encargado de la pila de delegates y las redirecciones
	private BaseViewsController parentManager;
	private LinearLayout rl;
	public BaseViewController getBaseViewController() {
		return baseViewController;
	}
	public void setBaseViewController(BaseViewController baseViewController) {
		this.baseViewController = baseViewController;
	}
	public BaseViewsController getParentManager() {
		return parentManager;
	}
	public void setParentManager(BaseViewsController parentManager) {
		this.parentManager = parentManager;
	}
	public ListaSeleccionViewController getListaSeleccion() {
		return listaSeleccion;
	}
	public void irAtras() {
		super.onBackPressed();
	}

	String valoracion ="";
	/**
	 * Default constructor for this activity
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		//[CGI-Configuracion-Obligatorio] Llamada a Activity
		super.onCreate(savedInstanceState);
		//[CGI-Configuracion-Obligatorio] Inicializacion del singleton de parametros globales
		InitAlertasDigitales init = InitAlertasDigitales.getInstance();
		//[CGI-Configuracion-Obligatorio] Tomamos la instancia del baseviewController encargado de la funcionalidad comun de las vistas
		baseViewController = init.getBaseViewController();
		//[CGI-Configuracion-Obligatorio] Tomamos la instancia del parentManager encargado de la pila de delegates y las redirecciones
		parentManager = init.getParentManager();
		TrackingHelper.trackState("alertasaccountslist", estados);
		//[CGI-Configuracion-Obligatorio] Llamadas para estandarizar las ventanas
		baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_alertas_digitales_layout_bmovil_alertas_digitales);
		//baseViewController.setTitle(this, R.string.bmovil_menu_alertasdigitales_titulo, R.drawable.api_alertas_digitales_an_ic_alertas);
		rl = (LinearLayout)findViewById(R.id.relativeFotter);
		//[CGI-Configuracion-Obligatorio] Tomamos el delegate en caso de estar ya instanciado
		alertasDigitalesDelegate = (AlertasDigitalesDelegate) parentManager.getBaseDelegateForKey(AlertasDigitalesDelegate.ALERTAS_DIGITALES_DELEGATE_ID);
		if (alertasDigitalesDelegate == null) {
			alertasDigitalesDelegate = new AlertasDigitalesDelegate();
			parentManager.addDelegateToHashMap(AlertasDigitalesDelegate.ALERTAS_DIGITALES_DELEGATE_ID,
					alertasDigitalesDelegate);
		}

		//[CGI-Configuracion-Obligatorio] Establecemos el viewController en el delegate
		alertasDigitalesDelegate.setAlertasDigitalesViewController(this);
		vista = (LinearLayout) findViewById(R.id.lista_consulta__otros_creditos_layout);
		// Peticion de consulta cuentas alertas
		alertasDigitalesDelegate.setDetalleCuentaAlertasData(null);
		alertasDigitalesDelegate.consultaCuentasAlertasDigitales();
		scaleToGui();
	}



	@Override
	protected void onResume() {
		super.onResume();
		//[CGI-Configuracion-Obligatorio] Parametros necesarios para hacer animaciones de vuelta atras
		parentManager.setCurrentActivityApp(this);
		parentManager.setActivityChanging(false);
		//[CGI-Configuracion-Obligatorio] Establecemos el activity actual en el baseViewController para establecer sobre que activity ha de actuar ya que no hereda de la vista
		baseViewController.setActivity(this);
		//[CGI-Configuracion-Obligatorio] Establecemos el delegate actual en el baseViewController para conocer a que delegate debe volver
		baseViewController.setDelegate(alertasDigitalesDelegate);
		if (adapter.fragments.size()>0){
			index=0;
			alertasDigitalesDelegate.cuenta.clear();
			obtencion_detalles(null);
		}
		alertasDigitalesDelegate.cuenta.clear();
	}

	public void scaleToGui() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scale(findViewById(R.id.layoutRoot));
		guiTools.scale(findViewById(R.id.lista_consulta__otros_creditos_layout));
		guiTools.scale(findViewById(R.id.ayudaCuentaSinAlertas));
	}

	private void setTab() {
		pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageScrollStateChanged(int position) {
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageSelected(int position) {
				// TODO Auto-generated method stub
				btnAction(position);
			}

		});

	}

	private void btnAction(int action){
		rl.removeAllViews();
		for(int i = 0; i < numerodeRegistros; i++)
		{
			final Button btn = new Button(this);
			if(i==action)
			{
				LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
				params.setMargins(10,10,10,10);
				rl.addView(btn);
				btn.setWidth(30);
				btn.setHeight(30);
				btn.setLayoutParams(params);
				btn.setBackgroundResource(R.drawable.rounded_cell2);
			}
			else
			{
				LayoutParams params3 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
				params3.setMargins(10,10,10,10);
				rl.addView(btn);
				btn.setWidth(30);
				btn.setHeight(30);
				btn.setLayoutParams(params3);
				btn.setBackgroundResource(R.drawable.rounded_cell);
			}
		}
	}
	int index=0;
	public void obtencion_detalles(DetalleCuentaAlertasData detalle){

		ArrayList<Object> liscta_cuentas = new ArrayList<Object>();
		liscta_cuentas = alertasDigitalesDelegate.getDatosTabla();
		if(detalle!=null && index<=liscta_cuentas.size() ){
			alertasDigitalesDelegate.cuenta.add(detalle);
		}
		if (index == liscta_cuentas.size()) {
			index++;
			if (adapter.fragments.size()==0){
				llenaListaSeleccion();
			}else{
				for (Fragment f:adapter.fragments) {
					if(f.isVisible() ){
						((ScreenSlidePageFragment)f).initView();
					}
				}
			}
		}else if(index>liscta_cuentas.size()){
			return;
		}else{
			Cuenta c=(Cuenta)alertasDigitalesDelegate.getConsultaCuentasAlertasData().getListaCuentas().get(index);
			if("N".equals(c.getIndAlertas())){
				index++;
				alertasDigitalesDelegate.cuenta.add(new DetalleCuentaAlertasData());
				if(index<liscta_cuentas.size()){
					c=(Cuenta)alertasDigitalesDelegate.getConsultaCuentasAlertasData().getListaCuentas().get(index);
					alertasDigitalesDelegate.detalleAlertasDigitales(c.getNumCuenta());
					index++;
				}else if(index==liscta_cuentas.size()){
					index++;
					if (adapter.fragments.size()==0){
						llenaListaSeleccion();
					}else{
						for (Fragment f:adapter.fragments) {
							if(f.isVisible() ){
								((ScreenSlidePageFragment)f).initView();
							}
						}
					}
				}else{
					return;
				}
			}else{
				alertasDigitalesDelegate.detalleAlertasDigitales(c.getNumCuenta());
				index++;
			}
		}

	}

	@SuppressWarnings("deprecation")
	public void llenaListaSeleccion() {
		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		adapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
		ArrayList<Object> lista = new ArrayList<Object>();
		ArrayList<Object> encabezado = new ArrayList<Object>();
		encabezado.add(null);
		encabezado.add(getString(R.string.bmovil_menu_alertasdigitales_cabecera1));
		encabezado.add(getString(R.string.bmovil_menu_alertasdigitales_cabecera2));

		lista = alertasDigitalesDelegate.getDatosTabla();
		this.pager = (ViewPager) this.findViewById(R.id.pager);
		numerodeRegistros = lista.size();


		for(int i=0;i<lista.size();i++)
		{
			ArrayList<Object> registro = (ArrayList<Object>) lista.get(i);
			adapter.addFragment(ScreenSlidePageFragment.newInstance(registro.get(1).toString(), registro.get(2).toString(),registro.get(3).toString(), alertasDigitalesDelegate, i));
		}

		this.pager.setAdapter(adapter);
		for(int j=0; j<lista.size();j++)
		{
			final Button btn = new Button(this);
			if(j==0)
			{
				LayoutParams params1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
				params1.setMargins(10,10,10,10);
				rl.addView(btn);
				btn.setWidth(30);
				btn.setHeight(30);
				btn.setLayoutParams(params1);
				btn.setBackgroundResource(R.drawable.rounded_cell2);
			}
			else
			{
				LayoutParams params2 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
				params2.setMargins(10,10,10,10);
				rl.addView(btn);
				btn.setWidth(30);
				btn.setHeight(30);
				btn.setLayoutParams(params2);
				btn.setBackgroundResource(R.drawable.rounded_cell);
			}
		}
		setTab();
		params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//		params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_bottom_margin);;

		if (listaSeleccion == null) {
			listaSeleccion = new ListaSeleccionViewController(this, params, baseViewController.getParentViewsController());
			listaSeleccion.setDelegate(alertasDigitalesDelegate);
			listaSeleccion.setNumeroColumnas(2);
			listaSeleccion.setEncabezado(encabezado);
			listaSeleccion.setTitle("");
			listaSeleccion.setLista(lista);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(lista.size());
			listaSeleccion.setExisteFiltro(false);
			listaSeleccion.cargarTabla();
			vista.addView(listaSeleccion);

		} else {
			listaSeleccion.setLista(lista);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(lista.size());
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.cargarTabla();
		}
		if (adapter.fragments.size()>0){
			for (Fragment f:adapter.fragments) {
				if(f.isVisible() ){
					((ScreenSlidePageFragment)f).initView();
				}
			}
		}
	}

	public void mostrarAyudaCuentaSinAlertas() {
		View ayudaCuentaSinAlertas = findViewById(R.id.ayudaCuentaSinAlertas);
		ayudaCuentaSinAlertas.setVisibility(View.VISIBLE);
	}

	public void ocultarAyudaCuentaSinAlertas() {
		View ayudaCuentaSinAlertas = findViewById(R.id.ayudaCuentaSinAlertas);
		ayudaCuentaSinAlertas.setVisibility(View.GONE);
	}

	@Override
	public void onUserInteraction() {
		baseViewController.onUserInteraction();
	}


	public void processNetworkResponse(int operationId, ServerResponseImpl response) {
		alertasDigitalesDelegate.analyzeResponse(operationId, response);
	}


	@Override
	protected void onPause() {
		TimerController timerContr = TimerController.getInstance();
		if (timerContr.isCerrandoSesion()) {
			parentManager.setCurrentActivityApp(this);
			parentManager.setActivityChanging(false);
			baseViewController.goBack(this, parentManager);
			if (Server.ALLOW_LOG) Log.d("Bebug>>", ">>>>>>>>>>>> onPause");
		}
		super.onPause();
		parentManager.consumeAccionesDePausa();
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		//baseViewController.goBack(this, parentManager);
		TrackingHelper.touchAtrasState();
	}
	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}


}