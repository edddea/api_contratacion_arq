package bancomer.api.alertasdigitales.models;

public class ConfirmaAlertas {

	private String tipoCuenta;
	private String numCuenta;
	private String aliasCuenta;
	private String medioEnvio;
	private String compCelular;
	private String correoAlerta;
	private String impCargo;
	private String impDeposito;
	private String accionAlerta;
	private String fechaAccion;
	
	

	/**
	 * @param tipoCuenta
	 * @param numCuenta
	 * @param aliasCuenta
	 * @param medioEnvio
	 * @param compCelular
	 * @param correoAlerta
	 * @param impCargo
	 * @param impDeposito
	 * @param accionAlerta
	 * @param fechaAccion
	 */
	public ConfirmaAlertas(String tipoCuenta, String numCuenta,
			String aliasCuenta, String medioEnvio, String compCelular,
			String correoAlerta, String impCargo, String impDeposito,
			String accionAlerta, String fechaAccion) {
		super();
		this.tipoCuenta = tipoCuenta;
		this.numCuenta = numCuenta;
		this.aliasCuenta = aliasCuenta;
		this.medioEnvio = medioEnvio;
		this.compCelular = compCelular;
		this.correoAlerta = correoAlerta;
		this.impCargo = impCargo;
		this.impDeposito = impDeposito;
		this.accionAlerta = accionAlerta;
		this.fechaAccion = fechaAccion;
	}
	
	
	/**
	 * @return the confirmaAlertas
	 */
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	
	/**
	 * @param confirmaAlertas the confirmaAlertas to set
	 */
	public void setConfirmaAlertas(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	/**
	 * @return the numCuenta
	 */
	public String getNumCuenta() {
		return numCuenta;
	}
	/**
	 * @param numCuenta the numCuenta to set
	 */
	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}
	/**
	 * @return the aliasCuenta
	 */
	public String getAliasCuenta() {
		return aliasCuenta;
	}
	/**
	 * @param aliasCuenta the aliasCuenta to set
	 */
	public void setAliasCuenta(String aliasCuenta) {
		this.aliasCuenta = aliasCuenta;
	}
	/**
	 * @return the medioEnvio
	 */
	public String getMedioEnvio() {
		return medioEnvio;
	}
	/**
	 * @param medioEnvio the medioEnvio to set
	 */
	public void setMedioEnvio(String medioEnvio) {
		this.medioEnvio = medioEnvio;
	}
	/**
	 * @return the compCelular
	 */
	public String getCompCelular() {
		return compCelular;
	}
	/**
	 * @param compCelular the compCelular to set
	 */
	public void setCompCelular(String compCelular) {
		this.compCelular = compCelular;
	}
	/**
	 * @return the correoAlerta
	 */
	public String getCorreoAlerta() {
		return correoAlerta;
	}
	/**
	 * @param correoAlerta the correoAlerta to set
	 */
	public void setCorreoAlerta(String correoAlerta) {
		this.correoAlerta = correoAlerta;
	}
	/**
	 * @return the impCargo
	 */
	public String getImpCargo() {
		return impCargo;
	}
	/**
	 * @param impCargo the impCargo to set
	 */
	public void setImpCargo(String impCargo) {
		this.impCargo = impCargo;
	}
	/**
	 * @return the impDeposito
	 */
	public String getImpDeposito() {
		return impDeposito;
	}
	/**
	 * @param impDeposito the impDeposito to set
	 */
	public void setImpDeposito(String impDeposito) {
		this.impDeposito = impDeposito;
	}
	/**
	 * @return the accionAlerta
	 */
	public String getAccionAlerta() {
		return accionAlerta;
	}
	/**
	 * @param accionAlerta the accionAlerta to set
	 */
	public void setAccionAlerta(String accionAlerta) {
		this.accionAlerta = accionAlerta;
	}
	/**
	 * @return the fechaAccion
	 */
	public String getFechaAccion() {
		return fechaAccion;
	}
	/**
	 * @param fechaAccion the fechaAccion to set
	 */
	public void setFechaAccion(String fechaAccion) {
		this.fechaAccion = fechaAccion;
	}
}
