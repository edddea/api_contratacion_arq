package bancomer.api.alertasdigitales.implementations;

import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import bancomer.api.alertasdigitales.gui.controllers.AlertasDigitalesSmsViewController;
import bancomer.api.alertasdigitales.gui.controllers.AlertasDigitalesViewController;
import bancomer.api.alertasdigitales.gui.controllers.ConfirmacionViewController;
import bancomer.api.alertasdigitales.gui.controllers.DetalleMensajeEnvViewController;
import bancomer.api.alertasdigitales.gui.controllers.IngresaDatosAlertaViewController;
import bancomer.api.alertasdigitales.gui.controllers.MensajesCuentaViewController;
import bancomer.api.alertasdigitales.gui.controllers.MenuAlertasViewController;
import bancomer.api.alertasdigitales.gui.controllers.ResultadoAlertasDigitalesViewController;
import bancomer.api.alertasdigitales.gui.delegates.AlertasDigitalesDelegate;
import bancomer.api.alertasdigitales.gui.delegates.AlertasDigitalesSmsDelegate;
import bancomer.api.alertasdigitales.gui.delegates.ConfirmacionDelegate;
import bancomer.api.alertasdigitales.gui.delegates.ConsultaData;
import bancomer.api.alertasdigitales.gui.delegates.IngresaDatosAlertasDelegate;
import bancomer.api.alertasdigitales.gui.delegates.MensajeAbonoCargoDelegate;
import bancomer.api.alertasdigitales.io.BaseSubapplication;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.alertasdigitales.models.AlertasDigitalesSendData;
import bancomer.api.alertasdigitales.models.ConfirmaAlertas;
import bancomer.api.alertasdigitales.models.Cuenta;
import bancomer.api.alertasdigitales.models.CuentaMensaje;
import bancomer.api.alertasdigitales.models.DetalleCuentaAlertasData;
import bancomer.api.alertasdigitales.models.DetalleMensajesCuentaData;
import bancomer.api.alertasdigitales.models.EstadoData;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.BaseViewsControllerImpl;

public class InitAlertasDigitales {

	private static InitAlertasDigitales mInstance = null;
	
	private BaseViewController baseViewController;
	
	private static BaseViewsController parentManager;
	
	private BaseSubapplication baseSubApp;
	
	private AlertasDigitalesSendData consultaSend; 	

	private Activity activity;
	
	private Boolean finishedModule;
	
	private ConsultaData consultaData;

	private String alertasDigitales;
	
	/**
     * Se utiliza un httpclient de Apache Jakarta que viene de la aplicaciÃ³n principal
     */
	public static DefaultHttpClient client;


	public static DefaultHttpClient getClient() {
		return client;
	}

	public static void setClient(DefaultHttpClient client) {
		InitAlertasDigitales.client = client;
	}

	/**
	 * Getters y Setters 
	 * 
	 */

	public Boolean getFinishedModule() {
		return finishedModule;
	}

	public AlertasDigitalesSendData getConsultaSend() {
		return consultaSend;
	}

	public void setConsultaSend(AlertasDigitalesSendData consultaSend) {
		this.consultaSend = consultaSend;
	}

	public void setConsultaData(ConsultaData consultaData) {
		this.consultaData = consultaData;
	}
	
	public ConsultaData getConsultaData(){
		ConsultaData delegate = (ConsultaData) parentManager.getBaseDelegateForKey(ConsultaData.CONSULTA_DATA_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new ConsultaData();
//			parentManager.addDelegateToHashMap(ConsultaData.CONSULTA_DATA_DELEGATE_ID,
//					delegate);
//			this.consultaData = delegate;
//		}
		
		return delegate;
	}

	public void setFinishedModule(Boolean finishedModule) {
		this.finishedModule = finishedModule;
	}

	public BaseViewController getBaseViewController() {
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitAlertasDigitales] Se toma la instancia de baseViewController");
		return baseViewController;
	}

	public void setBaseViewController(BaseViewController baseViewController) {
		this.baseViewController = baseViewController;
	}

	public BaseViewsController getParentManager() {
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitAlertasDigitales] Se toma la instancia de parentManager");
		return parentManager;
	}

	public void setParentManager(BaseViewsController parentManager) {
		this.parentManager = parentManager;
	}
		
	public BaseSubapplication getBaseSubApp() {
		return baseSubApp;
	}

	public void setBaseSubApp(BaseSubapplication baseSubApp) {
		this.baseSubApp = baseSubApp;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}
	/**
	 * End Getters y Setters 
	 * 
	 */
 
	/**
	 * Constructores
	 * 
	 */
//    public static InitAlertasDigitales getInstance(Activity activity){
//        if(mInstance == null)
//        {
//            mInstance = new InitAlertasDigitales();
//        }
//        
//        parentManager.setCurrentActivityApp(activity);
//        mInstance.setActivity(activity);
//        
//        return mInstance;
//    }
	
	public static InitAlertasDigitales getInstance(Activity activity ){
        if(mInstance == null) {
            mInstance = new InitAlertasDigitales(activity);
        }else{
        	mInstance.baseSubApp.getServer();
        }
        parentManager.setCurrentActivityApp(activity);
        mInstance.setActivity(activity);
        return mInstance;
    }
    
    
//    public InitAlertasDigitales(){
//    	consultaSend = new AlertasDigitalesSendData();
//    	baseViewController = new BaseViewControllerImpl();
//    	parentManager = new BaseViewsControllerImpl();
//    	baseSubApp = new BaseSubapplicationImpl(this.activity);
//    	finishedModule = false;
//    	
//    	initServerParams();
//    }
	public InitAlertasDigitales(Activity activity){

    	consultaSend = new AlertasDigitalesSendData();
    	baseViewController = new BaseViewControllerImpl();
    	parentManager = new BaseViewsControllerImpl();
    	baseSubApp = new BaseSubapplicationImpl(activity);
    	finishedModule = false;
    }
	
    private void initServerParams(){
    	Server.EMULATOR = consultaSend.getEmulator();
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitAlertasDigitales] Se inicializa emulator -> "+Server.EMULATOR);
    	Server.SIMULATION = consultaSend.getSimulation();
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitAlertasDigitales] Se inicializa simulation -> "+Server.SIMULATION);
    	Server.DEVELOPMENT = consultaSend.getDevelopment();
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitAlertasDigitales] Se inicializa development -> "+Server.DEVELOPMENT);
    }
 
    
    public static InitAlertasDigitales getInstance(){
        if(mInstance == null)
        {
			if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitAlertasDigitales] Se inicializa la instancia del singleton de datos");
            mInstance = new InitAlertasDigitales(null);
        }
        return mInstance;
    }
    /**
     * End Constructores
     */
    
    public void finishModuleFlags(){
    	finishedModule = true;
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitAlertasDigitales] FinishModuleFlags - finishedModule "+finishedModule);
//    	TimerManager.setMainAppInControl(true);
//    	Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitAlertasDigitales] FinishModuleFlags - isMainAppInControl "+TimerManager.isMainAppInControl());
    }
    
    public void initModuleFlags(){
    	finishedModule = false;
    	//TimerManager.initTimer(consultaSend.getTimeout());
    }
    
    public void lookForCreditosData(Activity act){
    	ConsultaData delegate = (ConsultaData) parentManager.getBaseDelegateForKey(ConsultaData.CONSULTA_DATA_DELEGATE_ID);
		if (delegate == null) {
			delegate = new ConsultaData();
			parentManager.addDelegateToHashMap(ConsultaData.CONSULTA_DATA_DELEGATE_ID,
					delegate);
		}
		
		delegate.consultaOtrosCreditos(act);
    }
	
	public void showViewController(Class<?> viewController, int flags, boolean inverted, String[] extrasKeys, Object[] extras) {
		initModuleFlags();
		Intent intent = new Intent(activity, viewController);
		if (flags != 0) {
			intent.setFlags(flags);
		}
		
		if (extrasKeys != null && extras != null && extras.length == extrasKeys.length) {
			int extrasLength = extras.length;
			for (int i=0; i<extrasLength; i++) {
				if (extras[i] instanceof String) {
					intent.putExtra(extrasKeys[i], (String)extras[i]);
				} else if (extras[i] instanceof Integer) {
					intent.putExtra(extrasKeys[i], ((Integer)extras[i]).intValue());
				} else if (extras[i] instanceof Boolean) {
					intent.putExtra(extrasKeys[i], ((Boolean)extras[i]).booleanValue());
				}else if(extras[i] instanceof Long){
					intent.putExtra(extrasKeys[i], ((Long)extras[i]).longValue());
				}
			}
		}
		
		activity.startActivity(intent);
		if (inverted) {
			getParentManager().overrideScreenBackTransition();
		} else {
			getParentManager().overrideScreenForwardTransition();
		}
    }
	
	public void showViewController(Class<?> viewController) {
		showViewController(viewController, 0, false, null, null);
	}

    public void showAlertasDigitales(){
    	showViewController(MenuAlertasViewController.class);
    }

	public void showAlertasDigitalesViewController(){
		AlertasDigitalesDelegate delegate = new AlertasDigitalesDelegate();
			parentManager.addDelegateToHashMap(AlertasDigitalesDelegate.ALERTAS_DIGITALES_DELEGATE_ID,delegate);
		delegate.setIsMensajeAbono(false);
		showViewController(AlertasDigitalesViewController.class);
	}

	public void showMensajesAbonoViewController(){
		AlertasDigitalesDelegate delegate = (AlertasDigitalesDelegate) parentManager.getBaseDelegateForKey(AlertasDigitalesDelegate.ALERTAS_DIGITALES_DELEGATE_ID);
		if (delegate == null) {
			delegate = new AlertasDigitalesDelegate();
			parentManager.addDelegateToHashMap(AlertasDigitalesDelegate.ALERTAS_DIGITALES_DELEGATE_ID,delegate);
		}
		delegate.setIsMensajeAbono(true);
		showViewController(AlertasDigitalesViewController.class);
	}

	public void ShowAlertasDigitalesSmsViewController(){
		AlertasDigitalesSmsDelegate delegate =  new AlertasDigitalesSmsDelegate();
		/*if (delegate == null) {
			delegate = new AlertasDigitalesSmsDelegate();		}*/
		parentManager.addDelegateToHashMap(AlertasDigitalesSmsDelegate.ALERTAS_DIGITALES_SMS_DELEGATE_ID,delegate);
		delegate.setIsMensajeAbono(true);
		showViewController(AlertasDigitalesSmsViewController.class);
	}

	public void showDetalleMensajeEnv(){
		MensajeAbonoCargoDelegate delegate = (MensajeAbonoCargoDelegate) parentManager.getBaseDelegateForKey(MensajeAbonoCargoDelegate.MENSAJES_ABONO_CARGO_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MensajeAbonoCargoDelegate();
			parentManager.addDelegateToHashMap(MensajeAbonoCargoDelegate.MENSAJES_ABONO_CARGO_DELEGATE_ID, delegate);
		}
		showViewController(DetalleMensajeEnvViewController.class);
	}


	public void showDetallesAlertasDigitales(){
    	showViewController(IngresaDatosAlertaViewController.class);
    }
    
    public void showConfirmacionAlertasDigitales(BaseDelegateImpl delegateBase){
    	ConfirmacionDelegate delegate = new ConfirmacionDelegate(delegateBase);
		parentManager.addDelegateToHashMap(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID, delegate);
    	showViewController(ConfirmacionViewController.class);
    }
    
    public void showConfirmacionAlertasDigitales(BaseDelegateImpl delegateBase, ConfirmaAlertas confirmaAlertas){
		
    	ConfirmacionDelegate delegate = new ConfirmacionDelegate(delegateBase);
    	//delegateBase.se
		parentManager.addDelegateToHashMap(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID, delegate);
		
    	showViewController(ConfirmacionViewController.class);
    }
    
    /**
     * Peticion para detalle desde fuera del modulo
     * @param cred
     */
    public void showDetallesAlertasDigitales(DetalleCuentaAlertasData detalleCuenta, Cuenta cuentaSeleccionada){
    	initModuleFlags();
    	IngresaDatosAlertasDelegate delegate = (IngresaDatosAlertasDelegate) parentManager.getBaseDelegateForKey(IngresaDatosAlertasDelegate.INGRESA_DATOS_ALERTA_DELEGATE_ID);
		if (delegate == null) {
			delegate = new IngresaDatosAlertasDelegate();
			parentManager.addDelegateToHashMap(IngresaDatosAlertasDelegate.INGRESA_DATOS_ALERTA_DELEGATE_ID,
					delegate);
		}
		
		delegate.setDetalleCuentaAlertasData(detalleCuenta);
		delegate.setCuentaSeleccionada(cuentaSeleccionada);
    	showViewController(IngresaDatosAlertaViewController.class);
    }

	public void showOperacionesAlertasDigitales(DetalleMensajesCuentaData detalleCuenta, Cuenta cuentaSeleccionada){
		initModuleFlags();
		MensajeAbonoCargoDelegate delegate = (MensajeAbonoCargoDelegate) parentManager.getBaseDelegateForKey(MensajeAbonoCargoDelegate.MENSAJES_ABONO_CARGO_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MensajeAbonoCargoDelegate();
			parentManager.addDelegateToHashMap(MensajeAbonoCargoDelegate.MENSAJES_ABONO_CARGO_DELEGATE_ID,
					delegate);
		}

		delegate.setDetalleMensajesCuentaData(detalleCuenta);
		delegate.setCuentaSeleccionada(cuentaSeleccionada);
		showViewController(MensajesCuentaViewController.class);
	}

	/**
	 * Peticion para detalle desde fuera del modulo
	 * @param cred
	 */
	public void showDetallesAlertasDigitales(Cuenta cuentaSeleccionada){
		initModuleFlags();
		IngresaDatosAlertasDelegate delegate = (IngresaDatosAlertasDelegate) parentManager.getBaseDelegateForKey(IngresaDatosAlertasDelegate.INGRESA_DATOS_ALERTA_DELEGATE_ID);
		if (delegate == null) {
			delegate = new IngresaDatosAlertasDelegate();
			parentManager.addDelegateToHashMap(IngresaDatosAlertasDelegate.INGRESA_DATOS_ALERTA_DELEGATE_ID,
					delegate);
		}

		delegate.setCuentaSeleccionada(cuentaSeleccionada);
		showViewController(IngresaDatosAlertaViewController.class);
	}
    
    /**
     * Peticion para detalle desde fuera del modulo
     * @param cred
     */
    public void showResultadoAlertasDigitales(EstadoData estadoAlertasData){
    	initModuleFlags();
    	IngresaDatosAlertasDelegate delegate = (IngresaDatosAlertasDelegate) parentManager.getBaseDelegateForKey(IngresaDatosAlertasDelegate.INGRESA_DATOS_ALERTA_DELEGATE_ID);
		if (delegate == null) {
			delegate = new IngresaDatosAlertasDelegate();
			parentManager.addDelegateToHashMap(IngresaDatosAlertasDelegate.INGRESA_DATOS_ALERTA_DELEGATE_ID,
					delegate);
		}
		
		delegate.setEstadoData(estadoAlertasData);
    	showViewController(AlertasDigitalesViewController.class);
    }

	public String getAlertasDigitales() {
		return alertasDigitales;
	}

	public void setAlertasDigitales(String alertasDigitales) {
		this.alertasDigitales = alertasDigitales;
	}
}
