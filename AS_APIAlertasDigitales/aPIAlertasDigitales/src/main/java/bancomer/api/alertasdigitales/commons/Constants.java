package bancomer.api.alertasdigitales.commons;

public class Constants {


	/**
	 * Constante que indica el campo alias
	 */
	public static final String CUENTA = "Cuenta";
	
	/**
	 * Constante que indica el campo alias
	 */
	public static final String ALIAS = "Alias";
	
	/**
	 * Constante que indica el campo medio envio
	 */
	public static final String MEDIO_ENVIO = "Medio envío";
	
	/**
	 * Constante que indica el campo compania celular
	 */
	public static final String COMPANIA_CELULAR = "Compañía Celular";
	
	/**
	 * Constante que indica el campo numero celular
	 */
	public static final String NUM_CELULAR = "Número celular";
	
	/**
	 * Constante que indica el campo importe cargo
	 */
	public static final String IMPORTE_CARGO = "Importe cargo";
	
	/**
	 * Constante que indica el campo importe deposito
	 */
	public static final String IMPORTE_DEPOSITO = "Importe depósito";
	
	/**
	 * Constante que indica el campo accion
	 */
	public static final String ACCION = "Acción";
	
	/**
	 * Constante que indica el campo fecha
	 */
	public static final String FECHA = "Fecha";
	
	/** Validacion de datos para alta de alertas **/
	public static final String ALIAS_NO_VALIDO = "Necesitas ingresar el nombre del Alias";
	public static final String COMPANIA_CELULAR_NO_VALIDO = "Necesitas seleccionar una compañia celular";
	public static final String NUMERO_CELULAR_NO_VALIDO = "Necesitas ingresar tu numero de celular";
	public static final String IMPORTE_CARGO_NO_VALIDO_GT1000 = "El importe de cargo no puede ser mayor a $1000, ingrese un monto entre $50 y $1000.";
	public static final String IMPORTE_CARGO_NO_VALIDO_LT50 = "El importe de cargo no puede ser menor a $50, ingrese un monto entre $50 y $1000.";
	public static final String IMPORTE_CARGO_NO_VALIDO = "En el importe de cargo debes ingresar un valor entre $50 y $1,000.";
	public static final String IMPORTE_DEPOSITO_NO_VALIDO = "En el importe de depósito debes ingresar un valor mayor o igual a $50";
	public static final String PARSE_ERROR = "Error de parseo";
	public static final String CERO_MODIFICACIONES = "Ningún elemento ha sido modificado";
	public static final String NUMERO_CELULAR_NO_CONFIRMADO = "Valor erróneo en confirmar Número celular";


	public enum OperacionesAlertas{
		alta("Alta"), baja("Baja"), modificacion("Modificación");
		
		public String value;

		private OperacionesAlertas(String value) {
			this.value = value;
		}
	}
}
