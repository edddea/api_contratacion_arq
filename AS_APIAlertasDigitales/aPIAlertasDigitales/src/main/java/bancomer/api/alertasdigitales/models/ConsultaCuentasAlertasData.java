package bancomer.api.alertasdigitales.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bancomer.api.alertasdigitales.io.ParsingHandler;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;

public class ConsultaCuentasAlertasData implements ParsingHandler{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String estado;
	
	private List<Cuenta> listaCuentas = new ArrayList<Cuenta>();
		
	
	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
		try {
			this.estado = parser.parseNextValue("estado");
		
			JSONArray datos = parser.parseNextValueWithArray("cuentas", false);
		
			if(datos != null){
				JSONObject item;
				for (int i = 0; i < datos.length(); i++) {
				
					item = datos.getJSONObject(i);
				
					Cuenta cuenta = new Cuenta();
				
					cuenta.setTipoCuenta(item.getString("tipoCuenta"));
					cuenta.setNumCuenta(item.getString("numCuenta"));
					cuenta.setIndAlertas(item.getString("indAlertas"));
				
					this.listaCuentas.add(cuenta);
				}
			
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
		}
		
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public List<Cuenta> getListaCuentas() {
		return listaCuentas;
	}

	public void setListaCuentas(List<Cuenta> listaCuentas) {
		this.listaCuentas = listaCuentas;
	}

}
