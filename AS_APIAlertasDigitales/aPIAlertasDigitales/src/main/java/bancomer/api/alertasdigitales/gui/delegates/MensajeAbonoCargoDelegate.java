package bancomer.api.alertasdigitales.gui.delegates;

import android.content.DialogInterface;
import android.util.Log;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.StringTokenizer;

import bancomer.api.alertasdigitales.gui.controllers.DetalleMensajeEnvViewController;
import bancomer.api.alertasdigitales.gui.controllers.MensajesCuentaViewController;
import bancomer.api.alertasdigitales.implementations.BaseDelegateImpl;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import bancomer.api.alertasdigitales.io.ParsingHandler;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.alertasdigitales.models.Cuenta;
import bancomer.api.alertasdigitales.models.CuentaMensaje;
import bancomer.api.alertasdigitales.models.DetalleMensajeEnvData;
import bancomer.api.alertasdigitales.models.DetalleMensajesCuentaData;
import bancomer.api.common.commons.ServerConstants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.io.ServerResponse;

public class MensajeAbonoCargoDelegate extends BaseDelegateImpl {


    public static final long MENSAJES_ABONO_CARGO_DELEGATE_ID = 0x96;

    private DetalleMensajesCuentaData detalleMensajesCuentaData;
    private Cuenta cuentaSeleccionada;
    private CuentaMensaje cuentaSel;
    private MensajesCuentaViewController mensajeCuentaViewController;
    private DetalleMensajeEnvViewController detalleMensajeEnvViewController;
    private DetalleMensajeEnvData detalleMensajeEnvData;

//        public MensajeAbonoViewController viewController;

    public void doNetworkOperation(int operationId, Hashtable<String, ?> params,final boolean isJson,
                                   final ParsingHandler handler, BaseViewController caller) {
        InitAlertasDigitales.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params,isJson,handler, caller, true);
    }


    public void setDetalleMensajesCuentaData(DetalleMensajesCuentaData detalleMensajesCuentaData) {
        this.detalleMensajesCuentaData = detalleMensajesCuentaData;
    }

    public DetalleMensajesCuentaData getDetalleMensajesCuentaData(){
        return detalleMensajesCuentaData;
    }


    public void setCuentaSeleccionada(Cuenta cuentaSeleccionada) {
        this.cuentaSeleccionada = cuentaSeleccionada;
    }

    public Cuenta getCuentaSeleccionada() {
        return cuentaSeleccionada;
    }

    public void setCuentaSel(CuentaMensaje cuentaSel) {
        this.cuentaSel = cuentaSel;
    }

    public CuentaMensaje getCuentaSel() {
        return cuentaSel;
    }


    public void setMensajesCuentaViewController(
            MensajesCuentaViewController mensajeCuentaViewController) {

        if (Server.ALLOW_LOG)
            Log.i("[CGI-Configuracion-Obligatorio] >> ", "[AlertasDigitalesDelegate] Se setea el viewController para AlertasDigitalesViewController");
        this.mensajeCuentaViewController = mensajeCuentaViewController;
    }


    public void setDetalleMensajeEnvViewController(
            DetalleMensajeEnvViewController detalleMensajeEnvViewController) {

        if (Server.ALLOW_LOG)
            Log.i("[CGI-Configuracion-Obligatorio] >> ", "[AlertasDigitalesDelegate] Se setea el viewController para AlertasDigitalesViewController");
        this.detalleMensajeEnvViewController = detalleMensajeEnvViewController;
    }

    public ArrayList<Object> getDatosTabla() {

        final ArrayList<Object> datos = new ArrayList<Object>();
        ArrayList<String> registro;

        if ((null == detalleMensajesCuentaData) || detalleMensajesCuentaData.getListaCuentas().isEmpty()) {
            registro = new ArrayList<String>();
            registro.add(null);
            registro.add("");
            registro.add("");
            registro.add("");
        } else {
            for (final CuentaMensaje cuenta : detalleMensajesCuentaData.getListaCuentas()) {
                registro = new ArrayList<String>();
                registro.add(null);
                registro.add(formatearFecha(cuenta.getfechaOp()));
                registro.add(cuenta.getdesOp());
                registro.add(formatImporte(cuenta.getimOp(),"$"));
                datos.add(registro);
            }
        }

        return datos;
    }

    private String formatImporte(String importe, String divisa){
        String sign = "";
        if(importe.startsWith("-")){
            importe = importe.substring(1);
            sign = "-";
        }else if(importe.startsWith("+")){
            importe = importe.substring(1);
        }

        try {
            importe = (Float.valueOf(importe)).toString();
        }catch(NumberFormatException e){
            if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), e.getMessage());
        }

        return sign+divisa+Tools.formatUserAmount(importe);
    }

    public ArrayList<Object> getDatosDetalleMensajeEnv() {
        return detalleMensajeEnvData.getDetalles();
    }

    public DetalleMensajeEnvData getDetalleMensajeEnvData() {
        return detalleMensajeEnvData;
    }

    public void analyzeResponse(int operationId, ServerResponse response) {
        if (operationId == Server.OP_DETALLE_MENSAJE_ENV_ALERTAS) {
            if (Server.ALLOW_LOG) Log.d(">> CGI", "Entra por consulta Detalle cuentas alertas");
            if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
                if (Server.ALLOW_LOG) Log.d(">> CGI", "Consulta otros creditos >> Success");
                detalleMensajeEnvData = (DetalleMensajeEnvData) response
                        .getResponse();

                InitAlertasDigitales.getInstance().showDetalleMensajeEnv();

                //alertasDigitalesViewController.cargaListaDatos(detalleCuentaAlertasData);

            } else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
                if (Server.ALLOW_LOG) Log.d(">> CGI", "Consulta Detalle otros creditos >> Warning");

                InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(mensajeCuentaViewController, response.getMessageText());

            } else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
                if (Server.ALLOW_LOG) Log.d(">> CGI", "Consulta Detalle otros creditos >> Error");

                InitAlertasDigitales.getInstance().getBaseViewController().ocultaIndicadorActividad();
                InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(mensajeCuentaViewController, response.getMessageText(), (new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        if (Server.ALLOW_LOG)
                            Log.d(">> CGI", "Consulta otros creditos >> EX#2 Error en el servicio");
                        mensajeCuentaViewController.getBaseViewController().goBack(mensajeCuentaViewController, mensajeCuentaViewController.getParentManager());
                        mensajeCuentaViewController.irAtras();

                    }
                }));

            }
        }
    }

    public void detalleMenEnv() {
        if(Server.ALLOW_LOG) Log.d(">> CGI", "Relleno petición Detalle Consulta otros Créditos");

        Hashtable<String, String> paramTable = new Hashtable<String, String>();
        int operacion = Server.OP_DETALLE_MENSAJE_ENV_ALERTAS;

        paramTable.put(ServerConstants.TIME_STAMP, cuentaSel.gettimeEst());


        doNetworkOperation(operacion, paramTable,true,new DetalleMensajeEnvData(), mensajeCuentaViewController.getBaseViewController());
    }

    public void performAction(Object obj) {
       cuentaSel = detalleMensajesCuentaData.getListaCuentas().get(mensajeCuentaViewController.getListaSeleccion_cuentasmensaje().getOpcionSeleccionada());
                detalleMenEnv();
            }

    private String formatearFecha(String fecha){
        String ret = "";
        StringTokenizer str = new StringTokenizer(fecha, "-");
        ArrayList<String> array = new ArrayList<String>();
        array.add(str.nextToken());//Año
        array.add(str.nextToken());//Mes
        array.add(str.nextToken());//Dia
        ret = array.get(2) +"/"+ array.get(1) +"/"+ array.get(0);

        return ret;
    }
    
}
