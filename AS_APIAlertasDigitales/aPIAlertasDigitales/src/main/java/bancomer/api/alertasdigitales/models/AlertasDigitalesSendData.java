package bancomer.api.alertasdigitales.models;

import bancomer.api.alertasdigitales.commons.Autenticacion;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.generaOTPS.GeneraOTPS;
import bancomer.api.common.model.CatalogoVersionado;
import android.util.Log;
import bancomer.api.common.model.CatalogoVersionado;

public class AlertasDigitalesSendData {
	
	private String IUM;
	
	private String username;
	
	private String cadenaAutenticacion;
	
	private String cveAcceso;
	
	private String tarjeta5Dig;
	
	private String codigoNip;
	
	private String codigoCvv2;
	
	private String codigoOtp;
	
	private String securityInstrument;
	
	private Boolean softTokenStatus;
	
	private Constants.Perfil perfil;
	
	private long timeout;
	
	private Boolean simulation;
	
	private Boolean development;
	
	private Boolean emulator;
	
	private CatalogoVersionado catalogoTelefonicas;
	
	private GeneraOTPS generaOtps;
	
	private CallBackModule callBackModule;
	
	private Autenticacion autenticacion;

	public AlertasDigitalesSendData() {
		super();
		this.IUM = "";
		this.username = "";
		this.cadenaAutenticacion = "";
		this.cveAcceso = "";
		this.tarjeta5Dig = "";
		this.codigoNip = "";
		this.codigoCvv2 = "";
		this.codigoOtp = "";
		this.securityInstrument = "";
		this.softTokenStatus = false;
		this.timeout = 0L;
		this.development = true;
		this.simulation = true;
		this.emulator = false;
		this.autenticacion = Autenticacion.getInstance();
	}
	
	public void setServerParams(Boolean dev, Boolean sim, Boolean em){
		this.development = dev;
		this.simulation = sim;
		this.emulator = em;
		
		// Seteo de parametros de servidor
		Server.DEVELOPMENT = dev;
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitConsultaOtrosCreditos] Se inicializa development -> "+Server.DEVELOPMENT);
		Server.SIMULATION = sim;
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitConsultaOtrosCreditos] Se inicializa simulation -> "+Server.SIMULATION);
		Server.EMULATOR = em;
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitConsultaOtrosCreditos] Se inicializa emulator -> "+Server.EMULATOR);
		
		//Server.setServer();
		Server.setAllowLog();
	}

	public Boolean getSimulation() {
		return simulation;
	}

	public void setSimulation(Boolean simulation) {
		this.simulation = simulation;
	}

	public Boolean getDevelopment() {
		return development;
	}

	public void setDevelopment(Boolean development) {
		this.development = development;
	}

	public Boolean getEmulator() {
		return emulator;
	}

	public void setEmulator(Boolean emulator) {
		this.emulator = emulator;
	}

	public long getTimeout() {
		return timeout;
	}

	public void setTimeout(long timeout) {
		long prep = (timeout*98)/100;

		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[ConsultaOtrosCreditosSendData] Inicializacion timeout -> "+prep);
		this.timeout = prep;
	}

	public String getIUM() {
		return IUM;
	}

	public void setIUM(String iUM) {
		IUM = iUM;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCadenaAutenticacion() {
		return cadenaAutenticacion;
	}

	public void setCadenaAutenticacion(String cadenaAutenticacion) {
		this.cadenaAutenticacion = cadenaAutenticacion;
	}

	public String getCveAcceso() {
		return cveAcceso;
	}

	public void setCveAcceso(String cveAcceso) {
		this.cveAcceso = cveAcceso;
	}

	public String getTarjeta5Dig() {
		return tarjeta5Dig;
	}

	public void setTarjeta5Dig(String tarjeta5Dig) {
		this.tarjeta5Dig = tarjeta5Dig;
	}

	public String getCodigoNip() {
		return codigoNip;
	}

	public void setCodigoNip(String codigoNip) {
		this.codigoNip = codigoNip;
	}

	public String getCodigoCvv2() {
		return codigoCvv2;
	}

	public void setCodigoCvv2(String codigoCvv2) {
		this.codigoCvv2 = codigoCvv2;
	}

	public String getCodigoOtp() {
		return codigoOtp;
	}

	public void setCodigoOtp(String codigoOtp) {
		this.codigoOtp = codigoOtp;
	}

	public CatalogoVersionado getCatalogoTelefonicas() {
		return catalogoTelefonicas;
	}

	public void setCatalogoTelefonicas(CatalogoVersionado catalogoTelefonicas) {
		this.catalogoTelefonicas = catalogoTelefonicas;
	}

	public String getSecurityInstrument() {
		return securityInstrument;
	}

	public void setSecurityInstrument(String securityInstrument) {
		this.securityInstrument = securityInstrument;
	}

	public Boolean getSoftTokenStatus() {
		return softTokenStatus;
	}

	public void setSoftTokenStatus(Boolean softTokenStatus) {
		this.softTokenStatus = softTokenStatus;
	}

	public Constants.Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		Constants.Perfil p = Constants.Perfil.recortado;
		if(perfil.equalsIgnoreCase(Constants.Perfil.avanzado.toString())){
			p = Constants.Perfil.avanzado;
		}else if(perfil.equalsIgnoreCase(Constants.Perfil.basico.toString())){
			p = Constants.Perfil.basico;
		}
		this.perfil = p;
	}

	public Autenticacion getAutenticacion() {
		return autenticacion;
	}

	public void setAutenticacion(Autenticacion autenticacion) {
		this.autenticacion = autenticacion;
	}

	public GeneraOTPS getGeneraOtps() {
		return generaOtps;
	}

	public void setGeneraOtps(GeneraOTPS generaOtps) {
		this.generaOtps = generaOtps;
	}

	public CallBackModule getCallBackModule() {
		return callBackModule;
	}

	public void setCallBackModule(CallBackModule callBackModule) {
		this.callBackModule = callBackModule;
	}
	
	

}
