package bancomer.api.alertasdigitales.models;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.NombreCuenta;

public class CuentaMensaje {
	
	private String fechaOp;
	private String horaOp;
	private String desOp;
	private String imOp;
	private String canalEnv;
	private String estEnv;
	private String timeEst;


	/**
	 * @return the timeEst
	 */
	public String gettimeEst() {
		return timeEst;
	}

	/**
	 * @param timeEst the fechaOp to set
	 */
	public void settimeEst(String timeEst) {
		this.timeEst = timeEst;
	}

	/**
	 * @return the estEnv
	 */
	public String getestEnv() {
		return estEnv;
	}

	/**
	 * @param estEnv the fechaOp to set
	 */
	public void setestEnv(String estEnv) {
		this.estEnv = estEnv;
	}

	/**
	 * @return the fechaOp
	 */
	public String getfechaOp() {
		return fechaOp;
	}

	/**
	 * @param fechaOp the fechaOp to set
	 */
	public void setfechaOp(String fechaOp) {
		this.fechaOp = fechaOp;
	}

	/**
	 * @return the canalEnv
	 */
	public String getcanalEnv() {
		return canalEnv;
	}

	/**
	 * @param canalEnv the fechaOp to set
	 */
	public void setcanalEnv(String canalEnv) {
		this.canalEnv = canalEnv;
	}
	
	/**
	 * @param imOp the fechaOp to set
	 */
	public void setimOp(String imOp) {
		this.imOp = imOp;
	}

	/**
	 * @return the imOp
	 */
	public String getimOp() {
		return imOp;
	}

	/**
	 * @return the horaOp
	 */
	public String gethoraOp() {
		return horaOp;
	}
	
	/**
	 * @param horaOp the horaOp to set
	 */
	public void sethoraOp(String horaOp) {
		this.horaOp = horaOp;
	}
	
	
	/**
	 * @return the desOp
	 */
	public String getdesOp() {
		return desOp;
	}
	
	/**
	 * @param desOp the desOp to set
	 */
	public void setdesOp(String desOp) {
		this.desOp = desOp;
	}
	
	
	public String getNombreTipoCta() {
		// Variable nombre cuenta
		NombreCuenta nombreCuenta = null;
		String sNombreCuenta = null;
		if(fechaOp.equals(Constants.LIBRETON_TYPE)){
			nombreCuenta = NombreCuenta.li;			
		}else if(fechaOp.equals(Constants.CHECK_TYPE)){
			nombreCuenta = NombreCuenta.ch;
		}else if(fechaOp.equals(Constants.SAVINGS_TYPE)){
			nombreCuenta = NombreCuenta.ah;
		}else if(fechaOp.equals(Constants.CREDIT_TYPE)){
			nombreCuenta = NombreCuenta.tc;
		}else if(fechaOp.equals(Constants.DEBIT_TYPE)){
			nombreCuenta = NombreCuenta.td;
		}else if(fechaOp.equals(Constants.PREPAID_TYPE)){
			nombreCuenta = NombreCuenta.tp;
		}else if(fechaOp.equals(Constants.EXPRESS_TYPE)){
			nombreCuenta = NombreCuenta.ce;
		}else if(fechaOp.equals(Constants.T_ADICIONAL)){
			nombreCuenta = NombreCuenta.ad;
		}else if(fechaOp.equals(Constants.T_MINI)){
			nombreCuenta = NombreCuenta.mi;
		}		
		if(nombreCuenta == null){
			sNombreCuenta = Constants.TIPO_CUENTA_NO_DEFINIDA;
		}else{
			sNombreCuenta = nombreCuenta.value; 
		}
		return sNombreCuenta;
	}
	
}

