package bancomer.api.alertasdigitales.gui.delegates;

import java.util.Hashtable;

import bancomer.api.alertasdigitales.gui.controllers.MenuAlertasViewController;
import bancomer.api.alertasdigitales.implementations.BaseDelegateImpl;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import bancomer.api.alertasdigitales.io.ParsingHandler;
import bancomer.api.common.gui.controllers.BaseViewController;


public class MenuAlertasDelegate extends BaseDelegateImpl {
    public static final long MENU_ALERTAS_DELEGATE_ID = 0x5;

    public MenuAlertasViewController viewController;

    public void doNetworkOperation(int operationId, Hashtable<String, ?> params,final boolean isJson,
                                   final ParsingHandler handler, BaseViewController caller) {
        InitAlertasDigitales.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params,isJson,handler, caller,true);
    }

}
