package bancomer.api.alertasdigitales.models;

import java.io.IOException;
import java.util.ArrayList;

import bancomer.api.alertasdigitales.io.ParsingHandler;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;

public class DetalleMensajeEnvData implements ParsingHandler {

    /**
     *
     */

    private static final long serialVersionUID = 1459030020044126L;

    private ArrayList<Object> detalles;
    private String estado;
    private String timeEst;
    private String alias;
    private String cliente;
    private String tarjeta;
    private String importeFiltro;
    private String importeOperacion;
    private String divisa;
    private String dispositivo;
    private String telefonica;
    private String correoPart1;
    private String correoPart2;
    private String correoPart3;
    private String correoPart4;
    private String mensaje1;
    private String mensaje2;
    private String mensaje3;
    private String estadoEnvio;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getTimeEst() {
        return timeEst;
    }

    public void setTimeEst(String timeEst) {
        this.timeEst = timeEst;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(String tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getImporteFiltro() {
        return importeFiltro;
    }

    public void setImporteFiltro(String importeFiltro) {
        this.importeFiltro = importeFiltro;
    }

    public String getImporteOperacion() {
        return importeOperacion;
    }

    public void setImporteOperacion(String importeOperacion) {
        this.importeOperacion = importeOperacion;
    }

    public String getDivisa() {
        return divisa;
    }

    public void setDivisa(String divisa) {
        this.divisa = divisa;
    }

    public String getDispositivo() {
        return dispositivo;
    }

    public void setDispositivo(String dispositivo) {
        this.dispositivo = dispositivo;
    }

    public String getTelefonica() {
        return telefonica;
    }

    public void setTelefonica(String telefonica) {
        this.telefonica = telefonica;
    }

    public String getCorreoPart1() {
        return correoPart1;
    }

    public void setCorreoPart1(String correoPart1) {
        this.correoPart1 = correoPart1;
    }

    public String getCorreoPart2() {
        return correoPart2;
    }

    public void setCorreoPart2(String correoPart2) {
        this.correoPart2 = correoPart2;
    }

    public String getCorreoPart3() {
        return correoPart3;
    }

    public void setCorreoPart3(String correoPart3) {
        this.correoPart3 = correoPart3;
    }

    public String getCorreoPart4() {
        return correoPart4;
    }

    public void setCorreoPart4(String correoPart4) {
        this.correoPart4 = correoPart4;
    }

    public String getMensaje1() {
        return mensaje1;
    }

    public void setMensaje1(String mensaje1) {
        this.mensaje1 = mensaje1;
    }

    public String getMensaje2() {
        return mensaje2;
    }

    public void setMensaje2(String mensaje2) {
        this.mensaje2 = mensaje2;
    }

    public String getMensaje3() {
        return mensaje3;
    }

    public void setMensaje3(String mensaje3) {
        this.mensaje3 = mensaje3;
    }

    public String getEstadoEnvio() {
        return estadoEnvio;
    }

    public void setEstadoEnvio(String estadoEnvio) {
        this.estadoEnvio = estadoEnvio;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }


    @Override
    public void process(Parser parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub

    }

    public ArrayList<Object> getDetalles(){
        detalles = new ArrayList<Object>();

        detalles.add(estado);
        detalles.add(timeEst);
        detalles.add(cliente);
        detalles.add(tarjeta);
        detalles.add(importeFiltro);
        detalles.add(importeOperacion);
        detalles.add(divisa);
        detalles.add(dispositivo);
        detalles.add(telefonica);
        detalles.add(correoPart1);
        detalles.add(correoPart2);
        detalles.add(correoPart3);
        detalles.add(correoPart4);
        detalles.add(mensaje1);
        detalles.add(mensaje2);
        detalles.add(mensaje3);
        detalles.add(alias);

        return detalles;
    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        this.estado = parser.parseNextValue("estado");
        this.timeEst = parser.parseNextValue("timeEst", false);
        this.cliente = parser.parseNextValue("cliente", false);
        this.tarjeta = parser.parseNextValue("tarjeta", false);
        this.importeFiltro = parser.parseNextValue("importeFiltro", false);
        this.importeOperacion = parser.parseNextValue("importeOperacion", false);
        this.divisa = parser.parseNextValue("divisa", false);
        this.dispositivo = parser.parseNextValue("dispositivo", false);
        this.telefonica = parser.parseNextValue("telefonica", false);
        this.correoPart1 = parser.parseNextValue("correoPart1", false);
        this.correoPart2 = parser.parseNextValue("correoPart2", false);
        this.correoPart3 = parser.parseNextValue("correoPart3", false);
        this.correoPart4 = parser.parseNextValue("correoPart4", false);
        this.mensaje1 = parser.parseNextValue("mensaje1", false);
        this.mensaje2 = parser.parseNextValue("mensaje2", false);
//        this.mensaje3 = parser.parseNextValue("mensaje3", false);
        this.estadoEnvio = parser.parseNextValue("estadoEnvio", false);
        this.alias = parser.parseNextValue("alias", false);

    }
}
