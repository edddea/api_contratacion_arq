/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package bancomer.api.alertasdigitales.io;

/**
 * ServerException notifies an exception in server processing.
 *
 * @author Stefanini IT Solutions.
 */
public class ServerException extends Exception {

    /**
     * Default constructor.
     * @param message the message
     */
    public ServerException(String message) {
        super(message);
    }

}
