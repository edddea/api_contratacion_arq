package bancomer.api.alertasdigitales.models;

import java.io.IOException;

import bancomer.api.alertasdigitales.io.ParsingHandler;
import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;

public class DetalleCuentaAlertasData implements ParsingHandler{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String estado;
	private String asunto;
	private String tipoVinculo;
	private String alias;
	private String importeCargo;
	private String importeAbono;
	private String dispositivoEnvioFav;
	private String telefonicaCod;
	private String telefonicaDescr;
	private String numeroCelular;
	private String switchMetodoEnvio;

	
	
	
	/**
	 * 
	 */
	public DetalleCuentaAlertasData() {
		super();
	}

	
	
	
	/**
	 * @param alias
	 * @param importeCargo
	 * @param importeAbono
	 * @param telefonicaDescr
	 * @param numeroCelular
	 */
	public DetalleCuentaAlertasData(String alias, String importeCargo,
			String importeAbono, String telefonicaDescr, String numeroCelular) {
		super();
		this.alias = alias;
		this.importeCargo = importeCargo;
		this.importeAbono = importeAbono;
		this.telefonicaDescr = telefonicaDescr;
		this.numeroCelular = numeroCelular;
	}




	/**
	 * @param estado
	 * @param asunto
	 * @param tipoVinculo
	 * @param alias
	 * @param importeCargo
	 * @param importeAbono
	 * @param dispositivoEnvioFav
	 * @param telefonicaCod
	 * @param telefonicaDescr
	 * @param numeroCelular
	 * @param correoPart1
	 * @param correoPart2
	 * @param correoPart3
	 * @param indicadorToken
	 */
	public DetalleCuentaAlertasData(String estado, String asunto,
			String tipoVinculo, String alias, String importeCargo,
			String importeAbono, String dispositivoEnvioFav,
			String telefonicaCod, String telefonicaDescr, String numeroCelular,
			String switchMetodoEnvio) {
		super();
		this.estado = estado;
		this.asunto = asunto;
		this.tipoVinculo = tipoVinculo;
		this.alias = alias;
		this.importeCargo = importeCargo;
		this.importeAbono = importeAbono;
		this.dispositivoEnvioFav = dispositivoEnvioFav;
		this.telefonicaCod = telefonicaCod;
		this.telefonicaDescr = telefonicaDescr;
		this.numeroCelular = numeroCelular;
		this.switchMetodoEnvio = switchMetodoEnvio;
	}

	/**
	 * @return the estado
	 */
	public String getEstado() {
		return estado;
	}
	
	/**
	 * @param estado the estado to set
	 */
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
	/**
	 * @return the asunto
	 */
	public String getAsunto() {
		return asunto;
	}
	
	/**
	 * @param asunto the asunto to set
	 */
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
	
	
	/**
	 * @return the tipoVinculo
	 */
	public String getTipoVinculo() {
		return tipoVinculo;
	}
	
	/**
	 * @param tipoVinculo the tipoVinculo to set
	 */
	public void setTipoVinculo(String tipoVinculo) {
		this.tipoVinculo = tipoVinculo;
	}
	
	
	/**
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}
	
	/**
	 * @param alias the alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}
	
	
	/**
	 * @return the importeCargo
	 */
	public String getImporteCargo() {
		return importeCargo;
	}
	
	/**
	 * @param importeCargo the importeCargo to set
	 */
	public void setImporteCargo(String importeCargo) {
		this.importeCargo = importeCargo;
	}
	
	
	/**
	 * @return the importeAbono
	 */
	public String getImporteAbono() {
		return importeAbono;
	}
	/**
	 * @param importeAbono the importeAbono to set
	 */
	public void setImporteAbono(String importeAbono) {
		this.importeAbono = importeAbono;
	}
	
	
	/**
	 * @return the dispositivoEnvioFav
	 */
	public String getDispositivoEnvioFav() {
		return dispositivoEnvioFav;
	}
	
	/**
	 * @param dispositivoEnvioFav the dispositivoEnvioFav to set
	 */
	public void setDispositivoEnvioFav(String dispositivoEnvioFav) {
		this.dispositivoEnvioFav = dispositivoEnvioFav;
	}
	
	
	/**
	 * @return the telefonicaCod
	 */
	public String getTelefonicaCod() {
		return telefonicaCod;
	}
	
	/**
	 * @param telefonicaCod the telefonicaCod to set
	 */
	public void setTelefonicaCod(String telefonicaCod) {
		this.telefonicaCod = telefonicaCod;
	}
	
	
	/**
	 * @return the telefonicaDescr
	 */
	public String getTelefonicaDescr() {
		return telefonicaDescr;
	}
	
	/**
	 * @param telefonicaDescr the telefonicaDescr to set
	 */
	public void setTelefonicaDescr(String telefonicaDescr) {
		this.telefonicaDescr = telefonicaDescr;
	}
	
	
	/**
	 * @return the numeroCelular
	 */
	public String getNumeroCelular() {
		return numeroCelular;
	}
	
	/**
	 * @param numeroCelular the numeroCelular to set
	 */
	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}

	/**
	 * @return the indicadorToken
	 */
	public String getSwitchMetodoEnvio() {
		return switchMetodoEnvio;
	}
	
	/**
	 * @param switchMetodoEnvio the indicadorToken to set
	 */
	public void setSwitchMetodoEnvio(String switchMetodoEnvio) {
		this.switchMetodoEnvio = switchMetodoEnvio;
	}


	/**
	 * @return the confirmaNumeroCelular
	 */
	public String getConfirmarNumeroCelular() {
		return numeroCelular;
	}


	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		this.estado = parser.parseNextValue("estado");
		this.asunto = parser.parseNextValue("asunto");
		this.tipoVinculo = parser.parseNextValue("tipoVinculo");
		this.alias = parser.parseNextValue("alias");
		this.importeCargo = parser.parseNextValue("importeCargo");
		this.importeAbono = parser.parseNextValue("importeAbono");
		this.dispositivoEnvioFav = parser.parseNextValue("dispositivoEnvioFav");
		this.telefonicaCod = parser.parseNextValue("telefonicaCod");
		this.telefonicaDescr = parser.parseNextValue("telefonicaDescr");
		this.numeroCelular = parser.parseNextValue("numeroCelular");
		this.switchMetodoEnvio = parser.parseNextValue("switch");

	}
	
	
}

