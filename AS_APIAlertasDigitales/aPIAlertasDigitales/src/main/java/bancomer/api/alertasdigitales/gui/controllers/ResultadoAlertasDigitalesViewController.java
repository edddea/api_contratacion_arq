package bancomer.api.alertasdigitales.gui.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import java.util.ArrayList;

import bancomer.api.alertasdigitales.R;
import bancomer.api.alertasdigitales.gui.delegates.IngresaDatosAlertasDelegate;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.alertasdigitales.io.ServerResponseImpl;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.ListaDatosViewController;
import bancomer.api.common.timer.TimerController;


public class ResultadoAlertasDigitalesViewController  extends Activity {
	
	private Button resultadoMenu;
	private TextView opExitosa;
	
	private ListaDatosViewController listaDatos;
	
	// Instancia del delegate
	private IngresaDatosAlertasDelegate ingresaDatosAlertasDelegate;

	
	public IngresaDatosAlertasDelegate getIngresaDatosAlertasDelegate() {
		return ingresaDatosAlertasDelegate;
	}
	public void setIngresaDatosAlertasDelegate(
			IngresaDatosAlertasDelegate ingresaDatosAlertasDelegate) {
		this.ingresaDatosAlertasDelegate = ingresaDatosAlertasDelegate;
	}
	//[CGI-Configuracion-Obligatorio] Instancia del baseviewController encargado de la funcionalidad comun de las vistas
	private BaseViewController baseViewController;
	
	//[CGI-Configuracion-Obligatorio] Instancia del parentManager encargado de la pila de delegates y las redirecciones
	private BaseViewsController parentManager;
	
	public BaseViewController getBaseViewController() {
		return baseViewController;
	}
	public void setBaseViewController(BaseViewController baseViewController) {
		this.baseViewController = baseViewController;
	}
	public BaseViewsController getParentManager() {
		return parentManager;
	}
	public void setParentManager(BaseViewsController parentManager) {
		this.parentManager = parentManager;
	}
	
	
	/**
	 * Default constructor for this activity
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		//[CGI-Configuracion-Obligatorio] Llamada a Activity
		super.onCreate(savedInstanceState);
		
		// Alberto Redireccion alertas detalle
		//Intent intent = new Intent(this, IngresaDatosAlertaViewController.class);
		//startActivity(intent);
		
		
		//[CGI-Configuracion-Obligatorio] Inicializacion del singleton de parametros globales
		InitAlertasDigitales init = InitAlertasDigitales.getInstance();
		
		//[CGI-Configuracion-Obligatorio] Tomamos la instancia del baseviewController encargado de la funcionalidad comun de las vistas
		baseViewController = init.getBaseViewController();
		//[CGI-Configuracion-Obligatorio] Tomamos la instancia del parentManager encargado de la pila de delegates y las redirecciones
		parentManager = init.getParentManager();
		
		//[CGI-Configuracion-Obligatorio] Establecemos el activity actual en el baseViewController para establecer sobre que activity ha de actuar ya que no hereda de la vista
		baseViewController.setActivity(this);
		
		//[CGI-Configuracion-Obligatorio] Llamadas para estandarizar las ventanas 
		baseViewController.onCreate(this, BaseViewController.SHOW_HEADER|BaseViewController.SHOW_TITLE, R.layout.api_alertas_digitales_resultado);
		baseViewController.setTitle(this, R.string.bmovil_menu_alertasdigitales_titulo, R.drawable.api_alertas_digitales_an_ic_alertas);
		
		//[CGI-Configuracion-Obligatorio] Tomamos el delegate en caso de estar ya instanciado
		ingresaDatosAlertasDelegate = (IngresaDatosAlertasDelegate) parentManager.getBaseDelegateForKey(IngresaDatosAlertasDelegate.INGRESA_DATOS_ALERTA_DELEGATE_ID);
		if (ingresaDatosAlertasDelegate == null) {
			ingresaDatosAlertasDelegate = new IngresaDatosAlertasDelegate();
			parentManager.addDelegateToHashMap(IngresaDatosAlertasDelegate.INGRESA_DATOS_ALERTA_DELEGATE_ID,
					ingresaDatosAlertasDelegate);
		}

		//[CGI-Configuracion-Obligatorio] Establecemos el delegate actual en el baseViewController para conocer a que delegate debe volver
		baseViewController.setDelegate(ingresaDatosAlertasDelegate);
		
		//[CGI-Configuracion-Obligatorio] Establecemos el viewController en el delegate 
		ingresaDatosAlertasDelegate.setResultadoAlertasDigitalesViewController(this);
		
//		vista = (LinearLayout)findViewById(R.id.lista_consulta__otros_creditos_layout);
		
		// Peticion de consulta cuentas alertas
//		ingresaDatosAlertasDelegate.consultaCuentasAlertasDigitales();
		cargarListaDatos();
		resultadoMenu = (Button)findViewById(R.id.resultados_menu_button);
		opExitosa = (TextView)findViewById(R.id.lblOperacionExitosa);
		scaleToGui();
		final Activity act = this;
		resultadoMenu.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v) {
	            // Perform action on click
//	        	InitAlertasDigitales.getInstance().setFinishedModule(true);
	        	baseViewController.goBack(act, parentManager);
	        	InitAlertasDigitales.getInstance().getConsultaSend().getCallBackModule().returnToPrincipal();
	        }
	    });
	}
	
	public void scaleToGui(){
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scale(findViewById(R.id.layoutRoot));
		guiTools.scale(findViewById(R.id.resultados_menu_button));
		guiTools.scale(findViewById(R.id.lblOperacionExitosa));
//		guiTools.scale(findViewById(R.id.lista_consulta__otros_creditos_layout));
	}
	

	
	private void cargarListaDatos() {
		ArrayList<Object> datos = ingresaDatosAlertasDelegate.cargarListaResultados();
		
		
		/////
		LinearLayout.LayoutParams layoutParams = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		listaDatos = new ListaDatosViewController(this, layoutParams, parentManager);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		
//		listaDatos.setTitulo(RESULT_OK);
		
		listaDatos.showLista();
		
		LinearLayout listaDatosLayout = (LinearLayout)findViewById(R.id.layoutListaDatos);
		listaDatosLayout.addView(listaDatos);
	}
	
	@Override
	public void onUserInteraction(){
		baseViewController.onUserInteraction();
	}
	
	
	public void processNetworkResponse(int operationId, ServerResponseImpl response) {
		ingresaDatosAlertasDelegate.analyzeResponse(operationId, response);
	}
		
	@Override
	protected void onResume() {
		super.onResume();
		//[CGI-Configuracion-Obligatorio] Parametros necesarios para hacer animaciones de vuelta atras
		parentManager.setCurrentActivityApp(this);
		parentManager.setActivityChanging(false);
		
		//[CGI-Configuracion-Obligatorio] Establecemos el activity actual en el baseViewController para establecer sobre que activity ha de actuar ya que no hereda de la vista
		baseViewController.setActivity(this);
	}
	
	@Override
	protected void onPause() {
		/*if(TimerManager.isMainAppInControl()){
			parentManager.setCurrentActivityApp(this);
			parentManager.setActivityChanging(false);
			baseViewController.goBack(this, parentManager);
			Log.d("Bebug>>", ">>>>>>>>>>>> onPause");
		}*/
		
		TimerController timerContr = TimerController.getInstance();
		if(timerContr.isCerrandoSesion()){
			parentManager.setCurrentActivityApp(this);
			parentManager.setActivityChanging(false);
			baseViewController.goBack(this, parentManager);
			if(Server.ALLOW_LOG) Log.d("Bebug>>", ">>>>>>>>>>>> onPause");
		}
		
		super.onPause();
		parentManager.consumeAccionesDePausa();
	}
	
	@Override
	public void onBackPressed(){
//		TimerManager.setMainAppInControl(true);
		//baseViewController.goBack(this, parentManager);
	}
	@Override
	protected void onStop() {
		super.onStop();
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
