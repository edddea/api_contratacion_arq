package bancomer.api.alertasdigitales.gui.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;


import com.bancomer.base.SuiteApp;

import java.util.ArrayList;

import bancomer.api.alertasdigitales.R;
import bancomer.api.alertasdigitales.gui.delegates.MenuAlertasDelegate;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.alertasdigitales.io.ServerResponseImpl;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;


public class MenuAlertasViewController extends Activity implements OnClickListener{

    //AMZ
    public ArrayList<String> estados = new ArrayList<String>();
    //[CGI-Configuracion-Obligatorio] Instancia del baseviewController encargado de la funcionalidad comun de las vistas
    private BaseViewController baseViewController;

    //[CGI-Configuracion-Obligatorio] Instancia del parentManager encargado de la pila de delegates y las redirecciones
    private BaseViewsController parentManager;
    public BaseViewController getBaseViewController() {
        return baseViewController;
    }
    public void setBaseViewController(BaseViewController baseViewController) {
        this.baseViewController = baseViewController;
    }
    public BaseViewsController getParentManager() {
        return parentManager;
    }
    public void setParentManager(BaseViewsController parentManager) {
        this.parentManager = parentManager;
    }
    private MenuAlertasDelegate delegate;
    private LinearLayout alertasButton, mensajesButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //[CGI-Configuracion-Obligatorio] Llamada a Activity
        super.onCreate(savedInstanceState);
        SuiteApp.appContext=this;
        //[CGI-Configuracion-Obligatorio] Inicializacion del singleton de parametros globales
        InitAlertasDigitales init = InitAlertasDigitales.getInstance();

        //[CGI-Configuracion-Obligatorio] Tomamos la instancia del baseviewController encargado de la funcionalidad comun de las vistas
        baseViewController = init.getBaseViewController();
        //[CGI-Configuracion-Obligatorio] Tomamos la instancia del parentManager encargado de la pila de delegates y las redirecciones
        parentManager = init.getParentManager();
        TrackingHelper.trackState("menualertas",estados);

        //[CGI-Configuracion-Obligatorio] Llamadas para estandarizar las ventanas
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER|BaseViewController.SHOW_TITLE, R.layout.api_alertas_digitales_activity_menu_alertas_layout);
        baseViewController.setTitle(this, R.string.bmovil_menu_alertasdigitales_titulo, R.drawable.api_alertas_digitales_an_ic_alertas);

        //[CGI-Configuracion-Obligatorio] Tomamos el delegate en caso de estar ya instanciado
        delegate = (MenuAlertasDelegate) parentManager.getBaseDelegateForKey(MenuAlertasDelegate.MENU_ALERTAS_DELEGATE_ID);
        if (delegate == null) {
            delegate = new MenuAlertasDelegate();
            parentManager.addDelegateToHashMap(MenuAlertasDelegate.MENU_ALERTAS_DELEGATE_ID,
                    delegate);
        }

        // Peticion de consulta cuentas alertas
        init();
        if (init.getAlertasDigitales().equals("Alertas")){
            alertasSelected();
            finish();
        }
        else if (init.getAlertasDigitales().equals("SMS")){
            mensajesSelected();
            finish();
        }
    }

    public void init(){
        findViews();
        scaleForCurrentScreen();
        alertasButton.setOnClickListener(this);
        mensajesButton.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v == alertasButton) {
            alertasSelected();
        }else if(v == mensajesButton){
            mensajesSelected();
        }
    }


    public void processNetworkResponse(int operationId, ServerResponseImpl response) {
        delegate.analyzeResponse(operationId, response);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //[CGI-Configuracion-Obligatorio] Parametros necesarios para hacer animaciones de vuelta atras
        parentManager.setCurrentActivityApp(this);
        parentManager.setActivityChanging(false);

        //[CGI-Configuracion-Obligatorio] Establecemos el activity actual en el baseViewController para establecer sobre que activity ha de actuar ya que no hereda de la vista
        baseViewController.setActivity(this);

        //[CGI-Configuracion-Obligatorio] Establecemos el delegate actual en el baseViewController para conocer a que delegate debe volver
        baseViewController.setDelegate(delegate);
    }

    @Override
    protected void onPause() {
        TimerController timerContr = TimerController.getInstance();
        if(timerContr.isCerrandoSesion()){
            parentManager.setCurrentActivityApp(this);
            parentManager.setActivityChanging(false);
            baseViewController.goBack(this, parentManager);
            if(Server.ALLOW_LOG) Log.d("Bebug>>", ">>>>>>>>>>>> onPause");
        }
        super.onPause();
        parentManager.consumeAccionesDePausa();
    }

    @Override
    public void onBackPressed(){
        //TimerManager.setMainAppInControl(true);
        baseViewController.goBack(this, parentManager);
        TrackingHelper.touchAtrasState();
    }

    private void findViews() {
        alertasButton = (LinearLayout)findViewById(R.id.bmovil_alertas_btn_1);
        mensajesButton = (LinearLayout)findViewById(R.id.bmovil_alertas_btn_2);
    }


    private void scaleForCurrentScreen() {
        GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());

        guiTools.scale(findViewById(R.id.rootLayout));

        //guiTools.scale(findViewById(R.id.lbl_consulta_mensajes), true);
        //guiTools.scale(findViewById(R.id.lbl_mis_cuentas), true);
        //guiTools.scale(findViewById(R.id.bmovil_alertas_btn_1));
        //guiTools.scale(findViewById(R.id.bmovil_alertas_btn_2));
    }

    private void alertasSelected(){
        /*Linea 269*/
        InitAlertasDigitales.getInstance().showAlertasDigitalesViewController();
    }

    private void mensajesSelected(){

        /*Linea 280*/
        InitAlertasDigitales.getInstance().ShowAlertasDigitalesSmsViewController();
    }

//    @Override
//    public void returnToPrincipal() {
//        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal(true);
//    }

}

