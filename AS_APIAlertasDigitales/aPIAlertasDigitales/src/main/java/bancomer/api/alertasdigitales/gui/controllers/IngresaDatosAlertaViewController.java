package bancomer.api.alertasdigitales.gui.controllers;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;

import bancomer.api.alertasdigitales.R;
import bancomer.api.alertasdigitales.gui.delegates.IngresaDatosAlertasDelegate;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.alertasdigitales.io.ServerResponseImpl;
import bancomer.api.alertasdigitales.models.DetalleCuentaAlertasData;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.ImportField;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.ListaDatosComboViewController;
import bancomer.api.common.gui.controllers.ListaSeleccionViewController;
import bancomer.api.common.gui.controllers.SeleccionHorizontalViewController;
import bancomer.api.common.model.Compania;
import bancomer.api.common.timer.TimerController;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

public class IngresaDatosAlertaViewController extends FragmentActivity implements AdapterView.OnItemSelectedListener{


	// Nuevos Campos

	private TextView tipoCuenta;
	private TextView numeroCuenta;
	private TextView numeroCelular;
	private ImageView aviso;

	private TextView txtDepositos;
	private TextView txtDepositosDesde;
	private ImageView txtImgDepositos;
	//private ImageButton imgCerrar;
	private RelativeLayout textoCargosAbono;

	// Fin Nuevos Campos
	
	private ImageButton continuarButton;
	
	// Layout de la lista de creditos
	private LinearLayout vista;
//	private TextView lblConfirmarNumeroCelular;
//	private EditText tbConfirmarNumeroCelular;
	private RelativeLayout layoutScroll;
	private String medioSelected;
	//AMZ
	public ArrayList<String> estados = new ArrayList<String>();

	public SeleccionHorizontalViewController getSeleccionHorizontal() {
		return seleccionHorizontal;
	}
	public void setSeleccionHorizontal(
			SeleccionHorizontalViewController seleccionHorizontal) {
		this.seleccionHorizontal = seleccionHorizontal;
	}
//	public EditText getTbNumeroTarjeta() {
//		return tbNumeroTarjeta;
//	}
//	public void setTbNumeroTarjeta(EditText tbNumeroTarjeta) {
//		this.tbNumeroTarjeta = tbNumeroTarjeta;
//	}
	public EditText getTbAlias() {
		return tbAlias;
	}
	public void setTbAlias(EditText tbAlias) {
		this.tbAlias = tbAlias;
	}
	/**
	 * @return the tbImporteDeposito
	 */
	public ImportField getTbImporteDeposito() {
		return tbImporteDeposito;
	}
	/**
	 * @param tbImporteDeposito the tbImporteDeposito to set
	 */
	public void setTbImporteDeposito(ImportField tbImporteDeposito) {
		this.tbImporteDeposito = tbImporteDeposito;
	}
	public CheckBox getCheckboxBaja() {
		return checkboxBaja;
	}
	public void setCheckboxBaja(CheckBox checkboxBaja) {
		this.checkboxBaja = checkboxBaja;
	}
//	public EditText getTbNumeroCelular() {
//		return tbNumeroCelular;
//	}
//	public void setTbNumeroCelular(EditText tbNumeroCelular) {
//		this.tbNumeroCelular = tbNumeroCelular;
//	}
	// Instancia del delegate
	private IngresaDatosAlertasDelegate ingresaDatosAlertasDelegate;

	/**
	 * @return the ingresaDatosAlertasDelegate
	 */
	public IngresaDatosAlertasDelegate getIngresaDatosAlertasDelegate() {
		return ingresaDatosAlertasDelegate;
	}
	/**
	 * @param ingresaDatosAlertasDelegate the ingresaDatosAlertasDelegate to set
	 */
	public void setIngresaDatosAlertasDelegate(
			IngresaDatosAlertasDelegate ingresaDatosAlertasDelegate) {
		this.ingresaDatosAlertasDelegate = ingresaDatosAlertasDelegate;
	}
	// Instancia del adapter para la lista seleccionable
	private ListaSeleccionViewController listaSeleccion;
	
	//[CGI-Configuracion-Obligatorio] Instancia del baseviewController encargado de la funcionalidad comun de las vistas
	private BaseViewController baseViewController;
	
	//[CGI-Configuracion-Obligatorio] Instancia del parentManager encargado de la pila de delegates y las redirecciones
	private BaseViewsController parentManager;

	private ListaDatosComboViewController listaDatos;

	private SeleccionHorizontalViewController seleccionHorizontal;

	
	private EditText tbAlias;
	private ImportField tbImporteCargo;
	/**
	 * @return the tbImporteCargo
	 */
	public ImportField getTbImporteCargo() {
		return tbImporteCargo;
	}
	/**
	 * @param tbImporteCargo the tbImporteCargo to set
	 */
	public void setTbImporteCargo(ImportField tbImporteCargo) {
		this.tbImporteCargo = tbImporteCargo;
	}
	private ImportField tbImporteDeposito;
	private CheckBox checkboxBaja;
//	private EditText tbNumeroCelular;
	protected boolean bajaAlerta;


//	private TextView lblNumeroCelular;

	private LinearLayout ayudaCargo;

	private LinearLayout layoutDepositoActivar;

	private ImageButton switchDepositoActivar;

	public ListaDatosComboViewController getListaDatos() {
		return listaDatos;
	}


	public BaseViewController getBaseViewController() {
		return baseViewController;
	}
	public void setBaseViewController(BaseViewController baseViewController) {
		this.baseViewController = baseViewController;
	}
	public BaseViewsController getParentManager() {
		return parentManager;
	}
	public void setParentManager(BaseViewsController parentManager) {
		this.parentManager = parentManager;
	}
	
	public ListaSeleccionViewController getListaSeleccion() {
		return listaSeleccion;
	}
	
	public void irAtras(){
		super.onBackPressed();
	}
	
	/**
	 * Default constructor for this activity
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		//[CGI-Configuracion-Obligatorio] Llamada a Activity
		super.onCreate(savedInstanceState);

		//[CGI-Configuracion-Obligatorio] Inicializacion del singleton de parametros globales
		InitAlertasDigitales init = InitAlertasDigitales.getInstance();
		
		//[CGI-Configuracion-Obligatorio] Tomamos la instancia del baseviewController encargado de la funcionalidad comun de las vistas
		baseViewController = init.getBaseViewController();
		//[CGI-Configuracion-Obligatorio] Tomamos la instancia del parentManager encargado de la pila de delegates y las redirecciones
		parentManager = init.getParentManager();

		TrackingHelper.trackState("alertas", estados);
		//[CGI-Configuracion-Obligatorio] Llamadas para estandarizar las ventanas 
		baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_alertas_digitales_ingresar_datos);
		//baseViewController.setTitle(this, R.string.bmovil_menu_alertasdigitales_titulo, R.drawable.api_alertas_digitales_an_ic_alertas);
		
		//[CGI-Configuracion-Obligatorio] Tomamos el delegate en caso de estar ya instanciado
		ingresaDatosAlertasDelegate = (IngresaDatosAlertasDelegate) parentManager.getBaseDelegateForKey(IngresaDatosAlertasDelegate.INGRESA_DATOS_ALERTA_DELEGATE_ID);
		if (ingresaDatosAlertasDelegate == null) {
			ingresaDatosAlertasDelegate = new IngresaDatosAlertasDelegate();
			parentManager.addDelegateToHashMap(IngresaDatosAlertasDelegate.INGRESA_DATOS_ALERTA_DELEGATE_ID,
					ingresaDatosAlertasDelegate);
		}
		
		//[CGI-Configuracion-Obligatorio] Establecemos el viewController en el delegate 
		ingresaDatosAlertasDelegate.setIngresaDatosAlertaViewController(this);
		init();
		//scaleToGui();
		aviso.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				FragmentManager fragmentManager = getSupportFragmentManager();
				AvisoDialogos alerts_dialogs = new AvisoDialogos();
				alerts_dialogs.show(fragmentManager, "alert_personalizado");
			}
		});
		// Carga de datos detalle
		cargaListaDatos(ingresaDatosAlertasDelegate.getDetalleCuentaAlertasData());
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		//[CGI-Configuracion-Obligatorio] Parametros necesarios para hacer animaciones de vuelta atras
		parentManager.setCurrentActivityApp(this);
		parentManager.setActivityChanging(false);
		//[CGI-Configuracion-Obligatorio] Establecemos el activity actual en el baseViewController para establecer sobre que activity ha de actuar ya que no hereda de la vista
		baseViewController.setActivity(this);
		//[CGI-Configuracion-Obligatorio] Establecemos el delegate actual en el baseViewController para conocer a que delegate debe volver
		baseViewController.setDelegate(ingresaDatosAlertasDelegate);

	}
	
	public void scaleToGui(){
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scale(findViewById(R.id.layoutRoot));
		
		// Escalado labels

		// Escalado texto editable
		//guiTools.scale(tbAlias, true);
		//guiTools.scale(tbImporteCargo, true);
		//guiTools.scale(tbImporteDeposito, true);
		//guiTools.scale(checkboxBaja);
		//guiTools.scale(layoutDepositoActivar);
		//guiTools.scale(switchDepositoActivar);
		//guiTools.scale(findViewById(R.id.layoutRoot));
	}
	
	
	private void init() {
		findViews();
		cargarComponenteListaDatos();
		cargarComponenteSeleccionHorizontal();
		scaleToCurrentScreen();
		tbImporteDeposito.setVisibility(View.GONE);

		tbImporteCargo.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if ((hasFocus) && (ingresaDatosAlertasDelegate.getMedioEnvioSMS().equals(medioSelected))) {
					ayudaCargo.setVisibility(View.VISIBLE);
				} else {
					ayudaCargo.setVisibility(View.GONE);
				}
			}
		});

		continuarButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String alias = tbAlias.getText().toString();
				String importeCargo = tbImporteCargo.getAmount();
				//CGI-Niko: Ceros en caso de que no este habilitado
				String importeDeposito;
				if (checkImporteDepositoVisibility()) {
					importeDeposito = tbImporteDeposito.getAmount();
				} else {
					importeDeposito = "0";
				}
				Compania company = (Compania) seleccionHorizontal.getSelectedItem();
				ingresaDatosAlertasDelegate.redirecciona(company, alias, importeCargo, importeDeposito);
			}
		});

		switchDepositoActivar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				switchDepositoActivar.setVisibility(View.GONE);
				setImporteDepositoVisibility();
			}
		});



		txtDepositos.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				switchDepositoActivar.setVisibility(View.VISIBLE);
				setImporteDepositoVisibility();
			}
		});
	}
	
	public String getAlias(){
		return tbAlias.getText().toString();
	}
	
	public String getImporteCargo(){
		return "$" + Tools.formatImportResult(tbImporteCargo.getAmount());
	}
	
	public String getImporteDeposito(){
		return "$" + Tools.formatImportResult(tbImporteDeposito.getAmount());
	}
	
	public Compania getCompany(){
		return (Compania) seleccionHorizontal.getSelectedItem();
	}

	/**
	 * Busca todas las referencias necesarias para el control de la pantalla.
	 */
	private void findViews() {

		layoutScroll = (RelativeLayout)findViewById(R.id.layoutScroll);
		ayudaCargo = (LinearLayout)findViewById(R.id.ayudaCargoLayout);
		textoCargosAbono=(RelativeLayout)findViewById(R.id.textoSize);
		tbAlias = (EditText)findViewById(R.id.tbAlias);
		InputFilter[] passFilterArray = new InputFilter[1];
		passFilterArray[0] = new InputFilter.LengthFilter(Constants.NUMERO_TELEFONO_LENGTH);
		tbImporteCargo = (ImportField)findViewById(R.id.tbImporteCargo);
		tbImporteDeposito = (ImportField)findViewById(R.id.tbImporteDeposito);
		continuarButton = (ImageButton)findViewById(R.id.btnContinuar);
		InputFilter[] filtersDeposito = new InputFilter[1];
		filtersDeposito[0] = new InputFilter.LengthFilter(8);
		tbImporteDeposito.setFilters(filtersDeposito);
		InputFilter[] filtersAlias= new InputFilter[1];
		filtersAlias[0] = new InputFilter.LengthFilter(10);
		tbAlias.setFilters(filtersAlias);
		layoutDepositoActivar = (LinearLayout)findViewById(R.id.layoutDepositoActivar);
		aviso = (ImageView)findViewById(R.id.signo);

		switchDepositoActivar = (ImageButton)findViewById(R.id.imgBotonDepositos);


		txtDepositos = (TextView) findViewById(R.id.txtDepositos);
		txtDepositosDesde = (TextView) findViewById(R.id.txtDepositosDesde);
		txtImgDepositos = (ImageView) findViewById(R.id.txtImgDepositos);
		//imgCerrar = (ImageButton) findViewById(R.id.imgCerrar);

	}

	public Boolean checkImporteDepositoVisibility(){

		if(switchDepositoActivar.getVisibility() == View.VISIBLE)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	public void setImporteDepositoVisibility(){
		if(checkImporteDepositoVisibility()){
			txtDepositos.setVisibility(View.VISIBLE);
			txtDepositosDesde.setVisibility(View.VISIBLE);
			txtImgDepositos.setVisibility(View.VISIBLE);
			textoCargosAbono.setVisibility(View.VISIBLE);
			tbImporteDeposito.setVisibility(View.VISIBLE);
		}else{
			tbImporteDeposito.resetVacio();
			switchDepositoActivar.setVisibility(View.VISIBLE);
			tbImporteDeposito.setVisibility(View.GONE);
			txtDepositos.setVisibility(View.GONE);
			textoCargosAbono.setVisibility(View.GONE);
			txtDepositosDesde.setVisibility(View.GONE);
			txtImgDepositos.setVisibility(View.GONE);
			//imgCerrar.setVisibility(View.GONE);
			tbImporteDeposito.setVisibility(View.GONE);
		}

		setImporteDepositoValue(ingresaDatosAlertasDelegate.getDetalleCuentaAlertasData());
	}

	public void setImporteDepositoValue(DetalleCuentaAlertasData detallesCuentaData){
		if(checkImporteDepositoVisibility()){
			if((detallesCuentaData.getImporteAbono() != null) && (!("".equals(detallesCuentaData.getImporteAbono())))){
				tbImporteDeposito.setAmount(Tools.formatAmountFromServerToImport(detallesCuentaData.getImporteAbono()));
			}else{
				tbImporteDeposito.resetVacio();
			}
		}else{
			tbImporteDeposito.resetVacio();
		}
	}
	
	/**
	 * Escala los elementos de la pantalla para la resoluci�n actual.
	 */
	private void scaleToCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(continuarButton);
	}
	/**
	 * Carga el componente lista datos con los elementos necesarios. 
	 */
	private void cargarComponenteListaDatos() {
		ArrayList<Object> datos = ingresaDatosAlertasDelegate.cargarElementosListaDatos();

		ArrayList<Object> registro = (ArrayList<Object>) datos.get(0);
		
		LinearLayout.LayoutParams layoutParams = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		
		listaDatos = new ListaDatosComboViewController(this, layoutParams, parentManager);

		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.showLista();

		//LinearLayout listaDatosLayout = (LinearLayout)findViewById(R.id.layoutListaDatos);
		//listaDatosLayout.addView(listaDatos);

		tipoCuenta = (TextView) findViewById(R.id.nombre_cuenta);
		tipoCuenta.setText(registro.get(0).toString());

		numeroCuenta = (TextView) findViewById(R.id.numero_cuenta);
		numeroCuenta.setText(registro.get(1).toString());

		registro = (ArrayList<Object>) datos.get(1);

		numeroCelular = (TextView) findViewById(R.id.num_telefono);
		numeroCelular.setText(registro.get(1).toString());


		listaDatos.setListener(this);
	}
	
	
	public void cargaListaDatos(DetalleCuentaAlertasData detallesCuentaData){
		tbAlias.setText(detallesCuentaData.getAlias());
		tbImporteCargo.setAmount(Tools.formatAmountFromServerToImport(detallesCuentaData.getImporteCargo()));
//		if((detallesCuentaData.getImporteAbono() != null) && (!("".equals(detallesCuentaData.getImporteAbono())))){
//			String importe = Tools.formatAmountFromServerToImport(detallesCuentaData.getImporteAbono());
//			tbImporteDeposito.setAmount(importe);
//
//			if(Integer.valueOf(importe)>0){
//				switchDepositoActivar.setChecked(true);
//			}
//		}else{
//			tbImporteDeposito.resetVacio();
//		}

		if((detallesCuentaData.getImporteAbono() != null) && (!("".equals(detallesCuentaData.getImporteAbono())))){
			if(Integer.valueOf(Tools.formatAmountFromServerToImport(detallesCuentaData.getImporteAbono()))>0)
				switchDepositoActivar.setVisibility(View.GONE);
		}
		setImporteDepositoVisibility();

		for (Object obj : seleccionHorizontal.getComponentes()) {
			Compania comp = (Compania) obj;

			if (comp.getNombre().equals(detallesCuentaData.getTelefonicaDescr())) {
				seleccionHorizontal.setSelectedItem(comp);
				break;
			}
		}
	}
	
	public void setSeleccionHorizontal(String companyName) {
		for (Object obj : seleccionHorizontal.getComponentes()) {
			Compania comp = (Compania) obj;

			if (comp.getNombre().equals(companyName)) {
				seleccionHorizontal.setSelectedItem(comp);
				break;
			}
		}
	}
	
	
	/**
	 * Carga el componente seleccion horizontal con los elementos necesarios.
	 */
	private void cargarComponenteSeleccionHorizontal() {
		//De  la vista principal
		LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		ArrayList<Object> companias = ingresaDatosAlertasDelegate.cargarCompaniasSeleccionHorizontal();
		seleccionHorizontal = new SeleccionHorizontalViewController(this, params, companias, getIngresaDatosAlertasDelegate(), false);

		LinearLayout seleccionHorizontalLayout = (LinearLayout)findViewById(R.id.layoutSeleccionHorizontal);
		seleccionHorizontalLayout.addView(seleccionHorizontal);
	}


	@Override
	public void onUserInteraction(){
		/*if(seleccionHorizontal!=null){
			seleccionHorizontal.setBandera(true);
		}*/
		baseViewController.onUserInteraction();
	}
	
	
	public void processNetworkResponse(int operationId, ServerResponseImpl response) {
		ingresaDatosAlertasDelegate.analyzeResponse(operationId, response);
	}
		
	
	@Override
	protected void onPause() {
		TimerController timerContr = TimerController.getInstance();
		if(timerContr.isCerrandoSesion()){
			parentManager.setCurrentActivityApp(this);
			parentManager.setActivityChanging(false);
			baseViewController.goBack(this, parentManager);
			if(Server.ALLOW_LOG) Log.d("Bebug>>", ">>>>>>>>>>>> onPause");
		}
		super.onPause();
		parentManager.consumeAccionesDePausa();
	}
	
	@Override
	public void onBackPressed(){
		TrackingHelper.touchAtrasState();
		baseViewController.goBack(this, parentManager);
	}
	@Override
	protected void onStop() {
		super.onStop();
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
		ingresaDatosAlertasDelegate.setMedioEnvioSelected(adapterView.getSelectedItem().toString());
	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {
		if(Server.ALLOW_LOG) Log.d("IngresaDatosAlertasViewController", ">>> onNothingSelected");
	}
}

