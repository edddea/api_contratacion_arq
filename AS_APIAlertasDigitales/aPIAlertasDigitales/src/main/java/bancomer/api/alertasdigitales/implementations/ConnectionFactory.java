package bancomer.api.alertasdigitales.implementations;

import com.bancomer.base.SuiteApp;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.util.Hashtable;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import bancomer.api.alertasdigitales.io.ParserImpl;
import bancomer.api.alertasdigitales.io.ParserJSONImpl;
import bancomer.api.alertasdigitales.io.ParsingHandler;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.alertasdigitales.io.ServerResponseImpl;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;
import suitebancomer.aplicaciones.commservice.service.ICommService;

public class ConnectionFactory {

	private int operationId;
	private Hashtable<String, ?> params;
	private Object responseObject;
	private ParametersTO parameters;
	private boolean isJson;

	public ConnectionFactory(int operationId, Hashtable<String, ?> params,
							 boolean isJson, Object responseObject) {
		super();
		this.operationId = operationId;
		this.params = params;
		this.responseObject = responseObject;
		this.isJson = isJson;
	}

	public IResponseService processConnectionWithParams() throws Exception {

		final Integer opId = Integer.valueOf(operationId);
		parameters = new ParametersTO();
		parameters.setSimulation(ServerCommons.SIMULATION);
		if (!ServerCommons.SIMULATION) {
			parameters.setProduction(!ServerCommons.DEVELOPMENT);
			parameters.setDevelopment(ServerCommons.DEVELOPMENT);
		}
		parameters.setOperationId(opId.intValue());
		parameters.setParameters(params.clone());
		parameters.setJson(isJson);
		IResponseService resultado = new ResponseServiceImpl();
		ICommService serverproxy = new CommServiceProxy(SuiteApp.appContext);
		try {
			resultado = serverproxy.request(parameters,
					responseObject == null ? new Object().getClass()
							: responseObject.getClass());
			// ConsultaEstatusMantenimientoData
			// estatusM=(ConsultaEstatusMantenimientoData)resultado.getObjResponse();
		} catch (UnsupportedEncodingException e) {
			throw new Exception(e);
		} catch (ClientProtocolException e) {
			throw new Exception(e);
		} catch (IllegalStateException e) {
			throw new Exception(e);
		} catch (IOException e) {
			throw new Exception(e);
		} catch (JSONException e) {
			throw new Exception(e);
		}

		return resultado;

	}

	public ServerResponseImpl parserConnection(IResponseService resultado,
										   ParsingHandler handler) throws Exception {
		ServerResponseImpl response=null;
		ParsingHandler handlerP=null;
		if (ApiConstants.OPERATION_SUCCESSFUL == resultado.getStatus()) {
			if(resultado.getObjResponse() instanceof ParsingHandler){
				handlerP= (ParsingHandler) resultado.getObjResponse();
			}
			if (parameters.isJson()) {
				if (handler != null) {
					if(Server.OP_DETALLE_MENSAJE_ENV_ALERTAS== this.operationId && ServerCommons.SIMULATION && resultado.getResponseString()!=null){
						String estado;
						if(Server.controlCiclos == 0){
							estado = "ER";
							Server.controlCiclos++;
						}else if(Server.controlCiclos == 1){
							estado = "EN";
							Server.controlCiclos++;
						}else{
							estado = "CA";
							Server.controlCiclos = 0;
						}
						resultado.setResponseString(resultado.getResponseString().replaceAll("estadoReplace","estado"));
					}
					ParserJSONImpl parser = new ParserJSONImpl(
							resultado.getResponseString());
					try {
						handlerP.process(parser);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						throw new Exception(e);
					} catch (bancomer.api.common.io.ParsingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						throw new Exception(e);
					}
				}
				response = new ServerResponseImpl(resultado.getStatus(),
						resultado.getMessageCode(), resultado.getMessageText(), handlerP);
			} else {
				// Que pasa si no es JSON la respuesta
				Reader reader = null;
				try {
					if (handler != null) {
						reader = new StringReader(resultado.getResponseString());
						ParserImpl parser = new ParserImpl(reader);
						if(handlerP==null){
							Class<?> clazz = handler.getClass();
							Constructor<?> ctor = clazz.getConstructor();
							handlerP = (ParsingHandler) ctor.newInstance();
						}
						response= new ServerResponseImpl(handlerP);
						response.process(parser);
					}else{
						response= new ServerResponseImpl(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),null);
					}
				}catch(Exception e){
					throw new Exception(e);

				}finally {
					if (reader != null) {
						try {
							reader.close();
						} catch (Throwable ignored) {
							throw new Exception(ignored);

						}
					}
				}
			}

		}else{
			try {
				response= new ServerResponseImpl(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),null);
			} catch (Exception e) {
				// TODO: handle exception
				throw new Exception(e);
			}

		}

		return response;
	}

}
