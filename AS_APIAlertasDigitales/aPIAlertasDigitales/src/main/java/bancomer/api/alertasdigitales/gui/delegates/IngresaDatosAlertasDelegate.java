package bancomer.api.alertasdigitales.gui.delegates;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;
import android.widget.ArrayAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import bancomer.api.alertasdigitales.R;
import bancomer.api.alertasdigitales.commons.Autenticacion;
import bancomer.api.alertasdigitales.commons.Constants;
import bancomer.api.alertasdigitales.commons.Constants.OperacionesAlertas;
import bancomer.api.alertasdigitales.gui.controllers.AlertasDigitalesViewController;
import bancomer.api.alertasdigitales.gui.controllers.IngresaDatosAlertaViewController;
import bancomer.api.alertasdigitales.gui.controllers.ResultadoAlertasDigitalesViewController;
import bancomer.api.alertasdigitales.implementations.BaseDelegateImpl;
import bancomer.api.alertasdigitales.implementations.InitAlertasDigitales;
import bancomer.api.alertasdigitales.io.ParsingHandler;
import bancomer.api.alertasdigitales.io.Server;
import bancomer.api.alertasdigitales.models.ConfirmaAlertas;
import bancomer.api.alertasdigitales.models.Cuenta;
import bancomer.api.alertasdigitales.models.DetalleCuentaAlertasData;
import bancomer.api.alertasdigitales.models.EstadoData;
import bancomer.api.common.commons.Constants.Operacion;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.commons.ServerConstants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.common.model.CatalogoVersionado;
import bancomer.api.common.model.Compania;

public class IngresaDatosAlertasDelegate extends BaseDelegateImpl{

	/**
	 * @return the detalleCuentaAleartasData
	 */
	public DetalleCuentaAlertasData getDetalleCuentaAlertasData() {
		return detalleCuentaAlertasData;
	}

	/**
	 * @param detalleCuentaAlertasData the detalleCuentaAlertasData to set
	 */
	public void setDetalleCuentaAlertasData(
			DetalleCuentaAlertasData detalleCuentaAlertasData) {
		this.detalleCuentaAlertasData = detalleCuentaAlertasData;
		setMedioEnvioSelected(detalleCuentaAlertasData.getDispositivoEnvioFav());
	}

	/**
	 * Identificador del delegado.
	 */
	public static final long INGRESA_DATOS_ALERTA_DELEGATE_ID = 0xb35981680eb5fe19L;

	private IngresaDatosAlertaViewController ingresaDatosAlertaViewController;
	
	private DetalleCuentaAlertasData detalleCuentaAlertasData;

	private AlertasDigitalesViewController alertasviewcontroller;
	
	private ResultadoAlertasDigitalesViewController resultadoAlertasDigitalesViewController;

	public ResultadoAlertasDigitalesViewController getResultadoAlertasDigitalesViewController() {
		return resultadoAlertasDigitalesViewController;
	}

	public void setResultadoAlertasDigitalesViewController(
			ResultadoAlertasDigitalesViewController resultadoAlertasDigitalesViewController) {
		this.resultadoAlertasDigitalesViewController = resultadoAlertasDigitalesViewController;
	}

	private Cuenta cuentaSeleccionada;
	
	private Operacion operacion = bancomer.api.common.commons.Constants.Operacion.altaModServicioAlertas;
	
	private EstadoData estadoData;
	
	public EstadoData getEstadoData() {
		return estadoData;
	}

	public void setEstadoData(EstadoData estadoData) {
		this.estadoData = estadoData;
	}

	private String tipoOperacion;

	private Dialog mProgressDialog;

	private ConfirmaAlertas confirmaAlertas;

	private OperacionesAlertas operacionAlertasAMostrar;
	
	public String getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(String tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public IngresaDatosAlertaViewController getIngresaDatosAlertaViewController() {
		return ingresaDatosAlertaViewController;
	}

	public void setIngresaDatosAlertaViewController(
			IngresaDatosAlertaViewController ingresaDatosAlertaViewController) {
		this.ingresaDatosAlertaViewController = ingresaDatosAlertaViewController;
	}

	public HashMap<String, String> getMediosEnvio() {
		return mediosEnvio;
	}

	public IngresaDatosAlertasDelegate setMediosEnvio(HashMap<String, String> mediosEnvio) {
		this.mediosEnvio = mediosEnvio;
		return this;
	}



	/**
	 * Array asociativo de medios de envio
	 */
	private HashMap<String,String> mediosEnvio;
	public String medioEnvioSms = "SMS";

	public String medioEnvioPush = "Notificación";

	public String medioEnvioSelected = "";

	public String getMedioEnvioSelected() {
		return medioEnvioSelected;
	}

	public void setMedioEnvioSelected(String medioEnvioSelected) {
		this.medioEnvioSelected = medioEnvioSelected;
	}

	public String getMedioEnvioPush() {
		return medioEnvioPush;
	}

	public String getMedioEnvioSMS() {
		return medioEnvioSms;
	}

	private void inicializaMediosEnvio() {
		mediosEnvio = new HashMap<String, String>();
		mediosEnvio.put(medioEnvioSms, "101");
		mediosEnvio.put(medioEnvioPush, "999");
	}

	public IngresaDatosAlertasDelegate() {
		inicializaMediosEnvio();
	}

	// #region IngresarDatos.
	/**
	 * Carga la lista de compañías para el componente seleccion horizontal.
	 * 
	 * @return La lista de compañías en el orden requerido.
	 */
	public ArrayList<Object> cargarCompaniasSeleccionHorizontal() {
		// Compania companiaArray[] = new Compania[4];

		CatalogoVersionado catalogoDineroMovil = InitAlertasDigitales.getInstance().getConsultaSend().getCatalogoTelefonicas();
		if(Server.ALLOW_LOG) Log.i("Catalogo telefonicas en modulo", "Catalogo " + catalogoDineroMovil + " " + catalogoDineroMovil.getObjetos());
		// CatalogoVersionado catalogoCompanias =
		// Session.getInstance(SuiteApp.appContext).getCatalogoTiempoAire();
		Vector<Object> vectorCompanias = catalogoDineroMovil.getObjetos();
		int companiasSize = vectorCompanias.size();
		ArrayList<Object> listaCompanias = new ArrayList<Object>(companiasSize);
		Compania currentCompania = null;
		for (int i = 0; i < companiasSize; i++) {
			currentCompania = (Compania) vectorCompanias.get(i);
			listaCompanias.add(currentCompania);
			currentCompania = null;
		}
		vectorCompanias = null;
		return listaCompanias;

		// return new ArrayList<Object>(Arrays.asList(companiaArray));
	}

	/**
	 * Carga los elementos a mostrar en el componente ListaDatos de la pantalla
	 * Ingresar Datos.
	 * 
	 * @return Los elementos a mostrar.
	 */
	public ArrayList<Object> cargarElementosListaDatos() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila1 = new ArrayList<String>();
		ArrayList<String> fila2 = new ArrayList<String>();
		ArrayList<Object> fila3 = new ArrayList<Object>();

		fila1.add(this.getCuentaSeleccionada().getNombreTipoCta());
		fila1.add(Tools.hideAccountNumber(this.getCuentaSeleccionada().getNumCuenta()));
		tabla.add(fila1);

		fila2.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_ingresar_datos_numero_celular));
		fila2.add(getDetalleCuentaAlertasData().getNumeroCelular());
		tabla.add(fila2);

		fila3.add(ingresaDatosAlertaViewController.getString(R.string.bmovil_contratacion_ingresar_datos_medio_envio));
		if(!detalleCuentaAlertasData.getSwitchMetodoEnvio().equalsIgnoreCase("S")){
			fila3.add(medioEnvioPush);
			tabla.add(fila3);
		}else {
			ArrayList<String> spinnerArray = ordenarMediosEnvio();
			ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(ingresaDatosAlertaViewController, android.R.layout.simple_spinner_item, spinnerArray);
			spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			fila3.add(spinnerArrayAdapter);
			tabla.add(fila3);
		}

		return tabla;
	}

	private ArrayList<String> ordenarMediosEnvio(){
		ArrayList<String> spinnerArray = new ArrayList<String>();
		String keySelected = "";

		if((detalleCuentaAlertasData!=null)&&(detalleCuentaAlertasData.getDispositivoEnvioFav()!=null)){
			if(mediosEnvio.containsValue(detalleCuentaAlertasData.getDispositivoEnvioFav())){
				Iterator<String> it = mediosEnvio.keySet().iterator();
				while (it.hasNext()) {
					String key = it.next();
					if(mediosEnvio.get(key).equals(detalleCuentaAlertasData.getDispositivoEnvioFav())) keySelected = key;
				}
			}
		}

		Iterator<String> it = mediosEnvio.keySet().iterator();
		while (it.hasNext()) {
			spinnerArray.add(it.next());
		}

		if(!keySelected.equals("")){
			String value = mediosEnvio.get(keySelected);
			spinnerArray.remove(keySelected);
			spinnerArray.add(0,keySelected);
		}

		return spinnerArray;
	}
	
	public ArrayList<Object> cargarListaResultados() {
		ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila = new ArrayList<String>();

		
    	String celular = getDetalleCuentaAlertasData().getNumeroCelular();
		
		fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_resultados_datos_cuenta));
		fila.add(Tools.hideAccountNumber(confirmaAlertas.getNumCuenta()));
		tabla.add(fila);
		fila = new ArrayList<String>();

		fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_resultados_datos_numero_celular));
		fila.add(celular);
		tabla.add(fila);
		fila = new ArrayList<String>();

		fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_resultados_datos_medio_envio));
		fila.add(getMetodoEnvio());
		tabla.add(fila);
		fila = new ArrayList<String>();
		
		fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_resultados_datos_compania_celular));
		fila.add(confirmaAlertas.getCompCelular());
		tabla.add(fila);
		fila = new ArrayList<String>();

		fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_resultados_datos_alias));
		fila.add(confirmaAlertas.getAliasCuenta());
		tabla.add(fila);
		fila = new ArrayList<String>();

		fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_resultados_datos_importe_cargo));
		fila.add(Tools.formatAmountToScreen(confirmaAlertas.getImpCargo()));
		tabla.add(fila);
		fila = new ArrayList<String>();


		if(Double.parseDouble(confirmaAlertas.getImpDeposito())>=50) {
			fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_resultados_datos_importe_deposito));
			fila.add(Tools.formatAmountToScreen(confirmaAlertas.getImpDeposito()));
			tabla.add(fila);
			fila = new ArrayList<String>();
		}
		
		fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_resultados_datos_accion));
		fila.add(confirmaAlertas.getAccionAlerta());
		tabla.add(fila);
		fila = new ArrayList<String>();
		
		fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_resultados_datos_fecha));
		fila.add(confirmaAlertas.getFechaAccion());
		tabla.add(fila);
		fila = new ArrayList<String>();

		return tabla;
	}

	public Cuenta getCuentaSeleccionada() {
		return cuentaSeleccionada;
	}

	public void setCuentaSeleccionada(Cuenta cuentaSeleccionada) {
		this.cuentaSeleccionada = cuentaSeleccionada;
	}
	
	//*******************************************CONFIRMACION*****************************************//

		@Override
		public String getTextoTituloResultado() {
			// TODO Auto-generated method stub
			return super.getTextoTituloResultado();
		}

		@Override
		public String getTextoAyudaNIP() {
			// TODO Auto-generated method stub
			return super.getTextoAyudaNIP();
		}

		@Override
		public String getTextoAyudaCVV() {
			// TODO Auto-generated method stub
			return super.getTextoAyudaCVV();
		}

		@Override
		public boolean mostrarContrasenia() {
			boolean mostrar = false;
			Perfil perfil = InitAlertasDigitales.getInstance().getConsultaSend().getPerfil();
			
			mostrar = Autenticacion.getInstance().mostrarContrasena(this.operacion, perfil, Tools.getDoubleAmountFromServerString("15000"));
			return mostrar;
		}

		@Override
		public TipoOtpAutenticacion tokenAMostrar() {
			TipoOtpAutenticacion value = null;
			Perfil perfil = InitAlertasDigitales.getInstance().getConsultaSend().getPerfil();
			
			value = Autenticacion.getInstance().tokenAMostrar(this.operacion, perfil, Tools.getDoubleAmountFromServerString("15000") );
			
			return value;
		}

		@Override
		public boolean mostrarNIP() {
			boolean mostrar = false;
			Perfil perfil = InitAlertasDigitales.getInstance().getConsultaSend().getPerfil();
			
			mostrar = Autenticacion.getInstance().mostrarNIP(this.operacion, perfil, Tools.getDoubleAmountFromServerString("15000") );
			
			return mostrar;
		}

		@Override
		public boolean mostrarCVV() {
			boolean mostrar = false;
			Perfil perfil = InitAlertasDigitales.getInstance().getConsultaSend().getPerfil();
			
			mostrar = Autenticacion.getInstance().mostrarCVV(this.operacion, perfil, Tools.getDoubleAmountFromServerString("15000") );
			
			return mostrar;
		}

		@Override
		public ArrayList<Object> getDatosTablaConfirmacion() {
			ArrayList<Object> tabla = new ArrayList<Object>();
			ArrayList<String> fila;
			
			fila = new ArrayList<String>();
			fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_confirmar_datos_cuenta));
			fila.add(Tools.hideAccountNumber(confirmaAlertas.getNumCuenta()));
			tabla.add(fila);

			fila = new ArrayList<String>();
			fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_confirmar_datos_numero_celular));
			fila.add(detalleCuentaAlertasData.getNumeroCelular());
			tabla.add(fila);
			
			fila = new ArrayList<String>();
			fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_confirmar_datos_medio_envio));
			//fila.add(ingresaDatosAlertaViewController.getListaDatos().getSpinner(0).getSelectedItem().toString());
			fila.add(getMetodoEnvio());
			tabla.add(fila);
			
			fila = new ArrayList<String>();
			fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_confirmar_datos_compania_celular));
			fila.add(confirmaAlertas.getCompCelular());
			tabla.add(fila);

			fila = new ArrayList<String>();
			fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_confirmar_datos_alias));
			fila.add(confirmaAlertas.getAliasCuenta());
			tabla.add(fila);

			fila = new ArrayList<String>();
			fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_confirmar_datos_importe_cargo));
			fila.add(Tools.formatAmountToScreen(confirmaAlertas.getImpCargo()));
			tabla.add(fila);
			if(Double.parseDouble(confirmaAlertas.getImpDeposito())>=50) {
				fila = new ArrayList<String>();
				fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_confirmar_datos_importe_deposito));
				fila.add(Tools.formatAmountToScreen(confirmaAlertas.getImpDeposito()));
				tabla.add(fila);
			}
			
			fila = new ArrayList<String>();
			fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_confirmar_datos_accion));			
			fila.add(confirmaAlertas.getAccionAlerta());
			tabla.add(fila);
			
			fila = new ArrayList<String>();
			fila.add(ingresaDatosAlertaViewController.getString(R.string.api_alertas_confirmar_datos_fecha));		
			fila.add(confirmaAlertas.getFechaAccion());
			tabla.add(fila);
			
			return tabla;
		}

		@Override
	public void realizaOperacionConfirmacion(
				Activity confirmacionViewController, String contrasena, String nip,
				String asm, String tarjeta) {
				altaModificacionAlertasDigitales(contrasena,nip,asm,tarjeta);
		}

		@Override
		public int getTituloConfirmacion() {
			return R.string.bmovil_menu_alertasdigitales_titulo;
		}

		@Override
		public int getIconoConfirmacion() {
			return R.drawable.api_alertas_digitales_an_ic_alertas;
		}
		
		//*******************************************CONFIRMACION*****************************************//
	
	public void redirecciona(Compania company, String alias, /*String celular, String confirmarCelular,*/ String importeCargo, String importeDeposito){
		if(cuentaSeleccionada.getIndAlertas().equals("S")){
    			if(datosModificados()){
        			// Modificacion
    	    		Boolean valido = validaCampos(company, alias, importeCargo, importeDeposito);
        			operacion = bancomer.api.common.commons.Constants.Operacion.altaModServicioAlertas;
        			operacionAlertasAMostrar = Constants.OperacionesAlertas.modificacion;
        			if (valido) {
        				// ConfirmaAlertas
        				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                    	ConfirmaAlertas confirmaAlertas = new ConfirmaAlertas(getCuentaSeleccionada().getTipoCuenta(), 
                    			getCuentaSeleccionada().getNumCuenta(), 
                    			alias, getMetodoEnvio(), company.getNombre(), "", importeCargo, importeDeposito,
                    			operacionAlertasAMostrar.value, sdf.format(new Date()));
                    	this.confirmaAlertas = confirmaAlertas;
        				InitAlertasDigitales.getInstance().showConfirmacionAlertasDigitales(this);
        			}
        		} else {
					InitAlertasDigitales.getInstance().getBaseViewController()
							.showInformationAlert(ingresaDatosAlertaViewController,
									Constants.CERO_MODIFICACIONES);        			
        		}
		}
    	
	}

	
	/**
	 * @return the confirmaAlertas
	 */
	public ConfirmaAlertas getConfirmaAlertas() {
		return confirmaAlertas;
	}

	/**
	 * @param confirmaAlertas the confirmaAlertas to set
	 */
	public void setConfirmaAlertas(ConfirmaAlertas confirmaAlertas) {
		this.confirmaAlertas = confirmaAlertas;
	}

	public Boolean validaMetodoEnvioModificado(){
		Boolean ret = true;
		if(detalleCuentaAlertasData.getSwitchMetodoEnvio().equalsIgnoreCase("S")){
			ret = detalleCuentaAlertasData.getDispositivoEnvioFav().equals(mediosEnvio.get(getMetodoEnvio()));
		}
		return ret;
	}

	public boolean datosModificados(){
		return (!(detalleCuentaAlertasData.getTelefonicaDescr().equals(ingresaDatosAlertaViewController.getCompany().getNombre()) &&
				validaMetodoEnvioModificado() &&
				detalleCuentaAlertasData.getAlias().equals(ingresaDatosAlertaViewController.getAlias()) && 
				Tools.formatAmountFromServerToImport(detalleCuentaAlertasData.getImporteCargo()).equals(ingresaDatosAlertaViewController.getTbImporteCargo().getAmount()) &&
				Tools.formatAmountFromServerToImport(detalleCuentaAlertasData.getImporteAbono()).equals(ingresaDatosAlertaViewController.getTbImporteDeposito().getAmount())));
	}
	
	public void altaModificacionAlertasDigitales(String contrasena, String nip,	String asm, String tarjeta) {
		if(Server.ALLOW_LOG) Log.d(">> CGI", "Relleno petición Alta Modificacion alertas digitales");
		
		ingresaDatosAlertaViewController.getBaseViewController().muestraIndicadorActividad(ingresaDatosAlertaViewController,
				ingresaDatosAlertaViewController.getString(R.string.alert_operation),
				ingresaDatosAlertaViewController.getString(R.string.alert_connecting));
		
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		int operacion = Server.OP_ALTA_MOD_SERVICIO_ALERTAS;
		
    	String alias = ingresaDatosAlertaViewController.getTbAlias().getText().toString();
    	String importeCargo = Tools.formatAmountForServer(ingresaDatosAlertaViewController.getTbImporteCargo().getAmount());

		// CGI-Niko: Va a ceros en caso de que no sea visible el campo
    	String importeDeposito = "0000";
		if(ingresaDatosAlertaViewController.checkImporteDepositoVisibility()){
			// CGI-Niko: Va como siempre en caso contrario
			importeDeposito = Tools.formatAmountForServer(ingresaDatosAlertaViewController.getTbImporteDeposito().getAmount());
		}

    	Compania company = (Compania) ingresaDatosAlertaViewController.getSeleccionHorizontal().getSelectedItem();
		
    	String numeroUsurio = InitAlertasDigitales.getInstance().getConsultaSend().getUsername();

		String mEnvio = getMetodoEnvio();//ingresaDatosAlertaViewController.getListaDatos().getSpinner(0).getSelectedItem().toString();

		paramTable.put(ServerConstants.OPERACION,"altaModServicioAlertas");
    	paramTable.put(ServerConstants.ALIAS, alias);
    	paramTable.put(ServerConstants.ASUNTO, cuentaSeleccionada.getNumCuenta());
		paramTable.put(ServerConstants.NUMERO_CELULAR, numeroUsurio);
		paramTable.put(ServerConstants.CELULAR_NUEVO, ((null == detalleCuentaAlertasData) || (detalleCuentaAlertasData.getNumeroCelular() == null)) ? "" : detalleCuentaAlertasData.getNumeroCelular());
		paramTable.put(ServerConstants.TELEFONICA_DESCR, company.getNombre());
		paramTable.put(ServerConstants.TELEFONICA_COD, company.getClave());
		paramTable.put(ServerConstants.IMPORTE_CARGO, importeCargo);
		paramTable.put(ServerConstants.IMPORTE_ABONO, importeDeposito);
		paramTable.put(ServerConstants.DISPOSITIVO_ENVIO_FAV, mediosEnvio.get(mEnvio));
		paramTable.put(ServerConstants.CORREO_PART1, "");
		paramTable.put(ServerConstants.CORREO_PART2, "");
		paramTable.put(ServerConstants.CORREO_PART3, "");
		
		paramTable.put(ServerConstants.CODIGO_OTP, (null == asm) ? "" : asm);
		paramTable.put(ServerConstants.CODIGO_CVV2, "");
		paramTable.put(ServerConstants.CVE_ACCESO, (null == contrasena) ? "" : contrasena);
		
		paramTable.put(ServerConstants.TARJETA_5DIG, (null == tarjeta) ? "" : tarjeta);
		paramTable.put(ServerConstants.CODIGO_NIP, (null == nip) ? "" : nip);
		
		paramTable.put(ServerConstants.CADENA_AUTENTICACION, Autenticacion.getInstance().getCadenaAutenticacion(bancomer.api.common.commons.Constants.Operacion.altaModServicioAlertas,InitAlertasDigitales.getInstance().getConsultaSend().getPerfil(), 1200.00));
		paramTable.put(ServerConstants.TIPO_OPERACION, "M");
		
		
		doNetworkOperation(operacion, paramTable,true,new EstadoData(), ingresaDatosAlertaViewController.getBaseViewController());
	}

	/* (non-Javadoc)
	 * @see bancomer.api.alertasdigitales.implementations.BaseDelegateImpl#doNetworkOperation(int, java.util.Hashtable, bancomer.api.common.gui.controllers.BaseViewController)
	 */
	public void doNetworkOperation(int operationId,
			Hashtable<String, ?> params,final boolean isJson,
								   final ParsingHandler handler, BaseViewController caller) {
		// TODO Auto-generated method stub
		InitAlertasDigitales.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params,isJson,handler, caller, true);
	}
	
	

	
	/* (non-Javadoc)
	 * @see bancomer.api.alertasdigitales.implementations.BaseDelegateImpl#analyzeResponse(int, bancomer.api.common.io.ServerResponse)
	 */
	@Override
	public void analyzeResponse(int operationId, ServerResponse response) {


		// TODO Auto-generated method stub
		super.analyzeResponse(operationId, response);
		if (operationId == Server.OP_DETALLE_CUENTA_ALERTAS) {
			if(Server.ALLOW_LOG) Log.d(">> CGI", "Entra por consulta Detalle cuentas alertas");
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta otros creditos >> Success");
				detalleCuentaAlertasData = (DetalleCuentaAlertasData) response
						.getResponse();
				ingresaDatosAlertaViewController.cargaListaDatos(detalleCuentaAlertasData);
				
			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta Detalle otros creditos >> Warning");
				
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(ingresaDatosAlertaViewController,response.getMessageText());

			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta Detalle otros creditos >> Error");
				
				InitAlertasDigitales.getInstance().getBaseViewController().ocultaIndicadorActividad();
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(ingresaDatosAlertaViewController,response.getMessageText() , (new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta otros creditos >> EX#2 Error en el servicio");
						ingresaDatosAlertaViewController.getBaseViewController().goBack(ingresaDatosAlertaViewController, ingresaDatosAlertaViewController.getParentManager());
						ingresaDatosAlertaViewController.irAtras();
				
					}
					}));
				
			}
		} else if (operationId == Server.OP_ALTA_MOD_SERVICIO_ALERTAS) {
			if(Server.ALLOW_LOG) Log.d(">> CGI", "Entra por alta modificacion alertas");
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "alta modificacion alertas >> Success");
				EstadoData estadoAlertasData = (EstadoData) response.getResponse();
				//InitAlertasDigitales.getInstance().showResultadoAlertasDigitales(estadoAlertasData);
				ingresaDatosAlertaViewController.finish();

			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "alta modificacion alertas >> Warning");
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(ingresaDatosAlertaViewController,response.getMessageText());
			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "alta modificacion alertas >> Error");
				InitAlertasDigitales.getInstance().getBaseViewController().ocultaIndicadorActividad();
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(ingresaDatosAlertaViewController,response.getMessageText() , (new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if(Server.ALLOW_LOG) Log.d(">> CGI", "alta modificacion alertas >> EX#2 Error en el servicio");
						ingresaDatosAlertaViewController.getBaseViewController().goBack(ingresaDatosAlertaViewController, ingresaDatosAlertaViewController.getParentManager());
						ingresaDatosAlertaViewController.irAtras();
				
					}
					}));
				
			}
		} else if (operationId == Server.OP_BAJA_SERVICIO_ALERTAS) {
			if(Server.ALLOW_LOG) Log.d(">> CGI", "Entra por baja alertas");
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "baja alertas >> Success");
				EstadoData estadoAlertasData = (EstadoData) response.getResponse();
				//InitAlertasDigitales.getInstance().showResultadoAlertasDigitales(estadoAlertasData);
				ingresaDatosAlertaViewController.finish();
			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "baja alertas >> Warning");
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(ingresaDatosAlertaViewController,response.getMessageText());
			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "baja alertas >> Error");

				InitAlertasDigitales.getInstance().getBaseViewController().ocultaIndicadorActividad();
				InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(ingresaDatosAlertaViewController,response.getMessageText() , (new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						if(Server.ALLOW_LOG) Log.d(">> CGI", "baja alertas >> EX#2 Error en el servicio");
						ingresaDatosAlertaViewController.getBaseViewController().goBack(ingresaDatosAlertaViewController, ingresaDatosAlertaViewController.getParentManager());
						ingresaDatosAlertaViewController.irAtras();
				
					}
					}));
				
			}
		}
	}
	
	public Boolean validaCampos(Compania compania, String alias, String importeCargo, String importeDeposito){
		Boolean valido = true;
		String error = "";

			try{

				int impCargo = Integer.parseInt(importeCargo);
				int impDeposito = Integer.parseInt(importeDeposito);
			
				if(Tools.isEmptyOrNull(alias.trim())){
					valido = false;
					error = Constants.ALIAS_NO_VALIDO;
				} else if (null == ingresaDatosAlertaViewController) {
					if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
							"No se registro un ViewController.");
					valido = false;
					error = Constants.NUMERO_CELULAR_NO_VALIDO;
				} else if (null == compania) {
					if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), Constants.COMPANIA_CELULAR_NO_VALIDO);
					valido = false;
					error = Constants.COMPANIA_CELULAR_NO_VALIDO;
				} else if (impCargo < 50){
					if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
							Constants.IMPORTE_CARGO_NO_VALIDO_LT50);
					valido = false;
					error = Constants.IMPORTE_CARGO_NO_VALIDO;
				}else if (impCargo > 1000){
					if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
							Constants.IMPORTE_CARGO_NO_VALIDO_GT1000);
					valido = false;
					error = Constants.IMPORTE_CARGO_NO_VALIDO;
				} else if ((getIngresaDatosAlertaViewController().checkImporteDepositoVisibility() && (impDeposito < 50))) {
					if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
							Constants.IMPORTE_DEPOSITO_NO_VALIDO);
					valido = false;
					error = Constants.IMPORTE_DEPOSITO_NO_VALIDO;
				}
						
			}catch(Exception e){
				valido = false;
				error = Constants.PARSE_ERROR;
				if(Server.ALLOW_LOG) Log.d("OpcionesTransferViewController","Entraste a transferencias otros bancos Indicator");
			}

		if(!valido){
			InitAlertasDigitales.getInstance().getBaseViewController().showInformationAlert(ingresaDatosAlertaViewController, error);
		}
		
		return valido;
	}

	public String getNumeroCuentaParaRegistroOperacion(){
		String cuenta = cuentaSeleccionada.getNumCuenta();
		return cuenta.substring(cuenta.length() - 5);
	}

	public String getMetodoEnvio(){
		String metodoEnvio = "";
		if(detalleCuentaAlertasData.getSwitchMetodoEnvio().equalsIgnoreCase("S")){
//			metodoEnvio = ingresaDatosAlertaViewController.getListaDatos().getSpinner(0).getSelectedItem().toString();
			metodoEnvio = getMedioEnvioSelected();
		}else{
			metodoEnvio = medioEnvioPush;
		}
		return metodoEnvio;
	}
}
