package bancomer.api.alertasdigitales.commons;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.model.OperacionAutenticacion;
import bancomer.api.common.model.PerfilAutenticacion;

/**
 * Authentication rules bridge to consult which ones of the authentication artifacts must be shown on each operation.
 */
public class Autenticacion {
	
	private static Autenticacion theInstance = null;
	private double limiteOperacion;
	private ArrayList<OperacionAutenticacion> basico;
	private ArrayList<OperacionAutenticacion> recortado;
	private ArrayList<OperacionAutenticacion> avanzado;
	private String version;
	
	public static Autenticacion getInstance() {
		if(null == theInstance){
			theInstance = new Autenticacion(1200.0);
		}
		return theInstance;
	}
	
	private Autenticacion(double limitePorOperacion) {
		this.basico = new ArrayList<OperacionAutenticacion>();
		this.recortado = new ArrayList<OperacionAutenticacion>();
		this.avanzado = new ArrayList<OperacionAutenticacion>();
		this.limiteOperacion = limitePorOperacion;
	}
	
	public boolean isOperable(Constants.Operacion tipoOperacion, Constants.Perfil perfil){
		PerfilAutenticacion perfilActual = leePerfil(tipoOperacion, perfil);
		return perfilActual.isOperar();
	}
	
	public boolean isVisible(Constants.Operacion tipoOperacion, Constants.Perfil perfil){
		PerfilAutenticacion perfilActual = leePerfil(tipoOperacion, perfil);
		return perfilActual.isVisible();
	}
	
	public boolean mostrarContrasena(Constants.Operacion tipoOperacion, Constants.Perfil perfil){
		PerfilAutenticacion perfilActual = leePerfil(tipoOperacion, perfil);
		return perfilActual.isContrasena();
	}
	
	
	public boolean mostrarOperacion(Constants.Operacion tipoOperacion, Constants.Perfil perfil){
		PerfilAutenticacion perfilActual = leePerfil(tipoOperacion, perfil);
		return perfilActual.isVisible();
	}
	
	public boolean operarOperacion(Constants.Operacion tipoOperacion, Constants.Perfil perfil){
		PerfilAutenticacion perfilActual = leePerfil(tipoOperacion, perfil);
		return perfilActual.isOperar();
	}
			
	public boolean mostrarContrasena(Constants.Operacion tipoOperacion, Constants.Perfil perfil, double importe){
		
		if(perfil == Constants.Perfil.avanzado){
			if(importe <= limiteOperacion){
				return mostrarContrasena(tipoOperacion, Constants.Perfil.basico);
			}
		}
		return mostrarContrasena(tipoOperacion, perfil);
	}
	
	public boolean mostrarNIP(Constants.Operacion tipoOperacion, Constants.Perfil perfil){
		PerfilAutenticacion perfilActual = leePerfil(tipoOperacion, perfil);
		return perfilActual.isNip();
	}
	
	public boolean mostrarNIP(Constants.Operacion tipoOperacion, Constants.Perfil perfil, double importe){
		if(perfil == Constants.Perfil.avanzado){
			if(importe <= limiteOperacion){
				return mostrarNIP(tipoOperacion, Constants.Perfil.basico);
			}
		}
		return mostrarNIP(tipoOperacion, perfil);
	}
	
	public TipoOtpAutenticacion tokenAMostrar(Constants.Operacion tipoOperacion, Constants.Perfil perfil){
		PerfilAutenticacion perfilActual = leePerfil(tipoOperacion, perfil);
		return perfilActual.getToken();
	}
	
	public TipoOtpAutenticacion tokenAMostrar(Constants.Operacion tipoOperacion, Constants.Perfil perfil, double importe){
		if(perfil == Constants.Perfil.avanzado){
			if(importe <= limiteOperacion){
				return tokenAMostrar(tipoOperacion, Constants.Perfil.basico);
			}
		}
		return tokenAMostrar(tipoOperacion, perfil);
	}
	
	public boolean mostrarCVV(Constants.Operacion tipoOperacion, Constants.Perfil perfil){
		PerfilAutenticacion perfilActual = leePerfil(tipoOperacion, perfil);
		return perfilActual.isCvv2();
	}
	
	public boolean mostrarCVV(Constants.Operacion tipoOperacion, Constants.Perfil perfil, double importe){
		if(perfil == Constants.Perfil.avanzado){
			if(importe <= limiteOperacion){
				return mostrarCVV(tipoOperacion, Constants.Perfil.basico);
			}
		}
		return mostrarCVV(tipoOperacion, perfil);
	}
	
	public String getCadenaAutenticacion (Constants.Operacion tipoOperacion, Constants.Perfil perfil){
		PerfilAutenticacion perfilActual = leePerfil(tipoOperacion, perfil);
		String cadenaAutentica = (perfilActual.isContrasena()?"1":"0")+
				(perfilActual.isNip()?"1":"0")+
				(perfilActual.getToken()==TipoOtpAutenticacion.codigo?"1":"0")+
				(perfilActual.getToken()==TipoOtpAutenticacion.registro?"1":"0")+
				(perfilActual.isCvv2()?"1":"0");
		return cadenaAutentica;
	}
	
	public String getCadenaAutenticacion (Constants.Operacion tipoOperacion, Constants.Perfil perfil, double importe){
		if(perfil == Constants.Perfil.avanzado){
			if(importe <= limiteOperacion){
				return getCadenaAutenticacion(tipoOperacion, Constants.Perfil.basico);
			}
		}
		return getCadenaAutenticacion(tipoOperacion, perfil);
	}
	
	
	public boolean validaRegistro(Constants.Operacion tipoOperacion, Constants.Perfil perfil){
		PerfilAutenticacion perfilActual = leePerfil(tipoOperacion, perfil);
		return perfilActual.isRegistro();
	}
	
	private PerfilAutenticacion leePerfil(Constants.Operacion tipoOperacion, Constants.Perfil perfil){
		ArrayList<OperacionAutenticacion> operaciones = null;
		
		switch(perfil){
			case basico:
				operaciones = this.basico;
				break;
			case avanzado:
				operaciones = this.avanzado;
				break;
			case recortado:
				operaciones = this.recortado;
				break;
			default:
				return null;
		}
		
		
		for(OperacionAutenticacion operacionActual : operaciones){
			if(operacionActual.getOperacion().equals(tipoOperacion)){
				return operacionActual.getCredenciales();
			}
		}
		
		return null;
	}
	
	public double getLimiteOperacion() {
		return limiteOperacion;
	}

	public void setLimiteOperacion(double limiteOperacion) {
		this.limiteOperacion = limiteOperacion;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
		
	public ArrayList<OperacionAutenticacion> getBasico() {
		return basico;
	}

	public void setBasico(ArrayList<OperacionAutenticacion> basico) {
		this.basico = basico;
	}

	public ArrayList<OperacionAutenticacion> getRecortado() {
		return recortado;
	}

	public void setRecortado(ArrayList<OperacionAutenticacion> recortado) {
		this.recortado = recortado;
	}

	public ArrayList<OperacionAutenticacion> getAvanzado() {
		return avanzado;
	}

	public void setAvanzado(ArrayList<OperacionAutenticacion> avanzado) {
		this.avanzado = avanzado;
	}
}

