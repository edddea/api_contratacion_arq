package suitebancomer.classes.gui.views;

import java.util.ArrayList;

public class FiltroListaDelegate
{

	ArrayList<Object> listaOriginal;
	FiltroListaViewControllerCR filtroLlistaviewcontroller;
	ArrayList<Object> nuevaLista;
	
	
	public FiltroListaViewControllerCR getFiltroListaviewcontroller() {
		return filtroLlistaviewcontroller;
	}

	public void setFiltroListaviewcontroller(final FiltroListaViewControllerCR listaviewcontroller) {
		this.filtroLlistaviewcontroller = listaviewcontroller;
	}

	public ArrayList<Object> getListaOriginal() {
		return listaOriginal;
	}

	public void guardaCopiaOriginal(final ArrayList<Object> listaOriginal)
	{
		this.listaOriginal = listaOriginal;
	}
	
	@SuppressWarnings("unchecked")
	public void realizaBusqueda(final String str)
	{
		if (!"".equals(str)) {
			final String strt = str.toLowerCase();
			nuevaLista = new ArrayList<Object>();
			for (int i = 0; i < listaOriginal.size(); i++) {
				final ArrayList<Object> registro = (ArrayList<Object>) listaOriginal.get(i);
				for (int j = 1; j < registro.size(); j++) {
					String reg = ((String)registro.get(j)).toLowerCase();
					reg = reg.replace(",", "");
					if (reg.contains(strt)) {
						if (!nuevaLista.contains(registro)) {
							nuevaLista.add(registro);
						}
					}
				}
			}
		}
		else {
			nuevaLista = listaOriginal;
		}
	}
	
	public void actualizaCampos()
	{
		filtroLlistaviewcontroller.getListaSeleccion().setLista(nuevaLista);
		filtroLlistaviewcontroller.getListaSeleccion().cargarTabla();		
	}

}
