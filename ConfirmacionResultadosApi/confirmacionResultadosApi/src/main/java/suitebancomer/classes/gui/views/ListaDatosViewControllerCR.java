package suitebancomer.classes.gui.views;

import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import suitebancomer.aplicaciones.resultados.R;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomercoms.classes.common.GuiTools;

public class ListaDatosViewControllerCR extends LinearLayout{

	//private BaseViewsControllerCommons parentManager;
	private final ListView listview;
	private ArrayList<Object> lista;
	private int numeroCeldas;
	private int numeroFilas;
	private LinearLayout.LayoutParams params;

	private final LayoutInflater inflater;
	private String title;
	private TextView titulo;
	private final List<String> tablaDatosAColorear;
	//O3
	private final List<String> tablaTitulosAColorear;
	private final List<String> tablaDatosACambiarTamaño;
	private final List<String> nuevaTablaDatosACambiarTamaño;
	
	public ListaDatosViewControllerCR(final Context context,final LinearLayout.LayoutParams layoutParams)
	{
		super(context);
	    inflater = LayoutInflater.from(context);
		final LinearLayout viewLayout = (LinearLayout) inflater.inflate(R.layout.layout_lista_datos_view, this, true);
	    viewLayout.setLayoutParams(layoutParams);
		//this.parentManager = parentManager;

		listview = (ListView) findViewById(R.id.layout_lista_Datos);
		tablaDatosAColorear = new ArrayList<String>();
		tablaDatosAColorear.add(SuiteAppCRApi.appContext.getString(R.string.transferir_interbancario_tabla_resultados_importe));
		tablaDatosAColorear.add(SuiteAppCRApi.appContext.getString(R.string.transferir_dineromovil_resultados_codigo));
		tablaDatosAColorear.add(SuiteAppCRApi.appContext.getString(R.string.administrar_cambiarpassword_folioOperacionLabel));
	
		tablaDatosAColorear.add(SuiteAppCRApi.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_liquidada));
		tablaDatosAColorear.add(SuiteAppCRApi.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_devuelta));
		tablaDatosAColorear.add(SuiteAppCRApi.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_proceso));
		tablaDatosAColorear.add(SuiteAppCRApi.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_validacion));
		tablaDatosAColorear.add(SuiteAppCRApi.appContext.getString(R.string.bmovil_consultar_interbancario_estatus_enviada));
		//O3
		tablaTitulosAColorear = new ArrayList<String>();
		tablaTitulosAColorear.add(SuiteAppCRApi.appContext.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_titulo_datodelenvio));
		tablaTitulosAColorear.add(SuiteAppCRApi.appContext.getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_titulo_datodeenvio));
		tablaTitulosAColorear.add(SuiteAppCRApi.appContext.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_titulo_datosuc));
		
		tablaDatosACambiarTamaño = new ArrayList<String>();
		//tablaDatosACambiarTamaño.add(SuiteApp.getInstance().getString(R.string.bmovil_consultar_obtenercomprobante_detalles_datos_tipoop));
		/******/
		nuevaTablaDatosACambiarTamaño = new ArrayList<String>();
		nuevaTablaDatosACambiarTamaño.add(SuiteAppCRApi.appContext.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_deporecibido));
		nuevaTablaDatosACambiarTamaño.add(SuiteAppCRApi.appContext.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_titulo_datosuc));
		nuevaTablaDatosACambiarTamaño.add(SuiteAppCRApi.appContext.getString(R.string.bmovil_consultar_depositosrecibidos_detalles_datos_referencianum));

	}

	public void setNumeroFilas(final int numeroFilas) {
		this.numeroFilas = numeroFilas;
	}
	
	//public void setTitulo(int resTitle) {
	//	this.title = parentManager.getCurrentViewControllerApp().getString(resTitle);
	//}
	
	public void setTitulo(final String title) {
		this.title = title;
	}

	public void showLista()
	{
		if (title != null) {
			titulo = (TextView) findViewById(R.id.lista_datos_titulo);
			titulo.setText(title);
			titulo.setVisibility(View.VISIBLE);
			
			GuiTools.getCurrent().scale(titulo, true);
		}
		final LlenarListadoAdapterCR adapter = new LlenarListadoAdapterCR();
		listview.setAdapter(adapter);
		listview.setEnabled(false);

		final ListAdapter listAdapter = listview.getAdapter();
		int totalHeight = 0;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			final View listItem = listAdapter.getView(i, null, listview);
			listItem.measure(0, 0);
			totalHeight = listItem.getMeasuredHeight()*numeroFilas;
		}

		final ViewGroup.LayoutParams params = listview.getLayoutParams();
		params.height = totalHeight	+ (listview.getDividerHeight() * (listview.getCount() - 1));
		listview.setLayoutParams(params);
		listview.requestLayout();

	}
	
	public ArrayList<Object> getLista() {
		return lista;
	}

	public void setLista(final ArrayList<Object> lista) {
		this.lista = lista;
	}

	public int getNumeroCeldas() {
		return numeroCeldas;
	}

	public void setNumeroCeldas(final int numeroCeldas) {
		this.numeroCeldas = numeroCeldas;
	}

	private class LlenarListadoAdapterCR extends BaseAdapter {

        @Override
        public int getCount() {
            return lista.size();
        }

        @Override
        public Object getItem(final int position) {
            return lista.get(position);
        }

		@Override
        public View getView(final int position, final View convertView, final ViewGroup parent) {
			final GuiTools guiTools = GuiTools.getCurrent();
			
			View convertVie;
            if (convertView == null) {
                convertVie = inflater.inflate(R.layout.list_item_productos, null);
            }
			else{
				convertVie=convertView;
			}



            if (getNumeroCeldas() == 2) {
	            params = new LayoutParams(0,LayoutParams.WRAP_CONTENT,0.47f);
				final ArrayList<Object> registro = (ArrayList<Object>) lista.get(position);

				final TextView celda1 = (TextView)convertVie.findViewById(R.id.lista_celda_1);
	            celda1.setText((String) registro.get(0));
	            celda1.setLayoutParams(params);
	            celda1.setGravity(Gravity.LEFT);
	            celda1.setMaxLines(1);
	            //celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
				celda1.setSingleLine(true);
	            celda1.setSelected(true);
	            /*****O3*/
	            //celda1.setEllipsize(null);
	            if (tablaTitulosAColorear.contains((String) registro.get(0))) {
		            celda1.setTextColor(getResources().getColor(R.color.primer_azul));
				}
	            if (nuevaTablaDatosACambiarTamaño.contains((String) registro.get(0))) 
	            {//celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10.0f);
	            	celda1.setEllipsize(null);
	            	celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 12.0f);
	            }
	            else
		            celda1.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
	            guiTools.tryScaleText(celda1);
				
	            
//	            ((TextView)convertVie.findViewById(R.id.lista_celda_1)).setText((String) registro.get(0));
//	            ((TextView)convertVie.findViewById(R.id.lista_celda_1)).setLayoutParams(params);
//	            ((TextView)convertVie.findViewById(R.id.lista_celda_1)).setGravity(Gravity.LEFT);
	            
	            params = new LayoutParams(0,LayoutParams.WRAP_CONTENT,0.53f);

				final TextView celda2 = (TextView)convertVie.findViewById(R.id.lista_celda_2);
	            celda2.setText((String) registro.get(1));
	            if (tablaDatosAColorear.contains((String) registro.get(0))) {
	            	celda2.setTextColor(getResources().getColor(R.color.tercer_azul));
	            	
				}else if(tablaDatosAColorear.contains((String) registro.get(1))){
					if(registro.get(1).toString().equalsIgnoreCase(SuiteAppCRApi.getInstance().getString(R.string.bmovil_consultar_interbancario_estatus_liquidada))){
	            		celda2.setTextColor(getResources().getColor(R.color.verde));
	            	}else if(registro.get(1).toString().equalsIgnoreCase(SuiteAppCRApi.getInstance().getString(R.string.bmovil_consultar_interbancario_estatus_devuelta))){
	            		celda2.setTextColor(getResources().getColor(R.color.magenta));
	            	}else if(registro.get(1).toString().equalsIgnoreCase(SuiteAppCRApi.getInstance().getString(R.string.bmovil_consultar_interbancario_estatus_enviada))){
	            		celda2.setTextColor(getResources().getColor(R.color.tercer_azul));
	            	}else if(registro.get(1).toString().equalsIgnoreCase(SuiteAppCRApi.getInstance().getString(R.string.bmovil_consultar_interbancario_estatus_proceso))){
	            		celda2.setTextColor(getResources().getColor(R.color.naranja));
	            	}else if(registro.get(1).toString().equalsIgnoreCase(SuiteAppCRApi.getInstance().getString(R.string.bmovil_consultar_interbancario_estatus_validacion))){
	            		celda2.setTextColor(getResources().getColor(R.color.naranja));
	            	}

	            }
	            
	            celda2.setLayoutParams(params);
	            celda2.setGravity(Gravity.RIGHT);
	            celda2.setMaxLines(1);
	            //celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
	            if (tablaDatosACambiarTamaño.contains((String) registro.get(0))) {
	            	celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 10.0f);
	            	
				}else{
		            celda2.setTextSize(TypedValue.COMPLEX_UNIT_PX, 13.0f);
				}
				celda2.setSingleLine(true);
	            celda2.setSelected(true);
				guiTools.tryScaleText(celda2);
				
				
				
	            
//	            ((TextView)convertVie.findViewById(R.id.lista_celda_2)).setText((String) registro.get(1));
//	            ((TextView)convertVie.findViewById(R.id.lista_celda_2)).setLayoutParams(params);
//	            ((TextView)convertVie.findViewById(R.id.lista_celda_2)).setGravity(Gravity.RIGHT);
	            
	            ((TextView)convertVie.findViewById(R.id.lista_celda_3)).setVisibility(View.GONE);
	            ((TextView)convertVie.findViewById(R.id.lista_celda_4)).setVisibility(View.GONE);
	            ((ImageButton)convertVie.findViewById(R.id.lista_celda_check)).setVisibility(View.GONE);
	            
			}
            Log.d("Filas", "fila "+position);
            
            return convertVie;
        }

		@Override
		public long getItemId(final int position) {
			// TODO Auto-generated method stub
			return 0;
		}
    }

}
