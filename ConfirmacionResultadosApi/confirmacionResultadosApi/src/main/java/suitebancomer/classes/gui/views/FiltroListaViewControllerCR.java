package suitebancomer.classes.gui.views;

import java.util.ArrayList;

import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.views.ListaSeleccionViewControllerCR;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;
import suitebancomer.aplicaciones.resultados.R;

public class FiltroListaViewControllerCR extends LinearLayout {

	public static ArrayList<Object> listaOriginal;
	private EditText txtFiltro;
	private FiltroListaDelegate delegate;
	ListaSeleccionViewControllerCR listaSeleccion;

	public FiltroListaViewControllerCR(final Context context, final LinearLayout.LayoutParams layoutParams) {
		super(context);
		final LayoutInflater inflater = LayoutInflater.from(context);
		final  LinearLayout viewLayout = (LinearLayout) inflater.inflate(R.layout.layout_filtro_lista_view, this, true);
	    viewLayout.setLayoutParams(layoutParams);
	    
	    delegate = new FiltroListaDelegate();
	    delegate.setFiltroListaviewcontroller(this);
		txtFiltro = (EditText) findViewById(R.id.txtFiltro);

		txtFiltro.addTextChangedListener(new TextWatcher(){
	        public void afterTextChanged(final Editable s) {
	        	delegate.realizaBusqueda(s.toString());
	        	delegate.actualizaCampos();
	        }
	        public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after){}
	        public void onTextChanged(final CharSequence s, final int start, final int before, final int count){}
	    });
		
		// Resize to screen.
		scaleForCurrentScreen();
	}
	
	public void init()
	{
		delegate.guardaCopiaOriginal(listaSeleccion.getLista());
		if (delegate.getListaOriginal() == null) {
			txtFiltro.setEnabled(false);
		}
	}

	public ListaSeleccionViewControllerCR getListaSeleccion() {
		return listaSeleccion;
	}

	public void setListaSeleccion(final ListaSeleccionViewControllerCR listaSeleccion) {
		this.listaSeleccion = listaSeleccion;
	}
	
	public void setListaOriginal(final ArrayList<Object> listaOriginal){
		delegate.guardaCopiaOriginal(listaOriginal);
	}

	private void scaleForCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
		
		guiTools.scale(findViewById(R.id.layoutRoot));
		guiTools.scale(txtFiltro, true);
		guiTools.scale(findViewById(R.id.btnBorrar));
		
	}

}
