/**
 * 
 */
package suitebancomer.aplicaciones.resultados.presenters;



import java.util.ArrayList;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.interactors.ResultadosAutenticacionInteractor;
import suitebancomer.aplicaciones.resultados.interactors.ResultadosAutenticacionInteractorImpl;
import suitebancomer.aplicaciones.resultados.listeners.OnResultadosFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IResultadosAutenticacionServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ResultadosViewTo;
import suitebancomer.aplicaciones.resultados.views.ResultadosAutenticacionView;
import android.content.Intent;

/**
 * @author amgonzalez
 *
 */
public class ResultadosAutenticacionPresenterImpl implements ResultadosAutenticacionPresenter, OnResultadosFinishedListener {
	
	private final ResultadosAutenticacionView cview;
	private final ResultadosAutenticacionInteractor interactor;

	public ResultadosAutenticacionPresenterImpl(final ResultadosAutenticacionView view, final Intent intent){
		this.cview = view;
		this.interactor = new ResultadosAutenticacionInteractorImpl(
				getServiceProxy(intent)); 
	}
	
	private IResultadosAutenticacionServiceProxy getServiceProxy(final Intent intent){
		final IResultadosAutenticacionServiceProxy p = (IResultadosAutenticacionServiceProxy)
				SuiteAppCRApi.getInstance().getProxy();
		return p;
	}

	@Override 
	public void onResume() {
        //mainView.showProgress();
    }
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener#onError()
	 */
	@Override
	public void onError() {
		//View.setUsernameError();
		// en el view username.setError(getString(R.string.username_error));
	}
	
	@Override
	public void getListaDatos() {
		interactor.getListaDatos(this);
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener#onFinished()
	 */
	@Override
	public void onFinishedListaDatos( final ResultadosViewTo lists ) {
		cview.setListaDatos((ArrayList<Object>)lists.getListaDatos());
	}

	@Override
	public void getShowFields() {
		interactor.getShowFields(this);
	}

	@Override
	public void onFinishedValidShowFields(final ResultadosViewTo fields) {
		cview.setResultadosViewTo(fields);
		cview.configuraPantalla();
	}

	@Override
	public void onFinishedConfirmacionOperacion(){
		cview.onSuccess();
	}
	
	@Override
	public void getOnPrepareOptionsMenu(){
		interactor.getOnPrepareOptionsMenu(this);
	}

	@Override
	public void onFinishedGetPrepareOptionsMenu(final ResultadosViewTo to) {
		cview.setOpcionesMenu(to);
	}
	
	public boolean optionsItemSelected(final int menuId){
		return interactor.optionsItemSelected(menuId);
	}
	
	public void botonMenuClick(){
		interactor.botonMenuClick();
		cview.onSuccess();
	}
	
	/*
	 * (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.presenters.ResultadosPresenter#setParamStateParentManager()
	 */
	@Override
	public void setParamStateParentManager(){
		interactor.setParamStateParentManager();
	}
	
	/**
	 * @return the interactor
	 */
	public final ResultadosAutenticacionInteractor getInteractor() {
		return interactor;
	}
		
}
