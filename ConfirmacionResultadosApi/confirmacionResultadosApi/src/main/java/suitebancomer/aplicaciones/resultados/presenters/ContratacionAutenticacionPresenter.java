/**
 * 
 */
package suitebancomer.aplicaciones.resultados.presenters;

import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;

/**
 * @author amgonzalez
 *
 */
public interface ContratacionAutenticacionPresenter {

	void show();
	void onResume();

	void limpiarCampos();
	void getListaDatos();
	void getShowFields();
	
	void confirmarClick(final ConfirmacionViewTo viewVo);
	void consultarTerminosDeUso();
}
