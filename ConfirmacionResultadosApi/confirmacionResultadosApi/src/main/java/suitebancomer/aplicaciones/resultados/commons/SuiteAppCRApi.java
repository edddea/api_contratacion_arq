/**
 * 
 */
package suitebancomer.aplicaciones.resultados.commons;

import java.util.Timer;

import suitebancomer.aplicaciones.resultados.proxys.IServiceProxy;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;
import android.content.Context;
import android.util.Log;

import com.bancomer.base.SuiteApp;
import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.base.callback.CallBackSession;

/**
 * @author lbermejo
 *
 */
public class SuiteAppCRApi extends SuiteApp {
	
	private final String TAG = getClass().getName();
	
	private static SuiteAppCRApi instance;
	//private static Context appContext;
	private BaseViewControllerCommons baseViews;
	private BaseViewsControllerCommons baseControllersViews;
	private static CallBackSession callBackSession;

	private static CallBackBConnect callBackBConnect;

	private SuiteApp parentSuiteApp;
	/**
	 * The local session validity timer.
	 */
	private Timer appSessionTimer = null;

	IServiceProxy proxy;
	
	//protected BaseViewControllerCommons intentToReturn;
	//protected BaseViewControllerCommons rootViewController;
	//protected BaseViewControllerCommons currentViewControllerApp;
	//private Class<? extends BaseViewControllerCommons> baseViewsControllerApp;

	public IServiceProxy getProxy() {
		return proxy;
	}

	public void setProxy(final IServiceProxy proxy) {
		this.proxy = proxy;
	}

	//private SuiteAppCRApi(){}
	
	//TODO 
	//setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
	
	/* (non-Javadoc)
	 * @see com.bancomer.base.SuiteApp#onCreate()
	 */
	public void onCreate(final Context context) {
		super.onCreate(context);
		isSubAppRunning = false;
		appContext = context;
		instance = this;

		Log.d(TAG, " : onCreate>context>"+ context);
	}
	
	public static SuiteAppCRApi getInstance(){
		return instance;
	}
	
	/**
	 * 
	 * @return Context
	 */
	public static Context getSubAppContext(){
		return appContext;
	}
	
	/**
	 * @return the baseViews
	 */
	public final BaseViewControllerCommons getBaseViews() {
		return baseViews;
	}

	/**
	 * @param baseViews the baseViews to set
	 */
	public final void setBaseViews(final BaseViewControllerCommons baseViews) {
		this.baseViews = baseViews;
	}

	/**
	 * @return the baseControllersViews
	 */
	public final BaseViewsControllerCommons getBaseControllersViews() {
		return baseControllersViews;
	}

	/**
	 * @param baseControllersViews the baseControllersViews to set
	 */
	public final void setBaseControllersViews(
			final BaseViewsControllerCommons baseControllersViews) {
		this.baseControllersViews = baseControllersViews;
	}

	/**
	 * @return the parentSuiteApp
	 */
	public final SuiteApp getParentSuiteApp() {
		return parentSuiteApp;
	}

	/**
	 * @param parentSuiteApp the parentSuiteApp to set
	 */
	public final void setParentSuiteApp(final SuiteApp parentSuiteApp) {
		this.parentSuiteApp = parentSuiteApp;
	}

	/**
	 * @return the appSessionTimer
	 */
	public final Timer getAppSessionTimer() {
		return appSessionTimer;
	}

	/**
	 * @param appSessionTimer the appSessionTimer to set
	 */
	public final void setAppSessionTimer(final Timer appSessionTimer) {
		this.appSessionTimer = appSessionTimer;
	}

	public static int getResourceId(final String nombre, final String tipo){
		return appContext.getResources().getIdentifier(nombre,tipo, appContext.getPackageName());
	}

	public static CallBackSession getCallBackSession() {
		return callBackSession;
	}

	public static void setCallBackSession(final CallBackSession callBackSession) {
		SuiteAppCRApi.callBackSession = callBackSession;
	}

	public static CallBackBConnect getCallBackBConnect() {
		return callBackBConnect;
	}

	public static void setCallBackBConnect(final CallBackBConnect callBackBConnect) {
		SuiteAppCRApi.callBackBConnect = callBackBConnect;
	}

}
