/**
 * 
 */
package suitebancomer.aplicaciones.resultados.to;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author lbermejo
 *
 */
public class ResultadosViewTo extends ParamTo implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2847030906395658544L;
	
	private String titulo;
	private String texto;
	private String instrucciones;
	private String tituloTextoEspecial;
	private String textoEspecial;
	
	private Integer imagenBotonResultados;
	private Integer colorTituloResultado;
	private Integer opcionesMenu;

	private Boolean frecOpOk;
	private Boolean goBack;
	private Boolean isConsultaInterbancario;
	private Boolean isMenuBtnEnable;
	
	private ArrayList<Object> listaDatos;
	private ArrayList<Object> listaClave;
	
	/**
	 * @return the titulo
	 */
	public final String getTitulo() {
		return titulo;
	}
	/**
	 * @param titulo the titulo to set
	 */
	public final void setTitulo(final String titulo) {
		this.titulo = titulo;
	}
	/**
	 * @return the texto
	 */
	public final String getTexto() {
		return texto;
	}
	/**
	 * @param texto the texto to set
	 */
	public final void setTexto(final String texto) {
		this.texto = texto;
	}
	/**
	 * @return the instrucciones
	 */
	public final String getInstrucciones() {
		return instrucciones;
	}
	/**
	 * @param instrucciones the instrucciones to set
	 */
	public final void setInstrucciones(final String instrucciones) {
		this.instrucciones = instrucciones;
	}
	/**
	 * @return the tituloTextoEspecial
	 */
	public final String getTituloTextoEspecial() {
		return tituloTextoEspecial;
	}
	/**
	 * @param tituloTextoEspecial the tituloTextoEspecial to set
	 */
	public final void setTituloTextoEspecial(final String tituloTextoEspecial) {
		this.tituloTextoEspecial = tituloTextoEspecial;
	}
	/**
	 * @return the textoEspecial
	 */
	public final String getTextoEspecial() {
		return textoEspecial;
	}
	/**
	 * @param textoEspecial the textoEspecial to set
	 */
	public final void setTextoEspecial(final String textoEspecial) {
		this.textoEspecial = textoEspecial;
	}
	/**
	 * @return the colorTituloResultado
	 */
	public final Integer getColorTituloResultado() {
		return colorTituloResultado;
	}
	/**
	 * @param colorTituloResultado the colorTituloResultado to set
	 */
	public final void setColorTituloResultado(final Integer colorTituloResultado) {
		this.colorTituloResultado = colorTituloResultado;
	}
	/**
	 * @return the isConsultaInterbancario
	 */
	public final Boolean getIsConsultaInterbancario() {
		return isConsultaInterbancario;
	}
	/**
	 * @param isConsultaInterbancario the isConsultaInterbancario to set
	 */
	public final void setIsConsultaInterbancario(final Boolean isConsultaInterbancario) {
		this.isConsultaInterbancario = isConsultaInterbancario;
	}
	/**
	 * @return the listaDatos
	 */
	public final ArrayList<Object> getListaDatos() {
		return listaDatos;
	}
	/**
	 * @param listaDatos the listaDatos to set
	 */
	public final void setListaDatos(final ArrayList<Object> listaDatos) {
		this.listaDatos = listaDatos;
	}
	/**
	 * @return the listaClave
	 */
	public final ArrayList<Object> getListaClave() {
		return listaClave;
	}
	/**
	 * @param listaClave the listaClave to set
	 */
	public final void setListaClave(final ArrayList<Object> listaClave) {
		this.listaClave = listaClave;
	}
	/**
	 * @return the opcionesMenu
	 */
	public final Integer getOpcionesMenu() {
		return opcionesMenu;
	}
	/**
	 * @param opcionesMenu the opcionesMenu to set
	 */
	public final void setOpcionesMenu(final Integer opcionesMenu) {
		this.opcionesMenu = opcionesMenu;
	}
	/**
	 * @return the frecOpOk
	 */
	public final Boolean getFrecOpOk() {
		return frecOpOk;
	}
	/**
	 * @param frecOpOk the frecOpOk to set
	 */
	public final void setFrecOpOk(final Boolean frecOpOk) {
		this.frecOpOk = frecOpOk;
	}
	/**
	 * @return the imagenBotonResultados
	 */
	public final Integer getImagenBotonResultados() {
		return imagenBotonResultados;
	}
	/**
	 * @param imagenBotonResultados the imagenBotonResultados to set
	 */
	public final void setImagenBotonResultados(final Integer imagenBotonResultados) {
		this.imagenBotonResultados = imagenBotonResultados;
	}
	/**
	 * @return the goBack
	 */
	public final Boolean getGoBack() {
		return goBack;
	}
	/**
	 * @param goBack the goBack to set
	 */
	public final void setGoBack(final Boolean goBack) {
		this.goBack = goBack;
	}
	/**
	 * @return the isMenuBtnEnable
	 */
	public final Boolean getIsMenuBtnEnable() {
		return isMenuBtnEnable;
	}
	/**
	 * @param isMenuBtnEnable the isMenuBtnEnable to set
	 */
	public final void setIsMenuBtnEnable(final Boolean isMenuBtnEnable) {
		this.isMenuBtnEnable = isMenuBtnEnable;
	}
	
}
