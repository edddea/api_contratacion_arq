/**
 * 
 */
package suitebancomer.aplicaciones.resultados.presenters;

import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;


/**
 * @author lbermejo
 *
 */
public interface ConfirmacionPresenter {

	void show();
	void onResume();

	void limpiarCampos();
	void getListaDatos();
	void getShowFields();
	
	void confirmarClick(final ConfirmacionViewTo viewVo);
	
}
