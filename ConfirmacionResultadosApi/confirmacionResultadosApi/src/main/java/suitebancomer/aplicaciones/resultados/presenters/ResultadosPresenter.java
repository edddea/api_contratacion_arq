/**
 * 
 */
package suitebancomer.aplicaciones.resultados.presenters;

import suitebancomer.aplicaciones.resultados.interactors.ResultadosInteractor;


/**
 * @author lbermejo
 *
 */
public interface ResultadosPresenter {

	void onResume();
	ResultadosInteractor getInteractor();
	//void limpiarCampos();
	void getListaDatos();
	void getShowFields();
	
	void botonMenuClick();
	void setParamStateParentManager();
	void getOnPrepareOptionsMenu();
	boolean optionsItemSelected(final int menuId);
}
