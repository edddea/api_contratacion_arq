/**
 * 
 */
package suitebancomer.aplicaciones.resultados.vo;

import java.io.Serializable;

import bancomer.api.common.commons.Constants;


/**
 * @author lbermejo
 *
 */
public class ConfirmacionViewVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1092028826946939850L;
	private String tarjeta;
	private String contrasena;
	private String nip;	
	private String asm;
	private String cvv;
	
	private Boolean showContrasena;
	private Boolean showTarjeta;
	private Boolean showNip;
	private Boolean showAsm;
	private Boolean showCvv;
	
	private String textoAyudaInsSeg;
	
	private Constants.TipoOtpAutenticacion tipoOtpAutenticacion;
	private Constants.TipoInstrumento instrumentoSeguridad;
	
	
	/**
	 * @return the tarjeta
	 */
	public final String getTarjeta() {
		return tarjeta;
	}

	/**
	 * @param tarjeta the tarjeta to set
	 */
	public final void setTarjeta(final String tarjeta) {
		this.tarjeta = tarjeta;
	}

	/**
	 * @return the contrasena
	 */
	public final String getContrasena() {
		return contrasena;
	}

	/**
	 * @param contrasena the contrasena to set
	 */
	public final void setContrasena(final String contrasena) {
		this.contrasena = contrasena;
	}

	/**
	 * @return the nip
	 */
	public final String getNip() {
		return nip;
	}

	/**
	 * @param nip the nip to set
	 */
	public final void setNip(final String nip) {
		this.nip = nip;
	}

	/**
	 * @return the asm
	 */
	public final String getAsm() {
		return asm;
	}

	/**
	 * @param asm the asm to set
	 */
	public final void setAsm(final String asm) {
		this.asm = asm;
	}

	/**
	 * @return the cvv
	 */
	public final String getCvv() {
		return cvv;
	}

	/**
	 * @param cvv the cvv to set
	 */
	public final void setCvv(final String cvv) {
		this.cvv = cvv;
	}

	/**
	 * @return the showContrasena
	 */
	public final Boolean getShowContrasena() {
		return showContrasena;
	}

	/**
	 * @param showContrasena the showContrasena to set
	 */
	public final void setShowContrasena(final Boolean showContrasena) {
		this.showContrasena = showContrasena;
	}

	/**
	 * @return the showTarjeta
	 */
	public final Boolean getShowTarjeta() {
		return showTarjeta;
	}

	/**
	 * @param showTarjeta the showTarjeta to set
	 */
	public final void setShowTarjeta(final Boolean showTarjeta) {
		this.showTarjeta = showTarjeta;
	}

	/**
	 * @return the showNip
	 */
	public final Boolean getShowNip() {
		return showNip;
	}

	/**
	 * @param showNip the showNip to set
	 */
	public final void setShowNip(final Boolean showNip) {
		this.showNip = showNip;
	}

	/**
	 * @return the showAsm
	 */
	public final Boolean getShowAsm() {
		return showAsm;
	}

	/**
	 * @param showAsm the showAsm to set
	 */
	public final void setShowAsm(final Boolean showAsm) {
		this.showAsm = showAsm;
	}

	/**
	 * @return the showCvv
	 */
	public final Boolean getShowCvv() {
		return showCvv;
	}

	/**
	 * @param showCvv the showCvv to set
	 */
	public final void setShowCvv(final Boolean showCvv) {
		this.showCvv = showCvv;
	}

	/**
	 * @return the textoAyudaInsSeg
	 */
	public final String getTextoAyudaInsSeg() {
		return textoAyudaInsSeg;
	}

	/**
	 * @param textoAyudaInsSeg the textoAyudaInsSeg to set
	 */
	public final void setTextoAyudaInsSeg(final String textoAyudaInsSeg) {
		this.textoAyudaInsSeg = textoAyudaInsSeg;
	}

	/**
	 * @return the tipoOtpAutenticacion
	 */
	public final Constants.TipoOtpAutenticacion getTipoOtpAutenticacion() {
		return tipoOtpAutenticacion;
	}

	/**
	 * @param tipoOtpAutenticacion the tipoOtpAutenticacion to set
	 */
	public final void setTipoOtpAutenticacion(
			final Constants.TipoOtpAutenticacion tipoOtpAutenticacion) {
		this.tipoOtpAutenticacion = tipoOtpAutenticacion;
	}

	/**
	 * @return the instrumentoSeguridad
	 */
	public final Constants.TipoInstrumento getInstrumentoSeguridad() {
		return instrumentoSeguridad;
	}

	/**
	 * @param instrumentoSeguridad the instrumentoSeguridad to set
	 */
	public final void setInstrumentoSeguridad(
			final Constants.TipoInstrumento instrumentoSeguridad) {
		this.instrumentoSeguridad = instrumentoSeguridad;
	}

}
