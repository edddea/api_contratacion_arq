/**
 * 
 */
package suitebancomer.aplicaciones.resultados.to;

/**
 * @author lbermejo
 *
 */
public class ConfirmacionAsignacionSpeiViewTo extends ConfirmacionViewTo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9044518090453164433L;
	
	private String otp;
	private Boolean showOtp; //TOKEN

	private ItemTo cardItem;
	private ItemTo nipItem;
	private ItemTo pwdItem;
	private ItemTo cvvItem;
	private ItemTo otpItem;	
	
	/**
	 * @return the otp
	 */
	public final String getOtp() {
		return otp;
	}

	/**
	 * @param otp the otp to set
	 */
	public final void setOtp(final String otp) {
		this.otp = otp;
	}

	/**
	 * @return the showOtp
	 */
	public final Boolean getShowOtp() {
		return showOtp;
	}

	/**
	 * @param showOtp the showOtp to set
	 */
	public final void setShowOtp(final Boolean showOtp) {
		this.showOtp = showOtp;
	}

	/**
	 * @return the cardItem
	 */
	public final ItemTo getCardItem() {
		return cardItem;
	}

	/**
	 * @param cardItem the cardItem to set
	 */
	public final void setCardItem(final ItemTo cardItem) {
		this.cardItem = cardItem;
	}

	/**
	 * @return the nipItem
	 */
	public final ItemTo getNipItem() {
		return nipItem;
	}

	/**
	 * @param nipItem the nipItem to set
	 */
	public final void setNipItem(final ItemTo nipItem) {
		this.nipItem = nipItem;
	}

	/**
	 * @return the pwdItem
	 */
	public final ItemTo getPwdItem() {
		return pwdItem;
	}

	/**
	 * @param pwdItem the pwdItem to set
	 */
	public final void setPwdItem(final ItemTo pwdItem) {
		this.pwdItem = pwdItem;
	}

	/**
	 * @return the cvvItem
	 */
	public final ItemTo getCvvItem() {
		return cvvItem;
	}

	/**
	 * @param cvvItem the cvvItem to set
	 */
	public final void setCvvItem(final ItemTo cvvItem) {
		this.cvvItem = cvvItem;
	}

	/**
	 * @return the otpItem
	 */
	public final ItemTo getOtpItem() {
		return otpItem;
	}

	/**
	 * @param otpItem the otpItem to set
	 */
	public final void setOtpItem(final ItemTo otpItem) {
		this.otpItem = otpItem;
	}
	
}
