/**
 * 
 */
package suitebancomer.aplicaciones.resultados.presenters;



import java.util.ArrayList;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.interactors.ResultadosInteractor;
import suitebancomer.aplicaciones.resultados.interactors.ResultadosInteractorImpl;
import suitebancomer.aplicaciones.resultados.listeners.OnResultadosFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IResultadosServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ResultadosViewTo;
import suitebancomer.aplicaciones.resultados.views.ResultadosView;
import android.content.Intent;

/**
 * @author lbermejo
 *
 */
public class ResultadosPresenterImpl implements ResultadosPresenter, OnResultadosFinishedListener {
	
	private final ResultadosView cview;
	private final ResultadosInteractor interactor;

	public ResultadosPresenterImpl(final ResultadosView view, final Intent intent){
		this.cview = view;
		this.interactor = new ResultadosInteractorImpl(
				getServiceProxy(intent)); 
	}
	
	private IResultadosServiceProxy getServiceProxy(final Intent intent){
		final IResultadosServiceProxy p = (IResultadosServiceProxy)
				SuiteAppCRApi.getInstance().getProxy();
		return p;
	}

	@Override 
	public void onResume() {
        //mainView.showProgress();
    }
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener#onError()
	 */
	@Override
	public void onError() {
		//View.setUsernameError();
		// en el view username.setError(getString(R.string.username_error));
	}
	
	@Override
	public void getListaDatos() {
		interactor.getListaDatos(this);
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener#onFinished()
	 */
	@Override
	public void onFinishedListaDatos( final ResultadosViewTo lists ) {
		cview.setListaDatos((ArrayList<Object>)lists.getListaDatos());
		if(lists.getListaClave()  !=null ){
			cview.setListaClave(lists.getListaClave(), lists.getIsConsultaInterbancario());	
		}
	}

	@Override
	public void getShowFields() {
		interactor.getShowFields(this);
	}

	@Override
	public void onFinishedValidShowFields(final ResultadosViewTo fields) {
		cview.setResultadosViewTo(fields);
		cview.configuraPantalla();
	}

	@Override
	public void onFinishedConfirmacionOperacion(){
		cview.onSuccess();
	}
	
	@Override
	public void getOnPrepareOptionsMenu(){
		interactor.getOnPrepareOptionsMenu(this);
	}

	@Override
	public void onFinishedGetPrepareOptionsMenu(final ResultadosViewTo to) {
		cview.setOpcionesMenu(to);
	}
	
	public boolean optionsItemSelected(final int menuId){
		return interactor.optionsItemSelected(menuId);
	}
	
	public void botonMenuClick(){
		interactor.botonMenuClick();
		cview.onSuccess();
	}
	
	/*
	 * (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.presenters.ResultadosPresenter#setParamStateParentManager()
	 */
	@Override
	public void setParamStateParentManager(){
		interactor.setParamStateParentManager();
	}
	
	/**
	 * @return the interactor
	 */
	public final ResultadosInteractor getInteractor() {
		return interactor;
	}
		
}
