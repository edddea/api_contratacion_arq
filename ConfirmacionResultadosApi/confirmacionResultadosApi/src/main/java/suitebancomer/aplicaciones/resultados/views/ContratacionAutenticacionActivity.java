/**
 * 
 */
package suitebancomer.aplicaciones.resultados.views;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.resultados.R;
import suitebancomer.aplicaciones.resultados.commons.ApiConstants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.presenters.ContratacionAutenticacionPresenter;
import suitebancomer.aplicaciones.resultados.presenters.ContratacionAutenticacionPresenterImpl;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.views.ListaDatosViewControllerCR;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

/**
 * @author amgonzalez
 *
 */
public class ContratacionAutenticacionActivity extends ApiBaseActivity 
implements ContratacionAutenticacionView, OnClickListener, TextWatcher {
	
	private LinearLayout contenedorCampoTarjeta;
	private LinearLayout contenedorPadre;
	private LinearLayout contenedorPrincipal;
	private LinearLayout contenedorContrasena;
	private LinearLayout contenedorNIP;
	private LinearLayout contenedorASM;
	private LinearLayout contenedorCVV;
	
	private TextView campoTarjeta;
	private TextView campoContrasena;
	private TextView campoNIP;
	private TextView campoASM;
	private TextView campoCVV;
	
	private EditText tarjeta;
	private EditText contrasena;
	private EditText nip;	
	private EditText asm;
	private EditText cvv;
	
	private TextView instruccionesTarjeta;
	private TextView instruccionesContrasena;
	private TextView instruccionesNIP;
	private TextView instruccionesASM;
	private TextView instruccionesCVV;
	
	private TextView aceptoTerminos;
	private TextView verTerminos;
	private CheckBox aceptoTerminosCB;

	
	private ImageButton confirmarButton;
	
	private ContratacionAutenticacionPresenter presenter;
	private ConfirmacionViewTo viewTo;
	
	private static final String VIEW = "confirmacion";
	//private String titleListaDatosView;
	private ArrayList<String> paramState;

	//AMZ
	public ArrayList<String> estados = new ArrayList<String>();
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		//setContentView(R.layout.layout_bmovil_confirmacion_act);
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_contratacion_autenticacion);
		setActivityChanging(getIntent().getBooleanExtra(ApiConstants.ACTIVITY_CHANGING, false));

		SuiteAppCRApi.getInstance().onCreate(getApplicationContext());
		SuiteApp.appContext=this;

		presenter = new ContratacionAutenticacionPresenterImpl(this,getIntent());
		paramState = getIntent().getStringArrayListExtra(ApiConstants.TRACKSTATE_PARAMETERS);
		TrackingHelper.trackState(VIEW, paramState);
		
		setTitle(getIntent());
		presenter.getListaDatos();
		
		findViews();
		configuraPantalla();
		//findViewsComp();
		scaleToScreenSize();
		moverScroll();
		

		/*
		 * 
		 * if(contratacionAutenticacionDelegate.consultaOperationsDelegate() instanceof ContratacionDelegate){ //Llegamos desde contratación
			ContratacionDelegate contratacionDelegate = (ContratacionDelegate)contratacionAutenticacionDelegate.consultaOperationsDelegate();
		
		 * 
		 * if(contratacionDelegate.isDeleteData())
				contratacionDelegate.deleteData();
		 */
		
		/*
		contrasena.addTextChangedListener(this);
		nip.addTextChangedListener(this);
		asm.addTextChangedListener(this);
		cvv.addTextChangedListener(this);
		tarjeta.addTextChangedListener(this);
		*/
	}

	@Override
	public void onBackPressed() {
		TrackingHelper.touchAtrasState();
		super.onBackPressed();
	}
	
	@Override 
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		//if(goBackToHome){
		//	SuiteApp.getInstance().getSuiteViewsController().showMenuSuite(true);
		//}
		//getParentViewsController().setCurrentActivityApp(this);
	}
	
	@Override
    protected void onPause(){
        super.onPause();
        
        //setActivityChanging(true);
	}
	
	/**
	 * Se dsactiva el boton de atras.
	 */
	@Override
	public void goBack() {
		//if(Server.ALLOW_LOG) Log.d(getClass().getSimpleName(), "Back presionado.");
		//parentViewsController.removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
		super.goBack();
	}
	
    /*
	 * (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.views.ConfirmacionView#reset()
	 * acceso desde presenter
	 */
	@Override
	public void reset() {
		this.contrasena.setText("");
		this.nip.setText("");

		if(this.asm.isEnabled())
			this.asm.setText("");

		this.cvv.setText("");
		this.tarjeta.setText("");
	}

	@Override
	public void onSuccess(Constants.Perfil clientePerfil) {

		//AMZ
		final Map<String,Object> OperacionRealizadaMap = new HashMap<String, Object>();
		//AMZ
		if (Constants.Perfil.avanzado.equals( clientePerfil ))
		{
			if(paramState.size() > 3)
			{
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","operaciones;admin+operar con token");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
			}
		}
		else
		{
			if(paramState.size() > 3)
			{
				//AMZ
				OperacionRealizadaMap.put("evento_realizada","event52");
				OperacionRealizadaMap.put("&&products","operaciones;admin+operar sin token");
				OperacionRealizadaMap.put("eVar12","operacion realizada");
				TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);

			}
		}
		reset();
		//finish();
	}

	public void limpiarCampos(){
		contrasena.setText("");
		nip.setText("");
		asm.setText("");
		cvv.setText("");
		tarjeta.setText("");
	}
	
	@Override 
	public void showMessage(final String message) {
		showInformationAlert(message);
	}
	
	/**
	 * 
	 */
	private void findViews() {
		presenter.getShowFields();
		contenedorPrincipal		= (LinearLayout)findViewById(R.id.contratacion_autenticacion_lista_datos);
		contenedorContrasena	= (LinearLayout)findViewById(R.id.campo_contratacion_autenticacion_contrasena_layout);
		contenedorNIP 			= (LinearLayout)findViewById(R.id.campo_contratacion_autenticacion_nip_layout);
		contenedorASM 			= (LinearLayout)findViewById(R.id.campo_contratacion_autenticacion_asm_layout);
		contenedorCVV 			= (LinearLayout) findViewById(R.id.campo_contratacion_autenticacion_cvv_layout);
		contenedorPadre 		= (LinearLayout)findViewById(R.id.contratacion_autenticacion_campos_layout);

		contrasena 				= (EditText)contenedorContrasena.findViewById(R.id.contratacion_autenticacion_contrasena_edittext);
		nip 					= (EditText)contenedorNIP.findViewById(R.id.contratacion_autenticacion_nip_edittext);
		asm						= (EditText)contenedorASM.findViewById(R.id.contratacion_autenticacion_asm_edittext);
		cvv						= (EditText)contenedorCVV.findViewById(R.id.contratacion_autenticacion_cvv_edittext);
		
		campoContrasena			= (TextView)contenedorContrasena.findViewById(R.id.contratacion_autenticacion_contrasena_label);
		campoNIP				= (TextView)contenedorNIP.findViewById(R.id.contratacion_autenticacion_nip_label);
		campoASM				= (TextView)contenedorASM.findViewById(R.id.contratacion_autenticacion_asm_label);
		campoCVV				= (TextView)contenedorCVV.findViewById(R.id.contratacion_autenticacion_cvv_label);
		
		instruccionesContrasena	= (TextView)contenedorContrasena.findViewById(R.id.contratacion_autenticacion_contrasena_instrucciones_label);
		instruccionesNIP		= (TextView)contenedorNIP.findViewById(R.id.contratacion_autenticacion_nip_instrucciones_label);
		instruccionesASM		= (TextView)contenedorASM.findViewById(R.id.contratacion_autenticacion_asm_instrucciones_label);
		instruccionesCVV		= (TextView)contenedorCVV.findViewById(R.id.contratacion_autenticacion_cvv_instrucciones_label);
		
		confirmarButton 		= (ImageButton)findViewById(R.id.contratacion_autenticacion_confirmar_button);
		
		aceptoTerminos     		= (TextView)findViewById(R.id.acepto_terminos_label);
		verTerminos				= (TextView)findViewById(R.id.acepto_terminos_link);
		aceptoTerminosCB		= (CheckBox)findViewById(R.id.acepto_terminos_checkbox);
		
		if (viewTo.getShowCvv() && (!viewTo.getShowNip())) {
			// El contenedor de campo tarjeta deberÌa ir entre el instrumento de seguridad y el cvv
			contenedorCampoTarjeta  = (LinearLayout)findViewById(R.id.campo_confirmacion_campotarjeta_sin_nip_layout);
			tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_sin_nip_edittext);
			campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_sin_nip_label);
			instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_sin_nip_instrucciones_label);
		} else {
			// El contenedor de campo tarjeta deberÌa ir encima del nip
			contenedorCampoTarjeta  = (LinearLayout)findViewById(R.id.campo_confirmacion_campotarjeta_layout);
			tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
			campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
			instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);
		}

	}

	/**
	 * 
	 */
	private void scaleToScreenSize() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(contenedorPrincipal);
		//contenedorPadre = (LinearLayout)findViewById(R.id.contratacion_autenticacion_campos_layout);
		guiTools.scale(contenedorPadre);
		
		guiTools.scale(contenedorContrasena);
		guiTools.scale(contenedorNIP);
		guiTools.scale(contenedorASM);
		guiTools.scale(contenedorCVV);
		
		guiTools.scale(contrasena, true);
		guiTools.scale(nip, true);
		guiTools.scale(asm, true);
		guiTools.scale(cvv, true);
		
		guiTools.scale(campoContrasena, true);
		guiTools.scale(campoNIP, true);
		guiTools.scale(campoASM, true);
		guiTools.scale(campoCVV, true);
		
		guiTools.scale(instruccionesContrasena, true);
		guiTools.scale(instruccionesNIP, true);
		guiTools.scale(instruccionesASM, true);
		guiTools.scale(instruccionesCVV, true);
		
		guiTools.scale(findViewById(R.id.aceptar_terminos_layout));
		guiTools.scale(aceptoTerminos, true);
		guiTools.scale(verTerminos, true);
		guiTools.scale(aceptoTerminosCB);
		
		guiTools.scale(contenedorCampoTarjeta);
		guiTools.scale(tarjeta, true);
		guiTools.scale(campoTarjeta, true);
		guiTools.scale(instruccionesTarjeta, true);
		
		guiTools.scale(confirmarButton);
		
	}
	
	/**
	 * 
	 * @param intent
	 */
	private void setTitle(final Intent intent ){
		
		final int titleText, iconResource;
		titleText = intent.getIntExtra(ApiConstants.TITLE_TEXT, 0) ;
		iconResource = intent.getIntExtra(ApiConstants.ICON_RESOURCE, 0);
		setTitle(titleText, iconResource);
	}
	
	/**
	 * 
	 * acceso desde presenter
	 */
	@Override
	public void setListaDatos(final ArrayList<Object> datos) {

		final LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		final ListaDatosViewControllerCR listaDatos = new ListaDatosViewControllerCR(this, params);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.setTitulo(getString(R.string.confirmation_subtitulo));
		listaDatos.showLista();
		final LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.contratacion_autenticacion_lista_datos);
		layoutListaDatos.addView(listaDatos);
		
	}
	
	/**
	 * 
	 */
	private void configuraPantalla() {
		showFields();
		//LinearLayout contenedorPadre = (LinearLayout)findViewById(R.id.contratacion_autenticacion_campos_layout);
		
		if (contenedorContrasena.getVisibility() == View.GONE &&
			contenedorNIP.getVisibility() == View.GONE &&
			contenedorASM.getVisibility() == View.GONE &&
			contenedorCVV.getVisibility() == View.GONE &&
			contenedorCampoTarjeta.getVisibility() == View.GONE) {

			if(ServerCommons.ALLOW_LOG) Log.d("CAActivi-ContraAut", "todo es GONE ");
			contenedorPadre.setBackgroundColor(0);
		}

		contenedorPadre.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		final float camposHeight = contenedorPadre.getMeasuredHeight();

		final LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.contratacion_autenticacion_lista_datos);
		layoutListaDatos.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		final float listaHeight = layoutListaDatos.getMeasuredHeight();

		final ViewGroup contenido = (ViewGroup)this.findViewById(android.R.id.content).getRootView();//findViewById(android.R.id.content);
		contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		final float contentHeight = contenido.getMeasuredHeight();
		
		//System.out.println("Los valores " + camposHeight + " y " + contentHeight + " y " + listaHeight);

		final float margin = getResources().getDimension(R.dimen.confirmacion_fields_initial_margin);

		final float maximumSize = (contentHeight * 4) / 5;
		//System.out.println("Altura maxima " +maximumSize);
		final float elementsSize = listaHeight + camposHeight;
		//System.out.println("Altura mixta " +elementsSize);
		final float heightParaValidar = (contentHeight*3)/4;
		//System.out.println("heightParaValidar " +contentHeight);
		
		confirmarButton.setOnClickListener(this);
		verTerminos.setOnClickListener(this);
	}

	private void showFields(){
		mostrarContrasena(viewTo.getShowContrasena());
		mostrarNIP(viewTo.getShowNip());
		mostrarASM(viewTo);
		mostrarCampoTarjeta(viewTo.getShowTarjeta());
		mostrarCVV(viewTo.getShowCvv());
	}

	@Override 
	public void onClick(final View v) {
		if (v.equals(confirmarButton)) {
			botonConfirmarClick();
		}else if(v.equals(verTerminos)){
			onVerTerminosLinkClik(v);
		}
	}
	
	//TODO go back setActivityChanging(true);
	private void botonConfirmarClick(){
		//setActivityChanging(true);

		viewTo.setContrasena(contrasena.getText().toString());
		viewTo.setTarjeta(tarjeta.getText().toString());
		viewTo.setNip(nip.getText().toString());
		viewTo.setAsm(asm.getText().toString());
		viewTo.setCvv(cvv.getText().toString());
		viewTo.setOkTerminos(aceptoTerminosCB.isChecked() );
		
		presenter.confirmarClick(viewTo);
	}
	
	public void processNetworkResponse( ){
		//	int operationId, ServerResponse response) {
		//confirmacionAutenticacionDelegate
		//		.analyzeResponse(operationId, response);
	}
	
	/**
	 * Muestra los terminos y condiciones.
	 */
	public void onVerTerminosLinkClik(final View sernder) {
		//contratacionAutenticacionDelegate.consultarTerminosDeUso();
		//if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Ver terminos y condiciones.");
		presenter.consultarTerminosDeUso(); 
	}
	

	
	public void showMensajePideContrasena(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if ( lenght ){
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.PASSWORD_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteContrasena));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteContrasena));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}
	}
	
	public void showMensajePideTarjeta(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			mensaje.append("Es necesario ingresar los últimos 5 dígitos de tu tarjeta");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append("Es necesario ingresar los últimos 5 dígitos de tu tarjeta");
			showInformationAlert(mensaje.toString());
		}
	}
	
	public void showMensajePideNip(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.NIP_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteNip));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteNip));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}
	}
	
	public void showMensajePideToken(final int msg, final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.ASM_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			/*
			 * switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
			 */
			mensaje.append(" ");
			mensaje.append(getString(msg));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(msg));
			
		/*	switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
		*/
		mensaje.append(".");
		showInformationAlert(mensaje.toString());
			
		}
	}
	
	public void showMensajePideCVV(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.CVV_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteCvv));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteCvv));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}
	}

	public void showMensajeErrorTerminos(){
		showInformationAlert(getString(R.string.contratacion_autenticacion_aceptar_terminos_y_condiciones_falta) );
		//getString(R.string.contratacion_autenticacion_aceptar_terminos_y_condiciones_falta);
	}
	
	/**
	 * acceso desde presenter
	 */
	public void mostrarContrasena(final boolean visibility){
		contenedorContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		contrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoContrasena.setText(getString(R.string.confirmation_contrasena));
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
			contrasena.setFilters(userFilterArray);
			contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		instruccionesContrasena.setVisibility(View.GONE);
	}
	
	/**
	 * 
	 */
	public void mostrarNIP(final boolean visibility){
		contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		nip.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoNIP.setText(getString(R.string.confirmation_nip));
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
			nip.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
			final String instrucciones = getString(R.string.confirmation_autenticacion_ayudaNip);
			if ("".equals(instrucciones)) {
				instruccionesNIP.setVisibility(View.GONE);
			} else {
				instruccionesNIP.setVisibility(View.VISIBLE);
				instruccionesNIP.setText(instrucciones);
			}
		} else {
			nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		
	}
	
	/**
	 * 
	 */
	public void mostrarASM(final ConfirmacionViewTo to){
	//public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){	
		
		switch(to.getTokenAMostrar()) {
			case ninguno:
				contenedorASM.setVisibility(View.GONE);
				campoASM.setVisibility(View.GONE);
				asm.setVisibility(View.GONE);
				asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
				break;
			case codigo:
			case registro:
				contenedorASM.setVisibility(View.VISIBLE);
				campoASM.setVisibility(View.VISIBLE);
				asm.setVisibility(View.VISIBLE);
				final InputFilter[] userFilterArray = new InputFilter[1];
				userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);	
				asm.setFilters(userFilterArray);
				cambiarAccionTexto(contrasena);
				cambiarAccionTexto(tarjeta);
				cambiarAccionTexto(nip);
				asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
		}
		
		//Constants.TipoInstrumento tipoInstrumento = confirmacionDelegate.consultaTipoInstrumentoSeguridad();
		switch (to.getInstrumentoSeguridad()) {
			case OCRA:
				//campoASM.setText(confirmacionDelegate.getEtiquetaCampoOCRA());
				campoASM.setText(getString(R.string.confirmation_ocra));
				//asm.setTransformationMethod(null);
				break;
			case DP270:
				//campoASM.setText(confirmacionDelegate.getEtiquetaCampoDP270());
				campoASM.setText(getString(R.string.confirmation_dp270));
				//asm.setTransformationMethod(null);
				break;
			case SoftToken:
				if(SuiteApp.getSofttokenStatus()) {
					asm.setText(Constants.DUMMY_OTP);
					asm.setEnabled(false);
					campoASM.setText(getString(R.string.confirmation_softtokenActivado));
				} else {
					asm.setText("");
					asm.setEnabled(true);
					campoASM.setText(getString(R.string.confirmation_softtokenDesactivado));
					//asm.setTransformationMethod(null);
				}
				
				break;
			default:
				break;
		}
		
		//String instrucciones = confirmacionDelegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
		final String instrucciones = to.getTextoAyudaInsSeg();
		if ("".equals(instrucciones)) {
			instruccionesASM.setVisibility(View.GONE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}		
	}
	
	/**
	 * 
	 */
	public void mostrarCampoTarjeta(final boolean visibility) {
		contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		tarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			//campoTarjeta.setText(confirmacionDelegate.getEtiquetaCampoTarjeta());
			campoTarjeta.setText(getString(R.string.confirmation_componente_digitos_tarjeta));
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(5);
			tarjeta.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
			//String instrucciones = confirmacionDelegate.getTextoAyudaTarjeta();
			final String instrucciones = getString(R.string.confirmation_ayuda_componente_digitos_tarjeta);
			if ("".equals(instrucciones)) {
				instruccionesTarjeta.setVisibility(View.GONE);
			} else {
				instruccionesTarjeta.setVisibility(View.VISIBLE);
				instruccionesTarjeta.setText(instrucciones);
			}
		} else {
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		
	}
	
	/**
	 * 
	 */
	public void mostrarCVV(final boolean visibility){
		contenedorCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		cvv.setVisibility(visibility ? View.VISIBLE:View.GONE);
		instruccionesCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		
		if (visibility) {
			//campoCVV.setText(confirmacionDelegate.getEtiquetaCampoCVV());
			campoCVV.setText(getString(R.string.confirmation_CVV));
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
			cvv.setFilters(userFilterArray);
			final String instrucciones = getString(R.string.confirmation_CVV_ayuda);;
			instruccionesCVV.setText(instrucciones);
			instruccionesCVV.setVisibility("".equals(instrucciones) ? View.GONE : View.VISIBLE);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			cambiarAccionTexto(nip);
			cambiarAccionTexto(asm);
			cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}
	
	//
	private void cambiarAccionTexto(final EditText campo) {
		if (campo.getVisibility() == View.VISIBLE) {
			campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
	}
	
	/**
	 * @return the viewVo
	 */
	public final ConfirmacionViewTo getViewTo() {
		return viewTo;
	}

	/**
	 * @param viewTo the viewVo to set
	 */
	public final void setViewTo(final ConfirmacionViewTo viewTo) {
		this.viewTo = viewTo;
	}

	@Override
	public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
	}

	@Override
	public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
	}

	@Override
	public void afterTextChanged(final Editable s) {
		onUserInteraction();
	}
		
}
