package suitebancomer.aplicaciones.resultados.views;

import java.util.ArrayList;

import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;

/**
 * @author amgonzalez
 *
 */
public interface ConfirmacionRegistroView extends IBaseView{
	
	void reset();
	void onSuccess(final int idTextoEncabezado);
	
	ConfirmacionViewTo getViewTo();
	void setViewTo(final ConfirmacionViewTo viewTo);
	void setListaDatos(final ArrayList<Object> datos);
	void mostrarDatosRegistro(final ArrayList<Object> datos);
	
	void mostrarContrasena(final boolean visibility);
	void mostrarNIP(final boolean visibility);
	void mostrarASM(final ConfirmacionViewTo to);
	void mostrarCVV(final boolean visibility);
	 
	void showMensajePideContrasena(final boolean lenght);
	void showMensajePideNip(final boolean lenght);
	void showMensajePideToken(final int msg, final boolean lenght);
	void showMensajePideCVV(final boolean lenght);
	 
}
