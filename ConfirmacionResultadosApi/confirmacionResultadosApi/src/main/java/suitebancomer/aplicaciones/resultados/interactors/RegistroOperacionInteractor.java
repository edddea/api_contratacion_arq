/**
 * 
 */
package suitebancomer.aplicaciones.resultados.interactors;

import suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener;

/**
 * @author amgonzalez
 *
 */
public interface RegistroOperacionInteractor {
	
	void getDatos(final OnConfirmacionFinishedListener listener);
	String getTitle();

}
