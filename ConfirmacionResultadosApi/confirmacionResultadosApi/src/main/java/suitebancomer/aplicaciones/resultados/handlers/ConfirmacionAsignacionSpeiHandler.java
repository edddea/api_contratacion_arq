/**
 * 
 */
package suitebancomer.aplicaciones.resultados.handlers;

import android.content.Intent;

import java.lang.reflect.Method;
import java.util.ArrayList;

import suitebancomer.aplicaciones.resultados.commons.ApiConstants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.proxys.IServiceProxy;
import suitebancomer.aplicaciones.resultados.views.ConfirmacionAsignacionSpeiActivity;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;

/**
 * @author amgonzalez
 *
 */
public class ConfirmacionAsignacionSpeiHandler{
	
	private final IServiceProxy proxy;
	private Long delegateId;
	private final BaseViewsControllerCommons parentViewsController;
	private final BaseViewControllerCommons rootViewController;
	private Boolean isActivityChanging;

	/*
	* Bloke usado para los efecto de transicicon
	* */
	private static Method methodOverridePendingTransition;
	
	/**
	 * 
	 * @param proxy
	 * @param parentManager
	 * @param rootViewController
	 */
	public ConfirmacionAsignacionSpeiHandler(final IServiceProxy proxy,
											 final BaseViewsControllerCommons parentManager,
											 final BaseViewControllerCommons rootViewController){
		
		this.proxy = proxy;
		this.parentViewsController=parentManager; 
		this.rootViewController = rootViewController;
		
	}

	// TODO Revisar en el delegate de donde salen los datos para el Handler
	public final void invokeActivity(final ArrayList<String> params, final Integer idText,
									 final Integer idNomInmgEncabezado, final Boolean statusDesactivado) {

		SuiteAppCRApi.getInstance().setBaseViews(rootViewController);
		final Intent intent = new Intent(
				SuiteAppCRApi.appContext,
				ConfirmacionAsignacionSpeiActivity.class);

		// TODO ConfirmAutenticacionViewVo
		//intent.putExtra(ApiConstants.PROXY_NAME, proxy);
		intent.putExtra(ApiConstants.KEY_BASE_DELEGATE, this.delegateId);
		intent.putExtra(ApiConstants.TITLE_TEXT, idText);
		intent.putExtra(ApiConstants.ICON_RESOURCE, idNomInmgEncabezado);
		intent.putExtra(ApiConstants.ACTIVITY_CHANGING, isActivityChanging());
		intent.putExtra(ApiConstants.TRACKSTATE_PARAMETERS, params);
		intent.putExtra(ApiConstants.CONF_AUTENT_STATUS_DESACTIVADO,
				statusDesactivado);

		SuiteAppCRApi.appContext.startActivity(intent);
		/*
		 * TODO
		 * String subtitle = parentViewsController.getCurrentViewControllerApp()
		 * 							.getString(R.string.confirmation_subtitulo);
		 * intent.putExtra(ApiConstants.LISTA_DATOS_CONFIRM_SUBTITULO, subtitle);
		 */

	}

	// TODO Revisar en el delegate de donde salen los datos para el Handler
	public final void invokeActivity(final String[]  extrasKeys,final Object[] extras, final ArrayList<String> params, final Integer idText,
									 final Integer idNomInmgEncabezado, final Boolean statusDesactivado) {

		SuiteAppCRApi.getInstance().setBaseViews(rootViewController);
		final Intent intent = new Intent(
				SuiteAppCRApi.appContext,
				ConfirmacionAsignacionSpeiActivity.class);

		// TODO ConfirmAutenticacionViewVo
		//intent.putExtra(ApiConstants.PROXY_NAME, proxy);
		intent.putExtra(ApiConstants.KEY_BASE_DELEGATE, this.delegateId);
		intent.putExtra(ApiConstants.TITLE_TEXT, idText);
		intent.putExtra(ApiConstants.ICON_RESOURCE, idNomInmgEncabezado);
		intent.putExtra(ApiConstants.ACTIVITY_CHANGING, isActivityChanging());
		intent.putExtra(ApiConstants.TRACKSTATE_PARAMETERS, params);
		intent.putExtra(ApiConstants.CONF_AUTENT_STATUS_DESACTIVADO,
				statusDesactivado);

		if (extrasKeys != null && extras != null && extras.length == extrasKeys.length) {
			final int extrasLength = extras.length;
			for (int i=0; i<extrasLength; i++) {
				if (extras[i] instanceof String) {
					intent.putExtra(extrasKeys[i], (String)extras[i]);
				} else if (extras[i] instanceof Integer) {
					intent.putExtra(extrasKeys[i], ((Integer)extras[i]).intValue());
				} else if (extras[i] instanceof Boolean) {
					intent.putExtra(extrasKeys[i], ((Boolean)extras[i]).booleanValue());
				}else if(extras[i] instanceof Long){
					intent.putExtra(extrasKeys[i], ((Long)extras[i]).longValue());
				}
			}
		}


		SuiteAppCRApi.appContext.startActivity(intent);
		/*
		 * TODO
		 * String subtitle = parentViewsController.getCurrentViewControllerApp()
		 * 							.getString(R.string.confirmation_subtitulo);
		 * intent.putExtra(ApiConstants.LISTA_DATOS_CONFIRM_SUBTITULO, subtitle);
		 */

	}
	
	public void setDelegateId(final Long id){
		this.delegateId = id;
	}

	/**
	 * @return the isActivityChanging
	 */
	public final boolean isActivityChanging() {
		return isActivityChanging;
	}

	/**
	 * @param isActivityChanging the isActivityChanging to set
	 */
	public final void setActivityChanging(final boolean isActivityChanging) {
		this.isActivityChanging = isActivityChanging;
	}
		
}
