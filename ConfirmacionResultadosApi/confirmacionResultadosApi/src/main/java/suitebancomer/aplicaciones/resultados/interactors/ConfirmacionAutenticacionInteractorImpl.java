/**
 * 
 */
package suitebancomer.aplicaciones.resultados.interactors;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import suitebancomer.aplicaciones.resultados.commons.ApiConstants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IConfirmacionAutenticacionServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import android.os.Handler;
import android.text.TextUtils;

/**
 * @author lbermejo
 *
 */
public class ConfirmacionAutenticacionInteractorImpl implements ConfirmacionAutenticacionInteractor {
	
	private final IConfirmacionAutenticacionServiceProxy proxy;
	
	public ConfirmacionAutenticacionInteractorImpl(final IConfirmacionAutenticacionServiceProxy proxy) {
		this.proxy = proxy;
	}
	
	/**
	 * @return the proxy
	 */
	public IConfirmacionAutenticacionServiceProxy getProxy() {
		return proxy;
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.interactors.ConfirmacionInteractor#getDatos(suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener)
	 */
	@Override
	public void getListaDatos(final OnConfirmacionFinishedListener listener) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				listener.onFinishedListaDatos((ArrayList<Object>) 
						proxy.getListaDatos());
			}
		}
		);
	}
	
	@Override
	public void getShowFields(final OnConfirmacionFinishedListener listener) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				listener.onFinishedValidShowFields(
						proxy.showFields());
				/* en el proxy 
				//mostrarContrasena(confirmacionDelegate.consultaDebePedirContrasena());
				//mostrarNIP(confirmacionDelegate.consultaDebePedirNIP());
				//mostrarASM(confirmacionDelegate.consultaInstrumentoSeguridad());
				//mostrarCVV(confirmacionDelegate.consultaDebePedirCVV());
				//mostrarCampoTarjeta(confirmacionDelegate.mostrarCampoTarjeta());
				 */
			}
		}
		);
	}
	
	@Override
	public Integer consultaOperationsIdTextoEncabezado() {
		return proxy.consultaOperationsIdTextoEncabezado();
	}
	
	//TODO
	public void doConfirmacionOperacion(final ConfirmacionViewTo viewTo,
							final OnConfirmacionFinishedListener listener){

		if( viewTo.getShowContrasena() ){
			if (TextUtils.isEmpty(viewTo.getContrasena() )){
                listener.onErrorContrasena(false);
                return;
            }else if(viewTo.getContrasena().trim().length() 
            		!= Constants.PASSWORD_LENGTH){
            	listener.onErrorContrasena(true);
            	return;
            }
		}
		
		if( viewTo.getShowTarjeta() ){
			if (TextUtils.isEmpty(viewTo.getTarjeta() )){
                listener.onErrorTarjeta(false);
                return;
            }else if(viewTo.getTarjeta().trim().length() 
            		!= ApiConstants.TARJETA_LENGTH  ){
            	listener.onErrorTarjeta(true);
            	return;
            }
		}
		
		if( viewTo.getShowNip() ){
			if (TextUtils.isEmpty(viewTo.getNip() )){
                listener.onErrorNip(false);
                return;
            }else if(viewTo.getNip().trim().length() 
            		!= Constants.NIP_LENGTH ){
            	listener.onErrorNip(true);
            	return;
            }
		}
		
		if( viewTo.getTokenAMostrar()
				!= Constants.TipoOtpAutenticacion.ninguno){
			
			if (TextUtils.isEmpty(viewTo.getAsm() )){
				final int msg = proxy.getMessageAsmError( viewTo.getInstrumentoSeguridad());
				/*
				 * en el proxy 
				 * switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
				 */
                listener.onErrorAsm(msg, false);
                return;
            }else if(viewTo.getAsm().trim().length() 
            		!= Constants.ASM_LENGTH ){
				final int msg = proxy.getMessageAsmError(viewTo.getInstrumentoSeguridad());
            	listener.onErrorAsm(msg, true);
            	return;
            }
		}
		
		if( viewTo.getShowCvv() ){
			if (TextUtils.isEmpty(viewTo.getCvv() )){
                listener.onErrorCvv(false);
                return;
            }else if(viewTo.getCvv().trim().length() 
            		!= Constants.CVV_LENGTH ){
            	listener.onErrorCvv(true);
            	return;
            }
		}
		
		String newToken = null;
		if ( viewTo.getTokenAMostrar() != TipoOtpAutenticacion.ninguno
				&& viewTo.getInstrumentoSeguridad() ==  TipoInstrumento.SoftToken
				&& SuiteAppCRApi.getSofttokenStatus() ){
			newToken = proxy.loadOtpFromSofttoken(viewTo.getTokenAMostrar());
		}
		if(null != newToken){
			viewTo.setAsm(newToken);
		}
		
		// operacion
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				 //TODO enviar Activity
				listener.onFinishedConfirmacionOperacion(
						proxy.doOperation(viewTo));
				
				/*  en el proxy
				 * 
				operationDelegate.realizaOperacion(confirmacionAutenticacionViewController, contrasena, nip, asm, cvv, tarjeta);
				confirmacionAutenticacionViewController.habilitarBtnContinuar();
				res = true;
				
				 * 
				 	public void analyzeResponse(int operationId, ServerResponse response) {
				if(response.getStatus() == ServerResponse.OPERATION_ERROR){
						confirmacionAutenticacionViewController.limpiarCampos();
						((BmovilViewsController)confirmacionAutenticacionViewController.getParentViewsController()).getCurrentViewControllerApp().showInformationAlert(response.getMessageText());
					}
					operationDelegate.analyzeResponse(operationId, response);
				}
				 * 
				public void processNetworkResponse(int operationId, 
													ServerResponse response) {
					confirmacionDelegate.analyzeResponse(operationId, response);
				}
				*/
			}
		}
		);
		
	}

}
