/**
 * 
 */
package suitebancomer.aplicaciones.resultados.proxys;

import suitebancomer.aplicaciones.resultados.to.ResultadosViewTo;

/**
 * @author lbermejo
 *
 */
public interface IResultadosAutenticacionServiceProxy extends IServiceProxy {
	
	ResultadosViewTo getListaDatos();
	ResultadosViewTo showFields();
	ResultadosViewTo getOnPrepareOptionsMenu();
	Boolean onOptionsItemSelected(final Integer idMenu);
	void setParamStateParentManager();
	void botonMenuClick();
	
}
