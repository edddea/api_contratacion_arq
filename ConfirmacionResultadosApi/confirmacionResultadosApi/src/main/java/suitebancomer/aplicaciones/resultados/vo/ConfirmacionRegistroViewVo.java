/**
 * 
 */
package suitebancomer.aplicaciones.resultados.vo;

import java.io.Serializable;



/**
 * @author amgonzalez
 *
 */
public class ConfirmacionRegistroViewVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1092028826946939850L;
	private String tarjeta;
	private String contrasena;
	private Long nip;	
	private String asm;
	private String cvv;

	/**
	 * @return the tarjeta
	 */
	public final String getTarjeta() {
		return tarjeta;
	}

	/**
	 * @param tarjeta the tarjeta to set
	 */
	public final void setTarjeta(final String tarjeta) {
		this.tarjeta = tarjeta;
	}

	/**
	 * @return the contrasena
	 */
	public final String getContrasena() {
		return contrasena;
	}

	/**
	 * @param contrasena the contrasena to set
	 */
	public final void setContrasena(final String contrasena) {
		this.contrasena = contrasena;
	}

	/**
	 * @return the nip
	 */
	public final Long getNip() {
		return nip;
	}

	/**
	 * @param nip the nip to set
	 */
	public final void setNip(final Long nip) {
		this.nip = nip;
	}

	/**
	 * @return the asm
	 */
	public final String getAsm() {
		return asm;
	}

	/**
	 * @param asm the asm to set
	 */
	public final void setAsm(final String asm) {
		this.asm = asm;
	}

	/**
	 * @return the cvv
	 */
	public final String getCvv() {
		return cvv;
	}

	/**
	 * @param cvv the cvv to set
	 */
	public final void setCvv(final String cvv) {
		this.cvv = cvv;
	}

}
