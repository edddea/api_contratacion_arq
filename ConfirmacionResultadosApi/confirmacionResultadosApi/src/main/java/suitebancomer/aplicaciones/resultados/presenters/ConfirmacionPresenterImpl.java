/**
 * 
 */
package suitebancomer.aplicaciones.resultados.presenters;



import java.util.ArrayList;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.interactors.ConfirmacionInteractor;
import suitebancomer.aplicaciones.resultados.interactors.ConfirmacionInteractorImpl;
import suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IConfirmacionServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.resultados.views.ConfirmacionView;
import android.content.Intent;

/**
 * @author lbermejo
 *
 */
public class ConfirmacionPresenterImpl implements ConfirmacionPresenter, OnConfirmacionFinishedListener {
	
	private final ConfirmacionView cview;
	private final ConfirmacionInteractor interactor;

	public ConfirmacionPresenterImpl(final ConfirmacionView view, final Intent intent){
		this.cview = view;
		this.interactor = new ConfirmacionInteractorImpl(
				getServiceProxy(intent)); 
	}
	
	private IConfirmacionServiceProxy getServiceProxy(final Intent intent){
		final IConfirmacionServiceProxy p = (IConfirmacionServiceProxy)
				SuiteAppCRApi.getInstance().getProxy();
		return p;
	}

	@Override 
	public void onResume() {
        //mainView.showProgress();
       
    }
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.presenters.ConfirmacionPresenter#show()
	 */
	@Override
	public void show() {
		/*
		 *  mainView.showProgress();
		 */
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener#onError()
	 */
	@Override
	public void onError() {
		//View.setUsernameError();
		// en el view username.setError(getString(R.string.username_error));
	}
	
	@Override
	public void limpiarCampos(){
		cview.reset();
	}

	@Override
	public void getListaDatos() {
		interactor.getListaDatos(this);
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener#onFinished()
	 */
	@Override
	public void onFinishedListaDatos( final ArrayList<Object> list ) {
		cview.setListaDatos(list);
	}

	@Override
	public void getShowFields() {
		interactor.getShowFields(this);
	}

	@Override
	public void onFinishedValidShowFields(final ConfirmacionViewTo fields) {
		cview.setViewTo(fields);
		cview.mostrarContrasena(fields.getShowContrasena());
		cview.mostrarNIP(fields.getShowNip());
		cview.mostrarASM(fields);
		cview.mostrarCampoTarjeta(fields.getShowTarjeta());
		cview.mostrarCVV(fields.getShowCvv());
	}

	@Override
	public void confirmarClick(final ConfirmacionViewTo viewTo) {
		interactor.doConfirmacionOperacion(viewTo, this);
	}
	
	@Override
	public void onFinishedConfirmacionOperacion(final Boolean resp){
		//int idTextoEncabezado
		if ( resp.booleanValue() ){
			final int idTextoEncabezado = interactor.consultaOperationsIdTextoEncabezado();
			cview.onSuccess(idTextoEncabezado);
		}else{
			cview.onErrorMsg();
		}
	}

	@Override
	public void onErrorContrasena(final boolean lenght) {
		cview.showMensajePideContrasena(lenght);
	}

	@Override
	public void onErrorNip(final boolean lenght) {
		cview.showMensajePideNip(lenght);
	}

	@Override
	public void onErrorCvv(final boolean lenght) {
		cview.showMensajePideCVV(lenght);
	}

	@Override
	public void onErrorAsm(final int msg, final boolean lenght) {
		cview.showMensajePideToken(msg, lenght);
	}

	@Override
	public void onErrorTarjeta(final boolean lenght) {
		cview.showMensajePideTarjeta(lenght);
	}

}
