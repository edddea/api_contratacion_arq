/**, 
 * 
 */
package suitebancomer.aplicaciones.resultados.interactors;

import suitebancomer.aplicaciones.resultados.listeners.OnConfirmRegistroFinishedListener;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;

/**
 * @author amgonzalez
 *
 */
public interface ConfirmacionRegistroInteractor {
	
	void getListaDatos(final OnConfirmRegistroFinishedListener listener);
	void getDatosRegistroExitoso(final OnConfirmRegistroFinishedListener listener);
	void getShowFields(final OnConfirmRegistroFinishedListener listener);
	void doConfirmacionOperacion(final ConfirmacionViewTo viewVo,
								 final OnConfirmRegistroFinishedListener listener);
	
	Integer consultaOperationsIdTextoEncabezado();
}
