/**
 * 
 */
package suitebancomer.aplicaciones.resultados.presenters;

import suitebancomer.aplicaciones.resultados.interactors.ResultadosAutenticacionInteractor;


/**
 * @author amgonzalez
 *
 */
public interface ResultadosAutenticacionPresenter {

	void onResume();
	ResultadosAutenticacionInteractor getInteractor();
	//void limpiarCampos();
	void getListaDatos();
	void getShowFields();
	
	void botonMenuClick();
	void setParamStateParentManager();
	void getOnPrepareOptionsMenu();
	boolean optionsItemSelected(final int menuId);
}
