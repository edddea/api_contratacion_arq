/**
 * 
 */
package suitebancomer.aplicaciones.resultados.views;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import suitebancomer.aplicaciones.resultados.R;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.presenters.RegistroOperacionPresenter;
import suitebancomer.aplicaciones.resultados.presenters.RegistroOperacionPresenterImpl;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomer.aplicaciones.resultados.vo.RegistroOperacionViewVo;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bancomer.base.SuiteApp;

/**
 * @author amgonzalez
 *
 */
public class RegistroOperacionActivity extends ApiBaseActivity implements RegistroOperacionView, OnClickListener{
	
	private LinearLayout contenedorLista;
	private LinearLayout contenedorASM;
	/*private ListaDatosViewController listaDatos;*/
	private TextView instruccionesASM;
	private TextView campoASM;
	private View helpImage;
	private EditText asm;
		
	//private ProgressBar progressBar;
	private RegistroOperacionPresenter presenter;
	private static final String VIEW = "confirmacion registro";
	//AMZ
	public ArrayList<String> estados = new ArrayList<String>();

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_bmovil_confirmacion_registro);
		SuiteAppCRApi.getInstance().onCreate(getApplicationContext());
		SuiteApp.appContext=this;
		presenter = new RegistroOperacionPresenterImpl(this,getIntent());
		//setTitle(1, 1); // TODO Dummy 
		findViews();
		//this.viewVo = presenter.getDatos();
		moverScroll();

		TrackingHelper.trackState(VIEW, estados);
	}

	@Override
	public void onBackPressed() {
		TrackingHelper.touchAtrasState();
		super.onBackPressed();
	}
	 @Override 
	 protected void onResume() {
		 super.onResume();
		 SuiteApp.appContext = this;
	     presenter.onResume();
	 }
	
	@Override 
	public void showProgress() {
	       //progressBar.setVisibility(View.VISIBLE);
	       //listView.setVisibility(View.INVISIBLE);
	}
	
	@Override 
	public void hideProgress() {
	       //progressBar.setVisibility(View.INVISIBLE);
	       //listView.setVisibility(View.VISIBLE);
	}
	
	@Override 
	public void showMessage(final String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}
	
	@Override 
	public void onClick(final View v) {
		//presenter.validate(username.getText().toString(), password.getText().toString());
	}
	

	private void findViews() {
		contenedorASM = (LinearLayout) findViewById(R.id.registrar_op_campo_asm_layout);
		asm = (EditText) findViewById(R.id.registrar_op_asm_edittext);
		campoASM = (TextView) findViewById(R.id.registrar_op_asm_label);
		instruccionesASM = (TextView) findViewById(R.id.registrar_op_asm_instrucciones_label);
		contenedorLista = (LinearLayout) findViewById(R.id.registrar_op_lista_datos);
		//SPEI
		helpImage = findViewById(R.id.helpImage);
		
		
	}
	
	private void scaleToScreenSize() {
		final GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(findViewById(R.id.registrar_op_contenedor_principal), false);
		gTools.scale(contenedorLista, false);
		gTools.scale(contenedorASM, false);
		gTools.scale(asm, true);
		gTools.scale(campoASM, true);
		gTools.scale(instruccionesASM, true);
		gTools.scale(findViewById(R.id.registrar_op_confirmar_button));
		//SPEI
		gTools.scale(helpImage);
		
	}
	
	private void addListeners(){
		//contrasena.addTextChangedListener(new BmovilTextWatcher(this));
		//nip.addTextChangedListener(new BmovilTextWatcher(this));
		//asm.addTextChangedListener(new BmovilTextWatcher(this));
		//cvv.addTextChangedListener(new BmovilTextWatcher(this));
		//tarjeta.addTextChangedListener(new BmovilTextWatcher(this));
	}
	
	
	/*
	*
	/*
	 *  @Override public void navigateToHome() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
	 * 
	 * 
	 *  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
       @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
      @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        presenter.onItemClicked(position);
    }
    
	 * 
	 */

		// llamado en el controller como "limpiarCampos"		
	//@Override
	public void limpiarCampos() {
		if(asm.getVisibility() == View.VISIBLE){
			asm.setText("");
		}
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSuccess(final int idTextoEncabezado) {
		// TODO Auto-generated method stub

		reset();
	}

	@Override
	public ConfirmacionViewTo getViewTo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setViewTo(final ConfirmacionViewTo viewTo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setListaDatos(final ArrayList<Object> datos) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mostrarContrasena(final boolean visibility) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mostrarNIP(final boolean visibility) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mostrarASM(final ConfirmacionViewTo vo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mostrarCampoTarjeta(final boolean visibility) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mostrarCVV(final boolean visibility) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showMensajePideContrasena(final boolean lenght) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showMensajePideTarjeta(final boolean lenght) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showMensajePideNip(final boolean lenght) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showMensajePideToken(final int msg, final boolean lenght) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showMensajePideCVV(final boolean lenght) {
		// TODO Auto-generated method stub
		
	}
	
}
