/**
 * 
 */
package suitebancomer.aplicaciones.resultados.interactors;

import suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IServiceProxy;

/**
 * @author amgonzalez
 *
 */
public class RegistroOperacionInteractorImpl implements RegistroOperacionInteractor {
	
	private final IServiceProxy proxy;
	
	public RegistroOperacionInteractorImpl(final IServiceProxy proxy) {
		this.proxy = proxy;
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.interactors.ConfirmacionInteractor#getDatos(suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener)
	 */
	@Override
	public void getDatos(final OnConfirmacionFinishedListener listener) {
		// TODO Auto-generated method stub
		// proxy operation
		
	}


	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	
	/**
	 * @return the proxy
	 */
	public IServiceProxy getProxy() {
		return proxy;
	}
	
	
}
