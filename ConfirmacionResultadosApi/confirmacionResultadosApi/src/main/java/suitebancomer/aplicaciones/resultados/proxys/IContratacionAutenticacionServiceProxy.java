/**
 * 
 */
package suitebancomer.aplicaciones.resultados.proxys;

import java.util.ArrayList;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;

/**
 * @author lbermejo
 *
 */
public interface IContratacionAutenticacionServiceProxy extends IServiceProxy {
	
	ArrayList<Object> getListaDatos();
	ConfirmacionViewTo showFields();
	Integer getMessageAsmError(	final Constants.TipoInstrumento tipoInstrumento);
	String loadOtpFromSofttoken(  final Constants.TipoOtpAutenticacion tipoOtpAutenticacion);
	//Integer consultaOperationsIdTextoEncabezado();
	Constants.Perfil consultaClienteProfile(); 
	void consultarTerminosDeUso();
	
}
