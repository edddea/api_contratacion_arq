/**
 * 
 */
package suitebancomer.aplicaciones.resultados.listeners;

import java.util.ArrayList;

import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;


/**
 * @author lbermejo
 *
 */
public interface OnContrataAutoFinishedListener {

	void onError();
	void onFinishedListaDatos(final ArrayList<Object> list);
	void onFinishedValidShowFields(final ConfirmacionViewTo fields);
	void onFinishedConfirmacionOperacion(final Boolean resp);
	
	void onErrorContrasena(final boolean lenght);
	void onErrorNip(final boolean lenght);
	void onErrorCvv(final boolean lenght);
	void onErrorAsm(final int msg, final boolean lenght);
	void onErrorTarjeta(final boolean lenght);
	void onErrorTerminos();
}
