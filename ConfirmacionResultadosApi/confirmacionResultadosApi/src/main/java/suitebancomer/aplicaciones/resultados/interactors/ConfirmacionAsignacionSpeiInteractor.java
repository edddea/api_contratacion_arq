/**
 * 
 */
package suitebancomer.aplicaciones.resultados.interactors;

import suitebancomer.aplicaciones.resultados.listeners.OnConfirmAsingSpeiFinishedListener;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionAsignacionSpeiViewTo;

/**
 * @author amgonzalez
 *
 */
public interface ConfirmacionAsignacionSpeiInteractor {
	
	void getListaDatos(final OnConfirmAsingSpeiFinishedListener listener);
	void getShowFields(final OnConfirmAsingSpeiFinishedListener listener);
	void doConfirmacionOperacion(final ConfirmacionAsignacionSpeiViewTo viewVo,
								 final OnConfirmAsingSpeiFinishedListener listener);
	Integer consultaOperationsIdTextoEncabezado();
	void requestSpeiTermsAndConditions(final OnConfirmAsingSpeiFinishedListener listener);
}
