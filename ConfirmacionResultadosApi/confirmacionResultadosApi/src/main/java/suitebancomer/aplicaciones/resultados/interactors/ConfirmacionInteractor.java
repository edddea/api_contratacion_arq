/**, 
 * 
 */
package suitebancomer.aplicaciones.resultados.interactors;

import suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;

/**
 * @author lbermejo
 *
 */
public interface ConfirmacionInteractor {
	
	void getListaDatos(final OnConfirmacionFinishedListener listener);
	void getShowFields(final OnConfirmacionFinishedListener listener);
	void doConfirmacionOperacion(final ConfirmacionViewTo viewVo,
								 final OnConfirmacionFinishedListener listener);
	Integer consultaOperationsIdTextoEncabezado();
}
