/**
 * 
 */
package suitebancomer.aplicaciones.resultados.commons;

/**
 * @author lbermejo
 *
 */
public final class ApiConstants {

	public static final String PROXY_NAME = "proxy";
	public static final String KEY_BASE_DELEGATE = "BaseDelegateForKey";
	public static final String TRACKSTATE_PARAMETERS = "trackstate.param";
	public static final String CONF_AUTENT_STATUS_DESACTIVADO = "statusDesactivado";
	public static final String LISTA_DATOS_CONFIRM_SUBTITULO="lstd_confirmation_subtitulo";
	public static final String TITLE_TEXT="titleText";
	public static final String ICON_RESOURCE ="iconResource";
	public static final String ACTIVITY_CHANGING = "ActivityChanging";
	public static final int    TARJETA_LENGTH= 5; 
	
	
	// DelegateBaseOperation 
	/**
     * Parametro para mostrar la opcion de menu SMS
     */
    public final static int SHOW_MENU_SMS = 2;
    
    /**
     * Parametro para mostrar la opcion de menu Correo electrónico
     */
    public final static int SHOW_MENU_EMAIL = 4;
    
    /**
     * Parametro para mostrar la opcion de menu PDF
     */
    public final static int SHOW_MENU_PDF = 8;
    
    /**
     * Parametro para mostrar la opcion de menu Frecuente
     */
    public final static int SHOW_MENU_FRECUENTE = 16;
    
    /**
     * Parametro para mostrar la opcion de menu Rapida
     */
    public final static int SHOW_MENU_RAPIDA = 32;
    
    /**
     * Parametro para mostrar la opcion de menu Rapida
     */
    public final static int SHOW_MENU_BORRAR = 64;
    
	
}
