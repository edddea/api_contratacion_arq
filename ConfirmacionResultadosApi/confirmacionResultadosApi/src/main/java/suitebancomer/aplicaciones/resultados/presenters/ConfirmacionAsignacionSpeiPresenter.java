/**
 * 
 */
package suitebancomer.aplicaciones.resultados.presenters;

import suitebancomer.aplicaciones.resultados.to.ConfirmacionAsignacionSpeiViewTo;

/**
 * @author amgonzalez
 *
 */
public interface ConfirmacionAsignacionSpeiPresenter {

	void show();
	void onResume();

	void limpiarCampos();
	void getListaDatos();
	void getShowFields();
	
	void confirmarClick(final ConfirmacionAsignacionSpeiViewTo viewVo);
	void requestSpeiTermsAndConditions();
	
}
