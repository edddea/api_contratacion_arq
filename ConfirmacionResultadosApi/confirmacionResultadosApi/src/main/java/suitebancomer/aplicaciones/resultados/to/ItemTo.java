/**
 * 
 */
package suitebancomer.aplicaciones.resultados.to;

import java.io.Serializable;

/**
 * @author lbermejo
 *
 */
public class ItemTo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8083217936758389137L;
	private String id;
	private String descripcion;
	private String label;
	private String instruction;
	private Boolean visible;
	private String code;
	
	public ItemTo() {
	}
	
	public ItemTo(final String id, final String des) {
		this.id = id;
		this.descripcion = des;

	}

	/**
	 * @return the id
	 */
	public final String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public final void setId(final String id) {
		this.id = id;
	}

	/**
	 * @return the descripcion
	 */
	public final String getDescripcion() {
		return descripcion;
	}

	/**
	 * @param descripcion the descripcion to set
	 */
	public final void setDescripcion(final String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * @return the label
	 */
	public final String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public final void setLabel(final String label) {
		this.label = label;
	}

	/**
	 * @return the instruction
	 */
	public final String getInstruction() {
		return instruction;
	}

	/**
	 * @param instruction the instruction to set
	 */
	public final void setInstruction(final String instruction) {
		this.instruction = instruction;
	}

	/**
	 * @return the visible
	 */
	public final Boolean getVisible() {
		return visible;
	}

	/**
	 * @param visible the visible to set
	 */
	public final void setVisible(final Boolean visible) {
		this.visible = visible;
	}

	/**
	 * @return the code
	 */
	public final String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public final void setCode(final String code) {
		this.code = code;
	}
	
	
}
