/**
 * 
 */
package suitebancomer.aplicaciones.resultados.listeners;

import java.util.ArrayList;

import suitebancomer.aplicaciones.resultados.to.ConfirmacionAsignacionSpeiViewTo;


/**
 * @author lbermejo
 *
 */
public interface OnConfirmAsingSpeiFinishedListener {

	void onError();
	void onFinishedListaDatos(final ArrayList<Object> list);
	void onFinishedValidShowFields(final ConfirmacionAsignacionSpeiViewTo fields);
	void onFinishedConfirmacionOperacion(final Boolean resp);
	void onFinishedTerminosCondiciones(final Boolean resp);
	
	void onErrorOtp(final int idMsg, final String format);
	void onErrorValue(final int idMsg);
	
}
