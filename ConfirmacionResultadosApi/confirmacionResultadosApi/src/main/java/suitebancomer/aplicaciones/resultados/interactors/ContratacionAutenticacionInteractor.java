/**
 * 
 */
package suitebancomer.aplicaciones.resultados.interactors;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.resultados.listeners.OnContrataAutoFinishedListener;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;

/**
 * @author amgonzalez
 *
 */
public interface ContratacionAutenticacionInteractor {
	
	void getListaDatos(final OnContrataAutoFinishedListener listener);
	void getShowFields(final OnContrataAutoFinishedListener listener);
	void doConfirmacionOperacion(final ConfirmacionViewTo viewVo,
								 final OnContrataAutoFinishedListener listener);
	
	Constants.Perfil consultaClienteProfile(final OnContrataAutoFinishedListener listener);
	
	void consultarTerminosDeUso();
}
