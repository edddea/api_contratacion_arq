package suitebancomer.aplicaciones.resultados.views;

import java.util.ArrayList;

import suitebancomer.aplicaciones.resultados.to.ConfirmacionAsignacionSpeiViewTo;



/**
 * @author amgonzalez
 *
 */
public interface ConfirmacionAsignacionSpeiView extends IBaseView{

	void showProgress();
	void hideProgress();
	void showMessage(final String message);
	void cleanAuthenticationFields();

	void reset();
	void onSuccess(final int idTextoEncabezado);
	ConfirmacionAsignacionSpeiViewTo getViewTo();
	void setViewTo(final ConfirmacionAsignacionSpeiViewTo viewTo);
	void setListaDatos(final ArrayList<Object> datos);
	
	/*
	void mostrarContrasena(boolean visibility);
	void mostrarNIP(boolean visibility);
	void mostrarASM(ConfirmacionAsignacionSpeiViewTo to);
	void mostrarCampoTarjeta(boolean visibility);
	void mostrarCVV(boolean visibility);
	*/
	
	void configureCardElement(final boolean visible, final String label, final String instructions);
	void configureNipElement(final boolean visible, final String label, final String instructions);
	void configurePasswordElement(final boolean visible, final String label, final String instructions);
	void configureCvvElement(final boolean visible, final String label, final String instructions);
	void configureOtpElement(final boolean visible, final String label, final String instructions, final String code);
	
	void showInformationAlert(final int msg, final String format);
	void showInformationAlert(final int msg);
	void onErrorMsg(final int idMsg);
	
}
