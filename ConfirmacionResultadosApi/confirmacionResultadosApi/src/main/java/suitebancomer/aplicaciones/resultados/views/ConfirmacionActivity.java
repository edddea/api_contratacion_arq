/**
 * 
 */
package suitebancomer.aplicaciones.resultados.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.resultados.R;
import suitebancomer.aplicaciones.resultados.commons.ApiConstants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.presenters.ConfirmacionPresenter;
import suitebancomer.aplicaciones.resultados.presenters.ConfirmacionPresenterImpl;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.views.ListaDatosViewControllerCR;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

/**
 * @author lbermejo
 *
 */
public class ConfirmacionActivity extends ApiBaseActivity implements ConfirmacionView, OnClickListener, TextWatcher {
	
	private LinearLayout contenedorPrincipal;
	private LinearLayout contenedorContrasena;
	private LinearLayout contenedorNIP;
	private LinearLayout contenedorASM;
	private LinearLayout contenedorCVV;
	private LinearLayout contenedorCampoTarjeta;
	
	private TextView campoTarjeta;
	private TextView campoContrasena;
	private TextView campoNIP;
	private TextView campoASM;
	private TextView campoCVV;
	
	private EditText tarjeta;
	private EditText contrasena;
	private EditText nip;	
	private EditText asm;
	private EditText cvv;
	
	private TextView instruccionesContrasena;
	private TextView instruccionesNIP;
	private TextView instruccionesASM;
	private TextView instruccionesCVV;
	private TextView instruccionesTarjeta;
	
	private ImageButton confirmarButton;
	
	private ConfirmacionPresenter presenter;
	private ConfirmacionViewTo viewTo;
	
	private static final String VIEW = "confirma";
	private String titleListaDatosView;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		//setContentView(R.layout.layout_bmovil_confirmacion_act);
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_confirmacion);
		setActivityChanging(getIntent().getBooleanExtra(ApiConstants.ACTIVITY_CHANGING, false));

		SuiteAppCRApi.getInstance().onCreate(getApplicationContext());
		SuiteApp.appContext=this;

		final ArrayList<String> param = getIntent()
				.getStringArrayListExtra(ApiConstants.TRACKSTATE_PARAMETERS);
		titleListaDatosView = getIntent()
				.getStringExtra(ApiConstants.LISTA_DATOS_CONFIRM_SUBTITULO);
		
		TrackingHelper.trackState(VIEW,param );
		presenter = new ConfirmacionPresenterImpl(this,getIntent()); 
		setTitle( getIntent() );
		
		findViews();
		scaleToScreenSize();
		
		presenter.getListaDatos();
		configuraPantalla();
		
		moverScroll();
		
		contrasena.addTextChangedListener(this);
		nip.addTextChangedListener(this);
		asm.addTextChangedListener(this);
		cvv.addTextChangedListener(this);
		tarjeta.addTextChangedListener(this);
		
	}

	@Override
	public void onBackPressed() {
		TrackingHelper.touchAtrasState();
		super.onBackPressed();
	}
	
	@Override
    protected void onStart(){
        super.onStart();
        // example
    }
	
	@Override
    protected void onRestart(){
        super.onRestart();
        // example
    }
	
	@Override 
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		setActivityChanging(false);
		presenter.onResume();
		
		/*
		 * TODO onResume ConfirmacionActivity
		 * super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		 */
	}
	
	@Override
    protected void onPause(){
        super.onPause();
        // example
        //TODO onPause ConfirmacionActivity
        //parentViewsController.consumeAccionesDePausa();
    }

	@Override
    protected void onStop(){
        super.onStop();
        setActivityChanging(false);
        // example
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        // example
    }
    
    public void onSuccess( final int idTextoEncabezado){

		final Map<String,Object> operacionRealizadaMap = new HashMap<String, Object>();
		//Comprobacion de titulos

    	if(getString(idTextoEncabezado) == getString(R.string.opcionesTransfer_menu_miscuentas))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;transferencias+mis cuentas");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		else if(getString(idTextoEncabezado) == getString(R.string.opcionesTransfer_menu_otrascuentasbbva))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;transferencias+otra cuenta bbva bancomer");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		else if(getString(idTextoEncabezado) == getString(R.string.opcionesTransfer_menu_cuentaexpress))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;transferencias+cuenta express");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		else if( getString(idTextoEncabezado) == getString(R.string.opcionesTransfer_menu_otrosbancos))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;transferencias+otros bancos");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		else if(getString(idTextoEncabezado) == getString(R.string.opcionesTransfer_menu_dineromovil))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;transferencias+dinero movil");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		else if( getString(idTextoEncabezado) == getString(R.string.servicesPayment_title))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;pagar+servicio");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		else if(getString(idTextoEncabezado) == getString(R.string.transferir_otrosBBVA_TDC_title))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;pagar+tarjeta credito");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		else if(getString(idTextoEncabezado) == getString(R.string.tiempo_aire_title))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;comprar+tiempo aire");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
    	
    	reset();
    	//finish();
    }
    
    /*
	 * (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.views.ConfirmacionView#reset()
	 * acceso desde presenter
	 */
	@Override
	public void reset() {
		this.contrasena.setText("");
		this.nip.setText("");
		if(this.asm.isEnabled())
			this.asm.setText("");

		this.cvv.setText("");
		this.tarjeta.setText("");
	}

	@Override 
	public void showProgress() {
	    //progressBar.setVisibility(View.VISIBLE);
	    //listView.setVisibility(View.INVISIBLE);
	}
	
	@Override 
	public void hideProgress() {
	    //progressBar.setVisibility(View.INVISIBLE);
	    //listView.setVisibility(View.VISIBLE);
	}
	
	private void findViews() {
		contenedorPrincipal		= (LinearLayout)findViewById(R.id.confirmacion_lista_datos);
		contenedorContrasena	= (LinearLayout)findViewById(R.id.campo_confirmacion_contrasena_layout);
		contenedorNIP 			= (LinearLayout)findViewById(R.id.campo_confirmacion_nip_layout);
		contenedorASM 			= (LinearLayout)findViewById(R.id.campo_confirmacion_asm_layout);
		contenedorCVV 			= (LinearLayout)findViewById(R.id.campo_confirmacion_cvv_layout);
		
		contrasena 				= (EditText)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
		nip 					= (EditText)contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
		asm						= (EditText)contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
		cvv						= (EditText)contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);
		
		campoContrasena			= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_label);
		campoNIP				= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_label);
		campoASM				= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_label);
		campoCVV				= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_label);
	
		instruccionesContrasena	= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
		instruccionesNIP		= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
		instruccionesASM		= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
		instruccionesCVV		= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);
		
		contenedorCampoTarjeta  = (LinearLayout)findViewById(R.id.campo_confirmacion_campotarjeta_layout);
		tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
		campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
		instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);
		
		confirmarButton 		= (ImageButton)findViewById(R.id.confirmacion_confirmar_button);
		
		
	}
	
	private void scaleToScreenSize() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(contenedorPrincipal);
		guiTools.scale(findViewById(R.id.confirmacion_campos_layout));
		
		guiTools.scale(contenedorContrasena);
		guiTools.scale(contenedorNIP);
		guiTools.scale(contenedorASM);
		guiTools.scale(contenedorCVV);
		
		guiTools.scale(contrasena, true);
		guiTools.scale(nip, true);
		guiTools.scale(asm, true);
		guiTools.scale(cvv, true);
		
		guiTools.scale(campoContrasena, true);
		guiTools.scale(campoNIP, true);
		guiTools.scale(campoASM, true);
		guiTools.scale(campoCVV, true);
		
		guiTools.scale(instruccionesContrasena, true);
		guiTools.scale(instruccionesNIP, true);
		guiTools.scale(instruccionesASM, true);
		guiTools.scale(instruccionesCVV, true);

		guiTools.scale(contenedorCampoTarjeta);
		guiTools.scale(tarjeta, true);
		guiTools.scale(campoTarjeta, true);
		guiTools.scale(instruccionesTarjeta, true);
			
		guiTools.scale(confirmarButton);
		
	}
	
	private void setTitle(final Intent intent ){
		
		final int titleText, iconResource;
		titleText = intent.getIntExtra(ApiConstants.TITLE_TEXT,0 );
		iconResource = intent.getIntExtra(ApiConstants.ICON_RESOURCE,0) ;
		setTitle(titleText, iconResource);
	}
	
	private void configuraPantalla() {
		presenter.getShowFields();

		final LinearLayout contenedorPadre = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);
		
		if (contenedorContrasena.getVisibility() == View.GONE &&
			contenedorNIP.getVisibility() == View.GONE &&
			contenedorASM.getVisibility() == View.GONE &&
			contenedorCVV.getVisibility() == View.GONE &&
			contenedorCampoTarjeta.getVisibility() == View.GONE) {
			contenedorPadre.setBackgroundColor(0);
		}
		
		contenedorPadre.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		//float camposHeight = contenedorPadre.getMeasuredHeight();

		final LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.confirmacion_lista_datos);
		layoutListaDatos.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		//float listaHeight = layoutListaDatos.getMeasuredHeight();

		final ViewGroup contenido = (ViewGroup)this.findViewById(android.R.id.content).getRootView();//findViewById(android.R.id.content);
		contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		//float contentHeight = contenido.getMeasuredHeight();

		//float margin = getResources().getDimension(R.dimen.confirmacion_fields_initial_margin);
		//float maximumSize = (contentHeight * 4) / 5;
		//float elementsSize = listaHeight + camposHeight;
		//float heightParaValidar = (contentHeight*3)/4;
		
		confirmarButton.setOnClickListener(this);
	}
	
	/*
	 * (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.views.ConfirmacionView#setListaDatos(java.util.ArrayList)
	 * acceso desde presenter
	 */
	@Override
	public void setListaDatos(final ArrayList<Object> datos) {

		final LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		final ListaDatosViewControllerCR listaDatos = new ListaDatosViewControllerCR(this, params);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		//listaDatos.setTitulo(R.string.confirmation_subtitulo);
		listaDatos.setTitulo(titleListaDatosView);
		listaDatos.showLista();
		final LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.confirmacion_lista_datos);
		layoutListaDatos.addView(listaDatos);
		
	}
	
	@Override 
	public void onClick(final View v) {
		if (v.equals(confirmarButton) && !isActivityChanging()) {
			botonConfirmarClick();
		}
	}
	
	//TODO go back setActivityChanging(true);
	
	private void botonConfirmarClick(){
		//setActivityChanging(true);
		
		viewTo.setContrasena(contrasena.getText().toString());
		viewTo.setTarjeta(tarjeta.getText().toString());
		viewTo.setNip(nip.getText().toString());
		viewTo.setAsm(asm.getText().toString());
		viewTo.setCvv(cvv.getText().toString());
		presenter.confirmarClick(viewTo);
	}
	
	public void showMensajePideContrasena(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if ( lenght ){
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.PASSWORD_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteContrasena));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteContrasena));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}
	}
	
	public void showMensajePideTarjeta(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			mensaje.append("Es necesario ingresar los últimos 5 dígitos de tu tarjeta");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append("Es necesario ingresar los últimos 5 dígitos de tu tarjeta");
			showInformationAlert(mensaje.toString());
		}
	}
	
	public void showMensajePideNip(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.NIP_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteNip));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteNip));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}
		
	}
	
	public void showMensajePideToken(final int msg, final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.ASM_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			/*
			 * switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
			 */
			mensaje.append(" ");
			mensaje.append(getString(msg));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(msg));
			
		/*	switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
		*/
		mensaje.append(".");
		showInformationAlert(mensaje.toString());
			
		}
	}
	
	public void showMensajePideCVV(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.CVV_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteCvv));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteCvv));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}
		
	}

	/*
	* acceso desde presenter
	*/
	public void mostrarContrasena(final boolean visibility){
		contenedorContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		contrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			//campoContrasena.setText(confirmacionDelegate.getEtiquetaCampoContrasenia());
			campoContrasena.setText(getString(R.string.confirmation_contrasena));
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
			contrasena.setFilters(userFilterArray);
			contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		instruccionesContrasena.setVisibility(View.GONE);
	}
	
	/*
	* 
	*/
	public void mostrarNIP(final boolean visibility){
		contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		nip.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			//campoNIP.setText(confirmacionDelegate.getEtiquetaCampoNip());
			campoNIP.setText(getString(R.string.confirmation_nip));
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
			nip.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
			//String instrucciones = confirmacionDelegate.getTextoAyudaNIP();
			final String instrucciones = getString(R.string.confirmation_autenticacion_ayudaNip);
			if ("".equals(instrucciones)) {
				instruccionesNIP.setVisibility(View.GONE);
			} else {
				instruccionesNIP.setVisibility(View.VISIBLE);
				instruccionesNIP.setText(instrucciones);
			}
		} else {
			nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}
	
	/*
	* 
	*/
	public void mostrarASM(final ConfirmacionViewTo to){
	//public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){	
		
		switch(to.getTokenAMostrar()) {
			case ninguno:
				contenedorASM.setVisibility(View.GONE);
				campoASM.setVisibility(View.GONE);
				asm.setVisibility(View.GONE);
				asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
				break;
			case codigo:
			case registro:
				contenedorASM.setVisibility(View.VISIBLE);
				campoASM.setVisibility(View.VISIBLE);
				asm.setVisibility(View.VISIBLE);
				final InputFilter[] userFilterArray = new InputFilter[1];
				userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);	
				asm.setFilters(userFilterArray);
				cambiarAccionTexto(contrasena);
				cambiarAccionTexto(tarjeta);
				cambiarAccionTexto(nip);
				asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
		}
		
		//Constants.TipoInstrumento tipoInstrumento = confirmacionDelegate.consultaTipoInstrumentoSeguridad();
		switch (to.getInstrumentoSeguridad()) {
			case OCRA:
				//campoASM.setText(confirmacionDelegate.getEtiquetaCampoOCRA());
				campoASM.setText(getString(R.string.confirmation_ocra));
				//asm.setTransformationMethod(null);
				break;
			case DP270:
				//campoASM.setText(confirmacionDelegate.getEtiquetaCampoDP270());
				campoASM.setText(getString(R.string.confirmation_dp270));
				//asm.setTransformationMethod(null);
				break;
			case SoftToken:
				if(SuiteApp.getSofttokenStatus()) {
					asm.setText(Constants.DUMMY_OTP);
					asm.setEnabled(false);
					campoASM.setText(getString(R.string.confirmation_softtokenActivado));
				} else {
					asm.setText("");
					asm.setEnabled(true);
					campoASM.setText(getString(R.string.confirmation_softtokenDesactivado));
					//asm.setTransformationMethod(null);
				}
				
				break;
			default:
				break;
		}
		
		//String instrucciones = confirmacionDelegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
		final String instrucciones = to.getTextoAyudaInsSeg();
		if ("".equals(instrucciones)) {
			instruccionesASM.setVisibility(View.GONE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}		
	}
	
	public void mostrarCampoTarjeta(final boolean visibility) {
		contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		tarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			//campoTarjeta.setText(confirmacionDelegate.getEtiquetaCampoTarjeta());
			campoTarjeta.setText(getString(R.string.confirmation_componente_digitos_tarjeta));
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(5);
			tarjeta.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
			//String instrucciones = confirmacionDelegate.getTextoAyudaTarjeta();
			final String instrucciones = getString(R.string.confirmation_ayuda_componente_digitos_tarjeta);
			if ("".equals(instrucciones)) {
				instruccionesTarjeta.setVisibility(View.GONE);
			} else {
				instruccionesTarjeta.setVisibility(View.VISIBLE);
				instruccionesTarjeta.setText(instrucciones);
			}
		} else {
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		
	}
	
	/*
	* 
	*/
	public void mostrarCVV(final boolean visibility){
		contenedorCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		cvv.setVisibility(visibility ? View.VISIBLE:View.GONE);
		instruccionesCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		
		if (visibility) {
			//campoCVV.setText(confirmacionDelegate.getEtiquetaCampoCVV());
			campoCVV.setText(getString(R.string.confirmation_CVV));
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
			cvv.setFilters(userFilterArray);
			//getString(R.string.confirmation_CVV_ayuda) .. 
			// el delegate correspondiente tiene return "";
			final String instrucciones = "";
			instruccionesCVV.setText(instrucciones);
			instruccionesCVV.setVisibility("".equals(instrucciones) ? View.GONE : View.VISIBLE);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(nip);
			cambiarAccionTexto(asm);
			cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}
	
	private void cambiarAccionTexto(final EditText campo) {
		if (campo.getVisibility() == View.VISIBLE) {
			campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
	}
	
	private String pideContrasena() {
		if (contrasena.getVisibility() == View.GONE) {
			return "";
		} else {
			return contrasena.getText().toString();
		}
	}
	
	private String pideNIP() {
		if (nip.getVisibility() == View.GONE) {
			return "";
		} else {
			return nip.getText().toString();
		}
	}
	
	private String pideASM() {
		if (asm.getVisibility() == View.GONE) {
			return "";
		} else {
			return asm.getText().toString();
		}
	}
	
	private String pideTarjeta(){
		if (tarjeta.getVisibility() == View.GONE) {
			return "";
		}else 
			return tarjeta.getText().toString();		
	}
	
	private String pideCVV() {
		if (cvv.getVisibility() == View.GONE) {
			return "";
		} else {
			return cvv.getText().toString();
		}
	}

	/**
	 * @return the viewVo
	 */
	public final ConfirmacionViewTo getViewTo() {
		return viewTo;
	}
	
	/**
	 * @param viewTo the viewVo to set
	 */
	public final void setViewTo(final ConfirmacionViewTo viewTo) {
		this.viewTo = viewTo;
	}

	@Override
	public void beforeTextChanged(final CharSequence s,final int start, final int count, final int after) {
	}

	@Override
	public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
	}

	@Override
	public void afterTextChanged(final Editable s) {
		onUserInteraction();
	}

}
