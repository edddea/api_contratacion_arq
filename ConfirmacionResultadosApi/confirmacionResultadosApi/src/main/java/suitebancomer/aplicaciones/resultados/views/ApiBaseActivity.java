/**
 * 
 */
package suitebancomer.aplicaciones.resultados.views;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent.Callback;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

import java.lang.reflect.Method;

import suitebancomer.aplicaciones.resultados.R;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ControlEventos;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

/**
 * @author lbermejo
 *
 */
public class ApiBaseActivity extends BaseViewControllerCommons implements Callback{
	private static Method methodOverridePendingTransition;
	static {
		final Method[] actMethods = Activity.class.getMethods();
		final int methodSize = actMethods.length;
		for (int i=0; i<methodSize; i++) {
			if (actMethods[i].getName().equals("overridePendingTransition")) {
				methodOverridePendingTransition = actMethods[i];
				break;
			}
		}
	}
	protected boolean habilitado = true;
	private int activityParameters;
	private boolean isActivityChanging;
	
	 /**
     * The alert error or information dialog.
     */
    private AlertDialog mAlertDialog;
	
    /**
     * A progress dialog for long waiting processes.
     */
    protected ProgressDialog mProgressDialog;
    
	/**
     * To use as parameter in aplicaEstilo method in order to show the header
     */
    public final static int SHOW_HEADER = 2;
    
    /**
     * To use as parameter in aplicaEstilo method in order to show the title
     */
    public final static int SHOW_TITLE = 4;

	/**
     * Referencia al layout del cabecero
     */
    private LinearLayout mHeaderLayout;
    
    /**
     * Referencia al layout del t�tulo
     */
    private LinearLayout mTitleLayout;
    
    /**
     * Referencia al separador del t�tulo
     */
    private ImageView mTitleDivider;
    
    /**
	 * Layout where each view will be drawn.
	 */
	private ViewGroup mBodyLayout;

	/**
	 * indica si es un flujo de estatus desactivado
	 */
	public boolean statusdesactivado;
	
    protected void onCreate(final Bundle savedInstanceState, final int parameters, final int layoutID) {
		try {
			methodOverridePendingTransition.invoke(this, new Object[]{R.anim.screen_enter, R.anim.screen_leave});
		} catch (final Exception e) {
			Log.d("APIConfirmacionResult",e.toString());
		}
    	super.onCreate(savedInstanceState);
    	getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		activityParameters = parameters;
		setContentView(R.layout.layout_base_activity_confirmacion_resultados);
		
		mHeaderLayout = (LinearLayout) findViewById(R.id.header_layout);
        mTitleLayout = (LinearLayout) findViewById(R.id.title_layout);
        mTitleDivider = (ImageView) findViewById(R.id.title_divider);
    	mBodyLayout = (ViewGroup) findViewById(R.id.body_layout);
    	
    	mHeaderLayout.setVisibility(((activityParameters & SHOW_HEADER) == SHOW_HEADER) ? View.VISIBLE : View.GONE);
        //set title visibility, by default is gone until a call to aplicaEstilo is made
        mTitleLayout.setVisibility(View.GONE);
        mTitleDivider.setVisibility(View.GONE);
        
    	// load received layout id in the body
		final LayoutInflater layoutInflater = (LayoutInflater)
        		getSystemService(SuiteAppCRApi.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(layoutID, mBodyLayout, true);
        
        scaleForCurrentScreen();
    }
    
    /**
	 * Establece la l�gica a seguir para el redimensionamiento de la pantalla.
	 */
	private void scaleForCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        guiTools.scale(mHeaderLayout);
		guiTools.scale(mTitleLayout);
		guiTools.scale(mTitleLayout.findViewById(R.id.title_icon));
		guiTools.scale(mTitleLayout.findViewById(R.id.title_text), true);
		guiTools.scale(mTitleDivider);
		guiTools.scale(mBodyLayout);
	}    
    
	 /**
     * Places the title string and respective icon on screen.
     * @param titleText the screen title
     * @param iconResource the resource of the screen icon
     */
    public void setTitle(final int titleText, final int iconResource) {
    	setTitle(titleText, iconResource, R.color.segundo_azul);
    }
    
    /**
     * Places the title string and respective icon on screen with the desired color
     * @param titleText the screen title
     * @param iconResource the resource of the screen icon
     */
    public void setTitle(final int titleText, final int iconResource, final int colorResource) {
    	
    	if ((activityParameters&SHOW_TITLE)==SHOW_TITLE) {
        	mTitleLayout.setVisibility(View.VISIBLE);
			final ImageView icon = (ImageView)mTitleLayout.findViewById(R.id.title_icon);
        	if(iconResource > 0){
        		//R.drawable.icono_activacion
				Drawable icnDrawable = null;
				try{
					icnDrawable = (SuiteApp.appContext.getResources().getDrawable(iconResource));
				} catch (Exception ex) {
					icnDrawable = null;
				}
				if(icnDrawable == null ){
					try {
						icnDrawable = SuiteAppCRApi.appContext.getResources().getDrawable(iconResource);
					}catch (Exception ex) {
							icnDrawable = null;
					}
				}
				if(icnDrawable != null){
					icon.setImageDrawable(icnDrawable);
				}
        	}

        	if(titleText > 0){
				final String title = getString(titleText);
				final TextView titleTextView = (TextView)mTitleLayout.findViewById(R.id.title_text);
        		titleTextView.setTextColor(getResources().getColor(colorResource));
        		titleTextView.setText(title);
        	}
        	mTitleDivider.setVisibility(View.VISIBLE);
        } else {
        	mTitleLayout.setVisibility(View.GONE);
        	mTitleDivider.setVisibility(View.GONE);
        }
    }
	
    /**
	 * @return the isActivityChanging
	 */
	public final boolean isActivityChanging() {
		return isActivityChanging;
	}


    /**
	 * the isActivityChanging
	 */
    public void setActivityChanging(final boolean flag) {
		isActivityChanging = flag;
	}
    
    
    // goBack() { setActivityChanging(true);
    
    /**
     * Shows an information alert with the given message text. No action is performed after this alert is closed.
     * @param message the message text to show
     */
    public void showInformationAlert(final String message) {
    	if (message.length() > 0) {
    		showInformationAlert(message, null);
    	}
    }
    
    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(final String message, final OnClickListener listener) {
    	showInformationAlert(getString(R.string.label_information), message, listener);
    }
    
    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param title The alert title resource id.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(final String title, final String message, final OnClickListener listener) {
    	showInformationAlert(title, message, getString(R.string.label_ok), listener);
    }
    
    /**
	 * Shows an alert dialog.
	 * @param title The alert title.
	 * @param message The alert message.
	 * @param okText the ok text
	 * @param listener The listener for the "Ok" button. 
	 */
	public void showInformationAlert(final String title, final String message, final String okText, final OnClickListener listener) {
		if(!habilitado)
			return;		
		habilitado = false;
    	if(message.length() > 0){
			final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    		alertDialogBuilder.setTitle(title);
    		alertDialogBuilder.setIcon(R.drawable.anicalertaaviso);    		
    		alertDialogBuilder.setMessage(message);
    		if (null == listener) {
				final OnClickListener listene = new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						dialog.dismiss();
					}
				};
				alertDialogBuilder.setPositiveButton(okText, listene);
    		}
			else{
				alertDialogBuilder.setPositiveButton(okText, listener);
			}
    		alertDialogBuilder.setCancelable(false);
    		mAlertDialog = alertDialogBuilder.create();
    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(final DialogInterface dialog) {
					habilitado = true;
				}
			});
    		mAlertDialog.show();
    	}
    }
    
	@Override
	public void onUserInteraction() {
		if(ServerCommons.ALLOW_LOG) {
			Log.i(getClass().getSimpleName(), "User Interacted");
		}
		if (SuiteAppCRApi.getCallBackSession() != null ){
			SuiteAppCRApi.getCallBackSession().userInteraction();
		} else if(SuiteAppCRApi.getCallBackBConnect() != null) {
			SuiteAppCRApi.getCallBackBConnect().userInteraction();
		}
		super.onUserInteraction();
	}
    
	private ScrollView getScrollView(){
		return(ScrollView)findViewById(R.id.body_layout);		
	}
	
	protected void moverScroll() {
		getScrollView().post(new Runnable() {

			@Override
			public void run() {
				getScrollView().fullScroll(ScrollView.FOCUS_UP);
				Log.d(getClass().getName(), "Mover scroll");
			}
		});		
	}
	
	/**
     * Places a progress dialog, for long waiting processes.
     * 
     * @param strTitle the dialog title
     * @param strMessage the message to show in the while
     */
    public void muestraIndicadorActividad(final String strTitle, final String strMessage) {
    	if(mProgressDialog != null){
    		ocultaIndicadorActividad();
    	}	
		mProgressDialog = ProgressDialog.show(this, strTitle, 
    			strMessage, true);
		mProgressDialog.setCancelable(false);
    }
    
    /**
     * Hides the progress dialog.
     */
    public void ocultaIndicadorActividad() {
    	if(mProgressDialog != null){
    		mProgressDialog.dismiss();
    	}
    }
	
 
	public void onErrorMsg() {
		showMessage(" Error !!! ");
	}

	public void onErrorMsg(final int idMensaje) {
		final Context mcontext=SuiteAppCRApi.appContext;
		final String message=mcontext.getResources().getString(idMensaje);
		showMessage(message);
	}
	
	public void showMessage(final String message) {
		showInformationAlert(message);
	}

	@Override
	protected void onStop() {
		if(ControlEventos.isAppIsInBackground(this.getBaseContext())){ //Valida el estado de la app si es change Activity no hace nada
			finish();
			if(ServerCommons.ALLOW_LOG) {
				Log.i(getClass().getSimpleName(), "La app se fue a Background");
			}
			if(SuiteAppCRApi.getCallBackSession() != null) {
				SuiteAppCRApi.getCallBackSession().cierraSesionBackground(Boolean.FALSE);
			} else if(SuiteAppCRApi.getCallBackBConnect() != null) {
				SuiteAppCRApi.getCallBackBConnect().cierraSesionBackground(Boolean.FALSE);
			}
		}
		super.onStop();
	}

	@Override
	protected void onPause() {
		super.onPause();
		if(SuiteApp.isReactivacion() && ControlEventos.isScreenOn(this)) {
			if(SuiteAppCRApi.getCallBackBConnect() != null) {
				SuiteApp.setIsReactivacion(Boolean.FALSE);
				SuiteAppCRApi.getCallBackBConnect().returDesactivada();
				finish();
				return;
			}
		}
		if(!ControlEventos.isScreenOn(this)) {
			if(ServerCommons.ALLOW_LOG) {
				Log.i(getClass().getSimpleName(), "Se ha bloqueado el telefono");
			}
			finish();
			if(SuiteAppCRApi.getCallBackSession() != null) {
				SuiteAppCRApi.getCallBackSession().cierraSesion();
			} else if(SuiteAppCRApi.getCallBackBConnect() != null) {
				SuiteAppCRApi.getCallBackBConnect().cierraSesion();
			}
		}
	}
}
