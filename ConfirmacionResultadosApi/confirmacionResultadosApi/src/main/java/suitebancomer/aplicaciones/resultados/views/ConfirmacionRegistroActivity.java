/**
 * 
 */
package suitebancomer.aplicaciones.resultados.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.resultados.R;
import suitebancomer.aplicaciones.resultados.commons.ApiConstants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.presenters.ConfirmacionRegistroPresenter;
import suitebancomer.aplicaciones.resultados.presenters.ConfirmacionRegistroPresenterImpl;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.views.ListaDatosViewControllerCR;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

/**
 * @author amgonzalez
 *
 */
public class ConfirmacionRegistroActivity extends ApiBaseActivity implements ConfirmacionRegistroView, OnClickListener, TextWatcher {
	
	private LinearLayout contenedorPrincipal;
	private LinearLayout contenedorContrasena;
	private LinearLayout contenedorNIP;
	private LinearLayout contenedorASM;
	private LinearLayout contenedorCVV;
	
	private TextView campoContrasena;
	private TextView campoNIP;
	private TextView campoASM;
	private TextView campoCVV;
	
	private EditText contrasena;
	private EditText nip;	
	private EditText asm;
	private EditText cvv;
	
	private TextView instruccionesContrasena;
	private TextView instruccionesNIP;
	private TextView instruccionesASM;
	private TextView instruccionesCVV;
	
	private ImageButton confirmarButton;
	
	private ConfirmacionRegistroPresenter presenter;
	private ConfirmacionViewTo viewTo;
	
	private static final String VIEW = "confreg";
	//private String titleListaDatosView;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		//setContentView(R.layout.layout_bmovil_confirmacion_act);
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_confirmacion_registro);
		setActivityChanging(getIntent().getBooleanExtra(ApiConstants.ACTIVITY_CHANGING, false));

		SuiteAppCRApi.getInstance().onCreate(getApplicationContext());
		SuiteApp.appContext=this;

		final ArrayList<String> param = getIntent()
				.getStringArrayListExtra(ApiConstants.TRACKSTATE_PARAMETERS);
		TrackingHelper.trackState(VIEW,param );
		
		presenter = new ConfirmacionRegistroPresenterImpl(this,getIntent());
		setTitle( getIntent() );
		
		findViews();
		scaleToScreenSize();
		presenter.getListaDatos();
		
		showDatosRegistroOk();
		configuraPantalla();
		moverScroll();
		
		contrasena.addTextChangedListener(this);
		nip.addTextChangedListener(this);
		asm.addTextChangedListener(this);
		cvv.addTextChangedListener(this);
		
	}

	@Override
	public void onBackPressed() {
		TrackingHelper.touchAtrasState();
		super.onBackPressed();
	}
		
	@Override 
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		//setActivityChanging(false);
		/*
		 * TODO onResume ConfirmacionRegistroActivity
		super.onResume();
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
		*/
	}
	
	@Override
    protected void onPause(){
        super.onPause();
        //TODO onPause ConfirmacionRegistroActivity
		//parentViewsController.consumeAccionesDePausa();
        //setActivityChanging(true);
	}
    
    /*
	 * (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.views.ConfirmacionView#reset()
	 * acceso desde presenter
	 */
	@Override
	public void reset() {
		this.contrasena.setText("");
		this.nip.setText("");
		if(this.asm.isEnabled())
			this.asm.setText("");
		this.cvv.setText("");
	}
	
	/**
	 * 
	 */
	private void findViews() {
		contenedorPrincipal		= (LinearLayout)findViewById(R.id.confirmacion_lista_datos);
		contenedorContrasena	= (LinearLayout)findViewById(R.id.campo_confirmacion_contrasena_layout);
		contenedorNIP 			= (LinearLayout)findViewById(R.id.campo_confirmacion_nip_layout);
		contenedorASM 			= (LinearLayout)findViewById(R.id.campo_confirmacion_asm_layout);
		contenedorCVV 			= (LinearLayout)findViewById(R.id.campo_confirmacion_cvv_layout);
		
		contrasena 				= (EditText)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
		nip 					= (EditText)contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
		asm						= (EditText)contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
		cvv						= (EditText)contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);
		
		campoContrasena			= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_label);
		campoNIP				= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_label);
		campoASM				= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_label);
		campoCVV				= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_label);
		
		
		instruccionesContrasena	= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
		instruccionesNIP		= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
		instruccionesASM		= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
		instruccionesCVV		= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);
		
		confirmarButton 		= (ImageButton)findViewById(R.id.confirmacion_confirmar_button);
	}
	
	/**
	 * 
	 */
	private void scaleToScreenSize() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(contenedorPrincipal);
		guiTools.scale(findViewById(R.id.confirmacion_campos_layout));
		
		guiTools.scale(contenedorContrasena);
		guiTools.scale(contenedorNIP);
		guiTools.scale(contenedorASM);
		guiTools.scale(contenedorCVV);
		
		guiTools.scale(contrasena, true);
		guiTools.scale(nip, true);
		guiTools.scale(asm, true);
		guiTools.scale(cvv, true);
		
		guiTools.scale(campoContrasena, true);
		guiTools.scale(campoNIP, true);
		guiTools.scale(campoASM, true);
		guiTools.scale(campoCVV, true);
		
		guiTools.scale(instruccionesContrasena, true);
		guiTools.scale(instruccionesNIP, true);
		guiTools.scale(instruccionesASM, true);
		guiTools.scale(instruccionesCVV, true);
		
		guiTools.scale(confirmarButton);
		
	}
	
	/**
	 * 
	 * @param intent
	 */
	private void setTitle(final Intent intent ){
		
		final int titleText, iconResource;
		titleText = intent.getIntExtra(ApiConstants.TITLE_TEXT, 0);
		iconResource = intent.getIntExtra(ApiConstants.ICON_RESOURCE,0);
		setTitle(titleText, iconResource);
	}
	
	private void showDatosRegistroOk(){
		presenter.getDatosRegistroExitoso();
	}
	
	/**
	 * 
	 */
	private void configuraPantalla() {
		presenter.getShowFields();
		final LinearLayout contenedorPadre = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);
		
		if (contenedorContrasena.getVisibility() == View.GONE &&
			contenedorNIP.getVisibility() == View.GONE &&
			contenedorASM.getVisibility() == View.GONE &&
			contenedorCVV.getVisibility() == View.GONE) {
			contenedorPadre.setBackgroundColor(0);			
		}		
		confirmarButton.setOnClickListener(this);
	}
	
	/**
	 * 
	 */
	public void mostrarDatosRegistro(final ArrayList<Object> datos){
		
		//delegate.getDatosRegistroExitoso();
		final LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		final ListaDatosViewControllerCR listaDatos = new ListaDatosViewControllerCR(this, params);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.setTitulo(getString(R.string.confirmation_subtitulo));
		listaDatos.showLista();
		final LinearLayout layoutRegistro = (LinearLayout)findViewById(R.id.confirmacion_lista_datos_reg_op);
		layoutRegistro.addView(listaDatos);
		
	}
	
	/**
	 * 
	 * acceso desde presenter
	 */
	@Override
	public void setListaDatos(final ArrayList<Object> datos) {

		final LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		final ListaDatosViewControllerCR listaDatos = new ListaDatosViewControllerCR(this, params);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.setTitulo(getString(R.string.confirmation_subtitulo_op));
		listaDatos.showLista();
		final LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.confirmacion_lista_datos);
		layoutListaDatos.addView(listaDatos);
		
	}
	
	@Override 
	public void onClick(final View v) {
		if (v.equals(confirmarButton) && !isActivityChanging()) {
			botonConfirmarClick();
		}
	}
	
	//TODO go back setActivityChanging(true);
	
	private void botonConfirmarClick(){
		//setActivityChanging(true);

		viewTo.setContrasena(contrasena.getText().toString());
		viewTo.setNip(nip.getText().toString());
		viewTo.setAsm(asm.getText().toString());
		viewTo.setCvv(cvv.getText().toString());
		
		presenter.confirmarClick(viewTo);
	}
	
	//TODO processNetworkResponse
	public void processNetworkResponse( ){
		//	int operationId, ServerResponse response) {
		//confirmacionAutenticacionDelegate
		//		.analyzeResponse(operationId, response);
		//delegate.analyzeResponse(operationId, response);
	}
	
	public void onSuccess(final int idTextoEncabezado) {

		final Map<String,Object> operacionRealizadaMap = new HashMap<String, Object>();
		//Comprobacion de titulos
		final String titulo = getString(idTextoEncabezado);
		if( titulo == getString(R.string.opcionesTransfer_menu_miscuentas))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;transferencias+mis cuentas");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		else if( titulo == getString(R.string.opcionesTransfer_menu_otrascuentasbbva))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;transferencias+otra cuenta bbva bancomer");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		else if( titulo == getString(R.string.opcionesTransfer_menu_cuentaexpress))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;transferencias+cuenta express");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		else if( titulo == getString(R.string.opcionesTransfer_menu_otrosbancos))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;transferencias+otros bancos");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		else if( titulo == getString(R.string.opcionesTransfer_menu_dineromovil))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;transferencias+dinero movil");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		else if( titulo == getString(R.string.transferir_otrosBBVA_TDC_title))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;pagar+tarjeta credito");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		else if( titulo == getString(R.string.tiempo_aire_title))
		{
			//ARR
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products", "operaciones;comprar+tiempo aire");
			operacionRealizadaMap.put("eVar12", "operacion realizada");

			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}

		reset();
	}
	
	public void showMensajePideContrasena(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if ( lenght ){
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.PASSWORD_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteContrasena));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteContrasena));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}
	}
	
	public void showMensajePideNip(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			mensaje.append((R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.NIP_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteNip));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteNip));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}
	}
	
	public void showMensajePideToken(final int msg, final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.ASM_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			/*
			 * switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
			 */
			mensaje.append(" ");
			mensaje.append(getString(msg));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(msg));
			
		/*	switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
		*/
		mensaje.append(".");
		showInformationAlert(mensaje.toString());
			
		}
	}
	
	public void showMensajePideCVV(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.CVV_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteCvv));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteCvv));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}
	}

	/**
	 * acceso desde presenter
	 */
	public void mostrarContrasena(final boolean visibility){
		contenedorContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoContrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		contrasena.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoContrasena.setText(getString(R.string.confirmation_contrasena));
			InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
			contrasena.setFilters(userFilterArray);
			contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		instruccionesContrasena.setVisibility(View.GONE);
	}
	
	/**
	 * 
	 */
	public void mostrarNIP(final boolean visibility){
		contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		nip.setVisibility(visibility ? View.VISIBLE:View.GONE);
		if (visibility) {
			campoNIP.setText(getString(R.string.confirmation_nip));
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
			nip.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
			final String instrucciones = getString(R.string.confirmation_ayudaNip);
			if ("".equals(instrucciones)) {
				instruccionesNIP.setVisibility(View.GONE);
			} else {
				instruccionesNIP.setVisibility(View.VISIBLE);
				instruccionesNIP.setText(instrucciones);
			}
		} else {
			nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}
	
	/**
	 * 
	 */
	public void mostrarASM(final ConfirmacionViewTo to){
	//public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){	
		
		switch(to.getTokenAMostrar()) {
			case ninguno:
				contenedorASM.setVisibility(View.GONE);
				campoASM.setVisibility(View.GONE);
				asm.setVisibility(View.GONE);
				asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
				break;
			case codigo:
			case registro:
				contenedorASM.setVisibility(View.VISIBLE);
				campoASM.setVisibility(View.VISIBLE);
				asm.setVisibility(View.VISIBLE);
				final InputFilter[] userFilterArray = new InputFilter[1];
				userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);	
				asm.setFilters(userFilterArray);
				cambiarAccionTexto(contrasena);
				cambiarAccionTexto(nip);
				asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
				break;
		}
		
		//Constants.TipoInstrumento tipoInstrumento = confirmacionDelegate.consultaTipoInstrumentoSeguridad();
		switch (to.getInstrumentoSeguridad()) {
			case OCRA:
				//campoASM.setText(confirmacionDelegate.getEtiquetaCampoOCRA());
				campoASM.setText(getString(R.string.confirmation_ocra));
				//asm.setTransformationMethod(null);
				break;
			case DP270:
				//campoASM.setText(confirmacionDelegate.getEtiquetaCampoDP270());
				campoASM.setText(getString(R.string.confirmation_dp270));
				//asm.setTransformationMethod(null);
				break;
			case SoftToken:
				if(SuiteApp.getSofttokenStatus()) {
					asm.setText(Constants.DUMMY_OTP);
					asm.setEnabled(false);
					campoASM.setText(getString(R.string.confirmation_softtokenActivado));
				} else {
					asm.setText("");
					asm.setEnabled(true);
					campoASM.setText(getString(R.string.confirmation_softtokenDesactivado));
					//asm.setTransformationMethod(null);
				}
				
				break;
			default:
				break;
		}
		
		//String instrucciones = confirmacionDelegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
		final String instrucciones = to.getTextoAyudaInsSeg();
		if ("".equals(instrucciones)) {
			instruccionesASM.setVisibility(View.GONE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}		
	}
	
	/**
	 * 
	 */
	public void mostrarCVV(final boolean visibility){
		contenedorCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		cvv.setVisibility(visibility ? View.VISIBLE:View.GONE);
		instruccionesCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		
		if (visibility) {
			//campoCVV.setText(confirmacionDelegate.getEtiquetaCampoCVV());
			campoCVV.setText(getString(R.string.confirmation_CVV));
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
			cvv.setFilters(userFilterArray);
			final String instrucciones = getString(R.string.confirmation_CVV_ayuda);;
			instruccionesCVV.setText(instrucciones);
			instruccionesCVV.setVisibility("".equals(instrucciones) ? View.GONE : View.VISIBLE);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(nip);
			cambiarAccionTexto(asm);
			cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}
	
	//
	private void cambiarAccionTexto(final EditText campo) {
		if (campo.getVisibility() == View.VISIBLE) {
			campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
	}
	
	
	/**
	 * @return the viewVo
	 */
	public final ConfirmacionViewTo getViewTo() {
		return viewTo;
	}

	/**
	 * @param viewVo the viewVo to set
	 */
	public final void setViewTo(final ConfirmacionViewTo viewTo) {
		this.viewTo = viewTo;
	}

	@Override
	public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
	}

	@Override
	public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
	}

	@Override
	public void afterTextChanged(final Editable s) {
		onUserInteraction();
	}
	
}
