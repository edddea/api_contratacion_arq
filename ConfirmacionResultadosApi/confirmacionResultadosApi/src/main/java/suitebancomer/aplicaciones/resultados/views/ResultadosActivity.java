/**
 * 
 */
package suitebancomer.aplicaciones.resultados.views;

import java.util.ArrayList;

import suitebancomer.aplicaciones.resultados.R;
import suitebancomer.aplicaciones.resultados.commons.ApiConstants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.presenters.ResultadosPresenter;
import suitebancomer.aplicaciones.resultados.presenters.ResultadosPresenterImpl;
import suitebancomer.aplicaciones.resultados.to.ResultadosViewTo;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.views.ListaDatosViewControllerCR;
import suitebancomer.classes.gui.views.ListaSeleccionViewControllerCR;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.bancomer.base.SuiteApp;

/**
 * @author lbermejo
 *
 */
public class ResultadosActivity extends ApiBaseActivity implements ResultadosView, OnClickListener {
	
	private TextView tituloResultados;
	private TextView textoResultados;
	private TextView instruccionesResultados;
	private TextView titulotextoEspecialResultados;
	private TextView textoEspecialResultados;
	private ImageButton menuButton;
	private ImageButton compartirButton;
	
	//AMZ
	//public BmovilViewsController parentManager;
	//AMZ
	
	private ResultadosPresenter presenter;
	private ResultadosViewTo resultadosViewTo;
	private int opcionesMenu;
	private boolean frecOpOk;
	
	//private static final String VIEW = "confirma";
	//private String titleListaDatosView;
	private ArrayList<String> paramState;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		//setContentView(R.layout.layout_bmovil_confirmacion_act);
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_resultados);
		setActivityChanging(getIntent().getBooleanExtra(ApiConstants.ACTIVITY_CHANGING, false));
		
		/**En el suite Api ******/	 	
		//setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		/********/
		SuiteAppCRApi.getInstance().onCreate(getApplicationContext());
		SuiteApp.appContext=this;
		paramState = getIntent().getStringArrayListExtra(ApiConstants.TRACKSTATE_PARAMETERS);
		/*
		titleListaDatosView = getIntent()
				.getStringExtra(ApiConstants.LISTA_DATOS_CONFIRM_SUBTITULO);
		*/
		presenter = new ResultadosPresenterImpl(this,getIntent()); 
		findViews();
		scaleToScreenSize();
		
		menuButton.setOnClickListener(this);
		compartirButton.setOnClickListener(this);
		setTitle( getIntent() );
		
		//TODO se encuentra en configurar pantalla
		//TrackingHelper.trackState(VIEW,param );
		
		presenter.getListaDatos();////*******************************
		presenter.getShowFields(); // --configuraPantalla();
		moverScroll();

	}
	
	//TODO onResume ResultadosActivity
	@Override 
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		setActivityChanging(false);
		
		/*
		 * if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		parentViewsController.setCurrentActivityApp(this);
		 */
	}
	
	//TODO onPause ResultadosActivity
	@Override
    protected void onPause(){
        super.onPause();
        // example
        
        /*if(resultadosDelegate.getmBroadcastReceiver() != null ){
    		unregisterReceiver(resultadosDelegate.getmBroadcastReceiver());
    		resultadosDelegate.setmBroadcastReceiver(null);
    	}
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
		*/
    }
    
    @Override
	public void goBack() {
		//Se supone que el boton de atras no haga nada
    	//go back setActivityChanging(true);
		if(ServerCommons.ALLOW_LOG){
			Log.d("ConfRes-ResActiv", " goBack() no debe hacer nada");}
	}

	public void onBackPressed(){
		//Se supone que el boton de atras no haga nada
		//go back setActivityChanging(true);
		if(ServerCommons.ALLOW_LOG){
			Log.d("ConfRes-ResActiv", " onBackPressed() no debe hacer nada");}

	}

	@Override
	public boolean onKeyDown(final int keyCode, final KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			//preventing default implementation previous to android.os.Build.VERSION_CODES.ECLAIR
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

    private void setTitle(final Intent intent ){
		final int titleText, iconResource;
		titleText = intent.getIntExtra(ApiConstants.TITLE_TEXT, 0);
		iconResource = intent.getIntExtra(ApiConstants.ICON_RESOURCE,0);
		setTitle(titleText, iconResource);
	}
    
    private void findViews() {
    	
    	tituloResultados = (TextView)findViewById(R.id.resultado_titulo);
    	textoResultados = (TextView)findViewById(R.id.resultado_texto);
    	instruccionesResultados = (TextView)findViewById(R.id.resultado_instrucciones);
    	textoEspecialResultados = (TextView) findViewById(R.id.resultado_texto_especial);
    	titulotextoEspecialResultados = (TextView) findViewById(R.id.resultado_titulo_texto_especial);
    	menuButton = (ImageButton)findViewById(R.id.resultados_menu_button);
    	//Mejoras Bmovil
    	compartirButton = (ImageButton)findViewById(R.id.resultados_compartir);
    	
    }

	private void scaleToScreenSize() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(tituloResultados, true);
		guiTools.scale(textoResultados, true);
		guiTools.scale(instruccionesResultados, true);
		guiTools.scale(textoEspecialResultados, true);
		guiTools.scale(titulotextoEspecialResultados, true);
		guiTools.scale(menuButton);
		
	}
    
	/*
	 * (non-Javadoc)
	 * 
	 * @see suitebancomer.aplicaciones.resultados.views.ConfirmacionView#setListaDatos(java.util.ArrayList) acceso desde presenter
	 */
	@Override
	public void setListaDatos(final ArrayList<Object> datos) {

		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(findViewById(R.id.resultados_lista_datos));

		final LinearLayout.LayoutParams params = new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		final ListaDatosViewControllerCR listaDatos = new ListaDatosViewControllerCR(
				this, params); // , parentViewsController para el titulo
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.showLista();
		final LinearLayout layoutListaDatos = (LinearLayout) findViewById(R.id.resultados_lista_datos);
		layoutListaDatos.addView(listaDatos);

	}
	
	public void setListaClave(final ArrayList<Object> datos, final Boolean isConsultaInterbancario) {


		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scalePaddings(findViewById(R.id.resultados_lista_clave));

		final LinearLayout.LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		/*
		 * ListaSeleccionViewControllerCR(Context context,LinearLayout.LayoutParams layoutParams, 
			BaseViewsControllerCommons parentManager, Boolean isConsultaInterbancario
		 */

		final ListaSeleccionViewControllerCR listaSeleccion = new ListaSeleccionViewControllerCR(this, params,
				SuiteAppCRApi.getInstance().getBaseControllersViews(), isConsultaInterbancario );
			
			listaSeleccion.setDelegate(presenter.getInteractor().getProxy().getDelegate() );
			listaSeleccion.setFijarLista(true);
			listaSeleccion.setNumeroColumnas(1);
			listaSeleccion.setEncabezado(null);
			listaSeleccion.setLista(datos);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(datos.size());
			listaSeleccion.setExisteFiltro(false);
			listaSeleccion.cargarTabla();
			final LinearLayout layoutListaDatos = (LinearLayout)findViewById(R.id.resultados_lista_clave);
			layoutListaDatos.addView(listaSeleccion);
			
		
			findViewById(R.id.resultados_lista_clave).setVisibility(View.VISIBLE);
	}
	
	public void configuraPantalla() {
		
		//getTextoTituloResultado
		final String titulo = resultadosViewTo.getTitulo();
		//getTextoPantallaResultados
		final String texto = resultadosViewTo.getTexto();
		//getTextoAyudaResultados
		final String instrucciones = resultadosViewTo.getInstrucciones();
		//getTituloTextoEspecialResultados
		final String tituloTextoEspecial = resultadosViewTo.getTituloTextoEspecial();
		//getTextoEspecialResultados
		final String textoEspecial = resultadosViewTo.getTextoEspecial();
		
		if ("".equals(titulo)) {
			tituloResultados.setVisibility(View.GONE);
		} else {
			tituloResultados.setText(titulo);
			//resultadosDelegate.getColorTituloResultado()
			tituloResultados.setTextColor(getResources().getColor(
					resultadosViewTo.getColorTituloResultado()));
		}
		
		if ("".equals(texto)) {
			textoResultados.setVisibility(View.GONE);
		} else {
			textoResultados.setText(texto);
		}
		
		if ("".equals(instrucciones)) {
			instruccionesResultados.setVisibility(View.GONE);
		} else {
			instruccionesResultados.setText(instrucciones);
		}
		
		if ("".equals(tituloTextoEspecial)) {
			titulotextoEspecialResultados.setVisibility(View.GONE);
		}else {
			titulotextoEspecialResultados.setText(tituloTextoEspecial);
		}
		
		if ("".equals(textoEspecial)) {
			textoEspecialResultados.setVisibility(View.GONE);
		} else {
			textoEspecialResultados.setText(textoEspecial);
		}
		
		//Mejoras Bmovil
		if(instruccionesResultados.getText() == ""){
			compartirButton.setVisibility(View.INVISIBLE);
		}
		
		if(titulo == this.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_operacion_cancelada)){
			TrackingHelper.trackState("cancelada", paramState);
		}else if(titulo == this.getString(R.string.transferir_detalle_operacion_exitosaTitle)){
			TrackingHelper.trackState("resul", paramState);
		}else{
			TrackingHelper.trackState("resul", paramState);
		}
	
		//One click
		if(!"".equals(textoEspecial)&& "".equals(tituloTextoEspecial)&& !"".equals(instrucciones)){
			final String text= textoEspecial;
			final SpannableString textS= new SpannableString(text);
			/*AbsoluteSizeSpan headerSpan = new AbsoluteSizeSpan(18);
			textS.setSpan(headerSpan, 0, text.length(), 0);*/
			textS.setSpan(new ForegroundColorSpan(Color.rgb(77,77,77)),0,text.length(),0);
			textoEspecialResultados.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
			textoEspecialResultados.setText(textS);
			final String text2= instrucciones;
			final SpannableString textS2= new SpannableString(text2);
			/*AbsoluteSizeSpan headerSpan2 = new AbsoluteSizeSpan(14);
			textS2.setSpan(headerSpan2, 0, text2.length(), 0);*/
			textS2.setSpan(new ForegroundColorSpan(Color.rgb(176,176,176)),0,text2.length(),0);
			instruccionesResultados.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);
			instruccionesResultados.setText(textS2);
			titulotextoEspecialResultados.setVisibility(View.GONE);
		}
		//Termina one CLlick
	}
    
	@Override 
	public void onClick(final View v) {
		if (v.equals(menuButton) && !isActivityChanging()) {
			botonMenuClick();
		}else if (v.equals(compartirButton)){
			openOptionsMenu();
		}
	}
    
	private void botonMenuClick() {
		// setActivityChanging(true);
		presenter.botonMenuClick();
		/*
		//AMZ    en el proxy
		((BmovilViewsController)parentViewsController).touchMenu();
		parentViewsController.removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
		parentViewsController.removeDelegateFromHashMap(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
		((BmovilViewsController)parentViewsController).showMenuPrincipal(true);
		 */
		
	}
 
    public void onSuccess(){
    	//reset();
    }
	
	public BroadcastReceiver createBroadcastReceiver() {
    	
	     return new BroadcastReceiver() {
			@Override
		     public void onReceive(final Context ctx, final Intent intent) {

				final String toastMessage;
		    	 
                final int resultCode = getResultCode();
				if (resultCode == Activity.RESULT_OK) {
					toastMessage = getString(R.string.sms_success);
				} else if (resultCode == SmsManager.RESULT_ERROR_NO_SERVICE) {
					toastMessage = getString(R.string.sms_error_noService);
				} else if (resultCode == SmsManager.RESULT_ERROR_NULL_PDU) {
					toastMessage = getString(R.string.sms_error_nullPdu);
				} else if (resultCode == SmsManager.RESULT_ERROR_RADIO_OFF) {
					toastMessage = getString(R.string.sms_error_radioOff);
				} else if (resultCode == SmsManager.RESULT_ERROR_GENERIC_FAILURE || true) {
					toastMessage = getString(R.string.sms_error);
				}
			     
			     ocultaIndicadorActividad();
			     Toast.makeText(getBaseContext(), toastMessage,Toast.LENGTH_SHORT).show();
		     }
	     };
	}
	
	public void setOpcionesMenu(final ResultadosViewTo to){
		this.opcionesMenu = to.getOpcionesMenu();
		this.frecOpOk = to.getFrecOpOk();
	}
	
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {

		final MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_bmovil_resultados, menu);

		//AMZ
		presenter.setParamStateParentManager();
		/*
		 * en el Proxy
		int opc = paramState.size() - 1;
		int opc2 = paramState.size() - 2;
		if (paramState.get(opc) == "opciones") {
			String rem = paramState.remove(opc);
		} else if (parentManager.estados.get(opc2) == "sms"
				|| parentManager.estados.get(opc2) == "correo"
				|| parentManager.estados.get(opc2) == "alta frecuentes") {
			String rem = parentManager.estados.remove(opc2);
			rem = parentManager.estados
					.remove(parentManager.estados.size() - 1);
		} else {
			TrackingHelper.trackState("opciones", parentManager.estados);
		}
		*/
				
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(final Menu menu) {
		//int opcionesMenu = resultadosDelegate.getOpcionesMenuResultados();
		presenter.getOnPrepareOptionsMenu();
		boolean showMenu = false;
		
		if ((opcionesMenu & ApiConstants.SHOW_MENU_SMS) == ApiConstants.SHOW_MENU_SMS) {
			showMenu = true;
		} else {
			menu.removeItem(R.id.save_menu_sms_button);
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & ApiConstants.SHOW_MENU_EMAIL) == ApiConstants.SHOW_MENU_EMAIL) {
			showMenu = true;
		} else {
			menu.removeItem(R.id.save_menu_email_button);
			//menu.removeItem(R.id.save_menu_email_button);//TODO remover para mostrar menu completo
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & ApiConstants.SHOW_MENU_PDF) == ApiConstants.SHOW_MENU_PDF) {
			menu.removeItem(R.id.save_menu_pdf_button);
		} else {
			//showMenu = true;
			menu.removeItem(R.id.save_menu_pdf_button);//TODO remover para mostrar menu completo
		}
		if (frecOpOk || ((opcionesMenu & ApiConstants.SHOW_MENU_FRECUENTE) 
				!= ApiConstants.SHOW_MENU_FRECUENTE)) {
			menu.removeItem(R.id.save_menu_frecuente_button);
		} else {
			showMenu = true;
		}
		//TODO remover para mostrar menu completo
		if ((opcionesMenu & ApiConstants.SHOW_MENU_RAPIDA) == ApiConstants.SHOW_MENU_RAPIDA) {
			menu.removeItem(R.id.save_menu_rapida_button);
		} else {
			//showMenu = true;
			menu.removeItem(R.id.save_menu_rapida_button);//TODO remover para mostrar menu completo
		}
		if ((opcionesMenu & ApiConstants.SHOW_MENU_BORRAR) == ApiConstants.SHOW_MENU_BORRAR) {
			showMenu = true;
		} else {
			menu.removeItem(R.id.save_menu_borrar_button);
		}
		
		return showMenu;
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {

		final int itemId = item.getItemId();
		
		return presenter.optionsItemSelected(itemId);
		//ARR
		/*
		 * En el proxy
		int itemId = item.getItemId();
		Map<String,Object> envioConfirmacionMap = new HashMap<String, Object>();
		
		if (itemId == R.id.save_menu_sms_button) {
			resultadosDelegate.enviaSMS();
			//AMZ
			int sms = parentManager.estados.size()-1;
			if(parentManager.estados.get(sms)=="resul"){
				String rem = parentManager.estados.remove(sms);
				rem = parentManager.estados.remove(parentManager.estados.size()-1);
			}
			TrackingHelper.trackState("sms", parentManager.estados);
			TrackingHelper.trackState("resul", parentManager.estados);
			if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_miscuentas))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+mis cuentas");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrascuentasbbva))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+otra cuenta bbva bancomer");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_cuentaexpress))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+cuenta express");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrosbancos))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+otros bancos");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_dineromovil))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "sms:transferencias+dinero movil");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			return true;
		} else if (itemId == R.id.save_menu_email_button) {
			resultadosDelegate.enviaEmail();
			//AMZ
			int correo = parentManager.estados.size()-1;
			if(parentManager.estados.get(correo)=="resul"){
				String rem = parentManager.estados.remove(correo);
				rem = parentManager.estados.remove(parentManager.estados.size()-1);
			}
			if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_miscuentas))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+mis cuentas");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrascuentasbbva))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+otra cuenta bbva bancomer");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_cuentaexpress))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+cuenta express");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_otrosbancos))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+otros bancos");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			else if(getString(resultadosDelegate.consultaOperationDelegate().getTextoEncabezado()) == getString(R.string.opcionesTransfer_menu_dineromovil))
			{
				//ARR
				envioConfirmacionMap.put("events", "event17");
				envioConfirmacionMap.put("eVar17", "correo:transferencias+dinero movil");

				TrackingHelper.trackEnvioConfirmacion(envioConfirmacionMap);
			}
			return true;
		} else if (itemId == R.id.save_menu_pdf_button) {
			resultadosDelegate.guardaPDF();
			return true;
		} else if (itemId == R.id.save_menu_frecuente_button) {
			resultadosDelegate.guardaFrecuente();
			//AMZ
			int frec = parentManager.estados.size()-1;
			if(parentManager.estados.get(frec)=="resul"){	
				String rem = parentManager.estados.remove(frec);
				rem = parentManager.estados.remove(parentManager.estados.size()-1);
			}
			return true;
		} else if (itemId == R.id.save_menu_rapida_button) {
			resultadosDelegate.guardaRapido();
			return true;
		} else if (itemId == R.id.save_menu_borrar_button) {
			resultadosDelegate.borraRapido();
			return true;
		} else {
			return false;
		}
		
		*/
	
	}
	
	/**
	 * @return the viewVo
	 */
	public final ResultadosViewTo getResultadosViewTo() {
		return this.resultadosViewTo;
	}
	
	/**
	 * @param viewTo the viewVo to set
	 */
	public final void setResultadosViewTo(final ResultadosViewTo viewTo) {
		this.resultadosViewTo = viewTo;
	}

	/*
	 * TODO
	 * 
	 * @Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		resultadosDelegate.analyzeResponse(operationId, response);
	}
	 */
}
