/**
 * 
 */
package suitebancomer.aplicaciones.resultados.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.resultados.R;
import suitebancomer.aplicaciones.resultados.commons.ApiConstants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.presenters.ConfirmacionAutenticacionPresenter;
import suitebancomer.aplicaciones.resultados.presenters.ConfirmacionAutenticacionPresenterImpl;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionViewTo;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.views.ListaDatosViewControllerCR;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

import android.content.Intent;

import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;

import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.bancomer.base.SuiteApp;

/**
 * @author lbermejo
 *
 */
public class ConfirmacionAutenticacionActivity extends ApiBaseActivity implements ConfirmacionAutenticacionView, OnClickListener, TextWatcher {


	private LinearLayout contenedorCampoTarjeta;
	private LinearLayout contenedorPrincipal;
	private LinearLayout contenedorPadre;
	private LinearLayout contenedorContrasena;
	private LinearLayout contenedorNIP;
	private LinearLayout contenedorASM;
	private LinearLayout contenedorCVV;
	
	private TextView campoTarjeta;
	private TextView campoContrasena;
	private TextView campoNIP;
	private TextView campoASM;
	private TextView campoCVV;
	
	private EditText tarjeta;
	private EditText contrasena;
	private EditText nip;	
	private EditText asm;
	private EditText cvv;
	
	private TextView instruccionesTarjeta;
	private TextView instruccionesContrasena;
	private TextView instruccionesNIP;
	private TextView instruccionesASM;
	private TextView instruccionesCVV;
	
	private ImageButton confirmarButton;
	
	private ConfirmacionAutenticacionPresenter presenter;
	private ConfirmacionViewTo viewTo;
	
	private static final String VIEW = "confirma";

	@Override
	protected void onCreate(final Bundle savedInstanceState) {

		//setContentView(R.layout.layout_bmovil_confirmacion_act);
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.layout_bmovil_confirmacion);
		setActivityChanging(getIntent().getBooleanExtra(ApiConstants.ACTIVITY_CHANGING, false));

		SuiteAppCRApi.getInstance().onCreate(getApplicationContext());
		SuiteApp.appContext=this;

		presenter = new ConfirmacionAutenticacionPresenterImpl(this,getIntent()); 
		setTitle(getIntent());
		findViews();
		scaleToScreenSize();
		
		presenter.getListaDatos();
		configuraPantalla();
		moverScroll();
		
		this.statusdesactivado =  getIntent().getBooleanExtra(
				ApiConstants.CONF_AUTENT_STATUS_DESACTIVADO, false);
		/*
		 * this.statusdesactivado = !(
				(confirmacionAutenticacionDelegate
				.getOperationDelegate() instanceof QuitarSuspencionDelegate)
				|| (confirmacionAutenticacionDelegate.getOperationDelegate() 
						instanceof NuevaContraseniaDelegate) 
				|| (confirmacionAutenticacionDelegate
				.getOperationDelegate() instanceof ReactivacionDelegate));
		 */

		final ArrayList<String> param = getIntent()
				.getStringArrayListExtra(ApiConstants.TRACKSTATE_PARAMETERS);
		//if(!(confirmacionAutenticacionDelegate.getOperationDelegate() instanceof ReactivacionDelegate)){
			//TrackingHelper.trackState("reactivacion", parentManager.estados);
			//}else{
			TrackingHelper.trackState(VIEW, param);
		//}

		contrasena.addTextChangedListener(this);
		nip.addTextChangedListener(this);
		asm.addTextChangedListener(this);
		cvv.addTextChangedListener(this);
		tarjeta.addTextChangedListener(this);
		
	}

	@Override
	public void onBackPressed() {
		TrackingHelper.touchAtrasState();
		super.onBackPressed();
	}
		
	@Override 
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext = this;
		//setActivityChanging(false);
		habilitarBtnContinuar();
		/*
		 * TODO onResume ConfirmacionAutenticacionActivity
		habilitarBtnContinuar();
		if (statusdesactivado)
			if (!(confirmacionAutenticacionDelegate
					.getOperationDelegate() instanceof MantenimientoAlertasDelegate)) {
				if (parentViewsController.consumeAccionesDeReinicio()) {
					return;
				}
			}
		getParentViewsController().setCurrentActivityApp(this);
		*/
	}
	
	@Override
    protected void onPause(){
        super.onPause();
        //TODO onPause ConfirmacionAutenticacionActivity
		//parentViewsController.consumeAccionesDePausa();
		//statusdesactivado = true;
        setActivityChanging(true);
	}
    
	@Override
	protected void onDestroy(){
		//TODO onDestroy ConfirmacionAutenticacionActivity
		//parentViewsController.removeDelegateFromHashMap( confirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		super.onDestroy();

	}
	
    /*
	 * (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.views.ConfirmacionView#reset()
	 * acceso desde presenter
	 */
	@Override
	public void reset() {
		this.contrasena.setText("");
		this.nip.setText("");
		if(this.asm.isEnabled())
			this.asm.setText("");
		this.cvv.setText("");
		this.tarjeta.setText("");
	}
	
	@Override 
	public void showMessage(final String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
		//showInformationAlert(message);
	}
	
	/**
	 * 
	 */
	private void findViews() {
		contenedorPrincipal = (LinearLayout) findViewById(R.id.confirmacion_lista_datos);
		contenedorContrasena = (LinearLayout) findViewById(R.id.campo_confirmacion_contrasena_layout);
		contenedorNIP = (LinearLayout) findViewById(R.id.campo_confirmacion_nip_layout);
		contenedorASM = (LinearLayout) findViewById(R.id.campo_confirmacion_asm_layout);
		contenedorCVV = (LinearLayout) findViewById(R.id.campo_confirmacion_cvv_layout);
		contenedorPadre = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);

		contrasena = (EditText) contenedorContrasena
				.findViewById(R.id.confirmacion_contrasena_edittext);
		nip = (EditText) contenedorNIP
				.findViewById(R.id.confirmacion_nip_edittext);
		asm = (EditText) contenedorASM
				.findViewById(R.id.confirmacion_asm_edittext);
		cvv = (EditText) contenedorCVV
				.findViewById(R.id.confirmacion_cvv_edittext);

		campoContrasena = (TextView) contenedorContrasena
				.findViewById(R.id.confirmacion_contrasena_label);
		campoNIP = (TextView) contenedorNIP
				.findViewById(R.id.confirmacion_nip_label);
		campoASM = (TextView) contenedorASM
				.findViewById(R.id.confirmacion_asm_label);
		campoCVV = (TextView) contenedorCVV
				.findViewById(R.id.confirmacion_cvv_label);

		instruccionesContrasena = (TextView) contenedorContrasena
				.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
		instruccionesNIP = (TextView) contenedorNIP
				.findViewById(R.id.confirmacion_nip_instrucciones_label);
		instruccionesASM = (TextView) contenedorASM
				.findViewById(R.id.confirmacion_asm_instrucciones_label);
		instruccionesCVV = (TextView) contenedorCVV
				.findViewById(R.id.confirmacion_cvv_instrucciones_label);

		contenedorCampoTarjeta = (LinearLayout) findViewById(R.id.campo_confirmacion_campotarjeta_layout);
		tarjeta = (EditText) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
		campoTarjeta = (TextView) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
		instruccionesTarjeta = (TextView) contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);

		confirmarButton = (ImageButton) findViewById(R.id.confirmacion_confirmar_button);
	
		
	}
	
	/**
	 * 
	 */
	private void scaleToScreenSize() {
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());

		// guiTools.scaleAll(contenedorPrincipal);

		// guiTools.scaleAll((LinearLayout)findViewById(R.id.confirmacion_parent_parent_view));

		guiTools.scale(contenedorPrincipal);
		guiTools.scale(contenedorPadre);

		guiTools.scale(contenedorContrasena);
		guiTools.scale(contenedorNIP);
		guiTools.scale(contenedorASM);
		guiTools.scale(contenedorCVV);

		guiTools.scale(contrasena, true);
		guiTools.scale(nip, true);
		guiTools.scale(asm, true);
		guiTools.scale(cvv, true);

		guiTools.scale(campoContrasena, true);
		guiTools.scale(campoNIP, true);
		guiTools.scale(campoASM, true);
		guiTools.scale(campoCVV, true);

		guiTools.scale(instruccionesContrasena, true);
		guiTools.scale(instruccionesNIP, true);
		guiTools.scale(instruccionesASM, true);
		guiTools.scale(instruccionesCVV, true);

		// nuevo campo
		guiTools.scale(contenedorCampoTarjeta);
		guiTools.scale(tarjeta, true);
		guiTools.scale(campoTarjeta, true);
		guiTools.scale(instruccionesTarjeta, true);

		guiTools.scale(confirmarButton);
		
	}
	
	/**
	 * 
	 * @param intent
	 */
	private void setTitle(final Intent intent ){
		
		final int titleText, iconResource;
		titleText =intent.getIntExtra(ApiConstants.TITLE_TEXT, 0);
		iconResource =intent.getIntExtra(ApiConstants.ICON_RESOURCE, 0);
		setTitle(titleText, iconResource);
	}
	
	/**
	 * 
	 * acceso desde presenter
	 */
	@Override
	public void setListaDatos(final ArrayList<Object> datos) {

		final LinearLayout.LayoutParams params = new LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		final ListaDatosViewControllerCR listaDatos = new ListaDatosViewControllerCR(
				this, params);
		listaDatos.setNumeroCeldas(2);
		listaDatos.setLista(datos);
		listaDatos.setNumeroFilas(datos.size());
		listaDatos.setTitulo(getString(R.string.confirmation_subtitulo));
		listaDatos.showLista();
		final LinearLayout layoutListaDatos = (LinearLayout) findViewById(R.id.confirmacion_lista_datos);
		layoutListaDatos.addView(listaDatos);
		
	}
	
	/**
	 * 
	 */
	private void configuraPantalla() {
		presenter.getShowFields();


		//LinearLayout contenedorPadre = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);

		if (contenedorContrasena.getVisibility() == View.GONE &&
			contenedorNIP.getVisibility() == View.GONE &&
			contenedorASM.getVisibility() == View.GONE &&
			contenedorCVV.getVisibility() == View.GONE &&
			contenedorCampoTarjeta.getVisibility() == View.GONE) {

			//contenedorPadre.setBackgroundColor(0);

		}

		contenedorPadre.measure(MeasureSpec.UNSPECIFIED,
				MeasureSpec.UNSPECIFIED);
		final float camposHeight = contenedorPadre.getMeasuredHeight();

		final LinearLayout layoutListaDatos = (LinearLayout) findViewById(R.id.confirmacion_lista_datos);
		layoutListaDatos.measure(MeasureSpec.UNSPECIFIED,
				MeasureSpec.UNSPECIFIED);
		final float listaHeight = layoutListaDatos.getMeasuredHeight();

		final ViewGroup contenido = (ViewGroup) this.findViewById(
				android.R.id.content).getRootView();// findViewById(android.R.id.content);
		contenido.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		final float contentHeight = contenido.getMeasuredHeight();

		final float margin = getResources().getDimension(
				R.dimen.confirmacion_fields_initial_margin);

		final float maximumSize = (contentHeight * 4) / 5;
		//System.out.println("Altura maxima " + maximumSize);
		final float elementsSize = listaHeight + camposHeight;
		//System.out.println("Altura mixta " + elementsSize);
		final float heightParaValidar = (contentHeight * 3) / 4;
		//System.out.println("heightParaValidar " + contentHeight);

		if (elementsSize >= contentHeight) {
			// RelativeLayout.LayoutParams camposLayout =
			// (RelativeLayout.LayoutParams)contenedorPadre.getLayoutParams();
			// camposLayout.addRule(RelativeLayout.BELOW,
			// R.id.confirmacion_lista_datos);
		}

		confirmarButton.setOnClickListener(this);
	}
	
	@Override 
	public void onClick(final View v) {
		if (v.equals(confirmarButton) && !isActivityChanging()) {
			botonConfirmarClick();
		}
	}
	
	//TODO go back setActivityChanging(true);
	
	private void botonConfirmarClick(){
		//setActivityChanging(true);

		viewTo.setContrasena(contrasena.getText().toString());
		viewTo.setTarjeta(tarjeta.getText().toString());
		viewTo.setNip(nip.getText().toString());
		viewTo.setAsm(asm.getText().toString());
		viewTo.setCvv(cvv.getText().toString());
		
		presenter.confirmarClick(viewTo);
	}
	
	public void processNetworkResponse( ){
		//	int operationId, ServerResponse response) {
		//confirmacionAutenticacionDelegate
		//		.analyzeResponse(operationId, response);
	}
	
	
	
	public void onSuccess(final int idTextoEncabezado) {

		final Map<String, Object> operacionRealizadaMap = new HashMap<String, Object>();
		// Comprobacion de titulos
		final String titulo = getString(idTextoEncabezado);
		if (titulo == getString(R.string.bmovil_consultar_dineromovil_titulo)) {
			// AMZ
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products",
					"operaciones;cancelar dinero movil");
			operacionRealizadaMap.put("eVar12", "operacion_cancelada");
			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);

		} else if (titulo == getString(R.string.bmovil_cambio_cuenta_title)) {
			// AMZ
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products",
					"operaciones;admin+cambio cuenta");
			operacionRealizadaMap.put("eVar12", "operacion realizada");
			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		} else if (titulo == getString(R.string.bmovil_configurar_correo_titulo)) {
			// AMZ
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products",
					"operaciones;admin+configura correo");
			operacionRealizadaMap.put("eVar12", "operacion realizada");
			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		} else if (titulo == getString(R.string.bmovil_cambio_telefono_title)) {
			// AMZ
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products",
					"operaciones;admin+cambio celular");
			operacionRealizadaMap.put("eVar12", "operacion realizada");
			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		} else if (titulo == getString(R.string.bmovil_suspender_title)) {
			// AMZ
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products",
					"operaciones;admin+suspender o cancelar");
			operacionRealizadaMap.put("eVar12", "operacion realizada");
			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		} else if (titulo == getString(R.string.bmovil_cancelar_title)) {
			// AMZ
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products",
					"operaciones;admin+suspender o cancelar");
			operacionRealizadaMap.put("eVar12", "operacion realizada");
			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		} else if (titulo == getString(R.string.bmovil_configurar_montos_title)) {
			// AMZ
			operacionRealizadaMap.put("evento_realizada", "event52");
			operacionRealizadaMap.put("&&products",
					"operaciones;admin+configura montos");
			operacionRealizadaMap.put("eVar12", "operacion realizada");
			TrackingHelper.trackOperacionRealizada(operacionRealizadaMap);
		}
		reset();

		if(this.viewTo.getCloseSession()){
		//	finish();
		}

	}
	
	public void showMensajePideContrasena(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if ( lenght ){
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.PASSWORD_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteContrasena));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteContrasena));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}
		habilitarBtnContinuar();
	}
	
	public void showMensajePideTarjeta(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			mensaje.append("Es necesario ingresar los últimos 5 dígitos de tu tarjeta");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje .append("Es necesario ingresar los últimos 5 dígitos de tu tarjeta");
			showInformationAlert(mensaje.toString());
		}
		habilitarBtnContinuar();
	}
	
	public void showMensajePideNip(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.NIP_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteNip));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteNip));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}
		habilitarBtnContinuar();
	}
	
	public void showMensajePideToken(final int msg, final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.ASM_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			/*
			 * switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
			 */
			mensaje.append(" ");
			mensaje.append(getString(msg));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(msg));
			
		/*	switch (tipoInstrumentoSeguridad) {
					case OCRA:
						mensaje += getEtiquetaCampoOCRA();
						break;
					case DP270:
						mensaje += getEtiquetaCampoDP270();
						break;
					case SoftToken:
						if (SuiteApp.getSofttokenStatus()) {
							mensaje += getEtiquetaCampoSoftokenActivado();
						} else {
							mensaje += getEtiquetaCampoSoftokenDesactivado();
						}
						break;
					default:
						break;
				}
		*/
		mensaje.append(".");
		showInformationAlert(mensaje.toString());
			
		}
		habilitarBtnContinuar();
	}
	
	public void showMensajePideCVV(final boolean lenght){
		final StringBuffer mensaje = new StringBuffer();
		if(lenght){
			
			mensaje.append(getString(R.string.confirmation_valorIncompleto1));
			mensaje.append(" ");
			mensaje.append(Constants.CVV_LENGTH);
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_valorIncompleto2));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteCvv));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}else{
			mensaje.append(getString(R.string.confirmation_valorVacio));
			mensaje.append(" ");
			mensaje.append(getString(R.string.confirmation_componenteCvv));
			mensaje.append(".");
			showInformationAlert(mensaje.toString());
		}
		habilitarBtnContinuar();
	}

	/**
	 * acceso desde presenter
	 */
	public void mostrarContrasena(final boolean visibility){
		contenedorContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
		campoContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
		contrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);

		if (visibility) {
			campoContrasena.setText(getString(R.string.confirmation_aut_contrasena));
			InputFilter[] userFilterArray= new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(
					Constants.PASSWORD_LENGTH);
			contrasena.setFilters(userFilterArray);
			contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
		} else {
			contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		instruccionesContrasena.setVisibility(View.GONE);
	}
	
	/**
	 * 
	 */
	public void mostrarNIP(final boolean visibility){
		contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
		nip.setVisibility(visibility ? View.VISIBLE:View.GONE);

		if (visibility) {
			//campoNIP.setText(confirmacionDelegate.getEtiquetaCampoNip());
			campoNIP.setText(getString(R.string.confirmation_nip));
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
			nip.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
			//String instrucciones = confirmacionDelegate.getTextoAyudaNIP();
			final String instrucciones = getString(R.string.confirmation_autenticacion_ayudaNip);
			if ("".equals(instrucciones)) {
				instruccionesNIP.setVisibility(View.GONE);
			} else {
				instruccionesNIP.setVisibility(View.VISIBLE);
				instruccionesNIP.setText(instrucciones);
			}
		} else {
			nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		
	}
	
	/**
	 * 
	 */
	public void mostrarASM(final ConfirmacionViewTo to){
	//public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){	

		switch(to.getTokenAMostrar()) {
			case ninguno:
				contenedorASM.setVisibility(View.GONE);
				campoASM.setVisibility(View.GONE);
				asm.setVisibility(View.GONE);
				asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
				break;
			case codigo:
			case registro:
				contenedorASM.setVisibility(View.VISIBLE);
				campoASM.setVisibility(View.VISIBLE);
				asm.setVisibility(View.VISIBLE);
				InputFilter[] userFilterArray = new InputFilter[1];
				userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);	
				asm.setFilters(userFilterArray);
				cambiarAccionTexto(contrasena);
				cambiarAccionTexto(tarjeta);
				cambiarAccionTexto(nip);
				asm.setImeOptions(EditorInfo.IME_ACTION_DONE);

				break;
		}
		
		//Constants.TipoInstrumento tipoInstrumento = confirmacionDelegate.consultaTipoInstrumentoSeguridad();
		switch (to.getInstrumentoSeguridad()) {
			case OCRA:
				//campoASM.setText(confirmacionDelegate.getEtiquetaCampoOCRA());
				campoASM.setText(getString(R.string.confirmation_ocra));
				//asm.setTransformationMethod(null);
				break;
			case DP270:
				//campoASM.setText(confirmacionDelegate.getEtiquetaCampoDP270());
				campoASM.setText(getString(R.string.confirmation_dp270));
				//asm.setTransformationMethod(null);
				break;
			case SoftToken:
				if(SuiteApp.getSofttokenStatus()) {
					asm.setText(Constants.DUMMY_OTP);
					asm.setEnabled(false);
					campoASM.setText(getString(R.string.confirmation_softtokenActivado));
				} else {
					asm.setText("");
					asm.setEnabled(true);
					campoASM.setText(getString(R.string.confirmation_softtokenDesactivado));
					//asm.setTransformationMethod(null);
				}
				
				break;
			default:
				break;
		}
		
		//String instrucciones = confirmacionDelegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento);
		final String instrucciones = to.getTextoAyudaInsSeg();
		if ("".equals(instrucciones)) {
			instruccionesASM.setVisibility(View.GONE);
		} else {
			instruccionesASM.setVisibility(View.VISIBLE);
			instruccionesASM.setText(instrucciones);
		}
	}
	
	/**
	 * 
	 */
	public void mostrarCampoTarjeta(final boolean visibility) {
		contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
		tarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);

		if (visibility) {
			//campoTarjeta.setText(confirmacionDelegate.getEtiquetaCampoTarjeta());
			campoTarjeta.setText(getString(R.string.confirmation_componente_digitos_tarjeta));
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(5);
			tarjeta.setFilters(userFilterArray);
			cambiarAccionTexto(contrasena);
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
			//String instrucciones = confirmacionDelegate.getTextoAyudaTarjeta();
			final String instrucciones = getString(R.string.confirmation_ayuda_componente_digitos_tarjeta);
			if ("".equals(instrucciones)) {
				instruccionesTarjeta.setVisibility(View.GONE);
			} else {
				instruccionesTarjeta.setVisibility(View.VISIBLE);
				instruccionesTarjeta.setText(instrucciones);
			}
		} else {
			tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
		
	}
	
	/**
	 * 
	 */
	public void mostrarCVV(final boolean visibility){
		contenedorCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		campoCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);
		cvv.setVisibility(visibility ? View.VISIBLE:View.GONE);
		instruccionesCVV.setVisibility(visibility ? View.VISIBLE:View.GONE);

		if (visibility) {
			//campoCVV.setText(confirmacionDelegate.getEtiquetaCampoCVV());
			campoCVV.setText(getString(R.string.confirmation_CVV));
			final InputFilter[] userFilterArray = new InputFilter[1];
			userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
			cvv.setFilters(userFilterArray);
			final String instrucciones = getString(R.string.confirmation_CVV_ayuda);;
			instruccionesCVV.setText(instrucciones);
			instruccionesCVV.setVisibility("".equals(instrucciones) ? View.GONE : View.VISIBLE);
			cambiarAccionTexto(contrasena);
			cambiarAccionTexto(tarjeta);
			cambiarAccionTexto(nip);
			cambiarAccionTexto(asm);
			cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);

		} else {
			cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
		}
	}
	
	//
	private void cambiarAccionTexto(final EditText campo) {
		if (campo.getVisibility() == View.VISIBLE) {
			campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		}
	}
	
	/**
	 * 
	 */
	private void habilitarBtnContinuar() {
		confirmarButton.setEnabled(true);
	}
	
	/**
	 * @return the viewVo
	 */
	public final ConfirmacionViewTo getViewTo() {
		return viewTo;
	}

	/**
	 * @param viewTo the viewVo to set
	 */
	public final void setViewTo(final ConfirmacionViewTo viewTo) {
		this.viewTo = viewTo;
	}

	@Override
	public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
	}

	@Override
	public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
	}

	@Override
	public void afterTextChanged(final Editable s) {
		onUserInteraction();
	}
	
}
