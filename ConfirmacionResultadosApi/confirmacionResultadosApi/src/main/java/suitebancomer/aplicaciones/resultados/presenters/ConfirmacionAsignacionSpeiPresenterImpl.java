/**
 * 
 */
package suitebancomer.aplicaciones.resultados.presenters;


import android.content.Intent;

import java.util.ArrayList;

import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.interactors.ConfirmacionAsignacionSpeiInteractor;
import suitebancomer.aplicaciones.resultados.interactors.ConfirmacionAsignacionSpeiInteractorImpl;
import suitebancomer.aplicaciones.resultados.listeners.OnConfirmAsingSpeiFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IConfirmacionAsignacionSpeiServiceProxy;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionAsignacionSpeiViewTo;
import suitebancomer.aplicaciones.resultados.views.ConfirmacionAsignacionSpeiView;

/**
 * @author amgonzalez
 *
 */
public class ConfirmacionAsignacionSpeiPresenterImpl 
	implements ConfirmacionAsignacionSpeiPresenter, OnConfirmAsingSpeiFinishedListener {
	
	private final ConfirmacionAsignacionSpeiView cview;
	private final ConfirmacionAsignacionSpeiInteractor interactor;

	public ConfirmacionAsignacionSpeiPresenterImpl(final ConfirmacionAsignacionSpeiView view, final Intent intent){
		this.cview = view;
		this.interactor = new ConfirmacionAsignacionSpeiInteractorImpl(
				getServiceProxy(intent)); 
	}
	
	@Override 
	public void onResume() {
        //mainView.showProgress();
        //findItemsInteractor.findItems(this);
    }
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.presenters.ConfirmacionPresenter#show()
	 */
	@Override
	public void show() {
		// TODO Auto-generated method stub
		/*
		 *  mainView.showProgress();
        findItemsInteractor.findItems(this);
		 */
	}

	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener#onError()
	 */
	@Override
	public void onError() {
		// TODO Auto-generated method stub
		//loginView.setUsernameError();
		// en el view username.setError(getString(R.string.username_error));
	}

	private IConfirmacionAsignacionSpeiServiceProxy getServiceProxy(final Intent intent){
		final IConfirmacionAsignacionSpeiServiceProxy p = (IConfirmacionAsignacionSpeiServiceProxy)
				SuiteAppCRApi.getInstance().getProxy();
		return p;
	}

	public void limpiarCampos(){
		cview.cleanAuthenticationFields();
	}
	
	@Override
	public void getListaDatos() {
		interactor.getListaDatos(this);
		
	}

	@Override
	public void onFinishedListaDatos(final ArrayList<Object> list) {
		cview.setListaDatos(list);
	}

	@Override
	public void getShowFields() {
		interactor.getShowFields(this);
	}
	
	@Override
	public void onFinishedValidShowFields(final ConfirmacionAsignacionSpeiViewTo fields) {
		final ConfirmacionAsignacionSpeiViewTo to = (ConfirmacionAsignacionSpeiViewTo)fields;
		cview.setViewTo(to);
		cview.configureCardElement(to.getCardItem().getVisible(), to.getCardItem().getLabel(), to.getCardItem().getInstruction());
		cview.configureNipElement(to.getNipItem().getVisible(), to.getNipItem().getLabel(), to.getNipItem().getInstruction());
		cview.configurePasswordElement(to.getPwdItem().getVisible(), to.getPwdItem().getLabel(), to.getPwdItem().getInstruction());
		cview.configureCvvElement(to.getCvvItem().getVisible(), to.getCvvItem().getLabel(), to.getCvvItem().getInstruction() );
		cview.configureOtpElement(to.getOtpItem().getVisible(), to.getOtpItem().getLabel(), to.getOtpItem().getInstruction(), to.getOtpItem().getCode());
	}

	@Override
	public void confirmarClick(final ConfirmacionAsignacionSpeiViewTo viewVo) {
		interactor.doConfirmacionOperacion(viewVo, this);
	}
	
	@Override
	public void onFinishedConfirmacionOperacion(final Boolean resp) {
		if ( resp.booleanValue() ){
			//int idTextoEncabezado = interactor.consultaOperationsIdTextoEncabezado();
			//cview.onSuccess(idTextoEncabezado);
			cview.onSuccess(0);
		}
		//cview.showMessage("Error!");
		
	}

	@Override
	public void onErrorOtp(final int msg, final String format) {
		// TODO Auto-generated method stub
		//errorMessage = String.format(ownerController.getString(R.string.bmovil_confirmacion_asignacion_spei_error_missing_otp), fieldName);
		//ownerController.showInformationAlert(errorMessage);
		cview.onErrorMsg();
	}
	
	@Override
	public void onErrorValue(final int idMsg){
				cview.onErrorMsg(idMsg);
		//Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

	@Override
	public void requestSpeiTermsAndConditions() {
		interactor.requestSpeiTermsAndConditions(this);
	}
	
	@Override
	public void onFinishedTerminosCondiciones(final Boolean resp) {
		// TODO Auto-generated method stub
	}

}
