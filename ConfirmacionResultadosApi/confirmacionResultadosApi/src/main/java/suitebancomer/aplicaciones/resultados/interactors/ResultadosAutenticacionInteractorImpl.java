/**
 * 
 */
package suitebancomer.aplicaciones.resultados.interactors;

import suitebancomer.aplicaciones.resultados.listeners.OnResultadosFinishedListener;
import suitebancomer.aplicaciones.resultados.proxys.IResultadosAutenticacionServiceProxy;
import android.os.Handler;

/**
 * @author amgonzalez
 *
 */
public class ResultadosAutenticacionInteractorImpl implements ResultadosAutenticacionInteractor {
	
	private final IResultadosAutenticacionServiceProxy proxy;
	
	public ResultadosAutenticacionInteractorImpl(final IResultadosAutenticacionServiceProxy proxy) {
		this.proxy = proxy;
	}
	
	/**
	 * @return the proxy
	 */
	public IResultadosAutenticacionServiceProxy getProxy() {
		return proxy;
	}
	
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.resultados.interactors.ConfirmacionInteractor#getDatos(suitebancomer.aplicaciones.resultados.listeners.OnConfirmacionFinishedListener)
	 */
	@Override
	public void getListaDatos(final OnResultadosFinishedListener listener) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				listener.onFinishedListaDatos( 
						proxy.getListaDatos());
			}
		}
		);
	}

	@Override
	public void getShowFields(final OnResultadosFinishedListener listener) {
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				listener.onFinishedValidShowFields(
						proxy.showFields());
				/* en el proxy 
				String titulo = resultadosDelegate.getTextoTituloResultado();
				String texto = resultadosDelegate.getTextoPantallaResultados();
				String instrucciones = resultadosDelegate.getTextoAyudaResultados();
				String tituloTextoEspecial = resultadosDelegate.getTituloTextoEspecialResultados();
				String textoEspecial = resultadosDelegate.getTextoEspecialResultados();
				 */
			}
		}
		);
	}
	
	public void setParamStateParentManager(){
		proxy.setParamStateParentManager();
	}
	
	public void getOnPrepareOptionsMenu(final OnResultadosFinishedListener listener){
		listener.onFinishedGetPrepareOptionsMenu(
				proxy.getOnPrepareOptionsMenu() );
	}
	
	public boolean optionsItemSelected(final int idMenu){
		return proxy.onOptionsItemSelected(idMenu);
	}
	
	//TODO 
	public void botonMenuClick(){
		proxy.botonMenuClick();
	}
	
}
