/**
 * 
 */
package suitebancomer.aplicaciones.resultados.handlers;

import java.lang.reflect.Method;
import java.util.ArrayList;

import suitebancomer.aplicaciones.resultados.R;
import suitebancomer.aplicaciones.resultados.commons.ApiConstants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.proxys.IServiceProxy;
import suitebancomer.aplicaciones.resultados.views.ConfirmacionActivity;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;
import android.content.Intent;

/**
 * @author lbermejo
 *
 */
public  class ConfirmacionHandler{
	
	private final IServiceProxy proxy;
	private Long delegateId;
	//private ViewControllerHelper helper;
	//private BaseViewsControllerCommons parentManager;
	private final BaseViewsControllerCommons parentViewsController;
	private final BaseViewControllerCommons rootViewController;
	private Boolean isActivityChanging;

	/*
	* Bloke usado para los efecto de transicicon
	* */
	private static Method methodOverridePendingTransition;
	
	public ConfirmacionHandler(final IServiceProxy proxy,
							   final BaseViewsControllerCommons parentManager,
							   final BaseViewControllerCommons rootViewController){
		
		this.proxy = proxy;
		this.parentViewsController=parentManager; 
		this.rootViewController = rootViewController;

	}
	
	public final void invokeActivity(final ArrayList<String> params, final Integer idText, final Integer idNomInmgEncabezado){

		final String subtitle = parentViewsController.getCurrentViewControllerApp()
							.getString(R.string.confirmation_subtitulo);
		SuiteAppCRApi.getInstance().setBaseViews(rootViewController);
		final Intent intent = new Intent(
				SuiteAppCRApi.appContext,
							ConfirmacionActivity.class);
		
		//TODO ConfirmacionViewVo
		//intent.putExtra(ApiConstants.PROXY_NAME, proxy );
		intent.putExtra(ApiConstants.KEY_BASE_DELEGATE, this.delegateId);
		intent.putExtra(ApiConstants.TRACKSTATE_PARAMETERS,params);
		intent.putExtra(ApiConstants.LISTA_DATOS_CONFIRM_SUBTITULO, subtitle);
		intent.putExtra(ApiConstants.TITLE_TEXT, idText);
		intent.putExtra(ApiConstants.ICON_RESOURCE, idNomInmgEncabezado);
		intent.putExtra(ApiConstants.ACTIVITY_CHANGING, isActivityChanging());

		SuiteAppCRApi.appContext.startActivity(intent);
		/**En el suite Api ******/	 	
		//setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		//isActivityChanging
		/********/
	}
		
	public final void setDelegateId(final Long id){
		this.delegateId = id;
	}
	

	/**
	 * @return the isActivityChanging
	 */
	public final boolean isActivityChanging() {
		return isActivityChanging;
	}

	/**
	 * @param isActivityChanging the isActivityChanging to set
	 */
	public final void setActivityChanging(final boolean isActivityChanging) {
		this.isActivityChanging = isActivityChanging;
	}
		
	
	
	
	
}
