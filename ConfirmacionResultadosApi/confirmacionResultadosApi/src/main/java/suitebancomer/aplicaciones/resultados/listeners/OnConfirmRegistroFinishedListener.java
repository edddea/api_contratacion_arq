/**
 * 
 */
package suitebancomer.aplicaciones.resultados.listeners;

import java.util.ArrayList;


/**
 * @author lbermejo
 *
 */
public interface OnConfirmRegistroFinishedListener extends OnConfirmacionFinishedListener{

	void onFinishedDatosRegistroOk(final ArrayList<Object> list);
	
	
}
