/**
 * 
 */
package suitebancomer.aplicaciones.resultados.handlers;

import java.lang.reflect.Method;
import java.util.ArrayList;

import suitebancomer.aplicaciones.resultados.commons.ApiConstants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.proxys.IServiceProxy;
import suitebancomer.aplicaciones.resultados.views.ResultadosActivity;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;
import android.content.Intent;

/**
 * @author lbermejo
 *
 */
public  class ResultadosHandler{
	
	private final IServiceProxy proxy;
	private Long delegateId;
	private final BaseViewsControllerCommons parentViewsController;
	private final BaseViewControllerCommons rootViewController;
	private Boolean isActivityChanging;

	/*
	* Bloke usado para los efecto de transicicon
	* */
	private static Method methodOverridePendingTransition;

	public ResultadosHandler(final IServiceProxy proxy,
							 final BaseViewsControllerCommons parentManager,
							 final BaseViewControllerCommons rootViewController){
		
		this.proxy = proxy;
		this.parentViewsController=parentManager;
		this.rootViewController = rootViewController;
		//this.parentViewsController.
	}
	
	public final void invokeActivity(final ArrayList<String> params, final Integer idText, final Integer idNomInmgEncabezado){

		SuiteAppCRApi.getInstance().setBaseViews(rootViewController);
		final Intent intent = new Intent(
				SuiteAppCRApi.appContext,
							ResultadosActivity.class);
		
		//TODO ConfirmacionViewVo
		//intent.putExtra(ApiConstants.PROXY_NAME, proxy );
		intent.putExtra(ApiConstants.KEY_BASE_DELEGATE, this.delegateId);
		intent.putExtra(ApiConstants.TRACKSTATE_PARAMETERS,params);
		intent.putExtra(ApiConstants.TITLE_TEXT, idText);
		intent.putExtra(ApiConstants.ICON_RESOURCE, idNomInmgEncabezado);
		intent.putExtra(ApiConstants.ACTIVITY_CHANGING, isActivityChanging());

		SuiteAppCRApi.appContext.startActivity(intent);
		
		/**En el suite Api ******/	 	
		//setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		//isActivityChanging
		/********/
	}
		
	public final void setDelegateId(final Long id){
		this.delegateId = id;
	}

	/**
	 * @return the isActivityChanging
	 */
	public final boolean isActivityChanging() {
		return isActivityChanging;
	}

	/**
	 * @param isActivityChanging the isActivityChanging to set
	 */
	public final void setActivityChanging(final boolean isActivityChanging) {
		this.isActivityChanging = isActivityChanging;
	}
		
	
}
