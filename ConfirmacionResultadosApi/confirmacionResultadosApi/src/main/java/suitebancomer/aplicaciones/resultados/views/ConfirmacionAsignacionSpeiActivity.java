/**
 * 
 */
package suitebancomer.aplicaciones.resultados.views;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.commons.Constants;
import suitebancomer.aplicaciones.resultados.R;
import suitebancomer.aplicaciones.resultados.commons.ApiConstants;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.presenters.ConfirmacionAsignacionSpeiPresenter;
import suitebancomer.aplicaciones.resultados.presenters.ConfirmacionAsignacionSpeiPresenterImpl;
import suitebancomer.aplicaciones.resultados.to.ConfirmacionAsignacionSpeiViewTo;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.views.ListaDatosViewControllerCR;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

/**
 * @author amgonzalez
 *
 */
public class ConfirmacionAsignacionSpeiActivity extends ApiBaseActivity 
implements ConfirmacionAsignacionSpeiView, TextWatcher{
	
	private LinearLayout dataTableLayout;
	private TextView termsLink;
	private CheckBox termsCheckBox;
	
	private View cardElementLayout;
	private TextView cardElementLabel;
	private EditText cardElementEdittext;
	private TextView cardElementInstructionsLabel;
	
	private View nipElementLayout;
	private TextView nipElementLabel;
	private EditText nipElementEdittext;
	private TextView nipElementInstructionsLabel;

	private View pwdElementLayout;
	private TextView pwdElementLabel;
	private EditText pwdElementEdittext;
	private TextView pwdElementInstructionsLabel;

	private View cvvElementLayout;
	private TextView cvvElementLabel;
	private EditText cvvElementEdittext;
	private TextView cvvElementInstructionsLabel;
	
	private View otpElementLayout;
	private TextView otpElementLabel;
	private EditText otpElementEdittext;
	private TextView otpElementInstructionsLabel;
	
	private View helpImage;

	private ConfirmacionAsignacionSpeiPresenter presenter;
	private ConfirmacionAsignacionSpeiViewTo viewTo;
	
	//Map<String,Object> OperacionRealizadaMap = new HashMap<String, Object>();
	
	private static final String VIEW = "confirma";
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		// setContentView(R.layout.layout_bmovil_confirmacion_act);
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE,R.layout.layout_bmovil_confirmacion_asignacion_spei);
		setActivityChanging(getIntent().getBooleanExtra(ApiConstants.ACTIVITY_CHANGING, false));

		SuiteAppCRApi.getInstance().onCreate(getApplicationContext());
		SuiteApp.appContext=this;

		final ArrayList<String> param = getIntent().getStringArrayListExtra(
				ApiConstants.TRACKSTATE_PARAMETERS);
		TrackingHelper.trackState(VIEW, param);

		presenter = new ConfirmacionAsignacionSpeiPresenterImpl(this, getIntent());
		setTitle(getIntent());

		findViews();
		scaleToScreenSize();
		presenter.getListaDatos();

		configuraPantalla();
		moverScroll();

		final boolean showHelpImage = (Boolean)getIntent().getExtras().get("showHelpImage");
		helpImage.setVisibility(showHelpImage ? View.VISIBLE : View.GONE);
		
		//contrasena.addTextChangedListener(this);
		//nip.addTextChangedListener(this);
		//asm.addTextChangedListener(this);
		//cvv.addTextChangedListener(this);
	}

	@Override
	public void onBackPressed() {
		TrackingHelper.touchAtrasState();
		super.onBackPressed();
	}
	
	@Override 
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext=this;
		// presenter.onResume();
		
		/*if (parentViewsController.consumeAccionesDeReinicio())
			return;

		ownDelegate.setOwnerController(this);
		getParentViewsController().setCurrentActivityApp(this);
		ownDelegate.configureScreen();
		*/
		moverScroll();
	}

	@Override
	protected void onPause() {
		super.onPause();
		//parentViewsController.consumeAccionesDePausa();
	}
	
	@Override
	public void goBack() {
		super.goBack();
		//getParentViewsController().removeDelegateFromHashMap(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID);
	}
	
	private void configuraPantalla(){
		
		presenter.getShowFields();

		final LinearLayout contenedorPadre = (LinearLayout)findViewById(R.id.confirmationFieldsLayout);
		if (cardElementEdittext.getVisibility() == View.GONE &&
			    nipElementEdittext.getVisibility() == View.GONE &&
			    pwdElementEdittext.getVisibility() == View.GONE &&
			    cvvElementEdittext.getVisibility() == View.GONE &&
			    otpElementEdittext.getVisibility() == View.GONE){
				contenedorPadre.setBackgroundColor(Color.TRANSPARENT);
		}

		final String mystring = termsLink.getText().toString();
		final SpannableString content = new SpannableString(mystring);
		content.setSpan(new UnderlineSpan(), 0, mystring.length(), 0);
		termsLink.setText(content);

		//controller.configureScreen(dataTable);
		//controller.setTitle(operationDelegate.getTextoEncabezado(), operationDelegate.getNombreImagenEncabezado());
	}
	
	@Override
	public void setListaDatos(final ArrayList<Object> datos) {
		
		//configureCardElement();
		//configureNipElement();
		//configurePasswordElement();
		//configureCvvElement();
		//configureOtpElement();
		
		@SuppressWarnings("deprecation")
		final LinearLayout.LayoutParams params =  new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		//ArrayList<Object> data = operationDelegate.getDatosTablaConfirmacion();
		final ListaDatosViewControllerCR dataTable = new ListaDatosViewControllerCR(this, params);
		dataTable.setNumeroCeldas(2);
		dataTable.setLista(datos);
		dataTable.setNumeroFilas(datos.size());
		dataTable.setTitulo(getString(R.string.confirmation_subtitulo));
		dataTable.showLista();
		
		dataTableLayout.removeAllViews();
		dataTableLayout.addView(dataTable);

		if (otpElementEdittext.getVisibility() != View.GONE)
			otpElementEdittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
		else if (cvvElementEdittext.getVisibility() != View.GONE)
			cvvElementEdittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
		else if (pwdElementEdittext.getVisibility() != View.GONE)
			pwdElementEdittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
		else if (nipElementEdittext.getVisibility() != View.GONE)
			nipElementEdittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
		else if (cardElementEdittext.getVisibility() != View.GONE)
			cardElementEdittext.setImeOptions(EditorInfo.IME_ACTION_DONE);
		
	}

	/**
	 * 
	 * @param intent
	 */
	private void setTitle(final Intent intent ){
		
		final int titleText, iconResource;
		titleText = intent.getIntExtra(ApiConstants.TITLE_TEXT, 0) ;
		iconResource = intent.getIntExtra(ApiConstants.ICON_RESOURCE,0);
		setTitle(titleText, iconResource);
	}
	

	private void findViews() {
		dataTableLayout = (LinearLayout)findViewById(R.id.confirmationDataTableLayout);
		termsLink = (TextView)findViewById(R.id.termsLink);
		termsCheckBox = (CheckBox)findViewById(R.id.termsCheckbox);
		
		cardElementLayout = findViewById(R.id.confirmationCardLayout);		
		cardElementLabel = (TextView)findViewById(R.id.confirmationCardLabel);
		cardElementEdittext = (EditText)findViewById(R.id.confirmationCardEdittext);
		cardElementInstructionsLabel = (TextView)findViewById(R.id.confirmationCardInstructionsLabel);
		
		nipElementLayout = findViewById(R.id.confirmationNipLayout);		
		nipElementLabel = (TextView)findViewById(R.id.confirmationNipLabel);
		nipElementEdittext = (EditText)findViewById(R.id.confirmationNipEdittext);
		nipElementInstructionsLabel = (TextView)findViewById(R.id.confirmationNipInstructionsLabel);
		
		pwdElementLayout = findViewById(R.id.confirmationPasswordLayout);		
		pwdElementLabel = (TextView)findViewById(R.id.confirmationPasswordLabel);
		pwdElementEdittext = (EditText)findViewById(R.id.confirmationPasswordEdittext);
		pwdElementInstructionsLabel = (TextView)findViewById(R.id.confirmationPasswordInstructionsLabel);
		
		cvvElementLayout = findViewById(R.id.confirmationCvvLayout);		
		cvvElementLabel = (TextView)findViewById(R.id.confirmationCvvLabel);
		cvvElementEdittext = (EditText)findViewById(R.id.confirmationCvvEdittext);
		cvvElementInstructionsLabel = (TextView)findViewById(R.id.confirmationCvvInstructionsLabel);
		
		otpElementLayout = findViewById(R.id.confirmationOtpLayout);		
		otpElementLabel = (TextView)findViewById(R.id.confirmationOtpLabel);
		otpElementEdittext = (EditText)findViewById(R.id.confirmationOtpEdittext);
		otpElementInstructionsLabel = (TextView)findViewById(R.id.confirmationOtpInstructionsLabel);
		
		helpImage = findViewById(R.id.helpImage);
		
		
	}
	
	private void scaleToScreenSize() {
		final GuiTools guitools = GuiTools.getCurrent();
		guitools.init(getWindowManager());
		
		guitools.scale(dataTableLayout);
		
		guitools.scale(findViewById(R.id.termsLabel), true);
		guitools.scale(findViewById(R.id.seeTermsLayout));
		guitools.scale(termsLink, true);
		guitools.scale(termsCheckBox);
		
		guitools.scale(findViewById(R.id.confirmationFieldsLayout));
		
		guitools.scale(cardElementLayout);
		guitools.scale(cardElementLabel, true);
		guitools.scale(cardElementEdittext, true);
		guitools.scale(cardElementInstructionsLabel, true);
		guitools.scale(findViewById(R.id.confirmationInnerCardLayout));
		
		guitools.scale(nipElementLayout);
		guitools.scale(nipElementLabel, true);
		guitools.scale(nipElementEdittext, true);
		guitools.scale(nipElementInstructionsLabel, true);
		guitools.scale(findViewById(R.id.confirmationInnerNipLayout));
		
		guitools.scale(pwdElementLayout);
		guitools.scale(pwdElementLabel, true);
		guitools.scale(pwdElementEdittext, true);
		guitools.scale(pwdElementInstructionsLabel, true);
		guitools.scale(findViewById(R.id.confirmationInnerPasswordLayout));
		
		guitools.scale(cvvElementLayout);
		guitools.scale(cvvElementLabel, true);
		guitools.scale(cvvElementEdittext, true);
		guitools.scale(cvvElementInstructionsLabel, true);
		guitools.scale(findViewById(R.id.confirmationInnerCvvLayout));
		
		guitools.scale(otpElementLayout);
		guitools.scale(otpElementLabel, true);
		guitools.scale(otpElementEdittext, true);
		guitools.scale(otpElementInstructionsLabel, true);
		guitools.scale(findViewById(R.id.confirmationInnerOtpLayout));
		
		guitools.scale(findViewById(R.id.confirmationConfirmButton));
		guitools.scale(helpImage);

	}
	
	@Override
	public void onSuccess(final int idTextoEncabezado) {
		//finish();
		reset();
	}
	
	//#region OnClick listeners.
	/**
	 * Behavior when the confirm button is clicked.
	 * 
	 * @param sender
	 *            The clicked view.
	 */
	public void onConfirmButtonClick(final View sender) {

		final Map<String,Object> OperacionRealizadaMap = new HashMap<String, Object>();
		// AMZ Ale
		OperacionRealizadaMap.put("evento_realizada", "event52");
		OperacionRealizadaMap.put("&&products","operaciones;admin+ asociar numero");
		OperacionRealizadaMap.put("eVar12", "operacion realizada");
		// TrackingHelper.trackOperacionRealizada(OperacionRealizadaMap);
		viewTo.setTarjeta(cardElementEdittext.getText().toString());
		viewTo.setNip(nipElementEdittext.getText().toString());
		viewTo.setContrasena(pwdElementEdittext.getText().toString());
		viewTo.setCvv(cvvElementEdittext.getText().toString());
		viewTo.setOtp(otpElementEdittext.getText().toString());
		viewTo.setOkTerminos(termsCheckBox.isChecked());
		
		//ownDelegate.confirm(termsAccepted, card, nip, pwd, cvv, otp);
		presenter.confirmarClick(viewTo);
	}
	
	/**
	 * Behavior when the terms link is clicked.
	 * @param sender The clicked view.
	 */
	public void onShowTermsLinkClick(final View sender) {
		presenter.requestSpeiTermsAndConditions();
	}
	
	//@Override
	//public void processNetworkResponse(int operationId, ServerResponse response) {
	//	ownDelegate.analyzeResponse(operationId, response);
	//}
	
/**
	 * Configures the card layout and inner views.
	 * @param visible Flag to indicate if the element is visible.
	 * @param label The title resource id.
	 * @param instructions The instructions resource id.
	 */
	public void configureCardElement(final boolean visible, final String label, final String instructions) {
		cardElementLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
		
		if(visible) {
			cardElementLabel.setText(label);
			cardElementInstructionsLabel.setText(instructions);
			cardElementEdittext.setText(Constants.EMPTY_STRING);
			
			cardElementInstructionsLabel.setVisibility(instructions.equals(Constants.EMPTY_STRING) ? View.GONE : View.VISIBLE);
		}
	}

	/**
	 * Configures the nip layout and inner views.
	 * @param visible Flag to indicate if the element is visible.
	 * @param label The title resource id.
	 * @param instructions The instructions resource id.
	 */
	public void configureNipElement(final boolean visible, final String label, final String instructions) {
		nipElementLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
		
		if(visible) {
			nipElementLabel.setText(label);
			nipElementInstructionsLabel.setText(instructions);
			nipElementEdittext.setText(Constants.EMPTY_STRING);
			
			nipElementInstructionsLabel.setVisibility(instructions.equals(Constants.EMPTY_STRING) ? View.GONE : View.VISIBLE);
		}
	}
	
	/**
	 * Configures the password layout and inner views.
	 * @param visible Flag to indicate if the element is visible.
	 * @param label The title resource id.
	 * @param instructions The instructions resource id.
	 */
	public void configurePasswordElement(final boolean visible, final String label, final String instructions) {
		pwdElementLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
		
		if(visible) {
			pwdElementLabel.setText(label);
			pwdElementInstructionsLabel.setText(instructions);
			pwdElementEdittext.setText(Constants.EMPTY_STRING);
			
			pwdElementInstructionsLabel.setVisibility(instructions.equals(Constants.EMPTY_STRING) ? View.GONE : View.VISIBLE);
		}
	}
	
	/**
	 * Configures the cvv layout and inner views.
	 * @param visible Flag to indicate if the element is visible.
	 * @param label The title resource id.
	 * @param instructions The instructions resource id.
	 */
	public void configureCvvElement(final boolean visible, final String label, final String instructions) {
		cvvElementLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
		
		if(visible) {
			cvvElementLabel.setText(label);
			cvvElementInstructionsLabel.setText(instructions);
			cvvElementEdittext.setText(Constants.EMPTY_STRING);
			
			cvvElementInstructionsLabel.setVisibility(instructions.equals(Constants.EMPTY_STRING) ? View.GONE : View.VISIBLE);
		}
	}
	
	/**
	 * Configures the OTP layout and inner views.
	 * @param visible Flag to indicate if the element is visible.
	 * @param label The title resource id.
	 * @param instructions The instructions resource id.
	 * @param code The code to be set in the OTP view, migth be and empty string.
	 */
	public void configureOtpElement(final boolean visible, final String label, final String instructions, final String code) {
		otpElementLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
		
		if(visible) {
			otpElementLabel.setText(label);
			otpElementInstructionsLabel.setText(instructions);
			otpElementEdittext.setText(code);
			
			otpElementInstructionsLabel.setVisibility(instructions.equals(Constants.EMPTY_STRING) ? View.GONE : View.VISIBLE);
		}
		
		if(Constants.EMPTY_STRING.equals(code)) {
			otpElementEdittext.setEnabled(true);
			otpElementEdittext.setTransformationMethod(null);
		} else {
			otpElementEdittext.setEnabled(false);
			otpElementEdittext.setTransformationMethod(PasswordTransformationMethod.getInstance());
		}
	}
	//#endregion
	
	@Override
	public void showInformationAlert(final int msg, final String format) {
		showInformationAlert(
			String.format(
				getString(R.string.bmovil_confirmacion_asignacion_spei_error_missing_otp), format));
	}

	@Override
	public void showInformationAlert(final int idMsg) {
		//ownerController.showInformationAlert(errorMessage);
		showInformationAlert(getString(idMsg));
	}


	/**
	 * Reset the text values for all the authentication elements.
	 */
	@Override
	public void reset() {
		cardElementEdittext.setText(Constants.EMPTY_STRING);
		nipElementEdittext.setText(Constants.EMPTY_STRING);
		pwdElementEdittext.setText(Constants.EMPTY_STRING);
		cvvElementEdittext.setText(Constants.EMPTY_STRING);
		otpElementEdittext.setText(Constants.EMPTY_STRING);
	}
	
	/**
	 * Reset the text values for all the authentication elements.
	 */
	//@Override
	public void cleanAuthenticationFields() {
		cardElementEdittext.setText(Constants.EMPTY_STRING);
		nipElementEdittext.setText(Constants.EMPTY_STRING);
		pwdElementEdittext.setText(Constants.EMPTY_STRING);
		cvvElementEdittext.setText(Constants.EMPTY_STRING);
		otpElementEdittext.setText(Constants.EMPTY_STRING);
	}

	@Override
	public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
	}

	@Override
	public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
	}

	@Override
	public void afterTextChanged(final Editable s) {
	}
	

	@Override
	public void showProgress() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hideProgress() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ConfirmacionAsignacionSpeiViewTo getViewTo() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void setViewTo(final ConfirmacionAsignacionSpeiViewTo viewTo) {
		this.viewTo = viewTo;
	}
	
}
