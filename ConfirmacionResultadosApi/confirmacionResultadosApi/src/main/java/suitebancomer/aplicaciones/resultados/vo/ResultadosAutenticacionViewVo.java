/**
 * 
 */
package suitebancomer.aplicaciones.resultados.vo;

import java.io.Serializable;

/**
 * @author amgonzalez
 *
 */
public class ResultadosAutenticacionViewVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1092028826946939850L;
	private String textoTituloResultado;
	private String textoPantallaResultados;
	private String textoAyudaResultados;
	private String textoEspecialResultados;
	private int colorTituloResultado;
	private int imagenBotonResultados;

	
	
	/**
	 * @return the colorTituloResultado
	 */
	public int getColorTituloResultado() {
		return colorTituloResultado;
	}

	/**
	 * @return the imagenBotonResultados
	 */
	public int getImagenBotonResultados() {
		return imagenBotonResultados;
	}

	/**
	 * @param imagenBotonResultados the imagenBotonResultados to set
	 */
	public void setImagenBotonResultados(final int imagenBotonResultados) {
		this.imagenBotonResultados = imagenBotonResultados;
	}

	/**
	 * @param colorTituloResultado the colorTituloResultado to set
	 */
	public void setColorTituloResultado(final int colorTituloResultado) {
		this.colorTituloResultado = colorTituloResultado;
	}


	/**
	 * @return the textoTituloResultado
	 */
	public String getTextoTituloResultado() {
		return textoTituloResultado;
	}

	/**
	 * @param textoTituloResultado the textoTituloResultado to set
	 */
	public void setTextoTituloResultado(final String textoTituloResultado) {
		this.textoTituloResultado = textoTituloResultado;
	}

	/**
	 * @return the textoPantallaResultados
	 */
	public String getTextoPantallaResultados() {
		return textoPantallaResultados;
	}

	/**
	 * @param textoPantallaResultados the textoPantallaResultados to set
	 */
	public void setTextoPantallaResultados(final String textoPantallaResultados) {
		this.textoPantallaResultados = textoPantallaResultados;
	}

	/**
	 * @return the textoAyudaResultados
	 */
	public String getTextoAyudaResultados() {
		return textoAyudaResultados;
	}

	/**
	 * @param textoAyudaResultados the textoAyudaResultados to set
	 */
	public void setTextoAyudaResultados(final String textoAyudaResultados) {
		this.textoAyudaResultados = textoAyudaResultados;
	}

	/**
	 * @return the textoEspecialResultados
	 */
	public String getTextoEspecialResultados() {
		return textoEspecialResultados;
	}

	/**
	 * @param textoEspecialResultados the textoEspecialResultados to set
	 */
	public void setTextoEspecialResultados(final String textoEspecialResultados) {
		this.textoEspecialResultados = textoEspecialResultados;
	}


}
