package com.bancomer.base.callback;

import android.app.Activity;
import android.content.Context;

import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.LoginData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Reactivacion;

public interface CallBackBConnect {

	/**
	 * Usada por el api de contratacion para regresar a
	 * la app base una vez que termina el proceso de contracion
	 * y muestra el mensaje que en breve reciviras un mensaje de texto
	 * normalmete usado para regresar a la pantalla principal.
	 */
	void returnToPrincipal();

	/**
	 * Usada por el api de softoken y por el api de activacion
	 * para regresar al app principal una vez terminada su funcion
	 * por ejemplo al tener una activacion exitosa o una activacion exitosa de softoken.
	 *
	 * si el parametro response es nulo entonces no se realiza nada mas que regresar
	 * a la pantalla que el app necesite. en caso que el parametro response contenga un objeto de tipo string
	 * se recomienda utilizar las siguentes linas para resetear softoken.
	 *
	 * SofttokenSession session = SofttokenSession.getCurrent();
	 * session.setSofttokenActivated(false);
	 *
	 *
	 * @param response
	 */
	void returnObjectFromApi(Object response);

	/**
	 *
	 * Usada por el api de activacion al precionar el link de generar nueva clave (link color rojo en pantalla de activacion)
	 * su implementacion depende de las reglas de negocio que el app tenga.
	 * por ejemplo para el flujo de bmovil su funcion es que cuando se precione el link antes
	 * mencionado se regrese a el api de reactivacion con las siguientes lineas.
	 *
	 *InitReactivacion initReactivacion = new InitReactivacion(new BanderasServer(Server.SIMULATION,
	 *Server.ALLOW_LOG, Server.EMULATOR, Server.DEVELOPMENT),
	 *SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp(),this,
	 *SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp());
	 *SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp().ocultaIndicadorActividad();
	 *initReactivacion.showReactivacion(reactivacion,
	 *SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().estados);

	 * @param reactivacion
	 */
	void showReactivacion(Reactivacion reactivacion);

	void showReactivacion(ConsultaEstatus consultaEstatus, String password);

	/**
	 * usado por el api de contratacion y el api de mantenimiento.
	 * contratacion invoca este metodo cuando alguno de sus activitys pierde el foco por ejemplo en el boton home
	 * Mantenimiento usa este metodo cuando desea regresar a la app principal
	 *
	 *
	 *
	 * los parametros que retorna son
	 * @param context el contexto usado por el api
	 * @param activity activity enviado cuando se inicializo el api (activityToReturn)
	 * @param cleanSession bandera que indica si se debera recargar los archivos de configuracion
	 *                        (unicamente usada por bmovil no tener en cuanta para otras app)
	 *
	 * su funcion principal es redirigir a un activity una vez que se llama a este medoto por ejemplo
	 *                     Intent launchActivity=new Intent(context,activity.getClass());
							context.startActivity(launchActivity);
	 */
	void returnToActivity(Context context, Activity activity,boolean cleanSession);

	/**
	 * Invocado por el api de activacion una vez que se concluye con la activacion exitosa
	 * la implementacion depende de el app base.
	 * la diferencia con el metodo returnObjectFromApi es que activacion
	 * llama el metodo "returnObjectFromApi" cuando se lanza el api desde aplicacion desactivada
	 * y llama el metodo "returnLogin" cuando activacion se lanza desde el api de login
	 *
	 * @param numeroCelular de usuario
	 * @param password de usuario
	 */
	void returnLogin(String numeroCelular, String password);


	/**
	 * Invocado por el api de Administracion y el api de ApiPagarCreditos
	 * su funcion es regresar a la app base una vez que se concluye el pago del credito
	 * o el api administracion termina su finciones.
	 *
	 *
	 * su implementacion depende completamente del ap base
	 */
	void returnMenuPrincipal();

	/**
	 * Invocado por las apis activacion,confirmacionResultadosApi,loginApi,Softoken
	 * usado por bmovil para recargar archivos de configuracion.
	 * su uso en otras aplicaciones no es necesario.
	 */
	void returDesactivada();

	/**
	 * invocado por el api de  administracion, apipagarcreditos,confirmacion y resultados
	 * su funcion es informar que el usuario aun sigure interactuando con la aplicacion
	 * y es llamado como su nombre lo indica cada vez que el usuario interactua con las apis
	 * su implemetacion depende completamente de el app base y generalmente es usado
	 * cuanso se tiene un timeout en la app base para evitar que el timeour lllgue a su fin
	 */
	void userInteraction();

	/**
	 * Invocado por las apis administracion,apipagarcreditos,confirmacionResultadosApi,mantenimiento
	 * lo que realice este metodo es propio de las reglas de cada app sin embargo puede ser
	 * utilizado para hacer un cierre de session cuando el usuario salga de la app inesperadamente
	 * ya sea por abrir otra app o por precionar home pues el metodo detecta cuando se llega al estado de
	 * stop de un activity en las apis antes mencionadas .
	 * @param isChanging
	 */
	void cierraSesionBackground(Boolean isChanging);

	/**
	 * Invocado por las apis administracion,apipagarcreditos,confirmacionResultadosApi,mantenimiento
	 * lo que realice este metodo es propio de las reglas de cada app sin embargo puede ser
	 * utilizado para hacer un cierre de session en el front mientras que el metodo cierraSesionBackground
	 * se puede utilizar cuando se realiza un cierre de session en el back por ejemplo al bloquera el telefono
	 */
	void cierraSesion();


	/**
	 * Invocado por el api de login una vez que se realizo la peticon de login al servidor,
	 * su funcion es continuar con el flujo del app base una vez que se realizo el login exitoso
	 *
	 * los flujos en el que puede entrar son.
	 * cuando se realiza un login exitos, el api login llama este metodo para informar que el api fue exitoso y que puede
	 * 				continuar con el flujo del app principal enviando los parametro llenos con los datos del login.
	 *
	 * 	cuando el login no es exitoso y la bandera continuaFlujoBconnect es enviada en false al invocar el api login.
	 * 				continua el flujo del app principal enviando el parametro loginData con valor nulo.
	 *
	 * 	Nota: cuando el login no es exitoso y la bandera continuaFlujoBconnect es enviada en true no se invoca
	 * 	este metodo en su lugar se invoca el api correspondiente segun la respuesta recivida del servidor.
	 *
	 * @param loginData (objeto llenado por api login en caso de exito)
	 * @param consultaEstatus (objeto llenado por api login)
	 * @param responsePlain (respuesta tal cual la manda el servidor web en texto plano)
	 */
	void returnLoginApi(LoginData loginData,ConsultaEstatus consultaEstatus,String responsePlain);


	/**
	 * Invoacada por el api aplicacion desactivada cuando la bandera continuaFlujoBconnect
	 * es enviada en falso al invocar el api de aplicacion desactivada.
	 *
	 * si la bandera continuaFlujoBconnect es enviada en true entonces no se invoca a este metodo en
	 * su lugar se invoca el api de bconnect correspondiente segun la respuesta del servidor
	 * al hacer la peticion.
	 *
	 * @param consultaEstatus
	 */
	void returnDesactivadaApi(ConsultaEstatus consultaEstatus);

	/**
	 * metodo que es llamado por el api de login
	 * con el fin de limpiar el campo contraseña cuando el
	 * usuario introduce una contraseña incrrecta.
	 */
	void limpiarPasswordLogin();
}
