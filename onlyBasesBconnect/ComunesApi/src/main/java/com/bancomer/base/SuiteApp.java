package com.bancomer.base;

import android.app.Application;
import android.content.Context;

import suitebancomercoms.classes.common.PropertiesManager;

public class SuiteApp extends Application {
	public static boolean isSubAppRunning;
	public static Context appContext;
	private static SuiteApp me;
	public static Context contextoActual;

	private static boolean isReactivacion = Boolean.FALSE;

	@Override
	public void onCreate() {
		super.onCreate();
		isSubAppRunning = false;
		me = this;
		appContext = getApplicationContext();
	};

	public void onCreate(final Context context){
		super.onCreate();
		isSubAppRunning = false;
		me = this;
		appContext = context;
	}


	public static SuiteApp getInstance() {
		return me;
	}
	
	public static boolean getSofttokenStatus() {
		return PropertiesManager.getCurrent().getSofttokenActivated();
	}
	
	public static boolean isInstanceClassOf(final Class<?> nameClassObject, final Class<?> nameClass){
	
		if(nameClassObject == null || nameClass == null){
			return false;
		}
			
		return nameClassObject.getName().equals(nameClass.getName());

	}

	public static int getResourceId(final String nombre, final String tipo){
		return appContext.getResources().getIdentifier(nombre, tipo, appContext.getPackageName());
	}

	public static boolean isReactivacion() {
		return isReactivacion;
	}

	public static void setIsReactivacion(final boolean isReactivacion) {
		SuiteApp.isReactivacion = isReactivacion;
	}

}
