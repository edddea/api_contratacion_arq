package suitebancomercoms.classes.gui.controllers;

import java.util.LinkedHashMap;

import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

public class BaseViewsControllerCommons {

	protected static LinkedHashMap<Long, BaseDelegateCommons> delegatesHashMap;
	
	private boolean isActivityChanging;

	public BaseViewsControllerCommons() {
		if(delegatesHashMap == null) {
			delegatesHashMap = new LinkedHashMap<Long, BaseDelegateCommons>();
		}
	}

	public BaseViewControllerCommons getCurrentViewControllerApp() {
		return null;
	}

	public void setCurrentActivityApp(final BaseViewControllerCommons currentViewController) {
		// Empty method
	}
	
	public boolean isActivityChanging() {
		return isActivityChanging;
	}
	
	public void setActivityChanging(final boolean flag) {
		isActivityChanging = flag;
	}
	
	public void cierraViewsController() {
		if (delegatesHashMap.size()>0){
			delegatesHashMap.clear();
		}
	}
	
	public void addDelegateToHashMap(final long id, final BaseDelegateCommons baseDelegate){
		delegatesHashMap.put(Long.valueOf(id), baseDelegate);
	}
	
	public BaseDelegateCommons getBaseDelegateForKey(final long key) {
		return delegatesHashMap.get(Long.valueOf(key));
	}
	
	public Long getLastDelegateKey() {
		return (Long) delegatesHashMap.keySet().toArray()[delegatesHashMap.size() - 1];
	}
	
	public void removeDelegateFromHashMap(final long key) {
		delegatesHashMap.remove(Long.valueOf(key));
	}
	
	public void clearDelegateHashMap() {
		delegatesHashMap.clear();
	}
	
	public void overrideScreenForwardTransition() {
		// Empty method
	}

	public void overrideScreenBackTransition() {
		// Empty method
	}
	
    public void onUserInteraction() {
		// Empty method
	}

    public boolean consumeAccionesDePausa() {
    	return false;
    }

    public boolean consumeAccionesDeReinicio() {
    	return false;
    }

   public boolean consumeAccionesDeAlto() {
   	return false;
    }

	public void showMenuInicial(){
		// Empty method
	}
	
	public BaseViewControllerCommons getCurrentViewController() {
		return null;
	}

	public void showConsultarEstatusEnvioEstadodeCuenta(){
		// Empty method
	}
}


