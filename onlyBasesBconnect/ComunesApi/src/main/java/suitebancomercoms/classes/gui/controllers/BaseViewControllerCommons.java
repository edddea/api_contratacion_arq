/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomercoms.classes.gui.controllers;

import android.app.Activity;
import android.content.DialogInterface.OnClickListener;
import android.view.KeyEvent.Callback;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

/**
 * BaseScreen is the base class for all the screens in the MBanking application
 * It provides basic, common functionality to be included in the business logic
 * so that the application can manage screens in a uniform, consistent way.
 *
 * @author Stefanini IT Solutions
 */
public abstract class BaseViewControllerCommons extends Activity implements Callback {

    /**
     * Determines if the user touched or performed some action in the screen.
     * If he or she did, then reset the logout timer.
     */
	public void onUserInteraction() {
		// Empty method
	}

    /**
     * Hides the soft keyboard if any editbox called it.
     */
    public void hideSoftKeyboard() {
		// Empty method
    }

    /**
     * This is called whenever the back button is pressed.
     * Normally, this function will return to the previous screen.
     */
    public void goBack() {
		// Empty method
    }

    public void showErrorMessage(final String errorMessage) {
		// Empty method
    }

    public void showErrorMessage(final int errorMessage) {
    	// Empty method
    }

    
    /**
     * Places a progress dialog, for long waiting processes.
     * 
     * @param strTitle the dialog title
     * @param strMessage the message to show in the while
     */
    public void muestraIndicadorActividad(final String strTitle, final String strMessage) {
		// Empty method
    }

    /**
     * Hides the progress dialog.
     */
    public void ocultaIndicadorActividad() {
		// Empty method
    }

	public BaseViewsControllerCommons getParentViewsController() {
		return null;
	}

	public void setParentViewsController(final BaseViewsControllerCommons parentViewsController) {
		// Empty method
	}

	public void hideCurrentDialog() {
		// Empty method
	}
    /**
     * Muestra una alerta con el mensaje dado.
     * @param message el mensaje a mostrar
     */
    public void showInformationAlert(final int message) {
		// Empty method
    }
    
    /**
     * Shows an information alert with the given message text. No action is performed after this alert is closed.
     * @param message the message text to show
     */
    public void showInformationAlert(final String message) {
		// Empty method
    }

    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param title The alert title resource id.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(final String title, final String message, final OnClickListener listener) {
		// Empty method
    }

    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param title The alert title resource id.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlertEspecial(final String title, final String errorCode, final String descripcionError, final OnClickListener listener) {
		// Empty method
    }

    /**
	 * Shows an alert dialog.
	 * @param title The alert title.
	 * @param message The alert message.
	 * @param okText the ok text
	 * @param listener The listener for the "Ok" button.
	 */
	public void showInformationAlert(final String title, final String message, final String okText, final OnClickListener listener) {
		// Empty method
    }

	 /**
      * Shows an alert dialog.
	  * @param title The alert title.
      * @param message The alert message.
      * @param okText the ok text
      * @param listener The listener for the "Ok" button.
      */
     public void showInformationAlertEspecial(final String title, final String errorCode, final String descripcionError, final String okText, final OnClickListener listener) {
		// Empty method
     }

	public void showYesNoAlert(final String message, final OnClickListener positiveListener) {
		// Empty method
	}

	public void showYesNoAlertEspecialTitulo(final String errorCode, final String descripcionError, final OnClickListener positiveListener) {
		// Empty method
	}

	public void showYesNoAlert(final int message, final OnClickListener positiveListener) {
		// Empty method
	}

	public void showYesNoAlert(final int message, final OnClickListener positiveListener, final OnClickListener negativeListener) {
		// Empty method
	}

	public void showYesNoAlert(final String title, final String message, final OnClickListener positiveListener) {
		// Empty method
	}

    public void showYesNoAlert(final String message, final OnClickListener positiveListener, final OnClickListener negativeListener) {
		// Empty method
	}

	public void showYesNoAlertEspecialTitulo(final String errorCode, final String descripcionError, final OnClickListener positiveListener, final OnClickListener negativeListener) {
		// Empty method
	}

	public void showYesNoAlert(final int title, final int message, final OnClickListener positiveListener) {
		// Empty method
	}

	public void showYesNoAlert(final String title, final String message, final String okText, final OnClickListener positiveListener) {
		// Empty method
	}

	public void showYesNoAlert(final int title, final int message, final int okText, final OnClickListener positiveListener) {
		// Empty method
	}

	public void showYesNoAlert(final String title, final String message, final String okText, final String calcelText, final OnClickListener positiveListener) {
		// Empty method
	}

	public void showYesNoAlert(final int title, final int message, final int okText, final int calcelText, final OnClickListener positiveListener) {
		// Empty method
	}

	public void showYesNoAlert(final String title,
							   final String message,
							   final String okText,
							   final String calcelText,
							   final OnClickListener positiveListener,
							   final OnClickListener negativeListener) {
		// Empty method
    }

	public void showYesNoAlert(final int title,
			   final int message,
			   final int okText,
			   final int calcelText,
			   final OnClickListener positiveListener,
			   final OnClickListener negativeListener) {
		// Empty method
	}
	public void showYesNoAlert1(final int message,
								final OnClickListener positiveListener,
								final OnClickListener negativeListener) {
		// Empty method
	}
	public void showYesNoAlert1(final String message,
								final OnClickListener positiveListener,
								final OnClickListener negativeListener) {
		// Empty method
	}
    public void processNetworkResponse(final int operationId, final ServerResponse response) {
		// Empty method
	}
	public void setHabilitado(final boolean habilitado) {
		// Empty method
	}
	public void plegarOpcionAplicacionDesactivada() {
		// Empty method
	}
	 public void showInformationAlert(final int title, final int message, final OnClickListener listener) {
		 // Empty method
	 }
	 public void showInformationAlert(final String message, final OnClickListener listener) {
		 // Empty method
	 }
	
	 public void showInformationAlert(final int message, final OnClickListener listener) {
		 // Empty method
	 }
	 public void showErrorMessage(final String errorMessage, final OnClickListener listener){
		 // Empty method
	 }
}