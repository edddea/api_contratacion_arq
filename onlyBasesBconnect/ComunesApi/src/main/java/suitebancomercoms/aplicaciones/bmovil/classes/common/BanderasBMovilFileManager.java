package suitebancomercoms.aplicaciones.bmovil.classes.common;

import android.content.Context;
import android.util.Log;

import com.bancomer.base.SuiteApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public final class BanderasBMovilFileManager {
	// #region Constantes.
	/**
	 * Nombre del archivo de propiedades.
	 */
	private static final String BANDERASBMOVIL_FILE_NAME = "BanderasBMovil.prop";
		
	/**
	 * 
	 */
	public static final String PROP_BANDERAS_CAMBIO_PERFIL = "cambioPerfil";
	
	/**
	 * 
	 */
	public static final String PROP_BANDERAS_CONTRATAR = "contratarBmovil";
	
	/**
	 * 
	 */
	public static final String PROP_BANDERAS_CONTRATAR2x1 = "contratar2x1";
	
	/**
	 * 
	 */
	public static final String PROP_BANDERAS_CAMBIO_CELULAR = "cambiodeCelular";

	/**
	 *
	 */
	public static final String PROP_BANDERAS_INDICADOR_CONTRATACION = "indicadorContratacion";

	
	// #endregion
	
	// #region Variables.
	/**
	 * Propiedades de la aplicacion.
	 */
	private Properties properties;
	// #endregion
	
	// #region Singleton.
	/**
	 * La instancia de la clase.
	 */
	private static BanderasBMovilFileManager manager = null;
	
	/**
	 * @return La instancia de la clase.
	 */
	public static BanderasBMovilFileManager getCurrent() {
		if(null == manager)
			manager = new BanderasBMovilFileManager();
		return manager;
	}
	
	/**
	 * Inicializa el administrador de propiedades. 
	 */
	private BanderasBMovilFileManager() {
		properties = null;
		final File file = new File(SuiteApp.appContext.getFilesDir(), BANDERASBMOVIL_FILE_NAME);
		
		if(!file.exists())
			initBanderasBMovilFile(file);
		else
			loadBanderasBMovilFile();
	}
	// #endregion
	
	// #region Carga del archivo de propiedades.
	/**
	 * Inicializa el archivo de propiedades con los valores iniciales para cada propiedad.
	 * @param file
	 */
	private void initBanderasBMovilFile(final File file) {
		try {
			file.createNewFile();
		} catch(IOException ioEx) {
			if(ServerCommons.ALLOW_LOG)Log.e(this.getClass().getName(), "Error al crear el archivo BanderasBMovil.", ioEx);
			return;
		}
				
		loadBanderasBMovilFile();
		
		if(null != properties) {
			setPropertynValue(PROP_BANDERAS_CAMBIO_PERFIL, "0");
			setPropertynValue(PROP_BANDERAS_CONTRATAR, "0");
			setPropertynValue(PROP_BANDERAS_CONTRATAR2x1, "0");
			setPropertynValue(PROP_BANDERAS_CAMBIO_CELULAR, "0");
			setPropertynValue(PROP_BANDERAS_INDICADOR_CONTRATACION, "0");
		}
	}
	
	/**
	 * Carga el archivo de propiedades.
	 */
	private void loadBanderasBMovilFile() {
		InputStream input;
		try {
			input = SuiteApp.appContext.openFileInput(BANDERASBMOVIL_FILE_NAME);
		} catch(FileNotFoundException fnfEx) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargar el archivo BanderasBMovil para lectura.", fnfEx);
			return;
		}

		properties = new Properties();
		try {
			properties.load(input);
		} catch(IOException ioEx) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargar BanderasBMovil.", ioEx);
			properties = null;
			return;
		}
	}
	// #endregion
	
	// #region Generic getters and setters for properties.
	/**
	 * Establece el valor especificado a la propiedad indicada.
	 * @param propertyName El nombre de la propiedad.
	 * @param value El valor a establecer.
	 */
	private void setPropertynValue(final String propertyName, String value) {
		if(null == properties || Tools.isEmptyOrNull(propertyName))
			return;
		if(null == value)
			value = "";
		
		properties.setProperty(propertyName, value);
		storeFileForProperty(propertyName, value);
	}
	
	/**
	 * Obtiene el valor de la propiedad especificada.
	 * @param propertyName El nombre de la propiedad.
	 * @return El valor de la propiedad.
	 */
	private String getPropertynValue(final String propertyName) {
		if(null == properties) {
			return null;
		}
		return properties.getProperty(propertyName);
	}
	// #endregion
	
	// #region Setters y Getters del estatus de activación para las aplicaciones.	
	public String getBanderasBMovilCambioPerfil(){
		return getPropertynValue(PROP_BANDERAS_CAMBIO_PERFIL);
	}
	
	public String getBanderasBMovilContratar(){
		return getPropertynValue(PROP_BANDERAS_CONTRATAR);
	}
	
	public String getBanderasBMovilContratar2x1(){
		return getPropertynValue(PROP_BANDERAS_CONTRATAR2x1);
	}
	
	public String getBanderasBMovilCambioCelular(){
		return getPropertynValue(PROP_BANDERAS_CAMBIO_CELULAR);
	}

	public String getBanderasBMovilIndicadorContratacion(){
		String indicaContratacion=getPropertynValue(PROP_BANDERAS_INDICADOR_CONTRATACION);
		if(indicaContratacion==null){
			indicaContratacion="0";
		}
		return indicaContratacion;
	}
	
	
		
	public void setBanderasBMovilCambioPerfil(final String value){
		setPropertynValue(PROP_BANDERAS_CAMBIO_PERFIL, value);
	}
	
	public void setBanderasBMovilContratar(final String value){
		setPropertynValue(PROP_BANDERAS_CONTRATAR, value);
	}
	
	public void setBanderasBMovilContratar2x1(final String value){
		setPropertynValue(PROP_BANDERAS_CONTRATAR2x1, value);
	}
	
	public void setBanderasBMovilCambioCelular(final String value){
		setPropertynValue(PROP_BANDERAS_CAMBIO_CELULAR, value);
	}

	public void setBanderasBMovilIndicadorContratacion(final String value){
		setPropertynValue(PROP_BANDERAS_INDICADOR_CONTRATACION, value);
	}
	
	// #endregion	
	
	/**
	 * Guarda el archivo de propiedades para preservar el cambio en alguna de las propiedades. 
	 * El nombre y valor de la propiedad modificada con usados para registrar el proceso mediante el log.  
	 * @param propertyName El nombre de la propiedad a guardar.
	 * @param propertyValue El valor de la propiedad a guardar.
	 * @return True si el archivo fue guardado exitosamente, False de otro modo.
	 */
	private boolean storeFileForProperty(String propertyName, final String propertyValue)	{
		if(Tools.isEmptyOrNull(propertyName))
			propertyName = "No name indicated.";
		if(Tools.isEmptyOrNull(propertyValue))
			propertyName = "No value indicated.";
		
		OutputStream output;
		try {	
			output = SuiteApp.appContext.openFileOutput(BANDERASBMOVIL_FILE_NAME, Context.MODE_PRIVATE);
		} catch(FileNotFoundException fnfEx) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al abrir el archivo para guardar la propiedad " + propertyName, fnfEx);
			return false;
		}
		
		try {
			properties.store(output, null);
		} catch (IOException ioEx) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al guardar en el archivo BanderasBMovil los valores: " + propertyName + " - " + propertyValue, ioEx);
			return false;
		}
		
		if(ServerCommons.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Archivo actualizado con los siguientes cambios: " + propertyName + " - " + propertyValue);
		return true;
	}
}
