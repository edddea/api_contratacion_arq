package suitebancomercoms.aplicaciones.bmovil.classes.model;

import java.util.HashMap;

import bancomer.api.common.model.Compania;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;

public class MantenimientoSpei {
	//#region Class structs and enums.
	/**
	 * The available operation types.
	 * <br/> 
	 * To retrieve the operation as a String use toString method.
	 */
	public enum TiposOperacionSpei {
		Associate(Constants.SPEI_OPERATION_TYPE_ASSOCIATE, Constants.SPEI_OPERATION_TYPE_FOR_SERVER_ASSOCIATE),
		Diassociate(Constants.SPEI_OPERATION_TYPE_DIASSOCIATE, Constants.SPEI_OPERATION_TYPE_FOR_SERVER_DIASSOCIATE),
		Modify(Constants.SPEI_OPERATION_TYPE_MODIFY, Constants.SPEI_OPERATION_TYPE_FOR_SERVER_MODIFY);
		
		/**
		 * The operation code.
		 */
		private String operationCode;
		
		/**
		 * The operation code for the Server.
		 */
		private String operationCodeForServer;
		
		/**
		 * @return The operation code.
		 */
		public String getOperationCode() {
			return operationCode;
		}
		
		/**
		 * @return The operation code for the Server.
		 */
		public String getOperationCodeForServer() {
			return operationCodeForServer;
		}
		
		@Override
		public String toString() {
			return operationCode;
		}
		
		/**
		 * Initialize the struct.
		 * @param code The operation code to be shown in confirmation and result tables. 
		 * @param codeForServer The operation code to be sent to the server transaction.
		 */
		TiposOperacionSpei(final String code, final String codeForServer) {
			operationCode = code;
			operationCodeForServer = codeForServer;
		}
	}
	//#endregion
	
	//#region Class fields.
	/**
	 * The account to operate.
	 */
	//private Account cuentaAsignada;
	
	private String cuentaAsignada;
	
	/**
	 * The phone to operate.
	 */
	private String celularAsociado;
	
	/**
	 * The company which the phone belongs.
	 */
	private HashMap<String, String> companiaMap;
	
	/**
	 * The operation type.
	 */
	private TiposOperacionSpei tipoOperacion;
	
	/**
	 * The selected company.
	 */
	private Compania company;
	//#endregion
	
	//#region Getters and Setters.
	/**
	 * @return The account to operate.
	 */
	public String getCuentaAsignada() {
		return cuentaAsignada;
	}

	/**
	 * @param cuentaAsignada The account to operate.
	 */
	public void setCuentaAsignada(final String cuentaAsignada) {
		this.cuentaAsignada = cuentaAsignada;
	}

	/**
	 * @return The phone to operate.
	 */
	public String getCelularAsociado() {
		return celularAsociado;
	}

	/**
	 * @param celularAsociado The phone to operate.
	 */
	public void setCelularAsociado(final String celularAsociado) {
		this.celularAsociado = celularAsociado;
	}

	/**
	 * @return The company which the phone belongs.
	 */
	public HashMap<String, String> getCompaniaMap() {
		return companiaMap;
	}

	/**
	 * @return The selected company.
	 */
	public Compania getCompany() {
		return company;
	}
	
	/**
	 * @param compania The company which the phone belongs.
	 */
	public void setCompany(final Compania company) {
		this.company = company;
		
		if(null == companiaMap)
			companiaMap = new HashMap<String, String>();
		else
			companiaMap.clear();
		
		companiaMap.put(company.getClave(), company.getNombre());
	}

	/**
	 * @return The operation type.
	 */
	public TiposOperacionSpei getTipoOperacion() {
		return tipoOperacion;
	}

	/**
	 * @param tipoOperacion The operation type.
	 */
	public void setTipoOperacion(final TiposOperacionSpei tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}
	//#endregion
	
	public MantenimientoSpei() {
		cuentaAsignada = null;
		tipoOperacion = null;
		celularAsociado = Constants.EMPTY_STRING;
		company = null;
		companiaMap = new HashMap<String, String>();
	}
}
