/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomercoms.aplicaciones.bmovil.classes.io;

import java.util.Hashtable;

/**
 * Server is responsible of acting as an interface between the application
 * business logic and the HTTP operations. It receive the network operation
 * requests and builds a network operation suitable for HttpInvoker class, which
 * eventually will perform the network connection. Server returns the server
 * response as a ServerResponse object, so that the application logic can
 * perform high level checks and processes.
 * 
 * @author Stefanini IT Solutions.
 */
public class Server extends ServerCommons {
	
	// public static boolean DEVELOPMENT = true; //TODO remove for TEST

	/**
	 * Indicates simulation usage.
	 */
	public static final long SIMULATION_RESPONSE_TIME = 5;

	/**
	 * Bancomer Infrastructure.
	 */
	public static final int BANCOMER = 0;

	/**
	 * Stefanini Infrastructure.
	 */
	public static final int STEFANINI = 1;

	/**
	 * Last operation provider.
	 */

	// Change this value to bancomer for production setup
	// public static int PROVIDER = STEFANINI;
	public static int PROVIDER = BANCOMER; // TODO remove for TEST

	// Simulates profile change status
	public static String INSTRUMENT_TYPE = "";// Constants.IS_TYPE_DP270;

	public static String PROFILE_RECORTADO = "*PRMF02*IS";
	public static String PROFILE_BASIC = "*PRMF01*IS";
	public static String PROFILE_ADVANCED_OCRA = "*PRMF03*IST6";
	public static String PROFILE_ADVANCED_DP270 = "*PRMF03*IST3";
	public static String PROFILE_ADVANCED_SOFTTOKEN = "*PRMF03*ISS2";
	public static String PROFILE_CHANGE_BASIC_TO_ADVANCED = "*PRMF01*IST3";
	public static String PROFILE_CHANGE_ADVANCED_TO_BASIC = "*PRMF03*ISD2";

	//public static String CURRENT_PROFILE_STRING = PROFILE_CHANGE_BASIC_TO_ADVANCED;
	public static String CURRENT_PROFILE_STRING = PROFILE_ADVANCED_SOFTTOKEN;//PROFILE_CHANGE_BASIC_TO_ADVANCED;


	// SIMULATES DIFFERENT ACCOUNT SETS
	public static String ACCOUNTS_ONE_EXPRESS = "ESOK*TED0026805*FE05012013*HR190752*TO2*CT*OC1*TPCE*AL*DVMX*AS00740036002753018236*IM0*CPC*VIS";
	public static String ACCOUNTS_ONE_ACCOUNT = "ESOK*TED0026805*FE05012013*HR190752*TO2*CT*OC1*TPLI*AL*DVMX*AS00743616002700628034*IM50000*CPC*VIS";
	public static String ACCOUNTS_TDC_IS_MAIN = "ESOK*TED0009006*FE07012013*HR085700*TO2*CT*OC3*TPTP*AL*DVMX*AS00740036002753594688*IM286504*CPC*VIN*TPTC*ALAZUL*DVMX*AS4931612268862560*IM000*CPC*VIN*TPTC*AL*DVMX*AS4931612501079881*IM000*CPC*VIS";
	public static String ACCOUNTS_MULTIPLE_ACCOUNTS_ZERO_BALANCE = "ESOK*TED0009006*FE05012013*HR085700*TO2*CT*OC5*TPTP*AL*DVMX*AS00740036002753594688*IM286504*CPC*VIS*TPLI*AL*DVMX*AS00740036002753938274*IM286504*CPC*VIN*TPCE*AL*DVMX*AS00740036002753018236*IM0*CPC*VIN*TPTC*ALAZUL*DVMX*AS4931612268862560*IM000*CPC*VIN*TPTC*AL*DVMX*AS4931612501079881*IM000*CPC*VIN";
	public static String ACCOUNTS_ALL_ACCOUNTS = "ESOK*TED0026805*FE27042015*HR062500*TO2*CT*OC8*TPLI*AL*DVMX*AS00743616002700628034*IM49999999998*CPC*VIS*TPAH*AL*DVMX*AS00743616002700142536*IM50000*CPC*VIN*TPCE*AL*DVMX*AS5520684849*IM0*CPC*VIN*TPCE*AL*DVMX*AS5520684850*IM0*CPC*VIN*TPTP*AL*DVMX*AS00743616002700846372*IM50000*CPC*VIN*TPCH*AL*DVMX*AS00743616002700894637*IM50000*CPC*VIN*TPTC*AL*DVMX*AS00743616002700374625*IM50000*CPC*VIN*TPTC*AL*DVMX*AS00743616002700374123*IM50000*CPC*VIN";
	public static String STATUS_NOT_A1 = "ESOK*TED0026805*FE19062013*HR171027*TO2*CT*OC0*VA*EI*STS4*LO1";
	public static String OPTIONAL_UPDATE = "ESAC*TED0027237*FE04072013*HR133027*TO2*CT*OC4*TPAH*AL*DVMX*AS00743616002900072907*IM4883862*CPC*VIS*TPCH*AL*DVMX*AS00743616000178463778*IM000*CPC*VIN*TPTC*AL*DVMX*AS4101810678996382*IM000*CPC*VIN*TPTC*AL*DVMX*AS4555456998072651*IM000*CPC*VIN";
	public static String MANDATORY_UPDATE = "ESERROR*COMBANK1111*MEBancomer móvil trae para tí la nueva opción de Dinero móvil, que te permitirá enviar dinero a quien tú quieras y cuando quieras, presiona Actualizar y comienza a utilizarla.*URhttps://www.bancomermovil.com/mbank/mbank/web/download.jsp?o=A";

	public static String CURRENT_ACCOUNT_SET = ACCOUNTS_ALL_ACCOUNTS;

	// Simulates diferent app status.
	// Simulate diferent fastpayment sets.
	public static String CODIGO_ESTATUS_APLICACION = "A1";
	public static String NO_FAST_PAYMENT = "*RP";
	// public static String NO_FAST_PAYMENT = "*RP{\"rapidas\":\"\"}";
	public static String FAST_PAYMENT_3 = "*RP{\"rapidas\":[{\"nombreCorto\":\"DOMO\",\"cuentaOrigen\":\"AH00741800000151951234\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"5000\",\"nombreBeneficiario\":\"\",\"companiaCelular\":\"Iusacell\",\"tipoRapido\":\"compraTiempoAire\",\"iDOperacion\":\"01\",\"concepto\":\"\"},{\"nombreCorto\":\"traspIvan\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"4152311034652888\",\"telefonoDestino\":\"\",\"importe\":\"15000\",\"nombreBeneficiario\":\"ARTURO ANTONIO ALVAREZ ALMAZAN\",\"companiaCelular\":\"\",\"tipoRapido\":\"otrosBancomer\",\"iDOperacion\":\"02\",\"concepto\":\"\"},{\"nombreCorto\":\"DineroM\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"9900000\",\"nombreBeneficiario\":\"Mario Mendez\",\"companiaCelular\":\"Telcel\",\"tipoRapido\":\"dineroMovil\",\"iDOperacion\":\"03\",\"concepto\":\"pagarTelefono\"}]}";
	public static String FAST_PAYMENT_5 = "*RP{\"rapidas\":[{\"nombreCorto\":\"DOMO\",\"cuentaOrigen\":\"AH00741800000151951234\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"5000\",\"nombreBeneficiario\":\"\",\"companiaCelular\":\"Iusacell\",\"tipoRapido\":\"compraTiempoAire\",\"iDOperacion\":\"01\",\"concepto\":\"\"},{\"nombreCorto\":\"traspIvan\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"4152311034652888\",\"telefonoDestino\":\"\",\"importe\":\"15000\",\"nombreBeneficiario\":\"ARTURO ANTONIO ALVAREZ ALMAZAN\",\"companiaCelular\":\"\",\"tipoRapido\":\"otrosBancomer\",\"iDOperacion\":\"02\",\"concepto\":\"\"},{\"nombreCorto\":\"DineroM\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"9900000\",\"nombreBeneficiario\":\"Mario Mendez\",\"companiaCelular\":\"Telcel\",\"tipoRapido\":\"dineroMovil\",\"iDOperacion\":\"03\",\"concepto\":\"pagarTelefono\"},{\"nombreCorto\":\"DOMO2\",\"cuentaOrigen\":\"AH00741800000151951234\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"5000\",\"nombreBeneficiario\":\"\",\"companiaCelular\":\"Telcel\",\"tipoRapido\":\"compraTiempoAire\",\"iDOperacion\":\"01\",\"concepto\":\"\"},{\"nombreCorto\":\"traspIva2\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"4152311034652888\",\"telefonoDestino\":\"\",\"importe\":\"15000\",\"nombreBeneficiario\":\"ARTURO ANTONIO ALVAREZ ALMAZAN\",\"companiaCelular\":\"\",\"tipoRapido\":\"otrosBancomer\",\"iDOperacion\":\"02\",\"concepto\":\"\"}]}";
	public static String FAST_PAYMENT_6 = "*RP{\"rapidas\":[{\"nombreCorto\":\"DOMO\",\"cuentaOrigen\":\"AH00741800000151951234\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"5000\",\"nombreBeneficiario\":\"\",\"companiaCelular\":\"Iusacell\",\"tipoRapido\":\"compraTiempoAire\",\"iDOperacion\":\"01\",\"concepto\":\"\"},{\"nombreCorto\":\"traspIvan\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"4152311034652888\",\"telefonoDestino\":\"\",\"importe\":\"15000\",\"nombreBeneficiario\":\"ARTURO ANTONIO ALVAREZ ALMAZAN\",\"companiaCelular\":\"\",\"tipoRapido\":\"otrosBancomer\",\"iDOperacion\":\"02\",\"concepto\":\"\"},{\"nombreCorto\":\"DineroM\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"9900000\",\"nombreBeneficiario\":\"Mario Mendez\",\"companiaCelular\":\"Telcel\",\"tipoRapido\":\"dineroMovil\",\"iDOperacion\":\"03\",\"concepto\":\"pagarTelefono\"},{\"nombreCorto\":\"DOMO2\",\"cuentaOrigen\":\"AH00741800000151951234\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"5000\",\"nombreBeneficiario\":\"\",\"companiaCelular\":\"Telcel\",\"tipoRapido\":\"compraTiempoAire\",\"iDOperacion\":\"01\",\"concepto\":\"\"},{\"nombreCorto\":\"traspIva2\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"4152311034652888\",\"telefonoDestino\":\"\",\"importe\":\"15000\",\"nombreBeneficiario\":\"ARTURO ANTONIO ALVAREZ ALMAZAN\",\"companiaCelular\":\"\",\"tipoRapido\":\"otrosBancomer\",\"iDOperacion\":\"02\",\"concepto\":\"\"},{\"nombreCorto\":\"DineroM2\",\"cuentaOrigen\":\"LI00740036002753594688\",\"cuentaDestino\":\"\",\"telefonoDestino\":\"5516947156\",\"importe\":\"9900000\",\"nombreBeneficiario\":\"Mario Mendez\",\"companiaCelular\":\"Telcel\",\"tipoRapido\":\"dineroMovil\",\"iDOperacion\":\"03\",\"concepto\":\"pagarTelefono\"}]}";

	public static String CURRENT_FAST_PAYMENT_SET = NO_FAST_PAYMENT;

	//-- OLD CATALOGS --
	//public static String OPERACIONES_CAT_AU_PREVIOUS = "{\"operaciones\":[{\"operacion\":\"contratacion\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"reactivacion\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"desbloqueo\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"login\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"transferirPropias\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"transferirBancomer\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"transferirBancomerR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"transferirInterbancaria\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"transferirInterbancariaR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"dineroMovil\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"pagoServicios\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"pagoServiciosF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"pagoServiciosR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"pagoServiciosP\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"compraTiempoAire\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"compraTiempoAireR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"compraComercios\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"cancelarDineroMovil\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"altaFrecuente\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"altaRapido\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"2\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"bajaFrecuente\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"bajaRapido\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"cambioTelefono\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"cambioCuenta\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"cambioLimites\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"administrarAlertas\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"suspenderCancelar\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"cambioPerfil\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"configurarCorreo\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"configurarAlertas\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"quitarSuspension\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"contratacionAlertas\",\"basico\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"actualizacionAlertas\",\"basico\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}}],\"version\":\"7\"}";
	//public static String OPERACIONES_CAT_AU = "{\"operaciones\":[{\"operacion\":\"contratacion\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"reactivacion\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"desbloqueo\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"login\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"transferirPropias\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"transferirBancomer\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"transferirBancomerR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"transferirInterbancaria\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"transferirInterbancariaR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"dineroMovil\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"pagoServicios\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"pagoServiciosF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"pagoServiciosR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"pagoServiciosP\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"2\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"compraTiempoAire\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"compraTiempoAireR\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"compraComercios\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"cancelarDineroMovil\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"altaFrecuente\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"2\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"altaRapido\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"2\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"bajaFrecuente\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"bajaRapido\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"cambioTelefono\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"cambioCuenta\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"cambioLimites\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"administrarAlertas\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"suspenderCancelar\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"cambioPerfil\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"configurarCorreo\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"configurarAlertas\",\"basico\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"quitarSuspension\",\"basico\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\"}},{\"operacion\":\"contratacionAlertas\",\"basico\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}},{\"operacion\":\"actualizacionAlertas\",\"basico\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"},\"avanzado\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\"}}],\"version\":\"5\"}";
	//public static String OPERACIONES_ALL_TRUE = "{\"operaciones\":[{\"operacion\":\"contratacion\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"reactivacion\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"desbloqueo\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"login\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirPropias\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"dineroMovil\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"pagoServicios\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"compraComercios\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"altaRapido\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"bajaRapido\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"cambioLimites\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"basico\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"},\"avanzado\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"true\"}}],\"version\":\"8\"}";
	
	//public static String OPERACIONES_CAT_AU = "{\"perfil\":[{\"recortado\":[{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}}]},{\"basico\":[{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}}]},{\"avanzado\":[{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"2\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}}]}],\"version\":\"9\"}";
	
	public static String OPERACIONES_CAT_AU = "{\"perfil\":[{\"recortado\":[{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"oneClickBmovilConsumo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"false\"}}]},{\"basico\":[{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"oneClickBmovilConsumo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"false\",\"visible\":\"false\"}}]},{\"avanzado\":[{\"operacion\":\"contratacion\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"reactivacion\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"desbloqueo\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"login\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirPropias\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomer\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirBancomerR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancaria\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"transferirInterbancariaR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"dineroMovilF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServicios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"pagoServiciosP\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAire\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"true\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireF\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraTiempoAireR\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"compraComercios\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cancelarDineroMovil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"altaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"2\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaFrecuente\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"bajaRapido\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioTelefono\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioCuenta\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioLimites\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"administrarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"suspenderCancelar\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"cambioPerfil\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarCorreo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"configurarAlertas\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"quitarSuspension\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"MantenimientoSpeimovil\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"solicitaAlertas\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"contratacionLink\",\"credenciales\":{\"contrasena\":\"false\",\"token\":\"0\",\"cvv2\":\"false\",\"nip\":\"false\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}},{\"operacion\":\"oneClickBmovilConsumo\",\"credenciales\":{\"contrasena\":\"true\",\"token\":\"1\",\"cvv2\":\"true\",\"nip\":\"true\",\"registro\":\"false\",\"operar\":\"true\",\"visible\":\"true\"}}]}],\"version\":\"12\"}";

	public static String LISTA_OPERACIONES = OPERACIONES_CAT_AU;

	public static String LISTA_TELEFONICAS = "[{\"companiaCelular\":\"TELCEL\",\"nombreImagen\":\"telcel.png\"},{\"companiaCelular\":\"MOVISTAR\",\"nombreImagen\":\"movistar.png\"},{\"companiaCelular\":\"IUSACELL\",\"nombreImagen\":\"iusacell.png\"},{\"companiaCelular\":\"UNEFON\",\"nombreImagen\":\"unefon.png\"}]";

	public static String CORREO_EJEMPLO = "ejemplo@gonet.us";

	// ///////////////////////////////////////////////////////////////////////////
	// Operations identifiers //
	// ///////////////////////////////////////////////////////////////////////////

	/**
	 * Activation step 1 operation.
	 */
	public static final int ACTIVATION_STEP1_OPERATION = 1;

	/**
	 * Acivation step 2 operation.
	 */
	public static final int ACTIVATION_STEP2_OPERATION = 2;

	/**
	 * Login oepration.
	 */
	public static final int LOGIN_OPERATION = 3;

	/**
	 * Movements operation.
	 */
	public static final int MOVEMENTS_OPERATION = 4;

	/**
	 * Transfer money operation between the user's accounts.
	 */
	public static final int SELF_TRANSFER_OPERATION = 5;

	/**
	 * Transfer money operation to someone else's account in Bancomer.
	 */
	public static final int BANCOMER_TRANSFER_OPERATION = 6;

	/**
	 * External transfer operation.
	 */
	public static final int EXTERNAL_TRANSFER_OPERATION = 7;

	/**
	 * Nipper store purchase operation.
	 */
	public static final int NIPPER_STORE_PURCHASE_OPERATION = 8;

	/**
	 * Nipper airtime purchase operation.
	 */
	public static final int NIPPER_AIRTIME_PURCHASE_OPERATION = 9;

	/**
	 * Change password operation.
	 */
	public static final int CHANGE_PASSWORD_OPERATION = 10;

	/**
	 * Close session operation.
	 */
	public static final int CLOSE_SESSION_OPERATION = 11;

	/**
	 * Get the owner of a credit card.
	 */
	public static final int CARD_OWNER_OPERATION = 12;

	/**
	 * Calculate fee.
	 */
	public static final int CALCULATE_FEE_OPERATION = 13;

	/**
	 * Calculate fee 2.
	 */
	public static final int CALCULATE_FEE_OPERATION2 = 14;

	/**
	 * Service payment.
	 */
	public static final int SERVICE_PAYMENT_OPERATION = 15;

	/**
	 * Help image for service payment.
	 */
	public static final int HELP_IMAGE_OPERATION = 16;

	/**
	 * Request frequent service payment operation list.
	 */
	public static final int FAVORITE_PAYMENT_OPERATION = 18;

	/**
	 * Fast Payment service.
	 */
	public static final int FAST_PAYMENT_OPERATION = 19;

	/**
	 * Service enterprise name operation.
	 */
	public static final int SERVICE_NAME_OPERATION = 20;

	/**
	 * Alta de operaciones sin tarjeta
	 */
	public static final int ALTA_OPSINTARJETA = 23;

	/**
	 * Consulta de operaciones sin tarjeta
	 */
	public static final int CONSULTA_OPSINTARJETA = 24;

	/**
	 * Baja de operaciones sin tarjeta
	 */
	public static final int BAJA_OPSINTARJETA = 25;

	/**
	 * Consulta status del servicio
	 */
	public static final int CONSULTA_ESTATUSSERVICIO = 26;

	/**
	 * Cambia perfil del cliente
	 */
	public static final int CAMBIA_PERFIL = 27;

	/**
	 * Consulta comision para transferir interbancario
	 */
	public static final int CONSULTAR_COMISION_I = 28;

	/**
	 * Consulta comision para transferir interbancario
	 */
	public static final int CONSULTAR_CODIGO_PAGO_SERVICIOS = 29;

	/**
	 * Preregistro de pago de servicios
	 */
	public static final int PREREGISTRAR_PAGO_SERVICIOS = 30;

	/**
	 * Compra de tiempo aire
	 */
	public static final int COMPRA_TIEMPO_AIRE = 31;

	/**
	 * Alta de frecuente
	 */
	public static final int ALTA_FRECUENTE = 32;

	/**
	 * Consulta del beneficiario
	 */
	public static final int CONSULTA_BENEFICIARIO = 33;

	/**
	 * Baja de frecuente
	 */
	public static final int BAJA_FRECUENTE = 34; // OP144

	/**
	 * Baja de frecuente
	 */
	public static final int CONSULTA_CIE = 35;

	/**
	 * 
	 */
	public static final int ACTUALIZAR_FRECUENTE = 36;

	/**
	 * Actualizacion de preregistrado a Frecuente
	 */
	public static final int ACTUALIZAR_PREREGISTRO_FRECUENTE = 37;

	/**
	 * Cambio de telefono asociado
	 * 
	 */
	public static final int CAMBIO_TELEFONO = 38;

	/**
	 * Cambio de cuenta Asociada
	 */
	public static final int CAMBIO_CUENTA = 39;

	/**
	 * Suspencion Temporal
	 */
	public static final int SUSPENDER_CANCELAR = 40;

	/**
	 * Actualizacion de cuentas del usuario
	 */
	public static final int ACTUALIZAR_CUENTAS = 41;

	public static final int CONSULTA_TARJETA_OPERATION = 42;

	/**
	 * Consulta de limites de operacion
	 */
	public static final int CONSULTAR_LIMITES = 43;

	/**
	 * Cambio de limites de operacion
	 */
	public static final int CAMBIAR_LIMITES = 44;

	/**
	 * Consulta de estatus mantenimiento.
	 */
	public static final int CONSULTA_MANTENIMIENTO = 45;

	/**
	 * Desbloqueo de contraseñas
	 */
	public static final int DESBLOQUEO = 46;

	/**
	 * Quitar suspension
	 */
	public static final int QUITAR_SUSPENSION = 47;

	/**
	 * Reactivacion
	 */
	public static final int OP_ACTIVACION = 48;

	/**
	 * Operación de contratación final para bmovil.
	 */
	public static final int OP_CONTRATACION_BMOVIL_ALERTAS = 49;

	/**
	 * Operación de consulta de terminos y condiciones de uso.
	 */
	public static final int OP_CONSULTAR_TERMINOS = 50;

	public static final int OP_ENVIO_CLAVE_ACTIVACION = 51;

	public static final int OP_CONFIGURAR_CORREO = 52;

	public static final int OP_ENVIO_CORREO = 53;

	public static final int OP_VALIDAR_CREDENCIALES = 54;

	public static final int OP_FINALIZAR_CONTRATACION_ALERTAS = 55;

	/** Activacion softtoken - Consulta de tipo de solicitud. */
	public static final int CONSULTA_TARJETA_ST = 56;

	/** Activacion softtoken - Autenticacion */
	public static final int AUTENTICACION_ST = 57;

	/**
	 * Activacion softtoken
	 */
	public static final int EXPORTACION_SOFTTOKEN = 58;
	/**
	 * Activacion softtoken
	 */
	public static final int SINCRONIZACION_SOFTTOKEN = 59;

	/** Activacion softtoken - Contratacion enrolamiento. */
	public static final int CONTRATACION_ENROLAMIENTO_ST = 60;

	/** Activacion softtoken - Finalizar contratacion. */
	public static final int FINALIZAR_CONTRATACION_ST = 61;

	/** Activacion softtoken - Cambio telefono asociado. */
	public static final int CAMBIO_TELEFONO_ASOCIADO_ST = 62;

	/** Activacion softtoken - Solicitud. */
	public static final int SOLICITUD_ST = 63;

	/** Mantenimiento Alertas. */
	public static final int MANTENIMIENTO_ALERTAS = 64;
	
	/** Consulta de Terminos y Condiciones Sesion */
	
	public static final int OP_CONSULTAR_TERMINOS_SESION = 65;
	
	/** Solicitar Alertas */
	
	public static final int OP_SOLICITAR_ALERTAS = 66;
	
	//Empieza codigo de SPEI revisar donde se usan las constantes
		/* Identifier for the request spei accounts operation.*/
		public static final int SPEI_ACCOUNTS_REQUEST = 67;//66

		/**
		 * Identifier for the request spei terms and conditions.
		 */
		public static final int SPEI_TERMS_REQUEST = 68; //67
		
		/**
		 * The SPEI maintenance operation.
		 */
		public static final int SPEI_MAINTENANCE = 69;//68
		
		/**
		 * The request beneficiary account number operation.
		 */
		public static final int CONSULT_BENEFICIARY_ACCOUNT_NUMBER = 70;//69
	//Termina Codigo de SPEI	
		/** Consulta Interbancarios */
		
		

		
		//One CLick
		/**
		 * Operation codes.
		 */
		/** consulta detalle ofertas ilc*/
		public static final int CONSULTA_DETALLE_OFERTA=71;//67;//66
		public static final int ACEPTACION_OFERTA=72;//68;//67
		public static final int EXITO_OFERTA=73;//69;//68
		public static final int RECHAZO_OFERTA=74;//70;//69
		/**consulta detalle ofertas EFI*/
		public static final int CONSULTA_DETALLE_OFERTA_EFI=75;//71;//70
		public static final int ACEPTACION_OFERTA_EFI=76;//71
		public static final int EXITO_OFERTA_EFI=77;//72
		public static final int SIMULADOR_EFI=78;//73
		/** consulta detalle consumo*/
		public static final int CONSULTA_DETALLE_OFERTA_CONSUMO=79;
		public static final int POLIZA_OFERTA_CONSUMO=80;
		public static final int TERMINOS_OFERTA_CONSUMO=81;
		public static final int EXITO_OFERTA_CONSUMO=82;
		public static final int DOMICILIACION_OFERTA_CONSUMO=83;
		public static final int CONTRATO_OFERTA_CONSUMO=84;
		public static final int RECHAZO_OFERTA_CONSUMO=85;
		public static final int SMS_OFERTA_CONSUMO=86;
		public static final int OP_CONSULTA_INTERBANCARIOS = 87;
		
		/**
		 * Request frequent service payment operation list for BBVA accounts.
		 */
		public static final int FAVORITE_PAYMENT_OPERATION_BBVA = 88;

		
		//Depositos Movil
		public static final int CONSULTA_DEPOSITOS_CHEQUES = 89;//87
		public static final int CONSULTA_DEPOSITOS_EFECTIVO = 90; // Ya no se usa 88
		public static final int CONSULTA_PAGO_SERVICIOS = 91;//89
		public static final int CONSULTA_TRANSFERENCIAS_CUENTA_BBVA = 92;//90
		public static final int CONSULTA_TRANSFERENCIAS_MIS_CUENTAS = 93;//91
		public static final int CONSULTA_TRANSFERENCIAS_A_OTROS_BANCOS = 94;//92
		public static final int CONSULTA_TRANSFERENCIAS_CLIENTES_BANCOMER = 95; // Ya no se usa 93
		public static final int CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS = 96;//94
		public static final int CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO = 97;//95
		public static final int CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO_DETALLE = 98;//96
		public static final int CONSULTA_DEPOSITOS_CHEQUES_DETALLE = 99;//97

		/** consulta importes tarjeta de crédito **/
		public static final int OP_CONSULTA_TDC = 100;
		/** consulta alta de retiro sin tarjeta**/
		public static final int OP_RETIRO_SIN_TAR = 101;
		public static final int CONSULTA_SIN_TARJETA = 102;
		

		/** consulta alta de retiro sin tarjeta**/
		public static final int OP_RETIRO_SIN_TAR_12_DIGITOS = 103;
		/** sincronizaExportaToken **/
		public static final int OP_SINC_EXP_TOKEN = 104;

		/** operacion actualizarEstatusEnvioEC*/
		public static final int ACTUALIZAR_ESTATUS_EC=106;
		/** operacion consultarEstadoCuenta*/
		public static final int CONSULTAR_ESTADO_CUENTA=107;
		/** operacion consultarEstatusEnvioEC*/
		public static final int CONSULTAR_ESTATUS_EC=108;
		/** operacion consultaTextoPaperles*/
		public static final int CONSULTAR_TEXTO_PAPERLESS=109;

		/** operacion inhibirEnvioEstadoCuenta*/
		public static final int INHIBIR_ENVIO_EC=110;

		/** operacion obtenerPeriodosEC*/
		public static final int OBTENER_PERIODO_EC=111;

		/**operacion consulta otros creditos*/
		public static final int OP_CONSULTA_OTROS_CREDITOS = 105;

		public static final int OP_CONSULTA_DETALLE_OTROS_CREDITOS = 120;

		public static final int OP_CONSULTA_SMS_OTROS_CREDITOS = 121;

		public static final int OP_CONSULTA_CORREO_OTROS_CREDITOS = 122;

		//para simular errores de TDC
		public Boolean errorComunicaciones = false;
		public Boolean errorContrato = false;

		//para simular errores en alta de retiro sin tarjeta
		public Boolean errorComunicacionesRetSinTarjeta=false;
		public Boolean errorMontosRetiroSinTarjeta=false;
		
		
		
		public Boolean errorIUM=true;
		
		// Simulaciones de fallo de login
		private Boolean simulateLogFail = false;
		private Integer simulateLogFailCont = 0;



	/**
	 * Operation codes.
	 */
	public static final String[] OPERATION_CODES = { "00", "01", "02", "103",
			"104", "105", "106", "107", "08", "09", "110", "111", "12", "13",
			"14", "115", "16", "17", "141", "19", "20", "21", "22", "123",
			"124", "125", "26", "cambioPerfil", "113", "130", "145", "109", "118",
			"112", "144", "120", "143", "146", "cambioNumCeluar",
			"cambioCuentaAsociada", "suspenderCancelarServicio",
			"actualizarCuentas", "consultaTarjetaContratacionE",
			"consultaLimites", "configuracionLimites",
			"consultaEstatusMantenimiento", "desbloqueoContrasena",
			"quitarSuspension", "102", "contratacionBMovilAlertas",
			"consultaTerminosCondiciones", "envioClaveActivacion",
			"configuracionCorreo", "envioCorreo", "valCredenciales",
			"finalizarContratacionAlertas", "consultaTarjetaST", "autenticacionToken", "204", "203",
			"contratacionE", "finalizarContratacionST",
			"cambioTelefonoAsociadoE", "solicitudST", "mantenimientoAlertas", 
			"consultaTerminosCondicionesSesion","solicitarAlertas",/* SPEI*/"ConsultaCuentasSPEI",
			"consultaTerminosCondicionesSPEI", "MantenimientoSpei", "ConsultaCuentaTerceros",
			/*OneClick*/"cDetalleOfertaBMovil","aceptacionILCBmovil","exitoILC","noAceptacionBMovil",
			"cDetalleOfertaBMovil","aceptaOfertaEFI","exitoEFI","SimulacionEFI","detalleConsumoBMovil",
			"polizaConsumoBMovil","consultaContratoConsumoBMovil","oneClickBmovilConsumo",
			"consultaDomiciliacionBovedaConsumoBMovil","consultaContratoBovedaConsumoBmovil",
			"noAceptacionBMovil","exitoConsumoBMovil","consultaTransferenciaSPEI","consultaFrecuentesTercerosCE",
			"consultarDepositosCheques","consultarDepositosEfectivo",
			"consultarTransfPagoServicios","consultarTransfBancomer","consultarTransfMisCuentas",
			"consultarTransfOtrosBancos","consultarClientesBancomer","consultarOtrosBancos","consultarDepositosEfectivo",
			"detalleDepositosEfectivo", "detalleDepositosCheques","importesTDC","retiroSinTarjeta",
			"consultaRetiroSinTarjeta", "claveRetiroSinTarjeta", "sincronizaExportaToken"};

	
	//one Click
			/**
			 * clase de enum para cambio de url;
			 */
			public enum isJsonValueCode{
				ONECLICK, NONE,CONSUMO,DEPOSITOS;
			}
			
			public static isJsonValueCode isjsonvalueCode= isJsonValueCode.NONE;
			//Termina One CLick


	/**
	 * Base URL for providers.
	 */
	public static String[] BASE_URL;

	/**
	 * Path table for provider/operation URLs.
	 */
	public static String[][] OPERATIONS_PATH;

	/**
	 * Operation URLs map.
	 */
	public static Hashtable<String, String> OPERATIONS_MAP = new Hashtable<String, String>();

	/**
	 * Operation providers map.
	 */
	public static Hashtable<String, Integer> OPERATIONS_PROVIDER = new Hashtable<String, Integer>();

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_PARAMETER = "OPERACION";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_VALUE = "BAN2O01";
	
	/**
	 * Operation code parameter for RECORTADO.
	 */
	public static final String OPERATION_CODE_VALUE_RECORTADO = "BREC001";

	/**
	 * Operation code parameter for BMOVIL MEJORADO.
	 */
	public static final String OPERATION_CODE_VALUE_BMOVIL_MEJORADO = "MJRS001";
	
	/**
	 * Operation code parameter for DEPOSITOS
	 */
	public static final String JSON_OPERATION_CODE_VALUE_DEPOSITOS = "O300001";

	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE = "BAN2O05";
	

	//one CLick
		/**
		 * Operation code parameter for new ops.
		 */
		public static final String JSON_OPERATION_CODE_VALUE_ONECLICK = "ONEC001";
		/**
		 * 
		 */
		
		//one CLick
		/**
		 * Operation code parameter for new ops.
		 */
		public static final String JSON_OPERATION_CODE_VALUE_CONSUMO = "ONEC002";
		/**
		 * 
		 */
	
	/**
	 * Operation code parameter for new ops.
	 */
//	public static final String JSON_OPERATION_SOLICITAR_ALERTAS_CODE_VALUE = "BREC001";
	public static final String JSON_OPERATION_RECORTADO_CODE_VALUE = "BREC001";

	/**
	 * Operation code for mejoras
	 */
	public static final String JSON_OPERATION_MEJORAS_CODE_VALUE = "MEJO001";
	/**
	 * Operation code for consultar otros creditos
	 */
	public static final String JSON_OPERATION_CONSULTAR_OTROS_CREDITOS = "CHIP001";
	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_VALUE = "es_ES";

	/**
	 * Determines if operation is fast or frequent payment.
	 */
	public static final String FASTORFRECUENTOPERATION = "FastOrFrequentOperation";

	/**
	 * Charge account text.
	 */
	public static final String CUENTA_CARGO = "cuenta cargo";

	static {
		if (DEVELOPMENT) {
			setupDevelopmentServer();
		} else {
			setupProductionServer();
		}

		setupOperations(PROVIDER);
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupProductionServer() {
		BASE_URL = new String[] { "https://www.bancomermovil.com",
				// "https://172.17.100.173",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Produccion"
						: "" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "", "/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP01
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP02
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP103
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP104
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP105
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP106
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP107
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP08
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP09
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP110
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP11
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP12
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP13
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP14
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP115
						"/mbhxp_mx_web/img", // OP16
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP17
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP141
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP19
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP20
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP21
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP22
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP123
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP124
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP125
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP26
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP140: cambioPerfil
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP113
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP30
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP146
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP109
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP118
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP112
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP144
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP120
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP143
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP146
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPcambioNumCelular
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPcambioCuentaAsociada
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPsuspenderCancelarServicio
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPactualizarCuentas
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPconsultaTarjetaParaContratacion
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPconsultaLimites
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPcambiarLimites
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPconsultaEstatusMantenimiento
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPdesbloqueo
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPquitarsuspension
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OP102
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPContratacionBmovilAlertas
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondiciones
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPEnvioClaveActivacion
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPConfigurarCorreo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPEnvioCorreo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPvalCredenciales
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb",// OPfinalizaContratacion
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // CONSULTA_TARJETA_ST
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP202
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP203
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP204
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // CONTRATACION_ENROLAMIENTO_ST
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // FINALIZAR_CONTRATACION_ST 				
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // CAMBIO_TELEFONO_ASOCIADO_ST 			
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // SOLICITUD_ST
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // MANTENIMIENTO_ALERTAS
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondicionesSesion
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Solicitud Alertas
						//SPEI revisar
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentasSPEI
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // consultaTerminosCondicionesSPEI
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // MantenimientoSpei
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentaTerceros
						//Termina SPEI
						//One Clcik
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // detalle oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // acepta oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // rechazo oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // detalle oferta efi promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // acepta oferta efi promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito oferta efi promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // simulador efi
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // detalle oferta consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // poliza consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // terminos consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // domiciliacion consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // contrato consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // rechazo consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // sms consumo promociones
						//Termina One click
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consulta Interbancario
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",//consultaTercerosCE
						//Depositos movil
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos cheques
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos efectivos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Pago Servicios
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia a cuentas BBVA
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencias a mis cuentas
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia a otros Bancos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencias a clientes Bancomer
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia de otros Bancos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP95: Consulta de depósitos BBVA Bancomer y Efectivo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP96: Consulta de depósitos BBVA Bancomer y Efectivo Detalle
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP97: Consulta de depositos cheques Detalle
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consulta Importes TDC

						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP alta retiro sin tarjeta
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consulta Tarjeta sin retiro
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consulta Clave 12 Digitos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb"	// OP Sinc Exp Token
				},
				new String[] { "", "/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP01
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP02
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP103
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP104
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP105
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP106
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP107
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP08
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP09
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP110
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP11
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP12
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP13
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP14
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP115
						"/mbhxp_mx_web/img", // OP16
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP17
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP141
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP19
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP20
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP21
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP22
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP123
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP124
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP125
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP26
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP140: cambioPerfil
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP113
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP30
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP146
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP109
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP118
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP112
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP144
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP120
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP143
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP146
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPcambioNumCelular
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPcambioCuentaAsociada
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPsuspenderCancelarServicio
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPactualizarCuentas
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPconsultaTarjetaParaContratacion
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPconsultaLimites
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPcambiarLimites
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPconsultaEstatusMantenimiento
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPdesbloqueo
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPquitarsuspension
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OP102
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPContratacionBmovilAlertas
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondiciones
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // OPEnvioClaveActivacion
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPConfigurarCorreo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPEnvioCorreo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPValCredenciales
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb",// OPfinalizaContratacion
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // CONSULTA_TARJETA_ST_SOFTTOKEN
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP202
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP203
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP204
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // CONTRATACION_ENROLAMIENTO_SOFTTOKEN
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // FINALIZAR_CONTRATACION_ST				
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // CAMBIO_TELEFONO_ASOCIADO_ST 			
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // SOLICITUD_ST
						"/bm3wxp_mx_web/servlet/ServletOperacionWeb", // MANTENIMIENTO_ALERTAS
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondicionesSesion
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Solicitud Alertas
						//SPEI revisar
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentasSPEI
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // consultaTerminosCondicionesSPEI
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // MantenimientoSpei
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentaTerceros
						//SPEI Termina
						//One click
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // oferta ilc promociones bmovil
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // rechazo oferta ilc promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // detalle oferta efi promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // acepta oferta efi promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito oferta efi promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // simulador efi
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // detalle oferta consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // poliza consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // terminos consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // exito consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // domiciliacion consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // contrato consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // rechazo consumo promociones
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // sms consumo promociones
						//Termina one click
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",// OP Consultar Interbancarios
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", //consultaTercerosCE
						//Depositos movil
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos cheques
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos efectivos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Pago Servicios
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia a cuentas BBVA
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencias a mis cuentas
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia a otros Bancos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencias a clientes Bancomer
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia de otros Bancos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP95: Consulta de depÃ³sitos BBVA Bancomer y Efectivo
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP96: Consulta de depÃ³sitos BBVA Bancomer y Efectivo Detalle
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP97: Consulta de depositos cheques Detalle
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consulta Importes TDC

						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP alta Retiro sin tarjeta
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",	//OP Consulta Tarjeta sin retiro
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consulta Clave 12 Digitos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb"  // OP Sinc Exp Token
				} };
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupDevelopmentServer() {
		BASE_URL = new String[] {
				"https://www.bancomermovil.net:11443",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Desarrollo"
						: "http://189.254.77.54:8080/servermobile/Servidor/Desarrollo" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "", "/eexd_mx_web/servlet/ServletOperacionWeb", // OP01
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP02
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP103
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP104
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP105
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP106
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP107
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP08
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP09
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP110
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP11
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP12
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP13
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP14
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP115
						"/eexd_mx_web/img", // OP16
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP17
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP141
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP19
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP20
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP21
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP22
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP123
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP124
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP125
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP26
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP140: cambioPerfil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP113
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP30
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP146
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP109
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP118
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP112
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP144
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP120
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP143
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP146
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPcambioNumCelular
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPcambioCuentaAsociada
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPsuspenderCancelarServicio
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPactualizarCuentas
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPconsultaTarjetaParaContratacion
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPconsultaLimites
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPcambiarLimites
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPconsultaEstatusMantenimiento
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPdesbloqueo
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPquitarSuspension
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OP102
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPContratacionBmovilAlertas
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondiciones
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPEnvioClaveActivacion
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPConfigurarCorreo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPEnvioCorreo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPValCredenciales
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPfinalizarContratacionAlertas
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // CONSULTA_TARJETA_ST_SOFTTOKEN
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP202
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP203
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP204
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // CONTRATACION_ENROLAMIENTO_SOFTTOKEN
						"/eexd_mx_web/servlet/ServletOperacionWeb", // FINALIZAR_CONTRATACION_ST				
						"/eexd_mx_web/servlet/ServletOperacionWeb", // CAMBIO_TELEFONO_ASOCIADO_ST 			
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // SOLICITUD_ST
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // MANTENIMIENTO_ALERTAS
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondicionesSesion
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Solicitud Alertas
						//SPEI revisar
						"/eexd_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentasSPEI
						"/eexd_mx_web/servlet/ServletOperacionWeb", // consultaTerminosCondicionesSPEI
						"/eexd_mx_web/servlet/ServletOperacionWeb", // MantenimientoSpei
						"/eexd_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentaTerceros
						//Termina SPEI
						//One  click
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // acepta oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // rechazo oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta efi promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // acepta oferta efi promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta efi promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // simulador efi
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // poliza oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // terminos oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // domiciliacion oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // contrato oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // rechazo oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // sms oferta consumo promocion bmovil
						//Termina one click
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Consultar Interbancarios
						"/eexd_mx_web/servlet/ServletOperacionWeb", //consultaTercerosCE
						//Deposito Movil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos con cheques
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos con efectivo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de Pago de Servicios
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia a cuentas BBVA
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias a mis cuentas
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias a otros Bancos
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias a clientes Bancomer
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias de otros Bancos
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP95: Consulta de depósitos BBVA Bancomer y Efectivo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP96: Consulta de depósitos BBVA Bancomer y Efectivo Detalle
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP97: Consulta de depositos con cheques Detalle
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Consultar Importes TDC

						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP alta retiro sin tarjeta
						"/eexd_mx_web/servlet/ServletOperacionWeb",	// OP Consultar Tarjeta Sin Retiro
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP Consulta Clave 12 Digitos
						"/eexd_mx_web/servlet/ServletOperacionWeb" // OP Sinc Exp Token
				},
				new String[] { "", "/eexd_mx_web/servlet/ServletOperacionWeb", // OP01
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP02
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP103
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP104
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP105
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP106
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP107
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP08
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP09
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP110
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP11
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP12
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP13
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP14
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP115
						"/eexd_mx_web/img", // OP16
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP17
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP141
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP19
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP20
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP21
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP22
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP123
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP124
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP125
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP26
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP140: cambioPerfil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP113
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP30
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP146
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP109
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP118
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP112
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP144
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP120
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP143
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP146
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPcambioNumCelular
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPcambioCuentaAsociada
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPsuspenderCancelarServicio
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPactualizarCuentas
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPconsultaTarjetaParaContratacion
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPconsultaLimites
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPcambiarLimites
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPconsultaEstatusMantenimiento
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPdesbloqueo
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPquitarSuspension
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OP102
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPContratacionBmovilAlertas
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondiciones
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPEnvioClaveActivacion
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPConfigurarCorreo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPEnvioCorreo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPValCredenciales
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // OPfinalizarContratacionAlertas
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // CONSULTA_TARJETA_ST_SOFTTOKEN
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP202
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP203
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP204
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // CONTRATACION_ENROLAMIENTO_SOFTTOKEN
						"/eexd_mx_web/servlet/ServletOperacionWeb", // FINALIZAR_CONTRATACION_ST				
						"/eexd_mx_web/servlet/ServletOperacionWeb", // CAMBIO_TELEFONO_ASOCIADO_ST 			
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // SOLICITUD_ST
						"/bm3wxd_mx_web/servlet/ServletOperacionWeb", // MANTENIMIENTO_ALERTAS
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OPConsultaTerminosCondiciones
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Solicitud Alertas
						//SPEI revisar
						"/eexd_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentasSPEI
						"/eexd_mx_web/servlet/ServletOperacionWeb", // consultaTerminosCondicionesSPEI
						"/eexd_mx_web/servlet/ServletOperacionWeb", // MantenimientoSpei
						"/eexd_mx_web/servlet/ServletOperacionWeb", // ConsultaCuentaTerceros
						//Termina SPEI
						//One Click
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // acepta oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // rechazo oferta ilc promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta efi promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // acepta oferta efi promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta efi promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // simulador efi
						"/eexd_mx_web/servlet/ServletOperacionWeb", // detalle oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // poliza oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // terminos oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // exito oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // domiciliacion oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // contrato oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // rechazo oferta consumo promocion bmovil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // sms oferta consumo promocion bmovil
						//Termnina one click
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Consultar Interbancarios
						"/eexd_mx_web/servlet/ServletOperacionWeb", //consultaTercerosCE
						//Deposito Movil
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos con cheques
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de depositos con efectivo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de Pago de Servicios
						"/eexd_mx_web/servlet/ServletOperacionWeb", // Consulta de Transferencia a cuentas BBVA
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias a mis cuentas
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias a otros Bancos
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias a clientes Bancomer
						"/eexd_mx_web/servlet/ServletOperacionWeb", //Consulta de Transferencias de otros Bancos
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP95: Consulta de depósitos BBVA Bancomer y Efectivo
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP96: Consulta de depósitos BBVA Bancomer y Efectivo Detalle
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP97: Consulta de depositos con cheques Detalle
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Consultar Importes TDC
						//Retiro sin tarjeta
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP alta retiro sin tarjeta
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Consultar Tarjeta sin retiro
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP Consulta Clave 12 Digitos
						"/eexd_mx_web/servlet/ServletOperacionWeb" // OP Sinc Exp Token
				} };
	}

	/**
	 * Setup operations for a provider.
	 * 
	 * @param provider
	 *            the provider
	 */
	private static void setupOperations(final int provider) {
		setupOperation(ACTIVATION_STEP1_OPERATION, provider); // OP01
		setupOperation(ACTIVATION_STEP2_OPERATION, provider); // OP02
		setupOperation(LOGIN_OPERATION, provider); // OP103
		setupOperation(MOVEMENTS_OPERATION, provider); // OP104
		setupOperation(SELF_TRANSFER_OPERATION, provider); // OP105
		setupOperation(BANCOMER_TRANSFER_OPERATION, provider); // OP106
		setupOperation(EXTERNAL_TRANSFER_OPERATION, provider); // OP107
		setupOperation(NIPPER_STORE_PURCHASE_OPERATION, provider); // OP08
		setupOperation(NIPPER_AIRTIME_PURCHASE_OPERATION, provider); // OP09
		setupOperation(CHANGE_PASSWORD_OPERATION, provider); // OP110
		setupOperation(CLOSE_SESSION_OPERATION, provider); // OP11
		setupOperation(CARD_OWNER_OPERATION, provider); // OP12
		setupOperation(CALCULATE_FEE_OPERATION, provider); // OP13
		setupOperation(CALCULATE_FEE_OPERATION2, provider); // OP14
		setupOperation(SERVICE_PAYMENT_OPERATION, provider); // OP15
		setupOperation(HELP_IMAGE_OPERATION, provider); // OP16
		setupOperation(17, 0); // OP17
		setupOperation(FAVORITE_PAYMENT_OPERATION, provider); // OP141
		setupOperation(FAST_PAYMENT_OPERATION, provider); // OP19
		setupOperation(SERVICE_NAME_OPERATION, provider); // OP20
		setupOperation(ALTA_OPSINTARJETA, provider); // OP123
		setupOperation(BAJA_OPSINTARJETA, provider); // OP124
		setupOperation(CONSULTA_OPSINTARJETA, provider); // OP125
		setupOperation(CONSULTA_ESTATUSSERVICIO, 0); // OP26
		setupOperation(CAMBIA_PERFIL, provider); // OP140: cambioPerfil
		setupOperation(CONSULTAR_COMISION_I, provider); // OP113
		setupOperation(CONSULTAR_CODIGO_PAGO_SERVICIOS, provider); // OP30
		setupOperation(PREREGISTRAR_PAGO_SERVICIOS, provider); // OP146
		setupOperation(COMPRA_TIEMPO_AIRE, provider); // OP109
		setupOperation(ALTA_FRECUENTE, provider); // OP118
		setupOperation(CONSULTA_BENEFICIARIO, provider); // OP112
		setupOperation(BAJA_FRECUENTE, provider); // OP144
		setupOperation(CONSULTA_CIE, provider); // OP120
		setupOperation(ACTUALIZAR_FRECUENTE, provider); // OP143
		setupOperation(ACTUALIZAR_PREREGISTRO_FRECUENTE, provider);// OP146
		setupOperation(CAMBIO_TELEFONO, provider);// OPcambioNumCelular
		setupOperation(CAMBIO_CUENTA, provider);// OPcambioCuentaAsociada
		setupOperation(SUSPENDER_CANCELAR, provider);// OPsuspenderCancelar
		setupOperation(ACTUALIZAR_CUENTAS, provider);// OPactualizarCuentas
		setupOperation(CONSULTA_TARJETA_OPERATION, provider);// OPconsultaTarjetaParaContratacion
		setupOperation(CONSULTAR_LIMITES, provider); // OPconsultaLimites
		setupOperation(CAMBIAR_LIMITES, provider);// OPcambiarLimites
		setupOperation(CONSULTA_MANTENIMIENTO, provider);// OPconsultaMantenimiento
		setupOperation(DESBLOQUEO, provider);
		setupOperation(QUITAR_SUSPENSION, provider);
		setupOperation(OP_ACTIVACION, provider);
		setupOperation(OP_CONTRATACION_BMOVIL_ALERTAS, provider);
		setupOperation(OP_CONSULTAR_TERMINOS, provider);
		setupOperation(OP_ENVIO_CLAVE_ACTIVACION, provider);
		setupOperation(OP_CONFIGURAR_CORREO, provider);
		setupOperation(OP_ENVIO_CORREO, provider);
		setupOperation(OP_VALIDAR_CREDENCIALES, provider);
		setupOperation(OP_FINALIZAR_CONTRATACION_ALERTAS, provider);
		setupOperation(CONSULTA_TARJETA_ST, provider);
		setupOperation(AUTENTICACION_ST, provider);// OP202
		setupOperation(SINCRONIZACION_SOFTTOKEN, provider);// OP203
		setupOperation(EXPORTACION_SOFTTOKEN, provider);// OP204
		setupOperation(CONTRATACION_ENROLAMIENTO_ST, provider);
		setupOperation(FINALIZAR_CONTRATACION_ST, provider);
		setupOperation(CAMBIO_TELEFONO_ASOCIADO_ST, provider);
		setupOperation(SOLICITUD_ST, provider);
		setupOperation(MANTENIMIENTO_ALERTAS, provider);
		setupOperation(OP_CONSULTAR_TERMINOS_SESION, provider);
		setupOperation(OP_SOLICITAR_ALERTAS, provider);
		//SPEI 
		setupOperation(SPEI_ACCOUNTS_REQUEST, provider);// SpeiAccountsRequest
		setupOperation(SPEI_TERMS_REQUEST, provider);// SpeiTermsRequest
		setupOperation(SPEI_MAINTENANCE, provider);// MantenimientoSpei
		setupOperation(CONSULT_BENEFICIARY_ACCOUNT_NUMBER, provider);// MantenimientoSpei
		//Termina SPEI
		//One click
		setupOperation(CONSULTA_DETALLE_OFERTA, provider);
		setupOperation(ACEPTACION_OFERTA, provider);
		setupOperation(EXITO_OFERTA, provider);
		setupOperation(RECHAZO_OFERTA, provider);
		setupOperation(ACEPTACION_OFERTA_EFI, provider);
		setupOperation(CONSULTA_DETALLE_OFERTA_EFI, provider);
		setupOperation(SIMULADOR_EFI, provider);
		setupOperation(EXITO_OFERTA_EFI, provider);
		setupOperation(CONSULTA_DETALLE_OFERTA_CONSUMO, provider);
		setupOperation(POLIZA_OFERTA_CONSUMO, provider);
		setupOperation(TERMINOS_OFERTA_CONSUMO, provider);
		setupOperation(EXITO_OFERTA_CONSUMO, provider);
		setupOperation(DOMICILIACION_OFERTA_CONSUMO, provider);
		setupOperation(CONTRATO_OFERTA_CONSUMO, provider);
		setupOperation(RECHAZO_OFERTA_CONSUMO, provider);
		setupOperation(SMS_OFERTA_CONSUMO, provider);
		//Termina One click
		setupOperation(OP_CONSULTA_INTERBANCARIOS, provider);
		setupOperation(FAVORITE_PAYMENT_OPERATION_BBVA, provider);
		//Deposito movil
		setupOperation(CONSULTA_DEPOSITOS_CHEQUES, provider);
		setupOperation(CONSULTA_DEPOSITOS_EFECTIVO, provider);
		setupOperation(CONSULTA_PAGO_SERVICIOS, provider);
		setupOperation(CONSULTA_TRANSFERENCIAS_CUENTA_BBVA, provider);
		setupOperation(CONSULTA_TRANSFERENCIAS_MIS_CUENTAS, provider);
		setupOperation(CONSULTA_TRANSFERENCIAS_A_OTROS_BANCOS, provider);
		setupOperation(CONSULTA_TRANSFERENCIAS_CLIENTES_BANCOMER, provider);
		setupOperation(CONSULTA_TRANSFERENCIAS_DE_OTROS_BANCOS, provider);
		setupOperation(CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO, provider);
		setupOperation(CONSULTA_DEPOSITOS_BBVA_BANCOMER_Y_EFECTIVO_DETALLE, provider);
		setupOperation(CONSULTA_DEPOSITOS_CHEQUES_DETALLE, provider);
		setupOperation(OP_CONSULTA_TDC,provider);
		//Retiro sin tarjeta
		setupOperation(OP_RETIRO_SIN_TAR,provider);
		setupOperation(CONSULTA_SIN_TARJETA,provider);
		setupOperation(OP_RETIRO_SIN_TAR_12_DIGITOS, provider);
		setupOperation(OP_SINC_EXP_TOKEN, provider);
	}

	/**
	 * Setup operation for a provider.
	 * @param operation the operation
	 * @param provider the provider
	 */
	private static void setupOperation(final int operation, final int provider) {
		final String code = OPERATION_CODES[operation];
		OPERATIONS_MAP.put(
				code,
				new StringBuffer(BASE_URL[provider]).append(
						OPERATIONS_PATH[provider][operation]).toString());
		OPERATIONS_PROVIDER.put(code, new Integer(provider));
	}

	/**
	 * Get the operation URL.
	 * @param operation the operation id
	 * @return the operation URL
	 */
	public static String getOperationUrl(final String operation) {
		return (String) OPERATIONS_MAP.get(operation);
	}

	// ///////////////////////////////////////////////////////////////////////////
	// Parameter definition for application interface //
	// ///////////////////////////////////////////////////////////////////////////

	/**
	 * The username.
	 */
	public static final String USERNAME_PARAM = "username";

	/**
	 * The password.
	 */
	public static final String PASSWORD_PARAM = "password";
	
	/**
	 * The IUM. SPEI
	 */
	public static final String IUM_PARAM = "ium";

	/**
	 * The version of catalog 1.
	 */
	public static final String VERSION_C1_PARAM = "version_c1";

	/**
	 * The version of catalog 4.
	 */
	public static final String VERSION_C4_PARAM = "version_c4";

	/**
	 * The version of catalog 5.
	 */
	public static final String VERSION_C5_PARAM = "version_c5";

	/**
	 * The version of catalog 8.
	 */
	public static final String VERSION_C8_PARAM = "version_c8";

	/**
	 * The version of catalog Tiempo aire.
	 */
	public static final String VERSION_TA_PARAM = "version_ta";

	/**
	 * The version of catalog Dinero movil.
	 */
	public static final String VERSION_DM_PARAM = "version_dm";
	
	
	/**
	 * The version of catalog Servicios.
	 */
	public static final String VERSION_SV_PARAM = "version_sv";

	public static final String VERSION_AU_PARAM = "version_au";
	//SPEI revisar
		/* The params for the SPEI catalog version.
		 */
		public static final String VERSION_MS_PARAM = "version_ms";
		//Termina SPEI

	/**
	 * Vía 1 o 2.
	 */
	public static final String VIA_PARAM = "via";
	/**
	 * The bancomer token.
	 */
	public static final String BANCOMER_TOKEN_PARAM = "sello_bancomer";

	/**
	 * The account.
	 */
	public static final String ACCOUNT_PARAM = "account";

	/**
	 * Origin account.
	 */
	public static final String ORIGIN_PARAM = "origin";

	/**
	 * Destination account.
	 */
	public static final String DESTINATION_PARAM = "destination";

	/**
	 * Money to transfer.
	 */
	public static final String AMOUNT_PARAM = "amount";

	/**
	 * Destination card number.
	 */
	public static final String CARDNUMBER_PARAM = "cardowner";

	/**
	 * Store.
	 */
	public static final String STORE_PARAM = "store";

	/**
	 * Cellular operator.
	 */
	public static final String OPERATOR_PARAM = "operator";
	
	//SPEI revisar

		/**
		 * Telephone number.
		 */
		public static final String PHONENUMBER_PARAM = "phonenumber";

		/**
		 * Old telephone number.
		 */
		public static final String OLD_PHONENUMBER_PARAM = "oldphonenumber";
	    //Termina SPEI

	/**
	 * Old password.
	 */
	public static final String OLD_PASSWORD_PARAM = "oldpassword";

	/**
	 * New password.
	 */
	public static final String NEW_PASSWORD_PARAM = "newpassword";

	/**
	 * Activation code.
	 */
	public static final String ACTIVATION_CODE_PARAM = "activation";

	/**
	 * User identifier.
	 */
	public static final String USER_ID_PARAM = "userid";

	/**
	 * Account type.
	 */
	public static final String ACCOUNT_TYPE_PARAM = "account_type";

	/**
	 * User identifier.
	 */
	public static final String BANK_PARAM = "userid2";

	/**
	 * Transfer receiver name identifier.
	 */
	public static final String RECEIVER_PARAM = "receiver";

	/**
	 * Transfer reference identifier.
	 */
	public static final String REFERENCE_PARAM = "reference";

	/**
	 * Transfer reason identifier.
	 */
	public static final String REASON_PARAM = "reason";

	/**
	 * Card type identifier.
	 */
	public static final String CARD_TYPE_PARAM = "card_type";

	/**
	 * Product type identifier.
	 */
	public static final String PRODUCT_TYPE_PARAM = "product_type";

	/**
	 * screen size identifier.
	 */
	public static final String SCREEN_SIZE_PARAM = "screen_size";

	/**
	 * Type of help image identifier.
	 */
	public static final String HELP_IMAGE_TYPE_PARAM = "help_image_type";

	/**
	 * Service provider identifier.
	 */
	public static final String SERVICE_PROVIDER_PARAM = "service_provider";

	/**
	 * CIE Agreement identifier.
	 */
	public static final String CIE_AGREEMENT_PARAM = "cie_agreement";

	/**
	 * Operation type.
	 */
	public static final String TYPE_OPER = "type_oper";

	/**
	 * Nick name.
	 */
	public static final String NICK_NAME_PARAM = "nickname";

	/**
	 * AP.
	 */
	public static final String IDNUMBER = "idNumber";

	/**
	 * CA.
	 */
	public static final String CUENTA_ABONO = "cuentaAbono";

	/**
	 * Payment operation code.
	 */
	public static final String PAYMENT_OPER_PARAM = "payment_oper";

	/**
	 * Payment ID.
	 */
	public static final String PAYMENT_ID_PARAM = "payment_id";

	/**
	 * Marca y modelo que se envía en Login, se concatenan las cadenas.
	 */
	public static final String MARCA_MODELO = "Marca Modelo";

	/**
	 * Plataforma en la que corre el midlet, en este caso es Android.
	 */
	public static final String PLATAFORMA = "Plataforma";

	/**
	 * Estatus de la operación sin tarjeta para efectivo móvil, se utiliza en la
	 * consulta de Efectivo móvil. Los estatus pueden ser VG=vigente,
	 * CN=Cancelada, CD=Expirada
	 */
	public static final String ESTATUS_OPSINTARJETA_PARAM = "estatus";

	/**
	 * Código de la operación de efectivo móvil.
	 */
	public static final String FOLIO_OPERACION_PARAM = "FolioOperacion";

	/**
	 * Código de la operación de CLAVE 12 DIGITOS RETIRO SIN TARJETA.
	 */
	public static final String FOLIO_OPERACION_RETIRO_SIN_TAR = "folioOperacion";

	/**
	 * Código del canal de efectivo m�vil.
	 */
	public static final String CODIGO_CANAL_PARAM = "CodigoCanal";

	/**
	 * Fecha de alta de la operación, efectivo móvil.
	 */
	public static final String FECHA_OPERACION = "FechaAlta";

	public static final String NUMERO_CLIENTE_PARAM = "NumeroCliente";

	public static final String BANK_CODE_PARAM = "CodigoBanco";

	public static final String BENEFICIARIO_PARAM = "Beneficiario";

	public static final String MOTIVO_PAGO_PARAM = "MotivoPago";
	
	//SPEI Revisar
		public static final String OLD_COMPANY_NAME_PARAM = "companiaVieja";
		
		public static final String NEW_COMPANY_NAME_PARAM = "companiaNueva";
		//Termina SPEI

	public static final String REFERENCIA_NUMERICA_PARAM = "ReferenciaNumeria";
	public static final String NIP_PARAM = "Nip";
	public static final String CVV2_PARAM = "cvv2";
	public static final String VA_PARAM = "InstruccionValidacion";
	public static final String PAYMENT_CODE_PARAM = "QR";
	public static final String MARCA = "marca";
	public static final String MODELO = "modelo";
	public static final String BRAND = "cellphoneBrand";
	public static final String MODEL = "cellphoneModel";

	public static final String DESCRIPCION_PARAM = "descripcion";
	public static final String CONCEPTO_PARAM = "concepto";
	public static final String TIPO_CONSULTA_PARAM = "tipoconsulta";
	public static final String OPERADORA_PARAM = "operadora";
	public static final String ID_TOKEN = "idToken";
	public static final String TIPO_COMISION_PARAM = "tipocomision";

	public static final String C1_PARAM = "C1";
	public static final String CN_PARAM = "CN";
	public static final String DE_PARAM = "DE";
	public static final String C2_PARAM = "C2";
	public static final String C3_PARAM = "C3";

	public static final String AUTH_CAT_PARAM = "authenticationVersion";
	public static final String COMPANIES_PARAM = "companiesVersion";

	public static final String ALERTSTATUS_PARAM = "estadoAlertas";

	/**
	 * Tags JSON
	 */
	public static final String J_NIP = "codigoNIP";
	public static final String J_CVV2 = "codigoCVV2";
	public static final String J_AUT = "cadenaAutenticacion";
	public static final String J_CUENTA_N = "cuentaNueva";
	public static final String J_CUENTA_A = "cuentaAnterior";
	public static final String J_CUENTA = "cuenta";
	
	//SPEI
	public static final String J_NUM_TELEFONO = "numeroTelefono";	
	public static final String J_NUM_CLIENTE = "numeroCliente";
	public static final String J_CVE_ACCESO = "cveAcceso";
	public static final String J_OTP = "codigoOTP";

	public static final String VOUCHER_TYPE_PARAM = "voucherType";
	public static final String SUBJECT_PARAM = "subject";
	public static final String CLIENTNAME_PARAM = "clientName";
	public static final String CELLPHONENUMBER_PARAM = "cellphoneNumber";
	public static final String OPERATION_DATE_PARAM = "operationDate";
	public static final String OPERATION_HOUR_PARAM = "operationHour";
	public static final String SECURITY_CODE_PARAM = "securityCode";
	public static final String REGISTER_DATE_PARAM = "registerDate";
	public static final String VALIDITY_DATE_PARAM = "validityDate";
	public static final String AGREEMENT_PARAM = "agreement";
	public static final String SERVICE_PARAM = "service";
	public static final String MSG_PARAM = "mensajeA";
	
	//SPEI revisar
		/**
		 * Last digists of the card for authentication prupose.
		 */
		public static final String LAST_CARD_DIGITS_PARAM = "tarjeta5Dig";
		/**
		 * The param for the maintenance type.
		 */
		public static final String SPEI_MAINTENANCE_TYPE_PARAM = "tipoMantenimientoSpei";
		
		//termina SPEI

	/**
	 * simulation option for the login.
	 */
	private static boolean simulateDeactivation = false;
	
	public static String JSON_OPERATION_CONSULTA_INTERBANCARIOS="BXCO001";

	/*
	 * public void setContext(Context context) { mContext = context;
	 * clienteHttp.setContext(mContext); }
	 * 
	 * public Context getContext() { return mContext; }
	 */

	/*
	 * Own accounts transaction operation.
	 */
	// public static final int OWN_ACCOUNTS_TRANSFER_OPERATION = 28;

	/**
	 * Card NIP parm.
	 */
	public static final String CARD_NIP_PARAM = "nip";

	/**
	 * Card CVV2 param.
	 */
	public static final String CARD_CVV2_PARAM = "cvv2";

	/**
	 * Transaction charge account param.
	 */
	public static final String CHARGE_ACCOUNT_PARAM = "charge_account";

	/**
	 * Transaction payment account pram.
	 */
	public static final String PAYMENT_ACCOUNT_PARAM = "payment_account";

	/**
	 * Validation instructions param.
	 */
	public static final String VALIDATION_INSTRUCTIONS_PARAM = "validation_instructions";

	/**
	 * Identificador del token param.
	 */
	public static final String IDENTIFICADOR_TOKEN_PARAM = "identificador_token";

	/**
	 * Funcion para pago de servicios.
	 */
	public static final String PAYMENT_SERVICES_PREREGISTER_FUNCTION = "I380";

	/**
	 * Funcion para pago de servicios.
	 */
	public static final String PAYMENT_SERVICES_PREREGISTER_REASON_TYPE = "CV";

	/**
	 * Tipo token para pago de servicios.
	 */
	public static final String PAYMENT_SERVICES_TOKEN_TYPE = "T";
	public static final String ID_CANAL = "canal";
	public static final String USUARIO = "usuario";

	//Termina SPEI
	
	// #region Softtoken
	public static final String OTP_TOKEN = "otp_token";
	public static final String OTP1 = "otp_gene1";
	public static final String OTP2 = "otp_gene2";
	public static final String NUMERO_CLIENTE = "Numerocliente";
	public static final String COMPANIA_TELEFONICA = "Compañiatelefonica";
	public static final String TIPO_SOLICITUD_ST = "tiposolicitudst";
	public static final String INSTRUMENTO_SEGURIDAD_ST = "instrumentoseguridadst";
	public static final String NIP_CAJERO = "nipcajeros";
	public static final String NUMERO_SERIE_ST = "numeroserie";
	public static final String EMAIL_ST = "mailst";
	public static final String NOMBRE_TOKEN = "nombreToken";
	public static final String NOMBRE_CLIENTE = "Nombrecliente";
	//Código Karen
	public static final String PERFIL_PARAM = "PerfilCliente";
	public static final String OTP_PARAM = "OTP";

	public static final String VM = "VM";

	//public static final String J_AUT = "cadenaAutenticacion";

	public static final int OP_MIGRA_BASICO = 130;
	public static final String TELEFONICAS_PARAM = "verCatTelefonicas";
	public static final String CAT_PARAM = "verCatAutenticacion";
	public static final int OP_GLOBAL = 141;

	/* getState para contratación*/
	public static final int GET_STATE = 159;

}