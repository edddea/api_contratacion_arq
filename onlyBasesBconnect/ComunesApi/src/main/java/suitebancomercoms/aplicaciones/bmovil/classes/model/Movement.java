/*
 * Copyright (c) 2009 BBVA. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomercoms.aplicaciones.bmovil.classes.model;

import java.io.Serializable;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;

/**
 * Movement wraps the data which define an the movement of an account
 * 
 * @author Stefanini IT Solutions
 */
public class Movement implements Serializable {

    /**
     * Charge movement (expenditure)
     */
    public static final String CHARGE_CONCEPT = "CGO";

    /**
     * Income movement
     */
    public static final String DEPOSIT_CONCEPT = "ABO";
    /**
     * The date
     */
    private String date = null;

    /**
     * The concept (income, expense)
     */
    private String concept = null;

    /**
     * The amount
     */    
    private String amount = null;

    /**
     * The description
     */
    private String description = null;

    /**
     * The authorization date
     */
    private String authorizationDate = null;

    /**
     * The application date
     */
    private String applicationDate = null;

    /**
     * Default constructor
     */
    public Movement() {
        // Default constructor
    }

    /**
     * Constructor with parameters
     * @param date the date
     * @param concept the concept
     * @param amount the amount
     * @param description the description
     * @param authorizationDate the authorization date
     * @param applicationDate the application date
     */
    public Movement(final String date, final String concept, final String amount, final String description,
            final String authorizationDate, final String applicationDate) {
        this.date = date;
        this.concept = concept;
        this.amount = amount;
        this.applicationDate = applicationDate;
        this.authorizationDate = authorizationDate;
        this.description = description;
    }

    /**
     * Get the amount
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Set the amount
     * @param amount the amount
     */
    public void setAmount(final String amount) {
        this.amount = amount;
    }

    /**
     * Get the concept
     * @return the concept
     */
    public String getConcept() {
        return concept;
    }

    /**
     * Set the concept
     * @param concept the concept
     */
    public void setConcept(final String concept) {
        this.concept = concept;
    }

    /**
     * get the movement date
     * @return the movement date
     */
    public String getDate() {
        return date;
    }

    /**
     * Set the movement date
     * @param date the movement date
     */
    public void setDate(final String date) {
        this.date = date;
    }

    /**
     * Get the application date
     * @return the application date
     */
    public String getApplicationDate() {
        return applicationDate;
    }

    /**
     * Set the application date
     * @param applicationDate
     */
    public void setApplicationDate(final String applicationDate) {
        this.applicationDate = applicationDate;
    }

    /**
     * Get the authorization date
     * @return the authorization date
     */
    public String getAuthorizationDate() {
        return authorizationDate;
    }

    /**
     * Set the authorization date
     * @param authorizationDate the authorization date
     */
    public void setAuthorizationDate(final String authorizationDate) {
        this.authorizationDate = authorizationDate;
    }

    /**
     * Get the description
     * @return  the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set  the description
     * @param description  the description
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Gets the description substring by the number of chars specified in Constants field MOVEMENT_SHORT_DESCRIPTION_LENGTh
     * @return The short description within n chars specified by Constants.MOVEMENT_SHORT_DESCRIPTION_LENGTH 
     */
    public String getShortDescription() {
    	String desc = description;
    	if (description.length() > Constants.MOVEMENT_SHORT_DESCRIPTION) {
    		desc = description.substring(0, Constants.MOVEMENT_SHORT_DESCRIPTION); 
    	}
    	return desc;
    }
    
}
