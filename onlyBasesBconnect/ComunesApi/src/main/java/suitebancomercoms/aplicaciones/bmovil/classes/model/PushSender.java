package suitebancomercoms.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class PushSender implements ParsingHandler {
	String estado;
	String codigoMensaje;
	String descripcionMensaje;

	public String getEstado() {
		return estado;
	}

	public void setEstado(final String estado) {
		this.estado = estado;
	}

	public String getCodigoMensaje() {
		return codigoMensaje;
	}

	public void setCodigoMensaje(final String codigoMensaje) {
		this.codigoMensaje = codigoMensaje;
	}

	public String getDescripcionMensaje() {
		return descripcionMensaje;
	}

	public void setDescripcionMensaje(final String descripcionMensaje) {
		this.descripcionMensaje = descripcionMensaje;
	}

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		estado=parser.parseNextValue("estado",false);
		codigoMensaje=parser.parseNextValue("codigoMensaje",false);
		descripcionMensaje=parser.parseNextValue("descripcionMensaje",false);

		
	}

}
