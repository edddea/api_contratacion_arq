package suitebancomercoms.aplicaciones.bmovil.classes.common;

import android.content.Context;
import android.util.Log;

import com.bancomer.base.SuiteApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public final class CambioPerfilFileManager {
	// #region Constantes.
		/**
		 * Nombre del archivo de propiedades.
		 */
		private static final String CAMBIO_PERFIL_FILE_NAME = "CambioPerfil.prop";
			
		/**
		 * Id de cambio de Perfil
		 */
		public static final String PROP_CAMBIO_PERFIL_ID = "9";
				
		
		public static final String PROP_CAMPANA_PAPERLESS = "noAceptacionPaperless";

		// #endregion
		
		// #region Variables.
		/**
		 * Propiedades de la aplicacion.
		 */
		private Properties properties;
		// #endregion
		
		// #region Singleton.
		/**
		 * La instancia de la clase.
		 */
		private static CambioPerfilFileManager manager = null;
		
		/**
		 * @return La instancia de la clase.
		 */
		public static CambioPerfilFileManager getCurrent() {
			if(null == manager)
				manager = new CambioPerfilFileManager();
			return manager;
		}
		
		/**
		 * Inicializa el administrador de propiedades. 
		 */
		private CambioPerfilFileManager() {
			properties = null;
			final File file = new File(SuiteApp.appContext.getFilesDir(), CAMBIO_PERFIL_FILE_NAME);
			
			if(!file.exists())
				initCambioPerfilFile(file);
			else
				loadCambioPerfilFile();
		}
		// #endregion
		
		// #region Carga del archivo de propiedades.
		/**
		 * Inicializa el archivo de propiedades con los valores iniciales para cada propiedad.
		 * @param file
		 */
		private void initCambioPerfilFile(final File file) {
			try {
				file.createNewFile();
			} catch(IOException ioEx) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al crear el archivo CambioPerfil.", ioEx);
				return;
			}
					
			loadCambioPerfilFile();
			
			if(null != properties) {
				setPropertynValue(PROP_CAMBIO_PERFIL_ID, "1");
				setPropertynValue(PROP_CAMPANA_PAPERLESS, Constants.CAMPAÑA_PAPERLESS_VISIBLE); //paperless
			}
		}
		
		public void borrarDatos(){
			setPropertynValue(PROP_CAMBIO_PERFIL_ID, "1");
		}
		
		/**
		 * Carga el archivo de propiedades.
		 */
		private void loadCambioPerfilFile() {
			InputStream input;
			try {
				input = SuiteApp.appContext.openFileInput(CAMBIO_PERFIL_FILE_NAME);
			} catch(FileNotFoundException fnfEx) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargar el archivo CambioPerfil para lectura.", fnfEx);
				return;
			}

			properties = new Properties();
			try {
				properties.load(input);
			} catch(IOException ioEx) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargar CambioPerfil.", ioEx);
				properties = null;
				return;
			}			
		}
		// #endregion
		
		// #region Generic getters and setters for properties.
		/**
		 * Establece el valor especificado a la propiedad indicada.
		 * @param propertyName El nombre de la propiedad.
		 * @param value El valor a establecer.
		 */
		private void setPropertynValue(final String propertyName, String value) {
			if(null == properties || Tools.isEmptyOrNull(propertyName))
				return;
			if(null == value)
				value = "";
			
			properties.setProperty(propertyName, value);
			storeFileForProperty(propertyName, value);
		}
		
		/**
		 * Obtiene el valor de la propiedad especificada.
		 * @param propertyName El nombre de la propiedad.
		 * @return El valor de la propiedad.
		 */
		private String getPropertynValue(final String propertyName) {
			if(null == properties) {
				return null;
			}
			return properties.getProperty(propertyName);
		}
		// #endregion
		
		// #region Setters y Getters del estatus de activación para las aplicaciones.	
		public String getCambioPerfil(){
			return getPropertynValue(PROP_CAMBIO_PERFIL_ID);
		}	
			
		public void setCambioPerfil(final String value){
			setPropertynValue(PROP_CAMBIO_PERFIL_ID, value);
		}

		public void setNoAceptacionCampanaPaperless(final String value){
			setPropertynValue(PROP_CAMPANA_PAPERLESS, value);
		}

		public String getNoAceptacionCampanaPaperless(){
			return getPropertynValue(PROP_CAMPANA_PAPERLESS);
		}


		// #endregion
		
		/**
		 * Guarda el archivo de propiedades para preservar el cambio en alguna de las propiedades. 
		 * El nombre y valor de la propiedad modificada con usados para registrar el proceso mediante el log.  
		 * @param propertyName El nombre de la propiedad a guardar.
		 * @param propertyValue El valor de la propiedad a guardar.
		 * @return True si el archivo fue guardado exitosamente, False de otro modo.
		 */
		private boolean storeFileForProperty(String propertyName, final String propertyValue)	{
			if(Tools.isEmptyOrNull(propertyName))
				propertyName = "No name indicated.";
			if(Tools.isEmptyOrNull(propertyValue))
				propertyName = "No value indicated.";
			
			OutputStream output;
			try {	
				output = SuiteApp.appContext.openFileOutput(CAMBIO_PERFIL_FILE_NAME, Context.MODE_PRIVATE);
			} catch(FileNotFoundException fnfEx) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al abrir el archivo para guardar la propiedad " + propertyName, fnfEx);
				return false;
			}
			
			try {
				properties.store(output, null);
			} catch (IOException ioEx) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al guardar en el archivo CambioPerfil los valores: " + propertyName + " - " + propertyValue, ioEx);
				return false;
			}
			
			if(ServerCommons.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Archivo actualizado con los siguientes cambios: " + propertyName + " - " + propertyValue);
			return true;
		}

		public static String getPropCampanaPaperless() {
			return PROP_CAMPANA_PAPERLESS;
		}

	/*
	* Metodo que sete el manager a nulo
	* con la finalidad de que la proxima vez que
	* se llame el archivo este se vea obligado a leer el archivo nuevamente
	* */
	public static void reloadFile(){
		manager=null;
	}
}
