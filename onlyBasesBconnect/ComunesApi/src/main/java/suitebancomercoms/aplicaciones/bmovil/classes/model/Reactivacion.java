package suitebancomercoms.aplicaciones.bmovil.classes.model;


import bancomer.api.common.commons.Constants.Perfil;

public class Reactivacion {
	private Perfil perfil;
	private String perfilAST;
	private String estatus;
	private String numCelular;
	private String companiaCelular;
	private String numCliente;
	
	/**
	 * @return the perfil
	 */
	public Perfil getPerfil() {
		return perfil;
	}
	/**
	 * @param perfil the perfil to set
	 */
	public void setPerfil(final Perfil perfil) {
		this.perfil = perfil;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(final String estatus) {
		this.estatus = estatus;
	}

	/**
	 * @return the numCelular
	 */
	public String getNumCelular() {
		return numCelular;
	}
	/**
	 * @param numCelular the numCelular to set
	 */
	public void setNumCelular(final String numCelular) {
		this.numCelular = numCelular;
	}
	/**
	 * @return the companiaCelular
	 */
	public String getCompaniaCelular() {
		return companiaCelular;
	}
	/**
	 * @param companiaCelular the companiaCelular to set
	 */
	public void setCompaniaCelular(final String companiaCelular) {
		this.companiaCelular = companiaCelular;
	}
	/**
	 * @return the numCliente
	 */
	public String getNumCliente() {
		return numCliente;
	}
	/**
	 * @param numCliente the numCliente to set
	 */
	public void setNumCliente(final String numCliente) {
		this.numCliente = numCliente;
	}
	
	public void setPerfilAST(final String perfilAST) {
		this.perfilAST = perfilAST;
	}
	
	public String getPerfilAST() {
		return perfilAST;
	}

}
