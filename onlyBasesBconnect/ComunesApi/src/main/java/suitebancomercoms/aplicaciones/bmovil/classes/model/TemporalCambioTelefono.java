package suitebancomercoms.aplicaciones.bmovil.classes.model;

/**
 * Modelo para el contenido de la tabla TemporalST.
 * 
 * @author CGI
 */
public class TemporalCambioTelefono {

	/** El numero de la tarjeta. */
	private String tarjeta;

	/**
	 * Constructor por defecto.
	 */
	public TemporalCambioTelefono() {
		super();
	}

	/**
	 * Constructor con parametros.

	 * @param tarjeta
	 *            el numero de tarjeta
	
	 */
	public TemporalCambioTelefono(final String tarjeta) {
		super();

		this.tarjeta = tarjeta;

	}



	/**
	 * Obtiene el numero de tarjeta.
	 * 
	 * @return el numero de tarjeta
	 */
	public String getTarjeta() {
		return tarjeta;
	}

	/**
	 * Establece el numero de tarjeta.
	 * 
	 * @param tarjeta
	 *            el numero de tarjeta a establecer
	 */
	public void setTarjeta(final String tarjeta) {
		this.tarjeta = tarjeta;
	}
}