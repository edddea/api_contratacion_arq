package suitebancomercoms.aplicaciones.bmovil.classes.model;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import android.content.Context;

import com.bancomer.base.R;
import com.bancomer.base.SuiteApp;

/**
 * Modelo de datos para una operación rápida.
 */
public class Rapido {
	// #region Variables.
	/**
	 * Nombre corto del r�pido.
	 */
	private String nombreCorto;
	
	/**
	 * Cuenta de origen para la operación.
	 */
	private String cuentaOrigen;
	
	/**
	 * Cuenta de destino para la operación.
	 */
	private String cuentaDestino;
	
	/**
	 * Teléfono de destino para dinero m�vil o compra de tiempo aire.
	 */
	private String telefonoDestino;
	
	/**
	 * Importe de la operación.
	 */
	private float importe;
	
	/**
	 * Nombre del beneficiario.
	 */
	private String beneficiario;
	
	/**
	 * Compa�ia de telefono celular para rápidos de dinero m�vil o compra de tiempo aire.
	 */
	private String companiaCelular;
	
	/**
	 * Tipo de operación.
	 */
	private String tipoRapido;
	
	/**
	 * Id de operación para darla de baja.
	 */
	private String idOperacion;
	
	/**
	 * Concepto de la operación para dinero m�vil.
	 */
	private String concepto;
	// #endregion
	
	// #region Setters / Getters.
	/**
	 * @return Nombre corto del r�pido.
	 */
	public String getNombreCorto() {
		return nombreCorto;
	}

	/**
	 * @param nombreCorto Nombre corto del r�pido a establecer.
	 */
	public void setNombreCorto(final String nombreCorto) {
		this.nombreCorto = nombreCorto;
	}

	/**
	 * @return Cuenta de origen para la operación.
	 */
	public String getCuentaOrigen() {
		return cuentaOrigen;
	}

	/**
	 * @param cuentaOrigen Cuenta de origen para la operación a establecer.
	 */
	public void setCuentaOrigen(final String cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	/**
	 * @return Cuenta de destino para la operación.
	 */
	public String getCuentaDestino() {
		return cuentaDestino;
	}

	/**
	 * @param cuentaDestino Cuenta de destino para la operación a establecer.
	 */
	public void setCuentaDestino(final String cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}

	/**
	 * @return Teléfono de destino.
	 */
	public String getTelefonoDestino() {
		return telefonoDestino;
	}

	/**
	 * @param telefonoDestino Teléfono de destino a establecer.
	 */
	public void setTelefonoDestino(final String telefonoDestino) {
		this.telefonoDestino = telefonoDestino;
	}

	/**
	 * @return Importe de la operación.
	 */
	public float getImporte() {
		return importe;
	}

	/**
	 * @param importe Importe de la operación a establecer.
	 */
	public void setImporte(final float importe) {
		this.importe = importe;
	}
		
	/**
	 * @return Nombre del beneficiario.
	 */
	public String getBeneficiario() {
		return beneficiario;
	}

	/**
	 * @param beneficiario Nombre del beneficiario a establecer.
	 */
	public void setBeneficiario(final String beneficiario) {
		this.beneficiario = beneficiario;
	}

	/**
	 * @return Compa�ia de telefono celular.
	 */
	public String getCompaniaCelular() {
		return companiaCelular;
	}

	/**
	 * @param companiaCelular Compa�ia de telefono celular a establecer.
	 */
	public void setCompaniaCelular(final String companiaCelular) {
		this.companiaCelular = companiaCelular;
	}

	/**
	 * @return Tipo de operación.
	 */
	public String getTipoRapido() {
		return tipoRapido;
	}

	/**
	 * @param tipoRapido Tipo de operación a establecer.
	 */
	public void setTipoRapido(final String tipoRapido) {
		this.tipoRapido = tipoRapido;
	}

	/**
	 * @return Id de operación.
	 */
	public String getIdOperacion() {
		return idOperacion;
	}

	/**
	 * @param idOperacion Id de operación a establecer.
	 */
	public void setIdOperacion(final String idOperacion) {
		this.idOperacion = idOperacion;
	}

	/**
	 * @return Concepto de la operación.
	 */
	public String getConcepto() {
		return concepto;
	}

	/**
	 * @param concepto Concepto de la operación a establecer.
	 */
	public void setConcepto(final String concepto) {
		this.concepto = concepto;
	}
	
	/**
	 * Obtiene el nombre a mostrar segun el tipo de r�pido.
	 */
	public String getNombreOperacionAMostrar() {
		final Context context = SuiteApp.appContext;
		
		if(Tools.isEmptyOrNull(tipoRapido))
			return "";
		else if(Constants.RAPIDOS_CODIGO_OPERACION_OTROS_BBVA.equals(tipoRapido))
			return context.getString(R.string.bmovil_rapidos_traspaso);
		else if(Constants.RAPIDOS_CODIGO_OPERACION_TIEMPO_AIRE.equals(tipoRapido))
			return context.getString(R.string.bmovil_rapidos_tiempo_aire);
		else if(Constants.RAPIDOS_CODIGO_OPERACION_DINERO_MOVIL.equals(tipoRapido))
			return context.getString(R.string.bmovil_rapidos_dinero_movil);
		else 
			return "";
	}
	// #endregion
	
	// #region Constructores.
	/**
	 */
	public Rapido() {
		nombreCorto = null;
		cuentaOrigen = null;
		cuentaDestino = null;
		importe = -1.0f;
		beneficiario = null;
		companiaCelular = null;
		tipoRapido = null;
		idOperacion = null;
		concepto = null;
	}

	/**
	 * @param nombreCorto Nombre corto del r�pido.
	 * @param cuentaOrigen Cuenta de origen para la operación.
	 * @param cuentaDestino Cuenta de destino para la operación.
	 * @param importe Importe de la operación.
	 * @param beneficiario Nombre del beneficiario.
	 * @param referencia Referenc�a de la operación.
	 * @param referenciaNumerica Referenc�a n�merica de la operación.
	 * @param codigoBanco Código del banco.
	 * @param codigoOperacion Código de la operación.
	 * @param idNumber El idNumber.
	 */
	public Rapido(final String nombreCorto, final String cuentaOrigen, final String cuentaDestino, final float importe, final String beneficiario,
				  final String companiaCelular, final String tipoRapido, final String idOperacion, final String concepto) {
		super();
		this.nombreCorto = nombreCorto;
		this.cuentaOrigen = cuentaOrigen;
		this.cuentaDestino = cuentaDestino;
		this.importe = importe;
		this.beneficiario = beneficiario;
		this.companiaCelular = companiaCelular;
		this.tipoRapido = tipoRapido;
		this.idOperacion = idOperacion;
		this.concepto = concepto;
	}
	// #endregion
}
