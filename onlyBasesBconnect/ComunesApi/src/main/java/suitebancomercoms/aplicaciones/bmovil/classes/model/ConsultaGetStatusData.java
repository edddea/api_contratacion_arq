package suitebancomercoms.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by nuttyBoy on 13/07/16.
 */
public class ConsultaGetStatusData implements ParsingHandler {

    public static ConsultaGetStatusData instance = null;

    /**
     * Fecha de contratación del servicio.
     */
    public String creationDate = "";

    /**
     * Fecha sel sistema.
     */
    public String serverDate = "";

    /**
     * Instrumento de seguridad del cliente.
     */
    public String securityInstrument = "";

    /**
     * Estatus del instrumento de seguridad.
     */
    public String securityInstrumentState = "";

    /**
            * Estatus de las alertas Bancomer.
    */
    public String state = "";

    /**
            * Tipo de perfil de cliente
     */

    public String profile = "";

    /**
            * Compañia del numero celular
     */

    public String mobilePhoneCompany = "";

    /**
             * Indica si tiene las alertas activas
     */

    public String alertIndicator = "";

    /**
            * Indicador de cuenta digital
     */

    public String digitalAccount = "";

    public static ConsultaGetStatusData sharedInstance(){
        if(instance == null){
            instance = new ConsultaGetStatusData();
        }

        return instance;
    }


    @Override
    public void process(Parser parser) throws IOException, ParsingException {

    }

    @Override
    public void process(ParserJSON parser) throws IOException, ParsingException {

        creationDate = parser.parseNextValue("creationDate");
        serverDate = parser.parseNextValue("serverDate");
        securityInstrument = parser.parseNextValue("securityInstrument");
        securityInstrumentState = parser.parseNextValue("securityInstrumentState");
        state = parser.parseNextValue("state");
        profile = parser.parseNextValue("profile");
        mobilePhoneCompany = parser.parseNextValue("mobilePhoneCompany");
        alertIndicator = parser.parseNextValue("alertIndicator");
        digitalAccount = parser.parseNextValue("digitalAccount");

    }
}
