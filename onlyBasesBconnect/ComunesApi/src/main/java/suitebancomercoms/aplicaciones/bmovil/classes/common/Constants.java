package suitebancomercoms.aplicaciones.bmovil.classes.common;



/**
 * Application constants.
 * 
 * @author Stefanini IT Solutions.
 */
public class Constants extends EncriptarConstants {

	/**
	 * Constante que indica el campo username en keystore
	 */
	public static final String USERNAME="username";
	
	/**
	 * Constante que indica el campo centro en keystore
	 */
	public static final String CENTRO="centro";
	
	/**
	 * Constante que indica el campo seed en keystore
	 */
	public static final String SEED="seed";
	
	/**
	 * Constante que indica el valor bmovil usado como valor en keystore
	 */
	public static  String BMOVIL="bmovil";
	
	/**
	 * Constante que indica el tiempo de espera de acceso en minutos
	 */
	public static final String TIEMPO_DE_ESPERA_VALIDACION_ACCESO = "30";
	
	/**
	 * 
	 */
	public static final int CHECK_ACCOUNT_NUMBER_LENGTH2 = 5;

	
	/**
	 * Define si hay que sobreescribir el Limite de operacion
	 */
	public static final boolean SOBREESCRIBIR_LO = false;

	/**
	 * Define el valor sobreescrito del limite de operacion
	 */
	public static final double LO_SOBREESCRITO = 0;

	/**
	 * The application version.
	 */
	public static final String APPLICATION_VERSION = "1021";
	
	/**
	 * The application version.
	 */
	public static final String SWITCH_ENROLAMIENTO_ACTIVADO = "S";

	
	/**
	 * The application version.
	 */
	public static final String SWITCH_ENROLAMIENTO_APAGADO = "N";

	/**
	 * Tel uri
	 */
	public final static String TEL_URI = "tel:";

	/**
	 * A credit card type.
	 */
	public static final int DEBIT_CARD_TYPE = 0;

	/**
	 * A credit card type.
	 */
	public static final int CREDIT_CARD_TYPE = 1;

	/**
	 * A credit card type.
	 */
	public static final int PREPAID_CARD_TYPE = 2;

	/**
	 * An express card type.
	 */
	public static final int EXPRESS_CARD_TYPE = 3;

	/**
	 * An check account type.
	 */
	public static final int CHECK_ACCOUNT_TYPE = 4;

	/**
	 * The login deactivation exclusive error code.
	 */
	public static final String DEACTIVATION_ERROR_CODE = "NKR6666";
	
	/**
	 * The login blocked exclusive error code.
	 */
	public static final String BLOCKED_ERROR_CODE = "CNE0362";
	public static final String BLOCKED_ERROR_CODE_2 = "CNE0010";
	
	/**
	 * The error code montos mayores
	 */
	public static final String MONTO_MAYOR_CODE_1 = "CNE0234";
	public static final String MONTO_MAYOR_CODE_2 = "CNE0235";
	public static final String MONTO_MAYOR_CODE_3 = "CNE0236";

	public final static String DELEGATE_PARAM_KEY = "delegate";

	// ///////////////////////////////////////////////////////////////////////////
	// Fast and Favorite Payment Constants //
	// ///////////////////////////////////////////////////////////////////////////

	/**
	 * Payment is being performed in a regular way
	 */
	public static final int PAYMENT_REGULAR_TYPE = 0;

	/**
	 * Payment is being added, deleted, or performed from a favorite payment
	 * option
	 */
	public static final int PAYMENT_FAVORITE_TYPE = 1;

	/**
	 * Payment is being added, deleted, or performed from a fast payment option
	 */
	public static final int PAYMENT_FAST_TYPE = 2;

	/**
	 * Defines the Service Payment type
	 */
	public static final String SERVICE_PAYMENT_TYPE = "01";

	/**
	 * Defines the Airtime purchase type
	 */
	public static final String AIRTIME_PURCHASE_TYPE = "06";

	/**
	 * Defines the Internal Transfer to credit cards type
	 */
	public static final String INTERNAL_TRANSFER_CREDIT_TYPE = "02";

	/**
	 * Defines the Internal Transfer to debit cards type
	 */
	public static final String INTERNAL_TRANSFER_DEBIT_TYPE = "03";

	/**
	 * Defines the Service Payment type
	 */
	public static final String EXTERNAL_TRANSFER_TYPE = "04";

	/**
	 * Defines the Pre-Paid type
	 */
	public static final String INTERNAL_TRANSFER_PREPAID_TYPE = "07";

	/**
	 * Defines the Express type
	 */
	public static final String INTERNAL_TRANSFER_EXPRESS_TYPE = "08";

	/**
	 * Defines the Express type
	 */
	public static final String OPERACION_EFECTIVO_MOVIL_TYPE = "09";

	/**
	 * Define el tipo de operaci��n ver movimientos
	 */
	public static final String OPERACION_VER_MOVIMIENTOS_TYPE = "10";

	/**
	 * Define el tipo de operaci��n transferencia mis cuentas
	 */
	public static final String OPERACION_TRANSFERENCIA_MIS_CUENTAS_TYPE = "11";

	/**
	 * Define el tipo de operaci��n compra de tiempo aire
	 */
	public static final String OPERACION_COMPRAR_TIEMPO_AIRE = "12";
	
	//SPEI
	  /**
     * Operation code for the SPEI m?vil transfers from the transfer menu.
     */
    public static final String OPERACION_SPEI_MOVIL = "13";
    //Termina SPEI
    
    /**
	 * Define el tipo de operaci��n depositos recibidos
	 */
	public static final String OPERACION_DEPOSITOS_RECIBIDOS = "14";

    //Mejoras Bmovil
    
    /**
     * Operation code for the traspaso mis cuentas
     */
    public static final String OPERACION_TRASPASO_MIS_CUENTAS = "15";
    
    /**
     * Operation code for the tdc imports
     */
    public static final String OPERACION_TDC = "16";
    
    /**
     * Operation code for the other tdc
     */
    public static final String OPERACION_PAGAR_OTRAS_TDC = "17";

    /**
    * Operation code for retiro sin tarjeta
    */
    public static final String OPERACION_RETIRO_SINTARJETA = "18";



    
    //Termina Mejoras Bmovil
    
    
	/**
	 * Defines the state when there are no operations involving fast or favorite
	 * payments.
	 */
	public static final int FFPOE_NONE = 0;

	/**
	 * Define the state in which Fast or Frequent payment is being registered.
	 */
	public static final int FFPOE_ADD = 1;

	/**
	 * Define the state in which Fast or Frequent payment is being performed.
	 */
	public static final int FFPOE_PERFORM = 2;

	/**
	 * Define the state in which Fast or Frequent payment is being deleted.
	 */
	public static final int FFPOE_DELETE = 3;

	// ///////////////////////////////////////////////////////////////////////////
	// Password & Visibility variables //
	// ///////////////////////////////////////////////////////////////////////////

	/**
	 * The number of visible digits of the user name (the cellular number).
	 */
	public static final int VISIBLE_NUMBER_CHARCOUNT = 5;

	/**
	 * The number of visible digits of the user name (the cellular number).
	 */
	public static final int VISIBLE_NUMBER_ACCOUNT = 5;

	/**
	 * The number of visible digits of the user name (the cellular number).
	 */
	public static final int VISIBLE_NUMBER_ACCOUNT_FOR_RECORTADO = 4;
	
	/**
	 * The character to mask the invisible digits of the user name.
	 */
	public static final char MASK_CHAR = '*';

	/**
	 * Defines the string to mask user name initial caracters
	 */
	public static final String LOGIN_STRING_MASK = "******";

	/**
	 * The fixed, static length of a password.
	 */
	public static final int PASSWORD_LENGTH = 6;
	
	//SPEI
	/**
     * The card last digits length for the confirmation screen validations.
     */
    public static final int CARD_LAST_DIGITS_LENGTH = 5;
	//Termina SPEI

	/**
	 * The fixed length of a card number.
	 */
	public static final int CARD_NUMBER_LENGTH = 16;

	/**
	 * The fixed length of an American Express card number.
	 */
	public static final int AMEX_CARD_NUMBER_LENGTH = 15;

	/**
	 * The code of American Express as bank.
	 */
	public static final String AMEX_ID = "40103";

	/**
	 * The fixed length of the short description of the movement
	 */
	public static final int MOVEMENT_SHORT_DESCRIPTION = 7;

	/**
	 * The fixed length of a telephone number.
	 */
	public static final int TELEPHONE_NUMBER_LENGTH = 10;

	/**
	 * The fixed length of an activation code.
	 */
	public static final int ACTIVATION_CODE_LENGTH = 10;

	/**
	 * The maximum length for amount fields(includes the decimal point if
	 * exists).
	 */
	public static final int AMOUNT_LENGTH = 13;
	
	/**
	 * The maximum length for amount fields(includes the decimal point if
	 * exists).
	 */
	public static final int AMOUNT_LENGTH_PAGO_TDC = 14;

	/**
	 * The maximum length for the credit card owner name.
	 */
	public static final int OWNER_NAME_LENGTH = 40;

	/**
	 * The maximum length for reason in an external transfer.
	 */
	public static final int REASON_LENGTH = 40;

	/**
	 * Maximum length for a commerce number in nipper.
	 */
	public static final int COMMERCE_NUMBER_LENGTH = 5;

	/**
	 * Fixed transfer numeric reference length.
	 */
	public static final int TRANSFER_REFERENCE_LENGTH = 6;

	/**
	 * Fixed services payment reference length.
	 */
	public static final int SERVICES_PAYMENT_REFERENCE_LENGTH = 20;

	/**
	 * The maximum length for reason in a service payment.
	 */
	public static final int SERVICES_PAYMENT_REASON_LENGTH = 30;

	/**
	 * The maximum length for cieAagreement in a service payment.
	 */
	public static final int SERVICES_PAYMENT_CIE_AGREEMENT_LENGTH = 7;

	/**
	 * The fixed length of a chech account.
	 */
	public static final int CHECK_ACCOUNT_NUMBER_LENGTH = 10;

	/**
	 * Longitud para numero de telefono
	 */
	public static final int NUMERO_TELEFONO_LENGTH = 10;

	/**
	 * Fixed services payment reference length.
	 */
	public static final int NICK_LENGTH = 20;

	// ///////////////////////////////////////////////////////////////////////////
	// Session variables //
	// ///////////////////////////////////////////////////////////////////////////

	/**
	 * Estatus de validacion de softoken
	 */
	public static final String ESTATUS_VALIDACION_SOFTOKEN_06 = "06";
	
	/**
	 * The names of columns.
	 */
	public static final String IDENTIFICADOR = "identificador";
	public static final String VALUE = "value";
	public static final String SCATALOGV = "scatalogv";
	public static final String ICATALOGV = "icatalogv";
	public static final String CAT_OA = "oa";
	public static final String CAT_MG = "mg";
	public static final String CAT_CV = "cv";
	public static final String CAT_IM = "im";
	public static final String CAT_OR = "orden";

	public static final String CAT_AU_PE = "perfil";
	public static final String CAT_AU_PW = "contrasena";
	public static final String CAT_AU_TK = "token";
	public static final String CAT_AU_C2 = "cvv2";
	public static final String CAT_AU_NP = "nip";
	public static final String CAT_AU_RE = "registro";

	public static final String CAT_TL_IM = "imagen";

	public static final String CAT_BM_DT = "primerActivacionFe";
	public static final String CAT_BM_HR = "primerActivacionHr";

	// Etiquetas para el cat��logo de autenticaci��n.
	public static final String AU_TAG_PERFIL = "perfil";
	public static final String AU_TAG_RECORTADO = "recortado";
	public static final String AU_TAG_BASICO = "basico";
	public static final String AU_TAG_AVANZADO = "avanzado";
	public static final String AU_TAG_OPERACION = "operacion";
	public static final String AU_TAG_CREDENCIALES = "credenciales";
	public static final String AU_TAG_CONTRASENA = "contrasena";
	public static final String AU_TAG_TOKEN = "token";
	public static final String AU_TAG_CVV2 = "cvv2";
	public static final String AU_TAG_NIP = "nip";
	public static final String AU_TAG_REGISTRO = "registro";
	public static final String AU_TAG_OPERAR = "operar";
	public static final String AU_TAG_VISIBLE = "visible";
	public static final String AU_TAG_VERSION = "version";

	// Etiquetas para el cat��logo de compa����as telef��nicas.
	public static final String TL_TAG_COMPANIA = "companiaCelular";
	public static final String TL_TAG_IMAGEN = "nombreImagen";

	// Etiquetas para el cat��logo de r������dos.
	public static final String QK_TAG_RAPIDAS = "rapidas";
	public static final String QK_TAG_NICK = "nombreCorto";
	public static final String QK_TAG_ORIGIN = "cuentaOrigen";
	public static final String QK_TAG_DESTINATION = "cuentaDestino";
	public static final String QK_TAG_PHONE = "telefonoDestino";
	public static final String QK_TAG_AMOUNT = "importe";
	public static final String QK_TAG_BENEFICIARY = "nombreBeneficiario";
	public static final String QK_TAG_COMPANY = "companiaCelular";
	public static final String QK_TAG_TYPE = "tipoRapido";
	public static final String QK_TAG_OPERATION = "iDOperacion";
	public static final String QK_TAG_CONCEPT = "concepto";

	/** Etiquetas para la tabla BanderasBmovil. */
	public static final String BANDERAS_CAMBIO_PERFIL = "cambioPerfil";
	public static final String BANDERAS_CONTRATAR = "contratarBmovil";
	public static final String BANDERAS_CONTRATAR2x1 = "contratar2x1";
	public static final String BANDERAS_CAMBIO_CELULAR = "cambiodeCelular";
	public static final String BANDERAS_INDICADOR_CONTRATACION = "indicadorContratacion";

	/** Etiquetas para la tabla temporalST */
	public static final String TEMPORALST_CELULAR = "celular";
	public static final String TEMPORALST_TARJETA = "tarjeta";
	public static final String TEMPORALST_COMPANIA = "compania";
	public static final String TEMPORALST_CONTRASENA = "contrasena";
	public static final String TEMPORALST_CORREO = "correo";
	public static final String TEMPORALST_PERFIL = "perfil";

	public static final String DATABASE_NAME = "MBanking.db";
	public static final String TABLE_MBANKING = "tableMBanking";
	public static final String TABLE_CAT = "tableCat";
	public static final String TABLE_APP_STATUS = "tableAppStatus";
	public static final String TABLE_PROFILE_STATUS = "tableCambioPerfil";
	public static final String TABLE_NUEVO_CATALOGO = "tableNuevoCatalogo";
	public static final String TABLE_AUTHENTICATION_CATALOG = "tableAuthenticationCatalog";
	public static final String TABLE_TELEFONICAS_CATALOG = "tableCompaniasTelefonicasCatalog";
	public static final String TABLE_BMOVIL_ACTIVATION = "tableActivacionBmovil";
	//SPEI
	public static final String TABLE_SPEI_COMPANIES = "tableSpeiCompanies";
	public static final String TABLE_BANDERAS_BMOVIL = "tableBanderasBmovil";
	public static final String TABLE_TEMPORAL_ST = "tableTemporalST";
	public static final String TABLE_TEMPORAL_CAMBIO_TELEFONO = "tableTemporalCambioTelefono";

	// ///////////////////////////////////////////////////////////////////////////
	// Other application constants //
	// ///////////////////////////////////////////////////////////////////////////

	public static final String ESTATUS_ALERTAS_NO ="NO";
	public static final String ESTATUS_ALERTAS_SI ="SI";

	/**
	 * Flag to the if the forms must clear the amount fields on every setup or
	 * after an alert.
	 */
	public static final boolean CLEAR_AMOUNT_ON_SETUP = true;

	/**
	 * OK result from an alert.
	 */
	public static final int OK_ALERT_RESULT = 0;

	/**
	 * Cancel result from an alert.
	 */
	public static final int CANCEL_ALERT_RESULT = 1;

	/**
	 * An clabe account type
	 */
	public static final int CLABE_TYPE = 5;

	/**
	 * The maximum length for Clabe account
	 */
	public static final int CUENTA_CLABE_LENGTH = 18;

	/**
	 * The other amount value
	 */
	public static final String OTHER_AMOUNT = "Otro";

	/**
      * 
      */
	public static final String STATUS_APP_ACTIVE = "A1";

	/**
      * 
      */
	public static final String STATUS_PASS_BLOCKED = "BI";

	/**
      * 
      */
	public static final String STATUS_USER_CANCELED = "C4";

	/**
      * 
      */
	public static final String STATUS_BANK_CANCELED = "CN";

	/**
      * 
      */
	public static final String STATUS_PENDING_ACTIVATION = "PA";

	/**
      * 
      */
	public static final String STATUS_NIP_BLOCKED = "PB";

	/**
      * 
      */
	public static final String STATUS_PENDING_SEND = "PE";

	/**
      * 
      */
	public static final String STATUS_APP_SUSPENDED = "S4";

	/**
	 * Estatus de ium incorrecto.
	 */
	public static final String STATUS_WRONG_IUM = "II";

	/**
	 * Cliente no existe.
	 */
	public static final String STATUS_CLIENT_NOT_FOUND = "NE";

	/**
	 * Cliente inexistente.
	 */
	public static final String STATUS_CLIENT_NOT_EXISTS = "CNE0007";

	/**
	 * Contrataci��n pendiente.
	 */
	public static final String STATUS_ENGAGEMENT_UNCOMPLETE = "PS";

	/**
      * 
      */
	public static final String PROFILE_BASIC_00 = "MF00";

	/**
      * 
      */
	public static final String PROFILE_BASIC_01 = "MF01";

	/**
      * 
      */
	public static final String PROFILE_RECORTADO_02 = "MF02";

	/**
      * 
      */
	public static final String PROFILE_ADVANCED_03 = "MF03";

	/**
	 * Type of account: Check.
	 */
	public static final String CHECK_TYPE = "CH";

	/**
	 * Type of account: Libreton.
	 */
	public static final String LIBRETON_TYPE = "LI";

	/**
	 * Type of account: Savings.
	 */
	public static final String SAVINGS_TYPE = "AH";

	/**
	 * Type of account: Credit.
	 */
	public static final String CREDIT_TYPE = "TC";

	/**
	 * Type of account: Debit.
	 */
	public static final String DEBIT_TYPE = "TD";

	/**
	 * Type of account: Prepaid.
	 */
	public static final String PREPAID_TYPE = "TP";

	/**
	 * Type of account: Express.
	 */
	public static final String EXPRESS_TYPE = "CE";
	
	//SPEI
	
	/**
     * Type of account: SPEI.
     */
    public static final String SPEI_TYPE = "SP";
    //Termina SPEI

	/**
	 * Type of concept for an account: charge (origin).
	 */
	public static final String ORIGIN_CONCEPT = "C";

	/**
	 * Type of concept for an account: deposit (destination).
	 */
	public static final String DESTINATION_CONCEPT = "A";

	/**
	 * Activa token
	 */
	public static final String ACTIVA_TOKEN = "activaToken";
	/**
	 * 
	 */
	public static final String CONTRATACION_BT = "contratacionBT"; 
	
	/**
	 * Type of account: Clabe
	 */
	public final static String CLABE_TYPE_ACCOUNT = "CL";
	
	//SPEI
	//para tipo celular
	//public final static String PHONE_TYPE_ACCOUNT = "TL";
	public final static String PHONE_TYPE_ACCOUNT = "10";

	public static final int PROFILE_CHANGE_MAX_RETRIES = 6;

	public static final int SPLASH_VIEW_CONTROLLER_DURATION = 2000;

	public static final int FLIP_ANIMATION_DURATION = 700;

	public static final String SHOW_BMOVIL_AT_STARTUP = "SHOW_BMOVIL_LOGIN";

	public static final String SEPARADOR_MONTOS_COMPANIA = ",";

	/*
	 * Opciones Menu Administrar *
	 */
	public static final String MADMINISTRAR_ACERCADE = "00";
	public static final String MADMINISTRAR_CAMBIAR_CONTRASENA = "01";
	public static final String MADMINISTRAR_CAMBIO_TELEFONO = "02";
	public static final String MADMINISTRAR_CAMBIO_CUENTA = "03";
	public static final String MADMINISTRAR_ACTUALIZAR_CUENTAS = "04";
	public static final String MADMINISTRAR_SUSPENDER_CANCELAR = "05";
	public static final String MADMINISTRAR_CONFIGURAR_MONTOS = "06";
	public static final String MADMINISTRAR_CONFIGURAR_ALERTAS = "07";
	public static final String MADMINISTRAR_CONFIGURAR_CORREO = "08";
	public static final String MADMINISTRAR_OPERAR_SIN_TOKEN = "09";
	public static final String MADMINISTRAR_OPERAR_CON_TOKEN = "10";
	public static final String MADMINISTRAR_CONSULTAR_CONTRATO = "11";
	public static final String MADMINISTRAR_OPERAR_RECORTADO = "12";
	//SPEI
	public static final String MADMINISTRAR_ASOCIAR_CELULAR	= "13"; 
	public static final String MADMINISTRAR_NOVEDADES = "14";


	/*
	 * Llave utilizada para enviar el valor del icono a la siguiente pantalla
	 */
	public final static String PANTALLA_BASE_ICONO = "PANTALLA_BASE_ICONO";

	/*
	 * Llave utilizada para enviar el valor del titulo a la siguiente pantalla
	 */
	public final static String PANTALLA_BASE_TITULO = "PANTALLA_BASE_TITULO";

	/*
	 * Llave utilizada para enviar el valor del subtitulo a la siguiente
	 * pantalla
	 */
	public final static String PANTALLA_BASE_SUBTITULO = "PANTALLA_BASE_SUBTITULO";

	/*
	 * Llave utilizada para enviar el valor del color al titulo de la siguiente
	 * pantalla
	 */
	public final static String PANTALLA_BASE_TITULO_COLOR = "PANTALLA_BASE_TITULO_COLOR";

	/*
	 * Llave utilizada para enviar el valor tipo de operacion a la siguiente
	 * pantalla
	 */
	public final static String PANTALLA_BASE_TIPO_OPERACION = "PANTALLA_BASE_TIPO_OPERACION";

	/**
	 * Valor que representa que el estatus del instrumento de seguridad es
	 * Activo
	 */
	public final static String ESTATUS_IS_ACTIVO = "A1";

	/**
	 * Valor que representa que el estatus del instrumento de seguridad es
	 * Bloqueado por extravio
	 */
	public final static String ESTATUS_IS_BLOQUEADO_EXTRAVIO = "AL";

	/**
	 * Valor que representa que el estatus del instrumento de seguridad es
	 * Bloqueado por robo o da��o
	 */
	public final static String ESTATUS_IS_BLOQUEADO_ROBO_DANO = "AS";

	/**
	 * Valor que representa que el estatus del instrumento de seguridad es
	 * Bloqueado por cliente
	 */
	public final static String ESTATUS_IS_BLOQUEADO_CLIENTE = "AT";

	/**
	 * Valor que representa que el estatus del instrumento de seguridad es
	 * Bloqueado por banco
	 */
	public final static String ESTATUS_IS_BLOQUEADO_BANCO = "AB";

	/**
	 * Valor que representa el dia con el que se tiene que validar si se
	 * mostrara cambio de perfil
	 */
	public final static int CAMBIO_PERFIL_DIA_MAXIMO = 6;

	/**
	 * Valor que representa que se hara un cambio de perfil de avanzado a basico
	 */
	public final static int CAMBIO_PERFIL_BASICO = 8023;

	/**
	 * Valor que representa que se hara un cambio de perfil de basico a avanzado
	 */
	public final static int CAMBIO_PERFIL_AVANZADO = 8024;

	/*
	 * Valor que representa la longitud del nip
	 */
	public static final int NIP_LENGTH = 4;

	/*
	 * Valor que respresenta la longitud de CVV
	 */
	public static final int CVV_LENGTH = 3;

	/*
	 * valor que representa la longitud del ASM
	 */
	public static final int ASM_LENGTH = 8;

	/*
	 * valor que representa la longitud del nombre corto
	 */
	public static final int NOMBRE_CORTO_LENGTH = 10;

	/*
	 * logitud de beneficiario
	 */
	public static final int TRANSFER_INTERBANCARIO_BENEFICIARIO_LENGTH = 20;

	public static final int UNCOMPLETE_BANK_CODE_LENGTH = 4;

	public static final int CUENTAS_PROPIAS_LENGTH = 1;

	public static final String TP_TC_VALUE = "TC";
	public static final String TP_TD_VALUE = "TD";

	/**
	 * The intent action we are using.
	 */
	public final static String SENT = "SMS_SENT";

	/**
	 * Valos de un cuenta en ceros.
	 */
	public static final double BALANCE_EN_CEROS = 0.0;

	/**
	 * Tipos de instrumento de seguridad como son devueltos por NACAR
	 */
	public static final String IS_TYPE_DP270 = "T3";
	public static final String IS_TYPE_OCRA = "T6";
	
	
	/*
	 * Tipo de consulta Frecuentes
	 */

	public static final String tipoCFOtrosBBVA = "01";
	public static final String tipoCFCExpress = "02";
	public static final String tipoCFOtrosBancos = "03";
	public static final String tipoCFDineroMovil = "04";
	public static final String tipoCFTiempoAire = "05";
	public static final String tipoCFPagosCIE = "06";
	public static final String tipoCFTarjetasCredito = "07";

	/**
	 * Tipo de Tarjeta ingresada
	 */
	public static final String tipoTDD = "TDD";
	public static final String tipoTDC = "TDC";

	/**
	 * Opciones menu consultar
	 */
	public static final String MenuConsultar_movimientos_opcion = "0";
	public static final String MenuConsultar_enviosdm_opcion = "1";
	public static final String MenuConsultar_opfrecuentes_opcion = "2";
	public static final String MenuConsultar_oprapidas_opcion = "3";
	public static final String MenuConsultar_interbancarios = "4";
	public static final String MenuConsultar_depositosrecibidos_opcion = "5";//VALIDAR SI SE INCREMENTA A 4
	public static final String MenuConsultar_obtenercomprobante_opcion = "6";//VALIDAR SI SE INCREMENTA A 5
	public static final String MenuConsultar_estadosdecuenta_opcion = "7";//VALIDAR SI SE INCREMENTA A 6
	public static final String MenuConsultar_retirosintarjeta_opcion ="8";


	public static final String FREQUENT_REQUEST_SCREEN_DELEGATE_KEY = "delegateID";
	public static final String FREQUENT_REQUEST_SCREEN_TITLE_KEY = "frequentScreenTitle";

	public static final String TPPagoServicios = "01";
	public static final String TPTransferInterbancarias = "04";
	public static final String TPTiempoAire = "06";
	public static final String TPTransferExpress = "08";
	public static final String TPDineroMovil = "09";
	public static final String TPTransferBBVA = "99"; // Aplica para cuando no
														// se sepa el tipo (p.e.
														// TDC, TDD, Prepago)
	// public static final String TPTransferOtrosBBVA = "01";
	public static final String TPTarjetaCredito = "02";
	public static final String TPTransferBBVA2 = "03"; // aplica solo ra
														// actualizacion
														// frecuente multicanal

	public static final int MESSAGE_NAME_MAX_LENGTH = 10;

	/**
	 * Indice de la cuenta origen dentro de la lista de cuentas devueltas por el
	 * servidor para las transferencias internas.
	 */
	public static final int SELF_TRANSFER_ORIGIN_ACCOUNT_INDEX = 0;

	/**
	 * Indice de la cuenta destino dentro de la lista de cuentas devueltas por
	 * el servidor para las transferencias internas.
	 */
	public static final int SELF_TRANSFER_DESTINATION_ACCOUNT_INDEX = 1;

	/**
	 * Indice del numero de cuenta dentro de la lista de datos de cada cuenta
	 * devuelta por el servidor para las transferencias internas.
	 */
	public static final int SELF_TRANSFER_ACCOUNT_NUMBER_INDEX = 0;

	/**
	 * Indice del saldo de la cuenta dentro de la lista de datos de cada cuenta
	 * devuelta por el servidor para las transferencias internas.
	 */
	public static final int SELF_TRANSFER_ACCOUNT_BALANCE_INDEX = 1;

	/**
	 * Identificador de movimientos vigentes de dinero movil.
	 */
	public static final int MOVIMIENTOS_DINERO_MOVIL_VIGENTES = 1;

	/**
	 * Identificador de movimientos cancelados de dinero movil.
	 */
	public static final int MOVIMIENTOS_DINERO_MOVIL_CANCELADOS = 2;

	/**
	 * Identificador de movimientos vencidos de dinero movil.
	 */
	public static final int MOVIMIENTOS_DINERO_MOVIL_VENCIDOS = 3;

	/**
	 * Valor que representa la consulta de comisi��n cuando es cuenta BBVA
	 */
	public final static String COMISION_B = "B";

	/**
	 * Valor que representa la consulta de comisi��n cuando es interbancario
	 */
	public final static String COMISION_I = "I";

	/**
	 * Clave de Movistar en los catalogos de compa����as.
	 */
	public final static String COMPANY_KEY_MOVISTAR = "0001";

	/**
	 * Clave de Unefon en los catalogos de compa����as.
	 */
	public final static String COMPANY_KEY_UNEFON = "0002";

	/**
	 * Clave de Telcel en los catalogos de compa����as.
	 */
	public final static String COMPANY_KEY_TELCEL = "0003";

	/**
	 * Clave de Iusacell en los catalogos de compa����as.
	 */
	public final static String COMPANY_KEY_IUSACELL = "0004";

	/**
	 * Nombres de las compannias telefonicas
	 */
	public final static String COMPANY_NAME_TELCEL = "TELCEL";
	public final static String COMPANY_NAME_MOVISTAR = "MOVISTAR";
	public final static String COMPANY_NAME_IUSACELL = "IUSACELL";
	public final static String COMPANY_NAME_UNEFON = "UNEFON";
	public final static String COMPANY_NAME_NEXTEL = "NEXTEL";

	// R��pidos
	/**
	 * N��mero m��ximo de r��pidos que pueden mostrarse en la p��gina, contando el
	 * bot��n de m��s o de agregar en caso de que aparescan.
	 */
	public final static int MAX_RAPIDOS_POR_PAGINA = 5;

	/**
	 * C��digo de una operaci��n r��pida de pago de servicios.
	 */
	public final static String RAPIDOS_CODIGO_OPERACION_TIEMPO_AIRE = "compraTiempoAire";

	/**
	 * C��digo de una operaci��n r��pida de recargas.
	 */
	public final static String RAPIDOS_CODIGO_OPERACION_DINERO_MOVIL = "dineroMovil";

	/**
	 * C��digo de una operaci��n r��pida de otros bancomer.
	 */
	public final static String RAPIDOS_CODIGO_OPERACION_OTROS_BBVA = "otrosBancomer";

	/**
	 * opciones suspender/cancelar del menu administrar
	 */
	public static final String CANCELAR_OP = "cancelar";

	/**
	 * opciones suspender/cancelar del menu administrar
	 */
	public static final String SUSPENDER_OP = "suspender";

	/**
	 * Extras key for use termns.
	 */
	public static final String TERMINOS_DE_USO_EXTRA = "terminosDeUso";
	//SPEI
	/**
	 * Extra key for a custom title.
	 */
	public static final String TITLE_EXTRA = "titleExtra";
	
	/**
	 * Extra key for a custom icon.
	 */
	public static final String ICON_EXTRA = "iconExtra";
	//Termina SPEI

	public static final String INVALID_ACCOUNT_NUMBER = "00000000000000000000";

	public static final String PERSONA_F32 = "F32";
	public static final String PERSONA_F33 = "F33";
	public static final String PERSONA_OTHER = "F34";

	/**
	 * Tipos de alertas
	 */
	public static final String ALERT01 = "01";
	public static final String ALERT02 = "02";
	public static final String ALERT03 = "03";
	public static final String ALERT04 = "04";

	/**
	 * Representaci��n de 30 minutos en milisegundos.
	 */
	//public static final long HALF_HOUR_AS_MILLIS = 1800000L;

	public static final String NON_REGISTERED_ACTIVATION_DATE = "NR";

	/**
	 * Token falso para mostrar en el campo enmascarado.
	 */
	public static final String DUMMY_OTP = "00000000";

	/**
	 * Empty string to avoid the continuous creation of empty String objects.
	 */
	public static final String EMPTY_STRING = "";
	
	//SPEI
	
	/**
	 * Code to determine if a SPEI account has a phone associated.
	 */
	public static final String SPEI_PHONE_ASSOCIATED_CODE = "S";
	
	/**
	 * Code to determine if a SPEI account has not a phone associated.
	 */
	public static final String SPEI_PHONE_UNASSOCIATED_CODE = "N";
	
	/**
	 * Operation code for the association of a phone to an account.
	 */
	public static final String SPEI_OPERATION_TYPE_ASSOCIATE = "Alta";
	
	/**
	 * Operation code for the diassociation of a phone to an account.
	 */
	public static final String SPEI_OPERATION_TYPE_DIASSOCIATE = "Baja";
	
	/**
	 * Operation code for the modification of a phone associated to an account.
	 */
	public static final String SPEI_OPERATION_TYPE_MODIFY = "Modificacion";
	
	/**
	 * Operation code for the association of a phone to an account.
	 */
	public static final String SPEI_OPERATION_TYPE_FOR_SERVER_ASSOCIATE = "ALTA";
	
	/**
	 * Operation code for the diassociation of a phone to an account.
	 */
	public static final String SPEI_OPERATION_TYPE_FOR_SERVER_DIASSOCIATE = "BAJA";
	
	/**
	 * Operation code for the modification of a phone associated to an account.
	 */
	public static final String SPEI_OPERATION_TYPE_FOR_SERVER_MODIFY = "MODIFICA";
	
	/**
	 * Nombre para identificar los shared preferences 
	 */
	public static final String SHARED_NAME = "preferenciasBBBVA";
	
	/**
	 * Nombre para identificar la version de catalogo de spei
	 * */
	public static final String VERSION_SPEI = "version_spei";
	
	/**
	 * Nombre del catalogo spei
	 * */
	public static final String CATALOGO_SPEI = "spei.tmp";

	//Termina SPEI
	
	

	public static final String SALTO_LINEA = "\n";

	/** Mantenimiento de alertas - Operacion de contratar. */
	public static final String MANTENIMIENTO_ALERTAS_CONTRATAR = "contratar";

	/** Mantenimiento de alertas - Operacion de actualizar. */
	public static final String MANTENIMIENTO_ALERTAS_ACTUALIZAR = "actualizar";
	
	/** Mantenimiento de alertas - Operacion de contratar. */
	public static final String OPERACION_ALERTAS_CONTRATAR = "alta";

	/** Mantenimiento de alertas - Operacion de actualizar. */
	public static final String OPERACION_ALERTAS_ACTUALIZAR = "modificacion";
	
	/**
	 * Alta para Estado de Alertas
	 */
	public static final String ESTADO_ALERTAS_ALTA = "Alta";
	
	/**
	 * Modificacion para Estado de Alertas
	 */
	public static final String ESTADO_ALERTAS_MODIFICACION = "Modificaci��n";
	
	
	/**
	 *  Codigos de errores capturados 
	 */
	public static final String CODE_CNE0234 = "CNE0234";
	public static final String CODE_CNE0235 = "CNE0235";
	public static final String CODE_CNE0236 = "CNE0236";
	public static final String CODE_CNE1446 = "CNE1446";
	public static final String CODE_CNE1506 = "CNE1506";
	public static final String CODE_CNE0500 = "CNE0500";
	
	/**
	 * Seleccion de movimientos
	 */
	public static final String MOVIMIENTOS_MES_ACTUAL = "0";
	public static final String MOVIMIENTOS_MES_ANTERIOR = "1";
	public static final String MOVIMIENTOS_DOS_MESES_ATRAS = "2";
	
	public static final String VERSION_FLUJO_CONTRATACION = "1030";
	
	public static final String TEXTO_CAMBIO_TELEFONO_VACIO = "ALERTAS";
	
	public static final String VALOR_VERSION_RECORTADO = "recortado";
	/**consumo clausulas**/
	public static final String PREFORMALIZADO = "F";
	public static final String PREABPROBADO = "P";
	public static final String IDBOVEDA1 = "PBCCMPPI02";
	public static final String IDBOVEDA2 = "PBCCMPPI03";
	
	/**
	 * key terminos consumo
	 */
	public static final String TERMINOS_DE_USO_CONSUMO = "terminosConsumo";

	/**
	 * key terminos consumo
	 */
	public static final String CONTRATO_CONSUMO = "contratoConsumo";

	/**
	 * key terminos consumo
	 */
	public static final String DOMICILIACION_CONSUMO = "domiciliacionConsumo";
	
	
	
	
	//SPEI OPERATIVA DIARIA CS CONSTANTS
	
	
	/**
	 * N��mero de tarjeta
	 */
	
	public static final String NUMERO_TARJETA="numeroTarjeta";
	
	/**
	 * Saldo actual de la cuenta
	 */
	
	public static final String SALDO="saldo";
	
	/**
	 * Indicador de visibilidad de la cuenta
	 */
	
	public static final String VISIBLE="visible";
	
	/**
	 * La moneda actual de la cuenta
	 */
	
	public static final String MONEDA="moneda";
	
	/**
	 * Indicador de tipo de cuenta
	 */
	
	public static final String TIPO_CUENTA="tipoCuenta";
	
	/**
	 * Indicador de concepto de la cuenta
	 */
	
	public static final String CONCEPTO="concepto";
	
	/**
	 * Alias de la cuenta
	 */
	
	public static final String ALIAS="alias";
	
	/**
	 * Celular asociado a la cuenta
	 */
	
	public static final String CELULAR_ASOCIADO="celularAsociado";
	
	/**
	 * C��digo de compania telef��nica
	 */
	
	public static final String CODIGO_COMPANIA="codigoCompania";
	
	/**
	 * Nombre de la compania asociada
	 */
	
	public static final String DESCRIPCION_COMPANIA="descripcionCompania";
	
	/**
	 * Indicador de la fecha de ��ltima modificaci��n sobre la cuenta
	 */
	
	public static final String FECHA_U_M="fechaUltimaModificacion";
	
	/**
	 * Indicadro de cuenta SPEI
	 */
	
	public static final String INDICADOR_SPEI="indicadorSPEI";
	
	
	/**
	 * Week days constants for HS parsing.
	 */
	
	public static final String MONDAY="lunes";
	public static final String TUESDAY="martes";
	public static final String WEDNESDAY="miercoles";
	public static final String THURSDAY="jueves";
	public static final String FRIDAY="viernes";
	public static final String SATURDAY="sabado";
	public static final String SUNDAY="domingo";
	
	public static final String SPEI_CELULAR_INVALIDO="speiCelularInvalido";
	public static final String NON_VALID_ONE="noValido1";
	public static final String NON_VALID_TWO="noValido2";
	
	
	public static final String textFornonValidOne="one";
	public static final String textFornonValidTwo="two";
	//O3
	public static final String CODIGO_ERROR_MBANK0003="MBANK0003";
	public static final String CODIGO_ERROR_MBANK0007="MBANK0007";
	public static final String CODIGO_AVISO_ACA022 = "ACA022";
	public static final String CODIGO_AVISO_ACA0022 = "ACA0022";
	public static final String CODIGO_AVISO_FEA0023 = "FEA0023";

	public static final String CAMPAÑA_PAPERLESS_VISIBLE = "";

	/**
	 * Regresa la cadena numeroTelefono para la transferencia de datos entre activities
	 */
	public static final String PAR_NUMERO_TELEFONO = "numeroTelefono";

}
