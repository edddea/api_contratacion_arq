package suitebancomercoms.aplicaciones.bmovil.classes.io;

/**
 * Result wraps the result data of a response (status, error code, error message).
 */
public class Result {

    /**
     * The status code.
     */
    private String status;

    /**
     * The error code.
     */
    private String code;

    /**
     * The error message.
     */
    private String message;

    /**
     * The URL for mandatory application update.
     */
    private String updateURL = null;

    /**
     * Default constructor.
     * @param stat the status code
     * @param cod the error code
     * @param msg the error message
     */
    public Result(final String stat, final String cod, final String msg) {
        this.status = stat;
        this.code = cod;
        this.message = msg;
    }

    /**
     * Default constructor.
     * @param st the status code
     * @param cod the error code
     * @param msg the error message
     * @param urlUpd the URL for mandatory application updating
     */
    public Result(final String st, final String cod, final String msg, final String urlUpd) {
        this.status = st;
        this.code = cod;
        this.message = msg;
        this.updateURL = urlUpd;
    }

    /**
     * Get the status code.
     * @return status code
     */
    public String getStatus() {
        return status;
    }

    /**
     * Get the error code.
     * @return the error code
     */
    public String getCode() {
        return code;
    }

    /**
     * Get the error message.
     * @return the error message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Get the update URL.
     * @return the update URL for mandatory updating
     */
    public String getUpdateURL() {
        return updateURL;
    }
}
