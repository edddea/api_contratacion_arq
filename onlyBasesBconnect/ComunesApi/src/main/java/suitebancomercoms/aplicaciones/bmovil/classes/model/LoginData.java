/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomercoms.aplicaciones.bmovil.classes.model;

import android.util.Log;

import com.bancomer.base.SuiteApp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import bancomer.api.common.model.CatalogoVersionado;
import bancomer.api.common.model.Compania;
import bancomer.api.common.model.Convenio;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Catalog;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * LoginData wraps the information involved in the response from the server
 * after a login operation.
 *
 * @author Stefanini IT Solutions.
 */

public class LoginData implements ParsingHandler {


    /**
     * Serial Version UID
     */
    private static final long serialVersionUID = -400496845278428572L;

    /**
     * Bandera para migrar booleano
     * TRUE o FALSE
     */
    private String mg;


    /**
     * Fecha de inicio de migracion básico Númerico
     * Formato DDMMAAAA
     */
    private String bi;


    /**
     * Fecha de inicio de bloqueo básico
     * Númerico
     * Formato DDMMAAAA
     */
    private String bf;

    /**
     * Fecha de inicio de migración recortado
     * Númerico
     * Formato DDMMAAAA
     */
    private String ri;


    /**
     * Fecha de inicio de bloqueo recortado
     * Númerico
     * Formato DDMMAAAA
     */
    private String rf;

    public String getMg() {
        return mg;
    }

    public String getBi() {
        return bi;
    }

    public String getBf() {
        return bf;
    }

    public String getRi() {
        return ri;
    }

    public String getRf() {
        return rf;
    }

    public Boolean getMgBooleano() {
        Boolean result = false;
        if (mg != null && "TRUE".equals(mg.toUpperCase())) {
            result = true;
        }
        return result;
    }


    /**
     * The Bancomer token.
     */
    private String bancomerToken = null;

    /**
     * Vía 1 o 2
     */
    private String via = null;

    /**
     * The client identifier for session purposes.
     */
    private String clientNumber = null;

    /**
     * The server date.
     */
    private String serverDate = null;

    /**
     * The server time.
     */
    private String serverTime = null;

    /**
     * The user account list.
     */
    private Account[] accounts = null;

    /**
     * The catalogs.
     */
    private Catalog[] catalogs = null;

    /**
     * The version of each catalog.
     */
    private String[] catalogVersions = null;

    /**
     * The optional message to show after a successful login.
     */
    private String message = null;

    /**
     * The message to show that is an update available for the application.
     */
    private String updateMessage = null;

    /**
     * URL for the case in that the response is an update message.
     */
    private String updateURL = null;

    /**
     * Nombre del cliente
     */
    private String nombreCliente;

    /**
     * Compañia a la que pertenece el teléfono del cliente
     */
    private String companiaTelCliente;

    /**
     * Correo electrónico del cliente
     */
    private String emailCliente;

    /**
     * Perfil del cliente
     */
    private String perfilCliente;

    /**
     * Tipo de instrumento de seguridad del cliente
     */
    private String insSeguridadCliente;

    /**
     * Estatus del servicio
     */
    private String estatusServicio;

    /**
     * The session timeout in minutes.
     */
    private int timeout = 1;

    private String estatusInstrumento;

    private CatalogoVersionado catalogoTA;

    private CatalogoVersionado catalogoDineroMovil;

    private CatalogoVersionado catalogoConvenios;

    private CatalogoVersionado catalogoMantenimientoSPEI;

    private String autenticacionJson;

    private String limiteOperacion;

    private String jsonRapidos;

    /**
     * Estatus de alertas (alta, modificacion o vacio).
     */
    private String estatusAlertas;

    /**
     * Fecha de contratacci�n de bancomer m�vil.
     */
    private String fechaContratacion;

    private String candidatoPromociones;
    private ArrayList<Campania> listaPromocionesJSON;
    private String contratoBBVAlink;
    private String marcaTendero;

    /**
     * Catálogo de compañías telefonicas.
     */
    private String catTelefonicas;
    //One Click
    /**
     * json de promociones
     */
    private String isPromocion;
    private String jSonPromociones;

/////////////////////////////////////////

    //CS raw Json.
    private String csJson;

    //HS raw Json.
    private String hsJson;

////////////////////////////////////////

    /**
     * Default constructor.
     */
    public LoginData() {
        // Default constructor
    }

    /**
     * @return the emailCliente
     */
    public String getEmailCliente() {
        return emailCliente;
    }

    /**
     * @return the estatusInstrumento
     */
    public String getEstatusInstrumento() {
        return estatusInstrumento;
    }


    /**
     * set estatusInstrumento
     */
    public void setEstatusInstrumento(String estatusInstrumento) {
        this.estatusInstrumento = estatusInstrumento;
    }

    /**
     * Get the client identifier for session purposes.
     *
     * @return the client identifier for session purposes
     */
    public String getClientNumber() {
        return clientNumber;
    }

    /**
     * get the server date.
     *
     * @return the server date
     */
    public String getServerDate() {
        return serverDate;
    }

    /**
     * Get the server time.
     *
     * @return the server time
     */
    public String getServerTime() {
        return serverTime;
    }

    /**
     * Get via
     *
     * @return via
     */
    public String getVia() {
        return via;
    }

    /**
     * Get the Bancomer token.
     *
     * @return the Bancomer token
     */
    public String getBancomerToken() {
        return bancomerToken;
    }

    /**
     * get the user's accounts.
     *
     * @return the user's accounts
     */
    public Account[] getAccounts() {
        if (Tools.isNull(accounts)) {
            return new Account[0];
        } else {
            return accounts.clone();
        }
    }

    /**
     * Get the catalogs.
     *
     * @return the catalogs
     */
    public Catalog[] getCatalogs() {
        if (Tools.isNull(catalogs)) {
            return new Catalog[0];
        } else {
            return catalogs.clone();
        }
    }

    /**
     * Get the authentication JSON.
     *
     * @return Authentication JSON string.
     */
    public String getAuthenticationJson() {
        return this.autenticacionJson;
    }

    public CatalogoVersionado getCatalogoTiempoAire() {
        return catalogoTA;
    }

    public CatalogoVersionado getCatalogoDineroMovil() {
        return catalogoDineroMovil;
    }

    public CatalogoVersionado getCatalogoServicios() {
        return catalogoConvenios;
    }

    public CatalogoVersionado getCatalogoMantenimientoSPEI() {
        return catalogoMantenimientoSPEI;
    }

    /**
     * Returns the catalog version array.
     *
     * @return the catalog version array
     */
    public String[] getCatalogVersions() {
        if (Tools.isNull(catalogVersions)) {
            return new String[0];
        } else {
            return catalogVersions.clone();
        }
    }

    /**
     * Get the optional message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Get the updating message.
     *
     * @return the updating message
     */
    public String getUpdateMessage() {
        return updateMessage;
    }

    /**
     * Get the updating URL.
     *
     * @return the URL
     */
    public String getUpdateURL() {
        return updateURL;
    }

    /**
     * Get the session timeout.
     *
     * @return the session timeout, in minutes
     */
    public int getTimeout() {
        return timeout;
    }

    public String getPerfiCliente() {
        return perfilCliente;
    }

    public String getInsSeguridadCliente() {
        return insSeguridadCliente;
    }

    public void setEstatusServicio(String estatusServicio) {
        this.estatusServicio = estatusServicio;
    }

    public String getEstatusServicio() {
        return estatusServicio;
    }

    public String getEstatusIS() {
        return estatusInstrumento;
    }

    public String getCompaniaTelCliente() {
        return companiaTelCliente;
    }

    public String getJsonRapidos() {
        return jsonRapidos;
    }

    /**
     * @return the estatusAlertas
     */
    public String getEstatusAlertas() {
        return estatusAlertas;
    }

    /**
     * @return the fechaContratacion
     */
    public String getFechaContratacion() {
        return fechaContratacion;
    }

    /**
     * @return the catTelefonicas
     */
    public String getCatTelefonicas() {
        return catTelefonicas;
    }

    /**
     * @return the nombreCliente
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    //One click
    public String getIsPromocion() {
        return isPromocion;
    }

    public void setIsPromocion(final String isPromocion) {
        this.isPromocion = isPromocion;
    }


    /////////////////////////////////////

    //Set and Get which belong to csJson.
    public String getCSJson() {
        return csJson;
    }

    public void setCSJson(final String csJson) {
        this.csJson = csJson;
    }

    //Set and Get which belong to hsJson


    public String getHSJson() {
        return hsJson;
    }

    public void setHSJson(final String hsJson) {
        this.hsJson = hsJson;
    }


    /////////////////////////////////////

    /**
     * Process the login response and store the attributes.
     *
     * @param parser reference to the parser
     * @throws IOException      on communication errors
     * @throws ParsingException on parsing errors
     */
    public void process(final Parser parser) throws IOException, ParsingException {
        //MODIFIED BY Michael Andrade
        //Parsing new server response
        clientNumber = parser.parseNextValue("TE");
        serverDate = parser.parseNextValue("FE");
        serverTime = parser.parseNextValue("HR");
        final String timeoutStr = parser.parseNextValue("TO");
        try {
            timeout = Integer.parseInt(timeoutStr);
        } catch (Throwable th) {
            timeout = 1;
        }
        accounts = parseAccounts(parser, serverDate);
        catalogs = new Catalog[4];
        catalogVersions = new String[8];
        final String next = parseCatalogs(parser, catalogs, catalogVersions);
        message = next.substring(2, next.length()); //MP value is required
        updateMessage = parser.parseNextValue("ME", false);
        updateURL = parser.parseNextValue("UR", false);
        via = parser.parseNextValue("VA");
        estatusInstrumento = parser.parseNextValue("EI");
        companiaTelCliente = parser.parseNextValue("CM", false);
        emailCliente = parser.parseNextValue("EM", false);
        perfilCliente = parser.parseNextValue("PR", false);
        insSeguridadCliente = parser.parseNextValue("IS", false);

        //TODO quickies parser
//	        String tmpEntity = parser.parseNextEntity();
//	        if (tmpEntity.equals("RP")) {
//	        	jsonRapidos = parser.parseNextValue("RP");
//	        	do {
//	        		tmpEntity = parser.parseNextEntity();
//	        	} while(!tmpEntity.equals("TA") && !tmpEntity.contains("ST"));
//	        }

        jsonRapidos = parser.parseNextValue("RP", false);
        if (!Tools.isEmptyOrNull(jsonRapidos))
            jsonRapidos = jsonRapidos.replaceAll("\":\"\"", "\":[]");

        // Se establece el json de rápidos como un arreglo vacio.
        jsonRapidos = "{\"rapidas\":[]}";

        Session.getInstance(SuiteApp.appContext).parseRapidosJson(jsonRapidos);
        String tmpEntity;
        do {
            tmpEntity = parser.parseNextEntity();
        }
        while (!tmpEntity.equals("TA") && !tmpEntity.contains("DM") && !tmpEntity.contains("SV") && !tmpEntity.contains("ST") && !tmpEntity.contains("MS"));

        if (tmpEntity.contains("ST")) {
            estatusServicio = tmpEntity.substring(2);
            parser.parseNextValue("ST", false);
        } else {
            if (tmpEntity.contains("TA")) {
                catalogoTA = parseCompanyCatalog(parser);
                catalogVersions[4] = catalogoTA.getVersion();
                tmpEntity = parser.parseNextEntity();
            }

            if (tmpEntity.contains("DM")) {
                catalogoDineroMovil = parseCompanyCatalog(parser);
                catalogVersions[5] = catalogoDineroMovil.getVersion();
                tmpEntity = parser.parseNextEntity();
            }

            if (tmpEntity.contains("SV")) {
                catalogoConvenios = parseConvenioCatalog(parser);
                catalogVersions[6] = catalogoConvenios.getVersion();
                tmpEntity = parser.parseNextEntity();
            }

            if (tmpEntity.contains("MS")) {
                catalogoMantenimientoSPEI = parseSpeiCatalog(parser);
                catalogVersions[7] = catalogoMantenimientoSPEI.getVersion();
                tmpEntity = "";
            }

            if (tmpEntity.equals("")) {
                estatusServicio = parser.parseNextValue("ST");
            } else if (tmpEntity.length() > 3) {
                estatusServicio = tmpEntity.substring(2, tmpEntity.length());
            } else {
                estatusServicio = "";
            }
        }

        nombreCliente = parser.parseNextValue("NO", false);
        autenticacionJson = parser.parseNextValue("AU", false);
        limiteOperacion = parser.parseNextValue("LO");

        estatusAlertas = parser.parseNextValue("EA");
        fechaContratacion = parser.parseNextValue("FC");
        if (null == fechaContratacion) {
            fechaContratacion = "";
            //One Click
            if (estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)) {
                isPromocion = parser.parseNextValue("PM");
                /*oneclick parseo
	            if(isPromocion.compareTo("SI")==0){ 
	             jSonPromociones= parser.parseNextValue("LP", false);
	             if(!Tools.isEmptyOrNull(jSonPromociones))
	             	jSonPromociones = jSonPromociones.replaceAll("\":\"\"", "\":[]");
	             
	             
	             Session.getInstance(SuiteApp.appContext).parsePromocionesJson(jSonPromociones);
	            }*/
            }
            //Termina One click
        }

        if (!estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)) {
            catTelefonicas = parser.parseNextValue("TM", false);
            if (!Tools.isEmptyOrNull(catTelefonicas))
                Session.getInstance(SuiteApp.appContext).saveCatalogoCompaniasTelefonicas(catTelefonicas);
        }

        if (estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)) {
            candidatoPromociones = parser.parseNextValue("PM");
            if (candidatoPromociones == null)
                candidatoPromociones = "";
            if (candidatoPromociones.compareTo("SI") == 0) {
                jSonPromociones = parser.parseNextValue("LP", false);

                if (!Tools.isEmptyOrNull(jSonPromociones))
                    jSonPromociones = jSonPromociones.replaceAll("\":\"\"", "\":[]");


                Session.getInstance(SuiteApp.appContext).parsePromocionesJson(jSonPromociones);
            } else {
                final String listaPromociones = parser.parseNextValue("LP", false);
                Session.getInstance(SuiteApp.appContext).parsePromocionesJson(listaPromociones);
                listaPromocionesJSON = parseListaPromociones(listaPromociones);
            }

            //String listaPromociones = parser.parseNextValue("LP", false);
            //listaPromocionesJSON = parseListaPromociones(listaPromociones);

            contratoBBVAlink = parser.parseNextValue("LK");

            final String stringMarcaTendero = parser.parseNextValue("TN", false);
            marcaTendero = parseMarcaTendero(stringMarcaTendero);
        }

        ////////////////////SPEI OPERATIVA DIARIA/////////////////////////////////////////

        if (estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)) {
            Session.setListA(new ArrayList<Account>());
            Session.setListB(new ArrayList<Account>());
            csJson = parser.parseNextValue("CS", false);
            if (!Tools.isEmptyOrNull(csJson))
                Session.getInstance(SuiteApp.appContext).parserCS(csJson);

        }

        if (estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)) {
            hsJson = parser.parseNextValue("HS", false);
            if (!Tools.isEmptyOrNull(hsJson))
                Session.getInstance(SuiteApp.appContext).saveCatalogoHorasServicio(hsJson);

            if (ServerCommons.ALLOW_LOG)
                Log.e("switch value", String.valueOf(Session.getSwitchSPEI()));
        }


        //////////////////////END SPEI OPERATIVA DIARIA./////////////////////////////////


        Session.getInstance(SuiteApp.appContext).setAuthenticationJson(autenticacionJson);
        double limite;
        if (!Constants.SOBREESCRIBIR_LO)
            limite = Double.parseDouble(limiteOperacion) / 100.0;
        else
            limite = Constants.LO_SOBREESCRITO;
        Autenticacion.getInstance().setLimiteOperacion(limite);

        mg = parser.parseNextValue("MR", false);
        bi = parser.parseNextValue("BI", false);
        bf = parser.parseNextValue("BF", false);
        ri = parser.parseNextValue("RI", false);
        rf = parser.parseNextValue("RF", false);

    }


    // private CatalogoVersionado parseMantenimientoSPEICatalog(Parser parser) {
    // TODO Auto-generated method stub
    //	return null;
    //}

    /**
     * Parse the account list.
     *
     * @param parser reference to the parse
     * @param date   the server date
     * @return the account list
     * @throws IOException           on input output errors
     * @throws ParsingException      on communication errors
     * @throws NumberFormatException on parsing errors
     */
    private static Account[] parseAccounts(final Parser parser, final String date) throws IOException, NumberFormatException, ParsingException {
        String accountType;
        String currency;
        String accountNumber;
        String amount;
        String concept;
        String visible;
        String alias;
        parser.parseNextValue("CT");
        final int accounts = Integer.parseInt(parser.parseNextValue("OC"));
        Account[] result = new Account[accounts];
        Account account;
        for (int i = 0; i < accounts; i++) {
            accountType = parser.parseNextValue("TP");
            alias = parser.parseNextValue("AL");
            currency = parser.parseNextValue("DV");
            accountNumber = parser.parseNextValue("AS");
            amount = parser.parseNextValue("IM");
            concept = parser.parseNextValue("CP");
            visible = parser.parseNextValue("VI");
            account = new Account(accountNumber, Tools.getDoubleAmountFromServerString(amount), Tools.formatDate(date), "S".equals(visible), currency, accountType, concept, alias);
            result[i] = account;
        }
        return result;
    }

    /**
     * Parse a catalog.
     *
     * @param parser a reference to the parser
     * @return the catalog
     * @throws IOException           on communication errors
     * @throws ParsingException      on parsing errors
     * @throws NumberFormatException on format errors
     */
    private static Catalog parseCatalog(final Parser parser) throws IOException, NumberFormatException, ParsingException {
        final Catalog result = new Catalog();
        String entry;
        String code;
        String value;
        final int entries = Integer.parseInt(parser.parseNextValue("OC"));
        for (int i = 0; i < entries; i++) {
            entry = parser.parseNextEntity(true);
            if (entry != null) {
                final int pos = entry.indexOf('-');
                code = entry.substring(0, pos);
                value = entry.substring(pos + 1);
                result.add(code, value);
            }
        }
        return result;
    }


    /**
     * Parse a catalog.
     *
     * @param parser a reference to the parser
     * @return the catalog
     * @throws IOException           on communication errors
     * @throws ParsingException      on parsing errors
     * @throws NumberFormatException on format errors
     */
    private static CatalogoVersionado parseCompanyCatalog(final Parser parser) throws IOException, NumberFormatException, ParsingException {
        String nombre;
        String imagen;
        String clave;
        String montos;
        String orden;
        final String version = parser.parseNextValue("VE");
        final int entries = Integer.parseInt(parser.parseNextValue("OC"));
        final CatalogoVersionado catalogoVersionado = new CatalogoVersionado(version);
        Compania compania;

        for (int i = 0; i < entries; i++) {
            nombre = parser.parseNextValue("OA");
            imagen = parser.parseNextValue("MG");
            clave = parser.parseNextValue("CV");
            montos = parser.parseNextValue("IM");
            orden = parser.parseNextValue("OR");

            compania = new Compania(nombre, imagen, clave, montos, orden);
            catalogoVersionado.agregarObjeto(compania);
        }
        return catalogoVersionado;
    }

    /**
     * Parse a catalog.
     *
     * @param parser a reference to the parser
     * @return the catalog
     * @throws IOException           on communication errors
     * @throws ParsingException      on parsing errors
     * @throws NumberFormatException on format errors
     */
    private static CatalogoVersionado parseConvenioCatalog(final Parser parser) throws IOException, NumberFormatException, ParsingException {
        String nombre;
        String imagen;
        String clave;
        String orden;
        final String version = parser.parseNextValue("VE");
        final int entries = Integer.parseInt(parser.parseNextValue("OC"));
        final CatalogoVersionado catalogoVersionado = new CatalogoVersionado(version);
        Convenio convenio;

        for (int i = 0; i < entries; i++) {
            clave = parser.parseNextValue("CI");
            imagen = parser.parseNextValue("MG");
            nombre = parser.parseNextValue("NM");
            orden = parser.parseNextValue("OR");

            convenio = new Convenio(nombre, imagen, clave, orden);
            catalogoVersionado.agregarObjeto(convenio);
        }
        return catalogoVersionado;
    }

    /**
     * The SPEI companies catalog.
     */
    CatalogoVersionado speiCatalog = null;

    /**
     * @return The SPEI companies catalog.
     */
    public CatalogoVersionado getSpeiCatalog() {
        return speiCatalog;
    }

    // private static CatalogoVersionado parseMantenimientoSPEICatalog(Parser parser){ //throws IOException, NumberFormatException, ParsingException {
    private CatalogoVersionado parseSpeiCatalog(final Parser parser) {
    	/*String nombreCompania;
    	String nombreImagen;
    	String claveCompania;
    	String orden;*/
        String nombre;
        String imagen;
        String clave;
        String orden;
        Compania compania;
        CatalogoVersionado catalog;
        try {
            final String version = parser.parseNextValue("VE");
            final int entries = Integer.parseInt(parser.parseNextValue("OC"));
            catalog = new CatalogoVersionado(version);
            //	MantenimientoSpei mantenimientoSpei;

            for (int i = 0; i < entries; i++) {
                nombre = parser.parseNextValue("OA");
                imagen = parser.parseNextValue("MG");
                clave = parser.parseNextValue("CV");
                orden = parser.parseNextValue("OR");
                compania = new Compania(nombre, imagen, clave, "", orden);
                // catalogoVersionado.agregarObjeto(mantenimientoSpei);
                catalog.agregarObjeto(compania);
            }
        } catch (Exception ex) {
            if (ServerCommons.ALLOW_LOG)
                Log.e(this.getClass().getSimpleName(), "Error while parsing the SPEI companies catalog.", ex);
            catalog = null;
        }
        return speiCatalog = catalog;
    }


    private static ArrayList<Campania> parseListaPromociones(final String listaPromociones) {

        ArrayList<Campania> listaCampaniasJSON = null;

        if ((listaPromociones != null) && (!"".equals(listaPromociones))) {
            listaCampaniasJSON = new ArrayList<Campania>();
            JSONObject jsonObject;

            try {
                jsonObject = new JSONObject(listaPromociones);
            } catch (JSONException ex) {
                jsonObject = null;
                if (ServerCommons.ALLOW_LOG)
                    Log.e("LoginData", "Error while trying to parse Json.", ex);
            }

            if (null != jsonObject) {

                try {
                    final JSONArray arrayCampanias = jsonObject.getJSONArray("campanias");

                    for (int i = 0; i < arrayCampanias.length(); i++) {
                        final JSONObject campaniaJSON = arrayCampanias.getJSONObject(i);
                        final String claveCampania = campaniaJSON.getString("cveCamp");
                        final String descripcionOferta = campaniaJSON.getString("desOferta");
                        final String monto = campaniaJSON.getString("monto");
                        String carrusel = campaniaJSON.getString("carrusel");

                        final Campania campania = new Campania(claveCampania, descripcionOferta, monto);
                        listaCampaniasJSON.add(campania);
                    }

                } catch (JSONException ex) {
                    if (ServerCommons.ALLOW_LOG)
                        Log.e("LoginData", "Error while reading the JSON.", ex);
                }
            }
        }

        return listaCampaniasJSON;
    }

    private String parseMarcaTendero(final String stringTendero) {
        JSONObject jsonObject;
        String tendero = null;

        if ((stringTendero != null) && (!"".equals(stringTendero))) {
            try {
                jsonObject = new JSONObject(stringTendero);
                tendero = jsonObject.getString("tendero");
            } catch (JSONException ex) {
                if (ServerCommons.ALLOW_LOG)
                    Log.e("LoginData", "Error while trying to parse Json.", ex);
            }
        }

        return tendero;
    }

    /**
     * Parse the collection of catalogs.
     *
     * @param parser          a reference to the parser
     * @param catalogs        (out) parsed catalogs
     * @param catalogVersions version of catalog
     * @return the optional message
     * @throws IOException      on communication errors
     * @throws ParsingException on parsing errors
     */
    private static String parseCatalogs(final Parser parser,
                                        final Catalog[] catalogs, final String... catalogVersions)
            throws IOException, ParsingException {
        String next = null;
        String entity;
        boolean finished = false;
        do {
            entity = parser.parseNextEntity();
            if (entity == null) {
                next = null;
                finished = true;
            } else {
                if (entity.startsWith("C1")) {
                    catalogVersions[0] = entity.substring(2);
                    catalogs[0] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C1=" + catalogVersions[0]);
                } else if (entity.startsWith("C4")) {
                    catalogVersions[1] = entity.substring(2);
                    catalogs[1] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C2=" + catalogVersions[1]);
                } else if (entity.startsWith("C5")) {
                    catalogVersions[2] = entity.substring(2);
                    catalogs[2] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C3=" + catalogVersions[2]);
                } else if (entity.startsWith("C8")) {
                    catalogVersions[3] = entity.substring(2);
                    catalogs[3] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C4=" + catalogVersions[3]);
                } else if (entity.startsWith("TA")) {
                    catalogVersions[4] = entity.substring(2);
                    catalogs[4] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C5=" + catalogVersions[4]);
                } else if (entity.startsWith("DM")) {
                    catalogVersions[5] = entity.substring(2);
                    catalogs[5] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C6=" + catalogVersions[5]);
                } else if (entity.startsWith("SV")) {////Se Agrego nuevo else if
                    catalogVersions[6] = entity.substring(2);
                    catalogs[6] = parseCatalog(parser);
                    //System.out.println("Received Parsed Version C7=" + catalogVersions[6]);
                } else if (entity.startsWith("MS")) {
                    catalogVersions[7] = entity.substring(2);
                    catalogs[7] = parseCatalog(parser);
                } else {
                    next = entity;
                    finished = true;
                }
            }
        } while (!finished);


        return next;
    }

    @Override
    public void process(final ParserJSON parser) throws IOException, ParsingException {
        // TODO Auto-generated method stub

    }

    public void process(JSONObject parserArqService) throws IOException, ParsingException, JSONException {
        // TODO Auto-generated method stub
        clientNumber = parserArqService.getString("contractId");
        final SimpleDateFormat formatter = new SimpleDateFormat("M/dd/yy hh:mm a", Locale.US);
        final SimpleDateFormat formattercontratacion = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        final SimpleDateFormat formatter2 = new SimpleDateFormat("ddMMyyyy", Locale.US);
        final SimpleDateFormat formatter3 = new SimpleDateFormat("HHmmss", Locale.US);

        final String datetime = parserArqService.getString("serverDate");//Formato M/DD/AA HH:MM PM/AM
        final String fechaContratacionServer = parserArqService.getString("modificationDate");//nuevo descomentar
        fechaContratacion = "";
        try {
            Date date = formatter.parse(datetime);
            serverDate = formatter2.format(date);
            serverTime = formatter3.format(date);
            date = formattercontratacion.parse(fechaContratacionServer);
            fechaContratacion = formatter2.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        timeout = 1;
        accounts = parseAccounts(parserArqService.getJSONArray("accounts"), parserArqService.optJSONObject("speiAccount"));
        catalogs = new Catalog[4];
        catalogVersions = new String[8];
        fillVesionCat(parserArqService.getJSONObject("catalogs"));
        parseCatalogos1(parserArqService.getJSONObject("catalogs").getJSONObject("catalogos"));
        parseCatalogos4(parserArqService.getJSONObject("catalogs").getJSONObject("catalogos"));
        parseCatalogos5(parserArqService.getJSONObject("catalogs").getJSONObject("catalogos"));
        parseCatalogos8(parserArqService.getJSONObject("catalogs").getJSONObject("catalogos"));

        parseCatalogoTA(parserArqService.getJSONObject("catalogs").getJSONObject("catalogos"));
        parseCatalogoDM(parserArqService.getJSONObject("catalogs").getJSONObject("catalogos"));
        parseCatalogoSV(parserArqService.getJSONObject("catalogs").getJSONObject("catalogos"));
        parseCatalogoMS(parserArqService.getJSONObject("catalogs").getJSONObject("catalogos"));
        parseCatalogoTM(parserArqService.getJSONObject("catalogs").getJSONObject("catalogos"));

        via = "3";
        estatusInstrumento = parserArqService.getString("securityInstrumentState");
        if ("ACTIVE".equals(estatusInstrumento)) {
            estatusInstrumento = "A1";
        } else {
            estatusInstrumento = "BL";
        }
        companiaTelCliente = parserArqService.getString("mobilePhoneCompany");
        emailCliente = parserArqService.getString("email");
        perfilCliente = parserArqService.getString("profile");

        if ("ADVANCED".equals(perfilCliente)) {
            perfilCliente = "MF03";
        } else if ("BASIC".equals(perfilCliente)) {
            perfilCliente = "MF01";
        } else if ("LIMITED".equals(perfilCliente)) {
            perfilCliente = "MF02";
        }
        insSeguridadCliente = parserArqService.getString("securityInstrument");
        if ("TOKEN_T1_GO3".equals(insSeguridadCliente)) {
            insSeguridadCliente = "T1";
        } else if ("TOKEN_T6_OCRA".equals(insSeguridadCliente)) {
            insSeguridadCliente = "T6";
        } else if ("TOKEN_T3_DP270".equals(insSeguridadCliente)) {
            insSeguridadCliente = "T3";
        } else if ("SOFTWARE_TOKEN".equals(insSeguridadCliente)) {
            insSeguridadCliente = "S1";
        }
        jsonRapidos = "{\"rapidas\":[]}";

        Session.getInstance(SuiteApp.appContext).parseRapidosJson(jsonRapidos);

        estatusServicio = parserArqService.getString("state");//A1
        nombreCliente = parserArqService.getString("name");
        autenticacionJson = getcatalogoAutenticacion(parserArqService.getJSONObject("catalogs").getJSONObject("catalogos"));
        Session.getInstance(SuiteApp.appContext).setAuthenticationJson(autenticacionJson);


        if (!estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)) {
            if (!Tools.isEmptyOrNull(catTelefonicas))
                Session.getInstance(SuiteApp.appContext).saveCatalogoCompaniasTelefonicas(catTelefonicas);
        }

        if (estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)) {
            //TODO FALTA DEFINIR LA LISTA DE PROMOCIONES
            final JSONArray lp = parserArqService.optJSONArray("campaings");
            final JSONArray lpICL = parserArqService.optJSONArray("offers");
            if (null != lp) {
                candidatoPromociones = "SI";
            } else {
                candidatoPromociones = "";
            }
            if (candidatoPromociones.compareTo("SI") == 0) {

                JSONObject camps = new JSONObject();
                JSONArray campArr = new JSONArray();
                for (int i = 0; i < lp.length(); i++) {
                    JSONObject camp = new JSONObject();
                    JSONObject ant = lp.getJSONObject(i);
                    camp.put("cveCamp", ant.opt("id"));
                    camp.put("desOferta", ant.opt("actionLineDescription"));
                    camp.put("monto", ant.getJSONArray("offers").getJSONObject(0).getJSONObject("amountOffer").opt("amount"));
                    camp.put("carrusel", "SI");
                    campArr.put(camp);
                }
                camps.put("campanias", campArr);

                jSonPromociones = camps.toString();
                Session.getInstance(SuiteApp.appContext).parsePromocionesJson(jSonPromociones);
            } else {
                String listaPromociones = null;//get lista promociones del json
                Session.getInstance(SuiteApp.appContext).parsePromocionesJson(listaPromociones);
                listaPromocionesJSON = parseListaPromociones(listaPromociones);
            }
            contratoBBVAlink = "FALSE";
            String stringMarcaTendero = null;//obtine marca tendero
            marcaTendero = parseMarcaTendero(stringMarcaTendero);
        }

        ////////////////////SPEI OPERATIVA DIARIA/////////////////////////////////////////

        if (estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)) {
            Session.setListA(new ArrayList<Account>());
            Session.setListB(new ArrayList<Account>());
            parseCs(accounts);


        }

        if (estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)) {
            String jsonhs = parserArqService.getJSONObject("catalogs").getJSONObject("catalogos").optString("catalogoHS");
            if (jsonhs != null) {
                hsJson = "{\"horariosOperacion\":" + jsonhs + ",\"version\":\"" + hsVersion + "\"}";
            } else {
                hsJson = null;
            }
            if (!Tools.isEmptyOrNull(hsJson))
                Session.getInstance(SuiteApp.appContext).saveCatalogoHorasServicio(hsJson);

            if (Server.ALLOW_LOG) Log.e("switch value", String.valueOf(Session.getSwitchSPEI()));
        }


        //////////////////////END SPEI OPERATIVA DIARIA./////////////////////////////////


        double limite;
        if (!Constants.SOBREESCRIBIR_LO)
            limite = Double.parseDouble(limiteOperacion) / 100.0;
        else
            limite = Constants.LO_SOBREESCRITO;
        Autenticacion.getInstance().setLimiteOperacion(limite);

    }

    private String switchSpey;

    private void parseCs(Account[] accounts) {
        StringBuilder strinjson = new StringBuilder();
        for (Account ac : accounts) {
            strinjson.append("{\"numeroTarjeta\":\"").append(ac.getNumber())
                    .append("\",\"saldo\":\"").append(ac.getBalance())
                    .append("\",\"visible\":\"").append((ac.isVisible() ? "SI" : "NO"))
                    .append("\",\"moneda\":\"MX\",\"tipoCuenta\":\"").append(ac.getType())
                    .append("\",\"concepto\":\"\",\"alias\":\"").append(ac.getAlias())
                    .append("\",\"celularAsociado\":\"").append(ac.getCelularAsociado() == null ? "" : ac.getCelularAsociado())
                    .append("\",\"codigoCompania\":\"").append(ac.getCodigoCompania() == null ? "" : ac.getCodigoCompania())
                    .append("\",\"descripcionCompania\":\"").append(ac.getDescripcionCompania() == null ? "" : ac.getDescripcionCompania())
                    .append("\",\"fechaUltimaModificacion\":\"").append(ac.getFechaUltimaModificacion() == null ? "" : ac.getFechaUltimaModificacion())
                    .append("\",\"indicadorSPEI\":\"").append(ac.getIndicadorSPEI()).append("\"},");
        }
        strinjson = strinjson.deleteCharAt(strinjson.length() - 1);
        csJson = "{\"cuentas\":[" + strinjson + "],\"switch\":\"" + (switchSpey == null || "".equals(switchSpey) ? "FALSE" : switchSpey) + "\"}";
        if (!Tools.isEmptyOrNull(csJson))
            Session.getInstance(SuiteApp.appContext).parserCS(csJson);
    }

    String tmVercion, hsVersion;

    private void fillVesionCat(JSONObject versioncar) throws JSONException {
        //versioncar.getString("verCatalogoAutent");
        JSONObject versiones = (JSONObject) versioncar.getJSONArray("versiones").get(0);
        catalogVersions[0] = versiones.getString("verCatalogo1");
        catalogVersions[1] = versiones.getString("verCatalogo4");
        catalogVersions[2] = versiones.getString("verCatalogo5");
        catalogVersions[4] = versiones.getString("verCatalogoTA");
        catalogVersions[5] = versiones.getString("verCatalogoDM");
        catalogVersions[6] = versiones.getString("verCatalogoSV");
        catalogVersions[7] = versiones.getString("verCatalogoMS");
        tmVercion = versiones.getString("verCatalogoTM");
        hsVersion = versiones.getString("verCatalogoHS");
        catalogVersions[3] = versiones.getString("verCatalogo8");
        limiteOperacion = versiones.getString("LO");


    }

    private void parseCatalogoSV(JSONObject catalogos) throws JSONException, IOException, ParsingException {
        JSONArray catalogo1 = catalogos.optJSONArray("catalogoSV");
        if (catalogo1 == null || catalogo1.length() < 1) {
            return;
        }
        StringBuilder catstring = new StringBuilder();
        int cantidad = 0;
        for (int i = 0; i < catalogo1.length(); i++) {
            JSONObject catalogojson = catalogo1.getJSONObject(i);
            catstring.append("*CI").append(catalogojson.getString("convenioCIE"))
                    .append("*MG").append(catalogojson.getString("imagen"))
                    .append("*NM").append(catalogojson.getString("nombre"))
                    .append("*OR").append(catalogojson.getString("orden"));
            cantidad++;
        }
        StringReader reader = new StringReader("VE" + catalogVersions[6] + "*OC" + cantidad + "" + catstring.toString());
        Parser parser = new Parser(reader);
        catalogoConvenios = parseConvenioCatalog(parser);
    }

    private void parseCatalogoMS(JSONObject catalogos) throws JSONException, IOException, ParsingException {
        JSONArray catalogo1 = catalogos.optJSONArray("catalogoMS");
        if (catalogo1 == null || catalogo1.length() < 1) {
            return;
        }
        StringBuilder catstring = new StringBuilder();
        int cantidad = 0;
        for (int i = 0; i < catalogo1.length(); i++) {
            JSONObject catalogojson = catalogo1.getJSONObject(i);
            catstring.append("*OA").append(catalogojson.getString("nombre"))
                    .append("*MG").append(catalogojson.getString("imagen"))
                    .append("*CV").append(catalogojson.getString("clave"))
                    .append("*OR").append(catalogojson.getString("orden"));
            cantidad++;
        }
        StringReader reader = new StringReader("VE" + catalogVersions[7] + "*OC" + cantidad + "" + catstring.toString());
        Parser parser = new Parser(reader);
        catalogoMantenimientoSPEI = parseSpeiCatalog(parser);
    }

    private void parseCatalogoTM(JSONObject catalogos) throws JSONException, IOException, ParsingException {
        // *TM{"telefonicasMantenimiento":[{"companiaCelular":"TELCEL","nombreImagen":"telcel.png"},{"companiaCelular":"MOVISTAR","nombreImagen":"movistar.png"},{"companiaCelular":"IUSACELL","nombreImagen":"iusacell.png"},{"companiaCelular":"UNEFON","nombreImagen":"unefon.png"}],"versionCatTelefonico":"6"}
        //"catalogoTM":[{"companiaCelular":"TELCEL","nombreImagen":"telcel.png","orden":"1"},{"companiaCelular":"MOVISTAR","nombreImagen":"movistar.png","orden":"2"},{"companiaCelular":"IUSACELL","nombreImagen":"iusacell.png","orden":"3"},{"companiaCelular":"UNEFON","nombreImagen":"unefon.png","orden":"4"},{"companiaCelular":"NEXTEL","nombreImagen":"nextel.png","orden":"5"}],

        JSONArray catalogo1 = catalogos.optJSONArray("catalogoTM");
        if (catalogo1 == null || catalogo1.length() < 1) {
            return;
        }
        StringBuilder catstring = new StringBuilder();
        for (int i = 0; i < catalogo1.length(); i++) {
            JSONObject catalogojson = catalogo1.getJSONObject(i);
            catstring.append("{\"companiaCelular\":\"")
                    .append(catalogojson.getString("companiaCelular"))
                    .append("\",").append("\"nombreImagen\":\"").append(catalogojson.getString("nombreImagen")).append("\"},");
        }
        catstring = catstring.deleteCharAt(catstring.length() - 1);
        catstring = new StringBuilder("{\"telefonicasMantenimiento\":[" + catstring.toString() + "],\"versionCatTelefonico\":\"" + tmVercion + "\"}");
        catTelefonicas = catstring.toString();
    }

    private void parseCatalogoDM(JSONObject catalogos) throws JSONException, IOException, ParsingException {
        JSONArray catalogo1 = catalogos.optJSONArray("catalogoDM");
        if (catalogo1 == null || catalogo1.length() < 1) {
            return;
        }
        StringBuilder catstring = new StringBuilder();
        int cantidad = 0;
        for (int i = 0; i < catalogo1.length(); i++) {
            JSONObject catalogojson = catalogo1.getJSONObject(i);
            catstring.append("*OA").append(catalogojson.getString("nombre"))
                    .append("*MG").append(catalogojson.getString("imagen"))
                    .append("*CV").append(catalogojson.getString("clave"))
                    .append("*IM").append(catalogojson.getString("importes"))
                    .append("*OR").append(catalogojson.getString("orden"));
            cantidad++;
        }
        StringReader reader = new StringReader("VE" + catalogVersions[5] + "*OC" + cantidad + "" + catstring.toString());
        Parser parser = new Parser(reader);
        catalogoDineroMovil = parseCompanyCatalog(parser);

    }

    private void parseCatalogoTA(JSONObject catalogos) throws JSONException, IOException, ParsingException {
        JSONArray catalogo1 = catalogos.optJSONArray("catalogoTA");
        if (catalogo1 == null || catalogo1.length() < 1) {
            return;
        }
        StringBuilder catstring = new StringBuilder();
        int cantidad = 0;
        for (int i = 0; i < catalogo1.length(); i++) {
            JSONObject catalogojson = catalogo1.getJSONObject(i);
            catstring.append("*OA").append(catalogojson.getString("nombre"))
                    .append("*MG").append(catalogojson.getString("imagen"))
                    .append("*CV").append(catalogojson.getString("clave"))
                    .append("*IM").append(catalogojson.getString("importes"))
                    .append("*OR").append(catalogojson.getString("orden"));
            cantidad++;
        }
        StringReader reader = new StringReader("VE" + catalogVersions[4] + "*OC" + cantidad + "" + catstring.toString());
        Parser parser = new Parser(reader);
        catalogoTA = parseCompanyCatalog(parser);
    }

    private void parseCatalogos1(JSONObject catalogos) throws JSONException, IOException, ParsingException {
        JSONArray catalogo1 = catalogos.optJSONArray("catalogo1");
        if (catalogo1 == null || catalogo1.length() < 1) {
            return;
        }
        StringBuilder catstring = new StringBuilder();
        int cantidad = 0;
        for (int i = 0; i < catalogo1.length(); i++) {
            JSONObject catalogojson = catalogo1.getJSONObject(i);
            catstring.append(catalogojson.getString("valor"));
            catstring.append("*");
            cantidad++;
        }
        catstring = catstring.deleteCharAt(catstring.length() - 1);
        StringReader reader = new StringReader("OC" + cantidad + "*" + catstring.toString());
        Parser parser = new Parser(reader);
        catalogs[0] = parseCatalog(parser);

    }

    private void parseCatalogos4(JSONObject catalogos) throws JSONException, IOException, ParsingException {
        JSONArray catalogo1 = catalogos.optJSONArray("catalogo4");
        if (catalogo1 == null || catalogo1.length() < 1) {
            return;
        }
        StringBuilder catstring = new StringBuilder();
        int cantidad = 0;
        for (int i = 0; i < catalogo1.length(); i++) {
            JSONObject catalogojson = catalogo1.getJSONObject(i);
            catstring.append(catalogojson.getString("valor"));
            catstring.append("*");
            cantidad++;
        }
        catstring = catstring.deleteCharAt(catstring.length() - 1);
        StringReader reader = new StringReader("OC" + cantidad + "*" + catstring.toString());
        Parser parser = new Parser(reader);
        catalogs[1] = parseCatalog(parser);
    }

    private void parseCatalogos5(JSONObject catalogos) throws JSONException, IOException, ParsingException {
        JSONArray catalogo1 = catalogos.optJSONArray("catalogo5");
        if (catalogo1 == null || catalogo1.length() < 1) {
            return;
        }
        StringBuilder catstring = new StringBuilder();
        int cantidad = 0;
        for (int i = 0; i < catalogo1.length(); i++) {
            JSONObject catalogojson = catalogo1.getJSONObject(i);
            catstring.append(catalogojson.getString("valor"));
            catstring.append("*");
            cantidad++;
        }
        catstring = catstring.deleteCharAt(catstring.length() - 1);
        StringReader reader = new StringReader("OC" + cantidad + "*" + catstring.toString());
        Parser parser = new Parser(reader);
        catalogs[2] = parseCatalog(parser);
    }

    private void parseCatalogos8(JSONObject catalogos) throws JSONException, IOException, ParsingException {
        JSONArray catalogo1 = catalogos.optJSONArray("catalogo8");
        if (catalogo1 == null || catalogo1.length() < 1) {
            return;
        }
        StringBuilder catstring = new StringBuilder();
        int cantidad = 0;
        for (int i = 0; i < catalogo1.length(); i++) {
            JSONObject catalogojson = catalogo1.getJSONObject(i);
            catstring.append(catalogojson.getString("valor"));
            catstring.append("*");
            cantidad++;
        }
        catstring = catstring.deleteCharAt(catstring.length() - 1);
        StringReader reader = new StringReader("OC" + cantidad + "*" + catstring.toString());
        Parser parser = new Parser(reader);
        catalogs[3] = parseCatalog(parser);
    }


    private String getcatalogoAutenticacion(JSONObject catalogos) throws JSONException {
        String au = null;
        if (catalogos.optJSONObject("autenticacion") != null) {
            String recortado = catalogos.getJSONObject("autenticacion").getJSONArray("recortado").toString();
            recortado = "{\"recortado\":" + recortado + "},";
            String basic = catalogos.getJSONObject("autenticacion").getJSONArray("basico").toString();
            basic = "{\"basico\":" + basic + "},";
            String avanzado = catalogos.getJSONObject("autenticacion").getJSONArray("avanzado").toString();
            avanzado = "{\"avanzado\":" + avanzado + "}";
            au = "{\"perfil\":[" + recortado + basic + avanzado + "],\"version\":\"31\"}";

        }
        return au;
    }

    private Account[] parseAccounts(final JSONArray accountsJo, final JSONObject speyjson) throws JSONException {
        List<Account> listAcount = new ArrayList<Account>();
        JSONObject balance;
        JSONObject avalibelBalance = null;
        for (int i = 0; i < accountsJo.length(); i++) {
            Account acount = new Account();
            JSONObject cuenta = accountsJo.getJSONObject(i);//product
            JSONObject product = cuenta.getJSONObject("product");
            JSONObject tipo = getTipo(product, acount);
//            String alias=tipo.getString("shortName");
            if(tipo==null){
                continue;
            }

            acount.setAlias("");
            JSONObject pb = tipo.getJSONObject("productBase");
            if (null != pb) {
                String tipoString = pb.getString("type");
                acount.setType(tipoString);
                balance = pb.getJSONObject("balance");
                if (null != balance) {
                    if ("AH".equals(tipoString) || "LI".equals(tipoString) || "CH".equals(tipoString)) {
                        avalibelBalance = balance.getJSONObject("pendingBalance");
                    } else if ("TC".equals(tipoString)) {
                        avalibelBalance = balance.getJSONObject("cuttingBalance");
                    } else if ("IN".equals(tipoString)) {
                        avalibelBalance = balance.getJSONObject("availableBalance");
                    }
                    if (!("PT".equals(tipoString)) && null != avalibelBalance) {
                        acount.setBalance(avalibelBalance.getDouble("amount"));
                    }
                }
            }
            acount.setVisible(false);
            String visible = pb.optString("shaftAccountIndicator", null);
            if ("O".equals(visible)) {
                acount.setVisible(true);
            }
            //todas son mx
            acount.setDate(serverDate);
            acount.setCurrency("MX");
            acount.setIndicadorSPEI("FALSE");

            final JSONArray speyAcounts = speyjson.optJSONArray("cuentas");
            if (null != speyjson) {
                switchSpey = speyjson.optString("switch");
                if (null != speyAcounts) {//completa con lo de cuentas spei
                    for (int y = 0; y < speyAcounts.length(); y++) {
                        final JSONObject speyAcount = speyAcounts.getJSONObject(y);
                        if (null != speyAcount) {
                            final String ncSpey = speyAcount.optString("numeroCuenta");
                            if (acount.getNumber().equals(ncSpey)) {//se complementa con lo de spey
                                acount.setCelularAsociado(speyAcount.getString("telefono"));
                                acount.setCodigoCompania(speyAcount.getString("codigoCompania"));
                                acount.setDescripcionCompania(speyAcount.getString("descripcionCompania"));
                                acount.setFechaUltimaModificacion(speyAcount.getString("fechaUltimaModificacion"));
                                acount.setIndicadorSPEI(speyAcount.getString("indicadorSPEI"));
                            }
                        }
                    }
                }
            }
            listAcount.add(acount);
        }
        return listAcount.toArray(new Account[listAcount.size()]);
    }

    /**
     * obtine el tipo del producto y con base a esto setea el numero de cuenta
     *
     * @param product
     * @param acount
     * @return
     * @throws JSONException
     */
    private JSONObject getTipo(final JSONObject product, final Account acount) throws JSONException {// solo se dejan las cuentas indicadas en el P026
        final String[] tipoCuentas = new String[]{"checkAccount", "savingAccount", "creditCard", "mobileAccount", "investment", "libretonAccount", "heritage"};
        JSONObject tipo = null;
        for (final String acountTipe : tipoCuentas) {
            tipo = product.optJSONObject(acountTipe);
            if (tipo != null) {
                String number = "";
                if ("mobileAccount".equals(acountTipe)) {
                    number = tipo.optString("cellphoneNumber");
                } else {
                    number = tipo.optString("accountNumber");
                    if (number == null) {
                        number = tipo.optString("cardNumber");
                    }
                } // sustitur tipos
                if ("heritage".equals(acountTipe) || "mobileAccount".equals(acountTipe)) {
                    JSONObject productBase;
                    productBase = tipo.getJSONObject("productBase");
                    if (null != productBase) {
                        productBase.remove("type");
                        if ("heritage".equals(acountTipe)) {
                            productBase.put("type", "PT");
                        } else if ("mobileAccount".equals(acountTipe)) {
                            productBase.put("type", "CE");
                        }
                    }
                }
                acount.setNumber(number);
                break;
            }
        }
        return tipo;
    }

}
