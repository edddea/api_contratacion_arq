package suitebancomercoms.aplicaciones.bmovil.classes.common;

import android.util.Log;

import com.bancomer.base.SuiteApp;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;

import bancomer.api.common.model.OperacionAutenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public final class CatalogoAutenticacionFileManager {

	/**
	 * Nombre del archivo de catalogo.
	 */
	private static final String CATALOGO_AUTENTICACION_FILE_NAME = "CatalogoAutenticacion.prop";
	
	/**
	 * Manejador del archivo de catalogo.
	 */
	private File file;
	
	/**
	 * La instancia de la clase.
	 */
	private static CatalogoAutenticacionFileManager manager = null;
	
	/**
	 * @return La instancia de la clase.
	 */
	public static CatalogoAutenticacionFileManager getCurrent() {
		if(null == manager){
			manager = new CatalogoAutenticacionFileManager();
		}
		return manager;
	}
	
	/**
	 * Inicializa el administrador de propiedades. 
	 */
	private CatalogoAutenticacionFileManager() {
		this.file = new File(SuiteApp.appContext.getFilesDir(), CATALOGO_AUTENTICACION_FILE_NAME);
		
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch(IOException ioEx) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error al crear el archivo CatalogoAutenticacion.", ioEx);
				return;
			}
		}
	}
	
	/**
	 * Guarda el archivo de catálogos.
	 */
	public void guardaCatalogoAutenticacion() {
		ObjectOutputStream os = null;
		try {
			os = new ObjectOutputStream(new FileOutputStream(this.file));
			os.writeObject(Autenticacion.getInstance().getVersion());
			os.writeObject(Autenticacion.getInstance().getBasico());
			os.writeObject(Autenticacion.getInstance().getAvanzado());
			os.writeObject(Autenticacion.getInstance().getRecortado());
			if(ServerCommons.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Catalogos de Autenticación guardados.");
		} catch (Exception e) {
			if(ServerCommons.ALLOW_LOG)Log.e(this.getClass().getSimpleName(), "Error en guardaCatalogoAutenticacion.", e);
		} finally{
			try {
				if(null != os){
					os.close();
				}
			} catch (Exception e) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error al cerrar en guardaCatalogoAutenticacion.", e);
			}
		}
	}
	
	/**
	 * Carga el archivo de catalogos.
	 */
	@SuppressWarnings("unchecked")
	public void cargaCatalogoAutenticacion(){
		ObjectInputStream is = null;
		try {
			is = new ObjectInputStream(new FileInputStream(file));
			Autenticacion.getInstance().setVersion((String) is.readObject());
			Autenticacion.getInstance().setBasico((ArrayList<OperacionAutenticacion>) is.readObject());
			Autenticacion.getInstance().setAvanzado((ArrayList<OperacionAutenticacion> ) is.readObject());
			Autenticacion.getInstance().setRecortado((ArrayList<OperacionAutenticacion> ) is.readObject());
			Tools.writeLogi(this.getClass().getSimpleName(), "Catalogos de Autenticación cargados.", ServerCommons.ALLOW_LOG);
		} catch (EOFException e) {
			Tools.writeLogi(this.getClass().getSimpleName(), "El archivo CatalogoAutenticacion esta vacio.", ServerCommons.ALLOW_LOG);
		} catch (StreamCorruptedException e) {
			Tools.writeLogi(this.getClass().getSimpleName(), "El archivo CatalogoAutenticacion es de una version antigua e ilegible.", ServerCommons.ALLOW_LOG);
		} catch (Exception e) {
			Tools.writeLogi(this.getClass().getSimpleName(), "Error en cargaCatalogoAutenticacion.", ServerCommons.ALLOW_LOG);
		} finally {
			try{
				if(null != is){
					is.close();
				}
			} catch (Exception e) {
				Tools.writeLogi(this.getClass().getSimpleName(), "Error al cerrar en cargaCatalogoAutenticacion.", ServerCommons.ALLOW_LOG);
			}
		}		
	}
	
	/**
	 * Lee la version del archivo de catalogos.
	 */
	public String leerVersionArchivoCatalogoAutenticacion(){
		ObjectInputStream is = null;
		String version = "0";
		try {
			is = new ObjectInputStream(new FileInputStream(file));
			version = (String) is.readObject();
		} catch (EOFException e) {
			Tools.writeLogi(this.getClass().getSimpleName(), "El archivo CatalogoAutenticacion esta vacio.", ServerCommons.ALLOW_LOG);
		} catch (StreamCorruptedException e) {
			Tools.writeLogi(this.getClass().getSimpleName(), "El archivo CatalogoAutenticacion es de una version antigua e ilegible.", ServerCommons.ALLOW_LOG);
		} catch (Exception e) {
			Tools.writeLoge(this.getClass().getSimpleName(), "Error en leerVersionArchivoCatalogoAutenticacion.", ServerCommons.ALLOW_LOG);
		} finally {
			try{
				if(null != is){
					is.close();
				}
			} catch (Exception e) {
				Tools.writeLoge(this.getClass().getSimpleName(), "Error al cerrar en cargaCatalogoAutenticacion.", ServerCommons.ALLOW_LOG);
			}
		}
		return version;
	}
	
	/**
	 * Vacía el archivo de catalogos.
	 */
	public void vaciaArchivoCatalogoAutenticacion() {
		ObjectOutputStream os = null;
		try {
			os = new ObjectOutputStream(new FileOutputStream(this.file));
			if(ServerCommons.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Catalogos de Autenticación vaciado.");
		} catch (Exception e) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error en vaciaArchivoCatalogoAutenticacion.", e);
		} finally{
			try {
				if(null != os){
					os.close();
				}
			} catch (Exception e) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error al cerrar en vaciaArchivoCatalogoAutenticacion.", e);
			}
		}
		
	}
}
