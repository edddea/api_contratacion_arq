package suitebancomercoms.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ConsultaTarjetaContratacionData implements ParsingHandler {
	/**
	 * Identificador
	 */
	private static final long serialVersionUID = -6969030739631399018L;

	// #region Variables.
	/**
	 * Estado de la operación
	 */

	private String estado;

	/**
	 * Descripción
	 */

	private String descripcion;

	/**
	 * El número del cliente.
	 */
	@Deprecated
	private String numeroCliente;

	/**
	 * El numero de cuenta del cliente.
	 */
	@Deprecated
	private String numeroCuenta;

	/**
	 * Tipo de tarjeta ingresada
	 */
	private String tipoTarjeta;

	/**
	 * Indica si es la unica tarjeta del cliente
	 */
	@Deprecated
	private String monoProducto;

	/**
	 * Validacion de las alertas
	 */
	@Deprecated
	private String validacionAlertas;

	/**
	 * Numero telefonico asociado a alertas
	 */
	@Deprecated
	private String numeroAlertas;

	/**
	 * Compa�ia telefonica asociada a Alertas
	 */
	@Deprecated
	private String companiaAlertas;

	/**
	 * Tipo de instrumento contrtado.
	 */
	private String tipoInstrumento;

	/**
	 * El estatus del instrumento.
	 */
	private String estatusInstrumento;

	/**
	 * Tipo de persona.
	 */
	private String tipoPersona;

	/**
	 * La fecha de contratación.
	 */
	private String fechaContratacion;

	/**
	 * La fecha de modificación.
	 */
	private String fechaModificacion;

	/**
	 * Indicador de Softtoken y los valores que puede tomar son: N, R, S, X (Token nuevo, Reactivación, Sustitución, No existe solicitud)
	 *
	 */
	private String indicadorContrato;

	/**
	 * Número de serie del Softtoken actual para poder borrar un token del MDA en el flujo de reactivación
	 *
	 */

	private String tokenSerial;

	/**
	 * Switch Enrolamiento
	 *
	 */

	private Boolean enrollSwitch;

	/**
	 *
	 * Este campo nos describirá si el cliente tiene o no dispositivo físico
	 */

	private Boolean useDevice;

	/**
	 *
	 * Nos indicará el estatus en el que se encuentra el dispositivo del cliente
	 * puede tomar los valores: A1, BL ó vacío
	 */

	private String deviceStatus;

	/**
	 *
	 * Nos indicara el tipo de tarjeta que es física o digital
	 */

	private Boolean isDigitalAccount;
	// #endregion

	// #region Setters/Getters

	public String getEstado() {
		return estado;
	}

	public void setEstado(final String estado) {
		this.estado = estado;
	}

	public String getTipoTarjeta() {
		return tipoTarjeta;
	}

	public void setTipoTarjeta(final String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	public String getMonoProducto() {
		return monoProducto;
	}

	public void setMonoProducto(final String monoProducto) {
		this.monoProducto = monoProducto;
	}

	public String getValidacionAlertas() {
		return validacionAlertas;
	}

	public void setValidacionAlertas(final String validacionAlertas) {
		this.validacionAlertas = validacionAlertas;
	}

	public String getNumeroAlertas() {
		return numeroAlertas;
	}

	public void setNumeroAlertas(final String numeroAlertas) {
		this.numeroAlertas = numeroAlertas;
	}

	public String getCompaniaAlertas() {
		return companiaAlertas;
	}

	public void setCompaniaAlertas(final String companiaAlertas) {
		this.companiaAlertas = companiaAlertas;
	}
	/**
	 * @return El número del cliente.
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}

	/**
	 * @param numeroCliente
	 *            El número del cliente a establecer.
	 */
	public void setNumeroCliente(final String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	/**
	 * @return El número de cuenta del cliente.
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	/**
	 * @param numeroCuenta
	 *            El número de cuenta del cliente a establecer.
	 */
	public void setNumeroCuenta(final String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	/**
	 * @return Tipo de instrumento contrtado.
	 */
	public String getTipoInstrumento() {
		return tipoInstrumento;
	}

	/**
	 * @param tipoInstrumento
	 *            Tipo de instrumento contrtado a establecer.
	 */
	public void setTipoInstrumento(final String tipoInstrumento) {
		this.tipoInstrumento = tipoInstrumento;
	}

	/**
	 * @return El estatus del instrumento.
	 */
	public String getEstatusInstrumento() {
		return estatusInstrumento;
	}

	/**
	 * @param estatusInstrumento
	 *            El estatus del instrumento a establecer.
	 */
	public void setEstatusInstrumento(final String estatusInstrumento) {
		this.estatusInstrumento = estatusInstrumento;
	}

	/**
	 * @return La fecha de contratación.
	 */
	public String getFechaContratacion() {
		return fechaContratacion;
	}

	/**
	 * @param fechaContratacion
	 *            La fecha de contratación a establecer.
	 */
	public void setFechaContratacion(final String fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}

	/**
	 * @return La fecha de modificación.
	 */
	public String getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion
	 *            La fecha de modificación a establecer.
	 */
	public void setFechaModificacion(final String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return Tipo de persona.
	 */
	public String getTipoPersona() {
		return tipoPersona;
	}

	/**
	 * @param tipoPersona
	 *            Tipo de persona.
	 */
	public void setTipoPersona(final String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	
	// #endregion


	public String getIndicadorContrato() {
		return indicadorContrato;
	}

	public void setIndicadorContrato(String indicadorContrato) {
		this.indicadorContrato = indicadorContrato;
	}

	public String getTokenSerial() {
		return tokenSerial;
	}

	public void setTokenSerial(String tokenSerial) {
		this.tokenSerial = tokenSerial;
	}

	public Boolean getEnrollSwitch() {
		return enrollSwitch;
	}

	public void setEnrollSwitch(Boolean enrollSwitch) {
		this.enrollSwitch = enrollSwitch;
	}

	public Boolean getUseDevice() {
		return useDevice;
	}

	public void setUseDevice(Boolean useDevice) {
		this.useDevice = useDevice;
	}

	public String getDeviceStatus() {
		return deviceStatus;
	}

	public void setDeviceStatus(String deviceStatus) {
		this.deviceStatus = deviceStatus;
	}

	public Boolean getIsDigitalAccount() {
		return isDigitalAccount;
	}

	public void setIsDigitalAccount(Boolean isDigitalAccount) {
		this.isDigitalAccount = isDigitalAccount;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	/**
	 * Crea una nueva instancia vacia de la clase.
	 */
	public ConsultaTarjetaContratacionData() {
		estado = null;
		descripcion = null;
		//numeroCliente = null;
		//numeroCuenta = null;
		tipoTarjeta = null;
		//monoProducto = null;
		//validacionAlertas = null;
		//numeroAlertas = null;
		//companiaAlertas = null;
		tipoInstrumento = null;
		estatusInstrumento = null;
		fechaContratacion = null;
		fechaModificacion = null;
		tipoPersona = null;
		indicadorContrato = null;
		tokenSerial = null;
		useDevice = false;
		deviceStatus = null;
		enrollSwitch = false;
		isDigitalAccount =  false;
	}

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		estado = null;
		descripcion = null;
		//numeroCliente = null;
		//numeroCuenta = null;
		tipoTarjeta = null;
		//monoProducto = null;
		//validacionAlertas = null;
		//numeroAlertas = null;
		//companiaAlertas = null;
		tipoInstrumento = null;
		estatusInstrumento = null;
		fechaContratacion = null;
		fechaModificacion = null;
		tipoPersona = null;
		indicadorContrato = null;
		tokenSerial = null;
		useDevice = false;
		deviceStatus = null;
		enrollSwitch = false;
		isDigitalAccount =  false;
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		estado = parser.parseNextValue("estado");
		descripcion = parser.parseNextValue("description");
		//numeroCliente = parser.parseNextValue("numeroCliente");
		//numeroCuenta = parser.parseNextValue("numeroCuenta");
		tipoTarjeta = parser.parseNextValue("cardType");
		//monoProducto = parser.parseNextValue("monoProducto");
		//validacionAlertas = parser.parseNexstValue("validacionAlertas");
		//numeroAlertas = parser.parseNextValue("numeroAlertas");
		//companiaAlertas = parser.parseNextValue("companiaAlertas");
		tipoInstrumento = parser.parseNextValue("instrumentType");
		estatusInstrumento = parser.parseNextValue("instrumentState");
		tipoPersona = parser.parseNextValue("personType");
		fechaModificacion = parser.parseNextValue("updateDate");
		fechaContratacion = parser.parseNextValue("contractDate");
		indicadorContrato = parser.parseNextValue("contractIndicator");
		tokenSerial = parser.parseNextValue("tokenSerial");
		enrollSwitch = Boolean.getBoolean(parser.parseNextValue("enrollSwitch"));
		useDevice = Boolean.getBoolean(parser.parseNextValue("useDevise"));
		deviceStatus = parser.parseNextValue("deviceStatus");
		isDigitalAccount =  Boolean.getBoolean(parser.parseNextValue("digitalAccount"));

	}
}
