/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomercoms.aplicaciones.bmovil.classes.common;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.bancomer.base.SuiteApp;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.regex.Pattern;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import orgcoms.bouncycastle.crypto.Digest;
import orgcoms.bouncycastle.crypto.digests.MD5Digest;
import orgcoms.bouncycastle.util.encoders.Hex;
import suitebancomer.aplicaciones.bmovil.classes.common.ToolsCommons;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Tools include some utility static methods that can be used by different.
 * classes of the application
 *
 */
public class Tools extends ToolsCommons {
	
	public final static int TAM_FRAGMENTO_TEXTO = 1000;

	/**
     * Obtiene el monto numï¿½rico desde la cadena devuelta por el servidor
     * @param amount el monto devuelto por el servidor
     * @return el monto como una variable de tipo doble
     */
	public static double getDoubleAmountFromServerString(final String amount) {
		double dResult = 0d;
        if (amount != null) {
            final int size = amount.length();
            switch (size) {
            case 0:
                dResult = 0d;
                break;
            case 1:
            case 2:
                dResult = Double.parseDouble(amount) / 100d;
                break;
            default:
                final String intPart = amount.substring(0, size - 2);
                final String decPart = amount.substring(size - 2);
                final double intDouble = Double.parseDouble(intPart);
                final double decDouble = Double.parseDouble(decPart)/100d;
                dResult = intDouble + decDouble;
                break;
            }
        }
		
		return dResult;
	}
	
	 /**
     * Formats aa amount received from server.
     * @param amount the input amount
     * @param negative true if the amount is negative
     * @return the output amount
     */
    public static String formatAmount(String amount, final boolean negative) {
        final StringBuffer result = new StringBuffer();
        if (amount != null) {
            result.append('$');
            if (negative) {
            	if(amount.startsWith("-"))
            		amount = amount.substring(1);
            	result.append('-');
            }
            final int size = amount.length();
            switch (size) {
            case 0:
                result.append("0.00");
                break;
            case 1:
                result.append("0.0");
                result.append(amount);
                break;
            case 2:
                result.append("0.");
                result.append(amount);
                break;
            default:
                final String intPart = amount.substring(0, size - 2);
                final String decPart = amount.substring(size - 2);
                result.append(formatPositiveIntegerAmount(intPart));
                result.append('.');
                result.append(decPart);
                break;
            }
        }
        return result.toString();
    }

    /**
     * Format a decimal amount wihtout decimal point, reserving the last two
     * digits for the decimals. Examples:
     * 12 will return 1200
     * 12.3 will return 1230
     * 12.35 will return 1235
     * @param amount decimal amount to format
     * @return the formatted amount
     */
    public static String formatAmountForServer(String amount) {
        if (amount == null) {
            return null;
        }
        
        amount = amount.replace(",", "");
        String integer = "";
        String decimal = "";
        final char separator = '.'; //Locale.DECIMAL_SEPARATOR; PENDIENTE
        final int index = amount.lastIndexOf((int) separator);
        if (index < 0) {
            // append two digits to the amount
            integer = amount;
            decimal = "00";
        } else if (index == 0) {
           integer = "0";
           decimal = amount.substring(1);
        } else {
            integer = amount.substring(0, index);
            if (index == amount.length() - 1) {
                decimal = "00";
            } else {
                decimal = amount.substring(index + 1);
            }
        }
        // format the decimal to 2 digits
        if (decimal.length() < 2) {
            for (int i = decimal.length(); i < 2; i++) {
                decimal += "0";
            }
        } else {
            decimal = decimal.substring(0, 2);
        }
        return integer + decimal;
    }

	/**
	  * format an amount entered by the user to show the decimal and thousand separators.
	  * Examples:
	  *  22 returns 22.00
	  * 22.3 returns 22.30
	  * 1230 returns 1,230.00
	  * 12345677.8 returns 1,234,677.80
	  * @param amount the amount as entered by the user
	  * @return the formatted amount
	 */
	public static String formatUserAmount(final String amount) {
		if ((amount == null) || (amount.length() == 0)) {
			return "0.00";
		}
		final StringBuffer decimal = new StringBuffer();
		StringBuffer integer = new StringBuffer();

		// format the decimal part
		final int decimalPos = amount.indexOf(".");
		if ((decimalPos < 0) || (decimalPos == amount.length() - 1)) {
			// there is not decimal point or it is the last character of the string
			decimal.append("00");
		} else {
			// get the decimal positions
			final String auxDecimal = amount.substring(decimalPos + 1);
			if (auxDecimal.length() >= 2) {
				decimal.append(auxDecimal.substring(0, 2));
			} else {
				// add 0s to complete 2 decimals
				decimal.append(auxDecimal);
				for (int i = decimal.length(); i < 2; i++) {
					decimal.append("0");
				}
			}
		}

		// format the integer part
		String auxInteger = amount;
		if (decimalPos == 0) {
			auxInteger = "0";
		} else if (decimalPos > 0) {
			auxInteger = amount.substring(0, decimalPos);
		}
		int curPos;
		for (int i = auxInteger.length(); i > 0; i = i - 3) {
			curPos = i - 3;
			if (curPos < 0) {
			    curPos = 0;
			}
			if (curPos > 0) {
				integer = new StringBuffer(",").
						  append(auxInteger.substring(curPos, i)).
						  		 append(integer);
			} else {
				integer = new StringBuffer(auxInteger.
							   substring(0, i)).append(integer);
			}
		}
		final StringBuffer result = integer.append(".").append(decimal.toString());
		return result.toString();
	}
	
	   /**
     * Format a decimal amount, reserving the last two digits for the decimals.
     * Examples:
     * 1200 will return 12.00
     * 1230 will return 12.30
     * 12.35 will return 1235
     * @param amount  amount to format
     * @return the formatted amount
     */
    public static String formatAmountFromServer(final String amount) {

        if (amount.length() == 0) {
            return "0.00";
        } else if (amount.length() == 1) {
           return "0.0" + amount;
        } else if (amount.length() == 2) {
            return "0." + amount;
        } else {
           return formatPositiveIntegerAmount(amount.substring(0, amount.length() - 2))
           		 + "." + amount.substring(amount.length() - 2);
        }

    } 

    /**
     * Formats an amount received from server.
     * @param amount the input amount
     * @return the output amount
     */
    private static String formatPositiveIntegerAmount(final String amount) {
        final StringBuffer result = new StringBuffer();
        if (amount != null) {
            final int size = amount.length();
            int remaining = size % 3;
            if (remaining == 0) {
                remaining = 3;
            }
            int start = 0;
            for (int end = remaining; end <= size; end += 3) {
                result.append(amount.substring(start, end));
                if (end < size) {
                    result.append(',');
                }
                start = end;
            }
        }
        return result.toString();
    }

    /**
     * Formats a date received from server.
     * @param date the input date
     * @return the output date
     */
    public static String formatDate(final String date) {
        final StringBuffer result = new StringBuffer();
        if (date != null) {
            if (date.length() == 8) {
                result.append(date.substring(0, 2));
                result.append('/');
                result.append(date.substring(2, 4));
                result.append('/');
                result.append(date.substring(4));
            } else {
                result.append(date);
            }
        }
        return result.toString();
    }
    
    /**
     * Formats a date received from server in YYYY-MM-DD format.
     * @param date the input date
     * @return the output date
     */
    public static String formatDateTDC(final String date) {
    	String anio = "";
    	String mes ="";
    	String dia="";
    	
        final StringBuffer result = new StringBuffer();
        if (date != null) {
            if (date.length() == 10) {
            	anio=date.substring(0, 4);
            	mes=date.substring(5, 7);
            	dia=date.substring(8);
                result.append(dia);
                result.append('/');
                result.append(mes);
                result.append('/');
                result.append(anio);
            } else {
                result.append(date);
            }
        }
        return result.toString();
    }

    /**
     * Formats a date received from server to format DD/MM.
     * @param date the input date
     * @return the output date
     */
    public static String formatShortDate(final String date) {
        final StringBuffer result = new StringBuffer();
        if (date != null) {
            if (date.length() == 8) {
                result.append(date.substring(0, 2));
                result.append('/');
                result.append(date.substring(2, 4));
            } else {
                result.append(date);
            }
        }
        return result.toString();
    }
    
    /**
     * Formats a date received from server to format DD/MM.
     * @param date the input date
     * @return the output date
     */
    public static String formatInverseDate(final String date) {
        final StringBuffer result = new StringBuffer();
        if (date != null) {
            if (date.length() == 10) {
            	result.append(date.substring(6, 10));
            	result.append('-');
            	result.append(date.substring(3, 5));
            	result.append('-');
            	result.append(date.substring(0, 2));
            } else {
                result.append(date);
            }
        }
        return result.toString();
    }


    /**
     * Mask the username (the telephone number), with a mask character and only.
     * letting visible a certain number of digits
     * @param username the telephone number
     * @return a text containing the fist digits of the number masked with a
     * character, and the last digits in clear
     */
    public static String hideUsername(final String username) {
        String result = "";
        if ((username != null) && (username.length() > Constants.VISIBLE_NUMBER_CHARCOUNT)) {
            final StringBuffer sb = new StringBuffer();
            // mask the first characters of the username with "*" and leave visible the
            // VISIBLE_USERNAME_CHARCOUNT last characters
            for (int i = 0; i < username.length() - Constants.VISIBLE_NUMBER_CHARCOUNT; i++) {
                sb.append("*");
            }
            sb.append(username.substring(username.length() - Constants.VISIBLE_NUMBER_CHARCOUNT));
            result = sb.toString();
        }
        return result;
    }

    /**
     * Hide part of the account number, showing the first digits of the account as
     * an asterisk (*), and only letting visible a certain number of digits. Example:
     * Account 12345678901234567890 would turn into *7890
     * @param account the account number
     * @return a string with the masked account number
     */
    public static String hideAccountNumber(final String account) {
		String result = "";
		if ((account != null) && (account.length() > Constants.VISIBLE_NUMBER_ACCOUNT)) {
			final StringBuffer sb = new StringBuffer("*");
			sb.append(account.substring(account.length() - Constants.VISIBLE_NUMBER_ACCOUNT));
			result = sb.toString();
		} else {
			if (account.length() == Constants.VISIBLE_NUMBER_ACCOUNT) {
				result = account;
			}
		}

		return result;
    }

    /**
     * Hide the decimal part of a number, treated as Strings. Sample:
     * "34.56" would turn into "34".
     * @param number text that represents a decimal number
     * @return the same number as text without decimals.
     */
    public static String hideDecimals(final String number) {
        String result = number;
        if (number != null) {
            final int index = number.indexOf(".");
            if (index > 0) {
                result = number.substring(0, index);
            }
        }
        return result;
    }

    // TODO: PENDING, Must check in real device -- folvera
    /**
     * Try to obtain the IMEI from the telephone.
     * @param applicationContext application context to retrieve phone data
     * @return the IMEI from the telephone, or null if it cannot be obtained
     */
    private static String getImei(final Context applicationContext) {
    	final TelephonyManager telephonyManager =
    		(TelephonyManager) applicationContext.getSystemService(Context.TELEPHONY_SERVICE);
        String imei = telephonyManager.getDeviceId();
    	if (TextUtils.isEmpty(imei)) {
    	   imei = "";
    	}
    	return imei;
    }

    // TODO: PENDING, Must check in real device -- folvera
    /**
     * Try to obtain the IMSI from the telephone SIM card.
     * @param applicationContext application context to retrieve phone data
     * @return the IMSI from the telephone SIM card, or null if it cannot be obtained
     */
    private static String getImsi(final Context applicationContext)  {
    	final TelephonyManager telephonyManager =
    		(TelephonyManager) applicationContext.getSystemService(Context.TELEPHONY_SERVICE);
        final String imsi = telephonyManager.getSubscriberId();
        if (imsi != null) {
            return "misdn:" + imsi;
        } else {
            return null;
        }
    }


    /* FOR REFERENCE ONLY, DELETE BEFORE PRODUCTION SET (folvera)
		android.vm.dexfile=true,
		java.runtime.name=Android Runtime,
		java.runtime.version=0.9,
		java.specification.version=0.9,
		java.specification.name=Dalvik Core Library,
		java.specification.vendor=The Android Project,
		java.vm.name=Dalvik,
		java.vm.version=1.0.1,
		java.vm.vendor=The Android Project,
		java.vm.vendor.url=http://www.android.com/,
		java.vm.specification.name=Dalvik Virtual Machine Specification,
		java.vm.specification.vendor=The Android Project,
		java.vm.specification.version=0.9,
		java.boot.class.path=/system/framework/core.jar:
							 /system/framework/ext.jar:
							 /system/framework/framework.jar:
							 /system/framework/android.policy.jar:
							 /system/framework/services.jar,
		java.class.version=46.0,
		java.class.path=.,
		java.compiler=,
		java.ext.dirs=,
		java.home=/system,
		java.io.tmpdir=/sdcard,
		java.library.path=/system/lib,
		java.vendor=The Android Project,
		java.vendor.url=http://www.android.com/,
		java.version=0,
		file.encoding=UTF-8,
		file.separator=/,
		line.separator=,
		os.arch=OS_ARCH,
		os.name=Linux,
		os.version=2.6.27-00110-g132305e,
		path.separator=:,
		javax.net.ssl.trustStore=/system/etc/security/cacerts.bks,
		user.home=,
		user.dir=/,
		user.name=,
		user.language=en,
		user.region=US
     */
    /**
     * .
     * @return SOMETHING.
     */
    private static String getSystemProperties() {
        final StringBuffer result = new StringBuffer();
        try {
            result.append(System.getProperty("java.runtime.name"));
        } catch (Throwable th) {
            ToolsCommons.writeLoge(Tools.class.getSimpleName(), th.getMessage(), ServerCommons.ALLOW_LOG);
        };
        try {
            result.append(System.getProperty("os.version"));
        } catch (Throwable th) {
            ToolsCommons.writeLoge(Tools.class.getSimpleName(), th.getMessage(), ServerCommons.ALLOW_LOG);
        };
        try {
            result.append(System.getProperty("java.vm.name"));
        } catch (Throwable th) {
            ToolsCommons.writeLoge(Tools.class.getSimpleName(), th.getMessage(), ServerCommons.ALLOW_LOG);
        };
        try {
            result.append(System.getProperty("java.runtime.version"));
        } catch (Throwable th) {
            ToolsCommons.writeLoge(Tools.class.getSimpleName(), th.getMessage(), ServerCommons.ALLOW_LOG);
        };
         try {
            result.append(System.getProperty("java.vm.version"));
        } catch (Throwable th) {
             ToolsCommons.writeLoge(Tools.class.getSimpleName(), th.getMessage(), ServerCommons.ALLOW_LOG);
         }

       // System.out.println("System Properties = " + result.toString());

        return result.toString();
    }

    /**
     * Build the IUM (unique identifier of the installation).
     * @param username the username
     * @param seed the random seed
     * @param applicationContext application context to retrieve phone data
     * @return the IUM as a hexadecimal string
     */
    public static String buildIUM(final String username, final long seed, final Context applicationContext) {

    	StringBuffer sb;

    	if(username != null)
    		sb = new StringBuffer(username);
    	else
    		sb = new StringBuffer();
    	
        sb.append(seed);
        final String imei = getImei(applicationContext);
        if (imei != null) {
            sb.append(imei);
        }
        final String imsi = getImsi(applicationContext);
        if (imsi != null) {
            sb.append(imsi);
        }

        final String props = getSystemProperties();
        if (props != null) {
            sb.append(props);
        }

        final String input = sb.toString();

        final Digest  digest = new MD5Digest();
        final byte[]  resBuf = new byte[digest.getDigestSize()];
        final byte[]  bytes = input.getBytes();
        digest.update(bytes, 0, bytes.length);
        digest.doFinal(resBuf, 0);

        final String output = new String(Hex.encode(resBuf)).toUpperCase();

        return output;

    }

    // TODO: PENDING, Must check in real device -- folvera
    /**
     * Returns midlet version read from the jad or null if not available.
     * @param applicationContext application context to retrieve phone data
     * @return the application version
     */
    public static String getVersion(final Context applicationContext) {
        //String value = System.getProperty("java.class.version");
        //String value = System.getProperty("java.version");

    	String value = "0";
        final PackageManager pm = applicationContext.getPackageManager();
        try {
            final PackageInfo pi =
                pm.getPackageInfo("com.bancomer.mbanking", 0);
            value = Integer.toString(pi.versionCode);
        } catch (NameNotFoundException e) {
            ToolsCommons.writeLoge(Tools.class.getSimpleName(), e.getMessage(), ServerCommons.ALLOW_LOG);
        }

        return value;
    }

    // PENDING, Must check in real device -- folvera
    /**
     * Returns device name or null if not available.
     * @return the device name
     */
    public static String getDevice() {
        String value = null;

        try {
            value = System.getProperty("android.os.Build.DEVICE");
        } catch (Exception e) {
            ToolsCommons.writeLoge(Tools.class.getSimpleName(), e.getMessage(), ServerCommons.ALLOW_LOG);
        };

        if (value == null) {
        	try {
        		value = System.getProperty("android.os.Build.MODEL");
        	} catch (Exception e) {
                ToolsCommons.writeLoge(Tools.class.getSimpleName(), e.getMessage(), ServerCommons.ALLOW_LOG);
            };
        }

        return value;
    }


    /**
     * Returns free memory available or null if not available.
     * @return the free memory
     */
    public static String getFreeMemory() {
        System.gc();
        System.gc();
        System.gc();
        final String value = String.valueOf(Runtime.getRuntime().freeMemory());

        return value;
    }


    /**
     * obtain a time stamp from a date and time expressed as string.
     * @param date the date as DDMMYYYY
     * @param time the time as HHMMSS
     * @return a timestamp specifying the date and time passed. Returns 0
     * if the date or time cannot be parsed
     */
    public static long parseDateTime(final String date, final String time) {
        long result = 0;
        try {
            final String day = date.substring(0, 2);
            final String month = date.substring(2, 4);
            final String year = date.substring(4);
            final String hour = time.substring(0, 2);
            final String minute = time.substring(2, 4);
            final String second = time.substring(4);
            final Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, Integer.parseInt(year));
            final int m = Integer.parseInt(month);
            int monthInt = 0;
            switch (m) {
                case 1:
                    monthInt = Calendar.JANUARY;
                    break;
                case 2:
                    monthInt = Calendar.FEBRUARY;
                    break;
                 case 3:
                    monthInt = Calendar.MARCH;
                    break;
                 case 4:
                    monthInt = Calendar.APRIL;
                    break;
                 case 5:
                    monthInt = Calendar.MAY;
                    break;
                 case 6:
                    monthInt = Calendar.JUNE;
                    break;
                 case 7:
                    monthInt = Calendar.JULY;
                    break;
                 case 8:
                    monthInt = Calendar.AUGUST;
                    break;
                 case 9:
                    monthInt = Calendar.SEPTEMBER;
                    break;
                 case 10:
                    monthInt = Calendar.OCTOBER;
                    break;
                 case 11:
                    monthInt = Calendar.NOVEMBER;
                    break;
                 case 12:
                    monthInt = Calendar.DECEMBER;
                    break;
            }

            calendar.set(Calendar.MONTH, monthInt);
            calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hour));
            calendar.set(Calendar.MINUTE, Integer.parseInt(minute));
            calendar.set(Calendar.SECOND, Integer.parseInt(second));
            result = calendar.getTime().getTime();
        } catch (Throwable th) {
        	if(ServerCommons.ALLOW_LOG) th.printStackTrace();
        }
        return result;
    }

     /**
     * Convert a time stammp into a formatted string date as DD/MM/YYYY.
     * @param timestamp the tiemestamp to format
     * @return the timestamp formatted as a date DD/MM/YYYY
     */
    public static String formatDate(final long timestamp) {
        String result = "";
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date(timestamp));
        final StringBuffer sb = new StringBuffer();
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        if (day < 10) {
            sb.append("0");
        }
        sb.append(day).append("/");

        final int m = calendar.get(Calendar.MONTH);
        String month = "";
        switch (m) {
            case Calendar.JANUARY:
                month = "01";
                break;
            case Calendar.FEBRUARY:
                month = "02";
                break;
            case Calendar.MARCH:
                month = "03";
                break;
            case Calendar.APRIL:
                month = "04";
                break;
            case Calendar.MAY:
                month = "05";
                break;
            case Calendar.JUNE:
                month = "06";
                break;
            case Calendar.JULY:
                month = "07";
                break;
            case Calendar.AUGUST:
                month = "08";
                break;
            case Calendar.SEPTEMBER:
                month = "09";
                break;
            case Calendar.OCTOBER:
                month = "10";
                break;
            case Calendar.NOVEMBER:
                month = "11";
                break;
            case Calendar.DECEMBER:
                month = "12";
                break;
        }
        sb.append(month).append("/");
        final int year = calendar.get(Calendar.YEAR);
        sb.append(year);
        result = sb.toString();
        return result;
    }

  

    /**
     * Get a Combo control and put items itself from <code>items</code> parameter and
     * set the initially selected item.
     * Example:
     * <pre>
     * Tools.getCombo(context,items,0);
     * </pre>
     * @param context , the context of Combo control
     * @param items , the items in the Combo
     * @param selected the item selected initially
     * @return the Combo object
     */
    /*public static Combo getCombo(Context context, ArrayList<?> items, int selected) {
    	Combo combo = new Combo(context);
    	combo.setDataStore(items);
    	combo.setSelection(selected);
    	return combo;
    }*/
    
    /**
     * Determina si el dispositivo puede realizar llamadas
     * @param context El contexto de la aplicaciÃ³n
     * @return true si el dispositivo puede realizar llamadas, false si no
     */
    public static boolean hasPhoneAbility(final Context context) {
       final TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
       if(telephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_NONE) {
           return false;
       }
       return true;
    }
    
    public static String getCurrentDate() {
    	final SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy");
    	return s.format(new Date());
    }
    
    public static boolean isServerDateOneMonthAhead(final Date serverDate, final String dateOrigin) {
    	final SimpleDateFormat sdf = new SimpleDateFormat("MM/yyyy", Locale.getDefault());
    	final String currentDate = sdf.format(serverDate);
    	final String currentMonth = currentDate.substring(0, currentDate.lastIndexOf("/"));
    	final String currentYear = currentDate.substring(currentDate.lastIndexOf("/")+1, currentDate.length());
    	final String originMonth = dateOrigin.substring(dateOrigin.indexOf("/")+1, dateOrigin.lastIndexOf("/"));
    	final String originYear = dateOrigin.substring(dateOrigin.lastIndexOf("/") + 1, dateOrigin.length());
    	
    	boolean flag = Integer.parseInt(currentMonth) > Integer.parseInt(originMonth);
    	flag = flag || Integer.parseInt(currentYear) > Integer.parseInt(originYear);
    	if(ServerCommons.ALLOW_LOG) Log.d("Tools", "Current month is ahead from month stored " + flag);
    	
    	return flag;
    }
    
    public static boolean isDateDayEarlierThanMaxDay(final Date currentDate) {
    	final SimpleDateFormat sdf = new SimpleDateFormat("dd", Locale.getDefault());
    	final String currentDay = sdf.format(currentDate);
    	final boolean flag = Integer.parseInt(currentDay) < Constants.CAMBIO_PERFIL_DIA_MAXIMO;
    	if(ServerCommons.ALLOW_LOG) Log.d("Tools", "Current day is earlier than max day " + flag);
    	return flag;
    }
    
    /**
     * Returns device name or null if not available.
     * @return the device name
     */
    public static String getDeviceForAcercaDe() {
        String device = null;
        String model  = null;
        String value  = "";
        
        try {
            device = Build.BRAND;//System.getProperty("android.os.Build.DEVICE");
        } catch (Exception e) {
            ToolsCommons.writeLoge(Tools.class.getSimpleName(), e.getMessage(), ServerCommons.ALLOW_LOG);
        };

        try {
        	model = Build.MODEL;// System.getProperty("android.os.Build.MODEL");
        } catch (Exception e) {
            ToolsCommons.writeLoge(Tools.class.getSimpleName(), e.getMessage(), ServerCommons.ALLOW_LOG);
        };
        if(device != null)
         value+=device;
        if(model != null)
         value+=" "+model;

        return value;
    }
    
    public static Constants.TipoOtpAutenticacion getTipoOtpFromString(final String tipo) {
    	Constants.TipoOtpAutenticacion tipoOtp = null;
    	if (tipo.equalsIgnoreCase("0")) {
    		tipoOtp = Constants.TipoOtpAutenticacion.ninguno;
    	} else if (tipo.equalsIgnoreCase("1")){
    		tipoOtp = Constants.TipoOtpAutenticacion.codigo;
    	} else if (tipo.equalsIgnoreCase("2")){
    		tipoOtp = Constants.TipoOtpAutenticacion.registro;
    	}
    	return tipoOtp;
    }
    
    /**
     * HHMMSS to HH:MM:SS
     * @param time The time string.
     * @return The formated time string.
     */
    public static String formatTime(final String time) {
    	String result = "";
    	
    	if(null == time)
    		return result;
    	if(6 != time.length())
    		return result;

    	result = result.concat(time.substring(0, 2) + ":" + time.substring(2, 4) + ":" + time.substring(4, 6) );
    	
    	return result;
    }

    public static String dateForServer(final Date currentDate){
    	final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy", Locale.getDefault());
    	return sdf.format(currentDate);
    }
    
    public static String dateToString(final Date currentDate){
    	final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
    	return sdf.format(currentDate);
    }
    
    public static String obtenerHora(final Date currentDate){
    	final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
    	return sdf.format(currentDate);
    }
    
    public static String dateForReference(final Date currentDate){
    	final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyy", Locale.getDefault());
    	return sdf.format(currentDate);
    }

    /**
     * Da formato a una cadena de texto con un nÃºmero de telefono, remueve todos los carcteres que no sean dÃ­gitos y el cÃ³digo lada con formado "+dd".
     * @param phoneNumber La cadena de texto con el nÃºmero de telÃ©fono.
     * @return La cadena de texto sin cÃ³digo lada ni caracteres que no sean dÃ­gitos.
     */
    public static String formatPhoneNumberFromContact(final String phoneNumber) {
    	String result;
    	
    	result = phoneNumber.replaceAll("[^0-9]", "");
    	if(result.length() > Constants.TELEPHONE_NUMBER_LENGTH)
    		result = result.substring(result.length() - Constants.TELEPHONE_NUMBER_LENGTH);
    	
    	return result;
    }

    public static Account obtenerCuenta(final String cuenta) {
    	final Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
    	
    	Account selectedAccount = null;
    	
    	for(final Account acc : accounts) {
    		if(acc.getNumber().equalsIgnoreCase(cuenta)) {
    			selectedAccount = acc;
    			break;
    		}
    	}
    	
    	if(null == selectedAccount) {
    		selectedAccount = new Account();
    		selectedAccount.setNumber(Constants.INVALID_ACCOUNT_NUMBER);
    	}
    	
    	return selectedAccount;
    }
    
    public static String convertDoubleToBigDecimalAndReturnString(final double importe)
    {
    	return convertDoubleToBigDecimalAndReturnString(importe, true);
    }
    
    public static String convertDoubleToBigDecimalAndReturnString(final double importe, final boolean remplazarPunto)
    {
    	BigDecimal fullImporte = new BigDecimal(importe);
    	fullImporte = fullImporte.setScale(2, RoundingMode.HALF_UP);
    	
    	return remplazarPunto ? fullImporte.toString().replace(".", "") : fullImporte.toString();
    }
    
    

    /**
     * Obtiene la cuenta eje del cliente.
     * @return La cuenta eje.
     */
    public static Account obtenerCuentaEje()
    {
    	final Account[] accounts = Session.getInstance(SuiteApp.appContext).getAccounts();
    	Account cuentaEje = null;
    	
    	for(final Account acc : accounts) {
			if(acc.isVisible()) {
				cuentaEje = acc;
				break;
			}
		}
    	
    	return cuentaEje;
    }
    
    public static boolean isExpress(final String numero) {
    	if(null == numero)
    		return false;
    	
    	for (final Account acc : Session.getInstance(SuiteApp.appContext).getAccounts()) {
    		if(acc.getNumber().equalsIgnoreCase(numero))
    			return true;
		}
    	
    	return false;
    }
    
    public static String enmascaraCuentaDestino(final String account) {
        String result;
        if ((account != null) && (account.length() == 20)) {
        	result =  account.substring(10);
        }else 
        	return account;
        return result;
    }
    
    public static String enmascaraCuentaDestinoPagoTDC(final String account) {
        String result = "";
        if ((account != null)) {
        	for (int i=0;i<account.length()-5;i++){
        		result=result+"*";
        	}
        	result =result + account.substring(account.length()-5,account.length());
        }else{ 
        	return account;
        }
        return result;
    }
    
	/**
     * Validates the new password according to the following policy:
     * a password cannot have more than 2 repeated digits together, i.e:
     * 123338 would be an invalid password 
     * @param pwd the password to validate
     * @return true if the password passes the policy , false if it fails
     */
    public static boolean validatePasswordPolicy1(final String pwd) {
        if (pwd == null) {
            return false;
        }
        boolean validate = true;
        for (int i = 0; ((i < pwd.length()) && (validate)); i++ ){
            if (i > 1) {
                final char c0 = pwd.charAt(i - 2);
                final char c1 = pwd.charAt(i - 1);
                final char c2 = pwd.charAt(i);
                final boolean fails = ((c0 == c1) && (c1 == c2));
                validate = !fails;
            }
        }
        return validate;
    }
    
    /**
     * Validates the new password according to the following policy:
     * a password cannot have more than 2 digits consecutive, either
     * ascending or descending. i.e., 12348 or 43276 would fail.
     * @param pwd the password to validate
     * @return true if the password passes the policy , false if it fails
     */
    public static boolean validatePasswordPolicy2(final String pwd) {
        if (pwd == null) {
            return false;
        }
        boolean validate = true;
        for (int i = 0; ((i < pwd.length()) && (validate)); i++ ){
            if (i > 1) {
                final char c0 = pwd.charAt(i - 2);
                final char c1 = pwd.charAt(i - 1);
                final char c2 = pwd.charAt(i);
                final boolean fails = (
                        ((c2 == c1 + 1) && (c1 == c0 + 1)) // ascending order
                        ||
                        ((c2 == c1 - 1) && (c1 == c0 -1)) // descending order
                        );
                validate = !fails;
            }
        }
        return validate;
    }
	
	/**
     * Remplaza los caracteres especiales por los caracteres admitidos por NACAR y devuelve una nueva cadena sin caracteres especiales.
     * <br/>
     * ï¿½ - cambia por "ï¿½".
     * <br/>
     * ï¿½ - cambia por "ï¿½".
     * <br/>
     * ï¿½, ï¿½, ï¿½ - cambia por "A".
     * <br/>
     * ï¿½, ï¿½, ï¿½ - cambia por "a".
     * <br/>
     * ï¿½, ï¿½, ï¿½ - cambia por "E".
     * <br/>
     * ï¿½, ï¿½, ï¿½ - cambia por "e".
     * <br/>
     * ï¿½, ï¿½, ï¿½ - cambia por "I".
     * <br/>
     * ï¿½, ï¿½, ï¿½ - cambia por "i".
     * <br/>
     * ï¿½, ï¿½, ï¿½ - cambia por "O".
     * <br/>
     * ï¿½, ï¿½, ï¿½ - cambia por "o".
     * <br/>
     * ï¿½, ï¿½, ï¿½ - cambia por "U".
     * <br/>
     * ï¿½, ï¿½, ï¿½ - cambia por "u".
     * @param value La cadena inicial a la que se le removeran los caracteres especiales.
     * @return Una nueva cadena sin caracteres especiales.
     */
    public static String removeSpecialCharacters(final String value) {
		if (null == value)
			return null;

		final StringBuffer sb = new StringBuffer("");
		char currentChar = '\0';

		String text = "";// value;
		int tam;

	//	System.out.println("MEnsaje: " + value);

		for (int count = 0; count < value.length(); count++) {
			currentChar = value.charAt(count);

			if (currentChar == 'Ñ')
				sb.append('N');
			else if (currentChar == 'ñ')
				sb.append('n');
			else if ((currentChar == 'Á') || (currentChar == 'À')
					|| (currentChar == 'Ä'))
				sb.append("A");
			else if ((currentChar == 'á') || (currentChar == 'à')
					|| (currentChar == 'ä'))
				sb.append("a");
			else if ((currentChar == 'É') || (currentChar == 'È')
					|| (currentChar == 'Ë'))
				sb.append("E");
			else if ((currentChar == 'é') || (currentChar == 'è')
					|| (currentChar == 'ë'))
				sb.append("e");
			else if ((currentChar == 'Í') || (currentChar == 'Ì')
					|| (currentChar == 'Ï'))
				sb.append("I");
			else if ((currentChar == 'í') || (currentChar == 'ì')
					|| (currentChar == 'ï'))
				sb.append("i");
			else if ((currentChar == 'Ó') || (currentChar == 'Ò')
					|| (currentChar == 'Ö'))
				sb.append("O");
			else if ((currentChar == 'ó') || (currentChar == 'ò')
					|| (currentChar == 'ö'))
				sb.append("o");
			else if ((currentChar == 'Ú') || (currentChar == 'Ù')
					|| (currentChar == 'Ü'))
				sb.append("U");
			else if ((currentChar == 'ú') || (currentChar == 'ù')
					|| (currentChar == 'ü'))
				sb.append("u");
			else
				sb.append(currentChar);
		}
		text = sb.toString();
		tam = text.length();
		//sb.delete(0, tam);

		//System.out.println("Mensaje: " + text);
		//	System.out.println("tamacadena: " + tam);
		if (tam <= 50) {
			sb.delete(0, tam);
			// Ciclo donde se eliminan los caracteres especiales
			for (int count = 0; count < tam; count++) {
				currentChar = text.charAt(count);
				// //Variable para obtener el valor ASCII del caracter
				final int vaASCII = (int) currentChar;
				//	System.out.println("ASCII: " + vaASCII);
				if ((vaASCII > 47 && vaASCII < 58)
						|| (vaASCII > 64 && vaASCII < 91)
						|| (vaASCII > 96 && vaASCII < 123) || (vaASCII == 32))
					sb.append(currentChar);
			}

		}

		return sb.toString();

	}
    
    /**
	 * Determina el perfil del cliente (BÃ¡sico o Avanzado).
	 * @param perfilCliente Perfil del cliente, puede ser MF00, MF01, MF02 y MF03.
	 * @param tipoInstrumento Tipo de instrumento del cliente, puede ser D2, D3, T1, T3, T4, T5, T6 ï¿½ S1.
	 * @param estadoInstrumento Estado del instrumento, puede ser A1, AL, AS, AT ï¿½ AB. 
	 * @return Tipo de perfil del cliente.
	 */
	public static Perfil determinaPerfil(final String perfilCliente, final String tipoInstrumento, final String estadoInstrumento) {
		boolean esAvanzado = true;
		
		if(Tools.isEmptyOrNull(tipoInstrumento) || Tools.isEmptyOrNull(estadoInstrumento) || Tools.isEmptyOrNull(perfilCliente))
			esAvanzado = false;
		else
			esAvanzado = Constants.PROFILE_ADVANCED_03.equalsIgnoreCase(perfilCliente);
		
		return esAvanzado ? Perfil.avanzado : Perfil.basico;
	}
	
	public static String determinarPerfil(final Perfil perfilEnum) {
		String perfilMF = null;
		
		if (Perfil.recortado == perfilEnum) {
			perfilMF = Constants.PROFILE_RECORTADO_02;
		} else if (Perfil.basico == perfilEnum) {
			perfilMF = Constants.PROFILE_BASIC_01;
		} else if (Perfil.avanzado == perfilEnum) {
			perfilMF = Constants.PROFILE_ADVANCED_03;
		}
		
		return perfilMF;
	}

	/**
	 * Invert the date elements order, goes from AAAA-MM-DD to DDMMAAAA format.
	 * @param fechaContratacion The date as String.
	 * @return The formated date.
	 */
	public static String invertDateOrder(final String fechaContratacion) {
		String result = "";
		
		if(Tools.isEmptyOrNull(fechaContratacion))
			return result;
		
		result += fechaContratacion.substring(8, 10);
		result += fechaContratacion.substring(5, 7);
		result += fechaContratacion.substring(0, 4);
		
		return result;
	}
	
	/**
	 * Convierte la cantidad indicada a una cadena de caracteres con formato de moneda.
	 * <br/>
	 * Ejemplo: 1234  ->  "$ 1,234.00" 
	 * @param value La cantidad deseada.
	 * @return La cadena de caracteres con el formato de moneda. 
	 */
	public static String formatToCurrencyAmount(final int value) {
		return formatToCurrencyAmount((double)value);
	}
	
	/**
	 * Convierte la cantidad indicada a una cadena de caracteres con formato de moneda.
	 * <br/>
	 * Ejemplo: 1234  ->  "$ 1,234.00" 
	 * @param value La cantidad deseada.
	 * @return La cadena de caracteres con el formato de moneda. 
	 */
	public static String formatToCurrencyAmount(final long value) {
		return formatToCurrencyAmount((double)value);
	}
	
	/**
	 * Convierte la cantidad indicada a una cadena de caracteres con formato de moneda.
	 * <br/>
	 * Ejemplo: 1234.5678  ->  "$ 1,234.56" 
	 * @param value La cantidad deseada.
	 * @return La cadena de caracteres con el formato de moneda. 
	 */
	public static String formatToCurrencyAmount(final float value) {
		return formatToCurrencyAmount((double) value);
	}
	
	/**
	 * Convierte la cantidad indicada a una cadena de caracteres con formato de moneda.
	 * <br/>
	 * Ejemplo: 1234.5678  ->  "$ 1,234.56" 
	 * @param value La cantidad deseada.
	 * @return La cadena de caracteres con el formato de moneda. 
	 */
	public static String formatToCurrencyAmount(final double value) {
		return Tools.formatAmount(Tools.convertDoubleToBigDecimalAndReturnString(value), false);
	}
	
	
	
	
	public static synchronized void storeFirstActivationDate() {
		final Calendar dateTime = Calendar.getInstance();
		String date, time;
		int fieldValue;
		StringBuilder builder = new StringBuilder();
		
		fieldValue = dateTime.get(Calendar.YEAR);
		builder.append(String.valueOf(fieldValue));
		fieldValue = dateTime.get(Calendar.MONTH) + 1;
		if(fieldValue < 10)
			builder.append("0");
		builder.append(String.valueOf(fieldValue));
		fieldValue = dateTime.get(Calendar.DAY_OF_MONTH);
		if(fieldValue < 10)
			builder.append("0");
		builder.append(String.valueOf(fieldValue));
		date = builder.toString();
		
		builder = new StringBuilder();
		fieldValue = dateTime.get(Calendar.HOUR_OF_DAY);
		if(fieldValue < 10)
			builder.append("0");
		builder.append(String.valueOf(fieldValue));
		fieldValue = dateTime.get(Calendar.MINUTE);
		if(fieldValue < 10)
			builder.append("0");
		builder.append(String.valueOf(fieldValue));
		time = builder.toString();
		
		Tools.storeFirstActivationDate(date, time);
	}
	
	public static synchronized void storeFirstActivationDate(final String date, final String hour) {
//		DBAdapter adapter;
//		
//		try {
//			adapter = new DBAdapter(SuiteApp.appContext);
//			adapter.open();
//		} catch (Throwable t) {
//			if(Server.ALLOW_LOG) Log.e("Tools", "Error opening the record store.", t);
//			return;
//		}

		StringBuilder builder = new StringBuilder();
		builder.append(date.substring(0, 4));
		builder.append("-");
		builder.append(date.substring(4, 6));
		builder.append("-");
		builder.append(date.substring(6, 8));
//		adapter.insertBmovilActivationEntry(DBAdapter.KEY_CAT_BM_DT, builder.toString());
		
		ActivacionBmovilFileManager.getCurrent().addActivacion(Constants.CAT_BM_DT, builder.toString());
		
		builder = new StringBuilder();
		builder.append(hour.substring(0, 2));
		builder.append("-");
		builder.append(hour.substring(2, 4));
		ActivacionBmovilFileManager.getCurrent().addActivacion(Constants.CAT_BM_HR, builder.toString());
		
//		adapter.insertBmovilActivationEntry(DBAdapter.KEY_CAT_BM_HR, builder.toString());
//		
//		adapter.close();
	}
	
	public static synchronized Calendar getFirstActivationDate() {
		final Calendar date = Calendar.getInstance();
//		DBAdapter adapter;
		
//		try {
//			adapter = new DBAdapter(SuiteApp.appContext);
//			adapter.open();
//		} catch (Throwable t) {
//			if(Server.ALLOW_LOG) Log.e("Tools", "Error opening the record store.", t);
//			return null;
//		}
		
//		Cursor cursor = adapter.selectBmovilActivationEntry(DBAdapter.KEY_CAT_BM_DT);
//		if(!cursor.moveToFirst())
//			return null;
		
		String propValue = ActivacionBmovilFileManager.getCurrent().getValueFile(Constants.CAT_BM_DT);
		
//		String propValue = cursor.getString(cursor.getColumnIndex(DBAdapter.KEY_VALUE));
		if(propValue==null || propValue.equalsIgnoreCase(Constants.NON_REGISTERED_ACTIVATION_DATE))
			return null;
		
		String[] values = propValue.split("-");
		final int year = Integer.parseInt(values[0]);
		final int month = Integer.parseInt(values[1]);
		final int day = Integer.parseInt(values[2]);
		date.set(Calendar.YEAR, year);
		// The Calendar object stores the Moth of the year as a zero based value. Example: January = 0, December = 11.
		date.set(Calendar.MONTH, month - 1);
		date.set(Calendar.DAY_OF_MONTH, day);
//		cursor.close();
		
//		cursor = adapter.selectBmovilActivationEntry(DBAdapter.KEY_CAT_BM_HR);
//		if(!cursor.moveToFirst())
//			return null;
		
//		propValue = cursor.getString(cursor.getColumnIndex(DBAdapter.KEY_VALUE));
		propValue = ActivacionBmovilFileManager.getCurrent().getValueFile(Constants.CAT_BM_HR);
		if(propValue.equalsIgnoreCase(Constants.NON_REGISTERED_ACTIVATION_DATE)|| propValue==null)
			return null;
		
		values = propValue.split("-");
		final int hour = Integer.parseInt(values[0]);
		final int minute = Integer.parseInt(values[1]);
		date.set(Calendar.HOUR_OF_DAY, hour);
		date.set(Calendar.MINUTE, minute);
//		cursor.close();
//		
//		adapter.close();
		return date;
	}
	
	public static synchronized boolean isFirstActivationStored() {
//		DBAdapter adapter;
//		
//		try {
//			adapter = new DBAdapter(SuiteApp.appContext);
//			adapter.open();
//		} catch (Throwable t) {
//			if(Server.ALLOW_LOG) Log.e("Tools", "Error opening the record store.", t);
//			return false;
//		}
		
		boolean firstActivationStored= false;
		final String firstActivation =ActivacionBmovilFileManager.getCurrent().getValueFile(Constants.CAT_BM_DT);
		
		if(firstActivation!=null){
			firstActivationStored=true;
		}
//		Cursor cursor = adapter.selectBmovilActivationEntry(DBAdapter.KEY_CAT_BM_DT);
//		boolean firstActivationStored = cursor.moveToFirst();
		
//		cursor.close();
//		adapter.close();
		
		return firstActivationStored;
	}

	public static void deleteFirstActivationData(){
//		DBAdapter adapter;
//		
//		try {
//			adapter = new DBAdapter(SuiteApp.appContext);
//			adapter.open();
//		} catch (Throwable t) {
//			if(Server.ALLOW_LOG) Log.e("Tools", "Error opening the record store.", t);
//			return;
//		}
		
		ActivacionBmovilFileManager.getCurrent().borrarActivacionBmovil();
		
		if(ServerCommons.ALLOW_LOG) Log.v(Tools.class.getSimpleName(), "Borrando los datos de activaciÃ³n");
//		adapter.deleteAllBmovilActivationEntries();
	}
	
	public static Calendar formatDateTimeFromServer(final String date, final String time) {
		final Calendar result = Calendar.getInstance();
		
		if(Tools.isEmptyOrNull(date) || Tools.isEmptyOrNull(time)) {
			if(ServerCommons.ALLOW_LOG) Log.d(Tools.class.getSimpleName(), "El parametro de fecha u hora fueron nulos o vacios, se regresa la fecha y hora del sistema");
			return result;
		}
		
		int year = 0, month = 0, day = 0, hour = 0, minute = 0, second = 0;
		
		day = Integer.parseInt(date.substring(0, 2));
		// El valor de los meses comienza desde 0 para Enero, por lo que debemos restar 1 al valor devuelto por el servidor
		month = Integer.parseInt(date.substring(2, 4)) - 1;
		year = Integer.parseInt(date.substring(4, 8));
		
		hour = Integer.parseInt(time.substring(0, 2));
		minute = Integer.parseInt(time.substring(2, 4));
		second = Integer.parseInt(time.substring(4, 6));
		
		result.set(year, month, day, hour, minute, second);
		return result;
	}
	
	public static long validaFechaDeActivacion() {
//		Calendar serverDateTime = Tools.formatDateTimeFromServer(loginData.getServerDate(), loginData.getServerTime());
		final Calendar serverDateTime = Calendar.getInstance();
		final Calendar activationDate = getFirstActivationDate();
		
    	if(null == activationDate)
    		return -1L;
    	
    	final long timeElapsed = serverDateTime.getTimeInMillis() - activationDate.getTimeInMillis();
    	
    	if(timeElapsed > Long.parseLong(Constants.TIEMPO_DE_ESPERA_VALIDACION_ACCESO, 10) * 60000L) {
    		return -1L;
    	} else {
    		return 30L - (timeElapsed / 60000L);
    	}
	}
	
	public static void trazaTexto(final String titulo, final String mensaje) {
		if(ServerCommons.ALLOW_LOG) Log.d(titulo,"");
		for (int i = 0; i < (mensaje.length() / TAM_FRAGMENTO_TEXTO) + 1; i++) {
			if(ServerCommons.ALLOW_LOG) Log.d("", mensaje.substring(i*TAM_FRAGMENTO_TEXTO, Math.min((i+1)*TAM_FRAGMENTO_TEXTO, mensaje.length())));
		}
	}
	
	   public static final String formatDateFromServer(){
			
			String fechaUltimaMod = "";
			
			   final Calendar fecha = new GregorianCalendar();
		  
		        final int anio = fecha.get(Calendar.YEAR);
		        final int mes = fecha.get(Calendar.MONTH);
		        final int dia = fecha.get(Calendar.DAY_OF_MONTH);
		        final int hora = fecha.get(Calendar.HOUR_OF_DAY);
		        final int minuto = fecha.get(Calendar.MINUTE);
		        final int segundo = fecha.get(Calendar.SECOND);
			
		        fechaUltimaMod = dia + "-" + (mes+1) + "-" + anio;
			
			return fechaUltimaMod;
			
		}
	   
	   public static String formatCR(String claveRastreo) {
			// TODO Auto-generated method stub
			try{
			claveRastreo=claveRastreo.trim();
			String res="";
			res+=claveRastreo.substring(0,4)+" ";
			res+=claveRastreo.substring(4,8)+" ";
			res+=claveRastreo.substring(8,12)+" ";
			res+=claveRastreo.substring(12,16)+" ";
			res+=claveRastreo.substring(16,20)+" ";
			res+=claveRastreo.substring(20)+" ";
			return res;
			}catch(Exception e){
				return claveRastreo;
			}
		}
	   
	   /**
		 * Metodo para validar telefono 
		 * @param telefono string
		 * @return true si el telefono tiene 10 digitos
		 */
		public static boolean validaTelefono(final String telefono){
			return ((telefono != null) && (Pattern.matches("[0-9]{10}", telefono)));		
		}
		
		/**
		 * Metodo para validar sedd
		 * @param seed string
		 * @return true si no esta vacio y no es espacio en blanco
		 */
		public static boolean validaSeed(final String seed){
			return ((seed != null) && (seed.length() != 0) && (!seed.equals(" ")) && (!seed.equals("0")));		
		}

    /**
     * obtain a time stamp from a time expressed as string.
     * @param time the time as HHMMSS
     * @return a timestamp specifying the date and time passed. Returns 0
     * if the date or time cannot be parsed
     */
    public static String parsetime(final String time) {
        String result = "";

        try {
            final String hour = time.substring(0, 2);
            final String minute = time.substring(2, 4);
            final String second = time.substring(4, 6);
            result = hour+":"+minute+":"+second;
        } catch (Throwable th) {
            ToolsCommons.writeLoge(Tools.class.getSimpleName(), th.getMessage(), ServerCommons.ALLOW_LOG);
        }
        return result;
    }

    /**
     * obtener la fecha en formato 2014 Enero.
     * @param date the date as YYYYMM
     */
    public static String parsePeriodoFecha(final String date) {
        String result = "";
        try {
            final String year = date.substring(0, 4);
            final String month = date.substring(4);
            String mes ="";
            final int m = Integer.parseInt(month);

            if(m==01){
                mes = "Enero";

            }else if(m==02){
                mes = "Febrero";

            }else if(m==03){
                mes = "Marzo";

            }else if(m==04){
                mes = "Abril";

            }else if(m==05){
                mes = "Mayo";

            }else if(m==06){
                mes = "Junio";

            }else if(m==07){
                mes = "Julio";

            }else if(m==8){
                mes = "Agosto";

            }else if(m==9){
                mes = "Septiembre";

            }else if(m==10){
                mes = "Octubre";

            }else if(m==11){
                mes = "Noviembre";

            }else if(m==12){
                mes = "Diciembre";
            }

            result = mes+" "+year;
        } catch (Throwable th) {
            ToolsCommons.writeLoge(Tools.class.getSimpleName(), th.getMessage(), ServerCommons.ALLOW_LOG);
        }
        return result;
    }
    public  static String formatPercentage(String number){
        return number + "%";
    }

    public static String compruebaFormatoFecha(String fecha) {
        if (fecha.matches("\\d{4}-\\d{2}-\\d{2}")) {
            String fechaA[] = fecha.split("-");
            fecha = fechaA[2]+"/"+fechaA[1]+"/"+fechaA[0];
        }
        return fecha;
    }

}
