package suitebancomercoms.aplicaciones.bmovil.classes.common;

import android.content.Context;
import android.util.Log;

import com.bancomer.base.SuiteApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;
import java.util.Map.Entry;
import java.util.Properties;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public final class CompaniasTelefonicasCatalogFileManager {
	// #region Constantes.
	/**
	 * Nombre del archivo de propiedades.
	 */
	private static final String FILE_NAME = "CompaniasTelefonicaCatalog.prop";
		
	/**
	 * 
	 */
	
	
	// #endregion
	
	// #region Variables.
	/**
	 * Propiedades de la aplicacion.
	 */
	private Properties properties;
	// #endregion
	
	// #region Singleton.
	/**
	 * La instancia de la clase.
	 */
	private static CompaniasTelefonicasCatalogFileManager manager = null;
	
	/**
	 * @return La instancia de la clase.
	 */
	public static CompaniasTelefonicasCatalogFileManager getCurrent() {
		if(null == manager)
			manager = new CompaniasTelefonicasCatalogFileManager();
		return manager;
	}
	
	/**
	 * Inicializa el administrador de propiedades. 
	 */
	private CompaniasTelefonicasCatalogFileManager() {
		properties = null;
		final File file = new File(SuiteApp.appContext.getFilesDir(), FILE_NAME);
		
		if(!file.exists())
			initFile(file);
		else
			loadFile();
	}
	// #endregion
	
	// #region Carga del archivo de propiedades.
	/**
	 * Inicializa el archivo de propiedades con los valores iniciales para cada propiedad.
	 * @param file
	 */
	private void initFile(final File file) {
		try {
			file.createNewFile();
		} catch(IOException ioEx) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al crear el archivo CompaniasTelefonicasCatalog.", ioEx);
			return;
		}
				
		loadFile();
		
		if(null != properties) {
			setPropertynValue("", "");
		}
	}
	
	/**
	 * Carga el archivo de propiedades.
	 */
	private void loadFile() {
		InputStream input;
		try {
			input = SuiteApp.appContext.openFileInput(FILE_NAME);
		} catch(FileNotFoundException fnfEx) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargar el archivo CompaniasTelefonicasCatalog para lectura.", fnfEx);
			return;
		}

		properties = new Properties();
		try {
			properties.load(input);
		} catch(IOException ioEx) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al cargar TemporalCambioTelefono.", ioEx);
			properties = null;
			return;
		}
	}
	// #endregion
	
	// #region Generic getters and setters for properties.
	/**
	 * Establece el valor especificado a la propiedad indicada.
	 * @param propertyName El nombre de la propiedad.
	 * @param value El valor a establecer.
	 */
	private void setPropertynValue(final String propertyName, String value) {
		if(null == properties || Tools.isEmptyOrNull(propertyName))
			return;
		if(null == value)
			value = "";
		
		properties.setProperty(propertyName, value);
		storeFileForProperty(propertyName, value);
	}
	public String getVersion() {
		if(null == properties)
			return null;
		if(null == properties)
			return null;
		
		final String values = (String) properties.get("VersionID");
		
				
		
        
		return values;
	}
	
	// #endregion
	
	// #region Setters y Getters del estatus de activación para las aplicaciones.	
	
	public void addNewValueCatalog(final String identificador, final String value)
	{
		setPropertynValue(identificador, value);
	};
	
	public Collection<Entry<Object,Object>> getAllCompaniasTelefonicas(){
		return properties.entrySet();
	};
	
	public void borrarCatalogo(){
		
		properties.clear();
	}
	
	// #endregion	
	
	/**
	 * Guarda el archivo de propiedades para preservar el cambio en alguna de las propiedades. 
	 * El nombre y valor de la propiedad modificada con usados para registrar el proceso mediante el log.  
	 * @param propertyName El nombre de la propiedad a guardar.
	 * @param propertyValue El valor de la propiedad a guardar.
	 * @return True si el archivo fue guardado exitosamente, False de otro modo.
	 */
	private boolean storeFileForProperty(String propertyName, final String propertyValue)	{
		if(Tools.isEmptyOrNull(propertyName))
			propertyName = "No name indicated.";
		if(Tools.isEmptyOrNull(propertyValue))
			propertyName = "No value indicated.";
		
		OutputStream output;
		try {	
			output = SuiteApp.appContext.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
		} catch(FileNotFoundException fnfEx) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al abrir el archivo para guardar la propiedad " + propertyName, fnfEx);
			return false;
		}
		
		try {
			properties.store(output, null);
		} catch (IOException ioEx) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al guardar en el archivo TemporalCambioTelefono los valores: " + propertyName + " - " + propertyValue, ioEx);
			return false;
		}
		
		if(ServerCommons.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Archivo actualizado con los siguientes cambios: " + propertyName + " - " + propertyValue);
		return true;
	}
}