/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomercoms.aplicaciones.bmovil.classes.io;

import java.io.IOException;
import java.io.Serializable;

/**
 * ParsingHandler must be implemented
 * by those interested in building response
 * objects from Bancomer server responses.
 *
 * @author Stefanini IT Solutions.
 */
public interface ParsingHandler extends Serializable {

    /**
     * Process a server response.
     * @param parser the parser
     * @throws IOException exception
     * @throws ParsingException exception
     */
    public void process(Parser parser) throws IOException, ParsingException;

    public void process(ParserJSON parser) throws IOException, ParsingException;

}
