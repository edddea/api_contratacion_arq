/*
 * Copyright (c) 2010 BBVA. All Rights Reserved

 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomercoms.aplicaciones.bmovil.classes.io;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.model.AceptaOfertaConsumo;
import suitebancomercoms.aplicaciones.bmovil.classes.model.AceptaOfertaEFI;
import suitebancomercoms.aplicaciones.bmovil.classes.model.AceptaOfertaILC;

//One click

/**
 * ServerResponse wraps the response from the server, containing attributes that.
 * define if the response has been successful or not, and the data content if the
 * response was successful
 *
 * @author Stefanini IT Solutions.
 */
public class ServerResponse implements ParsingHandler {

    /////////////////////////////////////////////////////////////////////////////
    //                Status code definitions                                  //
    /////////////////////////////////////////////////////////////////////////////

    /**
	 * Default UID
	 */
	private static final long serialVersionUID = 1L;

	/**
     * Operation successful.
     */
    public static final int OPERATION_SUCCESSFUL = 0;

    /**
     * Operation with warning.
     */
    public static final int OPERATION_WARNING = 1;

    /**
     * Operation failed.
     */
    public static final int OPERATION_ERROR = 2;

    /**
     * Operation succesful but asks for optinal updating.
     */
    public static final int OPERATION_OPTIONAL_UPDATE = 3;

    /**
     * Session expired.
     */
    public static final int OPERATION_SESSION_EXPIRED = -100;

    /**
     * Unknown operation result.
     */
    public static final int OPERATION_STATUS_UNKNOWN = -1000;

    /**
     * Status of the current response.
     */
    private int status = 0;

    /**
     * Response error code.
     */
    private String messageCode = null;

    /**
     * Error message.
     */
    private String messageText = null;

    /**
     * URL for mandatory updating(the mandatory updating order is received as.
     * error with code MBANK1111)
     */
    private String updateURL = null;

    /**
     * Response handler, containing the data content of the response.
     */
    private Object responseHandler = null;


    private String responsePlain=null;

    /**
     * Default constructor.
     */
    public ServerResponse() {
        // Default constructor
    }

    /**
     * Constructor.
     * @param handler the handler of the content
     */
    public ServerResponse(final Object handler) {
        this.responseHandler = handler;
    }

    /**
     * Constructor with parameters.
     * @param stat the response status
     * @param msgCode the error code
     * @param msgText the error text
     * @param handler the handler of the content
     */
    public ServerResponse(final int stat, final String msgCode,
    					  final String msgText, final Object handler) {
        this.status = stat;
        this.messageCode = msgCode;
        this.messageText = msgText;
        this.responseHandler = handler;
    }

    /**
     * Get the status code.
     * @return the status code
     */
    public int getStatus() {
        return status;
    }

    /**
     * Get the error code.
     * @return the error code
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Get the error text.
     * @return  the error text
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * Get the response.
     * @return the content response
     */
    public Object getResponse() {
        return responseHandler;
    }

    /**
     * Gets the value of the URL for mandatory application update.
     * @return SOMETHING
     */
    public String getUpdateURL() {
        return updateURL;
    }

    /**
     * Gets the value of the URL for mandatory application update.
     * @return SOMETHING
     */
    public void setUpdateURL(final String updateURL) {
        this.updateURL=updateURL;
    }

    /**
     * Parse the response to get the attributes.
     * @param parser instance capable of parsing the response
     * @throws IOException exception
     * @throws ParsingException exception
     */
    public void process(final Parser parser) throws IOException, ParsingException {

        if (parser != null) {

            status = OPERATION_STATUS_UNKNOWN;
            final Result result = parser.parseResult();
            final String statusText = result.getStatus();
            if (Parser.STATUS_OK.equals(statusText)) {
                status = OPERATION_SUCCESSFUL;
            } else if (Parser.STATUS_OPTIONAL_UPDATE.equals(statusText)) {
                status = OPERATION_OPTIONAL_UPDATE;
            } else if (Parser.STATUS_WARNING.equals(statusText)) {
                status = OPERATION_WARNING;
            } else if (Parser.STATUS_ERROR.equals(statusText)) {
                status = OPERATION_ERROR;
                updateURL = result.getUpdateURL();
            }

            messageCode = result.getCode();
            messageText = result.getMessage();

            if ((responseHandler != null) && (responseHandler instanceof ParsingHandler)
            		&& ((status == OPERATION_SUCCESSFUL)
            		|| (status == OPERATION_OPTIONAL_UPDATE))) {
                ((ParsingHandler) responseHandler).process(parser);
            }

        } else {

            status = OPERATION_STATUS_UNKNOWN;
            messageCode = null;
            messageText = null;

        }

    }

    public void process(final ParserJSON parser) throws IOException, ParsingException {

        if (parser != null) {

            status = OPERATION_STATUS_UNKNOWN;
            final Result result = parser.parseResult();
            final String statusText = result.getStatus();
            if (Parser.STATUS_OK.equals(statusText)) {
                status = OPERATION_SUCCESSFUL;
            } else if (Parser.STATUS_OPTIONAL_UPDATE.equals(statusText)) {
                status = OPERATION_OPTIONAL_UPDATE;
            } else if (Parser.STATUS_WARNING.equals(statusText)) {
                status = OPERATION_WARNING;
            } else if (Parser.STATUS_ERROR.equals(statusText)) {
                status = OPERATION_ERROR;
                updateURL = result.getUpdateURL();
            }

            messageCode = result.getCode();
            messageText = result.getMessage();

            if ((responseHandler != null) && (responseHandler instanceof ParsingHandler)
            		&& ((status == OPERATION_SUCCESSFUL)
            		|| (status == OPERATION_OPTIONAL_UPDATE))) {
                ((ParsingHandler) responseHandler).process(parser);
            }else if((responseHandler != null)&& (responseHandler instanceof AceptaOfertaILC)&& ((status == OPERATION_ERROR))){
            	((ParsingHandler) responseHandler).process(parser);
            }else if((responseHandler != null)&& (responseHandler instanceof AceptaOfertaEFI)&& ((status == OPERATION_ERROR))){
            	((ParsingHandler) responseHandler).process(parser);
                
            }else if((responseHandler != null)&& (responseHandler instanceof AceptaOfertaConsumo)&& ((status == OPERATION_ERROR))){
            	((ParsingHandler) responseHandler).process(parser);
                
            }

        } else {

            status = OPERATION_STATUS_UNKNOWN;
            messageCode = null;
            messageText = null;

        }

    }

    public String getResponsePlain() {
        return responsePlain;
    }

    public void setResponsePlain(final String responsePlain) {
        this.responsePlain = responsePlain;
    }

}
