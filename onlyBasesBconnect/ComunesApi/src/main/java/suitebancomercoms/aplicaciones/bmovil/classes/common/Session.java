/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package suitebancomercoms.aplicaciones.bmovil.classes.common;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.model.CatalogoVersionado;
import bancomer.api.common.model.Compania;
import bancomer.api.common.model.Convenio;
import bancomer.api.common.model.OperacionAutenticacion;
import bancomer.api.common.model.PerfilAutenticacion;
import suitebancomer.aplicaciones.bmovil.classes.common.ToolsCommons;
import suitebancomer.aplicaciones.bmovil.classes.model.CuentaEC;
import suitebancomer.aplicaciones.keystore.DataSend;
//import suitebancomer.aplicaciones.keystore.Monitor;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomer.aplicaciones.bmovil.classes.model.Catalog;
import suitebancomercoms.aplicaciones.bmovil.classes.model.LoginData;
import suitebancomercoms.aplicaciones.bmovil.classes.model.MantenimientoSpei;
import suitebancomer.aplicaciones.bmovil.classes.model.NameValuePair;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Promociones;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Rapido;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Schedule;
import suitebancomercoms.aplicaciones.bmovil.classes.model.TemporalCambioTelefono;
import suitebancomercoms.aplicaciones.bmovil.classes.model.TemporalST;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import com.bancomer.base.R;
import com.bancomer.base.SuiteApp;
//One click

public final class Session {

	public static BaseDelegateCommons takeViewToken;

	/**
	 * The singleton instance
	 */
	private static Session theInstance = null;
	
	//inicio integracion Keychain Api LBM 
	private static KeyStoreWrapper kswrapper;
	//fin integracion
	
	////////////////////////////// SPEI VALUES /////////////////////////
	private static ArrayList<Account> listA=null;
	
	private static ArrayList<Account> listB=null;
	
	private static boolean switchSPEI=false;
	
	private static String nonValidMessageIndicator = "";

	////////////////////////////// SPEI VALUES /////////////////////////
	
	private Context context;
	private int idNuevoCatalogo = 0;

	public static Boolean applicationLogged = Boolean.FALSE;
	
	public static Object bmovilApp;
///////////////////////Estado de cuenta

	private static ArrayList<CuentaEC> listaEC= new ArrayList<CuentaEC>();


	public static ArrayList<CuentaEC> getListaEC() {
		return listaEC;
	}

	public static void setListaEC(final ArrayList<CuentaEC> listaEC) {
		Session.listaEC = listaEC;
	}


	/**
	 * Public accessor method to the instance
	 * 
	 * @return the Session instance
	 */
	public static Session getInstance(final Context context) {
		if (Tools.isNull(theInstance)) {
			//inicio integracion Keychain Api LBM
			theInstance = new Session(context);
			//final File fexiste = new File("//data/data/com.bancomer.mbanking/keyChain.bks");
			final File  fexiste = new File(Environment.getExternalStorageDirectory() + "/.bancomer","keyChain.bks");
			try {
				//inicio integracion Keychain Api LBM
				kswrapper = KeyStoreWrapper.getInstance(context);

				if(!fexiste.exists()){
					kswrapper.setUserName(" ");
					kswrapper.setSeed(" ");
					kswrapper.storeValueForKey(Constants.CENTRO, " ");
					Runtime.getRuntime()
							.exec("chmod 777 /data/data/com.bancomer.mbanking/keyChain.bks");
				}
			} catch (IOException e) {
				//Monitor.sendData(context, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.APPLICATION_VERSION, DataSend.opcionesReactivacion.ERRFILE,null,"BMOVIL");
				if (Server.ALLOW_LOG) Log.e("Error KeyStore", "Session: Error al abrir o crear KeyStore");
			}


			//fin integracion
		}

		/*
		 * Sometimes the SO frees the Session object when pausing the
		 * application, we must restore it
		 */
		if (theInstance.getIum() == null
				&& applicationLogged) {
			theInstance.loadRecordStore();
			final String ium = Tools.buildIUM(theInstance.getUsername(), theInstance.getSeed(), context);
			theInstance.setIum(ium);
		}

		return theInstance;
	}
	
	
	
//////////////////////Getters and Setters from list A & B (& SWITCH INDICATOR)///////////////////////////
	
		public static ArrayList<Account> getListA()
		{
			return listA;
		}
		
		public static void setListA(final ArrayList<Account> list)
		{
			listA = list;
		}
		
		
		public static ArrayList<Account> getListB()
		{
			return listB;
		}
		
		public static void setListB(final ArrayList<Account> list)
		{
			listB = list;
		}
		
		public static boolean getSwitchSPEI()
		{
			return switchSPEI;
		}
		
		public static void setSwitchSPEI(final boolean value)
		{
			switchSPEI = value;
		}
		
		
/////////////////////////////End region///////////////////////////////////////////////////
	

	/**
	 * Session status unset (undefined, typically when the application starts or
	 * a login is in process)
	 */
	public static final int UNSET_STATUS = -1;

	/**
	 * Session status valid
	 */
	public static final int VALID_STATUS = 0;

	/**
	 * Session status invalid
	 */
	public static final int INVALID_STATUS = 1;

	/**
	 * Aplicacion en estado codigo de activacion
	 */
	public static final int APP_STATUS_ACTIVATION_CODE = -1;

	/**
	 * Aplicacion en estado ingresa datos
	 */
	public static final int APP_STATUS_MISSING_DATA = -2;

	/**
	 * Aplicacion en estado ingresa contraseña
	 */
	public static final int APP_STATUS_MISSING_PASSWORDS = -3;

	/**
	 * The current username
	 */
	private String username = null;
	/**
	 * The Estatus Alertas
	 */
	private String estatusAlertas = null;

	/**
	 * The password entered in login
	 */
	private String password = null;

	/**
	 * The client number, to identify the user in the server for session
	 * purposes
	 */
	private String clientNumber = null;

	/**
	 * The unique identifier of the user
	 */
	private String ium = null;

	/**
	 * The catalog versions(initially value 0)
	 */
	private String[] catalogVersions = { "0", "0", "0", "0", "0", "0", "0", "0" };// ahora
																				// solo
																				// son
																				// 8

	/**
	 * Vía 1 o 2
	 */
	private String via = "";

	/**
	 * The Bancomer token, for enhanced security in login operation
	 */
	private String bancomerToken = "";
	private String nombreCliente = "";
	private Constants.Perfil clientProfile;
	private String securityInstrument;
	private String serviceStatus;
	private String estatusIS;
	
	/**
	 * Flag acepta cambio de perfil
	 */
	private boolean aceptaCambioPerfil = true;

	/**
	 * Flag para mostrar el menu hambuerguesa desde las opciones del menu administrar
	 */
	private boolean cmbPerfilHamburguesa = false;
	
	public boolean isCmbPerfilHamburguesa() {
		return cmbPerfilHamburguesa;
	}

	public void setCmbPerfilHamburguesa(final boolean cmbPerfilHamburguesa) {
		this.cmbPerfilHamburguesa = cmbPerfilHamburguesa;
	}

	/**
	 * Lista de rápidos.
	 */
	private Rapido[] rapidos = null;
	
	//One click
		/**
		 * Lista de promociones.
		 */
		private Promociones[] promocion = null;

	//Termina One click

	/**
	 * The user accounts
	 */
	private Account[] accounts = null;

	/**
	 * The catalogs
	 */
	private Catalog[] catalogs = new Catalog[4];// ahora solo son 4 catalogos genericos
	private CatalogoVersionado catalogoTiempoAire; // Nueva definicion de catalogo
	private CatalogoVersionado catalogoDineroMovil; // Nueva definicion de catalogo
	private CatalogoVersionado catalogoServicios; // Nueva definicion de catalogo
	private CatalogoVersionado catalogoMantenimientoSPEI; // Nueva definicion de catalogo

	/**
	 * Index of the TDD banks catalog
	 */
	public static final int TDD_BANKS_CATALOG = 0;

	/**
	 * Index of the TDC banks catalog
	 */
	public static final int TDC_BANKS_CATALOG = 1;

	/**
	 * Index of the opening hours catalog
	 */
	public static final int OPENING_HOURS_CATALOG = 2;

	/**
	 * Index of the Help catalog
	 */
	public static final int AYUDAS_CATALOG = 3;

	/**
	 * Index of the cell company catalog
	 */
	public static final int TA_COMPANY_CATALOG = 4;

	/**
	 * Index of the catalog amounts
	 */
	public static final int DM_CATALOG = 5;

	/**
	 * Index of the services catalog
	 */
	public static final int SERVICES_CATALOG = 6;

	/**
	 * The server response date
	 */
	private long serverDate = 0;

	/**
	 * The server response time
	 */
	private long localReferenceDate = 0;

	/**
	 * Contiene el estatus pendiente de la aplicación
	 */
	private int pendingStatus;

	/**
	 * Flag to determine if the application is activated
	 */
	private boolean applicationActivated = false;

	/**
	 * Activation code
	 */
	private String activationCode = null;

	/**
	 * Random seed to generate the IUM
	 */
	private long seed = 0;

	/**
	 * Compañia telefonica del usuario
	 */
	private String companiaUsuario;

	/**
	 * Session validity status. One of UNSET_STATUS, VALID_STATUS,
	 * INVALID_STATUS
	 */
	private int validity = UNSET_STATUS;

	/**
	 * Session timeout in milis
	 */

	private int timeout = 60000;
	//private int timeout = 600000000; // FIXME: Eliminar para ejecucion real

	/**
	 * Correo del cliente.
	 */
	private String email;


	/**
	 * For persistent data storing
	 */
	DBAdapter adapter = null;

	/**
	 * This flag will be true if the catalogs has been updated but not saved to
	 * recorstore yet
	 */
	private boolean catalogsUpdated = false;

	private String title = "";

	/**
	 * Activation code record type for RMS persistence
	 */
	private static final int ACTIVATION_CODE_RMS_ID = 0;

	/**
	 * Activated record type for RMS persistence
	 */
	private static final int ACTIVATED_RMS_ID = 1;
	
	/**
	 * Login record type for RMS persistence
	 */
	private static final int LOGIN_RMS_ID = 2;

	/**
	 * Seed record type for RMS persistence
	 */
	private static final int SEED_RMS_ID = 5;

	/**
	 * via 1 o 2
	 */
	private static final int VIA_RMS_ID = 6;

	/**
	 * Bancomer Token record type for RMS persistence
	 */
	private static final int TOKEN_RMS_ID = 7;

	/**
	 * Bancomer Token record type for RMS persistence
	 */
	private static final int PENDING_STATUS_RMS_ID = 8;

	/**
	 * Softoken record type for RMS persistence
	 */
	private static final int SOFTTOKEN_RMS_ID = 11;

	/**
	 * First catalog record type for RMS persistence (It's mandatory that the
	 * value of this constant and the values of the rest of the catalogX_rms_id
	 * constants are consecutive)
	 */

	/**
	 * TA catalog record type for RMS persistence
	 */
	private static final int TA_CATALOG_RMS_ID = 104;

	/**
	 * Dinero Movil catalog record type for RMS persistence
	 */
	private static final int DM_CATALOG_RMS_ID = 105;

	/**
	 * Servicios catalog record type for RMS persistence
	 */
	private static final int SERVICES_CATALOG_RMS_ID = 106;
	
	private static final int MANTENIMIENTO_SPEI_CATALOG_RMS_ID = 107;

	/**
	 * First catalog version record type for RMS persistence (It's mandatory
	 * that the value of this constant and the values of the rest of the
	 * catalogX_version_rms_id constants are consecutive) 
	 */
	private static final int CATALOG1_VERSION_RMS_ID = 300;

	/**
	 * Fourth catalog version record type for RMS persistence
	 */
	private static final int CATALOG4_VERSION_RMS_ID = 301;

	/**
	 * Fifth catalog record type for RMS persistence
	 */
	private static final int CATALOG5_VERSION_RMS_ID = 302;
	

	/**
	 * Eighth catalog record type for RMS persistence
	 */
	private static final int CATALOG8_VERSION_RMS_ID = 303;

	/**
	 * TA catalog record type for RMS persistence
	 */
	private static final int TA_CATALOG_VERSION_RMS_ID = 304;

	/**
	 * DM catalog record type for RMS persistence
	 */
	private static final int DM_CATALOG_VERSION_RMS_ID = 305;

	/**
	 * Services catalog record type for RMS persistence
	 */
	private static final int SERVICES_CATALOG_VERSION_RMS_ID = 306;
	
	private static final int MANTENIMIENTO_SPEI_VERSION_CATALOG_RMS_ID = 307;

	/**
	 * Default constructor
	 */
	private Session(final Context context) {
		this.context = context;
		loadRecordStore();
		CatalogoAutenticacionFileManager.getCurrent().cargaCatalogoAutenticacion();
		loadCatalogoCompaniasTelefonicas();
		loadCatalogoHorasServicio();
	}

	/**
	 * Operation that must be called before closing the application. Stores all
	 * all data associated with the session
	 */
	public void storeSession(final SessionStoredListener listener) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				saveRecordStore();
				if (listener != null) {
					listener.sessionStored();
				}
			}
		}).start();
	}
	/**
	 * Operation that must be called before closing the application. Stores all
	 * all data associated with the session
	 */
	public void storeSession() {
				saveRecordStore();
	}


	/**
	 * Get the username
	 * 
	 * @return the username
	 */
	public String getUsername() {
		return username;		
	}

	/**
	 * Set the username
	 * 
	 * @param username
	 */
	public void setUsername(final String username) {
		this.username = username;
	}

	/**
	 * Get the password
	 * 
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Set the password
	 * 
	 * @param password
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * Get the client number
	 * 
	 * @return the client number for session synchronization purposes
	 */
	public String getClientNumber() {
		return clientNumber;
	}

	/**
	 * Set the client number
	 * 
	 * @param clientNumber
	 *            the client number for session synchronization purposes
	 */
	public void setClientNumber(final String clientNumber) {
		this.clientNumber = clientNumber;
	}

	/**
	 * Set the server date and time
	 * 
	 * @param date
	 *            the server date
	 * @param time
	 *            the server time
	 */
	public void setServerDateTime(final String date, final String time) {
		// System.out.println("Set server date:" + date + " time: " + time);
		this.localReferenceDate = System.currentTimeMillis();
		this.serverDate = Tools.parseDateTime(date, time);
		// System.out.println("The parsed date is: " + new
		// Date(serverDate).toString());
	}

	/**
	 * Get the current server date and time
	 * 
	 * @return the current server date and time, taking as reference the time
	 *         reported by the server in the login and the time elapsed since
	 *         the login operation
	 */
	public Date getServerDate() {

		final long time = System.currentTimeMillis() - this.localReferenceDate
				+ this.serverDate;
		
		final Date result = new Date(time);
		//System.out.println("The current server date time is "
			//	+ result.toString());
		return result;
	}

	/**
	 * Get the unique identifier for network operations security, in order to
	 * uniquely identify the application installation
	 * 
	 * @return the unique identifier
	 */
	public String getIum() {

	/*	if(ium!=null){
			//if(Server.ALLOW_LOG) Log.d("IUM", " IUM: "+ium);
		}else{
			//if(Server.ALLOW_LOG) Log.d("IUM", " IUM: null");
		}*/
		
		return ium;		
	}

	/**
	 * Set the unique identifier for network operations security, in order to
	 * uniquely identify the application installation
	 * 
	 * @param ium
	 *            the unique identifier
	 */
	public void setIum(final String ium) {

		this.ium = ium;
	}

	/**
	 * Get via
	 * 
	 * @return via
	 */
	public String getVia() {
		return via;
	}

	/**
	 * Set vía
	 * 
	 * @param v
	 *            �a
	 */
	public void setVia(final String via) {
		this.via = via;
	}

	/**
	 * Get the Bancomer token
	 * 
	 * @return the Bancomer token
	 */
	public String getBancomerToken() {
		return bancomerToken;
	}

	/**
	 * Set the Bancomer token
	 * 
	 * @param token
	 *            the Bancomer token
	 */
	public void setBancomerToken(final String token) {
		this.bancomerToken = token;
	}

	public String getCompaniaUsuario() {
		return companiaUsuario;
	}

	public void setCompaniaUsuario(final String companiaUsuario) {
		this.companiaUsuario = companiaUsuario;
	}

	public void setServerDate(final String date) {
		// Empty method
	}

	/**
	 * Update the amount and server date of an account
	 * 
	 * @param account
	 *            the account to update
	 */
	public void updateAccount(final Account account) {
		if (account == null) {
			return;
		}
		
		boolean found = false;
		for (int i = 0; (i < this.accounts.length && !found); i++) {
			if (this.accounts[i].getNumber().equals(account.getNumber())) {
				found = true;
				this.accounts[i].setBalance(account.getBalance());
				// update the date
				final long date = this.serverDate + (System.currentTimeMillis() - this.localReferenceDate);
				final String balanceDate = Tools.formatDate(date);
				this.accounts[i].setDate(balanceDate);
			}
		}
	}


	/**
	 * Get the bank list
	 * 
	 * @param forCreditCard
	 *            true if credit card bank
	 * @return the bank list
	 */
	public Catalog getBanks(final boolean forCreditCard) {
		if (forCreditCard) {
			return this.catalogs[TDC_BANKS_CATALOG];
		} else {
			return this.catalogs[TDD_BANKS_CATALOG];
		}
	}

	/**
	 * Get the help list
	 * 
	 * @return the help list
	 */
	public Catalog getHelps() {
		return this.catalogs[AYUDAS_CATALOG];
	}

	/**
	 * Get a list of the payable services
	 * 
	 * @return the catalog of the payable services
	 */
	public Catalog getServices() {
		return this.catalogs[SERVICES_CATALOG];
	}

	/**
	 * Get the activation code
	 * 
	 * @return the activation code
	 */
	public String getActivationCode() {
		return activationCode;
	}

	/**
	 * Set the activation code
	 * 
	 * @param activationCode
	 *            the activation code
	 */
	public void setActivationCode(final String activationCode) {
		this.activationCode = activationCode;
	}

	/**
	 * Obtiene el estado pendiente actual de la aplicación
	 * 
	 * @return el estado pendiente
	 */
	public int getPendingStatus() {
		return pendingStatus;
	}

	/**
	 * Establece el estado pendiente actual de la aplicación
	 * 
	 * @param pendingStatus
	 *            el estado pendiente actual
	 */
	public void setPendingStatus(final int pendingStatus) {
		this.pendingStatus = pendingStatus;
	}

	/**
	 * Get if the application has been activated
	 * 
	 * @return true if the application has been activated, false if not
	 */
	public boolean isApplicationActivated() {
		return applicationActivated;		
	}

	/**
	 * Set if the application has been activated
	 * 
	 * @param applicationActivated
	 *            true if the application has been activated, false if not
	 */
	public void setApplicationActivated(final boolean applicationActivated) {
		this.applicationActivated = applicationActivated;

		PropertiesManager.getCurrent().setBmovilActivated(applicationActivated);
	}

	/**
	 * Get if the softtoken has been activated
	 * 
	 * @return true if the application has been activated, false if not
	 */
	public boolean isSofttokenActivado() {
		return SuiteApp.getSofttokenStatus();
	}

	/**
	 * Get the full account list for the user
	 * 
	 * @return the full account list for the user
	 */
	/*public Account[] getAccounts() {
		return accounts;
	}*/

	/**
	 * Set the full account list for the user
	 * 
	 * @param accounts
	 *            the full account list for the user
	 * @param serverDate
	 *            the server date
	 * @param serverTime
	 *            the server time
	 */
	public void setAccounts(final Account[] accounts, final String serverDate,
			final String serverTime) {
		setServerDateTime(serverDate, serverTime);
		final String formattedServerDate = Tools.formatDate(serverDate);
		if(Tools.isNull(accounts)) {
			this.accounts = new Account[0];
		} else {
			this.accounts = accounts.clone();
		}
		for (int i = 0; i < accounts.length; i++) {
			accounts[i].setDate(formattedServerDate);
		}
	}

	public void setAccounts(final Account... accounts) {
		this.accounts=accounts;
	}
	/**
	 * Get all the accounts to get money from (the visible ones)
	 * 
	 * @return a list with all the accounts to get money from
	 */
	public ArrayList<Account> getOriginAccounts() {
		return this.getVisibleAccounts();
	}

	/**
	 * Get all the accounts with destination concept (to put money to)
	 * 
	 * @return all the accounts with destination concept (to put money to)
	 */
	public Account[] getDestinationAccounts() {
		if(Tools.isNull(accounts)) {
			return new Account[0];
		} else {
			return this.accounts.clone();
		}
	}

	/**
	 * Returns the destination accounts in a internal transfer valid for the
	 * given origin account
	 * 
	 * @param originAccount
	 *            an origin account from the InternalTransferOrigin
	 * @return An array containing the destination accounts
	 */
	public ArrayList<Account> getDestinationAccounts(final Account originAccount) {

		final ArrayList<Account> result = new ArrayList<Account>();
		final Account[] destinationAccounts = getDestinationAccounts();

		final String originType = originAccount.getType();
		final String originNumber = originAccount.getNumber();

		for (final Account account : destinationAccounts) {
			final String type = account.getType();
			final String number = account.getNumber();

			if (!type.equals(originType) || !number.equals(originNumber)) {
				if (!Constants.CREDIT_TYPE.equals(originType)
						|| !Constants.CREDIT_TYPE.equals(type)) {
					result.add(account);
				}
			}
		}

		return result;

	}

	/**
	 * Get all the accounts marked as visible
	 * 
	 * @return all the accounts marked as visible
	 */
	public ArrayList<Account> getVisibleAccounts() {
		final ArrayList<Account> visibleAccounts = new ArrayList<Account>(5);

		for (final Account a : accounts) {
			if (a.isVisible()) {
				visibleAccounts.add(a);
			}
		}

		return visibleAccounts;

	}

	/**
	 * Get if the user can do internal transfers, i.e., the user has Vista
	 * accounts as origin.
	 * 
	 * @return true if the user can do internal transfers, false if not
	 */
	public boolean canDoInternalTransfers() {
		boolean hasVistaAccounts = false;
		final Account[] accounts = getAccounts();
		if (accounts != null) {
			for (int i = 0; ((i < accounts.length) && (!hasVistaAccounts)); i++) {
				final String type = accounts[i].getType();
				// System.out.println("Visible account type = " + type);
				hasVistaAccounts = (!Constants.CREDIT_TYPE.equals(type));
			}
		}
		return hasVistaAccounts;

	}

	/**
	 * Get the seed to generate the IUM
	 * 
	 * @return the seed to generate the IUM
	 */
	public long getSeed() {
		
		//	//if(Server.ALLOW_LOG) Log.d("IUM", " SEED: "+seed);
		
		return seed;
	}

	/**
	 * Set the seed to generate the IUM
	 * 
	 * @param seed
	 *            the seed to generate the IUM
	 */
	public void setSeed(final long seed) {
		this.seed = seed;
	}

	/**
	 * Get the validity status of the current session.
	 * 
	 * @return the validity status of the current session. One of UNSET_STATUS,
	 *         VALID_STATUS, INVALID_STATUS
	 */
	public int getValidity() {
		return validity;
	}

	/**
	 * Set the validity status of the current session.
	 * 
	 * @param validity
	 *            the validity status of the current session. One of
	 *            UNSET_STATUS, VALID_STATUS, INVALID_STATUS
	 */
	public void setValidity(final int validity) {
		// System.out.println("Set validity = " + validity);
		this.validity = validity;
	}

	/**
	 * Get the local session timeout
	 * 
	 * @return the local session timeout, in milis
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * Set the local session timeout
	 * 
	 * @param timeout
	 *            the local session timeout, in milis
	 */
	public void setTimeout(final int timeout) {
		this.timeout = timeout;
	}

	/**
	 * Get the title header
	 * 
	 * @return the local session title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Set the title header
	 * 
	 * @param title
	 *            the local session title
	 */
	public void setTitle(final String title) {
		this.title = title;
	}

	public Constants.Perfil getClientProfile() {
		return clientProfile;
	}

	public String getSecurityInstrument() {
		return securityInstrument;
	}

	public String getServiceStatus() {
		return serviceStatus;
	}

	public void setClientProfile(final Constants.Perfil clientProfile) {
		this.clientProfile = clientProfile;
	}

	/*public boolean isAdvancedProfile() {
		if (Constants.Perfil.avanzado.equals(getClientProfile())) {
			return true;
		} else {
			return false;
		}
	}*/

	public void setSecurityInstrument(final String securityInstrument) {
		this.securityInstrument = securityInstrument;
	}

	public String getEstatusIS() {
		return estatusIS;
	}

	public void setEstatusIS(final String estatusIS) {
		this.estatusIS = estatusIS;
	}

	public void setServiceStatus(final String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	public Boolean getAceptaCambioPerfil() {
		return aceptaCambioPerfil;
	}

	public void setAceptaCambioPerfil(final Boolean aceptaCambioPerfil) {
		this.aceptaCambioPerfil = aceptaCambioPerfil;
	}

	/**
	 * Check if the current server date is between the available operation time,
	 * which is "hard coded" to Monday to Friday from 8:40 to 16:00
	 * 
	 * @return true if the current server date is between the available
	 *         operation hours, false if not
	 */
	public boolean isOpenHoursForExternalTransfers() {
		return isOpenHours("IB");
	}
	

	public boolean isOpenHoursForExternalTransfersUsingTC() {
		return isOpenHours();
	}

	public boolean isOpenHoursForServicePayments() {
		return isOpenHours("PS");
	}
	
	
	public boolean isWeekend(){
		String day="";
		final Calendar c = Calendar.getInstance();
		c.setTime(getServerDate());
		final int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		
		switch (dayOfWeek) {
		case Calendar.MONDAY:
			day = "lunes";
			break;
		case Calendar.TUESDAY:
			day = "martes";
			break;
		case Calendar.WEDNESDAY:
			day = "miercoles";
			break;
		case Calendar.THURSDAY:
			day = "jueves";
			break;
		case Calendar.FRIDAY:
			day = "viernes";
			break;
		case Calendar.SATURDAY:
			day = "sabado";
			break;
		case Calendar.SUNDAY:
			day = "domingo";
			break;
		}
		
		
		if(day.equals("domingo") || day.equals("sabado"))
			return true;
		
		return false;
	}

	private boolean isOpenHours(final String prefix) {
		final Calendar c = Calendar.getInstance();
		c.setTime(getServerDate());
		final int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		String day = "";
		String dayForNonValid="";
		// New code Added
		if(prefix.equals("IB"))
		{
			if(listA.size()>0)
				day="speiCelular";
			else
				day="spei";			
		}
		else
			day="servicios";
		//End new Code Added
		
		switch (dayOfWeek) {
		case Calendar.MONDAY:
			//day = "1";
			day += "lunes";
			dayForNonValid="lunes";
			break;
		case Calendar.TUESDAY:
			//day = "2";
			day += "martes";
			dayForNonValid="martes";
			break;
		case Calendar.WEDNESDAY:
			//day = "3";
			day += "miercoles";
			dayForNonValid="miercoles";
			break;
		case Calendar.THURSDAY:
			//day = "4";
			day += "jueves";
			dayForNonValid="jueves";
			break;
		case Calendar.FRIDAY:
			//day = "5";
			day += "viernes";
			dayForNonValid="viernes";
			break;
		case Calendar.SATURDAY:
			//day = "6";
			day += "sabado";
			dayForNonValid="sabado";
			break;
		case Calendar.SUNDAY:
			//day = "7";
			day += "domingo";
			dayForNonValid="domingo";
			break;
		}

		//String identifier = prefix + day;
		
		/*Catalog openingHoursCatalog = getCatalog(OPENING_HOURS_CATALOG);
		if (openingHoursCatalog == null) {
			return false;
		}

		boolean found = false;
		int size = openingHoursCatalog.size();

		String openingHoursStr = null;
		for (int i = 0; ((i < size) && (!found)); i++) {
			NameValuePair item = openingHoursCatalog.getItem(i);			
			//if (item.getName().equals(identifier)) {
			if (item.getName().equals(day)) {
				found = true;
				openingHoursStr = item.getValue();
				//if(Server.ALLOW_LOG) Log.e("Valor de la cadena",openingHoursStr);
			}
		}*/
		
		//Vector<Object> vectorHoras = loadCatalogoHorasServicio().getObjetos();
		final Vector<Object> vectorHoras = catalogoHoras.getObjetos();
		
		if (vectorHoras == null) {
			return false;
		}
		
		boolean found = false;
		final int size = vectorHoras.size();
		boolean result = false;
		
		
		if(listA.size()>0 && prefix.equals("IB"))
		{
			
			//35 is the number of elements catalogoHoras would contain whether it has "speiCelularInvalido" elements, otherwise it would have less than 35 elements. 
			if(size < 35)
			{
				return true;
			}
			else
			{
				String nonValidHourOne = null;
				String nonValidHourTwo = null;
				
				
				for (int i = 0; ((i < size) && (!found)); i++) {			
					final Schedule schedule = (Schedule)vectorHoras.get(i);
					if (schedule.getDay().equals(Constants.SPEI_CELULAR_INVALIDO+dayForNonValid+Constants.NON_VALID_ONE)) {
						nonValidHourOne =  schedule.getHour();
					}
					else if(schedule.getDay().equals(Constants.SPEI_CELULAR_INVALIDO+dayForNonValid+Constants.NON_VALID_TWO))
					{
						nonValidHourTwo = schedule.getHour();
						found = true;
					}
				}
				
				
				try {
					final int initHourOne = Integer.parseInt(nonValidHourOne.substring(0, 2));
					final int initMinuteOne = Integer.parseInt(nonValidHourOne.substring(2, 4));
					final int endHourOne = Integer.parseInt(nonValidHourOne.substring(4, 6));
					final int endMinuteOne = Integer.parseInt(nonValidHourOne.substring(6, 8));
					
					final int initHourTwo = Integer.parseInt(nonValidHourTwo.substring(0, 2));
					final int initMinuteTwo = Integer.parseInt(nonValidHourTwo.substring(2, 4));
					final int endHourTwo = Integer.parseInt(nonValidHourTwo.substring(4, 6));
					final int endMinuteTwo = Integer.parseInt(nonValidHourTwo.substring(6, 8));


					final int hour = c.get(Calendar.HOUR_OF_DAY);
					final int minute = c.get(Calendar.MINUTE);
					final int currentTime = (hour * 60) + minute;
					
					//The above lines are used for simulate nonValid hours.
					//int currentTime = (endHourTwo * 60) + endMinuteTwo;
					//int currentTime = (endHourOne * 60) + endMinuteOne;
					
					final int initTimeOne = (initHourOne * 60) + initMinuteOne;
					final int endTimeOne = (endHourOne * 60) + endMinuteOne;
					final int initTimeTwo = (initHourTwo * 60) + initMinuteTwo;
					final int endTimeTwo = (endHourTwo * 60) + endMinuteTwo;
					
					if(((currentTime >= initTimeOne) && (currentTime <= endTimeOne)))
					{
						nonValidMessageIndicator=Constants.textFornonValidOne;
					}else if (((currentTime >= initTimeTwo) && (currentTime <= endTimeTwo)))
					{
						nonValidMessageIndicator=Constants.textFornonValidTwo;
					}

					result = !(((currentTime >= initTimeOne) && (currentTime <= endTimeOne))||((currentTime >= initTimeTwo) && (currentTime <= endTimeTwo)));

				} catch (Throwable th) {
					ToolsCommons.writeLoge(Session.class.getSimpleName(), th.getMessage(), ServerCommons.ALLOW_LOG);
				}
				
			}
			
	
			
			
			
			
		}
		else
		{
			String openingHoursStr = null;
			for (int i = 0; ((i < size) && (!found)); i++) {			
				final Schedule schedule = (Schedule)vectorHoras.get(i);
				if (schedule.getDay().equals(day)) {
					//if (schedule.getDay().equals("speidomingo")) {
					found = true;
					openingHoursStr = schedule.getHour();
					//if(Server.ALLOW_LOG) Log.e("hora",openingHoursStr.toString());
				}
			}

			if ((!found) || (openingHoursStr == null)
					|| (openingHoursStr.equals("00000000"))) {
				return false;
			}


			// parse the time range
			try {
				final int initHour = Integer.parseInt(openingHoursStr.substring(0, 2));
				final int initMinute = Integer.parseInt(openingHoursStr.substring(2, 4));
				final int endHour = Integer.parseInt(openingHoursStr.substring(4, 6));
				final int endMinute = Integer.parseInt(openingHoursStr.substring(6, 8));


				final int hour = c.get(Calendar.HOUR_OF_DAY);
				final int minute = c.get(Calendar.MINUTE);
				final int currentTime = (hour * 60) + minute;
				final int initTime = (initHour * 60) + initMinute;
				final int endTime = (endHour * 60) + endMinute;

				result = ((currentTime >= initTime) && (currentTime <= endTime));

			} catch (Throwable th) {
				ToolsCommons.writeLoge(Session.class.getSimpleName(), th.getMessage(), ServerCommons.ALLOW_LOG);
			}
		}
		
		return result;

	}
	
	
	
	
	
	private boolean isOpenHours() {
		
		final Calendar c = Calendar.getInstance();
		c.setTime(getServerDate());
		final int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		String day = "spei";
		
		
		
		switch (dayOfWeek) {
		case Calendar.MONDAY:
			day += "lunes";
			break;
		case Calendar.TUESDAY:
			day += "martes";
			break;
		case Calendar.WEDNESDAY:
			day += "miercoles";
			break;
		case Calendar.THURSDAY:
			day += "jueves";
			break;
		case Calendar.FRIDAY:
			day += "viernes";
			break;
		case Calendar.SATURDAY:
			day += "sabado";
			break;
		case Calendar.SUNDAY:
			day += "domingo";
			break;
		}

		
		
		//Vector<Object> vectorHoras = loadCatalogoHorasServicio().getObjetos();
		final Vector<Object> vectorHoras = catalogoHoras.getObjetos();
		
		//Catalog openingHoursCatalog = getCatalog(OPENING_HOURS_CATALOG);
		//if (openingHoursCatalog == null) {
		if (vectorHoras == null) {
			return false;
		}

		//boolean found = false;
		//int size = openingHoursCatalog.size();
		boolean found = false;
		final int size = vectorHoras.size();
		

		String openingHoursStr = null;
		for (int i = 0; ((i < size) && (!found)); i++) {
			//NameValuePair item = openingHoursCatalog.getItem(i);
			final Schedule schedule = (Schedule)vectorHoras.get(i);
			/*if (item.getName().equals(day)) {
				found = true;
				openingHoursStr = item.getValue();
			}*/
			if (schedule.getDay().equals(day)) {
			//if (schedule.getDay().equals("speidomingo")) {
				found = true;
				openingHoursStr = schedule.getHour();
				//if(Server.ALLOW_LOG) Log.e("hora2",openingHoursStr.toString());
			}
		}
		
		
		if ((!found) || (openingHoursStr == null)
				|| (openingHoursStr.equals("00000000"))) {
			return false;
		}

		boolean result = false;
		// parse the time range
		try {
			final int initHour = Integer.parseInt(openingHoursStr.substring(0, 2));
			final int initMinute = Integer.parseInt(openingHoursStr.substring(2, 4));
			final int endHour = Integer.parseInt(openingHoursStr.substring(4, 6));
			final int endMinute = Integer.parseInt(openingHoursStr.substring(6, 8));
			
			
			final int hour = c.get(Calendar.HOUR_OF_DAY);
			final int minute = c.get(Calendar.MINUTE);
			final int currentTime = (hour * 60) + minute;
			final int initTime = (initHour * 60) + initMinute;
			final int endTime = (endHour * 60) + endMinute;
			
			result = ((currentTime >= initTime) && (currentTime <= endTime));

		} catch (Throwable th) {
			ToolsCommons.writeLoge(Session.class.getSimpleName(), th.getMessage(), ServerCommons.ALLOW_LOG);
		}
		
		//if(Server.ALLOW_LOG) Log.e("fin método de validación","b");
		//if(Server.ALLOW_LOG) Log.e("valor Result",String.valueOf(result));
		

		return result;

	}

	/**
	 * Method for getting the message to show in screen when accessing the
	 * external transfer menu outside the open hours using a TC account.
	 * 
	 * @return String with the message
	 */
	
	public String getOpenHoursForExternalTransfersMessageUsingTC() {
		return getOpenHoursMessage("TC");
	}
	

	/**
	 * Method for getting the message to show in screen when accessing the
	 * external transfer menu outside the open hours.
	 * 
	 * @return String with the message
	 */
	public String getOpenHoursForExternalTransfersMessage() {
		return getOpenHoursMessage("IB");
	}

	/**
	 * Method for getting the message to show in screen when accessing the
	 * services payment menu outside the open hours.
	 * 
	 * @return String with the message
	 */
	public String getOpenHoursForServicePaymentsMessage() {
		return getOpenHoursMessage("PS");
	}

	/**
	 * Method for getting the message to show in screen when accessing some
	 * menus outside the open hours.
	 * 
	 * @param prefix
	 *            String "PS" if accessing service payments or "IB" if accessing
	 *            external transfers
	 * @return String with the message
	 */
	public String notAssociatedMessage()
	{
		//return "Para realizar transferencias en fin de semana es necesario asociar tu celular.";
		return "Para realizar transferencias en días inhábiles debes asociar tu celular a tu cuenta en la opción \"Asociar número celular\", desde el menú administrar";
	}
	
	private String getOpenHoursMessage(String prefix) {
		//StringBuffer currentDate = new StringBuffer();
		//if(Server.ALLOW_LOG) Log.e("Prefix Value",prefix.toString());
		// New code Added
		if(prefix.equals("IB") && listA.size()>0)
		{
			if(nonValidMessageIndicator.equals(Constants.textFornonValidOne))
			{
				return context.getString(R.string.nonValidOneMessage);
			}
			else
			{
				return context.getString(R.string.nonValidTwoMessage);
			}
		}
		
		
		final String tipoCuenta = prefix;
		
		if(prefix.equals("IB"))
		{
			if(listA.size()>0)
				prefix="speiCelular";
			else
				prefix="spei";			
		}
		else if(prefix.equals(Constants.TP_TC_VALUE))
			prefix="spei";
		else
			prefix="servicios";
		//End new Code Added

		final Calendar c = Calendar.getInstance();
		c.setTime(getServerDate());
		final int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

		switch (dayOfWeek) {
		case Calendar.MONDAY:
			//currentDate.append(context.getString(R.string.calendar_monday));
			prefix+="lunes";
			break;
		case Calendar.TUESDAY:
			//currentDate.append(context.getString(R.string.calendar_tuesday));
			prefix+="martes";
			break;
		case Calendar.WEDNESDAY:
			//currentDate.append(context.getString(R.string.calendar_wednesday));
			prefix+="miercoles";
			break;
		case Calendar.THURSDAY:
			//currentDate.append(context.getString(R.string.calendar_thursday));
			prefix+="jueves";
			break;
		case Calendar.FRIDAY:
			//currentDate.append(context.getString(R.string.calendar_friday));
			prefix+="viernes";
			break;
		case Calendar.SATURDAY:
			//currentDate.append(context.getString(R.string.calendar_saturday));
			prefix+="sabado";
			break;
		case Calendar.SUNDAY:
			//currentDate.append(context.getString(R.string.calendar_sunday));
			prefix+="domingo";
			break;
		}

		/*int dayOfMonth = c.get(Calendar.DAY_OF_MONTH);
		String dayOfMonthStr = (dayOfMonth >= 10) ? String.valueOf(dayOfMonth)
				: "0" + String.valueOf(dayOfMonth);
		int month = c.get(Calendar.MONTH) + 1;

		String monthStr = (month >= 10) ? String.valueOf(month) : "0"
				+ String.valueOf(month);
		currentDate.append(" ").append(dayOfMonthStr).append("/")
				.append(monthStr);*/

		final String openingHours = getOpenHourStringFromCatalog(prefix);
		if ((openingHours == null) || (openingHours.equals("00000000"))
				|| (openingHours.length() < 8)){
			
			//se valida para cuando el tipo de cuenta destino es TC ya que no debe 
			//realizar operaciones en fin de semana aun cuando la cuenta cargo tenga un celular asociado
			if(tipoCuenta.equals(Constants.TP_TC_VALUE))
				return context.getString(R.string.calendar_availableHoursTC);
			else			
				return context.getString(R.string.calendar_noAvailable);
		}

		final String openHourString = openingHours.substring(0, 2) + ":"
				+ openingHours.substring(2, 4);
		final String closeHourString = openingHours.substring(4, 6) + ":"
				+ openingHours.substring(6, 8);

		if(tipoCuenta.equals("IB"))
			return context.getString(R.string.calendar_availableHoursIB);
		else
			return String.format(
					context.getString(R.string.calendar_availableHours),
					Tools.getCurrentDate(), openHourString, closeHourString);

	}
	/**
	 * Returns the hour string obtained from the catalog for the given operation
	 * prefix.
	 * 
	 * @param prefix
	 *            the operation prefix
	 * @return the catalog schedule or null if not found
	 */
	private String getOpenHourStringFromCatalog(final String prefix) {

		//Vector<Object> vectorHoras = loadCatalogoHorasServicio().getObjetos();
		final Vector<Object> vectorHoras = catalogoHoras.getObjetos();
		
		if (vectorHoras == null) {
			return null;
		}

		final int size = vectorHoras.size();
				
		for (int i = 0;i < size; i++) {			
			final Schedule schedule = (Schedule)vectorHoras.get(i);
			if (schedule.getDay().equals(prefix)) {
				return schedule.getHour();
			}
		}
		
		
		/*Catalog openingHoursCatalog = getCatalog(OPENING_HOURS_CATALOG);
		if (openingHoursCatalog == null) {
			return null;
		}*/

		/*Calendar c = Calendar.getInstance();
		c.setTime(getServerDate());
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

		String identifier = (dayOfWeek == Calendar.SUNDAY) ? prefix + "7"
				: prefix + (dayOfWeek - 1);*/

		//int size = openingHoursCatalog.size();
		//for (int i = 0; i < size; i++) {
		/*for (int i = 0; i < openingHoursCatalog.size(); i++) {
			NameValuePair item = openingHoursCatalog.getItem(i);
			//if (item.getName().equals(identifier)) {
			if (item.getName().equals(prefix)) {
				return item.getValue();
			}
		}*/

		return null;
	}

	public String getEstatusAlertas() {
		return estatusAlertas;
	}

	public void setEstatusAlertas(final String estatusAlertas) {
		this.estatusAlertas = estatusAlertas;
	}

	/**
	 * @return Correo del cliente.
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param eMail
	 *            Correo del cliente.
	 */
	public void setEmail(final String email) {
		this.email = email;
	}

	public String getNombreCliente() {
		return nombreCliente;
	}

	public void setNombreCliente(String nombreCliente) {
		nombreCliente=nombreCliente.replace("ÃÂ","Ñ");
		this.nombreCliente = nombreCliente;
	}

	// ///////////////////////////////////////////////////////////////////////////
	// Recordstore management //
	// ///////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * Open or create the file
	 * 
	 * @return boolean false on error
	 */
	synchronized private boolean openRecordStore() {
		try {

			adapter = new DBAdapter(this.context);
			adapter.open();

		} catch (Throwable t) {
			if(ServerCommons.ALLOW_LOG) Log.e("Session.java", "Error openRecordStore()");
			if(ServerCommons.ALLOW_LOG) t.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Closes a previously opened recordstore
	 * 
	 * @return boolean false on error
	 */
	synchronized private boolean closeRecordStore() {
		if (adapter != null) {
			try {
				adapter.close();
			} catch (Throwable t) {
				if(ServerCommons.ALLOW_LOG) Log.e("Session.java", "Error closeRecordStore()");
				return false;
			} finally {
				adapter = null;
			}
			return true;

		} else {
			return false;
		}
	}
	

	/**
	 * Loads data from recordstore.
	 */
	synchronized public final void loadRecordStore() {
		
		// this will store temporarily the loaded catalog versions
		String loadedCatalogVersions[] = { null, null, null, null, null, null, null, null };// Ahora son 8
			
		//WARNING! DO NOT COMMENT OR DO NOT DELETE.
		//FOR UPGRADE APP PORPOUSE.
		openRecordStore();
		closeRecordStore();
			
		try {

				setApplicationActivated(DatosBmovilFileManager.getCurrent().getActivado());
				setPendingStatus(DatosBmovilFileManager.getCurrent().getPendienteDeDescarga());
				setUsername(DatosBmovilFileManager.getCurrent().getLogin());
				setSeed(DatosBmovilFileManager.getCurrent().getSeed());
				setVia(DatosBmovilFileManager.getCurrent().getVia());
				setBancomerToken(DatosBmovilFileManager.getCurrent().getToken());

				loadedCatalogVersions[0] = DatosBmovilFileManager.getCurrent().getCatalogo1S();
				loadedCatalogVersions[1] = DatosBmovilFileManager.getCurrent().getCatalogo4S();
				loadedCatalogVersions[2] = DatosBmovilFileManager.getCurrent().getCatalogo5S();
				loadedCatalogVersions[3] = DatosBmovilFileManager.getCurrent().getCatalogo8S();
				loadedCatalogVersions[4] = DatosBmovilFileManager.getCurrent().getCatalogoTAS();
				loadedCatalogVersions[5] = DatosBmovilFileManager.getCurrent().getCatalogoDMS();
				loadedCatalogVersions[6] = DatosBmovilFileManager.getCurrent().getCatalogoServS();
				loadedCatalogVersions[7] = DatosBmovilFileManager.getCurrent().getCatalogoMantenimientoSPEIS();
				
				final String res = CambioPerfilFileManager.getCurrent().getCambioPerfil();
				
				if("1".equals(res)){
					setAceptaCambioPerfil(true);
				} else { // 0 o ""
					setAceptaCambioPerfil(Boolean.valueOf(false));
				}

			// sets the loaded catalogs versions
			if (loadedCatalogVersions[0] != null && loadedCatalogVersions[1] != null
					&& loadedCatalogVersions[2] != null	&& loadedCatalogVersions[3] != null
					&& loadedCatalogVersions[4] != null	&& loadedCatalogVersions[5] != null
					&& loadedCatalogVersions[6] != null && loadedCatalogVersions[7] != null) {
				setCatalogVersions(loadedCatalogVersions);
			}


		} catch (Throwable t) {
			ToolsCommons.writeLoge(Session.class.getSimpleName(), t.getMessage(), ServerCommons.ALLOW_LOG);
		} 

		// load the data from catalog recordstore
		setCatalogs(CatFileManager.getCurrent().cargaCatalogos()); //FIXME resetear catalogos y versiones si throwable??
		loadNewCatalogRecordStore();
	}

	/**
	 * Stores an object in the RMS along with a numeric ID to help to identify
	 * the object when retrieving. The ID may be not unique
	 * 
	 * @param id Identifier that will be stored with the object
	 * @param object Object to be stored in the RMS
	 */
	synchronized public final void saveRecord(final int id, final Object object) {

		int identificador = 0;
		String value = null;
		
		// prepare the data to be stored
		try {

			// stores the key ID for the current record
			identificador = id;

			// and then the data
			if (object instanceof String) {
				
				value = (String) object;

				if (identificador == LOGIN_RMS_ID) {
					DatosBmovilFileManager.getCurrent().setLogin(value);
				} else if (identificador == VIA_RMS_ID) {
					DatosBmovilFileManager.getCurrent().setVia(value);
				} else if (identificador == TOKEN_RMS_ID) {
					DatosBmovilFileManager.getCurrent().setToken(value);
				}

			} else if (object instanceof Integer) {
				
				final Integer tmp = (Integer) object;
				
				final String str = Integer.toString(tmp);
				value = str;
				if (identificador == PENDING_STATUS_RMS_ID) {
					DatosBmovilFileManager.getCurrent().setPendienteDeDescarga(str);
				}

			} else if (object instanceof Long) {
				
				final Long tmp = (Long) object;
				
				final String str = Long.toString(tmp);
				if (identificador == SEED_RMS_ID) {
					DatosBmovilFileManager.getCurrent().setSeed(str);
				} 

			} else if (object instanceof Boolean) {

				final Boolean bool = (Boolean) object;
				
				if (identificador == ACTIVATED_RMS_ID) {
					DatosBmovilFileManager.getCurrent().setActivado(bool);
				} else if (identificador == SOFTTOKEN_RMS_ID) {
					DatosBmovilFileManager.getCurrent().setSoftoken(bool);
				}
				
			} else if (object instanceof Vector<?>) {
				
				final Vector<?> receivedVector = (Vector<?>) object;
				
				if (receivedVector.size() == 1) {
					if (identificador == CATALOG1_VERSION_RMS_ID) {
						DatosBmovilFileManager.getCurrent().setCatalogo1S(String.valueOf(receivedVector.elementAt(0)));
					} else if (identificador == CATALOG4_VERSION_RMS_ID) {
						DatosBmovilFileManager.getCurrent().setCatalogo4S(String.valueOf( receivedVector.elementAt(0)));
					} else if (identificador == CATALOG5_VERSION_RMS_ID) {
						DatosBmovilFileManager.getCurrent().setCatalogo5S(String.valueOf( receivedVector.elementAt(0)));
					} else if (identificador == CATALOG8_VERSION_RMS_ID) {
						DatosBmovilFileManager.getCurrent().setCatalogo8S(String.valueOf( receivedVector.elementAt(0)));
					} else if (identificador == TA_CATALOG_VERSION_RMS_ID) {
						DatosBmovilFileManager.getCurrent().setCatalogoTAS(String.valueOf( receivedVector.elementAt(0)));
					} else if (identificador == DM_CATALOG_VERSION_RMS_ID) {
						DatosBmovilFileManager.getCurrent().setCatalogoDMS(String.valueOf( receivedVector.elementAt(0)));
					} else if (identificador == SERVICES_CATALOG_VERSION_RMS_ID) {
						DatosBmovilFileManager.getCurrent().setCatalogoServS(String.valueOf( receivedVector.elementAt(0)));
					} else if (identificador == MANTENIMIENTO_SPEI_VERSION_CATALOG_RMS_ID) {
						DatosBmovilFileManager.getCurrent().setCatalogoMantenimientoSPEIS(String.valueOf(receivedVector.elementAt(0)));
					}
				}
				
			} else if (object instanceof CatalogoVersionado) {
				int numeroObjetos = 0;
				int idCatalogo = 0;
				if (object.equals(catalogoTiempoAire)) {
					idNuevoCatalogo = 0;
					idCatalogo = TA_CATALOG_RMS_ID;
					numeroObjetos = catalogoTiempoAire.getNumeroObjetos();
				} else if (object.equals(catalogoDineroMovil)) {
					idCatalogo = DM_CATALOG_RMS_ID;
					numeroObjetos = catalogoDineroMovil.getNumeroObjetos();
				} else if (object.equals(catalogoServicios)) {
					idCatalogo = SERVICES_CATALOG_RMS_ID;
					numeroObjetos = catalogoServicios.getNumeroObjetos();
				} else if (object.equals(catalogoMantenimientoSPEI)) {
					idCatalogo = MANTENIMIENTO_SPEI_CATALOG_RMS_ID;
					numeroObjetos = catalogoMantenimientoSPEI.getNumeroObjetos();
				}
				Object elementoCatalogo;
				final Vector<Object> catalogObjects = ((CatalogoVersionado) object).getObjetos();
				for (int i = 0; i < numeroObjetos; i++) {
					idNuevoCatalogo++;
					elementoCatalogo = catalogObjects.get(i);
					if (elementoCatalogo instanceof Compania) {
						final Compania compania = (Compania) elementoCatalogo;

						NuevoCatalogoFileManager.getCurrent().insertarCatalogo(
								String.valueOf(idNuevoCatalogo),
								idCatalogo,
								compania.getNombre(),
								compania.getNombreImagen(),
								compania.getClave(),
								compania.getCadenaMontos(), 
								compania.getOrden());
					} else if (elementoCatalogo instanceof Convenio) {
						final Convenio convenio = (Convenio) elementoCatalogo;
						NuevoCatalogoFileManager.getCurrent().insertarCatalogo(
								String.valueOf(idNuevoCatalogo),
								idCatalogo,
								convenio.getNombre(),
								convenio.getNombreImagen(),
								convenio.getNumeroConvenio(), 
								null,
								convenio.getOrden());
					}

				}
			}

		} catch (Throwable t) {
			if(ServerCommons.ALLOW_LOG) Log.e("Session.java", "Inserted row error occurred (metodo saveRecord()) " + t.toString());
		}
	}

	/**
	 * Stores data in the recordstore
	 */
	synchronized public final void saveRecordStore() {
		// 1.- delete old recordstore
		try {
			DatosBmovilFileManager.getCurrent().borrarDatos();
			CambioPerfilFileManager.getCurrent().borrarDatos();
		} catch (Throwable t) {
			Tools.writeLoge("Session.java", "Error SaveRecordStore", ServerCommons.ALLOW_LOG);
		}
		
		// 2.- Save DatosBMovil and CambioPerfil
		saveCriticalSessionData();
		saveProfileData();
		
		// 3.- store catalogs version
		try {
			if (catalogVersions != null) {
				for (int i = 0; i < catalogVersions.length; i++) {
					final Vector<Object> paramData = new Vector<Object>();
					paramData.addElement(catalogVersions[i]);
					saveRecord(CATALOG1_VERSION_RMS_ID + i, paramData);
				}
			}
		} catch (Throwable t) {
			try {
				DatosBmovilFileManager.getCurrent().borrarDatos();
				CambioPerfilFileManager.getCurrent().borrarDatos();
				saveCriticalSessionData();
				saveProfileData();
			} catch (Throwable t1) {
				Tools.writeLoge("Session.java", "Error deleting FileManagers or SavingData", ServerCommons.ALLOW_LOG);
			}
		}
		
		// 4.- Save or load authentication Catalogs
		if (null != this.authenticationJson){
			saveAuthenticationData(true);
		}else{
			CatalogoAutenticacionFileManager.getCurrent().cargaCatalogoAutenticacion();
		}
		
		// 5.- Save ALL OTHERS catalogs.
		if (catalogsUpdated) {
			saveCatalogRecordStore();
		}

	}

	private void saveCriticalSessionData() {
		// store activation code
		if (getActivationCode() != null) {
			saveRecord(ACTIVATION_CODE_RMS_ID, getActivationCode());
		}

		// store activated state
		if (Boolean.valueOf(isApplicationActivated()) != null) {
			saveRecord(ACTIVATED_RMS_ID, Boolean.valueOf(isApplicationActivated()));
		}

		// store softoken status
		if (Boolean.valueOf(isSofttokenActivado()) != null) {
			saveRecord(SOFTTOKEN_RMS_ID, Boolean.valueOf(isSofttokenActivado()));
		}

		// NEW store pending status if exists
		if (Integer.valueOf(getPendingStatus()) != null) {
			saveRecord(PENDING_STATUS_RMS_ID, Integer.valueOf(getPendingStatus()));
		}

		// store user name
		if (getUsername() != null) {
			saveRecord(LOGIN_RMS_ID, getUsername());
		}

		// Incidencia 22218
		saveRecord(SEED_RMS_ID, Long.valueOf(getSeed()));

		if (getVia() != null) {
			saveRecord(VIA_RMS_ID, getVia());
		}

		// store bancomer token
		if (getBancomerToken() != null) {
			saveRecord(TOKEN_RMS_ID, getBancomerToken());
		}

	}

	private void saveProfileData() {
		String cambioPerfilFlag = "";
		if (Boolean.FALSE.equals(aceptaCambioPerfil)) {
			cambioPerfilFlag = "0";
		} else if (Boolean.TRUE.equals(aceptaCambioPerfil)) {
			cambioPerfilFlag = "1";
		}		
		CambioPerfilFileManager.getCurrent().setCambioPerfil(cambioPerfilFlag);
	}

	
	/**
	 * Borra todos los registros de la tabla DatosBmovil.
	 */
	public final void deleteRecordStore() {
//		openRecordStore();

		try {
			//adapter.deleteContent(Constants.TABLE_MBANKING);
			DatosBmovilFileManager.getCurrent().borrarDatos();
//			adapter.deleteContent(Constants.TABLE_PROFILE_STATUS);
			CambioPerfilFileManager.getCurrent().borrarDatos();
		} catch (Throwable t) {
			if(ServerCommons.ALLOW_LOG) Log.e("Session.java", "saveRecordStore error");
		} 
//		finally {
//			closeRecordStore();
//		}
	}

	/**
	 * Clear the session data
	 */
	public void clearSession() {
		username = null;
		password = null;
		clientNumber = null;
		clientProfile = null;
//		securityInstrument = null;
		aceptaCambioPerfil = true;
		ium = null;
		if (catalogVersions != null) {
			for (int i = 0; i < catalogVersions.length; i++) {
				catalogVersions[i] = "0";
			}
		}
		via = "";
		bancomerToken = "";
		accounts = null;
		if (catalogs != null){
			for (int i = 0; i < catalogs.length; i++) {
				catalogs[i] = null;
			}
		}
		catalogoTiempoAire = null;
		catalogoDineroMovil = null;
		catalogoServicios = null;
		catalogoMantenimientoSPEI = null;
		serverDate = 0;
		localReferenceDate = 0;
		applicationActivated = false;
		pendingStatus = -3;
		activationCode = null;
		seed = 0;
		validity = UNSET_STATUS;
		timeout = 1;
		catalogsUpdated = false;
		
		if(ServerCommons.ALLOW_LOG) Log.i("Session", "Session cleared.");
	}

	/**
	 * Limpia los catálogos.
	 */
	public void clearCatalogs(final boolean clearAutenticationCatalogs) {
		
		// 1.- Clear 7 catalogs
		if (catalogVersions != null) {
			for (int i = 0; i < catalogVersions.length; i++)
				catalogVersions[i] = "0";
		}
		if (catalogs != null) {
			for (int i = 0; i < catalogs.length; i++) {
				catalogs[i] = null;
			}
		}
		catalogoTiempoAire = null;
		catalogoDineroMovil = null;
		catalogoServicios = null;
		catalogoMantenimientoSPEI = null;
		catalogsUpdated = false;
		
		CatFileManager.getCurrent().vaciaArchivoCatalogos();
		
		NuevoCatalogoFileManager.getCurrent().borrarCat();
		//TODO eliminar el archivo de catalogos de TA... ?
		
		// 2.- Clear Companias
		CompaniasTelefonicasCatalogFileManager.getCurrent().borrarCatalogo();
		CompaniasTelefonicasCatalogFileManager.getCurrent().addNewValueCatalog("VersionID", "0");
		catalogoTelefonicas = null;
		
		// 3.- Clear CatalogoAutenticacion
		if(clearAutenticationCatalogs){
			CatalogoAutenticacionFileManager.getCurrent().vaciaArchivoCatalogoAutenticacion();
			//TODO eliminar modelos de Autenticacion.getInstance()
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		super.finalize();

	}

	/**
	 * Obtiene la lista de rápidos del cliente.
	 * 
	 * @return Arreglo con todos los rápidos del cliente.
	 */
	public Rapido[] getRapidos() {
		if(Tools.isNull(rapidos)) {
			return new Rapido[0];
		} else {
			return rapidos.clone();
		}
	}
	/**
	//One click
	/**

	 * Obtiene la lista de promociones del cliente.
	 * 
	 * @return Arreglo con todos las promociones del cliente.
	 */
	public Promociones[] getPromociones() {
		if(Tools.isNull(promocion)) {
			return new Promociones[0];
		} else {
			return this.promocion.clone();
		}
	}
	
	
	/*setea las promociones recibidas al aceptarOferta*/
	public void setPromocion(final Promociones... promocion) {
		this.promocion = promocion;
	}
	//Termina One click


	/**
	 * Parsea el json de rápidos y guarda la lista en el objeto "rapidos".
	 * 
	 * @param json
	 *            Cadena con el json de rápidos.
	 */
	public void parseRapidosJson(final String json) {
		if (Tools.isEmptyOrNull(json))
			return;

		JSONObject jObj = null;

		try {
			jObj = new JSONObject(json);
		} catch (JSONException ex) {
			jObj = null;
			if(ServerCommons.ALLOW_LOG) Log.e("Session", "Error while trying to parse Quicks Json.", ex);
			return;
		}

		final ArrayList<Rapido> rapidos = new ArrayList<Rapido>();

		try {
			final JSONArray array = jObj.getJSONArray(Constants.QK_TAG_RAPIDAS);

			for (int i = 0; i < array.length(); i++) {
				final JSONObject quick = array.getJSONObject(i);
				Rapido rapido = null;

				rapido = new Rapido();

				rapido.setNombreCorto(quick.getString(Constants.QK_TAG_NICK));
				rapido.setCuentaOrigen(quick.getString(Constants.QK_TAG_ORIGIN));
				rapido.setCuentaDestino(quick
						.getString(Constants.QK_TAG_DESTINATION));
				rapido.setTelefonoDestino(quick
						.getString(Constants.QK_TAG_PHONE));
				rapido.setImporte(Float.parseFloat(quick
						.getString(Constants.QK_TAG_AMOUNT)) / 100.0F);
				rapido.setBeneficiario(quick
						.getString(Constants.QK_TAG_BENEFICIARY));
				rapido.setCompaniaCelular(quick
						.getString(Constants.QK_TAG_COMPANY));
				rapido.setTipoRapido(quick.getString(Constants.QK_TAG_TYPE));
				rapido.setIdOperacion(quick
						.getString(Constants.QK_TAG_OPERATION));
				rapido.setConcepto(quick.getString(Constants.QK_TAG_CONCEPT));

				rapidos.add(rapido);
			}

			this.rapidos = rapidos.toArray(new Rapido[rapidos.size()]);
		} catch (JSONException ex) {
			this.rapidos = null;
			if(ServerCommons.ALLOW_LOG) Log.e("Session", "Error while reading the Quicks JSON.", ex);
		}
	}
	
	//One CLick
		/**
		 * Parsea el json de promociones y guarda la lista en el objeto "Promociones".
		 * 
		 * @param json
		 *            Cadena con el json de promociones.
		 */
		public void parsePromocionesJson(final String json) {
			if (Tools.isEmptyOrNull(json)){
				final ArrayList<Promociones> promociones = new ArrayList<Promociones>();
				this.promocion = promociones.toArray(new Promociones[0]);
				return;
			}

			JSONObject jObj = null;

			try {
				jObj = new JSONObject(json);
			} catch (JSONException ex) {
				jObj = null;
				if(ServerCommons.ALLOW_LOG) Log.e("Session", "Error while trying to parse Quicks Json.", ex);
				return;
			}

			final ArrayList<Promociones> promociones = new ArrayList<Promociones>();

			try {
				final JSONArray array = jObj.getJSONArray("campanias");

				for (int i = 0; i < array.length(); i++) {
					final JSONObject quick = array.getJSONObject(i);
					Promociones promocion = null;

					promocion = new Promociones();

					promocion.setCveCamp(quick.getString("cveCamp"));
					promocion.setDesOferta(quick.getString("desOferta"));
					promocion.setMonto(quick.getString("monto"));
					

					promociones.add(promocion);
				}

				this.promocion = promociones.toArray(new Promociones[promociones.size()]);
			} catch (JSONException ex) {
				this.promocion = null;
				if(ServerCommons.ALLOW_LOG) Log.e("Session", "Error while reading the Quicks JSON.", ex);
			}
		}
	//Termina One CLick

	/*
	 * Actualiza Montos de cuenta despues de transferencia
	 */
	public void actualizaMonto(final Account cuentaOrigen, final String importe) {
		final double montoActualizado = Double.valueOf(importe) / 100;
		for (final Account cuenta : getAccounts()) {
			if (cuenta.equals(cuentaOrigen)) {
				cuenta.setBalance(montoActualizado);
			}
		}

	}

	/**
	 * Retorna el nombre del banco en base al tipo de tarjeta.
	 */
	public String obtenerNombreBanco(final String codigo, final String typeCard) {

		NameValuePair banco = null;
		String nombreBanco = "";
		final Catalog banks = getBanks(Constants.CREDIT_TYPE.equals(typeCard));
		if ((banco = banks.getNameValuePair(codigo)) != null) {
			nombreBanco = banco.getValue();
		}
		return nombreBanco;
	}

	/**
	 * Retorna el código del banco en base al tipo de tarjeta.
	 */
	public String obtenerCodigoBanco(final String codigo, final String typeCard) {

		NameValuePair banco = null;
		String nombreBanco = "";
		final Catalog banks = getBanks(Constants.CREDIT_TYPE.equals(typeCard));
		if ((banco = banks.getNameValuePair(codigo)) != null) {
			nombreBanco = banco.getName();
		}
		return nombreBanco;
	}

	public boolean puedeOperarConTDCEje() {
		//if (!clientProfile.equals(Constants.PROFILE_ADVANCED_03)) {
		if (!clientProfile.equals(Constants.Perfil.avanzado)) {
			final ArrayList<Account> visibleAccounts = getVisibleAccounts();
			final int visibleAccountsSize = visibleAccounts.size();
			for (int i = 0; i < visibleAccountsSize; i++) {
				if (visibleAccounts.get(i).getType()
						.equals(Constants.CREDIT_TYPE)) {
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * Realiza la actualizacion de las cuentas enviadas de nacar al realizar el
	 * cambio de cuenta asociada
	 * 
	 */
     public void updateAccounts(final Account... updatedAccounts) {
		
		
		if (updatedAccounts != null){
		   
			Session.getListA().removeAll(listA);
			Session.getListB().removeAll(listB);
						
			for(final Account acc: updatedAccounts)
			{				
				
				if(acc.getIndicadorSPEI().equals("TRUE"))
				{
				    acc.setIndicadorSPEI("S");
					listA.add(acc);
					
				}else
				{
				    acc.setIndicadorSPEI("N");
					listB.add(acc);
					
				}
			}
		}
			
		this.accounts = updatedAccounts;
	}

	/**
	 * Get the catalog versions
	 * 
	 * @return the catalog versions
	 */
	public String[] getCatalogVersions() {
		if(Tools.isNull(catalogVersions)) {
			return new String[0];
		} else {
			return catalogVersions.clone();
		}
	}

	/**
	 * Set the catalog versions. Only if a new catalog version is not null, sets
	 * the new catalog version. In other case the catalog version is not
	 * modified
	 * 
	 * @param versions
	 *            the new versions of each catalog
	 */
	public void setCatalogVersions(final String... versions) {
		// System.out.println("setCatalogVersions: " + versions.length);
		if (versions != null) {
			for (int i = 0; i < catalogVersions.length; i++) {

				if (versions[i] != null) {
					catalogVersions[i] = versions[i];
				}
			}
		}

	}

	/**
	 * Sets the current catalogs. Only if the new catalog is not null, sets the
	 * new catalog. In other case the catalog is not modified
	 * 
	 * @param catalogs
	 *            the new catalogs to set
	 */
	public void setCatalogs(final Catalog... catalogs) {
		if (catalogs != null) {

			Catalog catalog;
			final int size = catalogs.length;

			for (int i = 0; i < size; i++) {

				catalog = catalogs[i];
				if (catalog != null) {
					this.catalogs[i] = catalog;
				}
			}
		}
	}

	public void setCatalogoTiempoAire(final CatalogoVersionado catalogo) {
		catalogoTiempoAire = catalogo;
	}

	public void updateCatalogoTiempoAire(final CatalogoVersionado catalogo) {
		catalogoTiempoAire = catalogo;
		catalogsUpdated = true;
	}

	public void setCatalogoDineroMovil(final CatalogoVersionado catalogo) {
		catalogoDineroMovil = catalogo;
	}

	public void updateCatalogoDineroMovil(final CatalogoVersionado catalogo) {
		catalogoDineroMovil = catalogo;
		catalogsUpdated = true;
	}

	public void setCatalogoServicios(final CatalogoVersionado catalogo) {
		catalogoServicios = catalogo;
	}

	public void updateCatalogoServicios(final CatalogoVersionado catalogo) {
		catalogoServicios = catalogo;
		catalogsUpdated = true;
	}
	
	public void updateCatalogoMantenimientoSPEI(final CatalogoVersionado catalogo) {
		catalogoMantenimientoSPEI = catalogo;
		catalogsUpdated = true;
	}

	/**
	 * Updates the current catalogs. Similar to setCatalogs but this method sets
	 * the flag catalogsUpdates that tells to the saveRecordStore method if it's
	 * necesary to store the catalogs(because of an update of the version) This
	 * method should be only invoked from a network operation(when receiving
	 * updated catalogs) and never from a recordstore operation
	 * 
	 * @param catalogs
	 *            the updated catalogs
	 */
	public void updateCatalogs(final Catalog... catalogs) {

		// System.out.println("updateCatalogs: " + catalogs.length);
		if (catalogs != null) {

			Catalog catalog;
			final int size = catalogs.length;

			for (int i = 0; i < size; i++) {

				catalog = catalogs[i];
				if (catalog != null) {
					this.catalogs[i] = catalog;

					// if at least one of the catalogs has changed, sets the
					// flag that tells saveRecordStore
					// to save the catalogs and catalog versions data
					catalogsUpdated = true;
				}
			}
		}
	}

	/**
	 * Get ALL the catalogs
	 * 
	 * @return Catalog array
	 */
	public Catalog[] getCatalogs() {
		if(Tools.isNull(catalogs)) {
			return new Catalog[0];
		} else {
			return this.catalogs.clone();
		}
	}

	/**
	 * Returns an specific catalog
	 * 
	 * @param catalogIndex
	 *            Index of the catalog to get. Possible values are
	 *            TDD_BANKS_CATALOG, CELL_COMPANY_CATALOG,
	 *            NIPPER_AMOUNTS_CATALOG, TDC_BANKS_CATALOG
	 * 
	 * @return The catalog specified by the index parameter
	 */
	public Catalog getCatalog(final int catalogIndex) {
		return this.catalogs[catalogIndex];
	}

	public CatalogoVersionado getCatalogoTiempoAire() {
		return catalogoTiempoAire;
	}

	public CatalogoVersionado getCatalogoDineroMovil() {
		return catalogoDineroMovil;
	}

	public CatalogoVersionado getCatalogoServicios() {
		return catalogoServicios;
	}
	
	public CatalogoVersionado getCatalogoMantenimientoSPEI() {
		return catalogoMantenimientoSPEI;
	}

	public void setCatalogoMantenimientoSPEI(final CatalogoVersionado catalogoMantenimientoSPEI){
		if(catalogoMantenimientoSPEI!=null)//evita que se resetee a nulo y cause problemas en ejecucion
			this.catalogoMantenimientoSPEI=catalogoMantenimientoSPEI;
	}
	/**
	 * Stores catalog data in the recordstore
	 */
	synchronized public final void saveCatalogRecordStore() {
		// 1.- Store first 4 catalogs
		if (catalogs != null) {
			CatFileManager.getCurrent().guardaCatalogos(catalogs);			
		}
		
		// 2.- Store last 3 catalogs
		NuevoCatalogoFileManager.getCurrent().borrarCat();
		
		if (catalogoTiempoAire != null) {
			saveRecord(TA_CATALOG_RMS_ID, catalogoTiempoAire);
		}
		if (catalogoDineroMovil != null) {
			saveRecord(DM_CATALOG_RMS_ID, catalogoDineroMovil);
		}
		if (catalogoServicios != null) {
			saveRecord(SERVICES_CATALOG_RMS_ID, catalogoServicios);
		}
		
		if (catalogoMantenimientoSPEI != null) {
			saveRecord(MANTENIMIENTO_SPEI_CATALOG_RMS_ID, catalogoMantenimientoSPEI);
		}
		
		catalogsUpdated = false;
	}

	/**
	 * Loads new catalog data from recordstore.
	 */
	synchronized public final void loadNewCatalogRecordStore() {
		// ID of the currently retrieved record
		int id = 0;
		try {
			final Collection<Entry<Object, Object>> cat = NuevoCatalogoFileManager.getCurrent().getAllCatalog();
			final Iterator<Entry<Object, Object>> it = cat.iterator();
			catalogoDineroMovil = null;
			catalogoTiempoAire = null;
			catalogoServicios = null;
			catalogoMantenimientoSPEI = null;
			while (it.hasNext()) {
								
				final Entry<Object,Object> catalogos = it.next();
				final Vector<String> values = NuevoCatalogoFileManager.getCurrent().getPropertynValue((String) catalogos.getKey());
				id = Integer.valueOf(NuevoCatalogoFileManager.getCurrent().getIdentificador(values));
				
				String name;
				String image;
				String clave;
				String amounts;
				int order = -1;
				
				switch (id) {
					case TA_CATALOG_RMS_ID:
						if (catalogoTiempoAire == null) {
							catalogoTiempoAire = new CatalogoVersionado(
									catalogVersions[4]);
						}
													
						name = NuevoCatalogoFileManager.getCurrent().getName(values);
					    image = NuevoCatalogoFileManager.getCurrent().getImage(values);
					    clave = NuevoCatalogoFileManager.getCurrent().getClave(values);
					    amounts = NuevoCatalogoFileManager.getCurrent().getAmounts(values);
					    order = Integer.valueOf(NuevoCatalogoFileManager.getCurrent().getOrder(values));							
	
						catalogoTiempoAire.agregarObjeto(new Compania(
								name, image, clave, amounts, Integer
										.toString(order)));
						name = null;
						image = null;
						clave = null;
						amounts = null;
						order = -1;
						break;
	
					case DM_CATALOG_RMS_ID:
						if (catalogoDineroMovil == null) {
							catalogoDineroMovil = new CatalogoVersionado(catalogVersions[5]);
						}
						
						name = NuevoCatalogoFileManager.getCurrent().getName(values);
					    image = NuevoCatalogoFileManager.getCurrent().getImage(values);
					    clave = NuevoCatalogoFileManager.getCurrent().getClave(values);
					    amounts = NuevoCatalogoFileManager.getCurrent().getAmounts(values);
					    order = Integer.valueOf(NuevoCatalogoFileManager.getCurrent().getOrder(values));
	
						catalogoDineroMovil.agregarObjeto(new Compania(name, image, clave, amounts, Integer.toString(order)));
						name = null;
						image = null;
						clave = null;
						amounts = null;
						order = -1;
						break;
	
					case SERVICES_CATALOG_RMS_ID:
						if (catalogoServicios == null) {
							catalogoServicios = new CatalogoVersionado(catalogVersions[6]);
						}
	
						name = NuevoCatalogoFileManager.getCurrent().getName(values);
					    image = NuevoCatalogoFileManager.getCurrent().getImage(values);
					    clave = NuevoCatalogoFileManager.getCurrent().getClave(values);
					    order = Integer.valueOf(NuevoCatalogoFileManager.getCurrent().getOrder(values));
						
						catalogoServicios.agregarObjeto(new Convenio(
								name, image, clave, Integer.toString(order)));
						name = null;
						image = null;
						clave = null;
						amounts = null;
						order = -1;
						break;
						
					case MANTENIMIENTO_SPEI_CATALOG_RMS_ID:
						if (catalogoMantenimientoSPEI == null) {
							catalogoMantenimientoSPEI = new CatalogoVersionado(catalogVersions[7]);
						}
						
						name = NuevoCatalogoFileManager.getCurrent().getName(values);
					    image = NuevoCatalogoFileManager.getCurrent().getImage(values);
					    clave = NuevoCatalogoFileManager.getCurrent().getClave(values);
					    order = Integer.valueOf(NuevoCatalogoFileManager.getCurrent().getOrder(values));
						
						catalogoMantenimientoSPEI.agregarObjeto(new MantenimientoSpei());
						
						name = null;
						image = null;
						clave = null;
						order = -1;
						break;
					}
				
			}
		} catch (Throwable t) {
			resetNewCatalogs();
		}
	}

	private void resetNewCatalogs() {
		if (catalogoTiempoAire != null) {
			catalogoTiempoAire = null;
		}

		if (catalogoDineroMovil != null) {
			catalogoDineroMovil = null;
		}

		if (catalogoServicios != null) {
			catalogoServicios = null;
		}
		
		if (catalogoMantenimientoSPEI != null) {
			catalogoMantenimientoSPEI = null;
		}
	}

	/**
	 * Json String for authentications values.
	 */
	private String authenticationJson = null;


	/**
	 * Sets the authentication catalog JSON string.
	 * 
	 * @param json
	 *            The JSON string.
	 */
	public void setAuthenticationJson(final String json) {
		if (null != json)
			this.authenticationJson = json;
	}

	/**
	 * Stores the Autentiction Catalog data in the file.
	 */
	public void saveAuthenticationData(final boolean registroEnabled) {
		if (null == authenticationJson){
			return;
		}
		
		try {
			parseAuthenticationJSON(authenticationJson, registroEnabled);
		} catch (Exception ex) {
			if(ServerCommons.ALLOW_LOG) Log.e(getClass().getSimpleName(), "Error al parsear el json de Autenticación.", ex);
			return;
		}

		CatalogoAutenticacionFileManager.getCurrent().vaciaArchivoCatalogoAutenticacion();
		CatalogoAutenticacionFileManager.getCurrent().guardaCatalogoAutenticacion();
		
		authenticationJson = null;
	}

	/**
	 * Parse the authentication catalog info from a JSON.
	 * 
	 * @param json
	 *            The JSON String
	 * @return The catalog.
	 */
	private void parseAuthenticationJSON(final String json, final boolean registroEnabled) {
		JSONObject jObj = null;

		try {
			jObj = new JSONObject(json);
		} catch (JSONException ex) {
			jObj = null;
			if(ServerCommons.ALLOW_LOG) Log.e("Session", "Error while trying to parse Json.", ex);
		}

		if (null == jObj){
			return;
		}

		try{
			final JSONArray perfiles = jObj.getJSONArray(Constants.AU_TAG_PERFIL);
			for (int i = 0; i < perfiles.length(); i++) {
				final JSONObject perfil = perfiles.getJSONObject(i);
				
				if(perfil.has(Constants.AU_TAG_BASICO)){
					final ArrayList<OperacionAutenticacion> basico = new ArrayList<OperacionAutenticacion> ();
					fillProfile(basico, perfil.getJSONArray(Constants.AU_TAG_BASICO), registroEnabled);
					Autenticacion.getInstance().setBasico(basico);
				}else if (perfil.has(Constants.AU_TAG_AVANZADO)){
					final ArrayList<OperacionAutenticacion> avanzado = new ArrayList<OperacionAutenticacion> ();
					fillProfile(avanzado, perfil.getJSONArray(Constants.AU_TAG_AVANZADO), registroEnabled);
					Autenticacion.getInstance().setAvanzado(avanzado);
				}else if (perfil.has(Constants.AU_TAG_RECORTADO)){
					final ArrayList<OperacionAutenticacion> recortado = new ArrayList<OperacionAutenticacion> ();
					fillProfile(recortado, perfil.getJSONArray(Constants.AU_TAG_RECORTADO), registroEnabled);
					Autenticacion.getInstance().setRecortado(recortado);
				}else{
					throw new JSONException("Does not recognize the type of profile inside the 'perfil' array.");
				}
			}

			Autenticacion.getInstance().setVersion(jObj.getString(Constants.AU_TAG_VERSION));

		} catch (JSONException ex) {
			if(ServerCommons.ALLOW_LOG) Log.e("Session", "Error while reading the JSON.", ex);
		}
	}

	private void fillProfile(final ArrayList<OperacionAutenticacion> operaciones,
			final JSONArray jsonArray, final boolean registroEnabled) throws JSONException {
		
		for (int i = 0; i < jsonArray.length(); i++) {
			final JSONObject operacionJSON = jsonArray.getJSONObject(i);
			OperacionAutenticacion operacion = null;
			
			String opName = "";
			try {
				opName = operacionJSON.getString(Constants.AU_TAG_OPERACION);
				operacion = new OperacionAutenticacion(Constants.Operacion.valueOf(opName));
			} catch (Exception ex) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while retrieving the value of the operation " + opName);
				continue;
			}

			final JSONObject credencialesJSON = operacionJSON.getJSONObject(Constants.AU_TAG_CREDENCIALES);
			final PerfilAutenticacion credenciales = new PerfilAutenticacion();
			
			credenciales.setContrasena(Boolean.parseBoolean(credencialesJSON.getString(Constants.AU_TAG_CONTRASENA)));
			credenciales.setToken(Tools.getTipoOtpFromString(credencialesJSON.getString(Constants.AU_TAG_TOKEN)));
			credenciales.setCvv2(Boolean.parseBoolean(credencialesJSON.getString(Constants.AU_TAG_CVV2)));
			credenciales.setNip(Boolean.parseBoolean(credencialesJSON.getString(Constants.AU_TAG_NIP)));
			if (registroEnabled){
				credenciales.setRegistro(Boolean.parseBoolean(credencialesJSON.getString(Constants.AU_TAG_REGISTRO)));
			}
			credenciales.setOperar(Boolean.parseBoolean(credencialesJSON.getString(Constants.AU_TAG_OPERAR)));
			credenciales.setVisible(Boolean.parseBoolean(credencialesJSON.getString(Constants.AU_TAG_VISIBLE)));
			
			operacion.setCredenciales(credenciales);
			
			operaciones.add(operacion);
		}
	
	}

	/**
	 * Catálogo de compañías telefónicas.
	 */
	private CatalogoVersionado catalogoTelefonicas;

	/**
	 * @return Catálogo de compañías telefónicas.
	 */
	public CatalogoVersionado getCatalogoTelefonicas() {
		return this.catalogoTelefonicas;
	}

	/**
	 * @param catalogo
	 *            Catálogo de compañías telefónicas.
	 */
	public void setCatalogoTelefonicas(final CatalogoVersionado catalogo) {
		this.catalogoTelefonicas = catalogo;
	}
	
//////////////////////////////////////////////////////////////////////////////
	
	//Regi�n HS SPEI operativa diaria.
	
	/**
	 * Catal�go de horas de servicio.
	 */
	
	private CatalogoVersionado catalogoHoras;
	
	/**
	 * 
	 * @return Catálogo de Horas de Servicio.
	 */	
	

	public CatalogoVersionado getCatalogoHoras() {
		return catalogoHoras;
	}
	
	/**
	 * 
	 * @param catalogoHoras
	 * 						Catálogo de horas de Servicio.
	 */


	public void setCatalogoHoras(final CatalogoVersionado catalogoHoras) {
		this.catalogoHoras = catalogoHoras;
	}
	
////////////////////////////////////////////////////////////////////////////////////
	

//////////////////////////////////////////////////////////////////////////
	/**
	 * Interpreta el json de compañías telefónicas y regresa el catálogo con los
	 * datos del json.
	 * 
	 * @param json
	 *            El json con los datos del catálogo.
	 * @return Catálogo de compañías telefónicas.
	 */
	private CatalogoVersionado parseCatalogoTelefonicas(final String json,
			final String version) {
		final CatalogoVersionado result = new CatalogoVersionado(
				Tools.isEmptyOrNull(version) ? "0" : version);
		Compania comp = null;
		String nombre = "";
		String imagen = "";
		JSONObject rootElement = null;
		JSONObject currentElement = null;
		JSONArray elements = null;

		try {
			rootElement = new JSONObject(json);
			elements = rootElement.getJSONArray("telefonicasMantenimiento");
		} catch (JSONException ex) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "No se pudo interpretar el json.", ex);
			return null;
		}

		if (null == elements) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(),
					"No se encontro la lista de elementos necesaria.");
			return null;
		}

		for (int i = 0; i < elements.length(); i++) {
			try {
				currentElement = elements.getJSONObject(i);
				nombre = currentElement.getString(Constants.TL_TAG_COMPANIA);
				imagen = currentElement.getString(Constants.TL_TAG_IMAGEN);
			} catch (JSONException jsonEx) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(),
						"Error while parsing the company with index: " + i + "\nfor the json: " + json, jsonEx);
				continue;
			}

			comp = new Compania(nombre, imagen, "", "", "");
			result.agregarObjeto(comp);
		}

		return result;
	}
//x
	/**
	 * Almacena de forma persistente el catálogo de compañías telefónicas.
	 * 
	 * @param json
	 *            El json con las compañías telefónicas.
	 */
	public void saveCatalogoCompaniasTelefonicas(final String json) {
		if (Tools.isEmptyOrNull(json)){
			return;
		}

		String telJson = "";
		String catVersion = "";

		try {
			final JSONObject jsonObj = new JSONObject(json);
			telJson = "{\"telefonicasMantenimiento\":" + jsonObj.getJSONArray("telefonicasMantenimiento").toString() + "}";
			catVersion = jsonObj.getString("versionCatTelefonico");
			saveCatalogoCompaniasTelefonicas(telJson, catVersion);
		} catch (Exception ex) {
			catalogoTelefonicas = null;
		}
	}

	/**
	 * Almacena de forma persistente el catálogo de compañías telefónicas.
	 * 
	 * @param json
	 *            El json con las compañías telefónicas.
	 * @param catalogVersion
	 *            Version del catálogo.
	 */
	public void saveCatalogoCompaniasTelefonicas(final String json, final String catalogVersion) {
		if (Tools.isEmptyOrNull(json)){
			return;
		}
		
		// Parse the catalog.
		try {
			catalogoTelefonicas = parseCatalogoTelefonicas(json,
					Tools.isEmptyOrNull(catalogVersion) ? "0" : catalogVersion);
		} catch (Exception ex) {
			if(ServerCommons.ALLOW_LOG)Log.e(getClass().getSimpleName(),
					"Error al parsear el catálogo de compañías telefónicas", ex);
			return;
		}
		
		if (null == catalogoTelefonicas) {
			return;
		}
		CompaniasTelefonicasCatalogFileManager.getCurrent().borrarCatalogo();
		
		CompaniasTelefonicasCatalogFileManager.getCurrent().addNewValueCatalog("VersionID", catalogoTelefonicas.getVersion());
		
		// Add the new data.
		for (final Object comp : catalogoTelefonicas.getObjetos()) {
			final Compania compania = (Compania) comp;
			CompaniasTelefonicasCatalogFileManager.getCurrent().addNewValueCatalog(compania.getNombre(), compania.getNombreImagen());
		}
	}


	/**
	 * Carga el catálogo de compañías telefónicas desde el almacenamiento del
	 * telefono.
	 */
	public CatalogoVersionado loadCatalogoCompaniasTelefonicas() {
		String version = null;
		CatalogoVersionado catalog = null;

		final Collection<Entry<Object,Object>> catalogos = CompaniasTelefonicasCatalogFileManager.getCurrent().getAllCompaniasTelefonicas();

		try {
			final Iterator<Entry<Object, Object>> cat = catalogos.iterator();
			Entry<Object, Object> actual;
			if(!cat.hasNext()){
				return null;
			}
			catalog = new CatalogoVersionado("");

			do{
				actual =(Entry<Object, Object>) cat.next();
				if(actual.getKey().toString().toLowerCase().contains("version")){
					version = (String) actual.getValue();
					catalog.setVersion(version);
				} else {
					final String nombre = (String) actual.getKey();
					final String imagen = (String) actual.getValue();
					final Compania compania = new Compania(nombre, imagen, "", "", "");

					catalog.agregarObjeto(compania);
				}
			}while(cat.hasNext());
		} catch (Exception ex) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while getting the data from the file.", ex);
			return null;
		}

		this.catalogoTelefonicas = catalog;
		return this.catalogoTelefonicas;
	}

	/**
	 * Obtiene la versión del catálogo de compañías telefónicas.
	 * 
	 * @return La versión del catálogo.
	 */
	public String getVersionCatalogoTelefonicas() {
		String version = "0";

		try {		
			version = CompaniasTelefonicasCatalogFileManager.getCurrent().getVersion();
		} catch (Exception ex) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(),
					"Error while getting the data from the file.", ex);
			version = "0";
		}

		return version;
	}
	
///////////////////////////////SPEI OPERATIVA DIARIA.//////////////////////////
	
	
	/**
	 * Interpreta el json de horas de servicio y regresa el catálogo con los
	 * datos del json.
	 * 
	 * @param json
	 *            El json con los datos del catálogo.
	 * @return Catálogo de horas de servicio.
	 */	
	private CatalogoVersionado parseCatalogoHoras(final String json,
			final String version) {
		final CatalogoVersionado result = new CatalogoVersionado(
				Tools.isEmptyOrNull(version) ? "0" : version);
		
		Schedule sche = null;		
		JSONObject rootElement = null;
		JSONObject currentElement = null;
		JSONObject currentDetails;
		JSONObject currentNonValidJson;
		JSONArray elements = null;
		String operation;
		
		try {
			rootElement = new JSONObject(json);
			elements = rootElement.getJSONArray("horariosOperacion");			
		} catch (JSONException ex) {
			if(ServerCommons.ALLOW_LOG) Log.e(LoginData.class.getSimpleName(), "No se pudo interpretar el json.", ex);
			
		}		
				

		if (null == elements) {
			if(ServerCommons.ALLOW_LOG) Log.e(LoginData.class.getSimpleName(),
					"No se encontro la lista de elementos necesaria.");
		}
		
		
		for (int i = 0; i < elements.length(); i++) {
			try {
				
				//Getting every array item.
				
				currentElement = elements.getJSONObject(i);
				operation = currentElement.getString("operacion");
				
				if(operation.equals(Constants.SPEI_CELULAR_INVALIDO))
				{
					if(currentElement.get("horarios") instanceof JSONObject)
					{
						
						currentDetails  = currentElement.getJSONObject("horarios");
						
						currentNonValidJson = currentDetails.getJSONObject(Constants.MONDAY);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.MONDAY+Constants.NON_VALID_ONE);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_ONE));				
						result.agregarObjeto(sche);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.MONDAY+Constants.NON_VALID_TWO);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_TWO));				
						result.agregarObjeto(sche);
						
						currentNonValidJson = currentDetails.getJSONObject(Constants.TUESDAY);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.TUESDAY+Constants.NON_VALID_ONE);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_ONE));				
						result.agregarObjeto(sche);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.TUESDAY+Constants.NON_VALID_TWO);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_TWO));				
						result.agregarObjeto(sche);
						
						currentNonValidJson = currentDetails.getJSONObject(Constants.WEDNESDAY);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.WEDNESDAY+Constants.NON_VALID_ONE);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_ONE));				
						result.agregarObjeto(sche);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.WEDNESDAY+Constants.NON_VALID_TWO);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_TWO));				
						result.agregarObjeto(sche);
						
						currentNonValidJson = currentDetails.getJSONObject(Constants.THURSDAY);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.THURSDAY+Constants.NON_VALID_ONE);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_ONE));				
						result.agregarObjeto(sche);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.THURSDAY+Constants.NON_VALID_TWO);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_TWO));				
						result.agregarObjeto(sche);
						
						currentNonValidJson = currentDetails.getJSONObject(Constants.FRIDAY);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.FRIDAY+Constants.NON_VALID_ONE);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_ONE));				
						result.agregarObjeto(sche);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.FRIDAY+Constants.NON_VALID_TWO);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_TWO));				
						result.agregarObjeto(sche);
						
						currentNonValidJson = currentDetails.getJSONObject(Constants.SATURDAY);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.SATURDAY+Constants.NON_VALID_ONE);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_ONE));				
						result.agregarObjeto(sche);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.SATURDAY+Constants.NON_VALID_TWO);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_TWO));				
						result.agregarObjeto(sche);
						
						currentNonValidJson = currentDetails.getJSONObject(Constants.SUNDAY);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.SUNDAY+Constants.NON_VALID_ONE);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_ONE));				
						result.agregarObjeto(sche);
						
						sche = new Schedule();				
						sche.setDay(operation+Constants.SUNDAY+Constants.NON_VALID_TWO);
						sche.setHour(currentNonValidJson.getString(Constants.NON_VALID_TWO));				
						result.agregarObjeto(sche);
						
					}
					
				}
				else
				{
					
					currentDetails  = currentElement.getJSONObject("horarios");
					
					sche = new Schedule();				
					sche.setDay(operation+Constants.MONDAY);
					sche.setHour(currentDetails.getString(Constants.MONDAY));				
					result.agregarObjeto(sche);
					
					sche = new Schedule();				
					sche.setDay(operation+Constants.TUESDAY);
					sche.setHour(currentDetails.getString(Constants.TUESDAY));				
					result.agregarObjeto(sche);
					
					sche = new Schedule();				
					sche.setDay(operation+Constants.WEDNESDAY);
					sche.setHour(currentDetails.getString(Constants.WEDNESDAY));				
					result.agregarObjeto(sche);
					
					sche = new Schedule();				
					sche.setDay(operation+Constants.THURSDAY);
					sche.setHour(currentDetails.getString(Constants.THURSDAY));				
					result.agregarObjeto(sche);
					
					sche = new Schedule();				
					sche.setDay(operation+Constants.FRIDAY);
					sche.setHour(currentDetails.getString(Constants.FRIDAY));				
					result.agregarObjeto(sche);
					
					sche = new Schedule();				
					sche.setDay(operation+Constants.SATURDAY);
					sche.setHour(currentDetails.getString(Constants.SATURDAY));				
					result.agregarObjeto(sche);
					
					sche = new Schedule();				
					sche.setDay(operation+Constants.SUNDAY);
					sche.setHour(currentDetails.getString(Constants.SUNDAY));				
					result.agregarObjeto(sche);
					
				}
				
			} catch (JSONException jsonEx) {
				if(ServerCommons.ALLOW_LOG) Log.e(LoginData.class.getSimpleName(),
						"Error while parsing the account with index: " + i + "\nfor the json: " + json, jsonEx);
				continue;
			}
		}

		return result;
	}	
	
	/**
	 * Almacena de forma persistente el catálogo de horas de servicio.
	 * @param json
	 * 			El json de las horas de servicio.
	 */
	public void saveCatalogoHorasServicio(final String json) {
		if (Tools.isEmptyOrNull(json)){
			return;
		}
		
		String catVersion = "";

		try {
			final JSONObject jsonObj = new JSONObject(json);
			catVersion = jsonObj.getString("version");
			Log.e("Entra al primer paso", "ok");
			saveCatalogoHorasServicio(json, catVersion);
		} catch (Exception ex) {
			catalogoHoras = null;
		}
	}
	
	/**
	 * Almacena de forma persistente el catálogo de horas de servicio.
	 * @param json
	 * 			El json de las horas de servicio
	 * @param catalogVersion
	 *          Version del catálogo.
	 */	
	public void saveCatalogoHorasServicio(final String json, final String catalogVersion) {

		if (Tools.isEmptyOrNull(json)){
			return;
		}
		
		// Parse the catalog.
		try {
			
			catalogoHoras = parseCatalogoHoras(json,
					Tools.isEmptyOrNull(catalogVersion) ? "0" : catalogVersion);
		} catch (Exception ex) {
			if(ServerCommons.ALLOW_LOG) Log.e(getClass().getSimpleName(),
					"Error al parsear el catálogo de horas de servicio", ex);
			return;
		}
		
		if (null == catalogoHoras) {
			return;
		}
		ServiceHourCatalogFileManager.getCurrent().borrarCatalogo();
		
		ServiceHourCatalogFileManager.getCurrent().addNewValueCatalog("VersionID", catalogoHoras.getVersion());
		
		// Add the new data.
		if(ServerCommons.ALLOW_LOG) Log.e("Entra al segundo paso", "ok");
		for (final Object sche : catalogoHoras.getObjetos()) {
			final Schedule schedule= (Schedule) sche;
			ServiceHourCatalogFileManager.getCurrent().addNewValueCatalog(schedule.getDay(), schedule.getHour());
			if(ServerCommons.ALLOW_LOG) Log.e("Entra al guardado de datos", "ok");
		}
		
	}
	
	/** Carga el catálogo de horas de servicio desde el almacenamiento del
	 * teléfono.
	 */
	public CatalogoVersionado loadCatalogoHorasServicio() {
		String version = null;
		CatalogoVersionado catalog = null;
		Schedule sche=null;
		
		final Collection<Entry<Object,Object>> catalogos = ServiceHourCatalogFileManager.getCurrent().getAllHourService();
		try {
			final Iterator<Entry<Object, Object>> cat = catalogos.iterator();
			Entry<Object, Object> actual;
			if(!cat.hasNext()){
				return null;
			}
			version = (String) cat.next().getValue();
			catalog = new CatalogoVersionado(version);
			
			do{
				actual =(Entry<Object, Object>) cat.next();
				
				sche = new Schedule();
				sche.setDay((String)actual.getKey());
				sche.setHour((String)actual.getValue());
				
				catalog.agregarObjeto(sche);
				
			}while(cat.hasNext());
		} catch (Exception ex) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while getting the data from the file.", ex);
			return null;
		}
		
		if(ServerCommons.ALLOW_LOG) Log.e("Carga el catálogo horas de servicio", "ok");
		this.catalogoHoras = catalog; 
		return this.catalogoHoras; 
	}

	/**
	 * Obtiene la versión del catálogo de horas de servicio.
	 * 
	 * @return La versión del catálogo.
	 */
	public String getVersionCatalogoHoras() {
		String version = "0";

		try {
			version = ServiceHourCatalogFileManager.getCurrent().getVersion();
		} catch (Exception ex) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(),
					"Error while getting the data from the file.", ex);
			version = "0";
		}

		return version;
	}
	
	
/////////////////////////FIN SPEI OPERATIVA DIARIA.//////////////////////////
	
	

	/**
	 * Carga el valor de un atributo de la tabla BanderasBMovil.
	 * 
	 * @param name
	 *            el nombre del atributo
	 * @return el contenido del atributo de la tabla. Nulo en caso de error
	 */
	public Boolean loadBanderasBMovilAttribute(final String name) {
		Boolean attribute = null;
		String res = "";
		try {			
			if (Constants.BANDERAS_CAMBIO_PERFIL.equals(name)){
				res = BanderasBMovilFileManager.getCurrent().getBanderasBMovilCambioPerfil();
			} else if(Constants.BANDERAS_CONTRATAR.equals(name)){
				res = BanderasBMovilFileManager.getCurrent().getBanderasBMovilContratar();
			} else if(Constants.BANDERAS_CONTRATAR2x1.equals(name)){
				res = BanderasBMovilFileManager.getCurrent().getBanderasBMovilContratar2x1();
			} else if(Constants.BANDERAS_CAMBIO_CELULAR.equals(name)){
				res = BanderasBMovilFileManager.getCurrent().getBanderasBMovilCambioCelular();
			}else if(Constants.BANDERAS_INDICADOR_CONTRATACION.equals(name)){
				res = BanderasBMovilFileManager.getCurrent().getBanderasBMovilIndicadorContratacion();
			}
		} catch (Exception ex) {
			if(ServerCommons.ALLOW_LOG)Log.e(this.getClass().getSimpleName(),
					"Error while getting the data from the BanderasBMovil.", ex);
		}		

		if("0".equals(res)){
			attribute = false;
		} else if ("1".equals(res)){
			attribute = true;
		}
		
		return attribute;
	}

	/**
	 * Guarda el valor de un atributo de la tabla BanderasBMovil.
	 * 
	 * @param name
	 *            el nombre del atributo
	 * @param value
	 *            el valor del atributo
	 */
	public void saveBanderasBMovil(final String name, final boolean value, final boolean createIfNecessary) {
		String v;
		if(value){
			v = "1";
		}else{
			v = "0";
		}
		
		try {
			if (Constants.BANDERAS_CAMBIO_PERFIL.equals(name)){
				BanderasBMovilFileManager.getCurrent().setBanderasBMovilCambioPerfil(v);
			} else if(Constants.BANDERAS_CONTRATAR.equals(name)){
				BanderasBMovilFileManager.getCurrent().setBanderasBMovilContratar(v);
			} else if(Constants.BANDERAS_CONTRATAR2x1.equals(name)){
				BanderasBMovilFileManager.getCurrent().setBanderasBMovilContratar2x1(v);
			} else if(Constants.BANDERAS_CAMBIO_CELULAR.equals(name)){
				BanderasBMovilFileManager.getCurrent().setBanderasBMovilCambioCelular(v);
			}else if(Constants.BANDERAS_INDICADOR_CONTRATACION.equals(name)){
				BanderasBMovilFileManager.getCurrent().setBanderasBMovilIndicadorContratacion(v);
			}
		} catch (Throwable t) {
			if(ServerCommons.ALLOW_LOG) Log.e("Session.java", "saveBanderasBMovil error");
		}	
	}

	/**
	 * Carga la tabla TemporalST.
	 * 
	 * @return el contenido del atributo de la tabla. Nulo en caso de error
	 */
	public TemporalST loadTemporalST() {
		TemporalST temporalST = null;

		try {
			final String perfil = TemporalSTFileManager.getCurrent().getTemporalSTPerfil();
			final String compannia = TemporalSTFileManager.getCurrent().getTemporalSTCompania();
			final String contrasenna = TemporalSTFileManager.getCurrent().getTemporalSTContrasena();
			final String correo = TemporalSTFileManager.getCurrent().getTemporalSTCorreo();
			final String tarjeta = TemporalSTFileManager.getCurrent().getTemporalSTTarjeta();
			final String celular = TemporalSTFileManager.getCurrent().getTemporalSTCelular();

			temporalST = new TemporalST(celular, tarjeta, compannia, contrasenna, correo, perfil);

		} catch (Exception ex) {
			if(ServerCommons.ALLOW_LOG)Log.e(this.getClass().getSimpleName(),
					"Error while getting the data from the TemporalSTFile.", ex);
		}

		return temporalST;
	}

	/**
	 * Guarda la tabla TemporalST.
	 * 
	 * @param mapNameValue
	 *            un mapa con los nombres y los valores de los atributos
	 */
	public void saveTemporalST(final TemporalST temporalST, final boolean createIfNecessary) {
		try {
			TemporalSTFileManager.getCurrent().setTemporalSTCelular(temporalST.getCelular());
			TemporalSTFileManager.getCurrent().setTemporalSTCompania(temporalST.getCompannia());
			TemporalSTFileManager.getCurrent().setTemporalSTContrasena(temporalST.getContrasenna());
			TemporalSTFileManager.getCurrent().setTemporalSTCorreo(temporalST.getCorreo());
			TemporalSTFileManager.getCurrent().setTemporalSTPerfil(temporalST.getPerfil());
			TemporalSTFileManager.getCurrent().setTemporalSTTarjeta(temporalST.getTarjeta());
		} catch (Throwable t) {
			if(ServerCommons.ALLOW_LOG) Log.e("Session.java", "saveTemporalST error");
		}			
	}

	/**
	 * Borrar la tabla TemporalST.
	 * 
	 */
	public void deleteTemporalST() {			
		try {
			TemporalSTFileManager.getCurrent().setTemporalSTCelular("");
			TemporalSTFileManager.getCurrent().setTemporalSTCompania("");
			TemporalSTFileManager.getCurrent().setTemporalSTContrasena("");
			TemporalSTFileManager.getCurrent().setTemporalSTCorreo("");
			TemporalSTFileManager.getCurrent().setTemporalSTPerfil("");
			TemporalSTFileManager.getCurrent().setTemporalSTTarjeta("");
		} catch (Throwable t) {
			if(ServerCommons.ALLOW_LOG) Log.e("Session.java", "deleteTemporalST error");
		}		
	}

	/**
	 * Carga la tabla TemporalCambioTelefono.
	 * 
	 * @return el contenido del atributo de la tabla. Nulo en caso de error
	 */
	public TemporalCambioTelefono loadTemporalCambioTelefono() {
		TemporalCambioTelefono temporalCambioTelefono = null;
		
		try {
			final String tarjeta = TemporalCambioTelefonoFileManager.getCurrent().getTemporalCambioTelefonoTarjeta();

			temporalCambioTelefono = new TemporalCambioTelefono(tarjeta);

		} catch (Exception ex) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(),
					"Error while getting the data from the TemporalCambioTelefonoFile.", ex);
		}

		
		return temporalCambioTelefono;
	}
	

	/**
	 * Guarda la tabla TemporalCambioTelefono.
	 * 
	 * @param mapNameValue
	 *            un mapa con los nombres y los valores de los atributos
	 */
	public void saveTemporalCambioTelefono(final TemporalCambioTelefono temporalCambioTelefono, final boolean createIfNecessary) {
		try {
			TemporalCambioTelefonoFileManager.getCurrent().setTemporalCambioTelefonoTarjeta(temporalCambioTelefono.getTarjeta());
		} catch (Throwable t) {
			if(ServerCommons.ALLOW_LOG) Log.e("Session.java", "saveTemporalCambioTelefono error");
		}	
	}

	/**
	 * Borrar la tabla TemporalCambioTelefono.
	 * 
	 */
	public void deleteTemporalCambioTelefono() {		
		try {
			TemporalCambioTelefonoFileManager.getCurrent().setTemporalCambioTelefonoTarjeta("");
		} catch (Throwable t) {
			if(ServerCommons.ALLOW_LOG) Log.e("Session.java", "deleteTemporalCambioTelefono error");
		}	
	}
	
///////////////////Here are the methods wich are going to be used for SPEI OPERATIVA DIARIA.////////////
	
	
	
		/**
		 * This method is gonna save the values from the Json in ListA and ListB
		 * @param rawJSON
		 */
		
		public void parserCS(final String rawJSON)
		{
			JSONObject rootElement = null;
			JSONObject currentElement = null;
			JSONArray elements = null;		
			Account account=null;
			try {
				rootElement = new JSONObject(rawJSON);
				
				if(rootElement.getString("switch").equals("TRUE"))
					setSwitchSPEI(true);
				else
					setSwitchSPEI(false);
				
				elements = rootElement.getJSONArray("cuentas");
			} catch (JSONException ex) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "No se pudo interpretar el json.", ex);
				
			}
			
			
			if (null == elements) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(),
				"No se encontro la lista de elementos necesaria.");
			}

	
			for (int i = 0; i < elements.length(); i++) {
				try {
					//Elements to be added to a list.
					account = new Account();
					
					//Getting every array item.
					currentElement = elements.getJSONObject(i);
					
					//Parsing.
					account.setNumber(currentElement.getString(Constants.NUMERO_TARJETA));
					account.setBalance(Tools.getDoubleAmountFromServerString(currentElement.getString(Constants.SALDO)));
					if(currentElement.getString(Constants.VISIBLE).equals("SI"))
						account.setVisible(true);
					else
						account.setVisible(false);
					
					account.setCurrency(currentElement.getString(Constants.MONEDA));
					account.setType(currentElement.getString(Constants.TIPO_CUENTA));
					account.setConcept(currentElement.getString(Constants.CONCEPTO));
					account.setAlias(currentElement.getString(Constants.ALIAS));
					
					account.setCelularAsociado(currentElement.getString(Constants.CELULAR_ASOCIADO));
					account.setCodigoCompania(currentElement.getString(Constants.CODIGO_COMPANIA));
					account.setDescripcionCompania(currentElement.getString(Constants.DESCRIPCION_COMPANIA));
					account.setFechaUltimaModificacion(currentElement.getString(Constants.FECHA_U_M));
					
					if(account.getType().equals("LI") || account.getType().equals("AH") || 
							account.getType().equals("CH")||account.getType().equals("CE")||
									account.getType().equals("TC")|| account.getType().equals("TP") || account.getType().equals("PT"))
					 {
					
					if(currentElement.getString(Constants.INDICADOR_SPEI).equals("TRUE"))
					{
						account.setIndicadorSPEI("S");
						listA.add(account);
					}
					else
					{
						account.setIndicadorSPEI("N");
						listB.add(account);
					}
				}
					
				} catch (JSONException jsonEx) {
					if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(),
							"Error while parsing the account with index: " + i + "\nfor the json: " + rawJSON, jsonEx);
					continue;
				}
			}
			
		}
		
		/**
		 * 
		 * @return All the accounts to be used according to service hours.
		 */
		
		public  Account[] getAccountsIB()
		{
			Account [] accounts = null;
			int i;
			
			if(listA.size()>0)
			{
				if(Session.getInstance(SuiteApp.appContext).isOpenHours())
				{
					accounts = new Account[listA.size()+listB.size()];
					
					for(i=0;i<listA.size();i++)
					{
						accounts[i]=listA.get(i);
						
					}
					
					for(i=listA.size();i<listB.size()+listA.size();i++)
					{
						accounts[i]=listB.get(i-listA.size());
					}
				}
				else
				{
					accounts = new Account[listA.size()];
					
					
					for(i=0;i<listA.size();i++)
					{
						accounts[i]=listA.get(i);					
					}
				}
			}
			else
			{
				accounts = new Account[listB.size()];
				

				for(i=0;i<listB.size();i++)
				{
					accounts[i]=listB.get(i);

				}
			}
			
			return accounts;
			
		}
		
		/**
		 * 
		 * @return all the accounts from CS data.
		 */
		
		
		public Account[] getAccounts()
		{
		//try{
			if(listA==null||listB==null){
				return null;
			}
			Account [] accounts = null;
			int i;

			accounts = new Account[listA.size()+listB.size()];

			for(i=0;i<listA.size();i++)
			{
				accounts[i]=listA.get(i);
				//if(Server.ALLOW_LOG) Log.e("Lista A","Lista A");
				//if(Server.ALLOW_LOG) Log.e("NC",String.valueOf(listA.get(i).getNumber()));
				//if(Server.ALLOW_LOG) Log.e("Balance",String.valueOf(listA.get(i).getBalance()));
				//if(Server.ALLOW_LOG) Log.e("Visible",String.valueOf(listA.get(i).isVisible()));
				//if(Server.ALLOW_LOG) Log.e("Currency",String.valueOf(listA.get(i).getCurrency()));
				//if(Server.ALLOW_LOG) Log.e("Type",String.valueOf(listA.get(i).getType()));
				//if(Server.ALLOW_LOG) Log.e("Concept",String.valueOf(listA.get(i).getConcept()));
				//if(Server.ALLOW_LOG) Log.e("Alias",String.valueOf(listA.get(i).getAlias()));
				//if(Server.ALLOW_LOG) Log.e("CelularAsociado",String.valueOf(listA.get(i).getCelularAsociado()));
				//if(Server.ALLOW_LOG) Log.e("CodigoCompania",String.valueOf(listA.get(i).getCodigoCompania()));
				//if(Server.ALLOW_LOG) Log.e("DescripcionCompania",String.valueOf(listA.get(i).getDescripcionCompania()));
				//if(Server.ALLOW_LOG) Log.e("IndicadorSPEI",String.valueOf(listA.get(i).getIndicadorSPEI()));
				//if(Server.ALLOW_LOG) Log.e("FechaUltimaModificacion",String.valueOf(listA.get(i).getFechaUltimaModificacion()));

			}

			for(i=listA.size();i<listB.size()+listA.size();i++)
			{
				accounts[i]=listB.get(i-listA.size());
				
				//if(Server.ALLOW_LOG) Log.e("Lista B","Lista B");
				//if(Server.ALLOW_LOG) Log.e("NC",String.valueOf(listB.get(i-listA.size()).getNumber()));
				//if(Server.ALLOW_LOG) Log.e("Balance",String.valueOf(listB.get(i-listA.size()).getBalance()));
				//if(Server.ALLOW_LOG) Log.e("Visible",String.valueOf(listB.get(i-listA.size()).isVisible()));
				//if(Server.ALLOW_LOG) Log.e("Currency",String.valueOf(listB.get(i-listA.size()).getCurrency()));
				//if(Server.ALLOW_LOG) Log.e("Type",String.valueOf(listB.get(i-listA.size()).getType()));
				//if(Server.ALLOW_LOG) Log.e("Concept",String.valueOf(listB.get(i-listA.size()).getConcept()));
				//if(Server.ALLOW_LOG) Log.e("Alias",String.valueOf(listB.get(i-listA.size()).getAlias()));
				//if(Server.ALLOW_LOG) Log.e("CelularAsociado",String.valueOf(listB.get(i-listA.size()).getCelularAsociado()));
				//if(Server.ALLOW_LOG) Log.e("CodigoCompania",String.valueOf(listB.get(i-listA.size()).getCodigoCompania()));
				//if(Server.ALLOW_LOG) Log.e("DescripcionCompania",String.valueOf(listB.get(i-listA.size()).getDescripcionCompania()));
				//if(Server.ALLOW_LOG) Log.e("IndicadorSPEI",String.valueOf(listB.get(i-listA.size()).getIndicadorSPEI()));
				//if(Server.ALLOW_LOG) Log.e("FechaUltimaModificacion",String.valueOf(listB.get(i-listA.size()).getFechaUltimaModificacion()));
			}
		//}
			//catch(NullPointerException ex){
			//	return null;
		//	}

			return accounts;
			
		}
		
		// Intergracion KeyChainAPI
		public KeyStoreWrapper getKeyStoreWrapper() {
			return kswrapper;
		}
		//----
		
		public static void closeSession(){
			if(theInstance!=null){
				theInstance=null;
			}
		}
		
}