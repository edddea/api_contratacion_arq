/*
 * Copyright (c) 2009 BBVA. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information").  You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomercoms.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * MovementExtract wraps an account extract, that is, the current situation of
 * the account and the list of the last movements
 * 
 * @author Stefanini IT Solutions
 */
public class MovementExtract  implements ParsingHandler {

    /**
	 * Serial Version UID
	 */
	private static final long serialVersionUID = 3272545845918202838L;

	/**
     * Account number
     */
    private String accountNumber = null;

    /**
     * Account type
     */
    private String accountType = null;

    /**
     * The current balance
     */
    private String balance = null;

    /**
     * The previous balance
     */
    private String previousBalance = null;

    /**
     * The payment deadline
     */
    private String deadline = null;

    /**
     * The minimum payment
     */
    private String minPayment = null;

    /**
     * The movements
     */
    private ArrayList<Movement> movements = null;

    /**
     * The balance date
     */
    private String date = null;

    /**
     * The periodo
     */
	private String periodo = null; 

    /**
     * Default constructor
     */
    public MovementExtract() {
    	movements = new ArrayList<Movement>();
    }

    /**
     * Constructor with parameters
     * @param accountNumber the account number
     * @param balance the current balance
     * @param previousBalance the previous balance
     * @param deadline the payment deadline
     * @param minPayment the minimum payment
     * @param movements the movements
     * @param date the balance date
     * @param periodo the periodo
     */
    public MovementExtract(final String accountNumber, final String balance, final String previousBalance,
            final String deadline, final String minPayment, final ArrayList<Movement> movements, final String date, final String periodo) {
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.previousBalance = previousBalance;
        this.deadline = deadline;
        this.minPayment = minPayment;
        this.movements = movements;
        this.date = date;
        this.periodo = periodo;
    }

    /**
     * Get the account number
     * @return the account number
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Get the current balance
     * @return the current balance
     */
    public String getBalance() {
        return balance;
    }

    /**
     * get the payment deadline
     * @return the payment deadline
     */
    public String getDeadline() {
        return deadline;
    }

    /**
     * Get the minimum payment
     * @return the minimum payment
     */
    public String getMinPayment() {
        return minPayment;
    }

    /**
     * Get the movements
     * @return the last movements
     */
    public ArrayList<Movement> getMovements() {
        return movements;
    }

    /**
     * Get the previous balance
     * @return the previous balance
     */
    public String getPreviousBalance() {
        return previousBalance;
    }


    /**
     * get the balance date
     * @return the balance date
     */
    public String getDate() {
        return date;
    }

    /**
     * Sets the account number
     * @param accountNumber the account number
     */
    public void setAccountNumber(final String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * Gets the account type
     * @return the account type
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Set the account type
     * @param accountType the account type
     */
    public void setAccountType(final String accountType) {
        this.accountType = accountType;
    }


    /**
	 * @return the periodo
	 */
	public String getPeriodo() {
		return periodo;
	}

	/**
	 * @param periodo the periodo to set
	 */
	public void setPeriodo(final String periodo) {
		this.periodo = periodo;
	}

	/**
     * Process the login response and store the attributes
     * @param parser reference to the parser
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     */
    public void process(final Parser parser) throws IOException, ParsingException {
    	this.periodo = parser.parseNextValue("PE");
    	this.previousBalance = parser.parseNextValue("SC", false);
        this.minPayment = parser.parseNextValue("PM", false);
        this.deadline = parser.parseNextValue("FL", false);
        this.date = parser.parseNextValue("FE");
        this.movements = parseMovements(parser);
    }

    /**
     * Parse movements from parser data
     * @param parser the parser
     * @return the movements
     * @throws IOException on communication errors
     * @throws ParsingException on parsing errors
     * @throws NumberFormatException on format errors 
     */
    private ArrayList<Movement> parseMovements(final Parser parser) throws IOException, NumberFormatException, ParsingException {
        final int movementCount = Integer.parseInt(parser.parseNextValue("OC"));

        final ArrayList<Movement> result = new ArrayList<Movement>(movementCount);
        Movement movement;
        for(int i = 0; i < movementCount; i++) {
            final String concept = parser.parseNextValue("CP");
            final String opDate = Tools.formatShortDate(parser.parseNextValue("FE"));
            boolean negativeSign = false;
            if (Constants.CREDIT_TYPE.equals(this.accountType)) {
                if (Movement.DEPOSIT_CONCEPT.equals(concept)) {
                    negativeSign = true;
                }
            } else {
                if (Movement.CHARGE_CONCEPT.equals(concept)) {
                    negativeSign = true;
                }
            }
            
            final String amount = Tools.formatAmount(parser.parseNextValue("IM"), negativeSign);
            final String description = parser.parseNextValue("DE");
            final String authDate = Tools.formatDate(parser.parseNextValue("AU"));
            final String appDate = Tools.formatDate(parser.parseNextValue("AP"));
            movement = new Movement(opDate, concept, amount, description, authDate, appDate);
            result.add(movement);
        }
        return result;
    }

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

}
