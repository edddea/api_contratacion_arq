package suitebancomercoms.aplicaciones.bmovil.classes.common;

import android.util.Log;

import com.bancomer.base.SuiteApp;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

import suitebancomer.aplicaciones.bmovil.classes.model.Catalog;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public final class CatFileManager {
	
	/**
	 * Nombre del archivo de catalogos.
	 */
	private static final String FILE_NAME = "Cat.prop";
	
	/**
	 * Manejador del archivo de catalogos.
	 */
	private File file;
			
	/**
	 * La instancia de la clase.
	 */
	private static CatFileManager manager = null;
	
	/**
	 * @return La instancia de la clase.
	 */
	public static CatFileManager getCurrent() {
		if(null == manager)
			manager = new CatFileManager();
		return manager;
	}
	
	/**
	 * Inicializa el administrador de propiedades. 
	 */
	private CatFileManager() {
		this.file = new File(SuiteApp.appContext.getFilesDir(), FILE_NAME);
		
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch(IOException ioEx) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error al crear el archivo Cat.", ioEx);
				return;
			}
		}
	}
	
	/**
	 * Guarda el archivo de catálogos.
	 */
	public void guardaCatalogos(final Catalog... catalogs) {
		ObjectOutputStream os = null;
		try {
			os = new ObjectOutputStream(new FileOutputStream(this.file));		
			os.writeObject(catalogs);
			if(ServerCommons.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Catalogos guardados.");
		} catch (Exception e) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error en guardaCatalogos.", e);
		} finally{
			try {
				if(null != os){
					os.close();
				}
			} catch (Exception e) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error al cerrar en guardaCatalogos.", e);
			}
		}
	}
	
	/**
	 * Carga el archivo de catalogos.
	 */
	public Catalog[] cargaCatalogos(){
		Catalog[] catalogs = null;
		ObjectInputStream is = null;
		try {
			is = new ObjectInputStream(new FileInputStream(file));
			catalogs = (Catalog[]) is.readObject();
			Tools.writeLogi(this.getClass().getSimpleName(), "Catalogos cargados.", ServerCommons.ALLOW_LOG);
		} catch (EOFException e) {
			Tools.writeLogi(this.getClass().getSimpleName(), "El archivo de Catalogos esta vacio.", ServerCommons.ALLOW_LOG);
		} catch (StreamCorruptedException e) {
			Tools.writeLogi(this.getClass().getSimpleName(), "El archivo de Catalogos es de una version antigua e ilegible.", ServerCommons.ALLOW_LOG);
		} catch (Exception e) {
			Tools.writeLoge(this.getClass().getSimpleName(), "Error en cargaCatalogos.", ServerCommons.ALLOW_LOG);
		} finally {
			try{
				if(null != is){
					is.close();
				}
			} catch (Exception e) {
				Tools.writeLoge(this.getClass().getSimpleName(), "Error al cerrar en cargaCatalogos.", ServerCommons.ALLOW_LOG);
			}
		}
		return catalogs;
	}
	
	/**
	 * Vacía el archivo de catalogos.
	 */
	public void vaciaArchivoCatalogos() {
		ObjectOutputStream os = null;
		try {
			os = new ObjectOutputStream(new FileOutputStream(this.file));
			if(ServerCommons.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Catalogos vaciados.");
		} catch (Exception e) {
			if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error en vaciaArchivoCatalogos.", e);
		} finally{
			try {
				if(null != os){
					os.close();
				}
			} catch (Exception e) {
				if(ServerCommons.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error al cerrar en vaciaArchivoCatalogos.", e);
			}
		}
		
	}
}
