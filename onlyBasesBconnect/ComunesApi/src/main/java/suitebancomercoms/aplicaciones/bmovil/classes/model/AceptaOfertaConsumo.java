package suitebancomercoms.aplicaciones.bmovil.classes.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public class AceptaOfertaConsumo implements ParsingHandler{
	String productoSal;
	String desProSal;
	String numeroCredito;
	String impsegSal;
	String montoSal;
	String tasaSal;
	String proBovSal;
	String montoMinSal;
	String plazoSal;
	String tipoPagoSal;
	String estatusSal;
	String scoreSal;
	Promociones[] promociones;
	String PM;
	
	public String getProductoSal() {
		return productoSal;
	}

	public void setProductoSal(final String productoSal) {
		this.productoSal = productoSal;
	}

	public String getDesProSal() {
		return desProSal;
	}

	public void setDesProSal(final String desProSal) {
		this.desProSal = desProSal;
	}

	public String getNumeroCredito() {
		return numeroCredito;
	}

	public void setNumeroCredito(final String numeroCredito) {
		this.numeroCredito = numeroCredito;
	}

	public String getImpsegSal() {
		return impsegSal;
	}

	public void setImpsegSal(final String impsegSal) {
		this.impsegSal = impsegSal;
	}

	public String getMontoSal() {
		return montoSal;
	}

	public void setMontoSal(final String montoSal) {
		this.montoSal = montoSal;
	}

	public String getTasaSal() {
		return tasaSal;
	}

	public void setTasaSal(final String tasaSal) {
		this.tasaSal = tasaSal;
	}

	public String getProBovSal() {
		return proBovSal;
	}

	public void setProBovSal(final String proBovSal) {
		this.proBovSal = proBovSal;
	}

	public String getMontoMinSal() {
		return montoMinSal;
	}

	public void setMontoMinSal(final String montoMinSal) {
		this.montoMinSal = montoMinSal;
	}

	public String getPlazoSal() {
		return plazoSal;
	}

	public void setPlazoSal(final String plazoSal) {
		this.plazoSal = plazoSal;
	}

	public String getTipoPagoSal() {
		return tipoPagoSal;
	}

	public void setTipoPagoSal(final String tipoPagoSal) {
		this.tipoPagoSal = tipoPagoSal;
	}

	public String getEstatusSal() {
		return estatusSal;
	}

	public void setEstatusSal(final String estatusSal) {
		this.estatusSal = estatusSal;
	}

	public String getScoreSal() {
		return scoreSal;
	}

	public void setScoreSal(final String scoreSal) {
		this.scoreSal = scoreSal;
	}

	public Promociones[] getPromociones() {
		if(Tools.isNull(promociones)) {
			return new Promociones[0];
		} else {
			return promociones.clone();
		}
	}

	public void setPromociones(final Promociones... promociones) {
		this.promociones = promociones;
	}

	public String getPM() {
		return PM;
	}

	public void setPM(final String pM) {
		PM = pM;
	}

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		productoSal=parser.parseNextValue("productoSal",false);
		desProSal=parser.parseNextValue("desProSal",false);
		numeroCredito=parser.parseNextValue("numeroCredito",false);
		impsegSal=parser.parseNextValue("impsegSal",false);
		montoSal=parser.parseNextValue("montoSal",false);
		tasaSal=parser.parseNextValue("tasaSal",false);
		proBovSal=parser.parseNextValue("proBovSal",false);
		montoMinSal=parser.parseNextValue("montoMinSal",false);
		plazoSal=parser.parseNextValue("plazoSal",false);
		tipoPagoSal=parser.parseNextValue("tipoPagoSal",false); 
		estatusSal=parser.parseNextValue("estatusSal",false); 
		scoreSal=parser.parseNextValue("scoreSal",false); 
		//PM=parser.parseNextValue("PM");
		PM=parser.parseNextValue("PM",false);
		try {
			if(PM!=null){
			if(PM.equals("SI")){
			final JSONObject jsonObjectPromociones= new JSONObject(parser.parseNextValue("LP"));
			final JSONArray arrPromocionesJson =jsonObjectPromociones.getJSONArray("campanias");
			final ArrayList<Promociones> arrPromociones = new ArrayList<Promociones>();
			for (int i = 0; i < arrPromocionesJson.length(); i++) {
				final JSONObject jsonPromocion = (JSONObject) arrPromocionesJson.get(i);
				final Promociones promo = new Promociones();
				promo.setCveCamp(jsonPromocion.getString("cveCamp"));
				promo.setDesOferta(jsonPromocion.getString("desOferta"));
				promo.setMonto(jsonPromocion.getString("monto"));
				arrPromociones.add(promo);
			}			
			this.promociones = arrPromociones.toArray(new Promociones[arrPromociones.size()]);			
			}
		}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			if(ServerCommons.ALLOW_LOG) e.printStackTrace();
		}
		
	}

}
