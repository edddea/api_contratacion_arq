package suitebancomercoms.aplicaciones.bmovil.classes.model;

import com.bancomer.base.SuiteApp;

import org.json.JSONObject;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.common.CatalogoAutenticacionFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

public class ConsultaEstatusMantenimientoData implements ParsingHandler {
	// #region Variables.
	/**
	 * Numero de cliente.
	 */
	private String numeroCliente;

	/**
	 * Estado del servicio (Bmovil).
	 */
	private String estatusServicio;

	/**
	 * Compa��a telefónica del cliente.
	 */
	private String companiaCelular;

	/**
	 * Fecha de contratación del servicio.
	 */
	private String fechaContratacion;

	/**
	 * Fecha sel sistema.
	 */
	private String fechaSistema;

	/**
	 * Perfil del cliente.
	 */
	private String perfilCliente;

	/**
	 * Instrumento de seguridad del cliente.
	 */
	private String tipoInstrumento;

	/**
	 * Estatus del instrumento de seguridad.
	 */
	private String estatusInstrumento;

	/**
	 * Correo electrónico del cliente.
	 */
	private String emailCliente;

	/**
	 * Nombre del cliente.
	 */
	private String nombreCliente;

	/**
	 * Estatus de las alertas Bancomer.
	 */
	private String estatusAlertas;

	// #endregion

	// #region Getters y Setters.
	/**
	 * @return Numero de cliente.
	 */
	public String getNumeroCliente() {
		return numeroCliente;
	}

	/**
	 * @return Estado del servicio (Bmovil).
	 */
	public String getEstatusServicio() {
		return estatusServicio;
	}

	/**
	 * @return Compa��a telefónica del cliente.
	 */
	public String getCompaniaCelular() {
		return companiaCelular;
	}

	/**
	 * @return Fecha de contratación del servicio.
	 */
	public String getFechaContratacion() {
		return fechaContratacion;
	}

	/**
	 * @return Fecha sel sistema.
	 */
	public String getFechaSistema() {
		return fechaSistema;
	}

	/**
	 * @return Perfil del cliente.
	 */
	public String getPerfilCliente() {
		return perfilCliente;
	}

	/**
	 * @return Instrumento de seguridad del cliente.
	 */
	public String getTipoInstrumento() {
		return tipoInstrumento;
	}

	/**
	 * @return Estatus del instrumento de seguridad.
	 */
	public String getEstatusInstrumento() {
		return estatusInstrumento;
	}

	/**
	 * @return Correo electrónico del cliente.
	 */
	public String getEmailCliente() {
		return emailCliente;
	}

	/**
	 * @return Nombre del cliente.
	 */
	public String getNombreCliente() {
		return nombreCliente;
	}

	/**
	 * @return Estatus de las alertas Bancomer.
	 */
	public String getEstatusAlertas() {
		return estatusAlertas;
	}

	// #endregion

	public ConsultaEstatusMantenimientoData() {
		numeroCliente = null;
		estatusServicio = null;
		companiaCelular = null;
		fechaContratacion = null;
		fechaSistema = null;
		perfilCliente = null;
		tipoInstrumento = null;
		estatusInstrumento = null;
		emailCliente = null;
		nombreCliente = null;
		estatusAlertas = null;
	}

	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
		numeroCliente = null;
		estatusServicio = null;
		companiaCelular = null;
		fechaContratacion = null;
		fechaSistema = null;
		perfilCliente = null;
		tipoInstrumento = null;
		estatusInstrumento = null;
		emailCliente = null;
		nombreCliente = null;
		estatusAlertas = null;
	}

	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		numeroCliente = parser.parseNextValue("numeroCliente");
		estatusServicio = parser.parseNextValue("estatusServicio");
		companiaCelular = parser.parseNextValue("companiaCelular");
		fechaContratacion = parser.parseNextValue("fechaContratacion");
		if (null == fechaContratacion)
			fechaContratacion = "";
		fechaSistema = parser.parseNextValue("fechaSistema");
		perfilCliente = parser.parseNextValue("perfilCliente");
		tipoInstrumento = parser.parseNextValue("tipoInstrumento");
		estatusInstrumento = parser.parseNextValue("estatusInstrumento");
		emailCliente = parser.parseNextValue("emailCliente");
		nombreCliente = parser.parseNextValue("nombreCliente");
		estatusAlertas = parser.parseNextValue("estatusAlertas");

		final Session session = Session.getInstance(SuiteApp.appContext);
		final String autenticacion = parser.parseNextValue("autenticacion");
		if (!Constants.EMPTY_STRING
				.equals(autenticacion)) {
			final JSONObject autJsonObj = parser.parserNextObject("autenticacion",
					false);
			final String autJson = autJsonObj.toString();

			if (!Tools.isEmptyOrNull(autJson) && autJson.length() > 3) {
				session.setAuthenticationJson(autJson);
				session.saveAuthenticationData(true);
			} else {
				CatalogoAutenticacionFileManager.getCurrent().cargaCatalogoAutenticacion();
			}
		}else{
			CatalogoAutenticacionFileManager.getCurrent().cargaCatalogoAutenticacion();
		}
		final String telInnerJson = parser.parseNextValue("telefonicasMantenimiento",
				false);
		final String telJson = "{\"telefonicasMantenimiento\":" + telInnerJson + "}";
		if (!Tools.isEmptyOrNull(telInnerJson)) {
			final String verTelefonicas = parser
					.parseNextValue("versionCatTelefonico");
			session.saveCatalogoCompaniasTelefonicas(telJson, verTelefonicas);
		} else {
			session.loadCatalogoCompaniasTelefonicas();
		}
	}

}
