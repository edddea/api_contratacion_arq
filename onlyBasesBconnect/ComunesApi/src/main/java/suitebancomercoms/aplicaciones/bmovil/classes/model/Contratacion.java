package suitebancomercoms.aplicaciones.bmovil.classes.model;


import bancomer.api.common.commons.Constants.Perfil;

/**
 * Modelo de datos para el flujo de contratación.
 */
public class Contratacion {
	// #region Variables.
	/**
	 * Tipo de perfil.
	 */
	private Perfil perfil;
	
	/**
	 * Número de celular.
	 */
	private String numCelular;
	
	/**
	 * Nombre de la compa�ia celular.
	 */
	private String companiaCelular;
	
	/**
	 * Número del cliente.
	 */
//	private String numeroCliente;
	
	/**
	 * Número de cuenta.
	 */
	private String numeroCuenta;
	
	/**
	 * Número de tarjeta.
	 */
	private String numeroTarjeta;
	
	/**
	 * Nombre del cliente.
	 */
//	private String nombreCliente;
	
	/**
	 * Email del cliente.
	 */
	private String emailCliente;
	
	/**
	 * Estatus de las alertas.
	 */
	private String estatusAlertas;
	
	/**
	 * Tipo de instrumento.
	 */
	private String tipoInstrumento;
	
	/**
	 * Estatus del instrumento.
	 */
	private String estatusInstrumento;
	
	/**
	 * Fecha de contratación.
	 */
	private String fechaContratacion;
	
	/**
	 * Fecha de modificación. 
	 */
	private String fechaModificacion;
	
	/**
	 * Contraseña del usuario
	 */
	private String contrasena;
	// #endregion
	
	// #region Getters / Setters
	/**
	 * @return El tipo de perfil.
	 */
	public Perfil getPerfil() {
		return perfil;
	}
	
	/**
	 * @param perfil El tipo de perfil a establecer.
	 */
	public void setPerfil(final Perfil perfil) {
		this.perfil = perfil;
	}
	
	/**
	 * @return El número de celular.
	 */
	public String getNumCelular() {
		return numCelular;
	}
	
	/**
	 * @param numCelular El número de celular a establecer.
	 */
	public void setNumCelular(final String numCelular) {
		this.numCelular = numCelular;
	}
	
	/**
	 * @return the numeroCliente
	 */
//	public String getNumeroCliente() {
//		return numeroCliente;
//	}

	/**
	 * @param numeroCliente the numeroCliente to set
	 */
//	public void setNumeroCliente(String numeroCliente) {
//		this.numeroCliente = numeroCliente;
//	}

	/**
	 * @return the numeroCuenta
	 */
	public String getNumeroCuenta() {
		return numeroCuenta;
	}

	/**
	 * @param numeroCuenta the numeroCuenta to set
	 */
	public void setNumeroCuenta(final String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}

	/**
	 * @return the nombreCliente
	 */
//	public String getNombreCliente() {
//		return nombreCliente;
//	}

	/**
	 * @param nombreCliente the nombreCliente to set
	 */
//	public void setNombreCliente(String nombreCliente) {
//		this.nombreCliente = nombreCliente;
//	}

	/**
	 * @return the emailCliente
	 */
	public String getEmailCliente() {
		return emailCliente;
	}

	/**
	 * @param emailCliente the emailCliente to set
	 */
	public void setEmailCliente(final String emailCliente) {
		this.emailCliente = emailCliente;
	}

	/**
	 * @return the aestatusAlertas
	 */
	public String getEstatusAlertas() {
		return estatusAlertas;
	}

	/**
	 * @param aestatusAlertas the aestatusAlertas to set
	 */
	public void setEstatusAlertas(final String estatusAlertas) {
		this.estatusAlertas = estatusAlertas;
	}

	/**
	 * @return the tipoInstrumento
	 */
	public String getTipoInstrumento() {
		return tipoInstrumento;
	}

	/**
	 * @param tipoInstrumento the tipoInstrumento to set
	 */
	public void setTipoInstrumento(final String tipoInstrumento) {
		this.tipoInstrumento = tipoInstrumento;
	}

	/**
	 * @return the estatusInstrumento
	 */
	public String getEstatusInstrumento() {
		return estatusInstrumento;
	}

	/**
	 * @param estatusInstrumento the estatusInstrumento to set
	 */
	public void setEstatusInstrumento(final String estatusInstrumento) {
		this.estatusInstrumento = estatusInstrumento;
	}

	/**
	 * @return the fechaContratacion
	 */
	public String getFechaContratacion() {
		return fechaContratacion;
	}

	/**
	 * @param fechaContratacion the fechaContratacion to set
	 */
	public void setFechaContratacion(final String fechaContratacion) {
		this.fechaContratacion = fechaContratacion;
	}

	/**
	 * @return the fechaModificacion
	 */
	public String getFechaModificacion() {
		return fechaModificacion;
	}

	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(final String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	/**
	 * @return Nombre de la compa�ia celular.
	 */
	public String getCompaniaCelular() {
		return companiaCelular;
	}

	/**
	 * @param companiaCelular Nombre de la compa�ia celular.
	 */
	public void setCompaniaCelular(final String companiaCelular) {
		this.companiaCelular = companiaCelular;
	}

	/**
	 * @return Número de tarjeta.
	 */
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	/**
	 * @param numeroTarjeta Número de tarjeta.
	 */
	public void setNumeroTarjeta(final String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
	// #endregion
	
	/**
	 * Constructor por defecto.
	 */
	public Contratacion() {
		perfil = null;
		numCelular = null;
		companiaCelular = null;
		numeroCuenta = null;
		numeroTarjeta = null;
		emailCliente = null;
		estatusAlertas = null;
		tipoInstrumento = null;
		estatusInstrumento = null;
		fechaContratacion = null;
		fechaModificacion = null;
		contrasena = null;
	}

	public String getContrasena() {
		return contrasena;
	}

	public void setContrasena(final String contrasena) {
		this.contrasena = contrasena;
	}
}
