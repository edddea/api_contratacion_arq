package mtto.suitebancomer.aplicaciones.bmovil.classes.common;

import android.text.Editable;
import android.text.TextWatcher;
import mtto.suitebancomer.classes.gui.controllers.BaseViewController;

/**
 * Custom TextWatcher to ensure the call of the onUserInteraction method when the text is changed.
 * @see BaseViewController#onUserInteraction()
 */
public class BmovilTextWatcher implements TextWatcher{
	/**
	 * The controller that holds the editable text field.
	 */
	private BaseViewController controller;
	
	/**
	 * Constructor.
	 * @param controller The controller that holds the editable text field. 
	 */
	public BmovilTextWatcher(final BaseViewController controller) {
		this.controller = controller;
	}

	@Override
	public void afterTextChanged(final Editable s) {
		if(null != controller) {
            controller.onUserInteraction();
        }
	}

	@Override
	public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
        //Empty method
    }

	@Override
	public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
        //Empty method
    }
}
