package mtto.suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Modelo de datos para Mantenimiento Alertas.
 * 
 * @author CGI
 */
public class MantenimientoAlertasResultado implements ParsingHandler {

	/** Serial id. */
	private static final long serialVersionUID = -7265044850173016807L;

	/** El folio. */
	private String folio;

	/** La fecha. */
	private String fecha;

	/** La hora. */
	private String hora;

	/**
	 * Constructor por defecto.
	 */
	public MantenimientoAlertasResultado() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler#process(
	 * suitebancomer.aplicaciones.bmovil.classes.io.Parser)
	 */
	@Override
	public void process(final Parser parser) throws IOException, ParsingException {
        //Empty method
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see suitebancomer.aplicaciones.bmovil.classes.io.ParsingHandler#process(
	 * suitebancomer.aplicaciones.bmovil.classes.io.ParserJSON)
	 */
	@Override
	public void process(final ParserJSON parser) throws IOException, ParsingException {
		this.folio = parser.parseNextValue(ServerConstants.FOLIO);
		this.fecha = parser.parseNextValue(ServerConstants.FECHA);
		this.hora = parser.parseNextValue(ServerConstants.HORA);
	}

	/**
	 * Obtiene el folio.
	 * 
	 * @return el folio
	 */
	public String getFolio() {
		return folio;
	}

	/**
	 * Establece el folio.
	 * 
	 * @param folio
	 *            el folio a establecer
	 */
	public void setFolio(final String folio) {
		this.folio = folio;
	}

	/**
	 * Obtiene la fecha.
	 * 
	 * @return la fecha
	 */
	public String getFecha() {
		return fecha;
	}

	/**
	 * Establece la fecha.
	 * 
	 * @param fecha
	 *            la fecha a establecer
	 */
	public void setFecha(final String fecha) {
		this.fecha = fecha;
	}

	/**
	 * Obtiene la hora.
	 * 
	 * @return la hora
	 */
	public String getHora() {
		return hora;
	}

	/**
	 * Establece la hora
	 * 
	 * @param hora
	 *            la hora a establecer
	 */
	public void setHora(final String hora) {
		this.hora = hora;
	}

}