/**
 * 
 */
package mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import mtto.com.bancomer.mbanking.R;
import mtto.com.bancomer.mbanking.SuiteAppMtto;
import mtto.suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.NuevaContraseniaDelegate;
import mtto.suitebancomer.aplicaciones.bmovil.classes.model.Desbloqueo;
import mtto.suitebancomer.classes.common.GuiTools;
import mtto.suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

/**
 * @author Francisco.Garcia
 *
 */
public class NuevaContraseniaViewController extends BaseViewController implements OnClickListener {
	
	private NuevaContraseniaDelegate delegate;
	private TextView lblContrasena,lblConfirmacion;
	private EditText contrasena;
	private EditText contrasenaConfirmacion;
	private ImageButton btnActivar;
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER|SHOW_TITLE, SuiteAppMtto.getResourceId("mtto_layout_bmovil_cambiar_password", "layout"));
		SuiteApp.appContext=this;
		SuiteAppMtto.appContext=this;
		setTitle(R.string.bmovil_desbloqueo_title, SuiteAppMtto.getResourceId("bmovil_desbloqueo_icono", "drawable"));
		final SuiteAppMtto suiteApp = (SuiteAppMtto)SuiteAppMtto.getInstance();
		setParentViewsController(suiteApp.getBmovilApplication().getBmovilViewsController());
		setDelegate(parentViewsController.getBaseDelegateForKey(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID));
		delegate = (NuevaContraseniaDelegate)getDelegate(); 
		delegate.setViewController(this);
		findViews();
		scaleForCurrentScreen();
		inicializarPantalla();
		
		contrasena.addTextChangedListener(new BmovilTextWatcher(this));
		contrasenaConfirmacion.addTextChangedListener(new BmovilTextWatcher(this));
	}
	
	/**
	 * referencias de vistas
	 */
	private void findViews(){
		contrasena = (EditText)findViewById(SuiteAppMtto.getResourceId("cambio_contrasena_contrasenaNueva_text", "id"));
		contrasenaConfirmacion = (EditText)findViewById(SuiteAppMtto.getResourceId("cambio_contrasena_contrasenaConfirmacion_text", "id"));
		btnActivar = (ImageButton)findViewById(SuiteAppMtto.getResourceId("cambio_contrasena_boton_confirmar","id"));
		lblContrasena = (TextView) findViewById(SuiteAppMtto.getResourceId("cambio_contrasena_contrasenaNueva_label", "id"));
		lblConfirmacion = (TextView) findViewById(SuiteAppMtto.getResourceId("cambio_contrasena_contrasenaConfirmacion_label", "id"));
	}
	
	/**
	 * escalamiento de pantalla
	 */
	private void scaleForCurrentScreen(){
		final GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());

		gTools.scale(contrasena,true);
		gTools.scale(contrasenaConfirmacion,true);

		gTools.scale(lblContrasena,true);
		gTools.scale(lblConfirmacion,true);

		gTools.scale(btnActivar);	
		gTools.scale(findViewById(SuiteAppMtto.getResourceId("cambiar_password_resultado_contenedor_superior", "id")));
	}
	
	/**
	 * inicializa la pantalla con los elementos necesarios
	 */
	private void inicializarPantalla(){
		findViewById(SuiteAppMtto.getResourceId("cambio_contrasena_contrasenaActual_label", "id")).setVisibility(View.GONE);
		findViewById(SuiteAppMtto.getResourceId("cambio_contrasena_contrasenaActual_text", "id")).setVisibility(View.GONE);
		
		btnActivar.setOnClickListener(this);
		InputFilter[] filters = new InputFilter[1];
		filters[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);

		contrasena.setFilters(filters);
		contrasenaConfirmacion.setFilters(filters);

		contrasena.setLongClickable(false);
		contrasenaConfirmacion.setLongClickable(false);
		final int backgroundBtn = SuiteAppMtto.getResourceId("btn_continuar", "drawable");
		btnActivar.setBackgroundResource(backgroundBtn);
		statusdesactivado = false;
		
		lblContrasena.setText(R.string.bmovil_desbloqueo_contrasena_label);
		lblConfirmacion.setText(R.string.bmovil_desbloqueo_confirmacion_label);
		btnActivar.setBackgroundResource(SuiteAppMtto.getResourceId("bmovil_btn_activar", "drawable"));
		contrasena.setHint(R.string.bmovil_common_hint_confirmar_contrasena);
		contrasenaConfirmacion.setHint(R.string.bmovil_common_hint_confirmar_contrasena);
	}

	@Override
	protected void onResume() {
		super.onResume();
		SuiteApp.appContext=this;
		if(statusdesactivado)
			if (parentViewsController.consumeAccionesDeReinicio()) {
					return;
				}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		final boolean accionesP = parentViewsController.consumeAccionesDePausa();
		statusdesactivado = accionesP;
	}

	@Override
	public void goBack() {
		parentViewsController.removeDelegateFromHashMap(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID);
		super.goBack();
	}
	
	@Override
	public void onClick(final View v) {
		if(v.equals(btnActivar)){
			if(validaDatos()){
				final Perfil perfilCliente = delegate.getConsultaEstatus().getPerfil();
				final String contrasenaNueva = contrasena.getText().toString();
				final String numCelular = delegate.getConsultaEstatus().getNumCelular();
				
				final Desbloqueo desbloqueo = delegate.getDesbloqueo();
				desbloqueo.setContrasenaNueva(contrasenaNueva);
				desbloqueo.setPerfilCliente(perfilCliente);
				desbloqueo.setNumCelular(numCelular);
				
				showConfirmacion();
			}
		}		
	}
	
	/**
	 * validacion de los datos ingresados por el usuario
	 * @return true si los datos son validos.
	 */
	private boolean validaDatos(){
		final String pass = contrasena.getText().toString();
		final String cPass = contrasenaConfirmacion.getText().toString();
		return delegate.validaDatos(pass,cPass);
	}
	
	/**
	 * muestra la pantalla de confirmacion
	 */
	private void showConfirmacion(){
		delegate.showConfirmacion();		
	}
		
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		delegate.analyzeResponse(operationId, response);
	}

}
