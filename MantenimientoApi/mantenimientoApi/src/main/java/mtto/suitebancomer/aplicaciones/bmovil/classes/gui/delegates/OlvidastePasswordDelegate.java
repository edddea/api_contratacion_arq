package mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.text.TextUtils;

import com.bancomer.base.SuiteApp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants;
import mtto.com.bancomer.mbanking.R;
import mtto.com.bancomer.mbanking.SuiteAppMtto;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.OlvidastePasswordViewController;
import mtto.suitebancomer.aplicaciones.bmovil.classes.model.DefinirContrasenia;
import mtto.suitebancomer.aplicaciones.bmovil.classes.model.DefinirContraseniaResult;
import mtto.suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.CatalogoAutenticacionFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatusMantenimientoData;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

import static suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.validatePasswordPolicy1;
import static suitebancomercoms.aplicaciones.bmovil.classes.common.Tools.validatePasswordPolicy2;

/**
 * Created by evaltierrah on 11/11/15.
 */
public class OlvidastePasswordDelegate extends DelegateBaseAutenticacion {

    /**
     * Id del delegate
     */
    public static final long OLVIDASTE_CONTRASENIA_DELEGATE_ID = 5543309525631958195L;
    /**
     * Instancia del controlador
     */
    OlvidastePasswordViewController viewController;
    /**
     * Instancia del perfil del cliente
     */
    Constants.Perfil perfilCliente;
    /**
     * Instancia del objeto DefinirContrasenia
     */
    DefinirContrasenia definirContrasenia;
    /**
     * Guarda el numero de telefono
     */
    private String numeroCelular;
    /**
     * El folio a mostrar en la pantalla de resultados.
     */
    private String folio;

    /**
     * Metodo que regresa una instancia del objeto DefinirConstrasenia
     *
     * @return instancia del objeto DefinirConstrasenia
     */
    public DefinirContrasenia getDefinirContrasenia() {
        if (definirContrasenia == null) {
            definirContrasenia = new DefinirContrasenia();
        }
        return definirContrasenia;
    }

    /**
     * Metodo que regresa si se debe mostrar o no el campo contraseña
     *
     * @return true si se debe mostrar contrasena, false en caso contrario.
     */
    @Override
    public boolean mostrarContrasenia() {
        return Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.desbloqueo,
                perfilCliente);
    }

    /**
     * Metodo que indica si se muestra o no el CVV
     *
     * @return true si se debe mostrar CVV, false en caso contrario.
     */
    @Override
    public boolean mostrarCVV() {
        return Autenticacion.getInstance().mostrarCVV(Constants.Operacion.desbloqueo, perfilCliente);
    }

    /**
     * Metodo que indica si se muestra el NIP o no
     *
     * @return true si se debe mostrar NIP, false en caso contrario.
     */
    @Override
    public boolean mostrarNIP() {
        return Autenticacion.getInstance().mostrarNIP(Constants.Operacion.desbloqueo,
                perfilCliente);
    }

    /**
     * Metodo que indica que tipo de token se debe mostrar
     *
     * @return El tipo de token a mostrar
     */
    @Override
    public Constants.TipoOtpAutenticacion tokenAMostrar() {
        Constants.TipoOtpAutenticacion tipoOTP;
        try {
            tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.desbloqueo,
                    perfilCliente);
        } catch (Exception ex) {
            tipoOTP = null;
        }
        return tipoOTP;
    }

    /**
     * Metodo que valida las contrasenias
     *
     * @param password         Texto con el nuevo password
     * @param confirmaPassword Texto con la confirmacion del password
     * @return Regresa true si son iguales
     */
    public boolean validaDatos(final String password, final String confirmaPassword) {
        boolean esOk = Boolean.TRUE;
        int msgError = 0;
        if (TextUtils.isEmpty(numeroCelular)) {
            esOk = Boolean.FALSE;
            msgError = R.string.forgot_password_tel_empty;
        } else if (numeroCelular.length() != Constants.TELEPHONE_NUMBER_LENGTH) {
            esOk = Boolean.FALSE;
            msgError = R.string.forgot_password_tel_err_long;
        } else if (password.length() == 0) {
            esOk = Boolean.FALSE;
            msgError = R.string.forgot_password_pass_vacia;
        } else if (password.length() != Constants.PASSWORD_LENGTH) {
            esOk = Boolean.FALSE;
            msgError = R.string.forgot_password_pass_err_long;
        } else if (!validatePasswordPolicy1(password)) {
            esOk = Boolean.FALSE;
            msgError = R.string.forgot_password_pass_error_policy1;
        } else if (!validatePasswordPolicy2(password)) {
            esOk = Boolean.FALSE;
            msgError = R.string.forgot_password_pass_error_policy2;
        } else if (confirmaPassword.length() == 0) {
            esOk = Boolean.FALSE;
            msgError = R.string.forgot_password_pass_conf_vacia;
        } else if (confirmaPassword.length() != Constants.PASSWORD_LENGTH) {
            esOk = Boolean.FALSE;
            msgError = R.string.forgot_password_pass_conf_err_long;
        } else if (!password.equals(confirmaPassword)) {
            esOk = Boolean.FALSE;
            msgError = R.string.forgot_password_error_diferentes;
        }
        if (!esOk) {
            viewController.showInformationAlert(msgError);
        }
        return esOk;
    }

    @Override
    public ArrayList<Object> getDatosTablaConfirmacion() {
        final ArrayList<Object> tabla = new ArrayList<Object>();
        List<String> fila;

        fila = new ArrayList<String>();
        fila.add(viewController.getString(R.string.forgot_password_operacion));
        fila.add(viewController.getString(R.string.forgot_password_valor));
        tabla.add(fila);

        return tabla;
    }

    /*
     * (non-Javadoc)
	 *
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #getDatosTablaResultados()
	 */
    @Override
    public ArrayList<Object> getDatosTablaResultados() {
        final ArrayList<Object> datosResultados = new ArrayList<Object>();
        final ArrayList<String> registro = new ArrayList<String>();
        registro.add(viewController.getString(R.string.forgot_password_folio));
        registro.add(folio);
        datosResultados.add(registro);
        return datosResultados;
    }

    /*
     * (non-Javadoc)
	 *
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #getTextoTituloResultado()
	 */
    @Override
    public String getTextoTituloResultado() {
        //return baseViewController.getString(R.string.cambioPerfil_tituloExito);
        return SuiteAppMtto.appContext.getString(R.string.forgot_password_exito);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getColorTituloResultado()
     */
    @Override
    public int getColorTituloResultado() {
        return R.color.verde_limon;
    }

    /*
	 * (non-Javadoc)
	 *
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #getTextoPantallaResultados()
	 */
    @Override
    public String getTextoPantallaResultados() {
        return "";
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
     * #getTextoEspecialResultados()
     */
    @Override
    public String getTextoEspecialResultados() {
        return SuiteAppMtto.appContext.getString(R.string.forgot_password_texto_especial);
    }

    /*
	 * (non-Javadoc)
	 *
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion
	 * #getOpcionesMenuResultados()
	 */
    @Override
    public int getOpcionesMenuResultados() {
		/*return SHOW_MENU_SMS | SHOW_MENU_EMAIL | SHOW_MENU_PDF
				| SHOW_MENU_FRECUENTE | SHOW_MENU_RAPIDA | SHOW_MENU_BORRAR;*/
        return 0;
    }

    /**
     * Muestra la pantalla de confirmacion.
     */
    public void showConfirmacion() {
        final int resSubtitle = 0;
        final int resTitleColor = 0;
        final int resIcon = 0;
        final int resTitle = 0;
        SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController().showConfirmacionAutenticacionViewController(this, resIcon, resTitle, resSubtitle, resTitleColor);
    }

    /**
     * Muestra la pantalla de resultados autenticacion.
     */
    private void mostrarResultados() {
        Session.getInstance(SuiteApp.appContext).setUsername(definirContrasenia.getNumCelular());
        if (!Session.getInstance(SuiteApp.appContext).isApplicationActivated()) {
            DatosBmovilFileManager.getCurrent().setLogin(definirContrasenia.getNumCelular());
        }
        SuiteAppMtto.getInstance().getBmovilApplication()
                .getBmovilViewsController()
                .showResultadosAutenticacionViewController(this, -1, -1);
    }

    /**
     * Metodo que ejecuta la consulta estatus mantenimiento
     */
    public void consultaMantenimiento() {
        final Session session = Session.getInstance(viewController);
        final String autVersion = CatalogoAutenticacionFileManager.getCurrent().leerVersionArchivoCatalogoAutenticacion();
        final String compVersion = session.getVersionCatalogoTelefonicas();
        final Hashtable<String, String> params = new Hashtable<String, String>();
        params.put(ServerConstants.NUMERO_TELEFONO,
                definirContrasenia.getNumCelular());
        params.put(ServerConstants.VERSION_MIDLET, ServerConstants.APP_VERSION_CONSULTA);
        params.put("verCatAutenticacion", Tools.isEmptyOrNull(autVersion) ? "0"
                : autVersion);
        params.put("verCatTelefonicas",
                Tools.isEmptyOrNull(compVersion) ? "0" : compVersion);
        doNetworkOperation(Server.CONSULTA_MANTENIMIENTO, params, Boolean.TRUE, new ConsultaEstatusMantenimientoData(),
                viewController);
    }

    @Override
    public void realizaOperacion(final ConfirmacionAutenticacionViewController confirmacionAut,
                                 final String contrasenia, final String nip, final String token, final String cvv, final String campoTarjeta) {
        final int operationId = ApiConstants.DESBLOQUEO;
        final Autenticacion aut = Autenticacion.getInstance();
        final String cadAutenticacion = aut.getCadenaAutenticacion(Constants.Operacion.desbloqueo, perfilCliente);
        final Hashtable<String, String> params = new Hashtable<String, String>();
        params.put("cveAccesoNueva", definirContrasenia.getContrasenaNueva());
        params.put("companiaCelular", definirContrasenia.getCompaniaCelular());
        params.put("codigoNIP", nip == null ? suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_STRING : nip);
        params.put("codigoCVV2", cvv == null ? suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_STRING : cvv);
        params.put("codigoOTP", token == null ? suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_STRING : token);
        params.put("cadenaAutenticacion", cadAutenticacion);
        params.put(ServerConstants.NUMERO_TELEFONO, definirContrasenia.getNumCelular());
        params.put("numeroCliente", suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_STRING);
        params.put("tarjeta5Dig", campoTarjeta == null ? "" : campoTarjeta);
        params.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
        final List<String> listaEncriptar = Arrays.asList("cveAccesoNueva", "codigoNIP", "codigoCVV2");
        Encripcion.setContext(SuiteAppMtto.appContext);
        Encripcion.encriptaCadenaAutenticacion(params, listaEncriptar);
        //JAIG
        doNetworkOperation(operationId, params, true, new DefinirContraseniaResult(), confirmacionAut);
    }

    @Override
    public void doNetworkOperation(final int operationId, final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
        ((BmovilViewsController) viewController.getParentViewsController()).getBmovilApp().invokeNetworkOperation(operationId, params, isJson, handler, caller);
    }

    @Override
    public void analyzeResponse(final int operationId, final ServerResponse response) {
        final BaseViewControllerCommons current = SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp();
        if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
            switch (operationId) {
                case Server.CONSULTA_MANTENIMIENTO:
                    final ConsultaEstatusMantenimientoData data = (ConsultaEstatusMantenimientoData) response.getResponse();
                    if (data.getEstatusServicio().equals(Constants.STATUS_USER_CANCELED)
                            || data.getEstatusServicio().equals(Constants.STATUS_BANK_CANCELED)
                            || data.getEstatusServicio().equals(Constants.STATUS_CLIENT_NOT_FOUND)
                            || data.getEstatusServicio().equals(Constants.STATUS_NIP_BLOCKED)) {
                        current.showInformationAlert(R.string.forgot_password_error_estatus);
                    } else {
                        definirContrasenia.setEstatus(data.getEstatusServicio());
                        definirContrasenia.setInstrumento(data.getTipoInstrumento());
                        definirContrasenia.setEstatusInstrumento(data.getEstatusInstrumento());
                        definirContrasenia.setCompaniaCelular(data.getCompaniaCelular());
                        definirContrasenia.setNumCliente(data.getNumeroCliente());
                        definirContrasenia.setPerfilCliente(data.getPerfilCliente());
                        Session.getInstance(SuiteAppMtto.appContext).setSecurityInstrument(data.getTipoInstrumento());
                        perfilCliente = determinaPerfil(definirContrasenia.getPerfilCliente());
                        showConfirmacion();
                    }
                    break;
                case ApiConstants.DESBLOQUEO:
                    if (response.getResponse() instanceof DefinirContraseniaResult) {
                        folio = ((DefinirContraseniaResult) response.getResponse()).getFolio();
                        // Se muestra la pantalla de resultados
                        mostrarResultados();
                    }
                    break;
                default:
                    break;
            }
        } else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
            current.showInformationAlert(response.getMessageText());
        }

    }

    /**
     * Metodo que determina el tipo de perfil del cliente
     *
     * @param perfilCliente Perfil del cliente
     * @return Regresa el tipo de perfil del cliente
     */
    private Constants.Perfil determinaPerfil(final String perfilCliente) {
            Constants.Perfil perfil = Constants.Perfil.basico;
        if (Constants.PROFILE_ADVANCED_03.equalsIgnoreCase(perfilCliente)) {
            perfil = Constants.Perfil.avanzado;
        } else if (Constants.PROFILE_RECORTADO_02.equalsIgnoreCase(perfilCliente)) {
            perfil = Constants.Perfil.recortado;
        } else if (Constants.PROFILE_BASIC_00.equalsIgnoreCase(perfilCliente)
                || Constants.PROFILE_BASIC_01.equalsIgnoreCase(perfilCliente)) {
            perfil = Constants.Perfil.basico;
        }
        return perfil;
    }

    /**
     * Metodo que regresa a la pantalla principal
     */
    private void showPantallaPrincipal() {
        SuiteAppMtto.getInstance().getBmovilApplication().reiniciaAplicacion();
        SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(this.viewController);
        SuiteAppMtto.getCallback().returnToActivity(SuiteAppMtto.appContext, SuiteAppMtto.getInstance().getActivityToReturn(), Boolean.TRUE);
    }

    @Override
    public boolean mostrarCampoTarjeta() {
        return (mostrarCVV() || mostrarNIP());
    }

    @Override
    public int getTextoEncabezado() {
        return R.string.forgot_password_title;
    }

    @Override
    public int getNombreImagenEncabezado() {
        return R.drawable.bmovil_cambiar_password_icono;
    }

    /**
     * Metodo que regresa la instancia del controlador
     *
     * @return Instancia del controlador
     */
    public OlvidastePasswordViewController getViewController() {
        return viewController;
    }

    /**
     * Metodo que setea el controlador
     *
     * @param viewController Instancia del controlador
     */
    public void setViewController(final OlvidastePasswordViewController viewController) {
        this.viewController = viewController;
    }

    @Override
    protected void accionBotonResultados() {
        showPantallaPrincipal();
    }

    @Override
    protected int getImagenBotonResultados() {
        return R.drawable.btn_continuar;
    }

    /**
     * Metodo que regresa el numero celular
     *
     * @return Numero celular
     */
    public String getNumeroCelular() {
        return numeroCelular;
    }

    /**
     * Metodo que asigna el numero celular
     *
     * @param numeroCelular Numero celular
     */
    public void setNumeroCelular(final String numeroCelular) {
        this.numeroCelular = numeroCelular;
    }

}
