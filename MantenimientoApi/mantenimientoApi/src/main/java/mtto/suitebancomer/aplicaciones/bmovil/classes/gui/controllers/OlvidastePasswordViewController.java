package mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import android.os.Bundle;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bancomer.base.SuiteApp;

import bancomer.api.common.commons.Constants;
import mtto.com.bancomer.mbanking.R;
import mtto.com.bancomer.mbanking.SuiteAppMtto;
import mtto.suitebancomer.aplicaciones.bmovil.classes.common.BmovilTextWatcher;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.OlvidastePasswordDelegate;
import mtto.suitebancomer.aplicaciones.bmovil.classes.model.DefinirContrasenia;
import mtto.suitebancomer.classes.common.GuiTools;
import mtto.suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

public class OlvidastePasswordViewController extends BaseViewController implements View.OnClickListener {

    /**
     * Instancia de la etiqueta Numero Celular
     */
    TextView lblNumeroCelular;
    /**
     * Instancia del texto para capturar el numero de celular
     */
    EditText txtNumeroCelular;
    /**
     * Instancia de la etiqueta Nueva Constrasenia
     */
    TextView lblNuevaContrasena;
    /**
     * Instancia del texto para capturar la nueva password
     */
    EditText txtNuevaContrasena;
    /**
     * Instancia de la etiqueta Confirma Numero Celular
     */
    TextView lblConfirmaContrasena;
    /**
     * Instancia del texto para capturar la confirmacion del password
     */
    EditText txtConfirmaContrasena;
    /**
     * Instancia del boton de confirmar
     */
    ImageButton btnConfirmar;
    /**
     * Instancia del delegate
     */
    OlvidastePasswordDelegate delegate;
    /**
     * Guarda el numero de telefono
     */
    String numeroTelefono;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, R.layout.mtto_layout_olvidaste_password);
        SuiteApp.appContext = this;
        SuiteAppMtto.appContext = this;
        final Bundle bundle = getIntent().getExtras();
        numeroTelefono = bundle.getString(suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.PAR_NUMERO_TELEFONO);
        setParentViewsController(SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController());
        setDelegate(parentViewsController.getBaseDelegateForKey(OlvidastePasswordDelegate.OLVIDASTE_CONTRASENIA_DELEGATE_ID));
        delegate = (OlvidastePasswordDelegate) getDelegate();
        delegate.setViewController(this);
        delegate.setNumeroCelular(numeroTelefono);
        setTitle(R.string.forgot_password_title, R.drawable.bmovil_cambiar_password_icono);
        findViews();
        scaleForCurrentScreen();
        inicializarPantalla();

        txtNuevaContrasena.addTextChangedListener(new BmovilTextWatcher(this));
        txtConfirmaContrasena.addTextChangedListener(new BmovilTextWatcher(this));
    }

    /**
     * referencias de vistas
     */
    private void findViews() {
        txtNumeroCelular = (EditText) findViewById(R.id.txtNumeroCelular);
        txtNuevaContrasena = (EditText) findViewById(R.id.txtNuevaContrasena);
        txtConfirmaContrasena = (EditText) findViewById(R.id.txtConfirmaContrasena);
        btnConfirmar = (ImageButton) findViewById(R.id.btnConfirmar);
        lblNuevaContrasena = (TextView) findViewById(R.id.lblNuevaContrasena);
        lblConfirmaContrasena = (TextView) findViewById(R.id.lblConfirmaContrasena);
        lblNumeroCelular = (TextView) findViewById(R.id.lblNumeroCelular);
    }

    /**
     * escalamiento de pantalla
     */
    private void scaleForCurrentScreen() {
        final GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(txtNumeroCelular, Boolean.TRUE);
        gTools.scale(txtNuevaContrasena, Boolean.TRUE);
        gTools.scale(txtConfirmaContrasena, Boolean.TRUE);
        gTools.scale(lblNuevaContrasena, Boolean.TRUE);
        gTools.scale(lblConfirmaContrasena, Boolean.TRUE);
        gTools.scale(lblNumeroCelular, Boolean.TRUE);
        gTools.scale(btnConfirmar);
        gTools.scale(findViewById(R.id.olvidaste_password_contenedor_superior));
    }

    /**
     * inicializa la pantalla con los elementos necesarios
     */
    private void inicializarPantalla() {
        btnConfirmar.setOnClickListener(this);
        final InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
        txtNuevaContrasena.setFilters(filters);
        txtConfirmaContrasena.setFilters(filters);
        txtNuevaContrasena.setLongClickable(false);
        txtConfirmaContrasena.setLongClickable(false);
        final InputFilter[] filtersPhone = new InputFilter[1];
        filtersPhone[0] = new InputFilter.LengthFilter(Constants.TELEPHONE_NUMBER_LENGTH);
        txtNumeroCelular.setFilters(filtersPhone);
        txtNumeroCelular.setLongClickable(false);
        txtNumeroCelular.setText(numeroTelefono);
        if (!TextUtils.isEmpty(numeroTelefono) &&
                Session.getInstance(SuiteApp.appContext).isApplicationActivated()) {
            txtNumeroCelular.setVisibility(View.GONE);
            lblNumeroCelular.setVisibility(View.GONE);
        }

    }

    @Override
    public void goBack() {
        parentViewsController.removeDelegateFromHashMap(OlvidastePasswordDelegate.OLVIDASTE_CONTRASENIA_DELEGATE_ID);
        super.goBack();
    }

    /**
     * validacion de los datos ingresados por el usuario
     *
     * @return true si los datos son validos.
     */
    private boolean validaDatos() {
        final String pass = txtNuevaContrasena.getText().toString();
        final String cPass = txtConfirmaContrasena.getText().toString();
        final DefinirContrasenia definirContrasenia = delegate.getDefinirContrasenia();
        if (!TextUtils.isEmpty(txtNumeroCelular.getText().toString())) {
            numeroTelefono = txtNumeroCelular.getText().toString();
        }
        if (TextUtils.isEmpty(numeroTelefono)) {
            numeroTelefono = txtNumeroCelular.getText().toString();
        }
        delegate.setNumeroCelular(numeroTelefono);
        definirContrasenia.setNumCelular(numeroTelefono);
        definirContrasenia.setContrasenaNueva(pass);
        return delegate.validaDatos(pass, cPass);
    }

    /**
     * Metodo que ejecuta la consultaMantenimiento
     */
    private void consultaMantenimiento() {
        delegate.consultaMantenimiento();
    }

    @Override
    public void onClick(final View view) {
        if (view.equals(btnConfirmar)) {
            if (validaDatos()) {
                consultaMantenimiento();
            }
        }
    }

    /**
     * Metodo que realiza las peticiones a server
     *
     * @param operationId Indica el id de la operacion
     * @param response    Respuesta del servidor
     */
    public void processNetworkResponse(final int operationId, final ServerResponse response) {
        delegate.analyzeResponse(operationId, response);
    }

    @Override
    protected void onResume() {
        super.onResume();
        txtNuevaContrasena.setText(suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_STRING);
        txtConfirmaContrasena.setText(suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_STRING);
        SuiteApp.appContext = this;
        if (statusdesactivado) {
            if (parentViewsController.consumeAccionesDeReinicio()) {
                return;
            }
        }
        getParentViewsController().setCurrentActivityApp(this);
    }

}
