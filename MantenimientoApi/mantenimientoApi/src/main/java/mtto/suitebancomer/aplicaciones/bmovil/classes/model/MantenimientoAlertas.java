package mtto.suitebancomer.aplicaciones.bmovil.classes.model;

import bancomer.api.common.commons.Constants;

/**
 * Modelo de datos para Mantenimiento Alertas.
 * 
 * @author CGI
 */
public class MantenimientoAlertas {

	/** Perfil del usuario. */
	private Constants.Perfil perfil;

	/** Numero de movil del usuario. */
	private String numCelular;

	/** Compannia de movil. */
	private String companiaCelular;

	/** El tipo de operacion a ejecutar. */
	private String operacionEjecutar;

	/** El numero de tarjeta. */
	private String numeroTarjeta;

	/**
	 * Constructor por defecto.
	 */
	public MantenimientoAlertas() {
	}

	/**
	 * Constructor con parametros.
	 * 
	 * @param perfil
	 *            el perfil
	 * @param numCelular
	 *            el numero de telefono
	 * @param companiaCelular
	 *            la compannia telefonica
	 * @param operacion
	 *            el tipo de operacion
	 */
	public MantenimientoAlertas(final Constants.Perfil perfil, final String numCelular,
			final String companiaCelular, final String operacion, final String numeroTarjeta) {
		this.perfil = perfil;
		this.numCelular = numCelular;
		this.companiaCelular = companiaCelular;
		this.operacionEjecutar = operacion;
		this.numeroTarjeta = numeroTarjeta;
	}
	
	/**
	 * Obtiene el perfil del usuario MF01 o MF03.
	 * 
	 * @return El perfil de usuario
	 */
	public String getPerfilString() {
		if (Constants.Perfil.avanzado.equals(getPerfil())){
			return Constants.PROFILE_ADVANCED_03;
		}
		return Constants.PROFILE_BASIC_01;
	}

	/**
	 * Obtiene el perfil del usuario.
	 * 
	 * @return El perfil de usuario
	 */
	public Constants.Perfil getPerfil() {
		return perfil;
	}

	/**
	 * Establece el perfil del usuario.
	 * 
	 * @param perfil
	 *            el perfil del usuario a establecer
	 */
	public void setPerfil(final Constants.Perfil perfil) {
		this.perfil = perfil;
	}

	/**
	 * Obtiene el numero de telefono del usuario.
	 * 
	 * @return el numero de telefono del usuario
	 */
	public String getNumCelular() {
		return numCelular;
	}

	/**
	 * Establece el numero de telefono del usuario
	 * 
	 * @param numCelular
	 *            el numero de telefono del usuario a establecer
	 */
	public void setNumCelular(final String numCelular) {
		this.numCelular = numCelular;
	}

	/**
	 * Obtiene la compannia de movil del usuario.
	 * 
	 * @return la compannia de movil del usuario
	 */
	public String getCompaniaCelular() {
		return companiaCelular;
	}

	/**
	 * Establece la compannia de movil del usuario
	 * 
	 * @param companiaCelular
	 *            la compannia de movil del usuario a establecer
	 */
	public void setCompaniaCelular(final String companiaCelular) {
		this.companiaCelular = companiaCelular;
	}

	/**
	 * Obtiene la operacion a ejecutar.
	 * 
	 * @return la operacion a ejecutar
	 */
	public String getOperacionEjecutar() {
		return operacionEjecutar;
	}

	/**
	 * Establece la operacion a ejecutar
	 * 
	 * @param operacionEjecutar
	 *            la operacion a ejecutar
	 */
	public void setOperacionEjecutar(final String operacionEjecutar) {
		this.operacionEjecutar = operacionEjecutar;
	}

	/**
	 * Obtiene el numero de la tarjeta.
	 * 
	 * @return el numero de la tarjeta
	 */
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}

	/**
	 * Establece el numero de la tarjeta.
	 * 
	 * @param numeroTarjeta
	 *            el numero de la tarjeta a establecer
	 */
	public void setNumeroTarjeta(final String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}
}
