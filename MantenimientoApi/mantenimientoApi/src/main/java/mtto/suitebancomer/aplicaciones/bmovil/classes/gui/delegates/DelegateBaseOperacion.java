package mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import java.util.ArrayList;
import java.util.Hashtable;
import mtto.com.bancomer.mbanking.SuiteAppMtto;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.model.SolicitarAlertasData;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import mtto.suitebancomer.classes.gui.controllers.BaseViewController;
import mtto.suitebancomer.classes.gui.delegates.BaseDelegate;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import mtto.com.bancomer.mbanking.R;

public class DelegateBaseOperacion extends BaseDelegate {
	
	/**
     * Parametro para mostrar la opcion de menu SMS
     */
    public final static int SHOW_MENU_SMS = 2;
    
    /**
     * Parametro para mostrar la opcion de menu Correo electr��nico
     */
    public final static int SHOW_MENU_EMAIL = 4;
    
    /**
     * Parametro para mostrar la opcion de menu PDF
     */
    public final static int SHOW_MENU_PDF = 8;
    
    /**
     * Parametro para mostrar la opcion de menu Frecuente
     */
    public final static int SHOW_MENU_FRECUENTE = 16;
    
    /**
     * Parametro para mostrar la opcion de menu Rapida
     */
    public final static int SHOW_MENU_RAPIDA = 32;
    
    /**
     * Parametro para mostrar la opcion de menu Rapida
     */
    public final static int SHOW_MENU_BORRAR = 64;
    
    /**
     * Respuesta del servidor para mostrar errores de transferencias
     */
    protected ServerResponse transferErrorResponse = null;
    protected SolicitarAlertasData sa;
    
    
	
	public ServerResponse getTransferErrorResponse() {
		return transferErrorResponse;
	}

	public void setTransferErrorResponse(final ServerResponse transferErrorResponse) {
		this.transferErrorResponse = transferErrorResponse;
	}
	
	

	public SolicitarAlertasData getSa() {
		return sa;
	}

	public void setSa(final SolicitarAlertasData sa) {
		this.sa = sa;
	}

	public String getTextoAyudaInstrumentoSeguridad(final Constants.TipoInstrumento tipoInstrumento) { return ""; }
	
	public String getEtiquetaCampoNip() { return ""; }
	
	public String getEtiquetaCampoContrasenia() { return ""; }
	
	public String getEtiquetaCampoOCRA() { return ""; }
	
	public String getEtiquetaCampoDP270() { return ""; }
	
	public String getEtiquetaCampoSoftokenActivado() { return ""; }
	
	public String getEtiquetaCampoSoftokenDesactivado() { return ""; }
	
	public String getEtiquetaCampoCVV() { return ""; }
	
	public int getOpcionesMenuResultados() { return 0; }
	
	public String getTextoSMS() { return ""; }
	
	public String getTextoEmail() { return ""; }
	
	public String getTextoPDF() { return ""; }
	
	public String getTituloTextoEspecialResultados() { return ""; }

	public String getTextoEspecialResultados() { return ""; }
	
	public String getTextoPantallaResultados() { return ""; }
	
	public boolean borraRapido() { return false; }
	
	public String getTextoTituloResultado() { return ""; }
	
	public int getColorTituloResultado() { return android.R.color.black; }
	
	public String getTextoTituloConfirmacion() { return ""; }
	
	public ArrayList<Object> getDatosTablaConfirmacion() { return null; }
	
	public ArrayList<Object> getDatosTablaResultados() { return null; }
	
	public String getTextoAyudaNIP() { return ""; }
	
	public String getTextoAyudaCVV() { return ""; }
	
	/**
	 * Consult if the password field should be visible.
	 * @param tipoOperacion The operation type.
	 * @param perfil The user profile.
	 * @return A Boolean with the flag, or null if an error occurred.
	 * @throws Exception
	 */
	public boolean mostrarContrasenia() { return false; }
	
	/**
	 * Consult the type of token that should be shown.
	 * @param tipoOperacion The operation type.
	 * @param perfil The user profile.
	 * @return The token type.
	 * @throws Exception
	 */
	public TipoOtpAutenticacion tokenAMostrar() { return null; }
	
	/**
	 * Consult if the NIP field should be visible.
	 * @param tipoOperacion The operation type.
	 * @param perfil The user profile.
	 * @return A Boolean with the flag, or null if an error occurred.
	 * @throws Exception
	 */
	public boolean mostrarNIP() { return false; }
	
	public boolean mostrarCVV() { return false; }
	
	public ArrayList<Object> getDatosTablaAltaFrecuentes() { return null; }
	
	public ArrayList<String> getDatosTablaAltaRapidos() { return null; }
	
	public int getNombreImagenEncabezado() { return -1; }
	
	public int getTextoEncabezado() { return -1; }
	
	public String getTextoBotonOperacionNueva() { return ""; }
	
	public String getTextoAyudaResultados(){ return ""; }
	
	public void consultarFrecuentes(final String tipoFrecuente) {
        // Empty method
    }
	
	public ArrayList<Object> getDatosHeaderTablaFrecuentes() { return null; }
	
	public ArrayList<Object> getDatosTablaFrecuentes() { return null; }

	public Hashtable<String, String> getParametrosAltaFrecuentes(){return null;}
	
	public void operarFrecuente(final int indexSelected){
        // Empty method
    }
	
	public void eliminarFrecuente(final int indexSelected){
        // Empty method
    }
	/**
	 * metodo para realizar la Acci���n del bot��n pantalla de resultados autenticaci��n
	 * 
	 */
	protected void accionBotonResultados(){
        // Empty method
    }

	protected int getImagenBotonResultados(){return 0;}
	
	/**
	 * Lista de datos para mostrar en la pantalla de RegistarOperacion
	 * @return ArrayList con los datos a mostrar en la pantalla de RegistarOperacion
	 */
	protected ArrayList<Object> getDatosRegistroOp() {return null;}
	
	/**
	 * @return  ArrayList con los datos a mostrar en la tabla de RegistroOperacionExitoso
	 */
	public ArrayList<Object> getDatosRegistroExitoso() {return null;}
	
	/**
	 * Obtiene los ultimos 5 digitos de la cuenta para el registro de operaci��n.
	 */
	public String getNumeroCuentaParaRegistroOperacion() { return "00000"; }
	
	/**
	 * Carga una OTP del api de Softtoken.
	 * @param tipoOTP El tipo de OTP a cargar (C��digo o Registro).
	 * @return La OTP generada por Softtoken, null en caso llamar este m��todo sin tener Softtoken activado o de alg���n error.
	 */
	public String loadOtpFromSofttoken(final Constants.TipoOtpAutenticacion tipoOTP) {
		return loadOtpFromSofttoken(tipoOTP, null);
	}
	
	/**
	 * Carga una OTP del api de Softtoken.
	 * @param tipoOTP El tipo de OTP a cargar (C��digo o Registro).
	 * @param opDelegate El delegado de la operaci��n actual, es requerido para obtener los 5 digitos necesarios para una OTP de tipo Registro. 
	 * En caso de estar seguro de que el tipo de OTP es C��digo este par���metro puede ser nulo. 
	 * @return La OTP generada por Softtoken, null en caso llamar este m��todo sin tener Softtoken activado o de alg���n error.
	 */
	public String loadOtpFromSofttoken(final Constants.TipoOtpAutenticacion tipoOTP, final DelegateBaseOperacion opDelegate) {
		String otp = null;
		
		// Si se llama a este m��todo sin que Softtoken est�� activado se regresa null.
		if(!SuiteAppMtto.getSofttokenStatus())
			return null;
		
		// Verifica y carga el token seg���n el tipo que corresponda.
		if(TipoOtpAutenticacion.codigo == tipoOTP)
			otp = GeneraOTPSTDelegate.generarOtpTiempo();
		else if(TipoOtpAutenticacion.registro == tipoOTP)
			otp = (null == opDelegate) ? null : GeneraOTPSTDelegate.generarOtpChallenge(opDelegate.getNumeroCuentaParaRegistroOperacion());
		
		return otp;
	}
	
	public String getEtiquetaCampoTarjeta(){
		return SuiteAppMtto.appContext.getString(R.string.confirmation_componente_digitos_tarjeta);
	}
	
	public String getTextoAyudaTarjeta() {
		return SuiteAppMtto.appContext.getString(R.string.confirmation_ayuda_componente_digitos_tarjeta);
	}
	
	public boolean mostrarCampoTarjeta(){
		return false;
	}
	
	public void solicitarAlertas (final BaseViewController caller){
		final Session session = Session.getInstance(SuiteAppMtto.appContext);
		final Account cEje = Tools.obtenerCuentaEje();
		
		final Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		paramTable.put(ServerConstants.NUMERO_TARJETA, cEje.getNumber());
		paramTable.put(ServerConstants.COMPANIA_CELULAR,session.getCompaniaUsuario());
	
		//JAIG
//		doNetworkOperation(Server.OP_SOLICITAR_ALERTAS, paramTable, true, new SolicitarAlertasData(), caller);
	}
	public void realizaCambioPerfilRecortadoABasico(){
        // Empty method
	}
	
	public void realizaCambioNumeroCompania(final String nuevoNum, final String nuevaCompania){
        // Empty method
	}
	
	public void analyzeAlertasRecortadoSinError(){
		
		final String validacionalertas = sa.getValidacionAlertas();

		if (Constants.ALERT01.equals(validacionalertas)) {
			// tiene que ir a Cambio de Perfil EA#8
			SuiteAppMtto.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
			SuiteAppMtto.getInstance().getSuiteViewsController().getCurrentViewController()
					.showYesNoAlert(R.string.transferir_mensaje_recortado_conalertsiguales,
							new OnClickListener() {
								@Override
								public void onClick(
										final DialogInterface dialog,
										final int which) {
									dialog.dismiss();
									realizaCambioPerfilRecortadoABasico();

//									if(Server.ALLOW_LOG) Log.e("Usuario RECORTADO Alertas coinciden",
//										"Acepto realizar el cambio de perfil");

								}
							}, new OnClickListener() {
								@Override
								public void onClick(
										final DialogInterface dialog,
										final int which) {
									
//									if(Server.ALLOW_LOG) Log.e("Usuario RECORTADO Alertas coinciden",
//											"No Acepto realizar el cambio de perfil");
									
									new AlertDialog.Builder(SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp()).setMessage(R.string.transferir_mensaje_recortado_conalertsiguales_cancela_cambio_perfil) 
									.setTitle(R.string.label_error)
									.setIcon(R.drawable.anicalertaaviso)
									.setCancelable(true) 
									.setNeutralButton(android.R.string.ok, 
									new DialogInterface.OnClickListener() { 
										public void onClick(final DialogInterface dialog, final int whichButton){
                                            // Empty method
									} 
									}) 
									.show();
								}
							});

		} else if (Constants.ALERT03.equals(validacionalertas)
				|| Constants.ALERT04.equals(validacionalertas)) {
			
			SuiteAppMtto.getInstance().getSuiteViewsController().getCurrentViewController().showYesNoAlert(SuiteAppMtto.getInstance().getString(R.string.transferir_mensaje_recortado_conalertsdistintas), new OnClickListener() {
								@Override
								public void onClick(
										final DialogInterface dialog,
										final int which) {

									if (DialogInterface.BUTTON_POSITIVE == which) {

										realizaCambioNumeroCompania(
												sa.getNumeroAlertas(),
												sa.getCompaniaAlertas());
									}
								}
							});
		}
		
	}
	
	public void analyzeAlertasRecortado(){
		final String codigoError=getTransferErrorResponse().getMessageCode();
		final String descripcionError=getTransferErrorResponse().getMessageText();
		setTransferErrorResponse(null);
						
		final String validacionalertas =sa.getValidacionAlertas();
		if(Constants.ALERT01.equals(validacionalertas)){
			
			SuiteAppMtto.getInstance().getSuiteViewsController().getCurrentViewController().showYesNoAlertEspecialTitulo(codigoError,descripcionError, new OnClickListener() {
				@Override
				public void onClick(final DialogInterface dialog, final int which) {
					
					dialog.dismiss();
					realizaCambioPerfilRecortadoABasico();
//					if(Server.ALLOW_LOG) Log.e("Usuario RECORTADO Alertas coinciden","Acepto realizar el cambio de perfil");
					
				}
				
			}, new OnClickListener() {
				@Override
				public void onClick(final DialogInterface dialog, final int which) {
					
//					if(Server.ALLOW_LOG) Log.e("Usuario RECORTADO Alertas coinciden","No Acepto realizar el cambio de perfil");
				
					new AlertDialog.Builder(SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp()).setMessage(R.string.transferir_mensaje_recortado_conalertsiguales_cancela_cambio_perfil) 
					.setTitle(R.string.label_error)
					.setIcon(R.drawable.anicalertaaviso)
					.setNeutralButton(R.string.common_alert_yesno_positive_button, 
								new DialogInterface.OnClickListener() { 
										public void onClick(final DialogInterface dialog, final int whichButton){
                                            // Empty method
                                        }
					}).show(); 
				
				}
			});
		}else if (Constants.ALERT02.equals(validacionalertas)){
			// no tiene alertas
			
			SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewController().ocultaIndicadorActividad();
			SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewController().showInformationAlertEspecial(SuiteAppMtto.getInstance().getString(R.string.label_error), codigoError, descripcionError,SuiteAppMtto.getInstance().getString(R.string.common_accept), null);
		
			
		}else if(Constants.ALERT03.equals(validacionalertas) ||Constants.ALERT04.equals(validacionalertas)){
			
			
			SuiteAppMtto.getInstance().getSuiteViewsController().getCurrentViewController().showYesNoAlertEspecialTitulo(codigoError,descripcionError, new OnClickListener() {
				@Override
				public void onClick(final DialogInterface dialog, final int which) {
					
					if (DialogInterface.BUTTON_POSITIVE == which) {
						realizaCambioNumeroCompania(sa.getNumeroAlertas(), sa.getCompaniaAlertas());
					}
				}
			});
			
			
		}
		
		
	}
}
