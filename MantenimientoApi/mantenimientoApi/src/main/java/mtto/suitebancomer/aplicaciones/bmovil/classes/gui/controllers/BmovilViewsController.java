package mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers;

import java.util.ArrayList;

import mtto.com.bancomer.mbanking.BmovilApp;
import mtto.com.bancomer.mbanking.R;
import mtto.com.bancomer.mbanking.SuiteAppMtto;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionAutenticacionDelegate;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseAutenticacion;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseOperacion;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.NuevaContraseniaDelegate;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.QuitarSuspencionDelegate;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ResultadosAutenticacionDelegate;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.proxys.ConfirmacionAutenticacionServiceProxy;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.proxys.ResultadosAutenticacionServiceProxy;
import mtto.suitebancomer.classes.gui.controllers.BaseViewsController;
import mtto.tracking.TrackingHelper;
import suitebancomer.aplicaciones.resultados.commons.SuiteAppCRApi;
import suitebancomer.aplicaciones.resultados.handlers.ConfirmacionAutenticacionHandler;
import suitebancomer.aplicaciones.resultados.handlers.ResultadosAutenticacionHandler;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.IngresoDatosSTViewController;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

public class BmovilViewsController extends BaseViewsController {

    //AMZ
    public ArrayList<String> estados = new ArrayList<String>();
    private BmovilApp bmovilApp;

    /*
     * @Override public void setCurrentActivity(BaseViewController
     * currentViewController) {
     * super.setCurrentActivity(currentViewController); Log.i("Si", "No"); }
     */
    public BmovilViewsController(final BmovilApp bmovilApp) {
        super();
        this.bmovilApp = bmovilApp;
    }

    public void cierraViewsController() {
        bmovilApp = null;
        clearDelegateHashMap();
        super.cierraViewsController();
    }

    public void cerrarSesionBackground() {
        SuiteAppMtto.getInstance().getBmovilApplication()
                .setApplicationInBackground(true);
        SuiteAppMtto.getInstance().getBmovilApplication()
                .logoutApp(true);
    }



    public BmovilApp getBmovilApp() {
        return bmovilApp;
    }

    public void setBmovilApp(final BmovilApp bmovilApp) {
        this.bmovilApp = bmovilApp;
    }

    @Override
    public void onUserInteraction() {

        if (bmovilApp != null) {
            if (SuiteAppMtto.getInstance().getBmovilApplication().getSessionTimer() == null) {
                bmovilApp.initTimer();
            }
            bmovilApp.resetLogoutTimer();
        }
        super.onUserInteraction();
    }

    @Override
    public boolean consumeAccionesDeReinicio() {
        if (!SuiteAppMtto.getInstance().getBmovilApplication()
                .isApplicationLogged()) {
            //SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController().showViewController(SuiteAppMtto.getInstance().getViewsControllerCommons().getClass());
            SuiteAppMtto.getCallback().returnToActivity(SuiteAppMtto.appContext, SuiteAppMtto.getInstance().getActivityToReturn(), true);
            return true;
        }
        return false;
    }

    @Override
    public boolean consumeAccionesDePausa() {
        if (SuiteAppMtto.getInstance().getBmovilApplication().isApplicationLogged()
                && !isActivityChanging()) {
            cerrarSesionBackground();
            return true;
        }
        return false;
    }

    @Override
    public boolean consumeAccionesDeAlto() {
        if (!SuiteAppMtto.getInstance().getBmovilApplication().isChangingActivity()
                && currentViewControllerApp != null) {
            currentViewControllerApp.hideSoftKeyboard();
        }
        SuiteAppMtto.getInstance().getBmovilApplication()
                .setChangingActivity(false);
        return true;
    }

    /**
     * Muestra la confirmacion para quitar la suspension
     *
     * @param ce TODO
     */
    public void showQuitarSuspension(final ConsultaEstatus ce) {
        QuitarSuspencionDelegate delegate = (QuitarSuspencionDelegate) getBaseDelegateForKey(QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID);
        if (delegate != null) {
            removeDelegateFromHashMap(QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID);
        }
        delegate = new QuitarSuspencionDelegate(ce);
        addDelegateToHashMap(
                QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID,
                delegate);
        showConfirmacionAutenticacionViewController(delegate, 0, 0, 0);
    }

    /**
     * Muestra la pantalla de confirmacion autenticacion
     * TODO
     */
    public void showConfirmacionAutenticacionViewController(
            final DelegateBaseAutenticacion autenticacionDelegate, final int resIcon,
            final int resTitle, final int resSubtitle) {
        showConfirmacionAutenticacionViewController(autenticacionDelegate,
                resIcon, resTitle, resSubtitle, R.color.primer_azul);
    }

    /**
     * Muestra la pantalla de confirmacion autenticacion
     */
    public void showConfirmacionAutenticacionViewController(
            final DelegateBaseAutenticacion autenticacionDelegate, final int resIcon,
            final int resTitle, final int resSubtitle, final int resTitleColor) {
        if (getBaseDelegateForKey(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID) != null && !(getBaseDelegateForKey(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID) instanceof ConfirmacionAutenticacionDelegate)) {
            removeDelegateFromHashMap(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);

        }

        ConfirmacionAutenticacionDelegate confirmacionDelegate = (ConfirmacionAutenticacionDelegate) getBaseDelegateForKey(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
        if (confirmacionDelegate != null) {
            removeDelegateFromHashMap(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
        }
        confirmacionDelegate = new ConfirmacionAutenticacionDelegate(
                autenticacionDelegate);
        addDelegateToHashMap(
                ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID,
                confirmacionDelegate);
        //AMZ
        if (estados.size() == 0) {
            //AMZ
            TrackingHelper.trackState("reactivacion", estados);
            //AMZ
        }
        //showViewController(ConfirmacionAutenticacionViewController.class);

        /// implementacion llamado a api de confirmacion y resultados
        final Integer idText = confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado();
        final Integer idNombreImagenEncabezado = confirmacionDelegate.consultaOperationsDelegate().getNombreImagenEncabezado();
        final Boolean statusDesactivado = false;
        final SuiteAppCRApi cr = new SuiteAppCRApi();
        cr.onCreate(SuiteAppMtto.appContext);
        SuiteAppCRApi.setCallBackBConnect(SuiteAppMtto.getCallback());
        SuiteAppCRApi.setCallBackSession(null);
        final ConfirmacionAutenticacionServiceProxy proxy = new ConfirmacionAutenticacionServiceProxy(confirmacionDelegate);

        cr.setProxy(proxy);
        final ConfirmacionAutenticacionHandler handler = new ConfirmacionAutenticacionHandler(proxy, this, new ConfirmacionAutenticacionViewController());
        handler.setActivityChanging(isActivityChanging());


        handler.setDelegateId(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
        handler.invokeActivity(this.estados, idText, idNombreImagenEncabezado, statusDesactivado);
    }

    /**
     * Muestra la pantalla de resultados
     */
    public void showResultadosAutenticacionViewController(
            final DelegateBaseOperacion delegateBaseOperacion, final int resIcon,
            final int resTitle) {
        //ResultadosDelegate.RESULTADOS_DELEGATE_ID
        ResultadosAutenticacionDelegate delegate = (ResultadosAutenticacionDelegate)
                getBaseDelegateForKey(ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID);
        if (delegate != null) {
            //removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
            removeDelegateFromHashMap(ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID);
        }
        delegate = new ResultadosAutenticacionDelegate(delegateBaseOperacion);
        addDelegateToHashMap(
                ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID,
                delegate);
        //showViewController(ResultadosAutenticacionViewController.class);

        // API confirmacion   ResultadosAutenticacion
        final Integer idText = delegate.getOperationDelegate().getTextoEncabezado();
        final Integer idNombreImagenEncabezado = delegate.getOperationDelegate().getNombreImagenEncabezado();
        final Boolean statusDesactivado = false;
        final SuiteAppCRApi cr = new SuiteAppCRApi();
        cr.onCreate(SuiteAppMtto.appContext);
        SuiteAppCRApi.setCallBackBConnect(SuiteAppMtto.getCallback());
        SuiteAppCRApi.setCallBackSession(null);
        final ResultadosAutenticacionServiceProxy proxy = new ResultadosAutenticacionServiceProxy(delegate);
        cr.setProxy(proxy);

        final ResultadosAutenticacionHandler handler = new ResultadosAutenticacionHandler(proxy, this, new ResultadosAutenticacionViewController());
        handler.setActivityChanging(isActivityChanging());
        handler.setDelegateId(ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID);
        handler.invokeActivity(this.estados, idText, idNombreImagenEncabezado, statusDesactivado);

    }

    public void showConfirmacionAutenticacionViewController(
            final DelegateBaseAutenticacion autenticacionDelegate, final int resIcon,
            final int resTitle, final int resSubtitle, final int resTitleColor, final BaseViewControllerCommons parentCommons) {
        ConfirmacionAutenticacionDelegate confirmacionDelegate = (ConfirmacionAutenticacionDelegate) getBaseDelegateForKey(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
        if (confirmacionDelegate != null) {
            removeDelegateFromHashMap(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
        }
        confirmacionDelegate = new ConfirmacionAutenticacionDelegate(
                autenticacionDelegate);
        addDelegateToHashMap(
                ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID,
                confirmacionDelegate);
        //AMZ
        if (estados.size() == 0) {
            //AMZ
            TrackingHelper.trackState("reactivacion", estados);
            //AMZ
        }
        //showViewController(ConfirmacionAutenticacionViewController.class, parentCommons);

        /// implementacion llamado a api de confirmacion y resultados
        final Integer idText = confirmacionDelegate.consultaOperationsDelegate().getTextoEncabezado();
        final Integer idNombreImagenEncabezado = confirmacionDelegate.consultaOperationsDelegate().getNombreImagenEncabezado();
        final Boolean statusDesactivado = false;
        final SuiteAppCRApi cr = new SuiteAppCRApi();
        cr.onCreate(SuiteAppMtto.appContext);
        final ConfirmacionAutenticacionServiceProxy proxy = new ConfirmacionAutenticacionServiceProxy(confirmacionDelegate);

        cr.setProxy(proxy);
        final ConfirmacionAutenticacionHandler handler = new ConfirmacionAutenticacionHandler(proxy, this, new ConfirmacionAutenticacionViewController());
        handler.setActivityChanging(isActivityChanging());


        handler.setDelegateId(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
        handler.invokeActivity(this.estados, idText, idNombreImagenEncabezado, statusDesactivado);
    }

    /**
     * Muestra la pantalla de autenticacion softoken.
     *
     * @param currentPassword
     * @param consultaEstatus
     */
    public void showAutenticacionSoftoken(final ConsultaEstatus consultaEstatus, final String currentPassword) {
        ContratacionSTDelegate delegate = (ContratacionSTDelegate) getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
        if (null == delegate) {
            delegate = new ContratacionSTDelegate();
            addDelegateToHashMap(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID, delegate);
        }

        delegate.setConsultaEstatus(consultaEstatus);
        //delegate.borrarDatosDeSession();


        //delegate.setPassword(currentPassword);
        //	delegate.setOwnerController(SuiteApp.getInstance()
        //			.getSuiteViewsController().getCurrentViewControllerApp());

        //	showViewController(IngresoDatosSTViewController.class);

        delegate.setOwnerController(SuiteAppMtto.getInstance()
                .getSuiteViewsController().getCurrentViewControllerApp());

        //showViewController(IngresoDatosSTViewController.class);

        final String[] extrasKeys = {"login"};
        final Object[] extras = {true};

        showViewController(IngresoDatosSTViewController.class, 0, false, extrasKeys, extras);


    }

	/**
	 * Muestra la pantalla para desbloqueo
	 *
	 * @param ConsultaEstatus
	 */
    public void showDesbloqueo(final ConsultaEstatus ce) {
        NuevaContraseniaDelegate delegate = (NuevaContraseniaDelegate) getBaseDelegateForKey(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID);
        if (delegate != null) {
            removeDelegateFromHashMap(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID);
        }
        delegate = new NuevaContraseniaDelegate(ce);
        addDelegateToHashMap(
                NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID,
                delegate);
        showViewController(NuevaContraseniaViewController.class);
    }

    public void touchMenu() {

        if (estados.size() == 2) {
            estados.remove(1);
        } else {
            final int tam = estados.size();
            for (int i = 1; i < tam; i++) {
                estados.remove(1);
            }
        }
    }

    public void touchAtras() {

        final int ultimo = estados.size() - 1;

        if (ultimo >= 0) {
            final String ult = estados.get(ultimo);
            if (ult == "reactivacion" || ult == "activacion" || ult == "menu token" || ult == "contratacion datos" || ult == "activacion datos") {
                estados.clear();
            } else {
                estados.remove(ultimo);
            }
        }
    }

}