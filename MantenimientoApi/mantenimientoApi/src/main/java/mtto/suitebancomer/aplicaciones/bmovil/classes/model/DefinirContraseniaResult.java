package mtto.suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by evaltierrah on 11/11/15.
 */
public class DefinirContraseniaResult implements ParsingHandler {

    /**
     * Folio de la operacion
     */
    private String folio;

    /**
     * Metodo que regresa el folio de la operacion
     *
     * @return Folio de la arquitectura que devuelve la trasacci�n
     */
    public String getFolio() {
        return folio;
    }

    @Override
    public void process(final Parser parser) throws IOException, ParsingException {
        //Empty method
    }

    @Override
    public void process(final ParserJSON parser) throws IOException, ParsingException {
        folio = parser.parseNextValue("folioArq");
    }
}
