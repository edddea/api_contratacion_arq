/**
 * 
 */
package mtto.suitebancomer.aplicaciones.bmovil.classes.common;

import android.content.Context;
import android.os.PowerManager;
import android.util.Log;

import mtto.com.bancomer.mbanking.SuiteAppMtto;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Implementation of the App power manager for api7 or greater.
 */
public final class EclairPowerManager extends AbstractSuitePowerManager {
	/* (non-Javadoc)
	 * @see suitebancomer.aplicaciones.bmovil.classes.common.AbstractSuitePowerManager#isScreenOn()
	 */
	@Override
	public boolean isScreenOn() {
		try {
			final PowerManager pm = (PowerManager) SuiteAppMtto.appContext.getSystemService(Context.POWER_SERVICE);
			return pm.isScreenOn();
		}catch(Exception ex) {
			if(ServerCommons.ALLOW_LOG) {
                Log.wtf("EclairPowerManager", "Error while getting the state of the screen.", ex);
            }
			return false;
		}
	}

}
