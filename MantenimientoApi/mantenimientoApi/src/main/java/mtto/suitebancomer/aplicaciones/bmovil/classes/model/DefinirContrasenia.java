package mtto.suitebancomer.aplicaciones.bmovil.classes.model;

/**
 * Created by evaltierrah on 11/11/15.
 */
public class DefinirContrasenia {

    /**
     * Guarda la contrasenia nueva
     */
    private String contrasenaNueva;
    /**
     * Guarda el perfil del cliente
     */
    private String perfilCliente;
    /**
     * Guarda el numero de celular del cliente
     */
    private String numCelular;
    /**
     * Guarda el estatus del servicio
     */
    private String estatus;
    /**
     * Guarda el tipo de instrumento
     */
    private String instrumento;
    /**
     * Guarsa el estatus del tipo de instrumento
     */
    private String estatusInstrumento;
    /**
     * Guarda la compania celular
     */
    private String companiaCelular;
    /**
     * Guarda el numero del cliente
     */
    private String numCliente;

    /**
     * Get numCliente
     *
     * @return numCliente
     */
    public String getNumCliente() {
        return numCliente;
    }

    /**
     * Set numCliente
     *
     * @param numCliente numCliente
     */
    public void setNumCliente(final String numCliente) {
        this.numCliente = numCliente;
    }

    /**
     * get ContrasenaNueva
     *
     * @return ContrasenaNueva
     */
    public String getContrasenaNueva() {
        return contrasenaNueva;
    }

    /**
     * Set ContrasenaNueva
     *
     * @param contrasenaNueva ContrasenaNueva
     */
    public void setContrasenaNueva(final String contrasenaNueva) {
        this.contrasenaNueva = contrasenaNueva;
    }

    /**
     * Get PerfilCliente
     *
     * @return PerfilCliente
     */
    public String getPerfilCliente() {
        return perfilCliente;
    }

    /**
     * Set perfilCliente
     *
     * @param perfilCliente perfilCliente
     */
    public void setPerfilCliente(final String perfilCliente) {
        this.perfilCliente = perfilCliente;
    }

    /**
     * Get numCelular
     *
     * @return numCelular
     */
    public String getNumCelular() {
        return numCelular;
    }

    /**
     * Set numCelular
     *
     * @param numCelular numCelular
     */
    public void setNumCelular(final String numCelular) {
        this.numCelular = numCelular;
    }

    /**
     * Get estatus
     *
     * @return estatus
     */
    public String getEstatus() {
        return estatus;
    }

    /**
     * Set estatus
     *
     * @param estatus estatus
     */
    public void setEstatus(final String estatus) {
        this.estatus = estatus;
    }

    /**
     * Get instrumento
     *
     * @return instrumento
     */
    public String getInstrumento() {
        return instrumento;
    }

    /**
     * Set instrumento
     *
     * @param instrumento instrumento
     */
    public void setInstrumento(final String instrumento) {
        this.instrumento = instrumento;
    }

    /**
     * Get estatusInstrumento
     *
     * @return estatusInstrumento
     */
    public String getEstatusInstrumento() {
        return estatusInstrumento;
    }

    /**
     * Set estatusInstrumento
     * @param estatusInstrumento estatusInstrumento
     */
    public void setEstatusInstrumento(final String estatusInstrumento) {
        this.estatusInstrumento = estatusInstrumento;
    }

    /**
     * Get companiaCelular
     * @return companiaCelular
     */
    public String getCompaniaCelular() {
        return companiaCelular;
    }

    /**
     * Set companiaCelular
     * @param companiaCelular companiaCelular
     */
    public void setCompaniaCelular(final String companiaCelular) {
        this.companiaCelular = companiaCelular;
    }

}
