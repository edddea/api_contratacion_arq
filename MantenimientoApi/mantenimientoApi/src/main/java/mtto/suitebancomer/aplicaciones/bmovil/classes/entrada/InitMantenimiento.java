package mtto.suitebancomer.aplicaciones.bmovil.classes.entrada;


import android.app.Activity;
import android.content.Context;
import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.mbanking.softtoken.SuiteAppApi;
import mtto.com.bancomer.mbanking.SuiteAppMtto;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.NuevaContraseniaViewController;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.OlvidastePasswordViewController;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.NuevaContraseniaDelegate;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.OlvidastePasswordDelegate;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.QuitarSuspencionDelegate;
import mtto.suitebancomer.aplicaciones.bmovil.classes.model.BanderasServer;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;

/**
 * Created by amgonzaleze on 15/08/2015.
 */
public class InitMantenimiento {


    final SuiteAppMtto apiMantenimiento = new SuiteAppMtto();
    final BmovilViewsController bmovilViewsController;

    /**
     * Constructor class
     *
     * @param context
     * @param callback
     * @param activityToReturn
     * @param banderasServer
     */
    public InitMantenimiento(final Context context, final CallBackBConnect callback, final Activity activityToReturn, final BanderasServer banderasServer) {
        ServerCommons.ALLOW_LOG = banderasServer.getLogActivo();
        ServerCommons.DEVELOPMENT = banderasServer.isDesarrollo();
        ServerCommons.EMULATOR = banderasServer.getEmulador();
        ServerCommons.SIMULATION = banderasServer.getSimulacion();

        //up the context api
        apiMantenimiento.onCreate(context);
        apiMantenimiento.setActivityToReturn(activityToReturn);
        apiMantenimiento.setCallback(callback);
        bmovilViewsController = apiMantenimiento.getBmovilApplication().getBmovilViewsController();

        final SuiteAppApi sa = new SuiteAppApi();
        sa.onCreate(context);

    }


    /**
     * metodo para iniciar el desbloqueo
     *
     * @param consultaEstatus
     */
    public void startDesbloqueo(final ConsultaEstatus consultaEstatus) {

        NuevaContraseniaDelegate delegate = (NuevaContraseniaDelegate) bmovilViewsController.getBaseDelegateForKey(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID);
        if (delegate != null) {
            bmovilViewsController.removeDelegateFromHashMap(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID);
        }
        Session.getInstance(SuiteAppMtto.appContext).setSecurityInstrument(consultaEstatus.getInstrumento());
        delegate = new NuevaContraseniaDelegate(consultaEstatus);
        bmovilViewsController.addDelegateToHashMap(
                NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID,
                delegate);

        bmovilViewsController.showViewController(NuevaContraseniaViewController.class);
    }

    /**
     * metodo para iniciar el quitarSuspencion
     *
     * @param consultaEstatus
     */
    public void startQuitarSuspencion(final ConsultaEstatus consultaEstatus) {
        QuitarSuspencionDelegate delegate = (QuitarSuspencionDelegate) bmovilViewsController.getBaseDelegateForKey(QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID);
        if (delegate != null) {
            bmovilViewsController.removeDelegateFromHashMap(QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID);
        }
        Session.getInstance(SuiteAppMtto.appContext).setSecurityInstrument(consultaEstatus.getInstrumento());
        delegate = new QuitarSuspencionDelegate(consultaEstatus);
        final int resSubtitle = 0;
        final int resTitleColor = 0;
        final int resIcon = 0;
        final int resTitle = 0;
        bmovilViewsController.showConfirmacionAutenticacionViewController(delegate, 0, 0, 0, 0);
    }

    /**
     * Metodo que inicia la pantalla de Olvidaste tu contrasenia
     *
     * @param numeroTelefono Se envia el numero de telefono
     */
    public void startOlvidastePassword(final String numeroTelefono, final CallBackBConnect callBackBConnect) {
        SuiteAppMtto.getInstance().setCallback(callBackBConnect);
        OlvidastePasswordDelegate delegate = (OlvidastePasswordDelegate) bmovilViewsController.getBaseDelegateForKey(OlvidastePasswordDelegate.OLVIDASTE_CONTRASENIA_DELEGATE_ID);
        if (delegate != null) {
            bmovilViewsController.removeDelegateFromHashMap(OlvidastePasswordDelegate.OLVIDASTE_CONTRASENIA_DELEGATE_ID);
        }
        delegate = new OlvidastePasswordDelegate();
        bmovilViewsController.addDelegateToHashMap(
                OlvidastePasswordDelegate.OLVIDASTE_CONTRASENIA_DELEGATE_ID,
                delegate);
        final String[] keys = {Constants.PAR_NUMERO_TELEFONO};
        final Object[] extras = {new String(numeroTelefono)};
        bmovilViewsController.showViewController(OlvidastePasswordViewController.class, 0, Boolean.FALSE, keys, extras);
    }

}
