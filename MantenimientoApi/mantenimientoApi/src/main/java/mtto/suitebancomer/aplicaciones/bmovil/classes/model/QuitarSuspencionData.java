package mtto.suitebancomer.aplicaciones.bmovil.classes.model;

import java.io.IOException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;

/**
 * Created by alanmichaelgonzalez on 22/12/15.
 */
public class QuitarSuspencionData implements ParsingHandler {

    private String estado;

    private String ivr;

    public String getEstado() {
        return estado;
    }

    public String getIvr() {
        return ivr;
    }

    public void setIvr(final String ivr) {
        this.ivr = ivr;
    }

    public void setEstado(final String estado) {
        this.estado = estado;
    }

    @Override
    public void process(final Parser parser) throws IOException, ParsingException {
        //Empty method
    }

    @Override
    public void process(final ParserJSON parser) throws IOException, ParsingException {
        //Empty method
    }
}
