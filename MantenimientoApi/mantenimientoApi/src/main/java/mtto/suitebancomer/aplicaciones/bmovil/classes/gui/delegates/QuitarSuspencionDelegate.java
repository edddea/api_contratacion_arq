package mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Constants.Perfil;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import mtto.com.bancomer.mbanking.R;
import mtto.com.bancomer.mbanking.SuiteAppMtto;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController;
import mtto.suitebancomer.aplicaciones.bmovil.classes.model.QuitarSuspencionData;
import mtto.suitebancomer.classes.gui.controllers.BaseViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Autenticacion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

//import mtto.suitebancomer.aplicaciones.bmovil.classes.io.Server;

public class QuitarSuspencionDelegate extends DelegateBaseAutenticacion {

	public static final long QUITAR_SUSPENSION_DELEGATE_ID = 879832225153703382L;
	private Perfil perfil;
	private ConsultaEstatus consultaEstatus;
	
	private ConfirmacionAutenticacionViewController viewController;
	
//	public QuitarSuspencionDelegate(Perfil perfil) {
//		this.perfil = perfil;
//	}
	
	public QuitarSuspencionDelegate(final ConsultaEstatus ce) {
		consultaEstatus = ce;
		perfil = ce.getPerfil();
	}
	/**
	 * @return true si se debe mostrar contrasena, false en caso contrario.
	 */
	@Override
	public boolean mostrarContrasenia() {
		final boolean value =  Autenticacion.getInstance().mostrarContrasena(Constants.Operacion.quitarSuspension,
				perfil);
		return value;
	}

	/**
	 * @return true si se debe mostrar CVV, false en caso contrario.
	 */
	@Override
	public boolean mostrarCVV() {
		return Autenticacion.getInstance().mostrarCVV(Constants.Operacion.quitarSuspension, perfil);
	}
	
	/**
	 * @return true si se debe mostrar NIP, false en caso contrario.
	 */
	@Override
	public boolean mostrarNIP() {
		final boolean value =  Autenticacion.getInstance().mostrarNIP(Constants.Operacion.quitarSuspension,
				perfil);
		return value;
	}
	
	/**
	 * @return El tipo de token a mostrar
	 */
	@Override
	public TipoOtpAutenticacion tokenAMostrar() {
		Constants.TipoOtpAutenticacion tipoOTP;
		try {
			tipoOTP = Autenticacion.getInstance().tokenAMostrar(Constants.Operacion.quitarSuspension,
									perfil);
		} catch (Exception ex) {
//			if(Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error on Autenticacion.tokenAMostrar execution.", ex);
			tipoOTP = null;
		}		
		return tipoOTP;
	}
	
	
	@Override
	public ArrayList<Object> getDatosTablaConfirmacion() {
		final ArrayList<Object> tabla = new ArrayList<Object>();
		ArrayList<String> fila;
	
		fila = new ArrayList<String>();
		fila.add(SuiteAppMtto.appContext.getString(R.string.bmovil_quitarsuspension_tabla_operacion));
		fila.add(SuiteAppMtto.appContext.getString(R.string.bmovil_quitarsuspension_tabla_suspension));
		tabla.add(fila);	
				
		return tabla;
	}
	
	
	@Override
	public void realizaOperacion(
			final ConfirmacionAutenticacionViewController confirmacion, final String contrasenia, final String nip, final String token, final String cvv, final String campoTarjeta) {
		final int operationId = ApiConstants.QUITAR_SUSPENSION;
		final Autenticacion aut = Autenticacion.getInstance();
		final String cadAutenticacion = aut.getCadenaAutenticacion(Constants.Operacion.quitarSuspension, perfil);

		final Session session = Session.getInstance(SuiteAppMtto.appContext);
		final String username = consultaEstatus.getNumCelular();
		long seed = session.getSeed();
			if (seed == 0) {
				seed = System.currentTimeMillis();
				session.setSeed(seed);
			}
		final String ium = Tools.buildIUM(username, seed, SuiteAppMtto.appContext);
		session.setIum(ium);
		final String numTarjeta = "";
		
		final Hashtable<String, String> params = new Hashtable<String, String>();
		params.put( ServerConstants.NUMERO_TELEFONO, username);
		params.put(ServerConstants.NUMERO_TARJETA, numTarjeta);
		params.put( ServerConstants.NUMERO_CLIENTE, consultaEstatus.getNumCliente());
		//params.put( ServerConstants.IUM, ium);
		params.put(ServerConstants.COMPANIA_CELULAR,consultaEstatus.getCompaniaCelular());
		params.put(ServerConstants.CVE_ACCESO, contrasenia == null ? "" : contrasenia);
		params.put(ServerConstants.CODIGO_NIP, nip == null ? "" : nip);
		params.put(ServerConstants.CODIGO_CVV2, cvv == null ? "" : cvv);
		params.put(ServerConstants.CODIGO_OTP, token == null ? "" : token);
		params.put(ServerConstants.CADENA_AUTENTICACION, cadAutenticacion);
		params.put(ServerConstants.TARJETA_5DIG, campoTarjeta == null ? "" : campoTarjeta);
		params.put("sistemaOperativo", "android");
		params.put(ServerConstants.PARAMS_TEXTO_EN, suitebancomercoms.aplicaciones.bmovil.classes.common.Constants.EMPTY_ENCRIPTAR);
		final List<String> listaEncriptar = Arrays.asList(ServerConstants.CVE_ACCESO, ServerConstants
						.CODIGO_NIP,
				ServerConstants.CODIGO_CVV2);
		Encripcion.setContext(SuiteAppMtto.appContext);
		Encripcion.encriptaCadenaAutenticacion(params, listaEncriptar);
		viewController = confirmacion;
		//JAIG
		doNetworkOperation(operationId, params,true, new QuitarSuspencionData(), confirmacion);
	}
	
	
	@Override
	public void doNetworkOperation(final int operationId, final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewController caller) {
		SuiteAppMtto.getInstance().getBmovilApplication().invokeNetworkOperation(operationId, params,isJson, handler, caller);
	}
	
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {

		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			mostrarAlertaResultados();
		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
			final BaseViewControllerCommons current = SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp();
			current.showInformationAlert(response.getMessageText());
		}
	}

	private void mostrarAlertaResultados(){
		final int message = R.string.bmovil_quitarsuspension_alerta;
		BaseViewControllerCommons current = SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp();
		OnClickListener listener ;
		listener = new OnClickListener() {
			
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.dismiss();
				showPantallaPrincipal();
			}
		};
		if(current==null)
			current=new ConfirmacionAutenticacionViewController();
		current.showInformationAlert(message, listener);
		
	}
	
	
	@Override
	public int getTextoEncabezado() {
		return R.string.bmovil_quitarsuspension_title;
	}
	
	@Override
	public int getNombreImagenEncabezado() {
		return R.drawable.bmovil_quitar_suspension_icono;
	}

	private void showPantallaPrincipal(){
		if(Server.ALLOW_LOG) Log.d(getClass().getName(), "mostrarPantallaPrincipal");
		SuiteAppMtto.getInstance().getBmovilApplication().reiniciaAplicacion();		
		
		SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(this.viewController);
		
		//SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController().showViewController(SuiteAppMtto.getInstance().getViewsControllerCommons().getClass());
		SuiteAppMtto.getCallback().returnToActivity(SuiteAppMtto.appContext, SuiteAppMtto.getInstance().getActivityToReturn(),true);
		
	}
	
	@Override
	public boolean mostrarCampoTarjeta() {
		return (mostrarCVV() || mostrarNIP());
	}
}
