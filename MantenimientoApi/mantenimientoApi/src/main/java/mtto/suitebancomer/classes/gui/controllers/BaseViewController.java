/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidental Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package mtto.suitebancomer.classes.gui.controllers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.bancomer.base.SuiteApp;
import java.lang.reflect.Method;
import mtto.com.bancomer.mbanking.SuiteAppMtto;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ControlEventos;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomercoms.classes.gui.controllers.BaseViewsControllerCommons;
import suitebancomercoms.classes.gui.delegates.BaseDelegateCommons;
import mtto.suitebancomer.classes.common.GuiTools;
import mtto.com.bancomer.mbanking.R;

/**
 * BaseScreen is the base class for all the screens in the MBanking application
 * It provides basic, common functionality to be included in the business logic
 * so that the application can manage screens in a uniform, consistent way.
 *
 * @author Stefanini IT Solutions
 */
public abstract class BaseViewController extends BaseViewControllerCommons {
	private static Method methodOverridePendingTransition;
	static {
		final Method[] actMethods = Activity.class.getMethods();
		final int methodSize = actMethods.length;
		for (int i=0; i<methodSize; i++) {
			if (actMethods[i].getName().equals("overridePendingTransition")) {
				methodOverridePendingTransition = actMethods[i];
				break;
			}
		}
	}
	//AMZ
		private BmovilViewsController bm;
		private SofttokenViewsController st;

	// #region Variables.
	/**
	 * indica si es un flujo de estatus desactivado
	 */
	public boolean statusdesactivado;
	
	/**
     * To use as parameter in aplicaEstilo method in order to show the header
     */
    public final static int SHOW_HEADER = 2;
    
    /**
     * To use as parameter in aplicaEstilo method in order to show the title
     */
    public final static int SHOW_TITLE = 4;
    
	/**
     * Current view edit fields' references. Each view should implement this variable.
     */
	protected static EditText[] sFields;
	
	/**
     * Determines if an alert pop-up is being shown
     */
    protected boolean mShowingDialogPopup = false;

    /**
     * A progress dialog for long waiting processes.
     */
    protected ProgressDialog mProgressDialog;
    
    /**
     * The alert error or information dialog.
     */
    private AlertDialog mAlertDialog;
	
	/**
	 * Referencias al controlador de vistas padre de esta vista
	 */
	protected BaseViewsControllerCommons parentViewsController;
	
	/**
     * Referencia al layout del cabecero
     */
    private LinearLayout mHeaderLayout;
    
    /**
     * Referencia al layout del t���tulo
     */
    private LinearLayout mTitleLayout;
    
    /**
     * Referencia al separador del t���tulo
     */
    private ImageView mTitleDivider;
	
	/**
	 * Layout where each view will be drawn.
	 */
	private ViewGroup mBodyLayout;
	
	/**
	 * Par���metros con los que se construir��� la pantalla b���sica
	 */
	private int activityParameters;
	
	/**
     * Referencia al delegate de esta vista
     */
	private BaseDelegateCommons delegate;
	
	/**
     * Referencia a la clase misma
     */
	private static BaseViewController me;
	
	/**
     * M���todo que devuelve una instancia de la clase
     * @return una instancia de esta clase
     */
	public static BaseViewController getInstance() {
		return me;
	}
	// #endregion
	
	/**
	 * Constructor por defecto
	 */
	public BaseViewController() {
		me = this;	
	}
	
	/**
	 * Called when the activity is first created.
	 * @param savedInstanceState state
	 */
	public void onCreate(final Bundle savedInstanceState, final int parameters, final int layoutID) {
		try {
			methodOverridePendingTransition.invoke(this, new Object[]{R.anim.screen_enter, R.anim.screen_leave});
		} catch (Exception e) {
			Log.d("APIMtto",e.toString());
		}
		super.onCreate(savedInstanceState);
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
    	activityParameters = parameters;
    	setContentView(SuiteAppMtto.getResourceId("mtto_layout_base_activity", "layout"));
    	
    	// references to ui elements
        mHeaderLayout = (LinearLayout) findViewById(SuiteAppMtto.getResourceId("header_layout", "id"));
        mTitleLayout = (LinearLayout) findViewById(SuiteAppMtto.getResourceId("title_layout","id"));
        mTitleDivider = (ImageView) findViewById(SuiteAppMtto.getResourceId("title_divider","id"));
        mBodyLayout = (ViewGroup) findViewById(SuiteAppMtto.getResourceId("body_layout", "id"));
        
        // 	set header visibility
        mHeaderLayout.setVisibility(((activityParameters&SHOW_HEADER)==SHOW_HEADER)?View.VISIBLE:View.GONE);
        //set title visibility, by default is gone until a call to aplicaEstilo is made
        mTitleLayout.setVisibility(View.GONE);
        mTitleDivider.setVisibility(View.GONE);
        
        // load received layout id in the body
        final LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(layoutID, mBodyLayout, true);
        
        //AMZ
        bm = SuiteAppMtto.getInstance().getBmovilApplication().getBmovilViewsController();
        //st = SuiteAppMtto.getInstance().getSofttokenApplication().getSottokenViewsController();
        
        
        /* ---------------------------------------------------- */
        scaleForCurrentScreen();
	}
	
	/**
	 * Screen is about to be shown.
	 */
	@Override
	protected void onStart() {
		super.onStart();
		sFields = null;
	}

	/*
	 * (non-Javadoc)
	 * @see android.app.Activity#onPause()
	 */
	@Override
	protected void onPause() {
		if(!ControlEventos.isScreenOn(this)) {
			if(ServerCommons.ALLOW_LOG) {
				Log.i(getClass().getSimpleName(), "Se ha bloqueado el telefono");
			}
			if(SuiteAppMtto.getCallback() != null) {
				if(ServerCommons.ALLOW_LOG) {
					Log.d(getClass().getSimpleName(), "Se ejecuta cierraSesion desde CallBackBConnect");
				}
				SuiteAppMtto.getCallback().cierraSesion();
			}
			finish();
		}
		super.onPause();
	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onStop()
	 */
	@Override
	protected void onStop() {
		if(ServerCommons.ALLOW_LOG) {
			Log.e(getClass().getSimpleName(), "OnStop");
		}
		if(ControlEventos.isAppIsInBackground(this.getBaseContext())){ //Valida el estado de la app si es change Activity no hace nada
			if(ServerCommons.ALLOW_LOG) {
				Log.d(getClass().getSimpleName(), "La app se fue a Background");
			}
			if(SuiteAppMtto.getCallback() != null) {
				if(ServerCommons.ALLOW_LOG) {
					Log.d(getClass().getSimpleName(), "Se ejecuta cierraSesionBackground desde CallBackBConnect");
				}
				SuiteAppMtto.getCallback().cierraSesionBackground(Boolean.FALSE);
			}
			finish();
		}
		super.onStop();
	}


	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		return false;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(final Menu menu) {
		return false;
	}
	
    /**
     * Places the title string and respective icon on screen.
     * @param titleText the screen title
     * @param iconResource the resource of the screen icon
     */
    public void setTitle(final int titleText, final int iconResource) {
    	setTitle(titleText, iconResource, SuiteAppMtto.getResourceId("segundo_azul", "color"));
    }
    
    /**
     * Places the title string and respective icon on screen with the desired color
     * @param titleText the screen title
     * @param iconResource the resource of the screen icon
     */
    public void setTitle(final int titleText, final int iconResource, final int colorResource) {
    	
    	if ((activityParameters&SHOW_TITLE)==SHOW_TITLE) {
        	mTitleLayout.setVisibility(View.VISIBLE);
        	final ImageView icon = (ImageView)mTitleLayout.findViewById(SuiteAppMtto.getResourceId("title_icon", "id"));
        	if(iconResource > 0){
        		icon.setImageDrawable(getResources().getDrawable(iconResource));
        	}
        	if(titleText > 0){
        		final String title = getString(titleText);
        		final TextView titleTextView = (TextView)mTitleLayout.findViewById(SuiteAppMtto.getResourceId("title_text", "id"));
        		titleTextView.setTextColor(getResources().getColor(colorResource));
        		titleTextView.setText(title);
        	}
        	mTitleDivider.setVisibility(View.VISIBLE);
        } else {
        	mTitleLayout.setVisibility(View.GONE);
        	mTitleDivider.setVisibility(View.GONE);
        }
    }

    /**
     * Overrides and cancels android's back button, if the button is visible then it does the same
     * as clicking on the application's back button.
     */
    @Override    
    public boolean onKeyDown(final int keyCode, final KeyEvent event) {
    	onUserInteraction();
    	switch (keyCode){
    		case KeyEvent.KEYCODE_BACK:
    			//AMZ
    			if(bm!=null){
    			bm.touchAtras();
    			}
    			if(st!=null){
    				st.touchAtras();
    			}
    			

    			goBack();
            	return true;
	        default:
	        	return super.onKeyDown(keyCode, event);
    	}
    } 
 
    /**
     * 
     * This function has to be overridden by child members, it implements
     * the processing for server response.
     * 
     * @param operationId
     * @param response
     */
    public void processNetworkResponse(final int operationId, final ServerResponse response) {
        // Empty method
    }
    
    /**
     * Determines if the user touched or performed some action in the screen.
     * If he or she did, then reset the logout timer.
     */
	@Override
	public void onUserInteraction() {
//		if(Server.ALLOW_LOG) Log.i(getClass().getSimpleName(), "User Interacted");
		if (parentViewsController != null) {
			parentViewsController.onUserInteraction();
		}
		super.onUserInteraction();
	}
	
	/**
	 * Defines when the last click occurred
	 */
	private long touchDownTime;
	
	/*
	 * Called to process touch screen events. 
	 */
	@Override
	public boolean dispatchTouchEvent(final MotionEvent ev) {
		
	    switch (ev.getAction()){
		    case MotionEvent.ACTION_DOWN:
		        touchDownTime = SystemClock.elapsedRealtime();
		        break;
		        
		    case MotionEvent.ACTION_UP:
		    	//to avoid drag events
		        if (SystemClock.elapsedRealtime() - touchDownTime <= 150){	
		        	
		        	final EditText[] textFields = this.getFields();
		        	if(textFields != null && textFields.length > 0){
		        		boolean clickIsOutsideEditTexts = true;
		        		
		        		for(final EditText field : textFields){
		        			if(isPointInsideView(ev.getRawX(), ev.getRawY(), field)){
		        				clickIsOutsideEditTexts = false;
		        				break;
		        			}
		        		}
		        		
		        		if(clickIsOutsideEditTexts){
		        			this.hideSoftKeyboard();
		        		}        		
		        	} else {
		        		this.hideSoftKeyboard();
		        	}
		        }
		        break;
	    }
		
		return super.dispatchTouchEvent(ev);
	}
	
	/**
	 * Override this from children to return the view's fields.
	 * @return the current screen fields.
	 */
	protected EditText[] getFields(){
		return sFields;
	}
	
	/**
	 * Gets the trimmed text of a text field content.
	 * @param textField the EditText component to retrieve text
	 * @return the textField value without trailing spaces
	 */
	protected String getText(final EditText textField){
		if(textField != null){
			return textField.getText().toString().trim();
		} else {
			return "";
		}
	}
	
	public float getHeaderHeight() {
		mHeaderLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		mTitleLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		mTitleDivider.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		
		final float headerHeight = mHeaderLayout.getMeasuredHeight() + mTitleLayout.getMeasuredHeight() +
								mTitleDivider.getMeasuredHeight();
		
		return headerHeight;
	}
	
	public float getBodyHeight() {
		mBodyLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		return mBodyLayout.getMeasuredHeight();
	}
	
	/**
	 * Sets the layout that will fill the empty body layout in base screen
	 * template.
	 *
	 * @param layoutResource the layout resources
	 */
	protected final void setBodyLayout(final int layoutResource) {
		final LayoutInflater inflater = (LayoutInflater)
							this.getSystemService(
							Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(layoutResource, mBodyLayout);
	}


    /**
     * Hides the soft keyboard if any editbox called it.
     */
    public void hideSoftKeyboard() {
    	final InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
    	final View focusedView = getCurrentFocus();
    	if(imm != null && focusedView != null){
    		imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
    	}
    }
    
    /**
     * This is called whenever the back button is pressed.
     * Normally, this function will return to the previous screen.
     */
    public void goBack() {
    	
    	this.hideSoftKeyboard();
    	parentViewsController.setActivityChanging(true);
    	finish();
    	parentViewsController.overrideScreenBackTransition();
    }
    
    
    /**
     * Shows an error message if the provided resource exists. The alert
     * view title will be "Error", and it will have one button to accept.
     *
     * @param errorMessage the error message resource to show
     */
    public void showErrorMessage(final int errorMessage) {
    	if (errorMessage > 0) {	
    		showErrorMessage(getString(errorMessage), null);
    	}
    }
    
    public void showErrorMessage(final String errorMessage) {
    	showErrorMessage(errorMessage, null);
    }
    
    /**
     * Places a progress dialog, for long waiting processes.
     * 
     * @param strTitle the dialog title
     * @param strMessage the message to show in the while
     */
    public void muestraIndicadorActividad(final String strTitle, final String strMessage) {
    	if(mProgressDialog != null){
    		ocultaIndicadorActividad();
    	}	
		mProgressDialog = ProgressDialog.show(SuiteApp.appContext, strTitle,
    			strMessage, true);
		mProgressDialog.setCancelable(false);
    }
    
    /**
     * Hides the progress dialog.
     */
    public void ocultaIndicadorActividad() {
    	if(mProgressDialog != null){
    		mProgressDialog.dismiss();
    	}
    }
	
	/**
     * Shows an error message if the message length is greater than 0. The alert
     * view title will be "Error", and it will have one button to accept.
     *
     * @param errorMessage the error message text to show
     *
     */
    public void showErrorMessage(final String errorMessage, final OnClickListener listener){
    	
    	if(errorMessage.length() > 0){ 		
    		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SuiteApp.appContext);
    		alertDialogBuilder.setTitle(SuiteAppMtto.getResourceId("label.error", "string"));
    		alertDialogBuilder.setIcon(SuiteAppMtto.getResourceId("anicalertaaviso", "drawable"));
    		alertDialogBuilder.setMessage(errorMessage);
    		alertDialogBuilder.setPositiveButton(SuiteAppMtto.getResourceId("label.ok", "string"), listener);
    		alertDialogBuilder.setCancelable(false);
        	mAlertDialog = alertDialogBuilder.show();		
    	}
    }
	
    /**
     * Determines if given points are inside view
     * @param x - x coordinate of point
     * @param y - y coordinate of point
     * @param view - view object to compare
     * @return true if the points are within view bounds, false otherwise
     */
    private boolean isPointInsideView(final float x, final float y, final View view){
    	final int location[] = new int[2];
		view.getLocationOnScreen(location);
		final int viewX = location[0];
		final int viewY = location[1];

		//point is inside view bounds
		if(( x > viewX && x < (viewX + view.getWidth())) &&
				( y > viewY && y < (viewY + view.getHeight()))){
			return true;
		} else {
			return false;
		}
    }

	public BaseViewsControllerCommons getParentViewsController() {
		return parentViewsController;
	}

	public void setParentViewsController(final BaseViewsControllerCommons parentViewsController) {
		this.parentViewsController = parentViewsController;
	}

	public BaseDelegateCommons getDelegate() {
		return delegate;
	}

	public void setDelegate(final BaseDelegateCommons delegate) {
		this.delegate = delegate;
	}
	
	public void hideCurrentDialog() {
		try {
			if (mAlertDialog != null) {
				mAlertDialog.dismiss();
				mAlertDialog = null;
			}
		} catch(Exception ex) {
            if(Server.ALLOW_LOG) {
                Log.e("MenuSuiteDelegate", ex.getMessage());
            }
		}
	}
	
	public void setCurrentDialog(final AlertDialog dialog) {
		this.mAlertDialog = dialog;
	}
	
	/**
	 * Establece la l���gica a seguir para el redimensionamiento de la pantalla.
	 */
	private void scaleForCurrentScreen() {
		final GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(getWindowManager());
        
        guiTools.scale(mHeaderLayout);
		guiTools.scale(mTitleLayout);
		guiTools.scale(mTitleLayout.findViewById(SuiteAppMtto.getResourceId("title_icon", "id")));
		guiTools.scale(mTitleLayout.findViewById(SuiteAppMtto.getResourceId("title_text", "id")), true);
		guiTools.scale(mTitleDivider);
		guiTools.scale(mBodyLayout);
	}
	
	private ScrollView getScrollView(){
		return(ScrollView)findViewById(SuiteAppMtto.getResourceId("body_layout", "id"));		
	}

	protected void moverScroll() {
		getScrollView().post(new Runnable() {

			@Override
			public void run() {
				getScrollView().fullScroll(ScrollView.FOCUS_UP);
//				if(Server.ALLOW_LOG) Log.d(getClass().getName(), "Mover scroll");
			}
		});		
	}

	// #region Information Alert.
    /**
     * Muestra una alerta con el mensaje dado.
     * @param message el mensaje a mostrar
     */
    public void showInformationAlert(final int message) {
    	if (message > 0) {		
    		showInformationAlert(getString(message));
    	}
    }
    
    /**
     * Shows an information alert with the given message text. No action is performed after this alert is closed.
     * @param message the message text to show
     */
    public void showInformationAlert(final String message) {
    	if (message.length() > 0) {
    		showInformationAlert(message, null);
    	}
    }
    
    /**
     * Shows an information alert with the given message.
     * @param message the message resource to show
     */
    public void showInformationAlert(final int message, final OnClickListener listener) {
    	if (message > 0) {
    		showInformationAlert(SuiteApp.appContext.getString(message), listener);
    	}
    }
    
	/**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(final String message, final OnClickListener listener) {
    	showInformationAlert(SuiteApp.appContext.getString(SuiteAppMtto.getResourceId("label.information", "string")), message, listener);
    }
    
    /**
	 * Shows an alert dialog.
	 * @param title The alert title resource id.
	 * @param message The alert message resource id.
	 * @param listener The listener for the "Ok" button. 
	 */
    public void showInformationAlert(final int title, final int message, final OnClickListener listener) {
    	if(message > 0) {
    		showInformationAlert(SuiteApp.appContext.getString(title), SuiteApp.appContext.getString(message), listener);
    	}
    }
    
    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param title The alert title resource id.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlert(final String title, final String message, final OnClickListener listener) {
    	showInformationAlert(title, message, SuiteApp.appContext.getString(SuiteAppMtto.getResourceId("label.ok", "string")), listener);
    }
    
    
    /**
     * Shows an information alert with the given message text. A listener is
     * passed to perform action on close.
     * @param title The alert title resource id.
     * @param message the message text to show
     * @param listener onClickListener to perform action on close
     */
    public void showInformationAlertEspecial(final String title, final String errorCode, final String descripcionError, final OnClickListener listener) {
    	showInformationAlertEspecial(title, errorCode, descripcionError,SuiteApp.appContext.getString(SuiteAppMtto.getResourceId("label.ok", "string")), listener);
    }
    
    /**
	 * Shows an alert dialog.
	 * @param title The alert title resource id.
	 * @param message The alert message resource id.
	 * @param okText the ok text
	 * @param listener The listener for the "Ok" button. 
	 */
    public void showInformationAlert(final int title, final int message, final int okText, final OnClickListener listener) {
    	if(message > 0) {
    		showInformationAlert(getString(title), getString(message), getString(okText), listener);
    	}
    }
    
    /**
	 * Shows an alert dialog.
	 * @param title The alert title.
	 * @param message The alert message.
	 * @param okText the ok text
	 * @param listener The listener for the "Ok" button. 
	 */
	public void showInformationAlert(final String title, final String message, final String okText, OnClickListener listener) {
		if(!habilitado)
			return;		
		habilitado = false;
    	if(message.length() > 0){
    		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(SuiteApp.appContext);
    		alertDialogBuilder.setTitle(title);
    		alertDialogBuilder.setIcon(SuiteAppMtto.getResourceId("anicalertaaviso", "drawable"));    		
    		alertDialogBuilder.setMessage(message);
    		if (null == listener) {
    			listener = new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						dialog.dismiss();
					}
				};
    		}

    		alertDialogBuilder.setPositiveButton(okText, listener);
    		alertDialogBuilder.setCancelable(false);
    		mAlertDialog = alertDialogBuilder.create();
    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(final DialogInterface dialog) {
					habilitado = true;
				}
			});
    		mAlertDialog.show();
    	}
    }
	
	
	 /**
		 * Shows an alert dialog.
		 * @param title The alert title.
		 * @param message The alert message.
		 * @param okText the ok text
		 * @param listener The listener for the "Ok" button. 
		 */
		public void showInformationAlertEspecial(final String title, final String errorCode, final String descripcionError, final String okText, OnClickListener listener) {
			if(!habilitado)
				return;		
			habilitado = false;
	    	if(descripcionError.length() > 0){

				final LayoutInflater inflater = (LayoutInflater)SuiteApp.appContext.getSystemService
						(Context.LAYOUT_INFLATER_SERVICE);
	    		final AlertDialog.Builder builder = new AlertDialog.Builder(SuiteApp.appContext);
	    		builder.setTitle(title);
	    		builder.setIcon(SuiteAppMtto.getResourceId("anicalertaaviso", "drawable"));    	
	    	    //LayoutInflater inflater = getLayoutInflater();
	    	    final View vista = inflater.inflate(SuiteAppMtto.getResourceId("mtto_layout_alert_codigo_error", "layout"), null);
	    	    /*builder.setView(vista)
	    	       .setPositiveButton(okText, new DialogInterface.OnClickListener() {
	    	           public void onClick(DialogInterface dialog, int id) {
	    	                  dialog.dismiss();
	    	           }
	    	    });*/
	    	    if (null == listener) {
	    			listener = new OnClickListener() {
						@Override
						public void onClick(final DialogInterface dialog, final int which) {
							dialog.dismiss();
						}
					};
	    		}
	    	    builder.setView(vista)
	    	       .setPositiveButton(okText, listener);
	    	    
	    	    final TextView text1 = (TextView) vista.findViewById(SuiteAppMtto.getResourceId("descripcionError", "id"));
	    	    final TextView text2 = (TextView) vista.findViewById(SuiteAppMtto.getResourceId("codigoError", "id"));
	    	    text1.setText(descripcionError);
	    	    text2.setText(errorCode);
	    	 
	    	    builder.setCancelable(false);
	    		
	    	    mAlertDialog = builder.create();
	    	    
	    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
					@Override
					public void onDismiss(final DialogInterface dialog) {
						habilitado = true;
					}
				});
	    		mAlertDialog.show();
	    	    
	    	}
	    }
	
	public void showYesNoAlert(final String message, final OnClickListener positiveListener) {
		showYesNoAlert(getString(SuiteAppMtto.getResourceId("label.information", "string")), 
					   message, 
					   getString(SuiteAppMtto.getResourceId("common.alert.yesno.positive.button", "string")), 
					   getString(SuiteAppMtto.getResourceId("common.alert.yesno.negative.button", "string")), 
					   positiveListener, 
					   null);
	}
	
	public void showYesNoAlertEspecialTitulo(final String errorCode, final String descripcionError, final OnClickListener positiveListener) {
		showYesNoAlertEspecial(getString(SuiteAppMtto.getResourceId("label.information", "string")), 
				 	   errorCode,
				 	   descripcionError,
					   getString(SuiteAppMtto.getResourceId("common.alert.yesno.positive_button", "string")), 
					   getString(SuiteAppMtto.getResourceId("common.alert.yesno.negative.button", "string")), 
					   positiveListener, 
					   null);
	}
	
	public void showYesNoAlert(final int message, final OnClickListener positiveListener) {
		showYesNoAlert(SuiteAppMtto.getResourceId("label.information", "string"), 
					   message, 
					   SuiteAppMtto.getResourceId("common.alert.yesno.positive.button", "string"), 
					   SuiteAppMtto.getResourceId("common.alert.yesno.negative.button", "string"), 
					   positiveListener, 
					   null);
	}
	
	public void showYesNoAlert(final int message, final OnClickListener positiveListener, final OnClickListener negativeListener) {
		showYesNoAlert(SuiteAppMtto.getResourceId("label.information", "string"), 
					   message, 
					   SuiteAppMtto.getResourceId("common.alert.yesno.positive.button", "string"), 
					   SuiteAppMtto.getResourceId("common.alert.yesno.negative.button", "string"), 
					   positiveListener, 
					   negativeListener);
	}
	
	public void showYesNoAlert(final String title, final String message, final OnClickListener positiveListener) {
		showYesNoAlert(title, 
					   message, 
					   getString(SuiteAppMtto.getResourceId("common.alert.yesno.positive.button", "string")), 
					   getString(SuiteAppMtto.getResourceId("common.alert.yesno.negative.button", "string")), 
					   positiveListener, 
					   null);
	}
	public void showYesNoAlert(final String message, final OnClickListener positiveListener, final OnClickListener negativeListener) {
		showYesNoAlert("", 
					   message, 
					   getString(SuiteAppMtto.getResourceId("common.alert.yesno.positive.button", "string")), 
					   getString(SuiteAppMtto.getResourceId("common.alert.yesno.negative.button", "string")), 
					   positiveListener, 
					   negativeListener);
	}
	
	public void showYesNoAlertEspecialTitulo(final String errorCode, final String descripcionError, final OnClickListener positiveListener, final OnClickListener negativeListener) {
		showYesNoAlertEspecial(getString(SuiteAppMtto.getResourceId("label.information", "string")), 
					   errorCode,
					   descripcionError,
					   getString(SuiteAppMtto.getResourceId("common.alert.yesno.positive.button", "string")), 
					   getString(SuiteAppMtto.getResourceId("common.alert.yesno.negative.button", "string")), 
					   positiveListener, 
					   negativeListener);
	}
	
	
	public void showYesNoAlert(final int title, final int message, final OnClickListener positiveListener) {
		showYesNoAlert(title, 
					   message, 
					   SuiteAppMtto.getResourceId("common.alert.yesno.positive.button", "string"), 
					   SuiteAppMtto.getResourceId("common.alert.yesno.negative.button", "string"), 
					   positiveListener, 
					   null);
	}
	
	
	
	public void showYesNoAlert(final String title, final String message, final String okText, final OnClickListener positiveListener) {
		showYesNoAlert(title, message, okText, getString(SuiteAppMtto.getResourceId("common.alert.yesno.negative.button", "string")), positiveListener, null);
	}
	
	public void showYesNoAlert(final int title, final int message, final int okText, final OnClickListener positiveListener) {
		showYesNoAlert(title, message, okText, SuiteAppMtto.getResourceId("common.alert.yesno.negative.button", "string"), positiveListener, null);
	}
	
	public void showYesNoAlert(final String title, final String message, final String okText, final String calcelText, final OnClickListener positiveListener) {
		showYesNoAlert(title, message, okText, calcelText, positiveListener, null);
	}
	
	public void showYesNoAlert(final int title, final int message, final int okText, final int calcelText, final OnClickListener positiveListener) {
		showYesNoAlert(title, message, okText, calcelText, positiveListener, null);
	}
	
	public void showYesNoAlert(final int title,
			   				   final int message,
			   				   final int okText,
			   				   final int calcelText,
			   				   final OnClickListener positiveListener,
			   				   final OnClickListener negativeListener) {
		showYesNoAlert(getString(title), getString(message), getString(okText), getString(calcelText), positiveListener, negativeListener);
	}
	
	public void showYesNoAlert(final String title,
							   final String message,
							   final String okText,
							   final String calcelText,
							   final OnClickListener positiveListener,
							   final OnClickListener negativeListener) {
		if(!habilitado)
			return;		
		habilitado = false;
    	if(message.length() > 0){
    		final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
    		alertDialogBuilder.setTitle(title);
    		alertDialogBuilder.setIcon(SuiteAppMtto.getResourceId("anicalertaaviso", "drawable"));
    		alertDialogBuilder.setMessage(message);
    		alertDialogBuilder.setPositiveButton(okText, positiveListener);
    		
    		if(null == negativeListener) {
    			alertDialogBuilder.setNegativeButton(calcelText, new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog, final int which) {
						habilitado = true;
						dialog.dismiss();
					}
				});
    		} else {
    			alertDialogBuilder.setNegativeButton(calcelText, negativeListener);
    		}
    		
    		alertDialogBuilder.setCancelable(false);
    		mAlertDialog = alertDialogBuilder.create();
    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(final DialogInterface dialog) {
					habilitado = true;
				}
			});
    		mAlertDialog.show();
	    	
    	}
    }
	
	
	
	
	public void showYesNoAlertEspecial(final String title, final String errorCode, final String descripcionError, final String okText,
			final String calcelText, final OnClickListener positiveListener,
			final OnClickListener negativeListener) {
		if (!habilitado)
			return;
		habilitado = false;
		if (descripcionError.length() > 0) {
			
			final AlertDialog.Builder builder = new AlertDialog.Builder(this);
    		builder.setTitle(title);
    		builder.setIcon(SuiteAppMtto.getResourceId("anicalertaaviso", "drawable"));    	
    	    final LayoutInflater inflater = getLayoutInflater();
    	    final View vista = inflater.inflate(SuiteAppMtto.getResourceId("mtto_layout_alert_codigo_error", "layout"), null);
    	    builder.setView(vista);
    	    builder.setPositiveButton(okText, positiveListener);
    	    
    	    if (null == negativeListener) {
    	    	builder.setNegativeButton(calcelText,
						new OnClickListener() {
							@Override
							public void onClick(final DialogInterface dialog,
									final int which) {
								habilitado = true;
								dialog.dismiss();
							}
						});
			} else {
				builder.setNegativeButton(calcelText,
						negativeListener);
			}
    	    
    	    final TextView text1 = (TextView) vista.findViewById(SuiteAppMtto.getResourceId("descripcionError", "id"));
    	    final TextView text2 = (TextView) vista.findViewById(SuiteAppMtto.getResourceId("codigoError", "id"));
    	    text1.setText(descripcionError);
    	    text2.setText(errorCode);
    	 
    	    builder.setCancelable(false);
    		
    	    mAlertDialog = builder.create();
    	    
    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(final DialogInterface dialog) {
					habilitado = true;
				}
			});
    		mAlertDialog.show();
			

		}
	}
	
    // #endregion	
	//private boolean habilitado = true;
	  protected boolean habilitado = true;

	public void setHabilitado(final boolean habilitado) {
		this.habilitado = habilitado;
	}
	
	public boolean isHabilitado() {
		return habilitado;
	}
}