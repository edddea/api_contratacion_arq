package mtto.suitebancomer.classes.gui.delegates;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.util.Log;
import android.view.View;

import bancomer.api.common.commons.Constants;
import mtto.com.bancomer.mbanking.BmovilApp;
import mtto.com.bancomer.mbanking.R;
import mtto.com.bancomer.mbanking.SofttokenApp;
import mtto.com.bancomer.mbanking.SuiteAppMtto;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import mtto.suitebancomer.classes.gui.controllers.MenuSuiteViewController;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.classes.common.PropertiesManager;

public class MenuSuiteDelegate extends BaseDelegate {
	
	public final static long MENU_SUITE_DELEGATE_ID = 0x3329474451002cd6L; 
	
	private MenuSuiteViewController menuSuiteViewController;
	
	private boolean isCallActive;
	private boolean bMovilSelected;
	
	public void setbMovilSelected(final boolean bMovilSelected) {
		this.bMovilSelected = bMovilSelected;
	}
	
	public boolean isbMovilSelected() {
		return bMovilSelected;
	}
	
	public MenuSuiteDelegate() {
	}
	
	public boolean isCallActive() {
		return isCallActive;
	}
	
	public void setCallActive(final boolean isCallActive) {
		this.isCallActive = isCallActive;
	}
	
	public boolean isDisconnected(){
		 final SuiteAppMtto suiteApp = SuiteAppMtto.getInstance();
		
		 final ConnectivityManager connectivity = (ConnectivityManager)
				 suiteApp.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
		 
         if (connectivity != null){
             final NetworkInfo[] info = connectivity.getAllNetworkInfo();
             if (info != null){
                 for (int i = 0; i < info.length; i++){
                     if (info[i].getState() == NetworkInfo.State.CONNECTED){
                         return false;
                     }
                 }
             }
         }
         return true;
	}
	
	public void startBmovilApp() {
		if (SuiteAppMtto.getInstance().getBmovilApplication() == null) {
			SuiteAppMtto.getInstance().startBmovilApp();
		}
	}
	
	public int getBmovilAppStatus(final SuiteAppMtto suiteApp) {
		return suiteApp.getBmovilApplication().getApplicationStatus();
	}
	
	public MenuSuiteViewController getMenuSuiteViewController() {
		return menuSuiteViewController;
	}

	public void setMenuSuiteViewController(final MenuSuiteViewController menuSuiteViewController) {
		this.menuSuiteViewController = menuSuiteViewController;
	}

	public void bmovilSelected() {
		
		//EA#10, RN8 y RN9: comprobar la conectividad a internet antes de acceder a la aplicaci��n
		if (this.isDisconnected()) {
			menuSuiteViewController.setButtonsDisabled(false);
			menuSuiteViewController.showInformationAlert(R.string.menuSuite_alert_disconnected);
			
		} else {
			startBmovilApp();
			// Contin���a el flujo normal del escenario principal
			// Modificacion P026 Suite
			if(existePendienteDescarga()){
				
				showActivacionSTEA12();
				
			} else {
				
				if(buscarBanderasBmovil()){
					
					showActivacionSTEP();
					
				} else {
					if(PropertiesManager.getCurrent().getBmovilActivated()) {
						showLogin();
					} else {
						consultaEstatusBmovil();
					}
				}
			}
		}
	}
	
	/**
	 * Devuelve si existe el archivo PendienteDescarga
	 * @return
	 */
	private boolean existePendienteDescarga(){
		boolean respuesta = false;
		final SofttokenActivationBackupManager manager = SofttokenActivationBackupManager
				.getCurrent();
		if (manager.existsABackup()) {
			respuesta = true;
		}
		return respuesta;
	}
	
	private void showActivacionSTEA12() {
		cambiarDeBMovilTokenMovil().showActivacionSTEA12();
	}

	/**
	 * Redirige al caso de uso ActivacionST escenario principal
	 */
	private void showActivacionSTEP() {
		cambiarDeBMovilTokenMovil().showPantallaIngresoDatos();
	}
	
	/*private void showActivacionST(){
		cambiarDeBMovilTokenMovil().showActivacionSTEP25();
	}*/
	
	private SofttokenViewsController cambiarDeBMovilTokenMovil() {
		final SofttokenViewsController viewsController = SuiteAppMtto.getInstance()
				.getSofttokenApplication().getSottokenViewsController();
		menuSuiteViewController.setParentViewsController(viewsController);
		viewsController.setCurrentActivityApp(menuSuiteViewController);

		return viewsController;
	}
	
	private boolean buscarBanderasBmovil(){
		final Session session = Session.getInstance(SuiteAppMtto.appContext);
	
		if(session.getUsername()==null || Constants.EMPTY_STRING.equals(session.getUsername())){
			PropertiesManager.getCurrent().setBmovilActivated(false);
		}
		return Boolean.TRUE.equals(session.loadBanderasBMovilAttribute(Constants.BANDERAS_CONTRATAR2x1));
	}
	
	private void consultaEstatusBmovil() {
		final BmovilApp bmovilApp = SuiteAppMtto.getInstance().getBmovilApplication();
		final BmovilViewsController bmovilViewsController = bmovilApp.getBmovilViewsController();
		bmovilViewsController.setCurrentActivityApp(menuSuiteViewController);
//		bmovilViewsController.showConsultaEstatusAplicacionDesactivada();
	}
	
	public void leerContratacion() {
		final int pendingStatus = Session.getInstance(SuiteAppMtto.appContext).getPendingStatus();
		
		if (pendingStatus > 0) {
		} else {
			showLogin();
		}
	}
	
	private void showLogin() {
        //Empty method
	}
	
	public void llamarLineaBancomer(final String numeroTel) {
		try {
			isCallActive = true;
	        final Intent callIntent = new Intent(Intent.ACTION_CALL);
	        callIntent.setData(Uri.parse(Constants.TEL_URI+numeroTel));
	        menuSuiteViewController.startActivity(callIntent);
	    } catch (ActivityNotFoundException e) {
	    	menuSuiteViewController.showErrorMessage(menuSuiteViewController.getString(R.string.menuSuite_callErrorMessage));
	    }
	}

	// #region Softtoken.
	public void softtokenSelected() {
		if(null == SuiteAppMtto.getInstance().getSofttokenApplication())
			SuiteAppMtto.getInstance().startSofttokenApp();
		
		final SofttokenViewsController viewsController = SuiteAppMtto.getInstance().getSofttokenApplication().getSottokenViewsController();
		if(SuiteAppMtto.getSofttokenStatus()) {
			menuSuiteViewController.restableceMenu();
			viewsController.setCurrentActivityApp(menuSuiteViewController);
			viewsController.showPantallaGeneraOTPST(false);
		} else {
			viewsController.showContratacionSotfttoken(menuSuiteViewController);
		}
	}
	
	public void leerContratacionST() {
        //Empty method
	}
	// #endregion
	
	public void onBackPressed() {		
		if(menuSuiteViewController.getContratacionSTViewController() != null &&
				  menuSuiteViewController.getContratacionSTViewController().getVisibility() == View.VISIBLE) {
			menuSuiteViewController.plegarOpcionST();
		} else {
			SuiteAppMtto.getInstance().cierraAplicacionSuite();
		}
	}
	
	
	/**
	 * Se selecciona la opci���n de men��� Token M���vil.
	 * @param sender la opci���n seleccionada
	 */
	public void onBtnContinuarclick(final View sender) {
		final SofttokenApp softtokenApp = SuiteAppMtto.getInstance().getSofttokenApplication();

		final SofttokenViewsController viewsController = softtokenApp.getSottokenViewsController();
		if (SuiteAppMtto.getSofttokenStatus()) {
			menuSuiteViewController.restableceMenu();
			viewsController.setCurrentActivityApp(menuSuiteViewController);
			viewsController.showPantallaGeneraOTPST(false);
		} else {

			//EA#10, RN8 y RN9: comprobar la conectividad a internet antes de acceder a la aplicaci��n
			if (this.isDisconnected()) {
				menuSuiteViewController.setButtonsDisabled(false);
				menuSuiteViewController.showInformationAlert(R.string.menuSuite_alert_disconnected);
				
			} else {
				// Contin���a el flujo normal del EA#2: paso 6
				if(existePendienteDescarga()) {
					showActivacionSTEA12();
				} else {
					showActivacionSTEP();
				}
			}
		}
	}
	
	/**
	 * Metodo para cargar datos implementacion P026 BConnect EA#9, EA#10, EA#11, EA#12,EA#13
	 */
	public void cargaTelSeedKeystore() {

		// Recoger sesion del contexto
		final Session session = Session.getInstance(SuiteAppMtto.appContext);
		// Integracion KeyChainAPI
		// Recoger KeyStoreManager
		final KeyStoreWrapper kswrapper = session.getKeyStoreWrapper();

		// Inicializa Telefono y seed
		String telefono = null;
		String seed = null;

		try {
			// Telefono e IUM de KeyStore
			//telefono = keySMan.getEntry(Constants.USERNAME);
			//seed = keySMan.getEntry(Constants.SEED);
			telefono = kswrapper.getUserName(); 
			seed = kswrapper.getSeed();
			//-----
			
//			if(Server.ALLOW_LOG) Log.i("Key", "BConnect MenuSuiteDelegate Username: " + telefono);
//			if(Server.ALLOW_LOG) Log.i("Key", "BConnect MenuSuiteDelegate Seed: " + seed);

		} catch (Exception e1) {
			if(Server.ALLOW_LOG) {
                Log.e("MenuSuiteDelegate", e1.getMessage());
            }
		}
		
		// Comprobar Bmovil en estatusAplicaciones
		if (PropertiesManager.getCurrent().getBmovilActivated()) {

			// Si bmovil activado, validar telefono e IUM en KeyStore
			if (!Tools.validaSeed(seed) || !Tools.validaTelefono(telefono)) {

				
				// Si datos no validos, comprueba en archivo datosBmovil
				final DatosBmovilFileManager datosBmovil = DatosBmovilFileManager
						.getCurrent();
//				if(Server.ALLOW_LOG) Log.i("BConnect", "datosBmovil " + datosBmovil);
				seed = datosBmovil.getSeedStr();
				telefono = datosBmovil.getLogin();
//				if(Server.ALLOW_LOG) Log.i("DatosBmovil", "seed " + seed);
//				if(Server.ALLOW_LOG) Log.i("DatosBmovil", "telefono " + telefono);
				// Validar telefono y seed
				if (Tools.validaSeed(seed) && Tools.validaTelefono(telefono)) {
					// EA#10
					try {
						// Copia telefono y seed a keyStore

//						if(Server.ALLOW_LOG) Log.i("Key",
//								"BConnect MenuSuiteDelegate SET Username: "
//										+ telefono);
//						if(Server.ALLOW_LOG) Log.i("Key", "BConnect MenuSuiteDelegate SET Seed: "+ seed);
						//Integracion KeyChainAPI
						kswrapper.setUserName( telefono); //setEntry(Constants.USERNAME, telefono);
						kswrapper.setSeed(seed); //setEntry(Constants.SEED, seed);
						kswrapper.storeValueForKey(Constants.CENTRO, Constants.BMOVIL); //setEntry(Constants.CENTRO, Constants.BMOVIL);
					
					} catch (Exception e) {
                        if(Server.ALLOW_LOG) {
                            Log.e("MenuSuiteDelegate", e.getMessage());
                        }
					}

					// No borrar ambos datos de datosBmovil EA310 Paso 5
					//datosBmovil.setSeed("");
					//datosBmovil.setLogin("");

				} else {
					// Si no se valida, cambia a bmovil = false EA#11
					PropertiesManager.getCurrent().setBmovilActivated(false);
				}
			}
		} else {
			// Si Bmovil desactivado o no existe estatusAplicaciones
			// Validar telefono y seed EA#12
			if (Tools.validaSeed(seed) && Tools.validaTelefono(telefono)) {
				// Si los datos son validos borra datos en keychain
				try {
					//Integración KeyChainAPI
					kswrapper.setUserName(" "); 	//Entry(Constants.USERNAME, " ");;
					kswrapper.setSeed(" "); 			//Entry(Constants.SEED, " ");
					kswrapper.storeValueForKey(Constants.CENTRO, " ");//Entry(Constants.CENTRO, " ");
					
				} catch (Exception e) {
                    if(Server.ALLOW_LOG) {
                        Log.e("MenuSuiteDelegate", e.getMessage());
                    }
				}
			}
		}
	}
}
