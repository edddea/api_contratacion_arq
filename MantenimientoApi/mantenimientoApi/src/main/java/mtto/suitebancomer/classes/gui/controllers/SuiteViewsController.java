package mtto.suitebancomer.classes.gui.controllers;

import android.app.AlertDialog;
import mtto.com.bancomer.mbanking.R;
import mtto.com.bancomer.mbanking.SuiteAppMtto;
import mtto.suitebancomer.classes.gui.delegates.MenuSuiteDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;

public class SuiteViewsController extends BaseViewsController {

	public void showMenuSuite(final boolean inverted) {
		showMenuSuite(inverted, null);
	}
	
	public void showMenuSuite(final boolean inverted, final String[] extras) {
		MenuSuiteDelegate suiteDelegate = (MenuSuiteDelegate)getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
		if (suiteDelegate == null) {
			suiteDelegate = new MenuSuiteDelegate();
			addDelegateToHashMap(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID, suiteDelegate);
		}
		final Session session = Session.getInstance(SuiteAppMtto.appContext);
		final boolean activado = session.isApplicationActivated();
		suiteDelegate.setbMovilSelected(extras != null && extras[0].equalsIgnoreCase("bmovilselected"));
		
		SuiteAppMtto.getCallback().returnToActivity(SuiteAppMtto.appContext, SuiteAppMtto.getInstance().getActivityToReturn(),true);
    }
	
	public void showContactanos() {
		final AlertDialog.Builder contactDialog = new AlertDialog.Builder(currentViewControllerApp);
		contactDialog.setTitle(currentViewControllerApp.getString(R.string.menuSuite_callTitle));
		
		if (Tools.hasPhoneAbility(currentViewControllerApp)) {
			contactDialog.setMessage(currentViewControllerApp.getString(R.string.menuSuite_callMessage));
			contactDialog.setPositiveButton(currentViewControllerApp.getString(R.string.menuSuite_callLocalNumberFormatted), (MenuSuiteViewController)currentViewControllerApp);
			contactDialog.setNeutralButton(currentViewControllerApp.getString(R.string.menuSuite_callAwayNumberFormatted), (MenuSuiteViewController)currentViewControllerApp);
			contactDialog.setNegativeButton(currentViewControllerApp.getString(R.string.common_cancel), (MenuSuiteViewController)currentViewControllerApp);
		} else {
			contactDialog.setMessage(currentViewControllerApp.getString(R.string.menuSuite_noCallMessage));
			contactDialog.setNeutralButton(currentViewControllerApp.getString(R.string.common_accept), null);
		}
		
		contactDialog.setCancelable(false);
		contactDialog.show();
	}
	
	public void showContratacionPendiente() {
        //Empty method
	}
	
}
