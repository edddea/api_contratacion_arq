package mtto.com.bancomer.mbanking;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import com.bancomer.base.callback.CallBackBConnect;
import mtto.suitebancomer.classes.gui.controllers.MenuSuiteViewController;
import mtto.suitebancomer.classes.gui.controllers.SuiteViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

public class SuiteAppMtto extends com.bancomer.base.SuiteApp {
	public static boolean isSubAppRunning;
	public static Context appContext;
	private static SuiteAppMtto me;
	private SuiteViewsController suiteViewsController;
	private BaseViewControllerCommons viewsControllerCommons;

	public void onCreate(final Context contexto) {
		super.onCreate(contexto);
		suiteViewsController = new SuiteViewsController();
		isSubAppRunning = false;
		
		me = this;
		appContext = contexto;
		this.startBmovilApp();
	};
	
	public static int getResourceId(final String nombre, final String tipo){
		return appContext.getResources().getIdentifier(nombre,tipo, appContext.getPackageName());
	}
	
	public static int getResourceId(final String nombre, final String tipo, final View vista){
		return vista.getResources().getIdentifier(nombre,tipo, appContext.getPackageName());
	}
	
	public void cierraAplicacionSuite() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public SuiteViewsController getSuiteViewsController() {
		return suiteViewsController;
	}

	public static SuiteAppMtto getInstance() {
		return me;
	}
	
	// #region BmovilApp
	private BmovilApp bmovilApplication;
	
	public BmovilApp getBmovilApplication() {
		if(bmovilApplication == null)
			bmovilApplication = new BmovilApp(this);
		return bmovilApplication;
	}
	
	public static boolean getBmovilStatus() {
		return PropertiesManager.getCurrent().getBmovilActivated();
	}
	
	public void startBmovilApp() {
		bmovilApplication = new BmovilApp(this);
		isSubAppRunning = true;
	}
	
	public void cierraAplicacionBmovil() {
		bmovilApplication.cierraAplicacion();
		bmovilApplication = null;
		isSubAppRunning = false;		
		//reiniciaAplicacionBmovil();
	}
	
	public void reiniciaAplicacionBmovil() {
		if(bmovilApplication == null)
			bmovilApplication = new BmovilApp(this);
		bmovilApplication.reiniciaAplicacion();
		isSubAppRunning = true;
	}
	// #endregion
	
	// #region SofttokenApp
	private SofttokenApp softtokenApp;
	
	public SofttokenApp getSofttokenApplication() {
		if(softtokenApp == null)
			startSofttokenApp();
		return softtokenApp;
	}
	
	public static boolean getSofttokenStatus() {
		return PropertiesManager.getCurrent().getSofttokenActivated();
	}
	
	public void startSofttokenApp() {
		softtokenApp = new SofttokenApp(this);
		isSubAppRunning = true;
	}
	
	public void closeSofttokenApp() {
		if(null == softtokenApp)
			return;
		softtokenApp.cierraAplicacion();
		softtokenApp = null;
		isSubAppRunning = false;
		
		if(suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController)
			((MenuSuiteViewController)suiteViewsController.getCurrentViewControllerApp()).setShouldHideLogin(true);
		
		suiteViewsController.showMenuSuite(true);
	}
	// #endregion
	
	
	public void closeBmovilAppSession(){
		
		if(suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController) {
            ((MenuSuiteViewController) suiteViewsController.getCurrentViewControllerApp()).setShouldHideLogin(true);
        }
		suiteViewsController.showMenuSuite(true);
		
		
		final SuiteAppMtto suiteApp = SuiteAppMtto.getInstance();
		final Session session = Session.getInstance(suiteApp.getApplicationContext());

		if(session.getValidity() == Session.VALID_STATUS){
			session.setValidity(Session.INVALID_STATUS);
		}
		
		bmovilApplication.closeBmovilAppSession(suiteViewsController.getCurrentViewControllerApp());;
		
	}


	private static CallBackBConnect callback;

	private  Activity activityToReturn;

	public static CallBackBConnect getCallback() {
		return callback;
	}

	public void setCallback(final CallBackBConnect callback) {
		this.callback = callback;
	}

	public Activity getActivityToReturn() {
		return activityToReturn;
	}

	public void setActivityToReturn(final Activity activityToReturn) {
		this.activityToReturn = activityToReturn;
	}



}
