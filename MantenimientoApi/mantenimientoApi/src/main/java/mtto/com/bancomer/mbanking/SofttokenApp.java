package mtto.com.bancomer.mbanking;

import android.content.Context;
import android.util.Log;

import net.otpmt.mtoken.MTokenCore;
import net.otpmt.mtoken.db.BasicFileProvider;
import net.otpmt.mtoken.db.DataProvider;
import net.otpmt.mtoken.db.FileDataProvider;
import net.otpmt.mtoken.db.FileProvider;
import net.otpmt.mtoken.db.ResourceOfflineDataProvider;
import net.otpmt.mtoken.net.SEHTTPConnection;
import net.otpmt.mtoken.util.Util;

import suitebancomer.aplicaciones.bmovil.classes.io.token.Server;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenSession;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;

public class SofttokenApp extends BaseSubapplication {

	private MTokenCore core;

	private final String DATABASE_NAME = "mtokendb2";

	public SofttokenApp(final SuiteAppMtto suiteApp) {
		super(suiteApp);
		viewsController = new SofttokenViewsController();
		SofttokenSession.getCurrent().loadSottokenRecordStore();
	}

	public SofttokenViewsController getSottokenViewsController() {
		return (SofttokenViewsController)viewsController;
	}

	public synchronized MTokenCore getCore() throws IllegalStateException {

		if (core == null) {
			try {
				DataProvider dataProvider;

				// Sets the database name
				/*
				// This is the SQLite based data provider.
				// This version will not be used because there is a bug in the 
				// SQLite library that may lead to random data loss. This is valid
				// in Android 2.3 and earlier.
				dataProvider = new MTSQLiteDataProvider(
						MBankingApp.MBankingApp, 
						DATABASE_NAME,
						new ResourceOfflineDataProvider(MBankingApp.MBankingApp, R.raw.e));
				 */
				// This is the file based data provider,

				dataProvider = new FileDataProvider(new BasicFileProvider(suiteApp.appContext.getDir(DATABASE_NAME, Context.MODE_PRIVATE)),
						new ResourceOfflineDataProvider(suiteApp.appContext, R.raw.e));

				/*ACTUALIZACIÓN A API 6.0.8*/
				// UPGRADE to 6.0.7 (6.0.8 edition)
				// Since the original code was:
				//    Options options = new Options();
				//    options.setPDFilter(Options.PDFILTER_WIFI);
				// I must keep those parameters on the upgrade to 6.0.7
				MTokenCore.Options options = new MTokenCore.Options();
				options.setPDFilter(MTokenCore.Options.PDFILTER_WIFI);
				if (MTokenCore.upgradeTo_6_0_7(dataProvider)) {
					if (Server.ALLOW_LOG) Log.i(getClass().getSimpleName(),"Upgraded to 6.0.7");
				}

				// From 6.0.8 onward, a few new filters must be added in order to mitigate problems
				// with PData on newer devices.
				options = new MTokenCore.Options();
				options.setPDFilter(MTokenCore.Options.PDFILTER_RECOMMENDED_201507);


				// Creates the MToken core
				core =  new MTokenCore(SuiteAppMtto.appContext,Util.getParticularData(SuiteAppMtto.appContext), dataProvider);

				// Add the on-line configuration.
				core.setConnection(new SEHTTPConnection(SuiteAppMtto.appContext.getString(R.string.downloadURL)));

				// Add the off-line configuration.
				core.setOfflineConfiguration(SuiteAppMtto.appContext.getString(R.string.offlineSeed));
				if(Server.ALLOW_LOG) Log.i(getClass().getSimpleName(), "Core created.");
			} catch (Exception e) {
				if(Server.ALLOW_LOG) Log.e(getClass().getSimpleName(), "Unable to initialize the core.", e);
				core = null;
				throw new IllegalStateException("Unable to initialize the core.", e);
			}
		}

		return core;

	}

	public void setCore(final MTokenCore core) {
		this.core = core;
	}

	public void resetApplicationData() throws Exception {
		// Dispose the core
		if(core != null) core.dispose();
		core = null;

		// Delete the existing database.
		//MBankingApp.MBankingApp.deleteDatabase(DATABASE_NAME);
		final FileProvider fileProvider = new BasicFileProvider(
				SuiteAppMtto.appContext.getDir(DATABASE_NAME, Context.MODE_PRIVATE));
		fileProvider.reset();
	}

	public synchronized boolean isLogged() {
		if (core != null) {
			return core.isLogged();
		} else {
			return false;
		}
	}
}
