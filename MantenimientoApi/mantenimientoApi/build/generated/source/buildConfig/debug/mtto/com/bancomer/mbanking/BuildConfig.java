/**
 * Automatically generated file. DO NOT MODIFY
 */
package mtto.com.bancomer.mbanking;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "mtto.com.bancomer.mbanking";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 924;
  public static final String VERSION_NAME = "";
}
