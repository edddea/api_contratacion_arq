package com.bancomer.mbanking.apipush.exeption;

/**
 * Created by evaltierrah on 21/01/16.
 */
public class PushException extends Exception {

    /**
     * Defautl Constructor
     */
    public PushException() {
        // Empty constructor
    }

    /**
     * Consutructor que recibe el mensaje
     *
     * @param mensaje Mensaje a enviar
     */
    public PushException(final String mensaje) {
        super(mensaje);
    }

    /**
     * Constructor que recibe la cause
     *
     * @param cause Causa de la excepcion
     */
    public PushException(final Throwable cause) {
        super(cause);
    }

    /**
     * Constructor que recibe la causa y el mensaje
     *
     * @param mensaje Mensaje a enviar
     * @param cause   Cause de la excepcion
     */
    public PushException(final String mensaje, final Throwable cause) {
        super(mensaje, cause);
    }

}
