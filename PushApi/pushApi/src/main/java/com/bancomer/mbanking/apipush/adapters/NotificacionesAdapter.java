package com.bancomer.mbanking.apipush.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bancomer.mbanking.apipush.R;
import com.bancomer.mbanking.apipush.UtilsGCM;
import com.bancomer.mbanking.apipush.models.NotificacionesPush;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by jinclan on 11/11/15.
 */
public class NotificacionesAdapter extends BaseAdapter {
    protected Activity activity;
    protected List<NotificacionesPush> items;

    public NotificacionesAdapter(final Activity activity, final List<NotificacionesPush> items) {
        this.activity = activity;
        this.items = items;
    }

    public int getCount() {
        return items.size();
    }

    public Object getItem(final int position) {
        return items.get(position);
    }


    public long getItemId(final int position) {
        return position;
    }

    public View getView(final int position, final View convertView, final ViewGroup parent) {
        View vi=convertView;

        if(convertView == null) {
            final LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vi = inflater.inflate(R.layout.notifications_custom_adapter, null);
        }

        final NotificacionesPush item = items.get(position);

        final SimpleDateFormat formatDate= new SimpleDateFormat(
                "EEE, MMM dd", new Locale("es_ES"));
        final SimpleDateFormat formatHour = new SimpleDateFormat(
                "hh:mm", Locale.getDefault());

        final TextView fecha = (TextView) vi.findViewById(R.id.fechaPush);
        fecha.setText(formatDate.format(item.getFecha()).toUpperCase());
        final TextView hora = (TextView) vi.findViewById(R.id.horaPush);
        hora.setText(formatHour.format(item.getFecha()).toUpperCase());
        final TextView menuTexto = (TextView) vi.findViewById(R.id.textoNotificacion);
        menuTexto .setText(item.getMensaje());
        final ImageView icon = (ImageView) vi.findViewById(R.id.imagenNotificacion);
        if(item.getTitulo().equalsIgnoreCase(UtilsGCM.pushCargo)){
            icon.setImageResource(R.mipmap.an_ic_cargos);
        }else{
            if(item.getTitulo().equalsIgnoreCase(UtilsGCM.pushAbono)){
                icon.setImageResource(R.mipmap.an_ic_abonos);
            }else{
                icon.setImageResource(R.mipmap.an_ic_todos);
            }
        }



        return vi;
    }

    @Override
    public void notifyDataSetChanged(){
        super.notifyDataSetChanged();
        Log.d("NotificacionesAdapter", "notifyDataSetChanged");
    }

    public List<NotificacionesPush> getItems() {
        return items;
    }

    public void setItems(final List<NotificacionesPush> menuItems) {
        this.items = menuItems;
    }

}
