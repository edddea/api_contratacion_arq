package com.bancomer.mbanking.apipush;

import android.content.Context;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

/**
 * Created by jinclan on 14/09/15.
 */
public final class UtilsGCM {


    public static String gcmTitulo= "TODOS";
    public static String pushCargo= "ALERTA CARGO";
    public static String pushAbono= "ALERTA ABONO";

    private UtilsGCM(){}

    public static boolean checkPlayServices(final Context context) {

        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int resultCode = apiAvailability.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            /*if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(context, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                //finish();
            }*/
            return false;
        }
        return true;
    }

    /**
     * Determines if a object is null
     *
     * @param value
     * @return true if the value is null
     */
    public static boolean isNull(final Object value) {
        return value == null;
    }

}
