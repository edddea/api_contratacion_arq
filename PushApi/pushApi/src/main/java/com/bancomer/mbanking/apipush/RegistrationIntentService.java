/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bancomer.mbanking.apipush;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.bancomer.mbanking.apipush.exeption.PushException;
import com.bancomer.mbanking.apipush.models.NotificacionesEstatus;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.Locale;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.PushSender;

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";
    private static final String[] TOPICS = {"global"};

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(final Intent intent) {
        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        try {
            // [START register_for_gcm]
            // Obtiene el valor del UUID Push
            // are local.
            // [START get_token]
            final InstanceID instanceID = InstanceID.getInstance(this);
            final String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            // [END get_token]
            Log.i(TAG, "GCM Registration Token: " + token);

            // TODO: Enviado hacia API Server
            sendRegistrationToServer(token);

            // Subscribe to topic channels
            subscribeTopics(token);

            // Bandera de identificacion para enviar o no enviar por Api Server
            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();
            // [END register_for_gcm]
        } catch (IOException e) {
            Log.d(TAG, "Error GCM", e);
            sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false).apply();
        }
        // Si se necesita notificar a la vista .
        final Intent registrationComplete = new Intent(QuickstartPreferences.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }

    /**
     * Envia el UUID Push al servidor via Api Server .
     *
     * @param token UUID Token .
     */
    private void sendRegistrationToServer(final String token) {
        // Realiza el registro del ID Push en el servidor de Bancomer Movil
       final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        //Lee las preferencias, si es el mismo token no hace nada
        final String tokenSender=sharedPreferences.getString(QuickstartPreferences.TOKEN_SENDER,null);
        if(token.equals(tokenSender)){
            //Si es el mismo no lo enviara
            /**
             * TODO HAY QUE QUITAR ESTE COMENTARIO
             * */
            return;
        }
        final File f=this.getBaseContext().getFilesDir();
        final Context c= this;
        final Thread thread = new Thread(new Runnable() {
            public void run() {
                try {
                    //ServerResponse responseNew=null;
                    final Hashtable<String,String> params= new Hashtable<String, String>();
                    final ApiPushData apiPushData= ApiPushData.getInstance();
                    params.put("numeroCelular",apiPushData.getCelular());
                    params.put("cliente",apiPushData.getCliente());
                    params.put("clientem",apiPushData.getCliente());
                    params.put("alias"," ");
                    params.put("importeAbono","0");
                    params.put("importeCargo","0");
                    //Checar si siempre son asi
                    params.put("dispositivoEnvioFav","130");
                    params.put("infDis","002");
                    //Checar si siempre son asi
                    //Se parte el token en cada 140 caracteres
                    String part1=null;String part2=null;String part3=null;
                    if(token != null){
                        int pushLength=token.length();
                        part1 = token.substring(0,140);
                        if(pushLength <= 280){
                            part2 = token.substring(140,pushLength);
                            part3 = "";
                        }else{
                            part2 = token.substring(140,280);
                            part3 = token.substring(280,pushLength);
                        }

                    }
                    params.put("correoPart1",part1);
                    params.put("correoPart2",part2);
                    params.put("correoPart3",part3);
                    params.put("accion","A");
                    //Llamando a API Server
                    final ConnectionFactory cf= new ConnectionFactory(Server.ID_SENDER_OPERATION, params, true, new PushSender(), ApiConstants.isJsonValueCode.NONE);
                    final IResponseService resultado=cf.processConnectionWithParams();
                    final ServerResponse responseNew=cf.parserConnection(resultado, new PushSender());
                    if(responseNew==null){
                        //No se pudo enviar nada
                        sharedPreferences.edit().remove(QuickstartPreferences.TOKEN_SENDER).apply();
                        sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false).apply();
                    }else{
                        //Guardar el archivo
                        //Verificar lo que respondera el Server
                        final ManageFile m= new ManageFile(c);
                        final Date date = new Date();
                        final SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMddhhmmss", Locale.getDefault());
                        final NotificacionesEstatus notificacionesEstatus= new NotificacionesEstatus("ACTIVADO", dt1.format(date),token, "130");
                       m.generateFile(f, QuickstartPreferences.PUSH_CONFIGURATION, notificacionesEstatus);
//                        Object o=m.readFile(f, "ARCHIVO_PUSH");
                        sharedPreferences.edit().putString(QuickstartPreferences.TOKEN_SENDER, token).apply();
                        sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, true).apply();
                    }
                } catch (Throwable t) {//se cacha cualqueir exception probeniente del server
                    t.printStackTrace();
                    sharedPreferences.edit().remove(QuickstartPreferences.TOKEN_SENDER).apply();
                    sharedPreferences.edit().putBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false).apply();
                }
            }

        });

        // Starts the worker thread to perform network operation
        thread.start();



    }

    /**
     * Si la comunicacoin se realizara via topis, lo suscribe
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    // [START subscribe_topics]
    private void subscribeTopics(final String token) throws IOException {
        final GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (final String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
    // [END subscribe_topics]

}
