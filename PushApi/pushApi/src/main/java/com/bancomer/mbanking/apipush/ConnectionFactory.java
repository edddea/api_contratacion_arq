package com.bancomer.mbanking.apipush;

import com.bancomer.base.SuiteApp;
import com.bancomer.mbanking.apipush.exeption.PushException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Hashtable;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;
import suitebancomer.aplicaciones.commservice.service.ICommService;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Parser;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

public class ConnectionFactory {

	private final int operationId;
	private final Hashtable<String, ?> params;
	private final Object responseObject;
	private ParametersTO parameters;
	private final boolean isJson;
	private final ApiConstants.isJsonValueCode isJsonValueCode;

	public ConnectionFactory(final int operationId, final Hashtable<String, ?> params,
							 final boolean isJson, final Object responseObject, final ApiConstants.isJsonValueCode isJsonValueCode) {
		super();
		this.operationId = operationId;
		this.params = params;
		this.responseObject = responseObject;
		this.isJson = isJson;
		this.isJsonValueCode = isJsonValueCode;
	}

	public IResponseService processConnectionWithParams() throws PushException {

		final Integer opId = Integer.valueOf(operationId);
		parameters = new ParametersTO();
		parameters.setSimulation(Server.SIMULATION);
		if (!Server.SIMULATION) {
			parameters.setProduction(!Server.DEVELOPMENT);
			parameters.setDevelopment(Server.DEVELOPMENT);
		}
		parameters.setOperationId(opId.intValue());
		parameters.setParameters(params.clone());
		parameters.setJson(isJson);
		parameters.setIsJsonValue(this.isJsonValueCode);
		IResponseService resultado = new ResponseServiceImpl();
		final ICommService serverproxy = new CommServiceProxy(SuiteApp.appContext);
		try {
			resultado = serverproxy.request(parameters,
					responseObject == null ? new Object().getClass()
							: responseObject.getClass());
			// ConsultaEstatusMantenimientoData
			// estatusM=(ConsultaEstatusMantenimientoData)resultado.getObjResponse();
		} catch (UnsupportedEncodingException e) {
			throw new PushException(e);
		} catch (ClientProtocolException e) {
			throw new PushException(e);
		} catch (IllegalStateException e) {
			throw new PushException(e);
		} catch (IOException e) {
			throw new PushException(e);
		} catch (JSONException e) {
			throw new PushException(e);
		}

		return resultado;

	}

    /**
     * Metodo que se ejecuta cuando la peticion es de tipo JSON
     * @param handler Handler
     * @param handlerP Handler
     * @param resultado Resultado
     * @return Regresa un objeto de tipo ServerResponse
     * @throws PushException Excepcion en caso de algun error
     */
    private ServerResponse executeJson(final ParsingHandler handler, final ParsingHandler handlerP,
                                       final IResponseService resultado) throws PushException {
        if (handler != null) {
            final ParserJSON parser = new ParserJSON(
                    resultado.getResponseString());
            try {
                handlerP.process(parser);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                throw new PushException(e);
            } catch (ParsingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                throw new PushException(e);
            }
        }
        return new ServerResponse(0,
                "0", resultado.getMessageText(), handlerP);
    }

    private ParsingHandler createHanlderP(final ParsingHandler handlerP, final ParsingHandler handler) throws PushException {
        if(handlerP==null){
            final Class<?> clazz = handler.getClass();
            final Constructor<?> ctor;
            try {
                ctor = clazz.getConstructor();
                return (ParsingHandler) ctor.newInstance();
            } catch (NoSuchMethodException e) {
                throw new PushException(e);
            } catch (InvocationTargetException e) {
                throw new PushException(e);
            } catch (InstantiationException e) {
                throw new PushException(e);
            } catch (IllegalAccessException e) {
                throw new PushException(e);
            }
        }
        return handlerP;
    }

	public ServerResponse parserConnection(final IResponseService resultado,
										   final ParsingHandler handler) throws PushException {

			//Verificar si es de tipo Imagen
            final Hashtable<String,String>  params2= (Hashtable<String, String>) parameters.getParameters();
            if(params2.get(ApiConstants.IMAGEN_BMOVIL) != null){
                return new ServerResponse(ServerResponse.OPERATION_SUCCESSFUL, null, null, resultado.getObjResponse());
            }
            ServerResponse response = null;
            ParsingHandler handlerP = null;
			if(resultado.getObjResponse() instanceof ParsingHandler){
				handlerP= (ParsingHandler) resultado.getObjResponse();
			}
			if (parameters.isJson()) {
				response = executeJson(handler, handlerP, resultado);
			} else {
				// Que pasa si no es JSON la respuesta
				Reader reader = null;
				try {
					if (handler == null) {
                        response= new ServerResponse(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),null);
					}else{
                        reader = new StringReader(resultado.getResponseString());
                        final Parser parser = new Parser(reader);
                        handlerP = createHanlderP(handlerP, handler);
                        response= new ServerResponse(handlerP);
                        response.process(parser);
					}
                }catch(IOException e){
                    throw new PushException(e);
                }catch(ParsingException e){
					throw new PushException(e);
					
				}finally {
					if (reader != null) {
						try {
							reader.close();
						} catch (IOException ignored) {
							throw new PushException(ignored);
							
						}
					}
				}
			}


		
		return response;
	}


}
