package com.bancomer.mbanking.apipush;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;

import com.bancomer.mbanking.apipush.adapters.NotificacionesAdapter;
import com.bancomer.mbanking.apipush.models.NotificacionesPush;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;


public class MainActivity extends Activity {

    private ListView lista;
    List<NotificacionesPush> notificaciones;
    private ImageView todos,cargos,abonos;
    //AMZ
    public ArrayList<String> estados = new ArrayList<String>();
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        MyGcmListenerService.notifications=new ArrayList<NotificacionesPush>();
        //final LinearLayout vista = (LinearLayout) findViewById(R.id.menu_notifications_layout);
        lista = (ListView) findViewById(R.id.listaNotificaciones);
        todos = (ImageView) findViewById(R.id.todosIcon);
        cargos = (ImageView) findViewById(R.id.cargosIcon);
        abonos = (ImageView) findViewById(R.id.abonosIcon);
        todos.setImageResource(R.mipmap.an_ic_todos);
        final View empty = findViewById(R.id.empty);
        lista.setEmptyView(empty);
        notificaciones= readFile();
        final NotificacionesAdapter notificacionesAdapter= new NotificacionesAdapter(this,notificaciones);
        //ArrayAdapter arrayAdapter = new ArrayAdapter(this,R.layout.notifications_adapter, R.id.notificaion, convertString(notificaciones));
        lista.setAdapter(notificacionesAdapter);
        TrackingHelper.trackState("notificaciones", estados);
    }

    @Override
    public void onBackPressed() {
        TrackingHelper.touchAtrasState();
        super.onBackPressed();
    }

    private List<NotificacionesPush> readFile(){
        final ManageFile m= new ManageFile(this);
        final File f=this.getBaseContext().getFilesDir();
        final List<NotificacionesPush> pushed= (List<NotificacionesPush>)m.readFile(f, QuickstartPreferences.PUSH_NOTIFICATIONS);
        return isSameDayList(pushed);
    }

    private List<NotificacionesPush> isSameDayList(final List<NotificacionesPush> push){
        final List<NotificacionesPush> pushed= new ArrayList<NotificacionesPush>();
        if(push==null){
            return pushed;
        }else{
            final Date date2= new Date();
            Date date1= null;
            final SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
            for(final NotificacionesPush p:push){
                date1= p.getFecha();
                if(fmt.format(date1).equals(fmt.format(date2))){
                    pushed.add(p);
                }
            }
            //return push;
        }
         return pushed;
    }

    private Boolean checkNotification(final List<NotificacionesPush> notificaciones) {
        if(notificaciones == null || notificaciones.isEmpty()){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }

    public void changeSelection (final View v) {
        if(checkNotification(notificaciones)){
            notificaciones= readFile();
        }
        final int i = v.getId();
        final List<NotificacionesPush> tmp= new ArrayList<NotificacionesPush>();
        NotificacionesAdapter notificacionesAdapter=null;
        if (i == R.id.todosPush) {
            notificacionesAdapter=new NotificacionesAdapter(this,notificaciones);
            todos.setImageResource(R.mipmap.an_ic_todos);
            abonos.setImageResource(R.mipmap.an_ic_abonosoff);
            cargos.setImageResource(R.mipmap.an_ic_cargosoff);
        } else if (i == R.id.abonosPush) {
            todos.setImageResource(R.mipmap.an_ic_todosoff);
            abonos.setImageResource(R.mipmap.an_ic_abonos);
            cargos.setImageResource(R.mipmap.an_ic_cargosoff);
                for(final NotificacionesPush p: notificaciones){
                    if(p.getTitulo().equalsIgnoreCase(UtilsGCM.pushAbono)){
                        tmp.add(p);
                    }
                }
            notificacionesAdapter=new NotificacionesAdapter(this,tmp);
        } else if (i == R.id.cargosPush) {
            todos.setImageResource(R.mipmap.an_ic_todosoff);
            abonos.setImageResource(R.mipmap.an_ic_abonosoff);
            cargos.setImageResource(R.mipmap.an_ic_cargos);
            for(final NotificacionesPush p: notificaciones){
                if(p.getTitulo().equalsIgnoreCase(UtilsGCM.pushCargo)){
                    tmp.add(p);
                }
            }
            notificacionesAdapter=new NotificacionesAdapter(this,tmp);
        }
        lista.setAdapter(notificacionesAdapter);
        notificacionesAdapter.notifyDataSetChanged();
    }


}

