package com.bancomer.mbanking.apipush;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


import com.bancomer.mbanking.apipush.models.NotificacionesPush;
import com.google.android.gms.gcm.GcmListenerService;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    protected static List<NotificacionesPush> notifications= new ArrayList<NotificacionesPush>();
    final static String GROUP_KEY_EMAILS = "group_bancomer_notifications";
    /**
     * Se invocara cuando un nuevo mensaje llegue
     *
     * @param from SenderID del sender .
     * @param data Data bundle contiene la informacion enviada
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(final String from, final Bundle data) {
        final String message = data.getString("message");
        final String title = data.getString("title");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

        /*if (from.startsWith("/topics/")) {
            // Topico en mensaje
        } else {
            // Mensaje Normal
        }*/
        //Se guarda la notificacion
        saveNotification(title, message);
        // Se presenta la notificacion de que llego un mensaje
        sendNotification(title, message);
    }
    // [END receive_message]

    /**
     * Crea la vista de la notificacion
     *
     * @param message GCM recibido
     */
    private void sendNotification(final String title,final String message) {
        //Este es a donde enviara cuando se le pique a la notificacion
        final Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        if(notifications.size() <100)
        notifications.add(new NotificacionesPush(message,title,new Date()));
        final Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE+ "://" + getPackageName() + "/raw/tono_bancomer");

                //RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        if(notifications.size() ==1){

            final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_stat_ic_notification)
                    .setContentTitle(title==null?"BANCOMER":title)
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
            final NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

        }else{
            final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_stat_ic_notification)
                    .setContentTitle(message)
                    .setContentText(notifications.size()+"  nuevas notificaciones")
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            final NotificationCompat.InboxStyle inboxStyle =
                    new NotificationCompat.InboxStyle();
            inboxStyle.setBigContentTitle("Event tracker details:");
// Moves events into the expanded layout
            for (int i=0; i < notifications.size(); i++) {
                inboxStyle.addLine(notifications.get(i).getMensaje().substring(0,40));
            }
// Moves the expanded layout object into the notification object.
            mBuilder.setStyle(inboxStyle);
            final NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, mBuilder.build());

        }

    }

    private void saveNotification(final String title,final String message) {
        final ManageFile m= new ManageFile(this);
        final File f=this.getBaseContext().getFilesDir();
        final Object data=m.readFile(f, QuickstartPreferences.PUSH_NOTIFICATIONS);
        final List<NotificacionesPush> pushList= new ArrayList<NotificacionesPush>();
        final NotificacionesPush n= new NotificacionesPush(message,title,new Date());
        if(data == null){ //Ni existe el archivo
            pushList.add(n);
        }else{
            final List<NotificacionesPush> pushListTmp= (List<NotificacionesPush>)data;
            pushList.add(n);
            pushList.addAll(isSameDayList(pushListTmp));
        }
        m.generateFile(f, QuickstartPreferences.PUSH_NOTIFICATIONS, pushList);
    }

    private List<NotificacionesPush> isSameDayList(final List<NotificacionesPush> push){
        final List<NotificacionesPush> pushed= new ArrayList<NotificacionesPush>();
        final Date date2= new Date();
        //final Date date1= null;
        final SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
        for(final NotificacionesPush p:push){
            final Date date1= p.getFecha();
            if(fmt.format(date1).equals(fmt.format(date2))){
                pushed.add(p);
            }
        }
        return pushed;
    }

}
