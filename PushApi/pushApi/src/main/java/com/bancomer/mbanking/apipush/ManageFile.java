package com.bancomer.mbanking.apipush;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.util.Properties;

import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;

/**
 * Created by jinclan on 18/09/15.
 */
public class ManageFile {
    final Context c;
    Properties properties;
    String confFileName;

    public  ManageFile (final Context c){
        this.c=c;
    }

    protected void generateFile(final File f, final String nombre, final Object data){
        confFileName=nombre;
        final File file = new File(f,nombre);
        try {
            if (!file.exists())
                file.createNewFile();
        } catch (IOException ioEx) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "Error al crear el archivo de DatosBmovil.", ioEx);
            return;
        }
        try{
            final FileOutputStream fos = new FileOutputStream(file);
            final ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(data);
            oos.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    protected Object readFile(final File f, final String nombre){
        confFileName=nombre;
        final File file = new File(f,nombre);
        if (!file.exists())
        {
            return null;
        }
        Object data=null;
        ObjectInputStream ois = null;
        try{

            final FileInputStream fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            data =  ois.readObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (OptionalDataException e) {
            e.printStackTrace();
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                ois.close();
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        return data;
    }

    /*    public void initPropertiesFile(File f,String nombre, HashMap<String,String> map) {
        CONFIGURATION_FILE_NAME=nombre;
        File file = new File(f,nombre);
        try {
            if (!file.exists())
            file.createNewFile();
        } catch (IOException ioEx) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "Error al crear el archivo de DatosBmovil.", ioEx);
            return;
        }
        properties = new Properties();
        for (Map.Entry<String, String> entry : map.entrySet())
        {
            setPropertynValue(entry.getKey() , entry.getValue());
        }
    }
    private void setPropertynValue(String propertyName, String value) {
        if ( Tools.isEmptyOrNull(propertyName))
            return;
        if (null == value)
            value = "";

        properties.setProperty(propertyName, value);
        storeFileForProperty(propertyName, value);
    }

    private boolean storeFileForProperty(String propertyName, String propertyValue) {
        if (Tools.isEmptyOrNull(propertyName))
            propertyName = "No name indicated.";
        if (Tools.isEmptyOrNull(propertyValue))
            propertyName = "No value indicated.";

        OutputStream output;
        try {
            output = c.openFileOutput(CONFIGURATION_FILE_NAME, Context.MODE_PRIVATE);
        } catch (FileNotFoundException fnfEx) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(),"Error al abrir el archivo para guardar la propiedad "
                    + propertyName, fnfEx);
            return false;
        }

        try {
            properties.store(output, null);
        } catch (IOException ioEx) {
            if (Server.ALLOW_LOG) Log.e(this.getClass().getName(), "Error al guardar en el archivo de propiedades los valores: "
                    + propertyName + " - " + propertyValue, ioEx);
            return false;
        }

        if (Server.ALLOW_LOG) Log.i(this.getClass().getSimpleName(), "Archivo actualizado con los siguientes cambios: "
                + propertyName + " - " + propertyValue);
        return true;
    }
*/

}
