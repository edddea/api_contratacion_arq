package com.bancomer.mbanking.apipush.models;

import java.io.Serializable;

/**
 * Created by jinclan on 18/09/15.
 */
public class NotificacionesEstatus implements Serializable {

    private static final long serialVersionUID = 1L;

    String estatus;
    String fechaActivacionPush;
    String idPush;
    String idApp;

    public NotificacionesEstatus(final String estatus, final String fechaActivacionPush, final String idPush, final String idApp) {
        this.estatus = estatus;
        this.fechaActivacionPush = fechaActivacionPush;
        this.idPush = idPush;
        this.idApp = idApp;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(final String estatus) {
        this.estatus = estatus;
    }

    public String getFechaActivacionPush() {
        return fechaActivacionPush;
    }

    public void setFechaActivacionPush(final String fechaActivacionPush) {
        this.fechaActivacionPush = fechaActivacionPush;
    }

    public String getIdPush() {
        return idPush;
    }

    public void setIdPush(final String idPush) {
        this.idPush = idPush;
    }

    public String getIdApp() {
        return idApp;
    }

    public void setIdApp(final String idApp) {
        this.idApp = idApp;
    }

    @Override
    public String toString() {
        return new StringBuffer(this.estatus)
                .append("-")
                .append(this.fechaActivacionPush)
                .append("-").append(this.getIdPush()).append("-").append(this.getIdApp()).toString();
    }
}
