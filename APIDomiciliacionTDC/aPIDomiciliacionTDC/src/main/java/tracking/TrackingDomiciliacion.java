package tracking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.adobe.mobile.Analytics;

public class TrackingDomiciliacion {
	
	public static void trackState( ArrayList<String> mapa){
	       HashMap<String, Object> purchaseDictionary = new HashMap<String,Object>();
	       purchaseDictionary.put("channel", "android");

	       String listString = "";

	       for (String s : mapa)
	       {
	           listString += ":" + s;
	       }
	       String res = (purchaseDictionary.get("channel").toString() + listString);
	       
	       switch (mapa.size()) {
	       case 1:
	             purchaseDictionary.put("prop1",res);
	             break;
	       case 2:
	             purchaseDictionary.put("prop2",res);
	             break;
	       case 3:
	             purchaseDictionary.put("prop3",res);
	             break;
	       case 4:
	             purchaseDictionary.put("prop4",res);
	             break;
	       case 5:
	             purchaseDictionary.put("prop5",res);
	             break;
	       case 6:
	             purchaseDictionary.put("prop6",res);
	             break;
	       case 7:
	             purchaseDictionary.put("prop7",res);
	             break;
	       case 8:
	             purchaseDictionary.put("prop8",res);
	             break;
	       case 9:
	             purchaseDictionary.put("prop9",res);
	             break;
	       case 10:
	             purchaseDictionary.put("prop10",res);
	             break;
	       case 11:
	    	   purchaseDictionary.put("prop11",res);
	             break;
	             
	       default:
	             break;
	       }
	       
	       
	       purchaseDictionary.put("appState",res);
	       purchaseDictionary.put("hier1",res);
	       
	       
	       
	       Analytics.trackState(res,purchaseDictionary);
	       
	}


	public static void trackEntrada(Map<String, Object> contextData){
		Analytics.trackAction("Entrada", contextData);
				
	}

	public static void trackSalida(Map<String, Object> contextData){
		Analytics.trackAction("Salida", contextData);
			
	}

	public static void trackRechazoOferta(Map<String, Object> contextData){
		Analytics.trackAction("rechazoOferta", contextData);
	}

	public static void trackOperacionRealizada(Map<String, Object> contextData){
		Analytics.trackAction("operacionRealizada", contextData);
	}

	public static void trackTipoPago(Map<String, Object> contextData){
		Analytics.trackAction("TipoPago", contextData);
	}
	
	
}
	

