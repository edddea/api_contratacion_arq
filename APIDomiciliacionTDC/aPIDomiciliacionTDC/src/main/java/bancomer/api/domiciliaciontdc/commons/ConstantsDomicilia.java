package bancomer.api.domiciliaciontdc.commons;


public class ConstantsDomicilia {
    public static final String CUENTA_CARGO="cuentaCargo";
    public static final String TDC_DOMICILIADA="tdcDomiciliada";
    public static final String ENVIO_CORREO="envioCorreoElectronico";
    public static final String ENVIO_SMS="envioSMS";
    public static final String PM="PM";
    public static final String SI="SI";
    public static final String LP="LP";
    public static final String CAMPANIAS="campanias";
    public static final String CVE_CAMP="cveCamp";
    public static final String DES_OFERTA="desOferta";
    public static final String MONTO="monto";
    public static final String CARRUSEL="carrusel";


    public static final String OPCION="opcion";
    public static final String DETALLE="D";
    public static final String FORMALIZACION="F";
    public static final String NUMERO_CELULAR="numeroCelular";
    public static final String IUM="IUM";
    public static final String TIPO_CARGO="tipoCargo";
    public static final String COGIDO_OTP="codigoOTP";
    public static final String CADENA_AUTENTICACION="cadenaAutenticacion";
    public static final String CODIGO_NIP="codigoNip";
    public static final String CODIGO_CVV="codigoCVV2";
    public static final String TARJETA="Tarjeta5ig";


    public static final String PAGO_MINIMO="M";
    public static final String PAGO_TOTAL="T";




}
