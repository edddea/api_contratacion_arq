package bancomer.api.domiciliaciontdc.gui.controllers;

import android.app.Activity;
import android.os.Bundle;
import com.adobe.mobile.Config;

/**
 * Created by WGUADARRAMA on 27/08/15.
 */
public class BaseActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Config.setContext(this.getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        Config.collectLifecycleData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Config.pauseCollectingLifecycleData();
    }
}
