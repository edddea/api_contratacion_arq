package bancomer.api.domiciliaciontdc.gui.delegates;

import java.util.Hashtable;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.domiciliaciontdc.gui.controllers.ExitoDomiciliaTDCViewController;
import bancomer.api.domiciliaciontdc.models.OfertaDomiciliacionTDC;

/**
 * Created by WGUADARRAMA on 31/08/15.
 */
public class ExitoDomiciliaTDCDelegate implements BaseDelegate {

    public static final long EXITO_DOMICILIA_DELEGATE = 851451334225483459L;

    public ExitoDomiciliaTDCViewController controller;

    public OfertaDomiciliacionTDC oferta;


    public ExitoDomiciliaTDCDelegate(OfertaDomiciliacionTDC oferta)
    {
        this.oferta = oferta;
    }

    public ExitoDomiciliaTDCViewController getController() {
        return controller;
    }

    public void setController(ExitoDomiciliaTDCViewController controller) {
        this.controller = controller;
    }

    public OfertaDomiciliacionTDC getOferta() {
        return oferta;
    }

    public void setOferta(OfertaDomiciliacionTDC oferta) {
        this.oferta = oferta;
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {

    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {

    }

    @Override
    public void performAction(Object obj) {

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }
}
