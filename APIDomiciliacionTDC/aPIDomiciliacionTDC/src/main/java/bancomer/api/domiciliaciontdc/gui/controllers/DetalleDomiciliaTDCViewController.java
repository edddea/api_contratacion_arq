package bancomer.api.domiciliaciontdc.gui.controllers;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.CuentaOrigenController;
import bancomer.api.common.model.HostAccounts;
import bancomer.api.common.timer.TimerController;
import bancomer.api.domiciliaciontdc.R;
import bancomer.api.domiciliaciontdc.commons.ConstantsDomicilia;
import bancomer.api.domiciliaciontdc.gui.delegates.DetalleDomiciliaTDCDelegate;
import bancomer.api.domiciliaciontdc.implementations.InitDomiciliacionTDC;
import tracking.TrackingDomiciliacion;

public class DetalleDomiciliaTDCViewController  extends BaseActivity{

    private DetalleDomiciliaTDCDelegate delegate;
    public BaseViewsController parentManager;
    private BaseViewController baseViewController;
    private InitDomiciliacionTDC init = InitDomiciliacionTDC.getInstance();
    private CuentaOrigenController componenteCtaOrigen;

    /*
     * Titulo
     */
    private TextView lblTitulo;
    private TextView lblSubTitulo;

    /*
     * Tarjeta
     */
    private RelativeLayout relative_tarjeta;
    private TextView  txtNumTarjeta;
    private TextView  txtNomCliente;

    /*
     * Cuenta Origen
     */
    private TextView txtCuentaOrigen;
    private LinearLayout layout_cuenta_origen_view;

    /*
     * Check Box Clausula
     */
    private LinearLayout linearCBClausula;
    private TextView txtClausula;
    private CheckBox cbClausula;

    /*
     * RadioButtons Pago Minimo y Total
     */
    private LinearLayout layout_pagos;

    private LinearLayout layout_pago_minimo;
    private TextView     txtPagoMinimo;
    private RadioButton  rbPagoMinimo;

    private LinearLayout layout_pago_total;
    private TextView     txtPagoTotal;
    private RadioButton  rbPagoTotal;

    /*
     * Boton Confirmar
     */
    private ImageButton btnConfirmar;

    /**
     * TOKEN
     */
    private LinearLayout layout_avisotoken;
    private ImageButton imgaviso_token;
    private TextView    txtaviso_token;
    private LinearLayout contenedorContrasena;
    private LinearLayout contenedorNIP;
    private LinearLayout contenedorASM;
    private LinearLayout contenedorCVV;
    private LinearLayout contenedorCampoTarjeta;

    private TextView campoContrasena;
    private TextView campoNIP;
    private TextView campoASM;
    private TextView campoCVV;
    private TextView campoTarjeta;

    private EditText contrasena;
    private EditText nip;
    private EditText asm;
    private EditText cvv;
    private EditText tarjeta;

    private TextView instruccionesContrasena;
    private TextView instruccionesNIP;
    private TextView instruccionesASM;
    private TextView instruccionesCVV;
    private TextView instruccionesTarjeta;



    //etiquetado Adobe
    String screen = "";
    ArrayList<String> list= new ArrayList<String>();
    Map<String,Object> operacionMap = new HashMap<String, Object>();

    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate=(DetalleDomiciliaTDCDelegate) parentManager.getBaseDelegateForKey(DetalleDomiciliaTDCDelegate.DETALLE_DOMICILIACION_DELEGATE);
        delegate.setController(this);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_domiciliacion_tdc_detalle);
        baseViewController.setTitle(this, R.string.title_Domiciliacion, R.drawable.an_ic_domiciliacion);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        initView();

        //etiquetado Adobe
        screen = "detalle domiciliacionTDC";
        list.add(screen);
        TrackingDomiciliacion.trackState(list);

        Map<String,Object> eventoEntrada = new HashMap<String, Object>();
        eventoEntrada.put("evento_entrar", "event22");
        eventoEntrada.put("eVar22", "entra a la api domiciliacion");
        TrackingDomiciliacion.trackEntrada(eventoEntrada);

    }

    public void initView(){

        findViews();
        setTextToView();
        scaleViews();
        muestraComponenteCuentaOrigen();
        setSecurityComponents();
    }

    /*
    public ArrayList<HostAccounts> obtieneCuentas(){
        ArrayList<HostAccounts> accountsArray = new ArrayList<HostAccounts>();
        HostAccounts[] accounts = init.getListaCuentas();

        for(HostAccounts acc : accounts) {
            if(acc.getType().equals(Constants.SAVINGS_TYPE)||acc.getType().equals(Constants.CHECK_TYPE)
                    ||acc.getType().equals(Constants.LIBRETON_TYPE)) {
                accountsArray.add(acc);
                Log.e("cuenta", acc.getAlias());
                Log.e("cuenta", acc.getNumber());
                Log.e("cuenta", String.valueOf(acc.getBalance()));
                Log.e("cuenta", acc.getType());
            }
        }
        return accountsArray;
    }*/

    public void muestraComponenteCuentaOrigen(){
        ArrayList<HostAccounts> listaCuetasAMostrar = delegate.cargaCuentasOrdenadas();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        componenteCtaOrigen = new CuentaOrigenController(this, params);
        componenteCtaOrigen.getTituloComponenteCtaOrigen().setText("");
        componenteCtaOrigen.setListaCuetasAMostrar(listaCuetasAMostrar);
        componenteCtaOrigen.setIndiceCuentaSeleccionada(0);
        componenteCtaOrigen.init();
        componenteCtaOrigen.getCuentaOrigenDelegate().setTipoOperacion(Constants.Operacion.oneClicDomiTDC);
        layout_cuenta_origen_view.addView(componenteCtaOrigen);
    }

    public void setCuentaCargo(){
        delegate.setCuentaCargo(componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getType() + componenteCtaOrigen.getCuentaOrigenDelegate().getCuentaSeleccionada().getNumber());
    }

    public void setTextToView(){

        txtNomCliente.setText(init.getSession().getNombreCliente().trim());
        txtNumTarjeta.setText(Tools.hideAccountNumber(delegate.getOfertaDomiciliacionTDC().getTdcDomiciliada()));
        lblTitulo.setText(getString(R.string.api_domiciliacion_titulo));
        lblSubTitulo.setText(getString(R.string.api_domiciliacion_subTitulo));
        txtCuentaOrigen.setText(getString(R.string.api_domiciliacion_txtCuentaOrigen));

        String textClausula = getString(R.string.api_domiciliacion_clausula);
        String textDom = getString(R.string.api_domiciliacion);
        int startTextDom= textClausula.indexOf(textDom);
        SpannableString clausulaDom = new SpannableString(textClausula);
        clausulaDom.setSpan(new ForegroundColorSpan(Color.rgb(9, 79, 164)), startTextDom, startTextDom + textDom.length(), 0);
        txtClausula.setText(clausulaDom);

        txtPagoMinimo.setText(getString(R.string.api_domiciliacion_pago_minimo));
        txtPagoTotal.setText(getString(R.string.api_domiciliacion_pago_total));
    }

    public void findViews(){
        //Titulo
        lblTitulo = (TextView) findViewById(R.id.lblTitulo);
        lblSubTitulo = (TextView) findViewById(R.id.lblSubTitulo);
        //Tarjeta
        relative_tarjeta = (RelativeLayout) findViewById(R.id.relative_tarjeta);
        txtNumTarjeta = (TextView) findViewById(R.id.txtNumTarjeta);
        txtNomCliente = (TextView) findViewById(R.id.txtNomCliente);
        //CuentaOrigen
        txtCuentaOrigen = (TextView) findViewById(R.id.txtCuentaOrigen);
        layout_cuenta_origen_view = (LinearLayout)findViewById(R.id.layout_cuenta_origen_view);
        //Check Box Clausula
        linearCBClausula = (LinearLayout)findViewById(R.id.linearCBClausula);
        txtClausula = (TextView) findViewById(R.id.txtClausula);
        cbClausula = (CheckBox) findViewById(R.id.cbClausula);
        //Pagos
        layout_pagos = (LinearLayout)findViewById(R.id.layout_pagos);
        layout_pagos.setVisibility(View.GONE);
        //pago mínimo
        layout_pago_minimo = (LinearLayout)findViewById(R.id.layout_pago_minimo);
        txtPagoMinimo = (TextView) findViewById(R.id.txtPagoMinimo);
        rbPagoMinimo = (RadioButton) findViewById(R.id.rbPagoMinimo);
        //rbPagoMinimo.setClickable(false);
        //rbPagoMinimo.setChecked(false);
        //pago total
        layout_pago_total = (LinearLayout)findViewById(R.id.layout_pago_total);
        txtPagoTotal = (TextView) findViewById(R.id.txtPagoTotal);
        rbPagoTotal = (RadioButton) findViewById(R.id.rbPagoTotal);
        //rbPagoTotal.setClickable(false);
        //rbPagoTotal.setChecked(false);
        //Boton Condirmacion
        btnConfirmar = (ImageButton) findViewById(R.id.confirmacion_confirmar_button);
        btnConfirmar.setOnClickListener(clickListener);


        /** Token **/
        layout_avisotoken	= (LinearLayout)findViewById(R.id.layout_avisotoken);
        txtaviso_token = (TextView) findViewById(R.id.txtaviso_token);
        imgaviso_token = (ImageButton) findViewById(R.id.imgaviso_token);
        layout_avisotoken.setVisibility(View.GONE);

        contenedorContrasena	= (LinearLayout)findViewById(R.id.campo_confirmacion_contrasena_layout);
        contenedorNIP 			= (LinearLayout)findViewById(R.id.campo_confirmacion_nip_layout);
        contenedorASM 			= (LinearLayout)findViewById(R.id.campo_confirmacion_asm_layout);
        contenedorCVV 			= (LinearLayout)findViewById(R.id.campo_confirmacion_cvv_layout);

        contrasena 				= (EditText)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_edittext);
        nip 					= (EditText)contenedorNIP.findViewById(R.id.confirmacion_nip_edittext);
        asm						= (EditText)contenedorASM.findViewById(R.id.confirmacion_asm_edittext);
        cvv						= (EditText)contenedorCVV.findViewById(R.id.confirmacion_cvv_edittext);

        campoContrasena			= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_label);
        campoNIP				= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_label);
        campoASM				= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_label);
        campoCVV				= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_label);

        instruccionesContrasena	= (TextView)contenedorContrasena.findViewById(R.id.confirmacion_contrasena_instrucciones_label);
        instruccionesNIP		= (TextView)contenedorNIP.findViewById(R.id.confirmacion_nip_instrucciones_label);
        instruccionesASM		= (TextView)contenedorASM.findViewById(R.id.confirmacion_asm_instrucciones_label);
        instruccionesCVV		= (TextView)contenedorCVV.findViewById(R.id.confirmacion_cvv_instrucciones_label);

        contenedorCampoTarjeta  = (LinearLayout) findViewById(R.id.campo_confirmacion_campotarjeta_layout);
        tarjeta					= (EditText)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_edittext);
        campoTarjeta			= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_label);
        instruccionesTarjeta	= (TextView)contenedorCampoTarjeta.findViewById(R.id.confirmacion_campotarjeta_instrucciones_label);

    }

    private void scaleViews(){

        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());

        //Titulo
        gTools.scale(lblTitulo, true);
        gTools.scale(lblSubTitulo,true);
        //Tarjeta
        gTools.scale(relative_tarjeta);
        gTools.scale(txtNumTarjeta,true);
        gTools.scale(txtNomCliente,true);
        //CuentaOrigen
        gTools.scale(txtCuentaOrigen,true);
        gTools.scale(layout_cuenta_origen_view);
        //Check Box Clausula
        gTools.scale(txtClausula,true);
        gTools.scale(cbClausula);
        //Pagos
        //pago mínimo
        gTools.scale(txtPagoMinimo,true);
        gTools.scale(rbPagoMinimo);
        //pago total
        gTools.scale(txtPagoTotal,true);
        gTools.scale(rbPagoTotal);
        //Boton
        gTools.scale(btnConfirmar);
        //Token
        gTools.scale(imgaviso_token);
        gTools.scale(txtaviso_token,true);

        gTools.scale(contenedorContrasena);
        gTools.scale(contenedorNIP);
        gTools.scale(contenedorASM);
        gTools.scale(contenedorCVV);

        gTools.scale(contrasena, true);
        gTools.scale(nip, true);
        gTools.scale(asm, true);
        gTools.scale(cvv, true);

        gTools.scale(campoContrasena, true);
        gTools.scale(campoNIP, true);
        gTools.scale(campoASM, true);
        gTools.scale(campoCVV, true);

        gTools.scale(instruccionesContrasena, true);
        gTools.scale(instruccionesNIP, true);
        gTools.scale(instruccionesASM, true);
        gTools.scale(instruccionesCVV, true);

        gTools.scale(contenedorCampoTarjeta);
        gTools.scale(tarjeta, true);
        gTools.scale(campoTarjeta, true);
        gTools.scale(instruccionesTarjeta, true);
    }


    public void onCheckboxClick(View sender){
        //Etiquetado Adobe
        Map<String,Object> TipoPagoMap = new HashMap<String, Object>();

        if(sender == cbClausula){
            if(cbClausula.isChecked()){
                cbClausula.setClickable(false);
                layout_pagos.setVisibility(View.VISIBLE);
                //rbPagoMinimo.setClickable(true);
                //rbPagoTotal.setClickable(true);
                rbPagoMinimo.setChecked(true);
                delegate.setTipoPago(ConstantsDomicilia.PAGO_MINIMO);

                //Etiquetado Adobe
                TipoPagoMap.put("evento_tipopago", "event46");
                TipoPagoMap.put("&&products", "promociones;detalle domiciliacionTDC+tipopago");
                TipoPagoMap.put("eVar12", "tipopago:minimo");
                TrackingDomiciliacion.trackTipoPago(TipoPagoMap);
            }
        }else if(sender == rbPagoMinimo){
            if(rbPagoMinimo.isChecked()){
                rbPagoTotal.setChecked(false);
                delegate.setTipoPago(ConstantsDomicilia.PAGO_MINIMO);

                //Etiquetado Adobe
                TipoPagoMap.put("evento_tipopago", "event46");
                TipoPagoMap.put("&&products", "promociones;detalle domiciliacionTDC+tipopago");
                TipoPagoMap.put("eVar12", "tipopago:minimo");
                TrackingDomiciliacion.trackTipoPago(TipoPagoMap);

            }
        }else if(sender == rbPagoTotal){
            if(rbPagoTotal.isChecked()){
                rbPagoMinimo.setChecked(false);
                delegate.setTipoPago(ConstantsDomicilia.PAGO_TOTAL);

                //Etiquetado Adobe
                TipoPagoMap.put("evento_tipopago", "event46");
                TipoPagoMap.put("&&products", "promociones;detalle domiciliacionTDC+tipopago");
                TipoPagoMap.put("eVar12", "tipopago:total");
                TrackingDomiciliacion.trackTipoPago(TipoPagoMap);

            }
        }
    }

    View.OnClickListener clickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            if(v == btnConfirmar){

                if(!cbClausula.isClickable()){

                    if(delegate.enviaPeticionOperacion()){
                        setCuentaCargo();
                        delegate.performAction(null);

                        //Etiquetado Adobe
                        Map<String,Object> OperacionMap = new HashMap<String, Object>();
                        OperacionMap.put("evento_realizada","event52");
                        OperacionMap.put("&&products","promociones;detalle domiciliacionTDC+exito domiciliacionTDC");
                        OperacionMap.put("eVar12","operacion realizada oferta domiciliacionTDC");
                        TrackingDomiciliacion.trackOperacionRealizada(OperacionMap);
                    }
                }else{
                    init.getBaseViewController().showInformationAlert(DetalleDomiciliaTDCViewController.this, getString(R.string.api_domiciliacion_aviso_clausula));
                }
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        if(!init.getParentManager().isActivityChanging())
        {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }
    }

    @Override
    protected void onDestroy() {
        //etiquetado Adobe
        Map<String,Object> eventoSalida = new HashMap<String, Object>();
        eventoSalida.put("evento_salir", "event23");
        eventoSalida.put("eVar23", "sale de la api domiciliacion");
        TrackingDomiciliacion.trackSalida(eventoSalida);

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    if (DialogInterface.BUTTON_POSITIVE == which) {
                        init.getParentManager().setActivityChanging(true);
                        //init.getParentManager().showMenuInicial();
                        ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
                        init.getParentManager().resetRootViewController();
                        init.resetInstance();

                        operacionMap.put("evento_paso1", "event46");
                        operacionMap.put("&&products", "promociones;detalle domiciliacionTDC");
                        operacionMap.put("eVar12", "rechazo oferta domiciliacionTDC");
                        TrackingDomiciliacion.trackRechazoOferta(operacionMap);
                    }else{
                        dialog.dismiss();
                    }

                }
            };
            init.getBaseViewController().showYesNoAlert(this, R.string.api_domiciliacion_aviso_rechazo, listener);

        }
        return true;
    }


    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }


    /**   Inherited Methods From Base Code**/


    private void setSecurityComponents() {
        mostrarContrasena(delegate.mostrarContrasenia());
        mostrarNIP(delegate.mostrarNIP());
        mostrarASM(delegate.tokenAMostrar());
        mostrarCVV(delegate.mostrarCVV());
        mostrarCampoTarjeta(delegate.mostrarCampoTarjeta());

        LinearLayout parentContainer = (LinearLayout)findViewById(R.id.confirmacion_campos_layout);

        if (contenedorContrasena.getVisibility() == View.GONE &&
                contenedorNIP.getVisibility() == View.GONE &&
                contenedorASM.getVisibility() == View.GONE &&
                contenedorCVV.getVisibility() == View.GONE &&
                contenedorCampoTarjeta.getVisibility() == View.GONE) {
            parentContainer.setBackgroundColor(0);
            parentContainer.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        }

        if (contenedorContrasena.getVisibility() == View.VISIBLE ||
                contenedorNIP.getVisibility() == View.VISIBLE ||
                contenedorASM.getVisibility() == View.VISIBLE ||
                contenedorCVV.getVisibility() == View.VISIBLE ||
                contenedorCampoTarjeta.getVisibility() == View.VISIBLE) {

            layout_avisotoken.setVisibility(View.VISIBLE);
        }

    }


    public void mostrarContrasena(boolean visibility){
        contenedorContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        campoContrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        contrasena.setVisibility(visibility ? View.VISIBLE : View.GONE);
        if (visibility) {
            campoContrasena.setText(delegate.getEtiquetaCampoContrasenia());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
            contrasena.setFilters(userFilterArray);
            contrasena.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            contrasena.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
        instruccionesContrasena.setVisibility(View.GONE);
    }


    public void mostrarNIP(boolean visibility){
        contenedorNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
        campoNIP.setVisibility(visibility ? View.VISIBLE:View.GONE);
        nip.setVisibility(visibility ? View.VISIBLE:View.GONE);
        if (visibility) {
            campoNIP.setText(delegate.getEtiquetaCampoNip());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.NIP_LENGTH);
            nip.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(tarjeta);
            nip.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = delegate.getTextoAyudaNIP();
            if (instrucciones.equals("")) {
                instruccionesNIP.setVisibility(View.GONE);
            } else {
                instruccionesNIP.setVisibility(View.VISIBLE);
                instruccionesNIP.setText(instrucciones);
            }
        } else {
            nip.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }


    public void mostrarASM(Constants.TipoOtpAutenticacion tipoOTP){
        switch(tipoOTP) {
            case ninguno:
                contenedorASM.setVisibility(View.GONE);
                campoASM.setVisibility(View.GONE);
                asm.setVisibility(View.GONE);
                asm.setImeOptions(EditorInfo.IME_ACTION_NONE);
                break;
            case codigo:
            case registro:
                contenedorASM.setVisibility(View.VISIBLE);
                campoASM.setVisibility(View.VISIBLE);
                asm.setVisibility(View.VISIBLE);
                InputFilter[] userFilterArray = new InputFilter[1];
                userFilterArray[0] = new InputFilter.LengthFilter(Constants.ASM_LENGTH);
                asm.setFilters(userFilterArray);
                cambiarAccionTexto(contrasena);
                cambiarAccionTexto(tarjeta);
                cambiarAccionTexto(nip);
                asm.setImeOptions(EditorInfo.IME_ACTION_DONE);
                break;
        }

        Constants.TipoInstrumento tipoInstrumento = delegate.getTipoInstrumentoSeguridad();

        switch (tipoInstrumento) {
            case OCRA:
                campoASM.setText(delegate.getEtiquetaCampoOCRA());
                asm.setTransformationMethod(null);
                break;
            case DP270:
                campoASM.setText(delegate.getEtiquetaCampoDP270());
                asm.setTransformationMethod(null);
                break;
            case SoftToken:
                if(init.getSofTokenStatus()) {
                    asm.setText(Constants.DUMMY_OTP);
                    asm.setEnabled(false);
                    campoASM.setText(delegate.getEtiquetaCampoSoftokenActivado());
                } else {
                    asm.setText("");
                    asm.setEnabled(true);
                    campoASM.setText(delegate.getEtiquetaCampoSoftokenDesactivado());
                    asm.setTransformationMethod(null);
                }

                break;
            default:
                break;
        }
        if(delegate.getContext()==null && getBaseContext()!=null){
            delegate.setContext(getBaseContext());
        }
        String instrucciones = delegate.getTextoAyudaInstrumentoSeguridad(tipoInstrumento,init.getSofTokenStatus(),delegate.tokenAMostrar());
        if (instrucciones.equals("")) {
            instruccionesASM.setVisibility(View.GONE);
        } else {
            instruccionesASM.setVisibility(View.VISIBLE);
            instruccionesASM.setText(instrucciones);
        }
    }


    private void mostrarCampoTarjeta(boolean visibility) {
        contenedorCampoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
        campoTarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
        tarjeta.setVisibility(visibility ? View.VISIBLE:View.GONE);
        if (visibility) {
            layout_avisotoken.setVisibility(View.VISIBLE);
            campoTarjeta.setText(delegate.getEtiquetaCampoTarjeta());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(5);
            tarjeta.setFilters(userFilterArray);
            cambiarAccionTexto(contrasena);
            tarjeta.setImeOptions(EditorInfo.IME_ACTION_DONE);
            String instrucciones = delegate.getTextoAyudaTarjeta();
            if (instrucciones.equals("")) {
                instruccionesTarjeta.setVisibility(View.GONE);
            } else {
                instruccionesTarjeta.setVisibility(View.VISIBLE);
                instruccionesTarjeta.setText(instrucciones);
            }
        } else {
            tarjeta.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }

    }

    public void mostrarCVV(boolean visibility){
        contenedorCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
        campoCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);
        cvv.setVisibility(visibility ? View.VISIBLE : View.GONE);
        instruccionesCVV.setVisibility(visibility ? View.VISIBLE : View.GONE);

        if (visibility) {
            campoCVV.setText(delegate.getEtiquetaCampoCVV());
            InputFilter[] userFilterArray = new InputFilter[1];
            userFilterArray[0] = new InputFilter.LengthFilter(Constants.CVV_LENGTH);
            cvv.setFilters(userFilterArray);
            String instrucciones = delegate.getTextoAyudaCVV();
            instruccionesCVV.setText(instrucciones);
            instruccionesCVV.setVisibility(instrucciones.equals("") ? View.GONE : View.VISIBLE);
            cambiarAccionTexto(contrasena);
            cambiarAccionTexto(nip);
            cambiarAccionTexto(asm);
            cvv.setImeOptions(EditorInfo.IME_ACTION_DONE);
        } else {
            cvv.setImeOptions(EditorInfo.IME_ACTION_NONE);
        }
    }

    private void cambiarAccionTexto(EditText campo) {
        if (campo.getVisibility() == View.VISIBLE) {
            campo.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        }
    }



    public String pideContrasena() {
        if (contrasena.getVisibility() == View.GONE) {
            return "";
        } else {
            return contrasena.getText().toString();
        }
    }

    public String pideNIP() {
        if (nip.getVisibility() == View.GONE) {
            return "";
        } else {
            return nip.getText().toString();
        }
    }

    public String pideASM() {
        if (asm.getVisibility() == View.GONE) {
            return "";
        } else {
            return asm.getText().toString();
        }
    }

    public String pideCVV() {
        if (cvv.getVisibility() == View.GONE) {
            return "";
        } else {
            return cvv.getText().toString();
        }
    }

    public String pideTarjeta(){
        if (tarjeta.getVisibility() == View.GONE) {
            return "";
        }else
            return tarjeta.getText().toString();
    }
}
