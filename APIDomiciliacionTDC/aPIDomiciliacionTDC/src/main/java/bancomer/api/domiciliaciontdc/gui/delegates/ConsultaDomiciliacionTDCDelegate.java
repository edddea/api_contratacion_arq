package bancomer.api.domiciliaciontdc.gui.delegates;

import android.content.DialogInterface;
import android.util.Log;

import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.domiciliaciontdc.commons.ConstantsDomicilia;
import bancomer.api.domiciliaciontdc.implementations.InitDomiciliacionTDC;
import bancomer.api.domiciliaciontdc.io.Server;
import bancomer.api.domiciliaciontdc.models.OfertaDomiciliacionTDC;
import bancomer.api.common.gui.controllers.BaseViewController;
import java.util.Hashtable;
/**
 * Created by WGUADARRAMA on 04/09/15.
 */
public class ConsultaDomiciliacionTDCDelegate implements BaseDelegate {

    private OfertaDomiciliacionTDC ofertaDomiciliacionTDC;
    private InitDomiciliacionTDC init = InitDomiciliacionTDC.getInstance();
    private BaseViewController baseViewController;
    private String opcion;

    /**
     * metodo que envia la peticion a la clase BaseSubaplication
     * @param operationId
     * @param params
     * @param caller
     * @param isJson
     * @param handler
     */
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller,final boolean isJson,final ParsingHandler handler) {
        init.getBaseSubApp().invokeNetworkOperation(operationId, params, caller, false,isJson,handler);
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
        //metodo vacio
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        ofertaDomiciliacionTDC = (OfertaDomiciliacionTDC)response.getResponse();

        if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)
        {
            init.showDetalleDomiciliacion(ofertaDomiciliacionTDC);
        }
        else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
            init.getParentManager().resetRootViewController();
            init.updateHostActivityChangingState();
            init.resetInstance();
            try {

                if (!ofertaDomiciliacionTDC.getPM().equals("")) {
                    //Log.e("Promociones", ofertaDomiciliacionTDC.getArrayPromociones().toString());
                    init.getSession().setPromocion(ofertaDomiciliacionTDC.getArrayPromociones());

                     DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener()
                     {
                         @Override
                         public void onClick(DialogInterface dialog, int which) {
                             dialog.dismiss();
                         }
                     };
                     init.getBaseViewController().showInformationAlert(init.getParentManager().getCurrentViewController(),response.getMessageText(),listener);

                }
                else{
                    init.getBaseViewController().showInformationAlert(init.getParentManager().getCurrentViewController(), response.getMessageText());
                }

            }catch(NullPointerException ex)
            {
                init.getBaseViewController().showErrorMessage(init.getParentManager().getCurrentViewController(),response.getMessageText());
            }
        }else
        {
            init.getBaseViewController().showInformationAlert(init.getParentManager().getCurrentViewController(), response.getMessageText());
        }
    }

    @Override
    public void performAction(Object obj) {

        baseViewController = init.getBaseViewController();
        baseViewController.setActivity(init.getParentManager().getCurrentViewController());
        baseViewController.setDelegate(this);
        init.setBaseViewController(baseViewController);

        opcion = ConstantsDomicilia.DETALLE;

        String user = init.getSession().getUsername();
        String ium  = init.getSession().getIum();
        String cveCamp = init.getOfertaDomiciliacion().getCveCamp();

        Log.w("DOMICILIACION--","operacion - oneClicDomiTDC");
        Log.w("DOMICILIACION--","numeroCelular - "+user);
        Log.w("DOMICILIACION--", "claveCamp - " + cveCamp);
        Log.w("DOMICILIACION--", "IUM - " + ium);
        Log.w("DOMICILIACION--", "opcion - D");

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put("numeroCelular", user);
        params.put("cveCamp", cveCamp);
        params.put("IUM", ium);
        params.put("opcion", opcion);

        doNetworkOperation(Server.OFERTA_DOMICILIACION, params, InitDomiciliacionTDC.getInstance().getBaseViewController(),true,new OfertaDomiciliacionTDC());

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }

}
