/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package bancomer.api.domiciliaciontdc.io;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.commons.NameValuePair;
import bancomer.api.domiciliaciontdc.commons.ConstantsDomicilia;
import bancomer.api.domiciliaciontdc.models.OfertaDomiciliacionTDC;


/**
 * Server is responsible of acting as an interface between the application
 * business logic and the HTTP operations. It receives the network operation
 * requests and builds a network operation suitable for ClienteHttp class, which
 * eventually will perform the network connection. Server returns the server
 * response as a ServerResponseImpl object, so that the application logic can
 * perform high level checks and processes.
 * 
 * @author GoNet.
 */

public class Server {
	
	/**
	 * Defines if the application show logs
	 */
	public static boolean ALLOW_LOG = true;

	/**
	 * Defines if the application is running on the emulator or in the device.
	 */
	public static boolean EMULATOR = false;

	/**
	 * Indicates simulation usage.
	 */
	public static boolean SIMULATION = false;

	/**
	 * Indicates if the base URL points to development server or production
	 */
	public static boolean DEVELOPMENT = false;
	// public static boolean DEVELOPMENT = true; //TODO remove for TEST

	/**
	 * Indicates simulation usage.
	 */
	public static final long SIMULATION_RESPONSE_TIME = 5;

	/**
	 * Bancomer Infrastructure.
	 */
	public static final int BANCOMER = 0;

	public static int PROVIDER = BANCOMER; // TODO remove for TEST


	// ///////////////////////////////////////////////////////////////////////////
	// Operations identifiers //
	// ///////////////////////////////////////////////////////////////////////////

	/** One Click DomiciliacionTDC **/
	public static final int OFERTA_DOMICILIACION = 135;

    public static final String JSON_OPERATION_DOMICILIACION_TDC = "VTDC001";


	/**
	 * Operation codes.
	 */
	public static final String[] OPERATION_CODES = { "00","oneClicDomiTDC"};


	/**
	 * Base URL for providers.
	 */
	public static String[] BASE_URL;

	/**
	 * Path table for provider/operation URLs.
	 */
	public static String[][] OPERATIONS_PATH;

	/**
	 * Operation URLs map.
	 */
	public static Hashtable<String, String> OPERATIONS_MAP = new Hashtable<String, String>();

	/**
	 * Operation providers map.
	 */
	public static Hashtable<String, Integer> OPERATIONS_PROVIDER = new Hashtable<String, Integer>();

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_PARAMETER = "OPERACION";


	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_VALUE = "es_ES";


	//static {
	public static void setEnviroment() {
		if (DEVELOPMENT) {
			setupDevelopmentServer();
		} else {
			setupProductionServer();
		}

		setupOperations(PROVIDER);
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupProductionServer() {
		BASE_URL = new String[] { "https://www.bancomermovil.com",
				// "https://172.17.100.173",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Produccion"
						: "" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "",
						"/mbhxp_mx_web/servlet/ServletOperacionWeb"  //OneClick DomiciliacionTDC
				},
				new String[] { "",
						"/mbhxp_mx_web/servlet/ServletOperacionWeb"   //OneClick DomiciliacionTDC

				} };
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupDevelopmentServer() {
		BASE_URL = new String[] {
				"https://www.bancomermovil.net:11443",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Desarrollo"
						: "http://189.254.77.54:8080/servermobile/Servidor/Desarrollo" };
		OPERATIONS_PATH = new String[][] {
				new String[] { "",
						"/eexd_mx_web/servlet/ServletOperacionWeb"  //OneClick DomiciliacionTDC
				},
				new String[] { "",
						"/eexd_mx_web/servlet/ServletOperacionWeb"  //OneClick DomiciliacionTDC

				} };
	}

	/**
	 * Setup operations for a provider.
	 * 
	 * @param provider
	 *            the provider
	 */
	private static void setupOperations(int provider) {

        setupOperation(OFERTA_DOMICILIACION, provider);

	}

	/**
	 * Setup operation for a provider.
	 * @param operation the operation
	 * @param provider the provider
	 */
	private static void setupOperation(int operation, int provider) {
		String code = OPERATION_CODES[operation-134];
		OPERATIONS_MAP.put(
				code,
				new StringBuffer(BASE_URL[provider]).append(
						OPERATIONS_PATH[provider][operation-134]).toString());
		OPERATIONS_PROVIDER.put(code, new Integer(provider));
	}

	/**
	 * Get the operation URL.
	 * @param operation the operation id
	 * @return the operation URL
	 */
	public static String getOperationUrl(String operation) {
		String url = (String) OPERATIONS_MAP.get(operation);
		return url;
	}

	/**
	 * Referencia a ClienteHttp, en lugar de HttpInvoker.
	 */
	private ClienteHttp clienteHttp = null;

	
	// protected Context mContext;

	public ClienteHttp getClienteHttp() {
		return clienteHttp;
	}

	public void setClienteHttp(ClienteHttp clienteHttp) {
		this.clienteHttp = clienteHttp;
	}


	
	public Server() {
		clienteHttp = new ClienteHttp();
	}

	/**
	 * perform the network operation identifier by operationId with the
	 * parameters passed.
	 * @param operationId the identifier of the operation
	 * @param params Hashtable with the parameters as key-value pairs.
	 * @return the response from the server
	 * @throws IOException exception
	 * @throws ParsingException exception
	 */
	public ServerResponseImpl doNetworkOperation(int operationId, Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

		ServerResponseImpl response = null;
		long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
		Integer provider = (Integer) OPERATIONS_PROVIDER.get(OPERATION_CODES[operationId-134]);
		if (provider != null) {
			PROVIDER = provider.intValue();
		}

		switch (operationId) {
			case OFERTA_DOMICILIACION:
				 response = ofertaDomiciliacionTDC(params);
			break;

		}

		if (sleep > 0) {
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
			}
		}
		return response;
	}



	private ServerResponseImpl ofertaDomiciliacionTDC(Hashtable<String, ?> params)
			throws IOException, ParsingException, URISyntaxException {
		OfertaDomiciliacionTDC data = new OfertaDomiciliacionTDC();
		ServerResponseImpl response = new ServerResponseImpl(data);

		String opcion = (String) params.get(ConstantsDomicilia.OPCION);
		NameValuePair[] parameters;

		if(opcion.equals(ConstantsDomicilia.DETALLE)){
			//OPCION DETALLE
			//String operacion = (String) params.get("operacion");
			//String opcion = (String) params.get("opcion");
			String numeroCelular = (String) params.get(ConstantsDomicilia.NUMERO_CELULAR);
			String cveCamp = (String) params.get(ConstantsDomicilia.CVE_CAMP);
			String IUM = (String) params.get(ConstantsDomicilia.IUM);


			parameters = new NameValuePair[4];
			//parameters[0] = new NameValuePair("operacion", operacion);
			parameters[0] = new NameValuePair(ConstantsDomicilia.NUMERO_CELULAR, numeroCelular);
			parameters[1] = new NameValuePair(ConstantsDomicilia.CVE_CAMP,cveCamp);
			parameters[2] = new NameValuePair(ConstantsDomicilia.IUM, IUM);
			parameters[3] = new NameValuePair(ConstantsDomicilia.OPCION,opcion);
		}
		else{
			//OPCION FORMALIZACION
			//String opcion = (String) params.get("opcion");
			String numeroCelular = (String) params.get(ConstantsDomicilia.NUMERO_CELULAR);
			String cveCamp = (String) params.get(ConstantsDomicilia.CVE_CAMP);
			String IUM = (String) params.get(ConstantsDomicilia.IUM);
			String cuentaCargo = (String) params.get(ConstantsDomicilia.CUENTA_CARGO);
			String tipoCargo = (String) params.get(ConstantsDomicilia.TIPO_CARGO);
			String codigoOTP = (String) params.get(ConstantsDomicilia.COGIDO_OTP);
			String cadenaAutenticacion = (String) params.get(ConstantsDomicilia.CADENA_AUTENTICACION);
			String codigoNip = (String) params.get(ConstantsDomicilia.CODIGO_NIP);
			String codigoCVV2 = (String) params.get(ConstantsDomicilia.CODIGO_CVV);
			String Tarjeta5ig = (String) params.get(ConstantsDomicilia.TARJETA);


			parameters = new NameValuePair[11];
			parameters[0] = new NameValuePair(ConstantsDomicilia.OPCION,opcion);
			parameters[1] = new NameValuePair(ConstantsDomicilia.NUMERO_CELULAR, numeroCelular);
			parameters[2] = new NameValuePair(ConstantsDomicilia.CVE_CAMP, cveCamp);
			parameters[3] = new NameValuePair(ConstantsDomicilia.IUM,IUM);
			parameters[4] = new NameValuePair(ConstantsDomicilia.CUENTA_CARGO, cuentaCargo);
			parameters[5] = new NameValuePair(ConstantsDomicilia.TIPO_CARGO,tipoCargo);
			parameters[6] = new NameValuePair(ConstantsDomicilia.COGIDO_OTP,codigoOTP);
			parameters[7] = new NameValuePair(ConstantsDomicilia.CADENA_AUTENTICACION,cadenaAutenticacion);
			parameters[8] = new NameValuePair(ConstantsDomicilia.CODIGO_NIP,codigoNip);
			parameters[9] = new NameValuePair(ConstantsDomicilia.CODIGO_CVV,codigoCVV2);
			parameters[10] = new NameValuePair(ConstantsDomicilia.TARJETA,Tarjeta5ig);
		}

		if (SIMULATION) {

			String mensaje = ""; //respuesta del servidor

			if(opcion.equals(ConstantsDomicilia.DETALLE)){
				//TRAZA DETALLE "OK"
				mensaje = "{\"estado\":\"OK\",\"cuentaCargo\":\"00743616002700628033\",\"tdcDomiciliada\":\"4772913006152955\"}";

				//TRAZA ERROR
				//mensaje = "{\"estado\":\"ERROR\",\"codigoMensaje\":\"KZE0485\",\"descripcionMensaje\":\"SERVICIO NO DISPONIBLE.\"}";

				//mensaje = "{\"estado\":\"ERROR\",\"codigoMensaje\":\"KZE0485\",\"descripcionMensaje\":\"SERVICIO NO DISPONIBLE.\" , \"PM\":\"SI\", \"LP\":{\"campanias\":[{\"cveCamp\":\"0130ABCDEGAS123\",\"desOferta\":\"Incremento de línea de credito\",\"monto\":\"50000\",\"carrusel\":\"SI\"}]}}";

			}else{
				//TRAZA FORMALIZACION "OK"

				//mensaje = "{\"estado\":\"ERROR\",\"codigoMensaje\":\"KZE0485\",\"descripcionMensaje\":\"SERVICIO NO DISPONIBLE.\"}";

				mensaje = "{\"estado\":\"OK\",\"cuentaCargo\":\" AH00740010000178659087\", \"tdcDomiciliada\":\"4152313112345678\",  \"envioCorreoElectronico\":\"SI\", \"envioSMS\":\"SI\", \"PM\":\"SI\", \"LP\":{\"campanias\":[{\"cveCamp\":\"0130ABCDEGAS123\",\"desOferta\":\"Incremento de línea de credito\",\"monto\":\"50000\",\"carrusel\":\"SI\"}]}}";
                //mensaje = "{\"estado\":\"OK\",\"cuentaCargo\":\"AH00743463002961503871\",\"tdcDomiciliada\":\"4772132911203056\",\"envioCorreoElectronico\":\"SI\",\"envioSMS\":\"SI\",\"PM\":\"NO\"}";

                //TRAZA ERROR
                //mensaje = "{\"estado\":\"ERROR\",\"codigoMensaje\":\"KZE0485\",\"descripcionMensaje\":\"SERVICIO NO DISPONIBLE.\" , \"PM\":\"NO\", \"LP\":{\"campanias\":[{\"cveCamp\":\"0130ABCDEGAS123\",\"desOferta\":\"Incremento de línea de credito\",\"monto\":\"50000\",\"carrusel\":\"SI\"}]}}";
			}


			this.clienteHttp.simulateOperation(OPERATION_CODES[OFERTA_DOMICILIACION-134],parameters,response,mensaje, true);
		}
		else{

			clienteHttp.invokeOperation(OPERATION_CODES[OFERTA_DOMICILIACION-134], parameters, response, false, true);
		}
		return response;
	}


	

	/**
	 * Cancel the ongoing network operation.
	 */
	public void cancelNetworkOperations() {
		clienteHttp.abort();
	}



}


/********************************                ********************************/
/******************************** GENERIC THINGS ********************************/
/********************************                ********************************/

			/*

			public ServerResponseImpl doNetworkOperationGeneric(String operationCode, HashMap<String, String> params,  ArrayList<String> urlsProd, ArrayList<String> urlsDev, Boolean isJSON, Object responseModel, String simulatedResponse) throws IOException, ParsingException, URISyntaxException {

				ServerResponseImpl response = null;

				if(!containOperationCode(operationCode)){

					Integer lengthZero = OPERATIONS_PATH[0].length;
					Integer lengthOne = OPERATIONS_PATH[1].length;
					String url0 = urlsProd.get(0);
					String url1 = urlsProd.get(1);
					if (DEVELOPMENT) {
						url0 = urlsDev.get(0);
						url1 = urlsDev.get(1);
					}

					OPERATIONS_PATH[0][lengthZero] = url0;
					OPERATIONS_PATH[1][lengthOne] = url1;

					OPERATION_CODES[OPERATION_CODES.length] = operationCode;

					setupOperation(OPERATION_CODES.length, PROVIDER);

				}

				long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
				Integer provider = (Integer) OPERATIONS_PROVIDER.get(operationCode);
				if (provider != null) {
					PROVIDER = provider.intValue();
				}

				response = genericOperation(params, operationCode, isJSON, responseModel, simulatedResponse);

				if (sleep > 0) {
					try {
						Thread.sleep(sleep);
					} catch (InterruptedException e) {
					}
				}

				return response;
			}

			private Boolean containOperationCode(String operation){
				Boolean ret = false;
				for(String op : OPERATION_CODES){
					if(op.equalsIgnoreCase(operation)) ret = true;
				}

				return ret;
			}

			private ServerResponseImpl genericOperation(HashMap<String, String> params, String operationCode, Boolean isJSON, Object responseModel, String simulatedResponse)
					throws IOException, ParsingException, URISyntaxException {

				//FIXME null ?
				ServerResponseImpl response = new ServerResponseImpl(null);

				if (SIMULATION) {

					this.httpClient.simulateOperation(simulatedResponse,(ParsingHandler) response, true);

				}else{

					Iterator<String> it = params.keySet().iterator();
					NameValuePair[] parameters = new NameValuePair[params.keySet().size()];

					Integer cont = 0;
					while(it.hasNext()){
						String key = it.next();
						parameters[cont] = new NameValuePair(key, params.get(key));

						++cont;
					}

					clienteHttp.invokeOperation(operationCode, parameters, response, false, isJSON);
				}

				return response;
			}


			*/

/********************************                ********************************/
/********************************                ********************************/
/********************************                ********************************/



//one Click
/**
 * clase de enum para cambio de url;
 */
			/*public enum isJsonValueCode{
				ONECLICK, NONE,CONSUMO,DEPOSITOS;
			}

			public static isJsonValueCode isjsonvalueCode= isJsonValueCode.NONE;
			//Termina One CLick*/