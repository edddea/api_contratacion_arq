package bancomer.api.domiciliaciontdc.gui.controllers;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bancomer.api.common.callback.CallBackModule;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.timer.TimerController;
import bancomer.api.domiciliaciontdc.R;
import bancomer.api.domiciliaciontdc.gui.delegates.ExitoDomiciliaTDCDelegate;
import bancomer.api.domiciliaciontdc.implementations.InitDomiciliacionTDC;
import tracking.TrackingDomiciliacion;

/**
 * Created by WGUADARRAMA on 31/08/15.
 */
public class ExitoDomiciliaTDCViewController extends BaseActivity implements View.OnClickListener{

    public BaseViewsController parentManager;
    private BaseViewController baseViewController;
    private InitDomiciliacionTDC init = InitDomiciliacionTDC.getInstance();
     private ExitoDomiciliaTDCDelegate delegate;

    LinearLayout vista;
    private TextView texto1;
    private TextView texto2;
    private TextView texto3;
    private TextView texto4;

    private LinearLayout layout_email;
    private ImageView imgIconoEmail;
    private TextView txtEmail;

    private LinearLayout layout_SMS;
    private ImageView imgIconoSMS;
    private TextView txtSMS;

    private TextView texto5;
    private TextView texto6;

    private ImageButton btnMenu;

    //etiquetado Adobe
    String screen1 = "";
    String screen2 = "";
    ArrayList<String> list= new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        baseViewController = init.getBaseViewController();

        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);

        delegate=(ExitoDomiciliaTDCDelegate) parentManager.getBaseDelegateForKey(ExitoDomiciliaTDCDelegate.EXITO_DOMICILIA_DELEGATE);
        delegate.setController(this);

        baseViewController.setActivity(this);
        baseViewController.onCreate(this, BaseViewController.SHOW_HEADER | BaseViewController.SHOW_TITLE, R.layout.api_domiciliacion_tdc_exito);
        baseViewController.setTitle(this, R.string.title_Domiciliacion, R.drawable.an_ic_domiciliacion);
        baseViewController.setDelegate(delegate);
        init.setBaseViewController(baseViewController);

        initView();

        //etiquetado Adobe
        screen1 = "detalle domiciliacionTDC";
        list.add(screen1);
        screen2 = "exito domiciliacionTDC";
        list.add(screen2);
        TrackingDomiciliacion.trackState(list);
    }


    public void initView(){
        findViews();
        setData();
        scaleViews();
    }

    public void findViews(){

        texto1 = (TextView) findViewById(R.id.texto1);
        texto2 = (TextView) findViewById(R.id.texto2);
        texto3 = (TextView) findViewById(R.id.texto3);
        texto4 = (TextView) findViewById(R.id.texto4);
        texto5 = (TextView) findViewById(R.id.texto5);
        texto6 = (TextView) findViewById(R.id.texto6);

        layout_email = (LinearLayout) findViewById(R.id.layout_email);
        imgIconoEmail = (ImageView) findViewById(R.id.imgIconoEmail);
        txtEmail = (TextView) findViewById(R.id.txtEmail);

        layout_SMS = (LinearLayout) findViewById(R.id.layout_SMS);
        imgIconoSMS = (ImageView) findViewById(R.id.imgIconoSMS);
        txtSMS = (TextView) findViewById(R.id.txtSMS);

        btnMenu = (ImageButton)findViewById(R.id.btnMenu);
        btnMenu.setOnClickListener(this);
    }

    public void setData(){
        //Texto Felicidades
        texto1.setText(getString(R.string.api_domiciliacion_tituloForm));

        String subTitulo = getString(R.string.api_domiciliacion_subTituloForm);
        String strCorretamente = getString(R.string.api_domiciliacion_strCorrectamente);
        int    startStrCorretamente = subTitulo.indexOf(strCorretamente);

        SpannableString textSubTitulo = new SpannableString(subTitulo);
        textSubTitulo.setSpan(new ForegroundColorSpan(Color.rgb(134, 200, 45)), startStrCorretamente, startStrCorretamente + strCorretamente.length(), 0);
        texto2.setText(textSubTitulo);
        //END Texto Felicidades

        texto3.setText(getString(R.string.api_domiciliacion_textoTeRecordamos));
        texto4.setText(getString(R.string.api_domiciliacion_textoEnvio));

        //Muestra componentes SMS y/o EMAIL
        if(delegate.oferta.getEnvioSMS().equalsIgnoreCase("SI"))
                showSMS();
        if(delegate.oferta.getEnvioCorreoElectronico().equalsIgnoreCase("SI"))
                showEmail();


        //Texto ModificarCorreo
        String modificaCorreo = getString(R.string.api_domiciliacion_textoModificarCorreo);
        String strAdministrar = getString(R.string.api_domiciliacion_strAdministrar);
        int    startStrAdministrar = modificaCorreo.indexOf(strAdministrar);

        SpannableString textModificarCorreo = new SpannableString(modificaCorreo);
        textModificarCorreo.setSpan(new ForegroundColorSpan(Color.rgb(0,0,0)),startStrAdministrar,startStrAdministrar+ strAdministrar.length(),0);
        texto5.setText(textModificarCorreo);
        //END Texto Modificar Correo

        //Texto AlertasSMS
        String alertasSMS1 = getString(R.string.api_domiciliacion_textoAlertasSMS1);
        String alertasSMS2 = getString(R.string.api_domiciliacion_textoAlertasSMS2);
        String strAlertas = getString(R.string.api_domiciliacion_strAlertas);

        texto6.setText(Html.fromHtml("<font color='" + getResources().getColor(R.color.gris_texto_3) + "'>" + alertasSMS1 + " " + "</font>"
                + "<font color='" + getResources().getColor(R.color.negro_bmovil_detalle) + "'>" + strAlertas+ "</font>"
                + "<font color='" + getResources().getColor(R.color.gris_texto_3) + "'>" + alertasSMS2 + "</font>"), TextView.BufferType.SPANNABLE);

        //END Texto AlertasSMS
    }

    public void showEmail(){
        texto4.setVisibility(View.VISIBLE);
        layout_email.setVisibility(View.VISIBLE);
        imgIconoEmail.setVisibility(View.VISIBLE);
        String email = init.getSession().getEmail();
        txtEmail.setText(email);

    }

    public void showSMS(){
        texto4.setVisibility(View.VISIBLE);
        layout_SMS.setVisibility(View.VISIBLE);
        imgIconoSMS.setVisibility(View.VISIBLE);
        String number = init.getSession().getUsername();
        txtSMS.setText(Tools.hideUsername(number));
    }

    public void scaleViews(){

        GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());

        gTools.scale(texto1, true);
        gTools.scale(texto2, true);
        gTools.scale(texto3, true);
        gTools.scale(texto4, true);
        gTools.scale(texto5, true);
        gTools.scale(texto6, true);

        gTools.scale(imgIconoEmail);
        gTools.scale(txtEmail, true);

        gTools.scale(imgIconoSMS);
        gTools.scale(txtSMS, true);

        gTools.scale(btnMenu);

    }


    @Override
    public void onClick(View v) {
        if(v == btnMenu)
        {
            init.getParentManager().setActivityChanging(true);
            //init.getParentManager().showMenuInicial();
            ((CallBackModule)init.getParentManager().getRootViewController()).returnToPrincipal();
            init.resetInstance();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
        }
        return true;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        //init.getParentManager().onUserInteraction();
        TimerController.getInstance().resetTimer();
    }

    @Override
    protected void onDestroy() {
        //etiquetado Adobe
        Map<String,Object> eventoSalida = new HashMap<String, Object>();
        eventoSalida.put("evento_salir", "event23");
        eventoSalida.put("eVar23", "sale de la api domiciliacion");
        TrackingDomiciliacion.trackSalida(eventoSalida);

        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        baseViewController = init.getBaseViewController();
        parentManager = init.getParentManager();
        parentManager.setCurrentActivityApp(this);
        baseViewController.setActivity(this);
        init.setBaseViewController(baseViewController);


    }

    @Override
    protected void onPause() {
        super.onPause();
        if(!init.getParentManager().isActivityChanging())
        {
            TimerController.getInstance().getSessionCloser().cerrarSesion();
            init.getParentManager().resetRootViewController();
            init.resetInstance();
        }

    }
}
