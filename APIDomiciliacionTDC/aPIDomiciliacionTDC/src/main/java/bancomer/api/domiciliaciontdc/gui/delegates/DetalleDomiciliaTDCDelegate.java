package bancomer.api.domiciliaciontdc.gui.delegates;


import android.content.DialogInterface;
import android.util.Log;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.Constants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.io.ParsingHandler;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.common.model.HostAccounts;
import bancomer.api.common.gui.delegates.SecurityComponentDelegate;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.domiciliaciontdc.R;
import bancomer.api.domiciliaciontdc.commons.ConstantsDomicilia;
import bancomer.api.domiciliaciontdc.gui.controllers.DetalleDomiciliaTDCViewController;
import bancomer.api.domiciliaciontdc.implementations.InitDomiciliacionTDC;
import bancomer.api.domiciliaciontdc.io.Server;
import bancomer.api.domiciliaciontdc.models.OfertaDomiciliacionTDC;


public class DetalleDomiciliaTDCDelegate extends SecurityComponentDelegate implements BaseDelegate {

    public static final long DETALLE_DOMICILIACION_DELEGATE = 631451334266768526L;

    private OfertaDomiciliacionTDC ofertaDomiciliacionTDC;
    private DetalleDomiciliaTDCViewController controller;
    private InitDomiciliacionTDC init;

    private Constants.Operacion tipoOperacion;
    private Constants.TipoInstrumento tipoInstrumentoSeguridad;

    private String cuentaCargo;
    private String tipoPago;
    private String opcion;

    private String token;
    private String contrasena;
    private String nip;
    private String cvv;
    private String tarjeta;

    public DetalleDomiciliaTDCDelegate(OfertaDomiciliacionTDC ofertaDomiciliacionTDC  ){

        init = InitDomiciliacionTDC.getInstance();
        this.tipoOperacion = Constants.Operacion.oneClicDomiTDC;
        this.ofertaDomiciliacionTDC = ofertaDomiciliacionTDC;

        String instrumento = init.getSession().getSecurityInstrument();

        if (instrumento.equals(Constants.IS_TYPE_DP270)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.DP270;
        } else if (instrumento.equals(Constants.IS_TYPE_OCRA)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.OCRA;
        } else if (instrumento.equals(Constants.TYPE_SOFTOKEN.S1.value)) {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.SoftToken;
        } else {
            tipoInstrumentoSeguridad = Constants.TipoInstrumento.sinInstrumento;
        }
    }
    public void setController(DetalleDomiciliaTDCViewController controller) {
        this.controller = controller;
        setContext(controller);
    }

    public String getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(String tipoPago) {
        this.tipoPago = tipoPago;
    }

    public OfertaDomiciliacionTDC getOfertaDomiciliacionTDC() {
        return ofertaDomiciliacionTDC;
    }

    public String getCuentaCargo() {
        return cuentaCargo;
    }

    public void setCuentaCargo(String cuentaCargo) {
        this.cuentaCargo = cuentaCargo;
    }

    public void setOfertaDomiciliacionTDC(OfertaDomiciliacionTDC ofertaDomiciliacionTDC) {
        this.ofertaDomiciliacionTDC = ofertaDomiciliacionTDC;
    }

    public Constants.TipoInstrumento getTipoInstrumentoSeguridad() {
        return tipoInstrumentoSeguridad;
    }

    //obtiene las cuentas de tipo AH, LI y CH
    public ArrayList<HostAccounts> obtieneCuentas(){
        ArrayList<HostAccounts> accountsArray = new ArrayList<HostAccounts>();
        HostAccounts[] accounts = init.getListaCuentas();

        for(HostAccounts acc : accounts) {
            if(acc.getType().equals(Constants.SAVINGS_TYPE)||acc.getType().equals(Constants.CHECK_TYPE)
                    ||acc.getType().equals(Constants.LIBRETON_TYPE)) {
                accountsArray.add(acc);
            }
        }
        return accountsArray;
    }

    /** ordena las cuentas y pone en la posicion 0 la cuenta cargo que viene por default
    * en la oferta de domiciliacion, en caso de no tener en el arreglo de cuentas la cuenta cargo
    * se obtiene la cuenta eje y es la que se setea en la posicion 0
    */

    public ArrayList<HostAccounts> cargaCuentasOrdenadas(){
        ArrayList<HostAccounts> accountsArray = obtieneCuentas();
        ArrayList<HostAccounts> accArrayOrdenado = new ArrayList<HostAccounts>();
        HostAccounts accountCargo = null;
        HostAccounts accountEje = null;

        accountEje = Tools.obtenerCuentaEje(init.getListaCuentas());

        for(HostAccounts acc : accountsArray) {
            if(acc.getNumber().equals(getOfertaDomiciliacionTDC().getCuentaCargo())) {
                accountCargo = acc;
            }
            else if(accountEje.getNumber().equals(acc.getNumber())){
            }
            else{
                accArrayOrdenado.add(acc);
            }


        }

        if(accountCargo != null) {
            accArrayOrdenado.add(0, accountCargo);

            if(accountEje.equals(accountCargo)){
            }
            else if(accountEje != null)
                    accArrayOrdenado.add(accountEje);

        } else
            accArrayOrdenado.add(0, accountEje);

        /*
        Log.e("accArrayOrdenado", "accArrayOrdenado");
        for(HostAccounts acc : accArrayOrdenado){
            Log.e("cuenta", acc.getAlias());
            Log.e("cuenta", acc.getNumber());
            Log.e("cuenta", String.valueOf(acc.getBalance()));
            Log.e("cuenta", acc.getType());
        }*/

        return accArrayOrdenado;
    }


    /**
     * metodo que envia la peticion a la clase BaseSubaplication
     * @param operationId
     * @param params
     * @param caller
     * @param isJson
     * @param handler
     */
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller,
                                   final boolean isJson,final ParsingHandler handler) {
        InitDomiciliacionTDC.getInstance().getBaseSubApp().invokeNetworkOperation(operationId,params,caller,false,isJson,handler);
    }

    @Override
    public void doNetworkOperation(int operationId, Hashtable<String, ?> params, BaseViewController caller) {
        //metodo vacio
    }

    @Override
    public void analyzeResponse(int operationId, ServerResponse response) {
        if(operationId == Server.OFERTA_DOMICILIACION)
        {
            ofertaDomiciliacionTDC = (OfertaDomiciliacionTDC)response.getResponse();

            if(response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL)
            {
                if(ofertaDomiciliacionTDC.getPM() != null)
                {
                    if(ofertaDomiciliacionTDC.getArrayPromociones() != null) {
                        Log.e("Promociones", ofertaDomiciliacionTDC.getArrayPromociones().toString());
                        init.getSession().setPromocion(ofertaDomiciliacionTDC.getArrayPromociones());
                    }else{
                        init.getSession().setPromocion(null);
                    }
                }
                init.getParentManager().setActivityChanging(true);
                init.showFormalizaDomiciliacion(ofertaDomiciliacionTDC);

            }
            else if(response.getStatus() == ServerResponse.OPERATION_ERROR)
            {
                try
                {

                    if(!ofertaDomiciliacionTDC.getPM().equals(""))
                    {
                        if(ofertaDomiciliacionTDC.getArrayPromociones() != null) {
                            Log.e("Promociones", ofertaDomiciliacionTDC.getArrayPromociones().toString());
                            init.getSession().setPromocion(ofertaDomiciliacionTDC.getArrayPromociones());
                        }else{
                            init.getSession().setPromocion(null);
                        }
                        //Log.e("Mensaje", response.getMessageText().toString());
                        DialogInterface.OnClickListener clickListener=new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                init.getParentManager().setActivityChanging(true);
                                init.getParentManager().showMenuInicial();
                                init.resetInstance();
                            }
                        };
                        init.getBaseViewController().showInformationAlert(controller, response.getMessageText(), clickListener);
                    }
                    else
                    {
                        init.getBaseViewController().showInformationAlert(controller, response.getMessageText());
                    }

                }
                catch (NullPointerException ex)
                {
                    init.getBaseViewController().showErrorMessage(controller,response.getMessageText());
                }
            }else
            {
                init.getBaseViewController().showInformationAlert(controller, response.getMessageText());
            }
        }


    }

    @Override
    public void performAction(Object obj) {

        opcion = ConstantsDomicilia.FORMALIZACION;

        String cadAutenticacion = init.getAutenticacion().getCadenaAutenticacion(this.tipoOperacion, init.getSession().getClientProfile());
        String user = init.getSession().getUsername();
        String ium  = init.getSession().getIum();

        Hashtable<String, String> params = new Hashtable<String, String>();
        params.put("opcion", opcion);
        params.put("numeroCelular", user);
        params.put("cveCamp", init.getOfertaDomiciliacion().getCveCamp());
        params.put("IUM", ium);
        params.put("cuentaCargo", getCuentaCargo());
        params.put("tipoCargo", getTipoPago());


        params.put("codigoOTP", (token != null)? token:"");
        params.put("cadenaAutenticacion", cadAutenticacion);
        params.put("codigoNip", (nip != null)? nip:"");
        params.put("codigoCVV2", (cvv != null)? cvv:"");
        params.put("Tarjeta5ig", (tarjeta != null)? tarjeta:"");


        doNetworkOperation(Server.OFERTA_DOMICILIACION, params, InitDomiciliacionTDC.getInstance().getBaseViewController(),true,new OfertaDomiciliacionTDC());

    }

    @Override
    public long getDelegateIdentifier() {
        return 0;
    }


    /////// Methods for Security components showing. /////////


    public boolean mostrarContrasenia() {
        return init.getAutenticacion().mostrarContrasena(this.tipoOperacion,
                init.getSession().getClientProfile());
    }

    public boolean mostrarNIP() {
        return init.getAutenticacion().mostrarNIP(this.tipoOperacion,
                init.getSession().getClientProfile());
    }

    public boolean mostrarCampoTarjeta(){
        return false;
    }

    public Constants.TipoOtpAutenticacion tokenAMostrar() {
        Constants.TipoOtpAutenticacion tipoOTP;
        try {
            tipoOTP = init.getAutenticacion().tokenAMostrar(this.tipoOperacion,
                    init.getSession().getClientProfile());

        } catch (Exception ex) {
            if(Server.ALLOW_LOG) Log.e(this.getClass().getName(),
                    "Error on Autenticacion.mostrarNIP execution.", ex);
            tipoOTP = null;
        }

        return tipoOTP;
    }

    public boolean mostrarCVV() {
        return init.getAutenticacion().mostrarCVV(this.tipoOperacion,
                init.getSession().getClientProfile());
    }




    public boolean enviaPeticionOperacion() {
        String asm = null;
        if(controller instanceof DetalleDomiciliaTDCViewController){
            if (mostrarContrasenia()) {
                contrasena = controller.pideContrasena();
                if (contrasena.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteContrasena);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (contrasena.length() != Constants.PASSWORD_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.PASSWORD_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteContrasena);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            if(mostrarCampoTarjeta()){
                tarjeta = controller.pideTarjeta();
                String mensaje = "";
                if(tarjeta.equals("")){
                    mensaje = controller.getString(R.string.confirmation_componente_digitos_tarjeta_alerta);
                    init.getBaseViewController().showInformationAlert(controller,mensaje);
                    return false;
                }else if(tarjeta.length() != 5){
                    mensaje =  controller.getString(R.string.confirmation_componente_digitos_tarjeta_alerta);
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            if (mostrarNIP()) {
                nip = controller.pideNIP();
                if (nip.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteNip);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (nip.length() != Constants.NIP_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.NIP_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteNip);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }
            if (tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno) {
                asm = controller.pideASM();
                if (asm.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    switch (tipoInstrumentoSeguridad) {
                        case OCRA:
                            mensaje += getEtiquetaCampoOCRA();
                            break;
                        case DP270:
                            mensaje += getEtiquetaCampoDP270();
                            break;
                        case SoftToken:
                            if (init.getSofTokenStatus()) {
                                mensaje += getEtiquetaCampoSoftokenActivado();
                            } else {
                                mensaje += getEtiquetaCampoSoftokenDesactivado();
                            }
                            break;
                        default:
                            break;
                    }
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (asm.length() != Constants.ASM_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.ASM_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    switch (tipoInstrumentoSeguridad) {
                        case OCRA:
                            mensaje += getEtiquetaCampoOCRA();
                            break;
                        case DP270:
                            mensaje += getEtiquetaCampoDP270();
                            break;
                        case SoftToken:
                            if (init.getSofTokenStatus()) {
                                mensaje += getEtiquetaCampoSoftokenActivado();
                            } else {
                                mensaje += getEtiquetaCampoSoftokenDesactivado();
                            }
                            break;
                        default:
                            break;
                    }
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }
            if (mostrarCVV()) {
                cvv = controller.pideCVV();
                if (cvv.equals("")) {
                    String mensaje = controller.getString(R.string.confirmation_valorVacio);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteCvv);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                } else if (cvv.length() != Constants.CVV_LENGTH) {
                    String mensaje = controller.getString(R.string.confirmation_valorIncompleto1);
                    mensaje += " ";
                    mensaje += Constants.CVV_LENGTH;
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_valorIncompleto2);
                    mensaje += " ";
                    mensaje += controller.getString(R.string.confirmation_componenteCvv);
                    mensaje += ".";
                    init.getBaseViewController().showInformationAlert(controller, mensaje);
                    return false;
                }
            }

            String newToken = null;
            if(tokenAMostrar() != Constants.TipoOtpAutenticacion.ninguno && tipoInstrumentoSeguridad == Constants.TipoInstrumento.SoftToken && init.getSofTokenStatus())
                newToken = init.getDelegateOTP().loadOtpFromSofttoken(tokenAMostrar(),init.getDelegateOTP());
            if(null != newToken)
                asm = newToken;
            token=asm;
            return true;
        }
        return false;
    }
}
