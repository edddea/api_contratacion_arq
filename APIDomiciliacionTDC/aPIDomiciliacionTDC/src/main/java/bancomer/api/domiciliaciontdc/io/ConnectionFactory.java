package bancomer.api.domiciliaciontdc.io;

import android.content.Context;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.Hashtable;
import java.util.Map;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.io.ParsingHandler;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;
import suitebancomer.aplicaciones.commservice.service.ICommService;


public class ConnectionFactory {

    private int operationId;
    private Hashtable<String, ?> params;
    private Object responseObject;
    private ParametersTO parameters;
    private boolean isJson;
    private Context context;
    private ApiConstants.isJsonValueCode isJsonValueCode;

    public ConnectionFactory(final int operationId, final Hashtable<String, ?> params,
                             final boolean isJson, final Object responseObject,Context context,final ApiConstants.isJsonValueCode isJsonValueCode) {
        super();
        this.operationId = operationId;
        this.params = params;
        this.responseObject = responseObject;
        this.isJson = isJson;
        this.isJsonValueCode = isJsonValueCode;
        this.context=context;
    }

    /**
     * realiza la peticon a server
     * @return
     * @throws Exception
     */
    public IResponseService processConnectionWithParams() throws Exception {

        final Integer opId = Integer.valueOf(operationId);
        parameters = new ParametersTO();
        parameters.setSimulation(Server.SIMULATION);
        if (!Server.SIMULATION) {
            parameters.setProduction(!Server.DEVELOPMENT);
            parameters.setDevelopment(Server.DEVELOPMENT);
        }
        parameters.setOperationId(opId.intValue());
        final Hashtable<String, String> mapa= new Hashtable<String, String>();
        for (final Map.Entry<String, ?> entry : params.entrySet()) {
            final String key = entry.getKey();
            final Object value = entry.getValue();
            mapa.put(key, (String) (value==null?"":value));
        }
        //
        parameters.setParameters(mapa);
        parameters.setJson(isJson);
        parameters.setIsJsonValue(this.isJsonValueCode);
        IResponseService resultado = new ResponseServiceImpl();
        final ICommService serverproxy = new CommServiceProxy(context);
        try {
            resultado = serverproxy.request(parameters,
                    responseObject == null ? new Object().getClass()
                            : responseObject.getClass());
        } catch (UnsupportedEncodingException e) {
            throw new Exception(e);
        } catch (ClientProtocolException e) {
            throw new Exception(e);
        } catch (IllegalStateException e) {
            throw new Exception(e);
        } catch (IOException e) {
            throw new Exception(e);
        } catch (JSONException e) {
            throw new Exception(e);
        }

        return resultado;

    }

    /**
     * convierte la respuesta del server del tipo IResponseService
     * a uns ServerResponseImpl originario del api
     * @param resultado
     * @param handler
     * @return
     * @throws Exception
     */
    public ServerResponseImpl parserConnection(final IResponseService resultado,
                                               final ParsingHandler handler) throws Exception {
        ServerResponseImpl response=null;
        ParsingHandler handlerP=null;
        if (ApiConstants.OPERATION_SUCCESSFUL == resultado.getStatus()) {
            if(resultado.getObjResponse() instanceof ParsingHandler){
                handlerP= (ParsingHandler) resultado.getObjResponse();
            }
            if (parameters.isJson() ) {
                if (handler != null) {
                    final ParserJSON parser = new ParserJSONImpl(
                            resultado.getResponseString());
                    try {
                        if(handlerP==null){
                            handlerP=handler;
                        }
                        handlerP.process(parser);
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        throw new Exception(e);
                    } catch (ParsingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        throw new Exception(e);
                    }
                }
                response = new ServerResponseImpl(resultado.getStatus(),
                        resultado.getMessageCode(), resultado.getMessageText(), handlerP);
            } else {
                // Que pasa si no es JSON la respuesta
                Reader reader = null;
                try {
                    reader = new StringReader(resultado.getResponseString());
                    Parser parser = new ParserImpl(reader);
                    if (handler != null) {
                        if(handlerP==null){
                            Class<?> clazz = handler.getClass();
                            //Constructor<?> ctor = clazz.getConstructor();
                            //handlerP = (ParsingHandler) ctor.newInstance();
                            handlerP = (ParsingHandler)clazz.cast(handler);
                        }
                        response= new ServerResponseImpl(handlerP);
                        response.process(parser);
                    }else{
                        response= new ServerResponseImpl(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),null);
                        response.process(parser);
                    }
                }catch(Exception e){
                    throw new Exception(e);

                }finally {
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (Throwable ignored) {
                            throw new Exception(ignored);
                        }
                    }
                }
            }

        }else{
            try {
                handlerP=null;
                response= new ServerResponseImpl(resultado.getStatus(),resultado.getMessageCode(),resultado.getMessageText(),null);
            } catch (Exception e) {
                // TODO: handle exception
                throw new Exception(e);
            }

        }

        return response;
    }

}

