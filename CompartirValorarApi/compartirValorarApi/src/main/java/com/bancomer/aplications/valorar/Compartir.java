package com.bancomer.aplications.valorar;

import android.content.Context;
import android.content.Intent;

import com.bancomer.constants.compartirvalorarapi.Constants;
import com.bancomer.mbanking.compartirvalorarapi.R;

import java.util.ArrayList;

import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

/**
 * Created by alanmichaelgonzalez on 09/11/15.
 */
public class Compartir {
    final Context context;
    String msjCompartir;
    String msjAsunto;

    public ArrayList<String> estados = new ArrayList<String>();

    /**
     * constructor
     * @param context
     */
    public Compartir(final Context context){
        this.context=context;
    }

    public void startCompartir(final Constants.Aplicaciones aplicacionAValorar){
        getMessageCompartir(aplicacionAValorar);

        TrackingHelper.trackState("compartir", estados);

        final Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, msjCompartir);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, msjAsunto);
        sendIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(sendIntent, context.getResources().getText(R.string.send_to)));
    }

    /**
     * obtienen el mensaje a compartir
     * @param aplicacionAValorar
     */
    private void getMessageCompartir(final Constants.Aplicaciones aplicacionAValorar){
        if(Constants.Aplicaciones.BMOVIL==aplicacionAValorar){
            msjCompartir=context.getString(R.string.compartir_bmovil) + context.getString(R.string.url_bbva_bmovil);
            msjAsunto=context.getString(R.string.asunto_mail_bmovil);
        }else if(Constants.Aplicaciones.LINEABANCOMER==aplicacionAValorar){
            msjCompartir=context.getString(R.string.compartir_linea_bancomer) + context.getString(R.string.url_linea_Bancomer);
            msjAsunto=context.getString(R.string.asunto_mail_linea_bancomer);
        }else if(Constants.Aplicaciones.VIDABANCOMER==aplicacionAValorar){
            msjCompartir=context.getString(R.string.compartir_vida_bancomer) + context.getString(R.string.url_vida_bancomer);
            msjAsunto=context.getString(R.string.asunto_mail_vida_bancomer);
        }else if(Constants.Aplicaciones.WALLET==aplicacionAValorar){
            msjCompartir=context.getString(R.string.compartir_wallet)+  context.getString(R.string.url_bbva_wallet);
            msjAsunto=context.getString(R.string.asunto_mail_wallet);
        }
    }
}
