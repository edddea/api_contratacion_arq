package com.bancomer.mbanking.compartirvalorarapi;

import android.content.Context;

import com.bancomer.aplications.valorar.Compartir;
import com.bancomer.aplications.valorar.Valorar;
import com.bancomer.constants.compartirvalorarapi.Constants;
import com.bancomer.utils.compartirvalorarapi.Tools;

/**
 * Created by alanmichaelgonzalez on 09/11/15.
 */
public class InitValorarCompartir {

    private final Context context;

    /**
     * constructor
     * @param context
     */
    public InitValorarCompartir(final Context context){
        this.context=context;
    }

    /**
     * metodo para inicializar la clase valorar
     * @param aplicacionAValorar
     */
    public void startValorar(final Constants.Aplicaciones aplicacionAValorar){
        Tools.muestraIndicadorActividad(context.getString(R.string.alert_operation),context.getString(R.string.alert_connecting), context);
        final Valorar val=new Valorar(context);
        val.startValorar(aplicacionAValorar);
        Tools.ocultaIndicadorActividad();
    }

    /**
     * metodo para inicializar la clase compartir
     * @param aplicacionAValorar
     */
    public void startCompartir(final Constants.Aplicaciones aplicacionAValorar){
        Tools.muestraIndicadorActividad(context.getString(R.string.alert_operation), context.getString(R.string.alert_connecting), context);
        final Compartir com=new Compartir(context);
        com.startCompartir(aplicacionAValorar);
        Tools.ocultaIndicadorActividad();
    }
}
