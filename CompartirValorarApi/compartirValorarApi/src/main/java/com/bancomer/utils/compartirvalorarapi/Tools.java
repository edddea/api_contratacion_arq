package com.bancomer.utils.compartirvalorarapi;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;

/**
 * Created by alanmichaelgonzalez on 09/11/15.
 */
public final class Tools {

    private Tools(){
        //constructor
    }
    /**
     * Metodo para detectar si una app esta instalada o no
     *
     * @param uri
     * @return
     */
    public static boolean appInstalled(final String uri, final Context context) {
        final PackageManager pm = context.getPackageManager();
        boolean appInstalled = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            appInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            appInstalled = false;
        }
        return appInstalled;
    }
    /**
     * A progress dialog for long waiting processes.
     */
    protected static ProgressDialog mProgressDialog;

    /**
     * Places a progress dialog, for long waiting processes.
     *
     * @param strTitle the dialog title
     * @param strMessage the message to show in the while
     */
    public static void muestraIndicadorActividad(final String strTitle,final String strMessage,final Context context) {
        if(mProgressDialog != null){
            ocultaIndicadorActividad();
        }
        if(context == null) {
            return;
        }
            mProgressDialog = ProgressDialog.show(context, strTitle,
                    strMessage, true);

        mProgressDialog.setCancelable(false);
    }

    /* Hides the progress dialog.
     */
    public static void ocultaIndicadorActividad() {
        if(mProgressDialog != null){
            mProgressDialog.dismiss();
        }
    }
}
