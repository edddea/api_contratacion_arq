package suitebancomer.activacion.aplicaciones.bmovil.classes.gui.controllers;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bancomer.mbanking.activacion.R;
import com.bancomer.mbanking.activacion.SuiteApp;

import suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate;
import suitebancomer.activacion.classes.common.GuiTools;
import suitebancomer.activacion.classes.gui.controllers.BaseViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Reactivacion;
import tracking.activacion.TrackingHelper;

public class ActivacionViewController extends BaseViewController implements OnClickListener {

	private TextView mTexto;
	private EditText mCodigo;
	private Button mBoton;
	private ImageButton btnLlamar;
	private ActivacionDelegate delegate;
	//AMZ
			public BmovilViewsController parentManager;
			//AMZ

	
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER | SHOW_TITLE, SuiteApp.getResourceId("layout_bmovil_activacion_act","layout"));
		com.bancomer.base.SuiteApp.appContext = this;


		setTitle(SuiteApp.getResourceId("activacion.reactivacion.title","string"), SuiteApp.getResourceId("icono_activacion","drawable"));
		//AMZ
		parentManager = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
	
		TrackingHelper.trackState("activacion", parentManager.estados);
		//int ultimo = parentManager.estados.size()-1;
		//final String rem = parentManager.estados.remove(ultimo);
		//rem = parentManager.estados.remove(ultimo-1);
	
		setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
		delegate=(ActivacionDelegate) getParentViewsController().getBaseDelegateForKey(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
		setDelegate(delegate);
		SuiteApp.getInstance().getSuiteViewsController().setCurrentActivityApp(this);
		//SuiteApp.getInstance().getSuiteViewsController().setCurrentActivityApp(new MenuSuiteViewController());
		
		findViews();
		scaleToScreenSize();
		inicializarPantalla();
		this.statusdesactivado = false;
		delegate.setViewController(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		parentViewsController.consumeAccionesDePausa();
		this.statusdesactivado = true;
		SuiteApp.setCallsAppDesactivada(Boolean.FALSE);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		com.bancomer.base.SuiteApp.appContext = this;
		if(statusdesactivado)
		if (parentViewsController.consumeAccionesDeReinicio()) {
			return;
		}
		getParentViewsController().setCurrentActivityApp(this);
	}

	@Override
	public void goBack() {
		super.goBack();
	}

	private void findViews(){
		mTexto = (TextView)findViewById(SuiteApp.getResourceId("activacion_texto", "id"));
		mCodigo = (EditText)findViewById(SuiteApp.getResourceId("activacion_usuario", "id")); 
		mBoton = (Button)findViewById(SuiteApp.getResourceId("activacion_boton", "id"));
		btnLlamar = (ImageButton)findViewById(SuiteApp.getResourceId("btnSolicitaCodigo", "id"));

		mBoton.setEnabled(false);

	}
	
	
	private void scaleToScreenSize(){
		final GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(mTexto, true);
		guiTools.scale(mCodigo, true);
		guiTools.scale(mBoton);
		guiTools.scale(findViewById(SuiteApp.getResourceId("linkReenviarCodigo", "id")));
		
		guiTools.scale(findViewById(SuiteApp.getResourceId("pagarServicios_cuentaOrigen", "id")));
		
	}
	
	private void inicializarPantalla(){
		final InputFilter[] userFilterArray = new InputFilter[1];
		userFilterArray[0] = new InputFilter.LengthFilter(Constants.ACTIVATION_CODE_LENGTH);
		mCodigo.setFilters(userFilterArray);		
		mBoton.setOnClickListener(this);
		btnLlamar.setOnClickListener(this);
		mTexto.setText("Clave de Activación");

		mCodigo.addTextChangedListener(new TextWatcher() {

				   @Override
				   public void onTextChanged(final CharSequence cs, final int arg1, final int arg2, final int arg3) {
				   }

				   @Override
				   public void beforeTextChanged(final CharSequence arg0, final int arg1, final int arg2, final int arg3) {
				   }

				   @Override
				   public void afterTextChanged(final Editable arg0) {
					   mBoton.setEnabled((mCodigo.getText().toString().length() == 10));

				   }
		});

	}
	
	@Override
	public void onClick(final View arg0) {
		if(arg0.equals(mBoton)){
			if(validaDatos())
			delegate.realizaActivacion();
		}
		if(arg0.equals(btnLlamar)){
			final Intent sIntent=new Intent(Intent.ACTION_CALL,Uri.parse(String.format("tel:%s", Uri.encode(this.getString(R.string.activacion_numero_solicita_codigo)))));
			sIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(sIntent);
		}
	}
	
	boolean validaDatos(){
		return delegate.validaDatos(mCodigo.getText().toString());		
	}	
	
	@Override
	public void processNetworkResponse(final int operationId, final ServerResponse response) {
		if("CNE0365".equals(response.getMessageCode())){
			showCodigoIncorrecto();
		}else{
			delegate.analyzeResponse(operationId, response);
		}
	}
	
	public String getClaveActivacion(){
		return mCodigo.getText().toString();
	}

	public void onLinkReenviarCodigo(final View sender) {
		//finish();
		//if(delegate.getPassword() == null) {
		final Reactivacion reactivacion = delegate.getReactivacion();
		//SuiteApp.getShowReactivacion().showReactivacion(reactivacion);
		delegate.confirmacionCambioCompania(reactivacion);
		//} else {
		//	ConsultaEstatus consultaEstatus = delegate.getcEstatus();
		//	SuiteApp.getShowReactivacion().showReactivacion(consultaEstatus, delegate.getPassword());
		//}
	}

	public void showCodigoIncorrecto() {
		final Dialog contactDialog = new Dialog(this);
		final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		final View layout = inflater.inflate(R.layout.codigo_incorrecto,null);
		contactDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// make dialog itself transparent
		contactDialog.setCancelable(false);
		contactDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		contactDialog.setContentView(layout);

		final int width = (int)(getResources().getDisplayMetrics().widthPixels * 0.90);
		contactDialog.getWindow().setLayout(width, DrawerLayout.LayoutParams.WRAP_CONTENT);

		final Button intentarNuevamente = (Button) layout.findViewById(R.id.btnintentarnuevamente);
		final Button nuevoCodigo = (Button) layout.findViewById(R.id.btnnuevocodigo);


		//contactDialog.setTitle(ctx.getString(R.string.menuSuite_menuTitle));
		intentarNuevamente.setHeight(nuevoCodigo.getHeight());

		intentarNuevamente.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(final View v) {
						intentarNuevamente();
						contactDialog.dismiss();
					}
				}
		);
		nuevoCodigo.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(final View v) {
						generarNuevoCodigo();
						contactDialog.dismiss();
					}
				}
		);
		contactDialog.show();
	}

	private void generarNuevoCodigo(){
		final Reactivacion reactivacion = delegate.getReactivacion();
		delegate.generarNuevoCodigo(reactivacion);
	}

	private void intentarNuevamente(){
		mCodigo.setText("");
		mBoton.setEnabled(false);
	}
}
