package suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.bancomer.mbanking.activacion.R;
import com.bancomer.mbanking.activacion.SuiteApp;

import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import suitebancomer.activacion.aplicaciones.bmovil.classes.gui.controllers.ActivacionViewController;
import suitebancomer.activacion.classes.gui.controllers.BaseViewController;
import suitebancomer.activacion.classes.gui.delegates.BaseDelegate;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomer.aplicaciones.keystore.KeyStoreWrapper;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.SessionStoredListener;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.io.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ActivacionResult;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Reactivacion;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;

public class ActivacionDelegate extends BaseDelegate implements SessionStoredListener {

	private Reactivacion reactivacion;
	private ActivacionViewController viewController;
	private ConsultaEstatus cEstatus;
	public static final long ACTIVACION_DELEGATE_ID = -5967914594309368287L;

	private String password;
	//private LoginDelegate loginDelegate;
	private boolean borrarDatos; 
	private ActivacionResult serverResponse;
	
	private BaseViewController currentMenu;
	
	public ActivacionDelegate(final Reactivacion r) {
		reactivacion = r;
		//loginDelegate = null;
		borrarDatos = false;
		serverResponse = null;
	}
	
	public ActivacionDelegate(final ConsultaEstatus ce, final String password){
		reactivacion = new Reactivacion();
		cEstatus = ce;
		this.password = password;
		reactivacion.setCompaniaCelular((null == cEstatus.getCompaniaCelular()) ? "" : cEstatus.getCompaniaCelular());
		reactivacion.setEstatus(cEstatus.getEstatus());
		reactivacion.setNumCelular(cEstatus.getNumCelular());
		reactivacion.setCompaniaCelular(cEstatus.getCompaniaCelular());
		reactivacion.setNumCliente(cEstatus.getNumCliente());
		reactivacion.setPerfil(cEstatus.getPerfil());
		reactivacion.setPerfilAST(cEstatus.getPerfilAST());
		//loginDelegate = null;
		borrarDatos = false;
		serverResponse = null;
	}

	/**
	 * Valida que la longitud de la clave d activacion sea la correcta
	 * @param cve
	 * @return
	 */
	public boolean validaDatos(final String cve){
		final boolean esOK = (cve.length() == 0);
		if(esOK)
			viewController.showInformationAlert(R.string.bmovil_activacion_validacioncve);
		
		return !esOK;		
	}
	
	/**
	 * 
	 */
	public void realizaActivacion() {
		final Session session = Session.getInstance(SuiteApp.appContext);
		final String username = reactivacion.getNumCelular();
		long seed = session.getSeed();
		if (seed == 0) {
			seed = System.currentTimeMillis();
			session.setSeed(seed);
		}

		final String ium = Tools.buildIUM(username, seed, SuiteApp.appContext);
		session.setIum(ium);
		session.setUsername(username);

		final String cveActivacion = viewController.getClaveActivacion();
		final int operationId = Server.OP_ACTIVACION;
		final Hashtable<String, String> params = new Hashtable<String, String>();
		params.put("NT", username);
		params.put("IU", ium);
		params.put("AC", cveActivacion == null ? "" : cveActivacion);
		params.put("PR", reactivacion.getPerfilAST());
		params.put("order","NT*IU*AC*PR");


		final List<String> listaEncriptar = Arrays.asList(
				"OP", "NT", "IU", "AC", "PR"
		);
		CommContext.operacionCode = operationId;
		CommContext.listaEncriptar = listaEncriptar;

		//JAIG
		doNetworkOperation(operationId, params,false,new ActivacionResult(), viewController);
	}

	/**
	 * Realiza la invocacion a server
	 */
	@Override
	public void doNetworkOperation(final int operationId, final Hashtable<String, ?> params, final boolean isJson, final ParsingHandler handler, final BaseViewControllerCommons caller) {
		SuiteApp.getInstance().getBmovilApplication().invokeNetworkOperation(operationId, params,isJson,handler, caller);
	}
	
	/**
	 * analiza la respuesta generada por el server
	 */
	@Override
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
			if(Server.OP_ACTIVACION == operationId) {
				activacionExitosa((ActivacionResult) response.getResponse());
			} //else if(Server.LOGIN_OPERATION == operationId) {
				//loginDelegate.analyzeResponse(operationId, response);
			//}
		} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				final BaseViewControllerCommons current = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp();
				current.showInformationAlert(response.getMessageText());
		}
	}
	
	/**
	 * 
	 * @param ac
	 */
	private void activacionExitosa(final ActivacionResult ac){
		serverResponse = ac;
    	viewController.muestraIndicadorActividad("", viewController.getString(R.string.alert_StoreSession));
		final Session session = Session.getInstance(SuiteApp.appContext);
		final String date = ac.getFecha();
		final  String time = ac.getHora();
        session.setServerDateTime(date, time);
        session.setApplicationActivated(true);
        
        //Integración KeyChainAPI
		final KeyStoreWrapper kswrapper = session.getKeyStoreWrapper();
        
        try {
        	
        	kswrapper.setUserName(session.getUsername());  //setEntry(Constants.USERNAME, session.getUsername());
        	kswrapper.setSeed(String.valueOf(session.getSeed()));  //setEntry(Constants.SEED, String.valueOf(session.getSeed()));
        	kswrapper.storeValueForKey(Constants.CENTRO, Constants.BMOVIL);  //setEntry(Constants.CENTRO, Constants.BMOVIL);
        	
		} catch (Exception e) {
			if(ServerCommons.ALLOW_LOG) {
				Log.e(this.getClass().getName(), e.getMessage());
			}
		}
       
        session.storeSession(this);
    }
	
	@Override
	public void sessionStored() {

		
//		if(!Tools.isFirstActivationStored() && 
//			estatus.equals(Constants.STATUS_PENDING_ACTIVATION) && 
//			!Constants.PROFILE_ADVANCED_03.equals(profile)) {
//			
//			Calendar dateTime = Calendar.getInstance();
//			
//			int fieldValue;
//			StringBuilder builder = new StringBuilder();
//			
//			fieldValue = dateTime.get(Calendar.YEAR);
//			builder.append(fieldValue);
//			fieldValue = dateTime.get(Calendar.MONTH);
//			builder.append(fieldValue);
//			fieldValue = dateTime.get(Calendar.DAY_OF_MONTH);
//			builder.append(fieldValue);
//			
//			
//			
//			
//			
//			
//			Tools.storeFirstActivationDate(serverResponse.getFecha(), serverResponse.getHora());
//		}
		
		if(borrarDatos){
			currentMenu.ocultaIndicadorActividad();
			SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showActivacion(cEstatus, password);
			return;
		}
		Server.CODIGO_ESTATUS_APLICACION = Constants.STATUS_APP_ACTIVE;
		//Session session = Session.getInstance(SuiteApp.appContext);
		viewController.ocultaIndicadorActividad();
		viewController.runOnUiThread(new Runnable(){
			@Override
			public void run() {
				viewController.showInformationAlert(R.string.bmovil_activacion_alert_titulo, 
													R.string.bmovil_activacion_alert_final,	
													new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog,	int which) {
						dialog.dismiss();
						if (password != null) {
//							// ejecuta caso de uso Login, escenario
//							// Principal Paso 7
//							loginDelegate = new LoginDelegate();
//							loginDelegate.setParentViewController(viewController);
//							loginDelegate.login(reactivacion.getNumCelular(),	password);
							SuiteApp.getShowReactivacion().returnLogin(reactivacion.getNumCelular(), password);
						} else {
							// caso de uso login escenario principal
							// paso 2 
							//SuiteApp.appContext.startActivity(SuiteApp.getIntentToReturn());
							//SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(viewController);
							//viewController.finish();
							SuiteApp.getShowReactivacion().returnObjectFromApi(null);
//							SuiteApp.getInstance().getSuiteViewsController().showViewController(SuiteApp.getInstance().getIntentToReturn(),0,true, new String[]{},new Object[]{});
							
							//SuiteApp.getInstance().getSuiteViewsController().showMenuSuite(true, new String[] { "bmovilselected" });
						}
					}
				});
			}
		});
	}

	public void setViewController(final ActivacionViewController activacionViewController) {
		viewController = activacionViewController;
		
	}

	/**
	 * @return the cEstatus
	 */
	public ConsultaEstatus getcEstatus() {
		return cEstatus;
	}

	/**
	 * @param cEstatus the cEstatus to set
	 */
	public void setcEstatus(final ConsultaEstatus cEstatus) {
		this.cEstatus = cEstatus;
	}

	/**
	 * @return the reactivacion
	 */
	public Reactivacion getReactivacion() {
		return reactivacion;
	}

	/**
	 * @param reactivacion the reactivacion to set
	 */
	public void setReactivacion(final Reactivacion reactivacion) {
		this.reactivacion = reactivacion;
	}
	
	public void borrarDatos(){
		borrarDatos = true;
		final Context appContext = SuiteApp.appContext;
		final Session session = Session.getInstance(appContext);
		final String telefono = session.getUsername();

		currentMenu = new ActivacionViewController();
		//currentMenu.muestraIndicadorActividad(SuiteApp.getIntentToReturn(), currentMenu, title, msg);
		//currentMenu.muestraIndicadorActividad(title, msg);
		
		session.clearSession();
		session.setApplicationActivated(false);
		session.setUsername(telefono);
		 //Integración KeyChainAPI
		final KeyStoreWrapper kswrapper = session.getKeyStoreWrapper();
        try {
        	kswrapper.setUserName(" ");  	//setEntry(Constants.USERNAME, " ");
        	kswrapper.setSeed(" ");  		//setEntry(Constants.SEED, " ");
        	kswrapper.storeValueForKey(Constants.CENTRO, " ");  //setEntry(Constants.CENTRO, " ");
			if(ServerCommons.ALLOW_LOG) {
				Log.i(this.getClass().getName(), "Eliminando datos de KeyChain...");
				Log.i("Key-> ", "UserName: " + kswrapper.getUserName());
				Log.i("Key->", "Seed: " + kswrapper.getSeed());
			}
		} catch (Exception e) { //------
			if(ServerCommons.ALLOW_LOG) {
				Log.e(this.getClass().getName(), e.getMessage());
			}
		}
		session.storeSession(this);
	}

	/**
	 * @return the currentMenu
	 */
	public BaseViewControllerCommons getCurrentMenu() {
		return currentMenu;
	}

	/**
	 * @param currentMenu the currentMenu to set
	 */
	public void setCurrentMenu(final BaseViewController currentMenu) {
		this.currentMenu = currentMenu;
	}


	public String getPassword() {
		return password;
	}

	public void confirmacionCambioCompania(final Reactivacion reactivacion){
		viewController.showYesNoAlert1(R.string.bmovil_activacion_alert_cambio_numero,
		new OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				dialog.dismiss();
				viewController.setHabilitado(true);
				dialog.dismiss();
				alertAcudeATM();
			}
		},
		new OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				SuiteApp.getShowReactivacion().showReactivacion(reactivacion);
			}
		});
	}

	public void generarNuevoCodigo(final Reactivacion reactivacion){
		SuiteApp.getShowReactivacion().showReactivacion(reactivacion);
	}

	public void alertAcudeATM(){
		viewController.showInformationAlert(R.string.bmovil_activacion_alert_acude_atm, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						SuiteApp.getShowReactivacion().returnObjectFromApi(null);
					}
				}
		);
	}
}
