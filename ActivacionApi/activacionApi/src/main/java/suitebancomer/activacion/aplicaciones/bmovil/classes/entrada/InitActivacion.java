package suitebancomer.activacion.aplicaciones.bmovil.classes.entrada;

import android.app.Activity;

import com.bancomer.base.callback.CallBackBConnect;
import com.bancomer.mbanking.activacion.SuiteApp;

import java.util.ArrayList;
import java.util.List;

import suitebancomer.activacion.aplicaciones.bmovil.classes.gui.controllers.ActivacionViewController;
import suitebancomer.activacion.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.model.BanderasServer;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Reactivacion;
import tracking.activacion.TrackingHelper;

/**
 * Created by evaltierrah on 12/08/2015.
 */
public class InitActivacion {

    public InitActivacion(final BanderasServer banderasServer, final Activity activity,
                          final CallBackBConnect callBackBConnect, final Activity intentReturn) {
        ServerCommons.ALLOW_LOG = banderasServer.getLogActivo();
        ServerCommons.DEVELOPMENT = banderasServer.isDesarrollo();
        ServerCommons.EMULATOR = banderasServer.getEmulador();
        ServerCommons.SIMULATION = banderasServer.getSimulacion();
        SuiteApp.setIntentToReturn(intentReturn);
        SuiteApp.setShowReactivacion(callBackBConnect);
        final SuiteApp suiteApp = new SuiteApp();
        suiteApp.onCreate(activity);
    }

    public void borraDatos(final ConsultaEstatus ce, final String password, final List<String> estados) {
        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().estados = (ArrayList<String>)estados;
        final BmovilViewsController bmovilViewsController = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
        ActivacionDelegate aDelegate = (ActivacionDelegate) bmovilViewsController.getBaseDelegateForKey(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
        if (aDelegate != null) {
            bmovilViewsController.removeDelegateFromHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
        }
        aDelegate = new ActivacionDelegate(ce,
                password);
        aDelegate.borrarDatos();
        bmovilViewsController.addDelegateToHashMap(
                ActivacionDelegate.ACTIVACION_DELEGATE_ID,
                aDelegate);
    }

    public void showActivacion(final ConsultaEstatus ce, final String password,
                               final List<String> estados) {
        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().estados = (ArrayList<String>)estados;
        final BmovilViewsController bmovilViewsController = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();

        ActivacionDelegate aDelegate = (ActivacionDelegate) bmovilViewsController.getBaseDelegateForKey(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
        if (aDelegate != null) {
            bmovilViewsController.removeDelegateFromHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
        }
        aDelegate = new ActivacionDelegate(ce, password);

        bmovilViewsController.addDelegateToHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID,
                aDelegate);
        //AMZ
        TrackingHelper.trackState("activacion", SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().estados);

        bmovilViewsController.showViewController(ActivacionViewController.class);

    }

    public void showActivacion(final Reactivacion reactivacion, final List<String> estados) {
        SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().estados = (ArrayList<String>)estados;
        final BmovilViewsController bmovilViewsController = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
        ActivacionDelegate aDelegate = (ActivacionDelegate) bmovilViewsController.getBaseDelegateForKey(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
        if (aDelegate != null) {
            bmovilViewsController.removeDelegateFromHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
        }
        aDelegate = new ActivacionDelegate(reactivacion);
        bmovilViewsController.addDelegateToHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID, aDelegate);

        TrackingHelper.trackState("activacion", SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().estados);

        bmovilViewsController.showViewController(ActivacionViewController.class);
    }

}
