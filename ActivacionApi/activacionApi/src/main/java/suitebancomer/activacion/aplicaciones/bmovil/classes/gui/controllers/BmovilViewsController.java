package suitebancomer.activacion.aplicaciones.bmovil.classes.gui.controllers;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;

import com.bancomer.mbanking.activacion.BmovilApp;
import com.bancomer.mbanking.activacion.R;
import com.bancomer.mbanking.activacion.SuiteApp;

import java.util.ArrayList;

import suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate;
import suitebancomer.activacion.classes.gui.controllers.BaseViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import tracking.activacion.TrackingHelper;

//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ContratoOfertaConsumoDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetalleOfertaConsumoDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetalleOfertaEFIDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DetalleOfertaILCDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ExitoEFIDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ExitoILCDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ExitoOfertaConsumoDelegate;


//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ModificarImporteDelegate;

//Retiro sin tarjeta
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.AltaRetiroSinTarjetaDelegate;
//import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConsultaRetiroSinTarjetaDelegate;


public class BmovilViewsController extends BaseViewsController {

	private BmovilApp bmovilApp;
	//AMZ
	public ArrayList<String> estados = new ArrayList<String>();

	
	/*
	 * @Override public void setCurrentActivity(BaseViewController
	 * currentViewController) { // TODO Auto-generated method stub
	 * super.setCurrentActivity(currentViewController); Log.i("Si", "No"); }
	 */
	public BmovilViewsController(final BmovilApp bmovilApp) {
		super();
		this.bmovilApp = bmovilApp;
	}

	public void cierraViewsController() {
		bmovilApp = null;
		clearDelegateHashMap();
		super.cierraViewsController();
	}

	public void cerrarSesionBackground() {
		//SuiteApp.getInstance().getBmovilApplication()
		//		.setApplicationInBackground(true);
		//bmovilApp.logoutApp(true);
	}



	public void showPantallaActivacion() {
		showViewController(ActivacionViewController.class,
				Intent.FLAG_ACTIVITY_NO_HISTORY);
	}

	public BmovilApp getBmovilApp() {
		return bmovilApp;
	}

	public void setBmovilApp(final BmovilApp bmovilApp) {
		this.bmovilApp = bmovilApp;
	}


//	public void showConsultarMovimientos() {
//		MovimientosDelegate delegate = (MovimientosDelegate) getBaseDelegateForKey(MovimientosDelegate.MOVIMIENTOS_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new MovimientosDelegate();
//			addDelegateToHashMap(MovimientosDelegate.MOVIMIENTOS_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(ConsultaMovimientosViewController.class);
//	}
	
//	public void showConsultarDepositosRecibidosCuenta(Account cuenta) {
//		DepositosRecibidosDelegate delegate = (DepositosRecibidosDelegate) getBaseDelegateForKey(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new DepositosRecibidosDelegate();
//			addDelegateToHashMap(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID,
//					delegate);
//		}
//		delegate.setCuentaActual(cuenta);
//		showViewController(ConsultarDepositosRecibidosViewController.class);
//	}
	
//	public void showConsultarDepositosRecibidos() {
//		DepositosRecibidosDelegate delegate = (DepositosRecibidosDelegate) getBaseDelegateForKey(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new DepositosRecibidosDelegate();
//			addDelegateToHashMap(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(ConsultarDepositosRecibidosViewController.class);
//	}
	
//	public void showConsultarObtenerComprobante() {
//		ObtenerComprobantesDelegate delegate = (ObtenerComprobantesDelegate) getBaseDelegateForKey(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new ObtenerComprobantesDelegate();
//			addDelegateToHashMap(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(ConsultarObtenerComprobantesViewController.class);
//	}


	/**
	 * Muestra la ventana de consultar envios de dinero movil.
	 */
//	public void showConsultarDineroMovil() {
//		DineroMovilDelegate delegate = (DineroMovilDelegate) getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new DineroMovilDelegate();
//			addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(ConsultarDineroMovilViewController.class);
//	}
	/**
	 * Muestra la pantalla para consultar Retiro sin tarjeta.
	 */
//	public void showConsultarRetiroSinTarjeta() {
//		ConsultaRetiroSinTarjetaDelegate delegate = (ConsultaRetiroSinTarjetaDelegate) getBaseDelegateForKey(ConsultaRetiroSinTarjetaDelegate.RETIRO_SIN_TARJETA_DELEGATE_ID);
//
//		if (delegate == null) {
//			delegate = new ConsultaRetiroSinTarjetaDelegate();
//			addDelegateToHashMap(
//					ConsultaRetiroSinTarjetaDelegate.RETIRO_SIN_TARJETA_DELEGATE_ID,
//					delegate);
//		}
//		//delegate.resetPeriodo();
//		showViewController(ConsultaRetiroSinTarjetaViewController.class);
//	}



	public void showCuentaOrigenListViewController() {
//		showViewController(CuentaOrigenListViewController.class);
	}

//	public void showDetalleMovimientosViewController() {
//		showViewController(DetalleMovimientosViewController.class);
//	}

	@Override
	public void onUserInteraction() {
		super.onUserInteraction();
	}

	@Override
	public boolean consumeAccionesDeReinicio() {
		if (!SuiteApp.getInstance().getBmovilApplication()
				.isApplicationLogged()) {
			SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().showViewController(SuiteApp.getIntentToReturn().getClass(), Intent.FLAG_ACTIVITY_NEW_TASK);
			
			return true;
		}
		return false;
	}

	@Override
	public boolean consumeAccionesDePausa() {
		if(SuiteApp.getShowReactivacion() != null) {
			SuiteApp.getShowReactivacion().returDesactivada();
		}
		return true;

	}

	@Override
	public boolean consumeAccionesDeAlto() {
		if (!SuiteApp.getInstance().getBmovilApplication().isChangingActivity()
				&& currentViewControllerApp != null) {
			currentViewControllerApp.hideSoftKeyboard();
		}
		SuiteApp.getInstance().getBmovilApplication()
				.setChangingActivity(false);
		return true;
	}

	
	public void showNovedadesLogin() {
//		showViewController(NovedadesViewController.class);
	}
	
	/*
	 * muestra la pantalla para cambiar password
	 */
//	public void showCambiarPassword() {
//		CambiarPasswordDelegate delegate = (CambiarPasswordDelegate) getBaseDelegateForKey(CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new CambiarPasswordDelegate();
//			addDelegateToHashMap(
//					CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(CambiarPasswordViewController.class);
//	}

	/*
	 * Muestra la pantalla con el resultado del cambio de password
	 */
//	public void showCambiarPasswordResultado() {
//		CambiarPasswordDelegate delegate = (CambiarPasswordDelegate) getBaseDelegateForKey(CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new CambiarPasswordDelegate();
//			addDelegateToHashMap(
//					CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(CambiarPasswordResultadoViewController.class);
//	}

	/**
	 * Muestra la pantalla de transferencia mis cuentas
	 */
//	public void showTransferirMisCuentas() {
//		TransferirMisCuentasDelegate delegate = (TransferirMisCuentasDelegate) getBaseDelegateForKey(TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new TransferirMisCuentasDelegate();
//			addDelegateToHashMap(
//					TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(TransferirMisCuentasSeleccionViewController.class);
//	}

	/**
	 * Muestra la pantalla de transferencia entre mis cuentas detalle
	 */
//	public void showTransferirMisCuentasDetalle() {
//		TransferirMisCuentasDelegate delegate = (TransferirMisCuentasDelegate) getBaseDelegateForKey(TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new TransferirMisCuentasDelegate();
//			addDelegateToHashMap(
//					TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(TransferMisCuentasDetalleViewController.class);
//	}

	/*
	 * Muestra la pantalla para alta de frecuente
	 */
//	public void showAltaFrecuente(DelegateBaseOperacion delegateBaseOperacion) {
//		AltaFrecuenteDelegate delegate = (AltaFrecuenteDelegate) getBaseDelegateForKey(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID);
//		if (delegate != null) {
//			removeDelegateFromHashMap(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID);
//		}
//		delegate = new AltaFrecuenteDelegate(delegateBaseOperacion);
//		addDelegateToHashMap(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID,
//				delegate);
//		showViewController(AltaFrecuenteViewController.class);
//	}



	/*
	 * Muestra la pantalla de transferViewController gen�rica para transferir
	 * comprar pagar consultar
	 */
//	public void showSelectTDCViewController(String tipoOperacion,
//			boolean isExpress, boolean isTDC) {
//		PagoTdcDelegate delegate = (PagoTdcDelegate) getBaseDelegateForKey(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID);
//
//		if (delegate == null) {
//			delegate = new PagoTdcDelegate();
//			addDelegateToHashMap(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID,
//					delegate);
//		}
//
////		delegate.setTipoOperacion(tipoOperacion);
////		delegate.setExpress(isExpress);
////		delegate.setTDC(isTDC);
////		delegate.crearModeloDeOperacion();
//		showViewController(SeleccionaTdcViewController.class);
//	}
	
	/*
	 * Muestra la pantalla de InterbancariosViewController
	 */
//	public void showInterbancariosViewController(Object obj) {
//		InterbancariosDelegate delegate = (InterbancariosDelegate) getBaseDelegateForKey(InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID);
//		if (delegate == null) {
//			if (obj instanceof TransferenciaInterbancaria) {
//				delegate = new InterbancariosDelegate(true);
//			} else
//				delegate = new InterbancariosDelegate();
//			addDelegateToHashMap(
//					InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID, delegate);
//		}
//		delegate.setBajaFrecuente(false);
//		if (obj instanceof Constants.Operacion) {
//			delegate.setTipoOperacion((Constants.Operacion) obj);
//			delegate.setEsFrecuente(false);
//		}
//		if (obj instanceof TransferenciaInterbancaria) {
//			delegate.setEsFrecuente(true);
//			delegate.setTransferenciaInterbancaria((TransferenciaInterbancaria) obj);
//		}
//		showViewController(InterbancariosViewController.class);
//	}

//	public void showOpcionesPagar() {
//		String[] llaves = new String[4];
//		Object[] valores = new Object[4];
//
//		llaves[0] = Constants.PANTALLA_BASE_ICONO;
//		valores[0] = Integer.valueOf(R.drawable.icono_pagar_servicios);
//		llaves[1] = Constants.PANTALLA_BASE_TITULO;
//		valores[1] = Integer.valueOf(R.string.pagar_title);
//		llaves[2] = Constants.PANTALLA_BASE_SUBTITULO;
//		valores[2] = Integer.valueOf(R.string.pagar_subtitle);
//
//		PagarDelegate delegate = (PagarDelegate) getBaseDelegateForKey(PagarDelegate.PAGAR_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new PagarDelegate();
//			addDelegateToHashMap(PagarDelegate.PAGAR_DELEGATE_ID, delegate);
//		}
//
//		showViewController(OpcionesPagosViewController.class, 0, false, llaves,
//				valores);
//	}

//	public void showTipoConsultaFrecuentes() {
//		TipoConsultaDelegate delegate = (TipoConsultaDelegate) getBaseDelegateForKey(TipoConsultaDelegate.TIPO_CONSULTA_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new TipoConsultaDelegate();
//			addDelegateToHashMap(
//					TipoConsultaDelegate.TIPO_CONSULTA_DELEGATE_ID, delegate);
//		}
//		showViewController(TipoConsultaViewController.class);
//	}

	/*
	 * Muestra la pantalla de PagoServiciosViewController
	 */
//	public void showPagosViewController(Constants.Operacion tipoOperacion) {
//		PagoServiciosDelegate delegate = (PagoServiciosDelegate) getBaseDelegateForKey(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new PagoServiciosDelegate();
//			addDelegateToHashMap(
//					PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID, delegate);
//		}
//		delegate.setTipoOperacion(tipoOperacion);
//		showViewController(PagoServiciosFrecuentesViewController.class);
//	}

	/*
	 * 
	 */
//	public void showConsultaFrecuentes(String tipoFrecuente) {
//		String[] llaves = new String[2];
//		Object[] valores = new Object[2];
//		DelegateBaseOperacion delegate = null;
//		llaves[0] = Constants.FREQUENT_REQUEST_SCREEN_DELEGATE_KEY;
//		llaves[1] = Constants.FREQUENT_REQUEST_SCREEN_TITLE_KEY;
//		Session session = Session.getInstance(SuiteApp.getInstance());
//		if (tipoFrecuente.equals(Constants.tipoCFOtrosBancos)) {
//			
//			
//			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirInterbancariaF,	session.getClientProfile())) {
//			valores[0] = InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID;
//			valores[1] = SuiteApp.getInstance().getString(
//					R.string.externalTransfer_frequentTitle);
//			removeDelegateFromHashMap(InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID);
//			delegate = new InterbancariosDelegate(true);
//			addDelegateToHashMap(
//					InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID, delegate);
//			}else{
//				//NO ESTA PERMITIDO
//				
//			}
//
//		} else if (tipoFrecuente.equals(Constants.tipoCFPagosCIE)) {
//			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.pagoServiciosF,	session.getClientProfile())) {
//			valores[0] = PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID;
//			valores[1] = SuiteApp.getInstance().getString(
//					R.string.servicesPayment_frequentTitle);
//			removeDelegateFromHashMap(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID);
//			delegate = new PagoServiciosDelegate();
//			addDelegateToHashMap(
//					PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID, delegate);
//		}else{
//			//NO ESTA PERMITIDO
//			
//		}
//
//		} else if (tipoFrecuente.equals(Constants.tipoCFTiempoAire)) {
//			
//				if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.compraTiempoAireF,	session.getClientProfile())) {
//				valores[0] = TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID;
//				valores[1] = SuiteApp.getInstance().getString(
//						R.string.tiempo_aire_title);
//				removeDelegateFromHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
//				delegate = new TiempoAireDelegate(true);
//				addDelegateToHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID,
//						delegate);
//			
//		}else{
//			//NO ESTA PERMITIDO
//			
//		}
//
//		} else if (tipoFrecuente.equals(Constants.tipoCFOtrosBBVA)) {
//			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirBancomerF,	session.getClientProfile())) {
//			valores[0] = OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID;
//			valores[1] = SuiteApp.getInstance().getString(
//					R.string.transferir_otrosBBVA_title);
//			removeDelegateFromHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
//			delegate = new OtrosBBVADelegate(false, false, true);
//			addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID,
//					delegate);
//		}else{
//			//NO ESTA PERMITIDO
//			
//		}
//
//		} else if (tipoFrecuente.equals(Constants.tipoCFCExpress)) {
//			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirBancomerF,	session.getClientProfile())) {
//			valores[0] = OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID;
//			valores[1] = SuiteApp.getInstance().getString(
//					R.string.transferir_otrosBBVA_express_title);
//			removeDelegateFromHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
//			delegate = new OtrosBBVADelegate(true, false, true);
//			addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID,
//					delegate);
//		}else{
//			//NO ESTA PERMITIDO
//			
//		}
//
//		} else if (tipoFrecuente.equals(Constants.tipoCFDineroMovil)) {
//					
//			if (Autenticacion.getInstance().operarOperacion(Constants.Operacion.dineroMovilF,	session.getClientProfile())) {
//			valores[0] = DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID;
//			valores[1] = SuiteApp.getInstance().getString(
//					R.string.transferir_dineromovil_titulo);
//			removeDelegateFromHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
//			delegate = new DineroMovilDelegate();
//			addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
//					delegate);
//		}else{
//			//NO ESTA PERMITIDO
//			TipoConsultaDelegate tipoConsultaDelegate = (TipoConsultaDelegate) getBaseDelegateForKey(TipoConsultaDelegate.TIPO_CONSULTA_DELEGATE_ID);
//			tipoConsultaDelegate.comprobarRecortado();
//			
//		}
//		}
//		if (delegate != null)
//			showViewController(ConsultarFrecuentesViewController.class, 0,
//					false, llaves, valores);
//	}

	/*
	 * Muestra la pantalla de PagoServiciosViewController
	 */
//	public void showPagoServiciosViewController(PagoServicio pagoServicio) {
//
//		PagoServiciosDelegate delegate = (PagoServiciosDelegate) getBaseDelegateForKey(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new PagoServiciosDelegate();
//			addDelegateToHashMap(
//					PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID, delegate);
//		}
//		delegate.setBajaFrecuente(false);
//		if (pagoServicio != null) {
//			delegate.setPagoServicio(pagoServicio);
//			delegate.setFrecuente(true);
//			delegate.setTipoOperacion(Constants.Operacion.pagoServiciosF);
//			delegate.setPreregister(false);
//		} else {
//			delegate.getPagoServicio().setConvenioServicio("");
//			delegate.setFrecuente(false);
//			if (delegate.isPreregister()) {
//				delegate.setTipoOperacion(Constants.Operacion.pagoServiciosP);
//			} else {
//				delegate.setTipoOperacion(Constants.Operacion.pagoServicios);
//			}
//		}
//		showViewController(PagoServiciosViewController.class);
//	}

//	public void showPagoServiciosAyudaViewController(String[] llaves,
//			Object[] valores) {
//		showViewController(PagoServiciosAyudaViewController.class, 0, false,
//				llaves, valores);
//	}

//	public void showLecturaCodigoViewController(int codigoActividad) {
//		showViewControllerForResult(CaptureActivity.class, codigoActividad);
//	}

	/**
	 * Muestra la pantalla de tranferencia de dinero m�vil.
	 */
//	public void showTransferirDineroMovil(TransferenciaDineroMovil frecuente,
//			boolean bajaDM) {
//		DineroMovilDelegate delegate = (DineroMovilDelegate) getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new DineroMovilDelegate();
//			addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
//					delegate);
//		}
//		if (null != frecuente) {
//			delegate.setTransferencia(frecuente);
//			delegate.esFrecuente(true);
//		}
//		//Valida que al entrar a la pantalla de dinero movil
//		//no precargue los datos de un frecuente después de operarñpo
//		// en una operación nueva
//		if(null == frecuente)
//		{
//			delegate.esFrecuente(false);
//		}
//		delegate.setEsBajaDM(bajaDM);
//		delegate.setBajaFrecuente(false);
//		showViewController(DineroMovilViewController.class);
//	}

	/**
	 * 
	 * @param rapido
	 */
//	public void showTransferirDineroMovil(Rapido rapido) {
//		DineroMovilDelegate delegate = (DineroMovilDelegate) getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new DineroMovilDelegate();
//			addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
//					delegate);
//		}
//		delegate.cargarRapido(rapido);
//		delegate.esFrecuente(false);
//		delegate.setEsBajaDM(false);
//		delegate.setBajaFrecuente(false);
//		showViewController(DineroMovilViewController.class);
//	}

	/*
	 * Muestra la pantalla del Menu Comprar
	 */
//	public void showComprarViewController() {
//		MenuComprarDelegate delegate = (MenuComprarDelegate) getBaseDelegateForKey(MenuComprarDelegate.MENU_COMPRAR_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new MenuComprarDelegate();
//			addDelegateToHashMap(MenuComprarDelegate.MENU_COMPRAR_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(MenuComprarViewController.class);
//	}

//	public void showTiempoAireViewController(Rapido rapido) {
//		TiempoAireDelegate delegate = (TiempoAireDelegate) getBaseDelegateForKey(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
//		if (delegate != null) {
//			removeDelegateFromHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
//		}
//		delegate = new TiempoAireDelegate(false);
//		addDelegateToHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID,
//				delegate);
//		delegate.setTipoOperacion(Operacion.compraTiempoAireR);
//		delegate.setFrecuente(false);
//		delegate.cargarRapido(rapido);
//
//		showViewController(TiempoAireDetalleViewController.class);
//	}

	/*
	 * Muestra la pantalla de Tiempo Aire
	 */
//	public void showTiempoAireViewController(Constants.Operacion tipoOpracion) {
//		TiempoAireDelegate delegate = (TiempoAireDelegate) getBaseDelegateForKey(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
//		if (delegate != null) {
//			removeDelegateFromHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
//		}
//		delegate = new TiempoAireDelegate(false);
//		addDelegateToHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID,
//				delegate);
//		delegate.setTipoOperacion(tipoOpracion);
//		showViewController(TiempoAireViewController.class);
//	}

	/*
	 * Muestra la segunda pantalla de Tiempo Aire
	 */
//	public void showTiempoAireDetalleViewController() {
//		TiempoAireDelegate delegate = (TiempoAireDelegate) getBaseDelegateForKey(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new TiempoAireDelegate(false);
//			addDelegateToHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(TiempoAireDetalleViewController.class);
//	}

	/*
	 * Muestra la pantalla de Transferencias terceros
	 */
//	public void showOtrosBBVAViewController(Object obj, boolean esExpress,
//			boolean esTDC) {
//		OtrosBBVADelegate delegate = (OtrosBBVADelegate) getBaseDelegateForKey(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
//		if (delegate == null) {
//			if (obj instanceof TransferenciaOtrosBBVA) {
//				delegate = new OtrosBBVADelegate(esExpress, esTDC, true);
//			} else
//				delegate = new OtrosBBVADelegate(esExpress, esTDC);
//			addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID,
//					delegate);
//		}
//		delegate.setEsExpress(esExpress);
//		delegate.setEsTDC(esTDC);
//		if (obj instanceof Constants.Operacion) {
//			delegate.setTipoOperacion((Constants.Operacion) obj);
//			delegate.setEsFrecuente(false);
//		}
//		if (obj instanceof TransferenciaOtrosBBVA) {
//			delegate.setEsFrecuente(true);
//			delegate.setTransferenciaOtrosBBVA((TransferenciaOtrosBBVA) obj);
//		}
//		showViewController(OtrosBBVAViewController.class);
//	}

//	public void showOtrosBBVAViewController(Rapido rapido) {
//		OtrosBBVADelegate delegate = (OtrosBBVADelegate) getBaseDelegateForKey(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new OtrosBBVADelegate(false, false);
//			addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID,
//					delegate);
//		}
//
//		delegate.cargarRapido(rapido);
//
//		// if(obj instanceof Constants.Operacion){
//		// delegate.setTipoOperacion((Constants.Operacion)obj);
//		// delegate.setEsFrecuente(false);
//		// }
//		// if(obj instanceof TransferenciaOtrosBBVA){
//		// delegate.setEsFrecuente(true);
//		// delegate.setTransferenciaOtrosBBVA((TransferenciaOtrosBBVA) obj);
//		// }
//		showViewController(OtrosBBVAViewController.class);
//	}
	
	/**
	 * Muestra la pantalla de detalles de un movimiento de Obtener Comprobante.
	 */
//	public void showDetalleObtenerComprobante() {
//		ObtenerComprobantesDelegate delegate = (ObtenerComprobantesDelegate) getBaseDelegateForKey(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new ObtenerComprobantesDelegate();
//			addDelegateToHashMap(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(DetalleObtenerComprobanteViewController.class);
//	}
	
	/**
	 * Muestra la pantalla de detalles de un movimiento de Depositos Recibidos.
	 */
//	public void showDetalleDepositosRecibidos() {
//		DepositosRecibidosDelegate delegate = (DepositosRecibidosDelegate) getBaseDelegateForKey(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new DepositosRecibidosDelegate();
//			addDelegateToHashMap(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(DetalleDepositosRecibidosViewController.class);
//	}


	/**
	 * Muestra la pantalla de detalles de un movimiento de dinero movil.
	 */
//	public void showDetalleDineroMovil() {
//		DineroMovilDelegate delegate = (DineroMovilDelegate) getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new DineroMovilDelegate();
//			addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(DetalleDineroMovilViewController.class);
//	}

	/**
	 * Muestra la pantalla de detalles de un movimiento de dinero movil.
	 */
//	public void showDetalleRetiroSinTarjeta() {
//		ConsultaRetiroSinTarjetaDelegate delegate = (ConsultaRetiroSinTarjetaDelegate) getBaseDelegateForKey(ConsultaRetiroSinTarjetaDelegate.RETIRO_SIN_TARJETA_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new ConsultaRetiroSinTarjetaDelegate();
//			addDelegateToHashMap(ConsultaRetiroSinTarjetaDelegate.RETIRO_SIN_TARJETA_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(DetalleConsultaRetiroSinTarjetaViewController.class);
//	}

	@Override
	public void removeDelegateFromHashMap(final long key) {
		// TODO Auto-generated method stub
		super.removeDelegateFromHashMap(key);
	}

//	public void showListaBancos(int codigoActividad) {
//		showViewControllerForResult(ListaBancosViewController.class,
//				codigoActividad);
//	}

	/**
	 * Muestra la pantalla para cambio de telefono asociado
	 */
//	public void showCambioDeTelefono() {
//		CambioTelefonoDelegate delegate = (CambioTelefonoDelegate) getBaseDelegateForKey(CambioTelefonoDelegate.CAMBIO_TELEFONO_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new CambioTelefonoDelegate();
//			addDelegateToHashMap(
//					CambioTelefonoDelegate.CAMBIO_TELEFONO_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(CambioTelefonoViewController.class);
//	}

	/**
	 * Muestra la pantalla del menu Suspender / Cancelar
	 */
//	public void showMenuSuspenderCancelar() {
//		MenuSuspenderCancelarDelegate delegate = (MenuSuspenderCancelarDelegate) getBaseDelegateForKey(MenuSuspenderCancelarDelegate.MENU_SUSPENDER_CANCELAR_ID);
//		if (delegate == null) {
//			delegate = new MenuSuspenderCancelarDelegate();
//			addDelegateToHashMap(
//					MenuSuspenderCancelarDelegate.MENU_SUSPENDER_CANCELAR_ID,
//					delegate);
//		}
//		showViewController(MenuSuspenderCancelarViewController.class);
//	}
	
	
	
	
	

	/**
	 * Muestra la pantalla de ayuda para agregar una operación rápida.
	 */
//	public void showAyudaAgregarRapido() {
//		showViewController(AyudaAgregarRapidoViewController.class);
//	}

	/**
	 * Muestra la pantalla de configurar montos con los limites elegidos por el
	 * usuario.
	 */
//	public void showConfigurarMontos(ConfigurarMontos cm) {
//		ConfigurarMontosDelegate delegate = (ConfigurarMontosDelegate) getBaseDelegateForKey(ConfigurarMontosDelegate.CONFIGURAR_MONTOS_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new ConfigurarMontosDelegate();
//			addDelegateToHashMap(
//					ConfigurarMontosDelegate.CONFIGURAR_MONTOS_DELEGATE_ID,
//					delegate);
//		}
//		delegate.setConfigurarMontos(cm);
//		showViewController(ConfigurarMontosViewController.class);
//	}

	/**
	 * Muestra la pantalla de resultados
	 */
//	public void showResultadosAutenticacionViewController(
//			DelegateBaseOperacion delegateBaseOperacion, int resIcon,
//			int resTitle) {
//		ResultadosAutenticacionDelegate delegate = (ResultadosAutenticacionDelegate) getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
//		if (delegate != null) {
//			removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
//		}
//		delegate = new ResultadosAutenticacionDelegate(delegateBaseOperacion);
//		addDelegateToHashMap(
//				ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID,
//				delegate);
//		showViewController(ResultadosAutenticacionViewController.class);
//	}

	/**
	 * Muestra la pantalla para desbloqueo
	 * 
	 * @param ConsultaEstatus
	 */
//	public void showDesbloqueo(ConsultaEstatus ce) {
//		NuevaContraseniaDelegate delegate = (NuevaContraseniaDelegate) getBaseDelegateForKey(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID);
//		if (delegate != null) {
//			removeDelegateFromHashMap(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID);
//		}
//		delegate = new NuevaContraseniaDelegate(ce);
//		addDelegateToHashMap(
//				NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID,
//				delegate);
//		showViewController(NuevaContraseniaViewController.class);
//	}

	/**
	 * Muestra la confirmacion para quitar la suspension
	 * 
	 * @param ConsultaEstatus
	 */
//	public void showQuitarSuspension(ConsultaEstatus ce) {
//		QuitarSuspencionDelegate delegate = (QuitarSuspencionDelegate) getBaseDelegateForKey(QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID);
//		if (delegate != null) {
//			removeDelegateFromHashMap(QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID);
//		}
//		delegate = new QuitarSuspencionDelegate(ce);
//		addDelegateToHashMap(
//				QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID,
//				delegate);
//		showConfirmacionAutenticacionViewController(delegate, 0, 0, 0);
//	}

	/**
	 * Muestra la pantalla de configurar alertas.
	 */
//	public void showConfigurarAlertas() {
//		ConfigurarAlertasDelegate delegate = (ConfigurarAlertasDelegate) getBaseDelegateForKey(ConfigurarAlertasDelegate.CONFIGURAR_ALERTAS_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new ConfigurarAlertasDelegate();
//			addDelegateToHashMap(
//					ConfigurarAlertasDelegate.CONFIGURAR_ALERTAS_DELEGATE_ID,
//					delegate);
//		}
//
//		showViewController(ConfigurarAlertasViewController.class);
//	}

	/**
	 * Muestra la pantalla de ayuda para la contratacion.
	 */
//	public void showAyudaContratacion() {
//		showViewController(AyudaContratacionViewController.class);
//	}

	/**
	 * Muestra la pantalla de informaci�n contratacion con y sin Token.
	 */
//	public void showAyudaContratacionTokens() {
//		showViewController(AyudaContratacionTokensViewController.class);
//	}

	/**
	 * Muestra la pantalla de confirmaci�n autenticación para el estatus PS.
	 */
//	public void showContratacionAutenticacion() {
//		ContratacionDelegate contrataciondelegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
//		ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
//
//		if (delegate != null){
//			removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
//		}
//		
//		delegate = new ContratacionAutenticacionDelegate(contrataciondelegate);
//		addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID, delegate);
//		showViewController(ContratacionAutenticacionViewController.class);
//	}

	/**
	 * Muestra la pantalla de confirmacion autenticacion
	 */
//	public void showContratacionAutenticacion(DelegateBaseAutenticacion autenticacionDelegate) {
//		ContratacionAutenticacionDelegate delegate = (ContratacionAutenticacionDelegate) getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
//		
//		if (delegate != null){
//			removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
//		}
//		
//		delegate = new ContratacionAutenticacionDelegate(autenticacionDelegate);
//		addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID, delegate);
//		showViewController(ContratacionAutenticacionViewController.class);
//	}

	/**
	 * Muestra los terminos y condiciones de uso.
	 * 
	 * @param terminosDeUso
	 *            Terminos de uso.
	 */
	public void showTerminosDeUso(String terminosDeUso) {
//		showViewController(TerminosCondicionesViewController.class, 0, false,
//				new String[] { Constants.TERMINOS_DE_USO_EXTRA },
//				new Object[] { terminosDeUso });
	}

	public void showActivacion(final ConsultaEstatus ce, final String password,
			final boolean appActivada) {
		if (appActivada) {
			SuiteApp.getInstance()
					.getSuiteViewsController()
					.getCurrentViewControllerApp()
					.showInformationAlert(
							R.string.bmovil_activacion_alerta_reactivar,
							new OnClickListener() {

								@Override
								public void onClick(final DialogInterface dialog,
													final int which) {
									ActivacionDelegate aDelegate = (ActivacionDelegate) getBaseDelegateForKey(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
									if (aDelegate != null)
										removeDelegateFromHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
									aDelegate = new ActivacionDelegate(ce,
											password);
									aDelegate.borrarDatos();
									addDelegateToHashMap(
											ActivacionDelegate.ACTIVACION_DELEGATE_ID,
											aDelegate);
								}
							});
		}
	}
	
	public void showActivacion(final ConsultaEstatus ce, final String password) {
		ActivacionDelegate aDelegate = (ActivacionDelegate) getBaseDelegateForKey(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
		if (aDelegate != null)
			removeDelegateFromHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
		aDelegate = new ActivacionDelegate(ce, password);

		addDelegateToHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID,
				aDelegate);
		//AMZ
		TrackingHelper.trackState("activacion", estados);
		//AMZ
		showViewController(ActivacionViewController.class);
	}

	public void touchMenu(){
		String eliminado;
		if (estados.size() == 2){
			eliminado = estados.remove(1);
		}else{
			int tam = estados.size();
		for (int i=1;i<tam;i++){
			eliminado = estados.remove(1);
		}
		}
	}
	
	public void touchAtras()
	{

		final int ultimo = estados.size()-1;

		final String eliminado;
		if(ultimo >= 0)
		{
			final String ult = estados.get(ultimo);
			if(ult.equals("reactivacion") || ult.equals("activacion") || ult.equals("menu token") || ult.equals("contratacion datos") || ult.equals("activacion datos"))
			{
				estados.clear();
			}else
			{
				 eliminado = estados.remove(ultimo);
			}
		} 
	}

	//One click
	
	/*
	 * Muestra la pantalla de detalle oferta ILC
	 */
//	public void showDetalleILC(OfertaILC ofertaILC, Promociones promocion ) {
//		// TODO Auto-generated method stub
//		DetalleOfertaILCDelegate delegate = (DetalleOfertaILCDelegate) getBaseDelegateForKey(DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE);
//		if (null != delegate) 
//			removeDelegateFromHashMap(DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE);
//		delegate = new DetalleOfertaILCDelegate(ofertaILC, promocion);
//		addDelegateToHashMap(
//					DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE, delegate);
//		
//		showViewController(DetalleILCViewController.class);
//	}
	
	/*
	 * Muestra la pantalla de exito oferta ILC
	 */
//	public void showExitoILC(AceptaOfertaILC aceptaOfertaILC, OfertaILC ofertaILC ) {
//		// TODO Auto-generated method stub
//		ExitoILCDelegate delegate = (ExitoILCDelegate) getBaseDelegateForKey(ExitoILCDelegate.EXITO_OFERTA_DELEGATE);
//		if (null != delegate) 
//				removeDelegateFromHashMap(ExitoILCDelegate.EXITO_OFERTA_DELEGATE);
//			delegate = new ExitoILCDelegate(aceptaOfertaILC,ofertaILC );
//			addDelegateToHashMap(
//					ExitoILCDelegate.EXITO_OFERTA_DELEGATE, delegate);
//		
//		showViewController(ExitoOfertaILCViewController.class);
//	}
	
	/*
	 * Muestra la pantalla de detalle oferta ILC
	 */
//	public void showDetalleEFI(OfertaEFI ofertaEFI, Promociones promocion ) {
//		// TODO Auto-generated method stub
//		DetalleOfertaEFIDelegate delegate = (DetalleOfertaEFIDelegate) getBaseDelegateForKey(DetalleOfertaEFIDelegate.DETALLE_OFERTA_EFI_DELEGATE);
//		if (null == delegate) {
//			delegate = new DetalleOfertaEFIDelegate(ofertaEFI, promocion);
//			addDelegateToHashMap(
//					DetalleOfertaEFIDelegate.DETALLE_OFERTA_EFI_DELEGATE, delegate);
//		}else{
//			delegate.setOfertaEFI(ofertaEFI);
//		}
//		showViewController(DetalleEFIViewController.class);
//	}
	
	/*
	 * Muestra la pantalla de exito oferta ILC
	 */
//	public void showExitoEFI(AceptaOfertaEFI aceptaOfertaEFI, OfertaEFI ofertaEFI) {
//		// TODO Auto-generated method stub
//		ExitoEFIDelegate delegate = (ExitoEFIDelegate) getBaseDelegateForKey(ExitoEFIDelegate.EXITO_OFERTA_EFI_DELEGATE);
//		if (null != delegate) 
//				removeDelegateFromHashMap(ExitoEFIDelegate.EXITO_OFERTA_EFI_DELEGATE);			
//		delegate = new ExitoEFIDelegate(aceptaOfertaEFI,ofertaEFI );
//			addDelegateToHashMap(
//					ExitoEFIDelegate.EXITO_OFERTA_EFI_DELEGATE, delegate);
//		
//		showViewController(ExitoOfertaEFIViewController.class);
//	}
	
	/*
	 * Muestra la pantalla de importe a modificar EFI
	 */
//	public void showModificaImporteEFI(OfertaEFI ofertaEFI) {
//		// TODO Auto-generated method stub
//		ModificarImporteDelegate delegate = (ModificarImporteDelegate) getBaseDelegateForKey(ModificarImporteDelegate.MODIFICAR_IMPORTE_DELEGATE_ID);
//		if (null != delegate) 
//				removeDelegateFromHashMap(ModificarImporteDelegate.MODIFICAR_IMPORTE_DELEGATE_ID);
//			delegate = new ModificarImporteDelegate(ofertaEFI);
//			addDelegateToHashMap(
//					ModificarImporteDelegate.MODIFICAR_IMPORTE_DELEGATE_ID, delegate);
//		
//		showViewController(ModificarImporteEFIViewController.class);
//	}
	
	/*
	 * Muestra la pantalla de detalle oferta consumo 
	 */
//	public void showDetalleConsumo(OfertaConsumo ofertaConsumo, Promociones promocion ) {
//		// TODO Auto-generated method stub
//		DetalleOfertaConsumoDelegate delegate = (DetalleOfertaConsumoDelegate) getBaseDelegateForKey(DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE);
//		if (null == delegate) {
//			delegate = new DetalleOfertaConsumoDelegate(ofertaConsumo, promocion);
//			addDelegateToHashMap(
//					DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE, delegate);
//		}else{
//			delegate.setOfertaConsumo(ofertaConsumo);
//		}
//		showViewController(DetalleConsumoViewController.class);
//	}
	
	/*
	 * Muestra la pantalla de contrato oferta consumo 
	 */
//	public void showContratoConsumo(OfertaConsumo ofertaConsumo, Promociones promocion) {
//		// TODO Auto-generated method stub
//		ContratoOfertaConsumoDelegate delegate = (ContratoOfertaConsumoDelegate) getBaseDelegateForKey(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE);			
//		if (null != delegate) 
//				removeDelegateFromHashMap(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE);
//			delegate = new ContratoOfertaConsumoDelegate(ofertaConsumo,promocion);
//			delegate.setTipoOperacion(Operacion.oneClickBmovilConsumo);
//			addDelegateToHashMap(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE, delegate);				
//			showViewController(ContratoConsumoViewController.class);
//	}
	
	/**
	 * Muestra la poliza consumo.
	 * 
	 * @param terminosDeUso
	 *            Terminos de uso.
	 */
//	public void showPoliza(String poliza) {
//		showViewController(CaracteristicasPolizaViewController.class, 0, false,
//				new String[] { Constants.TERMINOS_DE_USO_EXTRA },
//				new Object[] { poliza });
//	}
	
	/**
	 * Muestra formato domiciliacion
	 * 
	 * @param terminosDeUso
	 *            Terminos de uso.
	 */
	
//	public void showformatodomiciliacion(String terminos) {
//		showViewController(CaracteristicasPolizaViewController.class, 0, false,
//				new String[] { Constants.DOMICILIACION_CONSUMO },
//				new Object[] { terminos });
//	}
	
	/**
	 * Muestra contrato consumo
	 * 
	 * @param terminosDeUso
	 *            Terminos de uso.
	 */
	
//	public void showContrato(String contrato) {
//		showViewController(CaracteristicasPolizaViewController.class, 0, false,
//				new String[] { Constants.CONTRATO_CONSUMO },
//				new Object[] { contrato });
//	}		
	
	/**
	 * Muestra terminos y condiciones consumo.
	 * 
	 * @param terminosDeUso
	 *            Terminos de uso.
	 */
	
//	public void showTerminosConsumo(String terminos) {
//		showViewController(CaracteristicasPolizaViewController.class, 0, false,
//				new String[] { Constants.TERMINOS_DE_USO_CONSUMO },
//				new Object[] { terminos });
//	}
	
	/*
	 * Muestra la pantalla de exito  oferta consumo 
	 */
//	public void showExitoConsumo(AceptaOfertaConsumo aceptaOfertaConsumo, OfertaConsumo ofertaConsumo) {
//		// TODO Auto-generated method stub
//		ExitoOfertaConsumoDelegate delegate = (ExitoOfertaConsumoDelegate) getBaseDelegateForKey(ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE);
//		if (null == delegate) {
//			delegate = new ExitoOfertaConsumoDelegate(aceptaOfertaConsumo,ofertaConsumo);
//			addDelegateToHashMap(
//					ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE, delegate);
//		}else{
//			//delegate.setOfertaConsumo(ofertaConsumo);
//		}
//		showViewController(ExitoOfertaConsumoViewController.class);
//	}
	/**
	 * Muestra la pantalla para consultar Interbancarios.
	 */
//	public void showConsultarInterbancarios() {
//		ConsultaInterbancariosDelegate delegate = (ConsultaInterbancariosDelegate) getBaseDelegateForKey(ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID);
//		
//		if (delegate == null) {
//			delegate = new ConsultaInterbancariosDelegate();
//			addDelegateToHashMap(
//					ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID,
//					delegate);
//		}
//		//delegate.resetPeriodo();
//		delegate.resetPeriodoConsultas();
//		showViewController(ConsultaInterbancariosViewController.class);
//	}

	/**
	 * Muestra la pantalla detalle interbancaria
	 */
//	public void showDetalleInterbancaria(ConsultaInterbancariosDelegate delegate) {
//		// TODO Auto-generated method stub
//		if (delegate != null)
//			removeDelegateFromHashMap(ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID);
//		addDelegateToHashMap(
//				ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID,
//				delegate);
//		
//		showViewController(DetalleMoviemientoSpeiViewController.class);
//	}
//
//	public void showPagoTDCCuenta(Account cuenta) {
//		PagoTdcDelegate delegate = (PagoTdcDelegate) getBaseDelegateForKey(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new PagoTdcDelegate();
//			addDelegateToHashMap(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID,
//					delegate);
//		}
//		delegate.setCuentaActual(cuenta);
//		delegate.setCuentaOrigenSeleccionada(null);
//		showViewController(PagoTdcViewController.class);
//	}
//
//	public void showPagoTDCCuenta(Account cuenta, Account cuentaOrigen) {
//		PagoTdcDelegate delegate = (PagoTdcDelegate) getBaseDelegateForKey(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new PagoTdcDelegate();
//			addDelegateToHashMap(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID,
//					delegate);
//		}
//		delegate.setCuentaActual(cuenta);
//		delegate.setCuentaOrigenSeleccionada(cuentaOrigen);
//		showViewController(PagoTdcViewController.class);
//	}
//
//	public void showRetiroSinTarjetaViewController() {
//		AltaRetiroSinTarjetaDelegate delegate = (AltaRetiroSinTarjetaDelegate) getBaseDelegateForKey(AltaRetiroSinTarjetaDelegate.ALTA_RETIRO_SINTAR_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new AltaRetiroSinTarjetaDelegate();
//			addDelegateToHashMap(AltaRetiroSinTarjetaDelegate.ALTA_RETIRO_SINTAR_DELEGATE_ID,
//					delegate);
//		}
//
//		showViewController(AltaRetiroSinTarjetaViewController.class);
//
//	}
	
	
	
	

}