package com.bancomer.mbanking.activacion;

import java.util.Hashtable;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingHandler;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomer.activacion.aplicaciones.bmovil.classes.gui.controllers.BmovilViewsController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import android.annotation.SuppressLint;

@SuppressLint("HandlerLeak")
public class BmovilApp extends BaseSubapplication {

	//////////////////////////////////////////////////////////////
	//					Methods									//
	//////////////////////////////////////////////////////////////    

	/**
	 * Constructor básico, se inicializa aqui debido a que no puede extender de un objeto Aplicación, ya que no es posible
	 * que exista mas de uno
	 */
	public BmovilApp(final SuiteApp suiteApp) {
		super(suiteApp);
		viewsController = new BmovilViewsController(this);
	}

	public BmovilViewsController getBmovilViewsController() {
		return (BmovilViewsController)viewsController;
	}

	//////////////////////////////////////////////////////////////////////////////
	//                Network operations										//
	//////////////////////////////////////////////////////////////////////////////
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * custom texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param progressLabel text with the title of the progress popup
	 * @param progressMessage text with the content of the progress popup
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	@Override
	protected void invokeNetworkOperation(final int operationId,
										  final Hashtable<String, ?> params,
										  final boolean isJson,
										  final ParsingHandler handler,
										  final BaseViewControllerCommons caller,
										  final String progressLabel,
										  final String progressMessage,
										  final boolean callerHandlesError) {
		//we stop the timer during server calls
		super.invokeNetworkOperation(operationId, params,isJson,handler, caller, progressLabel, progressMessage, callerHandlesError);
	}

	/**
	 * Analyze the network response obtained from the server
	 * @param operationId network identifier. See Server class.
	 * @param response the ServerResponse instance built from the server response. It
	 * contains the type of result (success, failure), and the real content for
	 * the application business.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	@Override
	protected void analyzeNetworkResponse(final Integer operationId,
										  final ServerResponse response,
										  final Throwable throwable,
										  final BaseViewControllerCommons caller,
										  final boolean callerHandlesError) {

		super.analyzeNetworkResponse(operationId, response, throwable, caller, callerHandlesError);
	}

	public int getApplicationStatus() {
		return Session.getInstance(SuiteApp.appContext).getPendingStatus();
	}
	
}
