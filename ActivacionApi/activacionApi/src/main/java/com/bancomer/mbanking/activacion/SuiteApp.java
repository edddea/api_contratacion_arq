package com.bancomer.mbanking.activacion;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.bancomer.base.callback.CallBackBConnect;

import suitebancomer.activacion.classes.gui.controllers.SuiteViewsController;

public class SuiteApp extends com.bancomer.base.SuiteApp {
	public static boolean isSubAppRunning;
	public static Context appContext;
	private static SuiteApp me;
	private SuiteViewsController suiteViewsController;

	private static CallBackBConnect showReactivacion;

	private static Boolean callsAppDesactivada = Boolean.FALSE;
	
	/**
	 * Intent para regresar a consulta estatus desactivada
	 */
	private static Activity intentToReturn;

	public static Activity getIntentToReturn() {
		return SuiteApp.intentToReturn;
	}

	public static void setIntentToReturn(final Activity intentToReturn) {
		SuiteApp.intentToReturn = intentToReturn;
	}

	public static CallBackBConnect getShowReactivacion() {
		return showReactivacion;
	}

	public static void setShowReactivacion(final CallBackBConnect showReactivacion) {
		SuiteApp.showReactivacion = showReactivacion;
	}

	//@Override
	public void onCreate(final Context context) {
		super.onCreate(context);
		suiteViewsController = new SuiteViewsController();
		isSubAppRunning = false;
		appContext = context;
		me = this;
		//appContext = com.bancomer.base.SuiteApp.getInstance().getApplicationContext();
	}

	public static int getResourceId(final String nombre, final String tipo){
		return appContext.getResources().getIdentifier(nombre,tipo, appContext.getPackageName());
	}
	
	public static int getResourceId(final String nombre, final String tipo, final View vista){
		return vista.getResources().getIdentifier(nombre, tipo, appContext.getPackageName());
	}
	
	public void cierraAplicacionSuite() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public SuiteViewsController getSuiteViewsController() {
		return suiteViewsController;
	}

	public static SuiteApp getInstance() {
		return me;
	}
	
	// #region BmovilApp
	private BmovilApp bmovilApplication;
	
	public BmovilApp getBmovilApplication() {
		if(bmovilApplication == null)
			bmovilApplication = new BmovilApp(this);
		return bmovilApplication;
	}
	
	public void startBmovilApp() {
		bmovilApplication = new BmovilApp(this);
		isSubAppRunning = true;
	}

	// #endregion

	public static Boolean getCallsAppDesactivada() {
		return callsAppDesactivada;
	}

	public static void setCallsAppDesactivada(final Boolean callsAppDesactivada) {
		SuiteApp.callsAppDesactivada = callsAppDesactivada;
	}

}
