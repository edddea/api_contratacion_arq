package suitebancomer.aplicaciones.commservice.response;

/**
 * Result wraps the result data of a response (status, error code, error message).
 * 
 * @author 
 * 
 * IDS Comercial S.A. de C.V
 */
public class Result {

    /**
     * The status code.
     */
    private final String status;

    /**
     * The error code.
     */
    private  final String code;

    /**
     * The error message.
     */
    private final String message;

    /**
     * The URL for mandatory application update.
     */
    private   String updateURL = null;

    /**
     * Default constructor.
     * @param stat the status code
     * @param cod the error code
     * @param msg the error message
     */
    public Result(final String stat, final String cod, final String msg) {
        status = stat;
        code = cod;
        message = msg;
    }

    /**
     * Default constructor.
     * @param st the status code
     * @param cod the error code
     * @param msg the error message
     * @param urlUpd the URL for mandatory application updating
     */
    public Result(final String st, final String cod, 
    				final String msg, final String urlUpd) {
        status = st;
        code = cod;
        message = msg;
        updateURL = urlUpd;
    }

    /**
     * Get the status code.
     * @return status code
     */
    public String getStatus() {
        return status;
    }

    /**
     * Get the error code.
     * @return the error code
     */
    public String getCode() {
        return code;
    }

    /**
     * Get the error message.
     * @return the error message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Get the update URL.
     * @return the update URL for mandatory updating
     */
    public String getUpdateURL() {
        return updateURL;
    }
}
