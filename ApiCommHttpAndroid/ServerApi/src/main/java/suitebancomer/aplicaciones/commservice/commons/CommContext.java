package suitebancomer.aplicaciones.commservice.commons;

import android.content.Context;
import android.util.Log;

import org.apache.http.cookie.Cookie;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 *  
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class CommContext {

	/**
	 * Bancomer Infrastructure.
	 */
	// helper to get a value property
	private static AssetsPropertyReader assetsPropertyReader;
	private static Context context;
	private static Properties simulationProperties;
	private static Properties configProperties;
	public static Boolean allowLog = Boolean.FALSE;
	
	private static String classname = "CommContext";
	private static String propertieSimulationValue = "property simulation > ";
	private static String propertieConfigurationValue = "property configuration > ";

	public static Integer operacionCode = null;
	public static List<String> listaEncriptar;

	@SuppressWarnings("unused")
	private CommContext(){}
		
	public CommContext(final Context contextParam){
		context = contextParam;
		loadProperties();
	}
	
	private static void loadProperties(){
		if(simulationProperties == null){
			assetsPropertyReader = new AssetsPropertyReader( context );
			
			simulationProperties = assetsPropertyReader.getProperties(ApiConstants.simulationFile);
			configProperties = assetsPropertyReader.getProperties(ApiConstants.confApiCom);
		}
		allowLog = ServerCommons.ALLOW_LOG;
	}

	public static String getSimulationPropertyValue(final String id) {
		
		if(simulationProperties == null){
			loadProperties();
		}
		final String value = simulationProperties.getProperty(id);
		if(allowLog){ Log.d( classname, propertieSimulationValue + value);}
		
		return value;
	}
	
	public static String getConfigPropertyValue(final String id) {
		
		if(configProperties == null){
			loadProperties();
		}
		final String value = configProperties.getProperty(id);		
		if(allowLog){Log.d( classname, propertieConfigurationValue + value);}
		
		return value;
	}

	public static Context getContext() {
		return context;
	}

	public  static List<Cookie> listCookie=new ArrayList<Cookie>();

	public static List<HashMap<String, String>> cookiesToSend = new ArrayList<HashMap<String, String>>();
}
