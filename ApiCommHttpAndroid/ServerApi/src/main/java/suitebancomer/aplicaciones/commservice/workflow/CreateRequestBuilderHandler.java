package suitebancomer.aplicaciones.commservice.workflow;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import suitebancomer.aplicaciones.commservice.request.IRequestBuilder;
import suitebancomer.aplicaciones.commservice.request.IRequestFactory;
import suitebancomer.aplicaciones.commservice.request.RequestFactory;
import suitebancomer.aplicaciones.commservice.request.RequestService;
import suitebancomer.aplicaciones.commservice.response.IResponseService;


/**
 * This class (a) handles requests it is responsible for, (b) can access its
 * successor, and (c) if it can handle the request, does so, else it forwards it
 * to its successor.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class CreateRequestBuilderHandler extends AbstractInvokeHandler {
	
	private IInvokeHandle next;
	
	IRequestFactory requestFactory;
	public CreateRequestBuilderHandler(){
		requestFactory=new RequestFactory();
	}

	
	/**
	 * obtiene un IRequestBuilder a travez del metodo createRequestBuilder de la clase RequestFactory,
	 * a esa instancia de IRequestBuilder que se obtuvo de factory se le envia 
	 * el request con los parametros que llegaron desde el proxy y esperamos de regreso
	 * un RequestService
	 * 
	 * 
	 * @param request
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws JSONException 
	 * @throws IllegalStateException 
	 */
	public IResponseService handleRequest(final RequestService request,final Class<?> objResponse) 
			throws ClientProtocolException, IOException, IllegalStateException, JSONException{
		//crea el request
		final IRequestBuilder requestBuilder=requestFactory.createRequestBuilder();
		 
		//se obtiene el request ya formado y con los parametros recibidos
		final RequestService requestBuilded=requestBuilder.create(request);
		
		
		/**
		 * una vez que ya recibio la peticion formada con la url y los parametros
		 * llama la clase de sendrequestHandle
		 * */
		//se realiza la peticon al servidor web

		return  next.handleRequest(requestBuilded, objResponse);
	}

	public IResponseService handleRequestImage(final RequestService request,final Class<?> objResponse)
			throws ClientProtocolException, IOException, IllegalStateException, JSONException{
		//crea el request
		final IRequestBuilder requestBuilder=requestFactory.createRequestBuilder();

		//se obtiene el request ya formado y con los parametros recibidos
		final RequestService requestBuilded=requestBuilder.createImage(request);


		/**
		 * una vez que ya recibio la peticion formada con la url y los parametros
		 * llama la clase de sendrequestHandle
		 * */
		//se realiza la peticon al servidor web

		return  next.handleRequestImage(requestBuilded, objResponse);
	}


	
	/**
	 * @return the next
	 */
	public final IInvokeHandle getNext() {
		return next;
	}

	/**
	 * @param next the next to set
	 */
	public final void setNext(final IInvokeHandle next) {
		this.next = next;
	}

}