package suitebancomer.aplicaciones.commservice.request;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

import suitebancomer.aplicaciones.commservice.commons.ParametersTO;


/**
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class RequestService {

	private ParametersTO parametersTO;
	
	private HttpPost requestPost;
	private HttpGet requestGet;

	public ParametersTO getParametersTO() {
		return parametersTO;
	}

	public void setParametersTO(final ParametersTO parametersTO) {
		this.parametersTO = parametersTO;
	}
	
	/**
	 * 
	 * @param requestPost
	 */
	public void setRequestPost(final HttpPost requestPost){
		this.requestPost=requestPost;
	}
	
	/**
	 *
	 */
	public HttpPost getRequestPost(){
		return requestPost;
	}

	/**
	 *
	 * @param requestGet
	 */
	public void setRequestGet(final HttpGet requestGet){
		this.requestGet=requestGet;
	}


	public HttpGet getRequestGet(){
		return requestGet;
	}
}