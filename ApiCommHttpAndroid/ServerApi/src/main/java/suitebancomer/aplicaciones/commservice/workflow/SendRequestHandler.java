package suitebancomer.aplicaciones.commservice.workflow;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.util.Log;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomer.aplicaciones.commservice.request.RequestService;
import suitebancomer.aplicaciones.commservice.response.IResponseService;


/**
 * This class (a) handles requests it is responsible for, (b) can access its
 * successor, and (c) if it can handle the request, does so, else it forwards it
 * to its successor.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class SendRequestHandler extends AbstractInvokeHandler {
	
	private IInvokeHandle next;
	private IInvokeStrategy strategy;
	
	/**
	 * este verifica si se trata de una simulacion
	 * en caso de ser simulacion instancia LocalInvokeStrategy
	 * en caso contrario invocara HttpInvokeStrategy
	 * 
	 * enviando el request ya formado que se recibio
	 * 
	 * @param request
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws JSONException 
	 * @throws IllegalStateException 
	 */
	public IResponseService handleRequest(final RequestService request, final Class<?> objResponse) 
			throws ClientProtocolException, IOException, IllegalStateException, JSONException{
		
		//si no se trata de una simulacion instancia a HttpInvokeStrategy
		//de lo contrario insttancia LocalInvokeStrategy
		
		if(request.getParametersTO().isSimulation()){
			strategy=new LocalInvokeStrategy();
		}else{
			strategy=new HttpInvokeStrategy();
		}
		
		if (CommContext.allowLog && request.getRequestPost().getURI() !=null){
			String url="";
			if(request.getRequestGet()!=null){
				url=request.getRequestGet().getURI().toString();
			}else{
				url=request.getRequestPost().getURI().toString();
			}
			Log.d("APIComm:SendRequest", url);
		}
		
		//se obtiene la respuesta de la peticion http
		final IResponseService resphttp = strategy.process( request);
		((AdaptResponseHandler)next).setResponseService(resphttp);
		/**
		 * sendrequestHandle ya recibio el request que retorno el servidor web
		 * ahora llama el AdaptResponseHandler enviando el request que obtuvo previamente
		 * */
		
		//transforma el resultado de la peticion
		return next.handleRequest(request, objResponse);
	}

	public IResponseService handleRequestImage(final RequestService request, final Class<?> objResponse)
			throws ClientProtocolException, IOException, IllegalStateException, JSONException{
		if(request.getParametersTO().isSimulation()){
			strategy=new LocalInvokeStrategy();
		}else{
			strategy=new HttpInvokeStrategy();
		}

		if (CommContext.allowLog && request.getRequestPost().getURI() !=null){
				Log.d("APIComm:SendRequest", request.getRequestPost().getURI().toString());
		}

		//se obtiene la respuesta de la peticion http
		final IResponseService resphttp = strategy.process( request);
		((AdaptResponseHandler)next).setResponseService(resphttp);
		/**
		 * sendrequestHandle ya recibio el request que retorno el servidor web
		 * ahora llama el AdaptResponseHandler enviando el request que obtuvo previamente
		 * */

		//transforma el resultado de la peticion
		return next.handleRequestImage(request, objResponse);
	}

	/**
	 * @return the next
	 */
	public final IInvokeHandle getNext() {
		return next;
	}

	/**
	 * @param next the next to set
	 */
	public final void setNext(final IInvokeHandle next) {
		this.next = next;
	}


}