package suitebancomer.aplicaciones.commservice.workflow;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import suitebancomer.aplicaciones.commservice.localcomm.LocalResponseBuilder;
import suitebancomer.aplicaciones.commservice.request.RequestService;
import suitebancomer.aplicaciones.commservice.response.IResponseService;


/**
 * This class implements the algorithm using the Strategy interface.
 * 
 * TODO hacer singleton este objeto
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class LocalInvokeStrategy implements IInvokeStrategy {

	private static final LocalResponseBuilder RESPONOSE_BUILDER  =new LocalResponseBuilder();

	@Override
	public IResponseService process(final RequestService aRequest)
			throws ClientProtocolException, IOException {
		
		return RESPONOSE_BUILDER.createRequestSimulation(aRequest);

	}

}