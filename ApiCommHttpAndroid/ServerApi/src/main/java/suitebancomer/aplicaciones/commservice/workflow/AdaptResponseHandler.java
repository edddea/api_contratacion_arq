package suitebancomer.aplicaciones.commservice.workflow;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import suitebancomer.aplicaciones.commservice.request.RequestService;
import suitebancomer.aplicaciones.commservice.response.AdapterFactory;
import suitebancomer.aplicaciones.commservice.response.IAdapter;
import suitebancomer.aplicaciones.commservice.response.IAdapterFactory;
import suitebancomer.aplicaciones.commservice.response.IResponseService;


/**
 * This class (a) handles requests it is responsible for, (b) can access its
 * successor, and (c) if it can handle the request, does so, else it forwards it
 * to its successor.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:21 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class AdaptResponseHandler extends AbstractInvokeHandler {
	
	private IInvokeHandle next;
	private IResponseService responseService;
	
	final IAdapterFactory adapterFactory;
	public AdaptResponseHandler(){
		adapterFactory=new AdapterFactory();
	}
	
	public IResponseService handleRequest(final RequestService request, final Class<?> objResponse) 
			throws ClientProtocolException, IOException, IllegalStateException, JSONException{
		
		//se llama al factory para obetener ya sea un Adapter o un jsonAdapter
		final IAdapter factory = adapterFactory.factoryMethod(request.getParametersTO().isJson());
				
		//se llama el metodo transformResponse del IAdapter
		//para transformar la respuesta
		factory.transformResponse(responseService,objResponse);
				
		return responseService;
	}
	public IResponseService handleRequestImage(final RequestService request, final Class<?> objResponse)
			throws ClientProtocolException, IOException, IllegalStateException, JSONException{
		Bitmap image = null;

		if (responseService.getResponse().getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
			final	HttpEntity entity = responseService.getResponse().getEntity();
			if ( entity != null) {
				final byte[] buffer = EntityUtils.toByteArray(entity);
				image = BitmapFactory.decodeByteArray(buffer, 0, buffer.length);
                responseService.setObjResponse(image);
                responseService.setResponseString(null);
			}
		}
		return responseService;
	}
	
	
	/**
	 * @return the next
	 */
	public final IInvokeHandle getNext() {
		return next;
	}

	/**
	 * @param next the next to set
	 */
	public final void setNext(final IInvokeHandle next) {
		this.next = next;
	}
	
	/**
	 * @param responseService the responseService to set
	 */
	public final void setResponseService(final IResponseService responseService) {
		this.responseService = responseService;
	}
}