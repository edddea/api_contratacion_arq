package suitebancomer.aplicaciones.commservice.response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.json.JSONException;
import org.json.JSONObject;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomer.aplicaciones.commservice.commons.PojoGeneral;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;


/**
 * This class implements the Product interface.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class JsonAdapter implements IAdapter {

	
	/**
	 * el json adapeter
	 * se encarga de llenar los parametros de response service 
	 * comoo el estatus el codigo del mensaje y el texto
	 * con la informacion del response que se recibio
	 * y el parseo de datos en json.
	 * 
	 * esta respuesta ya formada y se retorna asta el handle que invoco
	 * el api
	 * */
	public void transformResponse( final IResponseService response, final Class<?> objResponse)throws IllegalStateException, IOException, JSONException{
		//convierte la cadena el contenido en una cadena
		if(response.getResponse().getStatusLine().getStatusCode() == ApiConstants.ESTATUS_OK_CODE && response.getResponse().getEntity()!=null){
			final BufferedReader rd = new BufferedReader( new InputStreamReader(response.getResponse().getEntity().getContent(), "ISO-8859-1"));
			//convertimos la respuesta a string
			final StringBuffer result = new StringBuffer();
			String line = ApiConstants.EMPTY_LINE;
			
			while (line  != null) {
				result.append(line);
				line = rd.readLine();
			}
			if (CommContext.allowLog){
				Log.d("APIComm:JsonAdapter", result.toString() ); 
			}
			JSONObject json;
			try {
				 final Gson gson = new Gson();
				 final Object  m = gson.fromJson(result.toString(), objResponse);

				response.setObjResponse(m);
				response.setResponseString(result.toString());
				json = new JSONObject(result.toString());
				
		    } catch (JSONException ex) {
		    	json=null;
				response.setResponseString(result.toString() );
		    }catch (JsonSyntaxException ex){
				json=null;
				response.setResponseString(result.toString() );
			}

			if (objResponse != PojoGeneral.class){
				setCodesMessageToResponse(json,response);
			}

		}else{

			try {
				response.setObjResponse(objResponse.newInstance());
				setCodesMessageToResponse(null,response);
			} catch (InstantiationException e) {
				setCodesMessageToResponse(null,response);
			} catch (IllegalAccessException e) {
				setCodesMessageToResponse(null,response);
			}
		}
	}


	/**
	 * se llenan los parametro genericos del response 
	 * */
	private void setCodesMessageToResponse(final JSONObject jsonResponse,
											final IResponseService response) 
			throws IOException, JSONException {

        if (jsonResponse == null) {
        	response.setStatus(OPERATION_STATUS_UNKNOWN);
            response.setMessageCode( null);
            response.setMessageText(null);

        } else {

        	response.setStatus(OPERATION_STATUS_UNKNOWN);
            final Result result = parseResult(jsonResponse);
            final String statusText = result.getStatus();
            if (ApiConstants.STATUS_OK.equals(statusText)) {
            	response.setStatus(ApiConstants.OPERATION_SUCCESSFUL);
            } else if (ApiConstants.STATUS_OPTIONAL_UPDATE.equals(statusText)) {
            	response.setStatus(ApiConstants.OPERATION_OPTIONAL_UPDATE);
            } else if (ApiConstants.STATUS_WARNING.equals(statusText)) {
            	response.setStatus(ApiConstants.OPERATION_WARNING);
            } else if (ApiConstants.STATUS_ERROR.equals(statusText)) {
            	response.setStatus(ApiConstants.OPERATION_ERROR);
            	response.setUpdateURL(result.getUpdateURL());
            }

            response.setMessageCode(result.getCode());
            response.setMessageText(result.getMessage());

        }

    }
	
	 /**
     * Parse the operation result from received message.
     * 
     * @return the operation result
     * @throws IOException on communication errors
	 * @throws JSONException 
     * @throws ParsingException on parsing errors
     */
    private Result parseResult(final JSONObject jsonResponse) throws IOException, JSONException {
        Result result = null;
        final String status = jsonResponse.getString(ApiConstants.STATUS_TAG);
        if (status != null) {
           
            if (ApiConstants.STATUS_OK.equalsIgnoreCase(status) || ApiConstants.STATUS_OPTIONAL_UPDATE.equals(status)) {
                result = new Result(status, null, null);
            } else if (ApiConstants.STATUS_WARNING.equals(status) || ApiConstants.STATUS_ERROR.equals(status)) {
            	result = parseResultExt(jsonResponse, status);
            }
        }
        return result;
    }
    
    
	/**
	 * @param jsonResponse
	 * @param status
	 * @return
	 * @throws JSONException
	 */
	private Result parseResultExt(final JSONObject jsonResponse,
			final String status) throws JSONException {
		Result result;
		final String code = jsonResponse.getString(ApiConstants.CODE_TAG);
            
		final String messageAux = (code == null) ? null : jsonResponse.getString(ApiConstants.MESSAGE_TAG);
		final String message = (messageAux == null) ? jsonResponse.getString(ApiConstants.MESSAGE_INFORMATIVO_TAG) : messageAux;
         
            
		String updateURL = null;
		if (code != null && code.equals(ApiConstants.CODE_MBANK1111)) {
		    updateURL = jsonResponse.getString(ApiConstants.URL_TAG);
		    result = new Result(status, code, message, updateURL);
		} else {
		    result = new Result(status, code, message);
		}
		return result;
	}

}