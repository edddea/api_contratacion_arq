package suitebancomer.aplicaciones.commservice.response;

import java.io.IOException;

import org.json.JSONException;


/**
 * This class defines the interface of objects the factory method creates.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public interface IAdapter {

	 /**
     * Unknown operation result.
     */
	 int OPERATION_STATUS_UNKNOWN = -1000;
    
	void transformResponse(IResponseService response,Class<?> objResponse) 
			throws IllegalStateException, IOException, JSONException;

}