package suitebancomer.aplicaciones.commservice.response;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.json.JSONException;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import android.util.Log;




/**
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:21 p.m.
 * 
 * IDS Comercial S.A. de C.V
 * 
 */
public class Adapter implements IAdapter {

	public void transformResponse(final IResponseService response, final Class<?> objResponse) throws IllegalStateException, IOException, JSONException {
		//TODO implements

		//convierte la cadena el contenido en una cadena
		if(response.getResponse().getStatusLine().getStatusCode()
				== ApiConstants.ESTATUS_OK_CODE && response.getResponse().getEntity()!=null){
			getEntity(response);

			//si la peticion es de login se realia una exception ya que loginn tabien aceptara las respuesta aunque no se 200
		}else if(objResponse.getSimpleName().equals("LoginData")
				&& response.getResponse().getEntity()!=null){
			getEntity(response);
		}
	}

	/**
	 * metodo que extrae la informacion de la peticon y lo pasa a un string
	 * cuano la peticon tenga un getEntity
	 * @param response
	 * @throws IllegalStateException
	 * @throws IOException
	 * @throws JSONException
	 */
	private void getEntity(final IResponseService response) throws IllegalStateException, IOException, JSONException {
		final BufferedReader rd = new BufferedReader( new InputStreamReader(response.getResponse().getEntity().getContent(), "ISO-8859-1"));
		//convertimos la respuesta a string
		final StringBuffer result = new StringBuffer();
		String line = ApiConstants.EMPTY_LINE;
		try {
			while (line  != null) {
				result.append(line);
				line = rd.readLine();
			}
			if (CommContext.allowLog){
				Log.d("APIComm:Adapter", result.toString() );
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			if (CommContext.allowLog){
				Log.e("APIComm:Adapter",e.getMessage());
			}
		}
		response.setResponseString(result.toString());
	}
}