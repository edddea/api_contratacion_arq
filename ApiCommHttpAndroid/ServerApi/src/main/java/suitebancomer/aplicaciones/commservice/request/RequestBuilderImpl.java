package suitebancomer.aplicaciones.commservice.request;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants.isJsonValueCode;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Encripcion;
import suitebancomercoms.aplicaciones.bmovil.classes.common.EncriptarConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * This class implements the Product interface.
 *
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * <p/>
 * IDS Comercial S.A. de C.V
 */
public class RequestBuilderImpl implements IRequestBuilder {

    public Mapper mapper;
    /**
     * esta clase se encarga de generar el request.
     * <p/>
     * 1.- obtiene el url a la que se hara la peticion llamando la clase Mapper
     * y en metodo getURLPattern.
     * <p/>
     * 2. parsea los parametros que envio el delegate a parametros
     * de la peticion post que se hara al server
     *
     * @throws UnsupportedEncodingException
     */
    private int opActual;

    public RequestBuilderImpl() {
        mapper = new Mapper();
    }

    public RequestService create(final RequestService request) throws UnsupportedEncodingException {

        final ParametersTO params = request.getParametersTO();
        //se agrega el operation code a los parametros del resquest
        request.getParametersTO().setOperationCode(getOperationCode(params.getOperationId()));
        opActual = params.getOperationId();
        //mapper.loadEnvironment(params.isDevelopment(), params.isEmulator(), params.getProvider());
        mapper.loadEnvironment(params.isDevelopment(), params.isEmulator());
        mapper.setupOperation(params.getOperationId(), params.getProvider(), params.isDevelopment(), ServerCommons.ARQSERVICE);
        //String operationCode=mapper.getOperationCode(params.getOperationId());

        //desde el mapper se obtiene la url del servidor ya sea para produccion o para test
        final String urlPatternRequest = mapper.getURLPattern(params.getOperationId());
        if (CommContext.allowLog) {
            Log.d("APIComm:RequestBuilderImpl", urlPatternRequest);
        }

        final HttpPost httpPost = new HttpPost(urlPatternRequest);
        HttpGet httpGet = null;

        //se agregan los parametros al request
        List<NameValuePair> listParams = null;
        Boolean requestCuentaDigital = false;

        try {
            if (params.getOperationId() == ApiConstants.CONSULTA_CURP || params.getOperationId() == ApiConstants.CONSULTA_CONTRATO || params.getOperationId() == ApiConstants.CONSULTA_GENERA_OTP || params.getOperationId() == ApiConstants.CONSULTA_VALIDA_OTP || params.getOperationId() == ApiConstants.CONSULTA_COLONIAS) {
                String digital = setParametersCuentaDIgital(request.getParametersTO());
                httpPost.addHeader("Content-type", "application/json");
                httpPost.setEntity(new StringEntity(digital));
                requestCuentaDigital = true;
            } else {
                listParams = setParameters(request.getParametersTO());


                if (mapper.isByArqService(params.getOperationId()) || params.getOperationId() == ApiConstants.OP_GLOBAL) {
                    if (params.getOperationId() == ApiConstants.CLOSE_SESSION_OPERATION) {
                        //el logout no usa parametros
                        listParams = new ArrayList<NameValuePair>();
                    } else if (params.getOperationId() == ApiConstants.OP_LOGIN_FN) {
                        httpPost.setHeader(HTTP.CONTENT_TYPE, "application/json");
                        JSONObject jo = setParametersLoginArqJson(request.getParametersTO());
                        listParams = null;
                        httpPost.setEntity(new StringEntity(jo.toString(), "UTF8"));
                    } else if (params.getOperationId() == ApiConstants.OP_GLOBAL) {
                        httpGet = new HttpGet(urlPatternRequest);
                        httpGet.setHeader(HTTP.CONTENT_TYPE, "application/json");
                        setParametersGlobalJson(request.getParametersTO(), httpGet);
                        listParams = null;
                        //httpPost.setEntity(new StringEntity(jo.toString(), "UTF8"));
                    } else {
                        httpPost.setHeader(HTTP.CONTENT_TYPE, "application/x-www-form-urlencoded");
                        String urlTemp = mapper.getUrlOriginal(params.getOperationId(), params.isDevelopment());
                        urlTemp = urlTemp + getParamsToGetMethod(listParams);
                        listParams = new ArrayList<NameValuePair>();
                        listParams.add(new BasicNameValuePair(ApiConstants.CADENA_STRING_LITERAL, urlTemp));

                    }
                }
            }
            if (CommContext.allowLog) {
                if (listParams != null) {
                    Log.d("APIComm:RequestBuilderImpl", listParams.toString());
                    Log.e("APIComm: REQUEST", "url: " + urlPatternRequest + ApiConstants.EMPTY_LINE + getParamsToGetMethod(listParams));
                } else {
                    Log.e("APIComm: REQUEST", "url: " + urlPatternRequest + ApiConstants.EMPTY_LINE);
                }
            }

        } catch (JSONException e) {
            if (CommContext.allowLog) {
                Log.v(ApiConstants.TAG, e.getMessage());
            }
        }

        //se crea el resquest
        if (requestCuentaDigital) {
            request.setRequestPost(httpPost);
            return request;
        } else {
            return build(request, httpPost, listParams, httpGet);
        }
    }

    private String setParametersCuentaDIgital(ParametersTO parametersTO) {
        final Hashtable<String, String> map = (Hashtable<String, String>) parametersTO.getParameters();
        final Hashtable<String, String> mapNoJson = (Hashtable<String, String>) parametersTO.getParameters();
        convertValuesToString(map);
        JSONObject json = new JSONObject();
        try {
            json = new JSONObject(map.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public RequestService createImage(final RequestService request) throws UnsupportedEncodingException {

        final ParametersTO params = request.getParametersTO();
        final Hashtable<String, String> params2 = (Hashtable<String, String>) params.getParameters();
        //Esta peticion no trae parametros
        //request.getParametersTO().setOperationCode(getOperationCode(params.getOperationId()));

        //mapper.loadEnvironment(params.isDevelopment(), params.isEmulator(), params.getProvider());
        mapper.loadEnvironment(params.isDevelopment(), params.isEmulator());
        mapper.setupOperation(params.getOperationId(), params.getProvider(), params.isDevelopment(), ServerCommons.ARQSERVICE);
        //String operationCode=mapper.getOperationCode(params.getOperationId());

        final StringBuffer sb = new StringBuffer();
        sb.append((String) params2.get(ApiConstants.SCREEN_SIZE_PARAM)).append("_").append((String) params2.get(ApiConstants.SERVICE_PROVIDER_PARAM)).append("_").append((String) params2.get(ApiConstants.HELP_IMAGE_TYPE_PARAM)).append(".png");
        //Construyendo la peticion
        final StringBuffer url = new StringBuffer();
        url.append(mapper.getURLPattern(params.getOperationId())).append("/").append(sb.toString());

        //desde el mapper se obtiene la url del servidor ya sea para produccion o para test
        final String urlPatternRequest = url.toString();
        if (CommContext.allowLog) {
            Log.d("APIComm:RequestBuilderImpl", urlPatternRequest);
        }

        final HttpPost httpPost = new HttpPost(urlPatternRequest);


        //se crea el resquest
        return build(request, httpPost, null, null);

    }

    public List<NameValuePair> setParametersLoginArq(final ParametersTO parameters) {
        final Hashtable<String, String> mapNoJson = (Hashtable<String, String>) ((Hashtable<String, String>) parameters.getParameters()).clone();

        convertStringToValues(mapNoJson);
        final List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair(ApiConstants.USR_STRING, mapNoJson.get(ApiConstants.USR_STRING)));
        params.add(new BasicNameValuePair(ApiConstants.PSW_STRING, mapNoJson.get(ApiConstants.PSW_STRING)));
        params.add(new BasicNameValuePair(ApiConstants.SC_STRING, mapNoJson.get(ApiConstants.SC_STRING)));
        params.add(new BasicNameValuePair(ApiConstants.IUM_STRING, mapNoJson.get(ApiConstants.IUM_STRING)));
        return params;
    }

    public JSONObject setParametersLoginArqJson(final ParametersTO parameters) {
        final Hashtable<String, String> mapNoJson = (Hashtable<String, String>) ((Hashtable<String, String>) parameters.getParameters()).clone();

        convertStringToValues(mapNoJson);
        JSONObject object = null;
        try {
            object = new JSONObject();
            object.put(ApiConstants.USR_STRING, mapNoJson.get(ApiConstants.USR_STRING));
            object.put(ApiConstants.PSW_STRING, mapNoJson.get(ApiConstants.PSW_STRING));
            object.put(ApiConstants.SC_STRING, mapNoJson.get(ApiConstants.SC_STRING));
            object.put(ApiConstants.IUM_STRING, mapNoJson.get(ApiConstants.IUM_STRING));
        } catch (JSONException e) {
            object = new JSONObject();
        }
        return object;
    }

    public void setParametersGlobalJson(final ParametersTO parameters, final HttpGet httpGet) {
        final Hashtable<String, String> mapNoJson = (Hashtable<String, String>) ((Hashtable<String, String>) parameters.getParameters()).clone();

        convertStringToValues(mapNoJson);
        httpGet.setHeader(ApiConstants.IUM_STRING, mapNoJson.get(ApiConstants.IUM_STRING));
        //httpGet.setHeader(ApiConstants.COOKIE_MAYUS, mapNoJson.get(ApiConstants.COOKIE));
        //	JSONObject object=null;
        //	try {
        //		object=new JSONObject();
        //		object.put(ApiConstants.IUM_STRING, mapNoJson.get(ApiConstants.IUM_STRING));
        //		object.put(ApiConstants.SC_STRING, mapNoJson.get(ApiConstants.COOKIE));
        //	} catch (JSONException e) {
        //		object=new JSONObject();
        //	}
        //	return object;
    }

    /**
     * que convierte los parametros de la clase ParametersTO
     * a una lista de NameValuePair
     *
     * @throws JSONException
     */
    @SuppressWarnings("unchecked")
    public List<NameValuePair> setParameters(final ParametersTO parameters) throws JSONException {
        final Hashtable<String, String> map = (Hashtable<String, String>) parameters.getParameters();
        final Hashtable<String, String> mapNoJson = (Hashtable<String, String>) parameters.getParameters();
        convertValuesToString(map);
        String paramsString;


        if (parameters.isJson()) {
            paramsString = setParametersAux(parameters, map);
        } else {
            paramsString = formaParamsNoJson(parameters.getOperationCode(), mapNoJson);

            if (CommContext.operacionCode != null &&
                    CommContext.operacionCode.equals(parameters.getOperationId()) && EncriptarConstants.encriptar) {
                final Boolean exist = mapNoJson.containsKey(ApiConstants.ORDER);
                if (exist) {
                    mapNoJson.remove(ApiConstants.ORDER);
                }
                mapNoJson.put("OP", parameters.getOperationCode());
                Encripcion.setContext(CommContext.getContext());
                Encripcion.encriptarPeticionString(mapNoJson, CommContext.listaEncriptar, "*", "*", true);
                paramsString = formaParamsNoJson(parameters.getOperationCode(), mapNoJson);
                if (paramsString.startsWith("OP") && mapNoJson.size() == 1) {
                    //se quita el parametro op ya que no debe ir cuando se encripta
                    paramsString = paramsString.subSequence(paramsString.indexOf("*EN") + 1, paramsString.length()).toString();
                }
                CommContext.operacionCode = null;
                CommContext.listaEncriptar = null;
            }


        }
        String operacion = getOperationCodeV(parameters.getOperationCode(), parameters.getIsJsonValue(), parameters.isJson());

        if (parameters.getOperationId() == ApiConstants.ID_SENDER) {
            //operacion=ApiConstants.JSON_OPERATION_ALERTAS_DIGITALES;
            operacion = ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
        }

        final List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair(ApiConstants.OPERACION_MAYUS, operacion));
        params.add(new BasicNameValuePair(ApiConstants.OPERATION_LOCALE_PARAMETER, ApiConstants.OPERATION_LOCALE_VALUE));
        params.add(new BasicNameValuePair(ApiConstants.PARAM_PAR_INICIO, paramsString));

        return params;
    }

    private String setParametersAux(final ParametersTO parameters, final Map<String, String> map) throws JSONException {
        JSONObject json;
        String paramsAux;
        try {
            if (CommContext.operacionCode != null &&
                    CommContext.operacionCode.equals(parameters.getOperationId())) {
                map.put(ApiConstants.OPERACION, parameters.getOperationCode());
                Encripcion.setContext(CommContext.getContext());
                Encripcion.encriptaCadenaAutenticacion(map, CommContext.listaEncriptar);
                json = new JSONObject(map.toString());
                CommContext.operacionCode = null;
                CommContext.listaEncriptar = null;
            } else {
                json = new JSONObject(map.toString());
                json.put(ApiConstants.OPERACION, parameters.getOperationCode());
            }
            paramsAux = json.toString();
        } catch (JSONException e) {
            json = new JSONObject(map);
            json.put(ApiConstants.OPERACION, parameters.getOperationCode());
            paramsAux = json.toString();
            paramsAux = paramsAux.replaceAll("\\\\\"", "\"").replaceAll("\"\"", "\"");
            paramsAux = paramsAux.replaceAll("\"\\{", "{").replaceAll("\\}\"", "}");

        }
        return paramsAux.replaceAll(ApiConstants.DOBLE_SLASH, ApiConstants.EMPTY_LINE);
    }

    public String formaParamsNoJson(final String operacion, final Map<String, String> parameters) {
        final StringBuffer sb = new StringBuffer("OP").append(operacion);
        if (parameters != null) {
            final Boolean exist = parameters.containsKey(ApiConstants.ORDER);
            if (exist) {
                String orden = parameters.get(ApiConstants.ORDER);
                orden = orden.replace("\"", "");

                final StringTokenizer st2 = new StringTokenizer(orden, ApiConstants.PARAMETRO_SEPARADOR);
                while (st2.hasMoreElements()) {
                    final String elementName = st2.nextElement().toString();
                    sb.append(ApiConstants.PARAMETRO_SEPARADOR);
                    sb.append(elementName);
                    sb.append(parameters.get(elementName));
                }
            } else {
                for (final String key : parameters.keySet()) {
                    sb.append(ApiConstants.PARAMETRO_SEPARADOR);
                    sb.append(key);
                    sb.append(parameters.get(key));
                }
            }
        }
        return sb.toString().replace("\"", "");
    }

    public void convertValuesToString(final Map<String, String> map) {

        for (final String key : map.keySet()) {
            map.put(key, ApiConstants.SLASH + map.get(key) + ApiConstants.SLASH);
        }
    }

    public void convertStringToValues(final Map<String, String> map) {
        for (final String key : map.keySet()) {
            map.put(key, map.get(key).toString().replace("\"", ""));
        }
    }

    /**
     * metodo para asignar los parametros a las peticiones
     * post
     */
    public RequestService build(final RequestService request, final HttpPost httpPost,
                                final List<NameValuePair> listParams, final HttpGet httpGet) throws UnsupportedEncodingException {
        if (httpGet != null) {
            request.setRequestPost(httpPost);
            request.setRequestGet(httpGet);
        } else {
            request.setRequestGet(null);
            if (listParams != null) {
                httpPost.setEntity(new UrlEncodedFormEntity(listParams));
            }
            request.setRequestPost(httpPost);
        }
        return request;
    }

    /* TODO refactor
     *
     */
    public String getOperationCodeAux(final String operation) {
        String operationCodeValueBMovil = ApiConstants.EMPTY_LINE;
        if (ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_TDC].equals(operation)) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.FAVORITE_PAYMENT_OPERATION_BBVA])) {
            operationCodeValueBMovil = ApiConstants.JSON_OPERATION_CONSULTA_INTERBANCARIOS;
        } else {
            operationCodeValueBMovil = getOperationCodeAux1(operation);

            if (ApiConstants.EMPTY_LINE.equals(operationCodeValueBMovil)) {
                operationCodeValueBMovil = getOperationCodeAux2(operation);

                if (ApiConstants.EMPTY_LINE.equals(operationCodeValueBMovil)) {
                    operationCodeValueBMovil = getOperationCodeAux3(operation);

                    if (ApiConstants.EMPTY_LINE.equals(operationCodeValueBMovil)) {
                        operationCodeValueBMovil = getOperationCodeAux4(operation);

                        if (ApiConstants.EMPTY_LINE.equals(operationCodeValueBMovil)) {
                            operationCodeValueBMovil = getOperationCodeAux5(operation);
                        }
                    }
                }
            }
        }

        return operationCodeValueBMovil;
    }

    private String getOperationCodeAux5(final String operation) {
        String operationCodeValueBMovil = ApiConstants.EMPTY_LINE;
        if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_IMPORTES_PAGO_CREDITOS])) {
            operationCodeValueBMovil = ApiConstants.CODE_TEMP;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_PAGO_CREDITO_CH])) {
            operationCodeValueBMovil = ApiConstants.CODE_TEMP;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_MIGRA_BASICO])) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE_RECORTADO;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CONSULTA_DETALLE_OFERTA_TDC]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CONSULTA_TERMINOS_CONDICIONES_TDC]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.ACEPTACION_OFERTA_TDC]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.FORMATOS_FINALES_TDC]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OFERTA_DOMICILIACION])
                ) {
            operationCodeValueBMovil = ApiConstants.JSON_OPERATION_VENTA_TDC;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_PATRIMONIAL])) {
            operationCodeValueBMovil = ApiConstants.JSON_OPERATION_CODE_PATRIMONIAL;

        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_ENVIO_CORREO]) && 53 == opActual) {
            operationCodeValueBMovil = ApiConstants.JSON_OPERATION_CODE_VALUE;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSUTA_CORREO])
                || operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CALCULO_ALTERNATIVA])
                || operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_ENVIO_CORREOS])
                || operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSUTA_ALTERNATIVA])
                || operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_DETALLE_ALTERNATIVA])
                || operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONTRATA_ALTERNATIVA_CONSUMO])
                ) {
            operationCodeValueBMovil = ApiConstants.JSON_OPERATION_CODE_CREDITOS;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_COORDENADAS])) {
            operationCodeValueBMovil = ApiConstants.JSON_OPERATION_CODE_VALUE_PM;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_AUTENTICACIONTOKENPM])) {
            operationCodeValueBMovil = ApiConstants.JSON_OPERATION_CODE_VALUE_PM;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_SINCRONIZACIONPMTOKEN])) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE_PM;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_EXPORTACIONPMTOKEN])) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE_PM;
        }
        return operationCodeValueBMovil;
    }

    private String getOperationCodeAux4(final String operation) {
        String operationCodeValueBMovil = ApiConstants.EMPTY_LINE;
        if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CONSULTAR_ESTATUS_EC])
                || operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.ACTUALIZAR_ESTATUS_EC]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CONSULTAR_TEXTO_PAPERLESS]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.INHIBIR_ENVIO_EC])
                ) {
            operationCodeValueBMovil = ApiConstants.JSON_OPERATION_CODE_VALUE_PAPERLESS;

        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_OTROS_CREDITOS]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_DETALLE_OTROS_CREDITOS]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_SMS_OTROS_CREDITOS]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_CORREO_OTROS_CREDITOS])
                ) {
            operationCodeValueBMovil = ApiConstants.CODE_TEMP;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_INVERSIONES_PLAZO])) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
        }
        return operationCodeValueBMovil;
    }

    private String getOperationCodeAux3(final String operation) {
        String operationCodeValueBMovil = ApiConstants.EMPTY_LINE;

        if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CHANGE_PASSWORD_OPERATION])) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.LOGIN_OPERATION])) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_CUENTAS_ALERTAS]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_DETALLE_CUENTA_ALERTAS]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_ALTA_MOD_SERVICIO_ALERTAS]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_BAJA_SERVICIO_ALERTAS]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_MENSAJES_ENVIADOS_ALERTAS]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_DETALLE_MENSAJE_ENV_ALERTAS]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_ID_SENDER])
                ) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_ALERTAS;

        }
        return operationCodeValueBMovil;
    }

    private String getOperationCodeAux2(final String operation) {
        String operationCodeValueBMovil = ApiConstants.EMPTY_LINE;
        if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_TDC])) {
            operationCodeValueBMovil = ApiConstants.JSON_OPERATION_MEJORAS_CODE_VALUE;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_SINC_EXP_TOKEN])) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.SINCRONIZACION_SOFTTOKEN])) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CLOSE_SESSION_OPERATION])) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_ACTIVACION])) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.EXPORTACION_SOFTTOKEN])) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CATALOGO_APLICACIONES])) {
            operationCodeValueBMovil = ApiConstants.CBC_CODE_VALUE;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.HAMBURGUESA])) {
            operationCodeValueBMovil = ApiConstants.CBC_CODE_VALUE;
        }
        return operationCodeValueBMovil;
    }

    private String getOperationCodeAux1(final String operation) {
        String operationCodeValueBMovil = ApiConstants.EMPTY_LINE;
        if (ApiConstants.OPERATION_CODES[ApiConstants.OP_RETIRO_SIN_TAR].equals(operation)) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
        } else if (ApiConstants.OPERATION_CODES[ApiConstants.CONSULTA_SIN_TARJETA].equals(operation)) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
        } else if (ApiConstants.OPERATION_CODES[ApiConstants.OP_RETIRO_SIN_TAR_12_DIGITOS].equals(operation)) {
            operationCodeValueBMovil = ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_SOLICITAR_ALERTAS])
                || operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.CAMBIA_PERFIL])) {
            operationCodeValueBMovil = ApiConstants.JSON_OPERATION_RECORTADO_CODE_VALUE;
        } else if (operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_INTERBANCARIOS]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_SPEI]) ||
                operation.equals(ApiConstants.OPERATION_CODES[ApiConstants.OP_ENVIOCORREO_SPEI])) {
            operationCodeValueBMovil = ApiConstants.JSON_OPERATION_CONSULTA_INTERBANCARIOS;
        }

        return operationCodeValueBMovil;
    }


    private String getOperationCodeAuxNotJson(final String operation) {
        if (ApiConstants.OPERATION_CODES[ApiConstants.OP_CONSULTA_TDC].equals(operation)) {
            return ApiConstants.OPERATION_CODE_VALUE_BMOVIL_MEJORADO;

        } else if (ApiConstants.OPERATION_CODES[ApiConstants.OP_SOLICITAR_ALERTAS].equals(operation)) {
            return ApiConstants.OPERATION_CODE_VALUE_RECORTADO;
        } else {
            return ApiConstants.OPERATION_CODE_VALUE;
        }
    }

    /* TODO refactor
     *
     */
    public String getOperationCodeV(final String operation, final isJsonValueCode isJsonValue, final boolean isJson) {
        String operationCodeValueBMovil = ApiConstants.EMPTY_LINE;
        //JAIG SE AGREGA PARA OPERACIONES NO JSON
        if (isJson) {
            operationCodeValueBMovil = getOperationCodeAux(operation);
        } else {
            operationCodeValueBMovil = getOperationCodeAuxNotJson(operation);
        }
        if (operationCodeValueBMovil.equals(ApiConstants.EMPTY_LINE)) {
            //One Click
            if (isJsonValue.name() == ApiConstants.ONCLICK) {
                operationCodeValueBMovil = ApiConstants.JSON_OPERATION_CODE_VALUE_ONECLICK;
            } else if (isJsonValue.name() == ApiConstants.CONSUMO) {
                operationCodeValueBMovil = ApiConstants.JSON_OPERATION_CODE_VALUE_CONSUMO;
            } else if (isJsonValue.name() == ApiConstants.DEPOSITOS) {
                operationCodeValueBMovil = ApiConstants.JSON_OPERATION_CODE_VALUE_DEPOSITOS;
            } else {
                operationCodeValueBMovil = ApiConstants.JSON_OPERATION_CODE_VALUE;
            }
            //Termina One click
            //sb.append(Server.JSON_OPERATION_CODE_VALUE);
        }
        return operationCodeValueBMovil;

    }

    /**
     * metodo para obtener el nombre de la operacion desde
     * el properties con el id de operacion que recive el api
     */
    private String getOperationCode(final int operationId) {

        //se forma el id que se buscara el el properties de configuracion para traer el nombbre de la operacion
        final StringBuilder operationName = new StringBuilder(ApiConstants.PREFIX_OPERACION_CODE);
        operationName.append(operationId);
        //se obtiene el operationCode
        return CommContext.getConfigPropertyValue(operationName.toString());
    }

    /**
     * Metodo que pasa un ArrayList<NameValuePair>
     * a un string de url tipo get
     *
     * @param parametros
     * @return String
     */
    private String getParamsToGetMethod(final List<NameValuePair> parametros) {
        final StringBuffer sb = new StringBuffer();
        if (parametros != null && parametros.size() > 0) {
            sb.append(ApiConstants.SIGNO_INTERROGACION);
        }
        int index = 0;
        for (final NameValuePair nvPair : parametros) {
            sb.append(nvPair.getName());
            sb.append(ApiConstants.SIGNO_IGUAL);
            sb.append(nvPair.getValue());
            index++;
            if (parametros.size() > index) {
                sb.append(ApiConstants.AMP_STRING);
            }

        }
        return sb.toString();
    }
}