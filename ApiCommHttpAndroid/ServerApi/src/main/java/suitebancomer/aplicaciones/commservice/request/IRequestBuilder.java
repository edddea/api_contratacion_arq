package suitebancomer.aplicaciones.commservice.request;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;


/**
 * This class defines the interface of objects the factory method creates.
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public interface IRequestBuilder {

	RequestService build(RequestService request,HttpPost httpPost,List <NameValuePair> listParams,final HttpGet httpGet )
			throws UnsupportedEncodingException;
	RequestService create(RequestService request) throws UnsupportedEncodingException;
	RequestService createImage(RequestService request) throws UnsupportedEncodingException;
	
}