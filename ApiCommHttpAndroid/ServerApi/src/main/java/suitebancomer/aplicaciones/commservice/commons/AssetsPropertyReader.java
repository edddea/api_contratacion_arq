/**
 * 
 */
package suitebancomer.aplicaciones.commservice.commons;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

/**
 * @author lbermejo
 * 
 * IDS Comercial S.A. de C.V
 */
public class AssetsPropertyReader {

	private final Context context;
    private final Properties properties;

	public AssetsPropertyReader(final Context context) {
		this.context = context;
		
		/**
		 * Constructs a new Properties object.
		 */
		properties = new Properties();
	}

	public Properties getProperties(final String FileName) {

		try {
			/**
			 * getAssets() Return an AssetManager instance for your
			 * application's package. AssetManager Provides access to an
			 * application's raw asset files;
			 */
			final AssetManager assetManager = context.getAssets();
			/**
			 * Open an asset using ACCESS_STREAMING mode. This
			 */
			final InputStream inputStream = assetManager.open(FileName);
			/**
			 * Loads properties from the specified InputStream,
			 */
			properties.load(inputStream);

		} catch (IOException e) {
			if(CommContext.allowLog){
				Log.e("APIComm:AssetsPropertyReader", e.toString()); }
		}
		
		return properties;

	}

}
