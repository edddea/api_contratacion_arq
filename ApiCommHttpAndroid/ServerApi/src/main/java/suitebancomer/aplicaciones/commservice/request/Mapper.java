package suitebancomer.aplicaciones.commservice.request;

import java.util.Hashtable;
import java.util.Map;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;


/**
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class Mapper {
	
	/**
	 * Base URL for providers.
	 */
	public static String[] baseUrl;

	/**
	 * Path table for provider/operation URLs.
	 */
	//public static String[][] OPERATIONS_PATH;
	public static Map<String, String> operationsPath;
	
	private boolean emulator;
	
	/**
	 * Operation URLs map.
	 */
	public static Map<String, String> operatonsMap = new Hashtable<String, String>();

	/**
	 * Operation providers map.
	 */
	public static Map<String, Integer> operationsProviders = new Hashtable<String, Integer>();


	public static int provider; // TODO remove for TEST
	
	/**
	 * Setup production server paths.
	 */
	private  void setupProductionServer() {
		
		//obtiene las urls del properties
		final String urlProduccion=CommContext.getConfigPropertyValue(ApiConstants.KEY_PRODUCTION_URL);
		final String urlProduccionEmulator=CommContext.getConfigPropertyValue(ApiConstants.KEY_PRODUCTION_EMULATOR_URL);
		
		baseUrl = new String[] {
				urlProduccion,
				emulator ? urlProduccionEmulator
						: ApiConstants.EMPTY_LINE };
		operationsPath = new Hashtable<String, String>();
	}

	/**
	 * Setup production server paths.
	 */
	private  void setupDevelopmentServer() {
		
		//obtiene las urls del properties
		final String urlDev=CommContext.getConfigPropertyValue(ApiConstants.KEY_DEVELOP_URL);
		final String urlDevEmulator=CommContext.getConfigPropertyValue(ApiConstants.KEY_DEVELOP_EMULATOR_URL);
		
		baseUrl = new String[] {
				urlDev,
				emulator ? urlDevEmulator
						:  ApiConstants.EMPTY_LINE };
		operationsPath = new Hashtable<String, String>();
	}



	//=============================================================================
	/* *
	 * Setup operations for a provider.
	 * 
	 * @param provider
	 *            the provider
	 * /
	private static void setupOperations(int provider) {
		setupOperation(ApiConstants.CONSULTA_MANTENIMIENTO, provider);// OPconsultaMantenimiento
	}
	
	
	/* *
	 * Setup operation for a provider.
	 * @param operation the operation
	 * @param provider the provider
	 * /
	private static void setupOperation(int operation, int provider) {
		String opetationCode = ApiConstants.OPERATION_CODES[operation];
		OPERATIONS_MAP.put(
				opetationCode,
				new StringBuffer(BASE_URL[provider]).append(
						OPERATIONS_PATH[provider][operation]).toString());
		OPERATIONS_PROVIDER.put(opetationCode, Integer.valueOf(provider));
	}*/
	//=============================================================================
	
	
	public void loadEnvironment(final boolean develoment, final boolean emulator){ //,int operation,int provider
		this.emulator=emulator;
		if(develoment){
			setupDevelopmentServer();
		}else{
			setupProductionServer();
		}
		
		//setupOperations(provider);
		//setupOperation(operation, provider);
	}
	
	/**
	 * mapper.setupOperation(params.getOperationId() = 45, operationCode, params.getProvider());
	 * Setup operation for a provider.
	 * @param operation the operation
	 * @param provider the provider
	 */
	public void setupOperation(final int operation, final int provider, final boolean isDevelop, final boolean isArqServ) {
		//String opetationCode = ApiConstants.OPERATION_CODES[operation];
		
		//se forma el id que se buscara el el properties de configuracion para traer el nombbre de la operacion
		final StringBuilder operationName = new StringBuilder(ApiConstants.PREFIX_OPERACION_CODE);
				operationName.append(operation);
		
		//se obtiene el operationCode
		final String operationCode=CommContext.getConfigPropertyValue(operationName.toString());
		
		//se forma el id del properties para buscar el path de produccion o de desarrollo
		StringBuilder operationPathProp = new StringBuilder(ApiConstants.EMPTY_LINE);
		final StringBuilder operationPathPropArq = new StringBuilder(ApiConstants.EMPTY_LINE);
		if(isDevelop){
				operationPathPropArq.append(ApiConstants.PREFIX_URL_ARQ_DEVELOPMENT);
				operationPathProp.append(ApiConstants.PREFIX_URL_DEVELOPMENT);
		}else{
				operationPathPropArq.append(ApiConstants.PREFIX_URL_ARQ_PRODUCTION);
				operationPathProp.append(ApiConstants.PREFIX_URL_PRODUCTION);
		}
		operationPathProp.append(operation);
		operationPathPropArq.append(operation);
		//se saca la url por arquitectura
		String operationPath=CommContext.getConfigPropertyValue(operationPathPropArq.toString());

		//si la bandera de arquitectura esta apagada o no tiene definida una url por arquitectura
		if(!isArqServ || operationPath==null){
			//se obtiene la url de la operacion por nacar
			operationPath = CommContext.getConfigPropertyValue(operationPathProp.toString());
		}
		operatonsMap.put(
				operationCode,
				new StringBuffer(baseUrl[provider])
					.append( operationPath)
					.toString()); 
				//OPERATIONS_PATH[provider][operation]).toString()
		operationsProviders.put(operationCode, Integer.valueOf(provider));
	}
	
	
	

	public String getURLPattern(final int operationId){
		//se forma el id que se buscara el el properties de configuracion para traer el nombbre de la operacion
		final StringBuilder operationName = new StringBuilder(ApiConstants.PREFIX_OPERACION_CODE);
		operationName.append(operationId);
		
		//se obtiene el operationCode
		final String operationCode=CommContext.getConfigPropertyValue(operationName.toString());
		return (String) operatonsMap.get(operationCode);
	}

	/**
	 * metodo que resuelbe si la operacion sera por arq o por nacar
	 * @param operation
	 * @return
	 */
	public Boolean isByArqService(int operation){
		Boolean isArq=Boolean.FALSE;
		if(ServerCommons.ARQSERVICE){
			final StringBuilder operationPathPropArq = new StringBuilder(ApiConstants.EMPTY_LINE);
			operationPathPropArq.append(ApiConstants.PREFIX_URL_ARQ_PRODUCTION);
			operationPathPropArq.append(operation);
			if(CommContext.getConfigPropertyValue(operationPathPropArq.toString())!=null){
				isArq=Boolean.TRUE;
			}
		}
		return isArq;
	}

	/**
	 * regresa la parte de la url original que requiere el arq service
	 * @param operation
	 * @param isDevelop
	 * @return
	 */
	public String getUrlOriginal(final int operation, final boolean isDevelop) {
			//se forma el id que se buscara el el properties de configuracion para traer el nombbre de la operacion
		final StringBuilder operationName = new StringBuilder(ApiConstants.PREFIX_OPERACION_CODE);
		operationName.append(operation);

		//se obtiene el operationCode
		final String operationCode=CommContext.getConfigPropertyValue(operationName.toString());

		//se forma el id del properties para buscar el path de produccion o de desarrollo
		StringBuilder operationPathProp = new StringBuilder(ApiConstants.EMPTY_LINE);
		if(isDevelop){
			operationPathProp.append(ApiConstants.PREFIX_URL_DEVELOPMENT);
		}else{
			operationPathProp.append(ApiConstants.PREFIX_URL_PRODUCTION);
		}
		operationPathProp.append(operation);
		//se saca la url por arquitectura
		 String operationPath=CommContext.getConfigPropertyValue(operationPathProp.toString());
		operationPath=operationPath.substring(1,operationPath.length());
		return  operationPath;

	}

}