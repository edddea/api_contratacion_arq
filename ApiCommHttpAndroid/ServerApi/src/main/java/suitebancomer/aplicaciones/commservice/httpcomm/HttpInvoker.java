package suitebancomer.aplicaciones.commservice.httpcomm;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.util.Log;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomer.aplicaciones.commservice.request.RequestService;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;


/**
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class HttpInvoker extends HttpRequestBase {
	
	/**
	 * http invoquer realiza la peticion post al servidor web con 
	 * el request que recibio enviando de regreso el response
	 * que recibio desde el servidor web
	 * 
	 */
	private static DefaultHttpClient client = new DefaultHttpClient();
	public IResponseService invoke(final RequestService aRequest) throws ClientProtocolException, IOException{
	
		//se forma el objeto de respuesta
		final IResponseService responseService=new ResponseServiceImpl();
		
		// Si la siguiente linea de codigo esta comentada entonces la aplicacion se comunica por la via 1 .173 cuando este en produccion, 
		// Si la linea de codigo no este comentada entonces se comunica por la via 2 .174
		aRequest.getRequestPost().addHeader(ApiConstants.LAN_STRING, ApiConstants.ANDROID_STRING);
					
		if(client == null){
			client = new DefaultHttpClient();
		}
		client.getConnectionManager().closeExpiredConnections();

		HttpResponse response = new BasicHttpResponse(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, ApiConstants.OK);		
		try {
			//Solo es de utilidad (cambio integracion Julio)
			//Utilities.getHeadersAsString(aRequest.getRequestPost());
			/*Cambio tiempo de espera en la peticion de Hamburguesa*/
			int idoperacion = aRequest.getParametersTO().getOperationId();
			HttpParams params = client.getParams();
			if (idoperacion == ApiConstants.HAMBURGUESA){
				HttpConnectionParams.setConnectionTimeout(params, 3000);
				HttpConnectionParams.setSoTimeout(params, 3000);
			}else {
				HttpConnectionParams.setConnectionTimeout(params, 300000);
				HttpConnectionParams.setSoTimeout(params, 300000);
			}

			if(aRequest.getRequestGet()!=null){
				response = client.execute(aRequest.getRequestGet());
			}else {
				response = client.execute(aRequest.getRequestPost());
			}

			final CookieStore cookieStore = client.getCookieStore();
			for (final Cookie cookies: cookieStore.getCookies()) {
				if (CommContext.allowLog){
					Log.d("APIComm:HttpInvoker COOKIES", cookies.getName()+" "+cookies.getValue() ); 
				}
			}
		} catch (IOException e) {
			if (CommContext.allowLog) {
				Log.v(ApiConstants.TAG, e.getMessage());
			}
		}
		// seteamos la respuesta a nuestro objeto de salida
		responseService.setResponse(response);
		if (CommContext.allowLog){
			Log.i("APIComm:HttpInvoker.invoke ", responseService.getResponse().toString()); }
		 
		 //retorna la respuesta del server
		return responseService;
		
	}


	public static org.apache.http.client.CookieStore getCookieStore(){
		return client.getCookieStore();
	}
	public static void setCookieStore(final org.apache.http.client.CookieStore cookieStore){
		 client.setCookieStore(cookieStore);
	}

	/*
	*	metodo que setea  una serie de cookies a el cliente antes de hacer la peticion
	* */
	public static void setSerialisableCookie(final Boolean clearCookies,final BasicClientCookie... listaCookiesSerialisables  ){
		if(listaCookiesSerialisables == null){
			return ;
		}
		if(client == null){
			client = new DefaultHttpClient();
		}
		if(clearCookies){
			client.getCookieStore().clear();
		}
		for (final BasicClientCookie cookie: listaCookiesSerialisables) {
			client.getCookieStore().addCookie(cookie);
		}
	}
}