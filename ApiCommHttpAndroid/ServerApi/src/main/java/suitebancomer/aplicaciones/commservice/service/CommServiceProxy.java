package suitebancomer.aplicaciones.commservice.service;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.content.Context;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;


/**
 * This class (a) maintains a reference that lets the proxy access the real
 * subject, (b) provides an interface identical to Subject's so that a proxy can
 * be substituted for the real subject, and (c) controls access to the real
 * subject and may be responsible for creating and deleting it.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class CommServiceProxy implements ICommService {

	private final ICommService realSubject;

	public CommServiceProxy(final Context context){
		 realSubject = new CommServiceImpl();
		 new CommContext(context);
	}

	/**
	 * metodo que solo servira como un proxy para llegar hasta 
	 * CommServiceImpl
	 * manda los parametros a CommServiceImpl y recibe de regreso un
	 * responseService
	 * 
	 * @param parameters
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws JSONException 
	 * @throws IllegalStateException 
	 */
	public IResponseService request(final ParametersTO parameters, final Class<?> objResponse) 
			throws ClientProtocolException, IOException, IllegalStateException, JSONException {
		
		if( parameters.isDevelopment() && parameters.isProduction() && parameters.isSimulation()){	
			//TODO impl //IF (estan los parametros ?? realiza petición : nada ) 
			return new ResponseServiceImpl();
		}else{
			//llamar CommServiceImpl.request
			return realSubject.request(parameters,objResponse);	
		}
	}

}