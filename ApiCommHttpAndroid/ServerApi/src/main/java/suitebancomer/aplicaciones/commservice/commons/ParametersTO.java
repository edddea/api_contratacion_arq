package suitebancomer.aplicaciones.commservice.commons;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants.isJsonValueCode;

/**
 * Clase que representa un Transfer Object para los parametros.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:23 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class ParametersTO {

	//parametros que se envian desde el delegate
	private Object parameters; 
	private int operationId;	//45 - consultaestatusmantenimiento
	private boolean isJson;
	private int provider;
	
	//parametros obtenidos en operacion
	private String operationPath;   ///eexd_mx_web/servlet/ServletOperacionWeb    /bm3wxp_mx_web/servlet/ServletOperacionWeb
	private String operationCode;   //146  //consultaEstatusMantenimiento
	private boolean isSimulation;   // Local
	private boolean isDevelopment;  // Test 
	private boolean isProduction;   // Prod
	private boolean isEmulator;     // emuladores


	private isJsonValueCode isJsonValue;
	 
	/**
	 * @return the operationId
	 */
	public final int getOperationId() {
		return operationId;
	}

	/**
	 * @param operationId the operationId to set
	 */
	public final void setOperationId(final int operationId) {
		this.operationId = operationId;
	}

	/**
	 * @return the isProduction
	 */
	public final boolean isProduction() {
		return isProduction;
	}

	/**
	 * @param isProduction the isProduction to set
	 */
	public final void setProduction(final boolean isProduction) {
		this.isProduction = isProduction;
	}
	
	/**
	 * @return the isSimulation
	 */
	public final boolean isSimulation() {
		return isSimulation;
	}

	/**
	 * @param isSimulation the isSimulation to set
	 */
	public final void setSimulation(final boolean isSimulation) {
		this.isSimulation = isSimulation;
	}
	
	/**
	 * @return the isJson
	 */
	public final boolean isJson() {
		return isJson;
	}

	/**
	 * @param isJson the isJson to set
	 */
	public final void setJson(final boolean isJson) {
		this.isJson = isJson;
	}


	/**
	 * @return the isDevelopment
	 */
	public final boolean isDevelopment() {
		return isDevelopment;
	}

	/**
	 * @param isDevelopment the isDevelopment to set
	 */
	public final void setDevelopment(final boolean isDevelopment) {
		this.isDevelopment = isDevelopment;
	}



	/**
	 * @return the isEmulator
	 */
	public final boolean isEmulator() {
		return isEmulator;
	}

	/**
	 * @param isEmulator the isEmulator to set
	 */
	public final void setEmulator(final boolean isEmulator) {
		this.isEmulator = isEmulator;
	}


	/**
	 * @return the isJsonValue
	 */
	public final isJsonValueCode getIsJsonValue() {
		return isJsonValue;
	}

	/**
	 * @param isJsonValue the isJsonValue to set
	 */
	public final void setIsJsonValue(final isJsonValueCode isJsonValue) {
		this.isJsonValue = isJsonValue;
	}
	
	/**
	 * @return the operationPath
	 */
	public final String getOperationPath() {
		return operationPath;
	}

	/**
	 * @param operationPath the operationPath to set
	 */
	public final void setOperationPath(final String operationPath) {
		this.operationPath = operationPath;
	}
	
	public Object getParameters() {
		return parameters;
	}


	public void setParameters(final Object parameters) {
		this.parameters = parameters;
	}

	public String getOperationCode() {
		return operationCode;
	}

	public void setOperationCode(final String operationCode) {
		this.operationCode = operationCode;
	}

	public int getProvider() {
		return provider;
	}

	public void setProvider(final int provider) {
		this.provider = provider;
	}

}