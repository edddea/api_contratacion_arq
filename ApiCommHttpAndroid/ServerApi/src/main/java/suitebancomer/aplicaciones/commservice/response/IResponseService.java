package suitebancomer.aplicaciones.commservice.response;
import org.apache.http.HttpResponse;


/**
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public interface IResponseService {

	void setResponse(final HttpResponse response);

	HttpResponse getResponse();

	int getStatus();

	void setStatus(final int status);

	String getMessageCode();

	void setMessageCode(final String messageCode);

	String getMessageText();

	void setMessageText(final String messageText);

	Object getObjResponse();

	void setObjResponse(final Object objResponse);

	String getUpdateURL();

	void setUpdateURL(final String updateURL);
	
	String getResponseString();

	 void setResponseString(final String responseString);

}