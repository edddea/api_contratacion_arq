package suitebancomer.aplicaciones.commservice.response;


/**
 * This class (a) declares the factory method, which returns an object of type
 * Product. Creator may also define a default implementation of the factory method
 * that returns a default ConcreteProduct object, and (b) may call the factory
 * method to create a Product object.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public interface IAdapterFactory {

	IAdapter factoryMethod(final boolean isJson);

}