package suitebancomer.aplicaciones.commservice.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;


/**
 * This class defines the common interface for RealSubject and Proxy so that a
 * Proxy can be used anywhere a RealSubject is expected.
 *  
 * @author lbermejo      
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public interface ICommService {

	/**
	 * 
	 * @param parameters
	 * @throws UnsupportedEncodingException 
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws JSONException 
	 * @throws IllegalStateException 
	 */
	IResponseService request(final ParametersTO parameters, final Class<?> objResponse) 
			throws UnsupportedEncodingException, ClientProtocolException, IOException, IllegalStateException, JSONException;

}