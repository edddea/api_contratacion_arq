package suitebancomer.aplicaciones.commservice.workflow;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

import suitebancomer.aplicaciones.commservice.request.RequestService;
import suitebancomer.aplicaciones.commservice.response.IResponseService;


/**
 * This class declares an interface common to all supported algorithms. Context
 * uses this interface to call the algorithm defined by a ConcreteStrategy.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public interface IInvokeStrategy {
	
	/**
	 * 
	 * @param aHandler
	 * @param aRequest
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	IResponseService process(final RequestService aRequest) 
			throws ClientProtocolException, IOException;

}