package suitebancomer.aplicaciones.commservice.service;

import java.io.IOException;
import java.util.Hashtable;


import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants.isJsonValueCode;
import suitebancomer.aplicaciones.commservice.request.RequestService;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.workflow.InvokeHandler;


/**
 * This class defines the real object that the proxy represents.
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:22 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class CommServiceImpl implements ICommService {

	private final InvokeHandler invokerHandle;

	public CommServiceImpl(){
		invokerHandle=new InvokeHandler() ;
	}
	
	/**
	 * este metodo recive los parametros que llegan desde el proxy,
	 * le pasa los parametros al IInvokeHandle
	 * Realiza el llamado al metodo de handleRequest enviando un request  vacio
	 * 
	 * 
	 * @param parameters
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws JSONException 
	 * @throws IllegalStateException 
	 */
	public IResponseService request(final ParametersTO parameters, final Class<?> objResponse) 
			throws ClientProtocolException, IOException, IllegalStateException, JSONException{
		
		//se le setean los parametros en IInvokeHandle
		//JAIG 
		if(parameters.getIsJsonValue() ==null){
			parameters.setIsJsonValue(isJsonValueCode.NONE);
		}else{
			parameters.setIsJsonValue(parameters.getIsJsonValue());
		}
		
		invokerHandle.setParametesTO(parameters);
		final Hashtable<String,String> params2= (Hashtable<String, String>) parameters.getParameters();
		//se llama la clase invokerHandle.handleRequest
		if(params2.get(ApiConstants.IMAGEN_BMOVIL)==null){
			return invokerHandle.handleRequest(new RequestService(),objResponse);
		}else{
			return invokerHandle.handleRequestImage(new RequestService(),objResponse);

		}


	}

}