/**
 * 
 */
package suitebancomer.aplicaciones.commservice.request;


/**
 * @author lbermejo
 *
 * IDS Comercial S.A. de C.V
 */
public final class NullRequest {

	/**
     * Returns Singleton instance of <code>NullRequest</code>.
     * 
     * @return Singleton instance of <code>NullRequest</code>.
     */
    public static NullRequest
    getInstance()
    {
        return INSTANCE;
    }    
    
    private NullRequest()
    {       
        // do nothing
    }
    
    /**
     * Singleton instance of <code>NullRequest</code>
     */
    private static final NullRequest INSTANCE = new NullRequest();

}
