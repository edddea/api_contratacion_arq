package suitebancomer.aplicaciones.commservice.response;

/**
 * This class overrides the factory method to return an instance of a
 * Adapter.
 * 
 * TODO Singleton
 * 
 * @author lbermejo
 * @version 1.0
 * @created 02-jun-2015 12:37:21 p.m.
 * 
 * IDS Comercial S.A. de C.V
 */
public class AdapterFactory implements IAdapterFactory {
	
	/**
	 * si el resultado es json 
	 * se retorna un JsonAdapter en caso contrario
	 * se retorna un Adapter
	 * */
	public IAdapter factoryMethod(final boolean isJson){
		IAdapter iadapteer;
		if(isJson){
			iadapteer= new JsonAdapter();
		}else{
			iadapteer=new Adapter();
		}
		return iadapteer;
	}

}
