/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */

package suitebancomer.aplicaciones.commservice.httpcomm;

import java.io.Serializable;

/**
 *
 * @author Stefanini IT Solutions.
 *
 * NameValuePair holds the information of a name and value pair of fields
 *
 */

public class NameValuePair implements Serializable {

    static final long serialVersionUID = 1L;
	 /**
     * The name.
     */
    private final String name;

    /**
     * The value.
     */
    private final String value;

    /**
     * Default constructor.
     * @param nam the name
     * @param val the value
     */
    public NameValuePair(final String nam, final String val) {
        this.name = emptyIfNull(nam);
        this.value = emptyIfNull(val);
    }

    /**
     * Gets the name.
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the value.
     * @return the value
     */
    public String getValue() {
        return value;
    }
    
    /**
     * Gets the value if not null or an empty string.
     * @param value the input value
     * @return the output value
     */
    private static String emptyIfNull(final String value) {
        String val=value;
        if(value == null){
            val = "";
        }
        return val;
    }
}
