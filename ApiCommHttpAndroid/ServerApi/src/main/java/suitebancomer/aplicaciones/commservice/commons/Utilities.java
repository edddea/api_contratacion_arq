package suitebancomer.aplicaciones.commservice.commons;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.ParseException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;

import android.util.Log;

public  class Utilities {


	  /**
	   * Print http headers. Useful for debugging.
	   * 
	   * @param headers
	   */
	  public static void getHeadersAsString(final HttpPost req) {
		  
		  try {
			  final ByteArrayOutputStream out = new ByteArrayOutputStream();
			  req.getEntity().writeTo(out);
			  if (CommContext.allowLog){
					Log.e("APIComm:Utilities entry", out.toString() ); 
				}
			  out.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		   StringBuilder s;
		  s= new StringBuilder();
		  s.append("Method: ").append(req.getMethod());
		  try {
			s.append("Entity: ").append(EntityUtils.toString(req.getEntity()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  

		  
		final org.apache.http.Header[] headers =req.getAllHeaders();
		  s.append("Headers: ").append("------------");
	    for (final org.apache.http.Header h : headers)
	      s.append(h.toString());
	    s.append("------------");
		if (CommContext.allowLog){
			Log.d("APIComm:Utilities", s.toString() ); 
		}
	  }

	}
