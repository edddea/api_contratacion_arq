package com.bancomer.mbanking.hamburguesa;

/**
 * @author eluna
 * @version 1.0
 * IDS Comercial S.A. de C.V
 */

import java.io.Serializable;
import java.util.List;

public class MenuHamburguesaData implements Serializable
{
	private static final long serialVersionUID = 1L;
	private String estado;
	private String nombreApp;
	private List<OpcionesMenuHamburguesa> opciones;
	private String versionMenuHam;
	private String codigoMensaje;
	private String descripcionMensaje;
	
	
	public String getEstado() {
		return estado;
	}
	
	public void setEstado(final String estado) {
		this.estado = estado;
	}	
	
	public String getCodigoMensaje() {
		return codigoMensaje;
	}

	public void setCodigoMensaje(final String codigoMensaje) {
		this.codigoMensaje = codigoMensaje;
	}

	public String getDescripcionMensaje() {
		return descripcionMensaje;
	}

	public void setDescripcionMensaje(final String descripcionMensaje) {
		this.descripcionMensaje = descripcionMensaje;
	}
	
	public String getVersionMenuHam() {
		return versionMenuHam;
	}
	public void setVersionMenuHam(final String versionMenuHam) {
		this.versionMenuHam = versionMenuHam;
	}

	public String getNombreApp() {
		return nombreApp;
	}

	public void setNombreApp(final String nombreApp) {
		this.nombreApp = nombreApp;
	}

	public List<OpcionesMenuHamburguesa> getOpciones() {
		return opciones;
	}

	public void setOpciones(final List<OpcionesMenuHamburguesa> opciones) {
		this.opciones = opciones;
	}

}
