package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;

/**
 * Created by amezquitillo on 12/08/2015.
 */
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.app.Activity;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import com.bancomer.mbanking.hamburguesa.OpcionesMenuHamburguesa;
import com.bancomer.mbanking.hamburguesa.R;
import suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa.ConstanstMenuOpcionesHamburguesa;

public class AdapterOpcionesMenu extends BaseAdapter {
    protected Activity activity;
    protected List<OpcionesMenuHamburguesa> items;
    private static final Integer[] IMAGE = {R.drawable.ic_novedades,R.drawable.an_ic_administrar,
            R.drawable.an_ic_help,R.drawable.an_ic_compartir,
            R.drawable.an_ic_conalertas,R.drawable.an_ic_masapp,
            R.drawable.an_ic_valorar,R.drawable.an_ic_noti,
            R.drawable.an_ic_contactanos,R.drawable.ic_novedades, R.drawable.ic_salir};

    public AdapterOpcionesMenu(final Activity activity,final List<OpcionesMenuHamburguesa> items){
        this.activity = activity;
        this.items= items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(final int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return items.get(position).getId();
    }

    @Override
    public View getView(final int position,final View convertView,final ViewGroup parent) {
        View vis = convertView;
        if(convertView == null){
            final LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vis= inflater.inflate(R.layout.laoyout_items ,null);
        }
        final OpcionesMenuHamburguesa item = items.get(position);

        final TextView opcion = (TextView) vis.findViewById(R.id.txt_opcion);
        opcion.setText(item.getNombreOpcion());
        opcion.setTextColor(Color.BLACK);
        opcion.setTextSize(16);

        final ImageView imagen = (ImageView) vis.findViewById(R.id.img_opcion);

        switch (item.getNombreOpcion())
        {
            case ConstanstMenuOpcionesHamburguesa.MENU_ADMINISTRAR:
                imagen.setImageResource(IMAGE[1]);
                break;
            case ConstanstMenuOpcionesHamburguesa.ADMINISTRAR_ALERTAS:
                imagen.setImageResource(IMAGE[4]);
                break;
            case ConstanstMenuOpcionesHamburguesa.MAS_APLICACIONES:
                imagen.setImageResource(IMAGE[5]);
                break;
            case ConstanstMenuOpcionesHamburguesa.NOTIFICACIONES:
                imagen.setImageResource(IMAGE[7]);
                break;
            case ConstanstMenuOpcionesHamburguesa.AYUDA:
                imagen.setImageResource(IMAGE[2]);
                break;
            case ConstanstMenuOpcionesHamburguesa.COMPARTIR:
                imagen.setImageResource(IMAGE[3]);
                break;
            case ConstanstMenuOpcionesHamburguesa.VALORAR:
                imagen.setImageResource(IMAGE[6]);
                break;
            case ConstanstMenuOpcionesHamburguesa.CONTACTANOS:
                imagen.setImageResource(IMAGE[8]);
                break;
            case ConstanstMenuOpcionesHamburguesa.SALIR:
                imagen.setImageResource(IMAGE[10]);
                break;
            default:
                imagen.setImageResource(IMAGE[9]);
                break;
        }
        if (item.getNombreApi().equals(ConstanstMenuOpcionesHamburguesa.ACERCA_DE)){
            imagen.setImageResource(IMAGE[0]);
        }
        return vis;
    }
}