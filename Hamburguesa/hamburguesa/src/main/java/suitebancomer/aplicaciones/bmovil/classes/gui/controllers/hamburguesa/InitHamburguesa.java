package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;

import com.bancomer.base.callback.CallBackSession;

import bancomer.api.common.callback.CallBackModule;
import suitebancomer.aplicaciones.bmovil.classes.model.hamburguesa.BanderasServer;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by eluna on 15/08/2015.
 */
public class InitHamburguesa {

    private static InitHamburguesa ourInstance = new InitHamburguesa();

    private CallBackSession callBackSession;
    private CallBackModule callBackModule;

    private InitHamburguesa() {
    }

    public InitHamburguesa(final BanderasServer banderasServer) {
        ServerCommons.ALLOW_LOG = banderasServer.getLogActivo();
        ServerCommons.DEVELOPMENT = banderasServer.isDesarrollo();
        ServerCommons.EMULATOR = banderasServer.getEmulador();
        ServerCommons.SIMULATION = banderasServer.getSimulacion();
    }

    public static InitHamburguesa getInstance() {
        return ourInstance;
    }

    public CallBackSession getCallBackSession() {
        return callBackSession;
    }

    public void setCallBackSession(final CallBackSession callBackSession) {
        this.callBackSession = callBackSession;
    }


    public CallBackModule getCallBackModule() {
        return callBackModule;
    }

    public void setCallBackModule(final CallBackModule callBackModule) {
        this.callBackModule = callBackModule;
    }

}
