package suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa;

public class ConstanstMenuOpcionesHamburguesa 
{
	/**Operations identifiers**/
	

	/**
	 * Constante que indica el campo username en Operación en curso
	 */
	public static  final String OPERACION_EN_CURSO = "Operación en curso";
	/**
	 * Constante que indica el campo username en Conectando con el servidor remoto
	 */
	public static final String CONECTANDO_CON_EL_SERVER = "Conectando con el servidor remoto";
	/**
	 * Constante que indica el campo username en nombreApp
	 */
	public static  final String NAME_NOMBRE_APP = "nombreApp";
	/**
	 * Constante que indica el campo username en bmovil
	 */
	public static final String VALUE_NOMBRE_APP = "bmovil";
	/**
	 * Constante que indica el campo username en banderaSession
	 */
	public static final String BAND_NAME_SESSION = "banderaSession";

	public static final String SERVICIO_NO_DISPONIBLE = "Servicio no disponible";
	/**
	 * Operation menu SIN_SESSION
	 */
	public static final String SIN_SESSION = "0";
	
	/**
	 * Operation menu CON_SESSION
	 */
	public static final String CON_SESSION = "1";
	
	/**
	 * Operation menu nombreApp
	 */
	public static final String NOMBRE_APP = "nombreApp";
	
	/**
	 * Operation menu versionMenuHam
	 */
	public static final String VERSION_MENU_HAM = "versionMenuHam";
	
	/**
	 * Operation menu banderaSession
	 */
	public static final String BANDERA_SESSION = "banderaSession";
	
	/**
	 * Operation menu opciones
	 */
	public static final String OPCIONES = "opciones";
	
	/**
	 * Operation menu estado
	 */
	public static final String ESTADO = "estado";
	
	/**
	 * Operation menu administrar
	 */
	public static final String MENU_ADMINISTRAR = "Administrar";
	
	/**
	 * Operation administrar alertas
	 */
	public static final String ADMINISTRAR_ALERTAS = "Administrar Alertas";
	
	/**
	 * Operation mas aplicaciones
	 */
	public static final String MAS_APLICACIONES = "Más aplicaciones";

	/**
	 * Operation mas aplicaciones
	 */
	public static final String CONTACTANOS = "Contáctanos";

	/**
	 * Operation mas NOTIFICACIONES
	 */
	public static final String NOTIFICACIONES = "Notificaciones";
	
	/**
	 * Operation ayuda
	 */
	public static final String AYUDA = "Ayuda";
	
	/**
	 * Operation compartir
	 */
	public static final String COMPARTIR = "Compartir";
	
	/**
	 * Operation valorar
	 */
	public static final String VALORAR =  "Valorar";
	
	/**
	 * Operation acerca de...
	 */
	public static final String ACERCA_DE = "AcercaDe";
	/**
	 * Operation acerca de...
	 */
	public static final String NOVEDADES = "Novedades";
	/**
	 * Operation acerca de...
	 */
	public static final String MEJORAR_APLICACION = "Estamos trabajando para mejorar tu aplicación";

	/**
	 * Operation BMOVIL
	 */
	public static final String BMOVIL = "Bancomer Móvil";

	/**
	 * Operation WALLET
	 */
	public static final String WALLET = "BBVA Wallet";

	/**
	 * Operation VIDABANCOMER
	 */
	public static final String VIDABANCOMER =  "Vida Bancomer";

	/**
	 * Operation SEND
	 */
	public static final String SEND = "BBVA Send";

	/**
	 * Operation CAMARA REQUEST
	 */
	public static final int CAMERA_REQUEST_CIRCLE  = 1888;

	/**
	 * Operation CAMARA REQUEST
	 */
	public static final int CAMERA_REQUEST_REC  = 1886;

	/**
	 * Operation SELECT PICTURE
	 */
	public static final int SELECT_PICTURE = 1;

	/**
	 * Operation SELECT_PICTURE FONDO
	 */
	public static final int SELECT_PICTURE_FONDO = 2;

	/**
	 * Indica el valor  fondoPerfilHamburguesa
	 */
	public static final String FONDO_PERFIL_HAMBURGUESA = "fondoPerfilHamburguesa";

	/**
	 * Indica el valor banderaGaleriaF
	 */
	public static final String BANDERA_GALERIAF = "banderaGaleriaF";

	/**
	 * Indica el valor perfilImageURI
	 */
	public static final String PERFIL_IMAGE_URI = "perfilImageURI";

	/**
	 * Operation mas Salir
	 */
	public static final String SALIR = "Salir";


	/**
	 * Indica el valor banderaGaleria
	 */
	public static final String BANDERA_GALERIA = "banderaGaleria";
	/**
	 * Operation SELECT_PICTURE FONDO
	 */
	public static final int CROP = 3;
	/**
	 * Operation SELECT_PICTURE FONDO
	 */
	public static final int CROP_REC = 4;


}
