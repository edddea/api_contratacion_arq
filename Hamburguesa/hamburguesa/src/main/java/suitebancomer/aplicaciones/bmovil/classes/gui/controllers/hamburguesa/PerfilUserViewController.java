package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa.ConstanstMenuOpcionesHamburguesa;
import suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa.Item;
import suitebancomer.aplicaciones.bmovil.classes.delegates.hamburguesa.SelectPhotoPerfilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.crop.CropImageIntentBuilder;
/**
 * Created by francisco on 17/03/16.
 */
public class PerfilUserViewController extends PerfilUserViewControllerBase implements View.OnClickListener
{

    private void alertGalerryCamera(){
        final Item[] items = {
                new Item("Cámara", android.R.drawable.ic_menu_camera),
                new Item("Galería", android.R.drawable.ic_menu_gallery),
                new Item("Cancelar", 0),
        };
        final ArrayAdapter adapter = new ArrayAdapter<Item>(
                this,
                android.R.layout.select_dialog_item,
                android.R.id.text1,
                items) {
            public View getView(final int position,final View convertView,final ViewGroup parent) {
                final View v = super.getView(position, convertView, parent);
                final TextView tv = (TextView) v.findViewById(android.R.id.text1);
                tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);
                final int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                tv.setCompoundDrawablePadding(dp5);

                return v;
            }
        };
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Selecciona una opción");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int item) {
                        if (item == 0) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File f = new File(android.os.Environment
                                    .getExternalStorageDirectory(), "temp.jpg");
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                            Uri selectedImage = Uri.fromFile(f);
                            editor.putString("perfilImageURIT", String.valueOf(selectedImage));
                            editor.commit();
                            startActivityForResult(intent, ConstanstMenuOpcionesHamburguesa.CAMERA_REQUEST_CIRCLE);
                        } else if (item == 1) {
                            MenuHamburguesaViewsControllers.setIsOterApp(Boolean.TRUE);
                            final Intent intent = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/*");
                            startActivityForResult(
                                    Intent.createChooser(intent, "Select File"),
                                    ConstanstMenuOpcionesHamburguesa.SELECT_PICTURE);
                        } else if (item == 2) {
                            dialog.dismiss();
                        }
                    }
                }
        );
        builder.show();
    }

    private void alertGalerryCameraFondo(){
        final Item[] items = {
                new Item("Cámara", android.R.drawable.ic_menu_camera),
                new Item("Galería", android.R.drawable.ic_menu_gallery),
                new Item("Predeterminadas", android.R.drawable.ic_input_get),
                new Item("Cancelar", 0),
        };

        final ArrayAdapter adapter = new ArrayAdapter<Item>(
                this,
                android.R.layout.select_dialog_item,
                android.R.id.text1,
                items) {
            public View getView(final int position,final View convertView,final ViewGroup parent) {
                final View v = super.getView(position, convertView, parent);
                final TextView tv = (TextView) v.findViewById(android.R.id.text1);
                tv.setCompoundDrawablesWithIntrinsicBounds(items[position].icon, 0, 0, 0);
                int dp5 = (int) (5 * getResources().getDisplayMetrics().density + 0.5f);
                tv.setCompoundDrawablePadding(dp5);

                return v;
            }
        };

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Selecciona una opción");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,final int item) {
                        if (item == 0) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            File f = new File(android.os.Environment
                                    .getExternalStorageDirectory(), "tempFondo.jpg");
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                            Uri selectedImage = Uri.fromFile(f);
                            editor.putString("fondoPerfilHamburguesaT", String.valueOf(selectedImage));
                            editor.putString(ConstanstMenuOpcionesHamburguesa.BANDERA_GALERIAF, "galeria");
                            editor.commit();
                            startActivityForResult(intent, ConstanstMenuOpcionesHamburguesa.CAMERA_REQUEST_REC);
                        } else if (item == 1) {
                            MenuHamburguesaViewsControllers.setIsOterApp(true);
                            final Intent intent = new Intent(
                                    Intent.ACTION_PICK,
                                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/*");
                            editor.putString(ConstanstMenuOpcionesHamburguesa.BANDERA_GALERIAF, "galeria");
                            editor.commit();
                            startActivityForResult(
                                    Intent.createChooser(intent, "Select File"),
                                    ConstanstMenuOpcionesHamburguesa.SELECT_PICTURE_FONDO);
                        } else if (item == 2) {
                            final Intent perfilFondoHamburguesa = new Intent(PerfilUserViewController.this, SeleccionaFondoPerfilViewController.class);
                            PerfilUserViewController.this.startActivity(perfilFondoHamburguesa);
                        }
                        else if (item == 3 ){
                            dialog.dismiss();
                        }
                    }
                }
        );
        builder.show();
    }

    protected void onActivityResult(final int requestCode,final int resultCode,final Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ConstanstMenuOpcionesHamburguesa.SELECT_PICTURE && resultCode == RESULT_OK) {
            if (data != null) {
                final File f = new File(android.os.Environment.getExternalStorageDirectory(), "temporalPerfil.jpg");
                final File fileOrigen = new File(getRealPathFromURI(data.getData()));
                final String ext = android.webkit.MimeTypeMap.getFileExtensionFromUrl(fileOrigen.getName());
                final File fileDestino = new File(android.os.Environment.getExternalStorageDirectory(), "GaleriaRecorteTemp." + ext);
                try {
                    copy(fileOrigen,fileDestino);
                } catch (IOException e) {
                    Log.e("","No se pudo realizar la copia de archivo de imagen en galeria");
                }
                editor.putString("perfilImageURIT", String.valueOf(Uri.parse(fileDestino.getAbsolutePath())));
                editor.commit();
                CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, Uri.parse(f.getAbsolutePath()));
                cropImage.setOutlineColor(0xFF03A9F4);
                cropImage.setSourceImage(Uri.parse(fileDestino.getAbsolutePath()));
                cropImage.setCircleCrop(true);
                startActivityForResult(cropImage.getIntent(this), ConstanstMenuOpcionesHamburguesa.CROP);
            }
        }
        if (requestCode == ConstanstMenuOpcionesHamburguesa.SELECT_PICTURE_FONDO && resultCode == RESULT_OK) {
            if (data != null)
            {
                final File f = new File(android.os.Environment.getExternalStorageDirectory(), "temporalFondo.jpg");
                final File fileOrigen = new File(getRealPathFromURI(data.getData()));
                final String ext = android.webkit.MimeTypeMap.getFileExtensionFromUrl(fileOrigen.getName());
                final File fileDestino = new File(android.os.Environment.getExternalStorageDirectory(), "GaleriaRecorteFondoTemp." + ext);
                try {
                    copy(fileOrigen,fileDestino);
                } catch (IOException e) {
                    Log.e("","No se pudo realizar la copia de archivo de imagen en galeria");
                }
                editor.putString("fondoPerfilHamburguesaT", String.valueOf(Uri.parse(fileDestino.getAbsolutePath())));
                editor.putString(ConstanstMenuOpcionesHamburguesa.BANDERA_GALERIAF, "galeria");
                editor.commit();
                CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, Uri.parse(f.getAbsolutePath()));
                cropImage.setOutlineColor(0xFF03A9F4);
                cropImage.setSourceImage(Uri.parse(fileDestino.getAbsolutePath()));
                startActivityForResult(cropImage.getIntent(this), ConstanstMenuOpcionesHamburguesa.CROP_REC);
            }
        }
        if (requestCode == ConstanstMenuOpcionesHamburguesa.CAMERA_REQUEST_CIRCLE && resultCode == RESULT_OK)
        {
            Uri imageCamara = Uri.parse(pref.getString("perfilImageURIT", null));
            final File f = new File(android.os.Environment.getExternalStorageDirectory(), "temporalPerfil.jpg");
            CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, Uri.parse(f.getAbsolutePath()));
            cropImage.setOutlineColor(0xFF03A9F4);
            cropImage.setSourceImage(imageCamara);
            cropImage.setCircleCrop(true);
            cropImage.setSorceUri(imageCamara);
            startActivityForResult(cropImage.getIntent(this), ConstanstMenuOpcionesHamburguesa.CROP);
        }
        if (requestCode == ConstanstMenuOpcionesHamburguesa.CAMERA_REQUEST_REC && resultCode == RESULT_OK)
        {
           Uri croppedImage = Uri.parse(pref.getString("fondoPerfilHamburguesaT", null));
           File f = new File(android.os.Environment.getExternalStorageDirectory(), "temporalFondo.jpg");
           CropImageIntentBuilder cropImage = new CropImageIntentBuilder(200, 200, Uri.parse(f.getAbsolutePath()));
           cropImage.setOutlineColor(0xFF03A9F4);
           cropImage.setSourceImage(croppedImage);
           cropImage.setCircleCrop(false);
           startActivityForResult(cropImage.getIntent(this), ConstanstMenuOpcionesHamburguesa.CROP_REC);
        }
        if ((requestCode == ConstanstMenuOpcionesHamburguesa.CROP) && (resultCode == RESULT_OK)) {
            if (data != null) {
                Uri selectedImage = Uri.parse(data.getAction());
                editor.putString(ConstanstMenuOpcionesHamburguesa.PERFIL_IMAGE_URI, String.valueOf(selectedImage));
                editor.commit();
            }
        }
        if ((requestCode == ConstanstMenuOpcionesHamburguesa.CROP_REC) && (resultCode == RESULT_OK)) {
            if (data != null) {
                Uri selectedImage = Uri.parse(data.getAction());
                editor.putString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA, String.valueOf(selectedImage));
                editor.commit();
            }
        }
    }
    public static ImageView getBtnCambiarImagePerfil() {
        return btnCambiarImagePerfil;
    }

    public static ImageView getLinearFondoHamburguesa() {
        return linearFondoHamburguesa;
    }

    @Override
    public void onClick(final View v) {

        if (v.equals(btnCambiarFondo)) {
            alertGalerryCameraFondo();
        } else if (v.equals(btnCambiarImagePerfil)) {
            if (null == pref.getString(ConstanstMenuOpcionesHamburguesa.PERFIL_IMAGE_URI, null)) {
                alertGalerryCamera();
            }
            else {
                final CharSequence[] items = {"Tu Foto", "Eliminar", "Cancelar"};
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Configura tu Perfil");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int item) {
                        if (item == 0) {
                            alertGalerryCamera();
                        } else if (item == 1) {

                            final String imagenActual = pref.getString(ConstanstMenuOpcionesHamburguesa.PERFIL_IMAGE_URI, null);
                            pref.edit().remove(ConstanstMenuOpcionesHamburguesa.PERFIL_IMAGE_URI).commit();
                            final SelectPhotoPerfilDelegate delegate = new SelectPhotoPerfilDelegate();
                            delegate.resetearImagen(2);
                            //elimina la imagen de disco
                            if (imagenActual != null) {
                                final File f2 = new File(imagenActual.replace("file:",""));
                                if (f2.exists()) {
                                    f2.delete();
                                }
                            }
                            dialog.dismiss();
                        } else if (item == 2) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }
        }
    }

    public void initValues()
    {
        super.initValues();
        btnCambiarImagePerfil.setOnClickListener(this);
        btnCambiarFondo.setOnClickListener(this);
    }

    public void copy(File src, File dst) throws IOException
    {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }

    private String getRealPathFromURI(Uri contentUri) {

        String[] proj = { MediaStore.Video.Media.DATA };
        Cursor cursor = getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
}
