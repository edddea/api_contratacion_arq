package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.crop;

import android.app.Activity;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.InitHamburguesa;

public class NoSearchActivity extends Activity {
    @Override
    public boolean onSearchRequested() {
        return false;
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        if(InitHamburguesa.getInstance().getCallBackSession() != null) {
            InitHamburguesa.getInstance().getCallBackSession().userInteraction();
        }
    }
}
