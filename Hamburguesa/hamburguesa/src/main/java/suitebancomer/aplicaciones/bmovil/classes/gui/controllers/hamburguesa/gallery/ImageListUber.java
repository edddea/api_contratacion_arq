package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.gallery;

import android.net.Uri;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.crop.ImageManager;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.crop.Util;

public class ImageListUber implements IImageList {
    @SuppressWarnings("unused")
    private static final String TAG = "ImageListUber";

    private final IImageList [] mSubList;
    private final PriorityQueue<MergeSlot> mQueue;

    private long[] mSkipList;
    private int mSkipListSize;
    private int [] mSkipCounts;
    private int mLastListIndex;

    public ImageListUber(IImageList [] sublist, int sort) {
        mSubList = sublist.clone();
        mQueue = new PriorityQueue<MergeSlot>(4, sort == ImageManager.SORT_ASCENDING ? new AscendingComparator() : new DescendingComparator());
        mSkipList = new long[16];
        mSkipListSize = 0;
        mSkipCounts = new int[mSubList.length];
        mLastListIndex = -1;
        mQueue.clear();
        for (int i = 0; i < mSubList.length; ++i) {
            final IImageList list = mSubList[i];
            final MergeSlot slot = new MergeSlot(list, i);
            if (slot.next()) mQueue.add(slot);
        }
    }

    public HashMap<String, String> getBucketIds() {
        final HashMap<String, String> hashMap = new HashMap<String, String>();
        for (final IImageList list : mSubList) {
            hashMap.putAll(list.getBucketIds());
        }
        return hashMap;
    }

    public int getCount() {
        int count = 0;
        for (final IImageList subList : mSubList) {
            count += subList.getCount();
        }
        return count;
    }

    public boolean isEmpty() {
        for (final IImageList subList : mSubList) {
            if (!subList.isEmpty()) return false;
        }
        return true;
    }

    public IImage getImageAt(int index) {
        if (index < 0 || index > getCount()) {
            throw new IndexOutOfBoundsException(
                    "index " + index + " out of range max is " + getCount());
        }

        final int skipCounts[] = mSkipCounts;
        Arrays.fill(skipCounts, 0);
        int skipCount = 0;

        for (int i = 0; i < mSkipListSize; ++i) {
            final long v = mSkipList[i];

            final int offset = (int) (v & 0xFFFFFFFF);
            final int which  = (int) (v >> 32);
            if (skipCount + offset > index) {
                final int subindex = mSkipCounts[which] + (index - skipCount);
                return mSubList[which].getImageAt(subindex);
            }
            skipCount += offset;
            mSkipCounts[which] += offset;
        }

        for (; true; ++skipCount) {
            final MergeSlot slot = nextMergeSlot();
            if (slot == null) return null;
            if (skipCount == index) {
                final IImage result = slot.mImage;
                if (slot.next()) mQueue.add(slot);
                return result;
            }
            if (slot.next()) mQueue.add(slot);
        }
    }

    private MergeSlot nextMergeSlot() {
       final MergeSlot slot = mQueue.poll();
        if (slot == null) return null;
        if (slot.mListIndex == mLastListIndex) {
            final int lastIndex = mSkipListSize - 1;
            ++mSkipList[lastIndex];
        } else {
            mLastListIndex = slot.mListIndex;
            if (mSkipList.length == mSkipListSize) {
                final long [] temp = new long[mSkipListSize * 2];
                System.arraycopy(mSkipList, 0, temp, 0, mSkipListSize);
                mSkipList = temp;
            }
            mSkipList[mSkipListSize++] = (((long) mLastListIndex) << 32) | 1;
        }
        return slot;
    }

    public IImage getImageForUri(final Uri uri) {
        for (final IImageList sublist : mSubList) {
           final IImage image = sublist.getImageForUri(uri);
            if (image != null) return image;
        }
        return null;
    }

    private void modifySkipCountForDeletedImage(final int index) {
        int skipCount = 0;
        for (int i = 0; i < mSkipListSize; i++) {
            final long v = mSkipList[i];
            final int offset = (int) (v & 0xFFFFFFFF);
            if (skipCount + offset > index) {
                mSkipList[i] = v - 1;
                break;
            }
            skipCount += offset;
        }
    }

    private boolean removeImage(final IImage image,final int index) {
        final IImageList list = image.getContainer();
        if (list != null && list.removeImage(image)) {
            modifySkipCountForDeletedImage(index);
            return true;
        }
        return false;
    }

    public boolean removeImage(IImage image) {
        return removeImage(image, getImageIndex(image));
    }

    public boolean removeImageAt(final int index) {
        final IImage image = getImageAt(index);
        if (image == null) return false;
        return removeImage(image, index);
    }

    public synchronized int getImageIndex(IImage image) {
        final IImageList list = image.getContainer();
        final int listIndex = Util.indexOf(mSubList, list);
        if (listIndex == -1) {
            throw new IllegalArgumentException();
        }
        int listOffset = list.getImageIndex(image);

        // Similar algorithm as getImageAt(int index)
        int skipCount = 0;
        for (int i = 0, n = mSkipListSize; i < n; ++i) {
            final long value = mSkipList[i];
            final int offset = (int) (value & 0xFFFFFFFF);
            final int which  = (int) (value >> 32);
            if (which == listIndex) {
                if (listOffset < offset) {
                    return skipCount + listOffset;
                }
                listOffset -= offset;
            }
            skipCount += offset;
        }

        for (; true; ++skipCount) {
            final MergeSlot slot = nextMergeSlot();
            if (slot == null) return -1;
            if (slot.mImage == image) {
                if (slot.next()) mQueue.add(slot);
                return skipCount;
            }
            if (slot.next()) mQueue.add(slot);
        }
    }

    private static class DescendingComparator implements Comparator<MergeSlot> {

        public int compare(MergeSlot m1, MergeSlot m2) {
            if (m1.mDateTaken != m2.mDateTaken) {
                return m1.mDateTaken < m2.mDateTaken ? 1 : -1;
            }
            return m1.mListIndex - m2.mListIndex;
        }
    }

    private static class AscendingComparator implements Comparator<MergeSlot> {

        public int compare(MergeSlot m1, MergeSlot m2) {
            if (m1.mDateTaken != m2.mDateTaken) {
                return m1.mDateTaken < m2.mDateTaken ? -1 : 1;
            }
            return m1.mListIndex - m2.mListIndex;
        }
    }

    private static class MergeSlot {
        private int mOffset = -1;
        private final IImageList mList;

        int mListIndex;
        long mDateTaken;
        IImage mImage;

        public MergeSlot(IImageList list, int index) {
            mList = list;
            mListIndex = index;
        }

        public boolean next() {
            if (mOffset >= mList.getCount() - 1) return false;
            mImage = mList.getImageAt(++mOffset);
            mDateTaken = mImage.getDateTaken();
            return true;
        }
    }

    public void close() {
        for (int i = 0; i < mSubList.length; ++i) {
            mSubList[i].close();
        }
    }
}
