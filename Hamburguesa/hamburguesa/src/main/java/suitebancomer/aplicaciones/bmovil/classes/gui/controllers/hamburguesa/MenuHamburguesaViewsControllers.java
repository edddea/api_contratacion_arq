package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;

import java.io.IOException;
import java.util.*;
import com.bancomer.mbanking.contactanosapi.suitebancomer.aplicaciones.bmovil.classes.gui.InitContactanos;
import com.bancomer.mbanking.hamburguesa.*;
import android.content.*;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.*;
import suitebancomer.aplicaciones.bmovil.classes.delegates.hamburguesa.SelectPhotoPerfilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.otrasapp.*;
import suitebancomercoms.aplicaciones.bmovil.classes.common.*;
import suitebancomer.aplicaciones.commservice.response.*;
import suitebancomer.aplicaciones.commservice.service.*;
import com.bancomer.base.SuiteApp;
import android.app.Activity;
import android.util.Log;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.administracion.MenuAdministrarViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa.ConstanstMenuOpcionesHamburguesa;
import suitebancomer.aplicaciones.bmovil.classes.model.otrasapp.BanderasServer;
import org.json.JSONException;

public final class MenuHamburguesaViewsControllers extends MenuHamburguesaBase
{


	static boolean contactanos = false;
	private static String appOrigen;
	private static MenuHamburguesaData responseObject;
	private static String responseString;

	private static ListView listaOpciones;
	private static List<OpcionesMenuHamburguesa> elementosMenu = null;

	final static SelectPhotoPerfilDelegate DELEGATE = new SelectPhotoPerfilDelegate();

	public MenuHamburguesaViewsControllers() {
	}

	public static ListView getListaOpciones() {
		return listaOpciones;
	}
	public static List<OpcionesMenuHamburguesa> getElementosMenu() {
		return elementosMenu;
	}

	public static void iniciaHamburguesa(final Activity context,final String bandera,final String app)
	{
		String fondo;
		setBanderaSession(bandera);
		appOrigen= app;
		listaOpciones = (ListView) context.findViewById(R.id.list_opciones_menu);
		pref = context.getSharedPreferences("MyPref", Activity.MODE_PRIVATE);
		editor = pref.edit();
		elementosMenu = new ArrayList<OpcionesMenuHamburguesa>();
		listaOpciones.setDivider(null);
		listaOpciones.setSelector(android.R.color.transparent);
		buildingHamburguesa(context);
		linearFondoHamburguesa = (ImageView) context.findViewById(R.id.layout_fondo_perfil_hamburguesa);
		imagenPerfil = (ImageView) context.findViewById(R.id.profile_image);
		usuarioPerfil = (TextView) context.findViewById(R.id.usuarioPerfil);

		if (ConstanstMenuOpcionesHamburguesa.SIN_SESSION.equals(getBanderaSession()))
		{
			imagenPerfil.setVisibility(View.GONE);
			usuarioPerfil.setVisibility(View.GONE);
			linearFondoHamburguesa.setBackgroundResource(R.drawable.an_ic_fondo_perfil);
		}else
		{
			imagenPerfil.setVisibility(View.VISIBLE);
			usuarioPerfil.setVisibility(View.VISIBLE);

			final Session session = Session.getInstance(context);
			usuarioPerfil.setText(session.getNombreCliente());

			if (null != pref.getString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA,null)){
				if (null != pref.getString(ConstanstMenuOpcionesHamburguesa.BANDERA_GALERIAF, null) && pref.getString(ConstanstMenuOpcionesHamburguesa.BANDERA_GALERIAF,null).equals("predeterminadas")){
					linearFondoHamburguesa.setBackgroundResource(Integer.parseInt(pref.getString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA, null)));
				}else{
					DELEGATE.buscarImagenFondo(Boolean.FALSE, Boolean.TRUE, Uri.parse(pref.getString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA, null)), context);
				}
			}

			if (DetalleInformacionHamburguesaViewController.isDifNull(pref.getString(ConstanstMenuOpcionesHamburguesa.PERFIL_IMAGE_URI, null))){
				DELEGATE.buscarImagenPerfil(Boolean.FALSE,Boolean.TRUE, Uri.parse(pref.getString(ConstanstMenuOpcionesHamburguesa.PERFIL_IMAGE_URI, null)));
			}else {
				DELEGATE.resetearImagen(1);
			}
			imagenPerfil.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(final View v) {
					setIsOterApp(true);
					final Intent perfilHamburguesa = new Intent(context, PerfilUserViewController.class);
					context.startActivity(perfilHamburguesa);
				}
			});
		}
	}

	public static void clickOpcionesHamburguesa(final String optionSelected, final Activity context,final int position)
	{
		switch (optionSelected) {
			case ConstanstMenuOpcionesHamburguesa.MENU_ADMINISTRAR:
				setIsOterApp(true);
				OpcionesHamburguesaViewController.showMenuAdministrar(context);
				final Intent intent = new Intent(context,MenuAdministrarViewController.class);
				context.startActivity(intent);
				break;
			case ConstanstMenuOpcionesHamburguesa.ADMINISTRAR_ALERTAS:
				setIsOterApp(true);
				OpcionesHamburguesaViewController.showAdministrarAlertas(context,"Alertas");
				break;
			case ConstanstMenuOpcionesHamburguesa.MAS_APLICACIONES:
				setIsOterApp(true);
				showOtrasApps(context);
				break;
			case ConstanstMenuOpcionesHamburguesa.NOTIFICACIONES:
				setIsOterApp(true);
				if (getBanderaSession().equals(ConstanstMenuOpcionesHamburguesa.CON_SESSION)){
					OpcionesHamburguesaViewController.showAdministrarAlertas(context,"SMS");
				}
				else if (getBanderaSession().equals(ConstanstMenuOpcionesHamburguesa.SIN_SESSION)){
					OpcionesApisHamburguesa.showNotificaciones(context);
				}
				break;
			case ConstanstMenuOpcionesHamburguesa.AYUDA:
				IndicadorHamburguesa.showErrorMessage(context, ConstanstMenuOpcionesHamburguesa.MEJORAR_APLICACION + " " + OpcionesApisHamburguesa.definirAplicativo(appOrigen), null);
				break;
			case ConstanstMenuOpcionesHamburguesa.COMPARTIR:
				setIsOterApp(true);
				OpcionesApisHamburguesa.showCompartir(context, appOrigen);
				break;
			case ConstanstMenuOpcionesHamburguesa.VALORAR:
				setIsOterApp(true);
				OpcionesApisHamburguesa.showValorar(context, appOrigen);
				break;
			case ConstanstMenuOpcionesHamburguesa.CONTACTANOS:
				setIsOterApp(true);
				final InitContactanos initContactanos= new InitContactanos(context);
				initContactanos.iniciarContactanos();
				contactanos = true;
				break;
			case ConstanstMenuOpcionesHamburguesa.NOVEDADES:
				setIsOterApp(true);
				Intent intentNovedades = new Intent(context,NovedadesViewController.class);
				context.startActivity(intentNovedades);
				break;
			case ConstanstMenuOpcionesHamburguesa.SALIR:
					InitHamburguesa.getInstance().getCallBackSession().cierraSesion();
				break;
			default:
				//Call Api Default
				break;
		}
		if(elementosMenu.get(position).getNombreApi().equals(ConstanstMenuOpcionesHamburguesa.ACERCA_DE)){
			setIsOterApp(true);
			OpcionesApisHamburguesa.showAcercaDe(context);
		}
	}

	public static void initValuesRequest(final Context context,final String nombreApp)
	{
		SuiteApp.appContext = context;
		datosBmovil = DetalleInformacionHamburguesaViewController.getDatosBmovil();
		final String versionMenuHam = datosBmovil.getVersionCatalogoHamburguesa();

		setParamTable(DetalleInformacionHamburguesaViewController.getParamTable());
		addParamTable(ConstanstMenuOpcionesHamburguesa.NOMBRE_APP, nombreApp);
		addParamTable(ConstanstMenuOpcionesHamburguesa.VERSION_MENU_HAM, versionMenuHam);
		try {
			final ICommService serverproxy = new CommServiceProxy(context);
			IResponseService resultado;
			final ParametersTO parameters = new ParametersTO();
			parameters.setSimulation(ServerCommons.SIMULATION);
			parameters.setDevelopment(ServerCommons.DEVELOPMENT);
			parameters.setParameters(((Hashtable<Object, Object>) getParamTable()).clone());
			parameters.setJson(true);
			parameters.setOperationId(112);
			resultado = serverproxy.request(parameters,MenuHamburguesaData.class);
			responseObject = (MenuHamburguesaData) resultado.getObjResponse();
			responseString = resultado.getResponseString();
			buildingData();
		} catch (JSONException e) {
			Log.e("Error JSON", e.toString(), e);

		} catch (IOException e) {
			Log.e("Error IO", e.toString(), e);
		}
	}

	private static void buildingHamburguesa(final Activity context)
	{
		final MenuHamburguesaData obj = DetalleInformacionHamburguesaViewController.procesarCatalogo(datosBmovil.getCatalogoHamburguesa());
		for (int i = 0; i < obj.getOpciones().size(); i++)
		{
			final OpcionesMenuHamburguesa opciones = obj.getOpciones().get(i);
			if(ConstanstMenuOpcionesHamburguesa.SIN_SESSION.equals(getBanderaSession())) {
				DetalleInformacionHamburguesaViewController.mostrarOpcionesSinSession(opciones, elementosMenu);
			}
			if(ConstanstMenuOpcionesHamburguesa.CON_SESSION.equals(getBanderaSession())) {
				DetalleInformacionHamburguesaViewController.mostrarOpcionesConSession(opciones, elementosMenu);
			}
		}
		DetalleInformacionHamburguesaViewController.agregaOpciones(elementosMenu, listaOpciones, context);
	}

	private static void showOtrasApps(final Activity context) {
		new InitOtrasApps(new BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR,ServerCommons.DEVELOPMENT));
		InitOtrasApps.getInstance().setCallBackSession(InitHamburguesa.getInstance().getCallBackSession());
		IndicadorOtrasApps.muestraIndicadorActividad(context,
				context.getString(R.string.alert_operation), context.getString(R.string.alert_connecting));
		new Thread(new Runnable() {
			@Override
			public void run() {
				OtrasAplicacionesViewControllers.nombreApp = ConstanstMenuOpcionesHamburguesa.VALUE_NOMBRE_APP;
				try {
					OtrasAplicacionesViewControllers.initValuesForRequest(context);
					final Intent intentOtrasApps = new Intent(context,OtrasAplicacionesViewControllers.class);
					context.startActivity(intentOtrasApps);
				}
				catch (IOException e) {
					DetalleInformacionOtrasAplicacionesViewController.logAndIndicador(e);
					context.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							IndicadorOtrasApps.showErrorMessage(context, OtrasAplicacionesViewControllers.getResponseString(), null);
						}
					});
				} catch (JSONException e) {
					Log.e("Error", e.toString(), e);
				}
			}
		}).start();
	}

	public static void bloquearDrawer(final DrawerLayout drawerLayout, final boolean modeInactivo)
	{
		if (modeInactivo){
			drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
		}
		else {
			drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
		}
	}

	public static boolean buildingData()
	{
		if(!DetalleInformacionHamburguesaViewController.checkRespuesta(datosBmovil, responseObject)
				&& DetalleInformacionHamburguesaViewController.checkCatalogo(datosBmovil,responseObject)&&
				DetalleInformacionHamburguesaViewController.isDifNull(responseObject.getOpciones()) && responseObject.getOpciones().size() != 0) {
			datosBmovil.setCatalogoHamburguesa(responseString);
			datosBmovil.setVersionCatalogoHamburguesa(responseObject.getVersionMenuHam());
			return true;
		}
		return false;
	}


}