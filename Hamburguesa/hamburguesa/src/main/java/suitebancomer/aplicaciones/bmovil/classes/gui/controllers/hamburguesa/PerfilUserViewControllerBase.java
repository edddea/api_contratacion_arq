package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;

import android.app.Activity;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.bancomer.mbanking.hamburguesa.R;
import suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa.ConstanstMenuOpcionesHamburguesa;
import suitebancomer.aplicaciones.bmovil.classes.delegates.hamburguesa.SelectPhotoPerfilDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
/**
 * Created by rodolfo on 4/25/16.
 * clase creada para cumplir reglas  sonar.
 */
public class PerfilUserViewControllerBase extends Activity
{
    protected Boolean cierraAplicacion = Boolean.TRUE;
    protected Boolean isActivityChanging = Boolean.TRUE;
    ImageButton btnCambiarFondo;
    static ImageView btnCambiarImagePerfil;
    public static ImageView linearFondoHamburguesa;
    public static SharedPreferences.Editor editor;
    public static SharedPreferences pref;
    public static TextView textViewUsername;

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getCallingActivity();
        cierraAplicacion = Boolean.TRUE;
        isActivityChanging = Boolean.TRUE;
        hideTitle();
        setContentView(R.layout.layout_perfil_user);
        linearFondoHamburguesa = (ImageView) this.findViewById(R.id.fondoPerfil);
        pref = this.getSharedPreferences("MyPref", Activity.MODE_PRIVATE);
        editor = pref.edit();
        initValues();
        textViewUsername = (TextView) findViewById(R.id.textViewUsername);
        if (DetalleInformacionHamburguesaViewController.isDifNull(MenuHamburguesaViewsControllers.getBanderaSession()) && MenuHamburguesaViewsControllers.getBanderaSession().equals(ConstanstMenuOpcionesHamburguesa.CON_SESSION)){
            final Session session = Session.getInstance(this);
            textViewUsername.setText(session.getNombreCliente());
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (null != pref.getString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA,null)){
            if (null != pref.getString(ConstanstMenuOpcionesHamburguesa.BANDERA_GALERIAF, null) && pref.getString(ConstanstMenuOpcionesHamburguesa.BANDERA_GALERIAF,null).equals("predeterminadas")){
                linearFondoHamburguesa.setBackgroundResource(Integer.parseInt(pref.getString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA, null)));
            }else{
                final SelectPhotoPerfilDelegate delegate = new SelectPhotoPerfilDelegate();
                delegate.buscarImagenFondo(Boolean.FALSE,Boolean.FALSE, Uri.parse(pref.getString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA, null)),this);
            }
        }
        if (null == pref.getString(ConstanstMenuOpcionesHamburguesa.PERFIL_IMAGE_URI, null)){
            final SelectPhotoPerfilDelegate delegate = new SelectPhotoPerfilDelegate();
            delegate.resetearImagen(2);
        }else {
            final SelectPhotoPerfilDelegate delegate = new SelectPhotoPerfilDelegate();
            delegate.buscarImagenPerfil(Boolean.FALSE, Boolean.FALSE, Uri.parse(pref.getString(ConstanstMenuOpcionesHamburguesa.PERFIL_IMAGE_URI, null)));
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(InitHamburguesa.getInstance().getCallBackSession() != null) {
            InitHamburguesa.getInstance().getCallBackSession().userInteraction();
        }
        if (null != pref.getString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA,null)){
            if (null != pref.getString(ConstanstMenuOpcionesHamburguesa.BANDERA_GALERIAF, null) && pref.getString(ConstanstMenuOpcionesHamburguesa.BANDERA_GALERIAF,null).equals("predeterminadas")){
                linearFondoHamburguesa.setBackgroundResource(Integer.parseInt(pref.getString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA, null)));
            }else{
                final SelectPhotoPerfilDelegate delegate = new SelectPhotoPerfilDelegate();
                delegate.buscarImagenFondo(Boolean.FALSE, Boolean.FALSE, Uri.parse(pref.getString(ConstanstMenuOpcionesHamburguesa.FONDO_PERFIL_HAMBURGUESA, null)), this);
            }
        }
        final SelectPhotoPerfilDelegate delegate = new SelectPhotoPerfilDelegate();
        if (null == pref.getString(ConstanstMenuOpcionesHamburguesa.PERFIL_IMAGE_URI, null)){
            delegate.resetearImagen(2);
        }else {
            delegate.buscarImagenPerfil(Boolean.FALSE, Boolean.FALSE, Uri.parse(pref.getString(ConstanstMenuOpcionesHamburguesa.PERFIL_IMAGE_URI, null)));
        }
    }

    private void hideTitle() {
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
    }

    public void initValues()
    {
        btnCambiarFondo = (ImageButton) this.findViewById(R.id.btnCambiarFondo);
        btnCambiarImagePerfil = (ImageView) findViewById(R.id.btnPerfil1);
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        cierraAplicacion = Boolean.TRUE;
        isActivityChanging = Boolean.TRUE;
        if(InitHamburguesa.getInstance().getCallBackSession() != null) {
            InitHamburguesa.getInstance().getCallBackSession().userInteraction();
        }
    }
}
