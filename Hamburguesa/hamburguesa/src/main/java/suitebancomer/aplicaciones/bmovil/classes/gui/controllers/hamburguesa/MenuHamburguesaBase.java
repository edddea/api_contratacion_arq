package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;


import android.content.SharedPreferences;
import android.widget.ImageView;
import android.widget.TextView;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
/**
 * Created by rodolfo on 4/25/16.
 * se crea clase base para cumplir con requerimientos de Sonar
 */
public abstract class MenuHamburguesaBase extends MenuHamburgesaExt {
    public static ImageView linearFondoHamburguesa;
    public static ImageView text;
    public static ImageView imagenPerfil;
    public static TextView usuarioPerfil;

    private static String banderaSession;


    public static ImageView getImagenPerfil() {
        return imagenPerfil;
    }
    public static SharedPreferences.Editor getEditor() {
        return editor;
    }
    public static ImageView getLinearFondoHamburguesa() {
        return linearFondoHamburguesa;
    }
    // Getters and Setters


    public static DatosBmovilFileManager getDatosBmovil() {
        return datosBmovil;
    }

    public static void setDatosBmovil(final DatosBmovilFileManager datosBmovil) {
        MenuHamburguesaViewsControllers.datosBmovil = datosBmovil;
    }


    public static void setBanderaSession(final String banderaSession) {
        MenuHamburguesaBase.banderaSession = banderaSession;
    }

    public static String getBanderaSession() {
        return banderaSession;
    }

}
