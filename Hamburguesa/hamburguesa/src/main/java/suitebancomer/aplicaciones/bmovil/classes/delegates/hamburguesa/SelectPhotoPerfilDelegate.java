package suitebancomer.aplicaciones.bmovil.classes.delegates.hamburguesa;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;
import com.bancomer.mbanking.hamburguesa.R;
import java.io.File;
import java.io.FileOutputStream;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.MenuHamburguesaViewsControllers;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.PerfilUserViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.SeleccionaFondoPerfilViewController;
/**
 * Created by eluna on 17/03/2016.
 */
public class SelectPhotoPerfilDelegate
{
    public void buscarImagenPerfil(final boolean fondo,final boolean burger,final Uri selectedImage)
    {
        Bitmap bitmap;
        bitmap = decodeFile(selectedImage.getPath());
        final int targetWidth = 200;
        final int targetHeight = 200;
        final Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,targetHeight,Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(targetBitmap);
        final Path path = new Path();
        path.addCircle(((float) targetWidth - 1) / 2, ((float) targetHeight - 1) / 2,Math.min((float) targetWidth,(float) targetHeight) / 2,Path.Direction.CCW);
        canvas.clipPath(path);
        final Bitmap sourceBitmap = bitmap;

        if(sourceBitmap==null){
            return;
        }
        canvas.drawBitmap(sourceBitmap, new Rect(0, 0, sourceBitmap.getWidth(), sourceBitmap.getHeight()), new Rect(0, 0, targetWidth, targetHeight), new Paint(Paint.FILTER_BITMAP_FLAG));
        if (burger)
        {
            MenuHamburguesaViewsControllers.getImagenPerfil().setImageBitmap(targetBitmap);
            MenuHamburguesaViewsControllers.getImagenPerfil().setBackgroundResource(R.drawable.border);
            MenuHamburguesaViewsControllers.getImagenPerfil().setAdjustViewBounds(true);
            MenuHamburguesaViewsControllers.getImagenPerfil().setScaleType(ImageView.ScaleType.FIT_CENTER);
        }
        else {
            PerfilUserViewController.getBtnCambiarImagePerfil().setImageBitmap(targetBitmap);
            PerfilUserViewController.getBtnCambiarImagePerfil().setBackgroundResource(R.drawable.border);
            PerfilUserViewController.getBtnCambiarImagePerfil().setAdjustViewBounds(true);
            PerfilUserViewController.getBtnCambiarImagePerfil().setScaleType(ImageView.ScaleType.FIT_CENTER);
        }
        if (fondo){
            SeleccionaFondoPerfilViewController.getImg().setImageBitmap(targetBitmap);
            SeleccionaFondoPerfilViewController.getImg().setBackgroundResource(R.drawable.border);
            PerfilUserViewController.getBtnCambiarImagePerfil().setAdjustViewBounds(true);
            SeleccionaFondoPerfilViewController.getImg().setScaleType(ImageView.ScaleType.FIT_CENTER);
        }
    }

    public void resetearImagen(final int pantalla)
    {
        if (pantalla == 1){
            MenuHamburguesaViewsControllers.getImagenPerfil().setImageResource(R.drawable.im_avatar);
            MenuHamburguesaViewsControllers.getImagenPerfil().setAdjustViewBounds(true);
        }
        else if (pantalla == 2){
            PerfilUserViewController.getBtnCambiarImagePerfil().setImageResource(R.drawable.ic_editaravatar);
            PerfilUserViewController.getBtnCambiarImagePerfil().setBackgroundResource(R.drawable.im_avatar);
            PerfilUserViewController.getBtnCambiarImagePerfil().setAdjustViewBounds(true);
        }
        else if (pantalla == 3){
            SeleccionaFondoPerfilViewController.getImg().setImageResource(R.drawable.im_avatar);
            SeleccionaFondoPerfilViewController.getImg().setAdjustViewBounds(true);
        }
    }

    public  Bitmap decodeFile(final String path) {
        final int orientation;
        try {
            if (path == null) {
                return null;
            }
            final BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            final int REQUIRED_SIZE = 70;
            int widthtmp = o.outWidth, heighttmp = o.outHeight;
            int scale = 0;
            while (true) {
                if (widthtmp / 2 < REQUIRED_SIZE
                        || heighttmp / 2 < REQUIRED_SIZE)
                    break;
                widthtmp /= 2;
                heighttmp /= 2;
                scale++;
            }
            final BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            final Bitmap bm = BitmapFactory.decodeFile(path, o2);
            Bitmap bitmap = bm;

            final ExifInterface exif = new ExifInterface(path);

            orientation = exif
                    .getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);

            Log.e("orientation", "" + orientation);
            final Matrix m = new Matrix();

            if ((orientation == ExifInterface.ORIENTATION_ROTATE_180)) {
                m.postRotate(180);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
                return bitmap;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                m.postRotate(90);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
                return bitmap;
            }
            else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                m.postRotate(270);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
                return bitmap;
            }
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }

    public void buscarImagenFondo(final boolean fondo,final boolean burger,final Uri selectedImage,final Context ctx)
    {
        Bitmap bitmap = decodeFile(selectedImage.getPath());
        final Drawable d = new BitmapDrawable(ctx.getResources(), bitmap);

        final Bitmap sourceBitmap = bitmap;

        if(sourceBitmap==null){
            return;
        }

        if (burger) {
            MenuHamburguesaViewsControllers.getLinearFondoHamburguesa().setBackgroundDrawable(d);
        } else {
            PerfilUserViewController.getLinearFondoHamburguesa().setBackgroundDrawable(d);
        }
        if (fondo) {
            SeleccionaFondoPerfilViewController.getLinearFondoAct().setBackgroundDrawable(d);
        }
    }

    public  Bitmap decodeFile(final String path, final int orientation) {
        try {
            if (path == null) {
                return null;
            }
            final BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            final int REQUIRED_SIZE = 70;
            int widthtmp = o.outWidth, heighttmp = o.outHeight;
            int scale = 0;
            while (true) {
                if (widthtmp / 2 < REQUIRED_SIZE
                        || heighttmp / 2 < REQUIRED_SIZE)
                    break;
                widthtmp /= 2;
                heighttmp /= 2;
                scale++;
            }
            final BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            final Bitmap bm = BitmapFactory.decodeFile(path, o2);
            Bitmap bitmap = bm;

            Log.e("orientation", "" + orientation);
            final Matrix m = new Matrix();

            if ((orientation == ExifInterface.ORIENTATION_ROTATE_180)) {
                m.postRotate(180);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
                return bitmap;
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                m.postRotate(90);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
                return bitmap;
            }
            else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                m.postRotate(270);
                bitmap = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(),
                        bm.getHeight(), m, true);
                return bitmap;
            }
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean saveImageToInternalStorage(final Bitmap bitmap,final Uri url)
    {
        try {
            File f = new File(url.getPath());
            FileOutputStream fis = new FileOutputStream (f);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fis);
            fis.close();
            return true;
        } catch (Exception e) {
            Log.e("saveToInternalStorage()", e.getMessage());
            return false;
        }
    }
}
