package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;

import android.content.SharedPreferences;

import java.util.Map;

import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;

/**
 * Created by rodolfo on 4/27/16.
 * clase creada para cumplir reglas de sonar
 */
public class MenuHamburgesaExt {
    public static SharedPreferences.Editor editor;
    public static SharedPreferences pref;
    private static boolean isOterApp=false;
    static DatosBmovilFileManager datosBmovil;
    private static Map<Object, Object> paramTable;

    public static boolean getIsOterApp() {return isOterApp;}

    public static void setIsOterApp(final boolean isOterApp) {
        MenuHamburgesaExt.isOterApp = isOterApp;
    }


    public static Map<Object, Object> getParamTable() {
        return paramTable;
    }

    public static void setParamTable(final Map<Object, Object> paramTable) {
        MenuHamburgesaExt.paramTable = paramTable;
    }

    public static void addParamTable(final String key, final String value){
        MenuHamburgesaExt.paramTable.put(key,value);

    }

}
