package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.bancomer.mbanking.hamburguesa.R;
import bancomer.api.common.commons.Constants;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
/**
 * @author Francisco.Garcia
 * @author IDS Comercial eluna
 *
 */
public class AcercaDeViewController extends Activity{

	TextView lblVersion;
	TextView lblIdentificador;
	TextView lblTerminal;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		hideTitle();
		setContentView(R.layout.layout_bmovil_acercade);

		lblVersion = (TextView) findViewById(R.id.acerca_de_version_value_label);
		lblIdentificador = (TextView) findViewById(R.id.acerca_de_identificador_value_label);
		lblTerminal = (TextView) findViewById(R.id.acerca_de_terminal_value_label);

		showDescripcion();
		configurarPantalla();
	}
	
	private void configurarPantalla(){
		final GuiTools gTools = GuiTools.getCurrent();
		gTools.init(getWindowManager());
		gTools.scale(lblVersion, true);
		gTools.scale(lblIdentificador,true);
		gTools.scale(lblTerminal, true);
		gTools.scale(findViewById(R.id.acerca_de_legal_label), true);
		gTools.scale(findViewById(R.id.acerca_de_info_sistema_label), true);
		gTools.scale(findViewById(R.id.title_text_acercadeh));
		gTools.scale(findViewById(R.id.title_icon_acercadeh));
		gTools.scale(findViewById(R.id.title_divider_acercadeh));
	}

	private void hideTitle() {
		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);

	}

	private void showDescripcion() {
		inicializaAcercaDe();
	}

	public void inicializaAcercaDe() {
		final String version = getString(R.string.administrar_acercade_version)
				+ " "
				+ Tools.getDoubleAmountFromServerString(Constants.APPLICATION_VERSION);
		final String identificador = getString(R.string.administrar_acercade_identificador)
				+ " "
				+ getString(R.string.app_identifier);
		final String terminal = getString(R.string.administrar_acercade_terminal)
				+ " "
				+ Tools.getDeviceForAcercaDe();
		getLblVersion().setText(version);
		getLblIdentificador().setText(identificador);
		getLblTerminal().setText(terminal);
	}

	public TextView getLblVersion() {
		return lblVersion;
	}

	public TextView getLblIdentificador() {
		return lblIdentificador;
	}

	public TextView getLblTerminal() {
		return lblTerminal;
	}

}