package suitebancomer.aplicaciones.bmovil.classes.alertas.impl;

import android.content.Context;
import android.util.Log;
import com.bancomer.mbanking.softtoken.SuiteAppApi;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.ContratacionSTDelegate;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

/**
 * Created by amgonzaleze on 28/08/2015.
 */
public class GeneraOTPS implements bancomer.api.common.generaOTPS.GeneraOTPS{
    public void inicializaCore(final Context context) {
        final SuiteAppApi softoken=new SuiteAppApi();
        softoken.onCreate(context);


        if(null == SuiteAppApi.getInstanceApi().getSofttokenApplicationApi())
            SuiteAppApi.getInstanceApi().startSofttokenAppApi();

    }

    @Override
    public boolean borraToken() {
        return false;
    }

    @Override
    public void inicializaCore() {
        //Metodo implementado por la clase GeneraOTPS
    }

    @Override
    public String generaOTPTiempo() {
        return null;
    }

    @Override
    public String generaOTPChallenger(final String challenge) {
        return null;
    }

    @Override
    public void validaDatos(final String cuenta) {
        //Metodo implementado por la clase GeneraOTPS
    }

    @Override
    public void borrarDatosConAlerta() {
        //Metodo implementado por la clase GeneraOTPS
    }

    @Override
    public String generarOtpTiempoApi() {
        if(ServerCommons.ALLOW_LOG) Log.d("GeneraOTPSTDelegate", "Se genero una otp por tiempo");
        String result = "";
        final ContratacionSTDelegate contDelegate = new ContratacionSTDelegate();
        contDelegate.generaTokendelegate.inicializaCore();
        result = contDelegate.generaTokendelegate.generaOTPTiempo();
        return Tools.isEmptyOrNull(result) ? null : result;
    }

    @Override
    public String generarOtpChallengeApi(final String challenge) {
        if(challenge.length() != 5) {
            if(ServerCommons.ALLOW_LOG) Log.e("GeneraOTPSTDelegate", "El numero de challenge '" + challenge + "' no tiene exactamente 5 digitos.");
            return null;
        }
        if(ServerCommons.ALLOW_LOG) Log.d("GeneraOTPSTDelegate", "Se genero una otp challenge para: " + challenge);
        String result = "";
        final ContratacionSTDelegate contDelegate = new ContratacionSTDelegate();
        contDelegate.generaTokendelegate.inicializaCore();
        result = contDelegate.generaTokendelegate.generaOTPChallenger(challenge);
        return Tools.isEmptyOrNull(result) ? null : result;
    }
}
