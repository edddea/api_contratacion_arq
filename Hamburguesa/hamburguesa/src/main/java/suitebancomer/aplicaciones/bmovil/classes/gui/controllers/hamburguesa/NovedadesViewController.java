package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import com.bancomer.mbanking.hamburguesa.R;
import suitebancomercoms.classes.common.GuiTools;
/**
 * Created by eluna on 16/05/2016.
 */
public class NovedadesViewController extends Activity
{

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hideTitle();
        setContentView(R.layout.layout_bmovil_novedades);
        final GuiTools gTools = GuiTools.getCurrent();
        gTools.init(getWindowManager());
        gTools.scale(findViewById(R.id.title_icon_acercadeh));
        gTools.scale(findViewById(R.id.acerca_de_info_sistema_label),true);

    }

    private void hideTitle() {
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

    }
}
