package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.crop;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.media.ExifInterface;
import android.media.FaceDetector;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import com.bancomer.mbanking.hamburguesa.R;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import suitebancomer.aplicaciones.bmovil.classes.delegates.hamburguesa.SelectPhotoPerfilDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.InitHamburguesa;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.gallery.IImage;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.gallery.IImageList;
/**
 * The activity can crop specific region of interest from an image.
 */
public class CropImage extends MonitoredActivity {
    private static final String TAG = "CropImage";

    // These are various options can be specified in the intent.
    private Bitmap.CompressFormat mOutputFormat = Bitmap.CompressFormat.JPEG; // only used with mSaveUri
    private int mOutputQuality = 100; // only used with mSaveUri and JPEG format
    private Uri mSaveUri = null;
    private boolean mSetWallpaper = false;
    private int mAspectX, mAspectY;
    private boolean mDoFaceDetection = true;
    private boolean mCircleCrop = false;
    private final Handler mHandler = new Handler();

    // These options specifiy the output image size and whether we should
    // scale the output to fit it (or just crop it).
    private int mOutputX, mOutputY;
    private boolean mScale;
    private boolean mScaleUp = true;

    boolean mWaitingToPick; // Whether we are wait the user to pick a face.
    boolean mSaving;  // Whether the "save" button is already clicked.

    private CropImageView mImageView;
    private ContentResolver mContentResolver;

    private Bitmap mBitmap;
    HighlightView mCrop;

    private IImageList mAllImages;
    private IImage mImage;

    private int mOutlineColor;
    private int mOutlineCircleColor;

    @Override
    public void onCreate(final Bundle icicle) {
        super.onCreate(icicle);
        mContentResolver = getContentResolver();

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.cropimage);

        mImageView = (CropImageView) findViewById(R.id.image);

        if (Build.VERSION.SDK_INT > 10 && Build.VERSION.SDK_INT < 16) { // >= Gingerbread && < Jelly Bean
            mImageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

       /* muestraIndicadorActividad("", getResources().getString(R.string.runningFaceDetection));
        new Thread(new Runnable() {
            @Override
            public void run() {
                rotaImagen();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        init();
                    }
                });

            }
        }).start();*/
        init();
    }

    private void rotaImagen()
    {
        final SharedPreferences pref = this.getSharedPreferences("MyPref", Activity.MODE_PRIVATE);
        /*Imagen entra en camara de perfil*/
        if (mSaveUri != null && mSaveUri.toString().contains("temporalPerfil"))
        {
            Uri imageCamara = Uri.parse(pref.getString("perfilImageURIT", null));
            int oritntacion = rotacionGaleria(imageCamara.getPath());
            final SelectPhotoPerfilDelegate delegate = new SelectPhotoPerfilDelegate();
            final Bitmap bitmapRotate = delegate.decodeFile(imageCamara.getPath(),oritntacion);
            delegate.saveImageToInternalStorage(bitmapRotate, imageCamara);
        }
        /*Imagen entra en camara Imagen de fondo de banner Hamburguesa*/
        else if (mSaveUri != null && mSaveUri.toString().contains("temporalFondo"))
        {
            Uri imageCamara = Uri.parse(pref.getString("fondoPerfilHamburguesaT", null));
            int oritntacion = rotacionGaleria(imageCamara.getPath());
            final SelectPhotoPerfilDelegate delegate = new SelectPhotoPerfilDelegate();
            final Bitmap bitmapRotate = delegate.decodeFile(imageCamara.getPath(),oritntacion);
            delegate.saveImageToInternalStorage(bitmapRotate, imageCamara);
        }
    }

    private  int  rotacionGaleria(String src)
    {
        int orientation = 1;
        try {
            ExifInterface exif = new ExifInterface(src);
            String orientationString=exif.getAttribute(ExifInterface.TAG_ORIENTATION);
            orientation = Integer.parseInt(orientationString);
        }
        catch(Exception e){
            Log.e("saveToInternalStorage()", e.getMessage());
        }
        return orientation;

    }

    ProgressDialog mProgressDialog;
    public void muestraIndicadorActividad(String strTitle, String strMessage) {
        if(mProgressDialog != null){
            if(mProgressDialog != null){
                mProgressDialog.dismiss();
            }
        }
            mProgressDialog = ProgressDialog.show(this, strTitle,
                    strMessage, true);
        mProgressDialog.setCancelable(false);
    }

    public void ocultaIndicadorActividad() {
            if (mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
    }



    private void init(){
        //ocultaIndicadorActividad();
        final Intent intent = getIntent();
        final Bundle extras = intent.getExtras();

        if (extras != null) {
            if (extras.getBoolean("circleCrop", false)) {
                mCircleCrop = true;
                mAspectX = 1;
                mAspectY = 1;
                mOutputFormat = Bitmap.CompressFormat.PNG;
            }
            mSaveUri = (Uri) extras.getParcelable(MediaStore.EXTRA_OUTPUT);
            if (mSaveUri != null) {
                final String outputFormatString = extras.getString("outputFormat");
                if (outputFormatString != null) {
                    mOutputFormat = Bitmap.CompressFormat.valueOf(
                            outputFormatString);
                }
                mOutputQuality = extras.getInt("outputQuality", 100);
            } else {
                mSetWallpaper = extras.getBoolean("setWallpaper");
            }
            final SharedPreferences.Editor editor;
            final SharedPreferences pref = getSharedPreferences("MyPref", MODE_PRIVATE);
            mBitmap = (Bitmap) extras.getParcelable("data");
            mAspectX = extras.getInt("aspectX");
            mAspectY = extras.getInt("aspectY");
            mOutputX = extras.getInt("outputX");
            mOutputY = extras.getInt("outputY");
            mOutlineColor = extras.getInt("outlineColor", HighlightView.DEFAULT_OUTLINE_COLOR);
            mOutlineCircleColor = extras.getInt("outlineCircleColor", HighlightView.DEFAULT_OUTLINE_CIRCLE_COLOR);
            mScale = extras.getBoolean("scale", true);
        }

        if (mBitmap == null) {
            final Uri target = intent.getData();
            mAllImages = ImageManager.makeImageList(mContentResolver, target, ImageManager.SORT_ASCENDING);
            mImage = mAllImages.getImageForUri(target);
            if (mImage != null) {
                mBitmap = mImage.thumbBitmap(IImage.ROTATE_AS_NEEDED);
            }
        }
        if (mBitmap == null) {
            finish();
            return;
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        findViewById(R.id.discard).setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        setResult(RESULT_CANCELED);
                        if(InitHamburguesa.getInstance().getCallBackSession() != null) {
                            InitHamburguesa.getInstance().getCallBackSession().userInteraction();
                        }
                        finish();
                    }
                });

        findViewById(R.id.save).setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        onSaveClicked();
                        if(InitHamburguesa.getInstance().getCallBackSession() != null) {
                            InitHamburguesa.getInstance().getCallBackSession().userInteraction();
                        }
                    }
                });

        startFaceDetection();
    }

    private void startFaceDetection() {
        if (isFinishing()) {
            return;
        }

        mImageView.setImageBitmapResetBase(mBitmap, true);

        Util.startBackgroundJob(this, null,
                getResources().getString(R.string.runningFaceDetection),
                new Runnable() {
                    public void run() {
                        rotaImagen();
                        final CountDownLatch latch = new CountDownLatch(1);
                        final Bitmap b = (mImage != null)
                                ? mImage.fullSizeBitmap(IImage.UNCONSTRAINED,
                                1024 * 1024)
                                : mBitmap;
                        mHandler.post(new Runnable() {
                            public void run() {
                                if (!b.equals(mBitmap) && b != null) {
                                    mImageView.setImageBitmapResetBase(b, true);
                                    mBitmap.recycle();
                                    mBitmap = b;
                                }
                                if (mImageView.getScale() == 1F) {
                                    mImageView.center(true, true);
                                }
                                latch.countDown();
                            }
                        });
                        try {
                            latch.await();
                        } catch (InterruptedException e) {
                            Log.e("","Interrupted Exception");
                        }
                        mRunFaceDetection.run();
                    }
                }, mHandler);
    }

    private void onSaveClicked() {
        // TODO this code needs to change to use the decode/crop/encode single
        // step api so that we don't require that the whole (possibly large)
        // bitmap doesn't have to be read into memory
        if (mCrop == null) {
            return;
        }

        if (mSaving) return;
        mSaving = true;

        Bitmap croppedImage;

        // If the output is required to a specific size, create an new image
        // with the cropped image in the center and the extra space filled.
        if (mOutputX != 0 && mOutputY != 0 && !mScale) {
            // Don't scale the image but instead fill it so it's the
            // required dimension
            croppedImage = Bitmap.createBitmap(mOutputX, mOutputY, Bitmap.Config.RGB_565);
            final Canvas canvas = new Canvas(croppedImage);
            final Rect srcRect = mCrop.getCropRect();
            final Rect dstRect = new Rect(0, 0, mOutputX, mOutputY);
            final int dx = (srcRect.width() - dstRect.width()) / 2;
            final int dy = (srcRect.height() - dstRect.height()) / 2;
            // If the srcRect is too big, use the center part of it.
            srcRect.inset(Math.max(0, dx), Math.max(0, dy));
            // If the dstRect is too big, use the center part of it.
            dstRect.inset(Math.max(0, -dx), Math.max(0, -dy));
            // Draw the cropped bitmap in the center
            canvas.drawBitmap(mBitmap, srcRect, dstRect, null);
            // Release bitmap memory as soon as possible
            mImageView.clear();
            mBitmap.recycle();
        } else {
            final Rect r = mCrop.getCropRect();

            final int width = r.width();
            final int height = r.height();

            // If we are circle cropping, we want alpha channel, which is the
            // third param here.
            croppedImage = Bitmap.createBitmap(width, height,mCircleCrop ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
            final Canvas canvas = new Canvas(croppedImage);
            final Rect dstRect = new Rect(0, 0, width, height);
            canvas.drawBitmap(mBitmap, r, dstRect, null);

            // Release bitmap memory as soon as possible
            mImageView.clear();
            mBitmap.recycle();

            if (mCircleCrop) {
                // OK, so what's all this about?
                // Bitmaps are inherently rectangular but we want to return
                // something that's basically a circle.  So we fill in the
                // area around the circle with alpha.  Note the all important
                // PortDuff.Mode.CLEAR.
                final Canvas c = new Canvas(croppedImage);
                final Path p = new Path();
                p.addCircle(width / 2F, height / 2F, width / 2F,Path.Direction.CW);
                c.clipPath(p, Region.Op.DIFFERENCE);
                c.drawColor(0x00000000, PorterDuff.Mode.CLEAR);
            }

            // If the required dimension is specified, scale the image.
            if (mOutputX != 0 && mOutputY != 0 && mScale) {
                croppedImage = Util.transform(new Matrix(), croppedImage,
                        mOutputX, mOutputY, mScaleUp, Util.RECYCLE_INPUT);
            }
        }

        mImageView.setImageBitmapResetBase(croppedImage, true);
        mImageView.center(true, true);
        mImageView.mHighlightViews.clear();

        // Return the cropped image directly or save it to the specified URI.
        final Bundle myExtras = getIntent().getExtras();
        if (myExtras != null && (myExtras.getParcelable("data") != null
                || myExtras.getBoolean("return-data"))) {
            final Bundle extras = new Bundle();
            extras.putParcelable("data", croppedImage);
            setResult(RESULT_OK,
                    (new Intent()).setAction("inline-data").putExtras(extras));
            finish();
        } else {
            final Bitmap b = croppedImage;
            final int msdId = mSetWallpaper ? R.string.wallpaper : R.string.savingImage;
            Util.startBackgroundJob(this, null, getResources().getString(msdId), new Runnable() {
                public void run() {
                    saveOutput(b);
                }
            }, mHandler);
        }
        //Intent intent = new Intent(getBaseContext(),PerfilViewController.class);
        //startActivity(intent);
    }

    private void saveOutput(final Bitmap croppedImage) {
        if (mSaveUri != null) {

            File f = new File(mSaveUri.getPath());
            FileOutputStream fis;
            //OutputStream outputStream = null;
            try {
                fis = new FileOutputStream (f);
                croppedImage.compress(mOutputFormat, mOutputQuality, fis);
                fis.close();
            } catch (IOException ex) {
                // TODO: report error to caller
                Log.e(TAG, "Cannot open file: " + mSaveUri, ex);
            }
            final Bundle extras = new Bundle();
            setResult(RESULT_OK, new Intent(mSaveUri.toString())
                    .putExtras(extras));
        } else if (mSetWallpaper) {
            try {
                WallpaperManager.getInstance(this).setBitmap(croppedImage);
                setResult(RESULT_OK);
            } catch (IOException e) {
                Log.e(TAG, "Failed to set wallpaper.", e);
                setResult(RESULT_CANCELED);
            }
        } else {
            final Bundle extras = new Bundle();
            extras.putString("rect", mCrop.getCropRect().toString());

            final File oldPath = new File(mImage.getDataPath());
            final File directory = new File(oldPath.getParent());

            int x = 0;
            String fileName = oldPath.getName();
            fileName = fileName.substring(0, fileName.lastIndexOf("."));

            // Try file-1.jpg, file-2.jpg, ... until we find a filename which
            // does not exist yet.
            while (true) {
                x += 1;
                final String candidate = directory.toString()
                        + "/" + fileName + "-" + x + ".jpg";
                final boolean exists = (new File(candidate)).exists();
                if (!exists) {
                    break;
                }
            }

            try {
                final int[] degree = new int[1];
                final Uri newUri = ImageManager.addImage(
                        mContentResolver,
                        mImage.getTitle(),
                        mImage.getDateTaken(),
                        null,    // TODO this null is going to cause us to lose
                        // the location (gps).
                        directory.toString(), fileName + "-" + x + ".jpg",
                        croppedImage, null,
                        degree);

                setResult(RESULT_OK, new Intent()
                        .setAction(newUri.toString())
                        .putExtras(extras));
            } catch (Exception ex) {
                // basically ignore this or put up
                // some ui saying we failed
                Log.e(TAG, "store image fail, continue anyway", ex);
            }
        }

        final Bitmap b = croppedImage;
        mHandler.post(new Runnable() {
            public void run() {
                mImageView.clear();
                b.recycle();
            }
        });

        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if (mAllImages != null) {
            mAllImages.close();
        }
        super.onDestroy();
    }

    Runnable mRunFaceDetection = new Runnable() {
        @SuppressWarnings("hiding")
        float mScale = 1F;
        Matrix mImageMatrix;
        FaceDetector.Face[] mFaces = new FaceDetector.Face[3];
        int mNumFaces;

        // For each face, we create a HightlightView for it.
        private void handleFace(FaceDetector.Face f) {
            PointF midPoint = new PointF();

            final int r = ((int) (f.eyesDistance() * mScale)) * 2;
            f.getMidPoint(midPoint);
            midPoint.x *= mScale;
            midPoint.y *= mScale;

            final int midX = (int) midPoint.x;
            final int midY = (int) midPoint.y;

           final HighlightView hv = new HighlightView(mImageView, mOutlineColor, mOutlineCircleColor);

            final int width = mBitmap.getWidth();
            final int height = mBitmap.getHeight();

            final Rect imageRect = new Rect(0, 0, width, height);

            final RectF faceRect = new RectF(midX, midY, midX, midY);
            faceRect.inset(-r, -r);
            if (faceRect.left < 0) {
                faceRect.inset(-faceRect.left, -faceRect.left);
            }

            if (faceRect.top < 0) {
                faceRect.inset(-faceRect.top, -faceRect.top);
            }

            if (faceRect.right > imageRect.right) {
                faceRect.inset(faceRect.right - imageRect.right,faceRect.right - imageRect.right);
            }

            if (faceRect.bottom > imageRect.bottom) {
                faceRect.inset(faceRect.bottom - imageRect.bottom,faceRect.bottom - imageRect.bottom);
            }

            hv.setup(mImageMatrix, imageRect, faceRect, mCircleCrop,mAspectX != 0 && mAspectY != 0);
            mImageView.add(hv);
        }

        // Create a default HightlightView if we found no face in the picture.
        private void makeDefault() {
            final HighlightView hv = new HighlightView(mImageView, mOutlineColor, mOutlineCircleColor);
            final int width = mBitmap.getWidth();
            final int height = mBitmap.getHeight();
            final Rect imageRect = new Rect(0, 0, width, height);
            // make the default size about 4/5 of the width or height
            int cropWidth = Math.min(width, height) * 4 / 5;
            int cropHeight = cropWidth;

            if (mAspectX != 0 && mAspectY != 0) {
                if (mAspectX > mAspectY) {
                    cropHeight = cropWidth * mAspectY / mAspectX;
                } else {
                    cropWidth = cropHeight * mAspectX / mAspectY;
                }
            }

            final int x = (width - cropWidth) / 2;
            final int y = (height - cropHeight) / 2;
            final RectF cropRect = new RectF(x, y, x + cropWidth, y + cropHeight);
            hv.setup(mImageMatrix, imageRect, cropRect, mCircleCrop, mAspectX != 0 && mAspectY != 0);
            mImageView.add(hv);
        }

        // Scale the image down for faster face detection.
        private Bitmap prepareBitmap() {
            if (mBitmap == null ||  mBitmap.isRecycled()) {
                return null;
            }

            // 256 pixels wide is enough.
            if (mBitmap.getWidth() > 256) {
                mScale = 256.0F / mBitmap.getWidth();
            }
            final Matrix matrix = new Matrix();
            matrix.setScale(mScale, mScale);
            return Bitmap.createBitmap(mBitmap, 0, 0, mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
        }

        public void run() {
            mImageMatrix = mImageView.getImageMatrix();
            final Bitmap faceBitmap = prepareBitmap();
            mScale = 1.0F / mScale;
            if (faceBitmap != null && mDoFaceDetection) {
                final FaceDetector detector = new FaceDetector(faceBitmap.getWidth(),faceBitmap.getHeight(), mFaces.length);
                mNumFaces = detector.findFaces(faceBitmap, mFaces);
            }

            if (faceBitmap != null && !faceBitmap.equals(mBitmap)) {
                faceBitmap.recycle();
            }

            mHandler.post(new Runnable() {
                public void run() {
                    mWaitingToPick = mNumFaces > 1;
                    if (mNumFaces > 0) {
                        for (int i = 0; i < mNumFaces; i++) {
                            handleFace(mFaces[i]);
                        }
                    } else {
                        makeDefault();
                    }
                    mImageView.invalidate();
                    if (mImageView.mHighlightViews.size() == 1) {
                        mCrop = mImageView.mHighlightViews.get(0);
                        mCrop.setFocus(true);
                    }

                    if (mNumFaces > 1) {
                        final Toast t = Toast.makeText(CropImage.this, R.string.multiface_crop_help,Toast.LENGTH_SHORT);
                        t.show();
                    }
                }
            });
        }
    };
}

class CropImageView extends ImageViewTouchBase {
    ArrayList<HighlightView> mHighlightViews = new ArrayList<HighlightView>();
    HighlightView mMotionHighlightView = null;
    float mLastX, mLastY;
    int mMotionEdge;

    @Override
    protected void onLayout(final boolean changed,final int left,final int top,final int right,final int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (mBitmapDisplayed.getBitmap() != null) {
            for (final HighlightView hv : mHighlightViews) {
                hv.mMatrix.set(getImageMatrix());
                hv.invalidate();
                if (hv.mIsFocused) {
                    centerBasedOnHighlightView(hv);
                }
            }
        }
    }

    public CropImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void zoomTo(final float scale,final float centerX, float centerY) {
        super.zoomTo(scale, centerX, centerY);
        for (final HighlightView hv : mHighlightViews) {
            hv.mMatrix.set(getImageMatrix());
            hv.invalidate();
        }
    }

    @Override
    protected void zoomIn() {
        super.zoomIn();
        for (final HighlightView hv : mHighlightViews) {
            hv.mMatrix.set(getImageMatrix());
            hv.invalidate();
        }
    }

    @Override
    protected void zoomOut() {
        super.zoomOut();
        for (final HighlightView hv : mHighlightViews) {
            hv.mMatrix.set(getImageMatrix());
            hv.invalidate();
        }
    }

    @Override
    protected void postTranslate(final float deltaX,final float deltaY) {
        super.postTranslate(deltaX, deltaY);
        for (int i = 0; i < mHighlightViews.size(); i++) {
            final HighlightView hv = mHighlightViews.get(i);
            hv.mMatrix.postTranslate(deltaX, deltaY);
            hv.invalidate();
        }
    }

    // According to the event's position, change the focus to the first
    // hitting cropping rectangle.
    private void recomputeFocus(MotionEvent event) {
        for (int i = 0; i < mHighlightViews.size(); i++) {
            final HighlightView hv = mHighlightViews.get(i);
            hv.setFocus(false);
            hv.invalidate();
        }

        for (int i = 0; i < mHighlightViews.size(); i++) {
            final HighlightView hv = mHighlightViews.get(i);
            final int edge = hv.getHit(event.getX(), event.getY());
            if (edge != HighlightView.GROW_NONE) {
                if (!hv.hasFocus()) {
                    hv.setFocus(true);
                    hv.invalidate();
                }
                break;
            }
        }
        invalidate();
    }

    @Override
    public boolean onTouchEvent(final MotionEvent event) {
        final CropImage cropImage = (CropImage) this.getContext();
        if (cropImage.mSaving) {
            return false;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (cropImage.mWaitingToPick) {
                    recomputeFocus(event);
                } else {
                    for (int i = 0; i < mHighlightViews.size(); i++) {
                        final HighlightView hv = mHighlightViews.get(i);
                        final int edge = hv.getHit(event.getX(), event.getY());
                        if (edge != HighlightView.GROW_NONE) {
                            mMotionEdge = edge;
                            mMotionHighlightView = hv;
                            mLastX = event.getX();
                            mLastY = event.getY();
                            mMotionHighlightView.setMode(
                                    (edge == HighlightView.MOVE)
                                            ? HighlightView.ModifyMode.Move
                                            : HighlightView.ModifyMode.Grow);
                            break;
                        }
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                if (cropImage.mWaitingToPick) {
                    for (int i = 0; i < mHighlightViews.size(); i++) {
                        final HighlightView hv = mHighlightViews.get(i);
                        if (hv.hasFocus()) {
                            cropImage.mCrop = hv;
                            for (int j = 0; j < mHighlightViews.size(); j++) {
                                if (j == i) {
                                    continue;
                                }
                                mHighlightViews.get(j).setHidden(true);
                            }
                            centerBasedOnHighlightView(hv);
                            ((CropImage) this.getContext()).mWaitingToPick = false;
                            return true;
                        }
                    }
                } else if (mMotionHighlightView != null) {
                    centerBasedOnHighlightView(mMotionHighlightView);
                    mMotionHighlightView.setMode(
                            HighlightView.ModifyMode.None);
                }
                mMotionHighlightView = null;
                break;
            case MotionEvent.ACTION_MOVE:
                if (cropImage.mWaitingToPick) {
                    recomputeFocus(event);
                } else if (mMotionHighlightView != null) {
                    mMotionHighlightView.handleMotion(mMotionEdge,
                            event.getX() - mLastX,
                            event.getY() - mLastY);
                    mLastX = event.getX();
                    mLastY = event.getY();

                    if (true) {
                        // This section of code is optional. It has some user
                        // benefit in that moving the crop rectangle against
                        // the edge of the screen causes scrolling but it means
                        // that the crop rectangle is no longer fixed under
                        // the user's finger.
                        ensureVisible(mMotionHighlightView);
                    }
                }
                break;
        }

        switch (event.getAction()) {
            case MotionEvent.ACTION_UP:
                center(true, true);
                break;
            case MotionEvent.ACTION_MOVE:
                // if we're not zoomed then there's no point in even allowing
                // the user to move the image around.  This call to center puts
                // it back to the normalized location (with false meaning don't
                // animate).
                if (getScale() == 1F) {
                    center(true, true);
                }
                break;
        }

        return true;
    }

    // Pan the displayed image to make sure the cropping rectangle is visible.
    private void ensureVisible(final HighlightView hv) {
        final Rect r = hv.mDrawRect;

        final int panDeltaX1 = Math.max(0, this.getLeft() - r.left);
        final int panDeltaX2 = Math.min(0, this.getRight() - r.right);
        final int panDeltaY1 = Math.max(0, this.getTop() - r.top);
        final int panDeltaY2 = Math.min(0, this.getBottom() - r.bottom);
        final int panDeltaX = panDeltaX1 != 0 ? panDeltaX1 : panDeltaX2;
        final int panDeltaY = panDeltaY1 != 0 ? panDeltaY1 : panDeltaY2;
        if (panDeltaX != 0 || panDeltaY != 0) {
            panBy(panDeltaX, panDeltaY);
        }
    }

    // If the cropping rectangle's size changed significantly, change the
    // view's center and scale according to the cropping rectangle.
    private void centerBasedOnHighlightView(HighlightView hv) {
        final Rect drawRect = hv.mDrawRect;

        final float width = drawRect.width();
        final float height = drawRect.height();
        final float thisWidth = getWidth();
        final float thisHeight = getHeight();
        final float z1 = thisWidth / width * .6F;
        final float z2 = thisHeight / height * .6F;
        float zoom = Math.min(z1, z2);
        zoom = zoom * this.getScale();
        zoom = Math.max(1F, zoom);

        if ((Math.abs(zoom - getScale()) / zoom) > .1) {
            final float [] coordinates = new float[] {hv.mCropRect.centerX(),
                    hv.mCropRect.centerY()};
            getImageMatrix().mapPoints(coordinates);
            zoomTo(zoom, coordinates[0], coordinates[1], 300F);
        }

        ensureVisible(hv);
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        for (int i = 0; i < mHighlightViews.size(); i++) {
            mHighlightViews.get(i).draw(canvas);
        }
    }

    public void add(final HighlightView hv) {
        mHighlightViews.add(hv);
        invalidate();
    }



}
