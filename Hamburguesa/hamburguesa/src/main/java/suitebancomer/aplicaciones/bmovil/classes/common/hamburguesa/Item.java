package suitebancomer.aplicaciones.bmovil.classes.common.hamburguesa;

/**
 * Created by eluna on 22/03/2016.
 */
public class Item
{
    public final String text;
    public final int icon;

    public Item(final String text,final Integer icon) {
        this.text = text;
        this.icon = icon;
    }
    @Override
    public String toString() {
        return text;
    }
}