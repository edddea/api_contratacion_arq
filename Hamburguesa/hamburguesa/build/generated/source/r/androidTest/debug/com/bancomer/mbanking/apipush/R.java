/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.bancomer.mbanking.apipush;

public final class R {
	public static final class attr {
		public static final int circleCrop = 0x7f010002;
		public static final int imageAspectRatio = 0x7f010001;
		public static final int imageAspectRatioAdjust = 0x7f010000;
	}
	public static final class bool {
		public static final int isAtLeastKitKat = 0x7f0a0000;
	}
	public static final class color {
		public static final int amarillo = 0x7f0b0000;
		public static final int anaranjado = 0x7f0b0001;
		public static final int azul_fondo = 0x7f0b0002;
		public static final int blanco = 0x7f0b0004;
		public static final int common_action_bar_splitter = 0x7f0b0005;
		public static final int common_signin_btn_dark_text_default = 0x7f0b0006;
		public static final int common_signin_btn_dark_text_disabled = 0x7f0b0007;
		public static final int common_signin_btn_dark_text_focused = 0x7f0b0008;
		public static final int common_signin_btn_dark_text_pressed = 0x7f0b0009;
		public static final int common_signin_btn_default_background = 0x7f0b000a;
		public static final int common_signin_btn_light_text_default = 0x7f0b000b;
		public static final int common_signin_btn_light_text_disabled = 0x7f0b000c;
		public static final int common_signin_btn_light_text_focused = 0x7f0b000d;
		public static final int common_signin_btn_light_text_pressed = 0x7f0b000e;
		public static final int common_signin_btn_text_dark = 0x7f0b0043;
		public static final int common_signin_btn_text_light = 0x7f0b0044;
		public static final int cuarto_azul = 0x7f0b000f;
		public static final int cuarto_rojo = 0x7f0b0010;
		public static final int fondo_autenticacion = 0x7f0b0011;
		public static final int fucsia = 0x7f0b0012;
		public static final int gris_fondo = 0x7f0b0014;
		public static final int gris_texto_1 = 0x7f0b0015;
		public static final int gris_texto_2 = 0x7f0b0016;
		public static final int gris_texto_3 = 0x7f0b0017;
		public static final int gris_texto_4 = 0x7f0b0018;
		public static final int magenta = 0x7f0b0019;
		public static final int naranja = 0x7f0b001a;
		public static final int naranja2 = 0x7f0b001b;
		public static final int naranja_tenue = 0x7f0b001c;
		public static final int negro_bmovil_detalle = 0x7f0b001d;
		public static final int paperles_azul = 0x7f0b001e;
		public static final int paperles_gris = 0x7f0b001f;
		public static final int paperles_transparente = 0x7f0b0020;
		public static final int paperles_verde = 0x7f0b0021;
		public static final int possible_result_points = 0x7f0b0022;
		public static final int primer_azul = 0x7f0b0023;
		public static final int primer_rojo = 0x7f0b0025;
		public static final int progreso_fondo = 0x7f0b0026;
		public static final int quinto_azul = 0x7f0b0027;
		public static final int quinto_rojo = 0x7f0b0028;
		public static final int result_image_border = 0x7f0b0029;
		public static final int result_minor_text = 0x7f0b002a;
		public static final int result_points = 0x7f0b002b;
		public static final int result_text = 0x7f0b002c;
		public static final int result_view = 0x7f0b002d;
		public static final int segundo_azul = 0x7f0b002e;
		public static final int segundo_rojo = 0x7f0b0030;
		public static final int sexto_azul = 0x7f0b0032;
		public static final int sexto_rojo = 0x7f0b0033;
		public static final int status_text = 0x7f0b0034;
		public static final int tercer_azul = 0x7f0b0035;
		public static final int tercer_rojo = 0x7f0b0036;
		public static final int trans = 0x7f0b0037;
		public static final int turquesa = 0x7f0b0038;
		public static final int turquesa2 = 0x7f0b0039;
		public static final int turquesa3 = 0x7f0b003a;
		public static final int verde = 0x7f0b003b;
		public static final int verde2 = 0x7f0b003c;
		public static final int verde_bmovil_detalle = 0x7f0b003d;
		public static final int verde_limon = 0x7f0b003e;
		public static final int verde_st_activacion = 0x7f0b003f;
		public static final int viewfinder_frame = 0x7f0b0040;
		public static final int viewfinder_laser = 0x7f0b0041;
		public static final int viewfinder_mask = 0x7f0b0042;
	}
	public static final class dimen {
		public static final int activity_horizontal_margin = 0x7f080000;
		public static final int activity_vertical_margin = 0x7f080003;
	}
	public static final class drawable {
		public static final int abonos = 0x7f020000;
		public static final int api_alertas_digitales_header_image = 0x7f02006c;
		public static final int api_alertas_digitales_list_border = 0x7f020071;
		public static final int api_alertas_digitales_list_border_bottom_cell = 0x7f020072;
		public static final int cargos = 0x7f020113;
		public static final int common_full_open_on_phone = 0x7f020117;
		public static final int common_ic_googleplayservices = 0x7f020118;
		public static final int common_signin_btn_icon_dark = 0x7f020119;
		public static final int common_signin_btn_icon_disabled_dark = 0x7f02011a;
		public static final int common_signin_btn_icon_disabled_focus_dark = 0x7f02011b;
		public static final int common_signin_btn_icon_disabled_focus_light = 0x7f02011c;
		public static final int common_signin_btn_icon_disabled_light = 0x7f02011d;
		public static final int common_signin_btn_icon_focus_dark = 0x7f02011e;
		public static final int common_signin_btn_icon_focus_light = 0x7f02011f;
		public static final int common_signin_btn_icon_light = 0x7f020120;
		public static final int common_signin_btn_icon_normal_dark = 0x7f020121;
		public static final int common_signin_btn_icon_normal_light = 0x7f020122;
		public static final int common_signin_btn_icon_pressed_dark = 0x7f020123;
		public static final int common_signin_btn_icon_pressed_light = 0x7f020124;
		public static final int common_signin_btn_text_dark = 0x7f020125;
		public static final int common_signin_btn_text_disabled_dark = 0x7f020126;
		public static final int common_signin_btn_text_disabled_focus_dark = 0x7f020127;
		public static final int common_signin_btn_text_disabled_focus_light = 0x7f020128;
		public static final int common_signin_btn_text_disabled_light = 0x7f020129;
		public static final int common_signin_btn_text_focus_dark = 0x7f02012a;
		public static final int common_signin_btn_text_focus_light = 0x7f02012b;
		public static final int common_signin_btn_text_light = 0x7f02012c;
		public static final int common_signin_btn_text_normal_dark = 0x7f02012d;
		public static final int common_signin_btn_text_normal_light = 0x7f02012e;
		public static final int common_signin_btn_text_pressed_dark = 0x7f02012f;
		public static final int common_signin_btn_text_pressed_light = 0x7f020130;
		public static final int ic_launcher = 0x7f020150;
		public static final int ic_notificaciones = 0x7f020151;
		public static final int ic_plusone_medium_off_client = 0x7f020155;
		public static final int ic_plusone_small_off_client = 0x7f020156;
		public static final int ic_plusone_standard_off_client = 0x7f020157;
		public static final int ic_plusone_tall_off_client = 0x7f020158;
		public static final int ic_stat_ic_notification = 0x7f02015d;
		public static final int logo_bancomer = 0x7f0201ca;
		public static final int notificaciones = 0x7f0201d9;
		public static final int todos = 0x7f0201f2;
	}
	public static final class id {
		public static final int abonosIcon = 0x7f0c001e;
		public static final int abonosPush = 0x7f0c001d;
		public static final int adjust_height = 0x7f0c0008;
		public static final int adjust_width = 0x7f0c0009;
		public static final int cargosIcon = 0x7f0c001c;
		public static final int cargosPush = 0x7f0c001b;
		public static final int empty = 0x7f0c0020;
		public static final int fechaPush = 0x7f0c0284;
		public static final int horaPush = 0x7f0c0285;
		public static final int imagenNotificacion = 0x7f0c0286;
		public static final int listaNotificaciones = 0x7f0c001f;
		public static final int menu_notifications_layout = 0x7f0c0018;
		public static final int none = 0x7f0c000a;
		public static final int notificaion = 0x7f0c0283;
		public static final int textoNotificacion = 0x7f0c0287;
		public static final int todosIcon = 0x7f0c001a;
		public static final int todosPush = 0x7f0c0019;
	}
	public static final class integer {
		public static final int google_play_services_version = 0x7f0d0002;
	}
	public static final class layout {
		public static final int activity_main = 0x7f040003;
		public static final int notifications_adapter = 0x7f040099;
		public static final int notifications_custom_adapter = 0x7f04009a;
	}
	public static final class mipmap {
		public static final int an_ic_abonos = 0x7f030000;
		public static final int an_ic_abonosoff = 0x7f030001;
		public static final int an_ic_aviso = 0x7f030002;
		public static final int an_ic_avisooff = 0x7f030003;
		public static final int an_ic_cargos = 0x7f030004;
		public static final int an_ic_cargosoff = 0x7f030005;
		public static final int an_ic_noti = 0x7f030006;
		public static final int an_ic_todos = 0x7f030007;
		public static final int an_ic_todosoff = 0x7f030008;
		public static final int header_image = 0x7f030009;
		public static final int ic_stat_ic_notification = 0x7f03000d;
		public static final int ic_stat_ic_notification_bak = 0x7f03000e;
		public static final int title_divider = 0x7f030012;
	}
	public static final class raw {
		public static final int confenccom = 0x7f060002;
		public static final int tono_bancomer = 0x7f060004;
	}
	public static final class string {
		public static final int accounttype_check = 0x7f070046;
		public static final int accounttype_clabe = 0x7f070047;
		public static final int accounttype_credit = 0x7f070048;
		public static final int accounttype_express = 0x7f070049;
		public static final int accounttype_libreton = 0x7f07004a;
		public static final int accounttype_patrimonial = 0x7f07004b;
		public static final int accounttype_prepaid = 0x7f07004c;
		public static final int accounttype_savings = 0x7f07004d;
		public static final int action_settings = 0x7f07004e;
		public static final int app_label = 0x7f0700d3;
		public static final int app_name = 0x7f0700d4;
		public static final int auth_google_play_services_client_facebook_display_name = 0x7f0700d9;
		public static final int auth_google_play_services_client_google_display_name = 0x7f0700da;
		public static final int bmovil_rapidos_dinero_movil = 0x7f07029d;
		public static final int bmovil_rapidos_tiempo_aire = 0x7f07029e;
		public static final int bmovil_rapidos_traspaso = 0x7f07029f;
		public static final int calendar_availableHours = 0x7f07031b;
		public static final int calendar_availableHoursIB = 0x7f07031c;
		public static final int calendar_availableHoursTC = 0x7f07031d;
		public static final int calendar_noAvailable = 0x7f070320;
		public static final int common_android_wear_notification_needs_update_text = 0x7f070000;
		public static final int common_android_wear_update_text = 0x7f070001;
		public static final int common_android_wear_update_title = 0x7f070002;
		public static final int common_google_play_services_api_unavailable_text = 0x7f070003;
		public static final int common_google_play_services_enable_button = 0x7f070004;
		public static final int common_google_play_services_enable_text = 0x7f070005;
		public static final int common_google_play_services_enable_title = 0x7f070006;
		public static final int common_google_play_services_error_notification_requested_by_msg = 0x7f070007;
		public static final int common_google_play_services_install_button = 0x7f070008;
		public static final int common_google_play_services_install_text_phone = 0x7f070009;
		public static final int common_google_play_services_install_text_tablet = 0x7f07000a;
		public static final int common_google_play_services_install_title = 0x7f07000b;
		public static final int common_google_play_services_invalid_account_text = 0x7f07000c;
		public static final int common_google_play_services_invalid_account_title = 0x7f07000d;
		public static final int common_google_play_services_needs_enabling_title = 0x7f07000e;
		public static final int common_google_play_services_network_error_text = 0x7f07000f;
		public static final int common_google_play_services_network_error_title = 0x7f070010;
		public static final int common_google_play_services_notification_needs_update_title = 0x7f070011;
		public static final int common_google_play_services_notification_ticker = 0x7f070012;
		public static final int common_google_play_services_sign_in_failed_text = 0x7f070013;
		public static final int common_google_play_services_sign_in_failed_title = 0x7f070014;
		public static final int common_google_play_services_unknown_issue = 0x7f070015;
		public static final int common_google_play_services_unsupported_text = 0x7f070016;
		public static final int common_google_play_services_unsupported_title = 0x7f070017;
		public static final int common_google_play_services_update_button = 0x7f070018;
		public static final int common_google_play_services_update_text = 0x7f070019;
		public static final int common_google_play_services_update_title = 0x7f07001a;
		public static final int common_google_play_services_updating_text = 0x7f07001b;
		public static final int common_google_play_services_updating_title = 0x7f07001c;
		public static final int common_open_on_phone = 0x7f07001d;
		public static final int common_signin_button_text = 0x7f07001e;
		public static final int common_signin_button_text_long = 0x7f07001f;
		public static final int gcm_defaultSenderId = 0x7f070457;
		public static final int hello_world = 0x7f070459;
		public static final int info_software = 0x7f07046d;
		public static final int info_software_detail = 0x7f07046e;
		public static final int nonValidOneMessage = 0x7f070538;
		public static final int nonValidTwoMessage = 0x7f070539;
		public static final int sin_datos = 0x7f070672;
		public static final int spei_phone_string = 0x7f0706e4;
		public static final int title_activity_main = 0x7f070700;
	}
	public static final class style {
		public static final int AppBaseTheme = 0x7f0e0000;
		public static final int AppTheme = 0x7f0e0001;
		public static final int mis_cuentas_celda_style = 0x7f0e0066;
	}
	public static final class styleable {
		public static final int[] LoadingImageView = { 0x7f010000, 0x7f010001, 0x7f010002 };
		public static final int LoadingImageView_circleCrop = 2;
		public static final int LoadingImageView_imageAspectRatio = 1;
		public static final int LoadingImageView_imageAspectRatioAdjust = 0;
	}
}
