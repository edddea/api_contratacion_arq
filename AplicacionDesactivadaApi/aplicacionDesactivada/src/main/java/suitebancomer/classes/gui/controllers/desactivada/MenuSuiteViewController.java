package suitebancomer.classes.gui.controllers.desactivada;

import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.desactivada.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.desactivada.ConsultaEstatusAplicacionDesactivadaViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.desactivada.LoginViewController;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.desactivada.ConsultaEstatusAplicacionDesactivadaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.desactivada.LoginDelegate;

import suitebancomer.aplicaciones.bmovil.classes.gui.views.desactivada.FlipAnimation;
import suitebancomer.aplicaciones.bmovil.classes.io.desactivada.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;

import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.delegates.desactivada.MenuSuiteDelegate;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.bancomer.mbanking.desactivada.R;
import com.bancomer.mbanking.desactivada.SuiteAppDesac;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import android.widget.TextView;

public class MenuSuiteViewController extends BaseViewController implements View.OnClickListener, DialogInterface.OnClickListener, AnimationListener {
	
	/**
	 * The layout where the options and the login view are supposed to appear
	 */
	private FrameLayout baseLayout;
	
	/**
	 * The layout containing the suite options
	 */
	//private RelativeLayout menuOptionsLayout;
	private LinearLayout menuOptionsLayout;
	
	/**
	 * Reference to the first button of the option layout
	 */
	private ImageButton firstMenuOption;
	
	/**
	 * Reference to the second button of the option layout
	 */
	private ImageButton secondMenuOption;
	
	/**
	 * Reference to the login view
	 */
	private LoginViewController loginViewController;
	
	/**
	 * Referencia al botón contáctanos
	 */
	private ImageButton contactButton;
	

	/**
	 * Referencia texto versión
	 */
	private TextView txtVersion;
	
	
	private MenuSuiteDelegate delegate;
	
	private boolean isFlipPerforming;
	
	private boolean shouldHideLogin;
	//AMZ
	public BmovilViewsController parentManager;
	//AMZ
	
	public boolean comesFromNov = false;
	
	public void setComesFromNov(final Boolean b){
		comesFromNov = b;
	}
	public void setIsFlipPerforming(final Boolean b){
		isFlipPerforming = b;
	}

	
	
	/**
	 * Default constructor for this activity
	 */
	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState, SHOW_HEADER, R.layout.layout_suite_menu_principal);
		//AMZ
				parentManager = SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController();
			
				TrackingHelper.trackState("menu suite", parentManager.estados);
			
		parentViewsController = SuiteAppDesac.getInstance().getSuiteViewsController();
//		delegate = (MenuSuiteDelegate)parentViewsController.getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
//		delegate.setMenuSuiteViewController(this);
		baseLayout = (FrameLayout)findViewById(R.id.suite_menu_options_layout);
		menuOptionsLayout = (LinearLayout)findViewById(R.id.suite_menu_options_view);
		firstMenuOption = (ImageButton)findViewById(R.id.suite_menu_option_first);
		firstMenuOption.setOnClickListener(this);
		secondMenuOption = (ImageButton)findViewById(R.id.suite_menu_option_second);
		secondMenuOption.setOnClickListener(this);
		contactButton = (ImageButton)findViewById(R.id.suite_contact_button);
		contactButton.setOnClickListener(this);
		txtVersion = (TextView)findViewById(R.id.textVersion);
		txtVersion.setText(SuiteAppDesac.appContext
				.getString(R.string.administrar_app_version)+" "
				+ Tools.getDoubleAmountFromServerString(Constants.APPLICATION_VERSION));
		
		scaleForCurrentScreen();
		
//		delegate.bmovilSelected();
//		menuOptionsLayout.setVisibility(View.GONE);
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		parentViewsController.setCurrentActivityApp(this);
//		BmovilViewsController bmovilViewsController = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
//		bmovilViewsController.setCurrentActivity(this);
		
		delegate = (MenuSuiteDelegate)parentViewsController.getBaseDelegateForKey(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID);
		if (delegate == null) {
			delegate = new MenuSuiteDelegate();
			parentViewsController.addDelegateToHashMap(MenuSuiteDelegate.MENU_SUITE_DELEGATE_ID, delegate);
		}
		delegate.setMenuSuiteViewController(this);
		
		if (loginViewController != null) {
			loginViewController.reestableceLogin();
			loginViewController.getDelegate().setParentViewController(this);
//			loginViewController.setParentManager(bmovilViewsController);
			delegate.startBmovilApp();
			loginViewController.getParentManager().setCurrentActivityApp(this);
			if (shouldHideLogin || !SuiteAppDesac.getInstance().getBmovilApplication().isApplicationLogged()) {
				restableceMenu();
			}
		}
		
		if(null != loginViewController) {
			loginViewController.setVisibility(View.GONE);
		}
//		if(null != contratacionSTViewController)
//			contratacionSTViewController.setVisibility(View.GONE);
		if(null != aplicacionDesactivadaViewController)
			aplicacionDesactivadaViewController.setVisibility(View.GONE);
		if(null != menuOptionsLayout)
			this.menuOptionsLayout.setVisibility(View.VISIBLE);
		else
			if(Server.ALLOW_LOG) Log.w("WTF", "");
		
		areButtonsDisabled = false;
		
		if(delegate.isbMovilSelected()){
//			delegate.bmovilSelected();
		}
		
		areButtonsDisabled = false;
		
		if(comesFromNov){
//			delegate.bmovilSelected();
			//plegarOpcion();
			this.menuOptionsLayout.setVisibility(View.GONE);
			loginViewController.setVisibility(View.VISIBLE);
			comesFromNov = false;
		}
		
		// Acciones de actualizacion
		delegate.cargaTelSeedKeystore();
/*		
		LoginDelegate loginDelegate = (LoginDelegate)(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().getBaseDelegateForKey(LoginDelegate.LOGIN_DELEGATE_ID));
		if (loginDelegate == null) {
			loginDelegate = new LoginDelegate();
			loginDelegate.setLoginViewController(loginViewController);
			loginDelegate.setParentViewController(this);
			loginViewController.setDelegate(loginDelegate);
			SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController().addDelegateToHashMap(LoginDelegate.LOGIN_DELEGATE_ID, loginDelegate);
		}
*/
	}

	@Override
	protected void onPause() {
		super.onPause();
		hideSoftKeyboard();
		
		if (SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController().consumeAccionesDePausa()) {
			parentViewsController.currentViewControllerApp.hideCurrentDialog();
		}
		parentViewsController.currentViewControllerApp.hideCurrentDialog();
		
//		setShouldHideLogin(true);
//		restableceMenu();
	}
	
	/**
	 * Method that listens whenever a view is touched (clicked)
	 */
	@Override
	public void onClick(final View v) {
		if(areButtonsDisabled)
			return;
		areButtonsDisabled = true;
		if (v == firstMenuOption) {
//			delegate.bmovilSelected();
		} else if (v == secondMenuOption) {
//			delegate.onBtnContinuarclick(v);
		} else if (v == contactButton) {
			//ARR Boton Contactanos
			Map<String,Object> paso1OperacionMap = new HashMap<String, Object>();
			//ARR
			paso1OperacionMap.put("evento_paso1", "event46");
			paso1OperacionMap.put("&&products", "menu suite");
			paso1OperacionMap.put("eVar12", "paso1:contáctanos");

			TrackingHelper.trackPaso1Operacion(paso1OperacionMap);
			((SuiteViewsController)parentViewsController).showContactanos();
		}
}
	
//	public void mostrarOpcion() {
//		if (isFlipPerforming) {
//			return;
//		}
//		isFlipPerforming = true;
//		FlipAnimation flipAnimation = new FlipAnimation(menuOptionsLayout, loginViewController);
//		flipAnimation.setAnimationListener(this);
//	    baseLayout.startAnimation(flipAnimation);
//	}
	
	public void setButtonsDisabled(final boolean value) {
		this.areButtonsDisabled = value;
	}
	
	public void plegarOpcion() {
		if (isFlipPerforming) {
			return;
		}
		isFlipPerforming = true;
		FlipAnimation flipAnimation = new FlipAnimation(menuOptionsLayout, loginViewController);
		flipAnimation.setAnimationListener(this);
	    if (menuOptionsLayout.getVisibility() == View.GONE) {
	        flipAnimation.reverse();
	    }
	    baseLayout.startAnimation(flipAnimation);
	}
	
	private void llamarLineaBancomer(final String numeroTel) {
		delegate.llamarLineaBancomer(numeroTel);
	}
	
	public FrameLayout getBaseLayout() {
		return baseLayout;
	}
	
	public LoginViewController getLoginViewController() {
		return loginViewController;
	}
	
	public void setLoginViewController(final LoginViewController loginViewController) {
		this.loginViewController = loginViewController;
	}
	
	public boolean getShouldHideLogin() {
		return shouldHideLogin;
	}
	
	public void setShouldHideLogin(final boolean shouldHideLogin) {
		this.shouldHideLogin = shouldHideLogin;
	}

	@Override
	public void onClick(final DialogInterface dialog,final int which) {
		if (which == Dialog.BUTTON_POSITIVE) {
			llamarLineaBancomer(getString(R.string.menuSuite_callLocalNumber));
		} else if (which == Dialog.BUTTON_NEUTRAL) {
			llamarLineaBancomer(getString(R.string.menuSuite_callAwayNumber));
		}
		areButtonsDisabled = false;
	}

	/**
	 * Stores the session information after a login operation. If registering
	 * is taking place, it show the activation screen instead.
	 *
	 * @param response The server response to the login operation
	 */
    public void processNetworkResponse(final int operationId,final ServerResponse response) {
		if(Server.CONSULTA_MANTENIMIENTO == operationId) {
			ConsultaEstatusAplicacionDesactivadaDelegate ceadDelegate = (ConsultaEstatusAplicacionDesactivadaDelegate)SuiteAppDesac
					.getInstance()
					.getBmovilApplication()
					.getBmovilViewsController()
					.getBaseDelegateForKey(ConsultaEstatusAplicacionDesactivadaDelegate.CONSULTA_APLICACION_DESACTIVADA_DELEGATE_ID);
			
			if(null == ceadDelegate) {
				ceadDelegate = new ConsultaEstatusAplicacionDesactivadaDelegate();
				SuiteAppDesac
    				.getInstance()
					.getBmovilApplication()
					.getBmovilViewsController()
					.addDelegateToHashMap(ConsultaEstatusAplicacionDesactivadaDelegate.CONSULTA_APLICACION_DESACTIVADA_DELEGATE_ID, ceadDelegate);
				ceadDelegate.setOwnerController(this);
			}
			
			ceadDelegate.analyzeResponse(operationId, response);
		} else if(operationId == Server.LOGIN_OPERATION) {
			LoginDelegate loginDelegate = (LoginDelegate)((SuiteAppDesac)getApplication()).getBmovilApplication().getBmovilViewsController().getBaseDelegateForKey(LoginDelegate.LOGIN_DELEGATE_ID);
			if (loginDelegate == null) {
				loginDelegate = new LoginDelegate();
				
				loginDelegate.setParentViewController(this);
				loginViewController.setDelegate(loginDelegate);
				SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController().addDelegateToHashMap(LoginDelegate.LOGIN_DELEGATE_ID, loginDelegate);
			}
			loginDelegate.setLoginViewController(loginViewController);
			loginDelegate.analyzeResponse(operationId, response);
		} else if(operationId == Server.CAMBIA_PERFIL) {
			BmovilViewsController viewsController = SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController();
//			CambioPerfilDelegate cpDelegate = (CambioPerfilDelegate)viewsController.getBaseDelegateForKey(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);
//			if(null == cpDelegate)
//				cpDelegate = new CambioPerfilDelegate();
//			else
//				viewsController.removeDelegateFromHashMap(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);
//			
//			cpDelegate.setChangeAdvancedToBasic(true);
//			cpDelegate.analyzeResponse(operationId, response);
		}
    }
    
    public void restableceMenu() {
    	if (loginViewController != null) {
			loginViewController.setVisibility(View.GONE);
		}
//    	if(contratacionSTViewController != null) {
//    		contratacionSTViewController.setVisibility(View.GONE);
//    		baseLayout.removeView(contratacionSTViewController);
//    		contratacionSTViewController = null;
//    	}
    	if(aplicacionDesactivadaViewController != null) {
    		aplicacionDesactivadaViewController.setVisibility(View.GONE);
    		baseLayout.removeView(aplicacionDesactivadaViewController);
    		aplicacionDesactivadaViewController = null;
    	}
    	menuOptionsLayout.setVisibility(View.VISIBLE);
    	
    }
    
//    @Override
//	public void goBack() {
//    	delegate.onBackPressed();
//    }
    
	@Override
	public void onAnimationEnd(final Animation animation) {
		FlipAnimation flipAnimation = (FlipAnimation)animation;
		if (!flipAnimation.isForward()) {
			if(null != loginViewController) {
				baseLayout.removeView(loginViewController);
				setLoginViewController(null);
			} 
//			if(null != contratacionSTViewController) {
//				baseLayout.removeView(contratacionSTViewController);
//				setLoginViewController(null);
//			}
			if(null != aplicacionDesactivadaViewController) {
				baseLayout.removeView(aplicacionDesactivadaViewController);
				setLoginViewController(null);
			}
		}
		isFlipPerforming = false;
		areButtonsDisabled = false;
	}

	@Override
	public void onAnimationRepeat(final Animation animation) {
		isFlipPerforming = true;
	}

	@Override
	public void onAnimationStart(final Animation animation) {
		isFlipPerforming = true;
	}

	/**
	 * Establece la l�gica a seguir para el redimensionamiento de la pantalla.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scale(baseLayout);
		guiTools.scale(menuOptionsLayout);
		guiTools.scale(firstMenuOption);
		guiTools.scale(secondMenuOption);
		guiTools.scale(findViewById(R.id.suite_advertising_image));
		guiTools.scale(contactButton);
	}
	
	// #region Softtoken
//	private void softtokenSelected() {
//		delegate.softtokenSelected();
//	}
	
//	private ContratacionSofttokenViewController contratacionSTViewController;
//	
//	public void setContratacionSTViewController(ContratacionSofttokenViewController contratacionSTViewController) {
//		this.contratacionSTViewController = contratacionSTViewController;
//	}
//	
//	public ContratacionSofttokenViewController getContratacionSTViewController() {
//		return contratacionSTViewController;
//	}
	
//	public void plegarOpcionST() {
//		if (isFlipPerforming) {
//			return;
//		}
//		isFlipPerforming = true;
////		FlipAnimation flipAnimation = new FlipAnimation(menuOptionsLayout, contratacionSTViewController);
////		flipAnimation.setAnimationListener(this);
//	    if (menuOptionsLayout.getVisibility() == View.GONE) {
//	        flipAnimation.reverse();
//	    }
//	    baseLayout.startAnimation(flipAnimation);
//	}
	// #endregion
	
	// #region Consulta Estatus Aplicación Desactivada.
	private ConsultaEstatusAplicacionDesactivadaViewController aplicacionDesactivadaViewController;
	
	public void setAplicacionDesactivadaViewController(final ConsultaEstatusAplicacionDesactivadaViewController aplicacionDesactivadaViewController) {
		this.aplicacionDesactivadaViewController = aplicacionDesactivadaViewController;
	}
	
	
	public ConsultaEstatusAplicacionDesactivadaViewController getAplicacionDesactivadaViewController() {
		return aplicacionDesactivadaViewController;
	}
	
	public void plegarOpcionAplicacionDesactivada() {
		if (isFlipPerforming) {
			return;
		}
		isFlipPerforming = true;
		FlipAnimation flipAnimation = new FlipAnimation(menuOptionsLayout, aplicacionDesactivadaViewController);
		flipAnimation.setAnimationListener(this);
	    if (menuOptionsLayout.getVisibility() == View.GONE) {
	        flipAnimation.reverse();
	    }
	    baseLayout.startAnimation(flipAnimation);
	}
	// #endregion
	
	private boolean areButtonsDisabled = false;
}

