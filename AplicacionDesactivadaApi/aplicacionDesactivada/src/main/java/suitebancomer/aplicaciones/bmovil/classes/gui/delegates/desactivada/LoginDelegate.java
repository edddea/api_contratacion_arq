package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.desactivada;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import bancomer.api.common.commons.Constants.TYPE_SOFTOKEN;
import bancomer.api.common.commons.Constants.Perfil;
import suitebancomercoms.aplicaciones.bmovil.classes.common.CatalogoAutenticacionFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.SessionStoredListener;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.desactivada.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.desactivada.LoginViewController;
import suitebancomer.aplicaciones.bmovil.classes.io.desactivada.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Catalog;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.LoginData;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomer.classes.gui.controllers.desactivada.BaseViewController;
import suitebancomer.classes.gui.controllers.desactivada.MenuSuiteViewController;
import suitebancomer.classes.gui.controllers.desactivada.SuiteViewsController;
import suitebancomer.classes.gui.delegates.desactivada.BaseDelegate;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.bancomer.mbanking.desactivada.R;
import com.bancomer.mbanking.desactivada.SuiteAppDesac;

public class LoginDelegate extends BaseDelegate implements
		SessionStoredListener {
	private final static String STORE_SESSION_LOGIN = "sessionLogin";
	private final static String STORE_SESSION_ACTIVATION = "sessionActivation";
	private final static String STORE_SESSION_PROFILE_CHANGE = "sessionProfileChange";

	public final static long LOGIN_DELEGATE_ID = 0xfad618f768f1e325L;

	private LoginViewController loginViewController;

	private BaseViewController parentViewController;

	private ServerResponse loginResponse;
	private LoginData loginData;
	private String storeSessionType;
	private String currentPassword;
	//ARR
		//Variable para almacenar el nombre de usuario
		private static String nombreUsuario;
		
		//ARR
		//Variable para usar en la identificacion de usuario en el Login. Para saltarsela o no si la aplicacion se inicia desde cero o no
		public static boolean encriptacionEnLogin = false;
	
	//Cátalogos Alejandra: Acyualiza el catalogo de autenticación si no coinciden el numero de operaciones
	//private ArrayList<OperacionAutenticacion> operaciones;
	
	/**
	 * Objeto ConsultaEstatus obtenido de ConsultaEstatusMantenimiento o de
	 * Login.
	 */
	private ConsultaEstatus consultaEstatus;

	public LoginViewController getLoginViewController() {
		return loginViewController;
	}

	public void setLoginViewController(final LoginViewController loginViewController) {
		this.loginViewController = loginViewController;
	}

	public void setParentViewController(final BaseViewController parentViewController) {
		this.parentViewController = parentViewController;
	}


	//ARR 
	public static String getNombreUsuario()
	{
		return nombreUsuario;
	}

	/**
	 * Establece el usuario y desactiva el campo si ya existe en sesión, si no
	 * lo activa
	 * 
	 * @param mUserEdit
	 *            el campo de texto usuario
	 */
	public void establecerUsuario(final EditText mUserEdit,final TextView mEraseData) {
		Session session = Session.getInstance(SuiteAppDesac.appContext);
		String user = session.getUsername();
		mUserEdit.setText("");
		mUserEdit.setEnabled(true);
		mUserEdit.setEnabled(true);
		mUserEdit.setFocusable(true);
		mUserEdit.setFocusableInTouchMode(true);
		mUserEdit.setClickable(true);
		if ((user != null && user.length() > 0)
				&& session.isApplicationActivated()) {
			mUserEdit.setText(Tools.hideUsername(user));
			mUserEdit.setEnabled(false);
			mUserEdit.setFocusable(false);
			mUserEdit.setClickable(false);
			mEraseData.setVisibility(View.VISIBLE);
			mEraseData.setClickable(true);
		} else {
			if (mUserEdit.getText().toString().indexOf("*") != -1) {
				mUserEdit.setText("");
				mUserEdit.setEnabled(true);
				mUserEdit.setFocusable(true);
				mUserEdit.setFocusableInTouchMode(true);
				mUserEdit.setClickable(true);
				mUserEdit.requestFocus();
			}
			mEraseData.setVisibility(View.GONE);
			mEraseData.setClickable(false);
		}

		mEraseData.setVisibility(View.GONE);
	}

	public void botonEntrar(final String user,final String pass) {
		if (validarCampos(user, pass)) {
			if (Session.getInstance(SuiteAppDesac.appContext)
					.isApplicationActivated()) {
				login(user, pass);
			} else {
				registrarAplicacion(user, pass);
			}
			//ARR
			encriptacionEnLogin = true;
			//ARR
			nombreUsuario = Session.getInstance(SuiteAppDesac.appContext).getUsername();
		}
	}

	/**
	 * Validate the data introduced before triggering the login process
	 * 
	 * @param userCellPhone
	 *            the user cellphone
	 * @param userPass
	 *            the user password
	 */
	public boolean validarCampos(final String userCellPhone,final String userPass) {
		if(Server.ALLOW_LOG) Log.d("LoginDelegate", "Aquí se debe validar los datos de acceso");

		Session session = Session.getInstance(SuiteAppDesac.appContext);
		/* Validate user field */
		if (userCellPhone.length() == 0) {
			parentViewController
					.showInformationAlert(R.string.login_usernameCannotBeEmpty);
			return false;
		} else if (userCellPhone.length() < Constants.TELEPHONE_NUMBER_LENGTH) {
			parentViewController
					.showInformationAlert(R.string.login_usernameTooShort);
			return false;
		}

		/* Validate password field */
		if (userPass.length() == 0) {
			parentViewController
					.showInformationAlert(session.isApplicationActivated() ? R.string.error_passwordCannotBeEmptyForLogin
							: R.string.error_passwordCannotBeEmptyForActivation);
			return false;
		} else if (userPass.length() < Constants.PASSWORD_LENGTH) {
			parentViewController
					.showInformationAlert(R.string.error_passwordTooShort);
			if (loginViewController != null)
				loginViewController.limpiarCampoContrasena();
			return false;
		}

		return true;
	}

	public void registrarAplicacion(final String username,final String password) {
		Session session = Session.getInstance(SuiteAppDesac.appContext);
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put(Server.USERNAME_PARAM, username);
		paramTable.put(Server.PASSWORD_PARAM, password);
		long seed = session.getSeed();
		if (seed == 0) {
			seed = System.currentTimeMillis();
			session.setSeed(seed);
		}
		String ium = Tools.buildIUM(username, seed, SuiteAppDesac.appContext);
		session.setIum(ium);
		session.setUsername(username);
		session.setPassword(password);
		paramTable.put(ServerConstants.IUM_ETIQUETA, ium);

		String marcaModelo = Build.BRAND + Build.MODEL;
		paramTable.put(Server.MARCA_MODELO, marcaModelo);

		doNetworkOperation(Server.ACTIVATION_STEP1_OPERATION, paramTable,
				parentViewController, false);
	}

	public void activarAplicacion(final String activationCode) {
		final Session session = Session.getInstance(SuiteAppDesac.appContext);
		if (!session.isApplicationActivated()) {
			String password = session.getPassword();
			String errorMsg = null;

			/* Validate activation code field */
			if (activationCode.length() == 0) {
				errorMsg = SuiteAppDesac.appContext
						.getString(R.string.activation_activationCodeCannotBeEmpty);
			} else if (activationCode.length() < Constants.ACTIVATION_CODE_LENGTH) {
				errorMsg = SuiteAppDesac.appContext.getString(
						R.string.activation_activationCodeTooShort,
						Constants.ACTIVATION_CODE_LENGTH);
			}

			/* If validation failed then send an error message */
			if (errorMsg != null) {
				parentViewController.showInformationAlert(errorMsg);
				return;
			}

			String ium = session.getIum();
			session.setActivationCode(activationCode);

			// prepare parameters
			Hashtable<String, String> paramTable = new Hashtable<String, String>();
			paramTable.put(Server.USERNAME_PARAM, session.getUsername());
			paramTable.put(Server.PASSWORD_PARAM, password);
			paramTable.put(Server.ACTIVATION_CODE_PARAM, activationCode);
			paramTable.put(ServerConstants.IUM_ETIQUETA, ium);

			// performs the network operation
			doNetworkOperation(Server.ACTIVATION_STEP2_OPERATION, paramTable,
					parentViewController, false);
		} else {
			storeSessionType = STORE_SESSION_LOGIN;
			parentViewController.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					login(session.getUsername(), session.getPassword());
				}
			});
		}
	}

	public void login(final String userCellPhone,final String password) {
		currentPassword = password;
		Session session = Session.getInstance(SuiteAppDesac.appContext);

		String username = session.getUsername();
		// String via = session.getVia();

		session.setPassword(password);

		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put(Server.USERNAME_PARAM, username);
		paramTable.put(Server.PASSWORD_PARAM, password);

		String ium = session.getIum();
		if (Tools.isEmptyOrNull(ium)) {
			ium = Tools.buildIUM(username, session.getSeed(),
					SuiteAppDesac.appContext);
			session.setIum(ium);
		}
		session.setUsername(username);

		paramTable.put(ServerConstants.IUM_ETIQUETA, ium);
		String marcaModelo = Build.BRAND + Build.MODEL;
		paramTable.put(Server.MARCA_MODELO, marcaModelo);

		String[] catalogVersions = session.getCatalogVersions();
		Catalog[] catalogos = session.getCatalogs();

		if ((catalogVersions != null) && (catalogVersions.length == 8)) {// solo
																			// son
																			// 8
			paramTable.put(Server.VERSION_C1_PARAM, catalogVersions[0]);
			paramTable.put(Server.VERSION_C4_PARAM, catalogVersions[1]);
			paramTable.put(Server.VERSION_C5_PARAM, catalogVersions[2]);
			paramTable.put(Server.VERSION_C8_PARAM, catalogVersions[3]);
			paramTable.put(Server.VERSION_TA_PARAM, catalogVersions[4]);
			paramTable.put(Server.VERSION_DM_PARAM, catalogVersions[5]);
			paramTable.put(Server.VERSION_SV_PARAM, catalogVersions[6]);
			paramTable.put("MS", catalogVersions[7]);
		}

		if (catalogos[0] == null || catalogos[1] == null
				|| catalogos[2] == null || catalogos[3] == null) { // Solo 4
																	// catalgos
																	// genericos
			if(Server.ALLOW_LOG) Log.d("Catálogo 0 nulo", "Catálogo 0 nulo");
			paramTable.put(Server.VERSION_C1_PARAM, "0");
			paramTable.put(Server.VERSION_C4_PARAM, "0");
			paramTable.put(Server.VERSION_C5_PARAM, "0");
			paramTable.put(Server.VERSION_C8_PARAM, "0");
		}

		if (session.getCatalogoTiempoAire() == null) {
			paramTable.put(Server.VERSION_TA_PARAM, "0");
		}

		if (session.getCatalogoDineroMovil() == null) {
			paramTable.put(Server.VERSION_DM_PARAM, "0");
		}

		if (session.getCatalogoServicios() == null) {
			paramTable.put(Server.VERSION_SV_PARAM, "0");
		}
		
		if (session.getCatalogoMantenimientoSPEI() == null) {
			paramTable.put("MS", "0");
		}
		paramTable.put(Server.VERSION_AU_PARAM,
				CatalogoAutenticacionFileManager.getCurrent().leerVersionArchivoCatalogoAutenticacion());
		
		//ARR 3/07
    	//ARR
		Map<String,Object> eventoLogin = new HashMap<String, Object>();
		
		eventoLogin.put("evento_login", "event22");
		
		TrackingHelper.trackClickLogin(eventoLogin);
		doNetworkOperation(Server.LOGIN_OPERATION, paramTable,
				parentViewController, true);
	
	}

	public boolean isBmovilAppActivated() {
		boolean actFlag = Session.getInstance(SuiteAppDesac.appContext)
				.isApplicationActivated();
		if(Server.ALLOW_LOG) Log.d(this.getClass().getName(), "isBmovilAppActivated = " + actFlag);

		return actFlag;
	}

	public void consultaEstatusCliente() {
		Session session = Session.getInstance(SuiteAppDesac.appContext);
		String username = session.getUsername();

		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put(Server.USERNAME_PARAM, username);

		doNetworkOperation(Server.CONSULTA_ESTATUSSERVICIO, paramTable,
				parentViewController, false);
	}

	public void doNetworkOperation(final int operationId,
			final Hashtable<String, ?> params, final BaseViewController caller,
			final boolean callerHandlesError) {
		SuiteAppDesac.getInstance()
				.getBmovilApplication()
				.invokeNetworkOperation(operationId, params, caller,
						callerHandlesError);
	}

	public void analyzeResponse(final int operationId,final ServerResponse response) {
		
		Session session = Session.getInstance(SuiteAppDesac.appContext);
		
		if (operationId == Server.LOGIN_OPERATION) {
			// Stores the session and goes to the menu screen
			// Now validates status
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL
					|| response.getStatus() == ServerResponse.OPERATION_OPTIONAL_UPDATE) {
				LoginData data = (LoginData) response.getResponse();

				consultaEstatus = new ConsultaEstatus();
				llenarConsultaEstatus(data);

				Session.getInstance(SuiteAppDesac.appContext).setSecurityInstrument(
						data.getInsSeguridadCliente());

				final BmovilViewsController bmovilViewsController = SuiteAppDesac
						.getInstance().getBmovilApplication()
						.getBmovilViewsController();

				if (data.getEstatusServicio().equals(
						Constants.STATUS_APP_ACTIVE)) {
					loginResponse = response;
					loginData = (LoginData) response.getResponse();

					storeSessionAfterLogin(response);
					// Calendar serverDateTime =
					// Tools.formatDateTimeFromServer(loginData.getServerDate(),
					// loginData.getServerTime());
					// Long timeElapsed =
					// validaFechaDeActivacion(serverDateTime);
					// if(-1 == timeElapsed) {
					// storeSessionAfterLogin(response);
					// } else {
					// String str =
					// SuiteApp.appContext.getString(R.string.login_wait_30_min);
					// String minuteStr = "";
					//
					// // if(timeElapsed < 10)
					// // minuteStr = "0" + String.valueOf(timeElapsed);
					// // else
					// // minuteStr = String.valueOf(timeElapsed);
					//
					// if(timeElapsed < 10)
					// minuteStr += "0";
					// minuteStr += String.valueOf(timeElapsed);
					//
					// str = String.format(str, minuteStr);
					// parentViewController.showInformationAlert(str);
					// }
				} else if (data.getEstatusServicio().equals(
						Constants.STATUS_PENDING_ACTIVATION)
						|| data.getEstatusServicio().equals(
								Constants.STATUS_PENDING_SEND)) {
					
				
					
					// Eliminacion 2x1
					if (session.getSecurityInstrument().equals("S1")){
						
						procesadoEscenarioAlterno25(session,data);
						
					}else{
					
					
//						bmovilViewsController.showActivacion(consultaEstatus,
//								currentPassword, true);
//					
					}
					
					
					
				} else if (data.getEstatusServicio().equals(
						Constants.STATUS_PASS_BLOCKED)) {
//					bmovilViewsController.showDesbloqueo(consultaEstatus);
				} else if (data.getEstatusServicio().equals(
						Constants.STATUS_APP_SUSPENDED)) {
//					bmovilViewsController.showQuitarSuspension(consultaEstatus);
				} else if (data.getEstatusServicio().equals(
						Constants.STATUS_USER_CANCELED)
						|| data.getEstatusServicio().equals(
								Constants.STATUS_BANK_CANCELED)) {
					String fecha = Tools.invertDateOrder(data
							.getFechaContratacion());
					if (fecha.equals(data.getServerDate()))
						parentViewController
								.showInformationAlert(parentViewController
										.getString(R.string.bmovil_estatus_aplicacion_desactivada_error_fechas_iguales));
//					else
//						bmovilViewsController.showContratacion(consultaEstatus,
//								data.getEstatusServicio(), true);
				} else if (data.getEstatusServicio().equals(
						Constants.STATUS_CLIENT_NOT_FOUND)
						|| data.getEstatusServicio().equals(
								Constants.STATUS_ENGAGEMENT_UNCOMPLETE)) {
//					bmovilViewsController.showContratacion(consultaEstatus,
//							data.getEstatusServicio(), true);
				} else if (data.getEstatusServicio().equals(
						Constants.STATUS_WRONG_IUM)) {
	
					// Eliminacion 2x1
					if (session.getSecurityInstrument().equals("S1")){
						
						procesadoEscenarioAlterno25(session,data);
						
					}else{
					
						parentViewController.showInformationAlert(
							R.string.bmovil_activacion_alerta_reactivar,
							new OnClickListener() {
								@Override
								public void onClick(final DialogInterface dialog,
										final int which) {
									
									
//									bmovilViewsController.showReactivacion(
//											consultaEstatus, currentPassword);
								}
							});
					}
					
					
				} else {
					parentViewController
							.showErrorMessage(R.string.error_communications);
				}
				// } else if (response.getStatus() ==
				// ServerResponse.OPERATION_ERROR) {
			} else {
				if (response
						.getMessageCode()
						.equals(SuiteAppDesac.appContext
								.getString(R.string.menuSuite_update_mandatoryCode))) {
					showUpdateConfirmationAlert(response.getMessageText(),
							true, response.getUpdateURL());
				} else {
					parentViewController.showInformationAlert(response
							.getMessageText());
					if (loginViewController != null) {
						// MenuSuiteViewController menuViewController =
						// (MenuSuiteViewController) SuiteApp
						// .getInstance().getSuiteViewsController()
						// .getCurrentViewController();
						// menuViewController.restableceMenu();
						loginViewController.limpiarCampoContrasena();
						// loginViewController.reestableceLogin();
					}
				}

			}

		} /*
		 * else if (operationId == Server.CONSULTA_ESTATUSSERVICIO) {
		 * EstatusDelServicioData data =
		 * (EstatusDelServicioData)response.getResponse();
		 * System.out.println("Resultado del servicio " +
		 * data.getStatusCliente());
		 * Session.getInstance(SuiteApp.appContext).setServiceStatus
		 * (data.getStatusCliente()); if
		 * (data.getStatusCliente().equals(Constants.STATUS_APP_ACTIVE)) {
		 * Session
		 * .getInstance(SuiteApp.appContext).setClientProfile(data.getPerfilCliente
		 * ());
		 * Session.getInstance(SuiteApp.appContext).setSecurityInstrument(data
		 * .getInstrumentoCliente());
		 * 
		 * BmovilViewsController bmovilViewsController =
		 * SuiteApp.getInstance().getBmovilApplication
		 * ().getBmovilViewsController(); bmovilViewsController.showLogin(); }
		 * else if
		 * (data.getStatusCliente().equals(Constants.STATUS_PENDING_ACTIVATION)
		 * || data.getStatusCliente().equals(Constants.STATUS_PENDING_SEND)) {
		 * SuiteApp
		 * .getInstance().getSuiteViewsController().getCurrentViewController
		 * ().showErrorMessage(R.string.login_statusReactivationMessage); } else
		 * if (data.getStatusCliente().equals(Constants.STATUS_PASS_BLOCKED)) {
		 * SuiteApp
		 * .getInstance().getSuiteViewsController().getCurrentViewController
		 * ().showErrorMessage(R.string.login_statusReactivationMessage); } else
		 * if (data.getStatusCliente().equals(Constants.STATUS_APP_SUSPENDED)) {
		 * SuiteApp
		 * .getInstance().getSuiteViewsController().getCurrentViewController
		 * ().showErrorMessage("Servicio suspendido"); } else if
		 * (data.getStatusCliente().equals(Constants.STATUS_BANK_CANCELED) ||
		 * data.getStatusCliente().equals(Constants.STATUS_USER_CANCELED)) {
		 * SuiteApp
		 * .getInstance().getSuiteViewsController().getCurrentViewController
		 * ().showErrorMessage("Realizar contratacion"); } }
		 */else if (operationId == Server.ACTIVATION_STEP1_OPERATION) {
			BmovilViewsController bmovilViewsController = SuiteAppDesac
					.getInstance().getBmovilApplication()
					.getBmovilViewsController();
//			bmovilViewsController.showPantallaActivacion();
		} else if (operationId == Server.ACTIVATION_STEP2_OPERATION) {
			SuiteViewsController suiteViewsController = SuiteAppDesac.getInstance()
					.getSuiteViewsController();
			if (suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController) {
				MenuSuiteViewController suiteViewController = (MenuSuiteViewController) suiteViewsController
						.getCurrentViewControllerApp();
				suiteViewController.restableceMenu();
			}
			storeSessionAfterActivation();
		}
	}



	private void procesadoEscenarioAlterno25(final Session session,final LoginData data) {
		
		
	/*	PropertiesManager.getCurrent().setSofttokenActivated(true);
		PropertiesManager.getCurrent().setBmovilActivated(true);
		
		DatosBmovilFileManager.getCurrent().setActivado(true);
		DatosBmovilFileManager.getCurrent().setSeed("1428588487931");
		DatosBmovilFileManager.getCurrent().setSoftoken(true);
		*/
	

		String estadoIs = data.getEstatusIS();
		session.setEstatusIS(estadoIs);
		

		//String estadoIsSession = session.getEstatusIS();
		
		//para probar solo
		//estadoIs="T3";
		if (estadoIs.equals("A1")){
			
			
//			if (session.isSofttokenActivado()){
//				
//				
//				
//				final BmovilViewsController bmovilViewsController = SuiteAppDesac
//						.getInstance().getBmovilApplication()
//						.getBmovilViewsController();
//
//				parentViewController.showInformationAlert(
//						R.string.bmovil_activacion_alerta_reactivar,
//						new OnClickListener() {
//							@Override
//							public void onClick(DialogInterface dialog,
//									int which) {
//								
//								Log.d(">> CGI-Nice-Ppl", "[EA#25] Entra en OK del alert");
//								
//								Log.d(">> CGI-Nice-Ppl", "[EA#25] Genera clase GeneraOTPSTDelegate");
//                                GeneraOTPSTDelegate generaOTPSTDelegate = new GeneraOTPSTDelegate();
//                                Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra el token");
//                                if (generaOTPSTDelegate.borraToken()) {
//                                    Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra el token >> true");
//                                    PropertiesManager.getCurrent().setSofttokenActivated(false);
//                                }
//                                
//                                PropertiesManager.getCurrent().setBmovilActivated(false);
//                                Log.d(">> CGI-Nice-Ppl", "[EA#25] BmovilActivated >> false");
//                                
//                                Log.d(">> CGI-Nice-Ppl", "[EA#25] DatosBMovil Borrado Inicio - Softoken>"+DatosBmovilFileManager.getCurrent().getSoftoken()+" - Activado>"+DatosBmovilFileManager.getCurrent().getActivado()+ " - Seed>"+DatosBmovilFileManager.getCurrent().getSeed());
//								DatosBmovilFileManager.getCurrent().setSoftoken(false);
//                                DatosBmovilFileManager.getCurrent().setActivado(false);
//                                DatosBmovilFileManager.getCurrent().setSeed("");
//                                Log.d(">> CGI-Nice-Ppl", "[EA#25] DatosBMovil Borrado Fin - Softoken>"+DatosBmovilFileManager.getCurrent().getSoftoken()+" - Activado>"+DatosBmovilFileManager.getCurrent().getActivado()+ " - Seed>"+DatosBmovilFileManager.getCurrent().getSeed());
//                                
//                                Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra Session Inicio");
//                                session.setSeed(0);
//                                session.setIum(null);
//                                session.saveCatalogRecordStore();
//                                Log.d(">> CGI-Nice-Ppl", "[EA#25] Borra Session Fin");
//                                
//                                //aqui solo debe borrarse el archivo bmovil y el de softoken
////                                bmovilViewsController.showAutenticacionSoftoken(consultaEstatus, currentPassword);
//                                
//							}
//						});
//				
//				
//				
//			}else {
			
				procesadoEscenarioAlterno27( session, data);
			}
			
			
//		}else {
//			
//			procesadoEscenarioAlterno26( session, data);
//			
//		}
		
		
		
	}

	private void procesadoEscenarioAlterno26(final Session session,final LoginData data) {

		parentViewController.showInformationAlert(R.string.alert_estado_instrumento_distintoA1, plegarOpcion());

	}

	private OnClickListener plegarOpcion() {
		
		OnClickListener listener = new OnClickListener() {
			@Override
			public void onClick(final DialogInterface dialog,final int which) {
				dialog.dismiss();
				loginViewController.getMenuSuiteViewController().plegarOpcion();
			}
		};

		return listener;
	}

	
	private void procesadoEscenarioAlterno27(final Session session,final LoginData data) {
		
		
		final BmovilViewsController bmovilViewsController = SuiteAppDesac
				.getInstance().getBmovilApplication()
				.getBmovilViewsController();

		parentViewController.showInformationAlert(
				R.string.bmovil_activacion_alerta_reactivar,
				new OnClickListener() {
					@Override
					public void onClick(final DialogInterface dialog,
							final int which) {

						PropertiesManager.getCurrent().setBmovilActivated(false);
						DatosBmovilFileManager.getCurrent().setActivado(false);
						DatosBmovilFileManager.getCurrent().setSeed("0");

						//aqui solo debe borrarse el archivo bmovil
//						bmovilViewsController.showAutenticacionSoftoken(
//								consultaEstatus, currentPassword);
					}
				});

	}
	
	
	
	
	
	
	
	
	/**
	 * Llena el objeto consulta estatus con los datos obtenidos de la respuesta
	 * de NACAR.
	 * 
	 * @param data
	 *            La respuesta de NACAR.
	 */
	private void llenarConsultaEstatus(final LoginData data) {
		consultaEstatus.setCompaniaCelular(data.getCompaniaTelCliente());
		consultaEstatus.setEmailCliente(data.getEmailCliente());
		consultaEstatus.setEstatus(data.getEstatusServicio());
		consultaEstatus.setEstatusInstrumento(data.getEstatusInstrumento());
		consultaEstatus.setInstrumento(data.getInsSeguridadCliente());
		consultaEstatus.setNombreCliente("");
		consultaEstatus.setNumCelular(Session.getInstance(SuiteAppDesac.appContext)
				.getUsername());
		consultaEstatus.setNumCliente(data.getClientNumber());
		consultaEstatus.setPerfilAST(data.getPerfiCliente());
		consultaEstatus.setPerfil(Tools.determinaPerfil(
				consultaEstatus.getPerfilAST(),
				consultaEstatus.getInstrumento(),
				consultaEstatus.getEstatusInstrumento()));
		consultaEstatus.setEstatusAlertas(data.getEstatusAlertas());
		
		//Guardamos EA (EsatusAlertas) para EA11# p026 Transferir BancomerNFR 
		
		Session session = Session.getInstance(SuiteAppDesac.appContext);
		session.setEstatusAlertas(data.getEstatusAlertas());

		if(Server.ALLOW_LOG) Log.d("ConsultaEstatus", consultaEstatus.toString());
	}

	/**
	 * Store session after login
	 * 
	 * @param response
	 *            the server response
	 */
	private void storeSessionAfterLogin(final ServerResponse response) {
		SuiteAppDesac.getInstance().getBmovilApplication()
				.setApplicationLogged(true);
		loginResponse = response;
		loginData = (LoginData) response.getResponse();
		final Session session = Session.getInstance(SuiteAppDesac.appContext);
		session.setClientNumber(loginData.getClientNumber());
		session.setAccounts(loginData.getAccounts(), loginData.getServerDate(),
				loginData.getServerTime());
		session.setEmail(loginData.getEmailCliente());

		session.setNombreCliente(Tools.isEmptyOrNull(loginData
				.getNombreCliente()) ? "" : loginData.getNombreCliente());
		if (loginData.getPerfiCliente().equals(Constants.PROFILE_ADVANCED_03)) {
			session.setClientProfile(Perfil.avanzado);
		} else if (loginData.getPerfiCliente().equals(Constants.PROFILE_RECORTADO_02)) {
			session.setClientProfile(Perfil.recortado);
		}else if (loginData.getPerfiCliente().equals(Constants.PROFILE_BASIC_01) ||loginData.getPerfiCliente().equals(Constants.PROFILE_BASIC_00) ){
			session.setClientProfile(Perfil.basico);
		}

		session.setCompaniaUsuario(loginData.getCompaniaTelCliente());
		session.setSecurityInstrument(loginData.getInsSeguridadCliente());
		session.setEstatusIS(loginData.getEstatusIS());
		session.setAuthenticationJson(loginData.getAuthenticationJson());
		if (loginData.getCatalogoTiempoAire() != null) {
			session.updateCatalogoTiempoAire(loginData.getCatalogoTiempoAire());
		}
		if (loginData.getCatalogoDineroMovil() != null) {
			session.updateCatalogoDineroMovil(loginData
					.getCatalogoDineroMovil());
		}
		if (loginData.getCatalogoServicios() != null) {
			session.updateCatalogoServicios(loginData.getCatalogoServicios());
		}
		if (loginData.getCatalogoMantenimientoSPEI() != null) {
			session.updateCatalogoMantenimientoSPEI(loginData.getCatalogoMantenimientoSPEI());
		}

		/*
		 * if the server has returned at least 1 catalog it means that the
		 * catalog version has changed
		 */
		if (loginData.getCatalogs() != null) {
			escribirCatalogos(loginData);
		}

		// Verifica el timeout de la pantalla del teléfono, si este timeout es
		// menor que el regresado por el servidor, se toma el del teléfono
		int defTimeOut = 0;
		final int DELAY = 3000;
		final int oneMinuteInMilis = 60000;

		if (SuiteAppDesac.getInstance()
				.getBmovilApplication().getBmovilViewsController()
				.getCurrentViewControllerApp() == null) {
			SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController().setCurrentActivityApp(
							loginViewController.getMenuSuiteViewController());
		}

		defTimeOut = Settings.System.getInt(SuiteAppDesac.getInstance()
				.getBmovilApplication().getBmovilViewsController()
				.getCurrentViewControllerApp().getContentResolver(),
				Settings.System.SCREEN_OFF_TIMEOUT, DELAY);

		if (defTimeOut < loginData.getTimeout() * oneMinuteInMilis
				&& defTimeOut > 15000)
			session.setTimeout(defTimeOut);
		else
			session.setTimeout(loginData.getTimeout() * oneMinuteInMilis);

		session.setValidity(Session.VALID_STATUS);
		parentViewController.muestraIndicadorActividad("",
				parentViewController.getString(R.string.alert_StoreSession));
		storeSessionType = STORE_SESSION_LOGIN;
		//SPEI
		//  if(loginData.getSpeiCatalog() != null ) {
			//  System.out.println("LoginData es nulo? "+ loginData.getSpeiCatalog() );
    	//session.setSpeiCatalog(loginData.getSpeiCatalog());
    	//session.saveSpeiCatalog();
    	//session.saveSpeiCatalogJson();
		// }
		//termina SPEI
		session.storeSession(this);
	}

	private void storeSessionAfterActivation() {
		storeSessionType = STORE_SESSION_ACTIVATION;
		parentViewController.muestraIndicadorActividad("",
				parentViewController.getString(R.string.alert_StoreSession));
		Session session = Session.getInstance(SuiteAppDesac.appContext);
		session.setApplicationActivated(true);
		session.storeSession(this);
	}

	public String getUsuario() {
		return Session.getInstance(SuiteAppDesac.appContext).getUsername();
	}

	public String getIum() {
		return Session.getInstance(SuiteAppDesac.appContext).getIum();
	}

	public Catalog[] leerCatalogos() {
		return Session.getInstance(SuiteAppDesac.appContext).getCatalogs();
	}

	public void escribirCatalogos(final LoginData loginData) {
		Session session = Session.getInstance(SuiteAppDesac.appContext);
		session.setCatalogVersions(loginData.getCatalogVersions());
		// the parameter(array) logindata.GetCatalogs may have null elements
		// indicating that the server has not sended a catalog in it's
		// reply(that
		// only happens when our local catalog have the same version that server
		// catalog). In this case the session.updateCatalogs method will ignore
		// the passed param
		session.updateCatalogs(loginData.getCatalogs());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.aplicaciones.bmovil.classes.common.SessionStoredListener
	 * #sessionStored()
	 */
	@Override
	public void sessionStored() {
		parentViewController.ocultaIndicadorActividad();
		if (storeSessionType.equals(STORE_SESSION_LOGIN)) {
			if (loginResponse.getStatus() == ServerResponse.OPERATION_OPTIONAL_UPDATE) {
				parentViewController.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						showUpdateConfirmationAlert(
								loginData.getUpdateMessage(), false,
								loginData.getUpdateURL());
					}
				});
			} else if (loginResponse.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
//				final CambioPerfilDelegate cambioPerfilDelegate = new CambioPerfilDelegate();

				Session session = Session.getInstance(SuiteAppDesac.appContext);
				//
				// Se valida si existe un cambio de perfil de basico a avanzado
//				if (cambioPerfilDelegate
//						.validarCambioPerfilAvanzado(parentViewController)) {
//
//					session.saveBanderasBMovil(
//							Constants.BANDERAS_CAMBIO_PERFIL, false, false);
//					if (isTokenActivo()) {
//						cambioPerfilDelegate
//								.mostrarCambioPerfil(parentViewController);
//
//					}
//				}

				/*
				 * }else { session.saveBanderasBMovil(
				 * Constants.BANDERAS_CAMBIO_PERFIL, false, false); }
				 */

				// Se valida si existe un cambio de perfil de avanzado a basico
//				if (cambioPerfilDelegate.validarCambioPerfilBasico()) {
//
//					// SuiteApp.getInstance().getBmovilApplication().initTimer();
//					// SuiteApp.getInstance().getBmovilApplication().resetLogoutTimer();
//					SuiteAppDesac.getInstance()
//							.getBmovilApplication()
//							.getBmovilViewsController()
//							.addDelegateToHashMap(
//									CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID,
//									cambioPerfilDelegate);
//					cambioPerfilDelegate
//							.setBaseViewController(parentViewController);
//					final CambioPerfilDelegate delegate = cambioPerfilDelegate;
//					parentViewController.runOnUiThread(new Runnable() {
//						@Override
//						public void run() {
//							delegate.realizaOperacion();
//						}
//					});
//					return;
//
//				}

				boolean checkLoginWarning = true;

				if (checkLoginWarning) {
					final String loginWarningMessage = loginData.getMessage();
					final String titleMessage = "";// SuiteApp.appContext.getString(R.string.label_important);
					
					// de Bienvenida
					//if (loginWarningMessage != null) { //MP vacio no muestra alert
					if (!("".equals(loginWarningMessage))) {	
						parentViewController.runOnUiThread(new Runnable() {
							@Override
							public void run() {

								// Parte de incidencia 22249 para que no se
								// muestre el alert
//								if (!(parentViewController instanceof ActivacionViewController)
//										&& !(cambioPerfilDelegate.isCambioPerfil())
//										) {
//									parentViewController.showInformationAlert(
//											titleMessage, loginWarningMessage,
//											new OnClickListener() {
//												public void onClick(DialogInterface dialog,	int which) {
//													dialog.dismiss();
//													SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController().showMenuPrincipal();
//													if (parentViewController instanceof ActivacionViewController) {
//														parentViewController.finish();
//														MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteAppDesac.getInstance().getSuiteViewsController()
//																.getCurrentViewControllerApp();
//														// menuViewController.restableceMenu();
//														menuViewController.setShouldHideLogin(true);
//													}
//													// SuiteApp.getInstance()
//													// .getBmovilApplication()
//													// .initTimer();
//													// SuiteApp.getInstance()
//													// .getBmovilApplication()
//													// .resetLogoutTimer();
//												}
//											});
//
//								}
								SuiteAppDesac.getInstance().getBmovilApplication()
										.initTimer();
								SuiteAppDesac.getInstance().getBmovilApplication()
										.resetLogoutTimer();
							}
						});
					} else {
//						SuiteAppDesac.getInstance().getBmovilApplication()
//								.getBmovilViewsController().showMenuPrincipal();
//						if (parentViewController instanceof ActivacionViewController) {
//							parentViewController.finish();
//							MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteAppDesac
//									.getInstance().getSuiteViewsController()
//									.getCurrentViewControllerApp();
//							// menuViewController.restableceMenu();
//							menuViewController.setShouldHideLogin(true);
//						}
						SuiteAppDesac.getInstance().getBmovilApplication()
								.initTimer();
						SuiteAppDesac.getInstance().getBmovilApplication()
								.resetLogoutTimer();
					}

				} else {
//					SuiteAppDesac.getInstance().getBmovilApplication()
//							.getBmovilViewsController().showMenuPrincipal();
//					if (parentViewController instanceof ActivacionViewController) {
//						parentViewController.finish();
//						MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteAppDesac
//								.getInstance().getSuiteViewsController()
//								.getCurrentViewControllerApp();
//						// menuViewController.restableceMenu();
//						menuViewController.setShouldHideLogin(true);
//					}
					SuiteAppDesac.getInstance().getBmovilApplication().initTimer();
					SuiteAppDesac.getInstance().getBmovilApplication()
							.resetLogoutTimer();
				}
				loginData = null;
				loginResponse = null;
			}
		} else if (storeSessionType.equals(STORE_SESSION_ACTIVATION)) {
			storeSessionType = STORE_SESSION_LOGIN;
			parentViewController.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					login(Session.getInstance(SuiteAppDesac.appContext)
							.getUsername(),
							Session.getInstance(SuiteAppDesac.appContext)
									.getPassword());
				}
			});
		} else if (storeSessionType.equals(STORE_SESSION_PROFILE_CHANGE)) {
			storeSessionType = STORE_SESSION_LOGIN;
			parentViewController.runOnUiThread(new Runnable() {
				@Override
				public void run() {
//					SuiteAppDesac.getInstance().getBmovilApplication()
//							.getBmovilViewsController().showMenuPrincipal();
					MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteAppDesac
							.getInstance().getSuiteViewsController()
							.getCurrentViewControllerApp();
					menuViewController.restableceMenu();
					SuiteAppDesac.getInstance().getBmovilApplication().initTimer();
					SuiteAppDesac.getInstance().getBmovilApplication()
							.resetLogoutTimer();
				}
			});
		}
	}

	/***
	 * Show an alert to inform the user that there is a new version available,
	 * and asks what to do.
	 * 
	 * @param message
	 *            the information to show
	 * @param mandatory
	 *            true if the update is mandatory, false if not
	 * @param updateUrl
	 *            the url where the update can be found
	 */
	public void showUpdateConfirmationAlert(final String message,final boolean mandatory,
			final String updateUrl) {
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				parentViewController);
		alertDialog.setTitle(R.string.menuSuite_update_alert_title);
		alertDialog.setMessage(message);
		alertDialog.setCancelable(false);
		// Update button
		alertDialog.setPositiveButton(
				R.string.menuSuite_update_alert_updateOption,
				new OnClickListener() {
					public void onClick(final DialogInterface dialog,final int which) {
						String url = updateUrl;
						if (url.length() > 0) {
							if (!url.startsWith("http://")
									&& !url.startsWith("https://")) {
								url = "http://" + updateUrl;
							}
							Intent browserIntent = new Intent(
									Intent.ACTION_VIEW, Uri.parse(url));
							dialog.dismiss();
							parentViewController.startActivity(browserIntent);
						}

					}
				});

		// Continue button
		if (!mandatory) {
			alertDialog.setNegativeButton(R.string.alert_continue,
					new OnClickListener() {
						public void onClick(final DialogInterface dialog,final int which) {
							dialog.dismiss();
							if (Session.getInstance(SuiteAppDesac.appContext)
									.getValidity() == Session.VALID_STATUS) {
//								SuiteAppDesac.getInstance().getBmovilApplication()
//										.getBmovilViewsController()
//										.showMenuPrincipal();
								MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteAppDesac
										.getInstance()
										.getSuiteViewsController()
										.getCurrentViewControllerApp();
								menuViewController.setShouldHideLogin(true);
								SuiteAppDesac.getInstance().getBmovilApplication()
										.initTimer();
								SuiteAppDesac.getInstance().getBmovilApplication()
										.resetLogoutTimer();
							} else {
								MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteAppDesac
										.getInstance()
										.getSuiteViewsController()
										.getCurrentViewControllerApp();
								menuViewController.restableceMenu();
							}

						}
					});
		} else {
			alertDialog.setNegativeButton(
					R.string.menuSuite_update_alert_exitOption,
					new OnClickListener() {
						public void onClick(final DialogInterface dialog,final int which) {
							dialog.dismiss();
							MenuSuiteViewController menuViewController = (MenuSuiteViewController) SuiteAppDesac
									.getInstance().getSuiteViewsController()
									.getCurrentViewControllerApp();
							menuViewController.restableceMenu();
							menuViewController.setShouldHideLogin(true);
							SuiteAppDesac.getInstance().getBmovilApplication()
									.setApplicationLogged(false);
						}
					});
		}

		parentViewController.hideSoftKeyboard();
		alertDialog.show();
	}
	
	private boolean isTokenActivo() {
		return TYPE_SOFTOKEN.S1.value.equals(consultaEstatus
				.getInstrumento())
				&& Constants.ESTATUS_IS_ACTIVO.equals(consultaEstatus
						.getEstatus());
	}
}
