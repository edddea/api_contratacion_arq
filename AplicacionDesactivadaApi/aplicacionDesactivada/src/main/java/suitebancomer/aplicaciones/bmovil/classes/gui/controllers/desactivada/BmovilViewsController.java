package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.desactivada;

import java.util.ArrayList;


/*import suitebancomer.aplicaciones.bmovil.classes.common.Constants;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionAutenticacionViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.IngresarDatosViewController;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.hamburguesa.MenuHamburguesaViewsControllers;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.NuevaContraseniaDelegate;
import mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.QuitarSuspencionDelegate;
import mtto.suitebancomer.classes.gui.delegates.BaseDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionAutenticacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.desactivada.ConsultaEstatusAplicacionDesactivadaDelegate;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.ReactivacionDelegate;
import suitebancomer.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomer.classes.gui.controllers.BaseViewControllerCommons;*/
import suitebancomer.activacion.aplicaciones.bmovil.classes.entrada.InitActivacion;
import suitebancomer.aplicaciones.bmovil.classes.entrada.InitContratacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.entradas.InitReactivacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.contratacion.ContratacionDelegate;
import suitebancomer.classes.gui.controllers.desactivada.BaseViewsController;
import suitebancomer.classes.gui.controllers.desactivada.MenuSuiteViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.model.BanderasServer;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;
import android.view.View;

import com.bancomer.mbanking.activacion.SuiteApp;
import com.bancomer.mbanking.desactivada.R;
import com.bancomer.mbanking.desactivada.BmovilApp;
import com.bancomer.mbanking.desactivada.SuiteAppDesac;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

//import com.bancomer.mbanking.reactivacion.SuiteAppApiReactivacion;

public class BmovilViewsController extends BaseViewsController {

	private BmovilApp bmovilApp;
	// AMZ
	public ArrayList<String> estados = new ArrayList<String>();
	//private MenuPrincipalViewController controladorMenuPrincipal = new MenuPrincipalViewController();

	// AMZ

	/*
	 * @Override public void setCurrentActivity(BaseViewController
	 * currentViewController) { // TODO Auto-generated method stub
	 * super.setCurrentActivity(currentViewController); Log.i("Si", "No"); }
	 */
	public BmovilViewsController(final BmovilApp bmovilApp) {
		super();
		this.bmovilApp = bmovilApp;
	}

//	public void cierraViewsController() {
//		bmovilApp = null;
//		clearDelegateHashMap();
//		super.cierraViewsController();
//	}

	public void cerrarSesionBackground() {
		SuiteAppDesac.getInstance().getBmovilApplication()
				.setApplicationInBackground(true);
		bmovilApp.logoutApp(true);
	}

//	@Override
//	public void showMenuInicial() {
//		showMenuPrincipal();
//	}

//	public void showMenuPrincipal() {
//		showMenuPrincipal(false);
//	}

//	public void showMenuPrincipal(boolean inverted) {
//
//		// One CLick
//		MenuPrincipalDelegate delegate = (MenuPrincipalDelegate) getBaseDelegateForKey(MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new MenuPrincipalDelegate(new Promociones());
//			addDelegateToHashMap(
//					MenuPrincipalDelegate.MENU_PRINCIPAL_DELEGATE_ID, delegate);
//		}
		// Termina One click

//		showViewController(MenuPrincipalViewController.class,
//				Intent.FLAG_ACTIVITY_CLEAR_TOP, inverted);
//		SuiteAppDesac.getInstance().getBmovilApplication()
//				.setApplicationLogged(true);
//	}

//	public void showLogin() {
//		MenuSuiteViewController menuSuiteViewController = (MenuSuiteViewController) getCurrentViewControllerApp();
//		LoginDelegate loginDelegate = (LoginDelegate) getBaseDelegateForKey(LoginDelegate.LOGIN_DELEGATE_ID);
//		if (loginDelegate == null) {
//			loginDelegate = new LoginDelegate();
//			addDelegateToHashMap(LoginDelegate.LOGIN_DELEGATE_ID, loginDelegate);
//		}
//		loginDelegate.setParentViewController(menuSuiteViewController);
//		if (menuSuiteViewController.getLoginViewController() == null) {
//			LoginViewController loginViewController = new LoginViewController(
//					menuSuiteViewController, this);
//			loginViewController.setDelegate(loginDelegate);
//			menuSuiteViewController.setLoginViewController(loginViewController);
//			menuSuiteViewController.getBaseLayout()
//					.addView(loginViewController);
//			loginViewController.setVisibility(View.GONE);
//		}
//
//		menuSuiteViewController.plegarOpcion();
//		// AMZ
//		estados.clear();
//		TrackingHelper.trackState("login", estados);
//		// AMZ
//
//	}
//
//	public void showPantallaActivacion() {
//		showViewController(ActivacionViewController.class,
//				Intent.FLAG_ACTIVITY_NO_HISTORY);
//	}
//
//	 public void showBorrarDatos() {
//	 BorrarDatosDelegate borrarDatosDelegate = new BorrarDatosDelegate();
//	 addDelegateToHashMap(BorrarDatosDelegate.BORRAR_DATOS_DELEGATE_ID,
//	 borrarDatosDelegate);
//	 showViewController(BorrarDatosViewController.class);
//	 }

	// public void showCambioPerfil() {
	// CambioPerfilDelegate delegate = (CambioPerfilDelegate)
	// getBaseDelegateForKey(CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID);
	// if (null == delegate) {
	// delegate = new CambioPerfilDelegate();
	// addDelegateToHashMap(
	// CambioPerfilDelegate.CAMBIO_PERFIL_DELEGATE_ID, delegate);
	// }
	// showViewController(CambioPerfilViewController.class);
	// }

	public BmovilApp getBmovilApp() {
		return bmovilApp;
	}

	public void setBmovilApp(final BmovilApp bmovilApp) {
		this.bmovilApp = bmovilApp;
	}

	// public void showMisCuentasViewController() {
	// MisCuentasDelegate delegate = new MisCuentasDelegate();
	// addDelegateToHashMap(MisCuentasDelegate.MIS_CUENTAS_DELEGATE_ID,
	// delegate);
	// showViewController(MisCuentasViewController.class);
	// }
	//
	// public void showConsultarMovimientos() {
	// MovimientosDelegate delegate = (MovimientosDelegate)
	// getBaseDelegateForKey(MovimientosDelegate.MOVIMIENTOS_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new MovimientosDelegate();
	// addDelegateToHashMap(MovimientosDelegate.MOVIMIENTOS_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(ConsultaMovimientosViewController.class);
	// }
	//
	// public void showConsultarDepositosRecibidosCuenta(Account cuenta) {
	// DepositosRecibidosDelegate delegate = (DepositosRecibidosDelegate)
	// getBaseDelegateForKey(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new DepositosRecibidosDelegate();
	// addDelegateToHashMap(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID,
	// delegate);
	// }
	// delegate.setCuentaActual(cuenta);
	// showViewController(ConsultarDepositosRecibidosViewController.class);
	// }
	//
	// public void showConsultarDepositosRecibidos() {
	// DepositosRecibidosDelegate delegate = (DepositosRecibidosDelegate)
	// getBaseDelegateForKey(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new DepositosRecibidosDelegate();
	// addDelegateToHashMap(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(ConsultarDepositosRecibidosViewController.class);
	// }
	//
	// public void showConsultarObtenerComprobante() {
	// ObtenerComprobantesDelegate delegate = (ObtenerComprobantesDelegate)
	// getBaseDelegateForKey(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new ObtenerComprobantesDelegate();
	// addDelegateToHashMap(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(ConsultarObtenerComprobantesViewController.class);
	// }
	//
	//
	// /**
	// * Muestra la ventana de consultar envios de dinero movil.
	// */
	// public void showConsultarDineroMovil() {
	// DineroMovilDelegate delegate = (DineroMovilDelegate)
	// getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new DineroMovilDelegate();
	// addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(ConsultarDineroMovilViewController.class);
	// }
	// /**
	// * Muestra la pantalla para consultar Retiro sin tarjeta.
	// */
	// public void showConsultarRetiroSinTarjeta() {
	// ConsultaRetiroSinTarjetaDelegate delegate =
	// (ConsultaRetiroSinTarjetaDelegate)
	// getBaseDelegateForKey(ConsultaRetiroSinTarjetaDelegate.RETIRO_SIN_TARJETA_DELEGATE_ID);
	//
	// if (delegate == null) {
	// delegate = new ConsultaRetiroSinTarjetaDelegate();
	// addDelegateToHashMap(
	// ConsultaRetiroSinTarjetaDelegate.RETIRO_SIN_TARJETA_DELEGATE_ID,
	// delegate);
	// }
	// //delegate.resetPeriodo();
	// showViewController(ConsultaRetiroSinTarjetaViewController.class);
	// }
	//
	//
	//
	// public void showCuentaOrigenListViewController() {
	// showViewController(CuentaOrigenListViewController.class);
	// }
	//
	// public void showDetalleMovimientosViewController() {
	// showViewController(DetalleMovimientosViewController.class);
	// }
	//
	// @Override
	// public void onUserInteraction() {
	//
	// if (bmovilApp != null) {
	// if(SuiteApp.getInstance().getBmovilApplication().getSessionTimer()==null){
	// bmovilApp.initTimer();
	// }
	// bmovilApp.resetLogoutTimer();
	// }
	// super.onUserInteraction();
	// }
	//
	// @Override
	// public boolean consumeAccionesDeReinicio() {
	// if (!SuiteApp.getInstance().getBmovilApplication()
	// .isApplicationLogged()) {
	// ((MenuSuiteViewController) SuiteApp.getInstance()
	// .getSuiteViewsController().getCurrentViewControllerApp())
	// .setShouldHideLogin(true);
	// SuiteApp.getInstance().getSuiteViewsController()
	// .showMenuSuite(true);
	// return true;
	// }
	// return false;
	// }
	//
	// @Override
	// public boolean consumeAccionesDePausa() {
	// if (SuiteApp.getInstance().getBmovilApplication().isApplicationLogged()
	// && !isActivityChanging()) {
	// cerrarSesionBackground();
	// return true;
	// }
	// return false;
	// }
	//
	// @Override
	// public boolean consumeAccionesDeAlto() {
	// if (!SuiteApp.getInstance().getBmovilApplication().isChangingActivity()
	// && currentViewControllerApp != null) {
	// currentViewControllerApp.hideSoftKeyboard();
	// }
	// SuiteApp.getInstance().getBmovilApplication()
	// .setChangingActivity(false);
	// return true;
	// }
	//
	// /*
	// * Muestra el menu de administrar
	// */
	// public void showMenuAdministrar() {
	// MenuAdministrarDelegate delegate = (MenuAdministrarDelegate)
	// getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new MenuAdministrarDelegate();
	// addDelegateToHashMap(
	// MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(MenuAdministrarViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de acerca de...
	// */
	// public void showAcercaDe() {
	// MenuAdministrarDelegate delegate = (MenuAdministrarDelegate)
	// getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new MenuAdministrarDelegate();
	// addDelegateToHashMap(
	// MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(AcercaDeViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de Novedades
	// */
	// public void showNovedades() {
	// MenuAdministrarDelegate delegate = (MenuAdministrarDelegate)
	// getBaseDelegateForKey(MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new MenuAdministrarDelegate();
	// addDelegateToHashMap(
	// MenuAdministrarDelegate.MENU_ADMINISTRAR_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(NovedadesViewController.class);
	// }
	//
	// public void showNovedadesLogin() {
	// showViewController(NovedadesViewController.class);
	// }
	//
	// /*
	// * muestra la pantalla para cambiar password
	// */
	// public void showCambiarPassword() {
	// CambiarPasswordDelegate delegate = (CambiarPasswordDelegate)
	// getBaseDelegateForKey(CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new CambiarPasswordDelegate();
	// addDelegateToHashMap(
	// CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(CambiarPasswordViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla con el resultado del cambio de password
	// */
	// public void showCambiarPasswordResultado() {
	// CambiarPasswordDelegate delegate = (CambiarPasswordDelegate)
	// getBaseDelegateForKey(CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new CambiarPasswordDelegate();
	// addDelegateToHashMap(
	// CambiarPasswordDelegate.CAMBIAR_PASSWORD_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(CambiarPasswordResultadoViewController.class);
	// }
	//
	/**
	 * Muestra la pantalla de confirmacion autenticacion
	 */
//	public void showConfirmacionAutenticacionViewController(
//			DelegateBaseAutenticacion autenticacionDelegate, int resIcon,
//			int resTitle, int resSubtitle) {
//		showConfirmacionAutenticacionViewController(autenticacionDelegate,
//				resIcon, resTitle, resSubtitle, R.color.primer_azul);
//	}
//
//	//
//	/**
//	 * Muestra la pantalla de confirmacion autenticacion
//	 */
//	public void showConfirmacionAutenticacionViewController(
//			DelegateBaseAutenticacion autenticacionDelegate, int resIcon,
//			int resTitle, int resSubtitle, int resTitleColor) {
//		ConfirmacionAutenticacionDelegate confirmacionDelegate = (ConfirmacionAutenticacionDelegate) getBaseDelegateForKey(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
//		if (confirmacionDelegate != null) {
//			removeDelegateFromHashMap(ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
//		}
//		confirmacionDelegate = new ConfirmacionAutenticacionDelegate(
//				autenticacionDelegate);
//		addDelegateToHashMap(
//				ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID,
//				confirmacionDelegate);
//		// AMZ
//		if (estados.size() == 0) {
//			// AMZ
//			TrackingHelper.trackState("reactivacion", estados);
//			// AMZ
//		}
//		showViewController(ConfirmacionAutenticacionViewController.class);
//	}

	//
	// /**
	// * Muestra la pantalla de resultados
	// */
	// public void showResultadosViewController(
	// DelegateBaseOperacion delegateBaseOperacion, int resIcon,
	// int resTitle) {
	// ResultadosDelegate resultadosDelegate = (ResultadosDelegate)
	// getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
	// if (resultadosDelegate != null) {
	// removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
	// }
	// resultadosDelegate = new ResultadosDelegate(delegateBaseOperacion);
	// addDelegateToHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID,
	// resultadosDelegate);
	// showViewController(ResultadosViewController.class);
	// }
	//
	// public void showResultadosViewControllerWithoutFreq(
	// DelegateBaseOperacion delegateBaseOperacion, int resIcon,
	// int resTitle) {
	// ResultadosDelegate resultadosDelegate = (ResultadosDelegate)
	// getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
	// if (resultadosDelegate != null) {
	// removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
	// }
	// resultadosDelegate = new ResultadosDelegate(delegateBaseOperacion);
	// resultadosDelegate.setFrecOpOK(true);
	// addDelegateToHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID,
	// resultadosDelegate);
	// showViewController(ResultadosViewController.class);
	// }
	//
	// public void showOpcionesTransferir(String tipoOperacion, int resIcon,
	// int resTitle, int resSubtitle) {
	// String[] llaves = new String[4];
	// Object[] valores = new Object[4];
	//
	// llaves[0] = Constants.PANTALLA_BASE_ICONO;
	// valores[0] = Integer.valueOf(resIcon);
	// llaves[1] = Constants.PANTALLA_BASE_TITULO;
	// valores[1] = Integer.valueOf(resTitle);
	// llaves[2] = Constants.PANTALLA_BASE_SUBTITULO;
	// valores[2] = Integer.valueOf(resSubtitle);
	// llaves[3] = Constants.PANTALLA_BASE_TIPO_OPERACION;
	// valores[3] = String.valueOf(tipoOperacion);
	//
	// TransferirDelegate delegate = (TransferirDelegate)
	// getBaseDelegateForKey(TransferirDelegate.TRANSFERIR_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new TransferirDelegate();
	// addDelegateToHashMap(TransferirDelegate.TRANSFERIR_DELEGATE_ID,
	// delegate);
	// }
	//
	// showViewController(OpcionesTransferViewController.class, 0, false,
	// llaves, valores);
	// }
	//
	// /**
	// * Muestra la pantalla de transferencia mis cuentas
	// */
	// public void showTransferirMisCuentas() {
	// TransferirMisCuentasDelegate delegate = (TransferirMisCuentasDelegate)
	// getBaseDelegateForKey(TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new TransferirMisCuentasDelegate();
	// addDelegateToHashMap(
	// TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(TransferirMisCuentasSeleccionViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla de transferencia entre mis cuentas detalle
	// */
	// public void showTransferirMisCuentasDetalle() {
	// TransferirMisCuentasDelegate delegate = (TransferirMisCuentasDelegate)
	// getBaseDelegateForKey(TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new TransferirMisCuentasDelegate();
	// addDelegateToHashMap(
	// TransferirMisCuentasDelegate.TRANSFERIR_MIS_CUENTAS_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(TransferMisCuentasDetalleViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla de confirmacion para transferencias.
	// *
	// * @param delegateBaseOperacion
	// * EL DelegateBaseOperacion que manda a llamar la pantalla de
	// * confirmaci�n.
	// */
	// public void showConfirmacion(DelegateBaseOperacion delegateBaseOperacion)
	// {
	// // El delegado de confirmaci�n se crea siempre, esto ya que el delegado
	// // que contiene el internamente cambia segun quien invoque este método.
	// ConfirmacionDelegate delegate = (ConfirmacionDelegate)
	// getBaseDelegateForKey(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
	// if (delegate != null) {
	// removeDelegateFromHashMap(ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID);
	// }
	// delegate = new ConfirmacionDelegate(delegateBaseOperacion);
	// addDelegateToHashMap(
	// ConfirmacionDelegate.CONFIRMACION_DELEGATE_DELEGATE_ID,
	// delegate);
	// showViewController(ConfirmacionViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla para alta de frecuente
	// */
	// public void showAltaFrecuente(DelegateBaseOperacion
	// delegateBaseOperacion) {
	// AltaFrecuenteDelegate delegate = (AltaFrecuenteDelegate)
	// getBaseDelegateForKey(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID);
	// if (delegate != null) {
	// removeDelegateFromHashMap(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID);
	// }
	// delegate = new AltaFrecuenteDelegate(delegateBaseOperacion);
	// addDelegateToHashMap(AltaFrecuenteDelegate.ALTA_FRECUENTE_DELEGATE_ID,
	// delegate);
	// showViewController(AltaFrecuenteViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de transferViewController gen�rica para
	// transferir
	// * comprar pagar consultar
	// */
	// public void showTransferViewController(String tipoOperacion,
	// boolean isExpress, boolean isTDC) {
	// TransferirDelegate delegate = (TransferirDelegate)
	// getBaseDelegateForKey(TransferirDelegate.TRANSFERIR_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new TransferirDelegate();
	// addDelegateToHashMap(TransferirDelegate.TRANSFERIR_DELEGATE_ID,
	// delegate);
	// }
	// delegate.setTipoOperacion(tipoOperacion);
	// delegate.setExpress(isExpress);
	// delegate.setTDC(isTDC);
	// delegate.crearModeloDeOperacion();
	// showViewController(TransferViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de transferViewController gen�rica para
	// transferir
	// * comprar pagar consultar
	// */
	// public void showSelectTDCViewController(String tipoOperacion,
	// boolean isExpress, boolean isTDC) {
	// PagoTdcDelegate delegate = (PagoTdcDelegate)
	// getBaseDelegateForKey(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID);
	//
	// if (delegate == null) {
	// delegate = new PagoTdcDelegate();
	// addDelegateToHashMap(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID,
	// delegate);
	// }
	//
	// // delegate.setTipoOperacion(tipoOperacion);
	// // delegate.setExpress(isExpress);
	// // delegate.setTDC(isTDC);
	// // delegate.crearModeloDeOperacion();
	// showViewController(SeleccionaTdcViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de InterbancariosViewController
	// */
	// public void showInterbancariosViewController(Object obj) {
	// InterbancariosDelegate delegate = (InterbancariosDelegate)
	// getBaseDelegateForKey(InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID);
	// if (delegate == null) {
	// if (obj instanceof TransferenciaInterbancaria) {
	// delegate = new InterbancariosDelegate(true);
	// } else
	// delegate = new InterbancariosDelegate();
	// addDelegateToHashMap(
	// InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID, delegate);
	// }
	// delegate.setBajaFrecuente(false);
	// if (obj instanceof Constants.Operacion) {
	// delegate.setTipoOperacion((Constants.Operacion) obj);
	// delegate.setEsFrecuente(false);
	// }
	// if (obj instanceof TransferenciaInterbancaria) {
	// delegate.setEsFrecuente(true);
	// delegate.setTransferenciaInterbancaria((TransferenciaInterbancaria) obj);
	// }
	// showViewController(InterbancariosViewController.class);
	// }
	//
	// public void showOpcionesPagar() {
	// String[] llaves = new String[4];
	// Object[] valores = new Object[4];
	//
	// llaves[0] = Constants.PANTALLA_BASE_ICONO;
	// valores[0] = Integer.valueOf(R.drawable.icono_pagar_servicios);
	// llaves[1] = Constants.PANTALLA_BASE_TITULO;
	// valores[1] = Integer.valueOf(R.string.pagar_title);
	// llaves[2] = Constants.PANTALLA_BASE_SUBTITULO;
	// valores[2] = Integer.valueOf(R.string.pagar_subtitle);
	//
	// PagarDelegate delegate = (PagarDelegate)
	// getBaseDelegateForKey(PagarDelegate.PAGAR_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new PagarDelegate();
	// addDelegateToHashMap(PagarDelegate.PAGAR_DELEGATE_ID, delegate);
	// }
	//
	// showViewController(OpcionesPagosViewController.class, 0, false, llaves,
	// valores);
	// }
	//
	// public void showTipoConsultaFrecuentes() {
	// TipoConsultaDelegate delegate = (TipoConsultaDelegate)
	// getBaseDelegateForKey(TipoConsultaDelegate.TIPO_CONSULTA_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new TipoConsultaDelegate();
	// addDelegateToHashMap(
	// TipoConsultaDelegate.TIPO_CONSULTA_DELEGATE_ID, delegate);
	// }
	// showViewController(TipoConsultaViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de PagoServiciosViewController
	// */
	// public void showPagosViewController(Constants.Operacion tipoOperacion) {
	// PagoServiciosDelegate delegate = (PagoServiciosDelegate)
	// getBaseDelegateForKey(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new PagoServiciosDelegate();
	// addDelegateToHashMap(
	// PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID, delegate);
	// }
	// delegate.setTipoOperacion(tipoOperacion);
	// showViewController(PagoServiciosFrecuentesViewController.class);
	// }
	//
	// /*
	// *
	// */
	// public void showConsultaFrecuentes(String tipoFrecuente) {
	// String[] llaves = new String[2];
	// Object[] valores = new Object[2];
	// DelegateBaseOperacion delegate = null;
	// llaves[0] = Constants.FREQUENT_REQUEST_SCREEN_DELEGATE_KEY;
	// llaves[1] = Constants.FREQUENT_REQUEST_SCREEN_TITLE_KEY;
	// Session session = Session.getInstance(SuiteApp.getInstance());
	// if (tipoFrecuente.equals(Constants.tipoCFOtrosBancos)) {
	//
	//
	// if
	// (Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirInterbancariaF,
	// session.getClientProfile())) {
	// valores[0] = InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID;
	// valores[1] = SuiteApp.getInstance().getString(
	// R.string.externalTransfer_frequentTitle);
	// removeDelegateFromHashMap(InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID);
	// delegate = new InterbancariosDelegate(true);
	// addDelegateToHashMap(
	// InterbancariosDelegate.INTERBANCARIOS_DELEGATE_ID, delegate);
	// }else{
	// //NO ESTA PERMITIDO
	//
	// }
	//
	// } else if (tipoFrecuente.equals(Constants.tipoCFPagosCIE)) {
	// if
	// (Autenticacion.getInstance().operarOperacion(Constants.Operacion.pagoServiciosF,
	// session.getClientProfile())) {
	// valores[0] = PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID;
	// valores[1] = SuiteApp.getInstance().getString(
	// R.string.servicesPayment_frequentTitle);
	// removeDelegateFromHashMap(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID);
	// delegate = new PagoServiciosDelegate();
	// addDelegateToHashMap(
	// PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID, delegate);
	// }else{
	// //NO ESTA PERMITIDO
	//
	// }
	//
	// } else if (tipoFrecuente.equals(Constants.tipoCFTiempoAire)) {
	//
	// if
	// (Autenticacion.getInstance().operarOperacion(Constants.Operacion.compraTiempoAireF,
	// session.getClientProfile())) {
	// valores[0] = TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID;
	// valores[1] = SuiteApp.getInstance().getString(
	// R.string.tiempo_aire_title);
	// removeDelegateFromHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
	// delegate = new TiempoAireDelegate(true);
	// addDelegateToHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID,
	// delegate);
	//
	// }else{
	// //NO ESTA PERMITIDO
	//
	// }
	//
	// } else if (tipoFrecuente.equals(Constants.tipoCFOtrosBBVA)) {
	// if
	// (Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirBancomerF,
	// session.getClientProfile())) {
	// valores[0] = OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID;
	// valores[1] = SuiteApp.getInstance().getString(
	// R.string.transferir_otrosBBVA_title);
	// removeDelegateFromHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
	// delegate = new OtrosBBVADelegate(false, false, true);
	// addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID,
	// delegate);
	// }else{
	// //NO ESTA PERMITIDO
	//
	// }
	//
	// } else if (tipoFrecuente.equals(Constants.tipoCFCExpress)) {
	// if
	// (Autenticacion.getInstance().operarOperacion(Constants.Operacion.transferirBancomerF,
	// session.getClientProfile())) {
	// valores[0] = OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID;
	// valores[1] = SuiteApp.getInstance().getString(
	// R.string.transferir_otrosBBVA_express_title);
	// removeDelegateFromHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
	// delegate = new OtrosBBVADelegate(true, false, true);
	// addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID,
	// delegate);
	// }else{
	// //NO ESTA PERMITIDO
	//
	// }
	//
	// } else if (tipoFrecuente.equals(Constants.tipoCFDineroMovil)) {
	//
	// if
	// (Autenticacion.getInstance().operarOperacion(Constants.Operacion.dineroMovilF,
	// session.getClientProfile())) {
	// valores[0] = DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID;
	// valores[1] = SuiteApp.getInstance().getString(
	// R.string.transferir_dineromovil_titulo);
	// removeDelegateFromHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
	// delegate = new DineroMovilDelegate();
	// addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
	// delegate);
	// }else{
	// //NO ESTA PERMITIDO
	// TipoConsultaDelegate tipoConsultaDelegate = (TipoConsultaDelegate)
	// getBaseDelegateForKey(TipoConsultaDelegate.TIPO_CONSULTA_DELEGATE_ID);
	// tipoConsultaDelegate.comprobarRecortado();
	//
	// }
	// }
	// if (delegate != null)
	// showViewController(ConsultarFrecuentesViewController.class, 0,
	// false, llaves, valores);
	// }
	//
	// /*
	// * Muestra la pantalla del menu consultar
	// */
	// public void showMenuConsultar() {
	// MenuConsultarDelegate delegate = (MenuConsultarDelegate)
	// getBaseDelegateForKey(MenuConsultarDelegate.MENU_CONSULTAR_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new MenuConsultarDelegate();
	// addDelegateToHashMap(
	// MenuConsultarDelegate.MENU_CONSULTAR_DELEGATE_ID, delegate);
	// }
	// showViewController(MenuConsultarViewController.class);
	//
	// }
	//
	// /*
	// * Muestra la pantalla de PagoServiciosViewController
	// */
	// public void showPagoServiciosViewController(PagoServicio pagoServicio) {
	//
	// PagoServiciosDelegate delegate = (PagoServiciosDelegate)
	// getBaseDelegateForKey(PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new PagoServiciosDelegate();
	// addDelegateToHashMap(
	// PagoServiciosDelegate.PAGO_SERVICIOS_DELEGATE_ID, delegate);
	// }
	// delegate.setBajaFrecuente(false);
	// if (pagoServicio != null) {
	// delegate.setPagoServicio(pagoServicio);
	// delegate.setFrecuente(true);
	// delegate.setTipoOperacion(Constants.Operacion.pagoServiciosF);
	// delegate.setPreregister(false);
	// } else {
	// delegate.getPagoServicio().setConvenioServicio("");
	// delegate.setFrecuente(false);
	// if (delegate.isPreregister()) {
	// delegate.setTipoOperacion(Constants.Operacion.pagoServiciosP);
	// } else {
	// delegate.setTipoOperacion(Constants.Operacion.pagoServicios);
	// }
	// }
	// showViewController(PagoServiciosViewController.class);
	// }
	//
	// public void showPagoServiciosAyudaViewController(String[] llaves,
	// Object[] valores) {
	// showViewController(PagoServiciosAyudaViewController.class, 0, false,
	// llaves, valores);
	// }
	//
	// public void showLecturaCodigoViewController(int codigoActividad) {
	// showViewControllerForResult(CaptureActivity.class, codigoActividad);
	// }
	//
	// /**
	// * Muestra la pantalla de tranferencia de dinero m�vil.
	// */
	// public void showTransferirDineroMovil(TransferenciaDineroMovil frecuente,
	// boolean bajaDM) {
	// DineroMovilDelegate delegate = (DineroMovilDelegate)
	// getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
	// if (null == delegate) {
	// delegate = new DineroMovilDelegate();
	// addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
	// delegate);
	// }
	// if (null != frecuente) {
	// delegate.setTransferencia(frecuente);
	// delegate.esFrecuente(true);
	// }
	// //Valida que al entrar a la pantalla de dinero movil
	// //no precargue los datos de un frecuente después de operarñpo
	// // en una operación nueva
	// if(null == frecuente)
	// {
	// delegate.esFrecuente(false);
	// }
	// delegate.setEsBajaDM(bajaDM);
	// delegate.setBajaFrecuente(false);
	// showViewController(DineroMovilViewController.class);
	// }
	//
	// /**
	// *
	// * @param rapido
	// */
	// public void showTransferirDineroMovil(Rapido rapido) {
	// DineroMovilDelegate delegate = (DineroMovilDelegate)
	// getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
	// if (null == delegate) {
	// delegate = new DineroMovilDelegate();
	// addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
	// delegate);
	// }
	// delegate.cargarRapido(rapido);
	// delegate.esFrecuente(false);
	// delegate.setEsBajaDM(false);
	// delegate.setBajaFrecuente(false);
	// showViewController(DineroMovilViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla del Menu Comprar
	// */
	// public void showComprarViewController() {
	// MenuComprarDelegate delegate = (MenuComprarDelegate)
	// getBaseDelegateForKey(MenuComprarDelegate.MENU_COMPRAR_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new MenuComprarDelegate();
	// addDelegateToHashMap(MenuComprarDelegate.MENU_COMPRAR_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(MenuComprarViewController.class);
	// }
	//
	// public void showTiempoAireViewController(Rapido rapido) {
	// TiempoAireDelegate delegate = (TiempoAireDelegate)
	// getBaseDelegateForKey(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
	// if (delegate != null) {
	// removeDelegateFromHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
	// }
	// delegate = new TiempoAireDelegate(false);
	// addDelegateToHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID,
	// delegate);
	// delegate.setTipoOperacion(Operacion.compraTiempoAireR);
	// delegate.setFrecuente(false);
	// delegate.cargarRapido(rapido);
	//
	// showViewController(TiempoAireDetalleViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de Tiempo Aire
	// */
	// public void showTiempoAireViewController(Constants.Operacion
	// tipoOpracion) {
	// TiempoAireDelegate delegate = (TiempoAireDelegate)
	// getBaseDelegateForKey(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
	// if (delegate != null) {
	// removeDelegateFromHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
	// }
	// delegate = new TiempoAireDelegate(false);
	// addDelegateToHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID,
	// delegate);
	// delegate.setTipoOperacion(tipoOpracion);
	// showViewController(TiempoAireViewController.class);
	// }
	//
	// /*
	// * Muestra la segunda pantalla de Tiempo Aire
	// */
	// public void showTiempoAireDetalleViewController() {
	// TiempoAireDelegate delegate = (TiempoAireDelegate)
	// getBaseDelegateForKey(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new TiempoAireDelegate(false);
	// addDelegateToHashMap(TiempoAireDelegate.TIEMPO_AIRE_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(TiempoAireDetalleViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de Transferencias terceros
	// */
	// public void showOtrosBBVAViewController(Object obj, boolean esExpress,
	// boolean esTDC) {
	// OtrosBBVADelegate delegate = (OtrosBBVADelegate)
	// getBaseDelegateForKey(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
	// if (delegate == null) {
	// if (obj instanceof TransferenciaOtrosBBVA) {
	// delegate = new OtrosBBVADelegate(esExpress, esTDC, true);
	// } else
	// delegate = new OtrosBBVADelegate(esExpress, esTDC);
	// addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID,
	// delegate);
	// }
	// delegate.setEsExpress(esExpress);
	// delegate.setEsTDC(esTDC);
	// if (obj instanceof Constants.Operacion) {
	// delegate.setTipoOperacion((Constants.Operacion) obj);
	// delegate.setEsFrecuente(false);
	// }
	// if (obj instanceof TransferenciaOtrosBBVA) {
	// delegate.setEsFrecuente(true);
	// delegate.setTransferenciaOtrosBBVA((TransferenciaOtrosBBVA) obj);
	// }
	// showViewController(OtrosBBVAViewController.class);
	// }
	//
	// public void showOtrosBBVAViewController(Rapido rapido) {
	// OtrosBBVADelegate delegate = (OtrosBBVADelegate)
	// getBaseDelegateForKey(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new OtrosBBVADelegate(false, false);
	// addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID,
	// delegate);
	// }
	//
	// delegate.cargarRapido(rapido);
	//
	// // if(obj instanceof Constants.Operacion){
	// // delegate.setTipoOperacion((Constants.Operacion)obj);
	// // delegate.setEsFrecuente(false);
	// // }
	// // if(obj instanceof TransferenciaOtrosBBVA){
	// // delegate.setEsFrecuente(true);
	// // delegate.setTransferenciaOtrosBBVA((TransferenciaOtrosBBVA) obj);
	// // }
	// showViewController(OtrosBBVAViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla de detalles de un movimiento de Obtener
	// Comprobante.
	// */
	// public void showDetalleObtenerComprobante() {
	// ObtenerComprobantesDelegate delegate = (ObtenerComprobantesDelegate)
	// getBaseDelegateForKey(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID);
	// if (null == delegate) {
	// delegate = new ObtenerComprobantesDelegate();
	// addDelegateToHashMap(ObtenerComprobantesDelegate.OBTENER_COMPROBANTE_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(DetalleObtenerComprobanteViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla de detalles de un movimiento de Depositos
	// Recibidos.
	// */
	// public void showDetalleDepositosRecibidos() {
	// DepositosRecibidosDelegate delegate = (DepositosRecibidosDelegate)
	// getBaseDelegateForKey(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID);
	// if (null == delegate) {
	// delegate = new DepositosRecibidosDelegate();
	// addDelegateToHashMap(DepositosRecibidosDelegate.DEPOSITOS_RECIBIDOS_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(DetalleDepositosRecibidosViewController.class);
	// }
	//
	//
	// /**
	// * Muestra la pantalla de detalles de un movimiento de dinero movil.
	// */
	// public void showDetalleDineroMovil() {
	// DineroMovilDelegate delegate = (DineroMovilDelegate)
	// getBaseDelegateForKey(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID);
	// if (null == delegate) {
	// delegate = new DineroMovilDelegate();
	// addDelegateToHashMap(DineroMovilDelegate.DINERO_MOVIL_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(DetalleDineroMovilViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla de detalles de un movimiento de dinero movil.
	// */
	// public void showDetalleRetiroSinTarjeta() {
	// ConsultaRetiroSinTarjetaDelegate delegate =
	// (ConsultaRetiroSinTarjetaDelegate)
	// getBaseDelegateForKey(ConsultaRetiroSinTarjetaDelegate.RETIRO_SIN_TARJETA_DELEGATE_ID);
	// if (null == delegate) {
	// delegate = new ConsultaRetiroSinTarjetaDelegate();
	// addDelegateToHashMap(ConsultaRetiroSinTarjetaDelegate.RETIRO_SIN_TARJETA_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(DetalleConsultaRetiroSinTarjetaViewController.class);
	// }
	//
	// @Override
	// public void removeDelegateFromHashMap(long key) {
	// // TODO Auto-generated method stub
	// if (key == BorrarDatosDelegate.BORRAR_DATOS_DELEGATE_ID) {
	// if(Server.ALLOW_LOG) Log.d("MES", "MES");
	// }
	// super.removeDelegateFromHashMap(key);
	// }
	//
	// public void showListaBancos(int codigoActividad) {
	// showViewControllerForResult(ListaBancosViewController.class,
	// codigoActividad);
	// }
	//
	// /**
	// * Muestra la pantalla para cambio de telefono asociado
	// */
	// public void showCambioDeTelefono() {
	// CambioTelefonoDelegate delegate = (CambioTelefonoDelegate)
	// getBaseDelegateForKey(CambioTelefonoDelegate.CAMBIO_TELEFONO_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new CambioTelefonoDelegate();
	// addDelegateToHashMap(
	// CambioTelefonoDelegate.CAMBIO_TELEFONO_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(CambioTelefonoViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla para cambio de cuenta asociado
	// */
	// public void showCambioCuentaAsociada() {
	// CambioCuentaDelegate delegate = (CambioCuentaDelegate)
	// getBaseDelegateForKey(CambioCuentaDelegate.CAMBIO_CUENTA_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new CambioCuentaDelegate();
	// addDelegateToHashMap(
	// CambioCuentaDelegate.CAMBIO_CUENTA_DELEGATE_ID, delegate);
	// }
	// showViewController(CambioCuentaViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla del menu Suspender / Cancelar
	// */
	// public void showMenuSuspenderCancelar() {
	// MenuSuspenderCancelarDelegate delegate = (MenuSuspenderCancelarDelegate)
	// getBaseDelegateForKey(MenuSuspenderCancelarDelegate.MENU_SUSPENDER_CANCELAR_ID);
	// if (delegate == null) {
	// delegate = new MenuSuspenderCancelarDelegate();
	// addDelegateToHashMap(
	// MenuSuspenderCancelarDelegate.MENU_SUSPENDER_CANCELAR_ID,
	// delegate);
	// }
	// showViewController(MenuSuspenderCancelarViewController.class);
	// }
	//
	/**
	 * Muestra la pantalla de ingresar datos de contratación.
	 */

	public void showContratacion(final ConsultaEstatus consultaEstatus,
			final String estatusServicio,final boolean deleteData) {
		
//		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new ContratacionDelegate();
//			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,
//					delegate);
//		}
//
//		delegate.setConsultaEstatus(consultaEstatus);
//		delegate.setDeleteData(deleteData);
//		if (estatusServicio
//				.equalsIgnoreCase(Constants.STATUS_ENGAGEMENT_UNCOMPLETE)) {
//			showContratacionAutenticacion(controladorPadre);
//		} else {
//			// AMZ
//			estados.clear();
//			// AMZ
//			TrackingHelper.trackState("contratacion datos", estados);
//			// AMZ
//			showViewController(IngresarDatosViewController.class, controladorPadre);
//		}

        //__________- invocacion al api de softoken ___________//
        SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/

        sftoken.onCreate(SuiteAppDesac.appContext);
        sftoken.setIntentToReturn(SuiteAppDesac.getInstance().getCallBackContratacion());
        sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());

        suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
                ServerCommons.SIMULATION,ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR,ServerCommons.DEVELOPMENT
        );
        //inicializa la clase
        InitContratacion initi=new InitContratacion(SuiteAppDesac.appContext, SuiteAppDesac.getInstance().getCallBackContratacion(), SuiteAppDesac.getInstance().getActivityForContratacion(),bs);
        //raliza el start del api
        initi.startContratacion(consultaEstatus,estatusServicio, deleteData);
	}

	//
	// /**
	// * Ejecuta flujo
	// *
	// */
//	public void showContratacionEP11(BaseViewController ownerController,
//			ConsultaEstatus consultaEstatus, String estatusServicio,
//			boolean deleteData) {
//
//		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new ContratacionDelegate();
//			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,
//					delegate);
//		}
//
//		delegate.setEscenarioAlternativoEA11(false);
//		delegate.setConsultaEstatus(consultaEstatus);
//		delegate.setDeleteData(deleteData);
//		IngresarDatosViewController ingresarDatosViewController = new IngresarDatosViewController(
//				true);
//		ingresarDatosViewController.setDelegate(delegate);
//		ingresarDatosViewController.setContratacionDelegate(delegate);
//		ingresarDatosViewController.setParentViewsController(SuiteAppDesac
//				.getInstance().getBmovilApplication()
//				.getBmovilViewsController());
//
//		// showViewController(IngresarDatosViewController.class);
//
//		SuiteAppDesac.getInstance().getBmovilApplication()
//				.getBmovilViewsController()
//				.setCurrentActivityApp(ownerController);
//
//		delegate.setOwnerController(ownerController);
//
//		delegate.validaCamposST(consultaEstatus.getCompaniaCelular(),
//				consultaEstatus.getNumTarjeta(), true);
//	}
//
//	//
//	//
//	// /**
//	// * Muestra la pantalla de ingresar datos de contratación.
//	// */
	public void showContratacion() {
//		ContratacionDelegate delegate = (ContratacionDelegate) getBaseDelegateForKey(ContratacionDelegate.CONTRATACION_DELEGATE_ID);
//		if (null == delegate) {
//			delegate = new ContratacionDelegate();
//			addDelegateToHashMap(ContratacionDelegate.CONTRATACION_DELEGATE_ID,
//					delegate);
//		}
//		showViewController(IngresarDatosViewController.class);



        //__________- invocacion al api de softoken ___________//
        SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/

        sftoken.onCreate(this.getCurrentViewController());
        sftoken.setIntentToReturn(SuiteAppDesac.getInstance().getCallBackContratacion());
        sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());


        //__________- invocacion al api de contratacion ___________//
        suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
                ServerCommons.SIMULATION,ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR,ServerCommons.DEVELOPMENT
        );
        //inicializa la clase
        InitContratacion initi=new InitContratacion(this.getCurrentViewController(), SuiteAppDesac.getInstance().getCallBackContratacion(), SuiteAppDesac.getInstance().getActivityForContratacion(),bs);
        //raliza el start del api
        initi.startContratacion(null, null, false);

	}
//
	public void showReactivacion(final ConsultaEstatus consultaEstatus) {
		//ReactivacionDelegate delegate = new ReactivacionDelegate();
		//suitebancomer.classes.gui.controllers.BaseViewsControllerCommons baseViewsController = SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController();
		//baseViewsController.addDelegateToHashMap(
		//		ReactivacionDelegate.REACTIVACION_DELEGATE_ID,
        //        delegate);
		//suitebancomer.classes.gui.controllers.BaseViewControllerCommons baseViewController = SuiteAppDesac.getInstance().getBmovilApplication().get.getBmovilViewController();
		// showViewController(ConfirmacionAutenticacionViewController.class, baseViewController);
		//showConfirmacionAutenticacionViewController(delegate, 0, 0, 0);
		BanderasServer bandera = new BanderasServer(ServerCommons.SIMULATION,
				ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT);
		InitReactivacion initReactivacion = new InitReactivacion(bandera, SuiteAppDesac.getInstance().getActivityReactivacion(),
				SuiteAppDesac.getInstance().getCallBackReactivacion(), SuiteAppDesac.getInstance().getActivityReactivacion());
		String instrumento = Session.getInstance(SuiteAppDesac.appContext).getSecurityInstrument();
		suitebancomercoms.aplicaciones.bmovil.classes.common.Session sessionApis = suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(SuiteAppDesac.appContext);
		sessionApis.setSecurityInstrument(instrumento);
		SuiteApp.setCallsAppDesactivada(Boolean.TRUE);
		initReactivacion.showReactivacion(consultaEstatus,
				SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController().estados);
	}

	/*public void showReactivacion(ConsultaEstatus consultaEstatus, BaseViewControllerCommons view) {
			suitebancomer.classes.gui.controllers.BaseViewsControllerCommons baseViewsController = SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController();
		ReactivacionDelegate delegate = (ReactivacionDelegate) getBaseDelegateForKey(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
		if (delegate != null) {
			removeDelegateFromHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
		}
		delegate = new ReactivacionDelegate(consultaEstatus);
		baseViewsController.addDelegateToHashMap(
				ReactivacionDelegate.REACTIVACION_DELEGATE_ID,
                delegate);
//		 showViewController(ConfirmacionAutenticacionViewController.class, view);
		
		showConfirmacionAutenticacionViewController((suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.DelegateBaseAutenticacion)delegate, 0, 0, 0, view);
	}*/


	/*public void showConfirmacionAutenticacionViewController(
			suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.DelegateBaseAutenticacion baseDelegate, int resIcon,
			int resTitle, int resSubtitle, BaseViewControllerCommons ownerController) {
		
		suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.BmovilViewsController baseViewRea = SuiteAppApiReactivacion.getInstance().getBmovilApplication().getBmovilViewsController();
		
		
		suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.ConfirmacionAutenticacionDelegate confirmacionDelegate = 
				(suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.ConfirmacionAutenticacionDelegate) 
				baseViewRea.getBaseDelegateForKey(suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		
		if (confirmacionDelegate != null) {
			baseViewRea.removeDelegateFromHashMap(suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		}
		confirmacionDelegate = new suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.ConfirmacionAutenticacionDelegate(
				baseDelegate);
		baseViewRea.addDelegateToHashMap(
				suitebancomer.aplicaciones.bmovil.classes.gui.delegates.reactivacion.ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID,
				confirmacionDelegate);
		
		showViewController(suitebancomer.aplicaciones.bmovil.classes.gui.controllers.reactivacion.ConfirmacionAutenticacionViewController.class, ownerController);
	}
	*/

//
//	//
//	// // /**
//	// * Muestra la pantalla de reactivación.
//	// */
//	public void showReactivacion(Reactivacion reactivacion) {
//		ReactivacionDelegate delegate = (ReactivacionDelegate) getBaseDelegateForKey(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
//		if (null != delegate)
//			removeDelegateFromHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
//
//		delegate = new ReactivacionDelegate();
//		delegate.setReactivacion(reactivacion);
//
//		addDelegateToHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID,
//				delegate);
//		showConfirmacionAutenticacionViewController(delegate, 0, 0, 0);
//	}
//
	public void showReactivacion(final ConsultaEstatus consultaEstatus,
			final String password) {
//		ReactivacionDelegate delegate = (ReactivacionDelegate) getBaseDelegateForKey(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
//		if (null != delegate)
//			removeDelegateFromHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID);
//		delegate = new ReactivacionDelegate(consultaEstatus);
//		addDelegateToHashMap(ReactivacionDelegate.REACTIVACION_DELEGATE_ID,
//				delegate);
//
//		delegate.setPassword(password);
//		delegate.setOwnerController(SuiteAppDesac.getInstance()
//				.getSuiteViewsController().getCurrentViewControllerApp());
//
//		Session session = Session.getInstance(SuiteAppDesac.appContext);
//		KeyStoreManager keyStore = session.getKeyStoreManager();
//
//		try {
//			keyStore.setEntry(Constants.USERNAME, " ");
//			keyStore.setEntry(Constants.SEED, " ");
//			keyStore.setEntry(Constants.CENTRO, " ");
//		} catch (KeyManagerStoreException e) {
//			// TODO Auto-generated catch block
//			if (Server.ALLOW_LOG)
//				e.printStackTrace();
//		}
//		delegate.borrarDatosDeSession();
		BanderasServer bandera = new BanderasServer(ServerCommons.SIMULATION,
				ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT);
		InitReactivacion initReactivacion = new InitReactivacion(bandera, SuiteAppDesac.getInstance().getActivityReactivacion(),
				SuiteAppDesac.getInstance().getCallBackReactivacion(), SuiteAppDesac.getInstance().getActivityReactivacion());
		String instrumento = Session.getInstance(SuiteAppDesac.appContext).getSecurityInstrument();
		suitebancomercoms.aplicaciones.bmovil.classes.common.Session sessionApis = suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(SuiteAppDesac.appContext);
		sessionApis.setSecurityInstrument(instrumento);
		initReactivacion.showReactivacion(consultaEstatus, password, SuiteAppDesac.getInstance().getEstados());
	}
//
//	// /**
//	// * Muestra la pantalla de definir contraseña la contratación.
//	// */
	 public void showDefinirPassword() {

         //__________- invocacion al api de softoken ___________//
         SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/

         sftoken.onCreate(this.getCurrentViewController());
         sftoken.setIntentToReturn(SuiteAppDesac.getInstance().getCallBackContratacion());
         sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());

         suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs = new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
                 ServerCommons.SIMULATION,ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR,ServerCommons.DEVELOPMENT
         );
         //inicializa la clase
         InitContratacion initi = new InitContratacion(this.getCurrentViewController(), null, new MenuSuiteViewController(), bs);
         //raliza el start del api
         initi.startContratacionDefinicionPassword();
     }
//	//
//	//
//	//
//	 /**
//	 * Muestra la pantalla de autenticacion softoken.
//	 * @param currentPassword
//	 * @param consultaEstatus
//	 */
//	 public void showAutenticacionSoftoken(ConsultaEstatus consultaEstatus,
//	 String currentPassword) {
//	 ContratacionSTDelegate delegate = (ContratacionSTDelegate)
//	 getBaseDelegateForKey(ContratacionSTDelegate.CONTRATACION_ST_DELEGATE_ID);
//	 if (null == delegate) {
//	 delegate = new ContratacionSTDelegate();
//	 delegate);
//	 }
//	
//	 delegate.setConsultaEstatus(consultaEstatus);
//	 //delegate.borrarDatosDeSession();
//	
//	
//	
//	 //delegate.setPassword(currentPassword);
//	 // delegate.setOwnerController(SuiteApp.getInstance()
//	 // .getSuiteViewsController().getCurrentViewControllerApp());
//	
//	 // showViewController(IngresoDatosSTViewController.class);
//	
//	 delegate.setOwnerController(SuiteAppDesac.getInstance()
//	 .getSuiteViewsController().getCurrentViewControllerApp());
//	
//	 //showViewController(IngresoDatosSTViewController.class);
//	
//	 String[] extrasKeys= {"login"};
//	 Object[] extras= {true};
//	
//	 showViewController(IngresoDatosSTViewController.class, 0, false,
//	 extrasKeys, extras);
//	
//	
//	
//	
//	
//	 }
	//
	//
	//
	//
	//
	//
	// /**
	// * Muestra la pantalla de ayuda para agregar una operación rápida.
	// */
	// public void showAyudaAgregarRapido() {
	// showViewController(AyudaAgregarRapidoViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla de configurar montos con los limites elegidos por
	// el
	// * usuario.
	// */
	// public void showConfigurarMontos(ConfigurarMontos cm) {
	// ConfigurarMontosDelegate delegate = (ConfigurarMontosDelegate)
	// getBaseDelegateForKey(ConfigurarMontosDelegate.CONFIGURAR_MONTOS_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new ConfigurarMontosDelegate();
	// addDelegateToHashMap(
	// ConfigurarMontosDelegate.CONFIGURAR_MONTOS_DELEGATE_ID,
	// delegate);
	// }
	// delegate.setConfigurarMontos(cm);
	// showViewController(ConfigurarMontosViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla de resultados
	// */
	// public void showResultadosAutenticacionViewController(
	// DelegateBaseOperacion delegateBaseOperacion, int resIcon,
	// int resTitle) {
	// ResultadosAutenticacionDelegate delegate =
	// (ResultadosAutenticacionDelegate)
	// getBaseDelegateForKey(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
	// if (delegate != null) {
	// removeDelegateFromHashMap(ResultadosDelegate.RESULTADOS_DELEGATE_ID);
	// }
	// delegate = new ResultadosAutenticacionDelegate(delegateBaseOperacion);
	// addDelegateToHashMap(
	// ResultadosAutenticacionDelegate.RESULTADOS_AUTENTICACION_DELEGATE_ID,
	// delegate);
	// showViewController(ResultadosAutenticacionViewController.class);
	// }
	//
	/**
	 * Muestra la pantalla para desbloqueo
	 * @param ownerController 
	 * 
	 * @param ce
	 */

	/*public void showDesbloqueo(ConsultaEstatus ce, BaseViewControllerCommons ownerController) {
		
		NuevaContraseniaDelegate delegate = (NuevaContraseniaDelegate) getBaseDelegateForKey(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID);
		if (delegate != null) {
			removeDelegateFromHashMap(NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID);
		}
		
		delegate = new NuevaContraseniaDelegate(ce);

		addDelegateToHashMap(
				NuevaContraseniaDelegate.NUEVA_CONTRASENIA_DELEGATE_ID,
				delegate);
		
		showViewController(mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.NuevaContraseniaViewController.class, ownerController);
	
	}*/

	//
	/**
	 * Muestra la confirmacion para quitar la suspension
	 * @param ownerController 
	 * 
	 * @param ce
	 */

	/*public void showQuitarSuspension(ConsultaEstatus ce, BaseViewControllerCommons ownerController) {
		
		mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseAutenticacion delegate = (QuitarSuspencionDelegate) getBaseDelegateForKey(QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID);
		if (delegate != null) {
			removeDelegateFromHashMap(QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID);
		}
		
		delegate = new QuitarSuspencionDelegate(ce);
		addDelegateToHashMap(
				QuitarSuspencionDelegate.QUITAR_SUSPENSION_DELEGATE_ID,
				(BaseDelegate) delegate);
		
		showConfirmacionAutenticacionViewController(delegate, 0, 0, 0, R.color.primer_azul, ownerController);
	}*/

	/*public void showConfirmacionAutenticacionViewController(
			mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.DelegateBaseAutenticacion baseDelegate, int resIcon,
			int resTitle, int resSubtitle, int resTitleColor, BaseViewControllerCommons ownerController) {
		
		mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionAutenticacionDelegate confirmacionDelegate = 
				(mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionAutenticacionDelegate) 
				getBaseDelegateForKey(mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		
		if (confirmacionDelegate != null) {
			removeDelegateFromHashMap(mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID);
		}
		confirmacionDelegate = new mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionAutenticacionDelegate(
				baseDelegate);
		addDelegateToHashMap(
				mtto.suitebancomer.aplicaciones.bmovil.classes.gui.delegates.ConfirmacionAutenticacionDelegate.CONFIRMACION_AUTENTICACION_DELEGATE_ID,
				confirmacionDelegate);
		
		showViewController(mtto.suitebancomer.aplicaciones.bmovil.classes.gui.controllers.ConfirmacionAutenticacionViewController.class, ownerController);
	}*/

	//
	// /**
	// * Muestra la pantalla de configurar alertas.
	// */
	// public void showConfigurarAlertas() {
	// ConfigurarAlertasDelegate delegate = (ConfigurarAlertasDelegate)
	// getBaseDelegateForKey(ConfigurarAlertasDelegate.CONFIGURAR_ALERTAS_DELEGATE_ID);
	// if (null == delegate) {
	// delegate = new ConfigurarAlertasDelegate();
	// addDelegateToHashMap(
	// ConfigurarAlertasDelegate.CONFIGURAR_ALERTAS_DELEGATE_ID,
	// delegate);
	// }
	//
	// showViewController(ConfigurarAlertasViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla de aplicación desactivada.
	// */

	/* public void showConsultaEstatusAplicacionDesactivada() {
	 MenuSuiteViewController menuSuiteViewController =
	 (MenuSuiteViewController) SuiteAppDesac
	 .getInstance().getSuiteViewsController()
	 .getCurrentViewControllerApp();
	 ConsultaEstatusAplicacionDesactivadaDelegate delegate =
	 (ConsultaEstatusAplicacionDesactivadaDelegate)
	 getBaseDelegateForKey(ConsultaEstatusAplicacionDesactivadaDelegate.CONSULTA_APLICACION_DESACTIVADA_DELEGATE_ID);
	 if (null == delegate) {
	 delegate = new ConsultaEstatusAplicacionDesactivadaDelegate();
	 addDelegateToHashMap(
	 ConsultaEstatusAplicacionDesactivadaDelegate.CONSULTA_APLICACION_DESACTIVADA_DELEGATE_ID,
	 delegate);
	 }
	
	 ConsultaEstatusAplicacionDesactivadaViewController viewController = new
	 ConsultaEstatusAplicacionDesactivadaViewController(
	 menuSuiteViewController, this);
	 viewController.setVisibility(View.GONE);
	 menuSuiteViewController
	 .setAplicacionDesactivadaViewController(viewController);
	 menuSuiteViewController.getBaseLayout().addView(viewController);
	
	 delegate.setOwnerController(menuSuiteViewController);
	 menuSuiteViewController.plegarOpcionAplicacionDesactivada();
	 }*/
	//

	 /**
	 * Muestra la pantalla de informaci�n contratacion con y sin Token.
	 */
//	 public void showAyudaContratacionTokens() {
//	 showViewController(AyudaContratacionTokensViewController.class);
//	 }
	
	 /**
	 * Muestra la pantalla de confirmaci�n autenticación para el estatus
	 PS.
	 */

	public void showContratacionAutenticacion() {

        //__________- invocacion al api de softoken ___________//
        SuiteAppApi sftoken=new SuiteAppApi();
		/*
			Setea los  parametros para simulacion, produccion y test
		*/

        sftoken.onCreate(this.getCurrentViewController());
        sftoken.setIntentToReturn(SuiteAppDesac.getInstance().getCallBackContratacion());
        sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());


        suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
                ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT
        );
        //inicializa la clase
        InitContratacion initi=new InitContratacion(this.getCurrentViewController(), SuiteAppDesac.getInstance().getCallBackContratacion(), SuiteAppDesac.getInstance().getActivityForContratacion(),bs);
        //raliza el start del api
        initi.startContratacionAutenticacion();
	}

	//
	// /**
	// * Muestra la pantalla de confirmacion autenticacion
	// */
	// public void showContratacionAutenticacion(DelegateBaseAutenticacion
	// autenticacionDelegate) {
	// ContratacionAutenticacionDelegate delegate =
	// (ContratacionAutenticacionDelegate)
	// getBaseDelegateForKey(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
	//
	// if (delegate != null){
	// removeDelegateFromHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID);
	// }
	//
	// delegate = new ContratacionAutenticacionDelegate(autenticacionDelegate);
	// addDelegateToHashMap(ContratacionAutenticacionDelegate.CONTRATACION_AUTENTICACION_DELEGATE_ID,
	// delegate);
	// showViewController(ContratacionAutenticacionViewController.class);
	// }
	//
	// /**
	// * Muestra los terminos y condiciones de uso.
	// *
	// * @param terminosDeUso
	// * Terminos de uso.
	// */
	// public void showTerminosDeUso(String terminosDeUso) {
	// showViewController(TerminosCondicionesViewController.class, 0, false,
	// new String[] { Constants.TERMINOS_DE_USO_EXTRA },
	// new Object[] { terminosDeUso });
	// }
	//
	// public void showActivacion(final ConsultaEstatus ce, final String
	// password,
	// final boolean appActivada) {
	// if (appActivada) {
	// SuiteApp.getInstance()
	// .getSuiteViewsController()
	// .getCurrentViewControllerApp()
	// .showInformationAlert(
	// R.string.bmovil_activacion_alerta_reactivar,
	// new OnClickListener() {
	//
	// @Override
	// public void onClick(DialogInterface dialog,
	// int which) {
	// ActivacionDelegate aDelegate = (ActivacionDelegate)
	// getBaseDelegateForKey(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
	// if (aDelegate != null)
	// removeDelegateFromHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
	// aDelegate = new ActivacionDelegate(ce,
	// password);
	// aDelegate.borrarDatos();
	// addDelegateToHashMap(
	// ActivacionDelegate.ACTIVACION_DELEGATE_ID,
	// aDelegate);
	// }
	// });
	// }
	// }
	//
	 public void showActivacion(final ConsultaEstatus ce,final String password) {
	// ActivacionDelegate aDelegate = (ActivacionDelegate)
	// getBaseDelegateForKey(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
	// if (aDelegate != null)
	// removeDelegateFromHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
	// aDelegate = new ActivacionDelegate(ce, password);
	//
	// addDelegateToHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID,
	// aDelegate);
	// //AMZ
	// TrackingHelper.trackState("activacion", estados);
	// //AMZ
	// showViewController(ActivacionViewController.class);
		 InitActivacion initActivacion = new InitActivacion(new BanderasServer(ServerCommons.SIMULATION,
				 ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT),
				 SuiteAppDesac.getInstance().getActivityActivacion(),
				 SuiteAppDesac.getInstance().getCallBackActivacion(),
				 SuiteAppDesac.getInstance().getActivityActivacion());
		 String instrumento = Session.getInstance(SuiteAppDesac.appContext).getSecurityInstrument();
		 suitebancomercoms.aplicaciones.bmovil.classes.common.Session sessionApis = suitebancomercoms.aplicaciones.bmovil.classes.common.Session.getInstance(SuiteAppDesac.appContext);
		 sessionApis.setSecurityInstrument(instrumento);
		 SuiteApp.setCallsAppDesactivada(Boolean.TRUE);


		 initActivacion.showActivacion(ce, password, estados);
	}
	//
	// public void showActivacion(Reactivacion reactivacion) {
	// ActivacionDelegate aDelegate = (ActivacionDelegate)
	// getBaseDelegateForKey(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
	// if (aDelegate != null)
	// removeDelegateFromHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID);
	// aDelegate = new ActivacionDelegate(reactivacion);
	// addDelegateToHashMap(ActivacionDelegate.ACTIVACION_DELEGATE_ID,
	// aDelegate);
	// //AMZ
	// TrackingHelper.trackState("activacion", estados);
	// //AMZ
	//
	// showViewController(ActivacionViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla para consultar rápidos.
	// */
	// public void showConsultarRapidas() {
	// ConsultaRapidosDelegate delegate = (ConsultaRapidosDelegate)
	// getBaseDelegateForKey(ConsultaRapidosDelegate.CONSULTA_RAPIDOS_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new ConsultaRapidosDelegate();
	// addDelegateToHashMap(
	// ConsultaRapidosDelegate.CONSULTA_RAPIDOS_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(ConsultaRapidosViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla de configurrar correo.
	// */
	// public void showConfigurarCorreo() {
	// ConfigurarCorreoDeleate delegate = (ConfigurarCorreoDeleate)
	// getBaseDelegateForKey(ConfigurarCorreoDeleate.CONFIGURAR_CORREO_DELEGATE_ID);
	// if (null == delegate) {
	// delegate = new ConfigurarCorreoDeleate();
	// addDelegateToHashMap(
	// ConfigurarCorreoDeleate.CONFIGURAR_CORREO_DELEGATE_ID,
	// delegate);
	// }
	// showViewController(ConfigurarCorreoViewController.class);
	// }
	//
	// //SPEI
	//
	// /**
	// * Shows the account association screen.
	// */
	// public void showAsociacionCuentaTelefono() {
	// MantenimientoSpeiMovilDelegate delegate =
	// (MantenimientoSpeiMovilDelegate)getBaseDelegateForKey(MantenimientoSpeiMovilDelegate.DELEGATE_ID);
	// if(null == delegate) {
	// delegate = new MantenimientoSpeiMovilDelegate();
	// addDelegateToHashMap(MantenimientoSpeiMovilDelegate.DELEGATE_ID,
	// delegate);
	// }
	// showViewController(ConsultaCuentaSpeiMovilViewController.class);
	// }
	//
	// /**
	// * Shows the screen to associate an account with a phone.
	// */
	// public void showDetailAsociacionCuentaTelefono() {
	// MantenimientoSpeiMovilDelegate delegate =
	// (MantenimientoSpeiMovilDelegate)getBaseDelegateForKey(MantenimientoSpeiMovilDelegate.DELEGATE_ID);
	// if(null == delegate) {
	// delegate = new MantenimientoSpeiMovilDelegate();
	// addDelegateToHashMap(MantenimientoSpeiMovilDelegate.DELEGATE_ID,
	// delegate);
	// }
	// showViewController(DetalleCuentaSpeiMovilViewController.class);
	// }
	//
	//
	// //Termina SPEI
	//
	//
	//
	//
	// /**
	// * Muestra la pantalla de enviar correo
	// */
	// public void showEnviarCorreo(ResultadosDelegate resultadosDelegate) {
	// EnviarCorreoDelegate delegate = (EnviarCorreoDelegate)
	// getBaseDelegateForKey(EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID);
	// if (null == delegate) {
	// delegate = new EnviarCorreoDelegate();
	// addDelegateToHashMap(
	// EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID, delegate);
	// }
	// delegate.setResultadosDelegate(resultadosDelegate);
	// //delegate.setOperacionDelegate(resultadosDelegate.getOperationDelegate());
	// showViewController(EnviarCorreoViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla de enviar correo
	// */
	// public void showEnviarCorreo(DepositosRecibidosDelegate
	// depositosDelegate) {
	// EnviarCorreoDelegate delegate = (EnviarCorreoDelegate)
	// getBaseDelegateForKey(EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID);
	// if (null == delegate) {
	// delegate = new EnviarCorreoDelegate();
	// addDelegateToHashMap(
	// EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID, delegate);
	// }
	// //delegate.setOperacionDelegate(depositosDelegate);
	// showViewController(EnviarCorreoViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla de enviar correo
	// */
	// public void showEnviarCorreo(ObtenerComprobantesDelegate
	// obtenerComprobantesDelegate) {
	// EnviarCorreoDelegate delegate = (EnviarCorreoDelegate)
	// getBaseDelegateForKey(EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID);
	// if (null == delegate) {
	// delegate = new EnviarCorreoDelegate();
	// addDelegateToHashMap(
	// EnviarCorreoDelegate.ENVIAR_CORREO_DELEGATE_ID, delegate);
	// }
	// //delegate.setOperacionDelegate(obtenerComprobantesDelegate);
	// showViewController(EnviarCorreoViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla de confirmacion para transferencias.
	// *
	// * @param delegateBaseOperacion
	// * EL DelegateBaseOperacion que manda a llamar la pantalla de
	// * confirmaci�n.
	// */
	// public void showConfirmacionRegistro(
	// DelegateBaseOperacion delegateBaseOperacion) {
	// // El delegado de confirmaci�n se crea siempre, esto ya que el delegado
	// // que contiene el internamente cambia segun quien invoque este método.
	// ConfirmacionRegistroDelegate delegate = (ConfirmacionRegistroDelegate)
	// getBaseDelegateForKey(ConfirmacionRegistroDelegate.CONFIRMACION_REGISTRO_DELEGATE_DELEGATE_ID);
	// if (delegate != null) {
	// removeDelegateFromHashMap(ConfirmacionRegistroDelegate.CONFIRMACION_REGISTRO_DELEGATE_DELEGATE_ID);
	// }
	// delegate = new ConfirmacionRegistroDelegate(delegateBaseOperacion);
	// addDelegateToHashMap(
	// ConfirmacionRegistroDelegate.CONFIRMACION_REGISTRO_DELEGATE_DELEGATE_ID,
	// delegate);
	// showViewController(ConfirmacionRegistroViewController.class);
	// }
	//
	// /**
	// *
	// * @param delegateOp
	// */
	// public void showRegistrarOperacion(DelegateBaseAutenticacion delegateOp)
	// {
	// //SPEI
	// showRegistrarOperacion(delegateOp, false);
	// }
	// public void showRegistrarOperacion(DelegateBaseAutenticacion delegateOp,
	// boolean showHelpImage){
	// //Termina SPEI
	//
	// RegistrarOperacionDelegate delegate = (RegistrarOperacionDelegate)
	// getBaseDelegateForKey(RegistrarOperacionDelegate.REGISTRAR_OPERACION_DELEGATE_ID);
	// if (delegate != null)
	// removeDelegateFromHashMap(RegistrarOperacionDelegate.REGISTRAR_OPERACION_DELEGATE_ID);
	// delegate = new RegistrarOperacionDelegate(delegateOp);
	// addDelegateToHashMap(
	// RegistrarOperacionDelegate.REGISTRAR_OPERACION_DELEGATE_ID,
	// delegate);
	// //SPEI
	//
	// String[] extrasKeys = { "showHelpImage" };
	// Object[] extrasValues = { Boolean.valueOf(showHelpImage) };
	// showViewController(RegistrarOperacionViewController.class, 0, false,
	// extrasKeys, extrasValues);
	// }
	//
	// /**
	// * Shows the confirmation screen for the SPEI maintenance operations.
	// * @param operationDelegate The delegate of the current operation.
	// */
	// public void showSpeiConfirmation(DelegateBaseAutenticacion
	// operationDelegate, boolean showHelpImage) {
	// ConfirmacionAsignacionSpeiDelegate confirmDelegate;
	// confirmDelegate =
	// (ConfirmacionAsignacionSpeiDelegate)getBaseDelegateForKey(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID);
	// if(confirmDelegate != null)
	// removeDelegateFromHashMap(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID);
	// confirmDelegate = new
	// ConfirmacionAsignacionSpeiDelegate(operationDelegate);
	//
	// addDelegateToHashMap(ConfirmacionAsignacionSpeiDelegate.CONFIRMATION_SPEI_DELEGATE_ID,
	// confirmDelegate);
	//
	// String[] keys = new String[] { "showHelpImage" };
	// Object[] values = new Object[] { Boolean.valueOf(showHelpImage) };
	//
	// showViewController(ConfirmacionAsignacionSpeiViewController.class, 0,
	// false, keys, values);
	// }
	//
	//
	//
	// /**
	// * Shows the terms and conditions screen.
	// * @param terminosDeUso Terms and conditions HTML.
	// * @param title The title resource identifier.
	// * @param icon The icon recource identifier.
	// */
	// public void showTermsAndConditions(String termsHtml, int title, int icon)
	// {
	// String[] keys = {Constants.TERMINOS_DE_USO_EXTRA, Constants.TITLE_EXTRA,
	// Constants.ICON_EXTRA};
	// Object[] values = {termsHtml, Integer.valueOf(title),
	// Integer.valueOf(icon)};
	//
	// showViewController(TerminosCondicionesViewController.class, 0, false,
	// keys, values);
	// }
	//
	// public void showOtrosBBVASpeiViewController(Object obj, boolean
	// esExpress, boolean esTDC) {
	// OtrosBBVADelegate delegate = (OtrosBBVADelegate)
	// getBaseDelegateForKey(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID);
	//
	// if (delegate == null) {
	// delegate = new OtrosBBVADelegate(esExpress, esTDC);
	// addDelegateToHashMap(OtrosBBVADelegate.OTROS_BBVA_DELEGATE_ID, delegate);
	// }
	//
	// delegate.setEsExpress(esExpress);
	// delegate.setEsTDC(esTDC);
	// delegate.setSpei(true);
	// delegate.setTipoOperacion((Constants.Operacion)obj);
	// delegate.setEsFrecuente(false);
	// //Termina SPEI
	// showViewController(RegistrarOperacionViewController.class);
	// }
	// public void touchMenu(){
	// String eliminado="";
	// if (estados.size() == 2){
	// eliminado = estados.remove(1);
	// }else{
	// int tam = estados.size();
	// for (int i=1;i<tam;i++){
	// eliminado = estados.remove(1);
	// }
	// }
	// }
	//
	public void touchAtras() {

		int ultimo = estados.size() - 1;

		if (ultimo >= 0) {
			String ult = estados.get(ultimo);
			if (ult == "reactivacion" || ult == "activacion"
					|| ult == "menu token" || ult == "contratacion datos"
					|| ult == "activacion datos") {
				estados.clear();
			} else {
				estados.remove(ultimo);
			}
		}
	}



	// //One click
	//
	// /*
	// * Muestra la pantalla de detalle oferta ILC
	// */
	// public void showDetalleILC(OfertaILC ofertaILC, Promociones promocion ) {
	// // TODO Auto-generated method stub
	// DetalleOfertaILCDelegate delegate = (DetalleOfertaILCDelegate)
	// getBaseDelegateForKey(DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE);
	// if (null != delegate)
	// removeDelegateFromHashMap(DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE);
	// delegate = new DetalleOfertaILCDelegate(ofertaILC, promocion);
	// addDelegateToHashMap(
	// DetalleOfertaILCDelegate.DETALLE_OFERTA_DELEGATE, delegate);
	//
	// showViewController(DetalleILCViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de exito oferta ILC
	// */
	// public void showExitoILC(AceptaOfertaILC aceptaOfertaILC, OfertaILC
	// ofertaILC ) {
	// // TODO Auto-generated method stub
	// ExitoILCDelegate delegate = (ExitoILCDelegate)
	// getBaseDelegateForKey(ExitoILCDelegate.EXITO_OFERTA_DELEGATE);
	// if (null != delegate)
	// removeDelegateFromHashMap(ExitoILCDelegate.EXITO_OFERTA_DELEGATE);
	// delegate = new ExitoILCDelegate(aceptaOfertaILC,ofertaILC );
	// addDelegateToHashMap(
	// ExitoILCDelegate.EXITO_OFERTA_DELEGATE, delegate);
	//
	// showViewController(ExitoOfertaILCViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de detalle oferta ILC
	// */
	// public void showDetalleEFI(OfertaEFI ofertaEFI, Promociones promocion ) {
	// // TODO Auto-generated method stub
	// DetalleOfertaEFIDelegate delegate = (DetalleOfertaEFIDelegate)
	// getBaseDelegateForKey(DetalleOfertaEFIDelegate.DETALLE_OFERTA_EFI_DELEGATE);
	// if (null == delegate) {
	// delegate = new DetalleOfertaEFIDelegate(ofertaEFI, promocion);
	// addDelegateToHashMap(
	// DetalleOfertaEFIDelegate.DETALLE_OFERTA_EFI_DELEGATE, delegate);
	// }else{
	// delegate.setOfertaEFI(ofertaEFI);
	// }
	// showViewController(DetalleEFIViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de exito oferta ILC
	// */
	// public void showExitoEFI(AceptaOfertaEFI aceptaOfertaEFI, OfertaEFI
	// ofertaEFI) {
	// // TODO Auto-generated method stub
	// ExitoEFIDelegate delegate = (ExitoEFIDelegate)
	// getBaseDelegateForKey(ExitoEFIDelegate.EXITO_OFERTA_EFI_DELEGATE);
	// if (null != delegate)
	// removeDelegateFromHashMap(ExitoEFIDelegate.EXITO_OFERTA_EFI_DELEGATE);
	// delegate = new ExitoEFIDelegate(aceptaOfertaEFI,ofertaEFI );
	// addDelegateToHashMap(
	// ExitoEFIDelegate.EXITO_OFERTA_EFI_DELEGATE, delegate);
	//
	// showViewController(ExitoOfertaEFIViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de importe a modificar EFI
	// */
	// public void showModificaImporteEFI(OfertaEFI ofertaEFI) {
	// // TODO Auto-generated method stub
	// ModificarImporteDelegate delegate = (ModificarImporteDelegate)
	// getBaseDelegateForKey(ModificarImporteDelegate.MODIFICAR_IMPORTE_DELEGATE_ID);
	// if (null != delegate)
	// removeDelegateFromHashMap(ModificarImporteDelegate.MODIFICAR_IMPORTE_DELEGATE_ID);
	// delegate = new ModificarImporteDelegate(ofertaEFI);
	// addDelegateToHashMap(
	// ModificarImporteDelegate.MODIFICAR_IMPORTE_DELEGATE_ID, delegate);
	//
	// showViewController(ModificarImporteEFIViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de detalle oferta consumo
	// */
	// public void showDetalleConsumo(OfertaConsumo ofertaConsumo, Promociones
	// promocion ) {
	// // TODO Auto-generated method stub
	// DetalleOfertaConsumoDelegate delegate = (DetalleOfertaConsumoDelegate)
	// getBaseDelegateForKey(DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE);
	// if (null == delegate) {
	// delegate = new DetalleOfertaConsumoDelegate(ofertaConsumo, promocion);
	// addDelegateToHashMap(
	// DetalleOfertaConsumoDelegate.DETALLE_OFERTA_CONSUMO_DELEGATE, delegate);
	// }else{
	// delegate.setOfertaConsumo(ofertaConsumo);
	// }
	// showViewController(DetalleConsumoViewController.class);
	// }
	//
	// /*
	// * Muestra la pantalla de contrato oferta consumo
	// */
	// public void showContratoConsumo(OfertaConsumo ofertaConsumo, Promociones
	// promocion) {
	// // TODO Auto-generated method stub
	// ContratoOfertaConsumoDelegate delegate = (ContratoOfertaConsumoDelegate)
	// getBaseDelegateForKey(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE);
	// if (null != delegate)
	// removeDelegateFromHashMap(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE);
	// delegate = new ContratoOfertaConsumoDelegate(ofertaConsumo,promocion);
	// delegate.setTipoOperacion(Operacion.oneClickBmovilConsumo);
	// addDelegateToHashMap(ContratoOfertaConsumoDelegate.CONTRATO_OFERTA_CONSUMO_DELEGATE,
	// delegate);
	// showViewController(ContratoConsumoViewController.class);
	// }
	//
	// /**
	// * Muestra la poliza consumo.
	// *
	// * @param terminosDeUso
	// * Terminos de uso.
	// */
	// public void showPoliza(String poliza) {
	// showViewController(CaracteristicasPolizaViewController.class, 0, false,
	// new String[] { Constants.TERMINOS_DE_USO_EXTRA },
	// new Object[] { poliza });
	// }
	//
	// /**
	// * Muestra formato domiciliacion
	// *
	// * @param terminosDeUso
	// * Terminos de uso.
	// */
	//
	// public void showformatodomiciliacion(String terminos) {
	// showViewController(CaracteristicasPolizaViewController.class, 0, false,
	// new String[] { Constants.DOMICILIACION_CONSUMO },
	// new Object[] { terminos });
	// }
	//
	// /**
	// * Muestra contrato consumo
	// *
	// * @param terminosDeUso
	// * Terminos de uso.
	// */
	//
	// public void showContrato(String contrato) {
	// showViewController(CaracteristicasPolizaViewController.class, 0, false,
	// new String[] { Constants.CONTRATO_CONSUMO },
	// new Object[] { contrato });
	// }
	//
	// /**
	// * Muestra terminos y condiciones consumo.
	// *
	// * @param terminosDeUso
	// * Terminos de uso.
	// */
	//
	// public void showTerminosConsumo(String terminos) {
	// showViewController(CaracteristicasPolizaViewController.class, 0, false,
	// new String[] { Constants.TERMINOS_DE_USO_CONSUMO },
	// new Object[] { terminos });
	// }
	//
	// /*
	// * Muestra la pantalla de exito oferta consumo
	// */
	// public void showExitoConsumo(AceptaOfertaConsumo aceptaOfertaConsumo,
	// OfertaConsumo ofertaConsumo) {
	// // TODO Auto-generated method stub
	// ExitoOfertaConsumoDelegate delegate = (ExitoOfertaConsumoDelegate)
	// getBaseDelegateForKey(ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE);
	// if (null == delegate) {
	// delegate = new
	// ExitoOfertaConsumoDelegate(aceptaOfertaConsumo,ofertaConsumo);
	// addDelegateToHashMap(
	// ExitoOfertaConsumoDelegate.EXITO_OFERTA_CONSUMO_DELEGATE, delegate);
	// }else{
	// //delegate.setOfertaConsumo(ofertaConsumo);
	// }
	// showViewController(ExitoOfertaConsumoViewController.class);
	// }
	// /**
	// * Muestra la pantalla para consultar Interbancarios.
	// */
	// public void showConsultarInterbancarios() {
	// ConsultaInterbancariosDelegate delegate =
	// (ConsultaInterbancariosDelegate)
	// getBaseDelegateForKey(ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID);
	//
	// if (delegate == null) {
	// delegate = new ConsultaInterbancariosDelegate();
	// addDelegateToHashMap(
	// ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID,
	// delegate);
	// }
	// //delegate.resetPeriodo();
	// delegate.resetPeriodoConsultas();
	// showViewController(ConsultaInterbancariosViewController.class);
	// }
	//
	// /**
	// * Muestra la pantalla detalle interbancaria
	// */
	// public void showDetalleInterbancaria(ConsultaInterbancariosDelegate
	// delegate) {
	// // TODO Auto-generated method stub
	// if (delegate != null)
	// removeDelegateFromHashMap(ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID);
	// addDelegateToHashMap(
	// ConsultaInterbancariosDelegate.CONSULTA_INTERBANCARIOS_DELEGATE_ID,
	// delegate);
	//
	// showViewController(DetalleMoviemientoSpeiViewController.class);
	// }
	//
	// public void showPagoTDCCuenta(Account cuenta) {
	// PagoTdcDelegate delegate = (PagoTdcDelegate)
	// getBaseDelegateForKey(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new PagoTdcDelegate();
	// addDelegateToHashMap(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID,
	// delegate);
	// }
	// delegate.setCuentaActual(cuenta);
	// delegate.setCuentaOrigenSeleccionada(null);
	// showViewController(PagoTdcViewController.class);
	// }
	//
	// public void showPagoTDCCuenta(Account cuenta, Account cuentaOrigen) {
	// PagoTdcDelegate delegate = (PagoTdcDelegate)
	// getBaseDelegateForKey(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID);
	// if (delegate == null) {
	// delegate = new PagoTdcDelegate();
	// addDelegateToHashMap(PagoTdcDelegate.PAGO_TDC_DELEGATE_ID,
	// delegate);
	// }
	// delegate.setCuentaActual(cuenta);
	// delegate.setCuentaOrigenSeleccionada(cuentaOrigen);
	// showViewController(PagoTdcViewController.class);
	// }
	//
	// public void showRetiroSinTarjetaViewController() {
	// AltaRetiroSinTarjetaDelegate delegate = (AltaRetiroSinTarjetaDelegate)
	// getBaseDelegateForKey(AltaRetiroSinTarjetaDelegate.ALTA_RETIRO_SINTAR_DELEGATE_ID);
	// if (null == delegate) {
	// delegate = new AltaRetiroSinTarjetaDelegate();
	// addDelegateToHashMap(AltaRetiroSinTarjetaDelegate.ALTA_RETIRO_SINTAR_DELEGATE_ID,
	// delegate);
	// }
	//
	// showViewController(AltaRetiroSinTarjetaViewController.class);
	//
	// }
	//
	//


	/*public void showActivacion(ConsultaEstatus ce, String password,
			BaseViewControllerCommons controladorPadre) {

		suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate aDelegate = (suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate) getBaseDelegateForKey(suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate.ACTIVACION_DELEGATE_ID);
		if (aDelegate != null)
			removeDelegateFromHashMap(suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate.ACTIVACION_DELEGATE_ID);

		aDelegate = new suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate(
				ce, password);

		addDelegateToHashMap(
						suitebancomer.activacion.aplicaciones.bmovil.classes.gui.delegates.ActivacionDelegate.ACTIVACION_DELEGATE_ID,
						aDelegate);
		// AMZ
		TrackingHelper.trackState("activacion", estados);
		// AMZ
		showViewController(
				suitebancomer.activacion.aplicaciones.bmovil.classes.gui.controllers.ActivacionViewController.class,
				controladorPadre);
	}*/


	/*public void showMenuHamburguesa(String[] arr1, String[] arr2, BaseViewControllerCommons controladorPadre)
	{	
		showViewController(MenuHamburguesaViewsControllers.class,0,false,arr1,arr2, controladorPadre);
	} */

	

}