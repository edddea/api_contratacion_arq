package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.desactivada;

import java.util.ArrayList;
import java.util.Hashtable;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import bancomer.api.common.commons.Constants.TipoOtpAutenticacion;
import bancomer.api.common.commons.Constants.TipoInstrumento;
import suitebancomer.aplicaciones.bmovil.classes.io.desactivada.Server;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomer.aplicaciones.bmovil.classes.model.Account;
import suitebancomercoms.aplicaciones.bmovil.classes.model.Rapido;
import suitebancomercoms.aplicaciones.bmovil.classes.model.SolicitarAlertasData;

import suitebancomer.classes.gui.controllers.desactivada.BaseViewController;
import suitebancomer.classes.gui.delegates.desactivada.BaseDelegate;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.bancomer.mbanking.desactivada.R;
import com.bancomer.mbanking.desactivada.SuiteAppDesac;

public class DelegateBaseOperacion extends BaseDelegate {
	
	/**
     * Parametro para mostrar la opcion de menu SMS
     */
    public final static int SHOW_MENU_SMS = 2;
    
    /**
     * Parametro para mostrar la opcion de menu Correo electrónico
     */
    public final static int SHOW_MENU_EMAIL = 4;
    
    /**
     * Parametro para mostrar la opcion de menu PDF
     */
    public final static int SHOW_MENU_PDF = 8;
    
    /**
     * Parametro para mostrar la opcion de menu Frecuente
     */
    public final static int SHOW_MENU_FRECUENTE = 16;
    
    /**
     * Parametro para mostrar la opcion de menu Rapida
     */
    public final static int SHOW_MENU_RAPIDA = 32;
    
    /**
     * Parametro para mostrar la opcion de menu Rapida
     */
    public final static int SHOW_MENU_BORRAR = 64;
    
    /**
     * Respuesta del servidor para mostrar errores de transferencias
     */
    protected ServerResponse transferErrorResponse = null;
    protected SolicitarAlertasData sa;
    
    
	
	public ServerResponse getTransferErrorResponse() {
		return transferErrorResponse;
	}

	public void setTransferErrorResponse(final ServerResponse transferErrorResponse) {
		this.transferErrorResponse = transferErrorResponse;
	}
	
	

	public SolicitarAlertasData getSa() {
		return sa;
	}

	public void setSa(final SolicitarAlertasData sa) {
		this.sa = sa;
	}

	public String getTextoAyudaInstrumentoSeguridad(final TipoInstrumento tipoInstrumento) { return ""; }
	
	public String getEtiquetaCampoNip() { return ""; }
	
	public String getEtiquetaCampoContrasenia() { return ""; }
	
	public String getEtiquetaCampoOCRA() { return ""; }
	
	public String getEtiquetaCampoDP270() { return ""; }
	
	public String getEtiquetaCampoSoftokenActivado() { return ""; }
	
	public String getEtiquetaCampoSoftokenDesactivado() { return ""; }
	
	public String getEtiquetaCampoCVV() { return ""; }
	
	public int getOpcionesMenuResultados() { return 0; }
	
	public String getTextoSMS() { return ""; }
	
	public String getTextoEmail() { return ""; }
	
	public String getTextoPDF() { return ""; }
	
	public String getTituloTextoEspecialResultados() { return ""; }

	public String getTextoEspecialResultados() { return ""; }
	
	public String getTextoPantallaResultados() { return ""; }
	
	public boolean borraRapido() { return false; }
	
	public String getTextoTituloResultado() { return ""; }
	
	public int getColorTituloResultado() { return android.R.color.black; }
	
	public String getTextoTituloConfirmacion() { return ""; }
	
	public ArrayList<Object> getDatosTablaConfirmacion() { return null; }
	
	public ArrayList<Object> getDatosTablaResultados() { return null; }
	
	public String getTextoAyudaNIP() { return ""; }
	
	public String getTextoAyudaCVV() { return ""; }
	
	/**
	 * Consult if the password field should be visible.
	 * @param tipoOperacion The operation type.
	 * @param perfil The user profile.
	 * @return A Boolean with the flag, or null if an error occurred.
	 * @throws Exception
	 */
	public boolean mostrarContrasenia() { return false; }
	
	/**
	 * Consult the type of token that should be shown.
	 * @param tipoOperacion The operation type.
	 * @param perfil The user profile.
	 * @return The token type.
	 * @throws Exception
	 */
	public TipoOtpAutenticacion tokenAMostrar() { return null; }
	
	/**
	 * Consult if the NIP field should be visible.
	 * @param tipoOperacion The operation type.
	 * @param perfil The user profile.
	 * @return A Boolean with the flag, or null if an error occurred.
	 * @throws Exception
	 */
	public boolean mostrarNIP() { return false; }
	
	public boolean mostrarCVV() { return false; }
	
	public ArrayList<Object> getDatosTablaAltaFrecuentes() { return null; }
	
	public ArrayList<String> getDatosTablaAltaRapidos() { return null; }
	
	public int getNombreImagenEncabezado() { return -1; }
	
	public int getTextoEncabezado() { return -1; }
	
	public String getTextoBotonOperacionNueva() { return ""; }
	
	public String getTextoAyudaResultados(){ return ""; }
	
	public void consultarFrecuentes(final String tipoFrecuente) {}
	
	public ArrayList<Object> getDatosHeaderTablaFrecuentes() { return null; }
	
	public ArrayList<Object> getDatosTablaFrecuentes() { return null; }
	
//	public void realizaOperacion(ConfirmacionViewController confirmacionViewController, String contrasenia,
//								 String nip, String token, String campoTarjeta, String cvv) { };
//								 
//	public void realizaOperacion(ConfirmacionViewController confirmacionViewController, String contrasenia,
//								 String nip, String token, String campoTarjeta) { };
								 
	public Hashtable<String, String> getParametrosAltaFrecuentes(){return null;}
	
	public void operarFrecuente(final int indexSelected){}
	
	public void eliminarFrecuente(final int indexSelected){}
	/**
	 * metodo para realizar la Acci�n del botón pantalla de resultados autenticación
	 * 
	 */
	protected void accionBotonResultados(){}
	protected int getImagenBotonResultados(){return 0;}
	
	/**
	 * Operar un r�pido.
	 * @param rapido El r�pido a operar.
	 */
	public void operarRapido(final Rapido rapido) {return;}
	
	/**
	 * Lista de datos para mostrar en la pantalla de RegistarOperacion
	 * @return ArrayList con los datos a mostrar en la pantalla de RegistarOperacion
	 */
	protected ArrayList<Object> getDatosRegistroOp() {return null;}
	
	/**
	 * @return  ArrayList con los datos a mostrar en la tabla de RegistroOperacionExitoso
	 */
	public ArrayList<Object> getDatosRegistroExitoso() {return null;}
	
	/**
	 * Invoca realizar el registro de operacion a cada operacion que la haya requerido.
	 * @param rovc referencia de la pantalla de RegistrarOperacion
	 * @param token Valor ingresado por el usuario, si este fue requerido
	 */
//	protected void realizaOperacion(RegistrarOperacionViewController rovc, String token){}
//
//	/**
//	 * Invoca la operacion para la confirmacion con Registro
//	 * @param viewController
//	 * @param contrasena
//	 * @param nip
//	 * @param asm
//	 */
//	public void realizaOperacion(ConfirmacionRegistroViewController viewController,	String contrasena, String nip, String asm) {};
//	
	
	
	/**
	* Invoca la operacion para la confirmacion con Registro
	* @param viewController
	* @param contrasena
	* @param nip
	* @param asm
	* @param cvv
	*/
	//public void realizaOperacion(ConfirmacionRegistroViewController viewController,	String contrasena, String nip, String asm, String cvv) {};
	
	
	
	
	/**
	 * Obtiene los ultimos 5 digitos de la cuenta para el registro de operación.
	 */
	public String getNumeroCuentaParaRegistroOperacion() { return "00000"; }
	
	/**
	 * Carga una OTP del api de Softtoken.
	 * @param tipoOTP El tipo de OTP a cargar (Código o Registro).
	 * @return La OTP generada por Softtoken, null en caso llamar este método sin tener Softtoken activado o de alg�n error.
	 */
//	public String loadOtpFromSofttoken(Constants.TipoOtpAutenticacion tipoOTP) {
//		return loadOtpFromSofttoken(tipoOTP, null);
//	}
	
	/**
	 * Carga una OTP del api de Softtoken.
	 * @param tipoOTP El tipo de OTP a cargar (Código o Registro).
	 * @param opDelegate El delegado de la operación actual, es requerido para obtener los 5 digitos necesarios para una OTP de tipo Registro. 
	 * En caso de estar seguro de que el tipo de OTP es Código este par�metro puede ser nulo. 
	 * @return La OTP generada por Softtoken, null en caso llamar este método sin tener Softtoken activado o de alg�n error.
	 */
//	public String loadOtpFromSofttoken(Constants.TipoOtpAutenticacion tipoOTP, DelegateBaseOperacion opDelegate) {
//		String otp = null;
//		
//		// Si se llama a este método sin que Softtoken esté activado se regresa null.
//		if(!SuiteApp.getSofttokenStatus())
//			return null;
//		
//		// Verifica y carga el token seg�n el tipo que corresponda.
//		if(TipoOtpAutenticacion.codigo == tipoOTP)
//			otp = GeneraOTPSTDelegate.generarOtpTiempo();
//		else if(TipoOtpAutenticacion.registro == tipoOTP)
//			otp = (null == opDelegate) ? null : GeneraOTPSTDelegate.generarOtpChallenge(opDelegate.getNumeroCuentaParaRegistroOperacion());
//		
//		return otp;
//	}
	
	public String getEtiquetaCampoTarjeta(){
		return SuiteAppDesac.appContext.getString(R.string.confirmation_componente_digitos_tarjeta);
		//return "Ingrese los últimos 5 dígitos de su tarjeta";
	}
	
	public String getTextoAyudaTarjeta() {
		return SuiteAppDesac.appContext.getString(R.string.confirmation_ayuda_componente_digitos_tarjeta);
		//return "Son los últimos dígitos de tu tarjeta";		
	}
	
	public boolean mostrarCampoTarjeta(){
		return false;
	}
	
	public void solicitarAlertas (final BaseViewController caller){
		Session session = Session.getInstance(SuiteAppDesac.appContext);
		Account cEje = Tools.obtenerCuentaEje();
		
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		paramTable.put(ServerConstants.NUMERO_TELEFONO, session.getUsername());
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, session.getIum());
		paramTable.put(ServerConstants.NUMERO_TARJETA, cEje.getNumber());
		paramTable.put(ServerConstants.COMPANIA_CELULAR,session.getCompaniaUsuario());
	
	
		doNetworkOperation(Server.OP_SOLICITAR_ALERTAS, paramTable, caller);
	}
//	public void realizaCambioPerfilRecortadoABasico(){
//		
//		CambioPerfilDelegate cambioPerfilDelegate = new CambioPerfilDelegate();
//		cambioPerfilDelegate.setNuevoPerfil(null);
//		cambioPerfilDelegate.setOperacionTransferirEnCurso(true);
//		cambioPerfilDelegate.mostrarContratacion();
//	}
	
//	public void realizaCambioNumeroCompania(String nuevoNum, String nuevaCompania){
//		CambioTelefono cambioTel = new CambioTelefono();
//		
//		cambioTel.setNuevoTelefono(nuevoNum);
//		cambioTel.setNombreCompania(nuevaCompania);
//		
//		CambioTelefonoDelegate ctd = new CambioTelefonoDelegate();
//		ctd.setVersion((Constants.Perfil.recortado.equals(Session.getInstance(SuiteApp.getInstance()).getClientProfile())) ? Constants.VALOR_VERSION_RECORTADO : "");
//		ctd.setViewController(new CambioTelefonoViewController());
//		
//		ctd.getViewController().setParentViewsController(SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController());
//		ctd.getViewController().setCambioTelefono(cambioTel);
//		
//		ctd.showConfirmacion();
//	}
//	
	public void analyzeAlertasRecortadoSinError(){
		
		String validacionalertas = sa.getValidacionAlertas();

		if (Constants.ALERT01.equals(validacionalertas)) {
			// tiene que ir a Cambio de Perfil EA#8
			SuiteAppDesac.getInstance().getSuiteViewsController().getCurrentViewController().ocultaIndicadorActividad();
			SuiteAppDesac.getInstance().getSuiteViewsController().getCurrentViewController()
					.showYesNoAlert(R.string.transferir_mensaje_recortado_conalertsiguales,
							new OnClickListener() {
								@Override
								public void onClick(
										final DialogInterface dialog,
										final int which) {
									dialog.dismiss();
//									realizaCambioPerfilRecortadoABasico();

									if(Server.ALLOW_LOG) Log.e("Usuario RECORTADO Alertas coinciden",
										"Acepto realizar el cambio de perfil");

								}
							}, new OnClickListener() {
								@Override
								public void onClick(
										final DialogInterface dialog,
										final int which) {
									
									if(Server.ALLOW_LOG) Log.e("Usuario RECORTADO Alertas coinciden",
											"No Acepto realizar el cambio de perfil");
									
									new AlertDialog.Builder(SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp()).setMessage(R.string.transferir_mensaje_recortado_conalertsiguales_cancela_cambio_perfil) 
									.setTitle(R.string.label_error)
									.setIcon(R.drawable.anicalertaaviso)
									.setCancelable(true) 
									.setNeutralButton(android.R.string.ok, 
									new DialogInterface.OnClickListener() { 
										public void onClick(final DialogInterface dialog,final int whichButton){
											
									} 
									}) 
									.show();
								}
							});

		} else if (Constants.ALERT03.equals(validacionalertas)
				|| Constants.ALERT04.equals(validacionalertas)) {
			
			SuiteAppDesac.getInstance().getSuiteViewsController().getCurrentViewController().showYesNoAlert(SuiteAppDesac.getInstance().getString(R.string.transferir_mensaje_recortado_conalertsdistintas), new OnClickListener() {
								@Override
								public void onClick(
										final DialogInterface dialog,
										final int which) {

									if (DialogInterface.BUTTON_POSITIVE == which) {

//										realizaCambioNumeroCompania(
//												sa.getNumeroAlertas(),
//												sa.getCompaniaAlertas());
									}
								}
							});
		}
		
	}
	
	public void analyzeAlertasRecortado(){
		String codigoError=getTransferErrorResponse().getMessageCode();
		String descripcionError=getTransferErrorResponse().getMessageText();
		setTransferErrorResponse(null);
						
		String validacionalertas =sa.getValidacionAlertas();	
		if(Constants.ALERT01.equals(validacionalertas)){
			
			SuiteAppDesac.getInstance().getSuiteViewsController().getCurrentViewController().showYesNoAlertEspecialTitulo(codigoError,descripcionError, new OnClickListener() {
				@Override
				public void onClick(final DialogInterface dialog,final int which) {
					
					dialog.dismiss();
//					realizaCambioPerfilRecortadoABasico();
					if(Server.ALLOW_LOG) Log.e("Usuario RECORTADO Alertas coinciden","Acepto realizar el cambio de perfil");
					
				}
				
			}, new OnClickListener() {
				@Override
				public void onClick(final DialogInterface dialog,final int which) {
					
					if(Server.ALLOW_LOG) Log.e("Usuario RECORTADO Alertas coinciden","No Acepto realizar el cambio de perfil");
				
					new AlertDialog.Builder(SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewControllerApp()).setMessage(R.string.transferir_mensaje_recortado_conalertsiguales_cancela_cambio_perfil) 
					.setTitle(R.string.label_error)
					.setIcon(R.drawable.anicalertaaviso)
					.setNeutralButton(R.string.common_alert_yesno_positive_button, 
								new DialogInterface.OnClickListener() { 
										public void onClick(final DialogInterface dialog,final int whichButton){}
					}).show(); 
				
				}
			});
		}else if (Constants.ALERT02.equals(validacionalertas)){
			// no tiene alertas
			
			SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewController().ocultaIndicadorActividad();
			SuiteAppDesac.getInstance().getBmovilApplication().getBmovilViewsController().getCurrentViewController().showInformationAlertEspecial(SuiteAppDesac.getInstance().getString(R.string.label_error), codigoError, descripcionError,SuiteAppDesac.getInstance().getString(R.string.common_accept), null);
		
			
		}else if(Constants.ALERT03.equals(validacionalertas) ||Constants.ALERT04.equals(validacionalertas)){
			
			
			SuiteAppDesac.getInstance().getSuiteViewsController().getCurrentViewController().showYesNoAlertEspecialTitulo(codigoError,descripcionError, new OnClickListener() {
				@Override
				public void onClick(final DialogInterface dialog,final int which) {
					
					if (DialogInterface.BUTTON_POSITIVE == which) {
//						realizaCambioNumeroCompania(sa.getNumeroAlertas(), sa.getCompaniaAlertas());
					}
				}
			});
			
			
		}
		
		
	}
}
