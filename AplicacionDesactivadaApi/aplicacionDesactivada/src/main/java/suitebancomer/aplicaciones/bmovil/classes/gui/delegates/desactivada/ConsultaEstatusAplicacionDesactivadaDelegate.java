package suitebancomer.aplicaciones.bmovil.classes.gui.delegates.desactivada;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.util.Log;

import com.bancomer.mbanking.desactivada.R;
import com.bancomer.mbanking.desactivada.SuiteAppDesac;
import com.bancomer.mbanking.softtoken.SuiteAppApi;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.List;

import bancomer.api.common.commons.Constants.TYPE_SOFTOKEN;
import suitebancomer.aplicaciones.bmovil.classes.entrada.InitContratacion;
import suitebancomer.aplicaciones.bmovil.classes.gui.controllers.desactivada.BmovilViewsController;
import suitebancomer.aplicaciones.bmovil.classes.io.desactivada.Server;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomer.aplicaciones.commservice.commons.CommContext;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;
import suitebancomer.aplicaciones.commservice.response.ResponseServiceImpl;
import suitebancomer.aplicaciones.commservice.service.CommServiceProxy;
import suitebancomer.aplicaciones.commservice.service.ICommService;
import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.IngresoDatosSTViewController;
import suitebancomer.aplicaciones.softtoken.classes.gui.controllers.token.SofttokenViewsController;
import suitebancomer.aplicaciones.softtoken.classes.gui.delegates.token.GeneraOTPSTDelegate;
import suitebancomer.classes.gui.controllers.desactivada.BaseViewController;
import suitebancomer.classes.gui.controllers.desactivada.MenuSuiteViewController;
import suitebancomercoms.aplicaciones.bmovil.classes.common.CatalogoAutenticacionFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.DatosBmovilFileManager;
import suitebancomercoms.aplicaciones.bmovil.classes.common.ServerConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.common.Tools;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParserJSON;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ParsingException;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatus;
import suitebancomercoms.aplicaciones.bmovil.classes.model.ConsultaEstatusMantenimientoData;
import suitebancomercoms.classes.common.PropertiesManager;

//import suitebancomer.aplicaciones.softtoken.classes.common.token.SofttokenActivationBackupManager;

public class ConsultaEstatusAplicacionDesactivadaDelegate extends
		DelegateBaseAutenticacion {
	// #region Variables.
	//ARR
		public static boolean res = false;
	public final static long CONSULTA_APLICACION_DESACTIVADA_DELEGATE_ID = 0x132c5e9721847e8eL;

	/**
	 * Controller asociado al delegate.
	 */
	private BaseViewController ownerController;

	/**
	 * Objeto con la consulta del estatus de la aplicación.
	 */
	private ConsultaEstatus consultaEstatus;

	/**
	 * Respuesta del servidor.
	 */
	private ConsultaEstatusMantenimientoData serverResponse;

	// #endregion

	// #region Setters y Getters.
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.classes.gui.delegates.BaseDelegate#getDelegateIdentifier()
	 */
	@Override
	public long getDelegateIdentifier() {
		return CONSULTA_APLICACION_DESACTIVADA_DELEGATE_ID;
	}

	/**
	 * @return Objeto con la consulta del estatus de la aplicación.
	 */
	public ConsultaEstatus getConsultaEstatus() {
		return consultaEstatus;
	}


	/**
	 * @return Controller asociado al delegate.
	 */
	public BaseViewController getOwnerController() {
		return ownerController;
	}

	/**
	 * @param ownerController
	 *            Controller asociado al delegate.
	 */
	public void setOwnerController(final BaseViewController ownerController) {
		this.ownerController = ownerController;
	}

	// #endregion

	// #region Constructores.
	public ConsultaEstatusAplicacionDesactivadaDelegate() {
		// TODO Auto-generated constructor stub
	}

	// #endregion

	// #region Web.
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.classes.gui.delegates.BaseDelegate#doNetworkOperation(int,
	 * java.util.Hashtable,
	 * suitebancomer.classes.gui.controllers.BaseViewController)
	 */

	public void doNetworkOperation(final int operationId,
								   final Hashtable<String, ?> params, final BaseViewController caller,
								   final boolean callerHandlesError) {

		if(ownerController instanceof MenuSuiteViewController) ((MenuSuiteViewController)ownerController).setButtonsDisabled(false);
		SuiteAppDesac.getInstance()
				.getBmovilApplication()
				.invokeNetworkOperation(operationId, params, caller,
						callerHandlesError);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * suitebancomer.classes.gui.delegates.BaseDelegate#analyzeResponse(int,
	 * suitebancomer.aplicaciones.bmovil.classes.io.ServerResponse)
	 */
	public void analyzeResponse(final int operationId, final ServerResponse response) {
		ownerController.ocultaIndicadorActividad();
		ownerController.runOnUiThread(new Runnable() {
			public void run() {
				if (Server.CONSULTA_MANTENIMIENTO == operationId) {
					if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
						serverResponse = (ConsultaEstatusMantenimientoData) response.getResponse();
						Session.getInstance(SuiteAppDesac.appContext).setSecurityInstrument(serverResponse.getTipoInstrumento());
						llenarConsultaEstatus();

						if(Server.ALLOW_LOG) Log.d("ConsultaEstatus", "estatus: " + consultaEstatus.getEstatus());
						if(Server.ALLOW_LOG) Log.d("ConsultaEstatus", "instrumento: " + consultaEstatus.getInstrumento());
						if(Server.ALLOW_LOG) Log.d("ConsultaEstatus", "PerfilAST: " + consultaEstatus.getPerfilAST());
						if ((consultaEstatus.getEstatus().equalsIgnoreCase(Constants.STATUS_USER_CANCELED) ||
								consultaEstatus.getEstatus().equalsIgnoreCase(Constants.STATUS_BANK_CANCELED))
								&& serverResponse.getFechaContratacion().equalsIgnoreCase(serverResponse.getFechaSistema())) {
							ownerController.showInformationAlert(R.string.bmovil_estatus_aplicacion_desactivada_error_fechas_iguales);
						} else {
							if(SuiteAppDesac.getInstance().getContinuaFlujoBconnect()){
								direccionarFlujo();
							}else{
								SuiteAppDesac.getInstance().getCallBackForDesactivada().returnDesactivadaApi(consultaEstatus);
							}
						}
					}else if (response.getStatus() == ServerResponse.OPERATION_WARNING
							|| response.getStatus() == ServerResponse.OPERATION_ERROR) {

						// TODO: Provisional, mientran logran NACAR no envie el status NE
						ownerController.showInformationAlert(response.getMessageText());
					}

				}
			}
		});
	}

	/**
	 * @param operationId
	 * @param params
	 * @return
	 */
	public void operacion(final int operationId, final Hashtable<String, ?> params){
		final Integer opId = Integer.valueOf(operationId);   
		ParametersTO parameters = new ParametersTO();
	    parameters.setSimulation(Server.SIMULATION);
	    parameters.setDevelopment(Server.DEVELOPMENT);
	    parameters.setOperationId(opId.intValue());
	    parameters.setParameters(params.clone());
	    parameters.setJson(true);
	    IResponseService resultado = new ResponseServiceImpl() ;
	    ConsultaEstatusMantenimientoData data = new ConsultaEstatusMantenimientoData();
	    ICommService serverproxy = new CommServiceProxy(SuiteAppDesac.appContext);
	    try {
			resultado = serverproxy.request(parameters, ConsultaEstatusMantenimientoData.class);
		   	ConsultaEstatusMantenimientoData estatusM=(ConsultaEstatusMantenimientoData)resultado.getObjResponse();
		   	if(ApiConstants.OPERATION_SUCCESSFUL==resultado.getStatus()){
		    	ParserJSON parser = new ParserJSON(resultado.getResponseString());   
		        estatusM.process(parser);
		   	}
		   	data = estatusM;
	    } catch (UnsupportedEncodingException e) {
	    	muestraMensajeError(e);
		} catch (ClientProtocolException e) {
			muestraMensajeError(e);
		} catch (IllegalStateException e) {
			muestraMensajeError(e);
		} catch (IOException e) {
			muestraMensajeError(e);
		} catch (JSONException e) {
			muestraMensajeError(e);
		} catch (ParsingException e) {
			muestraMensajeError(e);
		}
	   	ServerResponse response = new ServerResponse(resultado.getStatus(), resultado.getMessageCode(), resultado.getMessageText(), data);
		analyzeResponse(operationId, response);
		
	}
	
	private void muestraMensajeError(final Exception t) {
		if(Server.ALLOW_LOG) {
			Log.d(this.getClass().getSimpleName(), "Exception on analyzeNetworkResponse.", t);
		}
		if (ownerController != null) {
			String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
			ownerController.showErrorMessage(message);
		}
	}
	
	// #endregion

	// #region Métodos.
	/**
	 * Valida el número de celular ingresado
	 * 
	 * @param numeroCelular
	 *            El número de celular del cliente.
	 */
	public boolean validarDatos(final String numeroCelular,final Boolean continuaFlujoBconnect) {
		this.setOwnerController(new MenuSuiteViewController());
		SuiteAppDesac.getInstance().setContinuaFlujoBconnect(continuaFlujoBconnect);
		SuiteAppDesac
				.getInstance()
				.getBmovilApplication()
				.getBmovilViewsController()
				.addDelegateToHashMap(ConsultaEstatusAplicacionDesactivadaDelegate.CONSULTA_APLICACION_DESACTIVADA_DELEGATE_ID, this);
		if (Tools.isEmptyOrNull(numeroCelular)) {
			ownerController.showInformationAlert(R.string.bmovil_estatus_aplicacion_desactivada_error_celular_vacio, createEnableButtonsClickListener());
			return false;
		} else if (numeroCelular.length() < Constants.TELEPHONE_NUMBER_LENGTH) {
			ownerController.showInformationAlert(R.string.bmovil_estatus_aplicacion_desactivada_error_celular_corto, createEnableButtonsClickListener());
			return false;
		}

		consultaEstatus = new ConsultaEstatus();
		consultaEstatus.setNumCelular(numeroCelular);
		Session.getInstance(SuiteAppDesac.appContext).setUsername(numeroCelular);
		String autVersion = CatalogoAutenticacionFileManager.getCurrent().leerVersionArchivoCatalogoAutenticacion();

		Hashtable<String, String> params = new Hashtable<String, String>();
		params.put(Server.TELEFONICAS_PARAM, "0");
		params.put(ServerConstants.VERSION_MIDLET, ServerConstants.APP_VERSION_CONSULTA);
		params.put(ServerConstants.NUMERO_TELEFONO, numeroCelular);
		params.put(Server.CAT_PARAM, Tools.isEmptyOrNull(autVersion) ? "0" : autVersion);

		List<String> listaEncriptar = Arrays.asList(
				ApiConstants.OPERACION, ServerConstants.NUMERO_TELEFONO,
				ServerConstants.VERSION_MIDLET, Server.TELEFONICAS_PARAM,Server.CAT_PARAM
				);
		CommContext.operacionCode = Server.CONSULTA_MANTENIMIENTO;
		CommContext.listaEncriptar = listaEncriptar;

		doNetworkOperation(Server.CONSULTA_MANTENIMIENTO, params, ownerController, false);
		//ARR
//				res = true;
		return true;
	}


	private OnClickListener createEnableButtonsClickListener() {
		return new OnClickListener() {

			@Override
			public void onClick(final DialogInterface dialog,final int which) {
				((MenuSuiteViewController) getOwnerController()).setButtonsDisabled(false);
			}
		};
	}

	/**
	 * Obtiene la informaci�n necesaria de la respuesta del servidor
	 */
	private void llenarConsultaEstatus() {
		consultaEstatus.setEstatus(serverResponse.getEstatusServicio());
		consultaEstatus.setPerfil(Tools.determinaPerfil(
				serverResponse.getPerfilCliente(),
				serverResponse.getTipoInstrumento(),
				serverResponse.getEstatusInstrumento()));
		consultaEstatus.setNombreCliente(serverResponse.getNombreCliente());
		consultaEstatus.setInstrumento(serverResponse.getTipoInstrumento());
		consultaEstatus.setEstatusInstrumento(serverResponse.getEstatusInstrumento());
		consultaEstatus.setNumCliente(serverResponse.getNumeroCliente());
		consultaEstatus.setCompaniaCelular(serverResponse.getCompaniaCelular());
		consultaEstatus.setEmailCliente(serverResponse.getEmailCliente());
		consultaEstatus.setPerfilAST(serverResponse.getPerfilCliente());
		consultaEstatus.setEstatusAlertas(serverResponse.getEstatusAlertas());
	}

	/**
	 * Direcciona al flujo correspondiente seg�n el estatus recibido. <br/>
	 * A1 - Reactivación. <br/>
	 * PE - Activación. <br/>
	 * PA - Activación. <br/>
	 * PB - Manda mensaje de error �Para desbloquear tu NIP debes acudir al
	 * cajero�. <br/>
	 * BI - Desbloqueo de contraseña. <br/>
	 * C4 - Contratación. <br/>
	 * CN - Contratación. <br/>
	 * S4 - Quitar suspensi�n. <br/>
	 * PS - Contratación EA#2.
	 */
	private void direccionarFlujo() {
		String estatusServicio = consultaEstatus.getEstatus();
		BmovilViewsController viewsController = SuiteAppDesac.getInstance()
				.getBmovilApplication().getBmovilViewsController();
		SuiteAppApi suite=new SuiteAppApi();
		suite.onCreate(SuiteAppDesac.appContext);
		boolean existeArchivoP = SofttokenActivationBackupManager.getCurrent().existsABackup();

			if (estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)
				|| estatusServicio.equalsIgnoreCase(Constants.STATUS_PENDING_ACTIVATION)
				|| estatusServicio.equalsIgnoreCase(Constants.STATUS_PENDING_SEND)) {

			if(existeArchivoP) {
				ownerController.showActivacionSTEA12();
			}else if(estatusServicio.equalsIgnoreCase(Constants.STATUS_PENDING_ACTIVATION)){
				//flujo principal para PA
				resolvePAFlujo(viewsController);
			} else if (isSofttokenListoParaActivar()) {
				// EA#10
				intentaActivarSofttoken();
			} else if (isSofttokenYEstatusInstrumentoNoActivo()) {
				// EA#12
				ownerController.showInformationAlert(SuiteAppDesac.appContext.getString(R.string.label_information), SuiteAppDesac.appContext.getString(R.string.alert_estado_instrumento_distintoA1), SuiteAppDesac.appContext.getString(R.string.common_accept), createEnableButtonsClickListener());
			} else if (isSofttokenActivado()) {
				// EA#13
		        GeneraOTPSTDelegate generaOTPSTDelegate = new GeneraOTPSTDelegate();
		        if (generaOTPSTDelegate.borraToken()) {
		            PropertiesManager.getCurrent().setSofttokenActivated(false);
		        }
				intentaActivarSofttoken();
			}else if(estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_ACTIVE)){
					//se agrega para mostrar el alert de softtoken activo
					// EA#11
					viewsController.showReactivacion(consultaEstatus);

			} else if (estatusServicio.equalsIgnoreCase(Constants.STATUS_PENDING_SEND)) {
				// EA#11
				viewsController.showActivacion(consultaEstatus, null);
			} else if (estatusServicio.equalsIgnoreCase(Constants.STATUS_PENDING_ACTIVATION)) {
				// EA#11
				viewsController.showActivacion(consultaEstatus, null);
			}
		} else if (estatusServicio.equalsIgnoreCase(Constants.STATUS_NIP_BLOCKED)) {
			ownerController.showInformationAlert(R.string.bmovil_estatus_aplicacion_desactivada_estatus_nip_bloqueado);
		} else if (estatusServicio.equalsIgnoreCase(Constants.STATUS_PASS_BLOCKED)) {
			ownerController.showDesbloqueo(consultaEstatus);
		} else if (estatusServicio.equalsIgnoreCase(Constants.STATUS_USER_CANCELED)
				|| estatusServicio.equalsIgnoreCase(Constants.STATUS_BANK_CANCELED)
				|| estatusServicio.equalsIgnoreCase(Constants.STATUS_CLIENT_NOT_FOUND)) {

			viewsController.showContratacion(consultaEstatus, estatusServicio, false);
		} else if (estatusServicio.equalsIgnoreCase(Constants.STATUS_ENGAGEMENT_UNCOMPLETE)) {
			SuiteAppDesac.getInstance().getSofttokenApplication().getSottokenViewsController().setCurrentActivityApp(ownerController);
			String perfil = consultaEstatus.getPerfilAST();
			if (perfil.equals(Constants.PROFILE_ADVANCED_03)) {
				if (consultaEstatus.getInstrumento().equals(Constants.EMPTY_STRING)) {
					if (existeArchivoP) {
						// EA#6
					ownerController.showActivacionSTEA12();

					} else {
						// EA#7
						ownerController.activacionST();
					}
				} else {
					// EA#8

					viewsController.showContratacion(consultaEstatus, estatusServicio, false);
				}
			} else if ((Constants.PROFILE_BASIC_01.equals(perfil)) || (Constants.PROFILE_RECORTADO_02.equals(perfil))) {
				// EA#8, EA#9

				viewsController.showContratacion(consultaEstatus, estatusServicio, false);
			}
		} else if (estatusServicio.equalsIgnoreCase(Constants.STATUS_APP_SUSPENDED)) {

			ownerController.showQuitarSuspension(consultaEstatus);
		}
	}




	/**
	 * Busca el número de celular dentro de sesion, si hay uno registrado lo
	 * establece en el controlador.
	 */
	public String buscarNumero() {

		Session session = Session.getInstance(SuiteAppDesac.appContext);
		String celular = DatosBmovilFileManager.getCurrent().getLogin();//session.getUsername();

		return celular;
	}

	// #endregion

	/**
	 * Se valida la subaplicación Softtoken esta lista para ser activada. <br/>
	 * Esto sucede si el usuario tiene como instrumento de seguridad Softtoken
	 * activo y la subaplicación Softtoken no esta activa en el dispositivo.
	 */
	private boolean isSofttokenListoParaActivar() {
		boolean isSofttoken = TYPE_SOFTOKEN.S1.value
				.equals(consultaEstatus.getInstrumento());
		boolean isActive = Constants.ESTATUS_IS_ACTIVO.equals(consultaEstatus
				.getEstatusInstrumento());
		boolean isSofttokenActive = PropertiesManager.getCurrent()
				.getSofttokenActivated();
		return (isSofttoken && isActive && !isSofttokenActive);
	}

	/**
	 * RN4 flujo PA
	 * CAMBIOS EN EL CASO DE USO
	 */
	private void resolvePAFlujo(final BmovilViewsController viewsController){
		Session session = Session.getInstance(SuiteAppDesac.appContext);
		final boolean indicaContratacion = session.loadBanderasBMovilAttribute(bancomer.api.common.commons.Constants.BANDERAS_INDICADOR_CONTRATACION);
		if(TYPE_SOFTOKEN.S1.value
				.equals(consultaEstatus.getInstrumento())){
			//Si el instrumento de seguridad == S1 y bandera indicadorContratacion == true usa el API Activación.
			if(indicaContratacion){
				viewsController.showActivacion(consultaEstatus, null);
			//Si la bandera indicadorContratacion == false y el instrumento de seguridad == S1 se ejecuta EA#10
			}else{
				// EA#10
				if (isSofttokenListoParaActivar()) {
					// *EA#10 flujo principal
					intentaActivarSofttoken();
				} else if (isSofttokenYEstatusInstrumentoNoActivo()) {
					// *EA#12
					ownerController.showInformationAlert(SuiteAppDesac.appContext.getString(R.string.label_information), SuiteAppDesac.appContext.getString(R.string.alert_estado_instrumento_distintoA1), SuiteAppDesac.appContext.getString(R.string.common_accept), createEnableButtonsClickListener());
				} else if (isSofttokenActivado()) {
					// *EA#13
					GeneraOTPSTDelegate generaOTPSTDelegate = new GeneraOTPSTDelegate();
					if (generaOTPSTDelegate.borraToken()) {
						PropertiesManager.getCurrent().setSofttokenActivated(false);
					}
					intentaActivarSofttoken();
				}
			}

			//Si instrumento de seguridad  es diferente a S1 Api Activación,
		}else {
			viewsController.showActivacion(consultaEstatus, null);
		}
	}

	/**
	 * Consulta estatus aplicacion desactivada: EA#12
	 */
	private boolean isSofttokenYEstatusInstrumentoNoActivo() {
		boolean isSofttoken = bancomer.api.common.commons.Constants.TYPE_SOFTOKEN.S1.value
				.equals(consultaEstatus.getInstrumento());
		boolean isActive = bancomer.api.common.commons.Constants.ESTATUS_IS_ACTIVO.equals(consultaEstatus
				.getEstatusInstrumento());
		return (isSofttoken && !isActive);
	}

	private boolean isSofttokenActivado() {
		boolean isSofttoken = bancomer.api.common.commons.Constants.TYPE_SOFTOKEN.S1.value
				.equals(consultaEstatus.getInstrumento());
		boolean isActive = bancomer.api.common.commons.Constants.ESTATUS_IS_ACTIVO.equals(consultaEstatus
				.getEstatusInstrumento());
		boolean isSofttokenActive = PropertiesManager.getCurrent()
				.getSofttokenActivated();
		return (isSofttoken && isActive && isSofttokenActive);
	}

	/**
	 * Permite al cliente decidir si desea activar o no Softtoken.
	 */
	private void intentaActivarSofttoken() {
		if (null == ownerController)
			return;

						/*if (null == SuiteAppDesac.getInstance()
								.getSofttokenApplication())
							SuiteAppDesac.getInstance().startSofttokenApp();

						MenuSuiteViewController suiteViewController = (MenuSuiteViewController) SuiteAppDesac
								.getInstance().getSuiteViewsController()
								.getCurrentViewControllerApp();
						SofttokenViewsController viewsController = SuiteAppDesac
								.getInstance().getSofttokenApplication()
								.getSottokenViewsController();

						// el método showContratacionSotfttoken no muestra
						// realmente ninguna pantalla, sin embargo lleva a cabo
						// varia
						// inicializaciones requeridas a lo largo o al final del
						// flujo de contratación de Softtoken.
						viewsController
								.showContratacionSotfttoken(suiteViewController);
						viewsController.showPantallaIngresoDatos();*/
						//inicializa la clase
						suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer bs=new suitebancomer.aplicaciones.bmovil.classes.model.contratacion.BanderasServer(
								ServerCommons.SIMULATION,ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR,ServerCommons.DEVELOPMENT
						);
						InitContratacion initi=new InitContratacion(SuiteAppDesac.appContext,SuiteAppDesac.getInstance().getCallBackContratacion(),SuiteAppDesac.getInstance().getActivityForContratacion(),bs);


						//__________- invocacion al api de softoken ___________//
						SuiteAppApi sftoken=new SuiteAppApi();
  /*
   Setea los  parametros para simulacion, produccion y test
  */
						ServerCommons.ALLOW_LOG=Server.ALLOW_LOG;
						ServerCommons.DEVELOPMENT=Server.DEVELOPMENT;
						ServerCommons.SIMULATION=Server.SIMULATION;

						sftoken.onCreate(SuiteAppDesac.appContext);

						sftoken.setIntentToReturn(SuiteAppDesac.getInstance().getCallBackForSofttoken());
						sftoken.setIntentToContratacion(new suitebancomer.aplicaciones.bmovil.classes.gui.controllers.contratacion.ContratacionDefinicionPasswordViewController());
						sftoken.showViewController(IngresoDatosSTViewController.class, 0, false, null, null, (Activity)SuiteAppDesac.appContext);


	}
	
	private void showActivacionSTEP() {
		cambiarDeBMovilTokenMovil().showPantallaIngresoDatos();
	}
	
	/*private void showActivacionST(){
		cambiarDeBMovilTokenMovil().showActivacionSTEP25();
	}*/
	
	private SofttokenViewsController cambiarDeBMovilTokenMovil() {
		SofttokenViewsController viewsController = SuiteAppDesac.getInstance()
				.getSofttokenApplication().getSottokenViewsController();
		ownerController.setParentViewsController(viewsController);
		viewsController.setCurrentActivityApp(ownerController);

		return viewsController;
	}
	
	/**
	 * 
	 */
	private void showMenuSuite() {
		if (SuiteAppDesac.getInstance().getSuiteViewsController().getCurrentViewControllerApp() instanceof MenuSuiteViewController) {
			((MenuSuiteViewController) SuiteAppDesac.getInstance().getSuiteViewsController().getCurrentViewControllerApp()).restableceMenu();
			((MenuSuiteViewController) SuiteAppDesac.getInstance().getSuiteViewsController().getCurrentViewControllerApp()).setShouldHideLogin(true);
		}

//		SuiteAppDesac.getInstance().closeSofttokenApp();
	}
}
