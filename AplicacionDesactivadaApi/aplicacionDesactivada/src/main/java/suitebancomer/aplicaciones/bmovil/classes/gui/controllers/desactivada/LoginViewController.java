package suitebancomer.aplicaciones.bmovil.classes.gui.controllers.desactivada;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Constants;

import suitebancomer.aplicaciones.bmovil.classes.gui.delegates.desactivada.LoginDelegate;
import suitebancomercoms.classes.common.GuiTools;
import suitebancomer.classes.gui.controllers.desactivada.MenuSuiteViewController;
import android.graphics.Paint;
import android.text.Html;
import android.text.InputFilter;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.HashMap;
import java.util.Map;

import suitebancomer.aplicaciones.bmovil.classes.tracking.TrackingHelper;

import com.bancomer.mbanking.desactivada.R;

public class LoginViewController extends LinearLayout implements OnClickListener {
	/**
	 * The delegate reference
	 */
	private LoginDelegate delegate;
	
	/**
	 * 
	 */
	private BmovilViewsController parentManager;
	
	/**
	 * The view title
	 */
	private TextView mTitle;
	
	/**
	 * The flip back button
	 */
	private Button mFlipBackButton;
	
	/**
	 * The user cellphone label
	 */
	private TextView mUserLabel;
	
	/**
	 * The user cellphone input
	 */
	private EditText mUser;
	
	/**
	 * The user pass label
	 */
	private TextView mPassLabel;
	
	/**
	 * The user pass input
	 */
	private EditText mPass;
	
	/**
	 * The novelty label
	 */
	private TextView mNoveltiesLabel;
	
	/**
	 * The button which fires the login process
	 */
	private Button mLoginButton;
	
	/**
	 * The button which fires the access forgotten process
	 */
	private TextView mEraseDataButton;
	
	/**
	 * Reference to the parent controller, used to perform the flip back action
	 */
	private MenuSuiteViewController menuViewController;

	/**
	 * Default constructor for the view
	 * @param context the context
	 * @param attrs the attributes to be set to the view
	 * @param viewId the assigned id
	 */
	public LoginViewController(final MenuSuiteViewController menuSuiteViewController,final BmovilViewsController parentViewsController) {
		super(menuSuiteViewController);
		
		
		parentManager = parentViewsController;
		menuViewController = menuSuiteViewController;
		parentManager.setCurrentActivityApp(menuViewController);
		
	    LayoutInflater inflater = LayoutInflater.from(menuSuiteViewController);
	    LinearLayout viewLayout = (LinearLayout)inflater.inflate(R.layout.layout_login_view, this, true);
	    
	    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(-1,-1); // Fill/Match the parent.
	    viewLayout.setLayoutParams(params);

		mTitle = (TextView)findViewById(R.id.login_title);
		mFlipBackButton = (Button)findViewById(R.id.login_flip_button);
		mFlipBackButton.setOnClickListener(this);
		mUserLabel = (TextView)findViewById(R.id.login_user_label);
		mUser = (EditText)findViewById(R.id.login_user_input);
		InputFilter[] userFilterArray = new InputFilter[1];
		userFilterArray[0] = new InputFilter.LengthFilter(Constants.TELEPHONE_NUMBER_LENGTH);
		mUser.setFilters(userFilterArray);
		mPassLabel = (TextView)findViewById(R.id.login_pass_label);
		mPass = (EditText)findViewById(R.id.login_pass_input);
		//mPass.setInputType(InputType.TYPE_CLASS_PHONE);
		mPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
		//mPass.setKeyListener(DigitsKeyListener.getInstance());
		InputFilter[] passFilterArray = new InputFilter[1];
		passFilterArray[0] = new InputFilter.LengthFilter(Constants.PASSWORD_LENGTH);
		mPass.setFilters(passFilterArray);
		mNoveltiesLabel = (TextView)findViewById(R.id.login_novelties_label);
		mNoveltiesLabel.setPaintFlags(mNoveltiesLabel.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		mNoveltiesLabel.setOnClickListener(this);
		mLoginButton = (Button)findViewById(R.id.login_access_button);
		mLoginButton.setOnClickListener(this);
		mEraseDataButton = (TextView)findViewById(R.id.login_account_reactivation);
		String eraseButtonText = "<u>"+menuSuiteViewController.getString(R.string.login_reactivacion)+"</u>";
		mEraseDataButton.setText(Html.fromHtml(eraseButtonText));
		mEraseDataButton.setOnClickListener(this);
		
		// Resize elements.
		scaleForCurrentScreen();
		
		//mPass.setText("147258"); //TODO remove for TEST
	}
	
	
	
	@Override
	public void onClick(final View v) {
		if (v == mLoginButton) {
			botonEntrar();
		}else if (v == mEraseDataButton) {
			botonBorrarDatos();
		} else if (v == mFlipBackButton) {
			menuViewController.plegarOpcion();
		} else if (v == mNoveltiesLabel){
			//parentManager.showNovedadesLogin();
		}
	}
	
	private void botonEntrar() {
		LoginDelegate lDelegate = getDelegate();
		String user = mUser.getText().toString();
		String pass = mPass.getText().toString();
		lDelegate.botonEntrar(user, pass);
		
//		if(null != delegate)
//			lDelegate.setLoginViewController(this);
//		
//		if(-1 == timeElapsed) {
//			String user = mUser.getText().toString();
//			String pass = mPass.getText().toString();
//			lDelegate.botonEntrar(user, pass);
//		} else {
//			String str = menuViewController.getString(R.string.login_wait_30_min);
//			String minuteStr = "";
//			
////			if(timeElapsed < 10)
////				minuteStr = "0" + String.valueOf(timeElapsed);
////			else
////				minuteStr = String.valueOf(timeElapsed);
//			
//			if(timeElapsed < 10)
//				minuteStr += "0";
//			minuteStr += String.valueOf(timeElapsed);
//			
//			str = String.format(str, minuteStr);
//			
//			menuViewController.showInformationAlert(str);
//		}
	}
	
	private void botonBorrarDatos() {
	//	getParentManager().showBorrarDatos();
	}
	
	public BmovilViewsController getParentManager() {
		return parentManager;
	}
	
	public void setParentManager(final BmovilViewsController parentManager) {
		this.parentManager = parentManager;
	}
	
	public MenuSuiteViewController getMenuSuiteViewController() {
		return menuViewController;
	}

	public LoginDelegate getDelegate() {
		return delegate;
	}

	public void setDelegate(final LoginDelegate delegate) {
		this.delegate = delegate;
		delegate.setLoginViewController(this);
		delegate.establecerUsuario(mUser, mEraseDataButton);
	}
	
	public void limpiarCampoContrasena() {
		mPass.setText("");
		mPass.invalidate();
	}
	
	public void loginExitoso() {
		menuViewController.restableceMenu();
	}
	
	public void reestableceLogin() {
		if (delegate != null) {
			delegate.establecerUsuario(mUser, mEraseDataButton);
			limpiarCampoContrasena();
		}
	}
	
	/**
	 * Establece la l�gica a seguir para el redimensionamiento de la pantalla.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		
		guiTools.scale(findViewById(R.id.login_layout));
		guiTools.scale(findViewById(R.id.loginTitleLayout));
		guiTools.scale(findViewById(R.id.loginNumeroCelularLayout));
		guiTools.scale(findViewById(R.id.loginContrasenaLayout));
		guiTools.scale(findViewById(R.id.btnEntrarLayout));
		//guiTools.scale(findViewById(R.id.lblComoContratar), true);
		
		guiTools.scale(mTitle, true);
		guiTools.scale(mFlipBackButton);
		guiTools.scale(mUserLabel, true);
		guiTools.scale(mUser, true);
		guiTools.scale(mPassLabel, true);
		guiTools.scale(mPass, true);
		guiTools.scale(mLoginButton);
		guiTools.scale(mEraseDataButton, true);
		guiTools.scale(mNoveltiesLabel, true);
	}
}
