package com.bancomer.mbanking.desactivada;

import suitebancomercoms.aplicaciones.bmovil.classes.common.Session;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.classes.common.PropertiesManager;
import suitebancomercoms.classes.gui.controllers.BaseViewControllerCommons;
import suitebancomer.classes.gui.controllers.desactivada.MenuSuiteViewController;
import suitebancomer.classes.gui.controllers.desactivada.SuiteViewsController;
//import suitebancomer.classes.gui.controllers.MenuSuiteViewController;
//import suitebancomer.classes.gui.controllers.SuiteViewsController;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;

import com.bancomer.base.callback.CallBackBConnect;

import java.util.List;

public class SuiteAppDesac extends com.bancomer.base.SuiteApp {
	public static boolean isSubAppRunning;
	public static Context appContext;
	private static SuiteAppDesac me;
	private SuiteViewsController suiteViewsController;
	private static BaseViewControllerCommons intentToReturn;
	private CallBackBConnect callBackForMantenimiento;
	private Activity activityForMantenimiento;
	private CallBackBConnect callBackContratacion;
	private Activity activityForContratacion;
	private CallBackBConnect callBackForSofttoken;
	private Activity activityForSofttoken;

	private CallBackBConnect callBackForDesactivada;

	private CallBackBConnect callBackReactivacion;
	private CallBackBConnect callBackActivacion;
	private Activity activityReactivacion;
	private Activity activityActivacion;
	private List<String> estados;

	private Boolean continuaFlujoBconnect=Boolean.FALSE;

	public Boolean getContinuaFlujoBconnect() {
		return continuaFlujoBconnect;
	}

	public void setContinuaFlujoBconnect(final Boolean continuaFlujoBconnect) {
		this.continuaFlujoBconnect = continuaFlujoBconnect;
	}

	public List<String> getEstados() {
		return estados;
	}

	public void setEstados(final List<String> estados) {
		this.estados = estados;
	}

	public CallBackBConnect getCallBackReactivacion() {
		return callBackReactivacion;
	}

	public void setCallBackReactivacion(final CallBackBConnect callBackReactivacion) {
		this.callBackReactivacion = callBackReactivacion;
	}

	public CallBackBConnect getCallBackActivacion() {
		return callBackActivacion;
	}

	public void setCallBackActivacion(final CallBackBConnect callBackActivacion) {
		this.callBackActivacion = callBackActivacion;
	}

	public Activity getActivityReactivacion() {
		return activityReactivacion;
	}

	public void setActivityReactivacion(final Activity activityReactivacion) {
		this.activityReactivacion = activityReactivacion;
	}

	public Activity getActivityActivacion() {
		return activityActivacion;
	}

	public void setActivityActivacion(final Activity activityActivacion) {
		this.activityActivacion = activityActivacion;
	}

	public CallBackBConnect getCallBackContratacion() {
		return callBackContratacion;
	}

	public void setCallBackContratacion(final CallBackBConnect callBackContratacion) {
		this.callBackContratacion = callBackContratacion;
	}

	public Activity getActivityForContratacion() {
		return activityForContratacion;
	}

	public void setActivityForContratacion(final Activity activityForContratacion) {
		this.activityForContratacion = activityForContratacion;
	}

	public CallBackBConnect getCallBackForSofttoken() {
		return callBackForSofttoken;
	}

	public void setCallBackForSofttoken(final CallBackBConnect callBackForSofttoken) {
		this.callBackForSofttoken = callBackForSofttoken;
	}

	public Activity getActivityForSofttoken() {
		return activityForSofttoken;
	}

	public void setActivityForSofttoken(final Activity activityForSofttoken) {
		this.activityForSofttoken = activityForSofttoken;
	}

	public CallBackBConnect getCallBackForMantenimiento() {
		return callBackForMantenimiento;
	}

	public void setCallBackForMantenimiento(final CallBackBConnect callBackForMantenimiento) {
		this.callBackForMantenimiento = callBackForMantenimiento;
	}

	public Activity getActivityForMantenimiento() {
		return activityForMantenimiento;
	}

	public void setActivityForMantenimiento(final Activity activityForMantenimiento) {
		this.activityForMantenimiento = activityForMantenimiento;
	}

	/**
	 * @return the intentToReturn
	 */
	public static BaseViewControllerCommons getIntentToReturn() {
		return intentToReturn;
	}

	/**
	 * @param intentToReturn the intentToReturn to set
	 */
	public static void setIntentToReturn(final BaseViewControllerCommons intentToReturn) {
		SuiteAppDesac.intentToReturn = intentToReturn;
	}

	public void onCreate(final Context context) {
		super.onCreate(context);
		appContext = context;
		suiteViewsController = new SuiteViewsController();
		isSubAppRunning = false;
		
		me = this;
		//appContext = getApplicationContext();
	};
	
	public void cierraAplicacionSuite() {
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	public SuiteViewsController getSuiteViewsController() {
		return suiteViewsController;
	}

	public static SuiteAppDesac getInstance() {
		return me;
	}
	
	// #region BmovilApp
	private BmovilApp bmovilApplication;
	
	public BmovilApp getBmovilApplication() {
		if(bmovilApplication == null)
			bmovilApplication = new BmovilApp(this);
		return bmovilApplication;
	}
	
	public static boolean getBmovilStatus() {
		return PropertiesManager.getCurrent().getBmovilActivated();
	}
	
	public void startBmovilApp() {
		bmovilApplication = new BmovilApp(this);
		isSubAppRunning = true;
	}
	
//	public void cierraAplicacionBmovil() {
//		bmovilApplication.cierraAplicacion();
//		bmovilApplication = null;
//		isSubAppRunning = false;		
//		//reiniciaAplicacionBmovil();
//	}
	
//	public void reiniciaAplicacionBmovil() {
//		if(bmovilApplication == null)
//			bmovilApplication = new BmovilApp(this);
//		bmovilApplication.reiniciaAplicacion();
//		isSubAppRunning = true;
//	}
	// #endregion
	
	// #region SofttokenApp
	private SofttokenApp softtokenApp;
//	
	public SofttokenApp getSofttokenApplication() {
		if(softtokenApp == null)
			startSofttokenApp();
		return softtokenApp;
	}
//	
//	public static boolean getSofttokenStatus() {
//		return PropertiesManager.getCurrent().getSofttokenActivated();
//	}
//	
	public void startSofttokenApp() {
		softtokenApp = new SofttokenApp(this);
		isSubAppRunning = true;
	}
//	
//	public void closeSofttokenApp() {
//		if(null == softtokenApp)
//			return;
//		softtokenApp.cierraAplicacion();
//		softtokenApp = null;
//		isSubAppRunning = false;
//		
//		if(suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController)
//			((MenuSuiteViewController)suiteViewsController.getCurrentViewControllerApp()).setShouldHideLogin(true);
//		
//		suiteViewsController.showMenuSuite(true);
//	}
	// #endregion
	
	
	public void closeBmovilAppSession(){
		
		if(suiteViewsController.getCurrentViewControllerApp() instanceof MenuSuiteViewController)
			((MenuSuiteViewController)suiteViewsController.getCurrentViewControllerApp()).setShouldHideLogin(true);
		
		suiteViewsController.showMenuSuite(true);
		
		
		SuiteAppDesac suiteApp = SuiteAppDesac.getInstance();
		Session session = Session.getInstance(suiteApp.getApplicationContext());

		if(session.getValidity() == Session.VALID_STATUS){
			session.setValidity(Session.INVALID_STATUS);
		}
		
		bmovilApplication.closeBmovilAppSession(suiteViewsController.getCurrentViewControllerApp());;
		
	}


	public static int getResourceId(final String nombre,final String tipo){
		return appContext.getResources().getIdentifier(nombre,tipo, appContext.getPackageName());
	}
	public void setFlagsEnvironmment(final Boolean simulation,final Boolean allowLog,final Boolean emulator,final Boolean development){
		ServerCommons.SIMULATION=simulation;
		ServerCommons.ALLOW_LOG=allowLog;
		ServerCommons.EMULATOR=emulator;
		ServerCommons.DEVELOPMENT=development;

	}

	public CallBackBConnect getCallBackForDesactivada() {
		return callBackForDesactivada;
	}

	public void setCallBackForDesactivada(final CallBackBConnect callBackForDesactivada) {
		this.callBackForDesactivada = callBackForDesactivada;
	}
}
