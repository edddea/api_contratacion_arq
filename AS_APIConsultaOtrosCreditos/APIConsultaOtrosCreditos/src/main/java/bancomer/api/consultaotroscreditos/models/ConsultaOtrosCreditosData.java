package bancomer.api.consultaotroscreditos.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.consultaotroscreditos.io.ParsingHandler;
import bancomer.api.consultaotroscreditos.io.Server;

public class ConsultaOtrosCreditosData implements ParsingHandler  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4801921121098661710L;
	
	private String estado;
	
	private List<Creditos> listaCreditos;

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public List<Creditos> getListaCreditos() {
		return listaCreditos;
	}

	public void setListaCreditos(List<Creditos> listaCreditos) {
		this.listaCreditos = listaCreditos;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		try{
			this.estado = parser.parseNextValue("estado");
			this.listaCreditos = new ArrayList<Creditos>();
			JSONArray datos = parser.parseNextValueWithArray("listaCreditos", false);
			
			if(datos != null){
				JSONObject item;
				for (int i = 0; i < datos.length(); i++) {
					item = datos.getJSONObject(i);
					Creditos cr = new Creditos();
					
					cr.setAdeudo(item.getString("adeudo"));
					cr.setEstatusCredito(item.getString("estatusCredito"));
					cr.setNumeroCredito(item.getString("numeroCredito"));
					cr.setTipoCredito(item.getString("tipoCredito"));
					
					this.listaCreditos.add(cr);
				}
				
			}
		}catch(Exception e){
			if(Server.ALLOW_LOG) Log.e(this.getClass().getSimpleName(), "Error while parsing the json response.", e);
		}
		
	}

}
