package bancomer.api.consultaotroscreditos.models;

import java.io.IOException;

import bancomer.api.common.io.Parser;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.consultaotroscreditos.io.ParsingHandler;

public class DetalleConsultaOtrosCreditosData implements ParsingHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1276459030020044126L;
	
	private String estado;
	private String numeroCredito;
	private String fechaFoma;
	private String fechaDisp;
	private String impCredOtorg;
	private String fechaVencim;
	private String impAbonoCap;
	private String moneda;
	private String plazo;
	private String frecPago;
	private String impAdeudo;
	private String tasaRec;
	private String impRecVig;
	private String impRecVen;
	private String impTotal;
	private String fechaPago;
	private String numRec;
	

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNumeroCredito() {
		return numeroCredito;
	}

	public void setNumeroCredito(String numeroCredito) {
		this.numeroCredito = numeroCredito;
	}

	public String getFechaFoma() {
		return fechaFoma;
	}

	public void setFechaFoma(String fechaFoma) {
		this.fechaFoma = fechaFoma;
	}

	public String getFechaDisp() {
		return fechaDisp;
	}

	public void setFechaDisp(String fechaDisp) {
		this.fechaDisp = fechaDisp;
	}

	public String getImpCredOtorg() {
		return impCredOtorg;
	}

	public void setImpCredOtorg(String impCredOtorg) {
		this.impCredOtorg = impCredOtorg;
	}

	public String getFechaVencim() {
		return fechaVencim;
	}

	public void setFechaVencim(String fechaVencim) {
		this.fechaVencim = fechaVencim;
	}

	public String getImpAbonoCap() {
		return impAbonoCap;
	}

	public void setImpAbonoCap(String impAbonoCap) {
		this.impAbonoCap = impAbonoCap;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getPlazo() {
		return plazo;
	}

	public void setPlazo(String plazo) {
		this.plazo = plazo;
	}

	public String getFrecPago() {
		return frecPago;
	}

	public void setFrecPago(String frecPago) {
		this.frecPago = frecPago;
	}

	public String getImpAdeudo() {
		return impAdeudo;
	}

	public void setImpAdeudo(String impAdeudo) {
		this.impAdeudo = impAdeudo;
	}

	public String getTasaRec() {
		return tasaRec;
	}

	public void setTasaRec(String tasaRec) {
		this.tasaRec = tasaRec;
	}

	public String getImpRecVig() {
		return impRecVig;
	}

	public void setImpRecVig(String impRecVig) {
		this.impRecVig = impRecVig;
	}

	public String getImpRecVen() {
		return impRecVen;
	}

	public void setImpRecVen(String impRecVen) {
		this.impRecVen = impRecVen;
	}

	public String getImpTotal() {
		return impTotal;
	}

	public void setImpTotal(String impTotal) {
		this.impTotal = impTotal;
	}

	public String getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}

	public String getNumRec() {
		return numRec;
	}

	public void setNumRec(String numRec) {
		this.numRec = numRec;
	}

	@Override
	public void process(Parser parser) throws IOException, ParsingException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process(ParserJSON parser) throws IOException, ParsingException {
		
		this.estado = parser.parseNextValue("estado");
		this.numeroCredito = parser.parseNextValue("numeroCredito", false);
		this.fechaFoma = parser.parseNextValue("fechaFoma", false);
		this.fechaDisp = parser.parseNextValue("fechaDisp", false);
		this.impCredOtorg = parser.parseNextValue("impCredOtorg", false);
		this.fechaVencim = parser.parseNextValue("fechaVencim", false);
		this.impAbonoCap = parser.parseNextValue("impAbonoCap", false);
		this.moneda = parser.parseNextValue("moneda", false);
		this.plazo = parser.parseNextValue("plazo", false);
		this.frecPago = parser.parseNextValue("frecPago", false);
		this.impAdeudo = parser.parseNextValue("impAdeudo", false);
		this.tasaRec = parser.parseNextValue("tasaRec", false);
		this.impRecVig = parser.parseNextValue("impRecVig", false);
		this.impRecVen = parser.parseNextValue("impRecVen", false);
		this.impTotal = parser.parseNextValue("impTotal", false);
		this.fechaPago = parser.parseNextValue("fechaPago", false);
		this.numRec = parser.parseNextValue("numRec", false);
		
	}

//	@Override
//	public void process(Parser parser) throws IOException, ParsingException {
//		// TODO Auto-generated method stub
//		
//	}
//
//	@Override
//	public void process(ParserJSON parser) throws IOException, ParsingException {
//		this.estado = parser.parseNextValue("estado");
//		this.numeroCredito = parser.parseNextValue("numeroCredito", false);
//		this.fechaFoma = parser.parseNextValue("fechaFoma", false);
//		this.fechaDisp = parser.parseNextValue("fechaDisp", false);
//		this.impCredOtorg = parser.parseNextValue("impCredOtorg", false);
//		this.fechaVencim = parser.parseNextValue("fechaVencim", false);
//		this.impAbonoCap = parser.parseNextValue("impAbonoCap", false);
//		this.moneda = parser.parseNextValue("moneda", false);
//		this.plazo = parser.parseNextValue("plazo", false);
//		this.frecPago = parser.parseNextValue("frecPago", false);
//		this.impAdeudo = parser.parseNextValue("impAdeudo", false);
//		this.tasaRec = parser.parseNextValue("tasaRec", false);
//		this.impRecVig = parser.parseNextValue("impRecVig", false);
//		this.impRecVen = parser.parseNextValue("impRecVen", false);
//		this.impTotal = parser.parseNextValue("impTotal", false);
//		this.fechaPago = parser.parseNextValue("fechaPago", false);
//		this.numRec = parser.parseNextValue("numRec", false);
//		
//	}

}
