/*
 * Copyright (c) 2010 BBVA. All Rights Reserved
 *
 * This software is the confidential and proprietary information of
 * BBVA ("Confidential Information"). You shall not disclose such
 * Confidential Information and shall use it only in accordance with
 * the terms of the license agreement you entered into with BBVA.
 */
package bancomer.api.consultaotroscreditos.io;

import android.util.Log;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;

import bancomer.api.common.commons.NameValuePair;
import bancomer.api.common.commons.ServerConstants;
import bancomer.api.common.io.ParsingException;
import bancomer.api.common.session.CommonSession;
import bancomer.api.consultaotroscreditos.models.ConsultaOtrosCreditosData;
import bancomer.api.consultaotroscreditos.models.DetalleConsultaOtrosCreditosData;
import bancomer.api.consultaotroscreditos.models.EstadoData;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;


/**
 * Server is responsible of acting as an interface between the application
 * business logic and the HTTP operations. It receive the network operation
 * requests and builds a network operation suitable for HttpInvoker class, which
 * eventually will perform the network connection. Server returns the server
 * response as a ServerResponse object, so that the application logic can
 * perform high level checks and processes.
 * 
 * @author Stefanini IT Solutions.
 */
public class Server extends  ServerCommons {

	/**
	 * Indicates simulation usage.
	 */
	public static final long SIMULATION_RESPONSE_TIME = 5;

	/**
	 * Bancomer Infrastructure.
	 */
	public static final int BANCOMER = 0;

	/**
	 * Stefanini Infrastructure.
	 */
	public static final int STEFANINI = 1;

	/**
	 * Last operation provider.
	 */

	// Change this value to bancomer for production setup
	// public static int PROVIDER = STEFANINI;
	public static int PROVIDER = BANCOMER; // TODO remove for TEST


	// ///////////////////////////////////////////////////////////////////////////
	// Operations identifiers //
	// ///////////////////////////////////////////////////////////////////////////
	public static final int OP_CONSULTA_OTROS_CREDITOS = 105; //0

	public static final int OP_CONSULTA_DETALLE_OTROS_CREDITOS = 120;
		
	public static final int OP_CONSULTA_SMS_OTROS_CREDITOS = 121;

	public static final int OP_CONSULTA_CORREO_OTROS_CREDITOS = 122;
		
		//para simular errores de TDC
		public Boolean errorComunicaciones = false;
		public Boolean errorContrato = false;

	
	/**
	 * Operation codes.
	 */
	//public static final String[] OPERATION_CODES = {"consultarCreditos", "cDetalleCred", "envioSMSConsulta", "envioCorreoConsulta"};
	public static final String[] OPERATION_CODES = { "00", "01", "02", "103",
			"104", "105", "106", "107", "08", "09", "110", "111", "12", "13",
			"14", "115", "16", "17", "141", "19", "20", "21", "22", "123",
			"124", "125", "26", "cambioPerfil", "113", "130", "145", "109", "118",
			"112", "144", "120", "143", "146", "cambioNumCeluar",
			"cambioCuentaAsociada", "suspenderCancelarServicio",
			"actualizarCuentas", "consultaTarjetaContratacionE",
			"consultaLimites", "configuracionLimites",
			"consultaEstatusMantenimiento", "desbloqueoContrasena",
			"quitarSuspension", "102", "contratacionBMovilAlertas",
			"consultaTerminosCondiciones", "envioClaveActivacion",
			"configuracionCorreo", "envioCorreo", "valCredenciales",
			"finalizarContratacionAlertas", "consultaTarjetaST", "autenticacionToken", "204", "203",
			"contratacionE", "finalizarContratacionST",
			"cambioTelefonoAsociadoE", "solicitudST", "mantenimientoAlertas",
			"consultaTerminosCondicionesSesion","solicitarAlertas",/* SPEI*/"ConsultaCuentasSPEI",
			"consultaTerminosCondicionesSPEI", "MantenimientoSpei", "ConsultaCuentaTerceros",
			/*OneClick*/"cDetalleOfertaBMovil","aceptacionILCBmovil","exitoILC","noAceptacionBMovil",
			"cDetalleOfertaBMovil","aceptaOfertaEFI","exitoEFI","SimulacionEFI","detalleConsumoBMovil",
			"polizaConsumoBMovil","consultaContratoConsumoBMovil","oneClickBmovilConsumo",
			"consultaDomiciliacionBovedaConsumoBMovil","consultaContratoBovedaConsumoBmovil",
			"noAceptacionBMovil","exitoConsumoBMovil","consultaTransferenciaSPEI","consultaFrecuentesTercerosCE",
			"consultarDepositosCheques","consultarDepositosEfectivo",
			"consultarTransfPagoServicios","consultarTransfBancomer","consultarTransfMisCuentas",
			"consultarTransfOtrosBancos","consultarClientesBancomer","consultarOtrosBancos","consultarDepositosEfectivo",
			"detalleDepositosEfectivo", "detalleDepositosCheques","importesTDC","retiroSinTarjeta",
			"consultaRetiroSinTarjeta", "claveRetiroSinTarjeta", "sincronizaExportaToken", "consultarCreditos",/*paperles*/"actualizarEstatusEnvioEC",
			"consultarEstadoCuenta","consultarEstatusEnvioEC","consultaTextoPaperless","inhibirEnvioEstadoCuenta","obtenerPeriodosEC",
			"cDetalleCred", "envioSMSConsulta", "envioCorreoConsulta"};

	
	//one Click
			/**
			 * clase de enum para cambio de url;
			 */
			public enum isJsonValueCode{
				ONECLICK, NONE,CONSUMO,DEPOSITOS;
			}
			
			public static isJsonValueCode isjsonvalueCode= isJsonValueCode.NONE;
			//Termina One CLick


	/**
	 * Base URL for providers.
	 */
	public static String[] BASE_URL;

	/**
	 * Path table for provider/operation URLs.
	 */
	public static String[][] OPERATIONS_PATH;

	/**
	 * Operation URLs map.
	 */
	public static Hashtable<String, String> OPERATIONS_MAP = new Hashtable<String, String>();

	/**
	 * Operation providers map.
	 */
	public static Hashtable<String, Integer> OPERATIONS_PROVIDER = new Hashtable<String, Integer>();

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_DATA_PARAMETER = "PAR_INICIO.0";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_PARAMETER = "OPERACION";

	/**
	 * Operation code parameter.
	 */
	public static final String OPERATION_CODE_VALUE = "BAN2O01";
	
	/**
	 * Operation code parameter for RECORTADO.
	 */
	public static final String OPERATION_CODE_VALUE_RECORTADO = "BREC001";

	/**
	 * Operation code parameter for BMOVIL MEJORADO.
	 */
	public static final String OPERATION_CODE_VALUE_BMOVIL_MEJORADO = "MJRS001";
	
	/**
	 * Operation code parameter for DEPOSITOS
	 */
	public static final String JSON_OPERATION_CODE_VALUE_DEPOSITOS = "O300001";

	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_CODE_VALUE = "BAN2O05";
	

	//one CLick
		/**
		 * Operation code parameter for new ops.
		 */
		public static final String JSON_OPERATION_CODE_VALUE_ONECLICK = "ONEC001";
		/**
		 * 
		 */
		
		//one CLick
		/**
		 * Operation code parameter for new ops.
		 */
		public static final String JSON_OPERATION_CODE_VALUE_CONSUMO = "ONEC002";
		/**
		 * 
		 */
	
	/**
	 * Operation code parameter for new ops.
	 */
	public static final String JSON_OPERATION_RECORTADO_CODE_VALUE = "BREC001";

	/**
	 * Operation code for mejoras
	 */
	public static final String JSON_OPERATION_MEJORAS_CODE_VALUE = "MEJO001";
	
	/**
	 * Operation code for consultar otros creditos
	 */
	public static final String JSON_OPERATION_CONSULTAR_OTROS_CREDITOS = "CHIP001";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_PARAMETER = "LOCALE";

	/**
	 * Operation data parameter.
	 */
	public static final String OPERATION_LOCALE_VALUE = "es_ES";


	static {
		setServer();
	}

	public static void setServer(){
		/*if (DEVELOPMENT) {
			setupDevelopmentServer();
		} else {
			setupProductionServer();
		}

		setupOperations(PROVIDER);*/
	}

	public static void setAllowLog(){
		CommonSession cs = CommonSession.getInstance();
		ALLOW_LOG = cs.getCommonSessionService().getAllowLog();
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupProductionServer() {
		BASE_URL = new String[] { "https://www.bancomermovil.com",
				// "https://172.17.100.173",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Produccion"
						: "" };
		OPERATIONS_PATH = new String[][] {
				new String[] {
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consultar Otros Creditos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP Consulta Detalle Otros Creditos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP Envio SMS Consulta Creditos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb"  // OP Envio Correo Consulta Creditos
				},
				new String[] {
						"/mbhxp_mx_web/servlet/ServletOperacionWeb", // OP Consultar Otros Creditos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP Consulta Detalle Otros Creditos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb",  // OP Envio SMS Consulta Creditos
						"/mbhxp_mx_web/servlet/ServletOperacionWeb"  // OP Envio Correo Consulta Creditos

				} };
	}

	/**
	 * Setup production server paths.
	 */
	private static void setupDevelopmentServer() {
		BASE_URL = new String[] {
				"https://www.bancomermovil.net:11443",
				EMULATOR ? "http://10.0.3.10:8080/servermobile/Servidor/Desarrollo"
						: "http://189.254.77.54:8080/servermobile/Servidor/Desarrollo" };
		OPERATIONS_PATH = new String[][] {
				new String[] {
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Consultar Otros Creditos
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP Consulta Detalle Otros Creditos
						"/eexd_mx_web/servlet/ServletOperacionWeb",   // OP Envio SMS Otros Creditos
						"/eexd_mx_web/servlet/ServletOperacionWeb"  // OP Envio Correo Consulta Creditos
				},
				new String[] {
						"/eexd_mx_web/servlet/ServletOperacionWeb", // OP Consultar Otros Creditos
						"/eexd_mx_web/servlet/ServletOperacionWeb",  // OP Consulta Detalle Otros Creditos
						"/eexd_mx_web/servlet/ServletOperacionWeb",   // OP Envio SMS Otros Creditos
						"/eexd_mx_web/servlet/ServletOperacionWeb"  // OP Envio Correo Consulta Creditos
				} };
	}

	/**
	 * Setup operations for a provider.
	 * 
	 * @param provider
	 *            the provider
	 */
	private static void setupOperations(int provider) {

		setupOperation(OP_CONSULTA_OTROS_CREDITOS,provider);
		setupOperation(OP_CONSULTA_DETALLE_OTROS_CREDITOS,provider);
		setupOperation(OP_CONSULTA_SMS_OTROS_CREDITOS,provider);
		setupOperation(OP_CONSULTA_CORREO_OTROS_CREDITOS,provider);

	}

	/**
	 * Setup operation for a provider.
	 * @param operation the operation
	 * @param provider the provider
	 */
	private static void setupOperation(int operation, int provider) {
		String code = OPERATION_CODES[operation];
		OPERATIONS_MAP.put(
				code,
				new StringBuffer(BASE_URL[provider]).append(
						OPERATIONS_PATH[provider][operation]).toString());
		OPERATIONS_PROVIDER.put(code, new Integer(provider));
	}

	/**
	 * Get the operation URL.
	 * @param operation the operation id
	 * @return the operation URL
	 */
	public static String getOperationUrl(String operation) {
		String url = (String) OPERATIONS_MAP.get(operation);
		return url;
	}

	/**
	 * Reference to HttpInvoker, which performs the network connection.
	 */
	private HttpInvoker httpClient = null;
	
	public static String JSON_OPERATION_CONSULTA_INTERBANCARIOS="BXCO001";


	/**
	 * Referencia a ClienteHttp, en lugar de HttpInvoker.
	 */
	private ClienteHttp clienteHttp = null;

	
	// protected Context mContext;

	public ClienteHttp getClienteHttp() {
		return clienteHttp;
	}

	public void setClienteHttp(ClienteHttp clienteHttp) {
		this.clienteHttp = clienteHttp;
	}
	
	public Server() {
		//httpClient = new HttpInvoker();
		//clienteHttp = new ClienteHttp();
	}

	/**
	 * perform the network operation identifier by operationId with the
	 * parameters passed.
	 * @param operationId the identifier of the operation
	 * @param params Hashtable with the parameters as key-value pairs.
	 * @return the response from the server
	 * @throws IOException exception
	 * @throws ParsingException exception
	 */
	public ServerResponseImpl doNetworkOperation(int operationId, Hashtable<String, ?> params) throws IOException, ParsingException, URISyntaxException {

		ServerResponseImpl response = null;
		long sleep = (SIMULATION ? SIMULATION_RESPONSE_TIME : 0);
		Integer provider = (Integer) OPERATIONS_PROVIDER.get(OPERATION_CODES[operationId]);
		if (provider != null) {
			PROVIDER = provider.intValue();
		}

		if(ALLOW_LOG){
			if(Server.ALLOW_LOG) Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Server] Flags");
			if(Server.ALLOW_LOG) Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Server] Simulation -> "+SIMULATION);
			if(Server.ALLOW_LOG) Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Server] Development -> " + DEVELOPMENT);
			if(Server.ALLOW_LOG) Log.d("[CGI-Configuracion-Obligatorio] >> ", "[Server] Emulator -> " + EMULATOR);
		}

		switch (operationId) {
			case OP_CONSULTA_OTROS_CREDITOS:
				response = consultaOtrosCreditos(params);
				break;
			case OP_CONSULTA_DETALLE_OTROS_CREDITOS:
				response = consultaDetalleOtrosCreditos(params);
				break;
			case OP_CONSULTA_SMS_OTROS_CREDITOS:
				response = envioSmsConsulta(params);
				break;
			case OP_CONSULTA_CORREO_OTROS_CREDITOS:
				response = envioCorreoConsulta(params);
				break;
		}
		//Termina SPEI	
		if (sleep > 0) {
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
			}
		}
		return response;
	}
	

	/**
	 * Cancel the ongoing network operation.
	 */
	public void cancelNetworkOperations() {
		httpClient.abort();
		clienteHttp.abort();
	}

	public static final String NUMERO_CLIENTE = "Numerocliente";
	
			private ServerResponseImpl consultaDetalleOtrosCreditos(Hashtable<String, ?> params)	throws IOException, ParsingException, URISyntaxException {
				DetalleConsultaOtrosCreditosData data = new DetalleConsultaOtrosCreditosData();
				ServerResponseImpl response = new ServerResponseImpl(data);
		
				String IUM = (String) params.get(ServerConstants.JSON_IUM_ETIQUETA);
				String numeroCelular = (String) params.get(ServerConstants.NUMERO_CELULAR);
				String numeroCredito = (String) params.get(ServerConstants.NUMERO_CREDITO);
		
				NameValuePair[] parameters = new NameValuePair[3];
				parameters[0] = new NameValuePair(ServerConstants.JSON_IUM_ETIQUETA, IUM);
				parameters[1] = new NameValuePair(ServerConstants.NUMERO_CELULAR, numeroCelular);
				parameters[2] = new NameValuePair(ServerConstants.NUMERO_CREDITO, numeroCredito);
		
				Boolean error = false;
				if (SIMULATION) {
		
					if(error){
						// error
						clienteHttp.simulateOperation(OPERATION_CODES[OP_CONSULTA_DETALLE_OTROS_CREDITOS], parameters, response, "{\"estado\":\"ERROR\",\"codigoMensaje\":\"\",\"descripcionMensaje\":\"ERROR DE COMUNICACIONES\"}", true);
					}else{
		
						final String result = "{\"estado\":\"OK\",\"numeroCredito\":\"00743616519616250459\",\"fechaFoma\":\"25\\10\\2014\",\"fechaDisp\":\"25\\10\\2014\",\"impCredOtorg\":\"7890000\",\"fechaVencim\":\"25\\09\\2016\",\"impAbonoCap\":\"2890000\",\"moneda\":\"Pesos\",\"plazo\":\"120\",\"frecPago\":\"Quincenal\",\"impAdeudo\":\"5000000\",\"tasaRec\":\"41.76\",\"impRecVig\":\"110459\",\"impRecVen\":\"0\",\"impTotal\":\"10000\",\"fechaPago\":\"31\\03\\2015\",\"numRec\":\"45 DE 60\"}";
						
						clienteHttp.simulateOperation(OPERATION_CODES[OP_CONSULTA_DETALLE_OTROS_CREDITOS], parameters, response, result, true);
		
					
					}
				}else{
		
					clienteHttp.invokeOperation(OPERATION_CODES[OP_CONSULTA_DETALLE_OTROS_CREDITOS], parameters, response, false, true);
				}
				return response;
			}
	
			private ServerResponseImpl consultaOtrosCreditos(Hashtable<String, ?> params)	throws IOException, ParsingException, URISyntaxException {
				ConsultaOtrosCreditosData data = new ConsultaOtrosCreditosData();
				ServerResponseImpl response = new ServerResponseImpl(data);
				isjsonvalueCode=isJsonValueCode.NONE;

				String IUM = (String) params.get(ServerConstants.JSON_IUM_ETIQUETA);
				String numeroCelular = (String) params.get(ServerConstants.NUMERO_CELULAR);
				String codigoNip = (String) params.get(ServerConstants.CODIGO_NIP);
				String codigoCvv2 = (String) params.get(ServerConstants.CODIGO_CVV2);
				String codigoOtp = (String) params.get(ServerConstants.CODIGO_OTP);
				String cadAut = (String) params.get(ServerConstants.CADENA_AUTENTICACION);
				String cveAcceso = (String) params.get(ServerConstants.CVE_ACCESO);
				String campoTarjeta = (String) params.get(ServerConstants.TARJETA_5DIG);

				NameValuePair[] parameters = new NameValuePair[8];
				parameters[0] = new NameValuePair(ServerConstants.JSON_IUM_ETIQUETA, IUM);
				parameters[1] = new NameValuePair(ServerConstants.NUMERO_CELULAR, numeroCelular);
				parameters[2] = new NameValuePair(ServerConstants.CODIGO_NIP, codigoNip);
				parameters[3] = new NameValuePair(ServerConstants.CODIGO_CVV2,codigoCvv2);
				parameters[4] = new NameValuePair(ServerConstants.CODIGO_OTP, codigoOtp);
				parameters[5] = new NameValuePair(ServerConstants.CADENA_AUTENTICACION, cadAut);
				parameters[6] = new NameValuePair(ServerConstants.CVE_ACCESO, cveAcceso);
				parameters[7] = new NameValuePair(ServerConstants.TARJETA_5DIG, campoTarjeta);

				if (SIMULATION) {
//					errorComunicaciones=true;

					if(errorComunicaciones){
						// error
						clienteHttp.simulateOperation(OPERATION_CODES[OP_CONSULTA_OTROS_CREDITOS], parameters, response, "{\"estado\":\"ERROR\",\"codigoMensaje\":\"UGE0351\",\"descripcionMensaje\":\"NO TIENE CREDITOS OTORGADOS\"}", true);
					}else{

						final String result = "{\"estado\":\"OK\",\"listaCreditos\":[{\"numeroCredito\":\"00743616519616250458\",\"tipoCredito\":\"P\",\"adeudo\":\"1456900\",\"estatusCredito\": \"0\"}, {\"numeroCredito\":\"00743616519616250459\",\"tipoCredito\":\"C\",\"adeudo\": \"9500000\",\"estatusCredito\":\"2\"},{\"numeroCredito\":\"00743616519616254474\",\"tipoCredito\":\"H\",\"adeudo\":\"9456900\",\"estatusCredito\": \"V\"},{\"numeroCredito\":\"00743616519616254494\",\"tipoCredito\":\"N\",\"adeudo\":\"1456900\",\"estatusCredito\": \"V\"},{\"numeroCredito\":\"00743616519616254457\",\"tipoCredito\":\"P\",\"adeudo\":\"1456900\",\"estatusCredito\": \"C\"},{\"numeroCredito\":\"00743616519616250458\",\"tipoCredito\":\"P\",\"adeudo\":\"1456900\",\"estatusCredito\": \"0\"}, {\"numeroCredito\":\"00743616519616250459\",\"tipoCredito\":\"C\",\"adeudo\": \"9500000\",\"estatusCredito\":\"2\"},{\"numeroCredito\":\"00743616519616254474\",\"tipoCredito\":\"H\",\"adeudo\":\"9456900\",\"estatusCredito\": \"V\"},{\"numeroCredito\":\"00743616519616254494\",\"tipoCredito\":\"N\",\"adeudo\":\"1456900\",\"estatusCredito\": \"V\"},{\"numeroCredito\":\"00743616519616254457\",\"tipoCredito\":\"P\",\"adeudo\":\"1456900\",\"estatusCredito\": \"C\"},{\"numeroCredito\":\"00743616519616250458\",\"tipoCredito\":\"P\",\"adeudo\":\"1456900\",\"estatusCredito\": \"0\"}, {\"numeroCredito\":\"00743616519616250459\",\"tipoCredito\":\"C\",\"adeudo\": \"9500000\",\"estatusCredito\":\"2\"},{\"numeroCredito\":\"00743616519616254474\",\"tipoCredito\":\"H\",\"adeudo\":\"9456900\",\"estatusCredito\": \"V\"},{\"numeroCredito\":\"00743616519616254494\",\"tipoCredito\":\"N\",\"adeudo\":\"1456900\",\"estatusCredito\": \"V\"},{\"numeroCredito\":\"00743616519616254457\",\"tipoCredito\":\"P\",\"adeudo\":\"1456900\",\"estatusCredito\": \"C\"},{\"numeroCredito\":\"00743616519616250458\",\"tipoCredito\":\"P\",\"adeudo\":\"1456900\",\"estatusCredito\": \"0\"}, {\"numeroCredito\":\"00743616519616250459\",\"tipoCredito\":\"C\",\"adeudo\": \"9500000\",\"estatusCredito\":\"2\"},{\"numeroCredito\":\"00743616519616254474\",\"tipoCredito\":\"H\",\"adeudo\":\"9456900\",\"estatusCredito\": \"V\"},{\"numeroCredito\":\"00743616519616254494\",\"tipoCredito\":\"N\",\"adeudo\":\"1456900\",\"estatusCredito\": \"V\"},{\"numeroCredito\":\"00743616519616254457\",\"tipoCredito\":\"P\",\"adeudo\":\"1456900\",\"estatusCredito\": \"C\"}]}";
						
						
						
						clienteHttp.simulateOperation(OPERATION_CODES[OP_CONSULTA_OTROS_CREDITOS], parameters, response, result, true);

					
					}
				}else{

					clienteHttp.invokeOperation(OPERATION_CODES[OP_CONSULTA_OTROS_CREDITOS], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponseImpl envioSmsConsulta(Hashtable<String, ?> params)	throws IOException, ParsingException, URISyntaxException {
				EstadoData data = new EstadoData();
				ServerResponseImpl response = new ServerResponseImpl(data);
		
				String IUM = (String) params.get(ServerConstants.JSON_IUM_ETIQUETA);
				String numeroCelular = (String) params.get(ServerConstants.NUMERO_CELULAR);
				String tipoCredito = (String) params.get(ServerConstants.TIPO_CREDITO);
				String numeroCredito = (String) params.get(ServerConstants.NUMERO_CREDITO);
		
				NameValuePair[] parameters = new NameValuePair[4];
				parameters[0] = new NameValuePair(ServerConstants.JSON_IUM_ETIQUETA, IUM);
				parameters[1] = new NameValuePair(ServerConstants.NUMERO_CELULAR, numeroCelular);
				parameters[2] = new NameValuePair(ServerConstants.TIPO_CREDITO, tipoCredito);
				parameters[3] = new NameValuePair(ServerConstants.NUMERO_CREDITO, numeroCredito);
		
				if (SIMULATION) {
					//errorComunicaciones=true;
		
					if(errorComunicaciones){
						// error
						clienteHttp.simulateOperation(OPERATION_CODES[OP_CONSULTA_SMS_OTROS_CREDITOS], parameters, response, "{\"estado\":\"ERROR\",\"codigoMensaje\":\"MBANK0001\",\"descripcionMensaje\":\"ERROR DE COMUNICACIONES\"}", true);
					}else{
		
						final String result = "{\"estado\":\"OK\"}";
						
						clienteHttp.simulateOperation(OPERATION_CODES[OP_CONSULTA_SMS_OTROS_CREDITOS], parameters, response, result, true);
		
					
					}
				}else{
		
					clienteHttp.invokeOperation(OPERATION_CODES[OP_CONSULTA_SMS_OTROS_CREDITOS], parameters, response, false, true);
				}
				return response;
			}
			
			private ServerResponseImpl envioCorreoConsulta(Hashtable<String, ?> params)	throws IOException, ParsingException, URISyntaxException {
				EstadoData data = new EstadoData();
				ServerResponseImpl response = new ServerResponseImpl(data);
		
				String IUM = (String) params.get(ServerConstants.JSON_IUM_ETIQUETA);
				String numeroCelular = (String) params.get(ServerConstants.NUMERO_CELULAR);
				String tipoCredito = (String) params.get(ServerConstants.TIPO_CREDITO);
				String numeroCredito = (String) params.get(ServerConstants.NUMERO_CREDITO);
		
				NameValuePair[] parameters = new NameValuePair[4];
				parameters[0] = new NameValuePair(ServerConstants.JSON_IUM_ETIQUETA, IUM);
				parameters[1] = new NameValuePair(ServerConstants.NUMERO_CELULAR, numeroCelular);
				parameters[2] = new NameValuePair(ServerConstants.TIPO_CREDITO, tipoCredito);
				parameters[3] = new NameValuePair(ServerConstants.NUMERO_CREDITO, numeroCredito);
		
				if (SIMULATION) {
					//errorComunicaciones=true;
		
					if(errorComunicaciones){
						// error
						clienteHttp.simulateOperation(OPERATION_CODES[OP_CONSULTA_CORREO_OTROS_CREDITOS], parameters, response, "{\"estado\":\"ERROR\",\"codigoMensaje\":\"MBANK0001\",\"descripcionMensaje\":\"ERROR DE COMUNICACIONES\"}", true);
					}else{
		
						final String result = "{\"estado\":\"OK\"}";
						
						clienteHttp.simulateOperation(OPERATION_CODES[OP_CONSULTA_CORREO_OTROS_CREDITOS], parameters, response, result, true);
		
					
					}
				}else{
		
					clienteHttp.invokeOperation(OPERATION_CODES[OP_CONSULTA_CORREO_OTROS_CREDITOS], parameters, response, false, true);
				}
				return response;
			}

}