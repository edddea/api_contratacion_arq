package bancomer.api.consultaotroscreditos.gui.delegates;

import java.util.Hashtable;

import android.app.Activity;
import bancomer.api.common.commons.ServerConstants;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consultaotroscreditos.R;
import bancomer.api.consultaotroscreditos.implementations.BaseDelegateImpl;
import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;
import bancomer.api.consultaotroscreditos.io.ParsingHandler;
import bancomer.api.consultaotroscreditos.io.Server;
import bancomer.api.consultaotroscreditos.models.ConsultaOtrosCreditosData;


public class ConsultaData  extends BaseDelegateImpl{

	public final static long CONSULTA_DATA_DELEGATE_ID = 0x4fd454ce67b39f7L;
	
	// Variable para conocer si la petición ha acabado. Variable de espera para el callback
	private Boolean finishedCall;
	
	// Respuesta de la peticion. Se lee al ejecutar el callback
	private ServerResponse response;
	
	public Boolean getFinishedCall() {
		return finishedCall;
	}

	public void setFinishedCall(Boolean finishedCall) {
		this.finishedCall = finishedCall;
	}

	public ServerResponse getResponse() {
		return response;
	}

	public void setResponse(ServerResponse response) {
		this.response = response;
	}

	public void consultaOtrosCreditos(Activity activity) {
		// Al iniciar toda peticion se debe poner a false
		finishedCall = false;
		
		InitConsultaOtrosCreditos.getInstance().getBaseViewController().muestraIndicadorActividad(activity,
				activity.getString(R.string.alert_operation),
				activity.getString(R.string.alert_connecting));
		
		InitConsultaOtrosCreditos.getInstance().getBaseViewController().setDelegate(this);
		InitConsultaOtrosCreditos.getInstance().getBaseViewController().setActivity(activity);
		
		//prepare data
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		int operacion = 0;
		operacion = Server.OP_CONSULTA_OTROS_CREDITOS;
		paramTable.put(ServerConstants.OPERACION, "consultarCreditos");
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA, InitConsultaOtrosCreditos.getInstance().getConsultaSend().getIUM());
		paramTable.put(ServerConstants.NUMERO_CELULAR, InitConsultaOtrosCreditos.getInstance().getConsultaSend().getUsername());
		paramTable.put(ServerConstants.CODIGO_NIP, InitConsultaOtrosCreditos.getInstance().getConsultaSend().getCodigoNip());
		paramTable.put(ServerConstants.CODIGO_CVV2, InitConsultaOtrosCreditos.getInstance().getConsultaSend().getCodigoCvv2());
		paramTable.put(ServerConstants.CODIGO_OTP, InitConsultaOtrosCreditos.getInstance().getConsultaSend().getCodigoOtp());
		paramTable.put(ServerConstants.CADENA_AUTENTICACION, InitConsultaOtrosCreditos.getInstance().getConsultaSend().getCadenaAutenticacion());
		paramTable.put(ServerConstants.CVE_ACCESO, InitConsultaOtrosCreditos.getInstance().getConsultaSend().getCveAcceso());
		paramTable.put(ServerConstants.TARJETA_5DIG, InitConsultaOtrosCreditos.getInstance().getConsultaSend().getTarjeta5Dig());
        
        doNetworkOperation(operacion, paramTable, true, new ConsultaOtrosCreditosData(), InitConsultaOtrosCreditos.getInstance().getBaseViewController());
	}
	
	public void doNetworkOperation(int operationId, Hashtable<String, ?> params, boolean isJson,ParsingHandler handler, BaseViewController caller) {
		InitConsultaOtrosCreditos.getInstance().getBaseSubApp().invokeNetworkOperation(operationId, params,isJson,handler, caller, true);
	}
	
	public void analyzeResponse(int operationId, ServerResponse response) {
		// Devolvemos la respuesta dada por el server y actualizamos el flag, indicando que la peticion ha finalizado
		this.response = response;
		finishedCall = true;
	}

}
