package bancomer.api.consultaotroscreditos.gui.delegates;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.bancomer.base.callback.CallBackAPI;

import java.util.ArrayList;
import java.util.Hashtable;

import bancomer.api.common.commons.ServerConstants;
import bancomer.api.common.commons.Tools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consultaotroscreditos.R;
import bancomer.api.consultaotroscreditos.gui.controllers.ConsultaOtrosCreditosViewController;
import bancomer.api.consultaotroscreditos.gui.controllers.DetalleConsultaOtrosCreditosViewController;
import bancomer.api.consultaotroscreditos.implementations.BaseDelegateImpl;
import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;
import bancomer.api.consultaotroscreditos.io.ParsingHandler;
import bancomer.api.consultaotroscreditos.io.Server;
import bancomer.api.consultaotroscreditos.models.ConsultaOtrosCreditosData;
import bancomer.api.consultaotroscreditos.models.Creditos;
import bancomer.api.consultaotroscreditos.models.DetalleConsultaOtrosCreditosData;
import bancomer.api.consultaotroscreditos.models.EstadoData;
import bancomer.api.pagarcreditos.gui.controllers.PagarCreditoViewController;
import bancomer.api.pagarcreditos.implementations.InitPagoCredito;
import bancomer.api.pagarcreditos.io.BanderasServer;
import bancomer.api.pagarcreditos.models.Credito;
import bancomer.api.pagarcreditos.models.ImportesPagoCreditoData;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;

public class ConsultaOtrosCreditosDelegate extends BaseDelegateImpl implements CallBackAPI {

	public final static long CONSULTA_OTROS_CREDITOS_DELEGATE_ID = 0x4fd454ce67b39f5L;

	private ConsultaOtrosCreditosViewController consultaOtrosCreditosViewController;

	private DetalleConsultaOtrosCreditosViewController detalleConsultaOtrosCreditosViewController;

	private ConsultaOtrosCreditosData consultaOtrosCreditosData;

	private Creditos creditoSeleccionado;

	private DetalleConsultaOtrosCreditosData detalleConsultaOtrosCreditosData;
	
	private Boolean comesFromOut = false;

	/**
	 * Getters & Setters
	 * 
	 */

	public ConsultaOtrosCreditosData getConsultaOtrosCreditosData() {
		return consultaOtrosCreditosData;
	}

	public Boolean getComesFromOut() {
		return comesFromOut;
	}

	public void setComesFromOut(Boolean comesFromOut) {
		this.comesFromOut = comesFromOut;
	}

	public Creditos getCreditoSeleccionado() {
		return creditoSeleccionado;
	}

	public void setCreditoSeleccionado(Creditos creditoSeleccionado) {
		this.creditoSeleccionado = creditoSeleccionado;
	}

	public void setConsultaOtrosCreditosData(
			ConsultaOtrosCreditosData consultaOtrosCreditosData) {
		this.consultaOtrosCreditosData = consultaOtrosCreditosData;
	}

	public ConsultaOtrosCreditosViewController getConsultaOtrosCreditosViewController() {
		return consultaOtrosCreditosViewController;
	}

	public void setConsultaOtrosCreditosViewController(
			ConsultaOtrosCreditosViewController consultaOtrosCreditosViewController) {

		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ",
				"[ConsultaOtrosCreditosDelegate] Se setea el viewController para ConsultaOtrosCreditosViewController");
		this.consultaOtrosCreditosViewController = consultaOtrosCreditosViewController;
	}

	public DetalleConsultaOtrosCreditosViewController getDetalleConsultaOtrosCreditosViewController() {
		return detalleConsultaOtrosCreditosViewController;
	}

	public void setDetalleConsultaOtrosCreditosViewController(
			DetalleConsultaOtrosCreditosViewController detalleConsultaOtrosCreditosViewController) {
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ",
				"[ConsultaOtrosCreditosDelegate] Se setea el viewController para DetalleConsultaOtrosCreditosViewController");
		this.detalleConsultaOtrosCreditosViewController = detalleConsultaOtrosCreditosViewController;
	}

	public DetalleConsultaOtrosCreditosData getDetalleConsultaOtrosCreditosData() {
		return detalleConsultaOtrosCreditosData;
	}

	public void setDetalleConsultaOtrosCreditosData(
			DetalleConsultaOtrosCreditosData detalleConsultaOtrosCreditosData) {
		this.detalleConsultaOtrosCreditosData = detalleConsultaOtrosCreditosData;
	}

	/**
	 * End Getters & Setters
	 * 
	 */

	public void performAction(Object obj) {
		// Hacer show de la ventana de detalle
		// movimientoSeleccionadoCOC =
		// consultaOtrosCreditosViewController.getListaSeleccion().getOpcionSeleccionada();
		creditoSeleccionado = consultaOtrosCreditosData.getListaCreditos().get(
				consultaOtrosCreditosViewController.getListaSeleccion()
						.getOpcionSeleccionada());

		if(Server.ALLOW_LOG) Log.d(">> CGI - EA#5 - RN8",
				"Comprobación estatusCredito ? 2 || 3 || C || V ");
		if (!creditoSeleccionado.getEstatusCredito().equalsIgnoreCase("2")
				&& !creditoSeleccionado.getEstatusCredito().equalsIgnoreCase(
						"3")
				&& !creditoSeleccionado.getEstatusCredito().equalsIgnoreCase(
						"C")
				&& !creditoSeleccionado.getEstatusCredito().equalsIgnoreCase(
						"V")) {
			if(Server.ALLOW_LOG) Log.d(">> CGI - EA#5 - RN8", "A lo mejor se muestra el detalle");

			if (!bancomer.api.common.commons.Tools.isNetworkConnected(consultaOtrosCreditosViewController)) {
				InitConsultaOtrosCreditos
						.getInstance()
						.getBaseViewController()
						.showErrorMessage(consultaOtrosCreditosViewController, consultaOtrosCreditosViewController.getString(R.string.error_communications));

				return;
			}

			//TODO indicador
			consultaOtrosCreditosViewController.getBaseViewController().muestraIndicadorActividad(consultaOtrosCreditosViewController, consultaOtrosCreditosViewController.getString(R.string.alert_operation),	consultaOtrosCreditosViewController.getString(R.string.alert_connecting));
			detalleConsultaOtrosCreditos();
		} else if (creditoSeleccionado.getEstatusCredito()
				.equalsIgnoreCase("V")) {
			if(Server.ALLOW_LOG) Log.d(">> CGI - EA#5 - RN8", "El Estatus es vencido");

			if (creditoSeleccionado.getTipoCredito().equalsIgnoreCase("C")
					|| creditoSeleccionado.getTipoCredito().equalsIgnoreCase(
							"P")
					|| creditoSeleccionado.getTipoCredito().equalsIgnoreCase(
							"N")) {

				if(Server.ALLOW_LOG) Log.d(">> CGI - EA#5 - RN8", "Vencido - Tipo C");
				InitConsultaOtrosCreditos
						.getInstance()
						.getBaseViewController()
						.showInformationAlert(
								consultaOtrosCreditosViewController,
								R.string.bmovil_menu_miscuentas_otroscreditos_error_estatusVC);

			} else if (creditoSeleccionado.getTipoCredito().equalsIgnoreCase(
					"H")) {

				InitConsultaOtrosCreditos
						.getInstance()
						.getBaseViewController()
						.showInformationAlert(
								consultaOtrosCreditosViewController,
								R.string.bmovil_menu_miscuentas_otroscreditos_error_estatusVH);

			}
		} else if (creditoSeleccionado.getEstatusCredito()
				.equalsIgnoreCase("2")
				|| creditoSeleccionado.getEstatusCredito()
						.equalsIgnoreCase("3")
				|| creditoSeleccionado.getEstatusCredito()
						.equalsIgnoreCase("C")) {

			if(Server.ALLOW_LOG) Log.d(">> CGI - EA#5 - RN8",
					"Aparentemente el Estatus es bloqueado");
			if (creditoSeleccionado.getTipoCredito().equalsIgnoreCase("C")
					|| creditoSeleccionado.getTipoCredito().equalsIgnoreCase(
							"P")
					|| creditoSeleccionado.getTipoCredito().equalsIgnoreCase(
							"N")) {

				if(Server.ALLOW_LOG) Log.d(">> CGI - EA#5 - RN8", "Bloqueado - Tipo C");
				InitConsultaOtrosCreditos
						.getInstance()
						.getBaseViewController()
						.showInformationAlert(
								consultaOtrosCreditosViewController,
								R.string.bmovil_menu_miscuentas_otroscreditos_error_estatusBC);

			} else if (creditoSeleccionado.getTipoCredito().equalsIgnoreCase(
					"H")) {

				if(Server.ALLOW_LOG) Log.d(">> CGI - EA#5 - RN8", "Bloqueado - Tipo H");
				InitConsultaOtrosCreditos
						.getInstance()
						.getBaseViewController()
						.showInformationAlert(
								consultaOtrosCreditosViewController,
								R.string.bmovil_menu_miscuentas_otroscreditos_error_estatusBH);

			}
		}
	}

	public void consultaOtrosCreditos() {

		InitConsultaOtrosCreditos.getInstance().getBaseViewController().setActivity(InitConsultaOtrosCreditos.getInstance().getActivity());
		InitConsultaOtrosCreditos.getInstance().getBaseViewController().setDelegate(this);

		// prepare data
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		int operacion = 0;
		operacion = Server.OP_CONSULTA_OTROS_CREDITOS;
		paramTable.put(ServerConstants.OPERACION, "consultarCreditos");
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA,
				InitConsultaOtrosCreditos.getInstance().getConsultaSend()
						.getIUM());
		paramTable.put(ServerConstants.NUMERO_CELULAR,
				InitConsultaOtrosCreditos.getInstance().getConsultaSend()
						.getUsername());
		paramTable.put(ServerConstants.CODIGO_NIP, InitConsultaOtrosCreditos
				.getInstance().getConsultaSend().getCodigoNip());
		paramTable.put(ServerConstants.CODIGO_CVV2, InitConsultaOtrosCreditos
				.getInstance().getConsultaSend().getCodigoCvv2());
		paramTable.put(ServerConstants.CODIGO_OTP, InitConsultaOtrosCreditos
				.getInstance().getConsultaSend().getCodigoOtp());
		paramTable.put(ServerConstants.CADENA_AUTENTICACION,
				InitConsultaOtrosCreditos.getInstance().getConsultaSend()
						.getCadenaAutenticacion());
		paramTable.put(ServerConstants.CVE_ACCESO, InitConsultaOtrosCreditos
				.getInstance().getConsultaSend().getCveAcceso());
		paramTable.put(ServerConstants.TARJETA_5DIG, InitConsultaOtrosCreditos
				.getInstance().getConsultaSend().getTarjeta5Dig());

		doNetworkOperation(operacion, paramTable, true, new ConsultaOtrosCreditosData(), InitConsultaOtrosCreditos.getInstance().getBaseViewController());

	}
	
	public void outDetalleConsultaOtrosCreditos() {

		if(Server.ALLOW_LOG) Log.d(">> CGI", "Relleno petición Detalle Consulta otros Créditos");

		InitConsultaOtrosCreditos.getInstance().getBaseViewController().setActivity(InitConsultaOtrosCreditos.getInstance().getActivity());
		InitConsultaOtrosCreditos.getInstance().getBaseViewController().setDelegate(this);
		
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		int operacion = Server.OP_CONSULTA_DETALLE_OTROS_CREDITOS;

		paramTable.put(ServerConstants.OPERACION, "cDetalleCred");
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA,
				InitConsultaOtrosCreditos.getInstance().getConsultaSend()
						.getIUM());
		paramTable.put(ServerConstants.NUMERO_CELULAR,
				InitConsultaOtrosCreditos.getInstance().getConsultaSend()
						.getUsername());
		paramTable.put(ServerConstants.NUMERO_CREDITO,
				creditoSeleccionado.getNumeroCredito());

		doNetworkOperation(operacion, paramTable, true, new DetalleConsultaOtrosCreditosData(), InitConsultaOtrosCreditos.getInstance().getBaseViewController());
	}

	public void detalleConsultaOtrosCreditos() {

		if(Server.ALLOW_LOG) Log.d(">> CGI", "Relleno petición Detalle Consulta otros Créditos");

		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		int operacion = Server.OP_CONSULTA_DETALLE_OTROS_CREDITOS;

		paramTable.put(ServerConstants.OPERACION, "cDetalleCred");
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA,
				InitConsultaOtrosCreditos.getInstance().getConsultaSend()
						.getIUM());
		paramTable.put(ServerConstants.NUMERO_CELULAR,
				InitConsultaOtrosCreditos.getInstance().getConsultaSend()
						.getUsername());
		paramTable.put(ServerConstants.NUMERO_CREDITO,
				creditoSeleccionado.getNumeroCredito());

		doNetworkOperation(operacion, paramTable, true, new DetalleConsultaOtrosCreditosData(), InitConsultaOtrosCreditos.getInstance().getBaseViewController());
	}

	/**
	 * Peticion envio de email
	 */
	public void emailOtrosCreditos() {
		if (!bancomer.api.common.commons.Tools.isNetworkConnected(getContextSendEmailSms())) {
			InitConsultaOtrosCreditos
					.getInstance()
					.getBaseViewController()
					.showErrorMessage(detalleConsultaOtrosCreditosViewController, detalleConsultaOtrosCreditosViewController.getString(R.string.error_communications));

			return;
		}

		detalleConsultaOtrosCreditosViewController.getBaseViewController()
				.muestraIndicadorActividad(
						detalleConsultaOtrosCreditosViewController,
						detalleConsultaOtrosCreditosViewController
								.getString(R.string.alert_operation),
						detalleConsultaOtrosCreditosViewController
								.getString(R.string.alert_connecting));

		// prepare data
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		int operacion = Server.OP_CONSULTA_CORREO_OTROS_CREDITOS;
		paramTable.put(ServerConstants.OPERACION, "envioCorreoConsulta");
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA,
				InitConsultaOtrosCreditos.getInstance().getConsultaSend()
						.getIUM());
		paramTable.put(ServerConstants.NUMERO_CELULAR,
				InitConsultaOtrosCreditos.getInstance().getConsultaSend()
						.getUsername());
		paramTable.put(ServerConstants.TIPO_CREDITO,
				creditoSeleccionado.getTipoCredito());
		paramTable.put(ServerConstants.NUMERO_CREDITO,
				creditoSeleccionado.getNumeroCredito());

		doNetworkOperation(operacion, paramTable, true, new EstadoData(), InitConsultaOtrosCreditos.getInstance().getBaseViewController());
	}

	/**
	 * Peticion envio de sms
	 */
	public void smsOtrosCreditos() {
		if (!bancomer.api.common.commons.Tools.isNetworkConnected(getContextSendEmailSms())) {
			InitConsultaOtrosCreditos
					.getInstance()
					.getBaseViewController()
					.showErrorMessage(detalleConsultaOtrosCreditosViewController, detalleConsultaOtrosCreditosViewController.getString(R.string.error_communications));

			return;
		}

		detalleConsultaOtrosCreditosViewController.getBaseViewController()
				.muestraIndicadorActividad(
						detalleConsultaOtrosCreditosViewController,
						detalleConsultaOtrosCreditosViewController
								.getString(R.string.alert_operation),
						detalleConsultaOtrosCreditosViewController
								.getString(R.string.alert_connecting));

		// prepare data
		Hashtable<String, String> paramTable = new Hashtable<String, String>();
		int operacion = Server.OP_CONSULTA_SMS_OTROS_CREDITOS;
		paramTable.put(ServerConstants.OPERACION, "envioSMSConsulta");
		paramTable.put(ServerConstants.JSON_IUM_ETIQUETA,
				InitConsultaOtrosCreditos.getInstance().getConsultaSend()
						.getIUM());
		paramTable.put(ServerConstants.NUMERO_CELULAR,
				InitConsultaOtrosCreditos.getInstance().getConsultaSend()
						.getUsername());
		paramTable.put(ServerConstants.TIPO_CREDITO,
				creditoSeleccionado.getTipoCredito());
		paramTable.put(ServerConstants.NUMERO_CREDITO,
				creditoSeleccionado.getNumeroCredito());

		doNetworkOperation(operacion, paramTable, true, new EstadoData(), InitConsultaOtrosCreditos.getInstance().getBaseViewController());
	}

	public void doNetworkOperation(int operationId,
								   Hashtable<String, ?> params, boolean isJson,ParsingHandler handler, BaseViewController caller) {
		InitConsultaOtrosCreditos.getInstance().getBaseSubApp()
				.invokeNetworkOperation(operationId, params, isJson, handler, caller, true);
	}

	public void analyzeResponse(int operationId, ServerResponse response) {
		if (operationId == Server.OP_CONSULTA_OTROS_CREDITOS) {
			if(Server.ALLOW_LOG) Log.d(">> CGI", "Entra por consulta Otros Creditos");
			
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta otros creditos >> Success");
				consultaOtrosCreditosData = (ConsultaOtrosCreditosData) response
						.getResponse();

			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta otros creditos >> Warning");

			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta otros creditos >> Error");

			}

			InitConsultaOtrosCreditos.getInstance().returnDataToPrincipal(String.valueOf(ApiConstants.OP_CONSULTA_OTROS_CREDITOS),response);
			
		} else if (operationId == Server.OP_CONSULTA_DETALLE_OTROS_CREDITOS) {

			if(Server.ALLOW_LOG) Log.d(">> CGI", "Entra por consulta Detalle Otros Creditos");
			
			if(!comesFromOut){
				//FIXME indicador
				consultaOtrosCreditosViewController.getBaseViewController().ocultaIndicadorActividad();
				
				if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
					if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta Detalle otros creditos >> Success");
					detalleConsultaOtrosCreditosData = (DetalleConsultaOtrosCreditosData) response
							.getResponse();
					if(InitConsultaOtrosCreditos.getInstance().getActivity().getLocalClassName().contains(InitConsultaOtrosCreditos.MENU_OPCIONES_PAGAR)){
						if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta Detalle otros creditos >> Entró Por menu PAGAR");
						if(Server.ALLOW_LOG) Log.d(">> CGI", "Se ejecuta el caso de uso pagar Credito");
						consultarImportesPagoCredito(getCreditoSeleccionado().getNumeroCredito());

					} else {
						if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta Detalle otros creditos >> Entró Por menu Consulta Creditos");
						InitConsultaOtrosCreditos.getInstance().showDetallesOtrosCreditos();
					}

				} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
					if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta Detalle otros creditos >> Warning");
					InitConsultaOtrosCreditos
							.getInstance()
							.getBaseViewController()
							.showInformationAlert(
									consultaOtrosCreditosViewController,
									response.getMessageText());
	
				} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
					if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta Detalle otros creditos >> Error");
					InitConsultaOtrosCreditos.getInstance().getBaseViewController().showInformationAlert(consultaOtrosCreditosViewController,response.getMessageText());
	
				}
			}else{
				comesFromOut = false;
				
				if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
					if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta Detalle otros creditos >> Success");
					detalleConsultaOtrosCreditosData = (DetalleConsultaOtrosCreditosData) response
							.getResponse();

				} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
					if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta otros creditos >> Warning");

				} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
					if(Server.ALLOW_LOG) Log.d(">> CGI", "Consulta otros creditos >> Error");

				}

				InitConsultaOtrosCreditos.getInstance().returnDataToPrincipal(ApiConstants.OPERATION_CODES[Server.OP_CONSULTA_DETALLE_OTROS_CREDITOS], response);

			}
			
		} else if (operationId == Server.OP_CONSULTA_CORREO_OTROS_CREDITOS) {
			if(Server.ALLOW_LOG) Log.d(">> CGI", "Entra por Correo Otros Creditos");
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "Correo otros creditos >> Success");

				InitConsultaOtrosCreditos
						.getInstance()
						.getBaseViewController()
						.showInformationAlert(
								detalleConsultaOtrosCreditosViewController,
								detalleConsultaOtrosCreditosViewController
										.getString(R.string.bmovil_enviar_correo_exito));

			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "Correo otros creditos >> Warning");

				InitConsultaOtrosCreditos
						.getInstance()
						.getBaseViewController()
						.showInformationAlert(
								detalleConsultaOtrosCreditosViewController,
								response.getMessageText());

			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "Correo otros creditos >> Error");

				InitConsultaOtrosCreditos.getInstance().getBaseViewController()
						.ocultaIndicadorActividad();
				InitConsultaOtrosCreditos
						.getInstance()
						.getBaseViewController()
						.showInformationAlert(
								detalleConsultaOtrosCreditosViewController,
								response.getMessageText());

			}
		} else if (operationId == Server.OP_CONSULTA_SMS_OTROS_CREDITOS) {
			if(Server.ALLOW_LOG) Log.d(">> CGI", "Entra por Sms Otros Creditos");
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "Sms otros creditos >> Success");

				detalleConsultaOtrosCreditosViewController
						.getBaseViewController().setOpcionesMenu(
								detalleConsultaOtrosCreditosViewController
										.getMenuOptionsNoSms());
				InitConsultaOtrosCreditos
						.getInstance()
						.getBaseViewController()
						.showInformationAlert(
								detalleConsultaOtrosCreditosViewController,
								detalleConsultaOtrosCreditosViewController
										.getString(R.string.sms_efectivoSuccess));

			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "Sms otros creditos >> Warning");

				InitConsultaOtrosCreditos
						.getInstance()
						.getBaseViewController()
						.showInformationAlert(
								detalleConsultaOtrosCreditosViewController,
								response.getMessageText());

			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				if(Server.ALLOW_LOG) Log.d(">> CGI", "Sms otros creditos >> Error");

				InitConsultaOtrosCreditos.getInstance().getBaseViewController()
						.ocultaIndicadorActividad();
				InitConsultaOtrosCreditos
						.getInstance()
						.getBaseViewController()
						.showInformationAlert(
								detalleConsultaOtrosCreditosViewController,
								response.getMessageText());

			}
		}
	}

	private String getTipoCredito(String tipo) {
		String ret = tipo;
		if (tipo.equalsIgnoreCase("C")) {
			ret = "Consumo";
		} else if (tipo.equalsIgnoreCase("H")) {
			ret = "Hipotecario";
		}else if (tipo.equalsIgnoreCase("P")) {
			ret = "Préstamo Personal";
		}else if (tipo.equalsIgnoreCase("N")) {
			ret = "Nómina";
		}

		return ret;
	}

	public ArrayList<Object> getDatosTabla() {
		final ArrayList<Object> datos = new ArrayList<Object>();
		ArrayList<String> registro;

		if ((null == consultaOtrosCreditosData)
				|| consultaOtrosCreditosData.getListaCreditos().isEmpty()) {
			registro = new ArrayList<String>();
			registro.add(null);
			registro.add("");
			registro.add("");
			registro.add("");
			registro.add("");
		} else {
			for (final Creditos consulta : consultaOtrosCreditosData
					.getListaCreditos()) {
				registro = new ArrayList<String>();
				registro.add(null);
				registro.add(Tools.hideAccountNumber(consulta
						.getNumeroCredito()));
				registro.add(getTipoCredito(consulta.getTipoCredito()));

				Boolean negative = false;
				if (Tools.getDoubleAmountFromServerString(consulta.getAdeudo()) < 0)
					negative = true;
				registro.add(Tools.formatAmount(consulta.getAdeudo(), negative));
				datos.add(registro);
			}
		}

		return datos;
	}

	public ArrayList<Object> getDatosDetalleCredito() {
		if(Server.ALLOW_LOG) Log.d(">> CGI", "Get Datos del Detalle Crédito");
		final ArrayList<Object> datos = new ArrayList<Object>();
		ArrayList<String> fila;

		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_numeroCredito));
		fila.add(detalleConsultaOtrosCreditosData.getNumeroCredito());
		datos.add(fila);

		if(Server.ALLOW_LOG) Log.d(">> CGI", "#EP - Paso 7. RN1.1");
		fila = new ArrayList<String>();
		if (creditoSeleccionado.getTipoCredito().equalsIgnoreCase("C")
				|| creditoSeleccionado.getTipoCredito()
						.equalsIgnoreCase("P")
				|| creditoSeleccionado.getTipoCredito()
						.equalsIgnoreCase("N")) {
			fila.add(detalleConsultaOtrosCreditosViewController
					.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_fechaContratacion));
			if(Server.ALLOW_LOG) Log.d(">> CGI",
					"#EP - Paso 7. RN1.1 Estatus Crédito = C -> Fecha Contratación");
		} else {
			if(Server.ALLOW_LOG) Log.d(">> CGI",
					"#EP - Paso 7. RN1.1 Estatus Crédito != C -> Fecha Formalización");
			fila.add(detalleConsultaOtrosCreditosViewController
					.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_fechaFormalizacion));
		}
		fila.add(Tools.formatDate(detalleConsultaOtrosCreditosData
				.getFechaFoma()));
		datos.add(fila);

		if(Server.ALLOW_LOG) Log.d(">> CGI", "#EP - Paso 7. RN1.2");
		if (creditoSeleccionado.getTipoCredito().equalsIgnoreCase("C")
				|| creditoSeleccionado.getTipoCredito()
						.equalsIgnoreCase("P")
				|| creditoSeleccionado.getTipoCredito()
						.equalsIgnoreCase("N")) {
			if(Server.ALLOW_LOG) Log.d(">> CGI",
					"#EP - Paso 7. RN1.2 - Estatus Crédito = C -> Se muestra fecha Disposición");
			fila = new ArrayList<String>();
			fila.add(detalleConsultaOtrosCreditosViewController
					.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_fechaDisposicion));
			fila.add(Tools.formatDate(detalleConsultaOtrosCreditosData
					.getFechaDisp()));
			datos.add(fila);
		} else {
			if(Server.ALLOW_LOG) Log.d(">> CGI",
					"#EP - Paso 7. RN1.2 - Estatus Crédito != C -> No se muestra fecha Disposición");
		}


		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_creditoOtorgado));
		if(creditoSeleccionado.getTipoCredito().equalsIgnoreCase("H") && (detalleConsultaOtrosCreditosData.getMoneda().equals("UDI P") ||
				(detalleConsultaOtrosCreditosData.getMoneda().equals("UDI") || (detalleConsultaOtrosCreditosData.getMoneda().equals("VSM"))))){
			if(detalleConsultaOtrosCreditosData.getMoneda().equals("UDI")|| detalleConsultaOtrosCreditosData.getMoneda().equals("UDI P")){
				fila.add(Tools.formatUDIAmount(
						detalleConsultaOtrosCreditosData.getImpCredOtorg(), false));
			}else{
				fila.add(Tools.formatVSMAmount(
						detalleConsultaOtrosCreditosData.getImpCredOtorg(), false));
			}
		}else{
            if(detalleConsultaOtrosCreditosData.getMoneda().equalsIgnoreCase("UDI P") && creditoSeleccionado.getTipoCredito().equalsIgnoreCase("H")){
                fila.add(Tools.formatUDIAmount(
                        detalleConsultaOtrosCreditosData.getImpCredOtorg(), false));
            }else{
                fila.add(Tools.formatAmount(
                        detalleConsultaOtrosCreditosData.getImpCredOtorg(), false));
            }}
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_fechaVencimiento));
		fila.add(Tools.formatDate(detalleConsultaOtrosCreditosData
				.getFechaVencim()));
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_abonadoCapital));

		if(creditoSeleccionado.getTipoCredito().equalsIgnoreCase("H") && (detalleConsultaOtrosCreditosData.getMoneda().equals("UDI P") ||
				(detalleConsultaOtrosCreditosData.getMoneda().equals("UDI") || (detalleConsultaOtrosCreditosData.getMoneda().equals("VSM"))))){
			if(detalleConsultaOtrosCreditosData.getMoneda().equals("UDI")|| detalleConsultaOtrosCreditosData.getMoneda().equals("UDI P")){
				fila.add(Tools.formatUDIAmount(
						detalleConsultaOtrosCreditosData.getImpAbonoCap(), false));
			}else {
				fila.add(Tools.formatVSMAmount(
						detalleConsultaOtrosCreditosData.getImpAbonoCap(), false));			}
		}else {
			fila.add(Tools.formatPosNegAmount(
					(detalleConsultaOtrosCreditosData.getImpAbonoCap()), true));
		}
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_moneda));
		if (creditoSeleccionado.getTipoCredito().equalsIgnoreCase("H") && (detalleConsultaOtrosCreditosData.getMoneda().equals("UDI") || detalleConsultaOtrosCreditosData.getMoneda().equals("UDI P"))) {
			fila.add("UDI");
		} else {
			fila.add(detalleConsultaOtrosCreditosData.getMoneda());
		}
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_plazoRestante));
		fila.add(detalleConsultaOtrosCreditosData.getPlazo());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_frecuenciaPago));
		fila.add(detalleConsultaOtrosCreditosData.getFrecPago());
		datos.add(fila);

		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_saldoEnPesos));
		fila.add(Tools.formatAmount(
				detalleConsultaOtrosCreditosData.getImpAdeudo(), false));
		datos.add(fila);

		return datos;
	}

	public ArrayList<Object> getDatosDetalleRecibo() {
		if(Server.ALLOW_LOG) Log.d(">> CGI", "Get datos del Detalle Recibo");
		final ArrayList<Object> datos = new ArrayList<Object>();

		ArrayList<String> fila;
		fila = new ArrayList<String>();
		if(Server.ALLOW_LOG) Log.d(">> CGI", "#EP - Paso 8. RN2");
		if (creditoSeleccionado.getTipoCredito().equalsIgnoreCase("C")
				|| creditoSeleccionado.getTipoCredito().equalsIgnoreCase("P")
				|| creditoSeleccionado.getTipoCredito().equalsIgnoreCase("N")) {
			if(Server.ALLOW_LOG) Log.d(">> CGI",
					"#EP - Paso 8. RN2 - Tipo Crédito = C -> Se muestra Tasa anual fija C/IVA");
			fila.add(detalleConsultaOtrosCreditosViewController
					.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_tasaFija));
			fila.add(detalleConsultaOtrosCreditosData.getTasaRec()+ "%");
			datos.add(fila);
		} else if (creditoSeleccionado.getTipoCredito().equalsIgnoreCase("H")) {
			if(Server.ALLOW_LOG) Log.d(">> CGI",
					"#EP - Paso 8. RN2 - Tipo Crédito = H -> Se muestra Tasa");
			fila.add(detalleConsultaOtrosCreditosViewController
					.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_tasa));
			fila.add(detalleConsultaOtrosCreditosData.getTasaRec()+ "%");
			datos.add(fila);

			//añadir aqui las comprobaciones de credito = H

		}
		
		//Modificación - Nacar puede devolver un importe o cualquier texto
		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_importeRecibo));
		if(creditoSeleccionado.getTipoCredito().equalsIgnoreCase("H") && detalleConsultaOtrosCreditosData.getMoneda().equals("UDI P")
				&& Tools.isNumeric(detalleConsultaOtrosCreditosData.getImpRecVig() )){
			fila.add(Tools.formatUDIAmount(detalleConsultaOtrosCreditosData.getImpRecVig(), false));
		}else{
			if (Tools.isNumeric(detalleConsultaOtrosCreditosData.getImpRecVig())) {
				fila.add(Tools.formatAmount(detalleConsultaOtrosCreditosData.getImpRecVig(), false));
			} else {
				fila.add(detalleConsultaOtrosCreditosData.getImpRecVig());
			}
		}
		datos.add(fila);
		
		//Modificación - Nacar puede devolver un importe o cualquier texto
		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_recibosVencidos));
		if(creditoSeleccionado.getTipoCredito().equalsIgnoreCase("H") && detalleConsultaOtrosCreditosData.getMoneda().equals("UDI P")
				&& Tools.isNumeric(detalleConsultaOtrosCreditosData.getImpRecVen() )){
			fila.add(Tools.formatUDIAmount(detalleConsultaOtrosCreditosData.getImpRecVen(), false));
		}
		else{
		if(Tools.isNumeric(detalleConsultaOtrosCreditosData.getImpRecVen())){
			fila.add(Tools.formatAmount(detalleConsultaOtrosCreditosData.getImpRecVen(), false));
		}else{
			fila.add(detalleConsultaOtrosCreditosData.getImpRecVen());
		}}
		datos.add(fila);
		
		//Modificación - Nacar puede devolver un importe o cualquier texto
		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_totalPagar));
		if(creditoSeleccionado.getTipoCredito().equalsIgnoreCase("H") && detalleConsultaOtrosCreditosData.getMoneda().equals("UDI P")
				&& Tools.isNumeric(detalleConsultaOtrosCreditosData.getImpTotal())){
			fila.add(Tools.formatUDIAmount(detalleConsultaOtrosCreditosData.getImpTotal(), false));
		}else{
		if(Tools.isNumeric(detalleConsultaOtrosCreditosData.getImpTotal())){
			fila.add(Tools.formatAmount(detalleConsultaOtrosCreditosData.getImpTotal(), false));
		}else{
			fila.add(detalleConsultaOtrosCreditosData.getImpTotal());
		}}
		datos.add(fila);
		
		//Modificación - Nacar puede devolver un importe o cualquier texto
		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_fechaPago));
		if(Server.ALLOW_LOG) Log.d("Consulta Otros Creditos Delegate -> ", detalleConsultaOtrosCreditosData.getFechaPago());
		if(Tools.isDate(detalleConsultaOtrosCreditosData.getFechaPago()) || Tools.isNumeric(detalleConsultaOtrosCreditosData.getFechaPago())){
			fila.add(Tools.formatDate(detalleConsultaOtrosCreditosData.getFechaPago()));
		}else{
			fila.add(detalleConsultaOtrosCreditosData.getFechaPago());
		}
		datos.add(fila);
		
		//Modificación - Nacar devuelve cualquier texto o Otro valor (Ejem. 45 de 120) 
		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_menu_consultar_detalle_otroscreditos_reciboNumero));
//		if(Tools.isNoNumeric(detalleConsultaOtrosCreditosData.getNumRec())){	<--Modificación que suprime el comportamiento de esta modificación
//			// No contiene números
//			fila.add(detalleConsultaOtrosCreditosData.getNumRec());
//		}else{
			fila.add(detalleConsultaOtrosCreditosData.getNumRec());
	//	}
		datos.add(fila);
		return datos;
	}

	public ArrayList<Object> getDatosTablaDetalleCreditoOC() {
		if(Server.ALLOW_LOG) Log.d(">> CGI", "Cargamos los datos del credito seleccionado");
		final ArrayList<Object> datos = new ArrayList<Object>();

		ArrayList<String> fila;

		fila = new ArrayList<String>();
		fila.add(detalleConsultaOtrosCreditosViewController
				.getString(R.string.bmovil_consultar_dineromovil_detalles_datos_otp));
		fila.add(creditoSeleccionado.getAdeudo());
		datos.add(fila);

		return datos;
	}

	/**
	 * Metodo para asignar el contexto al acceder desde consultar creditos o
	 * al acceder desde mis cuentas.
	 * @return context
	 */
	private Context getContextSendEmailSms(){
		Context context = consultaOtrosCreditosViewController;
		if(context ==  null){
			context = detalleConsultaOtrosCreditosViewController;
		}
		return context;
	}

	public void consultarImportesPagoCredito(String numeroCredito) {
		//consultaOtrosCreditosViewController.getBaseViewController().muestraIndicadorActividad(consultaOtrosCreditosViewController, consultaOtrosCreditosViewController.getString(R.string.alert_operation), consultaOtrosCreditosViewController.getString(R.string.alert_connecting));
		//consultaOtrosCreditosViewController.getBaseViewController().getParentViewsController().setActivityChanging(true);

		InitPagoCredito init = InitPagoCredito.getInstance(consultaOtrosCreditosViewController, new BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		init.setCallBackSession(InitConsultaOtrosCreditos.getInstance().getConsultaSend().getCallBackSession());
		init.setCallBackAPI(this);
		init.configurar();

		init.consultarImportes(numeroCredito);
	}

	public void mostrarPagoCredito(ImportesPagoCreditoData importesPagoCreditoData) {
		InitPagoCredito init = InitPagoCredito.getInstance(consultaOtrosCreditosViewController, new BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		//consultaOtrosCreditosViewController.getBaseViewController().getParentViewsController().setActivityChanging(true);
		init.setCallBackSession(InitConsultaOtrosCreditos.getInstance().getConsultaSend().getCallBackSession());
		init.setCallBackAPI(this);

		Credito creditos = new Credito();
		creditos.setAdeudo(creditoSeleccionado.getAdeudo());
		creditos.setEstatusCredito(creditoSeleccionado.getEstatusCredito());
		creditos.setNumeroCredito(creditoSeleccionado.getNumeroCredito());
		creditos.setTipoCredito(creditoSeleccionado.getTipoCredito());

		init.configurar();
		init.setImportesAndCredito(importesPagoCreditoData, creditos);

		final Intent intent = new Intent(consultaOtrosCreditosViewController,
				PagarCreditoViewController.class);
		consultaOtrosCreditosViewController.startActivity(intent);
	}

	@Override
	public void returnToPrincipal() {

	}

	@Override
	public void returnDataFromModule(String operation, suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse response) {
		consultaOtrosCreditosViewController.getBaseViewController().ocultaIndicadorActividad();
		//consultaOtrosCreditosViewController.getBaseViewController().getParentViewsController().setActivityChanging(false);
		if (Integer.parseInt(operation) == ApiConstants.OP_CONSULTA_IMPORTES_PAGO_CREDITOS) {
			if (response.getStatus() == ServerResponse.OPERATION_SUCCESSFUL) {
				ImportesPagoCreditoData importesPagoCredito = (ImportesPagoCreditoData) response.getResponse();
				mostrarPagoCredito(importesPagoCredito);
			} else if (response.getStatus() == ServerResponse.OPERATION_WARNING) {

			} else if (response.getStatus() == ServerResponse.OPERATION_ERROR) {
				consultaOtrosCreditosViewController.getBaseViewController().showInformationAlertEspecial(consultaOtrosCreditosViewController, consultaOtrosCreditosViewController.getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
			}
		}
	}

	@Override
	public void cierraSesion() {

	}

	@Override
	public void cierraSesionBackground(Boolean isChanging) {

	}

	@Override
	public void userInteraction() {

	}

	@Override
	public void returnMenuPrincipal() {

	}
}
