package bancomer.api.consultaotroscreditos.gui.controllers;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

import java.util.ArrayList;

import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.ListaSeleccionViewController;
import bancomer.api.common.timer.TimerController;
import bancomer.api.consultaotroscreditos.R;
import bancomer.api.consultaotroscreditos.gui.delegates.ConsultaOtrosCreditosDelegate;
import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;
import bancomer.api.consultaotroscreditos.io.Server;
import bancomer.api.consultaotroscreditos.io.ServerResponseImpl;



public class ConsultaOtrosCreditosViewController  extends Activity {
	
	// Layout de la lista de creditos
	private LinearLayout vista;
	
	// Instancia del delegate
	private ConsultaOtrosCreditosDelegate consultaOtrosCreditosDelegate;
	
	// Instancia del adapter para la lista seleccionable
	private ListaSeleccionViewController listaSeleccion;
	
	//[CGI-Configuracion-Obligatorio] Instancia del baseviewController encargado de la funcionalidad comun de las vistas
	private BaseViewController baseViewController;
	
	//[CGI-Configuracion-Obligatorio] Instancia del parentManager encargado de la pila de delegates y las redirecciones
	private BaseViewsController parentManager;
	
	public BaseViewController getBaseViewController() {
		return baseViewController;
	}
	public void setBaseViewController(BaseViewController baseViewController) {
		this.baseViewController = baseViewController;
	}
	public BaseViewsController getParentManager() {
		return parentManager;
	}
	public void setParentManager(BaseViewsController parentManager) {
		this.parentManager = parentManager;
	}
	
	public ListaSeleccionViewController getListaSeleccion() {
		return listaSeleccion;
	}
	
	public void irAtras(){
		super.onBackPressed();
	}
	
	/**
	 * Default constructor for this activity
	 */

	@Override
	public void onCreate(Bundle savedInstanceState) {
		//[CGI-Configuracion-Obligatorio] Llamada a Activity
		super.onCreate(savedInstanceState);
		
		//[CGI-Configuracion-Obligatorio] Inicializacion del singleton de parametros globales
		InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance();
		
		//[CGI-Configuracion-Obligatorio] Tomamos la instancia del baseviewController encargado de la funcionalidad comun de las vistas
		baseViewController = init.getBaseViewController();
		//[CGI-Configuracion-Obligatorio] Tomamos la instancia del parentManager encargado de la pila de delegates y las redirecciones
		parentManager = init.getParentManager();
		
		//[CGI-Configuracion-Obligatorio] Establecemos el activity actual en el baseViewController para establecer sobre que activity ha de actuar ya que no hereda de la vista
		baseViewController.setActivity(this);
		
		//[CGI-Configuracion-Obligatorio] Llamadas para estandarizar las ventanas
		baseViewController.onCreate(this, BaseViewController.SHOW_HEADER|BaseViewController.SHOW_TITLE, R.layout.api_consulta_otros_creditos_layout_bmovil_consulta_otros_creditos);
		// Controlar desde que menu se ha invocado el caso de uso Consultar Creditos
		if(init.getActivity().getLocalClassName().contains(InitConsultaOtrosCreditos.MENU_OPCIONES_PAGAR)){
			baseViewController.setTitle(this, R.string.bmovil_menu_pagar_otroscreditos_titulo, R.drawable.api_consulta_otros_creditos_icono_pagar_servicios);
		} else {
			baseViewController.setTitle(this, R.string.bmovil_menu_consultar_otroscreditos_titulo, R.drawable.api_consulta_otros_creditos_bmovil_consultar_icono);

		}

		//[CGI-Configuracion-Obligatorio] Tomamos el delegate en caso de estar ya instanciado
		consultaOtrosCreditosDelegate = (ConsultaOtrosCreditosDelegate) parentManager.getBaseDelegateForKey(ConsultaOtrosCreditosDelegate.CONSULTA_OTROS_CREDITOS_DELEGATE_ID);
		if (consultaOtrosCreditosDelegate == null) {
			consultaOtrosCreditosDelegate = new ConsultaOtrosCreditosDelegate();
			parentManager.addDelegateToHashMap(ConsultaOtrosCreditosDelegate.CONSULTA_OTROS_CREDITOS_DELEGATE_ID,
					consultaOtrosCreditosDelegate);
		}

		//[CGI-Configuracion-Obligatorio] Establecemos el delegate actual en el baseViewController para conocer a que delegate debe volver
		baseViewController.setDelegate(consultaOtrosCreditosDelegate);
		
		//[CGI-Configuracion-Obligatorio] Establecemos el viewController en el delegate 
		consultaOtrosCreditosDelegate.setConsultaOtrosCreditosViewController(this);
		
		vista = (LinearLayout)findViewById(R.id.lista_consulta__otros_creditos_layout);
		
		// Peticion de consulta otros creditos
		//consultaOtrosCreditosDelegate.consultaOtrosCreditos();
		llenaListaSeleccion();
		scaleToGui();
	}
	
	public void scaleToGui(){
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		guiTools.scale(findViewById(R.id.layoutRoot));
		guiTools.scale(findViewById(R.id.lista_consulta__otros_creditos_layout));
	}
	
	
	@SuppressWarnings("deprecation")
	public void llenaListaSeleccion(){
		
		LinearLayout.LayoutParams params;
		params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		ArrayList<Object> lista = new ArrayList<Object>();
		ArrayList<Object> encabezado = new ArrayList<Object>();
		encabezado.add(null);
		encabezado.add(getString(R.string.bmovil_menu_consultar_otroscreditos_cabecera1));
		encabezado.add(getString(R.string.bmovil_menu_consultar_otroscreditos_cabecera2));
		encabezado.add(getString(R.string.bmovil_menu_consultar_otroscreditos_cabecera3));

		/**
		 * Recorrer datos y añadir a registros
		 */
		if(consultaOtrosCreditosDelegate != null) lista = consultaOtrosCreditosDelegate.getDatosTabla();
		
		params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		params.bottomMargin = getResources().getDimensionPixelOffset(R.dimen.resultados_bottom_margin);;
		
		if (listaSeleccion == null) {
			listaSeleccion = new ListaSeleccionViewController(this, params, baseViewController.getParentViewsController());
			listaSeleccion.setDelegate(consultaOtrosCreditosDelegate);
			listaSeleccion.setNumeroColumnas(3);
			listaSeleccion.setEncabezado(encabezado);
			listaSeleccion.setTitle(getString(R.string.bmovil_menu_consultar_otroscreditos_tablatitulo));
			listaSeleccion.setLista(lista);
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.setSeleccionable(false);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setSingleLine(true);
			listaSeleccion.setNumeroFilas(lista.size());
			listaSeleccion.setExisteFiltro(false);
			listaSeleccion.cargarTabla();
			vista.addView(listaSeleccion);

		}else {
			listaSeleccion.setLista(lista);
			listaSeleccion.setAlturaFija(true);
			listaSeleccion.setNumeroFilas(lista.size());
			listaSeleccion.setOpcionSeleccionada(-1);
			listaSeleccion.cargarTabla();
		}
	}
	
	@Override
	public void onUserInteraction(){
		baseViewController.onUserInteraction();
	}
	
	
	public void processNetworkResponse(int operationId, ServerResponseImpl response) {
		consultaOtrosCreditosDelegate.analyzeResponse(operationId, response);
	}
		
	@Override
	protected void onResume() {
		super.onResume();
		//[CGI-Configuracion-Obligatorio] Parametros necesarios para hacer animaciones de vuelta atras
		parentManager.setCurrentActivityApp(this);
		parentManager.setActivityChanging(false);
		
		//[CGI-Configuracion-Obligatorio] Establecemos el activity actual en el baseViewController para establecer sobre que activity ha de actuar ya que no hereda de la vista
		baseViewController.setActivity(this);
	}
	
	@Override
	protected void onPause() {
		if(TimerController.getInstance().isCerrandoSesion()){
			parentManager.setCurrentActivityApp(this);
			parentManager.setActivityChanging(false);
			baseViewController.goBack(this, parentManager);
			if(Server.ALLOW_LOG) Log.d("Bebug>>", ">>>>>>>>>>>> onPause");
		}
		super.onPause();
		parentManager.consumeAccionesDePausa();
	}
	
	@Override
	public void onBackPressed(){
		baseViewController.goBack(this, parentManager);
	}
	@Override
	protected void onStop() {
		super.onStop();
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
}
