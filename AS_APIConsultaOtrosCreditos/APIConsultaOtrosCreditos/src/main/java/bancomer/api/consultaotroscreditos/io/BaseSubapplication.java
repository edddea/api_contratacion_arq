package bancomer.api.consultaotroscreditos.io;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.io.IOException;
import java.io.Reader;
import java.util.Hashtable;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.io.ParserJSON;
import bancomer.api.common.io.ParsingException;
import bancomer.api.consultaotroscreditos.R;
import bancomer.api.consultaotroscreditos.commons.AbstractSuitePowerManager;
import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;
import suitebancomer.aplicaciones.commservice.commons.ParametersTO;
import suitebancomer.aplicaciones.commservice.response.IResponseService;


public abstract class BaseSubapplication {
	// #region Variables.
	/**
	 * Determines if this activity is in process of changing screen.
	 */
	protected boolean changingActivity = false;
	
	protected boolean applicationLogged = false;
	
	protected boolean applicationInBackground = false;
	
	
	
	/**
	 * Reference to the main application.
	 */
	protected Activity suiteApp = null;
	
	/**
	 * Referencia al controlador de vistas.
	 */
	protected BaseViewsController viewsController;

	/**
	 * Reference to a pending network operation, in case that an
	 * operation launched a session expired error. This operation
	 * will be relaunched after a successful login
	 */
	protected NetworkOperation pendingOperation = null;

	/**
	 * Reference to server communication handler.
	 */
	protected Server server;

	/**
	 * Pending network operations.
	 */
	protected Hashtable<Integer, NetworkOperation> networkOperationsTable = null;
	
	protected boolean isHomeKeyPressed = false;
	// #endregion
	
	// #region Getters y Setters.
	
	/**
	 * @return the changingActivity
	 */
	public boolean isChangingActivity() {
		return changingActivity;
	}

	private boolean isHomeKeyPressed() {
		return isHomeKeyPressed;
	}

	protected void setHomeKeyPressed(boolean isHomeKeyPressed) {
		this.isHomeKeyPressed = isHomeKeyPressed;
	}

	/**
	 * @param changingActivity the changingActivity to set
	 */
	public void setChangingActivity(boolean changingActivity) {
		this.changingActivity = changingActivity;
	}

	/**
	 * @return the applicationLogged
	 */
	public boolean isApplicationLogged() {
		return applicationLogged;
	}

	/**
	 * @param applicationLogged the applicationLogged to set
	 */
	public void setApplicationLogged(boolean applicationLogged) {
		this.applicationLogged = applicationLogged;
	}

	/**
	 * @return the applicationInBackground
	 */
	public boolean isApplicationInBackground() {
		return applicationInBackground;
	}

	/**
	 * @param applicationInBackground the applicationInBackground to set
	 */
	public void setApplicationInBackground(boolean applicationInBackground) {
		this.applicationInBackground = applicationInBackground;
	}

	/**
	 * @return the suiteApp
	 */
	public Activity getSuiteApp() {
		return suiteApp;
	}

	/**
	 * @param suiteApp the suiteApp to set
	 */
	public void setSuiteApp(Activity suiteApp) {
		this.suiteApp = suiteApp;
	}

	/**
	 * @return the viewsController
	 */
	public BaseViewsController getViewsController() {
		return viewsController;
	}

	/**
	 * @param viewsController the viewsController to set
	 */
	public void setViewsController(BaseViewsController viewsController) {
		this.viewsController = viewsController;
	}

	/**
	 * @return the pendingOperation
	 */
	public NetworkOperation getPendingOperation() {
		return pendingOperation;
	}

	/**
	 * @param pendingOperation the pendingOperation to set
	 */
	public void setPendingOperation(NetworkOperation pendingOperation) {
		this.pendingOperation = pendingOperation;
	}

	/**
	 * @return the server
	 */
	public Server getServer() {
		return server;
	}

	/**
	 * @param server the server to set
	 */
	public void setServer(Server server) {
		this.server = server;
	}

	/**
	 * @return the networkOperationsTable
	 */
	public Hashtable<Integer, NetworkOperation> getNetworkOperationsTable() {
		return networkOperationsTable;
	}

	/**
	 * @param networkOperationsTable the networkOperationsTable to set
	 */
	public void setNetworkOperationsTable(
			Hashtable<Integer, NetworkOperation> networkOperationsTable) {
		this.networkOperationsTable = networkOperationsTable;
	}
	// #endregion

	// #region Network.
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * predefined texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 */
	public void invokeNetworkOperation(int operationId, Hashtable<String,?> params,boolean isJson,  ParsingHandler handler,
									   BaseViewController caller) {
		invokeNetworkOperation(operationId, params, isJson,handler, caller, false);
	}
	
	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * predefined texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	public void invokeNetworkOperation(int operationId, Hashtable<String,?> params,boolean isJson,  ParsingHandler handler
										, BaseViewController caller,boolean callerHandlesError) {
		invokeNetworkOperation(operationId, params, isJson,handler, caller,
				caller.getActivity().getString(R.string.alert_operation), 
				caller.getActivity().getString(R.string.alert_connecting), callerHandlesError);
	}

	/**
	 * Invoke a network operation. It shows a popup indicating progress with 
	 * custom texts.
	 * @param operationId network operation identifier. See Server class.
	 * @param params Hashtable with the parameters passed to the Server. See Server
	 * class for parameter names.
	 * @param caller the BaseScreen instance (that is, the screen), which requests the
	 * network operation. Must be null if the caller is not a screen.
	 * @param progressLabel text with the title of the progress popup
	 * @param progressMessage text with the content of the progress popup
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	protected void invokeNetworkOperation(int operationId, final Hashtable<String, ?> params,
										  final boolean isJson, final ParsingHandler handler,
										  final BaseViewController caller, 
										  String progressLabel, 
										  String progressMessage, 
										  final boolean callerHandlesError) {
		final Integer opId = Integer.valueOf(operationId);          

		if (isNotAlreadyCalling(opId)) {

			try {

				NetworkOperation operation = new NetworkOperation(operationId, params, caller);
				this.networkOperationsTable.put(opId, operation);
				this.pendingOperation = operation;

//				//TODO quitar el numero de operacion en hardcode.
//				if (caller != null && 11 != operationId)
//				    caller.muestraIndicadorActividad(progressLabel, progressMessage);
				
				if (caller != null) {
					if(AbstractSuitePowerManager.getSuitePowerManager().isScreenOn() && (!isHomeKeyPressed())) {
						InitConsultaOtrosCreditos.getInstance().getBaseViewController().muestraIndicadorActividad(caller.getActivity(),progressLabel, progressMessage);
					} else {
						if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "La aplicación estaba bloqueada.");
					}
				}

				Thread thread = new Thread(new Runnable() {
					public void run() {
						try {

							ServerResponseImpl responseNew=null;
							ParametersTO parameters=new ParametersTO();
							parameters.setSimulation(Server.SIMULATION);
							parameters.setDevelopment(Server.DEVELOPMENT);
							parameters.setOperationId(opId.intValue());
							parameters.setParameters(params);
							parameters.setJson(isJson);

							ConnectionFactory cf= new ConnectionFactory(opId.intValue(), params, isJson, handler);
							IResponseService resultado=cf.processConnectionWithParams();
							responseNew=cf.parserConnection(resultado, handler);
							returnFromNetworkOperation(opId, responseNew, caller, null, callerHandlesError);

						} catch (Throwable t) {
							returnFromNetworkOperation(opId, null, caller, t, callerHandlesError);
						}
					}

				});

				// Starts the worker thread to perform network operation
				thread.start();

			} catch (Throwable th) {
				returnFromNetworkOperation(opId, null, caller, th, callerHandlesError);
			}

		} else {
			// unable to process request, show error and return
			return;
		}

	}

	/**
	 * Called internally after a network operation has returned. It ends the progress
	 * popup and invokes method to analyze the result
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message from the server or method should handle in the default
	 * implementation
	 */
	protected void returnFromNetworkOperation(final Integer operationId, 
											  final ServerResponseImpl response, 
											  final BaseViewController caller, 
											  final Throwable throwable, 
											  final boolean callerHandlesError) {

		if (caller != null) {
			caller.ocultaIndicadorActividad();
			
			caller.getActivity().runOnUiThread(new Runnable() {
				public void run() {
					try {
						analyzeNetworkResponse(operationId, response, throwable, caller, callerHandlesError);
					} catch (Throwable t) {
						if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName(), "Exception on analyzeNetworkResponse.", t);
						if (caller != null) {
							String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
							InitConsultaOtrosCreditos.getInstance().getBaseViewController().showErrorMessage(caller.getActivity(), message);
						}
					}
				}
			});
			
		} else { //TODO session timer expired, go to login screen
			Thread thread = new Thread(new Runnable() {
				public void run() {
					try {
						analyzeNetworkResponse(operationId, response, throwable, null, callerHandlesError);
					} catch (Throwable t) {
						String message = new StringBuffer(t.getClass().getName()).append(" (").append(t.getMessage()).append(")").toString();
						if(Server.ALLOW_LOG) Log.d(this.getClass().getSimpleName() + " :: returnFromNetworkOperation", message);
					}
				}
			});
			thread.run();
		}
	}

	// TODO revisar en hijos.
	/**
	 * Analyze the network response obtained from the server
	 * @param operationId network identifier. See Server class.
	 * @param response the ServerResponse instance built from the server response. It
	 * contains the type of result (success, failure), and the real content for
	 * the application business.
	 * @param operationId network operation identifier. See Server class.
	 * @param response the ServerResponse instance returned from the server.
	 * @param throwable the throwable received.
	 * @param callerHandlesError boolean flag indicating wheter the caller is capable 
	 * of handling an error message or the network flux should handle in the default
	 * implementation
	 */
	protected void analyzeNetworkResponse(Integer operationId, 
										  ServerResponseImpl response, 
										  Throwable throwable, 
										  final BaseViewController caller, 
										  boolean callerHandlesError) {
		// get the caller           
		NetworkOperation operation = this.networkOperationsTable.get(operationId);
		if (operation != null) {
			// remove the operation id from the table
			this.networkOperationsTable.remove(operationId);
			if (operation.isActive()) {	
				if (response != null) {
//					BaseViewController baseViewController = operation.getCaller();
					int resultCode = response.getStatus();
					switch (resultCode) {
					case ServerResponseImpl.OPERATION_SUCCESSFUL:
					case ServerResponseImpl.OPERATION_WARNING:
						
						caller.processNetworkResponse(operationId.intValue(), response);						
											
						break;
					case ServerResponseImpl.OPERATION_OPTIONAL_UPDATE:
						caller.processNetworkResponse(operationId.intValue(), response);
						break;
					case ServerResponseImpl.OPERATION_ERROR:
						this.pendingOperation = null;
						String errorCode = response.getMessageCode();						
						int operationError = operationId.intValue();
						
//						if( (operationError != Server.QUITAR_SUSPENSION) && (operationError != Server.LOGIN_OPERATION) && (operationError != Server.OP_ENVIO_CLAVE_ACTIVACION) && (operationError != Server.OP_ACTIVACION) && (operationError != Server.AUTENTICACION_ST) && (Constants.BLOCKED_ERROR_CODE.equals(errorCode) || Constants.BLOCKED_ERROR_CODE_2.equals(errorCode))){
//							caller.showInformationAlert( response.getMessageText(), new OnClickListener() {							
//								@Override
//								public void onClick(DialogInterface dialog, int which) {
//									SuiteApp.getInstance().getBmovilApplication().closeSession(Session.getInstance(SuiteApp.appContext).getUsername(),Session.getInstance(SuiteApp.appContext).getIum(),Session.getInstance(SuiteApp.appContext).getClientNumber());
//								}
//							});
//						}else if ((operationError == Server.LOGIN_OPERATION) && (Constants.DEACTIVATION_ERROR_CODE.equals(errorCode))) {
//							Session session = Session.getInstance(SuiteApp.appContext);
//							String username = session.getUsername();
//							String password = session.getPassword();
//							Session.getInstance(suiteApp.getApplicationContext()).setValidity(Session.UNSET_STATUS);
//
//							Bundle params = new Bundle();
//							params.putString(Server.USERNAME_PARAM, username);
//							params.putString(Server.PASSWORD_PARAM, password);
//						} else if ((operationError == Server.SELF_TRANSFER_OPERATION) ||
//								(operationError == Server.BANCOMER_TRANSFER_OPERATION) ||
//								(operationError == Server.EXTERNAL_TRANSFER_OPERATION) ||
//								(operationError == Server.ALTA_OPSINTARJETA) ||
//								(operationError == Server.SERVICE_PAYMENT_OPERATION) ||
//								(operationError == Server.OP_RETIRO_SIN_TAR) ||
//								(operationError == Server.COMPRA_TIEMPO_AIRE)){		
//							
//								// Mensaje de error en Transferencias.
//								Log.d("BaseSubapplication", "ERROR Transferencias >>> operationError = " +  operationError + " errorCode = " + errorCode);
//								mostrarErrorTransfer(response, errorCode, baseViewController, caller,operationId.intValue());
//								//baseViewController.processNetworkResponse(operationId.intValue(), response);
//								
//														
//						} else {
								if (callerHandlesError) {
									caller.processNetworkResponse(operationId.intValue(), response);
								} else {
							//		caller.ocultaIndicadorActividad();
//									caller.showErrorMessage(errorCode + "\n" + response.getMessageText());
									
									caller.processNetworkResponse(operationId.intValue(), response);
//									InitConsultaOtrosCreditos.getInstance().getBaseViewController().showInformationAlertEspecial(caller.getActivity(),caller.getActivity().getString(R.string.label_error), errorCode, response.getMessageText(), null);
//								}
						}
						break;
//					case ServerResponse.OPERATION_SESSION_EXPIRED:
//						this.pendingOperation = operation;
//						Session.getInstance(suiteApp.getApplicationContext()).setValidity(Session.INVALID_STATUS);
//						suiteApp.getSuiteViewsController().showMenuSuite(true);
//						break;						
					default:
//						caller.ocultaIndicadorActividad();
//						InitConsultaOtrosCreditos.getInstance().getBaseViewController().showErrorMessage(caller.getActivity(),R.string.error_format);
						

						response = unexpectedErrorResponse(caller.getActivity().getString(R.string.error_format));
						caller.processNetworkResponse(operationId.intValue(), response);
						
						break;
					}
				} else {
					if (throwable != null) {
//						InitConsultaOtrosCreditos.getInstance().getBaseViewController().ocultaIndicadorActividad();
//						InitConsultaOtrosCreditos.getInstance().getBaseViewController().showErrorMessage(caller.getActivity(),getErrorMessage(throwable));
						
						response = unexpectedErrorResponse(getErrorMessage(throwable));
						caller.processNetworkResponse(operationId.intValue(), response);
					} else {
//						InitConsultaOtrosCreditos.getInstance().getBaseViewController().ocultaIndicadorActividad();
//						InitConsultaOtrosCreditos.getInstance().getBaseViewController().showErrorMessage(caller.getActivity(),R.string.error_communications);
						
						response = unexpectedErrorResponse(caller.getActivity().getString(R.string.error_communications));
						caller.processNetworkResponse(operationId.intValue(), response);
					}
				}
			}
		} else {
//			InitConsultaOtrosCreditos.getInstance().getBaseViewController().ocultaIndicadorActividad();
//			InitConsultaOtrosCreditos.getInstance().getBaseViewController().showErrorMessage(caller.getActivity(),R.string.error_unexpected);
			
			response = unexpectedErrorResponse(caller.getActivity().getString(R.string.error_unexpected));
			caller.processNetworkResponse(operationId.intValue(), response);
		}		
	}
	
	private ServerResponseImpl unexpectedErrorResponse(String mensaje){
		String msg = "{\"estado\":\"ERROR\",\"codigoMensaje\":\"\",\"descripcionMensaje\":\""+mensaje+"\"}";
		
		ServerResponseImpl res = new ServerResponseImpl();
		
		try {
			
			parseJSON(msg, res);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParsingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return res;
	}
	
	private void parseJSON(String response, ParsingHandler handler) throws IOException, ParsingException {

        Reader reader = null;
        try {
//            reader = new StringReader(response);
            ParserJSON parser = new ParserJSONImpl(response);
            handler.process(parser);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Throwable ignored) {
                }
            }
        }
    }
	
	/**
	 * Muestra un aviso de error en caso de que el servidor rechace la transferencia. Si es
	 * avanzado mostrará el mensaje devuelto y si es básico mostrará uno por defecto.
	 * 
	 * @param response Respuesta del servidor
	 * @param baseViewController Controlador base
	 * @param caller Controlador padre
	 */
//	private void mostrarErrorTransfer(ServerResponse response, String errorCode, BaseViewController baseViewController, final BaseViewController caller, int operationValue) {		
//		Session session = Session.getInstance(SuiteApp.appContext);
//		Constants.Perfil perfil = session.getClientProfile();
//		// Si es perfil avanzado mostrará el mensaje recibido, sino mostrará el definido en String.xml
//		
//		Account cEje = Tools.obtenerCuentaEje();
//		String cEjeType = cEje == null ? "" : cEje.getType();
//		
//		if ((Constants.CODE_CNE0234.equals(errorCode)) || (Constants.CODE_CNE0235.equals(errorCode)) || (Constants.CODE_CNE0236.equals(errorCode))) {
//			
//			if ((Constants.Perfil.basico.equals(perfil)) && (!cEjeType.equals(Constants.CREDIT_TYPE))) {
//				String msg = baseViewController.getString(R.string.transferir_aviso_texto);
//				caller.ocultaIndicadorActividad();
//				caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,msg, null);
//				
//			} else if (Constants.Perfil.recortado.equals(perfil)) {
//				baseViewController.processNetworkResponse(operationValue, response);
//				
//			} else {
//				// Avanzado, o básico con tarjeta de crédito como cuenta eje
//				caller.ocultaIndicadorActividad();
//				caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,response.getMessageText(), null);
//			}
//			
//		} else {
//			caller.ocultaIndicadorActividad();
//			caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,response.getMessageText(), null);
//		}
		
//		if(Constants.Perfil.recortado.equals(perfil)){
//			
//			baseViewController.processNetworkResponse(operationValue, response);
//	
//		}else if(Constants.Perfil.basico.equals(perfil) && 
//				(Constants.CODE_CNE0234.equals(errorCode) ||
//				 Constants.CODE_CNE0235.equals(errorCode) ||
//				 Constants.CODE_CNE0236.equals(errorCode)) &&
//				 !cEjeType.equals(Constants.CREDIT_TYPE)){
//			String msg = baseViewController.getString(R.string.transferir_aviso_texto);
//			caller.ocultaIndicadorActividad();
//			caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,msg, null);
//		}else {
//			caller.ocultaIndicadorActividad();
//			caller.showInformationAlertEspecial(baseViewController.getString(R.string.label_error), errorCode,response.getMessageText(), null);
//		}								
//	}
	
	

	/**
	 * Cancel any ongoing network operation, and clears the network
	 * operation table to prevent any other network operation from
	 * executing
	 */
	protected void cancelOngoingNetworkOperations() {
		this.networkOperationsTable.clear();
		this.pendingOperation = null;
		this.server.cancelNetworkOperations();
	}
	
	/**
	 * Determines if the requested operation is not being called already
	 * @param opId the operation id
	 * @return true if the operation isn't already waiting for a response
	 */
	private boolean isNotAlreadyCalling(int opId){
		return !this.networkOperationsTable.containsKey(opId);
	}
	// #endregion
	
	// #region Otros métodos.
	// TODO revisar en hijos.
	public void cierraAplicacion() {
		viewsController.cierraViewsController();
		viewsController = null;
		server.cancelNetworkOperations();
		server = null;
		applicationLogged = false;
	}
	
	/**
	 * Shows the Main menu screen, removing any activity on top
	 */
	public void requestMenu(Context activityContext){
		changingActivity = true;
		viewsController.showMenuInicial();
	}

	/**
	 * Get the operation error message
	 * @param throwable the throwable
	 * @return the operation error message 
	 */
	protected String getErrorMessage(Throwable throwable) {
		StringBuffer sb = new StringBuffer();
		if (throwable != null) {
			if (throwable instanceof NumberFormatException) {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_format));
			} else if (throwable instanceof ParsingException) {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_format));
			} else if (throwable instanceof IOException) {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_communications));
			} else {
				sb.append(suiteApp.getApplicationContext().getString(R.string.error_communications));
			}
		}
		return sb.toString();
	}
	// #endregion
	
	// TODO revisar en hijos.
	public BaseSubapplication(Activity suiteApp) {
		this.suiteApp = suiteApp;
		server = new Server();
		networkOperationsTable = new Hashtable<Integer, NetworkOperation>();
		viewsController = null;
		applicationLogged = false;
	}
	
//	public BaseSubapplication(Activity suiteApp) {
//		this.suiteApp = suiteApp;
//		server = new Server();
//		networkOperationsTable = new Hashtable<Integer, NetworkOperation>();
//		viewsController = null;
//		applicationLogged = false;
//	}
}
