package bancomer.api.consultaotroscreditos.implementations;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import org.apache.http.impl.client.DefaultHttpClient;

import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.BaseViewsControllerImpl;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.consultaotroscreditos.gui.controllers.ConsultaOtrosCreditosViewController;
import bancomer.api.consultaotroscreditos.gui.controllers.DetalleConsultaOtrosCreditosViewController;
import bancomer.api.consultaotroscreditos.gui.delegates.ConsultaData;
import bancomer.api.consultaotroscreditos.gui.delegates.ConsultaOtrosCreditosDelegate;
import bancomer.api.consultaotroscreditos.gui.delegates.PreProcessModuleEnterData;
import bancomer.api.consultaotroscreditos.io.BaseSubapplication;
import bancomer.api.consultaotroscreditos.io.Server;
import bancomer.api.consultaotroscreditos.models.ConsultaOtrosCreditosSendData;
import bancomer.api.consultaotroscreditos.models.Creditos;

public class InitConsultaOtrosCreditos {

	public final static String MENU_OPCIONES_PAGAR= "OpcionesPagosViewController";

	private static InitConsultaOtrosCreditos mInstance = null;
	
	private BaseViewController baseViewController;
	
	private static BaseViewsController parentManager;
	
	private BaseSubapplication baseSubApp;
	
	private ConsultaOtrosCreditosSendData consultaSend; 	

	private Activity activity;
	
	private ConsultaData consultaData;
	
	private PreProcessModuleEnterData preProcessModuleEnterData;
	
	/**
     * Se utiliza un httpclient de Apache Jakarta que viene de la aplicación principal
     */
	public static DefaultHttpClient client;


	/**
	 * Getters y Setters 
	 * 
	 */


	public PreProcessModuleEnterData getPreProcessModuleEnterData() {
		return preProcessModuleEnterData;
	}

	public void setPreProcessModuleEnterData(
			PreProcessModuleEnterData preProcessModuleEnterData) {
		this.preProcessModuleEnterData = preProcessModuleEnterData;
	}

	public DefaultHttpClient getClient() {
		return client;
	}

	public void setClient(DefaultHttpClient client) {
		InitConsultaOtrosCreditos.client = client;
	}

	public ConsultaOtrosCreditosSendData getConsultaSend() {
		return consultaSend;
	}

	public void setConsultaSend(ConsultaOtrosCreditosSendData consultaSend) {
		this.consultaSend = consultaSend;
	}

	public void setConsultaData(ConsultaData consultaData) {
		this.consultaData = consultaData;
	}
	
	public ConsultaData getConsultaData(){
		ConsultaData delegate = (ConsultaData) parentManager.getBaseDelegateForKey(ConsultaData.CONSULTA_DATA_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new ConsultaData();
//			parentManager.addDelegateToHashMap(ConsultaData.CONSULTA_DATA_DELEGATE_ID,
//					delegate);
//			this.consultaData = delegate;
//		}
		
		return delegate;
	}

	public BaseViewController getBaseViewController() {
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitConsultaOtrosCreditos] Se toma la instancia de baseViewController");
		return baseViewController;
	}

	public void setBaseViewController(BaseViewController baseViewController) {
		this.baseViewController = baseViewController;
	}

	public BaseViewsController getParentManager() {
		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitConsultaOtrosCreditos] Se toma la instancia de parentManager");
		return parentManager;
	}

	public void setParentManager(BaseViewsController parentManager) {
		this.parentManager = parentManager;
	}
		
	public BaseSubapplication getBaseSubApp() {
		return baseSubApp;
	}

	public void setBaseSubApp(BaseSubapplication baseSubApp) {
		this.baseSubApp = baseSubApp;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}
	/**
	 * End Getters y Setters 
	 * 
	 */
 
	/**
	 * Constructores
	 * 
	 */
    
   /* public static InitConsultaOtrosCreditos getInstance(Activity activity , DefaultHttpClient cliente){
        if(mInstance == null)
        {
            mInstance = new InitConsultaOtrosCreditos(cliente);
        
        }else{
        	mInstance.baseSubApp.getServer().getClienteHttp().setClient(cliente);
            InitConsultaOtrosCreditos.client = cliente;
        }

		if(Server.ALLOW_LOG) Log.d("Cliente Init", cliente.toString()+"  --  "+InitConsultaOtrosCreditos.client);
        parentManager.setCurrentActivityApp(activity);
        mInstance.setActivity(activity);
        
        return mInstance;
    }*/
    
    public InitConsultaOtrosCreditos(Activity activity){

    	consultaSend = new ConsultaOtrosCreditosSendData();
    	preProcessModuleEnterData = new PreProcessModuleEnterData();
    	baseViewController = new BaseViewControllerImpl();
    	parentManager = new BaseViewsControllerImpl();
    	baseSubApp = new BaseSubapplicationImpl(activity);
    }

	public static InitConsultaOtrosCreditos getInstance(Activity activity ){
		if(mInstance == null)
		{
			mInstance = new InitConsultaOtrosCreditos(activity);
		}else{
			mInstance.baseSubApp.getServer();
		}

		parentManager.setCurrentActivityApp(activity);
		mInstance.setActivity(activity);

		return mInstance;
	}

	/*public InitConsultaOtrosCreditos(Activity activity){

		consultaSend = new ConsultaOtrosCreditosSendData();
		baseViewController = new BaseViewControllerImpl();
		parentManager = new BaseViewsControllerImpl();
		baseSubApp = new BaseSubapplicationImpl(activity);
	}*/
    public static InitConsultaOtrosCreditos getInstance(){
        if(mInstance == null)
        {
			if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[InitConsultaOtrosCreditos] Se inicializa la instancia del singleton de datos");
            mInstance = new InitConsultaOtrosCreditos(null);
        }
        return mInstance;
    }
    /**
     * End Constructores
     */
    
    public void returnToPrincipal(){
    	getConsultaSend().getCallBackModule().returnToPrincipal();
    }
    
    public void returnDataToPrincipal(String operation, ServerResponse response){
    	getConsultaSend().getCallBackModule().returnDataFromModule(operation, response);
    }
    
    public void lookForCreditosData(Activity act){
//    	ConsultaData delegate = (ConsultaData) parentManager.getBaseDelegateForKey(ConsultaData.CONSULTA_DATA_DELEGATE_ID);
//		if (delegate == null) {
//			delegate = new ConsultaData();
//			parentManager.addDelegateToHashMap(ConsultaData.CONSULTA_DATA_DELEGATE_ID,
//					delegate);
//		}
//		
//		delegate.consultaOtrosCreditos(act);
    	
    	ConsultaOtrosCreditosDelegate delegate = (ConsultaOtrosCreditosDelegate) parentManager.getBaseDelegateForKey(ConsultaOtrosCreditosDelegate.CONSULTA_OTROS_CREDITOS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new ConsultaOtrosCreditosDelegate();
			parentManager.addDelegateToHashMap(ConsultaOtrosCreditosDelegate.CONSULTA_OTROS_CREDITOS_DELEGATE_ID,
					delegate);
		}
		
		delegate.consultaOtrosCreditos();
    }
	
	public void showViewController(Class<?> viewController, int flags, boolean inverted, String[] extrasKeys, Object[] extras) {
		
		Intent intent = new Intent(activity, viewController);
		if (flags != 0) {
			intent.setFlags(flags);
		}
		
		if (extrasKeys != null && extras != null && extras.length == extrasKeys.length) {
			int extrasLength = extras.length;
			for (int i=0; i<extrasLength; i++) {
				if (extras[i] instanceof String) {
					intent.putExtra(extrasKeys[i], (String)extras[i]);
				} else if (extras[i] instanceof Integer) {
					intent.putExtra(extrasKeys[i], ((Integer)extras[i]).intValue());
				} else if (extras[i] instanceof Boolean) {
					intent.putExtra(extrasKeys[i], ((Boolean)extras[i]).booleanValue());
				}else if(extras[i] instanceof Long){
					intent.putExtra(extrasKeys[i], ((Long)extras[i]).longValue());
				}
			}
		}
		
		activity.startActivity(intent);
		if (inverted) {
			getParentManager().overrideScreenBackTransition();
		} else {
			getParentManager().overrideScreenForwardTransition();
		}
    }
	
	public void showViewController(Class<?> viewController) {
		showViewController(viewController, 0, false, null, null);
	}
	
	public void preShowConsultarOtrosCreditos(){
		ConsultaOtrosCreditosDelegate delegate = (ConsultaOtrosCreditosDelegate) parentManager.getBaseDelegateForKey(ConsultaOtrosCreditosDelegate.CONSULTA_OTROS_CREDITOS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new ConsultaOtrosCreditosDelegate();
			parentManager.addDelegateToHashMap(ConsultaOtrosCreditosDelegate.CONSULTA_OTROS_CREDITOS_DELEGATE_ID,
					delegate);
		}
		delegate.consultaOtrosCreditos();
    }
    
    public void showConsultarOtrosCreditos(){
    	showViewController(ConsultaOtrosCreditosViewController.class);
    }
    
    public void showDetallesOtrosCreditos(){
    	showViewController(DetalleConsultaOtrosCreditosViewController.class);
    }
    
    /**
     * Peticion para detalle desde fuera del modulo
     * @param cred
     */
    public void showDetallesOtrosCreditos(Object cred){
    	ConsultaOtrosCreditosDelegate delegate = (ConsultaOtrosCreditosDelegate) parentManager.getBaseDelegateForKey(ConsultaOtrosCreditosDelegate.CONSULTA_OTROS_CREDITOS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new ConsultaOtrosCreditosDelegate();
			parentManager.addDelegateToHashMap(ConsultaOtrosCreditosDelegate.CONSULTA_OTROS_CREDITOS_DELEGATE_ID,
					delegate);
		}
		
		Creditos credito = (Creditos)cred;
		delegate.setCreditoSeleccionado(credito);
    	showViewController(DetalleConsultaOtrosCreditosViewController.class);
    }
    
    public void preShowDetalleConsultarOtrosCreditos(Object cred){
		ConsultaOtrosCreditosDelegate delegate = (ConsultaOtrosCreditosDelegate) parentManager.getBaseDelegateForKey(ConsultaOtrosCreditosDelegate.CONSULTA_OTROS_CREDITOS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new ConsultaOtrosCreditosDelegate();
			parentManager.addDelegateToHashMap(ConsultaOtrosCreditosDelegate.CONSULTA_OTROS_CREDITOS_DELEGATE_ID,
					delegate);
		}
		preProcessModuleEnterData.setFinishedCall(false);
		Creditos credito = (Creditos)cred;
		delegate.setCreditoSeleccionado(credito);
		delegate.setComesFromOut(true);
		delegate.outDetalleConsultaOtrosCreditos();   
	}


}
