package bancomer.api.consultaotroscreditos.gui.controllers;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.bancomer.base.callback.CallBackAPI;

import java.util.ArrayList;

import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.controllers.ListaDatosViewController;
import bancomer.api.common.timer.TimerController;
import bancomer.api.consultaotroscreditos.R;
import bancomer.api.consultaotroscreditos.gui.delegates.ConsultaOtrosCreditosDelegate;
import bancomer.api.consultaotroscreditos.implementations.InitConsultaOtrosCreditos;
import bancomer.api.consultaotroscreditos.io.Server;
import bancomer.api.consultaotroscreditos.models.Creditos;
import bancomer.api.pagarcreditos.gui.controllers.PagarCreditoViewController;
import bancomer.api.pagarcreditos.implementations.InitPagoCredito;
import bancomer.api.pagarcreditos.io.BanderasServer;
import bancomer.api.pagarcreditos.models.Credito;
import bancomer.api.pagarcreditos.models.ImportesPagoCreditoData;
import suitebancomer.aplicaciones.commservice.commons.ApiConstants;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerCommons;
import suitebancomercoms.aplicaciones.bmovil.classes.io.ServerResponse;


public class DetalleConsultaOtrosCreditosViewController extends Activity implements View.OnClickListener, CallBackAPI {
	
	//[CGI-Configuracion-Obligatorio] Instancia del parentManager encargado de la pila de delegates y las redirecciones
	public BaseViewsController parentManager;
	
	//[CGI-Configuracion-Obligatorio] Instancia del baseviewController encargado de la funcionalidad comun de las vistas
	private BaseViewController baseViewController;
	
	// Instancia del delegate
	private ConsultaOtrosCreditosDelegate delegate;
	
	// Layout de la lista de detalles
	private LinearLayout rootLayout;
	private LinearLayout listaDetllesLayout, listaDetallesLayout2;
	private ListaDatosViewController listaDetalles, listaDetalles2;
	
	private boolean isMenuPressed = false;
	
	private ImageButton menuButton;
	private ImageButton pagarButton;
	private ImageButton compartirButton;
	private TextView textoEspecialResultados;

	public BaseViewsController getParentManager() {
		return parentManager;
	}

	public void setParentManager(BaseViewsController parentManager) {
		this.parentManager = parentManager;
	}

	public BaseViewController getBaseViewController() {
		return baseViewController;
	}

	public void setBaseViewController(BaseViewController baseViewController) {
		this.baseViewController = baseViewController;
	}

	/*
	 * No javadoc.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//[CGI-Configuracion-Obligatorio] Llamada a Activity
		super.onCreate(savedInstanceState);
		
		isMenuPressed = false;
		
		//[CGI-Configuracion-Obligatorio] Inicializacion del singleton de parametros globales
		InitConsultaOtrosCreditos init = InitConsultaOtrosCreditos.getInstance();
		
		//[CGI-Configuracion-Obligatorio] Tomamos la instancia del baseviewController encargado de la funcionalidad comun de las vistas
		baseViewController = init.getBaseViewController();
		//[CGI-Configuracion-Obligatorio] Tomamos la instancia del parentManager encargado de la pila de delegates y las redirecciones
		parentManager = init.getParentManager();
		
		//[CGI-Configuracion-Obligatorio] Establecemos el activity actual en el baseViewController para establecer sobre que activity ha de actuar ya que no hereda de la vista
		baseViewController.setActivity(this);
		
		//[CGI-Configuracion-Obligatorio] Establecemos la lista de opciones del menu a mostrar
		baseViewController.setOpcionesMenu(getMenuOptions());
		
		//[CGI-Configuracion-Obligatorio] Llamadas para estandarizar las ventanas 
		baseViewController.onCreate(this, BaseViewController.SHOW_HEADER|BaseViewController.SHOW_TITLE, R.layout.api_consulta_otros_creditos_layout_bmovil_consulta_otros_creditos_detalles);
		baseViewController.setTitle(this, R.string.bmovil_menu_consultar_otroscreditos_titulo, R.drawable.api_consulta_otros_creditos_bmovil_consultar_icono);		
		
		//[CGI-Configuracion-Obligatorio] Tomamos el delegate en caso de estar ya instanciado
		delegate = (ConsultaOtrosCreditosDelegate) parentManager.getBaseDelegateForKey(ConsultaOtrosCreditosDelegate.CONSULTA_OTROS_CREDITOS_DELEGATE_ID);
		if (delegate == null) {
			delegate = new ConsultaOtrosCreditosDelegate();
			parentManager.addDelegateToHashMap(ConsultaOtrosCreditosDelegate.CONSULTA_OTROS_CREDITOS_DELEGATE_ID,
					delegate);
		}
		
		//[CGI-Configuracion-Obligatorio] Establecemos el delegate actual en el baseViewController para conocer a que delegate debe volver
		baseViewController.setDelegate(delegate);
		
		//[CGI-Configuracion-Obligatorio] Establecemos el viewController en el delegate 
		delegate.setDetalleConsultaOtrosCreditosViewController(this);
		
		init();
	}
	
	private ArrayList<Integer> getMenuOptions(){
		ArrayList<Integer> lista = new ArrayList<Integer>();
		lista.add(BaseViewController.SHOW_MENU_SMS);
		lista.add(BaseViewController.SHOW_MENU_EMAIL);
		
		return lista;
	}
	
	public ArrayList<Integer> getMenuOptionsNoSms(){
		ArrayList<Integer> lista = new ArrayList<Integer>();
		lista.add(BaseViewController.SHOW_MENU_EMAIL);
		
		return lista;
	}
	
	/**
	 * Inicializa el controlador de la ventana.
	 */
	private void init() {
		
		findViews();
		if(Server.ALLOW_LOG) Log.d(">> CGI", "Lanzamos la petición de Detalle desde el init de la vista");

		cargaListaDatos();
		configurarPantalla();
		scaleForCurrentScreen();
		
	}

	public void configurarPantalla() {
	
		menuButton.setOnClickListener(this);
		pagarButton.setOnClickListener(this);
		compartirButton.setOnClickListener(this);
		textoEspecialResultados.setText(R.string.bmovil_menu_consultar_otroscreditos_textoAyuda);
		compartirButton.setVisibility(View.VISIBLE);
	}
	
	/**
	 * Busca las vistas usadas y guarda sus referencias.
	 */
	private void findViews() {
		rootLayout = (LinearLayout)findViewById(R.id.rootLayout);
		listaDetllesLayout = (LinearLayout)findViewById(R.id.listaDetallesLayout);
		listaDetallesLayout2 = (LinearLayout) findViewById(R.id.listaDetallesLayout2);
		textoEspecialResultados = (TextView) findViewById(R.id.resultado_texto_especial);
		pagarButton = (ImageButton)findViewById(R.id.detalle_pagar_button);
		menuButton = (ImageButton)findViewById(R.id.resultados_menu_button);
		compartirButton = (ImageButton)findViewById(R.id.resultados_compartir);
	}
	
	/**
	 * Redimensiona las vistas para que mantegan la relacion de aspecto en cualquier pantalla.
	 */
	private void scaleForCurrentScreen() {
		GuiTools guiTools = GuiTools.getCurrent();
		guiTools.init(getWindowManager());
		
		guiTools.scalePaddings(rootLayout);
		guiTools.scale(listaDetllesLayout);
		guiTools.scale(listaDetallesLayout2);
		guiTools.scale(textoEspecialResultados, true);
		guiTools.scale(pagarButton);
		guiTools.scale(menuButton);
		guiTools.scale(compartirButton);
	}
	
	/**
	 * Carga el componente de lista de detalles.
	 */
	public void cargaListaDatos() {
		if(Server.ALLOW_LOG) Log.d(">> CGI", "Pintamos Detalle del crédito en pantalla");
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);

		ArrayList<Object> detalles = new ArrayList<Object>();

		listaDetalles = new ListaDatosViewController(this, params,
				baseViewController.getParentViewsController());
		detalles = delegate.getDatosDetalleCredito();
		listaDetalles.setNumeroCeldas(2);
		listaDetalles.setLista(detalles);
		listaDetalles.setNumeroFilas(detalles.size());
		listaDetalles
				.setTitulo(R.string.bmovil_menu_consultar_detalle_otroscreditos_credito);
		listaDetalles.showLista();
		listaDetllesLayout.addView(listaDetalles);

		if(Server.ALLOW_LOG) Log.d(">> CGI", "Pintamos Detalle del recibo en pantalla");
		detalles = new ArrayList<Object>();
		listaDetalles2 = new ListaDatosViewController(this, params,
				baseViewController.getParentViewsController());
		listaDetalles2 = new ListaDatosViewController(this, params,
				baseViewController.getParentViewsController());
		detalles = delegate.getDatosDetalleRecibo();
		listaDetalles2.setNumeroCeldas(2);
		listaDetalles2.setLista(detalles);
		listaDetalles2.setNumeroFilas(detalles.size());
		listaDetalles2
				.setTitulo(R.string.bmovil_menu_consultar_detalle_otroscreditos_recibo);
		listaDetalles2.showLista();
		listaDetallesLayout2.addView(listaDetalles2);
	}
	
	public void botonMenuClick() {

		isMenuPressed = true;
		parentManager.removeDelegateFromHashMap(ConsultaOtrosCreditosDelegate.CONSULTA_OTROS_CREDITOS_DELEGATE_ID);
//		InitConsultaOtrosCreditos.getInstance().finishModuleFlags();
		InitConsultaOtrosCreditos.getInstance().returnToPrincipal();
		baseViewController.goBack(this, parentManager);
	}
	
	@Override
	public void onClick(View v) {
		if (v == menuButton) {
			botonMenuClick();
		}else if (v==pagarButton){
			if(Server.ALLOW_LOG) Log.d(">> CGI", "Se ejecuta el caso de uso pagar Credito");
			consultarImportesPagoCredito(this.delegate.getCreditoSeleccionado().getNumeroCredito());
		}else if (v==compartirButton){
			openOptionsMenu();
		}
	}

	@Override
	public void onBackPressed(){
		baseViewController.goBack(this, parentManager);
	}
	
	public void onResume(){	
		super.onResume();
		//[CGI-Configuracion-Obligatorio] Parametros necesarios para hacer animaciones de vuelta atras
		parentManager.setCurrentActivityApp(this);
		parentManager.setActivityChanging(false);	
		
		//[CGI-Configuracion-Obligatorio] Establecemos el activity actual en el baseViewController para establecer sobre que activity ha de actuar ya que no hereda de la vista
		baseViewController.setActivity(this);

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return this.getBaseViewController().onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		return this.getBaseViewController().onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Boolean ret = false;
		if(item.getItemId() == R.id.save_menu_email_button){
			delegate.emailOtrosCreditos();
			ret = true;
		}else if(item.getItemId() == R.id.save_menu_sms_button){
			delegate.smsOtrosCreditos();
			ret = true;
		}
		
		return ret;
	}
	public void irAtras(){
		super.onBackPressed();
	}
	
	@Override
	protected void onPause() {

		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[DetalleConsultaOtrosCreditosViewController] isMenuPressed -> "+isMenuPressed);
		if(TimerController.getInstance().isCerrandoSesion() && !isMenuPressed){
			if(Server.ALLOW_LOG) Log.d("Bebug>>", ">>>>>>>>>>>> onPause isMenuPressed"+isMenuPressed);
			parentManager.setCurrentActivityApp(this);
			parentManager.setActivityChanging(false);
			baseViewController.goBack(this, parentManager);
			if(Server.ALLOW_LOG) Log.d("Bebug>>", ">>>>>>>>>>>> onPause");
		}
		super.onPause();
		parentManager.consumeAccionesDePausa();
	}
	
	@Override
	public void onUserInteraction(){
		baseViewController.onUserInteraction();
	}

	public void consultarImportesPagoCredito(String numeroCredito) {
		//getBaseViewController().muestraIndicadorActividad(this, getString(R.string.alert_operation), getString(R.string.alert_connecting));
		//getBaseViewController().getParentViewsController().setActivityChanging(true);

		InitPagoCredito init = InitPagoCredito.getInstance(this, new BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		init.setCallBackSession(InitConsultaOtrosCreditos.getInstance().getConsultaSend().getCallBackSession());
		init.setCallBackAPI(this);
		init.configurar();

		init.consultarImportes(numeroCredito);
	}

	public void mostrarPagoCredito(ImportesPagoCreditoData importesPagoCreditoData) {
		InitPagoCredito init = InitPagoCredito.getInstance(this, new BanderasServer(ServerCommons.SIMULATION, ServerCommons.ALLOW_LOG, ServerCommons.EMULATOR, ServerCommons.DEVELOPMENT));
		//getBaseViewController().getParentViewsController().setActivityChanging(true);
		init.setCallBackSession(InitConsultaOtrosCreditos.getInstance().getConsultaSend().getCallBackSession());
		init.setCallBackAPI(this);

		Credito creditos = new Credito();
		creditos.setAdeudo(delegate.getCreditoSeleccionado().getAdeudo());
		creditos.setEstatusCredito(delegate.getCreditoSeleccionado().getEstatusCredito());
		creditos.setNumeroCredito(delegate.getCreditoSeleccionado().getNumeroCredito());
		creditos.setTipoCredito(delegate.getCreditoSeleccionado().getTipoCredito());

		init.configurar();
		init.setImportesAndCredito(importesPagoCreditoData, creditos);

		final Intent intent = new Intent(this,
				PagarCreditoViewController.class);
		this.startActivity(intent);
	}

	@Override
	public void returnToPrincipal() {

	}

	@Override
	public void returnDataFromModule(String operation, ServerResponse response) {
		getBaseViewController().ocultaIndicadorActividad();
		//getBaseViewController().getParentViewsController().setActivityChanging(false);
		if (Integer.parseInt(operation) == ApiConstants.OP_CONSULTA_IMPORTES_PAGO_CREDITOS) {
			if (response.getStatus() == bancomer.api.common.io.ServerResponse.OPERATION_SUCCESSFUL) {
				ImportesPagoCreditoData importesPagoCredito = (ImportesPagoCreditoData) response.getResponse();
				mostrarPagoCredito(importesPagoCredito);
			} else if (response.getStatus() == bancomer.api.common.io.ServerResponse.OPERATION_WARNING) {

			} else if (response.getStatus() == bancomer.api.common.io.ServerResponse.OPERATION_ERROR) {
				getBaseViewController().showInformationAlertEspecial(this, getString(R.string.label_error), response.getMessageCode(), response.getMessageText(), null);
			}
		}
	}

	@Override
	public void cierraSesion() {

	}

	@Override
	public void cierraSesionBackground(Boolean isChanging) {

	}

	@Override
	public void userInteraction() {

	}

	@Override
	public void returnMenuPrincipal() {

	}
}
