package bancomer.api.consultaotroscreditos.implementations;


import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import bancomer.api.common.commons.GuiTools;
import bancomer.api.common.gui.controllers.BaseViewController;
import bancomer.api.common.gui.controllers.BaseViewsController;
import bancomer.api.common.gui.delegates.BaseDelegate;
import bancomer.api.common.io.ServerResponse;
import bancomer.api.common.timer.TimerController;
import bancomer.api.consultaotroscreditos.R;
import bancomer.api.consultaotroscreditos.io.Server;


public class BaseViewControllerImpl implements BaseViewController{

	private Activity activity;
	private BaseDelegate delegate;
	private boolean habilitado = true;
	/**
     * The alert error or information dialog.
     */
    private AlertDialog mAlertDialog;
	private int activityParameters;
	private LinearLayout mHeaderLayout;
	private LinearLayout mTitleLayout;
	private ImageView mTitleDivider;
	private ViewGroup mBodyLayout;
	private EditText[] sFields;
	private Dialog mProgressDialog;
    private ArrayList<Integer> opcionesMenu;
	
	public ArrayList<Integer> getOpcionesMenu() {
		return opcionesMenu;
	}

	public void setOpcionesMenu(ArrayList<Integer> opcionesMenu) {
		this.opcionesMenu = opcionesMenu;
	}

	@Override
	public void onCreate(Activity activity, int parameters, int layoutID) {

		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[BaseViewController] Se llama a OnCreate de BaseViewController");
		
		this.setActivity(activity);
		activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN); 
		activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
    	activityParameters = parameters;
    	activity.setContentView(R.layout.api_consulta_otros_creditos_layout_base_activity);
    	
    	// references to ui elements
        mHeaderLayout = (LinearLayout) activity.findViewById(R.id.header_layout);
        mTitleLayout = (LinearLayout) activity.findViewById(R.id.title_layout);
        mTitleDivider = (ImageView) activity.findViewById(R.id.title_divider);
        mBodyLayout = (ViewGroup) activity.findViewById(R.id.body_layout);
        
        // 	set header visibility
        mHeaderLayout.setVisibility(((activityParameters&SHOW_HEADER)==SHOW_HEADER)?View.VISIBLE:View.GONE);
        //set title visibility, by default is gone until a call to aplicaEstilo is made
        mTitleLayout.setVisibility(View.GONE);
        mTitleDivider.setVisibility(View.GONE);
        
        // load received layout id in the body
        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layoutInflater.inflate(layoutID, mBodyLayout, true);
        
        // Alberto
//        bm = SuiteApp.getInstance().getBmovilApplication().getBmovilViewsController();
//        st = SuiteApp.getInstance().getSofttokenApplication().getSottokenViewsController();
        
        
        /* ---------------------------------------------------- */
        scaleForCurrentScreen(activity);
	}

	@Override
	public void setTitle(Activity activity, int titleText, int iconResource) {

		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[BaseViewController] Se llama a SetTitle de BaseViewController ");
		setTitle(activity, titleText, iconResource, R.color.segundo_azul);
	}

	@Override
	public void setTitle(Activity activity, int titleText, int iconResource,
			int colorResource) {
		// TODO Auto-generated method stub
		if ((activityParameters&SHOW_TITLE)==SHOW_TITLE) {
        	mTitleLayout.setVisibility(View.VISIBLE);
        	ImageView icon = (ImageView)mTitleLayout.findViewById(R.id.title_icon);
        	if(iconResource > 0){
        		icon.setImageDrawable(activity.getResources().getDrawable(iconResource));
        	}
        	if(titleText > 0){
        		String title = activity.getString(titleText);
        		TextView titleTextView = (TextView)mTitleLayout.findViewById(R.id.title_text);
        		titleTextView.setTextColor(activity.getResources().getColor(colorResource));
        		titleTextView.setText(title);
        	}
        	mTitleDivider.setVisibility(View.VISIBLE);
        } else {
        	mTitleLayout.setVisibility(View.GONE);
        	mTitleDivider.setVisibility(View.GONE);
        }
		
	}

	@Override
	public void processNetworkResponse(int operationId, ServerResponse response) {
		// TODO Auto-generated method stub
		delegate.analyzeResponse(operationId, response);
		
	}

	@Override
	public EditText[] getFields() {
		// TODO Auto-generated method stub
		return sFields;
	}

	@Override
	public String getText(EditText textField) {
		// TODO Auto-generated method stub
		if(textField != null){
			return textField.getText().toString().trim();
		} else {
			return "";
		}
	}

	@Override
	public float getHeaderHeight() {
		// TODO Auto-generated method stub
		mHeaderLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		mTitleLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		mTitleDivider.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		
		float headerHeight = mHeaderLayout.getMeasuredHeight() + mTitleLayout.getMeasuredHeight() +
								mTitleDivider.getMeasuredHeight();
		
		return headerHeight;
	}

	@Override
	public float getBodyHeight() {
		// TODO Auto-generated method stub
		mBodyLayout.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
		return mBodyLayout.getMeasuredHeight();
	}

	@Override
	public void setBodyLayout(Activity activity, int layoutResource) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater)
				activity.getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(layoutResource, mBodyLayout);
	}

	@Override
	public void hideSoftKeyboard(Activity activity) {
		// TODO Auto-generated method stub
		InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
    	View focusedView = activity.getCurrentFocus();
    	if(imm != null && focusedView != null){
    		imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
    	}
	}

	@Override
	public void goBack(Activity activity, BaseViewsController baseViewsController) {
		// TODO Auto-generated method stub
		hideSoftKeyboard(activity);
		// ALberto
    	baseViewsController.setActivityChanging(true);
    	activity.finish();
    	// ALberto
    	baseViewsController.overrideScreenBackTransition();
	}

	@Override
	public void showErrorMessage(Activity activity, int errorMessage) {
		// TODO Auto-generated method stub
		if (errorMessage > 0) {	
    		showErrorMessage(activity, activity.getString(errorMessage), null);
    	}
	}

	@Override
	public void showErrorMessage(Activity activity, String errorMessage) {
		// TODO Auto-generated method stub
		showErrorMessage(activity, errorMessage, null);
	}

	@Override
	public void muestraIndicadorActividad(final Activity activity,final String strTitle,final String strMessage) {
		this.activity.runOnUiThread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(mProgressDialog != null){
		    		ocultaIndicadorActividad();
		    	}	
				mProgressDialog = ProgressDialog.show(activity, strTitle, 
		    			strMessage, true);
				 mProgressDialog.setCancelable(false);
			}
			
		});
	}

	@Override
	public void ocultaIndicadorActividad() {
		// TODO Auto-generated method stub
		if(mProgressDialog != null){
    		mProgressDialog.dismiss();
    	}
	}

	@Override
	public void showErrorMessage(Activity activity, String errorMessage, OnClickListener listener) {
		// TODO Auto-generated method stub
		if(errorMessage.length() > 0){ 		
    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
    		alertDialogBuilder.setTitle(R.string.label_error);
    		alertDialogBuilder.setIcon(R.drawable.api_consulta_otros_creditos_anicalertaaviso);
    		alertDialogBuilder.setMessage(errorMessage);
    		alertDialogBuilder.setPositiveButton(R.string.label_ok, listener);
    		alertDialogBuilder.setCancelable(false);
        	mAlertDialog = alertDialogBuilder.show();		
    	}
	}

	
	@Override
	public boolean isPointInsideView(float x, float y, View view) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BaseViewsController getParentViewsController() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setParentViewsController(
			BaseViewsController parentViewsController) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public BaseDelegate getDelegate() {
		return delegate;
	}

	@Override
	public void setDelegate(BaseDelegate delegate) {

		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[BaseViewController] Se setea el delegate base de BaseViewController");
		this.delegate = delegate;
		
	}

	@Override
	public void hideCurrentDialog() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCurrentDialog(AlertDialog dialog) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void scaleForCurrentScreen(Activity activity) {
		// TODO Auto-generated method stub
		GuiTools guiTools = GuiTools.getCurrent();
        guiTools.init(activity.getWindowManager());
        
        guiTools.scale(mHeaderLayout);
		guiTools.scale(mTitleLayout);
		guiTools.scale(mTitleLayout.findViewById(R.id.title_icon));
		guiTools.scale(mTitleLayout.findViewById(R.id.title_text), true);
		guiTools.scale(mTitleDivider);
		guiTools.scale(mBodyLayout);
	}

	@Override
	public ScrollView getScrollView() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void moverScroll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void showInformationAlert(Activity activity, int message) {
		// TODO Auto-generated method stub
		if (message > 0) {		
    		showInformationAlert(activity, activity.getString(message));
    	}
	}

	@Override
	public void showInformationAlert(Activity activity, String message) {
		// TODO Auto-generated method stub
		if (message.length() > 0) {
    		showInformationAlert(activity, message, null);
    	}
	}

	@Override
	public void showInformationAlert(Activity activity, int message,
			OnClickListener listener) {
		// TODO Auto-generated method stub
		if (message > 0) {
    		showInformationAlert(activity, activity.getString(message), listener);
    	}
	}

	@Override
	public void showInformationAlert(Activity activity, String message,
			OnClickListener listener) {
		// TODO Auto-generated method stub
		showInformationAlert(activity, activity.getString(R.string.label_information), message, listener);
	}

	@Override
	public void showInformationAlert(Activity activity, int title, int message,
			OnClickListener listener) {
		// TODO Auto-generated method stub
		if(message > 0) {
    		showInformationAlert(activity, activity.getString(title), activity.getString(message), listener);
    	}
	}

	@Override
	public void showInformationAlert(Activity activity, String title,
			String message, OnClickListener listener) {
		// TODO Auto-generated method stub
    	showInformationAlert(activity, title, message, activity.getString(R.string.label_ok), listener);
	}

	@Override
	public void showInformationAlertEspecial(Activity activity, String title,
			String errorCode, String descripcionError, OnClickListener listener) {
		// TODO Auto-generated method stub
    	showInformationAlertEspecial(activity, title, errorCode, descripcionError, activity.getString(R.string.label_ok), listener);
	}

	@Override
	public void showInformationAlert(Activity activity, int title, int message,
			int okText, OnClickListener listener) {
		// TODO Auto-generated method stub
		if(message > 0) {
    		showInformationAlert(activity, activity.getString(title), activity.getString(message), activity.getString(okText), listener);
    	}
	}

	@Override
	public void showInformationAlert(Activity activity, String title,
			String message, String okText, OnClickListener listener) {
		// TODO Auto-generated method stub
		if(!habilitado)
			return;		
		habilitado = false;
    	if(message.length() > 0){
    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
    		alertDialogBuilder.setTitle(title);
    		alertDialogBuilder.setIcon(R.drawable.api_consulta_otros_creditos_anicalertaaviso);    		
    		alertDialogBuilder.setMessage(message);
    		if (null == listener) {
    			listener = new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				};
    		}

    		alertDialogBuilder.setPositiveButton(okText, listener);
    		alertDialogBuilder.setCancelable(false);
    		mAlertDialog = alertDialogBuilder.create();
    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					habilitado = true;
				}
			});
    		mAlertDialog.show();
    	}
	}

	@Override
	public void showInformationAlertEspecial(Activity activity, String title,
			String errorCode, String descripcionError, String okText,
			OnClickListener listener) {
		// TODO Auto-generated method stub
		if(!habilitado)
			return;		
		habilitado = false;
    	if(descripcionError.length() > 0){
    		
    		
    		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
    		builder.setTitle(title);
    		builder.setIcon(R.drawable.api_consulta_otros_creditos_anicalertaaviso);    	
    	    LayoutInflater inflater = activity.getLayoutInflater();
    	    View vista = inflater.inflate(R.layout.api_consulta_otros_creditos_layout_alert_codigo_error, null);
    	    /*builder.setView(vista)
    	       .setPositiveButton(okText, new DialogInterface.OnClickListener() {
    	           public void onClick(DialogInterface dialog, int id) {
    	                  dialog.dismiss();
    	           }
    	    });*/
    	    if (null == listener) {
    			listener = new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				};
    		}
    	    builder.setView(vista)
    	       .setPositiveButton(okText, listener);
    	    
    	    TextView text1 = (TextView) vista.findViewById(R.id.descripcionError);
    	    TextView text2 = (TextView) vista.findViewById(R.id.codigoError);
    	    text1.setText(descripcionError);
    	    text2.setText(errorCode);
    	 
    	    builder.setCancelable(false);
    		
    	    mAlertDialog = builder.create();
    	    
    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					habilitado = true;
				}
			});
    		mAlertDialog.show();
    	    
    	}
	}

	@Override
	public void showYesNoAlert(Activity activity, String message,
			OnClickListener positiveListener) {
		// TODO Auto-generated method stub
		showYesNoAlert(activity, activity.getString(R.string.label_information), 
				   message, 
				   activity.getString(R.string.common_alert_yesno_positive_button), 
				   activity.getString(R.string.common_alert_yesno_negative_button), 
				   positiveListener, 
				   null);
	}

	@Override
	public void showYesNoAlertEspecialTitulo(Activity activity,
			String errorCode, String descripcionError,
			OnClickListener positiveListener) {
		// TODO Auto-generated method stub
		showYesNoAlertEspecial(activity, activity.getString(R.string.label_information), 
			 	   errorCode,
			 	   descripcionError,
			 	   activity.getString(R.string.common_alert_yesno_positive_button), 
			 	   activity.getString(R.string.common_alert_yesno_negative_button), 
				   positiveListener, 
				   null);
	}

	@Override
	public void showYesNoAlert(Activity activity, int message,
			OnClickListener positiveListener) {
		// TODO Auto-generated method stub
		showYesNoAlert(activity, R.string.label_information, 
				   message, 
				   R.string.common_alert_yesno_positive_button, 
				   R.string.common_alert_yesno_negative_button, 
				   positiveListener, 
				   null);
	}

	@Override
	public void showYesNoAlert(Activity activity, int message,
			OnClickListener positiveListener, OnClickListener negativeListener) {
		// TODO Auto-generated method stub
		showYesNoAlert(activity, R.string.label_information, 
				   message, 
				   R.string.common_alert_yesno_positive_button, 
				   R.string.common_alert_yesno_negative_button, 
				   positiveListener, 
				   negativeListener);
	}

	@Override
	public void showYesNoAlert(Activity activity, String title, String message,
			OnClickListener positiveListener) {
		// TODO Auto-generated method stub
		showYesNoAlert(activity, title, 
				   message, 
				   activity.getString(R.string.common_alert_yesno_positive_button), 
				   activity.getString(R.string.common_alert_yesno_negative_button), 
				   positiveListener, 
				   null);
	}

	@Override
	public void showYesNoAlert(Activity activity, String message,
			OnClickListener positiveListener, OnClickListener negativeListener) {
		// TODO Auto-generated method stub
		showYesNoAlert(activity, "", 
				   message, 
				   activity.getString(R.string.common_alert_yesno_positive_button), 
				   activity.getString(R.string.common_alert_yesno_negative_button), 
				   positiveListener, 
				   negativeListener);
	}

	@Override
	public void showYesNoAlertEspecialTitulo(Activity activity,
			String errorCode, String descripcionError,
			OnClickListener positiveListener, OnClickListener negativeListener) {
		// TODO Auto-generated method stub
		showYesNoAlertEspecial(activity, activity.getString(R.string.label_information), 
				   errorCode,
				   descripcionError,
				   activity.getString(R.string.common_alert_yesno_positive_button), 
				   activity.getString(R.string.common_alert_yesno_negative_button), 
				   positiveListener, 
				   negativeListener);
	}

	@Override
	public void showYesNoAlert(Activity activity, int title, int message,
			OnClickListener positiveListener) {
		// TODO Auto-generated method stub
		showYesNoAlert(activity, title, 
				   message, 
				   R.string.common_alert_yesno_positive_button, 
				   R.string.common_alert_yesno_negative_button, 
				   positiveListener, 
				   null);
	}

	@Override
	public void showYesNoAlert(Activity activity, String title, String message,
			String okText, OnClickListener positiveListener) {
		// TODO Auto-generated method stub
		showYesNoAlert(activity, title, message, okText, activity.getString(R.string.common_alert_yesno_negative_button), positiveListener, null);
	}

	@Override
	public void showYesNoAlert(Activity activity, int title, int message,
			int okText, OnClickListener positiveListener) {
		// TODO Auto-generated method stub
		showYesNoAlert(activity, title, message, okText, R.string.common_alert_yesno_negative_button, positiveListener, null);
	}

	@Override
	public void showYesNoAlert(Activity activity, String title, String message,
			String okText, String calcelText, OnClickListener positiveListener) {
		// TODO Auto-generated method stub
		showYesNoAlert(activity, title, message, okText, calcelText, positiveListener, null);
	}

	@Override
	public void showYesNoAlert(Activity activity, int title, int message,
			int okText, int calcelText, OnClickListener positiveListener) {
		// TODO Auto-generated method stub
		showYesNoAlert(activity, title, message, okText, calcelText, positiveListener, null);
	}

	@Override
	public void showYesNoAlert(Activity activity, int title, int message,
			int okText, int calcelText, OnClickListener positiveListener,
			OnClickListener negativeListener) {
		// TODO Auto-generated method stub
		showYesNoAlert(activity, activity.getString(title), activity.getString(message), activity.getString(okText), activity.getString(calcelText), positiveListener, negativeListener);
	}

	@Override
	public void showYesNoAlert(Activity activity, String title, String message,
			String okText, String calcelText, OnClickListener positiveListener,
			OnClickListener negativeListener) {
		// TODO Auto-generated method stub
		if(!habilitado)
			return;		
		habilitado = false;
    	if(message.length() > 0){
    		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
    		alertDialogBuilder.setTitle(title);
    		alertDialogBuilder.setIcon(R.drawable.api_consulta_otros_creditos_anicalertaaviso);
    		alertDialogBuilder.setMessage(message);
    		alertDialogBuilder.setPositiveButton(okText, positiveListener);
    		
    		if(null == negativeListener) {
    			alertDialogBuilder.setNegativeButton(calcelText, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						habilitado = true;
						dialog.dismiss();
					}
				});
    		} else {
    			alertDialogBuilder.setNegativeButton(calcelText, negativeListener);
    		}
    		
    		alertDialogBuilder.setCancelable(false);
    		mAlertDialog = alertDialogBuilder.create();
    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					habilitado = true;
				}
			});
    		mAlertDialog.show();
	    	
    	}
	}

	@Override
	public void showYesNoAlertEspecial(Activity activity, String title,
			String errorCode, String descripcionError, String okText,
			String calcelText, OnClickListener positiveListener,
			OnClickListener negativeListener) {
		// TODO Auto-generated method stub
		if (!habilitado)
			return;
		habilitado = false;
		if (descripcionError.length() > 0) {
			
			AlertDialog.Builder builder = new AlertDialog.Builder(activity);
    		builder.setTitle(title);
    		builder.setIcon(R.drawable.api_consulta_otros_creditos_anicalertaaviso);    	
    	    LayoutInflater inflater = activity.getLayoutInflater();
    	    View vista = inflater.inflate(R.layout.api_consulta_otros_creditos_layout_alert_codigo_error, null);
    	    builder.setView(vista);
    	    builder.setPositiveButton(okText, positiveListener);
    	    
    	    if (null == negativeListener) {
    	    	builder.setNegativeButton(calcelText,
						new OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								habilitado = true;
								dialog.dismiss();
							}
						});
			} else {
				builder.setNegativeButton(calcelText,
						negativeListener);
			}
    	    
    	    TextView text1 = (TextView) vista.findViewById(R.id.descripcionError);
    	    TextView text2 = (TextView) vista.findViewById(R.id.codigoError);
    	    text1.setText(descripcionError);
    	    text2.setText(errorCode);
    	 
    	    builder.setCancelable(false);
    		
    	    mAlertDialog = builder.create();
    	    
    		mAlertDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
					habilitado = true;
				}
			});
    		mAlertDialog.show();
			

		}
	}

	@Override
	public void setHabilitado(boolean habilitado) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isHabilitado() {
		// TODO Auto-generated method stub
		return false;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {

		if(Server.ALLOW_LOG) Log.i("[CGI-Configuracion-Obligatorio] >> ", "[BaseViewController] Se setea el activity base de BaseViewController");
		this.activity = activity;
	}
	
	/**
	 * MENU THINGS
	 * @param menu
	 * @return
	 */
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = this.getActivity().getMenuInflater();
		inflater.inflate(R.menu.api_consulta_otros_creditos_menu_bmovil_resultados, menu);
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		
//		int opcionesMenu = resultadosDelegate.getOpcionesMenuResultados();
		boolean showMenu = false;
		if (!opcionesMenu.contains(SHOW_MENU_SMS)) {
			menu.removeItem(R.id.save_menu_sms_button);
		} else {
			showMenu = true;
		}
		//TODO remover para mostrar menu completo
		if (!opcionesMenu.contains(SHOW_MENU_EMAIL)) {
			menu.removeItem(R.id.save_menu_email_button);
		} else {
			showMenu = true;
			//menu.removeItem(R.id.save_menu_email_button);//TODO remover para mostrar menu completo
		}
		//TODO remover para mostrar menu completo
		if (!opcionesMenu.contains(SHOW_MENU_PDF)) {
			menu.removeItem(R.id.save_menu_pdf_button);
		} else {
			//showMenu = true;
			menu.removeItem(R.id.save_menu_pdf_button);//TODO remover para mostrar menu completo
		}
		if (!opcionesMenu.contains(SHOW_MENU_FRECUENTE)) {
			menu.removeItem(R.id.save_menu_frecuente_button);
		} else {
			showMenu = true;
		}
		//TODO remover para mostrar menu completo
		if (!opcionesMenu.contains(SHOW_MENU_RAPIDA)) {
			menu.removeItem(R.id.save_menu_rapida_button);
		} else {
			//showMenu = true;
			menu.removeItem(R.id.save_menu_rapida_button);//TODO remover para mostrar menu completo
		}
		if (!opcionesMenu.contains(SHOW_MENU_BORRAR)) {
			menu.removeItem(R.id.save_menu_borrar_button);
		} else {
			showMenu = true;
		}
		
		return showMenu;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return false;
	}


	public void onUserInteraction() {
		TimerController.getInstance().resetTimer();
//		TimerManager.resetLogoutTimerCallback();
//		TimerManager.setResetTimer(true);	
	}
}
